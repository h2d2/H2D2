C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C      FUNCTION NS_MACRO3D_CL001_COD()
C      FUNCTION NS_MACRO3D_CL001_PRC()
C      FUNCTION NS_MACRO3D_CL001_CLC()
C      FUNCTION NS_MACRO3D_CL001_ASMF()
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_CL001_COD
C
C Description:
C     La sous-routine privée NS_MACRO3D_CL001_COD impose tous les degrés de
C     liberté comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C
C Sortie:
C     KDIMP          Codes des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL001_COD(IL,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL001_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL001_COD
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND) !pas utilisé
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV) ! pas utilisé
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE) ! pas utilisé
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, ID, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
         ENDDO
      ENDDO

      NS_MACRO3D_CL001_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL001_PRC
C
C Description:
C     La sous-routine privée NS_MACRO3D_CL001_PRC impose tous les degrés de
C     liberté comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C     KDIMP          Codes des ddl imposés
C
C Sortie:
C     VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL001_PRC (  IL,
     &                                 KCLCND,
     &                                 VCLCNV,
     &                                 KCLLIM,
     &                                 KCLNOD,
     &                                 KCLELE,
     &                                 KDIMP,
     &                                 VDIMP )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL001_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL001_PRC
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE) ! pas utilisé
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL) !pas utilisé
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, ID, IN, INDEB, INFIN, IV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)

CCC??? à checker...
!!!D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. LM_CMMN_NDLN)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IV = KCLCND(3,IC)
         DO ID=1,LM_CMMN_NDLN
            VDIMP(ID,IN) = VCLCNV(IV)
            IV = IV + 1
         ENDDO
      ENDDO

      NS_MACRO3D_CL001_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL001_CLC
C
C Description:
C     La fonction NS_MACRO3D_CL001_CLC est vide.
C
C Entrée:
C
C Sortie:
C
C Notes: Pas encore vérifiée
C************************************************************************
      FUNCTION NS_MACRO3D_CL001_CLC(IL,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO, !pas utilisé
     &                           VPREV,
     &                           VPRES,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL001_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL001_CLC
      INTEGER IL !pas utilisé
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)  !pas utilisé
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)  !pas utilisé
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)  !pas utilisé
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)  !pas utilisé
      REAL*8  VPRGL (LM_CMMN_NPRGL)                !pas utilisé
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)   !pas utilisé
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)  !pas utilisé
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)  !pas utilisé
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)  !pas utilisé
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)  !pas utilisé
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)  !pas utilisé
      INTEGER KCLELE(    EG_CMMN_NCLELE)  !pas utilisé
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL) !pas utilisé
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL) !pas utilisé
      INTEGER KEIMP (EG_CMMN_NELS)              !pas utilisé
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      NS_MACRO3D_CL001_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL001_ASMF
C
C Description:
C     La fonction NS_MACRO3D_CL001_ASMF est vide.
C
C Entrée:
C
C Sortie:
C
c Notes: pas encore vérifiée
C************************************************************************
      FUNCTION NS_MACRO3D_CL001_ASMF(IL,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO, !pas utilisé
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL001_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL001_ASMF
      INTEGER IL   !pas utilisé
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL) !pas utilisé
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL) !pas utilisé
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      NS_MACRO3D_CL001_ASMF = ERR_TYP()
      RETURN
      END
