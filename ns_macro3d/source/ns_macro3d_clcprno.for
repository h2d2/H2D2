C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C     CALCUL DES PROPRIETES NODALES DÉPENDANT DE LA SOLUTION (DS)
C
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Voir VPRNO.doc pour description des propriétés nodales.
C     l'imposition des noeuds milieux devrait être faite dans une
C     fonction séparée, pour bien séparer les concepts
C    3 boucles sur les éléments pourraient être regroupées
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRNO ( VCORG,
     &                                KNGV,
     &                                VDJV, ! pas utilisé
     &                                VPRGL,
     &                                VPRNO,
     &                                VDLG,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV) ! pas utilisé
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6      !connectivités élémentaires
      REAL*8 prnoPrfaN1, prnoPrfaN3, prnoPrfaN5 !Prfa lues
      REAL*8 prnoPrfaN2, prnoPrfaN4, prnoPrfaN6 !Pfra imposées
      INTEGER IDLH !indice du degré de liberté H

C---  Paramètres pour le calcul des vitesses vertical
C     Les appels et les variables suivent le modèle de asmk
      INTEGER II, NO !IC, IE, IN

      INTEGER NDJV_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NDIM_LCL
      INTEGER NNELV_LCL

      PARAMETER (NDJV_LCL = 5)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NNELV_LCL = 6)

      INTEGER KNE(6)
      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL, 6)
      REAL*8   VCORE(NDIM_LCL, NNELV_LCL)
!      REAL*8  PENA
C---  Paramètres supplémentairse pour le calcul des vitesses vertical
      REAL*8 WF(NNELV_LCL), WS(NNELV_LCL)

      INTEGER IQF
      INTEGER IQS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDJV_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------
      IQF = NS_IPRNO_QF
      IQS = NS_IPRNO_QS

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, II, NO, NO1, NO2, NO3, NO4, NO5, NO6,
!$omp& prnoPrfaN1, prnoPrfaN3, prnoPrfaN5, prnoPrfaN2, prnoPrfaN4,
!$omp& prnoPrfaN6, IDLH)

C---     IMPOSE LES NIVEAUX D'EAU MILIEUX
      DO IC=1,EG_CMMN_NELCOL
!$omp do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         IDLH = NS_IDL_H

         VDLG(IDLH,NO2) = (VDLG(IDLH,NO1)+VDLG(IDLH,NO3))*UN_2
         VDLG(IDLH,NO4) = (VDLG(IDLH,NO3)+VDLG(IDLH,NO5))*UN_2
         VDLG(IDLH,NO6) = (VDLG(IDLH,NO5)+VDLG(IDLH,NO1))*UN_2
      ENDDO
!$omp end do
      ENDDO

C---     BOUCLE SUR LES NOEUDS
!$omp do
      DO IN=1,EG_CMMN_NNL
         CALL NS_MACRO3D_CLCPRN_1N_YSe( VPRGL,
     &                                  VPRNO(1,IN),
     &                                  VDLG(1,IN),
     &                                  IERR)
      ENDDO
!$omp end do

C---     PROFONDEUR MILIEUX
      DO IC=1,EG_CMMN_NELCOL
!$omp do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         prnoPrfaN1 = VPRNO(NS_IPRNO_PRFA,NO1)
         prnoPrfaN3 = VPRNO(NS_IPRNO_PRFA,NO3)
         prnoPrfaN5 = VPRNO(NS_IPRNO_PRFA,NO5)

         prnoPrfaN2 = (prnoPrfaN1+prnoPrfaN3)*UN_2
         prnoPrfaN4 = (prnoPrfaN3+prnoPrfaN5)*UN_2
         prnoPrfaN6 = (prnoPrfaN5+prnoPrfaN1)*UN_2

         VPRNO(NS_IPRNO_PRFA,NO2) = prnoPrfaN2
         VPRNO(NS_IPRNO_PRFA,NO4) = prnoPrfaN4
         VPRNO(NS_IPRNO_PRFA,NO6) = prnoPrfaN6

         VPRNO(IQF, NO1) = ZERO
         VPRNO(IQF, NO2) = ZERO
         VPRNO(IQF, NO3) = ZERO
         VPRNO(IQF, NO4) = ZERO
         VPRNO(IQF, NO5) = ZERO
         VPRNO(IQF, NO6) = ZERO

         VPRNO(IQS, NO1) = ZERO
         VPRNO(IQS, NO2) = ZERO
         VPRNO(IQS, NO3) = ZERO
         VPRNO(IQS, NO4) = ZERO
         VPRNO(IQS, NO5) = ZERO
         VPRNO(IQS, NO6) = ZERO

      ENDDO ! Boucle sur les éléments
!$omp end do
      ENDDO ! Boucle sur les ELCOL ?MGia c'est quoi ?
!$omp end parallel

      CALL NS_MACRO3D_CLCPRNOQFS ( VCORG,
     &                             KNGV,
     &                             VDJV, ! pas utilisé
     &                             VPRGL,
     &                             VPRNO, ! sortie
     &                             VDLG,
     &                             IERR   )



      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRNE
C
C Description:
C     Calcul des propriétés nodales élémentaires dépendantes de VDLG
C
C Entrée:
C
C Sortie: Profondeur noeuds milieux et niveaux d'eau noeuds milieux imposés
C
C Notes:
C     Le calcul des vitesses VPRN(7:8) sur les noeuds milieux est fait
C     avec les prof locales qui ne sont pas les moyennes des prof des sommets.
C     Est-ce une erreur, une inconsistance?--- n'a plus trop de sens sur sv2d --
C     Fonction appelée lors du calcul des propriétés nodales pertubées
C     dans asmkt.
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRNE (VPRGL,
     &                             VPRNE,
     &                             VDLE,
     &                             IERR)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNE(LM_CMMN_NPRNO, EG_CMMN_NCELV)
      REAL*8  VDLE (LM_CMMN_NDLN, EG_CMMN_NCELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER IN
      REAL*8 prnoPrfaN1, prnoPrfaN3, prnoPrfaN5 !Prfa lues
      INTEGER IDLH ! indice du degré de liberté H dans VDLE
C-----------------------------------------------------------------------

C---     CONNECTIVITÉ DU T6
      NO1 = 1
      NO2 = 2
      NO3 = 3
      NO4 = 4
      NO5 = 5
      NO6 = 6

C---     IMPOSE LES NIVEAUX D'EAU MILIEUX
      IDLH = NS_IDL_H
      VDLE(IDLH,NO2) = (VDLE(IDLH,NO1)+VDLE(IDLH,NO3))*UN_2
      VDLE(IDLH,NO4) = (VDLE(IDLH,NO3)+VDLE(IDLH,NO5))*UN_2
      VDLE(IDLH,NO6) = (VDLE(IDLH,NO5)+VDLE(IDLH,NO1))*UN_2

C---     BOUCLE SUR LES NOEUDS
      DO IN=1,EG_CMMN_NNELV
         CALL NS_MACRO3D_CLCPRN_1N_YSe( VPRGL,
     &                                  VPRNE(1,IN),
     &                                  VDLE(1,IN),
     &                                  IERR)
      ENDDO

C---     IMPOSE LES PROFONDEURS MILIEUX
      prnoPrfaN1 = VPRNE(NS_IPRNO_PRFA,NO1)
      prnoPrfaN3 = VPRNE(NS_IPRNO_PRFA,NO3)
      prnoPrfaN5 = VPRNE(NS_IPRNO_PRFA,NO5)

      VPRNE(NS_IPRNO_PRFA,NO2) = ( prnoPrfaN1 + prnoPrfaN3 )*UN_2
      VPRNE(NS_IPRNO_PRFA,NO4) = ( prnoPrfaN3 + prnoPrfaN5 )*UN_2
      VPRNE(NS_IPRNO_PRFA,NO6) = ( prnoPrfaN5 + prnoPrfaN1 )*UN_2

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRN_1N_YS
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRGL       Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C      REAL*8  VPRN        Les Propriétés Nodales lues 1, 2 , 3, soient
C                          prnoZf, prnoCoefMan, prnoGlace
C
C
C Sortie:
C      REAL*8  VPRN Les PRopriétés Nodales calculées 9, 10, 11, 12, 13 ,14, 15, 16
C                   prnoprnoPrfa, prnoPoro, prnoFrot, prnoConv, prnoDiff, prnoDecou, prnoWs, prnoWf

C      INTEGER IERR
C
C Notes: La fonction porte la mention YSe en référence à son pendant 2d,
C        mais a été modifiée par MGia pour un ns_macro3d
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRN_1N_YSe( VPRGL,
     &                                     VPRN,
     &                                     VDLN,
     &                                     IERR)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO)
      REAL*8  VDLN (LM_CMMN_NDLN)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      REAL*8  uF, vF, h                                       !VDLN lu
      REAL*8  prnoZf, prnoCoefMan, prnoGlace                  !VPRN lu
      REAL*8  EPAIGL, PROF, PRFE, UN_PRFA, VMAN, QMOD, HRAP   !Variables
      REAL*8  FCVT, FDIF, FDRC                                !Facteurs
      REAL*8  prnoPrfa, prnoPoro, prnoFrot!, prnoZs           !VPRN assigné
      REAL*8  prnoConv, prnoDiff, prnoDecou                   !VPRN assigné(suite)
 !     REAL*8  prnoWf, prnoWs                                 !VPRN assigné (suite)
      REAL*8  UN_TIER
      PARAMETER (UN_TIER = 1.0D0/3.0D0)                       !MGia 7/3 ou 1/3 ??
C-----------------------------------------------------------------------

C---  Assignations
      prnoZf = VPRN(NS_IPRNO_ZF)
      prnoCoefMan = VPRN(NS_IPRNO_COEFMAN)
      prnoGlace = VPRN(NS_IPRNO_GLACE)

      uF = VDLN(NS_IDL_UF)
      vF = VDLN(NS_IDL_VF)
      h = VDLN(NS_IDL_H)

C---       PROFONDEUR
      EPAIGL    = 0.9D0 * prnoGlace
      PROF      = h - prnoZf
      EPAIGL    = MIN(EPAIGL, PROF)
      EPAIGL    = MAX(EPAIGL, ZERO)                  ! 0 < EPAIGL < PROF
      PRFE      = PROF - EPAIGL                      ! Prof effective
      prnoPrfa  = MAX(PRFE, NS_MACRO3D_DECOU_HMIN)   ! Prof absolue
      UN_PRFA   = UN / prnoPrfa                      ! Inverse prof absolue

C---        PARAMETRES VARIABLES POUR LE DECOUVREMENT
      IF (PRFE .GE. NS_MACRO3D_DECOU_HMIN) THEN   ! Non découvert
C        HRAP = UN
         prnoPoro = UN                            ! Porosité normale
         VMAN = NS_MACRO3D_CMULT_MAN*prnoCoefMan  ! Manning normal
         FCVT = UN                                ! Facteur de convection
         FDIF = ZERO                              ! Facteur de diffusion
         FDRC = ZERO                              ! Facteur de Darcy

      ELSEIF (PRFE .LE. ZERO) THEN                ! Découvert
C        HRAP = ZERO
         prnoPoro = (UN - NS_MACRO3D_DECOU_PORO)      ! Porosite de découvrement
         VMAN = NS_MACRO3D_DECOU_MAN              ! Manning de découvrement
         FCVT = NS_MACRO3D_DECOU_CON_FACT         ! Facteur de convection
         FDIF = UN                                ! Facteur de diffusion
         FDRC = UN                                ! Facteur de Darcy

      ELSE                                        ! Transition linéaire
         HRAP = PRFE / NS_MACRO3D_DECOU_HMIN
         prnoPoro = (UN - NS_MACRO3D_DECOU_PORO)
     &        + HRAP*NS_MACRO3D_DECOU_PORO
         VMAN = NS_MACRO3D_CMULT_MAN*prnoCoefMan
         VMAN = NS_MACRO3D_DECOU_MAN
     &        + HRAP*(VMAN - NS_MACRO3D_DECOU_MAN)
         FCVT = NS_MACRO3D_DECOU_CON_FACT
     &        + HRAP*(UN - NS_MACRO3D_DECOU_CON_FACT)
         FDIF = HRAP
         FDRC = HRAP
      ENDIF

C---        Pour les termes de frottement
!      CALL LOG_TODO('AJOUTER À MODULE DES VITESSES WF*WF')
      QMOD = SQRT(uF*uF + vF*vF ) !+wf*WF
      prnoFrot= NS_MACRO3D_GRAVITE *
     &                        QMOD *
     &                        VMAN*VMAN *
     &                        (UN_PRFA**UN_TIER) !MGia J'arrive à H**1/3 plutôt que 7/3. Trouver d'où vient le 1/H^2 en trop...

C---     Coefficient d'inclusion/omission
      prnoConv  = FCVT*NS_MACRO3D_CMULT_CON
      prnoDiff  = FDIF*NS_MACRO3D_DECOU_DIF_NU
      prnoDecou = FDRC*NS_MACRO3D_DECOU_DRC_NU

C---     Assignation propriétés nodales
      VPRN(NS_IPRNO_PRFA) = prnoPrfa    ! Prof absolue
      VPRN(NS_IPRNO_PORO) = prnoPoro    ! Porosité pour le découvrement
      VPRN(NS_IPRNO_FROT) = prnoFrot    ! g n2 |q| / H**(1/3) !MGia ??->le 1/h^2 de trop vient p-etre du eta^2
      VPRN(NS_IPRNO_CONV) = prnoConv
      VPRN(NS_IPRNO_DIFF) = prnoDiff
      VPRN(NS_IPRNO_DECOU)= prnoDecou
      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRN_1N_MHe
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRGL       Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     Version Mourad Heniche
C************************************************************************
!      SUBROUTINE NS_MACRO3D_CLCPRN_1N_MHe(VPRGL,
!     &                                  VPRN,
!     &                                  VDLN,
!     &                                  IERR)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      REAL*8  VPRGL(LM_CMMN_NPRGL)
!      REAL*8  VPRN (LM_CMMN_NPRNO)
!      REAL*8  VDLN (LM_CMMN_NDLN)
!      INTEGER IERR
!
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!      INCLUDE 'ns_macro3d.fc'
!
!      REAL*8  BATHY, EPAIGL, PROF
!      REAL*8  PRFE, PRFA, UN_PRFA
!      REAL*8  FROTT, VMAN, PORO, QMOD, FCVT, FVIS
!
!      REAL*8  SEPT_TIER
!      PARAMETER (SEPT_TIER = 7.0D0/3.0D0)
!C-----------------------------------------------------------------------
!
!C---       PROFONDEUR
!      BATHY   = VPRN(1)
!      EPAIGL  = 0.9D0 * VPRN(3)
!      PROF    = VDLN(3) - BATHY
!      EPAIGL  = MIN(EPAIGL, PROF)
!      EPAIGL  = MAX(EPAIGL, ZERO)
!      PRFE    = PROF - EPAIGL                ! Prof effective
!      PRFA    = MAX(ABS(PRFE), NS_MACRO3D_DECOU_HMIN)   ! Prof absolue ABS superflu mais aide lecture
!      UN_PRFA = UN / PRFA                    ! Inverse prof absolue
!
!C---        PARAMETRES VARIABLES POUR LE DECOUVREMENT
!      IF (PRFE .GE. ZERO) THEN               ! Non découvert
!         PORO = UN                           !   Porosité normale
!         VMAN = NS_MACRO3D_CMULT_MAN*VPRN(2)       !   Manning normal
!         FCVT = UN                           !   Limiteur de convection
!         FVIS = UN
!      ELSE                                   ! Découvert
!         PORO = (UN - NS_MACRO3D_DECOU_PORO)       !   Porosite de découvrement
!         VMAN = VPRN(2)*(NS_MACRO3D_CMULT_MAN +
!     &                   10.0D0*NS_MACRO3D_DECOU_MAN*PRFA)
!         FCVT = UN                           !   Limiteur de convection
!         FVIS = UN
!      ENDIF
!
!C---        POUR LES TERMES DE FROTTEMENT
!      QMOD = SQRT(VDLN(1)*VDLN(1) + VDLN(2)*VDLN(2))
!      FROTT= NS_MACRO3D_GRAVITE*QMOD * VMAN*VMAN * (UN_PRFA**SEPT_TIER)
!
!C---        VALEURS NODALES
!      VPRN( 7) = VDLN(1) * UN_PRFA        ! U
!      VPRN( 8) = VDLN(2) * UN_PRFA        ! V
!      VPRN( 9) = PRFA                     ! Prof absolue
!      VPRN(10) = PORO
!      VPRN(11) = FROTT                    ! g n2 |q| / H**(7/3)
!      VPRN(12) = NS_MACRO3D_CMULT_CON*FCVT
!      VPRN(13) = FVIS
!      VPRN(14) = FVIS
!!      VPRN(13) = NS_MACRO3D_CMULT_GRA
!!      VPRN(14) = NS_MACRO3D_CMULT_VENT
!
!      RETURN
!      END

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRNOQFS
C
C Description:
C     Calcul les propriétés nodales de débits au fond et en surface
C
C Entrée :  VCORG :  Table des coorodnnées globales
C           KNGV  :  Table des connectivités globales de volume
C           VDJV  :  Table des métriques (utilise det)
C           VPRGL :  Table des propriétés globales (pas utilisées)
C           VDLG  :  Table des degrés de libertés
C
C
C Sortie: VPRNO : modification de PRNOQF et PRNOQS
C         IERR
C
C Notes:  Respecte la forme de ns_macro3d_asmk pour faciliter un déplacement
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRNOQFS (  VCORG,
     &                                    KNGV,
     &                                    VDJV,
     &                                    VPRGL,
     &                                    VPRNO,
     &                                    VDLG,
     &                                    IERR  )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'

C---  Paramètres pour le calcul des vitesses vertical
C     Les appels et les variables suivent le modèle de asmk
      INTEGER II, NO, IC, IE, IN

      INTEGER NDJV_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NDIM_LCL
      INTEGER NNELV_LCL


      PARAMETER (NDJV_LCL = 5)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NNELV_LCL = 6)

      INTEGER KNE(6)
      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL, 6)
      REAL*8  VCORE(NDIM_LCL, NNELV_LCL)
!      REAL*8  PENA

C---  Paramètres supplémentairse pour le calcul des vitesses verticales
      INTEGER IQF, IQS !INDICES POUR L'ASSEMBLAGE
      REAL*8 QFELE(NNELV_LCL), QSELE(NNELV_LCL)

C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDJV_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, II, NO, IQF, IQS, QSELE, QFELE, VCORE, VPRN,
!$omp& KNE)

C---  CALCUL DES VITESSES VERTICALES
C     SUIT LE MODÈLE DE ASMK
      DO IC=1,EG_CMMN_NELCOL
!$omp do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---     TRANSFERT DES CONNECTIVITÉS ÉLÉMENTAIRES
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        TRANSFERT DES DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            VDLE(II+4) = VDLG(4,NO)
            VDLE(II+5) = VDLG(5,NO)
            CALL DCOPY(EG_CMMN_NDIM,  VCORG(1,NO), 1, VCORE(1,IN), 1)
            II = II + 5
         ENDDO

C---     TRANSFERT DES PRNO
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(1,IN), 1)
         ENDDO

C---     Remise à 0 des variables de cacul
         CALL DINIT(EG_CMMN_NNELV,ZERO,QSELE, 1)
         CALL DINIT(EG_CMMN_NNELV,ZERO,QFELE, 1)

C---     CALCUL DE LA CONTRIBUTION ÉLÉMENTAIRE A WS ET WF
         CALL NS_MACRO3D_CLCPRNEQ    ( VCORE,
     &                                 VDJV(1,IE),
     &                                 VPRGL,
     &                                 VPRN, !entree
     &                                 VDLE ,
     &                                 QSELE, !sortie
     &                                 QFELE )!sortie

C---     Ajouter à IPRNO_QF et IPRNO_QS les wf; soutraire les WQELE à IPRNO_QF
         IQF = NS_IPRNO_QF
         IQS = NS_IPRNO_QS

         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
!$omp atomic
            VPRNO(IQF, NO) = VPRNO(IQF , NO) + QFELE(IN)
!$omp atomic
            VPRNO(IQS, NO) = VPRNO(IQS , NO) + QFELE(IN) - QSELE(IN) !POSSIBILITÉ DE N'UTILISER QUE QSPRIME ET DE SOUSTRAIRE À QF SEULEMENT À L'ASSEMBLAGE...
         ENDDO

      ENDDO ! Boucle sur les éléments
!$omp end do
      ENDDO ! Boucle sur les ELCOL ?MGia c'est quoi ?
!$omp end parallel

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRNEQ
C
C Description:
C     LA fonction NS_MACRO3D_CLCPRNEQ calcule les propriétés nodales de
C     débits en surface et au fond pour un élément
C
C Entrée:
C     VCORE       Table des coordonnées de l'élément
C     VDJE        Table du Jacobien Elémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales Élémentaires
C     VPRE        Table de PRopriétés Elémentaires de l'élément (seulement)
C     VDLE        Table de Degrés de Libertés Elémentaires
C
C Sortie:
C     QSELE       Table des contribution élémentaire au débit en surface en
C                 chacun des noeuds
C     QFELE       Table des contribution élémentaire au débit au fond en chacun
C                 des noeuds
C
C Notes:
C************************************************************************

      SUBROUTINE NS_MACRO3D_CLCPRNEQ ( VCORE,
     &                                 VDJE,
     &                                 VPRG,
     &                                 VPRN, !entree
     &                                 VDLE,
     &                                 QSELE,
     &                                 QFELE )
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

!      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8   VCORE(EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE (2, 4)                         ! 2 POUR CHAQUE P6
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   QSELE(EG_CMMN_NNELV)
      REAL*8   QFELE(EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ni_ip2p3.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'


      REAL*8 VKX, VEX, VKY, VEY, VSX, VSY, DETJT6  !métriques du t6
      REAL*8 ZF1, ZF2, ZF3, ZF4, ZF5, ZF6 !BATHY
      REAL*8 DZFDX1, DZFDX2, DZFDX3, DZFDX4 ! PRODUIT MÉTRIQUES BATHY
      REAL*8 DZFDY1, DZFDY2, DZFDY3, DZFDY4 !PRODUITS MÉTRIQUES BATHY
      REAL*8 UF1, UF2, UF3, UF4, UF5, UF6 !VITESES AU FOND
      REAL*8 VF1, VF2, VF3, VF4, VF5, VF6 !VITESES AU FOND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NDJV .EQ. 5)
D     CALL ERR_PRE(LM_CMMN_NPRNO .GE. NS_IPRNO_ZF)
d     CALL ERR_PRE(LM_CMMN_NDLN .GE. NS_IDL_UF)
d     CALL ERR_PRE(EG_CMMN_NNELV .EQ. 6)
C-----------------------------------------------------------------------


C---  CALCUL DE QF
C---     METRIQUES DU T6
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)
      !note : les "vrais" métriques sont ces dernières divisées par le detj
      !or, on a une multiplication par le detJ qui doit aussi être faite à l'intégration
      !les deux s'annulant, on est correct ainsi

C---  VITESSSES
      UF1 = VDLE(NS_IDL_UF, 1)
      UF2 = VDLE(NS_IDL_UF, 2)
      UF3 = VDLE(NS_IDL_UF, 3)
      UF4 = VDLE(NS_IDL_UF, 4)
      UF5 = VDLE(NS_IDL_UF, 5)
      UF6 = VDLE(NS_IDL_UF, 6)
      VF1 = VDLE(NS_IDL_VF, 1)
      VF2 = VDLE(NS_IDL_VF, 2)
      VF3 = VDLE(NS_IDL_VF, 3)
      VF4 = VDLE(NS_IDL_VF, 4)
      VF5 = VDLE(NS_IDL_VF, 5)
      VF6 = VDLE(NS_IDL_VF, 6)

C---     Bathy
      ZF1 =  VPRN(NS_IPRNO_ZF, 1)
      ZF2 =  VPRN(NS_IPRNO_ZF, 2)
      ZF3 =  VPRN(NS_IPRNO_ZF, 3)
      ZF4 =  VPRN(NS_IPRNO_ZF, 4)
      ZF5 =  VPRN(NS_IPRNO_ZF, 5)
      ZF6 =  VPRN(NS_IPRNO_ZF, 6)

C---  DERIVEES (pourraient aussi être des prev)
      dZFdx1=vsx * ZF1 + vkx * ZF2 + vex * ZF6
      dZFdx2=vsx * ZF2 + vkx * ZF3 + vex * ZF4
      dZFdx3=vsx * ZF6 + vkx * ZF4 + vex * ZF5
      dZFdx4=-(vsx * ZF4 + vkx * ZF6 + vex * ZF2) !métriques inversées sur t6#4
      dZFdy1=vsy * ZF1 + vky * ZF2 + vey * ZF6
      dZFdy2=vsy * ZF2 + vky * ZF3 + vey * ZF4
      dZFdy3=vsy * ZF6 + vky * ZF4 + vey * ZF5
      dZFdy4=-(vsy * ZF4 + vky * ZF6 + vey * ZF2) !métriques inversées sur t6#4

      !TEST : DONNE QF = 2 sur noeuds centraux et QF = 6 sur noeuds sommets ->ok
 !     QFELE(1) = UN
 !     QFELE(2) = UN
 !     QFELE(3) = UN
 !     QFELE(4) = UN
 !     QFELE(5) = UN
 !     QFELE(6) = UN


      !TEST : devrait donner QF = 6 partout OK
      !QFELE(1) = UN
      !QFELE(2) = TROIS
      !QFELE(3) = UN
      !QFELE(4) = TROIS
      !QFELE(5) = UN
      !QFELE(6) = TROIS

      QFELE(1) = UN_24*(dZFdx1*(2*UF1+UF2+UF6) + dZFdy1*(2*VF1+VF2+VF6)) !t3#1n1
      QFELE(2) = UN_24*(   dZFdx1*(UF1+2*UF2+UF6)     +                  !t3#1n2
     &                     dZFdx2*(UF3 + 2*UF2 + UF4) +                  !t3#2n1
     &                     dZFdx4*(2*UF2 + UF4 + UF6) +                  !t3#4n3
     &                     dZFdy1*(VF1+2*VF2+VF6)     +                  !t3#1n2
     &                     dZFdy2*(VF3 + 2*VF2 + VF4) +                  !t3#2n1
     &                     dZFdy4*(2*VF2 + VF4 + VF6)    )               !t3#4n3
      QFELE(3) = UN_24*(dZFdx2*(2*UF3+UF2+UF4) + dZFdy2*(2*VF3+VF2+VF4)) !T3#2n2
      QFELE(4) = UN_24*(   dZFdx2*(UF2+2*UF4+UF3)     +                  !t3#2 n3
     &                     dZFdx3*(UF6 + 2*UF4 + UF5) +                  !t3#3n2
     &                     dZFdx4*(2*UF4 + UF2 + UF6) +                  !t3#4n1
     &                     dZFdy2*(VF2+2*VF4+VF3)     +                  !t3#2n3
     &                     dZFdy3*(VF6 + 2*VF4 + VF5) +                  !t3#3 n2
     &                     dZFdy4*(2*VF4 + VF2 + VF6)    )               !t3#4n1
      QFELE(5) = UN_24*(dZFdx3*(2*UF5+UF6+UF4) + dZFdy3*(2*VF5+VF6+VF4)) !t3#3n3
      QFELE(6) = UN_24*(   dZFdx1*(UF1+2*UF6+UF2)     +                  !t3#1n3
     &                     dZFdx3*(UF4 + 2*UF6 + UF5) +                  !t3#3n1
     &                     dZFdx4*(2*UF6 + UF2 + UF4) +                  !t3#4n2
     &                     dZFdy1*(VF1+2*VF6+VF2)     +                  !t3#1 n3
     &                     dZFdy3*(VF4 + 2*VF6 + VF5) +                  !t3#3 n1
     &                     dZFdy4*(2*VF6 + VF2 + VF4)    )               !t3#4n2

C---  CALCUL DE QS (prime)
      CALL NS_MACRO3D_CLCPRNEQS( VCORE,
     &                           VDJE,
     &                           VPRG,
     &                           VPRN, !entree
     &                           VPRE,
     &                           VDLE,
     &                           QSELE )  !sortie

      END


C************************************************************************
C Sommaire: NS_MACRO3D_CLCPRNEQS
C
C Description:
C     LA fonction NS_MACRO3D_CLCPRNEQFS calcule les propriétés nodales de
C     débits en surface pour un élément
C
C Entrée:
C     VCORE       Table des coordonnées de l'élément
C     VDJE        Table du Jacobien Elémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales Élémentaires
C     VPRE        Table de PRopriétés Elémentaires de l'élément (seulement)
C     VDLE        Table de Degrés de Libertés Elémentaires
C
C Sortie:
C     QSELE       Table des contribution élémentaire au débit en surface en
C                 chacun des noeuds
C     QFELE       Table des contribution élémentaire au débit au fond en chacun
C                 des noeuds
C
C Notes: Respecte la même implantation que asmke pour faciliter
C        la reprise...
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPRNEQS( VCORE,
     &                                 VDJE,
     &                                 VPRG,
     &                                 VPRN, !entree
     &                                 VPRE,
     &                                 VDLE,
     &                                 QSELE )
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

!      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8   VCORE(EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE (2, 4)                         ! 2 POUR CHAQUE P6
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   QSELE(EG_CMMN_NNELV)
      REAL*8   QFELE(EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ni_ip2p3.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'


      !Entiers pour dimensionnement
 !     INTEGER  NDLP6
      INTEGER  NDIMELV              !nombre de dimension du vrai élément (3)
      INTEGER  NFNAPPROXUV          !nombre de fonction d'approximation pour uetv
      INTEGER  NFNAPPROXH           !nombre de fonction d'approximation pour h

 !    PARAMETER(NDLP6 = 15)         !nombre de dl par sous-élémnet de volume = nombre de noeud par sous-élémnet (3) X nombre de dl par noeud (5)
      PARAMETER(NDIMELV = 3)
      PARAMETER ( NFNAPPROXUV = 6)
      PARAMETER ( NFNAPPROXH = 3)

      !Connectivités et table de localisation
      INTEGER KNET3  (3, 4)         !nombre de noeud par sous-élément , Nombre de sous-élément par élément
 !     INTEGER KLOCET3(NDLP6, 4)
 !     INTEGER KLOCE  (NDLP6)        !15 dl de l'élément T3 global (dl aux sommets)

      !Variables pour l'intégration numérique
      INTEGER  NPG                  !nombre de points de gauss (pour l'intégration numérique)
      INTEGER  IPG                  !itérateur sur les points de gauss
      REAL*8   VCOPG(NDIMELV)       !coordonnées du points de Gauss;
      REAL*8   POIDSPG              !poids du point de gauss

      REAL*8   VZCORE(2, EG_CMMN_NNELV) ! donne les zs et zf pour chacun des 6 noeuds.

      !Évaluations au point de gauss des métriques, dl, fonctions d'approximation et
      !leur dérivés
      REAL*8   VNUV(NFNAPPROXUV)          !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)            !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV,NFNAPPROXUV ) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH) !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VJP12L(NDIMELV,NDIMELV)
      REAL*8   VJP6(NDIMELV,NDIMELV)
      REAL*8   DJP12L                     !évaluation du déterminant complet au pt de gauss
      REAL*8   DJP6                       !évaluation du déterminant du sous élément au pt de gauss
      REAL*8   VDLPT(3)                   !évaluation des degrés de liberté au point d'intégration
      REAL*8   VDL_XIPT(3, NDIMELV)       !évalutation des dérivés des dl p/r à x,y et z au point d'intégration
      REAL*8   WPT                        !évaluation de la vitesse verticale au point d'intégration
      REAL*8   VW_XIPT(NDIMELV)           !évalutation des dérivées de la vitesse verticale au point d'intégration

      INTEGER  IP6                        !contient l'indice du P6 correspondant au point de Gauss

      INTEGER  IN !itérateur sur les noeuds de l'élément
      INTEGER  IERR !pas vérifié ni utilisé

      DATA KNET3  / 1,2,6,  2,3,4,  6,4,5,  4,6,2/
 !     DATA KLOCET3/  1, 2, 3, 4, 5,   6 ,7 ,8 ,9 ,10,   26,27,28,29,30,   ! NOEUDS 1,2,6,
 !    &               6 ,7 ,8 ,9,10,   11,12,13,14,15,   16,17,18,19,20,   ! NOEUDS 2,3,4,
 !    &              26,27,28,29,30,   16,17,18,19,20,   21,22,23,24,25,   ! NOEUDS 6,4,5,
 !    &              16,17,18,19,20,   26,27,28,29,30,   6 ,7 ,8 ,9 ,10 /  ! NOEUDS 4,6,2
 !     DATA KLOCE /  1 ,2 ,3 , 4, 5,   11,12,13,14,15,   21,22,23,24,25 /



C---- VARIABLES PROPRES :
      INTEGER I_U, I_V !Indices des dl u (1) et v (2) sur un noeud
      INTEGER I_WELE !Indice du noeud w (pour assemblage dans l'élément global)
      INTEGER I !itérateur
      REAL*8   CST   ![<N3d,x>{u} + <N3d,y>{v}]detJP12L*poids

C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV .EQ. 2*4)           ! 2 POUR CHAQUE T3
!     QS EST VIDE
C-----------------------------------------------------------------------

C---  Construction de la table contenant les coordonnées en z
      DO IN = 1, EG_CMMN_NNELV !pour chaque noeud
         VZCORE(1,IN) = VPRN(NS_IPRNO_ZF, IN)
         VZCORE(2,IN) = VZCORE(1, IN) + VPRN(NS_IPRNO_PRFA, IN)
      END DO ! Boucle sur les noeuds

C---  INTÉGRATION NUMÉRIQUE
      NPG = NI_IP2P3_REQNPG()
D     CALL ERR_ASR(NPG .GT. 0)
      DO IPG = 1, NPG

C---     Récupére les coordonnées du points de Gauss et le poids associé
         IERR = NI_IP2P3_REQCPG(IPG, NDIMELV, VCOPG)
         IERR = NI_IP2P3_REQWPG(IPG, POIDSPG)

C---     Calcul des métriques, des fonctions d'approximation et de leur dérivés,
C        des dl et de leur dérivées ainsi que de w (à traiter) et ses dérivées (à traiter)
C        le tout au point d'intégration
         CALL NS_MACRO3D_EVALUE (   NFNAPPROXUV,  ! entrée : dim
     &                              NFNAPPROXH,   ! entrée : dim
     &                              NDIMELV,      ! entrée : dim
     &                              VCORE,        ! entrée
     &                              VZCORE,       ! entrée
     &                              VDJE,         ! entrée
     &                              VCOPG,        ! entrée
     &                              VDLE,         ! entrée
     &                              VPRN,         ! ENTRÉE
     &                              VNUV,         ! sortie
     &                              VNH,          ! sortie
     &                              VNUV_XI,      ! sortie
     &                              VNH_XI,       ! sortie
     &                              VDLPT,        ! sortie
     &                              VDL_XIPT,     ! sortie
     &                              WPT,          ! pas de sens ici, devrait être 0
     &                              VW_XIPT,      ! pas de sens ici, devrait être 0
     &                              DJP12L,       ! sortie
     &                              DJP6,         ! sortie (pas utilisée)
     &                              IP6 )         ! sortie


C---  POURRAIT ÊTRE UN APPEL POUR RESPECTER STRUCTURE DE ASMKE
C---  Évaluation au point de gauss de {N2d}[<N3d,x>{u} + <N3d,y>{v}]detJP12L*poids
C---                                  {VNH}[VDL_XIPT(u,x) + VDL_XIPT(v,y) ]djp12*poidspg
         I_U = NS_IDL_UPT
         I_V = NS_IDL_VPT
         CST = ( VDL_XIPT(I_U,1) + VDL_XIPT(I_V,2) )*DJP12L * POIDSPG

         DO I=1,NFNAPPROXH
            I_WELE = KNET3(I,IP6)
            QSELE(I_WELE) =   QSELE(I_WELE) + VNH(I)*CST
         END DO

      END DO !ITÉRATION SUR LES PG
      RETURN

      END
