C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_ASMKE_V
C
C Description:
C     La fonction SV2D_CBS_ASMK_V calcule la matrice de rigidité
C     élémentaire due à un élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VCORE       Table des coordonnées de l'élément
C     VDJE        Table du Jacobien Elémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales Élémentaires
C     VPRE        Table de PRopriétés Elémentaires de l'élément (seulement)
C     VDLE        Table de Degrés de Libertés Elémentaires
C
C Sortie:
C     VKE         Matrice élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMKE_V(   VKE,
     &                                 VCORE,
     &                                 VDJE,
     &                                 VPRG,
     &                                 VPRN,
     &                                 VPRE,
     &                                 VDLE  )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8   VCORE(EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE (2, 4)                         ! 2 POUR CHAQUE P6
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ni_ip2p3.fi'
      INCLUDE 'ns_macro3d_iprno.fi'

      !Entiers pour dimensionnement
      INTEGER  NDLP6
      INTEGER  NDIMELV              !nombre de dimension du vrai élément (3)
      INTEGER  NFNAPPROXUV          !nombre de fonction d'approximation pour uetv
      INTEGER  NFNAPPROXH           !nombre de fonction d'approximation pour h

      PARAMETER(NDLP6 = 15)         !nombre de dl par sous-élémnet de volume = nombre de noeud par sous-élémnet (3) X nombre de dl par noeud (5)
      PARAMETER(NDIMELV = 3)
      PARAMETER ( NFNAPPROXUV = 6)
      PARAMETER ( NFNAPPROXH = 3)

      !Connectivités et table de localisation
      INTEGER KNET3  (3, 4)         !nombre de noeud par sous-élément , Nombre de sous-élément par élément
      INTEGER KLOCEP6(NDLP6, 4)
      INTEGER KLOCE  (NDLP6)        !15 dl de l'élément T3 global (dl aux sommets)

      !Variables pour l'intégration numérique
      INTEGER  NPG                  !nombre de points de gauss (pour l'intégration numérique)
      INTEGER  IPG                  !itérateur sur les points de gauss
      REAL*8   VCOPG(NDIMELV)       !coordonnées du points de Gauss;
      REAL*8   POIDSPG              !poids du point de gauss

      REAL*8   VZCORE(2, EG_CMMN_NNELV) ! donne les zs et zf pour chacun des 6 noeuds.

      !Évaluations au point de gauss des métriques, dl, fonctions d'approximation et
      !leur dérivés
      REAL*8   VNUV(NFNAPPROXUV)          !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)            !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV,NFNAPPROXUV ) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH) !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VJP12L(NDIMELV,NDIMELV)
      REAL*8   VJP6(NDIMELV,NDIMELV)
      REAL*8   DJP12L                     !évaluation du déterminant complet au pt de gauss
      REAL*8   DJP6                       !évaluation du déterminant du sous élément au pt de gauss
      REAL*8   VDLPT(3)                   !évaluation des degrés de liberté au point d'intégration
      REAL*8   VDL_XIPT(3, NDIMELV)       !évalutation des dérivés des dl p/r à x,y et z au point d'intégration
      REAL*8   WPT                        !évaluation de la vitesse verticale au point d'intégration
      REAL*8   VW_XIPT(NDIMELV)           !évalutation des dérivées de la vitesse verticale au point d'intégration

      INTEGER  IP6                        !contient l'indice du P6 correspondant au point de Gauss
      INTEGER  ID
      REAL*8   VDJT3(EG_CMMN_NDJV)

      INTEGER  IN !itérateur sur les noeuds de l'élément
      INTEGER  IERR !pas vérifié ni utilisé

      DATA KNET3  / 1,2,6,  2,3,4,  6,4,5,  4,6,2/
      DATA KLOCEP6/  1, 2, 3, 4, 5,   6 ,7 ,8 ,9 ,10,   26,27,28,29,30,   ! NOEUDS 1,2,6,
     &               6 ,7 ,8 ,9,10,   11,12,13,14,15,   16,17,18,19,20,   ! NOEUDS 2,3,4,
     &              26,27,28,29,30,   16,17,18,19,20,   21,22,23,24,25,   ! NOEUDS 6,4,5,
     &              16,17,18,19,20,   26,27,28,29,30,   6 ,7 ,8 ,9 ,10 /  ! NOEUDS 4,6,2
      DATA KLOCE /  1 ,2 ,3 , 4, 5,   11,12,13,14,15,   21,22,23,24,25 /
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV .EQ. 2*4)           ! 2 POUR CHAQUE T3
C-----------------------------------------------------------------------

C---  Construction de la table contenant les coordonnées en z
      DO IN = 1, EG_CMMN_NNELV !pour chaque noeud
         VZCORE(1,IN) = VPRN(NS_IPRNO_ZF, IN)
         VZCORE(2,IN) = VZCORE(1, IN) + VPRN(NS_IPRNO_PRFA, IN)
      END DO ! Boucle sur les noeuds

C---  INTÉGRATION NUMÉRIQUE
      NPG = ni_ip2p3_REQNPG()
D     CALL ERR_ASR(NPG .GT. 0)
      DO IPG = 1, NPG

C---     Récupére les coordonnées du points de Gauss et le poids associé
         IERR = ni_ip2p3_REQCPG(IPG, NDIMELV, VCOPG)
         IERR = ni_ip2p3_REQWPG(IPG, POIDSPG)

C---     Calcul des métriques, des fonctions d'approximation et de leur dérivés,
C        des dl et de leur dérivées ainsi que de w (à traiter) et ses dérivées (à traiter)
C        le tout au point d'intégration
         CALL NS_MACRO3D_EVALUE (   NFNAPPROXUV,  ! entrée : dim
     &                              NFNAPPROXH,   ! entrée : dim
     &                              NDIMELV,      ! entrée : dim
     &                              VCORE,        ! entrée
     &                              VZCORE,       ! entrée
     &                              VDJE,         ! entrée
     &                              VCOPG,        ! entrée
     &                              VDLE,         ! entrée
     &                              VPRn,         !ENTRÉE
     &                              VNUV,         ! sortie
     &                              VNH,          ! sortie
     &                              VNUV_XI,      ! sortie
     &                              VNH_XI,       ! sortie
     &                              VDLPT,        ! sortie
     &                              VDL_XIPT,     ! sortie
     &                              WPT,          ! sortie à analyser...
     &                              VW_XIPT,      ! sortie à analyser
     &                              DJP12L,       ! sortie
     &                              DJP6,         ! sortie (pas utilisée)
     &                              IP6 )         ! sortie


C---     CONVECTION
         CALL NS_MACRO3D_ASMK_V_CNV ( NFNAPPROXUV,    !dim
     &                                NDIMELV,        !dim
     &                                KNET3(1,IP6),   !entrée  !pas utilisé car pas de prno
     &                                KLOCEP6(1,IP6),
     &                                POIDSPG,
     &                                VDLPT,
     &                                WPT,  !a analyser...
     &                                VNUV,
     &                                VNUV_XI,
     &                                DJP12L,
     &                                VPRN,          !entrée : pas utilisé
     &                                VKE   )        !sortie



C---      DIFFUSION
          CALL NS_MACRO3D_ASMK_V_DIF( NFNAPPROXUV,    !entrée : dim
     &                                NDIMELV,        !entrée : dim
     &                                KNET3(1,IP6),   !pas utilisé car ne lit pas de prno
     &                                KLOCEP6(1,IP6),
     &                                POIDSPG,
     &                                VDLPT,          !pas utilisé
     &                                VDL_XIPT,
     &                                WPT,  !a analyser...
     &                                VW_XIPT, !à analyser...
     &                                VNUV,           !pas utilisé
     &                                VNUV_XI,
     &                                DJP12L,
     &                                VPRN,           !pas certaine d'en avoir besoin
     &                                VPRE(1,IP6),    !envoie que les 2 PRE associées au sous-élément
     &                                VPRG,           !pas utilisé pas certaine d'en avoir besoin
     &                                VKE )           !sortie

!C---        CORIOLIS
!         CALL NS_MACRO3D_ASMK_V_COR( NFNAPPROXUV,
!     &                               KLOCET3(1,IP6),
!     &                               POIDSPG,
!     &                               VNUV,
!     &                               DJP12L,
!     &                               VKE  )



C---        GRAVITE
      CALL NS_MACRO3D_ASMK_V_GRV(   NFNAPPROXUV,
     &                              NFNAPPROXH,
     &                              NDIMELV,
     &                              KLOCEP6(1,IP6),
     &                              POIDSPG,
     &                              VNUV,
     &                              VNH_XI,
     &                              DJP12L,
     &                              VKE      )

C---        CONTINUITÉ
      CALL NS_MACRO3D_ASMK_V_CNT(   NFNAPPROXUV,
     &                              NFNAPPROXH,
     &                              NDIMELV,
     &                              KLOCEP6(1,IP6),
     &                              POIDSPG,
     &                              VNUV,
     &                              VNH_XI,
     &                              VDLPT,
     &                              VDL_XIPT,
     &                              DJP12L,
     &                              DJP6,
     &                              VDJE,
     &                              VPRN,
     &                              VKE     )



      ENDDO !Boucle sur PG

C---  MANNING
C---     METRIQUES DU SOUS-ÉLÉMENT
      VDJT3(1) = UN_2*VDJE(1)
      VDJT3(2) = UN_2*VDJE(2)
      VDJT3(3) = UN_2*VDJE(3)
      VDJT3(4) = UN_2*VDJE(4)
      VDJT3(5) = UN_4*VDJE(5)

C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
C           ============================
      DO IP6=1,4

C---        LE T3 INTERNE EST INVERSÉ
         IF (IP6 .EQ. 4) THEN
            DO ID=1,4
               VDJT3(ID) = -VDJT3(ID)
            ENDDO
         ENDIF
C---        MANNING
         CALL NS_MACRO3D_ASMK_V_MAN (  VKE,
     &                                 KNET3(1,IP6),
     &                                 KLOCEP6(1,IP6),
     &                                 VDJT3,
     &                                 VPRG,
     &                                 VPRN,
     &                                 VPRE(1,IP6),
     &                                 VDLE           ) !VPRE pas utilisé

      END DO

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V_CNT
C
C Description:
C     La subroutine privée NS_MACRO3D_ASMK_V_CNT assemble dans la
C     matrice Ke la contribution de volume de l'équation de
C     continuité.
C
C Entrée:
C      NFNAPPROXUV   Nombre de fonctions d'approximation pour u et v (6)
C      NFNAPPROXH    Nombre de fonctions d'approximation pour h (3)
C      NDIMELV       Nombre de dimension de l'élément de volume (3)
C      INTEGER  KLOCE (15)                      Table de localisation des dl dans la matrice vke (15 = 3 noeuds X 5dl par noeud)
C      REAL*8   POIDS                           Poids à donner à l'évaluation sur le point considérée
C      REAL*8   VNUV(NFNAPPROXUV)               Évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
C      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH)      Évaluation des N,x_i (les N pour les h) aux pt de Gauss
C      REAL*8   VDLPT(3)                   !évaluation des degrés de liberté au point d'intégration
C      REAL*8   VDL_XIPT(3, NDIMELV)            Évalutation des dérivés des dl p/r à x,y et z au point d'intégration
C      REAL*8   DJP12L                          Évaluation du déterminant au point de gauss
C      REAL*8   DJP6                            Évaluation du déterminant du P6 au point de gauss
C      REAL*8   VDJE(5)                         Métriques 2D
C      REAL*8   VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV) Propriétés nodales
C
C
C Sortie:
C      REAL*8   VKE  Matrice de contribution élémentaire
C
C
C Notes:
C        - <NP12L,x> {NP6}      ! continuité
C        + <NP12L,x> {NP12L,x}    ! stabilisation Lapidus
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK_V_CNT(   NFNAPPROXUV,
     &                                    NFNAPPROXH,
     &                                    NDIMELV,
     &                                    KLOCE,
     &                                    POIDS,
     &                                    VNUV,
     &                                    VNH_XI,
     &                                    VDLPT,
     &                                    VDL_XIPT,
     &                                    DJP12L,
     &                                    DJP6,
     &                                    VDJE,
     &                                    VPRN,
     &                                    VKE     )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  NFNAPPROXUV
      INTEGER  NFNAPPROXH
      INTEGER  NDIMELV

      INTEGER  KLOCE (15)                    !3sommets X 5dl par sommet
      REAL*8   POIDS
      REAL*8   VNUV(NFNAPPROXUV)             ! évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH)    !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VDLPT(3)                   !évaluation des degrés de liberté au point d'intégration
      REAL*8   VDL_XIPT(3, NDIMELV)          !évalutation des dérivés des dl p/r à x,y et z au point d'intégration
      REAL*8   DJP12L                        ! évaluation du déterminant au point de gauss
      REAL*8   DJP6                          ! évaluation du déterminant du P6 au point de gauss
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VKE(LM_CMMN_NDLEV,LM_CMMN_NDLEV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d.fc'
      !Dimensions

      INTEGER NFNAPPROXUV_LOC
      INTEGER NFNAPPROXH_LOC
      PARAMETER (NFNAPPROXUV_LOC = 6)
      PARAMETER (NFNAPPROXH_LOC = 3)

      !ITÉRATEURS
      INTEGER I,J

      !COEFFICIENT : POIDS*DETJP12L
      REAL*8 COEFF

      !LAPLACIEN
      REAL*8 VDX(3)  !(VSX, VEX, VKX)
      REAL*8 VDY(3)  !(VSY, VEY, VKY)

      REAL*8 U,V
      REAL*8 DHDX, DHDY                    !DH/DX; DH/DY
      REAL*8 DHNRM                         !NOMBRE DU VECTEUR (DH/DX, DH/DY)
      REAL*8 DNHDXNRM , DNHDYNRM           !NORME DE <NH,X>
      REAL*8 RHNRM                         !NORME DU RÉSIDU
      REAL*8 C                             !COEFFICIENT DEVANT LAPLACIEN EN HH
      REAL*8 C1, C3, C5, CM                !COEFFICIENTS POUR LE DÉCOUVREMENT
   !   REAL*8 NS_MACRO3D_STABI_DARC
      !TABLES DE CONTRIBUTION DE LA CONTINUITÉ
      REAL*8 CNT_HU(NFNAPPROXH_LOC,NFNAPPROXUV_LOC)
      REAL*8 CNT_HV(NFNAPPROXH_LOC,NFNAPPROXUV_LOC)
   !  REAL*8 CNT_HH(NFNAPPROXH_LOC,NFNAPPROXUV_LOC)

      !INDICES POUR POSITIONNER U,V ET H DANS VKE
      INTEGER JU_P12L, JV_P12L, IH_P12L, JH_P12L

      !TABLES DE LOCALISATION
      INTEGER KLOCENU(NFNAPPROXUV_LOC)
      INTEGER KLOCENV(NFNAPPROXUV_LOC)
      INTEGER KLOCEH(NFNAPPROXH_LOC)

      DATA KLOCENU /1, 6, 11, 2, 7, 12/
      DATA KLOCENV /3, 8, 13, 4, 9, 14/
      DATA KLOCEH /5, 15, 25/

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DJP6 .NE. ZERO)
C-----------------------------------------------------------------------
    !  NS_MACRO3D_STABI_DARC = UN
C---  COEFFICIENT
      COEFF = POIDS * DJP12L

C---  TERMES EN HU ET EN HV
C      -{Nh,x}<Nuv> * detJP12L * poids en hu
C      -{Nh,y}<Nuv>*detJP12L*poids en hv
      DO J = 1,6
         DO I=1,3
            CNT_HU(I,J) = -COEFF*VNH_XI(1,I)*VNUV(J)
            CNT_HV(I,J) = -COEFF*VNH_XI(2,I)*VNUV(J)
         END DO
      END DO

C---  NORME DU GRADIENT DE LA SURFACE
      DHDX = VDL_XIPT(NS_IDL_HPT,1)
      DHDY = VDL_XIPT(NS_IDL_HPT,2)
      DHNRM = MAX(SQRT(DHDX*DHDX + DHDY*DHDY), 1.0D-12)


C---  RÉSIDU =wi[ {Nh,x}<Nuv>{u} + {Nh,y}<Nuv>{v} ]detJP12L
C            =wi[ {Nh,x}u + {Nh,y}v ] detJP12L
C     NORME DU RÉSIDU ( on n'utilise pas la norme euclidienne pour que la pondération
C                       via pg soit bonne)
C            = 1/3 * (wi detJP12L) * [|u|(|Nh1,x| + |Nh2,x| + |Nh3,x|) + |v|(|Nh1,y| + |Nh2,y| + |Nh3,y|)]
      U = VDLPT(NS_IDL_UPT )
      V = VDLPT(NS_IDL_VPT )

      DNHDXNRM = ABS(VNH_XI(1,1)) + ABS(VNH_XI(1,2)) + ABS(VNH_XI(1,3))
      DNHDYNRM = ABS(VNH_XI(2,1)) + ABS(VNH_XI(2,2)) + ABS(VNH_XI(2,3))
      RHNRM =  UN_3 * COEFF *
     &         ( ABS(U)*DNHDXNRM + ABS(V)*DNHDYNRM )

C---  LAPIDUS (le detjP6 n'est pas affecté à dhnrm car ils le sont déjà p/r à sv2d...
      C = UN_2 * NS_MACRO3D_STABI_LAPIDUS*( DHNRM + RHNRM/DJP6 )

C---     COEFFICIENTS DE DARCY
      C1 = VPRN(NS_IPRNO_DECOU,1)    ! Coeff pour le découvrement
      C3 = VPRN(NS_IPRNO_DECOU,3)    ! Coeff pour le découvrement
      C5 = VPRN(NS_IPRNO_DECOU,5)    ! Coeff pour le découvrement
      CM = UN_3*(C1 + C3 + C5)
      C = C + UN_2*(NS_MACRO3D_STABI_DARCY + CM) / DJP6

C---  LAPLACIEN
      VDX(1) = VDJE(1)
      VDX(2) = VDJE(2)
      VDX(3) = -(VDJE(1)+VDJE(2))
      VDY(1) = VDJE(3)
      VDY(2) = VDJE(4)
      VDY(3) = -(VDJE(3)+VDJE(4))


C---     SOUS-MATRICE H-U; H-V
C---  ASSEMBLAGE DANS LES MATRICES
      DO J=1,NFNAPPROXUV
         JU_P12L = KLOCE( KLOCENU(J) )
         JV_P12L = KLOCE( KLOCENV(J) )
         DO I=1,NFNAPPROXH
            IH_P12L = KLOCEH(I)

            VKE(IH_P12L,JU_P12L) = VKE(IH_P12L,JU_P12L) + CNT_HU(I,J)
            VKE(IH_P12L,JV_P12L) = VKE(IH_P12L,JV_P12L) + CNT_HV(I,J)
         ENDDO
      ENDDO


C---     SOUS-MATRICE H-H
      DO J = 1,NFNAPPROXH
         JH_P12L = KLOCEH(J)
         DO I=1,NFNAPPROXH
            IH_P12L = KLOCEH(I)
            VKE(IH_P12L,JH_P12L) = VKE(IH_P12L,JH_P12L) +
     &                     ( VDX(I)*VDX(J) + VDY(I)*VDY(J) )*C
         END DO
      END DO


      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V_GRV
C
C Description:
C     La subroutine privée NS_MACRO3D_ASMK_V_GRV assemble dans la
C     matrice Ke la contribution de volume des termes de gravité
C     des équations de mouvement pondéré du poids correspondant au point
C     d'intégration
C
C Entrée:
C     KLOCE(1) Table de localisation des dl du T3 complet.
C              Servira à extraire la localisation des "h" aux sommets
C     POIDSPT  Poids à associer au point évalué
C     VNUV     Table contenant les fonctions d'approximation {N}
C               pour u et v évaluées au point d'intégration
C     VNH_XI    Table contenant les dérivées des fonctions d'approximations {N,h} par rapport à x et y
C     DJP6    Déterminant du P6
C     VPRG     Table des propriétés globales
C
C Sortie:
C     VKE
C
C Notes:
C    possibilité de regrouper tout lors de l'assemblage de la matrice.
C************************************************************************

      SUBROUTINE NS_MACRO3D_ASMK_V_GRV(   NFNAPPROXUV,
     &                                    NFNAPPROXH,
     &                                    NDIMELV,
     &                                    KLOCE,
     &                                    POIDS,
     &                                    VNUV,
     &                                    VNH_XI,
     &                                    DJP12L,
     &                                    VKE      )
      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !ZERO
      INCLUDE 'eacmmn.fc' !LM_CMMN
!      INCLUDE 'egcmmn.fc' !EG_..

      INTEGER  NFNAPPROXUV
      INTEGER  NFNAPPROXH
      INTEGER  NDIMELV

      INTEGER  KLOCE (15) !3sommets X 5dl par sommet
      REAL*8   POIDS
      REAL*8   VNUV(NFNAPPROXUV)             ! évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH_XI(NDIMELV,NFNAPPROXH )   ! évaluation des N,x_i (les N pour les h) au pt de Gauss
      REAL*8   DJP12L                        ! évaluation du déterminant au point de gauss

      REAL*8   VKE(LM_CMMN_NDLEV,LM_CMMN_NDLEV)

      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d.fc'
!      INCLUDE 'log.fi'
!      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER NFNAPPROXUV_LOC
      INTEGER NFNAPPROXH_LOC
      PARAMETER(NFNAPPROXUV_LOC = 6)
      PARAMETER(NFNAPPROXH_LOC = 3)
      REAL*8 COEFF

      INTEGER I, J

      INTEGER JH_P12L, IU_P12L, IV_P12L
      INTEGER KLOCENU(NFNAPPROXUV_LOC)
      INTEGER KLOCENV(NFNAPPROXUV_LOC)
      INTEGER KLOCEH(NFNAPPROXH_LOC)

      DATA KLOCEH/5, 15, 25/
      DATA KLOCENU /1, 6, 11, 2, 7, 12/
      DATA KLOCENV /3, 8, 13, 4, 9, 14/

      REAL*8 GRAUH(NFNAPPROXUV_LOC, NFNAPPROXH_LOC)         !Terme de gravité à distribuer dans la matrice
      REAL*8 GRAVH(NFNAPPROXUV_LOC, NFNAPPROXH_LOC)         !Terme de gravité à distribuer dans la matrice
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. NFNAPPROXUV_LOC)
!D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELV_LOC)
C-----------------------------------------------------------------------

      COEFF = POIDS * DJP12L*NS_MACRO3D_GRAVITE*NS_MACRO3D_CMULT_GRA

      DO J=1,3
         DO I=1,6

C---        POIDS*DJP12L*GRAVITÉ*{NUV}<NH,X>
            GRAUH(I,J) = COEFF*VNUV(I)*VNH_XI(1,J)

C---        POIDS*DJP12L*GRAVITÉ*{NUV}<NH,Y>
            GRAVH(I,J) = COEFF*VNUV(I)*VNH_XI(2,J)
         END DO
      END DO

C---  ASSEMBLAGE DANS LES MATRICES
      DO J=1,NFNAPPROXH
         JH_P12L = KLOCEH( J )
         DO I=1,NFNAPPROXUV
            IU_P12L = KLOCE(KLOCENU(I))
            IV_P12L = KLOCE(KLOCENV(I))

            VKE(IU_P12L,JH_P12L) = VKE(IU_P12L,JH_P12L) + GRAUH(I,J)
            VKE(IV_P12L,JH_P12L) = VKE(IV_P12L,JH_P12L) + GRAVH(I,J)
         ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V_CNV
C
C Description:
C     La subroutine privée NS_MACRO3D_ASMK_V_CNV assemble dans la
C     matrice Ke la contribution de volume des termes de convection
C     des équations de mouvement pour un point d'intégration
C
C Entrée:
C     KNE         Connectivités du P6 sur lequel est le point considéré au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3                                KLOCE,
C     POIDS       Poids à donner au valeurs calculées
C     VDLPT       Évaluation de degrés de liberté au point actuellement considéré
C     WPT         Vitesse verticale au point actuellement considéré : n'a pas encore été établie...
C     VNUV        Évaluation des fonctions "N" au point actuellement considéré
C     VNUV_XI     Table contenant les évaluations des fonctions N,x , N,y et N,z au point considéré
C     DJP6      Déterminant du P6 au point considéré
C     VPRN        Table des propriétés nodales, pour les limiteurs de convection
C
C Sortie:
C     VKE ! table des contribution élémentaires
C
C Notes:
C     NOTE SV2D ne s'applique plus ici :
C     Le résidu :
C        {N} < N1,x ; N2,x ; N3,x > {U Qx}
C     développé donne
C        {N} ( N1,x*U1*Qx1 + N2,x*U2*Qx2 + N3,x*u3*Qx3)
C     qui est écrit ensuite sous la forme
C        {N} < U1*N1,x ; U2*N2,x ; U3*N3,x > {Qx}
C
C     NOTE NS_MACRO
C     WPT pas encore traité
C
C     NOTE NS_MACRO
C     Limiteur de convection à inclure
C-------------------------------------------------------------------
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK_V_CNV (  NFNAPPROXUV,
     &                                    NDIMELV,
     &                                    KNE,   !pas utilisé
     &                                    KLOCE,
     &                                    POIDS,
     &                                    VDLPT,
     &                                    WPT,
     &                                    VNUV,
     &                                    VNUV_XI,
     &                                    DJP12L,
     &                                    VPRN,
     &                                    VKE   )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !ZERO
      INCLUDE 'eacmmn.fc' !LM_CMMN
      INCLUDE 'egcmmn.fc' !EG_..

      INTEGER  NFNAPPROXUV
      INTEGER  NDIMELV

      INTEGER  KLOCE (15) !3sommets X 5dl par sommet
      INTEGER  KNE   (3)  !pas utilisé
      REAL*8   POIDS
      REAL*8   VNUV(NFNAPPROXUV)             ! évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNUV_XI(NDIMELV,NFNAPPROXUV ) ! évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   DJP12L                        ! évaluation du déterminant au point de gauss
      REAL*8   VDLPT(3)
      REAL*8   WPT
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)

      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d.fc'


      INTEGER NFNAPPROXUV_LOC
      INTEGER NDIMELV_LOC ! utilisé en debug
      PARAMETER(NFNAPPROXUV_LOC = 6)
      PARAMETER(NDIMELV_LOC = 3)

      INTEGER I, J
      REAL*8 U,V

      INTEGER JU_P12, JV_P12, IU_P12, IV_P12
      INTEGER KLOCENU(NFNAPPROXUV_LOC)
      INTEGER KLOCENV(NFNAPPROXUV_LOC)
      DATA KLOCENU /1, 6, 11, 2, 7, 12/
      DATA KLOCENV /3, 8, 13, 4, 9, 14/

      REAL*8 UJN_XJ(NFNAPPROXUV_LOC)                   !u<N,x> + v<N,y> + w<N,z>
      REAL*8 CNV(NFNAPPROXUV_LOC, NFNAPPROXUV_LOC)         !Terme de convection à distribuer dans la matrice
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. NFNAPPROXUV_LOC)
D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELV_LOC)
C-----------------------------------------------------------------------

C---  u<N,x> + v<N,y> + w<N,z>
      U = VDLPT(NS_IDL_UPT)
      V = VDLPT(NS_IDL_VPT)

      DO I=1,NFNAPPROXUV
         UJN_XJ(I) = U*VNUV_XI(1,I)
     &                + V*VNUV_XI(2,I)
     &                + WPT*VNUV_XI(3,I)*NS_MACRO3D_CMULT_WCNV !PERMET DE NE PAS METTRE LA VITESSE VERTICALE DANS LE TERME DE CONVECTION (MAUDE = 1, YVES = 0)
      END DO

      CALL DINIT(NFNAPPROXUV*NFNAPPROXUV, ZERO,CNV,1)

C--   poidsI N^{\psi^{u}}(\xi_i} X somme des uN,x_i (produit matriciel)
C--   N :VNUV(NFNAPPROXUV)
C--   somme des uN,x_i UJN_XJ
      !c := alpha*op(a)*op(b) + beta*c    où
      ! a(mXk), b(kXn), c(mXn)
      CALL DGEMM( 'N', !op sur a 'N' indique aucune op
     &            'T',  !op sur b
     &            NFNAPPROXUV, !m : a(mXk) : 6X1
     &            NFNAPPROXUV, !n : b(kXn) : 1X6
     &            1, !k : a(mXk), b(kXn)
     &            POIDS * DJP12L*NS_MACRO3D_CMULT_CON, !alpha !mgia
     &            VNUV, !a
     &            NFNAPPROXUV, !first dimension of a
     &            UJN_XJ, !b
     &            NFNAPPROXUV, !firt dimension of b (k)
     &            ZERO, !beta
     &            CNV, !c
     &            NFNAPPROXUV) !first dimension of c)

C---  ASSEMBLAGE DANS LES MATRICES
      DO J=1,NFNAPPROXUV
         JU_P12 = KLOCE( KLOCENU(J) )
         JV_P12 = KLOCE( KLOCENV(J) )
         DO I=1,NFNAPPROXUV
            IU_P12 = KLOCE(KLOCENU(I))
            IV_P12 = KLOCE(KLOCENV(I))

            VKE(IU_P12,JU_P12) = VKE(IU_P12,JU_P12) + CNV(I,J)
            VKE(IV_P12,JV_P12) = VKE(IV_P12,JV_P12) + CNV(I,J)
         ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V_COR
C
C Description:
C     La sous-routine privée NS_MACRO3D_ASMK_V_COR assemble dans la
C     matrice Ke l'évaluation des termes de coriolis en un point
C      pondéré par le poids du point.
C
C Entrée:
C     KLOCE :     Table de localisation des dl du P6 dans le P12L
C     POIDS :   Poids à donner à l'évaluation
C     VNUV     :  Fonction d'approximation {N} pour u et v évaluées au points d'intégration
C     DJP6   :  déterminant du P6 au point d'intégration
C     NFNAPPROXUV  : nombre de fonctions d'approximation (6)
C
C Sortie:
C     VKE : matrice de contributions élémentaires de volume.
C
C Notes:
C     - Fc*detJ* {N} <N> {v}
C     + Fc*detJ* {N} <N> {u}
C
C     Dans sv2d, la matrice est ??lumpée??. Je ne sais pas c'est quoi,
C     mais suite à cette opération, elle n'a des termes que sur sa diagonale.
C     On ne fera pas cela ici... dans un premier temps.
C
C     UN_RHO fixé à UN pas vraiment traité. Voir si sa présence n'est pas une err théorique
!C************************************************************************
      SUBROUTINE  NS_MACRO3D_ASMK_V_COR ( NFNAPPROXUV,
     &                                    KLOCE,
     &                                    POIDS,
     &                                    VNUV,
     &                                    DJP12L,
     &                                    VKE  )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !UN, ZERO
      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
      INTEGER  NFNAPPROXUV
      INTEGER  KLOCE (15) !3sommets X 5dl par sommet
      REAL*8   POIDS
      REAL*8   VNUV(NFNAPPROXUV)             ! évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   DJP12L                        ! évaluation du déterminant au pt de gauss
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)

      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d.fc' !NS_MACRO3D_CORIOLIS

      INTEGER NFNAPPROXUV_LOC
      PARAMETER(NFNAPPROXUV_LOC = 6)

      INTEGER I, J

      INTEGER JU_P12, JV_P12, IU_P12, IV_P12
      INTEGER KLOCENU(NFNAPPROXUV_LOC)
      INTEGER KLOCENV(NFNAPPROXUV_LOC)
      DATA KLOCENU /1, 6, 11, 2, 7, 12/
      DATA KLOCENV /3, 8, 13, 4, 9, 14/

      REAL*8 UN_RHO !1/rho, terme fixé à 1 mais rho pas dynamique encore
      REAL*8 COR(NFNAPPROXUV_LOC, NFNAPPROXUV_LOC) !contient les termes de coriolis à assembler, positivement pour y, négativement pour x
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. NFNAPPROXUV_LOC)
C-----------------------------------------------------------------------

C---  UN_RHO
      UN_RHO = UN

C--   poids *  NS_MACRO3D_CORIOLIS * DJP6 * UN_RHO {N} X <N>
C--   {N} :VNUV(NFNAPPROXUV), <N> = VNUV transposée
         !c := alpha*op(a)*op(b) + beta*c    où
      CALL DGEMM( 'N', !op sur a 'N' indique aucune op
     &            'T',  !op sur b
     &            NFNAPPROXUV, !m : a(mXk) : 6X1
     &            NFNAPPROXUV, !n : b(kXn) : 1X6
     &            1, !k : a(mXk), b(kXn)
     &            POIDS * DJP12L*  NS_MACRO3D_CORIOLIS * UN_RHO , !alpha
     &            VNUV, !a
     &            NFNAPPROXUV, !first dimension of a
     &            VNUV, !b
     &            NFNAPPROXUV, !firt dimension of b (k)
     &            ZERO, !beta
     &            COR, !c
     &            NFNAPPROXUV) !first dimension of c)

C---  ASSEMBLAGE DDANS LES MATRICES
      DO J=1,NFNAPPROXUV
         JU_P12 = KLOCE( KLOCENU(J) )
         JV_P12 = KLOCE( KLOCENV(J) )
         DO I=1,NFNAPPROXUV
            IU_P12 = KLOCE(KLOCENU(I))
            IV_P12 = KLOCE(KLOCENV(I))

            VKE(IU_P12,JV_P12) = VKE(IU_P12,JV_P12) + COR(I,J)
            VKE(IV_P12,JU_P12) = VKE(IV_P12,JU_P12) - COR(I,J)
         ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMK_V_MAN
C
C Description:
C     La subroutine privée NS_MACRO3D_ASMK_V_MAN assemble dans la
C     matrice Ke la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l !pas utilisé
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     La matrice masse est lumpée. De la forme de base:
C        {N} <N> {fr} <N>
C     développée en:
C        [ N1.N1  N1.N2  N1.N3 ]
C        [ N2.N1  N2.N2  N2.N3 ] <N>{fr}
C        [ N3.N1  N3.N2  N3.N3 ]
C     lumpée en:
C        [ N1(N1+N2+N3)      0            0      ]
C        [       0      N2(N1+N2+N3)      0      ] <N>{fr}
C        [       0           0       N3(N1+N2+N3)]
C     ce qui revient à:
C        [ N1 0  0 ]
C        [ 0  N2 0 ] <N>{fr}
C        [ 0  0 N3 ]
C     qui à les mêmes composantes que la matrice:
C         {N}<N>
C
C     Masse non lumpée:
C        Dans Maxima
C        n1(x,y):= 1-x-y;
C        n2(x,y):=x;
C        n3(x,y):=y;
C        u(x,y):=n1(x,y)*u1 + n2(x,y)*u2 + n3(x,y)*u3;
C        integrate(integrate(n1(x,y)*n1(x,y)*u(x,y),y,0,1-x),x,0,1);
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK_V_MAN (  VKE,
     &                                    KNE,
     &                                    KLOCE,
     &                                    VDJE,
     &                                    VPRGL,
     &                                    VPRN,
     &                                    VPRE, !pas utilisé
     &                                    VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      INTEGER  KLOCE (15)  !NDLN*3noeuds
      INTEGER  KNE   (3)   !3 noeuds
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE  (2) !pas utilisé
      REAL*8   VDLE  (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER NO1, NO2, NO3
      INTEGER IKUF1, IKUF2, IKUF3
      INTEGER IKVF1, IKVF2, IKVF3

      REAL*8  DETJ, COEF
      REAL*8  VFROT1, VFROT2, VFROT3, VFROTS
C-----------------------------------------------------------------------

C---     CONNECTIVITÉS
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     DETERMINANT
      DETJ = VDJE(5)

C---     COEFFICIENT
C      COEF = UN_24*DETJ      !! Ml
      COEF = NS_MACRO3D_CMULT_MAN * UN_120*DETJ

C---     COEFFICIENTS DE FROTTEMENTS
      VFROT1 = COEF*VPRN(NS_IPRNO_FROT,NO1)
      VFROT2 = COEF*VPRN(NS_IPRNO_FROT,NO2)
      VFROT3 = COEF*VPRN(NS_IPRNO_FROT,NO3)

C---     SOMMATIONS DES FROTTEMENTS
      VFROTS = VFROT1 + VFROT2 + VFROT3

C---     INDICE DE DDL (HARD CODÉ : uf us vf vs h
      IKUF1 = KLOCE(1)
      IKVF1 = KLOCE(3)
      IKUF2 = KLOCE(6)
      IKVF2 = KLOCE(8)
      IKUF3 = KLOCE(11)
      IKVF3 = KLOCE(13)

C---     ASSEMBLAGE DE LA SOUS-MATRICE U.U
!!      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + VFROTS + VFROT1      !! Ml
!!      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + VFROTS + VFROT2      !! Ml
!!      VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + VFROTS + VFROT3      !! Ml
      VKE(IKUF1,IKUF1) = VKE(IKUF1,IKUF1) + DEUX*(VFROTS+VFROT1+VFROT1)
      VKE(IKUF2,IKUF1) = VKE(IKUF2,IKUF1) +      (VFROTS+VFROT2+VFROT1)
      VKE(IKUF3,IKUF1) = VKE(IKUF3,IKUF1) +      (VFROTS+VFROT3+VFROT1)
      VKE(IKUF1,IKUF2) = VKE(IKUF1,IKUF2) +      (VFROTS+VFROT1+VFROT2)
      VKE(IKUF2,IKUF2) = VKE(IKUF2,IKUF2) + DEUX*(VFROTS+VFROT2+VFROT2)
      VKE(IKUF3,IKUF2) = VKE(IKUF3,IKUF2) +      (VFROTS+VFROT3+VFROT2)
      VKE(IKUF1,IKUF3) = VKE(IKUF1,IKUF3) +      (VFROTS+VFROT1+VFROT3)
      VKE(IKUF2,IKUF3) = VKE(IKUF2,IKUF3) +      (VFROTS+VFROT2+VFROT3)
      VKE(IKUF3,IKUF3) = VKE(IKUF3,IKUF3) + DEUX*(VFROTS+VFROT3+VFROT3)

C---     ASSEMBLAGE DE LA SOUS-MATRICE V.V
!!      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + VFROTS + VFROT1      !! Ml
!!      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + VFROTS + VFROT2      !! Ml
!!      VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + VFROTS + VFROT3      !! Ml
      VKE(IKVF1,IKVF1) = VKE(IKVF1,IKVF1) + DEUX*(VFROTS+VFROT1+VFROT1)
      VKE(IKVF2,IKVF1) = VKE(IKVF2,IKVF1) +      (VFROTS+VFROT2+VFROT1)
      VKE(IKVF3,IKVF1) = VKE(IKVF3,IKVF1) +      (VFROTS+VFROT3+VFROT1)
      VKE(IKVF1,IKVF2) = VKE(IKVF1,IKVF2) +      (VFROTS+VFROT1+VFROT2)
      VKE(IKVF2,IKVF2) = VKE(IKVF2,IKVF2) + DEUX*(VFROTS+VFROT2+VFROT2)
      VKE(IKVF3,IKVF2) = VKE(IKVF3,IKVF2) +      (VFROTS+VFROT3+VFROT2)
      VKE(IKVF1,IKVF3) = VKE(IKVF1,IKVF3) +      (VFROTS+VFROT1+VFROT3)
      VKE(IKVF2,IKVF3) = VKE(IKVF2,IKVF3) +      (VFROTS+VFROT2+VFROT3)
      VKE(IKVF3,IKVF3) = VKE(IKVF3,IKVF3) + DEUX*(VFROTS+VFROT3+VFROT3)

      RETURN
      END

!C************************************************************************
!C Sommaire: NS_MACRO3D_ASMK_V_DIF
!C
!C Description:
!C     La subroutine privée NS_MACRO3D_ASMK_V_DIF assemble dans la
!C     matrice Ke la contribution de volume des termes de frottement de
!C     diffusion des équations de mouvement pour un sous-élément T3.
!C
!C Entrée:
C     KNE      Connectivités du T3 au sein du T6L
C     KLOCE    Localisation élémentaire de l'élément T3
C     POIDS    Poids à donner au valeurs calculées
C     VNUV_XI  Table contenant les évaluations des fonctions N,x , N,y et N,z au point considéré
C     VDLPT
C     VDL_XIPT Table contenant les évalutation des dérivées des dl par rapport à x, y et z.
C     WPT,     !a analyser...
C     VW_XIPT  !table d'Évaluation des dérivées de W au point : à analyser
C     VNUV
C     DJP12L     Déterminant du P6 au point considéré
C     VPRN     Table des propriétés nodales, pour les limiteurs de convection
C     VPRE     Table des proprpiétés élémentaires du P6
C     NFNAPPROXUV Nombre de fonction d'approximation pour u et v
C     NDIMELV     Nombre de dimension
C
C
!C
!C Sortie:
!C     VKE
!C
!C Notes:
C  Assume NDIMELV = 3
C  On ne sait pas quoi faire avec certains termes...comme en "uw" et "vw"
C  RHO à traiter
C  Ajouter un Coeff 0/1 pour retirer ou insérer le terme
!C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMK_V_DIF(   NFNAPPROXUV,
     &                                    NDIMELV,
     &                                    KNE, !pas utilisé
     &                                    KLOCE,
     &                                    POIDS,
     &                                    VDLPT, !pas utilisé
     &                                    VDL_XIPT,
     &                                    WPT,
     &                                    VW_XIPT,
     &                                    VNUV, !pas utilisé
     &                                    VNUV_XI,
     &                                    DJP12L,
     &                                    VPRN,    !pas utilisé
     &                                    VPRE,
     &                                    VPRG,   !pas utilisé
     &                                    VKE )


      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !ZERO
      INCLUDE 'eacmmn.fc' !LM_CMMN
      INCLUDE 'egcmmn.fc' !EG_..

      INTEGER  NFNAPPROXUV
      INTEGER  NDIMELV

      INTEGER  KLOCE (15) !3sommets X 5dl par sommet
      INTEGER  KNE   (3)  !pas utilisé
      REAL*8   POIDS
      REAL*8   VDLPT(3)                ! 3 : nombre de dl par noeud
      REAL*8   VDL_XIPT(3,NDIMELV)     ! 3 : nombre de dl par noeud
      REAL*8   WPT  !a analyser...
      REAL*8   VW_XIPT(NDIMELV)
      REAL*8   VNUV(NFNAPPROXUV)
      REAL*8   VNUV_XI(NDIMELV,NFNAPPROXUV ) ! évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   DJP12L                        ! évaluation du déterminant au pt de gauss
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRE (2)             ! 2 POUR CHAQUE T3 (réécrire autrement)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)


      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d_iprev.fi'
      INCLUDE 'ns_macro3d.fc' !NS_MACRO3D_VISCO_CST

      !Dimensions
      INTEGER NDIMELV_LOC
      INTEGER NFNAPPROXUV_LOC

      PARAMETER(NDIMELV_LOC = 3)
      PARAMETER(NFNAPPROXUV_LOC = 6)

      INTEGER I      !itérateur
      INTEGER J      !itérateur

      INTEGER JU_P12, JV_P12, IU_P12, IV_P12
      INTEGER KLOCENU(NFNAPPROXUV_LOC)
      INTEGER KLOCENV(NFNAPPROXUV_LOC)
      DATA KLOCENU /1, 6, 11, 2, 7, 12/
      DATA KLOCENV /3, 8, 13, 4, 9, 14/

      REAL*8  VISCOCSTP6               !Viscosité constante sur le sous-élément (VNUM du sous-ele + NS_MACRO3D_VISCO_CST)
      REAL*8  VVISTOT(2, NDIMELV_LOC)  !Table des viscosité totales;
      REAL*8  VISXY                    !vxx vxy vyx vyy = vtot = vnum + vl + vt
      REAL*8  VISZ                     !vxz vyz = vphy + vl pas de vnum car calculé peclet 2d
      REAL*8  LM                       !longeur de mélange

      REAL*8  VDIFUU(NFNAPPROXUV_LOC,NFNAPPROXUV_LOC) !matrice contient la contribution a vke en uu
      REAL*8  VDIFUV(NFNAPPROXUV_LOC,NFNAPPROXUV_LOC) !matrice contient la contriubtion à vke en uv
      REAL*8  VDIFVU(NFNAPPROXUV_LOC,NFNAPPROXUV_LOC) !matrice contient la contriubtion à vke en vu
      REAL*8  VDIFVV(NFNAPPROXUV_LOC,NFNAPPROXUV_LOC) !matrice contient la contriubtion à vke en vv

      REAL*8  UN_RHO ! 1/rho = 1 mais laisse la liberté d'établir rho autrement
      REAL*8  CMULT  ! vaut 1 ou 2 et permet de doubler ou pas un terme
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. 6)
D     CALL ERR_PRE(NDIMELV     .EQ. 3)
D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELV_LOC)
C-----------------------------------------------------------------------

C---   UN SUR RHO ET VISCO
      UN_RHO = UN
      VISXY = VPRE(NS_IPREV_VTOT)
      VISZ = VPRE(NS_IPREV_VTOT)*(NS_MACRO3D_CMULT_VISZ) ! SANS PECLET : VPRE(NS_IPREV_VPHY)*NS_MACRO3D_CMULT_VISZ  ! pas de peclet en z; on peut désativer la visco en z

      CMULT =  POIDS * DJP12L * UN_RHO

C---  CALCUL DES TERMES
      DO J = 1, NFNAPPROXUV
         JU_P12 = KLOCE( KLOCENU(J) )
         JV_P12 = KLOCE( KLOCENV(J) )
         DO I = 1, NFNAPPROXUV
            IU_P12 = KLOCE(KLOCENU(I))
            IV_P12 = KLOCE(KLOCENV(I))

C---        2* VTXX{N,X}<N,X> + VTXY{N,Y}<N,Y> + VTXZ{N,Z}<N,Z>
            VKE(IU_P12,JU_P12) = VKE(IU_P12,JU_P12) +
     &                            CMULT* (
     &                 DEUX*
     &                  VISXY*VNUV_XI(1,I)*VNUV_XI(1,J)
     &               +  VISXY*VNUV_XI(2,I)*VNUV_XI(2,J)
     &              +   VISZ*VNUV_XI(3,I)*VNUV_XI(3,J)     )

C---        VTYX{N,X}<N,X> + 2 * VTYY{N,Y}<N,Y> + VTYZ{N,Z}<N,Z>
            VKE(IV_P12,JV_P12) =  VKE(IV_P12,JV_P12) +
     &                            CMULT* (
     &                      VISXY*VNUV_XI(1,I)*VNUV_XI(1,J) +
     &                 DEUX*
     &                        VISXY*VNUV_XI(2,I)*VNUV_XI(2,J)
     &                   +   VISZ*VNUV_XI(3,I)*VNUV_XI(3,J)     )

C---        VTYX{N,Y}<N,X>
            VKE(IU_P12,JV_P12) = VKE(IU_P12,JV_P12) +
     &                           CMULT*NS_MACRO3D_CMULT_DIFXY*
     &                           VISXY*VNUV_XI(2,I)*VNUV_XI(1,J)
C---        VTXY{N,X}<N,Y>
            VKE(IV_P12,JU_P12) = VKE(IV_P12,JU_P12) +
     &                            CMULT*NS_MACRO3D_CMULT_DIFXY*
     &                            VISXY*VNUV_XI(1,I)*VNUV_XI(2,J)

         END DO
      END DO

      RETURN
      END
!
!C************************************************************************
!C Sommaire: SV2D_CBS_ASMKE_S
!C
!C Description:
!C     La fonction SV2D_CBS_ASMK_S calcul le matrice de rigidité
!C     élémentaire due à un élément de surface.
!C     Les tables passées en paramètre sont des tables élémentaires,
!C     à part la table VDLG.
!C
!C Entrée:
!C     VDJV        Table du Jacobien Elémentaire de l'élément de volume
!C     VDJS        Table du Jacobien Elémentaire de l'élément de surface
!C     VPRG        Table de PRopriétés Globales
!C     VPRN        Table de PRopriétés Nodales de Volume
!C     VPREV       Table de PRopriétés Elémentaires de Volume
!C     VPRES       Table de PRopriétés Elémentaires de Surface pas utilisé
!C     VDLE        Table de Degrés de Libertés Elémentaires de Volume
!C     ICOTE       L'élément de surface forme le Ieme CÔTÉ du triangle
!C
!C Sortie:
!C     VKE         Matrice élémentaire
!C
!C Notes:
!C     On permute les métriques du T6L parent pour que le côté à traiter
!C     soit le premier côté (noeuds 1-2-3).
!C
!C************************************************************************
!      SUBROUTINE SV2D_CBS_ASMKE_S(VKE,
!     &                            VDJV,
!     &                            VDJS,
!     &                            VPRG,
!     &                            VPRN,
!     &                            VPREV,
!     &                            VPRES, ! pas utilisé
!     &                            VDLE,
!     &                            ICOTE)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
!      REAL*8   VDJV (EG_CMMN_NDJV)
!      REAL*8   VDJS (EG_CMMN_NDJS)
!      REAL*8   VPRG (LM_CMMN_NPRGL)
!      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
!      REAL*8   VPREV(2, 4)             ! 2 POUR CHAQUE T3
!      REAL*8   VPRES(3, 2)             ! 3 POUR CHAQUE L2 pas utilisé
!      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
!      INTEGER  ICOTE
!
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!
!      INTEGER IL2, IT3
!      INTEGER KNET3  (3, 2, 3)    ! 3 noeuds, 2 sous-elem T3, 3 cotés
!      INTEGER KLOCET3(9, 2, 3)    ! 9 ddl, 2 sous-elem T3, 3 cotés
!      INTEGER KT3    (2, 3)       ! 2 sous-elem T3, 3 cotés
!      REAL*8  VDJX(4), VDJY(4)
!      REAL*8  VDJL2(3)
!      REAL*8  VDJT3(5)
!
!      DATA KNET3  / 1,2,6,  2,3,4,  3,4,2,  4,5,6,  5,6,4,  6,1,2/
!      DATA KLOCET3/ 1, 2, 3,   4, 5, 6,  16,17,18,   ! NOEUDS 1,2,6,
!     &              4, 5, 6,   7, 8, 9,  10,11,12,   ! NOEUDS 2,3,4,
!     &              7, 8, 9,  10,11,12,   4, 5, 6,   ! NOEUDS 3,4,2,
!     &             10,11,12,  13,14,15,  16,17,18,   ! NOEUDS 4,5,6,
!     &             13,14,15,  16,17,18,  10,11,12,   ! NOEUDS 5,6,4,
!     &             16,17,18,   1, 2, 3,   4, 5, 6/   ! NOEUDS 6,1,2,
!      DATA KT3    / 1,2, 2,3, 3,1/
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(LM_CMMN_NPREV .EQ. 2*4)           ! 2 POUR CHAQUE T3
!D     CALL ERR_PRE(LM_CMMN_NPRES .EQ. 3*2)           ! 3 POUR CHAQUE L2
!D     CALL ERR_PRE(ICOTE .GE. 1 .AND. ICOTE .LE. 3)
!C-----------------------------------------------------------------------
!
!C---     MONTE LES TABLES AUX. POUR PERMUTER LES MÉTRIQUES
!      VDJX(1) = VDJV(1)
!      VDJX(2) = VDJV(2)
!      VDJX(3) = -(VDJX(1)+VDJX(2))
!      VDJX(4) = VDJX(1)
!      VDJY(1) = VDJV(3)
!      VDJY(2) = VDJV(4)
!      VDJY(3) = -(VDJY(1)+VDJY(2))
!      VDJY(4) = VDJY(1)
!
!C---     MÉTRIQUES PERMUTÉES DU SOUS-ÉLÉMENT T3
!      VDJT3(1) = UN_2*VDJX(ICOTE)
!      VDJT3(2) = UN_2*VDJX(ICOTE+1)
!      VDJT3(3) = UN_2*VDJY(ICOTE)
!      VDJT3(4) = UN_2*VDJY(ICOTE+1)
!      VDJT3(5) = UN_4*VDJV(5)
!
!C---     METRIQUES DU SOUS-ÉLÉMENT L2
!      VDJL2(1) = VDJS(1)
!      VDJL2(2) = VDJS(2)
!      VDJL2(3) = UN_2*VDJS(3)
!
!C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
!C           ============================
!      DO IL2=1,2
!         IT3 = KT3(IL2, ICOTE)
!
!C---        DIFFUSION
!         CALL SV2D_CBS_ASMK_S_DIF(VKE,
!     &                            KNET3  (1, IL2, ICOTE),
!     &                            KLOCET3(1, IL2, ICOTE),
!     &                            VDJT3,
!     &                            VDJL2,
!     &                            VPRG,
!     &                            VPRN,
!     &                            VPREV(1,IT3),
!     &                            VPRES(1,IL2), !pas utilisé
!     &                            VDLE)
!
!      ENDDO
!
!      RETURN
!      END
!
!C************************************************************************
!C Sommaire: SV2D_CBS_ASMK_S_DIF
!C
!C Description:
!C     La fonction SV2D_CBS_ASMK_S_DIF calcule la partie de diffusion de la
!C     matrice de rigidité élémentaire pour un élément de surface.
!C
!C Entrée:
!C
!C Sortie:
!C
!C Notes:
!C     On prend pour acquis que KNE et KLOCE sont pour un T3 et que
!C     l'élement de contour L2 à calculer forme le premier côté (noeuds 1-2).
!C
!C     {NL2} <NT3> {H} < NT3_1,x / H1 ; NT3_2,x / H2 ; NT3_3,x / H3>
!C************************************************************************
!      SUBROUTINE SV2D_CBS_ASMK_S_DIF(VKE,
!     &                               KNE,
!     &                               KLOCE,
!     &                               VDJV,
!     &                               VDJS,
!     &                               VPRG,
!     &                               VPRN,
!     &                               VPREV,
!     &                               VPRES, !pas utilisé
!     &                               VDLE)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
!      INTEGER  KNE  (3)
!      INTEGER  KLOCE(9)
!      REAL*8   VDJV (EG_CMMN_NDJV)
!      REAL*8   VDJS (EG_CMMN_NDJS)
!      REAL*8   VPRG (LM_CMMN_NPRGL)
!      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
!      REAL*8   VPREV(2)
!      REAL*8   VPRES(3)             ! 3 POUR CHAQUE L2 pas utilisé
!      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
!
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!      INCLUDE 'sv2d_cbs.fc'
!
!      INTEGER NO1, NO2, NO3
!      INTEGER IKU1, IKU2, IKU3, IKV1, IKV2, IKV3
!      REAL*8  COEF
!      REAL*8  H1, H2, H3, HS1, HS2
!      REAL*8  VNX, VNY
!      REAL*8  VSX, VKX, VEX
!      REAL*8  VSY, VKY, VEY
!      REAL*8  DJL2, DJT3
!      REAL*8  TIIH11, TIIH21, TIIH12, TIIH22, TIIH13, TIIH23
!      REAL*8  TIJH11, TIJH21, TIJH12, TIJH22, TIJH13, TIJH23
!C-----------------------------------------------------------------------
!
!C---     CONNECTIVITES DU T3
!      NO1 = KNE(1)
!      NO2 = KNE(2)
!      NO3 = KNE(3)
!
!C---     NORMALES ET METRIQUES DE L'ELEMENT L2
!      VNX =  VDJS(2)     ! VNX =  VTY
!      VNY = -VDJS(1)     ! VNY = -VTX
!      DJL2=  VDJS(3)
!
!C---     METRIQUES DU T3
!      VKX = VDJV(1)
!      VEX = VDJV(2)
!      VKY = VDJV(3)
!      VEY = VDJV(4)
!      VSX = -(VKX+VEX)
!      VSY = -(VKY+VEY)
!      DJT3= VDJV(5)
!
!C---     PROFONDEURS NODALES
!      H1 = VPRN(9, NO1)
!      H2 = VPRN(9, NO2)
!      H3 = VPRN(9, NO3)
!      HS1 = H1+H1 + H2
!      HS2 = H1 + H2+H2
!
!C---     COEFFICIENT
!!      COEF = UN_3*VPREV(1)*DJL2/DJT3     ! Uniquement la visco physique
!      COEF = UN_3*SV2D_CMULT_INTGCTR*VPREV(2)*DJL2/DJT3      !
!      TIJH11 = COEF*HS1/H1
!      TIJH12 = COEF*HS1/H2
!      TIJH13 = COEF*HS1/H3
!      TIJH21 = COEF*HS2/H1
!      TIJH22 = COEF*HS2/H2
!      TIJH23 = COEF*HS2/H3
!      TIIH11 = TIJH11 + TIJH11
!      TIIH12 = TIJH12 + TIJH12
!      TIIH13 = TIJH13 + TIJH13
!      TIIH21 = TIJH21 + TIJH21
!      TIIH22 = TIJH22 + TIJH22
!      TIIH23 = TIJH23 + TIJH23
!
!C---     INDICE DE DDL
!      IKU1 = KLOCE(1)
!      IKV1 = KLOCE(2)
!      IKU2 = KLOCE(4)
!      IKV2 = KLOCE(5)
!      IKU3 = KLOCE(7)
!      IKV3 = KLOCE(8)
!
!C---     SOUS-MATRICE U.U
!      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) - TIIH11*VSX*VNX - TIJH11*VSY*VNY
!      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) - TIIH21*VSX*VNX - TIJH21*VSY*VNY
!      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) - TIIH12*VKX*VNX - TIJH12*VKY*VNY
!      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) - TIIH22*VKX*VNX - TIJH22*VKY*VNY
!      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) - TIIH13*VEX*VNX - TIJH13*VEY*VNY
!      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) - TIIH23*VEX*VNX - TIJH23*VEY*VNY
!
!C---     SOUS-MATRICE U.V
!      VKE(IKU1,IKV1) = VKE(IKU1,IKV1) - TIJH11*VSX*VNY
!      VKE(IKU2,IKV1) = VKE(IKU2,IKV1) - TIJH21*VSX*VNY
!      VKE(IKU1,IKV2) = VKE(IKU1,IKV2) - TIJH12*VKX*VNY
!      VKE(IKU2,IKV2) = VKE(IKU2,IKV2) - TIJH22*VKX*VNY
!      VKE(IKU1,IKV3) = VKE(IKU1,IKV3) - TIJH13*VEX*VNY
!      VKE(IKU2,IKV3) = VKE(IKU2,IKV3) - TIJH23*VEX*VNY
!
!C---     SOUS-MATRICE V.U
!      VKE(IKV1,IKU1) = VKE(IKV1,IKU1) - TIJH11*VSY*VNX
!      VKE(IKV2,IKU1) = VKE(IKV2,IKU1) - TIJH21*VSY*VNX
!      VKE(IKV1,IKU2) = VKE(IKV1,IKU2) - TIJH12*VKY*VNX
!      VKE(IKV2,IKU2) = VKE(IKV2,IKU2) - TIJH22*VKY*VNX
!      VKE(IKV1,IKU3) = VKE(IKV1,IKU3) - TIJH13*VEY*VNX
!      VKE(IKV2,IKU3) = VKE(IKV2,IKU3) - TIJH23*VEY*VNX
!
!C---     SOUS-MATRICE V.V
!      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) - TIJH11*VSX*VNX - TIIH11*VSY*VNY
!      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) - TIJH21*VSX*VNX - TIIH21*VSY*VNY
!      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) - TIJH12*VKX*VNX - TIIH12*VKY*VNY
!      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) - TIJH22*VKX*VNX - TIIH22*VKY*VNY
!      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) - TIJH13*VEX*VNX - TIIH13*VEY*VNY
!      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) - TIJH23*VEX*VNX - TIIH23*VEY*VNY
!
!      RETURN
!      END

