C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Notes:
C************************************************************************
C$NOREFERENCE

C---     FONCTIONS PRIVÉES
      INTEGER NS_MACRO3D_REQFCLIM

C------------------------------------------------------------------------

C---     ATTRIBUTS DE GESTION
      INTEGER NS_MACRO3D_CL_NMDL
      INTEGER NS_MACRO3D_CL_HMDL
      INTEGER NS_MACRO3D_CL_NMAX
      PARAMETER (NS_MACRO3D_CL_NMAX = 20)

C---     COMMON DE GESTION
      COMMON /NS_MACRO3D_CGO/ NS_MACRO3D_CL_NMDL,
     &                       NS_MACRO3D_CL_HMDL(4, NS_MACRO3D_CL_NMAX)

C------------------------------------------------------------------------

C---     ATTRIBUTS PRIVES
      REAL*8 NS_MACRO3D_GRAVITE
      REAL*8 NS_MACRO3D_CORIOLIS
      REAL*8 NS_MACRO3D_VISCO_CST
      REAL*8 NS_MACRO3D_VISCO_LM
      REAL*8 NS_MACRO3D_VISCO_SMGO
      REAL*8 NS_MACRO3D_VISCO_BINF
      REAL*8 NS_MACRO3D_VISCO_BSUP
      REAL*8 NS_MACRO3D_DECOU_MAN
      REAL*8 NS_MACRO3D_DECOU_PORO
      REAL*8 NS_MACRO3D_DECOU_HMIN
      REAL*8 NS_MACRO3D_DECOU_CON_FACT
      REAL*8 NS_MACRO3D_DECOU_DIF_NU
      REAL*8 NS_MACRO3D_DECOU_DRC_NU
      REAL*8 NS_MACRO3D_STABI_PECLET
      REAL*8 NS_MACRO3D_STABI_DARCY
      REAL*8 NS_MACRO3D_STABI_LAPIDUS
      REAL*8 NS_MACRO3D_CMULT_CON
      REAL*8 NS_MACRO3D_CMULT_MAN
      REAL*8 NS_MACRO3D_CMULT_GRA
      REAL*8 NS_MACRO3D_CMULT_VENT
      REAL*8 NS_MACRO3D_CMULT_INTGCTR
      REAL*8 NS_MACRO3D_CMULT_WCNV     ! Vitesse verticale dans le terme de convection
      REAL*8 NS_MACRO3D_CMULT_VISTXY   ! viscosité turbulente xy
      REAL*8 NS_MACRO3D_CMULT_VISTZ    ! Viscosité turbulente z (INUTILE SI VISZ ==0)
      REAL*8 NS_MACRO3D_CMULT_VISZ     ! VISCOSITÉ TOTALE Z (AJOUT OU NON DES N,Z N,Z)
      REAL*8 NS_MACRO3D_CMULT_DIFXY    ! TERME DE DIFFUSION txy et tyX
      REAL*8 NS_MACRO3D_PNUMR_PENALITE
      REAL*8 NS_MACRO3D_PNUMR_DELPRT
      REAL*8 NS_MACRO3D_PNUMR_OMEGAKT

      COMMON/NS_MACRO3D_CDT/NS_MACRO3D_GRAVITE,
     &                    NS_MACRO3D_CORIOLIS,
     &                    NS_MACRO3D_VISCO_CST,
     &                    NS_MACRO3D_VISCO_LM,
     &                    NS_MACRO3D_VISCO_SMGO,
     &                    NS_MACRO3D_VISCO_BINF,
     &                    NS_MACRO3D_VISCO_BSUP,
     &                    NS_MACRO3D_DECOU_MAN,
     &                    NS_MACRO3D_DECOU_PORO,
     &                    NS_MACRO3D_DECOU_HMIN,
     &                    NS_MACRO3D_DECOU_CON_FACT,
     &                    NS_MACRO3D_DECOU_DIF_NU,
     &                    NS_MACRO3D_DECOU_DRC_NU,
     &                    NS_MACRO3D_STABI_PECLET,
     &                    NS_MACRO3D_STABI_DARCY,
     &                    NS_MACRO3D_STABI_LAPIDUS,
     &                    NS_MACRO3D_CMULT_CON,
     &                    NS_MACRO3D_CMULT_MAN,
     &                    NS_MACRO3D_CMULT_GRA,
     &                    NS_MACRO3D_CMULT_VENT,
     &                    NS_MACRO3D_CMULT_INTGCTR,
     &                    NS_MACRO3D_CMULT_WCNV,     ! Vitesse verticale dans le terme de convection
     &                    NS_MACRO3D_CMULT_VISTXY,   ! viscosité turbulente xy
     &                    NS_MACRO3D_CMULT_VISTZ,    ! Viscosité turbulente z (INUTILE SI VISZ ==0)
     &                    NS_MACRO3D_CMULT_VISZ,     ! VISCOSITÉ TOTALE Z (AJOUT OU NON DES N,Z N,Z)
     &                    NS_MACRO3D_CMULT_DIFXY,    ! TERME DE DIFFUSION txy et tyX
     &                    NS_MACRO3D_PNUMR_PENALITE,
     &                    NS_MACRO3D_PNUMR_DELPRT,
     &                    NS_MACRO3D_PNUMR_OMEGAKT

C$REFERENCE
