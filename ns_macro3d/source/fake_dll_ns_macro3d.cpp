//************************************************************************
// H2D2 - External declaration of public symbols
// Module: ns_macro3d
// Entry point: extern "C" void fake_dll_ns_macro3d()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:08.697780
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_NS3D
F_PROT(IC_NS3D_PST_Q_CMD, ic_ns3d_pst_q_cmd);
F_PROT(IC_NS3D_PST_Q_REQCMD, ic_ns3d_pst_q_reqcmd);
 
// ---  class IC_NS_MACRO3D
F_PROT(IC_NS_MACRO3D_PST_SIM_XEQCTR, ic_ns_macro3d_pst_sim_xeqctr);
F_PROT(IC_NS_MACRO3D_PST_SIM_XEQMTH, ic_ns_macro3d_pst_sim_xeqmth);
F_PROT(IC_NS_MACRO3D_PST_SIM_REQCLS, ic_ns_macro3d_pst_sim_reqcls);
F_PROT(IC_NS_MACRO3D_PST_SIM_REQHDL, ic_ns_macro3d_pst_sim_reqhdl);
 
// ---  class NS_MACRO3D
F_PROT(NS_MACRO3D_ASGCMN, ns_macro3d_asgcmn);
F_PROT(NS_MACRO3D_ASMF, ns_macro3d_asmf);
F_PROT(NS_MACRO3D_ASMK, ns_macro3d_asmk);
F_PROT(NS_MACRO3D_ASMKT, ns_macro3d_asmkt);
F_PROT(NS_MACRO3D_ASMKU, ns_macro3d_asmku);
F_PROT(NS_MACRO3D_CL001_COD, ns_macro3d_cl001_cod);
F_PROT(NS_MACRO3D_CL001_PRC, ns_macro3d_cl001_prc);
F_PROT(NS_MACRO3D_CL001_CLC, ns_macro3d_cl001_clc);
F_PROT(NS_MACRO3D_CL001_ASMF, ns_macro3d_cl001_asmf);
F_PROT(NS_MACRO3D_CL002_COD, ns_macro3d_cl002_cod);
F_PROT(NS_MACRO3D_CL002_PRC, ns_macro3d_cl002_prc);
F_PROT(NS_MACRO3D_CL002_CLC, ns_macro3d_cl002_clc);
F_PROT(NS_MACRO3D_CL002_ASMF, ns_macro3d_cl002_asmf);
F_PROT(NS_MACRO3D_CL003_COD, ns_macro3d_cl003_cod);
F_PROT(NS_MACRO3D_CL003_PRC, ns_macro3d_cl003_prc);
F_PROT(NS_MACRO3D_CL003_CLC, ns_macro3d_cl003_clc);
F_PROT(NS_MACRO3D_CL003_ASMF, ns_macro3d_cl003_asmf);
F_PROT(NS_MACRO3D_CL004_COD, ns_macro3d_cl004_cod);
F_PROT(NS_MACRO3D_CL004_PRC, ns_macro3d_cl004_prc);
F_PROT(NS_MACRO3D_CL004_CLC, ns_macro3d_cl004_clc);
F_PROT(NS_MACRO3D_CL004_ASMF, ns_macro3d_cl004_asmf);
F_PROT(NS_MACRO3D_CLCCLIM, ns_macro3d_clcclim);
F_PROT(NS_MACRO3D_CLCDLIB, ns_macro3d_clcdlib);
F_PROT(NS_MACRO3D_CLCPRES, ns_macro3d_clcpres);
F_PROT(NS_MACRO3D_CLCPREV, ns_macro3d_clcprev);
F_PROT(NS_MACRO3D_CLCPREVE, ns_macro3d_clcpreve);
F_PROT(NS_MACRO3D_CLCPRNO, ns_macro3d_clcprno);
F_PROT(NS_MACRO3D_CLCPRNOQFS, ns_macro3d_clcprnoqfs);
F_PROT(NS_MACRO3D_EVALUE, ns_macro3d_evalue);
F_PROT(NS_MACRO3D_PRCCLIM, ns_macro3d_prcclim);
F_PROT(NS_MACRO3D_PRCDLIB, ns_macro3d_prcdlib);
F_PROT(NS_MACRO3D_PRCPRES, ns_macro3d_prcpres);
F_PROT(NS_MACRO3D_PRCPREV, ns_macro3d_prcprev);
F_PROT(NS_MACRO3D_PRCPRGL, ns_macro3d_prcprgl);
F_PROT(NS_MACRO3D_PRCPRNO, ns_macro3d_prcprno);
F_PROT(NS_MACRO3D_PRCSOLC, ns_macro3d_prcsolc);
F_PROT(NS_MACRO3D_PRCSOLR, ns_macro3d_prcsolr);
F_PROT(NS_MACRO3D_PRNCLIM, ns_macro3d_prnclim);
F_PROT(NS_MACRO3D_PRNPRGL, ns_macro3d_prnprgl);
F_PROT(NS_MACRO3D_PRNPRNO, ns_macro3d_prnprno);
F_PROT(NS_MACRO3D_PSLCLIM, ns_macro3d_pslclim);
F_PROT(NS_MACRO3D_PSLPREV, ns_macro3d_pslprev);
F_PROT(NS_MACRO3D_PSLPRGL, ns_macro3d_pslprgl);
F_PROT(NS_MACRO3D_PSLPRNO, ns_macro3d_pslprno);
F_PROT(NS_MACRO3D_PSLSOLC, ns_macro3d_pslsolc);
F_PROT(NS_MACRO3D_PSLSOLR, ns_macro3d_pslsolr);
F_PROT(NS_MACRO3D_PST_SIM_000, ns_macro3d_pst_sim_000);
F_PROT(NS_MACRO3D_PST_SIM_999, ns_macro3d_pst_sim_999);
F_PROT(NS_MACRO3D_PST_SIM_CTR, ns_macro3d_pst_sim_ctr);
F_PROT(NS_MACRO3D_PST_SIM_DTR, ns_macro3d_pst_sim_dtr);
F_PROT(NS_MACRO3D_PST_SIM_INI, ns_macro3d_pst_sim_ini);
F_PROT(NS_MACRO3D_PST_SIM_RST, ns_macro3d_pst_sim_rst);
F_PROT(NS_MACRO3D_PST_SIM_REQHBASE, ns_macro3d_pst_sim_reqhbase);
F_PROT(NS_MACRO3D_PST_SIM_HVALIDE, ns_macro3d_pst_sim_hvalide);
F_PROT(NS_MACRO3D_PST_SIM_ACC, ns_macro3d_pst_sim_acc);
F_PROT(NS_MACRO3D_PST_SIM_FIN, ns_macro3d_pst_sim_fin);
F_PROT(NS_MACRO3D_PST_SIM_XEQ, ns_macro3d_pst_sim_xeq);
F_PROT(NS_MACRO3D_PST_SIM_ASGHSIM, ns_macro3d_pst_sim_asghsim);
F_PROT(NS_MACRO3D_PST_SIM_REQHVNO, ns_macro3d_pst_sim_reqhvno);
F_PROT(NS_MACRO3D_PST_SIM_REQNOMF, ns_macro3d_pst_sim_reqnomf);
F_PROT(NS_MACRO3D_REQNFN, ns_macro3d_reqnfn);
F_PROT(NS_MACRO3D_REQPRM, ns_macro3d_reqprm);
F_PROT(NS_MACRO3D_CLC, ns_macro3d_clc);
F_PROT(NS_MACRO3D_REQFCLIM, ns_macro3d_reqfclim);
 
// ---  class NS3D_PST
F_PROT(NS3D_PST_Q_000, ns3d_pst_q_000);
F_PROT(NS3D_PST_Q_999, ns3d_pst_q_999);
F_PROT(NS3D_PST_Q_CTR, ns3d_pst_q_ctr);
F_PROT(NS3D_PST_Q_DTR, ns3d_pst_q_dtr);
F_PROT(NS3D_PST_Q_INI, ns3d_pst_q_ini);
F_PROT(NS3D_PST_Q_RST, ns3d_pst_q_rst);
F_PROT(NS3D_PST_Q_REQHBASE, ns3d_pst_q_reqhbase);
F_PROT(NS3D_PST_Q_HVALIDE, ns3d_pst_q_hvalide);
F_PROT(NS3D_PST_Q_ACC, ns3d_pst_q_acc);
F_PROT(NS3D_PST_Q_FIN, ns3d_pst_q_fin);
F_PROT(NS3D_PST_Q_XEQ, ns3d_pst_q_xeq);
F_PROT(NS3D_PST_Q_ASGHSIM, ns3d_pst_q_asghsim);
F_PROT(NS3D_PST_Q_REQHVNO, ns3d_pst_q_reqhvno);
F_PROT(NS3D_PST_Q_REQNOMF, ns3d_pst_q_reqnomf);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_ns_macro3d()
{
   static char libname[] = "ns_macro3d";
 
   // ---  class IC_NS3D
   F_RGST(IC_NS3D_PST_Q_CMD, ic_ns3d_pst_q_cmd, libname);
   F_RGST(IC_NS3D_PST_Q_REQCMD, ic_ns3d_pst_q_reqcmd, libname);
 
   // ---  class IC_NS_MACRO3D
   F_RGST(IC_NS_MACRO3D_PST_SIM_XEQCTR, ic_ns_macro3d_pst_sim_xeqctr, libname);
   F_RGST(IC_NS_MACRO3D_PST_SIM_XEQMTH, ic_ns_macro3d_pst_sim_xeqmth, libname);
   F_RGST(IC_NS_MACRO3D_PST_SIM_REQCLS, ic_ns_macro3d_pst_sim_reqcls, libname);
   F_RGST(IC_NS_MACRO3D_PST_SIM_REQHDL, ic_ns_macro3d_pst_sim_reqhdl, libname);
 
   // ---  class NS_MACRO3D
   F_RGST(NS_MACRO3D_ASGCMN, ns_macro3d_asgcmn, libname);
   F_RGST(NS_MACRO3D_ASMF, ns_macro3d_asmf, libname);
   F_RGST(NS_MACRO3D_ASMK, ns_macro3d_asmk, libname);
   F_RGST(NS_MACRO3D_ASMKT, ns_macro3d_asmkt, libname);
   F_RGST(NS_MACRO3D_ASMKU, ns_macro3d_asmku, libname);
   F_RGST(NS_MACRO3D_CL001_COD, ns_macro3d_cl001_cod, libname);
   F_RGST(NS_MACRO3D_CL001_PRC, ns_macro3d_cl001_prc, libname);
   F_RGST(NS_MACRO3D_CL001_CLC, ns_macro3d_cl001_clc, libname);
   F_RGST(NS_MACRO3D_CL001_ASMF, ns_macro3d_cl001_asmf, libname);
   F_RGST(NS_MACRO3D_CL002_COD, ns_macro3d_cl002_cod, libname);
   F_RGST(NS_MACRO3D_CL002_PRC, ns_macro3d_cl002_prc, libname);
   F_RGST(NS_MACRO3D_CL002_CLC, ns_macro3d_cl002_clc, libname);
   F_RGST(NS_MACRO3D_CL002_ASMF, ns_macro3d_cl002_asmf, libname);
   F_RGST(NS_MACRO3D_CL003_COD, ns_macro3d_cl003_cod, libname);
   F_RGST(NS_MACRO3D_CL003_PRC, ns_macro3d_cl003_prc, libname);
   F_RGST(NS_MACRO3D_CL003_CLC, ns_macro3d_cl003_clc, libname);
   F_RGST(NS_MACRO3D_CL003_ASMF, ns_macro3d_cl003_asmf, libname);
   F_RGST(NS_MACRO3D_CL004_COD, ns_macro3d_cl004_cod, libname);
   F_RGST(NS_MACRO3D_CL004_PRC, ns_macro3d_cl004_prc, libname);
   F_RGST(NS_MACRO3D_CL004_CLC, ns_macro3d_cl004_clc, libname);
   F_RGST(NS_MACRO3D_CL004_ASMF, ns_macro3d_cl004_asmf, libname);
   F_RGST(NS_MACRO3D_CLCCLIM, ns_macro3d_clcclim, libname);
   F_RGST(NS_MACRO3D_CLCDLIB, ns_macro3d_clcdlib, libname);
   F_RGST(NS_MACRO3D_CLCPRES, ns_macro3d_clcpres, libname);
   F_RGST(NS_MACRO3D_CLCPREV, ns_macro3d_clcprev, libname);
   F_RGST(NS_MACRO3D_CLCPREVE, ns_macro3d_clcpreve, libname);
   F_RGST(NS_MACRO3D_CLCPRNO, ns_macro3d_clcprno, libname);
   F_RGST(NS_MACRO3D_CLCPRNOQFS, ns_macro3d_clcprnoqfs, libname);
   F_RGST(NS_MACRO3D_EVALUE, ns_macro3d_evalue, libname);
   F_RGST(NS_MACRO3D_PRCCLIM, ns_macro3d_prcclim, libname);
   F_RGST(NS_MACRO3D_PRCDLIB, ns_macro3d_prcdlib, libname);
   F_RGST(NS_MACRO3D_PRCPRES, ns_macro3d_prcpres, libname);
   F_RGST(NS_MACRO3D_PRCPREV, ns_macro3d_prcprev, libname);
   F_RGST(NS_MACRO3D_PRCPRGL, ns_macro3d_prcprgl, libname);
   F_RGST(NS_MACRO3D_PRCPRNO, ns_macro3d_prcprno, libname);
   F_RGST(NS_MACRO3D_PRCSOLC, ns_macro3d_prcsolc, libname);
   F_RGST(NS_MACRO3D_PRCSOLR, ns_macro3d_prcsolr, libname);
   F_RGST(NS_MACRO3D_PRNCLIM, ns_macro3d_prnclim, libname);
   F_RGST(NS_MACRO3D_PRNPRGL, ns_macro3d_prnprgl, libname);
   F_RGST(NS_MACRO3D_PRNPRNO, ns_macro3d_prnprno, libname);
   F_RGST(NS_MACRO3D_PSLCLIM, ns_macro3d_pslclim, libname);
   F_RGST(NS_MACRO3D_PSLPREV, ns_macro3d_pslprev, libname);
   F_RGST(NS_MACRO3D_PSLPRGL, ns_macro3d_pslprgl, libname);
   F_RGST(NS_MACRO3D_PSLPRNO, ns_macro3d_pslprno, libname);
   F_RGST(NS_MACRO3D_PSLSOLC, ns_macro3d_pslsolc, libname);
   F_RGST(NS_MACRO3D_PSLSOLR, ns_macro3d_pslsolr, libname);
   F_RGST(NS_MACRO3D_PST_SIM_000, ns_macro3d_pst_sim_000, libname);
   F_RGST(NS_MACRO3D_PST_SIM_999, ns_macro3d_pst_sim_999, libname);
   F_RGST(NS_MACRO3D_PST_SIM_CTR, ns_macro3d_pst_sim_ctr, libname);
   F_RGST(NS_MACRO3D_PST_SIM_DTR, ns_macro3d_pst_sim_dtr, libname);
   F_RGST(NS_MACRO3D_PST_SIM_INI, ns_macro3d_pst_sim_ini, libname);
   F_RGST(NS_MACRO3D_PST_SIM_RST, ns_macro3d_pst_sim_rst, libname);
   F_RGST(NS_MACRO3D_PST_SIM_REQHBASE, ns_macro3d_pst_sim_reqhbase, libname);
   F_RGST(NS_MACRO3D_PST_SIM_HVALIDE, ns_macro3d_pst_sim_hvalide, libname);
   F_RGST(NS_MACRO3D_PST_SIM_ACC, ns_macro3d_pst_sim_acc, libname);
   F_RGST(NS_MACRO3D_PST_SIM_FIN, ns_macro3d_pst_sim_fin, libname);
   F_RGST(NS_MACRO3D_PST_SIM_XEQ, ns_macro3d_pst_sim_xeq, libname);
   F_RGST(NS_MACRO3D_PST_SIM_ASGHSIM, ns_macro3d_pst_sim_asghsim, libname);
   F_RGST(NS_MACRO3D_PST_SIM_REQHVNO, ns_macro3d_pst_sim_reqhvno, libname);
   F_RGST(NS_MACRO3D_PST_SIM_REQNOMF, ns_macro3d_pst_sim_reqnomf, libname);
   F_RGST(NS_MACRO3D_REQNFN, ns_macro3d_reqnfn, libname);
   F_RGST(NS_MACRO3D_REQPRM, ns_macro3d_reqprm, libname);
   F_RGST(NS_MACRO3D_CLC, ns_macro3d_clc, libname);
   F_RGST(NS_MACRO3D_REQFCLIM, ns_macro3d_reqfclim, libname);
 
   // ---  class NS3D_PST
   F_RGST(NS3D_PST_Q_000, ns3d_pst_q_000, libname);
   F_RGST(NS3D_PST_Q_999, ns3d_pst_q_999, libname);
   F_RGST(NS3D_PST_Q_CTR, ns3d_pst_q_ctr, libname);
   F_RGST(NS3D_PST_Q_DTR, ns3d_pst_q_dtr, libname);
   F_RGST(NS3D_PST_Q_INI, ns3d_pst_q_ini, libname);
   F_RGST(NS3D_PST_Q_RST, ns3d_pst_q_rst, libname);
   F_RGST(NS3D_PST_Q_REQHBASE, ns3d_pst_q_reqhbase, libname);
   F_RGST(NS3D_PST_Q_HVALIDE, ns3d_pst_q_hvalide, libname);
   F_RGST(NS3D_PST_Q_ACC, ns3d_pst_q_acc, libname);
   F_RGST(NS3D_PST_Q_FIN, ns3d_pst_q_fin, libname);
   F_RGST(NS3D_PST_Q_XEQ, ns3d_pst_q_xeq, libname);
   F_RGST(NS3D_PST_Q_ASGHSIM, ns3d_pst_q_asghsim, libname);
   F_RGST(NS3D_PST_Q_REQHVNO, ns3d_pst_q_reqhvno, libname);
   F_RGST(NS3D_PST_Q_REQNOMF, ns3d_pst_q_reqnomf, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
