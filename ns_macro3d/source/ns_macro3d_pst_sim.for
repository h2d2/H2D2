C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER NS_MACRO3D_PST_SIM_000
C     INTEGER NS_MACRO3D_PST_SIM_999
C     INTEGER NS_MACRO3D_PST_SIM_CTR
C     INTEGER NS_MACRO3D_PST_SIM_DTR
C     INTEGER NS_MACRO3D_PST_SIM_INI
C     INTEGER NS_MACRO3D_PST_SIM_RST
C     INTEGER NS_MACRO3D_PST_SIM_REQHBASE
C     LOGICAL NS_MACRO3D_PST_SIM_HVALIDE
C     INTEGER NS_MACRO3D_PST_SIM_ACC
C     INTEGER NS_MACRO3D_PST_SIM_FIN
C     INTEGER NS_MACRO3D_PST_SIM_XEQ
C     INTEGER NS_MACRO3D_PST_SIM_ASGHSIM
C     INTEGER NS_MACRO3D_PST_SIM_REQHVNO
C     CHARACTER*256 NS_MACRO3D_PST_SIM_REQNOMF
C   Private:
C     INTEGER NS_MACRO3D_PST_SIM_CLC
C     INTEGER NS_MACRO3D_PST_SIM_CLC2
C     INTEGER NS_MACRO3D_PST_SIM_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NS_MACRO3D_PST_SIM_NOBJMAX,
     &                   NS_MACRO3D_PST_SIM_HBASE,
     &                   'SV2D - Post-traitement Simulation')

      NS_MACRO3D_PST_SIM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER  IERR
      EXTERNAL NS_MACRO3D_PST_SIM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NS_MACRO3D_PST_SIM_NOBJMAX,
     &                   NS_MACRO3D_PST_SIM_HBASE,
     &                   NS_MACRO3D_PST_SIM_DTR)

      NS_MACRO3D_PST_SIM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NS_MACRO3D_PST_SIM_NOBJMAX,
     &                   NS_MACRO3D_PST_SIM_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
         IOB = HOBJ - NS_MACRO3D_PST_SIM_HBASE

         NS_MACRO3D_PST_SIM_HPRNT(IOB) = 0
      ENDIF

      NS_MACRO3D_PST_SIM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NS_MACRO3D_PST_SIM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NS_MACRO3D_PST_SIM_NOBJMAX,
     &                   NS_MACRO3D_PST_SIM_HBASE)

      NS_MACRO3D_PST_SIM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL NS_MACRO3D_PST_SIM_CLC, NS_MACRO3D_PST_SIM_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = NS_MACRO3D_PST_SIM_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,
     &                                      HOBJ,NS_MACRO3D_PST_SIM_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,
     &                                      HOBJ,NS_MACRO3D_PST_SIM_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - NS_MACRO3D_PST_SIM_HBASE
         NS_MACRO3D_PST_SIM_HPRNT(IOB) = HPRNT
      ENDIF

      NS_MACRO3D_PST_SIM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - NS_MACRO3D_PST_SIM_HBASE
      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      NS_MACRO3D_PST_SIM_HPRNT(IOB) = 0

      NS_MACRO3D_PST_SIM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NS_MACRO3D_PST_SIM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'
C------------------------------------------------------------------------

      NS_MACRO3D_PST_SIM_REQHBASE = NS_MACRO3D_PST_SIM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NS_MACRO3D_PST_SIM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'
C------------------------------------------------------------------------

      NS_MACRO3D_PST_SIM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       NS_MACRO3D_PST_SIM_NOBJMAX,
     &                                       NS_MACRO3D_PST_SIM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ - NS_MACRO3D_PST_SIM_HBASE)
      NS_MACRO3D_PST_SIM_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ - NS_MACRO3D_PST_SIM_HBASE)
      NS_MACRO3D_PST_SIM_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ - NS_MACRO3D_PST_SIM_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, NS_MACRO3D_PST_SIM_NPOST)

      NS_MACRO3D_PST_SIM_XEQ = IERR
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ - NS_MACRO3D_PST_SIM_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, NS_MACRO3D_PST_SIM_NPOST)

      NS_MACRO3D_PST_SIM_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_PST_SIM_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ-NS_MACRO3D_PST_SIM_HBASE)
      NS_MACRO3D_PST_SIM_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_PST_SIM_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PST_SIM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS_MACRO3D_PST_SIM_HPRNT(HOBJ-NS_MACRO3D_PST_SIM_HBASE)
      NS_MACRO3D_PST_SIM_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée NS_MACRO3D_PST_SIM_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_CLC(HOBJ,
     &                                HELE,
     &                                NPST,
     &                                NNL,
     &                                VPOST)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELE
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      GDTA => LM_GEOM_REQGDTA(HELE)
      EDTA => LM_ELEM_REQEDTA(HELE)

C---     Fait le calcul
      IERR = NS_MACRO3D_PST_SIM_CLC2(HOBJ,
     &                               GDTA%KNGV,
     &                               GDTA%VDJV,
     &                               EDTA%VPRNO,
     &                               EDTA%VPREV,
     &                               EDTA%VDLG,
     &                               NPST,
     &                               NNL,
     &                               VPOST)

      NS_MACRO3D_PST_SIM_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée NS_MACRO3D_PST_SIM_CLC fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_CLC2(HOBJ,
     &                           KNGV,
     &                           VDJV,
     &                           VPRNO,
     &                           VPREV,
     &                           VDLG,
     &                           NPST,
     &                           NNL,
     &                           VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  DETJT3
      REAL*8  VIS1, VIS2, VIS3, VIS4
      REAL*8  VNM1, VNM2, VNM3, VNM4
      REAL*8  UX, UY, P, PA, V, Q, VP, VN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. 10)
D     CALL ERR_PRE(NNL  .EQ. EG_CMMN_NNL)
C-----------------------------------------------------------------------

C---     INITIALISATION DE VPOST ET VPRNO(LM_CMMN_NPRNO,.)
      CALL DINIT(NPST*NNL, ZERO, VPOST, 1)
!      CALL DINIT(EG_CMMN_NNL,ZERO,VPRNO(LM_CMMN_NPRNO,1),LM_CMMN_NPRNO)

C----- BOUCLE SUR LES ELEMENTS
      DO 10 IC=1,EG_CMMN_NELCOL
CDIR$ IVDEP
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITÉS DU T6
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

C---        METRIQUES DU T3
         DETJT3 = VDJV(5,IE)*UN_4

C---        VISCOSITE PHYSIQUE (UTILISER IPREV...)
         VIS1 = VPREV(1,IE)
         VIS2 = VPREV(3,IE)
         VIS3 = VPREV(5,IE)
         VIS4 = VPREV(7,IE)

C---        VISCOSITE NUMERIQUE
         VNM1 = VPREV(2,IE) - VPREV(1,IE)
         VNM2 = VPREV(4,IE) - VPREV(3,IE)
         VNM3 = VPREV(6,IE) - VPREV(5,IE)
         VNM4 = VPREV(8,IE) - VPREV(7,IE)

C---        ASSEMBLAGE AUX NOEUDS POUR LE LISSAGE
         VPOST(8,NO1) = VPOST(8,NO1) + VIS1*DETJT3
         VPOST(8,NO2) = VPOST(8,NO2) + (VIS1+VIS2+VIS4)*DETJT3 *UN_3
         VPOST(8,NO3) = VPOST(8,NO3) + VIS2*DETJT3
         VPOST(8,NO4) = VPOST(8,NO4) + (VIS2+VIS3+VIS4)*DETJT3 *UN_3
         VPOST(8,NO5) = VPOST(8,NO5) + VIS3*DETJT3
         VPOST(8,NO6) = VPOST(8,NO6) + (VIS1+VIS3+VIS4)*DETJT3 *UN_3

C---        PROPRIÉTÉ ÉLÉMENTAIRE
         VPOST(9,NO1) = VPOST(9,NO1) + VNM1*DETJT3
         VPOST(9,NO2) = VPOST(9,NO2) + (VNM1+VNM2+VNM4)*DETJT3 *UN_3
         VPOST(9,NO3) = VPOST(9,NO3) + VNM2*DETJT3
         VPOST(9,NO4) = VPOST(9,NO4) + (VNM2+VNM3+VNM4)*DETJT3 *UN_3
         VPOST(9,NO5) = VPOST(9,NO5) + VNM3*DETJT3
         VPOST(9,NO6) = VPOST(9,NO6) + (VNM1+VNM3+VNM4)*DETJT3 *UN_3

C---        ACCUMULATION DES DETJT3 (dans vpost10.... mais devrait être dans un table loc)
         VPOST(10,NO1) = VPOST(10,NO1) + DETJT3
         VPOST(10,NO2) = VPOST(10,NO2) + DETJT3
         VPOST(10,NO3) = VPOST(10,NO3) + DETJT3
         VPOST(10,NO4) = VPOST(10,NO4) + DETJT3
         VPOST(10,NO5) = VPOST(10,NO5) + DETJT3
         VPOsT(10,NO6) = VPOST(10,NO6) + DETJT3

20    CONTINUE
10    CONTINUE

C---- TRANSFERT DES VALEURS AUX NOEUDS
      DO IN=1,EG_CMMN_NNL
         UX  = VDLG(NS_IDL_Uf,IN) !VDLG(NS_IDL_UF,IN)
                                  !VPRNO(NS_IPRNO_QF,IN)/VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VDLG(NS_IDL_US,IN)
                                  !VPRNO(NS_IPRNO_QF,IN)/VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VDLG(NS_IDL_UF,IN) !note, fond seulement...
         UY  = VDLG(NS_IDL_Vf,IN) !/VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VDLG(NS_IDL_VF,IN)
                                  !VPRNO(NS_IPRNO_QS,IN)/VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VPRNO(NS_IPRNO_QF,IN)
                                  !VDLG(NS_IDL_VS,IN)
                                  !VPRNO(NS_IPRNO_QS,IN)/VPRNO(NS_IPRNO_MASSELUMPE,IN)
                                  !VDLG(NS_IDL_VF,IN)
         PA  = VPRNO(NS_IPRNO_PRFA,IN)
         P   = VDLG( NS_IDL_H ,IN) - VPRNO(NS_IPRNO_ZF ,IN)
         V   = SQRT(UX*UX+UY*UY)
         Q   = PA*V

         VP = VPOST(8,IN) / VPOST(10,IN)  !VPOST 10 contient l'acc des detJ
         VN = VPOST(9,IN) / VPost(10,IN)  !mais plus loin sera modifié... (à modifier...)

C---        CHARGEMENT DE VPOST
         VPOST( 1,IN) = UX !VOIR CE QUI EST STOCKÉ LIGNE 565
         VPOST( 2,IN) = UY !VOIR CE QUI EST STOCKÉ LIGNE 566
         VPOST( 3,IN) = VDLG(NS_IDL_H,IN)
         VPOST( 4,IN) = V
         VPOST( 5,IN) = P
         VPOST( 6,IN) = Q

         VPOST( 7,IN) = V / SQRT(NS_MACRO3D_GRAVITE*PA)

         VPOST( 8,IN) = SQRT(Q*VPRNO(NS_IPRNO_FROT,IN))
         VPOST( 9,IN) = VP
         VPOST(10,IN) = VN
      ENDDO

      !pour prochaine boucle
      CALL DINIT(EG_CMMN_NNL,ZERO,VPRNO(LM_CMMN_NPRNO,1),LM_CMMN_NPRNO)

      NS_MACRO3D_PST_SIM_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_PST_SIM_LOG
C
C Description:
C     La fonction privée NS_MACRO3D_PST_SIM_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_PST_SIM_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'ns_macro3d_pst_sim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'ns_macro3d_pst_sim.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NS_MACRO3D_PST_SIM_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. 10)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_NSMACRO3D_POST_DDL:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'u')
      IERR = PS_PSTD_LOGVAL(HNUMR,  2, NPST, NNL, VPOST, 'v')
      IERR = PS_PSTD_LOGVAL(HNUMR,  3, NPST, NNL, VPOST, 'h')
      IERR = PS_PSTD_LOGVAL(HNUMR,  4, NPST, NNL, VPOST, '|u|')
      IERR = PS_PSTD_LOGVAL(HNUMR,  5, NPST, NNL, VPOST, 'H')
      IERR = PS_PSTD_LOGVAL(HNUMR,  6, NPST, NNL, VPOST, '|q|')
      IERR = PS_PSTD_LOGVAL(HNUMR,  7, NPST, NNL, VPOST, 'Fr')
      IERR = PS_PSTD_LOGVAL(HNUMR,  8, NPST, NNL, VPOST, 'u*')
      IERR = PS_PSTD_LOGVAL(HNUMR,  9, NPST, NNL, VPOST, 'nu physical')
      IERR = PS_PSTD_LOGVAL(HNUMR, 10, NPST, NNL, VPOST, 'nu numerical')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      NS_MACRO3D_PST_SIM_LOG = ERR_TYP()
      RETURN
      END
