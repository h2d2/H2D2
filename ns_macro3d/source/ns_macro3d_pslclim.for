C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Fichier:  NS_MACRO3D_PSLCLIM
C
C Description:
C     La sous-routine NS_MACRO3D_PSLCLIM fait tout le traitement
C     post-lecture des conditions limites.
C
C Entrée:
C
C Sortie:
C
C Notes: Aucun traitement post-lecteur
C************************************************************************
      SUBROUTINE NS_MACRO3D_PSLCLIM (KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PSLCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KCLCND(4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(   LM_CMMN_NCLCNV)
      INTEGER KCLLIM(7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(   EG_CMMN_NCLNOD)
      INTEGER KCLELE(   EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END

