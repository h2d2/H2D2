C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_ASMKT
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C
C Entrée: VCORG : table des coordonnées globales
C         KLOCN : Table de localisation des noeuds
C         KNGV  : Table de localisation des éléments de volume
C         KNGS  : Table de localisation des éléments de surface
C         VDJS  : Table des métriques de surface
C         VDJV  : Table des métriques élémentaires pouvant être pré-calculées
C         VPRGL : Table des propriétés globales
C         VPRNO : Table des propriétés nodales
C         VPREV : Table des propriétés élémentaires de volume
C         VPRES : Table des propriétés élémentaires de surface (pas utilisé mais calculé... sortie)
C         VSOLR : Table des sollicitations réparties
C         KCLCND : Talbe des conditions limites
C         KCLNOD : Table pour les conditions limites
C         KDIMP : Table pour les conditions limites
C         VDIMP : Valeur des degrés de libertés imposés (conditions limites)
C         KEIMP :
C         VDLG : Table des degrés de liberté globaux
C         F_ASM :
C Sortie: HMTX :
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMKT(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES, !pas utilisé mais calculé (sortie)
     &                          VSOLC, !pas utilisé
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_ASMKT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) !pas utilisé
      REAL*8   VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER IERR

      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6)
      PARAMETER (NDLNMAX=5)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      INTEGER   KLOCE(NDLEMAX)
      REAL*8    VKE  (NDLEMAX*NDLEMAX)
      REAL*8    VKT1 (NDLEMAX*NDLEMAX)
      REAL*8    VKT2 (NDLEMAX*NDLEMAX)
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

C---     Pré-traitement avant calcul
      CALL NS_MACRO3D_CLC(VCORG,
     &                  KNGV,
     &                  KNGS,
     &                  VDJV,
     &                  VDJS,
     &                  VPRGL,
     &                  VPRNO,
     &                  VPREV,
     &                  VPRES, ! sortie
     &                  KCLCND,
     &                  VCLCNV,
     &                  KCLLIM,
     &                  KCLNOD,
     &                  KCLELE,
     &                  KDIMP,
     &                  VDIMP,
     &                  KEIMP,
     &                  VDLG,
     &                  IERR)


C---     Contributions de volume
      CALL NS_MACRO3D_ASMKT_V(KLOCE,
     &                      VKE,
     &                      VKT1,
     &                      VKT2,
     &                      VCORG,
     &                      KLOCN,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES, ! pas utilisé
     &                      VSOLR,
     &                      VDLG,
     &                      HMTX,
     &                      F_ASM)

!C---     Contributions de surface
!      CALL NS_MACRO3D_ASMKT_S(KLOCE,
!     &                      VKE,
!     &                      VKT1,
!     &                      VKT2,
!     &                      VCORG,
!     &                      KLOCN,
!     &                      KNGV,
!     &                      KNGS,
!     &                      VDJV,
!     &                      VDJS,
!     &                      VPRGL,
!     &                      VPRNO,
!     &                      VPREV,
!     &                      VPRES, ! pas utilisé
!     &                      VSOLR,
!     &                      VDLG,
!     &                      HMTX,
!     &                      F_ASM)

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_ASMKT_V
C
C Description:
C     La fonction NS_MACRO3D_ASMKT_V calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C         VPREV : Table gobale des propriétés élémentaires de volume
C         VCORG : Table des coordonnées globales
C         VPRNO : Table des propriétés nodales
C         VDLG  : Table globale des degrés de libertés
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes: NOTE DE SV2D (a vérifier)
C     La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     La perturbation d'un ddl en h sur un noeud sommet entraine une
C     modif du ddl pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restauré à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier ddl perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C
C     Note : possible a cet endroit de remettre l element en un vrai 3d
C            lors du passage aux matrices elementaires. A considérer
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMKT_V(   KLOCE,
     &                                 VKE,
     &                                 VKT,
     &                                 VKT_TR,
     &                                 VCORG,
     &                                 KLOCN,
     &                                 KNGV,
     &                                 KNGS, !pas utilisé
     &                                 VDJV,
     &                                 VDJS, !pas utilisé
     &                                 VPRGL,
     &                                 VPRNO,
     &                                 VPREV,
     &                                 VPRES, !pas utilisé
     &                                 VSOLR, !pas utilisé
     &                                 VDLG,
     &                                 HMTX,
     &                                 F_ASM )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLEV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) !pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER NDIM_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NNELE_LCL

      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NNELE_LCL = 6)


      INTEGER IERR
      INTEGER I, J, IC, IE, IN, ID,IDL !Itérateurs IN:sur les noeuds, IDL:sur les dl
      INTEGER II    !Itérateur sur le nombre de dl traité. Permet
                    ! d'inscrire un ensemble de ligne sur une même ligne
      INTEGER NO !numéro de noeud
      INTEGER KNE(6)
      REAL*8  VCORE(NDIM_LCL, NNELE_LCL) !Table des coordonnées élémentaires
      REAL*8  VDLE (NDLE_LCL) !Table des dl élémentaires
      REAL*8  VPRN (NPRN_LCL, NNELE_LCL) ! Table des propriétés nodales de l'élément
      REAL*8  VPRE (NPRN_LCL) ! Propriétés de volume de l'élément
      REAL*8  PENA
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8  TEMP

      DATA KNE / 1, 2, 3, 4, 5, 6/
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, II, IDL, ID, I, J, NO, VDL_ORI, VDL_DEL,
!$omp& VDL_INV, PENA, TEMP, KNE, KLOCE, VDLE, VPRN, VPRE, VCORE, VKE,
!$omp& VKT, VKT_TR, IERR)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)


C---    TRANSFERT DE TABLE GLOBALES VERS TABLES ÉLÉMENTAIRES
C---    TRANSFERTS DIRECTS
C---    Table des propriétés élémentaires
        CALL DCOPY(LM_CMMN_NPREV, VPREV(1,IE), 1, VPRE(1), 1)

C---    TRANSFERTS DEVANT BOUClER SUR LES NOEUDS
         II=1                   ! itérateur pour mettre les lignes d'une
                                ! table ndim sur la même ligne d'une table 1dim.
         DO IN=1,EG_CMMN_NNELV ! Boucle sur les noeuds d'un élémnet
            NO = KNGV(IN,IE) ! ! numéro du noeud IN de l'élément
c---        Transfert des coordonnées
            CALL DCOPY(EG_CMMN_NDIM,  VCORG(1,NO), 1, VCORE(1,IN), 1)
c---        Transfert des propriétés nodales
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(1,IN), 1)

C---        TRANSFERTS DEVANT BOUCLER SUR LES NOEUDS ET LEUR DL
            DO IDL = 1, LM_CMMN_NDLN ! Itère sur les 5 dl d'un noeud

C---           Table de localisation élémentaire
               KLOCE(II) = KLOCN(IDL,NO)
C---           table de dl
               VDLE(II) = VDLG(IDL, NO)
            II = II + 1
            END DO  ! Itère sur les 5 dl d'un noeud
         ENDDO ! Boucle sur les noeuds d'un élémnet

C---        INITIALISE LA MATRICE ELEMENTAIRE
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        [K(u)]
         CALL NS_MACRO3D_ASMKE_V(   VKE,
     &                              VCORE,
     &                              VDJV(1,IE),
     &                              VPRGL,
     &                              VPRN,
     &                              VPRE,
     &                              VDLE  )

C---        INITIALISE LA MATRICE DES DÉRIVÉES
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT,1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,LM_CMMN_NDLEV
            IF (KLOCE(ID) .EQ. 0) GOTO 199

C---           PERTURBE LE DDL
            VDL_ORI = VDLE(ID)
            IF (ABS(VDLE(ID)) .LE. PETIT) THEN
               VDL_DEL = NS_MACRO3D_PNUMR_DELPRT
            ELSE
               VDL_DEL = VDLE(ID)*NS_MACRO3D_PNUMR_DELPRT
            ENDIF
            VDL_INV = NS_MACRO3D_PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID) = VDLE(ID) + VDL_DEL

C---           CALCULE LES PROPRIÉTÉS NODALES PERTURBÉES
            CALL NS_MACRO3D_CLCPRNE (VPRGL, VPRN, VDLE, IERR)

C---           CALCULE LES PROPRIÉTÉS ELEMENTAIRES PERTURBÉES
            CALL NS_MACRO3D_CLCPREVE(  VCORE,
     &                                 KNE,
     &                                 VDJV(1,IE),
     &                                 VPRGL,
     &                                 VPRN,
     &                                 VPRE,
     &                                 VDLE,
     &                                 IERR  )

C---           INITIALISE LA MATRICE DE TRAVAIL
            CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKT_TR, 1)

C---           [K(u + du_id)]
            CALL NS_MACRO3D_ASMKE_V(   VKT_TR,
     &                                 VCORE,
     &                                 VDJV(1,IE),
     &                                 VPRGL,
     &                                 VPRN,
     &                                 VPREV(1,IE),
     &                                 VDLE  )

C---           RESTAURE LA VALEUR ORIGINALE (avant? après?)
            VDLE(ID) = VDL_ORI

C---           ([K(u + du_id)] - [K(u)]){u}/du
            DO J=1,LM_CMMN_NDLEV
               TEMP = VDL_INV*VDLE(J)
               DO I=1,LM_CMMN_NDLEV
                  VKT(I, ID) = VKT(I,ID) + (VKT_TR(I,J)-VKE(I,J))*TEMP
               ENDDO
            ENDDO

199         CONTINUE
         ENDDO

C---        SOMME [K(u)] ET LA DÉRIVÉE
         CALL DAXPY(LM_CMMN_NDLEV*LM_CMMN_NDLEV, UN, VKT, 1, VKE, 1)

C---        TERME PETIT SUR LA DIAGONALE
         PENA = NS_MACRO3D_PNUMR_PENALITE*VDJV(5,IE)
         DO ID=1,LM_CMMN_NDLEV
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = PENA
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE
!$omp end parallel

      RETURN
      END

!C************************************************************************
!C Sommaire: NS_MACRO3D_ASMKT_S
!C
!C Description:
!C     La fonction NS_MACRO3D_ASMKT_S calcul la matrice tangente élémentaire
!C     due à un élément de surface.
!C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
!C
!C Entrée:
!C
!C Sortie:
!C
!C Notes:
!C     c.f. NS_MACRO3D_ASMKT_V
!C
!C     Le noeud opposé à l'élément de surface est perturbé en trop.
!C     La matrice Ke peut être nulle.
!C************************************************************************
!      SUBROUTINE NS_MACRO3D_ASMKT_S(KLOCE,
!     &                            VKE,
!     &                            VKT,
!     &                            VKT_TR,
!     &                            VCORG,
!     &                            KLOCN,
!     &                            KNGV,
!     &                            KNGS,
!     &                            VDJV,
!     &                            VDJS,
!     &                            VPRGL,
!     &                            VPRNO,
!     &                            VPREV,
!     &                            VPRES, ! pas utilisé
!     &                            VSOLR,
!     &                            VDLG,
!     &                            HMTX,
!     &                            F_ASM)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      INTEGER  KLOCE (LM_CMMN_NDLEV)
!      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
!      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
!      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
!      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
!      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
!      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
!      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
!      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
!      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
!      REAL*8   VPRGL (LM_CMMN_NPRGL)
!      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
!      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
!      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
!      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
!      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
!      INTEGER  HMTX
!      INTEGER  F_ASM
!      EXTERNAL F_ASM
!
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!      INCLUDE 'ns_macro3d.fc'
!
!      INTEGER NDIM_LCL
!      INTEGER NDLE_LCL
!      INTEGER NPRN_LCL
!      INTEGER NPRE_LCL
!      PARAMETER (NDIM_LCL =  2)
!      PARAMETER (NDLE_LCL = 18)
!      PARAMETER (NPRN_LCL = 14)
!      PARAMETER (NPRE_LCL =  8)
!
!      INTEGER IERR
!      INTEGER I, J
!      INTEGER IN, ID, II, NO
!      INTEGER IES, IEV, ICT
!      INTEGER KNE(6)
!      REAL*8  VCORE(NDIM_LCL, 6)
!      REAL*8  VDLEV(NDLE_LCL)
!      REAL*8  VPRNV(NPRN_LCL, 6)
!      REAL*8  VPRE (NPRE_LCL)
!      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
!      REAL*8  TEMP
!
!      REAL*8  DELPRT
!      PARAMETER (DELPRT = 1.0D-08)
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
!D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
!D     CALL ERR_PRE(NPRE_LCL .EQ. LM_CMMN_NPREV)
!C-----------------------------------------------------------------------
!
!      DO I=1,6
!         KNE(I)=I
!      ENDDO
!
!C-------  BOUCLE SUR LES ELEMENTS
!C         =======================
!      DO IES=1,EG_CMMN_NELS
!
!C---        ELEMENT PARENT ET COTÉ
!         IEV = KNGS(4,IES)
!         ICT = KNGS(5,IES)
!
!C---        TABLE KLOCE DE L'ÉLÉMENT T6L
!         II=0
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            KLOCE(II+1) = KLOCN(1,NO)
!            KLOCE(II+2) = KLOCN(2,NO)
!            KLOCE(II+3) = KLOCN(3,NO)
!            II=II+3
!         ENDDO
!
!C---        TRANSFERT DES DDL DU T6L
!         II=0
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            VDLEV(II+1) = VDLG(1,NO)
!            VDLEV(II+2) = VDLG(2,NO)
!            VDLEV(II+3) = VDLG(3,NO)
!            II=II+3
!         ENDDO
!
!C---        TRANSFERT DES VCORG
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            CALL DCOPY(EG_CMMN_NDIM, VCORG(1,NO), 1, VCORE(1,IN), 1)
!         ENDDO
!
!C---        TRANSFERT DES PRNO
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(1,IN), 1)
!         ENDDO
!
!C---        TRANSFERT DES PREV
!         CALL DCOPY(LM_CMMN_NPREV, VPREV(1,IEV), 1, VPRE(1), 1)
!
!C---        INITIALISE LA MATRICE ELEMENTAIRE
!         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
!
!C---        [K(u)]
!         CALL NS_MACRO3D_ASMKE_S(VKE,
!     &                         VDJV(1,IEV),
!     &                         VDJS(1,IES),
!     &                         VPRGL,
!     &                         VPRNV,
!     &                         VPRE,
!     &                         VPRES(1,IES), ! pas utilisé
!     &                         VDLEV,
!     &                         ICT)
!
!C---        INITIALISE LA MATRICE DES DÉRIVÉES
!         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT,1)
!
!C---        BOUCLE DE PERTURBATION SUR LES DDL
!C           ==================================
!         DO ID=1,LM_CMMN_NDLEV
!            IF (KLOCE(ID) .EQ. 0) GOTO 199
!
!C---           PERTURBE LE DDL DU T6L
!            VDL_ORI = VDLEV(ID)
!            IF (ABS(VDLEV(ID)) .LE. PETIT) THEN
!               VDL_DEL = DELPRT
!            ELSE
!               VDL_DEL = VDLEV(ID)*DELPRT
!            ENDIF
!            VDL_INV = UN / VDL_DEL
!            VDLEV(ID) = VDLEV(ID) + VDL_DEL
!
!C---           CALCULE LES PROPRIÉTÉS NODALES PERTURBÉES
!            CALL NS_MACRO3D_CLCPRNE (VPRGL, VPRNV, VDLEV, IERR)
!
!C---           CALCULE LES PROPRIÉTÉS ELEMENTAIRES PERTURBÉES
!            CALL NS_MACRO3D_CLCPREVE(VCORE,
!     &                             KNE,
!     &                             VDJV(1,IEV),
!     &                             VPRGL,
!     &                             VPRNV,
!     &                             VPRE,
!     &                             VDLEV,
!     &                             IERR)
!
!C---           INITIALISE LA MATRICE DE TRAVAIL
!            CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKT_TR, 1)
!
!C---           [K(u + du_id)]
!            CALL NS_MACRO3D_ASMKE_S(VKT_TR,
!     &                            VDJV(1,IEV),
!     &                            VDJS(1,IES),
!     &                            VPRGL,
!     &                            VPRNV,
!     &                            VPRE,
!     &                            VPRES(1,IES), ! pas utilisé
!     &                            VDLEV,
!     &                            ICT)
!
!C---           RESTAURE LA VALEUR ORIGINALE
!            VDLEV(ID) = VDL_ORI
!
!C---           ([K(u + du_id)] - [K(u)]){u}/du
!            DO J=1,LM_CMMN_NDLEV
!               TEMP = VDL_INV*VDLEV(J)
!               DO I=1,LM_CMMN_NDLEV
!                  VKT(I, ID) = VKT(I,ID) + (VKT_TR(I,J)-VKE(I,J))*TEMP
!               ENDDO
!            ENDDO
!
!199         CONTINUE
!         ENDDO
!
!C---        SOMME [K(u)] ET LA DÉRIVÉE
!         CALL DAXPY(LM_CMMN_NDLEV*LM_CMMN_NDLEV, UN, VKT, 1, VKE, 1)
!
!C---       ASSEMBLAGE DE LA MATRICE
!         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
!D        IF (ERR_BAD()) THEN
!D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
!D              CALL ERR_RESET()
!D           ELSE
!D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
!D              CALL LOG_ECRIS(LOG_BUF)
!D           ENDIF
!D        ENDIF
!
!      ENDDO
!
!      IF (.NOT. ERR_GOOD() .AND.
!     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
!         CALL ERR_RESET()
!      ENDIF
!
!      RETURN
!      END
!
