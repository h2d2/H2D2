C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_CLCCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCCLIM(VCORG,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES, !allocalisation à KI seulement
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCCLIM
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  IERR

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(17)

      INTEGER  NS_MACRO3D_CLCCLIM_CB
      EXTERNAL NS_MACRO3D_CLCCLIM_CB
C-----------------------------------------------------------------------

      KI( 1) = SO_ALLC_REQKHND(KNGV)
      KI( 2) = SO_ALLC_REQKHND(KNGS)
      KI( 3) = SO_ALLC_REQVHND(VDJV)
      KI( 4) = SO_ALLC_REQVHND(VDJS)
      KI( 5) = SO_ALLC_REQVHND(VPRGL)
      KI( 6) = SO_ALLC_REQVHND(VPRNO)
      KI( 7) = SO_ALLC_REQVHND(VPREV)
      KI( 8) = SO_ALLC_REQVHND(VPRES)
      KI( 9) = SO_ALLC_REQKHND(KCLCND)
      KI(10) = SO_ALLC_REQVHND(VCLCNV)
      KI(11) = SO_ALLC_REQKHND(KCLLIM)
      KI(12) = SO_ALLC_REQKHND(KCLNOD)
      KI(13) = SO_ALLC_REQKHND(KCLELE)
      KI(14) = SO_ALLC_REQKHND(KDIMP)
      KI(15) = SO_ALLC_REQVHND(VDIMP)
      KI(16) = SO_ALLC_REQKHND(KEIMP)
      KI(17) = SO_ALLC_REQVHND(VDLG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD()) IERR = NS_MACRO3D_REQFCLIM(IT, 'CLC', HFNC)
         IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC2(HFNC,
     &                                    NS_MACRO3D_CLCCLIM_CB,
     &                                    SO_ALLC_CST2B(IL),
     &                                    SO_ALLC_CST2B(KI))
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CLCCLIM_CB
C
C Description:
C     La fonction NS_MACRO3D_CLCCLIM_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CLCCLIM_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  NS_MACRO3D_CLCCLIM_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(17)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         SO_ALLC_REQKIND(KA, KI( 1)),
     &         SO_ALLC_REQKIND(KA, KI( 2)),
     &         SO_ALLC_REQKIND(KA, KI( 3)),
     &         SO_ALLC_REQKIND(KA, KI( 4)),
     &         SO_ALLC_REQKIND(KA, KI( 5)),
     &         SO_ALLC_REQKIND(KA, KI( 6)),
     &         SO_ALLC_REQKIND(KA, KI( 7)),
     &         SO_ALLC_REQKIND(KA, KI( 8)),
     &         SO_ALLC_REQKIND(KA, KI( 9)),
     &         SO_ALLC_REQKIND(KA, KI(10)),
     &         SO_ALLC_REQKIND(KA, KI(11)),
     &         SO_ALLC_REQKIND(KA, KI(12)),
     &         SO_ALLC_REQKIND(KA, KI(13)),
     &         SO_ALLC_REQKIND(KA, KI(14)),
     &         SO_ALLC_REQKIND(KA, KI(15)),
     &         SO_ALLC_REQKIND(KA, KI(16)),
     &         SO_ALLC_REQKIND(KA, KI(17)))

      NS_MACRO3D_CLCCLIM_CB = ERR_TYP()
      RETURN
      END
