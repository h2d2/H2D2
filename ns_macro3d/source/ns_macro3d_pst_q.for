C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER NS3D_PST_Q_000
C     INTEGER NS3D_PST_Q_999
C     INTEGER NS3D_PST_Q_CTR
C     INTEGER NS3D_PST_Q_DTR
C     INTEGER NS3D_PST_Q_INI
C     INTEGER NS3D_PST_Q_RST
C     INTEGER NS3D_PST_Q_REQHBASE
C     LOGICAL NS3D_PST_Q_HVALIDE
C     INTEGER NS3D_PST_Q_ACC
C     INTEGER NS3D_PST_Q_FIN
C     INTEGER NS3D_PST_Q_XEQ
C     INTEGER NS3D_PST_Q_ASGHSIM
C     INTEGER NS3D_PST_Q_REQHVNO
C     CHARACTER*256 NS3D_PST_Q_REQNOMF
C   Private:
C     INTEGER NS3D_PST_Q_CLC
C     INTEGER NS3D_PST_Q_CLC2
C     INTEGER NS3D_PST_Q_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NS3D_PST_Q_NOBJMAX,
     &                   NS3D_PST_Q_HBASE,
     &                   'NS3D_PST_Q')

      NS3D_PST_Q_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER  IERR
      EXTERNAL NS3D_PST_Q_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NS3D_PST_Q_NOBJMAX,
     &                   NS3D_PST_Q_HBASE,
     &                   NS3D_PST_Q_DTR)

      NS3D_PST_Q_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NS3D_PST_Q_NOBJMAX,
     &                   NS3D_PST_Q_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NS3D_PST_Q_HVALIDE(HOBJ))
         IOB = HOBJ - NS3D_PST_Q_HBASE

         NS3D_PST_Q_HPRNT(IOB) = 0
      ENDIF

      NS3D_PST_Q_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NS3D_PST_Q_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NS3D_PST_Q_NOBJMAX,
     &                   NS3D_PST_Q_HBASE)

      NS3D_PST_Q_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL NS3D_PST_Q_CLC, NS3D_PST_Q_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = NS3D_PST_Q_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,NS3D_PST_Q_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,HOBJ,NS3D_PST_Q_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - NS3D_PST_Q_HBASE
         NS3D_PST_Q_HPRNT(IOB) = HPRNT
      ENDIF

      NS3D_PST_Q_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - NS3D_PST_Q_HBASE
      HPRNT = NS3D_PST_Q_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      NS3D_PST_Q_HPRNT(IOB) = 0

      NS3D_PST_Q_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NS3D_PST_Q_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'
C------------------------------------------------------------------------

      NS3D_PST_Q_REQHBASE = NS3D_PST_Q_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NS3D_PST_Q_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'
C------------------------------------------------------------------------

      NS3D_PST_Q_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       NS3D_PST_Q_NOBJMAX,
     &                                       NS3D_PST_Q_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS3D_PST_Q_HPRNT(HOBJ - NS3D_PST_Q_HBASE)
      NS3D_PST_Q_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS3D_PST_Q_HPRNT(HOBJ - NS3D_PST_Q_HBASE)
      NS3D_PST_Q_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = NS3D_PST_Q_HPRNT(HOBJ - NS3D_PST_Q_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, NS3D_PST_Q_NPOST)

      NS3D_PST_Q_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION NS3D_PST_Q_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = NS3D_PST_Q_HPRNT(HOBJ - NS3D_PST_Q_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, NS3D_PST_Q_NPOST)

      NS3D_PST_Q_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NS3D_PST_Q_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS3D_PST_Q_HPRNT(HOBJ-NS3D_PST_Q_HBASE)
      NS3D_PST_Q_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: NS3D_PST_Q_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NS3D_PST_Q_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS3D_PST_Q_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = NS3D_PST_Q_HPRNT(HOBJ-NS3D_PST_Q_HBASE)
      NS3D_PST_Q_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  NS3D_CBS_PST_CLC
C
C Description:
C     La fonction privée NS3D_PST_Q_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_CLC(HOBJ,
     &                        HELE,
     &                        NPST,
     &                        NNL,
     &                        VPOST)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELE
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
      INTEGER L_TRV
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C------------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      GDTA => LM_GEOM_REQGDTA(HELE)
      EDTA => LM_ELEM_REQEDTA(HELE)

C---     Alloue la table temporaire
      L_TRV = 0
      IF (ERR_GOOD())
     &   IERR = SO_ALLC_ALLRE8(NNL, L_TRV)

C---     Fait le calcul
      IF (ERR_GOOD())
     &   IERR = NS3D_PST_Q_CLC2(HOBJ,
     &                          GDTA%KNGV,
     &                          GDTA%VDJV,
     &                          EDTA%VPRNO,
     &                          EDTA%VPREV,
     &                          EDTA%VDLG,
     &                          NPST,
     &                          NNL,
     &                          VPOST,
     &                          VA(SO_ALLC_REQVIND(VA, L_TRV)))

C---     Désalloue la table temporaire
      IF (SO_ALLC_HEXIST(L_TRV))
     &   IERR = SO_ALLC_ALLRE8(0, L_TRV)

      NS3D_PST_Q_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS3D_CR_PST
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_CLC2(HOBJ,
     &                         KNGV,
     &                         VDJV,
     &                         VPRNO,
     &                         VPREV,
     &                         VDLG,
     &                         NPST,
     &                         NNL,
     &                         VPOST,
     &                         VTRV)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)
      REAL*8  VTRV  (NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'


      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DET3
      REAL*8  QX1, QX2, QX3, QX4, QX5, QX6
      REAL*8  QY1, QY2, QY3, QY4, QY5, QY6
      REAL*8  ERRQ, DEBIQ, QMAX

      INTEGER IUF, IUS, IVF, IVS, IH
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPST .EQ. NS3D_PST_Q_NPOST)
C-----------------------------------------------------------------------

      QMAX = ZERO

C---     INITIALISATION DE VPOST ET VTRV
      CALL DINIT(EG_CMMN_NNL*2, ZERO, VPOST, 1)
      CALL DINIT(EG_CMMN_NNL,   ZERO, VTRV,  1)

C---  Indices
      IUF = NS_IDL_UF
      IVF = NS_IDL_VF
      IUS = NS_IDL_US
      IVS = NS_IDL_VS
      IH = NS_IPRNO_PRFA

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
CDIR$ IVDEP
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITÉS DU T6
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)

C---        METRIQUES DU T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)
         DET3 = VDJV(5,IE)

C---        DÉBIT UNITAIRE
         QX1 = (VDLG(IUF,NO1) + VDLG(IUS,NO1))*VPRNO(IH, NO1)
         QY1 = (VDLG(IVF,NO1) + VDLG(IVS,NO1))*VPRNO(IH, NO1)
         QX2 = (VDLG(IUF,NO2) + VDLG(IUS,NO2))*VPRNO(IH, NO2)
         QY2 = (VDLG(IVF,NO2) + VDLG(IVS,NO2))*VPRNO(IH, NO2)
         QX3 = (VDLG(IUF,NO3) + VDLG(IUS,NO3))*VPRNO(IH, NO3)
         QY3 = (VDLG(IVF,NO3) + VDLG(IVS,NO3))*VPRNO(IH, NO3)
         QX4 = (VDLG(IUF,NO4) + VDLG(IUS,NO4))*VPRNO(IH, NO4)
         QY4 = (VDLG(IVF,NO4) + VDLG(IVS,NO4))*VPRNO(IH, NO4)
         QX5 = (VDLG(IUF,NO5) + VDLG(IUS,NO5))*VPRNO(IH, NO5)
         QY5 = (VDLG(IVF,NO5) + VDLG(IVS,NO5))*VPRNO(IH, NO5)
         QX6 = (VDLG(IUF,NO6) + VDLG(IUS,NO6))*VPRNO(IH, NO6)
         QY6 = (VDLG(IVF,NO6) + VDLG(IVS,NO6))*VPRNO(IH, NO6)

C--- DERIVÉES TEMPORELLES DES NIVEAUX DE SURFACE (AVEC PRISE EN COMPTE DE LA POROSITE)
!        DHNS1DT=ZERO
!        DHNS3DT=ZERO
!        DHNS5DT=ZERO
!        IF(STEMP.EQ.'EULER') THEN
!          DHNS1DT=VDLG(3,NO1+NNT)*VPRNO(14,NO1)
!          DHNS3DT=VDLG(3,NO3+NNT)*VPRNO(14,NO3)
!          DHNS5DT=VDLG(3,NO5+NNT)*VPRNO(14,NO5)
!        ENDIF

C---        BILAN DE MASSE SUR L'ELEMENT (Q.n)
         ERRQ = ZERO !     4.D0*UN_3*DET3*(DHNS1DT+DHNS3DT+DHNS5DT)
         ERRQ = ERRQ - VEX*(QX1+QX2 + QX2+QX3) - VEY*(QY1+QY2 + QY2+QY3) ! 1-2-3
         ERRQ = ERRQ - VSX*(QX3+QX4 + QX4+QX5) - VSY*(QY3+QY4 + QY4+QY5) ! 3-4-5
         ERRQ = ERRQ - VKX*(QX5+QX6 + QX6+QX1) - VKY*(QY5+QY6 + QY6+QY1) ! 5-6-1
         QMAX = QMAX + UN_2*ERRQ

C---        ASSEMBLAGE DU BILAN DE MASSE GLOBAL
         ERRQ = UN_2*ABS(ERRQ)*DET3
         VPOST(1,NO1) = VPOST(1,NO1) + ERRQ
         VPOST(1,NO2) = VPOST(1,NO2) + ERRQ
         VPOST(1,NO3) = VPOST(1,NO3) + ERRQ
         VPOST(1,NO4) = VPOST(1,NO4) + ERRQ
         VPOST(1,NO5) = VPOST(1,NO5) + ERRQ
         VPOST(1,NO6) = VPOST(1,NO6) + ERRQ

C---        CALCUL DU DEBIT
         DEBIQ = ZERO    ! 4.D0*TIER*DETJT3*ABS(DHNS1DT+DHNS3DT+DHNS5DT)
         DEBIQ = DEBIQ + ABS(VEX*(QX1+QX2)+VEY*(QY1+QY2))  ! 1-2-3
     &                 + ABS(VEX*(QX2+QX3)+VEY*(QY2+QY3))
         DEBIQ = DEBIQ + ABS(VKX*(QX3+QX4)+VKY*(QY3+QY4))  ! 3-4-5
     &                 + ABS(VKX*(QX4+QX5)+VKY*(QY4+QY5))
         DEBIQ = DEBIQ + ABS(VSX*(QX5+QX6)+VSY*(QY5+QY6))  ! 5-6-1
     &                 + ABS(VSX*(QX6+QX1)+VSY*(QY6+QY1))
         DEBIQ = MAX(DEBIQ,PETIT)

C---        ASSEMBLAGE PAR LISSAGE DU BILAN DE MASSE LOCAL
         ERRQ  = ERRQ/(DEBIQ*UN_4)
         IF ((ERRQ/DET3) .GT. UN) ERRQ = ZERO
         VPOST(2,NO1) = VPOST(2,NO1) + ERRQ
         VPOST(2,NO2) = VPOST(2,NO2) + ERRQ
         VPOST(2,NO3) = VPOST(2,NO3) + ERRQ
         VPOST(2,NO4) = VPOST(2,NO4) + ERRQ
         VPOST(2,NO5) = VPOST(2,NO5) + ERRQ
         VPOST(2,NO6) = VPOST(2,NO6) + ERRQ

C---        ACCUMULATION DES DETJT3
         VTRV(NO1) = VTRV(NO1) + DET3
         VTRV(NO2) = VTRV(NO2) + DET3
         VTRV(NO3) = VTRV(NO3) + DET3
         VTRV(NO4) = VTRV(NO4) + DET3
         VTRV(NO5) = VTRV(NO5) + DET3
         VTRV(NO6) = VTRV(NO6) + DET3

20    CONTINUE
10    CONTINUE

C---     TRANSFERT DES VALEURS AUX NOEUDS
      QMAX = MAX(ABS(QMAX), PETIT)
      DO IN=1,EG_CMMN_NNL
         VPOST(1,IN) = 100.D0* VPOST(1,IN) / VTRV(IN)
         VPOST(2,IN) = 100.D0* VPOST(2,IN) /(VTRV(IN)*QMAX)
      ENDDO

C---     CALCUL DES BILANS DE MASSE AUX NOEUDS MILIEUX
      DO 40 IC=1,EG_CMMN_NELCOL
      DO 50 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)
         VPOST(1,NO2) = UN_2 * (VPOST(1,NO1) + VPOST(1,NO3))
         VPOST(2,NO2) = UN_2 * (VPOST(2,NO1) + VPOST(2,NO3))
         VPOST(1,NO4) = UN_2 * (VPOST(1,NO3) + VPOST(1,NO5))
         VPOST(2,NO4) = UN_2 * (VPOST(2,NO3) + VPOST(2,NO5))
         VPOST(1,NO6) = UN_2 * (VPOST(1,NO5) + VPOST(1,NO1))
         VPOST(2,NO6) = UN_2 * (VPOST(2,NO5) + VPOST(2,NO1))
50    CONTINUE
40    CONTINUE

      NS3D_PST_Q_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS3D_PST_Q_LOG
C
C Description:
C     La fonction privée NS3D_PST_Q_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS3D_PST_Q_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'ns_macro3d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'ns_macro3d_pst_q.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NS3D_PST_Q_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. NS3D_PST_Q_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_NS3D_POST_Q:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)') 'MSG_VALEUR#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR, 1, NPST, NNL, VPOST, 'global')
      IERR = PS_PSTD_LOGVAL(HNUMR, 2, NPST, NNL, VPOST, 'local')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      NS3D_PST_Q_LOG = ERR_TYP()
      RETURN
      END
