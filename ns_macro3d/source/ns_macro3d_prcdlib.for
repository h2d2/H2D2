C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_PRCDLIB
C
C Description:
C     La fonction NS_MACRO3D_PRCDLIB monte la table de localisation
C     des DDL des noeuds et assigne les conditions de diriclet dans
C     le vecteur de liberté global.
C
C Entrée:
C      INTEGER KNGV (EG_CMMN_NCELV = 6,EG_CMMN_NELV)
C           Pour chaque élément dit le numéro des noeuds
C      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
C           Pour chaque dl de chaque noeud garde le code de cl
C      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
C           Pour chaque dl de chaque noeud,si une valeur est imposée
C           (si une condition de Diriclet s'applique sur le noeud) alors
C           la valeur y est stockée.
C
C Sortie:
C      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
C           Table de localisation. Pour chaque dl, donne le numéro d'équation.
C      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
C           Vecteur global des degrés de liberté. Les conditions de
C           diriclet y seront imposées
C
C Notes: Correspond à une numérotation normale.
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRCDLIB(KNGV,
     &                              KLOCN,
     &                              KDIMP,
     &                              VDIMP,
     &                              VDLG,
     &                              IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

      INTEGER ID, IL, IN, IK
C-----------------------------------------------------------------------
      LOGICAL EST_RETIRE
      LOGICAL EST_DIRICHLET
      LOGICAL EST_PHANTOME
      LOGICAL EST_PARALLEL
      EST_DIRICHLET(ID) = BTEST(ID, EA_TPCL_DIRICHLET)
      EST_PHANTOME (ID) = BTEST(ID, EA_TPCL_PHANTOME)
      EST_PARALLEL (ID) = BTEST(ID, EA_TPCL_PARALLEL)
      EST_RETIRE(ID) = EST_DIRICHLET(ID) .OR.
     &                 EST_PHANTOME (ID)
C-----------------------------------------------------------------------

C---     Initialise KLOCN
      CALL IINIT(LM_CMMN_NDLN*EG_CMMN_NNL, 0, KLOCN, 1)

C---     Assigne les numéros d'équation
      IL = 0
      DO IN=1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IK = KDIMP(ID,IN)
            IF (.NOT. EST_RETIRE(IK)) THEN
               IL = IL + 1
               IF (.NOT. EST_PARALLEL(IK)) THEN
                  KLOCN(ID,IN) = IL
               ELSE
                  KLOCN(ID,IN) = -IL
               ENDIF
            ENDIF
         ENDDO
      ENDDO
      LM_CMMN_NEQL = IL

C---     Impose les C.L. de Dirichlet dans VDLG
      DO IN=1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IK = KDIMP(ID,IN)
            IF (EST_DIRICHLET(IK)) THEN
               VDLG(ID, IN) = VDIMP(ID, IN)
            ENDIF
         ENDDO
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
