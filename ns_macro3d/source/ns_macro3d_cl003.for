C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C      FUNCTION NS_MACRO3D_CL003_COD()
C      FUNCTION NS_MACRO3D_CL003_PRC()
C      FUNCTION NS_MACRO3D_CL003_CLC()
C      FUNCTION NS_MACRO3D_CL003_ASMF()
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_CL003_COD
C
C Description:
C     La sous-routine privée NS_MACRO3D_CL003_COD impose le degré de
C     vitesse en x comme condition de Dirichlet sur la limite <code>IL</code>.
C     Il impose à la fois la vitesse au fond et celle en surface
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C
C Sortie:
C     KDIMP          Codes des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL003_COD (  IL,
     &                                 KCLCND,
     &                                 VCLCNV,
     &                                 KCLLIM,
     &                                 KCLNOD,
     &                                 KCLELE,
     &                                 KDIMP )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL003_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL003_COD
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IDLUF  !position du dl uf parmis les autres dl (1)
      INTEGER IDLUS  !position du dl us parmis les autres dl (2)


      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER I, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------
      IDLUF = NS_IDL_UF
      IDLUS = NS_IDL_US
      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         KDIMP(IDLUF,IN) = IBSET(KDIMP(IDLUF,IN), EA_TPCL_DIRICHLET)
         KDIMP(IDLUS,IN) = IBSET(KDIMP(IDLUS,IN), EA_TPCL_DIRICHLET)
      ENDDO

      NS_MACRO3D_CL003_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL003_PRC
C
C Description:
C     La sous-routine privée NS_MACRO3D_CL003_PRC impose le degré de
C     vitesse en x comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C     KDIMP          Codes des ddl imposés
C
C Sortie:
C     VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL003_PRC (  IL,
     &                                 KCLCND,
     &                                 VCLCNV,
     &                                 KCLLIM,
     &                                 KCLNOD,
     &                                 KCLELE,
     &                                 KDIMP,
     &                                 VDIMP )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL003_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL003_PRC
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, IN, INDEB, INFIN, IV
      REAL*8  V
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 1)
      IV = KCLCND(3, IC)
      V  = VCLCNV(IV)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         VDIMP(1,IN) = V
      ENDDO

      NS_MACRO3D_CL003_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL003_CLC
C
C Description:
C     La fonction NS_MACRO3D_CL003_CLC est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL003_CLC (  IL,
     &                                 KNGV,
     &                                 KNGS,
     &                                 VDJV,
     &                                 VDJS,
     &                                 VPRGL,
     &                                 VPRNO,
     &                                 VPREV,
     &                                 VPRES, ! pas utilisé
     &                                 KCLCND,
     &                                 VCLCNV,
     &                                 KCLLIM,
     &                                 KCLNOD,
     &                                 KCLELE,
     &                                 KDIMP,
     &                                 VDIMP,
     &                                 KEIMP,
     &                                 VDLG  )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL003_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL003_CLC
      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS) !pas utilisé
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------

      NS_MACRO3D_CL003_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CL003_ASMF
C
C Description:
C     La fonction NS_MACRO3D_CL003_ASMF est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_CL003_ASMF   (  IL,
     &                                    VCORG,
     &                                    KLOCN,
     &                                    KNGV,
     &                                    KNGS,
     &                                    VDJV,
     &                                    VDJS,
     &                                    VPRGL,
     &                                    VPRNO,
     &                                    VPREV,
     &                                    VPRES, !pas utilisé
     &                                    VSOLC,
     &                                    VSOLR,
     &                                    KCLCND,
     &                                    VCLCNV,
     &                                    KCLLIM,
     &                                    KCLNOD,
     &                                    KCLELE,
     &                                    KDIMP,
     &                                    VDIMP,
     &                                    KEIMP,
     &                                    VDLG,
     &                                    VFG   )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CL003_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NS_MACRO3D_CL003_ASMF
      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS) !pas utilisé
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------

      NS_MACRO3D_CL003_ASMF = ERR_TYP()
      RETURN
      END
