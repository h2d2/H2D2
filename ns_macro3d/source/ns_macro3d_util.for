C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_CLC
C
C Description:
C     La fonction NS_MACRO3D_CLC fait le traitement de calcul qui dépend de
C     VDLG, avant l'assemblage.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il n'y a pas de calcul de sollicitations, car elles ne
C     dépendent pas des ddl.
C     n'a pas été vérifié pour ns_macro3d
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLC(VCORG,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C---     CALCULE LES PROP. NODALE
      CALL NS_MACRO3D_CLCPRNO(VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VPRGL,
     &                      VPRNO,
     &                      VDLG,
     &                      IERR)

C---     CALCULE LES PROP. ELEMENTAIRES (VOLUME ET SURFACE)
      CALL NS_MACRO3D_CLCPREV(VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VDLG,
     &                      IERR)
      CALL NS_MACRO3D_CLCPRES(VCORG,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      VDLG,
     &                      IERR)

C---     CALCULE LES COND. LIMITES
      CALL NS_MACRO3D_CLCCLIM(VCORG,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      KCLCND,
     &                      VCLCNV,
     &                      KCLLIM,
     &                      KCLNOD,
     &                      KCLELE,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP,
     &                      VDLG,
     &                      IERR)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_REQFCLIM
C
C Description:
C     La fonction NS_MACRO3D_REQFCLIM retourne la fonction de condition limites
C     correspondant au type et au nom passés en argument.
C
C Entrée:
C     INTGER        IT        Type de condition limite
C     CHARACTER*(*) NFNC      Nom de la fonction demandée
C
C Sortie:
C     INTEGER       HFNC      Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION NS_MACRO3D_REQFCLIM(IT,
     &                           NFNC,
     &                           HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_REQFCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IT
      CHARACTER*(*) NFNC
      INTEGER  HFNC

      INCLUDE 'err.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IERR
      INTEGER IM
      INTEGER CFNC
      INTEGER HMDL
      CHARACTER*(32) NOMMDL
C-----------------------------------------------------------------------

      HFNC = -1
      IERR = C_ST_CRC32(NFNC, CFNC)
      DO IM=1, NS_MACRO3D_CL_NMDL
         IF (NS_MACRO3D_CL_HMDL(1,IM) .EQ. IT .AND.
     &       NS_MACRO3D_CL_HMDL(2,IM) .EQ. CFNC) THEN
            HFNC = NS_MACRO3D_CL_HMDL(4,IM)
            GOTO 199
         ENDIF
      ENDDO
199   CONTINUE

      IF (HFNC .EQ. -1) THEN
         WRITE(NOMMDL, '(A,I3.3)') 'NS_MACRO3D_CL', IT

         IF (ERR_GOOD())
     &         IERR = SO_UTIL_REQHMDLNOM(SO_MDUL_TYP_ELEMENT_BC,
     &                                   NOMMDL,
     &                                   HMDL)
         IF (ERR_GOOD())
     &         IERR = SO_MDUL_REQFNC(HMDL, NFNC, HFNC)

         IF (ERR_GOOD()) THEN
            NS_MACRO3D_CL_NMDL = NS_MACRO3D_CL_NMDL + 1
            NS_MACRO3D_CL_HMDL(1,NS_MACRO3D_CL_NMDL) = IT
            NS_MACRO3D_CL_HMDL(2,NS_MACRO3D_CL_NMDL) = CFNC
            NS_MACRO3D_CL_HMDL(3,NS_MACRO3D_CL_NMDL) = HMDL
            NS_MACRO3D_CL_HMDL(4,NS_MACRO3D_CL_NMDL) = HFNC
         ENDIF
      ENDIF

      NS_MACRO3D_REQFCLIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Block d'initialisation.
C
C Description:
C     Le block data privé <code>NS_MACRO3D_DATA_000()</code> initialise le
C     nombre de modules de conditions limites à 0.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA NS_MACRO3D_DATA_000

      INCLUDE 'ns_macro3d.fc'

      DATA NS_MACRO3D_CL_NMDL /0/

      END


