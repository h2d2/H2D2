C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $ $
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMKU(  VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              VPREV,
     &                              VPRES, ! sortie seulement...
     &                              VSOLC, ! pas utilisé
     &                              VSOLR,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              KDIMP,
     &                              VDIMP,
     &                              KEIMP,
     &                              VDLG,
     &                              VFG   )
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie seulement
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL) ! pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND) !VÉRIFIER
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM) !VÉRIFIER
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6) !VS 12
      PARAMETER (NDLNMAX=5)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      INTEGER   KLOCE(NDLEMAX)
      REAL*8    VKE  (NDLEMAX*NDLEMAX)
      REAL*8    VFE  (NDLEMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

C---     Pré-traitement avant calcul
      CALL NS_MACRO3D_CLC( VCORG,
     &                     KNGV,
     &                     KNGS,
     &                     VDJV,
     &                     VDJS,
     &                     VPRGL,
     &                     VPRNO,
     &                     VPREV,
     &                     VPRES, ! sortie
     &                     KCLCND,
     &                     VCLCNV,
     &                     KCLLIM,
     &                     KCLNOD,
     &                     KCLELE,
     &                     KDIMP,
     &                     VDIMP,
     &                     KEIMP,
     &                     VDLG,
     &                     IERR  )

C---     Contributions de volume
      CALL NS_MACRO3D_ASMKU_V(   KLOCE,
     &                           VKE,
     &                           VFE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,  ! pas utilisé
     &                           VSOLR,
     &                           VDLG,
     &                           VFG   )

!C---     Contributions de surface
!      CALL NS_MACRO3D_ASMKU_S(KLOCE,
!     &                      VKE,
!     &                      VFE,
!     &                      VCORG,
!     &                      KLOCN,
!     &                      KNGV,
!     &                      KNGS,
!     &                      VDJV,
!     &                      VDJS,
!     &                      VPRGL,
!     &                      VPRNO,
!     &                      VPREV,
!     &                      VPRES, ! pas utilisé
!     &                      VSOLR,
!     &                      VDLG,
!     &                      VFG)

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_ASMKU_V(   KLOCE,
     &                                 VKE,
     &                                 VFE,
     &                                 VCORG,
     &                                 KLOCN,
     &                                 KNGV,
     &                                 KNGS,
     &                                 VDJV,
     &                                 VDJS,
     &                                 VPRGL,
     &                                 VPRNO,
     &                                 VPREV,
     &                                 VPRES, ! pas utilisé
     &                                 VSOLR,
     &                                 VDLG,
     &                                 VFG   )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS) ! pas utilisé
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS) ! pas utilisé
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)  ! pas utilisé
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, II, NO
      INTEGER NDLEV

      INTEGER NDIM_LCL
      INTEGER NDJV_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NNELE_LCL

      PARAMETER (NDIM_LCL = 2)
      PARAMETER (NDJV_LCL = 5)
      PARAMETER (NDLE_LCL = 30)
      PARAMETER (NPRN_LCL = 14)
      PARAMETER (NNELE_LCL = 6)


      INTEGER KNE(6)        !6 NOEUDS
      REAL*8  VDJE(NDJV_LCL)
      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL, NNELE_LCL)
      REAL*8  VCORE(NDIM_LCL, NNELE_LCL) !Table des coordonnées élémentaires

C-----------------------------------------------------------------------
D     CALL ERR_PRE(NDJV_LCL .GE. EG_CMMN_NDJV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

      NDLEV = LM_CMMN_NDLEV

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN, ID, II, NO, VKE, VFE, KNE, KLOCE, VDJE, VDLE,
!$omp& VPRN, VCORE, IERR)

C---     INITIALISE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        INITIALISE LA MATRICE ELEMENTAIRE
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        TRANSFERT DES CONNECTIVITÉS ÉLÉMENTAIRES
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO


C---        TRANSFERT DES MÉTRIQUES ÉLÉMENTAIRES
         DO ID=1,EG_CMMN_NDJV
            VDJE(ID) = VDJV(ID,IE)
         ENDDO

C---        TRANSFERT DES DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            VDLE(II+4) = VDLG(4,NO)
            VDLE(II+5) = VDLG(5,NO)
            II=II+5
         ENDDO

C---        TRANSFERT DES PRNO et DES coor
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(1,IN), 1)
            CALL DCOPY(EG_CMMN_NDIM,  VCORG(1,NO), 1, VCORE(1,IN), 1)
         ENDDO

C---        CALCUL DE LA MATRICE ELEMENTAIRE DE VOLUME
         CALL NS_MACRO3D_ASMKE_V(   VKE,
     &                              VCORE,
     &                              VDJE,
     &                              VPRGL,
     &                              VPRN,
     &                              VPREV(1,IE),
     &                              VDLE        )

C---        PRODUIT MATRICE-VECTEUR
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---        TABLE KLOCE DE L'ÉLÉMENT
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1, IN) = KLOCN(1, KNE(IN))
            KLOCE(2, IN) = KLOCN(2, KNE(IN))
            KLOCE(3, IN) = KLOCN(3, KNE(IN))
            KLOCE(4, IN) = KLOCN(4, KNE(IN))
            KLOCE(5, IN) = KLOCN(5, KNE(IN))
         ENDDO

C---       ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE
!$omp end parallel

      RETURN
      END

!C************************************************************************
!C Sommaire:
!C
!C Description:
!C
!C Entrée:
!C
!C Sortie:
!C
!C Notes:
!C     On transfert les propriétés du T6L et du L3L par simplicité de
!C     gestion. Le L3L n'aurait besoin que de deux sous-triangles T3.
!C     On assemble une matrice complète 18x18, car la matrice Ke doit
!C     être carrée pour l'assemblage.
!C
!C************************************************************************
!      SUBROUTINE NS_MACRO3D_ASMKU_S(KLOCE,
!     &                            VKE,2D

!     &                            VFE,
!     &                            VCORG,
!     &                            KLOCN,
!     &                            KNGV,
!     &                            KNGS,
!     &                            VDJV,
!     &                            VDJS,
!     &                            VPRGL,
!     &                            VPRNO,
!     &                            VPREV,
!     &                            VPRES, ! pas utilisé
!     &                            VSOLR,
!     &                            VDLG,
!     &                            VFG)
!
!      IMPLICIT NONE
!
!      INCLUDE 'eacnst.fi'
!      INCLUDE 'eacmmn.fc'
!      INCLUDE 'egcmmn.fc'
!
!      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
!      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
!      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
!      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
!      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
!      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
!      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
!      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
!      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
!      REAL*8  VPRGL (LM_CMMN_NPRGL)
!      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
!      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
!      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
!      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
!      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
!      REAL*8  VFG   (LM_CMMN_NEQ)
!
!      INCLUDE 'spelem.fi'
!      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
!
!      INTEGER IERR
!      INTEGER IN, II, NO
!      INTEGER IES, IEV, ICT
!
!      INTEGER NDLV_LCL
!      INTEGER NPRN_LCL
!      PARAMETER (NDLV_LCL = 18)
!      PARAMETER (NPRN_LCL = 14)
!
!      REAL*8  VDLEV(NDLV_LCL)
!      REAL*8  VPRNV(NPRN_LCL, 6)
!C-----------------------------------------------------------------------
!D     CALL ERR_PRE(NDLV_LCL .GE. LM_CMMN_NDLEV)
!D     CALL ERR_PRE(NPRN_LCL .EQ. LM_CMMN_NPRNO)
!C-----------------------------------------------------------------------
!
!C-------  BOUCLE SUR LES ELEMENTS
!C         =======================
!      DO IES=1,EG_CMMN_NELS
!
!C---        ELEMENT PARENT ET COTÉ
!         IEV = KNGS(4,IES)
!         ICT = KNGS(5,IES)
!
!C---        TRANSFERT DES DDL
!         II=0
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN, IEV)
!            VDLEV(II+1) = VDLG(1,NO)
!            VDLEV(II+2) = VDLG(2,NO)
!            VDLEV(II+3) = VDLG(3,NO)
!            II=II+3
!         ENDDO
!
!C---        TRANSFERT DES PRNO
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN, IEV)
!            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(1,IN), 1)
!         ENDDO
!
!C---        INITIALISE LA MATRICE ELEMENTAIRE
!         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
!
!C---        CALCUL DE LA MATRICE ELEMENTAIRE DE SURFACE
!         CALL NS_MACRO3D_ASMKE_S(VKE,
!     &                         VDJV(1,IEV),
!     &                         VDJS(1,IES),
!     &                         VPRGL,
!     &                         VPRNV,
!     &                         VPREV(1,IEV),
!     &                         VPRES(1,IES), ! pas utilisé
!     &                         VDLEV,
!     &                         ICT)
!
!C---        PRODUIT MATRICE-VECTEUR
!         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
!     &              LM_CMMN_NDLEV,     ! m rows
!     &              LM_CMMN_NDLEV,     ! n columns
!     &              UN,                ! alpha
!     &              VKE,               ! a
!     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
!     &              VDLEV,             ! x
!     &              1,                 ! incx
!     &              ZERO,              ! beta
!     &              VFE,               ! y
!     &              1)                 ! incy
!
!C---        TABLE KLOCE DE L'ÉLÉMENT T6L
!         DO IN=1,EG_CMMN_NNELV
!            NO = KNGV(IN,IEV)
!            KLOCE(1,IN) = KLOCN(1,NO)
!            KLOCE(2,IN) = KLOCN(2,NO)
!            KLOCE(3,IN) = KLOCN(3,NO)
!         ENDDO
!
!C---        ASSEMBLAGE DU VECTEUR GLOBAL
!         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)
!
!      ENDDO
!
!      RETURN
!      END
