C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : NS_MACRO3D_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRCPRGL  (VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d.fc'

      REAL*8  VLATDEG, VLATRAD
      REAL*8  DEGRAD
      REAL*8  VROTTERRE
      PARAMETER (DEGRAD=1.74532925199432977D-2)
      PARAMETER (VROTTERRE = 360.0D0*DEGRAD/(24.0D0*3600.0D0))
C-----------------------------------------------------------------------

C---     CALCULE LA FORCE DE CORIOLIS
      VLATDEG = VPRGL(2)
      VLATRAD = VLATDEG*DEGRAD
      VPRGL(30) = DEUX*VROTTERRE*SIN(VLATRAD)

C---     AFFECTE LES VALEURS AUX PROPRIÉTÉS GLOBALES
      NS_MACRO3D_GRAVITE         = VPRGL( 1)
      NS_MACRO3D_CORIOLIS        = VPRGL(30)
      NS_MACRO3D_VISCO_CST       = VPRGL( 3)
      NS_MACRO3D_VISCO_LM        = VPRGL( 4)
      NS_MACRO3D_VISCO_SMGO      = VPRGL( 5)
      NS_MACRO3D_VISCO_BINF      = VPRGL( 6)
      NS_MACRO3D_VISCO_BSUP      = VPRGL( 7)

      NS_MACRO3D_DECOU_MAN       = VPRGL( 8)
      NS_MACRO3D_DECOU_PORO      = VPRGL( 9)
      NS_MACRO3D_DECOU_HMIN      = VPRGL(10)
      NS_MACRO3D_DECOU_CON_FACT  = VPRGL(11)
      NS_MACRO3D_DECOU_DIF_NU    = VPRGL(12)
      NS_MACRO3D_DECOU_DRC_NU    = VPRGL(13)

      NS_MACRO3D_STABI_PECLET    = VPRGL(14)
      NS_MACRO3D_STABI_DARCY     = VPRGL(15)
      NS_MACRO3D_STABI_LAPIDUS   = VPRGL(16)

      NS_MACRO3D_CMULT_CON       = VPRGL(17)
      NS_MACRO3D_CMULT_GRA       = VPRGL(18)
      NS_MACRO3D_CMULT_MAN       = VPRGL(19)
      NS_MACRO3D_CMULT_VENT      = VPRGL(20)
      NS_MACRO3D_CMULT_INTGCTR   = VPRGL(21)
      NS_MACRO3D_CMULT_WCNV      = VPRGL(22)
      NS_MACRO3D_CMULT_VISTXY    = VPRGL(23)
      NS_MACRO3D_CMULT_VISTZ     = VPRGL(24)
      NS_MACRO3D_CMULT_VISZ      = VPRGL(25)
      NS_MACRO3D_CMULT_DIFXY     = VPRGL(26)
      NS_MACRO3D_PNUMR_PENALITE  = VPRGL(27)
      NS_MACRO3D_PNUMR_DELPRT    = VPRGL(28)
      NS_MACRO3D_PNUMR_OMEGAKT   = VPRGL(29)

      IERR = ERR_TYP()
      RETURN
      END
