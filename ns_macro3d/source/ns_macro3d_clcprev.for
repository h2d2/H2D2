C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  NS_MACRO3D_CLCPREV
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume.
C
C Entrée:
C     VCORG : Table des coordonnées globales - pas utilisé
C     KNGV  : Table des connectivités globales de volume
C     VDJV  : Métriques de l'élment de volume
C     VPRGL : Table des propriétés globales - pas utilisés
C     VPRNO : Table des propriétés nodales
C     VDLG  : Table des degrés de libertés globaux
C
C Sortie:
C     VPREV : Table des propriétés élémentaires de volume
C     IERR
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPREV( VCORG, !pas utilisé
     &                               KNGV,
     &                               VDJV,
     &                               VPRGL, !pas utilisé
     &                               VPRNO,
     &                               VPREV,
     &                               VDLG,
     &                               IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  *)  !pas utilisé
      INTEGER KNGV (EG_CMMN_NCELV, *)
      REAL*8  VDJV (EG_CMMN_NDJV,  *)
      REAL*8  VPRGL(LM_CMMN_NPRGL)     !pas utilisé
      REAL*8  VPRNO(LM_CMMN_NPRNO, *)
      REAL*8  VPREV(LM_CMMN_NPREV, *)
      REAL*8  VDLG (LM_CMMN_NDLN,  *)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d.fc'

      INTEGER IC, IE
    !  INTEGER NBT3
    !  REAL*8  VIS1, VIS2, VIS3, VIS4
    !  REAL*8  VISMIN, VISMAX, VISMOY
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IERR)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         CALL NS_MACRO3D_CLCPREVE(VCORG,  !pas utilisé
     &                            KNGV(1,IE),
     &                            VDJV(1,IE),
     &                            VPRGL,  !pas utilisé
     &                            VPRNO,
     &                            VPREV(1,IE),
     &                            VDLG,
     &                            IERR)

20    CONTINUE
!$omp end do
10    CONTINUE
!$omp end parallel

C---     LIMITEURS SUR LA VISCOSITÉ (éliminé car on a pas à ce stade la visco totale)
!      DO IE=1,EG_CMMN_NELV
!       VPREV(2,IE)=MAX(   MIN(VPREV(2,IE),NS_MACRO3D_VISCO_BSUP),
!     &                    NS_MACRO3D_VISCO_BINF                        )
!       VPREV(4,IE)=MAX(   MIN(VPREV(4,IE),NS_MACRO3D_VISCO_BSUP),
!     &                    NS_MACRO3D_VISCO_BINF                        )
!       VPREV(6,IE)=MAX(   MIN(VPREV(6,IE),NS_MACRO3D_VISCO_BSUP),
!     &                    NS_MACRO3D_VISCO_BINF                        )
!       VPREV(8,IE)=MAX(   MIN(VPREV(8,IE),
!     &                    NS_MACRO3D_VISCO_BSUP),NS_MACRO3D_VISCO_BINF )
!      ENDDO

C---     STATISTIQUE SUR LES VISCOSITÉS éliminé (on a pas la visco totale à ce stade)
!      NBT3   = 0
!      VISMOY = ZERO
!      VISMAX = ZERO
!      VISMIN = GRAND
!      DO IE=1,EG_CMMN_NELV
!         VIS1 = VPREV(2,IE)
!         VIS2 = VPREV(4,IE)
!         VIS3 = VPREV(6,IE)
!!         VIS4 = VPREV(8,IE)
!         VISMOY = VISMOY + VIS1 + VIS2 + VIS3 + VIS4
!         VISMAX = MAX(VISMAX, VIS1, VIS2, VIS3, VIS4)
!         VISMIN = MIN(VISMIN, VIS1, VIS2, VIS3, VIS4)
!         NBT3 = NBT3+4
!      ENDDO
!      VISMOY = VISMOY / DBLE(NBT3)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  NS_MACRO3D_CLCPREVE
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume pour
C     un élément.
C
C Entrée:
C     VCORG : Table des coordonnées globale (pas utilisé)
C     KNE   : Table des connectivités élémentaires (numéro des 6 noeuds dans maillage global)
C     VDJE  : Table des métriques élémentaires
C     VPRGL : Table des propriétés globales (pas utilisé)
C     VPRNO : Table des propriétés nodales ( on y récupère PRFA )
C     VDLG  : Table globale des degrés de libertés (on y récupère us, uf, vs, vf pour h vient plutot de PRFA)
C
C Sortie:
C     VPRE
C     IERR
C
C Notes:
C     - "4" pour nombre de sous-élément et "KNET3" devraient être placés dans des .fc globaux
C     - Nettoyer le passage aux indices
C************************************************************************
      SUBROUTINE NS_MACRO3D_CLCPREVE(VCORG, !pas utilisé
     &                               KNE,
     &                               VDJE,
     &                               VPRGL, !pas utilisé
     &                               VPRNO,
     &                               VPRE,
     &                               VDLG,
     &                               IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_CLCPREVE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !UN_3 UN_6 UN_48
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

C---  ENTRÉES
      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL) ! pas utilisé
      INTEGER KNE  (EG_CMMN_NCELV)              ! pas utilisé
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)              ! pas utilisé
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)

C---  SORTIES
      REAL*8  VPRE (LM_CMMN_NPREV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d.fc'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'ns_macro3d_iprev.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'sphdro.fi'

C---  VARIABLES LOCALES
      INTEGER IN1, IN2, IN3     ! Indice des 3 noeuds d'un T3 dans le T6L (entier entre 1 et 6)
      REAL*8  VKX2D, VEX2D, VKY2D, VEY2D !métriques du T3 (non?)
      REAL*8  DETJT3, UN_DT3          ! déterminant du T3 et son inverse
      REAL*8  US(EG_CMMN_NNELV) ! Les vitesses en en x surface aux 6 noeuds
      REAL*8  UF(EG_CMMN_NNELV) ! Les vitessses en x au fond aux 6 noeuds
      REAL*8  VS(EG_CMMN_NNELV) ! Les vitesses en y en surface aux 6 noeuds
      REAL*8  VF(EG_CMMN_NNELV) ! Les vitesses en y au fond aux 6 noeuds
      REAL*8  P(EG_CMMN_NNELV)  ! PRFA pour chaque noeud
      REAL*8  PM                ! PRFA moyen sur un sous-élément
      REAL*8  UM                ! vitesse moyenne en x sur chacun des sous éléments
      REAL*8  VM                ! vitesse moyenne en y sur chacun des sous élément
      REAL*8  LM(4)             ! Longeur de mélange sur les sous-éléments
      REAL*8  VIS1, VIS2, VIS3, VIS4
      REAL*8  VNM(4)            ! Viscosité numériques sur les sous élément
      REAL*8  CLMFIN
      INTEGER KNET3  (3, 4)     ! Connectivités du T3, pour chaque T3, donne les noeuds (1..6) associés
      INTEGER NSE               ! Nombre de sous-élément (4)
      INTEGER I                 ! Itérateur sur les noeuds ( 1 à 6) de l'élément de volume
      INTEGER IT3               ! Itérateur sur les sous-élément (1à4) de l'Élment de volume

      DATA KNET3 / 1,2,6,  2,3,4,  6,4,5,  4,6,2/ !serait à mettre dans un .fc

      REAL*8 U1, U2, U3, U4, U5, U6
      REAL*8 V1, V2, V3, V4, V5, V6
      REAL*8 UX1, UX2, UX3, UX4
      REAL*8 VX1, VX2, VX3, VX4
      REAL*8 UY1, UY2, UY3, UY4
      REAL*8 VY1, VY2, VY3, VY4
C-----------------------------------------------------------------------

C---  Nombre de sous-élément
      NSE = 4

C---  METRIQUES 2D des T3 (pour PECLET)
      VKX2D = VDJE(1)*UN_2
      VEX2D = VDJE(2)*UN_2
      VKY2D = VDJE(3)*UN_2
      VEY2D = VDJE(4)*UN_2

C---  DETERMINANT DU T3 (2D)
      DETJT3 = UN_4*VDJE(5) ! voir pkoi le UN_4 -> 4 sous éléments. Ce n'est pas le detJ mais le vol. Or pas "vraiment" le vol car rien ne dit que les 4 T3 ont le même vol.
      UN_DT3 = UN / DETJT3

C---- COEFICIENT POUR LA LONGUEUR DE MELANGE
      CLMFIN = NS_MACRO3D_VISCO_LM + NS_MACRO3D_VISCO_SMGO*SQRT(DETJT3) ! LM CALCULÉ EN FN DE LA GRANDEUR 2d

C---  ITÉRATION SUR LES NOEUDS DU T6L
      DO I=1,EG_CMMN_NNELV
        US(I) = VDLG(NS_IDL_US,KNE(I))
        VS(I) = VDLG(NS_IDL_VS,KNE(I))
        UF(I) = VDLG(NS_IDL_UF,KNE(I))
        VF(I) = VDLG(NS_IDL_VF,KNE(I))
        P(I) =  VPRNO(NS_IPRNO_PRFA,KNE(I))
      END DO

      U1 = (US(1) + UF(1))*UN_2
      U2 = (US(2) + UF(2))*UN_2
      U3 = (US(3) + UF(3))*UN_2
      U4 = (US(4) + UF(4))*UN_2
      U5 = (US(5) + UF(5))*UN_2
      U6 = (US(6) + UF(6))*UN_2

      V1 = (VS(1) + VF(1))*UN_2
      V2 = (VS(2) + VF(2))*UN_2
      V3 = (VS(3) + VF(3))*UN_2
      V4 = (VS(4) + VF(4))*UN_2
      V5 = (VS(5) + VF(5))*UN_2
      V6 = (VS(6) + vF(6))*UN_2

C---     DÉRIVÉ EN X DE U SUR LES 4 T3
      UX1 = VKX2D*(U2-U1)+VEX2D*(U6-U1)
      UX2 = VKX2D*(U3-U2)+VEX2D*(U4-U2)
      UX3 = VKX2D*(U4-U6)+VEX2D*(U5-U6)
      UX4 = -(VKX2D*(U6-U4)+VEX2D*(U2-U4))

C---     DÉRIVÉ EN Y DE U SUR LES 4 T3
      UY1 = VKY2D*(U2-U1)+VEY2D*(U6-U1)
      UY2 = VKY2D*(U3-U2)+VEY2D*(U4-U2)
      UY3 = VKY2D*(U4-U6)+VEY2D*(U5-U6)
      UY4 = - (VKY2D*(U6-U4)+VEY2D*(U2-U4))

C---     DÉRIVÉ EN X DE V SUR LES 4 T3
      VX1 = VKX2D*(V2-V1)+VEX2D*(V6-V1)
      VX2 = VKX2D*(V3-V2)+VEX2D*(V4-V2)
      VX3 = VKX2D*(V4-V6)+VEX2D*(V5-V6)
      VX4 = -(VKX2D*(V6-V4)+VEX2D*(V2-V4))

C---     DÉRIVÉ EN Y DE V SUR LES 4 T3
      VY1 = VKY2D*(V2-V1)+VEY2D*(V6-V1)
      VY2 = VKY2D*(V3-V2)+VEY2D*(V4-V2)
      VY3 = VKY2D*(V4-V6)+VEY2D*(V5-V6)
      VY4 = -(VKY2D*(V6-V4)+VEY2D*(V2-V4))

C---  ITÉRATION SUR LES SOUS-ÉLÉMENTS
      DO IT3=1, NSE
         IN1 = KNET3(1,IT3)
         IN2 = KNET3(2,IT3)
         IN3 = KNET3(3,IT3)

C----       Vitesse en x moyenne sur le sous élément
         UM = UN_6*(US(IN1)+UF(IN1) + US(IN2)+UF(IN2) + US(IN3)+UF(IN3))
         VM = UN_6*(VS(IN1)+VF(IN1) + VS(IN2)+VF(IN2) + VS(IN3)+VF(IN3))

!         UM(IT3) = UN_48/(P(IN1) + P(IN2) + P(IN3))*(
!     &               ( US(IN1)+ UF(IN1) )*(2*P(IN1)+ P(IN2)+P(IN3) )
!     &             + ( US(IN2)+ UF(IN2) )*(P(IN1) +2*P(IN2)+P(IN3) )
!     &             + ( US(IN3)+ UF(IN3) )*( P(IN1)+P(IN2)+2*P(IN3) )  )

C----    Vitesse en y moyenne sur le sous-élément
!         VM(IT3) = UN_48/(P(IN1) + P(IN2) + P(IN3))*(
!     &               ( VS(IN1)+ VF(IN1) )*(2*P(IN1)+ P(IN2)+P(IN3) )
!     &             + ( VS(IN2)+ VF(IN2) )*(P(IN1) +2*P(IN2)+P(IN3) )
!     &             + ( VS(IN3)+ VF(IN3) )*( P(IN1)+P(IN2)+2*P(IN3) )  )

C----    Profondeur moyenne sur le sous élément
         PM = UN_3*(P(IN1) + P(IN2) + P(IN3))

C---     LONGUEUR DE MELANGE FINALE SUR LE sous-élément
         LM(IT3) = CLMFIN * PM

C---     Calcul de la viscosité numérique : avec un T3 2d pour l'instant
         VNM(IT3) = SP_HDRO_PECLET(UN,
     &                             UM,
     &                             VM,
     &                             PM,
     &                             VEY2D,
     &                            -VEX2D,
     &                            -VKY2D,
     &                             VKX2D) / NS_MACRO3D_STABI_PECLET

      END DO ! FIN ITÉRATION SUR LES SOUS ÉLÉMENT

C---     CALCUL DE LA VISCOSITÉ TURBULENTE  PRB : FN DE LA POSITION
      VIS1 = UN_DT3*LM(1)*LM(1)*
     &          SQRT(DEUX*UX1*UX1 + DEUX*VY1*VY1 + (UY1+VX1)*(UY1+VX1))
      VIS2 = UN_DT3*LM(2)*LM(2)*
     &          SQRT(DEUX*UX2*UX2 + DEUX*VY2*VY2 + (UY2+VX2)*(UY2+VX2))
      VIS3 = UN_DT3*LM(3)*LM(3)*
     &          SQRT(DEUX*UX3*UX3 + DEUX*VY3*VY3 + (UY3+VX3)*(UY3+VX3))
      VIS4 = UN_DT3*LM(4)*LM(4)*
     &          SQRT(DEUX*UX4*UX4 + DEUX*VY4*VY4 + (UY4+VX4)*(UY4+VX4))

C---  PROPRIÉTÉ ÉLÉMENTAIRE : par rapport à SV2D, on enregistre (pour le moment) la visco numérique
C                             et le coefficient de longueur de mélange. On ne peut pas enregistrer
C                             la visco turbulente pour l'instant...

      VPRE(NS_IPREV_VPHY1) = NS_MACRO3D_VISCO_CST + VIS1    ! Visco physique
      VPRE(NS_IPREV_VTOT1) = VPRE(NS_IPREV_VPHY1) + VNM(1)

      VPRE(NS_IPREV_VPHY2) = NS_MACRO3D_VISCO_CST + VIS2    ! Visco physique
      VPRE(NS_IPREV_VTOT2) = VPRE(NS_IPREV_VPHY2) + VNM(2)

      VPRE(NS_IPREV_VPHY3) = NS_MACRO3D_VISCO_CST + VIS3    ! Visco physique
      VPRE(NS_IPREV_VTOT3) = VPRE(NS_IPREV_VPHY3) + VNM(3)

      VPRE(NS_IPREV_VPHY4) = NS_MACRO3D_VISCO_CST + VIS4    ! Visco physique
      VPRE(NS_IPREV_VTOT4) = VPRE(NS_IPREV_VPHY4) + VNM(4)

      IERR = ERR_TYP()
      RETURN
      END

