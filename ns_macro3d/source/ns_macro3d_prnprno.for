C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: NS_MACRO3D_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C     VOIR NS_MACRO3d_iprno pour définition des prno
C
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRNPRNO
CDEC$ ENDIF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_ECRIS('--------DEVRONT ETRE MISES A JOUR --------------')
      WRITE(LOG_BUF,1000) 'PROPRIETES NODALES LUES :'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 1, 'TOPOGRAPHIE DU FOND'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 2, 'COEFFICIENT DE MANNING'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 3, 'EPAISSEUR DE GLACE'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 4, 'MANNING DE LA GLACE'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 5, 'COMPOSANTE DU VENT EN X'
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,1001) 6, 'COMPOSANTE DU VENT EN Y'
      CALL LOG_ECRIS(LOG_BUF)
      IF (LOG_REQNIV() .GE. LOG_LVL_INFO) THEN
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,1000) 'PROPRIETES NODALES CALCULEES APRES RESI:'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)  7,
     &              'VITESSE EN X - a eliminer obtenue directement MGIA'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)  8,
     &              'VITESSE EN Y - a eliminer obtenue directement MGIA'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)  9, 'PROFONDEUR > 0'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 10, 'POROSITE POUR DECOUVREMENT'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 11, 'FROTTEMENT'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 12, 'CONVECTION'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 13, 'DIFFUSION'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 14, 'MANNING POUR LE DÉCOUVREMENT'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 15, 'VITESSE VERTICALE EN SURFACE'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001) 16, 'VITESSE VERTICALE AU FOND'
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---------------------------------------------------------------
1000  FORMAT(15X,A)
1001  FORMAT(15X,I2,1X,A)
C---------------------------------------------------------------

      RETURN
      END

