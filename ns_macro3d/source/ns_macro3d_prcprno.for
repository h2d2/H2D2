C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : NS_MACRO3D_PRCPRNO
C
C Description:
C     Pré-traitement des propriétés nodales
C
C     CALCUL DES PROPRIETES NODALES INDÉPENDANTES DES DLL
C     Voir ns_macro3d_iprno.fi pour définition des prno
C
C Entrée:
C
C Sortie: La bathymétrie est forcée linéaire
C
C Notes:
C************************************************************************
      SUBROUTINE NS_MACRO3D_PRCPRNO (VCORG,
     &                            KNGV,
     &                            VDJV,
     &                            VPRGL,
     &                            VPRNO,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ns_macro3d_iprno.fi'

      INTEGER IZF            ! Indice de la la bathymétrie dans vprno
      INTEGER IML            ! Indice de la contribution de la matrice masse lumpée dans vprno
      INTEGER IE
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
C-----------------------------------------------------------------------

      IZF = NS_IPRNO_ZF
      IML = NS_IPRNO_MASSELUMPE

C--      Initialise la matrice lumpée
      CALL DINIT(EG_CMMN_NNL, ZERO, VPRNO(IML,1), LM_CMMN_NPRNO)

C---  BOUCLE SUR LES ELEMENTS
C     =======================

      DO IE=1,EG_CMMN_NELV

         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

C---        FORCE LA TOPO LINÉAIRE
         VPRNO(IZF,NO2) = (VPRNO(IZF,NO1)+VPRNO(IZF,NO3))*UN_2
         VPRNO(IZF,NO4) = (VPRNO(IZF,NO3)+VPRNO(IZF,NO5))*UN_2
         VPRNO(IZF,NO6) = (VPRNO(IZF,NO5)+VPRNO(IZF,NO1))*UN_2

C---        Pour la matrice MLumpee, ajoute le detJ2D de l'élément à la prno
         VPRNO(IML,NO1) = VPRNO(IML,NO1) + VDJV(5,IE)*UN_24
         VPRNO(IML,NO2) = VPRNO(IML,NO2) + VDJV(5,IE)*UN_24 *TROIS
         VPRNO(IML,NO3) = VPRNO(IML,NO3) + VDJV(5,IE)*UN_24
         VPRNO(IML,NO4) = VPRNO(IML,NO4) + VDJV(5,IE)*UN_24 *TROIS
         VPRNO(IML,NO5) = VPRNO(IML,NO5) + VDJV(5,IE)*UN_24
         VPRNO(IML,NO6) = VPRNO(IML,NO6) + VDJV(5,IE)*UN_24 *TROIS

      ENDDO ! boucle sur les éléments

      IERR = ERR_TYP()
      RETURN
      END

