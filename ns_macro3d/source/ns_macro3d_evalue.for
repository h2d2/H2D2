C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************


C************************************************************************
C Sommaire: NS_MACRO3D_EVALUE
C
C Description:
C     La fonction NS_MACRO3D_EVALUE évalue au point de  passé en
C     paramètre, les Nuv, le Nh, leurs dérivées, ainsi que les
C     u,v,h, w et leur dérivées
C
C Entrée:
C     NFNAPPROXUV Nombre de fonction d'approximation pour u et v = 6
C     NFNAPPROXH  Nombre de fonction d'approximation pour h = 3
C     NDIMELV     Nombre de dimension du vrai élément = 3
C     VCORE       Table des coordonnées en x et y de l'Élément
C     VZCORE      Table des coordonnées en z (zf, zs) de l'élément
C     VDJE        Table des métriques 2d de l'élément
C     VCOPT       Coordonnées du point de gauss
C     VDLE        Table des degrés de liberté de l'élément
C
C Sortie:
C     VNUV        Table des évaluation des fonctions d'approximations pour u et v au pt de gauss
C     VNH         Table des évaluation des fonctions d'approximation pour h au pt de gauss
C     VNUV_XI     Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C                 des fonctions d'approximation pour u et v
C     VNH_XI      Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C                 des fonctions d'approximation pour h
C     DJP12L      Table des évaluation du déterminant du jacobien de l'élément 3d complet
C     DJP6        Table des évaluation du déterminant du jacobien du sous élément
C     VDLEPT      Table des évaluation des degrés de libertés au point
C     VDL_XIPT    Table des évalutaions des dérivées par rapport à x, y et z des degrés de liberté
C     WPT         Évaluation de la vitesse verticale au point : à déterminer
C     VW_XIPT     Évalutation des dérivées de la vitesse verticale au point : à déterminer
C     IP6         Entier de 1 à 4 indiquant le sous-élément du pt de gauss
C
C Notes:
C     - Exportée seulement pour le module test
C       Vitesse verticale à déterminer (WPT) ainsi que ses dérivées (VW_XIPT)
C************************************************************************
      SUBROUTINE NS_MACRO3D_EVALUE(    NFNAPPROXUV,
     &                                 NFNAPPROXH,
     &                                 NDIMELV,
     &                                 VCORE,
     &                                 VZCORE,
     &                                 VDJE,
     &                                 VCOPT,
     &                                 VDLE,
     &                                 VPRN,
     &                                 VNUV,
     &                                 VNH,
     &                                 VNUV_XI,
     &                                 VNH_XI,
     &                                 VDLEPT,
     &                                 VDL_XIPT,
     &                                 WPT,  !à analyser...
     &                                 VW_XIPT, !à analyser
     &                                 DJP12L,
     &                                 DJP6,
     &                                 IP6)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NS_MACRO3D_EVALUE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !UN_2
      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV

C--   Entrées : Dimensions
      INTEGER  NFNAPPROXUV
      INTEGER  NFNAPPROXH
      INTEGER  NDIMELV

C--   Entrées
      REAL*8   VCORE(EG_CMMN_NDIM, EG_CMMN_NNELV)
      REAL*8   VZCORE(2, EG_CMMN_NNELV)       ! donne les zs et zf pour chacun des 6 noeuds.
      REAL*8   VDJE (EG_CMMN_NDJV)
      REAL*8   VCOPT(NDIMELV)
      REAL*8   VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)

C--   Sorties
      REAL*8   VNUV(NFNAPPROXUV)             !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)               !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV, NFNAPPROXUV) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV, NFNAPPROXH)   !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VDL_XIPT(3,NDIMELV)
      REAL*8   VW_XIPT(NDIMELV)
      REAL*8   VDLEPT(3)                     !sortie : évaluation des degrés de liberté au point d'intégration
      REAL*8   WPT                           !sortie : évaluation de la vitesse verticale au point d'intégration
      REAL*8   DJP12L                      !évaluation du déterminant au pt de gauss
      REAL*8   DJP6                        !évaluation du déterminant au pt de gauss
      INTEGER  IP6                           !Entier de 1 à 4 indiquant dans quel sous-élémnet se trouve le pt de gauss

!      INCLUDE 'err.fi'
      INCLUDE 'lmgo_p12l.fi'
      INCLUDE 'ns_macro3d_idl.fi'

      INTEGER  NDIMELE
      PARAMETER (NDIMELE = 3)

      INTEGER  IERR ! pas retourné
      REAL*8   VJP12L(NDIMELE,NDIMELE)    !Table contenant l'évaluation des métriques dxi_i/deta_j au point de gauss
      REAL*8   VJP6(NDIMELE,NDIMELE)

C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. 6)
D     CALL ERR_PRE(NFNAPPROXH  .EQ. 3)
D     CALL ERR_PRE(NDIMELV     .EQ. 3)
D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELE)
D     CALL ERR_PRE(VCOPT(1) .GE. ZERO .AND. VCOPT(1) .LE. UN )
D     CALL ERR_PRE(VCOPT(2) .GE. ZERO .AND. VCOPT(2) .LE. UN )
D     CALL ERR_PRE(VCOPT(1) + VCOPT(2) .LE. UN )
D     CALL ERR_PRE(VCOPT(3) .GE. (-UN) .AND. VCOPT(3) .LE. UN )
C-----------------------------------------------------------------------

C---  MÉTRIQUES
      IERR = LMGO_P12L_EVAJELV ( EG_CMMN_NDIM,
     &                           NDIMELV,
     &                           EG_CMMN_NNELV,
     &                           EG_CMMN_NDJV,
     &                           VCORE, !pas utilisé
     &                           VZCORE,
     &                           VDJE,
     &                           VCOPT,
     &                           VJP12L,  ! sortie
     &                           DJP12L,  ! sortie
     &                           VJP6,    ! sortie
     &                           DJP6,    ! sortie
     &                           IP6)     ! sortie

C---     Calcul des fonctions d'approximation et de leur
C        dérivées au point d'évaluation
         CALL  NS_MACRO3D_EVALUEN(  NFNAPPROXUV,   !entrée : dim
     &                              NFNAPPROXH,    !entrée : dim
     &                              NDIMELV,       !entrée : dim
     &                              VJP12L,     !entrée
     &                              VJP6,       !entrée
     &                              VCOPT,        !entrée
     &                              IP6,           !entrée
     &                              VNUV,          !sortie
     &                              VNH,           !sortie
     &                              VNUV_XI,       !sortie
     &                              VNH_XI)        !sortie

C---     Calcul des dl et w, des dérivées des dl et de w au point
C        d'évaluation
         CALL  NS_MACRO3D_EVALUEDL( NFNAPPROXUV,   !entrée : dim
     &                              NFNAPPROXH,    !entrée : dim
     &                              NDIMELV,       !entrée : dim
     &                              VDLE,          !entrée
     &                              VPRN,          !ENTRÉE
     &                              VNUV,          !entrée
     &                              VNH,           !entrée
     &                              VNUV_XI,       !entrée
     &                              VNH_XI,        !entrée
     &                              IP6,           !entrée
     &                              VDLEPT,        !sortie
     &                              VDL_XIPT,      !sortie
     &                              WPT,           !sortie : à analyser...
     &                              VW_XIPT     )  !sortie : à analyser

      RETURN
      END

C************************************************************************
C Sommaire: NS_MACRO3D_EVALUEN
C
C Description:
C     La fonction NS_MACRO3D_EVALUEN évalue au point de Gauss passé en
C     paramètre, les Nuv, le Nh, leurs dérivées.
C
C Entrée:
C     NFNAPPROXUV   Nombre de fonction d'approximation pour u et v = 6
C     NFNAPPROXH    Nombre de fonction d'approximation pour h = 3
C     NDIMELV       Nombre de dimension du vrai élément = 3
C     VJP12L        Matrice jacobienne du p12l au point d'évaluation
C     VJP6          Matrice jacobienne du p6 au point d'évaluation
C     VCOPT         Coordonnées du point de gauss
C     IP6           Entier de 1 à 4 indiquant le sous-élément du pt de gauss
C
C Sortie:
C     VNUV     Table des évaluation des fonctions d'approximations pour u et v au pt de gauss
C     VNH      Table des évaluation des fonctions d'approximation pour h au pt de gauss
C     VNUV_XI  Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C              des fonctions d'approximation pour u et v
C     VNH_XI   Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C              des fonctions d'approximation pour h
C
C NOTE:
C     VNH_XI   Indépendante du point de gauss, pourrait être sortie de évalue
C
C************************************************************************
      SUBROUTINE NS_MACRO3D_EVALUEN(   NFNAPPROXUV,
     &                                 NFNAPPROXH,
     &                                 NDIMELV,
     &                                 VJP12L,
     &                                 VJP6,
     &                                 VCOPT,
     &                                 IP6,
     &                                 VNUV,
     &                                 VNH,
     &                                 VNUV_XI,
     &                                 VNH_XI      )

      IMPLICIT NONE

      INCLUDE 'eacnst.fi' !UN_2
      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NNELV

C--   Entrées : Dimensions
      INTEGER  NFNAPPROXUV
      INTEGER  NFNAPPROXH
      INTEGER  NDIMELV

C--   Entrées
      REAL*8   VJP12L(NDIMELV,NDIMELV)    !Table contenant l'évaluation des métriques dxi_i/deta_j au point de gauss
      REAL*8   VJP6(NDIMELV,NDIMELV)
      REAL*8   VCOPT(NDIMELV)
      INTEGER  IP6

C--   Sorties
      REAL*8   VNUV(NFNAPPROXUV)             !évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)               !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV, NFNAPPROXUV) !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV, NFNAPPROXH)   !évaluation des N,x_i (les N pour les h) aux pt de Gauss

!      INCLUDE 'err.fi'
!      INCLUDE 'lmgo_p12l.fi'

      INTEGER  IERR ! pas retourné
      INTEGER  NDIMELV_LOC
      INTEGER  NFNAPPROXUV_LOC
      INTEGER  NFNAPPROXH_LOC

      PARAMETER(NDIMELV_LOC = 3)
      PARAMETER(NFNAPPROXUV_LOC = 6)
      PARAMETER(NFNAPPROXH_LOC = 3)

      REAL*8   XI, ETA, ZETA                 !Point d'évaluation
      REAL*8   XIP6, ETAP6, ZETAP6           !Point d'évaluation dans le sous élément
      REAL*8   LAMBDA
      REAL*8   A
      REAL*8   B
      REAL*8   VNUV_KSII(NDIMELV_LOC, NFNAPPROXUV_LOC) !dérivées des fonctions d'approximation de uv par rapport a xi, eta et zeta
      REAL*8   VNH_KSII(NDIMELV_LOC, NFNAPPROXH_LOC)   !dérivées des fonctions d'approximation de h par rapport a xi, eta et zeta

!      REAL*8 DGEMM    !fonction MKL

C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. 6)
D     CALL ERR_PRE(NFNAPPROXH  .EQ. 3)
D     CALL ERR_PRE(NDIMELV     .EQ. 3)
D     CALL ERR_PRE(NFNAPPROXUV .EQ. NFNAPPROXUV_LOC)
D     CALL ERR_PRE(NFNAPPROXH  .EQ. NFNAPPROXH_LOC)
D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELV_LOC)
D     CALL ERR_PRE(VCOPT(1) .GE. ZERO .AND. VCOPT(1) .LE. UN )
D     CALL ERR_PRE(VCOPT(2) .GE. ZERO .AND. VCOPT(2) .LE. UN )
D     CALL ERR_PRE(VCOPT(1) + VCOPT(2) .LE. UN )
D     CALL ERR_PRE(VCOPT(3) .GE. (- UN) .AND. VCOPT(3) .LE. UN )
C-----------------------------------------------------------------------

C---  EXTRACTION DES INFORMATIONS
C--   POINTS D'ÉVALUATION
      XI = VCOPT(1)
      ETA = VCOPT(2)
      ZETA = VCOPT(3)

C---   TROUVE LES XI ETA ZETA "LOCAUX"
      ! pas de vérification des variables puisque cela a été fait en précond
      IF     (IP6 .EQ. 1) THEN
         XIP6 = XI + XI
         ETAP6 = ETA + ETA
         ZETAP6 = ZETA
      ELSEIF (IP6 .EQ. 2) THEN
         XIP6 = XI + XI - UN
         ETAP6 = ETA + ETA
         ZETAP6 = ZETA
      ELSEIF (IP6 .EQ. 3) THEN
         XIP6 = XI + XI
         ETAP6 = ETA + ETA - UN
         ZETAP6 = ZETA
      ELSE
         XIP6 = UN- XI - XI
         ETAP6 = UN - ETA - ETA
         ZETAP6 = ZETA
      ENDIF

C---  VNH
      VNH(1) = UN - XI - ETA
      VNH(2) = XI
      VNH(3) = ETA

C---  VNUV = APPROX 2D          * APPROX EN ZETA
C               1-xi-eta, xi, eta         A,B
      !--variables utilisées
      LAMBDA = UN - XIP6 - ETAP6
      A = (UN-ZETAP6)*UN_2
      B = (UN+ZETAP6)*UN_2

      VNUV(1) = LAMBDA*A
      VNUV(2) = XIP6*A
      VNUV(3) = ETAP6*A
      VNUV(4) = LAMBDA*B
      VNUV(5) = XIP6*B
      VNUV(6) = ETAP6*B

C---  VNUV_XI(NDIMELV, NFNAPPROXUV) !évaluation des N,x_i (les N pour les uv) aux pt
!     VNUV_XI(NFNAPPROXUV, NDIMELV) = VJP6(NDIMELV,NDIMELV)&*VNUV_KXII(NDIMELV, NFNAPPROXUV)

C--   VNUV_KSII
      VNUV_KSII(1,1) = -A
      VNUV_KSII(2,1) = -A
      VNUV_KSII(3,1) = -UN_2 * LAMBDA
      VNUV_KSII(1,2) =  A
      VNUV_KSII(2,2) = ZERO
      VNUV_KSII(3,2) = -UN_2 * XIP6
      VNUV_KSII(1,3) = ZERO
      VNUV_KSII(2,3) =  A
      VNUV_KSII(3,3) = -UN_2*ETAP6
      VNUV_KSII(1,4) = -B
      VNUV_KSII(2,4) = -B
      VNUV_KSII(3,4) =  UN_2 * LAMBDA
      VNUV_KSII(1,5) =  B
      VNUV_KSII(2,5) =  ZERO
      VNUV_KSII(3,5) =  UN_2 * XIP6
      VNUV_KSII(1,6) =  ZERO
      VNUV_KSII(2,6) =  B
      VNUV_KSII(3,6) =  UN_2 * ETAP6

C--   PRODUIT
         !c := alpha*op(a)*op(b) + beta*c    où
         ! a(mXk), b(kXn), c(mXn)
      call DGEMM( 'N',           !op sur a 'N' indique aucune op
     &            'N',           !op sur b
     &             NDIMELV,      !m : a(mXk)
     &             NFNAPPROXUV,  !n : b(kXn)
     &             NDIMELV,      !k : a(mXk), b(kXn)
     &             UN,           !alpha
     &             VJP6,      !a
     &             NDIMELV,      !first dimension of a
     &             VNUV_KSII,    !b
     &             NDIMELV,      !firt dimension of b (k)
     &             ZERO,         !beta
     &             VNUV_XI,      !c
     &             NDIMELV)      !first dimension of c)

C---  VNH_XI(NDIMELV, NFNAPPROXH) !évaluation des N,x_i (les N pour H) aux pt
!      VNH_XI(NFNAPPROXUV, NDIMELV) = VJP12L(NDIMELV,NDIMELV)&*VNH_KSII(NDIMELV, NFNAPPROXH)

C--   VNH_KSII
      VNH_KSII(1, 1) = -UN    !DNH1/DXI
      VNH_KSII(2, 1) = -UN
      VNH_KSII(3, 1) =  ZERO
      VNH_KSII(1, 2) =  UN    !DNH2/DXI
      VNH_KSII(2, 2) =  ZERO
      VNH_KSII(3, 2) =  ZERO
      VNH_KSII(1, 3) =  ZERO
      VNH_KSII(2, 3) =  UN
      VNH_KSII(3, 3) =  ZERO

C--   PRODUIT
         !c := alpha*op(a)*op(b) + beta*c    où
         ! a(mXk), b(kXn), c(mXn)
      call DGEMM( 'N', !op sur a 'N' indique aucune op
     &            'N',  !op sur b
     &             NDIMELV, !m : a(mXk)
     &             NFNAPPROXH, !n : b(kXn)
     &             NDIMELV, !k : a(mXk), b(kXn)
     &             UN, !alpha
     &             VJP12L, !a
     &             NDIMELV, !first dimension of a
     &             VNH_KSII, !b
     &             NDIMELV, !firt dimension of b (k)
     &             ZERO, !beta
     &             VNH_XI, !c
     &             NFNAPPROXH) !first dimension of c)


      RETURN
      END



C************************************************************************
C Sommaire: NS_MACRO3D_EVALUEDL
C
C Description:
C     La fonction NS_MACRO3D_EVALUEDL évalue au point de passé en
C     paramètre, les dl u,v,h et leur dérivées ainsi que w et ses dérivées
C
C Entrées:
C         NFNAPPROXUV   Nombre de fonction d'approximation pour u et v = 6
C         NFNAPPROXH Nombre de fonction d'approximation pour h = 3
C         NDIMELV    Nombre de dimension du vrai élément = 3
C         VDLE       Table des degrés de liberté de l'élément
C         VNUV       Table des évaluation des fonctions d'approximations pour u et v au pt de gauss
C         VNH        Table des évaluation des fonctions d'approximation pour h au pt de gauss
C         VNUV_XI    Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C                       des fonctions d'approximation pour u et v
C         VNH_XI     Table des évaluation au point de gauss de la dérivée par rapport a x, y et z
C                       des fonctions d'approximation pour
C         IP6        Indice du sous-élément dans lequel se trouve le pt d'évaluation
C
C Sorties:
C         VDLEPT     Table des évaluation des degrés de libertés au point
C         VDL_XIPT   Table des évalutaions des dérivées par rapport à x, y et z des degrés de liberté
C         WPT        Évaluation de la vitesse verticale au point : à déterminer
C         VW_XIPT    Évalutation des dérivées de la vitesse verticale au point : à déterminer

C Notes:
C        Vitesse verticale à déterminer (WPT) ainsi que ses dérivées (VW_XIPT)
C************************************************************************
      SUBROUTINE NS_MACRO3D_EVALUEDL(  NFNAPPROXUV,
     &                                 NFNAPPROXH,
     &                                 NDIMELV,
     &                                 VDLE,
     &                                 VPRN,
     &                                 VNUV,
     &                                 VNH,
     &                                 VNUV_XI,
     &                                 VNH_XI,
     &                                 IP6,
     &                                 VDLEPT,
     &                                 VDL_XIPT,
     &                                 WPT,  !à analyser...
     &                                 VW_XIPT) !à analyser


      IMPLICIT NONE

      INCLUDE 'eacmmn.fc' !LM_CMMN_NDLN
      INCLUDE 'egcmmn.fc' !EG_CMMN_NDLN

C--   Entrées
      ! Dimensions
      INTEGER  NFNAPPROXUV
      INTEGER  NFNAPPROXH
      INTEGER  NDIMELV

      !Tables
      REAL*8   VDLE(LM_CMMN_NDLN, EG_CMMN_NNELV) !entrée : Table des degrés de liberté de l'élément (de l'itération précédente)
      REAL*8   VNUV(NFNAPPROXUV)                 !Table d'évaluation des N de uv au pt gauss : ( N1(ptGauss), ...N6(ptGauss) )
      REAL*8   VNH(NFNAPPROXH)                   !évaluation des N de h au pt gauss
      REAL*8   VNUV_XI(NDIMELV, NFNAPPROXUV)     !évaluation des N,x_i (les N pour les uv) aux pt de Gauss
      REAL*8   VNH_XI(NDIMELV, NFNAPPROXH)       !évaluation des N,x_i (les N pour les h) aux pt de Gauss
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)

      !autre
      INTEGER  IP6

C--   Sorties
      REAL*8   VDL_XIPT(3,NDIMELV)
      REAL*8   VW_XIPT(NDIMELV)
      REAL*8   VDLEPT(3)          !sortie : évaluation des degrés de liberté au point d'intégration
      REAL*8   WPT                           !sortie : évaluation de la vitesse verticale au point d'intégration

!      INCLUDE 'err.fi'
      INCLUDE 'ns_macro3d_idl.fi'
      INCLUDE 'ns_macro3d_iprno.fi'
      INCLUDE 'eacnst.fi'

C--   Variables locales
      INTEGER  IERR ! pas retourné

      INTEGER  NDIMELV_LOC
      INTEGER  NFNAPPROXUV_LOC
      INTEGER  NFNAPPROXH_LOC
 !     INTEGER  NNELV_LOC

      PARAMETER(NDIMELV_LOC = 3)
      PARAMETER(NFNAPPROXUV_LOC = 6)
      PARAMETER(NFNAPPROXH_LOC = 3)
    ! PARAMETER(NNELV_LOC = 6)


      REAL*8 VTRANSNUV_XI(NFNAPPROXUV_LOC, NDIMELV_LOC) !matrice transposée de VNUV_XI
      REAL*8 VTRANSNH_XI(NFNAPPROXH_LOC,NDIMELV_LOC)    !matrice transposée de VNH_XI

      INTEGER NIUV   ! indice du i-eme noeud d'un sous-élément dans l'élément global
      INTEGER NIH    !indice du ième noeud de l'élément global (1 = 1, 2 = 3, 3 = 5)
      REAL*8  VU(6)  ! table des vitesses en u aux noeuds d'un sous élément
      REAL*8  VV(6)  ! table des vitessses en v aux noeuds d'un sous élément
      REAL*8  VH(3)  ! table des hauteurs d'eau aux noeuds de l'élément
      REAL*8  VW(6)  ! table des VITESSES EN W AUX NOEUDS D'UN SOUS ÉLÉMENT
      INTEGER I,J,K  !itérateurs
      REAL*8   COEFF !

      INTEGER KNET3  (3, 4) !nombre de noeud par sous-élément X Nombre de sous-élément par élémnet

      REAL*8 DDOT    !fonction MK

      DATA KNET3  / 1,2,6,  2,3,4,  6,4,5,  4,6,2/ !à passer plus dynamiquement
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NFNAPPROXUV .EQ. 6)
D     CALL ERR_PRE(NFNAPPROXH  .EQ. 3)
D     CALL ERR_PRE(NDIMELV     .EQ. 3)
D     CALL ERR_PRE(NFNAPPROXUV .EQ. NFNAPPROXUV_LOC)
D     CALL ERR_PRE(NFNAPPROXH  .EQ. NFNAPPROXH_LOC)
D     CALL ERR_PRE(NDIMELV     .EQ. NDIMELV_LOC)
C-----------------------------------------------------------------------

C---   Remplissage des tables VU, VV et VH donnant les valeurs aux sommets
      !doivent être ordonnées comme le sont les N soit
      ! fondN1, fondN2, fondN3, surfaceN1,surfaceN2, surfaceN3 pour UV
      ! n1, n2, n3 pour H
      DO I = 1, 3
         NIUV     =  KNET3(I,IP6)
         NIH      =  I + I - 1
         VU(I)    =  VDLE(NS_IDL_UF, NIUV)
         VU(I+3)  =  VDLE(NS_IDL_US, NIUV)
         VV(I)    =  VDLE(NS_IDL_VF,NIUV)
         VV(I+3)  =  VDLE(NS_IDL_VS,NIUV)
         VH(I)    =  VDLE(NS_IDL_H, NIH)
      END DO

C---   Remplissage des tables VQF ET VQS donnant les valeurs
      !doivent être ordonnées comme le sont les N soit
      ! fondN1, fondN2, fondN3, surfaceN1,surfaceN2, surfaceN3 pour UV
      ! n1, n2, n3 pour H
      DO I = 1, 3
         NIUV        =  KNET3(I,IP6)
         !assertion VPRN(NS_IPRNO_MASSELUMPE, NIUV) != 0
         COEFF       =  UN / VPRN(NS_IPRNO_MASSELUMPE, NIUV) !POSSIBILITÉ QU'IL Y AIT AUSSI DES UN_8
         VW(I)       =  COEFF * VPRN(NS_IPRNO_QF, NIUV)
         VW(I + 3)   =  COEFF * VPRN(NS_IPRNO_QS, NIUV)
      END DO



C---  CALCUL DE VDLEPT
C--      Produits pour trouver les valeurs au point VDLPT
C-          U
           !u(pt) = <N_u>{u_i}; <N_u> := VNUV {u_i}:=VU
      VDLEPT(NS_IDL_UPT) = DDOT( 6,    !grandeur des vecteur du dot product
     &                           VNUV, !x
     &                           1,    !incrément de sur x
     &                           VU,   !y
     &                           1   ) !incrément sur y

C-    V
      VDLEPT(NS_IDL_VPT) = DDOT( 6,    !grandeur des vecteur du dot product
     &                           VNUV, !x
     &                           1,    !incrément de sur x
     &                           VV,   !y
     &                           1   ) !incrément sur y

C-     H
       VDLEPT(NS_IDL_HPT) = DDOT( 3,    !grandeur des vecteur du dot product
     &                           VNH,  !x
     &                           1,    !incrément de sur x
     &                           VH,   !y
     &                           1   ) !incrément sur y


C---  CALCUL DE VDL_XIPT
C--   INVERSION DE VNUV_XI(NDIMELV,NFNAPPROXUV ) DANS VTRANSNUV_XI
C--   ET DE VNH_XI(I,J) dans VTRANSNH_XI(J,I)
        !il faudrait trouver l'opérateur approprié dans mkl
      DO I = 1, NDIMELV
         DO J = 1,NFNAPPROXUV
            VTRANSNUV_XI(J,I)= VNUV_XI(I,J)
         ENDDO
         DO K = 1, NFNAPPROXH
            VTRANSNH_XI(K,I)= VNH_XI(I,K)
         END DO
      ENDDO

      DO I=1,NDIMELV
C--      U,x, U,y, U,z
         VDL_XIPT(NS_IDL_UPT ,I) = DDOT( NFNAPPROXUV,    !grandeur des vecteur du dot product
     &                                   VTRANSNUV_XI(1, I),  !x
     &                                   1,    !incrément de sur x
     &                                   VU,   !y
     &                                   1   ) !incrément sur y

C--      V,x  V,y V,z
         VDL_XIPT(NS_IDL_VPT ,I) = DDOT( NFNAPPROXUV,    !grandeur des vecteur du dot product
     &                                   VTRANSNUV_XI(1, I),  !x
     &                                   1,    !incrément de sur x
     &                                   VV,   !y
     &                                   1   ) !incrément sur y

C---    h,x h,y h,z
        VDL_XIPT(NS_IDL_HPT ,I) = DDOT(  NFNAPPROXH,
     &                                   VTRANSNH_XI(1, I),
     &                                   1,
     &                                   VH,
     &                                   1   )
      END DO ! de 1 à 3 itération sur x, y ,z pour la dérivée

C---   WPT
!      N1 WF1 + N2 WF2 + N3 WF3 + N4 WS1 + N5 WS2 + N6 WS3
        WPT = DDOT(  NFNAPPROXUV,    !grandeur des vecteur du dot product
     &               VNUV, !x
     &               1,    !incrément de sur x
     &               VW,
     &               1   ) !incrément sur y

C---   VW_XIPTw,x, w,y, w,z
      DO I=1,NDIMELV
        VW_XIPT(I) = DDOT(  NFNAPPROXUV,
     &                      VTRANSNUV_XI(1, I),
     &                      1,
     &                      VW,
     &                      1 )
      END DO ! de 1 à 3 itération sur x, y ,z pour la dérivée

      RETURN
      END
