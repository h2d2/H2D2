//************************************************************************
// H2D2 - External declaration of public symbols
// Module: numr_pmetis
// Entry point: extern "C" void fake_dll_numr_pmetis()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:04.348000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_PMTS
F_PROT(IC_PMTS_XEQCTR, ic_pmts_xeqctr);
F_PROT(IC_PMTS_XEQMTH, ic_pmts_xeqmth);
F_PROT(IC_PMTS_REQCLS, ic_pmts_reqcls);
F_PROT(IC_PMTS_REQHDL, ic_pmts_reqhdl);
 
// ---  class NM_PMTS
F_PROT(NM_PMTS_000, nm_pmts_000);
F_PROT(NM_PMTS_999, nm_pmts_999);
F_PROT(NM_PMTS_CTR, nm_pmts_ctr);
F_PROT(NM_PMTS_DTR, nm_pmts_dtr);
F_PROT(NM_PMTS_INI, nm_pmts_ini);
F_PROT(NM_PMTS_RST, nm_pmts_rst);
F_PROT(NM_PMTS_REQHBASE, nm_pmts_reqhbase);
F_PROT(NM_PMTS_HVALIDE, nm_pmts_hvalide);
F_PROT(NM_PMTS_PART, nm_pmts_part);
F_PROT(NM_PMTS_RENUM, nm_pmts_renum);
F_PROT(NM_PMTS_GENNUM, nm_pmts_gennum);
F_PROT(NM_PMTS_SAUVE, nm_pmts_sauve);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_numr_pmetis()
{
   static char libname[] = "numr_pmetis";
 
   // ---  class IC_PMTS
   F_RGST(IC_PMTS_XEQCTR, ic_pmts_xeqctr, libname);
   F_RGST(IC_PMTS_XEQMTH, ic_pmts_xeqmth, libname);
   F_RGST(IC_PMTS_REQCLS, ic_pmts_reqcls, libname);
   F_RGST(IC_PMTS_REQHDL, ic_pmts_reqhdl, libname);
 
   // ---  class NM_PMTS
   F_RGST(NM_PMTS_000, nm_pmts_000, libname);
   F_RGST(NM_PMTS_999, nm_pmts_999, libname);
   F_RGST(NM_PMTS_CTR, nm_pmts_ctr, libname);
   F_RGST(NM_PMTS_DTR, nm_pmts_dtr, libname);
   F_RGST(NM_PMTS_INI, nm_pmts_ini, libname);
   F_RGST(NM_PMTS_RST, nm_pmts_rst, libname);
   F_RGST(NM_PMTS_REQHBASE, nm_pmts_reqhbase, libname);
   F_RGST(NM_PMTS_HVALIDE, nm_pmts_hvalide, libname);
   F_RGST(NM_PMTS_PART, nm_pmts_part, libname);
   F_RGST(NM_PMTS_RENUM, nm_pmts_renum, libname);
   F_RGST(NM_PMTS_GENNUM, nm_pmts_gennum, libname);
   F_RGST(NM_PMTS_SAUVE, nm_pmts_sauve, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
