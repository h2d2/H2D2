//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#ifndef C_GBP_H_DEJA_INCLU
#define C_GBP_H_DEJA_INCLU

#ifndef MODULE_GBP
#  define MODULE_GBP 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_GBP_INIT    C_GBP_INIT
#  define C_GBP_RELEASE C_GBP_RELEASE
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_GBP_INIT    c_gbp_init_
#  define C_GBP_RELEASE c_gbp_release_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_GBP_INIT    c_gbp_init__
#  define C_GBP_RELEASE c_gbp_release__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_GBP)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_GBP_INIT    ();
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_GBP_RELEASE ();

#ifdef __cplusplus
}
#endif

#endif   // C_GBP_H_DEJA_INCLU

