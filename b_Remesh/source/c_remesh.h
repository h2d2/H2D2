//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#ifndef C_RMESH_H_DEJA_INCLU
#define C_RMESH_H_DEJA_INCLU

#ifndef MODULE_REMESH
#  define MODULE_REMESH 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_RMESH_CTR       C_RMESH_CTR
#  define C_RMESH_DTR       C_RMESH_DTR
#  define C_RMESH_ERRMSG    C_RMESH_ERRMSG
#  define C_RMESH_ASGGRI    C_RMESH_ASGGRI
#  define C_RMESH_ASGLMT    C_RMESH_ASGLMT
#  define C_RMESH_ASGERR    C_RMESH_ASGERR
#  define C_RMESH_AJTERR    C_RMESH_AJTERR
#  define C_RMESH_ADAPT     C_RMESH_ADAPT
#  define C_RMESH_REQNNT    C_RMESH_REQNNT
#  define C_RMESH_REQNELT   C_RMESH_REQNELT
#  define C_RMESH_REQGRO    C_RMESH_REQGRO
#  define C_RMESH_REQNCLLIM C_RMESH_REQNCLLIM
#  define C_RMESH_REQNCLNOD C_RMESH_REQNCLNOD
#  define C_RMESH_REQLMT    C_RMESH_REQLMT
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_RMESH_CTR       c_rmesh_ctr_
#  define C_RMESH_DTR       c_rmesh_dtr_
#  define C_RMESH_ERRMSG    c_rmesh_errmsg_
#  define C_RMESH_ASGGRI    c_rmesh_asggri_
#  define C_RMESH_ASGLMT    c_rmesh_asglmt_
#  define C_RMESH_ASGERR    c_rmesh_asgerr_
#  define C_RMESH_AJTERR    c_rmesh_ajterr_
#  define C_RMESH_ADAPT     c_rmesh_adapt_
#  define C_RMESH_REQNNT    c_rmesh_reqnnt_
#  define C_RMESH_REQNELT   c_rmesh_reqnelt_
#  define C_RMESH_REQGRO    c_rmesh_reqgro_
#  define C_RMESH_REQNCLLIM c_rmesh_reqncllim_
#  define C_RMESH_REQNCLNOD c_rmesh_reqnclnod_
#  define C_RMESH_REQLMT    c_rmesh_reqlmt_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_RMESH_CTR       c_rmesh_ctr__
#  define C_RMESH_DTR       c_rmesh_dtr__
#  define C_RMESH_ERRMSG    c_rmesh_errmsg__
#  define C_RMESH_ASGGRI    c_rmesh_asggri__
#  define C_RMESH_ASGLMT    c_rmesh_asglmt__
#  define C_RMESH_ASGERR    c_rmesh_asgerr__
#  define C_RMESH_AJTERR    c_rmesh_ajterr__
#  define C_RMESH_ADAPT     c_rmesh_adapt__
#  define C_RMESH_REQNNT    c_rmesh_reqnnt__
#  define C_RMESH_REQNELT   c_rmesh_reqnelt__
#  define C_RMESH_REQGRO    c_rmesh_reqgro__
#  define C_RMESH_REQNCLLIM c_rmesh_reqncllim__
#  define C_RMESH_REQNCLNOD c_rmesh_reqnclnod__
#  define C_RMESH_REQLMT    c_rmesh_reqlmt__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_REMESH)

typedef void (F2C_CONF_CNV_APPEL * TTInfoCb)(fint_t*, double*);

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_CTR      (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_DTR      (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ERRMSG   (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGGRI   (hndl_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGLMT   (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ASGERR   (hndl_t*, fint_t*, fint_t*, double*, double*, double*, double*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_AJTERR   (hndl_t*, fint_t*, fint_t*, double*, double*, double*, double*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_ADAPT    (hndl_t*, fint_t*, double*, TTInfoCb, fint_t*, double*, fint_t*, fint_t*, fint_t*, fint_t*, double*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNNT   (hndl_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNELT  (hndl_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQGRO   (hndl_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNCLLIM(hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQNCLNOD(hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_RMESH_REQLMT   (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_RMESH_H_DEJA_INCLU
