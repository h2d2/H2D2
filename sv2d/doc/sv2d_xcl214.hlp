bc_type_214
   Boundary condition of type **214** :   
   Harmonic Dirichlet condition on the water level. The condition is
   interpolated between boundary start and end node. 
   Conceptually the condition is described by: 
       
                 (             (                     ! Start node             
     (o,a,p),                (o,a,p), ...             ),             (        
               ! End node                (o,a,p),                (o,a,p), ... 
              )          )
   where o is the period, a the amplitude and p the phase shift. The water
   level H is then: 
       
             H = sum(a * cos(o*t + p))
   Period, amplitude and phase shift are interpolated between boundary start
   and end node. 
     * Kind: Dirichlet
     * Code: 214
     * Values: o, a, p, o, a, p, ... 
     * Units: [s-1] m [-]
     * Example: 214 0.0314 2.0 0.0 0.0314 1.9 0.1
 
