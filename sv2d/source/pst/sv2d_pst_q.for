C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_PST_Q_000
C     INTEGER SV2D_PST_Q_999
C     INTEGER SV2D_PST_Q_CTR
C     INTEGER SV2D_PST_Q_DTR
C     INTEGER SV2D_PST_Q_INI
C     INTEGER SV2D_PST_Q_RST
C     INTEGER SV2D_PST_Q_REQHBASE
C     LOGICAL SV2D_PST_Q_HVALIDE
C     INTEGER SV2D_PST_Q_ACC
C     INTEGER SV2D_PST_Q_FIN
C     INTEGER SV2D_PST_Q_XEQ
C     INTEGER SV2D_PST_Q_ASGHSIM
C     INTEGER SV2D_PST_Q_REQHVNO
C     CHARACTER*256 SV2D_PST_Q_REQNOMF
C   Private:
C     INTEGER SV2D_PST_Q_CLC
C     INTEGER SV2D_PST_Q_CLC2
C     INTEGER SV2D_PST_Q_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_Q_NOBJMAX,
     &                   SV2D_PST_Q_HBASE,
     &                   'SV2D - Post-treatment Discharge balance')

      SV2D_PST_Q_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_Q_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_Q_NOBJMAX,
     &                   SV2D_PST_Q_HBASE,
     &                   SV2D_PST_Q_DTR)

      SV2D_PST_Q_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_Q_NOBJMAX,
     &                   SV2D_PST_Q_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_Q_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_Q_HBASE

         SV2D_PST_Q_HPRNT(IOB) = 0
      ENDIF

      SV2D_PST_Q_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_Q_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_Q_NOBJMAX,
     &                   SV2D_PST_Q_HBASE)

      SV2D_PST_Q_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SV2D_PST_Q_CLC, SV2D_PST_Q_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_PST_Q_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,SV2D_PST_Q_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,HOBJ,SV2D_PST_Q_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SV2D_PST_Q_HBASE
         SV2D_PST_Q_HPRNT(IOB) = HPRNT
      ENDIF

      SV2D_PST_Q_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SV2D_PST_Q_HBASE
      HPRNT = SV2D_PST_Q_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SV2D_PST_Q_HPRNT(IOB) = 0

      SV2D_PST_Q_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_Q_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'sv2d_pst_q.fc'
C------------------------------------------------------------------------

      SV2D_PST_Q_REQHBASE = SV2D_PST_Q_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_Q_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_q.fc'
C------------------------------------------------------------------------

      SV2D_PST_Q_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SV2D_PST_Q_NOBJMAX,
     &                                       SV2D_PST_Q_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_Q_HPRNT(HOBJ - SV2D_PST_Q_HBASE)
      SV2D_PST_Q_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_Q_HPRNT(HOBJ - SV2D_PST_Q_HBASE)
      SV2D_PST_Q_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_Q_HPRNT(HOBJ - SV2D_PST_Q_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, SV2D_PST_Q_NPOST)

      SV2D_PST_Q_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION SV2D_PST_Q_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_Q_HPRNT(HOBJ - SV2D_PST_Q_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, SV2D_PST_Q_NPOST)

      SV2D_PST_Q_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_Q_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_Q_HPRNT(HOBJ-SV2D_PST_Q_HBASE)
      SV2D_PST_Q_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_Q_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_Q_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_Q_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_Q_HPRNT(HOBJ-SV2D_PST_Q_HBASE)
      SV2D_PST_Q_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_Q_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel est va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_Q_CLC(HOBJ,
     &                        HELM,
     &                        NPST,
     &                        NNL,
     &                        VPOST)

      USE SV2D_LGCY_M
      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_ELEM_M,  ONLY: LM_ELEM_T
      USE LM_HELE_M,  ONLY: LM_HELE_REQOMNG
      USE LM_GDTA_M,  ONLY: LM_GDTA_T
      USE LM_EDTA_M,  ONLY: LM_EDTA_T
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
      INTEGER L_TRV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      CLASS(LM_ELEM_T),  POINTER :: OELE
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      IF (SV2D_LGCY_HVALIDE(HELM)) THEN   ! cf note
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         XPRG => SV2D_LGCY_REQXPRG(HELM)
      ELSEIF (LM_HELE_HVALIDE(HELM)) THEN
         OELE => LM_HELE_REQOMNG(HELM)
         SELECT TYPE(OELE)
            CLASS IS (SV2D_XBS_T)
               GDTA => OELE%GDTA
               EDTA => OELE%EDTA
               IPRN => OELE%IPRN
               XPRG => OELE%XPRG
            CLASS DEFAULT
               CALL ERR_ASG(ERR_FTL, 'Unsupported type')
         END SELECT
      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Alloue la table temporaire
      L_TRV = 0
      IF (ERR_GOOD())
     &   IERR = SO_ALLC_ALLRE8(NNL, L_TRV)

C---     Fait le calcul
      IF (ERR_GOOD())
     &   IERR = SV2D_PST_Q_CLC2(HOBJ,
     &                          GDTA,
     &                          EDTA,
     &                          XPRG,
     &                          IPRN,
     &                          NPST,
     &                          NNL,
     &                          VPOST,
     &                          VA(SO_ALLC_REQVIND(VA, L_TRV)))

C---     Désalloue la table temporaire
      IF (SO_ALLC_HEXIST(L_TRV))
     &   IERR = SO_ALLC_ALLRE8(0, L_TRV)

      SV2D_PST_Q_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CR_PST
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_CLC2(HOBJ,
     &                         GDTA,
     &                         EDTA,
     &                         XPRG,
     &                         IPRN,
     &                         NPST,
     &                         NNL,
     &                         VPOST,
     &                         VTRV)

      USE SV2D_XBS_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      TYPE (LM_GDTA_T),   INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T),   INTENT(IN) :: EDTA
      TYPE (SV2D_XPRG_T), INTENT(IN) :: XPRG
      TYPE (SV2D_IPRN_T), INTENT(IN) :: IPRN
      INTEGER, INTENT(IN) :: NPST
      INTEGER, INTENT(IN) :: NNL
      REAL*8, INTENT(INOUT) :: VPOST (NPST, NNL)
      REAL*8, INTENT(INOUT) :: VTRV  (NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DET3
      REAL*8  QX1, QX2, QX3, QX4, QX5, QX6
      REAL*8  QY1, QY2, QY3, QY4, QY5, QY6
      REAL*8  ERRQ, DEBIQ, QMAX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPST .EQ. SV2D_PST_Q_NPOST)
C-----------------------------------------------------------------------

      QMAX = ZERO

C---     Initialisation de VPOST
      VPOST(:,:) = ZERO
      VTRV(:) = ZERO

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        CONNECTIVITÉS DU T6
         NO1 = GDTA%KNGV(1,IE)
         NO2 = GDTA%KNGV(2,IE)
         NO3 = GDTA%KNGV(3,IE)
         NO4 = GDTA%KNGV(4,IE)
         NO5 = GDTA%KNGV(5,IE)
         NO6 = GDTA%KNGV(6,IE)

C---        Métriques des T3
         VKX  = GDTA%VDJV(1,IE)
         VEX  = GDTA%VDJV(2,IE)
         VKY  = GDTA%VDJV(3,IE)
         VEY  = GDTA%VDJV(4,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)
         DET3 = GDTA%VDJV(5,IE)

C---        Débit unitaire
         QX1 = EDTA%VDLG(1,NO1)
         QY1 = EDTA%VDLG(2,NO1)
         QX2 = EDTA%VDLG(1,NO2)
         QY2 = EDTA%VDLG(2,NO2)
         QX3 = EDTA%VDLG(1,NO3)
         QY3 = EDTA%VDLG(2,NO3)
         QX4 = EDTA%VDLG(1,NO4)
         QY4 = EDTA%VDLG(2,NO4)
         QX5 = EDTA%VDLG(1,NO5)
         QY5 = EDTA%VDLG(2,NO5)
         QX6 = EDTA%VDLG(1,NO6)
         QY6 = EDTA%VDLG(2,NO6)

C---        Dérivées temporelles des niveaux de surface (avec prise en compte de la porosité)
!        DHNS1DT=ZERO
!        DHNS3DT=ZERO
!        DHNS5DT=ZERO
!        IF(STEMP.EQ.'EULER') THEN
!          DHNS1DT=EDTA%VDLG(3,NO1+NNT)*EDTA%VPRNO(SV2D_IPRNO_COEFF_DRCY,NO1)
!          DHNS3DT=EDTA%VDLG(3,NO3+NNT)*EDTA%VPRNO(SV2D_IPRNO_COEFF_DRCY,NO3)
!          DHNS5DT=EDTA%VDLG(3,NO5+NNT)*EDTA%VPRNO(SV2D_IPRNO_COEFF_DRCY,NO5)
!        ENDIF

C---        Bilan de masse sur l'élément (Q.n)
         ERRQ = ZERO !     4.D0*UN_3*DET3*(DHNS1DT+DHNS3DT+DHNS5DT)
         ERRQ = ERRQ-VEX*(QX1+QX2 + QX2+QX3) - VEY*(QY1+QY2 + QY2+QY3) ! 1-2-3
         ERRQ = ERRQ-VSX*(QX3+QX4 + QX4+QX5) - VSY*(QY3+QY4 + QY4+QY5) ! 3-4-5
         ERRQ = ERRQ-VKX*(QX5+QX6 + QX6+QX1) - VKY*(QY5+QY6 + QY6+QY1) ! 5-6-1
         QMAX = QMAX + UN_2*ERRQ

C---        Assemblage du bilan de masse global
         ERRQ = UN_2*ABS(ERRQ)*DET3
         VPOST(1,NO1) = VPOST(1,NO1) + ERRQ
         VPOST(1,NO2) = VPOST(1,NO2) + ERRQ
         VPOST(1,NO3) = VPOST(1,NO3) + ERRQ
         VPOST(1,NO4) = VPOST(1,NO4) + ERRQ
         VPOST(1,NO5) = VPOST(1,NO5) + ERRQ
         VPOST(1,NO6) = VPOST(1,NO6) + ERRQ

C---        Calcul du débit
         DEBIQ = ZERO    ! 4.D0*TIER*DETJT3*ABS(DHNS1DT+DHNS3DT+DHNS5DT)
         DEBIQ = DEBIQ + ABS(VEX*(QX1+QX2)+VEY*(QY1+QY2))  ! 1-2-3
     &                 + ABS(VEX*(QX2+QX3)+VEY*(QY2+QY3))
         DEBIQ = DEBIQ + ABS(VKX*(QX3+QX4)+VKY*(QY3+QY4))  ! 3-4-5
     &                 + ABS(VKX*(QX4+QX5)+VKY*(QY4+QY5))
         DEBIQ = DEBIQ + ABS(VSX*(QX5+QX6)+VSY*(QY5+QY6))  ! 5-6-1
     &                 + ABS(VSX*(QX6+QX1)+VSY*(QY6+QY1))
         DEBIQ = MAX(DEBIQ, PETIT)

C---        Assemblage par lissage du bilan de masse local
         ERRQ  = ERRQ/(DEBIQ*UN_4)
         IF ((ERRQ/DET3) .GT. UN) ERRQ = ZERO
         VPOST(2,NO1) = VPOST(2,NO1) + ERRQ
         VPOST(2,NO2) = VPOST(2,NO2) + ERRQ
         VPOST(2,NO3) = VPOST(2,NO3) + ERRQ
         VPOST(2,NO4) = VPOST(2,NO4) + ERRQ
         VPOST(2,NO5) = VPOST(2,NO5) + ERRQ
         VPOST(2,NO6) = VPOST(2,NO6) + ERRQ

C---        Accumulation des DETJT3
         VTRV(NO1) = VTRV(NO1) + DET3
         VTRV(NO2) = VTRV(NO2) + DET3
         VTRV(NO3) = VTRV(NO3) + DET3
         VTRV(NO4) = VTRV(NO4) + DET3
         VTRV(NO5) = VTRV(NO5) + DET3
         VTRV(NO6) = VTRV(NO6) + DET3

20    CONTINUE
10    CONTINUE

C---     Transfert des valeurs aux noeuds
      QMAX = MAX(ABS(QMAX), PETIT)
      DO IN=1, GDTA%NNL
         VPOST(1,IN) = 100.D0* VPOST(1,IN) / VTRV(IN)
         VPOST(2,IN) = 100.D0* VPOST(2,IN) /(VTRV(IN)*QMAX)
      ENDDO

C---     Calcul des bilans de masse aux noeuds milieux
!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)
         NO1 = GDTA%KNGV(1,IE)
         NO2 = GDTA%KNGV(2,IE)
         NO3 = GDTA%KNGV(3,IE)
         NO4 = GDTA%KNGV(4,IE)
         NO5 = GDTA%KNGV(5,IE)
         NO6 = GDTA%KNGV(6,IE)
         VPOST(1,NO2) = UN_2 * (VPOST(1,NO1) + VPOST(1,NO3))
         VPOST(2,NO2) = UN_2 * (VPOST(2,NO1) + VPOST(2,NO3))
         VPOST(1,NO4) = UN_2 * (VPOST(1,NO3) + VPOST(1,NO5))
         VPOST(2,NO4) = UN_2 * (VPOST(2,NO3) + VPOST(2,NO5))
         VPOST(1,NO6) = UN_2 * (VPOST(1,NO5) + VPOST(1,NO1))
         VPOST(2,NO6) = UN_2 * (VPOST(2,NO5) + VPOST(2,NO1))
      ENDDO
!$omp  end do
      ENDDO
!$omp end parallel

      SV2D_PST_Q_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_PST_Q_LOG
C
C Description:
C     La fonction privée SV2D_PST_Q_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_Q_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sv2d_pst_q.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_Q_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. SV2D_PST_Q_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_Q:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)') 'MSG_VALEUR#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR, 1, NPST, NNL, VPOST, 'global')
      IERR = PS_PSTD_LOGVAL(HNUMR, 2, NPST, NNL, VPOST, 'local')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SV2D_PST_Q_LOG = ERR_TYP()
      RETURN
      END
