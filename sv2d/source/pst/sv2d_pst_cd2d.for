C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_PST_CD2D_000
C     INTEGER SV2D_PST_CD2D_999
C     INTEGER SV2D_PST_CD2D_CTR
C     INTEGER SV2D_PST_CD2D_DTR
C     INTEGER SV2D_PST_CD2D_INI
C     INTEGER SV2D_PST_CD2D_RST
C     INTEGER SV2D_PST_CD2D_REQHBASE
C     LOGICAL SV2D_PST_CD2D_HVALIDE
C     INTEGER SV2D_PST_CD2D_ACC
C     INTEGER SV2D_PST_CD2D_FIN
C     INTEGER SV2D_PST_CD2D_XEQ
C     INTEGER SV2D_PST_CD2D_ASGHSIM
C     INTEGER SV2D_PST_CD2D_REQHVNO
C     CHARACTER*256 SV2D_PST_CD2D_REQNOMF
C   Private:
C     INTEGER SV2D_PST_CD2D_CLC
C     INTEGER SV2D_PST_CD2D_CLC2
C     INTEGER SV2D_PST_CD2D_LOG
C     INTEGER SV2D_PST_CD2D_CLC3
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_CD2D_NOBJMAX,
     &                   SV2D_PST_CD2D_HBASE,
     &                   'SV2D - Post-traitement CD2D')

      SV2D_PST_CD2D_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_CD2D_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_CD2D_NOBJMAX,
     &                   SV2D_PST_CD2D_HBASE,
     &                   SV2D_PST_CD2D_DTR)

      SV2D_PST_CD2D_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_CD2D_NOBJMAX,
     &                   SV2D_PST_CD2D_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_CD2D_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_CD2D_HBASE

         SV2D_PST_CD2D_HPRNT(IOB) = 0
      ENDIF

      SV2D_PST_CD2D_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_CD2D_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_CD2D_NOBJMAX,
     &                   SV2D_PST_CD2D_HBASE)

      SV2D_PST_CD2D_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SV2D_PST_CD2D_CLC, SV2D_PST_CD2D_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_PST_CD2D_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIMTH(HFCLC,HOBJ,SV2D_PST_CD2D_CLC)
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIMTH(HFLOG,HOBJ,SV2D_PST_CD2D_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SV2D_PST_CD2D_HBASE
         SV2D_PST_CD2D_HPRNT(IOB) = HPRNT
      ENDIF

      SV2D_PST_CD2D_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SV2D_PST_CD2D_HBASE
      HPRNT = SV2D_PST_CD2D_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SV2D_PST_CD2D_HPRNT(IOB) = 0

      SV2D_PST_CD2D_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_CD2D_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'
C------------------------------------------------------------------------

      SV2D_PST_CD2D_REQHBASE = SV2D_PST_CD2D_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_CD2D_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'
C------------------------------------------------------------------------

      SV2D_PST_CD2D_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SV2D_PST_CD2D_NOBJMAX,
     &                                       SV2D_PST_CD2D_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ - SV2D_PST_CD2D_HBASE)
      SV2D_PST_CD2D_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ - SV2D_PST_CD2D_HBASE)
      SV2D_PST_CD2D_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ - SV2D_PST_CD2D_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, SV2D_PST_CD2D_NPOST)

      SV2D_PST_CD2D_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION SV2D_PST_CD2D_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ - SV2D_PST_CD2D_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, SV2D_PST_CD2D_NPOST)

      SV2D_PST_CD2D_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CD2D_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ-SV2D_PST_CD2D_HBASE)
      SV2D_PST_CD2D_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CD2D_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CD2D_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CD2D_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CD2D_HPRNT(HOBJ-SV2D_PST_CD2D_HBASE)
      SV2D_PST_CD2D_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_CD2D_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel et va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_CD2D_CLC(HOBJ,
     &                           HELM,
     &                           NPST,
     &                           NNL,
     &                           VPOST)

      USE SV2D_LGCY_M
      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_ELEM_M,  ONLY: LM_ELEM_T
      USE LM_HELE_M,  ONLY: LM_HELE_REQOMNG
      USE LM_GDTA_M,  ONLY: LM_GDTA_T
      USE LM_EDTA_M,  ONLY: LM_EDTA_T
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      CLASS(LM_ELEM_T),  POINTER :: OELE
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      IF (SV2D_LGCY_HVALIDE(HELM)) THEN   ! cf note
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         XPRG => SV2D_LGCY_REQXPRG(HELM)
      ELSEIF (LM_HELE_HVALIDE(HELM)) THEN
         OELE => LM_HELE_REQOMNG(HELM)
         SELECT TYPE(OELE)
            CLASS IS (SV2D_XBS_T)
               GDTA => OELE%GDTA
               EDTA => OELE%EDTA
               IPRN => OELE%IPRN
               XPRG => OELE%XPRG
            CLASS DEFAULT
               CALL ERR_ASG(ERR_FTL, 'Unsupported type')
         END SELECT
      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Fait le calcul
      IERR = SV2D_PST_CD2D_CLC2(HOBJ,
     &                          GDTA,
     &                          EDTA,
     &                          XPRG,
     &                          IPRN,
     &                          NPST,
     &                          NNL,
     &                          VPOST)

      SV2D_PST_CD2D_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC2
C
C Description:
C     La fonction privée SV2D_PST_CD2D_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_CLC2(HOBJ,
     &                            GDTA,
     &                            EDTA,
     &                            XPRG,
     &                            IPRN,
     &                            NPST,
     &                            NNL,
     &                            VPOST)

      USE SV2D_XBS_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      TYPE (LM_GDTA_T),   INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T),   INTENT(IN) :: EDTA
      TYPE (SV2D_XPRG_T), INTENT(IN) :: XPRG
      TYPE (SV2D_IPRN_T), INTENT(IN) :: IPRN
      INTEGER, INTENT(IN) :: NPST
      INTEGER, INTENT(IN) :: NNL
      REAL*8, INTENT(INOUT) :: VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  DETJT3
      REAL*8  VIS1, VIS2, VIS3, VIS4
      REAL*8  VNM1, VNM2, VNM3, VNM4
      REAL*8  QX, QY, H
      REAL*8  ZF, UX, UY, P, Q, PA, VM, US, DV, DH
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. SV2D_PST_CD2D_NPOST)
D     CALL ERR_PRE(NNL  .EQ. GDTA%NNL)
C-----------------------------------------------------------------------

C---     Initialisation de VPOST
      VPOST(:,:) = ZERO

C---     BOUCLE SUR LES ELEMENTS
C        =======================
      DO 10 IC=1,GDTA%NELCOL
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Connectivités du T6
         NO1  = GDTA%KNGV(1,IE)
         NO2  = GDTA%KNGV(2,IE)
         NO3  = GDTA%KNGV(3,IE)
         NO4  = GDTA%KNGV(4,IE)
         NO5  = GDTA%KNGV(5,IE)
         NO6  = GDTA%KNGV(6,IE)

C---        Métriques du T3
         DETJT3 = GDTA%VDJV(5,IE)*UN_4

C---        Viscosité physique
         VIS1 = EDTA%VPREV(1,1,IE)
         VIS2 = EDTA%VPREV(1,2,IE)
         VIS3 = EDTA%VPREV(1,3,IE)
         VIS4 = EDTA%VPREV(1,4,IE)

C---        Assemblage aux noeuds pour le lissage
         VPOST(4,NO1) = VPOST(4,NO1) + VIS1*DETJT3
         VPOST(4,NO2) = VPOST(4,NO2) + (VIS1+VIS2+VIS4)*DETJT3*UN_3
         VPOST(4,NO3) = VPOST(4,NO3) + VIS2*DETJT3
         VPOST(4,NO4) = VPOST(4,NO4) + (VIS2+VIS3+VIS4)*DETJT3*UN_3
         VPOST(4,NO5) = VPOST(4,NO5) + VIS3*DETJT3
         VPOST(4,NO6) = VPOST(4,NO6) + (VIS1+VIS3+VIS4)*DETJT3*UN_3

C---        Accumulation des DETJT3
         VPOST(5,NO1) = VPOST(5,NO1) + DETJT3
         VPOST(5,NO2) = VPOST(5,NO2) + DETJT3
         VPOST(5,NO3) = VPOST(5,NO3) + DETJT3
         VPOST(5,NO4) = VPOST(5,NO4) + DETJT3
         VPOST(5,NO5) = VPOST(5,NO5) + DETJT3
         VPOST(5,NO6) = VPOST(5,NO6) + DETJT3

20    CONTINUE
10    CONTINUE

C---     Transfert des valeurs aux noeuds
      DO IN=1, GDTA%NNL
         QX = EDTA%VDLG ( 1,IN)
         QY = EDTA%VDLG ( 2,IN)
         H  = EDTA%VDLG ( 3,IN)

         ZF = EDTA%VPRNO(IPRN%Z, IN)
         UX = EDTA%VPRNO(IPRN%U, IN)
         UY = EDTA%VPRNO(IPRN%V, IN)
         PA = EDTA%VPRNO(IPRN%H, IN)          ! Prof absolue
         VM = EDTA%VPRNO(IPRN%COEFF_FROT, IN) ! g n2 |u| / H**(4/3)

         P  = H - ZF
         Q  = HYPOT(QX, QY)
         US = SQRT(Q*VM)
         DV = 0.15D0 * PA * US
         DH = VPOST(4,IN) / VPOST(5,IN)  ! Viscosité physique

C---        CHARGEMENT DE VPOST
         VPOST( 1,IN) = UX
         VPOST( 2,IN) = UY
         VPOST( 3,IN) = PA
         VPOST( 4,IN) = DV
         VPOST( 5,IN) = DH
      ENDDO

      SV2D_PST_CD2D_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_PST_CD2D_LOG
C
C Description:
C     La fonction privée SV2D_PST_CD2D_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CD2D_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_cd2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sv2d_pst_cd2d.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CD2D_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. SV2D_PST_CD2D_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_SIMULATION:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<25>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<25>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'qx')
      IERR = PS_PSTD_LOGVAL(HNUMR,  2, NPST, NNL, VPOST, 'qy')
      IERR = PS_PSTD_LOGVAL(HNUMR,  3, NPST, NNL, VPOST, 'H')
      IERR = PS_PSTD_LOGVAL(HNUMR,  4, NPST, NNL, VPOST, 'Dv')
      IERR = PS_PSTD_LOGVAL(HNUMR,  5, NPST, NNL, VPOST, 'Dh')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SV2D_PST_CD2D_LOG = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:  SV2D_PST_CD2D_CLC3
!!!C
!!!C Description:
!!!C     Calcul des propriétés élémentaires des éléments de volume pour
!!!C     un élément.
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SV2D_PST_CD2D_CLC3(HOBJ,
!!!     &                            GDTA%KNGV,
!!!     &                            GDTA%VDJV,
!!!     &                            EDTA%VPRNO,
!!!     &                            EDTA%VPREV,
!!!     &                            EDTA%VDLG,
!!!     &                            NPST,
!!!     &                            NNL,
!!!     &                            VPOST)
!!!
!!!      USE SV2D_XBS_M
!!!      IMPLICIT NONE
!!!
!!!
!!!      INTEGER SV2D_PST_CD2D_CLC3
!!!      INTEGER HOBJ
!!!      INTEGER HOBJ
!!!      TYPE (LM_GDTA_T), POINTER :: GDTA
!!!      TYPE (LM_EDTA_T), POINTER :: EDTA
!!!      INTEGER NPST
!!!      INTEGER NNL
!!!      REAL*8  VPOST (NPST, NNL)
!!!
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'sv2d_cbs.fc'
!!!
!!!      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ, COEF
!!!      REAL*8  U1, U2, U3, U4, U5, U6
!!!      REAL*8  V1, V2, V3, V4, V5, V6
!!!      REAL*8  US1, US2, US3, US4
!!!      REAL*8  VS1, VS2, VS3, VS4
!!!      REAL*8  CLM, DJ, DH
!!!      REAL*8  UX, UY, VX, VY, P
!!!      REAL*8  VLM, DV
!!!      INTEGER IC, IE, IN
!!!      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
!!!C-----------------------------------------------------------------------
!!!      REAL*8 u1x, u2x, u3x, u4x, u5x, u6x
!!!      REAL*8 u1y, u2y, u3y, u4y, u5y, u6y
!!!      u1x(u1, u2, u3, u4, u5, u6) =
!!!     &   +(u2*(3*vkx+vex))/30-(u1*(2*vkx+2*vex))/30
!!!     &   +(u6*(vkx+3*vex))/30-(u4*(vkx+vex))/30-(u3*vkx)/30-(u5*vex)/30
!!!      u2x(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u1*(3*vkx+3*vex))/30+(2*u4*(vkx+2*vex))/15
!!!     &   -(2*u6*(vkx-vex))/15+(u3*vkx)/10-(u5*vex)/30-(4*u2*vex)/15
!!!      u3x(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u2*(3*vkx+2*vex))/30+(u1*(vkx+vex))/30
!!!     &   -(u4*(vkx-2*vex))/30+(u6*vkx)/30+(u3*vkx)/15-(u5*vex)/30
!!!      u4x(u1, u2, u3, u4, u5, u6) =
!!!     &   -(2*u6*(2*vkx+vex))/15-(2*u2*(vkx+2*vex))/15
!!!     &   +(4*u4*(vkx+vex))/15+(u1*(vkx+vex))/30+(u3*vkx)/10+(u5*vex)/10
!!!      u5x(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u6*(2*vkx+3*vex))/30+(u4*(2*vkx-vex))/30
!!!     &   +(u1*(vkx+vex))/30-(u3*vkx)/30+(u5*vex)/15+(u2*vex)/30
!!!      u6x(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u1*(3*vkx+3*vex))/30+(2*u4*(2*vkx+vex))/15
!!!     &   +(2*u2*(vkx-vex))/15-(4*u6*vkx)/15-(u3*vkx)/30+(u5*vex)/10
!!!
!!!      u1y(u1, u2, u3, u4, u5, u6) =
!!!     &   +(u2*(3*vky+vey))/30-(u1*(2*vky+2*vey))/30
!!!     &   +(u6*(vky+3*vey))/30-(u4*(vky+vey))/30-(u3*vky)/30-(u5*vey)/30
!!!      u2y(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u1*(3*vky+3*vey))/30+(2*u4*(vky+2*vey))/15
!!!     &   -(2*u6*(vky-vey))/15+(u3*vky)/10-(u5*vey)/30-(4*u2*vey)/15
!!!      u3y(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u2*(3*vky+2*vey))/30+(u1*(vky+vey))/30
!!!     &   -(u4*(vky-2*vey))/30+(u6*vky)/30+(u3*vky)/15-(u5*vey)/30
!!!      u4y(u1, u2, u3, u4, u5, u6) =
!!!     &   -(2*u6*(2*vky+vey))/15-(2*u2*(vky+2*vey))/15
!!!     &   +(4*u4*(vky+vey))/15+(u1*(vky+vey))/30+(u3*vky)/10+(u5*vey)/10
!!!      u5y(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u6*(2*vky+3*vey))/30+(u4*(2*vky-vey))/30
!!!     &   +(u1*(vky+vey))/30-(u3*vky)/30+(u5*vey)/15+(u2*vey)/30
!!!      u6y(u1, u2, u3, u4, u5, u6) =
!!!     &   -(u1*(3*vky+3*vey))/30+(2*u4*(2*vky+vey))/15
!!!     &   +(2*u2*(vky-vey))/15-(4*u6*vky)/15-(u3*vky)/30+(u5*vey)/10
!!!C-----------------------------------------------------------------------
!!!
!!!
!!!C---     Boucle sur les elements
!!!      DO 10 IC=1,GDTA%NELCOL
!!!      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)
!!!
!!!C---        Connectivités du T6
!!!         NO1 = GDTA%KNGV(1,IE)
!!!         NO2 = GDTA%KNGV(2,IE)
!!!         NO3 = GDTA%KNGV(3,IE)
!!!         NO4 = GDTA%KNGV(4,IE)
!!!         NO5 = GDTA%KNGV(5,IE)
!!!         NO6 = GDTA%KNGV(6,IE)
!!!
!!!C---        Métriques des T3
!!!         VKX = GDTA%VDJV(1,IE)  !*UN_2
!!!         VEX = GDTA%VDJV(2,IE)  !*UN_2
!!!         VKY = GDTA%VDJV(3,IE)  !*UN_2
!!!         VEY = GDTA%VDJV(4,IE)  !*UN_2
!!!         VSX = -(VKX+VEX)
!!!         VSY = -(VKY+VEY)
!!!
!!!C---        Déterminant du T3
!!!         DETJ = GDTA%VDJV(5,IE)  !*UN_4
!!!         COEF = DETJ
!!!
!!!C---        Valeurs nodales
!!!         U1 = EDTA%VPRNO(IPRN%U,NO1)     ! NOEUD 1
!!!         V1 = EDTA%VPRNO(IPRN%V,NO1)
!!!         U2 = EDTA%VPRNO(IPRN%U,NO2)     ! NOEUD 2
!!!         V2 = EDTA%VPRNO(IPRN%V,NO2)
!!!         U3 = EDTA%VPRNO(IPRN%U,NO3)     ! NOEUD 3
!!!         V3 = EDTA%VPRNO(IPRN%V,NO3)
!!!         U4 = EDTA%VPRNO(IPRN%U,NO4)     ! NOEUD 4
!!!         V4 = EDTA%VPRNO(IPRN%V,NO4)
!!!         U5 = EDTA%VPRNO(IPRN%U,NO5)     ! NOEUD 5
!!!         V5 = EDTA%VPRNO(IPRN%V,NO5)
!!!         U6 = EDTA%VPRNO(IPRN%U,NO6)     ! NOEUD 6
!!!         V6 = EDTA%VPRNO(IPRN%V,NO6)
!!!
!!!C---        Somme des vitesses
!!!         US1 = U1+U2+U6
!!!         US2 = U2+U3+U4
!!!         US3 = U6+U4+U5
!!!         US4 = U4+U6+U2
!!!         VS1 = V1+V2+V6
!!!         VS2 = V2+V3+V4
!!!         VS3 = V6+V4+V5
!!!         VS4 = V4+V6+V2
!!!
!!!C---        Assemblage
!!!         VPOST(1,NO1) = VPOST(1,NO1) + u1x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO1) = VPOST(2,NO1) + u1y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO1) = VPOST(3,NO1) + u1x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO1) = VPOST(4,NO1) + u1y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO1) = VPOST(5,NO1) + COEF
!!!
!!!         VPOST(1,NO2) = VPOST(1,NO2) + u2x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO2) = VPOST(2,NO2) + u2y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO2) = VPOST(3,NO2) + u2x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO2) = VPOST(4,NO2) + u2y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO2) = VPOST(5,NO2) + COEF
!!!
!!!         VPOST(1,NO3) = VPOST(1,NO3) + u3x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO3) = VPOST(2,NO3) + u3y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO3) = VPOST(3,NO3) + u3x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO3) = VPOST(4,NO3) + u3y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO3) = VPOST(5,NO3) + COEF
!!!
!!!         VPOST(1,NO4) = VPOST(1,NO4) + u4x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO4) = VPOST(2,NO4) + u4y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO4) = VPOST(3,NO4) + u4x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO4) = VPOST(4,NO4) + u4y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO4) = VPOST(5,NO4) + COEF
!!!
!!!         VPOST(1,NO5) = VPOST(1,NO5) + u5x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO5) = VPOST(2,NO5) + u5y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO5) = VPOST(3,NO5) + u5x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO5) = VPOST(4,NO5) + u5y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO5) = VPOST(5,NO5) + COEF
!!!
!!!         VPOST(1,NO6) = VPOST(1,NO6) + u6x(u1, u2, u3, u4, u5, u6)
!!!         VPOST(2,NO6) = VPOST(2,NO6) + u6y(u1, u2, u3, u4, u5, u6)
!!!         VPOST(3,NO6) = VPOST(3,NO6) + u6x(v1, v2, v3, v4, v5, v6)
!!!         VPOST(4,NO6) = VPOST(4,NO6) + u6y(v1, v2, v3, v4, v5, v6)
!!!         VPOST(5,NO6) = VPOST(5,NO6) + COEF
!!!
!!!!!         VPOST(1,NO1) = VPOST(1,NO1) - VSX*US1
!!!!!         VPOST(2,NO1) = VPOST(2,NO1) - VSY*US1
!!!!!         VPOST(3,NO1) = VPOST(3,NO1) - VSX*VS1
!!!!!         VPOST(4,NO1) = VPOST(4,NO1) - VSY*VS1
!!!!!         VPOST(5,NO1) = VPOST(5,NO1) + COEF
!!!!!
!!!!!         VPOST(1,NO2) = VPOST(1,NO2) - VKX*US1 - VSX*US2 + VSX*US4
!!!!!         VPOST(2,NO2) = VPOST(2,NO2) - VKY*US1 - VSY*US2 + VSY*US4
!!!!!         VPOST(3,NO2) = VPOST(3,NO2) - VKX*VS1 - VSX*VS2 + VSX*VS4
!!!!!         VPOST(4,NO2) = VPOST(4,NO2) - VKY*VS1 - VSY*VS2 + VSY*VS4
!!!!!         VPOST(5,NO2) = VPOST(5,NO2) + COEF
!!!!!
!!!!!         VPOST(1,NO3) = VPOST(1,NO3) - VKX*US2
!!!!!         VPOST(2,NO3) = VPOST(2,NO3) - VKY*US2
!!!!!         VPOST(3,NO3) = VPOST(3,NO3) - VKX*VS2
!!!!!         VPOST(4,NO3) = VPOST(4,NO3) - VKY*VS2
!!!!!         VPOST(5,NO3) = VPOST(5,NO3) + COEF
!!!!!
!!!!!         VPOST(1,NO4) = VPOST(1,NO4) - VEX*US2 - VKX*US3 + VKX*US4
!!!!!         VPOST(2,NO4) = VPOST(2,NO4) - VEY*US2 - VKY*US3 + VKY*US4
!!!!!         VPOST(3,NO4) = VPOST(3,NO4) - VEX*VS2 - VKX*VS3 + VKX*VS4
!!!!!         VPOST(4,NO4) = VPOST(4,NO4) - VEY*VS2 - VKY*VS3 + VKY*VS4
!!!!!         VPOST(5,NO4) = VPOST(5,NO4) + COEF
!!!!!
!!!!!         VPOST(1,NO5) = VPOST(1,NO5) - VEX*US3
!!!!!         VPOST(2,NO5) = VPOST(2,NO5) - VEY*US3
!!!!!         VPOST(3,NO5) = VPOST(3,NO5) - VEX*VS3
!!!!!         VPOST(4,NO5) = VPOST(4,NO5) - VEY*VS3
!!!!!         VPOST(5,NO5) = VPOST(5,NO5) + COEF
!!!!!
!!!!!         VPOST(1,NO6) = VPOST(1,NO6) - VSX*US3 - VEX*US1 + VEX*US4
!!!!!         VPOST(2,NO6) = VPOST(2,NO6) - VSY*US3 - VEY*US1 + VEY*US4
!!!!!         VPOST(3,NO6) = VPOST(3,NO6) - VSX*VS3 - VEX*VS1 + VEX*VS4
!!!!!         VPOST(4,NO6) = VPOST(4,NO6) - VSY*VS3 - VEY*VS1 + VEX*VS4
!!!!!         VPOST(5,NO6) = VPOST(5,NO6) + COEF
!!!
!!!20    CONTINUE
!!!10    CONTINUE
!!!
!!!C---     TRANSFERT DES VALEURS AUX NOEUDS
!!!      DO IN=1, GDTA%NNL
!!!         UX = VPOST(1,IN)
!!!         UY = VPOST(2,IN)
!!!         VX = VPOST(3,IN)
!!!         VY = VPOST(4,IN)
!!!         DJ = VPOST(5,IN)
!!!
!!!         P  = EDTA%VPRNO(SV2D_IPRNO_H,IN)
!!!
!!!         DETJ = GDTA%VDJV(5,IE)*UN_4
!!!         CLM = SV2D_VISCO_LM + SV2D_VISCO_SMGO*SQRT(DETJ)
!!!
!!!C---        Longueur de melange
!!!         VLM = CLM*P/DJ
!!!         DH = VLM*VLM * SQRT(DEUX*UX*UX + DEUX*VY*VY + (UY+VX)*(UY+VX))
!!!         DV =0.0D0
!!!
!!!C---        CHARGEMENT DE VPOST
!!!         VPOST(1,IN) = UX
!!!         VPOST(2,IN) = UY
!!!         VPOST(3,IN) = P
!!!         VPOST(4,IN) = DV
!!!         VPOST(5,IN) = DH
!!!      ENDDO
!!!
!!!      SV2D_PST_CD2D_CLC3 = ERR_TYP()
!!!      RETURN
!!!      END
