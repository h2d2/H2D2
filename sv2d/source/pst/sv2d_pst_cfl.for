C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_PST_CFL_000
C     INTEGER SV2D_PST_CFL_999
C     INTEGER SV2D_PST_CFL_CTR
C     INTEGER SV2D_PST_CFL_DTR
C     INTEGER SV2D_PST_CFL_INI
C     INTEGER SV2D_PST_CFL_RST
C     INTEGER SV2D_PST_CFL_REQHBASE
C     LOGICAL SV2D_PST_CFL_HVALIDE
C     INTEGER SV2D_PST_CFL_ACC
C     INTEGER SV2D_PST_CFL_FIN
C     INTEGER SV2D_PST_CFL_XEQ
C     INTEGER SV2D_PST_CFL_ASGHSIM
C     INTEGER SV2D_PST_CFL_REQHVNO
C     CHARACTER*256 SV2D_PST_CFL_REQNOMF
C   Private:
C     INTEGER SV2D_PST_CFL_CLC
C     INTEGER SV2D_PST_CFL_CLC2
C     INTEGER SV2D_PST_CFL_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SV2D_PST_CFL_NOBJMAX,
     &                   SV2D_PST_CFL_HBASE,
     &                   'SV2D - Post-traitement CFL')

      SV2D_PST_CFL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER  IERR
      EXTERNAL SV2D_PST_CFL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SV2D_PST_CFL_NOBJMAX,
     &                   SV2D_PST_CFL_HBASE,
     &                   SV2D_PST_CFL_DTR)

      SV2D_PST_CFL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur par défaut de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SV2D_PST_CFL_NOBJMAX,
     &                   SV2D_PST_CFL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SV2D_PST_CFL_HVALIDE(HOBJ))
         IOB = HOBJ - SV2D_PST_CFL_HBASE

         SV2D_PST_CFL_HPRNT(IOB) = 0
      ENDIF

      SV2D_PST_CFL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SV2D_PST_CFL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SV2D_PST_CFL_NOBJMAX,
     &                   SV2D_PST_CFL_HBASE)

      SV2D_PST_CFL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCLC, HFLOG
      INTEGER HPRNT
      EXTERNAL SV2D_PST_CFL_CLC, SV2D_PST_CFL_LOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_PST_CFL_RST(HOBJ)

C---     Construit et initialise les call-back
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFCLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFCLC,HOBJ,SV2D_PST_CFL_CLC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFLOG)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HFLOG,HOBJ,SV2D_PST_CFL_LOG)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_SIMU_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_SIMU_INI(HPRNT,
     &                                HFCLC, HFLOG,
     &                                NOMFIC, ISTAT, IOPR, IOPW)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SV2D_PST_CFL_HBASE
         SV2D_PST_CFL_HPRNT(IOB) = HPRNT
      ENDIF

      SV2D_PST_CFL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - SV2D_PST_CFL_HBASE
      HPRNT = SV2D_PST_CFL_HPRNT(IOB)

C---     Détruis les attributs
      IF (PS_SIMU_HVALIDE(HPRNT)) IERR = PS_SIMU_DTR(HPRNT)

C---     Reset
      SV2D_PST_CFL_HPRNT(IOB) = 0

      SV2D_PST_CFL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_PST_CFL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'sv2d_pst_cfl.fc'
C------------------------------------------------------------------------

      SV2D_PST_CFL_REQHBASE = SV2D_PST_CFL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_PST_CFL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sv2d_pst_cfl.fc'
C------------------------------------------------------------------------

      SV2D_PST_CFL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SV2D_PST_CFL_NOBJMAX,
     &                                       SV2D_PST_CFL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ - SV2D_PST_CFL_HBASE)
      SV2D_PST_CFL_ACC = PS_SIMU_ACC(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ - SV2D_PST_CFL_HBASE)
      SV2D_PST_CFL_FIN = PS_SIMU_FIN(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ - SV2D_PST_CFL_HBASE)
      IERR = PS_SIMU_XEQ(HPRNT, HSIM, SV2D_PST_CFL_NPOST)

      SV2D_PST_CFL_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de cohérence est faible. En fait, il faudrait contrôler
C     si HSIM et la formulation sont bien les bonnes.
C************************************************************************
      FUNCTION SV2D_PST_CFL_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Transfert l'appel au parent
      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ - SV2D_PST_CFL_HBASE)
      IERR = PS_SIMU_ASGHSIM(HPRNT, HSIM, SV2D_PST_CFL_NPOST)

      SV2D_PST_CFL_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CFL_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ-SV2D_PST_CFL_HBASE)
      SV2D_PST_CFL_REQHVNO = PS_SIMU_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_PST_CFL_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_PST_CFL_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_PST_CFL_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'pssimu.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = SV2D_PST_CFL_HPRNT(HOBJ-SV2D_PST_CFL_HBASE)
      SV2D_PST_CFL_REQNOMF = PS_SIMU_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_CFL_CLC fait le calcul de post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'appel à SV2D_LGCY_HVALIDE n'est pas virtuel est va donc détecter
C     si on a un vrai SV2D_LGCY. Il doit donc être fait en premier.
C************************************************************************
      FUNCTION SV2D_PST_CFL_CLC(HOBJ,
     &                          HELM,
     &                          NPST,
     &                          NNL,
     &                          VPOST)

      USE SV2D_LGCY_M
      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_ELEM_M,  ONLY: LM_ELEM_T
      USE LM_HELE_M,  ONLY: LM_HELE_REQOMNG
      USE LM_GDTA_M,  ONLY: LM_GDTA_T
      USE LM_EDTA_M,  ONLY: LM_EDTA_T
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELM
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST(NPST, NNL)

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      CLASS(LM_ELEM_T),  POINTER :: OELE
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les données
      IF (SV2D_LGCY_HVALIDE(HELM)) THEN   ! cf note
         GDTA => SV2D_LGCY_REQGDTA(HELM)
         EDTA => SV2D_LGCY_REQEDTA(HELM)
         IPRN => SV2D_LGCY_REQIPRN(HELM)
         XPRG => SV2D_LGCY_REQXPRG(HELM)
      ELSEIF (LM_HELE_HVALIDE(HELM)) THEN
         OELE => LM_HELE_REQOMNG(HELM)
         SELECT TYPE(OELE)
            CLASS IS (SV2D_XBS_T)
               GDTA => OELE%GDTA
               EDTA => OELE%EDTA
               IPRN => OELE%IPRN
               XPRG => OELE%XPRG
            CLASS DEFAULT
               CALL ERR_ASG(ERR_FTL, 'Unsupported type')
         END SELECT
      ELSE
         CALL ERR_ASG(ERR_FTL, 'Unsupported type')
      ENDIF

C---     Fait le calcul
      IERR = SV2D_PST_CFL_CLC2(HOBJ,
     &                         GDTA,
     &                         EDTA,
     &                         XPRG,
     &                         IPRN,
     &                         NPST,
     &                         NNL,
     &                         VPOST)

      SV2D_PST_CFL_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_PST_CLC
C
C Description:
C     La fonction privée SV2D_PST_CFL_CLC fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_CLC2(HOBJ,
     &                           GDTA,
     &                           EDTA,
     &                           XPRG,
     &                           IPRN,
     &                           NPST,
     &                           NNL,
     &                           VPOST)

      USE SV2D_XBS_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      TYPE (LM_GDTA_T),   INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T),   INTENT(IN) :: EDTA
      TYPE (SV2D_XPRG_T), INTENT(IN) :: XPRG
      TYPE (SV2D_IPRN_T), INTENT(IN) :: IPRN
      INTEGER, INTENT(IN) :: NPST
      INTEGER, INTENT(IN) :: NNL
      REAL*8, INTENT(INOUT) :: VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IC, IE, IN
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DJT3
      REAL*8  P1, P2, P3, P4, P5, P6
      REAL*8  U1, U2, U3, U4, U5, U6
      REAL*8  V1, V2, V3, V4, V5, V6
      REAL*8  PM1, PM2, PM3, PM4
      REAL*8  UM1, UM2, UM3, UM4
      REAL*8  VM1, VM2, VM3, VM4
      REAL*8  UX1, UX2, UX3, UX4
      REAL*8  UY1, UY2, UY3, UY4
      REAL*8  VX1, VX2, VX3, VX4
      REAL*8  VY1, VY2, VY3, VY4
      REAL*8  VIS1, VIS2, VIS3, VIS4
      REAL*8  CFL1, CFL2, CFL3, CFL4
      REAL*8  PE1, PE2, PE3, PE4
      REAL*8  DT, G, UX, UY, PA, V
      REAL*8  CFL, Pe, Fr
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .GE. SV2D_PST_CFL_NPOST)
D     CALL ERR_PRE(NNL  .EQ. GDTA%NNL)
C-----------------------------------------------------------------------

D     CALL LOG_TODO('DT!!')
      DT = 1.0D0
      G  = XPRG%GRAVITE

C---     Initialisation de VPOST
      VPOST(:,:) = ZERO
      
C---     BOUCLE SUR LES ELEMENTS
C        =======================
      DO 10 IC=1,GDTA%NELCOL
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Connectivités du T6
         NO1  = GDTA%KNGV(1,IE)
         NO2  = GDTA%KNGV(2,IE)
         NO3  = GDTA%KNGV(3,IE)
         NO4  = GDTA%KNGV(4,IE)
         NO5  = GDTA%KNGV(5,IE)
         NO6  = GDTA%KNGV(6,IE)

C---        Métriques des T3
         VKX = GDTA%VDJV(1,IE)*UN_2
         VEX = GDTA%VDJV(2,IE)*UN_2
         VKY = GDTA%VDJV(3,IE)*UN_2
         VEY = GDTA%VDJV(4,IE)*UN_2
         VSX = -(VKX+VEX)
         VSY = -(VKY+VEY)
         DJT3 = GDTA%VDJV(5,IE)*UN_4

C---        Valeurs nodales
         U1 = EDTA%VPRNO(IPRN%U,NO1)     ! NOEUD 1
         V1 = EDTA%VPRNO(IPRN%V,NO1)
         P1 = EDTA%VPRNO(IPRN%H,NO1)
         U2 = EDTA%VPRNO(IPRN%U,NO2)     ! NOEUD 2
         V2 = EDTA%VPRNO(IPRN%V,NO2)
         P2 = EDTA%VPRNO(IPRN%H,NO2)
         U3 = EDTA%VPRNO(IPRN%U,NO3)     ! NOEUD 3
         V3 = EDTA%VPRNO(IPRN%V,NO3)
         P3 = EDTA%VPRNO(IPRN%H,NO3)
         U4 = EDTA%VPRNO(IPRN%U,NO4)     ! NOEUD 4
         V4 = EDTA%VPRNO(IPRN%V,NO4)
         P4 = EDTA%VPRNO(IPRN%H,NO4)
         U5 = EDTA%VPRNO(IPRN%U,NO5)     ! NOEUD 5
         V5 = EDTA%VPRNO(IPRN%V,NO5)
         P5 = EDTA%VPRNO(IPRN%H,NO5)
         U6 = EDTA%VPRNO(IPRN%U,NO6)     ! NOEUD 6
         V6 = EDTA%VPRNO(IPRN%V,NO6)
         P6 = EDTA%VPRNO(IPRN%H,NO6)

C---        Vitesses moyenne en X
         UM1 = (U1+U2+U6)*UN_3
         UM2 = (U2+U3+U4)*UN_3
         UM3 = (U6+U4+U5)*UN_3
         UM4 = (U4+U6+U2)*UN_3

C---        Vitesses moyenne en Y
         VM1 = (V1+V2+V6)*UN_3
         VM2 = (V2+V3+V4)*UN_3
         VM3 = (V6+V4+V5)*UN_3
         VM4 = (V4+V6+V2)*UN_3

C---        Profondeur moyenne
         PM1 = (P1+P2+P6)*UN_3
         PM2 = (P2+P3+P4)*UN_3
         PM3 = (P6+P4+P5)*UN_3
         PM4 = (P4+P6+P2)*UN_3

C---        Viscosité physique
         VIS1 = EDTA%VPREV(1,1,IE)
         VIS2 = EDTA%VPREV(1,2,IE)
         VIS3 = EDTA%VPREV(1,3,IE)
         VIS4 = EDTA%VPREV(1,4,IE)

C---        CFL
         CFL1 = SP_HDRO_CFL(DT, G, UM1, VM1, PM1, VEY, -VEX, -VKY, VKX)
         CFL2 = SP_HDRO_CFL(DT, G, UM2, VM2, PM2, VEY, -VEX, -VKY, VKX)
         CFL3 = SP_HDRO_CFL(DT, G, UM3, VM3, PM3, VEY, -VEX, -VKY, VKX)
         CFL4 = SP_HDRO_CFL(DT, G, UM4, VM4, PM4, VEY, -VEX, -VKY, VKX)

C---        Peclets
         PE1 = SP_HDRO_PECLET(VIS1, UM1, VM1, PM1, VEY, -VEX, -VKY, VKX)
         PE2 = SP_HDRO_PECLET(VIS2, UM2, VM2, PM2, VEY, -VEX, -VKY, VKX)
         PE3 = SP_HDRO_PECLET(VIS3, UM3, VM3, PM3, VEY, -VEX, -VKY, VKX)
         PE4 = SP_HDRO_PECLET(VIS4, UM4, VM4, PM4, VEY, -VEX, -VKY, VKX)

C---        Accumulation des CFL
         VPOST(1,NO1) = VPOST(1,NO1) + CFL1*DJT3
         VPOST(1,NO2) = VPOST(1,NO2) + (CFL1+CFL2+CFL4)*DJT3*UN_3
         VPOST(1,NO3) = VPOST(1,NO3) + CFL2*DJT3
         VPOST(1,NO4) = VPOST(1,NO4) + (CFL2+CFL3+CFL4)*DJT3*UN_3
         VPOST(1,NO5) = VPOST(1,NO5) + CFL3*DJT3
         VPOST(1,NO6) = VPOST(1,NO6) + (CFL1+CFL3+CFL4)*DJT3*UN_3

C---        Accumulation des Peclets
         VPOST(2,NO1) = VPOST(2,NO1) + PE1*DJT3
         VPOST(2,NO2) = VPOST(2,NO2) + (PE1+PE2+PE4)*DJT3*UN_3
         VPOST(2,NO3) = VPOST(2,NO3) + PE2*DJT3
         VPOST(2,NO4) = VPOST(2,NO4) + (PE2+PE3+PE4)*DJT3*UN_3
         VPOST(2,NO5) = VPOST(2,NO5) + PE3*DJT3
         VPOST(2,NO6) = VPOST(2,NO6) + (PE1+PE3+PE4)*DJT3*UN_3

C---        Accumulation des DJT3
         VPOST(3,NO1) = VPOST(3,NO1) + DJT3
         VPOST(3,NO2) = VPOST(3,NO2) + DJT3
         VPOST(3,NO3) = VPOST(3,NO3) + DJT3
         VPOST(3,NO4) = VPOST(3,NO4) + DJT3
         VPOST(3,NO5) = VPOST(3,NO5) + DJT3
         VPOST(3,NO6) = VPOST(3,NO6) + DJT3

20    CONTINUE
10    CONTINUE

C---     Transfert des valeurs aux noeuds
      DO IN=1, GDTA%NNL
         UX = EDTA%VPRNO(IPRN%U,IN)
         UY = EDTA%VPRNO(IPRN%V,IN)
         PA = EDTA%VPRNO(IPRN%H,IN)
         V  = HYPOT(UX, UY)

         CFL = VPOST(1,IN) / VPOST(3,IN)  ! CFL
         Pe  = VPOST(2,IN) / VPOST(3,IN)  ! Peclet
         Fr  = V / SQRT(XPRG%GRAVITE*PA)

C---        Chargement de VPOST
         VPOST(1,IN) = CFL
         VPOST(2,IN) = Pe
         VPOST(3,IN) = Fr
      ENDDO

      SV2D_PST_CFL_CLC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_PST_CFL_LOG
C
C Description:
C     La fonction privée SV2D_PST_CFL_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_PST_CFL_LOG(HOBJ, HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'sv2d_pst_cfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sv2d_pst_cfl.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_PST_CFL_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPST .EQ. SV2D_PST_CFL_NPOST)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SV2D_POST_CLF:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<25>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)')    'MSG_VALEUR#<25>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'CFL')
      IERR = PS_PSTD_LOGVAL(HNUMR,  2, NPST, NNL, VPOST, 'Pe')
      IERR = PS_PSTD_LOGVAL(HNUMR,  3, NPST, NNL, VPOST, 'Fr')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SV2D_PST_CFL_LOG = ERR_TYP()
      RETURN
      END
