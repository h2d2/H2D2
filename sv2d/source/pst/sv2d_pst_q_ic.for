C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_SV2D_PST_Q_CMD
C     CHARACTER*(32) IC_SV2D_PST_Q_REQCMD
C   Private:
C     SUBROUTINE IC_SV2D_PST_Q_AID
C     INTEGER IC_SV2D_PST_Q_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_PST_Q_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_PST_Q_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_pst_q_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'sv2d_pst_q_ic.fc'

      INTEGER IERR
      INTEGER ISTAT
      INTEGER HOBJ
      INTEGER HPST
      INTEGER IOPR, IOPW
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SV2D_PST_Q_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the post-treatment file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IERR .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 3, OPRD)
      IF (IERR .NE. 0) OPRD = 'noop'
C     <comment>Writing operation ['write', 'nowrite'] (default 'nowrite')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 4, OPWR)
      IF (IERR .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9905
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9906

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR = SV2D_PST_Q_CTR(HPST)
      IF (ERR_GOOD()) IERR = SV2D_PST_Q_INI(HPST, NOMFIC, ISTAT,
     &                                      IOPR, IOPW)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_SV2D_PST_Q_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_PST_Q_AID()

9999  CONTINUE
      IC_SV2D_PST_Q_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_PST_Q_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_PST_Q_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_q_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>sv2d_pst_bilan_debit</b> constructs the post-treatment that
C  computes the flow balance on each node for the sv2d element. Flow
C  balance can be used as a measure of a more "physical" error compared
C  to an error estimate like the error ellipses that relies only on the
C  approximation.
C</comment>
      IC_SV2D_PST_Q_REQCMD = 'sv2d_pst_bilan_debit'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SV2D_PST_Q_AID()

      IMPLICIT NONE

      INCLUDE 'sv2d_pst_q_ic.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_pst_q_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_PST_Q_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_pst_q.fi'
      INCLUDE 'sv2d_pst_q_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     En-tête
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_SV2D_POST_Q'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = SV2D_PST_Q_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 60)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_SV2D_PST_Q_PRN = ERR_TYP()
      RETURN
      END

