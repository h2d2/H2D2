C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PSLSOLC
C
C Description:
C     Traitement post-lecture des sollicitations réparties
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PSLSOLC (VSOLC,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PSLSOLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VSOLC(LM_CMMN_NSOLC, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END

