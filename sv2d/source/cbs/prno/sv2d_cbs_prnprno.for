C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  COEF. COMP. D'AMORTISSEMENT
C                    DS    16)  POROSITÉ
C
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I
      INTEGER SV2D_CBS_PRN1
C-----------------------------------------------------------------------

C---     IMPRIME ENTETE
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     PRNO lues
      I = SV2D_CBS_PRN1(SV2D_IPRNO_Z,    'MSG_ZF')
      I = SV2D_CBS_PRN1(SV2D_IPRNO_N,    'MSG_COEF_MANNING')
      I = SV2D_CBS_PRN1(SV2D_IPRNO_ICE_E,'MSG_GLACE_EPAISSEUR')
      I = SV2D_CBS_PRN1(SV2D_IPRNO_ICE_N,'MSG_GLACE_COEF_MANNING')
      I = SV2D_CBS_PRN1(SV2D_IPRNO_WND_X,'MSG_VENT_X')
      I = SV2D_CBS_PRN1(SV2D_IPRNO_WND_Y,'MSG_VENT_Y')

C---     PRNO calculées
      IF (LOG_REQNIV() .GE. LOG_LVL_INFO) THEN
         CALL LOG_ECRIS(' ')
         WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT_CALCULEES:'
         CALL LOG_ECRIS(LOG_BUF)

         I = SV2D_CBS_PRN1(SV2D_IPRNO_U,         'MSG_VITESSE_X')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_V,         'MSG_VITESSE_Y')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_H,         'MSG_PROF_POSITIVE')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_CNVT,'MSG_COEF_CONVECTION')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_GRVT,'MSG_COEF_GRAVITE')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_FROT,'MSG_COEF_MANNING_EFF')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_DIFF,'MSG_COEF_DIFFUSION')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_PE  ,'MSG_COEF_PECLET')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_VENT,'MSG_COEF_VENT')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_DRCY,'MSG_COEF_DARCY')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_COEFF_PORO,'MSG_COEF_POROSITE')
         I = SV2D_CBS_PRN1(SV2D_IPRNO_DECOU_PENA,'MSG_COEF_PENALITE')
      ENDIF

      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_PRN1 imprime une seule
C     propriété nodale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_PRN1(IP, TXT)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_PRN1
      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      IF (IP .GT. 0) THEN
         WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      SV2D_CBS_PRN1 = ERR_TYP()
      RETURN
      END
