C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_HLPPRGL
C   Private:
C     INTEGER SV2D_CBS_HLP1
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_HLPPRGL
C
C Description:
C     Aide sur les propriétés globales
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_HLPPRGL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_HLPPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I
      INTEGER SV2D_CBS_HLP1

      INTERFACE
         INTEGER FUNCTION FUNC_HLP(IP, TXT)
            INTEGER,       INTENT(IN) :: IP
            CHARACTER*(*), INTENT(IN) :: TXT
         END FUNCTION
      END INTERFACE

      EXTERNAL SV2D_CBS_HLP1
      PROCEDURE (FUNC_HLP), POINTER :: H => NULL()
C-----------------------------------------------------------------------

      H => SV2D_CBS_HLP1

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Groupe des propriétés physiques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I=H(SV2D_IPRGL_GRAVITE,       'MSG_GRAVITE')
      I=H(SV2D_IPRGL_LATITUDE,      'MSG_LATITUDE')
      I=H(SV2D_IPRGL_FCT_CW_VENT,   'MSG_FCT_CW_VENT')
      I=H(SV2D_IPRGL_VISCO_CST,     'MSG_VISCOSITE_MOLECULAIRE')
      I=H(SV2D_IPRGL_VISCO_LM,      'MSG_COEF_LONGUEUR_MELANGE')
      I=H(SV2D_IPRGL_VISCO_SMGO,    'MSG_COEF_SMAGORINSKY')
      I=H(SV2D_IPRGL_VISCO_BINF,    'MSG_BORNE_INF_VISCOSITE')
      I=H(SV2D_IPRGL_VISCO_BSUP,    'MSG_BORNE_SUP_VISCOSITE')

C---     Groupe des propriétés du découvrement
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I=H(SV2D_IPRGL_DECOU_HTRG,    'MSG_DECOUVREMENT_PROF_TRIG')
      I=H(SV2D_IPRGL_DECOU_HMIN,    'MSG_DECOUVREMENT_PROF_MIN')
      I=H(SV2D_IPRGL_DECOU_PENA_H,  'MSG_DECOUVREMENT_PENA_H')
      I=H(SV2D_IPRGL_DECOU_PENA_Q,  'MSG_DECOUVREMENT_PENA_Q')
      I=H(SV2D_IPRGL_DECOU_MAN,     'MSG_DECOUVREMENT_MAN')
      I=H(SV2D_IPRGL_DECOU_UMAX,    'MSG_DECOUVREMENT_UMAX')
      I=H(SV2D_IPRGL_DECOU_PORO,    'MSG_DECOUVREMENT_PORO')
      I=H(SV2D_IPRGL_DECOU_AMORT,   'MSG_DECOUVREMENT_AMORT')
      I=H(SV2D_IPRGL_DECOU_CON_FACT,'MSG_DECOUVREMENT_COEF_CONVECTION')
      I=H(SV2D_IPRGL_DECOU_GRA_FACT,'MSG_DECOUVREMENT_COEF_GRAVITE')
      I=H(SV2D_IPRGL_DECOU_DIF_NU,  'MSG_DECOUVREMENT_NU_DIFFUSION')
      I=H(SV2D_IPRGL_DECOU_DIF_PE,  'MSG_DECOUVREMENT_PECLET')
      I=H(SV2D_IPRGL_DECOU_DRC_NU,  'MSG_DECOUVREMENT_NU_DARCY')

C---     Groupe des propriétés de stabilisation
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I=H(SV2D_IPRGL_STABI_PECLET,  'MSG_PECLET')
      I=H(SV2D_IPRGL_STABI_AMORT,   'MSG_COEF_AMORTISSEMENT')
      I=H(SV2D_IPRGL_STABI_DARCY,   'MSG_COEF_SURFACE_LIBRE_DARCY')
      I=H(SV2D_IPRGL_STABI_LAPIDUS, 'MSG_COEF_SURFACE_LIBRE_LAPIDUS')

C---     Groupe des prop d'activation-désactivation des termes
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I=H(SV2D_IPRGL_CMULT_CON,     'MSG_COEF_CONVECTION')
      I=H(SV2D_IPRGL_CMULT_GRA,     'MSG_COEF_GRAVITE')
      I=H(SV2D_IPRGL_CMULT_PDYN,    'MSG_COEF_PDYN')
      I=H(SV2D_IPRGL_CMULT_MAN,     'MSG_COEF_FROTTEMENT_FOND')
      I=H(SV2D_IPRGL_CMULT_VENT,    'MSG_COEF_FROTTEMENT_VENT')
      I=H(SV2D_IPRGL_CMULT_INTGCTR, 'MSG_COEF_INTGRL_CONTOUR')

C---     Groupe des propriétés numériques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I=H(SV2D_IPRGL_PNUMR_PENALITE,'MSG_COEF_PENALITE')
      I=H(SV2D_IPRGL_PNUMR_DELPRT,  'MSG_COEF_PERTURBATION_KT')
      I=H(SV2D_IPRGL_PNUMR_DELMIN,  'MSG_COEF_PERTURBATION_KT_MIN')
      I=H(SV2D_IPRGL_PNUMR_OMEGAKT, 'MSG_COEF_RELAXATION_KT')
      I=H(SV2D_IPRGL_PNUMR_PRTPREL, 'MSG_COEF_PERTURB_PREL')
      I=H(SV2D_IPRGL_PNUMR_PRTPRNO, 'MSG_COEF_PERTURB_PRNO')

      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_HLP1 imprime la description d'une
C     seule propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_HLP1(IP, TXT)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_HLP1
      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      IF (IP .GT. 0) THEN
         WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      SV2D_CBS_HLP1 = ERR_TYP()
      RETURN
      END
