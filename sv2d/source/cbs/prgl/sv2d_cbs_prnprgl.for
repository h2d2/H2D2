C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_PRNPRGL
C   Private:
C     INTEGER SV2D_CBS_1RE8
C     INTEGER SV2D_CBS_1INT
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_PRNPRGL
C
C Description:
C     IMPRESSION DE PROPRIETES GLOBALES
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_PRNPRGL(NPRGL, VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I, ICW
      INTEGER IERR
      CHARACTER*(64)  T
      CHARACTER*(128) BUF
      
      INTEGER SV2D_CBS_1RE8
      INTEGER SV2D_CBS_1INT
      INTEGER SV2D_CBS_1STR
C-----------------------------------------------------------------------

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Groupe des propriétés physiques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SV2D_IPRGL_GRAVITE
      IERR=SV2D_CBS_1RE8(I,'MSG_GRAVITE', VPRGL(I))
      I = SV2D_IPRGL_LATITUDE
      IERR=SV2D_CBS_1RE8(I,'MSG_LATITUDE', VPRGL(I))
      I = SV2D_IPRGL_FCT_CW_VENT
      IF (I .GT. 0) THEN
         ICW = NINT(VPRGL(I))
         T = SP_HDRO_CWNAME(ABS(ICW))
         IF (ICW .LT. 0) T = T(1:SP_STRN_LEN(T)) // '; MSG_VENT_RELATIF'
         IF (ICW .GT. 0) T = T(1:SP_STRN_LEN(T)) // '; MSG_VENT_ABSOLU'
         WRITE(BUF,'(I3,3A)') ICW, ' (', T(1:SP_STRN_LEN(T)), ')'
         IERR = SV2D_CBS_1STR(I, 'MSG_FCT_CW_VENT', BUF)
      ENDIF
      I = SV2D_IPRGL_VISCO_CST
      IERR=SV2D_CBS_1RE8(I,'MSG_VISCOSITE_MOLECULAIRE',VPRGL(I))
      I = SV2D_IPRGL_VISCO_LM
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_LONGUEUR_MELANGE',VPRGL(I))
      I = SV2D_IPRGL_VISCO_SMGO
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_SMAGORINSKY',VPRGL(I))
      I = SV2D_IPRGL_VISCO_BINF
      IERR=SV2D_CBS_1RE8(I,'MSG_BORNE_INF_VISCOSITE',VPRGL(I))
      I = SV2D_IPRGL_VISCO_BSUP
      IERR=SV2D_CBS_1RE8(I,'MSG_BORNE_SUP_VISCOSITE',VPRGL(I))

C---     Groupe des propriétés du découvrement
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SV2D_IPRGL_DECOU_HTRG
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PROF_TRIG',VPRGL(I))
      I = SV2D_IPRGL_DECOU_HMIN
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PROF_MIN',VPRGL(I))
      I = SV2D_IPRGL_DECOU_PENA_H
      IF (I .GT. 0)
     &   IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PENA_H', VPRGL(I))
      I = SV2D_IPRGL_DECOU_PENA_Q
      IF (I .GT. 0)
     &   IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PENA_Q', VPRGL(I))
      I = SV2D_IPRGL_DECOU_MAN
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_MAN',VPRGL(I))
      I = SV2D_IPRGL_DECOU_UMAX
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_UMAX',VPRGL(I))
      I = SV2D_IPRGL_DECOU_PORO
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PORO',VPRGL(I))
      I = SV2D_IPRGL_DECOU_AMORT
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_AMORT',VPRGL(I))
      I = SV2D_IPRGL_DECOU_CON_FACT
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_COEF_CONVECTION',VPRGL(I))
      I = SV2D_IPRGL_DECOU_GRA_FACT
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_COEF_GRAVITE',VPRGL(I))
      I = SV2D_IPRGL_DECOU_DIF_NU
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_NU_DIFFUSION',VPRGL(I))
      I = SV2D_IPRGL_DECOU_DIF_PE
      IF (I .GT. 0)
     &   IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_PECLET',VPRGL(I))
      I = SV2D_IPRGL_DECOU_DRC_NU
      IERR=SV2D_CBS_1RE8(I,'MSG_DECOUVREMENT_NU_DARCY',VPRGL(I))

C---     Groupe des propriétés de stabilisation
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SV2D_IPRGL_STABI_PECLET
      IERR=SV2D_CBS_1RE8(I,'MSG_PECLET',VPRGL(I))
      I = SV2D_IPRGL_STABI_AMORT
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_AMORTISSEMENT',VPRGL(I))
      I = SV2D_IPRGL_STABI_DARCY
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_SURFACE_LIBRE_DARCY',VPRGL(I))
      I = SV2D_IPRGL_STABI_LAPIDUS
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_SURFACE_LIBRE_LAPIDUS',VPRGL(I))

C---     Groupe des prop d'activation-désactivation des termes
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SV2D_IPRGL_CMULT_CON
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_CONVECTION',VPRGL(I))
      I = SV2D_IPRGL_CMULT_GRA
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_GRAVITE',VPRGL(I))
      I = SV2D_IPRGL_CMULT_PDYN
      IF (I .GT. 0)
     &   IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PDYN',VPRGL(I))
      I = SV2D_IPRGL_CMULT_MAN
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_FROTTEMENT_FOND',VPRGL(I))
      I = SV2D_IPRGL_CMULT_VENT
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_FROTTEMENT_VENT',VPRGL(I))
      I = SV2D_IPRGL_CMULT_INTGCTR
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_INTGRL_CONTOUR',VPRGL(I))

C---     Groupe des propriétés numériques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SV2D_IPRGL_PNUMR_PENALITE
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PENALITE',VPRGL(I))
      I = SV2D_IPRGL_PNUMR_DELPRT
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PERTURBATION_KT',VPRGL(I))
      I = SV2D_IPRGL_PNUMR_DELMIN
      IF (I .GT. 0)
     &   IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PERTURBATION_KT_MIN',VPRGL(I))
      I = SV2D_IPRGL_PNUMR_OMEGAKT
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_RELAXATION_KT',VPRGL(I))
      I = SV2D_IPRGL_PNUMR_PRTPREL
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PERTURB_PREL',VPRGL(I))
      I = SV2D_IPRGL_PNUMR_PRTPRNO
      IERR=SV2D_CBS_1RE8(I,'MSG_COEF_PERTURB_PRNO',VPRGL(I))

      CALL LOG_DECIND()

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_CALCULEES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()
      I = SV2D_IPRGL_CORIOLIS
      IERR=SV2D_CBS_1RE8(I,'MSG_FORCE_CORIOLIS',VPRGL(I))
      I = SV2D_IPRGL_FCT_CW_VENT
      IF (I .LE. 0)
     &   IERR=SV2D_CBS_1INT(I,'MSG_FCT_CW_VENT', SV2D_CW_WU1969)
      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_1RE8 imprime une seule
C     propriété globale de type Real*8
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_1RE8(IP, TXT, V)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_1RE8
      INTEGER       IP
      CHARACTER*(*) TXT
      REAL*8        V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,1PE14.6E3)') IP, TXT, '#<60>#=', V
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_CBS_1RE8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_1RE8 imprime une seule
C     propriété globale de type I8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_1INT(IP, TXT, IV)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_1INT
      INTEGER       IP
      CHARACTER*(*) TXT
      INTEGER       IV

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,I12)') IP, TXT, '#<60>#=', IV
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_CBS_1INT = ERR_TYP()
      RETURN
      END
      
C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_1STR imprime une seule
C     propriété globale de type STRing
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_1STR(IP, TXT, V)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_1STR
      INTEGER       IP
      CHARACTER*(*) TXT
      CHARACTER*(*) V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,A)') IP, TXT, '#<60>#=', V
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_CBS_1STR = ERR_TYP()
      RETURN
      END
