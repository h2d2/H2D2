C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_PSLPRGL
C   Private:
C     INTEGER CHKVAL
C     SUBROUTINE SV2D_CBS_PSLPRGL_E
C     SUBROUTINE SV2D_CBS_INI_IPRGL
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      CALL SV2D_CBS_INI_IPRGL()
      CALL SV2D_CBS_PSLPRGL_E(VPRGL, IERR)

      RETURN
      END

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PSLPRGL_E(VPRGL, IERR)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I, IE
      REAL*8  VVAL, VMIN, VMAX, VLAT

      REAL*8  DIX
      REAL*8  CENT
      REAL*8  TGV
      PARAMETER (DIX  = 1.0D+01)
      PARAMETER (CENT = 1.0D+02)
      PARAMETER (TGV  = 1.0D+99)

      REAL*8  GRAMIN
      REAL*8  GRAMAX
      PARAMETER (GRAMAX=9.86605398D0)
      PARAMETER (GRAMIN=9.75258282D0)

      REAL*8  VISCOMAX
      REAL*8  LMMAX
      PARAMETER (VISCOMAX=1.0D0)
      PARAMETER (LMMAX=10.0D0)

      INTEGER CHKVAL
C-----------------------------------------------------------------------
      REAL*8 V, V1, V2
      LOGICAL IS_OUT
      IS_OUT(V,V1,V2) = (V .LT. V1 .OR.  V .GT. V2)
C-----------------------------------------------------------------------

!      SV2D_GRAVITE         = VPRGL( 1)
!      SV2D_CORIOLIS        = VPRGL(33)
!      SV2D_FCT_CW_VENT     = VPRGL( 3)
!      SV2D_VISCO_CST       = VPRGL( 4)
!      SV2D_VISCO_LM        = VPRGL( 5)
!      SV2D_VISCO_SMGO      = VPRGL( 6)
!      SV2D_VISCO_BINF      = VPRGL( 7)
!      SV2D_VISCO_BSUP      = VPRGL( 8)
!
!      SV2D_DECOU_HTRG      = VPRGL( 9)
!      SV2D_DECOU_HMIN      = VPRGL(10)
!      SV2D_DECOU_MAN       = VPRGL(11)
!      SV2D_DECOU_UMAX      = VPRGL(12)
!      SV2D_DECOU_PORO      = VPRGL(13)
!      SV2D_DECOU_AMORT     = VPRGL(14)
!      SV2D_DECOU_CON_FACT  = VPRGL(15)
!      SV2D_DECOU_GRA_FACT  = VPRGL(16)
!      SV2D_DECOU_DIF_NU    = VPRGL(17)
!      SV2D_DECOU_DRC_NU    = VPRGL(18)
!
!      SV2D_STABI_PECLET    = VPRGL(19)
!      SV2D_STABI_AMORT     = VPRGL(20)
!      SV2D_STABI_DARCY     = VPRGL(21)
!      SV2D_STABI_LAPIDUS   = VPRGL(22)
!
!      SV2D_CMULT_CON       = VPRGL(23)
!      SV2D_CMULT_GRA       = VPRGL(24)
!      SV2D_CMULT_MAN       = VPRGL(25)
!      SV2D_CMULT_VENT      = VPRGL(26)
!      SV2D_CMULT_INTGCTR   = VPRGL(27)
!
!      SV2D_PNUMR_PENALITE  = VPRGL(28)
!      SV2D_PNUMR_DELPRT    = VPRGL(28)
!      SV2D_PNUMR_OMEGAKT   = VPRGL(30)
!      SV2D_PNUMR_PRTPREL   = (NINT(VPRGL(31)) .NE. 0)
!      SV2D_PNUMR_PRTPRNO   = (NINT(VPRGL(32)) .NE. 0)

C---     Gravité
      I = SV2D_IPRGL_GRAVITE
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_GRAVITE')

C---     Coriolis
      I = SV2D_IPRGL_LATITUDE
      IE = CHKVAL(I, VPRGL(I), -90.0D+00, 90.0D+00, 'MSG_LATITUDE')

C---     Viscosité
      I = SV2D_IPRGL_VISCO_CST
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_LAMINAIRE')
      I = SV2D_IPRGL_VISCO_LM
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_LM')
      I = SV2D_IPRGL_VISCO_SMGO
      IE = CHKVAL(I, VPRGL(I), ZERO, DIX, 'MSG_VISCO_SMGO')
      I = SV2D_IPRGL_VISCO_BINF
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_BORNE_INF')
      I = SV2D_IPRGL_VISCO_BSUP
      IE = CHKVAL(I, VPRGL(I), VPRGL(I-1), TGV, 'MSG_VISCO_BORNE_SUP')

C---     Découvrement
      I = SV2D_IPRGL_DECOU_HTRG
      IE = CHKVAL(I, VPRGL(I), ZERO,CENT, 'MSG_DECOUVREMENT_PROF_TRIG')
      I = SV2D_IPRGL_DECOU_HMIN
      VMAX = (1.0D0-1.0D-12)*VPRGL(SV2D_IPRGL_DECOU_HTRG)
      IE = CHKVAL(I, VPRGL(I), ZERO,VMAX, 'MSG_DECOUVREMENT_PROF_MIN')
      I = SV2D_IPRGL_DECOU_MAN
      IE = CHKVAL(I, VPRGL(I), ZERO, DIX, 'MSG_DECOUVREMENT_MAN')
      I = SV2D_IPRGL_DECOU_UMAX
      IE = CHKVAL(I, VPRGL(I), ZERO,CENT, 'MSG_DECOUVREMENT_UMAX')
      I = SV2D_IPRGL_DECOU_PORO
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_PORO')
      I = SV2D_IPRGL_DECOU_AMORT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_AMORT')

      I = SV2D_IPRGL_DECOU_CON_FACT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_CON_FACT')
      I = SV2D_IPRGL_DECOU_GRA_FACT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_GRA_FACT')
      I = SV2D_IPRGL_DECOU_DIF_NU
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_DECOUVREMENT_DIF_NU')
      I = SV2D_IPRGL_DECOU_DRC_NU
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_DECOUVREMENT_DRC_NU')

C---     Stabilité
      I = SV2D_IPRGL_STABI_PECLET
      IE = CHKVAL(I, VPRGL(I),PETIT, TGV, 'MSG_STABI_PECLET')
      I = SV2D_IPRGL_STABI_AMORT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_STABI_AMORT')
      I = SV2D_IPRGL_STABI_DARCY
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_STABI_DARCY')
      I = SV2D_IPRGL_STABI_LAPIDUS
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_STABI_LAPIDUS')

C---     Coefficients
      I = SV2D_IPRGL_CMULT_CON
      IE = CHKVAL(I, VPRGL(I), ZERO, DEUX, 'MSG_CMULT_CON')
      I = SV2D_IPRGL_CMULT_GRA
      IE = CHKVAL(I, VPRGL(I), ZERO, DEUX, 'MSG_CMULT_GRA')
      I = SV2D_IPRGL_CMULT_MAN
      IE = CHKVAL(I, VPRGL(I), ZERO, DEUX, 'MSG_CMULT_MAN')
      I = SV2D_IPRGL_CMULT_VENT
      IE = CHKVAL(I, VPRGL(I), ZERO, DEUX, 'MSG_CMULT_VENT')
      I = SV2D_IPRGL_CMULT_INTGCTR
      IE = CHKVAL(I, VPRGL(I), ZERO, DEUX, 'MSG_CMULT_INTGCTR')

C---     Numérique
      I = SV2D_IPRGL_PNUMR_PENALITE
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_PNUMR_PENALITE')
      I = SV2D_IPRGL_PNUMR_DELPRT
      IE = CHKVAL(I, VPRGL(I),PETIT, UN, 'MSG_PNUMR_DELPRT')
      I = SV2D_IPRGL_PNUMR_OMEGAKT
      IE = CHKVAL(I, VPRGL(I),PETIT, UN, 'MSG_PNUMR_OMEGAKT')
      I = SV2D_IPRGL_PNUMR_PRTPREL
      IE = CHKVAL(I, VPRGL(I),ZERO, UN, 'MSG_PNUMR_PRTPREL')
      I = SV2D_IPRGL_PNUMR_PRTPRNO
      IE = CHKVAL(I, VPRGL(I),ZERO, UN, 'MSG_PNUMR_PRTPRNO')

C---     Avertissements
      IF (ERR_BAD()) GOTO 9999
      IF (IS_OUT(VPRGL(SV2D_IPRGL_GRAVITE), GRAMIN, GRAMAX)) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3,A,1PE14.6E3)')
     &      'MSG_GRAVITE_NON_TERRESTRE: ', GRAMIN,' <= g <= ',GRAMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (IS_OUT(VPRGL(SV2D_IPRGL_VISCO_CST), ZERO, VISCOMAX)) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_VISCO_NON_PHYSIQUE: ', 'nu <= ',VISCOMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (IS_OUT(VPRGL(SV2D_IPRGL_VISCO_LM), ZERO, LMMAX)) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_LM_PEU_REALISTE: ', 'lm <= ', LMMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Calcule la force de Coriolis
      ! Remarque:
      ! Le calcul ne devrait pas être fait car il doit être refait dans PRC.
      ! Sauf que la valeur de Coriolis est affichée dans PRN.
      ! Il faudrait peut-être éliminer l'affichage des propriétés calculées?
      IF (ERR_GOOD()) THEN
         VLAT = VPRGL(SV2D_IPRGL_LATITUDE)
         VPRGL(SV2D_IPRGL_CORIOLIS) = SP_HDRO_CORIOLIS(VLAT)
      ENDIF

9999  CONTINUE
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : CHKVAL
C
C Description:
C     Contrôle qu'une PRGL soit bien entre les valeurs MIN et MAX.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CHKVAL(I, V, VMIN, VMAX, TXT)

      IMPLICIT NONE

      INTEGER CHKVAL
      INTEGER I
      REAL*8  V
      REAL*8  VMIN
      REAL*8  VMAX
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IF ((I .GT. 0) .AND. (V .LT. VMIN .OR. V .GT. VMAX)) THEN
         WRITE(ERR_BUF,'(A)') 'ERR_VALEUR_INVALIDE'
         IF (ERR_GOOD()) THEN
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ELSE
            CALL ERR_AJT(ERR_BUF)
         ENDIF
         WRITE(ERR_BUF,'(3A)') '#<3>#MSG_VARIABLE', '#<18>#= ', TXT
         CALL ERR_AJT(ERR_BUF)

         WRITE(ERR_BUF,'(2A,I6)')       '#<3>#MSG_INDICE', '#<18>#= ', I
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_VALEUR', '#<18>#= ', V
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_MIN', '#<18>#= ', VMIN
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_MAX', '#<18>#= ', VMAX
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      CHKVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_CBS_INI_IPRGL
C
C Description:
C     La fonction privée SV2D_CBS_INI_IPRGL initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_INI_IPRGL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      SV2D_IPRGL_GRAVITE         =  1
      SV2D_IPRGL_LATITUDE        =  2
      SV2D_IPRGL_FCT_CW_VENT     = -1
      SV2D_IPRGL_VISCO_CST       =  3
      SV2D_IPRGL_VISCO_LM        =  4
      SV2D_IPRGL_VISCO_SMGO      =  5
      SV2D_IPRGL_VISCO_BINF      =  6
      SV2D_IPRGL_VISCO_BSUP      =  7
      SV2D_IPRGL_DECOU_HTRG      =  8
      SV2D_IPRGL_DECOU_HMIN      =  9
      SV2D_IPRGL_DECOU_PENA_H    = -1
      SV2D_IPRGL_DECOU_PENA_Q    = -1
      SV2D_IPRGL_DECOU_MAN       = 10
      SV2D_IPRGL_DECOU_UMAX      = 11
      SV2D_IPRGL_DECOU_PORO      = 12
      SV2D_IPRGL_DECOU_AMORT     = 13
      SV2D_IPRGL_DECOU_CON_FACT  = 14
      SV2D_IPRGL_DECOU_GRA_FACT  = 15
      SV2D_IPRGL_DECOU_DIF_NU    = 16
      SV2D_IPRGL_DECOU_DIF_PE    = -1
      SV2D_IPRGL_DECOU_DRC_NU    = 17
      SV2D_IPRGL_STABI_PECLET    = 18
      SV2D_IPRGL_STABI_AMORT     = 19
      SV2D_IPRGL_STABI_DARCY     = 20
      SV2D_IPRGL_STABI_LAPIDUS   = 21
      SV2D_IPRGL_CMULT_CON       = 22
      SV2D_IPRGL_CMULT_GRA       = 23
      SV2D_IPRGL_CMULT_PDYN      = -1
      SV2D_IPRGL_CMULT_MAN       = 24
      SV2D_IPRGL_CMULT_VENT      = 25
      SV2D_IPRGL_CMULT_INTGCTR   = 26
      SV2D_IPRGL_PNUMR_PENALITE  = 27
      SV2D_IPRGL_PNUMR_DELPRT    = 28
      SV2D_IPRGL_PNUMR_DELMIN    = -1
      SV2D_IPRGL_PNUMR_OMEGAKT   = 29
      SV2D_IPRGL_PNUMR_PRTPREL   = 30
      SV2D_IPRGL_PNUMR_PRTPRNO   = 31
      SV2D_IPRGL_CORIOLIS        = 32
      SV2D_IPRGL_CMULT_VENT_REL  = -1

      RETURN
      END
