C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_PRCPRGL
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PRCPRGL (VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  DELH
C-----------------------------------------------------------------------
      REAL*8  V
      LOGICAL R2L
      R2L(V) = (NINT(V) .NE. 0)
C-----------------------------------------------------------------------

C---     (Re)-Calcule Coriolis
      V = VPRGL(SV2D_IPRGL_LATITUDE)
      VPRGL(SV2D_IPRGL_CORIOLIS) = SP_HDRO_CORIOLIS(V)

C---     Affecte les valeurs aux propriétés globales
      SV2D_GRAVITE         = VPRGL(SV2D_IPRGL_GRAVITE)
      SV2D_FCT_CW_VENT     = SV2D_CW_WU1969
      SV2D_VISCO_CST       = VPRGL(SV2D_IPRGL_VISCO_CST)
      SV2D_VISCO_LM        = VPRGL(SV2D_IPRGL_VISCO_LM)
      SV2D_VISCO_SMGO      = VPRGL(SV2D_IPRGL_VISCO_SMGO)
      SV2D_VISCO_BINF      = VPRGL(SV2D_IPRGL_VISCO_BINF)
      SV2D_VISCO_BSUP      = VPRGL(SV2D_IPRGL_VISCO_BSUP)

      SV2D_DECOU_HTRG      = VPRGL(SV2D_IPRGL_DECOU_HTRG)
      SV2D_DECOU_HMIN      = VPRGL(SV2D_IPRGL_DECOU_HMIN)
      DELH                 = SV2D_DECOU_HTRG - SV2D_DECOU_HMIN
      SV2D_DECOU_DHST      = 0.2D+00 * DELH
      SV2D_DECOU_UN_HDIF   = UN / MAX(1.0D-32, DELH)
      SV2D_DECOU_PENA_H    = ZERO
      SV2D_DECOU_PENA_Q    = ZERO
      SV2D_DECOU_MAN       = VPRGL(SV2D_IPRGL_DECOU_MAN)
      SV2D_DECOU_UMAX      = VPRGL(SV2D_IPRGL_DECOU_UMAX)
      SV2D_DECOU_PORO      = VPRGL(SV2D_IPRGL_DECOU_PORO)
      SV2D_DECOU_AMORT     = VPRGL(SV2D_IPRGL_DECOU_AMORT)
      SV2D_DECOU_CON_FACT  = VPRGL(SV2D_IPRGL_DECOU_CON_FACT)
      SV2D_DECOU_GRA_FACT  = VPRGL(SV2D_IPRGL_DECOU_GRA_FACT)
      SV2D_DECOU_DIF_NU    = VPRGL(SV2D_IPRGL_DECOU_DIF_NU)
      SV2D_DECOU_DRC_NU    = VPRGL(SV2D_IPRGL_DECOU_DRC_NU)

      SV2D_STABI_PECLET    = VPRGL(SV2D_IPRGL_STABI_PECLET)
      SV2D_STABI_AMORT     = VPRGL(SV2D_IPRGL_STABI_AMORT)
      SV2D_STABI_DARCY     = VPRGL(SV2D_IPRGL_STABI_DARCY)
      SV2D_STABI_LAPIDUS   = VPRGL(SV2D_IPRGL_STABI_LAPIDUS)

      SV2D_CMULT_CON       = VPRGL(SV2D_IPRGL_CMULT_CON)
      SV2D_CMULT_GRA       = VPRGL(SV2D_IPRGL_CMULT_GRA)
      SV2D_CMULT_PDYN      = ZERO
      SV2D_CMULT_MAN       = VPRGL(SV2D_IPRGL_CMULT_MAN)
      SV2D_CMULT_VENT      = VPRGL(SV2D_IPRGL_CMULT_VENT)
      SV2D_CMULT_INTGCTRQX = VPRGL(SV2D_IPRGL_CMULT_INTGCTR)
      SV2D_CMULT_INTGCTRQY = VPRGL(SV2D_IPRGL_CMULT_INTGCTR)
      SV2D_CMULT_INTGCTRH  = VPRGL(SV2D_IPRGL_CMULT_INTGCTR)

      SV2D_PNUMR_PENALITE  = VPRGL(SV2D_IPRGL_PNUMR_PENALITE)
      SV2D_PNUMR_DELPRT    = VPRGL(SV2D_IPRGL_PNUMR_DELPRT)
      SV2D_PNUMR_DELMIN    = 1.0D-07
      SV2D_PNUMR_OMEGAKT   = VPRGL(SV2D_IPRGL_PNUMR_OMEGAKT)
      SV2D_PNUMR_PRTPREL   = R2L(VPRGL(SV2D_IPRGL_PNUMR_PRTPREL))
      SV2D_PNUMR_PRTPRNO   = R2L(VPRGL(SV2D_IPRGL_PNUMR_PRTPRNO))

      SV2D_DECOU_DIF_PE    = VPRGL(SV2D_IPRGL_STABI_PECLET) 
      SV2D_CMULT_VENT_REL  = ZERO
      SV2D_CORIOLIS        = VPRGL(SV2D_IPRGL_CORIOLIS)

      IERR = ERR_TYP()
      RETURN
      END
