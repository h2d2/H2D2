C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Block d'initialisation.
C
C Description:
C     Le block data privé <code>SV2D_CBS_DATA_000()</code> initialise le
C     nombre de modules à 0.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CBS_DATA_000

      INCLUDE 'sv2d_cbs.fc'

      DATA SV2D_VT_HVFT_ACTU  /0/
      DATA SV2D_CL_NMDL       /0/

      END
