C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Functions:
C   Public:
C     INTEGER SV2D_LGCY_000
C     INTEGER SV2D_LGCY_999
C     INTEGER SV2D_LGCY_CTR
C     INTEGER SV2D_LGCY_DTR
C     INTEGER SV2D_LGCY_INI
C     INTEGER SV2D_LGCY_RST
C     INTEGER SV2D_LGCY_REQHBASE
C     LOGICAL SV2D_LGCY_HVALIDE
C     INTEGER SV2D_LGCY_REQHPRN
C   Private:
C     SUBROUTINE SV2D_LGCY_REQSELF
C     SUBROUTINE SV2D_LGCY_REQGDTA
C     SUBROUTINE SV2D_LGCY_REQEDTA
C     SUBROUTINE SV2D_LGCY_REQXPRG
C     SUBROUTINE SV2D_LGCY_REQIPRG
C     SUBROUTINE SV2D_LGCY_REQIPRN
C     INTEGER SV2D_LGCY_INIVTBL
C     INTEGER SV2D_LGCY_RAZ
C
C************************************************************************

      MODULE SV2D_LGCY_M

      USE LM_LGCY_M, ONLY: LM_LGCY_T
      USE SV2D_IPRG_M
      USE SV2D_IPRN_M
      USE SV2D_XPRG_M
      IMPLICIT NONE

      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: SV2D_LGCY_HBASE = 0
      TYPE(SV2D_XPRG_T), SAVE, TARGET :: SV2D_XPRG
      TYPE(SV2D_IPRG_T), SAVE, TARGET :: SV2D_IPRG
      TYPE(SV2D_IPRN_T), SAVE, TARGET :: SV2D_IPRN

C---     Attributs privés
      TYPE, EXTENDS(LM_LGCY_T) :: SV2D_LGCY_T
         INTEGER, PUBLIC :: HVTBL

      END TYPE SV2D_LGCY_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée SV2D_LGCY_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La méthode est virtuelle dans le sens ou elle fonctionne
C     pour un objet qui a hérité de LM_ELEM. Par contre, on ne
C     peut pas contrôler le type.
C************************************************************************
      FUNCTION SV2D_LGCY_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (SV2D_LGCY_T), POINTER :: SV2D_LGCY_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (SV2D_LGCY_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      SV2D_LGCY_REQSELF => SELF
      RETURN
      END FUNCTION SV2D_LGCY_REQSELF

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs de l'élément géométrique.
C
C Description:
C     La fonction privée SV2D_LGCY_REQGDTA(...) retourne le pointeur à
C     la structure de données de l'élément géométrique associé à l'objet
C     courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La méthode est virtuelle dans le sens ou elle fonctionne
C     pour un objet qui a hérité de LM_ELEM. Par contre, on ne
C     peut pas contrôler le type.
C************************************************************************
      FUNCTION SV2D_LGCY_REQGDTA(HOBJ)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      IMPLICIT NONE

      TYPE (LM_GDTA_T), POINTER :: SV2D_LGCY_REQGDTA
      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'

      TYPE (SV2D_LGCY_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SV2D_LGCY_REQSELF(HOBJ)
      SV2D_LGCY_REQGDTA => SELF%GDTA
      RETURN
      END FUNCTION SV2D_LGCY_REQGDTA

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée SV2D_LGCY_REQEDTA(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La méthode n'est pas virtuelle, ni héritable. Elle ne fonctionne
C     que pour des SV2D_CBS.
C************************************************************************
      FUNCTION SV2D_LGCY_REQEDTA(HOBJ)

      USE LM_ELEM_M
      IMPLICIT NONE

      TYPE (LM_EDTA_T), POINTER :: SV2D_LGCY_REQEDTA
      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'

      TYPE (SV2D_LGCY_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SV2D_LGCY_REQSELF(HOBJ)
      SV2D_LGCY_REQEDTA => SELF%EDTA
      RETURN
      END FUNCTION SV2D_LGCY_REQEDTA

C************************************************************************
C Sommaire:    Retourne le pointeur aux propriétés globales.
C
C Description:
C     La fonction privée SV2D_LGCY_REQPRGL(...) retourne le pointeur à
C     la structure des propriétés globales sous forme nominative.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_REQXPRG(HOBJ)

      IMPLICIT NONE

      TYPE (SV2D_XPRG_T), POINTER :: SV2D_LGCY_REQXPRG
      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'

      TYPE (SV2D_LGCY_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SV2D_LGCY_REQXPRG => SV2D_XPRG
      RETURN
      END FUNCTION SV2D_LGCY_REQXPRG

C************************************************************************
C Sommaire:    Retourne le pointeur aux indices de propriétés globales.
C
C Description:
C     La fonction privée SV2D_LGCY_REQIPRG(...) retourne le pointeur à
C     la structure des indice des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_REQIPRG(HOBJ)

      IMPLICIT NONE

      TYPE (SV2D_IPRG_T), POINTER :: SV2D_LGCY_REQIPRG
      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'

      TYPE (SV2D_LGCY_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => SV2D_LGCY_REQSELF(HOBJ)
      SV2D_LGCY_REQIPRG => SV2D_IPRG
      RETURN
      END FUNCTION SV2D_LGCY_REQIPRG

C************************************************************************
C Sommaire:    Retourne le pointeur aux indices de propriétés nodales.
C
C Description:
C     La fonction privée SV2D_LGCY_REQIPRN(...) retourne le pointeur à
C     la structure des indice des propriétés nodales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_REQIPRN(HOBJ)

      IMPLICIT NONE

      TYPE (SV2D_IPRN_T), POINTER :: SV2D_LGCY_REQIPRN
      INTEGER HOBJ

      TYPE (SV2D_LGCY_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => SV2D_LGCY_REQSELF(HOBJ)
      SV2D_LGCY_REQIPRN => SV2D_IPRN
      RETURN
      END FUNCTION SV2D_LGCY_REQIPRN

      END MODULE SV2D_LGCY_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_000
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(SV2D_LGCY_HBASE,
     &                   'Finite Element - SV2D Legacy element')

      SV2D_LGCY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_999
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL SV2D_LGCY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(SV2D_LGCY_HBASE, SV2D_LGCY_DTR)

      SV2D_LGCY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_CTR(HOBJ, FORML, ITPGEO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_CTR
CDEC$ ENDIF

      USE SV2D_LGCY_M
      USE LM_LGCY_M, ONLY: LM_LGCY_CTR
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER,       INTENT(OUT) :: HOBJ
      CHARACTER*(*), INTENT(IN)  :: FORML
      INTEGER,       INTENT(IN)  :: ITPGEO

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_lgcy.fc'

      INTEGER IERR, IRET
      INTEGER HELEM
      TYPE (LM_LGCY_T),   POINTER :: OPRNT
      TYPE (SV2D_LGCY_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FORML) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   SV2D_LGCY_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = SV2D_LGCY_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. SV2D_LGCY_HVALIDE(HOBJ))

C---     Construis le parent
      HELEM = 0
      IF (ERR_GOOD()) OPRNT => LM_LGCY_CTR(FORML, ITPGEO)

C---     Remplis la table virtuelle
      IF (ERR_GOOD()) IERR = SV2D_LGCY_INIVTBL(HOBJ)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SV2D_LGCY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_DTR
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE
      
      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'obvtbl.fi'

      INTEGER  IERR
      INTEGER  HELEM
      TYPE (SV2D_LGCY_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

!!      IERR = ERR_OK
!!      HELEM = 0
!!      SELF => NULL()
!!
!!C---     Récupère les attributs
!!      SELF => SV2D_LGCY_REQSELF(HOBJ)
!!      IF (OB_VTBL_HVALIDE(SELF%HVTBL)) THEN
!!         HELEM = LM_UTIL_REQxxx(HOBJ, IX_ELE)
!!      ENDIF
!!
!!C---     Reset les données
!!      IERR = SV2D_LGCY_RST(HOBJ)
!!
!!C---     Détruis le parent
!!      IF (LM_LGCY_HVALIDE(HELEM)) IERR = LM_LGCY_DTR(HELEM)
!!
!!C---     Efface du registre
!!      IERR = OB_OBJN_DTR(HOBJ, SV2D_LGCY_HBASE)
!!      HOBJ = 0
!!
!!C---     Désalloue la structure
!!      IF (ASSOCIATED(SELF)) THEN
!!         DEALLOCATE(SELF)
!!      ENDIF
      
      SV2D_LGCY_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_LGCY_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_INI (HOBJ,
     &                        HGRID,
     &                        HDLIB,
     &                        HCLIM,
     &                        HSOLC,
     &                        HSOLR,
     &                        HPRGL,
     &                        HPRNO,
     &                        HPREV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREV

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'lmlgcy.fi'

      INTEGER IERR
      INTEGER HELEM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SV2D_LGCY_RST(HOBJ)

C---     Récupère les attributs
      HELEM = 0
      IF (ERR_GOOD()) THEN
         HELEM = LM_UTIL_REQxxx(HOBJ, IX_ELE)
D        CALL ERR_ASR(LM_LGCY_HVALIDE (HELEM))
      ENDIF

C---     Initialise les parents
      IF (ERR_GOOD()) IERR = LM_LGCY_INI (HELEM,
     &                                    HGRID,
     &                                    HDLIB,
     &                                    HCLIM,
     &                                    HSOLC,
     &                                    HSOLR,
     &                                    HPRGL,
     &                                    HPRNO,
     &                                    HPREV)

      SV2D_LGCY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_LGCY_INIVTBL
C
C Description:
C     La fonction privée SV2D_LGCY_INIVTBL initialise la table virtuelle pour
C     la classe.
C
C Entrée:
C     INTEGER HT        Handle sur la table virtuelle
C
C Sortie:
C
C Notes:
C   La méthode pourrait être statique
C************************************************************************
      FUNCTION SV2D_LGCY_INIVTBL(HOBJ)

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_lgcy.fc'

      INTEGER I
      INTEGER HT, HO
      TYPE (SV2D_LGCY_T), POINTER :: SELF

      CHARACTER*(4), PARAMETER :: DLL = 'sv2d'
C-----------------------------------------------------------------------
      INTEGER AJT
      INTEGER ID
      CHARACTER*(18) NF
      AJT(ID,NF)=OB_VTBL_AJTMSO(HT, ID, HO, DLL, NF(1:SP_STRN_LEN(NF)))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Les attributs
      SELF => SV2D_LGCY_REQSELF(HOBJ)
      HT = SELF%HVTBL
      HO = HOBJ
D     CALL ERR_ASR(OB_VTBL_HVALIDE(HT))

C---     Méthodes virtuelles de gestion
      IF (ERR_GOOD()) I = AJT(LM_VTBL_FNC_REQHVALD,'SV2D_LGCY_HVALIDE ')

      SV2D_LGCY_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_RST
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'sv2d_lgcy.fc'

      INTEGER IERR
      INTEGER HELEM
      TYPE (SV2D_LGCY_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      HELEM = 0

C---     Récupère les attributs
      SELF => SV2D_LGCY_REQSELF(HOBJ)
      IF (OB_VTBL_HVALIDE(SELF%HVTBL)) THEN
         HELEM = LM_UTIL_REQxxx(HOBJ, IX_ELE)
D        CALL ERR_ASR(LM_ELEM_HVALIDE(HELEM))
      ENDIF

C---     Reset le parent
      IF (LM_ELEM_HVALIDE(HELEM)) IERR = LM_ELEM_RST(HELEM)

C---     Remise à zero
      IERR = SV2D_LGCY_RAZ(HOBJ)

      SV2D_LGCY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_RAZ(HOBJ)

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_lgcy.fc'

      TYPE (SV2D_LGCY_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SV2D_LGCY_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SV2D_LGCY_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_REQHBASE
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy.fi'
C------------------------------------------------------------------------

      SV2D_LGCY_REQHBASE = SV2D_LGCY_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SV2D_LGCY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_HVALIDE
CDEC$ ENDIF

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      SV2D_LGCY_HVALIDE = OB_OBJN_HVALIDE(HOBJ, SV2D_LGCY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le parent.
C
C Description:
C     La fonction SV2D_LGCY_REQHPRN retourne le handle sur le parent,
C     stocké dans la table virtuelle. En fait on remonte jusqu'au
C     grand-parent, le LM_ELEM de LM_LGCY.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_REQHPRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_LGCY_REQHPRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmutil.fi'
      INCLUDE 'lmlgcy.fi'

      INTEGER HELEM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      HELEM = LM_UTIL_REQxxx(HOBJ, IX_ELE)
D     CALL ERR_PRE(LM_LGCY_HVALIDE(HOBJ))
      SV2D_LGCY_REQHPRN = LM_UTIL_REQxxx(HELEM, IX_ELE)
      RETURN
      END

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SV2D_LGCY_CMN2CLS copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_LGCY_CMN2CLS()

      USE SV2D_LGCY_M
      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

C---     Assigne les indices des propriétés globales
      SV2D_IPRG%GRAVITE         = SV2D_IPRGL_GRAVITE
      SV2D_IPRG%LATITUDE        = SV2D_IPRGL_LATITUDE
      SV2D_IPRG%FCT_CW_VENT     = SV2D_IPRGL_FCT_CW_VENT
      SV2D_IPRG%VISCO_CST       = SV2D_IPRGL_VISCO_CST
      SV2D_IPRG%VISCO_LM        = SV2D_IPRGL_VISCO_LM
      SV2D_IPRG%VISCO_SMGO      = SV2D_IPRGL_VISCO_SMGO
      SV2D_IPRG%VISCO_BINF      = SV2D_IPRGL_VISCO_BINF
      SV2D_IPRG%VISCO_BSUP      = SV2D_IPRGL_VISCO_BSUP
      SV2D_IPRG%DECOU_HTRG      = SV2D_IPRGL_DECOU_HTRG
      SV2D_IPRG%DECOU_HMIN      = SV2D_IPRGL_DECOU_HMIN
      SV2D_IPRG%DECOU_PENA_H    = SV2D_IPRGL_DECOU_PENA_H
      SV2D_IPRG%DECOU_PENA_Q    = SV2D_IPRGL_DECOU_PENA_Q
      SV2D_IPRG%DECOU_MAN       = SV2D_IPRGL_DECOU_MAN
      SV2D_IPRG%DECOU_UMAX      = SV2D_IPRGL_DECOU_UMAX
      SV2D_IPRG%DECOU_PORO      = SV2D_IPRGL_DECOU_PORO
      SV2D_IPRG%DECOU_AMORT     = SV2D_IPRGL_DECOU_AMORT
      SV2D_IPRG%DECOU_CON_FACT  = SV2D_IPRGL_DECOU_CON_FACT
      SV2D_IPRG%DECOU_GRA_FACT  = SV2D_IPRGL_DECOU_GRA_FACT
      SV2D_IPRG%DECOU_DIF_NU    = SV2D_IPRGL_DECOU_DIF_NU
      SV2D_IPRG%DECOU_DIF_PE    = SV2D_IPRGL_DECOU_DIF_PE
      SV2D_IPRG%DECOU_DRC_NU    = SV2D_IPRGL_DECOU_DRC_NU
      SV2D_IPRG%STABI_PECLET    = SV2D_IPRGL_STABI_PECLET
      SV2D_IPRG%STABI_AMORT     = SV2D_IPRGL_STABI_AMORT
      SV2D_IPRG%STABI_DARCY     = SV2D_IPRGL_STABI_DARCY
      SV2D_IPRG%STABI_LAPIDUS   = SV2D_IPRGL_STABI_LAPIDUS
      SV2D_IPRG%CMULT_CON       = SV2D_IPRGL_CMULT_CON
      SV2D_IPRG%CMULT_GRA       = SV2D_IPRGL_CMULT_GRA
      SV2D_IPRG%CMULT_PDYN      = SV2D_IPRGL_CMULT_PDYN
      SV2D_IPRG%CMULT_MAN       = SV2D_IPRGL_CMULT_MAN
      SV2D_IPRG%CMULT_VENT      = SV2D_IPRGL_CMULT_VENT
      SV2D_IPRG%CMULT_INTGCTR   = SV2D_IPRGL_CMULT_INTGCTR
      SV2D_IPRG%PNUMR_PENALITE  = SV2D_IPRGL_PNUMR_PENALITE
      SV2D_IPRG%PNUMR_DELPRT    = SV2D_IPRGL_PNUMR_DELPRT
      SV2D_IPRG%PNUMR_DELMIN    = SV2D_IPRGL_PNUMR_DELMIN
      SV2D_IPRG%PNUMR_OMEGAKT   = SV2D_IPRGL_PNUMR_OMEGAKT
      SV2D_IPRG%PNUMR_PRTPREL   = SV2D_IPRGL_PNUMR_PRTPREL
      SV2D_IPRG%PNUMR_PRTPRNO   = SV2D_IPRGL_PNUMR_PRTPRNO
      
C---     Assigne les indices des propriétés nodales
      SV2D_IPRN%Z               = SV2D_IPRNO_Z
      SV2D_IPRN%N               = SV2D_IPRNO_N
      SV2D_IPRN%ICE_E           = SV2D_IPRNO_ICE_E
      SV2D_IPRN%ICE_N           = SV2D_IPRNO_ICE_N
      SV2D_IPRN%WND_X           = SV2D_IPRNO_WND_X
      SV2D_IPRN%WND_Y           = SV2D_IPRNO_WND_Y
      SV2D_IPRN%U               = SV2D_IPRNO_U
      SV2D_IPRN%V               = SV2D_IPRNO_V
      SV2D_IPRN%H               = SV2D_IPRNO_H
      SV2D_IPRN%COEFF_CNVT      = SV2D_IPRNO_COEFF_CNVT
      SV2D_IPRN%COEFF_GRVT      = SV2D_IPRNO_COEFF_GRVT
      SV2D_IPRN%COEFF_FROT      = SV2D_IPRNO_COEFF_FROT
      SV2D_IPRN%COEFF_DIFF      = SV2D_IPRNO_COEFF_DIFF
      SV2D_IPRN%COEFF_PE        = SV2D_IPRNO_COEFF_PE
      SV2D_IPRN%COEFF_DMPG      = SV2D_IPRNO_COEFF_DMPG
      SV2D_IPRN%COEFF_VENT      = SV2D_IPRNO_COEFF_VENT
      SV2D_IPRN%COEFF_DRCY      = SV2D_IPRNO_COEFF_DRCY
      SV2D_IPRN%COEFF_PORO      = SV2D_IPRNO_COEFF_PORO
      SV2D_IPRN%DECOU_PENA      = SV2D_IPRNO_DECOU_PENA
!      SV2D_IPREV_VTRB            = SV2D_IPREV_VTRB
!      SV2D_IPREV_VTOT            = SV2D_IPREV_VTOT
!      SV2D_IPREV_STTS            = SV2D_IPREV_STTS
      
C---     Affecte les valeurs aux propriétés globales
      SV2D_XPRG%GRAVITE         = SV2D_GRAVITE
      SV2D_XPRG%FCT_CW_VENT     = SV2D_FCT_CW_VENT
      SV2D_XPRG%VISCO_CST       = SV2D_VISCO_CST
      SV2D_XPRG%VISCO_LM        = SV2D_VISCO_LM
      SV2D_XPRG%VISCO_SMGO      = SV2D_VISCO_SMGO
      SV2D_XPRG%VISCO_BINF      = SV2D_VISCO_BINF
      SV2D_XPRG%VISCO_BSUP      = SV2D_VISCO_BSUP

      SV2D_XPRG%DECOU_HTRG      = SV2D_DECOU_HTRG
      SV2D_XPRG%DECOU_HMIN      = SV2D_DECOU_HMIN
      SV2D_XPRG%DECOU_DHST      = SV2D_DECOU_DHST
      SV2D_XPRG%DECOU_UN_HDIF   = SV2D_DECOU_UN_HDIF
      SV2D_XPRG%DECOU_PENA_H    = SV2D_DECOU_PENA_H
      SV2D_XPRG%DECOU_PENA_Q    = SV2D_DECOU_PENA_Q
      SV2D_XPRG%DECOU_MAN       = SV2D_DECOU_MAN
      SV2D_XPRG%DECOU_UMAX      = SV2D_DECOU_UMAX
      SV2D_XPRG%DECOU_PORO      = SV2D_DECOU_PORO
      SV2D_XPRG%DECOU_AMORT     = SV2D_DECOU_AMORT
      SV2D_XPRG%DECOU_CON_FACT  = SV2D_DECOU_CON_FACT
      SV2D_XPRG%DECOU_GRA_FACT  = SV2D_DECOU_GRA_FACT
      SV2D_XPRG%DECOU_DIF_NU    = SV2D_DECOU_DIF_NU
      SV2D_XPRG%DECOU_DIF_PE    = SV2D_DECOU_DIF_PE
      SV2D_XPRG%DECOU_DRC_NU    = SV2D_DECOU_DRC_NU

      SV2D_XPRG%STABI_PECLET    = SV2D_STABI_PECLET
      SV2D_XPRG%STABI_AMORT     = SV2D_STABI_AMORT
      SV2D_XPRG%STABI_DARCY     = SV2D_STABI_DARCY
      SV2D_XPRG%STABI_LAPIDUS   = SV2D_STABI_LAPIDUS

      SV2D_XPRG%CMULT_CON       = SV2D_CMULT_CON
      SV2D_XPRG%CMULT_GRA       = SV2D_CMULT_GRA
      SV2D_XPRG%CMULT_PDYN      = SV2D_CMULT_PDYN
      SV2D_XPRG%CMULT_MAN       = SV2D_CMULT_MAN
      SV2D_XPRG%CMULT_VENT      = SV2D_CMULT_VENT
      SV2D_XPRG%CMULT_INTGCTRQX = SV2D_CMULT_INTGCTRQX
      SV2D_XPRG%CMULT_INTGCTRQY = SV2D_CMULT_INTGCTRQY
      SV2D_XPRG%CMULT_INTGCTRH  = SV2D_CMULT_INTGCTRH

      SV2D_XPRG%PNUMR_PENALITE  = SV2D_PNUMR_PENALITE
      SV2D_XPRG%PNUMR_DELPRT    = SV2D_PNUMR_DELPRT
      SV2D_XPRG%PNUMR_DELMIN    = SV2D_PNUMR_DELMIN
      SV2D_XPRG%PNUMR_OMEGAKT   = SV2D_PNUMR_OMEGAKT
      SV2D_XPRG%PNUMR_PRTPREL   = SV2D_PNUMR_PRTPREL
      SV2D_XPRG%PNUMR_PRTPRNO   = SV2D_PNUMR_PRTPRNO

      SV2D_XPRG%CMULT_VENT_REL  = SV2D_CMULT_VENT_REL
      SV2D_XPRG%CORIOLIS        = SV2D_CORIOLIS
      
      SV2D_LGCY_CMN2CLS = ERR_TYP()
      RETURN
      END
