C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_DMPPROP
C   Private:
C     SUBROUTINE SV2D_CBS_DMPPRGL
C     SUBROUTINE SV2D_CBS_DMPPRNO
C     INTEGER SV2D_CBS_DMP1
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_DMPPROP
C
C Description:
C     IMPRESSION DE PROPRIETES
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_DMPPROP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_DMPPROP
CDEC$ ENDIF

      IMPLICIT NONE
C-----------------------------------------------------------------------

      CALL SV2D_CBS_DMPPRGL()
      CALL SV2D_CBS_DMPPRNO()

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_DMPPRGL
C
C Description:
C     IMPRESSION DE PROPRIETES
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_DMPPRGL()

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
      INTEGER IERR
      INTEGER SV2D_CBS_DMP1
C-----------------------------------------------------------------------

      I = 0

C---     IMPRIME ENTETE
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     GROUPE DES PROP PHYSIQUES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_GRAVITE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_LATITUDE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_FCT_CW_VENT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_VISCOSITE_MOLECULAIRE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_LONGUEUR_MELANGE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_SMAGORINSKY')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_BORNE_INF_VISCOSITE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_BORNE_SUP_VISCOSITE')

C---     GROUPE DES PROP DU DECOUVREMENT
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_PROF_TRIG')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_PROF_MIN')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_MAN')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_UMAX')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_PORO')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_AMORT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_COEF_CONVECTION')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_COEF_GRAVITE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_NU_DIFFUSION')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_DECOUVREMENT_NU_DARCY')

C---     GROUPE DES PROP DE STABILISATION
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_PECLET')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_AMORTISSEMENT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_SURFACE_LIBRE_DARCY')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_SURFACE_LIBRE_LAPIDUS')

C---     GROUPE DES PROP D'ACTIVATION-DESACTIVATION DES TERMES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_CONVECTION')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_GRAVITE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_FROTTEMENT_FOND')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_FROTTEMENT_VENT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_INTGRL_CONTOUR')

C---     GROUPE DES PROP NUMÉRIQUES
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_PENALITE')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_PERTURBATION_KT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_RELAXATION_KT')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_PERTURB_PREL')
      I = I + 1
      IERR=SV2D_CBS_DMP1(I,'MSG_COEF_PERTURB_PRNO')

      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  COEF. COMP. D'AMORTISSEMENT
C                    DS    16)  POROSITÉ
C
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_DMPPRNO ()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, IERR
      INTEGER SV2D_CBS_DMP1
C-----------------------------------------------------------------------

      I = 0

C---     IMPRIME ENTETE
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     PRNO LUES
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_ZF')
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_COEF_MANNING')
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_GLACE_EPAISSEUR')
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_GLACE_COEF_MANNING')
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_VENT_X')
      I = I + 1
      IERR = SV2D_CBS_DMP1(I,'MSG_VENT_Y')

      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_CBS_DMP1 imprime une seule
C     propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SV2D_CBS_DMP1(IP, TXT)

      IMPLICIT NONE

      INTEGER       SV2D_CBS_DMP1
      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_CBS_DMP1 = ERR_TYP()
      RETURN
      END
