C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_PSLDLIB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CBS_PSLDLIB
C
C Description:
C     La fonction SV2D_CBS_PSLDLIB
C        Post-Lecture sur les Degrés de LIBerté
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_PSLDLIB(KNGV,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PSLDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'

      INTEGER IC, IE
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      REAL*8  V1, V3, V5
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(V1, V3, V5)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)

C---     Assigne les noeuds milieux en niveau d'eau
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         V1 = VDLG(3, NO1)
         V3 = VDLG(3, NO3)
         V5 = VDLG(3, NO5)
         VDLG(3, NO2) = UN_2*(V1 + V3)
         VDLG(3, NO4) = UN_2*(V3 + V5)
         VDLG(3, NO6) = UN_2*(V5 + V1)

      ENDDO
!$omp end do
      ENDDO

!$omp end parallel

      IERR = ERR_TYP()
      RETURN
      END
