C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Saint-Venant 2D
C Objet:   Élément LEGACY
C Type:    ---
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_LGCY_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire cet élément via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_LGCY_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_LGCY_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_lgcy_ic.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      IPRM = ' '

      IC_SV2D_LGCY_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_LGCY_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type SV2D_Y4_CNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_LGCY_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_LGCY_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_lgcy_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'sv2d_lgcy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_LGCY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_LGCY_DTR(HOBJ)

      ELSE
C        <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_SV2D_LGCY_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_LGCY_OPBDOT(...) exécute les méthodes statiques
C     sur la classe SV2D_Y4_CNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_LGCY_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_LGCY_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_lgcy_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem_ic.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <include>IC_LM_ELEM_OPBDOT@lmelem_ic.for</include>
      IERR = IC_LM_ELEM_OPBDOT(HOBJ, IMTH, IPRM)

      IC_SV2D_LGCY_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_LGCY_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_LGCY_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy_ic.fi'
C-------------------------------------------------------------------------

      IC_SV2D_LGCY_REQCLS = '#__dummy_placeholder__#__SV2D_LGCY__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_LGCY_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_LGCY_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_lgcy_ic.fi'
      INCLUDE 'sv2d_lgcy.fi'
C-------------------------------------------------------------------------

      IC_SV2D_LGCY_REQHDL = SV2D_LGCY_REQHBASE()
      RETURN
      END
