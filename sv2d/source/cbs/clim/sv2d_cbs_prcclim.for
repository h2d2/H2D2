C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PRCCLIM
C
C Description:
C     Traitement PRé-Calcul des Conditions LIMites
C
C Entrée:
C     KNGV(EG_CMMN_NCELV=NNELV=6, EG_CMMN_NELV),
C     KNGS(EG_CMMN_NCELS, EG_CMMN_NELS),
C     VDJV(EG_CMMN_NDJV,  EG_CMMN_NELV),
C     VDJS(EG_CMMN_NDJS,  EG_CMMN_NELS),
C     VPRGL(LM_CMMN_NPRGL),
C     VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL),
C     VPREV(LM_CMMN_NPREV, EG_CMMN_NELV),
C     VPRES(LM_CMMN_NPRES, EG_CMMN_NELS),
C     KCLCND( 4, LM_CMMN_NCLCND),
C     VCLCNV(    LM_CMMN_NCLCNV),
C     KCLLIM( 7, EG_CMMN_NCLLIM),
C     KCLNOD(    EG_CMMN_NCLNOD),
C     KCLELE(    EG_CMMN_NCLELE),
C
C Sortie:
C     IERR
C     KDIMP     Pour chaque ddl, contient les code de CL
C     VDIMP     Pour chaque ddl, contient la valeur imposée par la cl
C     KEIMP     Pour chaque ELS, contient les codes des CL
C
C Notes:
C     Le test n'est pas bon. Les noeuds milieux en H sont flagués et donc
C     H sera toujours considéré comme ayant au moins un noeud imposé.
C************************************************************************
      SUBROUTINE SV2D_CBS_PRCCLIM(KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRCCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER SV2D_CBS_PRCCHK
      INTEGER IC, IE, ID, IN
      INTEGER NEQ, NCLT, NCLTI
C-----------------------------------------------------------------------

C---- INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:'
C      WRITE(MP,1001)'CODE C.L.=1 CONDITION DE DIRICHLET;'
C      WRITE(MP,1001)'CODE C.L.=2 CONDITION DE CAUCHY;'
C      WRITE(MP,1001)'CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.'
C1000  FORMAT(/)
C1001  FORMAT(15X,A)

C---     Initialise
      NEQ  = LM_CMMN_NDLL
      NCLT = 0

C---     Reset les C.L.
      CALL IINIT(LM_CMMN_NDLL,    0, KDIMP, 1)
      CALL IINIT(EG_CMMN_NELS,    0, KEIMP, 1)
      CALL DINIT(LM_CMMN_NDLL, ZERO, VDIMP, 1)

!$omp  parallel
!$omp& default(shared)
!$omp& private(IE, IN)

C---     Flag les DDL de H sur les noeuds milieux
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         IN = KNGV(2,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
         IN = KNGV(4,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
         IN = KNGV(6,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
      ENDDO
!$omp end do
      ENDDO
      
!$omp end parallel

C---     Assigne les codes de condition
      IERR = SV2D_CL_COD(KCLCND,
     &                   VCLCNV,
     &                   KCLLIM,
     &                   KCLNOD,
     &                   KCLELE,
     &                   KDIMP)

C---     Contrôle les C.L. pour chaque type de DDL
C---     S'assure que chaque DDL a au moins une CL (cf. note)
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         DO ID=1,LM_CMMN_NDLN
            NCLTI = 0
            DO IN=1,EG_CMMN_NNL
               IF (KDIMP(ID,IN) .NE. 0) NCLTI = NCLTI + 1
            ENDDO
            IF (NCLTI .EQ. 0) THEN
               WRITE(LOG_BUF,'(2A,I12)') 'MSG_DDL_SANS_CLIM',': ',ID
               CALL LOG_ECRIS(LOG_BUF)
            ENDIF
         ENDDO
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CBS_PRCCHK(KNGV,
     &                          KNGS,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP)
      ENDIF

C---     Compte le nombre de C.L.
      NCLT = 0
      DO IN = 1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IF (BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)) THEN
               NCLT = NCLT + 1
            ENDIF
         ENDDO
      ENDDO

C---     Met à jour NEQ
      NEQ = NEQ - NCLT

C---     Contrôle NEQ
      IF (NEQ. LE. 0) GOTO 9900

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_PRC(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      KCLCND,
     &                      VCLCNV,
     &                      KCLLIM,
     &                      KCLNOD,
     &                      KCLELE,
     &                      VCLDST,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP)
      ENDIF


      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_CLIM_NBR_EQUATIONS',': ',NEQ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IERR = ERR_TYP()
      RETURN
      END

      
C************************************************************************
C Sommaire:  SV2D_CBS_PRCCHK
C
C Description:
C     La fonction SV2D_CBS_PRCCHK fait l'étape de pré-calcul pour les
C     conditions limites.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CBS_PRCCHK(KNGV,
     &                         KNGS,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SV2D_CBS_PRCCHK
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IEDEB, IEFIN, IE, IES
      INTEGER KMASK
      INTEGER NO1, NO3, KV
      LOGICAL HDRDONE
      LOGICAL ESTIMP_X, ESTIMP_Y, ESTIMP_H
C-----------------------------------------------------------------------
      LOGICAL ESTIMP
      INTEGER ID, IN
      ESTIMP(ID,IN) = (IAND(KDIMP(ID,IN), KMASK) .NE. 0)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.clim'

C---     Codes de CL à tester
      KMASK = 0
      DO IN=EA_TPCL_NOOP+1, EA_TPCL_SEC-1
         KMASK = IBSET(KMASK, IN)
      ENDDO

C---     Boucle sur les éléments
      HDRDONE = .FALSE.
      DO IL=1, EG_CMMN_NCLLIM
         IEDEB = KCLLIM(5, IL)
         IEFIN = KCLLIM(6, IL)
      
         DO IE=IEDEB,IEFIN
            IES = KCLELE(IE)
            NO1 = KNGS(1,IES)
            NO3 = KNGS(3,IES)

            ESTIMP_X = (ESTIMP(1,NO1) .AND. ESTIMP(1,NO3)) 
            ESTIMP_Y = (ESTIMP(2,NO1) .AND. ESTIMP(2,NO3)) 
            ESTIMP_H = (ESTIMP(3,NO1) .AND. ESTIMP(3,NO3)) 
            
            ! ---  Diagnostic
            IF (ESTIMP_H .AND. (ESTIMP_X .OR. ESTIMP_Y)) THEN
               IF (.NOT. HDRDONE) THEN
                  CALL LOG_WRN(LOG_ZNE, ' ')
                  WRITE(LOG_BUF,'(2A)') 'MSG_ELEMENT_SURIMPOSE', ':'
                  CALL LOG_WRN(LOG_ZNE, LOG_BUF)
                  CALL LOG_INCIND()
                  HDRDONE = .TRUE.
               ENDIF
               WRITE(LOG_BUF,'(A,2(I12,A))')
     &            'MSG_NOEUDS: (', NO1, ', ', NO3, ')'
               CALL LOG_WRN(LOG_ZNE, LOG_BUF)
            ENDIF
            
         ENDDO
      ENDDO
      IF (HDRDONE) CALL LOG_DECIND()

      SV2D_CBS_PRCCHK = ERR_TYP()
      RETURN
      END

      