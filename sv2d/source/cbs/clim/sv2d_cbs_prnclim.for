C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_CBS_PRNCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CBS_PRNCLIM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_PRNCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      CALL LOG_INCIND()

      CALL LOG_ECRIS('DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:')
      CALL LOG_ECRIS('CODE C.L.=1 CONDITION DE DIRICHLET;')
      CALL LOG_ECRIS('CODE C.L.=2 CONDITION DE CAUCHY;')
      CALL LOG_ECRIS('CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.')

      CALL LOG_DECIND()

      RETURN
      END
