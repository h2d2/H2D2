C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     SUBROUTINE SV2D_CMP_KE_V_CNT
C     SUBROUTINE SV2D_CMP_KE_V_GRV
C     SUBROUTINE SV2D_CMP_KE_V_CNV
C     SUBROUTINE SV2D_CMP_KE_V_COR
C     SUBROUTINE SV2D_CMP_KE_V_MAN
C     SUBROUTINE SV2D_CMP_KE_V_DIF
C     SUBROUTINE SV2D_CMP_KE_S_DIF
C     SUBROUTINE SV2D_CMP_KE_S_CNT
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_CNT
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_CNT assemble dans la
C     matrice Ke la contribution de volume de l'équation de
C     continuité. La divergence du débit est intégrée par partie.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     Les coefficients sont pour les métriques du T6
C        - <NT6,x> {NT3}      ! continuité
C        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
C
C     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
C     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
C     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
C     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
C     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
C
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_CNT(VKE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VKE  (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO,EG_CMMN_NNELV)
      REAL*8  VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ
      REAL*8  CHU11, CHU21, CHU31, CHU12, CHU22, CHU32
      REAL*8  CHV11, CHV21, CHV31, CHV12, CHV22, CHV32
      REAL*8  H1, H3, H5
      REAL*8  U1, U2, U3, U4, U5, U6
      REAL*8  V1, V2, V3, V4, V5, V6
      REAL*8  DHDX, DHDY, DHNRM
      REAL*8  C1, C3, C5, CM, C
      REAL*8  RH1, RH2, RH3, RHNRM

      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER IKU1, IKV1, IKH1
      INTEGER IKU2, IKV2
      INTEGER IKU3, IKV3, IKH2
      INTEGER IKU4, IKV4
      INTEGER IKU5, IKV5, IKH3
      INTEGER IKU6, IKV6

C---     CONNECTIVITÉS DU T6 EXTERNE
      PARAMETER(NO1 = 1)
      PARAMETER(NO2 = 2)
      PARAMETER(NO3 = 3)
      PARAMETER(NO4 = 4)
      PARAMETER(NO5 = 5)
      PARAMETER(NO6 = 6)

C---     INDICE DE DDL
      PARAMETER(IKU1 =  1)
      PARAMETER(IKV1 =  2)
      PARAMETER(IKH1 =  3)
      PARAMETER(IKU2 =  4)
      PARAMETER(IKV2 =  5)   ! on n'assemble pas IKH2
      PARAMETER(IKU3 =  7)
      PARAMETER(IKV3 =  8)
      PARAMETER(IKH2 =  9)
      PARAMETER(IKU4 = 10)
      PARAMETER(IKV4 = 11)   ! on n'assemble pas IKH4
      PARAMETER(IKU5 = 13)
      PARAMETER(IKV5 = 14)
      PARAMETER(IKH3 = 15)
      PARAMETER(IKU6 = 16)
      PARAMETER(IKV6 = 17)   ! on n'assemble pas IKH6
C-----------------------------------------------------------------------

C---     METRIQUES DU T6
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     DETERMINANT DU T6
      DETJ = VDJE(5)

C---     COEFFICIENTS EN hQx
      CHU11 = UN_24*VSX
      CHU21 = UN_24*VKX
      CHU31 = UN_24*VEX
      CHU12 = 3*CHU11
      CHU22 = 3*CHU21
      CHU32 = 3*CHU31

C---     COEFFICIENTS EN hQy
      CHV11 = UN_24*VSY
      CHV21 = UN_24*VKY
      CHV31 = UN_24*VEY
      CHV12 = 3*CHV11
      CHV22 = 3*CHV21
      CHV32 = 3*CHV31

C---     COEFFICIENTS DE LAPIDUS
      U1 = VDLE(1,NO1)
      V1 = VDLE(2,NO1)
      H1 = VDLE(3,NO1)
      U2 = VDLE(1,NO2)
      V2 = VDLE(2,NO2)
      U3 = VDLE(1,NO3)
      V3 = VDLE(2,NO3)
      H3 = VDLE(3,NO3)
      U4 = VDLE(1,NO4)
      V4 = VDLE(2,NO4)
      U5 = VDLE(1,NO5)
      V5 = VDLE(2,NO5)
      H5 = VDLE(3,NO5)
      U6 = VDLE(1,NO6)
      V6 = VDLE(2,NO6)
      DHDX = (VSX*H1 + VKX*H3 + VEX*H5)
      DHDY = (VSY*H1 + VKY*H3 + VEY*H5)
      RH1  = CHU11*U1+CHU12*U2+CHU11*U3+CHU12*U4+CHU11*U5+CHU12*U6
     &     + CHV11*V1+CHV12*V2+CHV11*V3+CHV12*V4+CHV11*V5+CHV12*V6
      RH2  = CHU21*U1+CHU22*U2+CHU21*U3+CHU22*U4+CHU21*U5+CHU22*U6
     &     + CHV21*V1+CHV22*V2+CHV21*V3+CHV22*V4+CHV21*V5+CHV22*V6
      RH3  = CHU31*U1+CHU32*U2+CHU31*U3+CHU32*U4+CHU31*U5+CHU32*U6
     &     + CHV31*V1+CHV32*V2+CHV31*V3+CHV32*V4+CHV31*V5+CHV32*V6
      DHNRM = MAX(HYPOT(DHDX, DHDY), 1.0D-12)
      RHNRM = 0 !MAX(SQRT(RH1*RH1 + RH2*RH2 + RH3*RH3), 1.0D-12)
      C = UN_2 * SV2D_STABI_LAPIDUS*(DHNRM + RHNRM) / DETJ

C---     COEFFICIENTS DE DARCY
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO5)
      CM = UN_3*(C1 + C3 + C5)
      C = C + UN_2*CM/DETJ

C---     SOUS-MATRICE H-U
C---     SOUS-MATRICE H-V
C---     SOUS-MATRICE H-H
      VKE(IKH1,IKU1) = VKE(IKH1,IKU1) - CHU11               ! H-U
      VKE(IKH2,IKU1) = VKE(IKH2,IKU1) - CHU21
      VKE(IKH3,IKU1) = VKE(IKH3,IKU1) - CHU31
      VKE(IKH1,IKV1) = VKE(IKH1,IKV1) - CHV11               ! H-V
      VKE(IKH2,IKV1) = VKE(IKH2,IKV1) - CHV21
      VKE(IKH3,IKV1) = VKE(IKH3,IKV1) - CHV31
      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) + C*(VSX*VSX+VSY*VSY) ! H-H
      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) + C*(VKX*VSX+VKY*VSY)
      VKE(IKH3,IKH1) = VKE(IKH3,IKH1) + C*(VEX*VSX+VEY*VSY)

      VKE(IKH1,IKU2) = VKE(IKH1,IKU2) - CHU12               ! H-U
      VKE(IKH2,IKU2) = VKE(IKH2,IKU2) - CHU22
      VKE(IKH3,IKU2) = VKE(IKH3,IKU2) - CHU32
      VKE(IKH1,IKV2) = VKE(IKH1,IKV2) - CHV12               ! H-V
      VKE(IKH2,IKV2) = VKE(IKH2,IKV2) - CHV22
      VKE(IKH3,IKV2) = VKE(IKH3,IKV2) - CHV32

      VKE(IKH1,IKU3) = VKE(IKH1,IKU3) - CHU11               ! H-U
      VKE(IKH2,IKU3) = VKE(IKH2,IKU3) - CHU21
      VKE(IKH3,IKU3) = VKE(IKH3,IKU3) - CHU31
      VKE(IKH1,IKV3) = VKE(IKH1,IKV3) - CHV11               ! H-V
      VKE(IKH2,IKV3) = VKE(IKH2,IKV3) - CHV21
      VKE(IKH3,IKV3) = VKE(IKH3,IKV3) - CHV31
      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) + C*(VSX*VKX+VSY*VKY) ! H-H
      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) + C*(VKX*VKX+VKY*VKY)
      VKE(IKH3,IKH2) = VKE(IKH3,IKH2) + C*(VEX*VKX+VEY*VKY)

      VKE(IKH1,IKU4) = VKE(IKH1,IKU4) - CHU12               ! H-U
      VKE(IKH2,IKU4) = VKE(IKH2,IKU4) - CHU22
      VKE(IKH3,IKU4) = VKE(IKH3,IKU4) - CHU32
      VKE(IKH1,IKV4) = VKE(IKH1,IKV4) - CHV12               ! H-V
      VKE(IKH2,IKV4) = VKE(IKH2,IKV4) - CHV22
      VKE(IKH3,IKV4) = VKE(IKH3,IKV4) - CHV32

      VKE(IKH1,IKU5) = VKE(IKH1,IKU5) - CHU11               ! H-U
      VKE(IKH2,IKU5) = VKE(IKH2,IKU5) - CHU21
      VKE(IKH3,IKU5) = VKE(IKH3,IKU5) - CHU31
      VKE(IKH1,IKV5) = VKE(IKH1,IKV5) - CHV11               ! H-V
      VKE(IKH2,IKV5) = VKE(IKH2,IKV5) - CHV21
      VKE(IKH3,IKV5) = VKE(IKH3,IKV5) - CHV31
      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) + C*(VSX*VEX+VSY*VEY) ! H-H
      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) + C*(VKX*VEX+VKY*VEY)
      VKE(IKH3,IKH3) = VKE(IKH3,IKH3) + C*(VEX*VEX+VEY*VEY)

      VKE(IKH1,IKU6) = VKE(IKH1,IKU6) - CHU12               ! H-U
      VKE(IKH2,IKU6) = VKE(IKH2,IKU6) - CHU22
      VKE(IKH3,IKU6) = VKE(IKH3,IKU6) - CHU32
      VKE(IKH1,IKV6) = VKE(IKH1,IKV6) - CHV12               ! H-V
      VKE(IKH2,IKV6) = VKE(IKH2,IKV6) - CHV22
      VKE(IKH3,IKV6) = VKE(IKH3,IKV6) - CHV32

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_GRV
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_GRV assemble dans la
C     matrice Ke la contribution de volume des termes de gravité
C     des équations de mouvement pour un élément T6L.
C
C Entrée:
C     VDJE        Métrique de l'élément T6
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     Le résidu est :
C        {N} < N1 ; N2 ; N3 > {H} < N1,x ; N2,x ; N3,x > {h}
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_GRV(VKE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VKE  (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8  VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IKU1, IKV1, IKH1
      INTEGER IKU2, IKV2
      INTEGER IKU3, IKV3, IKH2
      INTEGER IKU4, IKV4
      INTEGER IKU5, IKV5, IKH3
      INTEGER IKU6, IKV6, idum_align

      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  P1, P2, P3, P4, P5, P6
      REAL*8  PE1, PE2, PE3, PE4
      REAL*8  CQH
      REAL*8  CGVT1, CGVT2, CGVT3, CGVT4, CGVT5, CGVT6
C-----------------------------------------------------------------------

C---     MÉTRIQUES DU T6L
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     PROFONDEUR NODALES
      P1 = VPRN(SV2D_IPRNO_H,1) * VPRN(SV2D_IPRNO_COEFF_GRVT,1)   ! Profondeur * limiteur
      P2 = VPRN(SV2D_IPRNO_H,2) * VPRN(SV2D_IPRNO_COEFF_GRVT,2)
      P3 = VPRN(SV2D_IPRNO_H,3) * VPRN(SV2D_IPRNO_COEFF_GRVT,3)
      P4 = VPRN(SV2D_IPRNO_H,4) * VPRN(SV2D_IPRNO_COEFF_GRVT,4)
      P5 = VPRN(SV2D_IPRNO_H,5) * VPRN(SV2D_IPRNO_COEFF_GRVT,5)
      P6 = VPRN(SV2D_IPRNO_H,6) * VPRN(SV2D_IPRNO_COEFF_GRVT,6)

C---     PROFONDEURS ELEMENTAIRES
      PE1 = P1 + P2 + P6
      PE2 = P2 + P3 + P4
      PE3 = P6 + P4 + P5
      PE4 = P4 + P6 + P2

C---     COEFFICIENTS EN Qh
      CQH  = SV2D_GRAVITE * UN_96
      CGVT1 = CQH*(PE1+P1)
      CGVT2 = CQH*(PE1+P2 + PE2+P2 + PE4+P2)
      CGVT3 = CQH*(PE2+P3)
      CGVT4 = CQH*(PE2+P4 + PE3+P4 + PE4+P4)
      CGVT5 = CQH*(PE3+P5)
      CGVT6 = CQH*(PE3+P6 + PE1+P6 + PE4+P6)

C---     INDICE DE DDL
      IKU1 =  1
      IKV1 =  2
      IKH1 =  3
      IKU2 =  4
      IKV2 =  5   ! on n'assemble pas IKH2
      IKU3 =  7
      IKV3 =  8
      IKH2 =  9
      IKU4 = 10
      IKV4 = 11   ! on n'assemble pas IKH4
      IKU5 = 13
      IKV5 = 14
      IKH3 = 15
      IKU6 = 16
      IKV6 = 17   ! on n'assemble pas IKH6

C---     ASSEMBLAGE DE LA SOUS-MATRICE U.H
      VKE(IKU1,IKH1) = VKE(IKU1,IKH1) + CGVT1*VSX
      VKE(IKU2,IKH1) = VKE(IKU2,IKH1) + CGVT2*VSX
      VKE(IKU3,IKH1) = VKE(IKU3,IKH1) + CGVT3*VSX
      VKE(IKU4,IKH1) = VKE(IKU4,IKH1) + CGVT4*VSX
      VKE(IKU5,IKH1) = VKE(IKU5,IKH1) + CGVT5*VSX
      VKE(IKU6,IKH1) = VKE(IKU6,IKH1) + CGVT6*VSX
      VKE(IKU1,IKH2) = VKE(IKU1,IKH2) + CGVT1*VKX
      VKE(IKU2,IKH2) = VKE(IKU2,IKH2) + CGVT2*VKX
      VKE(IKU3,IKH2) = VKE(IKU3,IKH2) + CGVT3*VKX
      VKE(IKU4,IKH2) = VKE(IKU4,IKH2) + CGVT4*VKX
      VKE(IKU5,IKH2) = VKE(IKU5,IKH2) + CGVT5*VKX
      VKE(IKU6,IKH2) = VKE(IKU6,IKH2) + CGVT6*VKX
      VKE(IKU1,IKH3) = VKE(IKU1,IKH3) + CGVT1*VEX
      VKE(IKU2,IKH3) = VKE(IKU2,IKH3) + CGVT2*VEX
      VKE(IKU3,IKH3) = VKE(IKU3,IKH3) + CGVT3*VEX
      VKE(IKU4,IKH3) = VKE(IKU4,IKH3) + CGVT4*VEX
      VKE(IKU5,IKH3) = VKE(IKU5,IKH3) + CGVT5*VEX
      VKE(IKU6,IKH3) = VKE(IKU6,IKH3) + CGVT6*VEX

C---     ASSEMBLAGE DE LA SOUS-MATRICE V.H
      VKE(IKV1,IKH1) = VKE(IKV1,IKH1) + CGVT1*VSY
      VKE(IKV2,IKH1) = VKE(IKV2,IKH1) + CGVT2*VSY
      VKE(IKV3,IKH1) = VKE(IKV3,IKH1) + CGVT3*VSY
      VKE(IKV4,IKH1) = VKE(IKV4,IKH1) + CGVT4*VSY
      VKE(IKV5,IKH1) = VKE(IKV5,IKH1) + CGVT5*VSY
      VKE(IKV6,IKH1) = VKE(IKV6,IKH1) + CGVT6*VSY
      VKE(IKV1,IKH2) = VKE(IKV1,IKH2) + CGVT1*VKY
      VKE(IKV2,IKH2) = VKE(IKV2,IKH2) + CGVT2*VKY
      VKE(IKV3,IKH2) = VKE(IKV3,IKH2) + CGVT3*VKY
      VKE(IKV4,IKH2) = VKE(IKV4,IKH2) + CGVT4*VKY
      VKE(IKV5,IKH2) = VKE(IKV5,IKH2) + CGVT5*VKY
      VKE(IKV6,IKH2) = VKE(IKV6,IKH2) + CGVT6*VKY
      VKE(IKV1,IKH3) = VKE(IKV1,IKH3) + CGVT1*VEY
      VKE(IKV2,IKH3) = VKE(IKV2,IKH3) + CGVT2*VEY
      VKE(IKV3,IKH3) = VKE(IKV3,IKH3) + CGVT3*VEY
      VKE(IKV4,IKH3) = VKE(IKV4,IKH3) + CGVT4*VEY
      VKE(IKV5,IKH3) = VKE(IKV5,IKH3) + CGVT5*VEY
      VKE(IKV6,IKH3) = VKE(IKV6,IKH3) + CGVT6*VEY

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_CNV
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_CNV assemble dans la
C     matrice Ke la contribution de volume des termes de convection
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     Le résidu :
C        {N} < N1,x ; N2,x ; N3,x > {U Qx}
C     développé donne
C        {N} ( N1,x*U1*Qx1 + N2,x*U2*Qx2 + N3,x*u3*Qx3)
C     qui est écrit ensuite sous la forme
C        {N} < U1*N1,x ; U2*N2,x ; U3*N3,x > {Qx}
C
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_CNV(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NO1, NO2, NO3
      INTEGER IKU1, IKU2, IKU3
      INTEGER IKV1, IKV2, IKV3
      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  U1, U2, U3, V1, V2, V3, C1, C2, C3
      REAL*8  CNV1, CNV2, CNV3
C-----------------------------------------------------------------------

C---     CONNECTIVITÉS
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     MÉTRIQUES
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     VITESSES
      U1 = VPRN(SV2D_IPRNO_U,NO1)            !
      V1 = VPRN(SV2D_IPRNO_V,NO1)            !
      C1 = VPRN(SV2D_IPRNO_COEFF_CNVT,NO1)   ! Limiteur de convection
      U2 = VPRN(SV2D_IPRNO_U,NO2)
      V2 = VPRN(SV2D_IPRNO_V,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_CNVT,NO2)
      U3 = VPRN(SV2D_IPRNO_U,NO3)
      V3 = VPRN(SV2D_IPRNO_V,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_CNVT,NO3)

C---     COEFFICIENTS DE CONVECTION
      CNV1 = UN_6*C1*(U1*VSX + V1*VSY)
      CNV2 = UN_6*C2*(U2*VKX + V2*VKY)
      CNV3 = UN_6*C3*(U3*VEX + V3*VEY)

C---     INDICE DE DDL
      IKU1 = KLOCE(1)
      IKV1 = KLOCE(2)
      IKU2 = KLOCE(4)
      IKV2 = KLOCE(5)
      IKU3 = KLOCE(7)
      IKV3 = KLOCE(8)

C---     ASSEMBLAGE DE VKE
      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + CNV1
      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) + CNV1
      VKE(IKU3,IKU1) = VKE(IKU3,IKU1) + CNV1
      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + CNV1
      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) + CNV1
      VKE(IKV3,IKV1) = VKE(IKV3,IKV1) + CNV1

      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) + CNV2
      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + CNV2
      VKE(IKU3,IKU2) = VKE(IKU3,IKU2) + CNV2
      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) + CNV2
      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + CNV2
      VKE(IKV3,IKV2) = VKE(IKV3,IKV2) + CNV2

      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) + CNV3
      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) + CNV3
      VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + CNV3
      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) + CNV3
      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) + CNV3
      VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + CNV3

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_COR
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_COR assemble dans la
C     matrice Ke la contribution de volume des termes de Coriolis
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     La matrice lumpée est trop dissipative
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_COR(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IKU1, IKU2, IKU3
      INTEGER IKV1, IKV2, IKV3
      REAL*8  CX, CY
      REAL*8  DETJ
C-----------------------------------------------------------------------

C---     MÉTRIQUES
      DETJ = VDJE(5)

C---     COEFFICIENTS DE CORIOLIS
      CY = UN_24*SV2D_CORIOLIS*DETJ
      CX = -CY

C---     INDICE DE DDL
      IKU1 = KLOCE(1)
      IKV1 = KLOCE(2)
      IKU2 = KLOCE(4)
      IKV2 = KLOCE(5)
      IKU3 = KLOCE(7)
      IKV3 = KLOCE(8)

C---     ASSEMBLAGE DE VKE
      VKE(IKV1,IKU1) = VKE(IKV1,IKU1) + CY+CY
      VKE(IKV2,IKU1) = VKE(IKV2,IKU1) + CY
      VKE(IKV3,IKU1) = VKE(IKV3,IKU1) + CY
      VKE(IKU1,IKV1) = VKE(IKU1,IKV1) + CX+CX
      VKE(IKU2,IKV1) = VKE(IKU2,IKV1) + CX
      VKE(IKU3,IKV1) = VKE(IKU3,IKV1) + CX

      VKE(IKV1,IKU2) = VKE(IKV1,IKU2) + CY
      VKE(IKV2,IKU2) = VKE(IKV2,IKU2) + CY+CY
      VKE(IKV3,IKU2) = VKE(IKV3,IKU2) + CY
      VKE(IKU1,IKV2) = VKE(IKU1,IKV2) + CX
      VKE(IKU2,IKV2) = VKE(IKU2,IKV2) + CX+CX
      VKE(IKU3,IKV2) = VKE(IKU3,IKV2) + CX

      VKE(IKV1,IKU3) = VKE(IKV1,IKU3) + CY
      VKE(IKV2,IKU3) = VKE(IKV2,IKU3) + CY
      VKE(IKV3,IKU3) = VKE(IKV3,IKU3) + CY+CY
      VKE(IKU1,IKV3) = VKE(IKU1,IKV3) + CX
      VKE(IKU2,IKV3) = VKE(IKU2,IKV3) + CX
      VKE(IKU3,IKV3) = VKE(IKU3,IKV3) + CX+CX

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_MAN
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_MAN assemble dans la
C     matrice Ke la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     La matrice masse est lumpée. De la forme de base:
C        {N} <N> {fr} <N>
C     développée en:
C        [ N1.N1  N1.N2  N1.N3 ]
C        [ N2.N1  N2.N2  N2.N3 ] <N>{fr}
C        [ N3.N1  N3.N2  N3.N3 ]
C     lumpée en:
C        [ N1(N1+N2+N3)      0            0      ]
C        [       0      N2(N1+N2+N3)      0      ] <N>{fr}
C        [       0           0       N3(N1+N2+N3)]
C     ce qui revient à:
C        [ N1 0  0 ]
C        [ 0  N2 0 ] <N>{fr}
C        [ 0  0 N3 ]
C     qui à les mêmes composantes que la matrice:
C         {N}<N>
C
C     Masse non lumpée:
C        Dans Maxima
C        n1(x,y):= 1-x-y;
C        n2(x,y):=x;
C        n3(x,y):=y;
C        u(x,y):=n1(x,y)*u1 + n2(x,y)*u2 + n3(x,y)*u3;
C        integrate(integrate(n1(x,y)*n1(x,y)*u(x,y),y,0,1-x),x,0,1);
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_MAN(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  DETJ, COEF_HY
      REAL*8  VFROT1, VFROT2, VFROT3, VFROTS

      INTEGER NO1, NO2, NO3
      INTEGER IKU1, IKU2, IKU3
      INTEGER IKV1, IKV2, IKV3
C-----------------------------------------------------------------------

C---     CONNECTIVITÉS
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     DETERMINANT
      DETJ = VDJE(5)

C---     COEFFICIENT
      COEF_HY = UN_24*DETJ   !! Hydrosim
C      COEF = UN_120*DETJ     !! Matrice complète   (avec cette méthode, on arrive à des écarts
C                                                    par rapport à Hydrosim)

C---     COEFFICIENTS DE FROTTEMENTS
      VFROT1 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO1)
      VFROT2 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO2)
      VFROT3 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO3)

C---     SOMMATIONS DES FROTTEMENTS
      VFROTS = VFROT1 + VFROT2 + VFROT3

C---     INDICE DE DDL
      IKU1 = KLOCE(1)
      IKV1 = KLOCE(2)
      IKU2 = KLOCE(4)
      IKV2 = KLOCE(5)
      IKU3 = KLOCE(7)
      IKV3 = KLOCE(8)

C---     ASSEMBLAGE DE VKE
      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + (VFROT1+VFROT1)     !! Hydrosim
      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) + VFROT1              !! Hydrosim
      VKE(IKU3,IKU1) = VKE(IKU3,IKU1) + VFROT1              !! Hydrosim
      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + (VFROT1+VFROT1)     !! Hydrosim
      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) + VFROT1              !! Hydrosim
      VKE(IKV3,IKV1) = VKE(IKV3,IKV1) + VFROT1              !! Hydrosim

      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) + VFROT2              !! Hydrosim
      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + (VFROT2+VFROT2)     !! Hydrosim
      VKE(IKU3,IKU2) = VKE(IKU3,IKU2) + VFROT2              !! Hydrosim
      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) + VFROT2              !! Hydrosim
      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + (VFROT2+VFROT2)     !! Hydrosim
      VKE(IKV3,IKV2) = VKE(IKV3,IKV2) + VFROT2              !! Hydrosim

      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) + VFROT3              !! Hydrosim
      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) + VFROT3              !! Hydrosim
      VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + (VFROT3+VFROT3)     !! Hydrosim
      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) + VFROT3              !! Hydrosim
      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) + VFROT3              !! Hydrosim
      VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + (VFROT3+VFROT3)     !! Hydrosim

!      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + DEUX*(VFROTS+VFROT1+VFROT1)     !! Matrice complète
!      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) +      (VFROTS+VFROT2+VFROT1)     !! Matrice complète
!      VKE(IKU3,IKU1) = VKE(IKU3,IKU1) +      (VFROTS+VFROT3+VFROT1)     !! Matrice complète
!      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) +      (VFROTS+VFROT1+VFROT2)     !! Matrice complète
!      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + DEUX*(VFROTS+VFROT2+VFROT2)     !! Matrice complète
!      VKE(IKU3,IKU2) = VKE(IKU3,IKU2) +      (VFROTS+VFROT3+VFROT2)     !! Matrice complète
!      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) +      (VFROTS+VFROT1+VFROT3)     !! Matrice complète
!      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) +      (VFROTS+VFROT2+VFROT3)     !! Matrice complète
!      VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + DEUX*(VFROTS+VFROT3+VFROT3)     !! Matrice complète

!      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + DEUX*(VFROTS+VFROT1+VFROT1)     !! Matrice complète
!      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) +      (VFROTS+VFROT2+VFROT1)     !! Matrice complète
!      VKE(IKV3,IKV1) = VKE(IKV3,IKV1) +      (VFROTS+VFROT3+VFROT1)     !! Matrice complète
!      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) +      (VFROTS+VFROT1+VFROT2)     !! Matrice complète
!      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + DEUX*(VFROTS+VFROT2+VFROT2)     !! Matrice complète
!      VKE(IKV3,IKV2) = VKE(IKV3,IKV2) +      (VFROTS+VFROT3+VFROT2)     !! Matrice complète
!      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) +      (VFROTS+VFROT1+VFROT3)     !! Matrice complète
!      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) +      (VFROTS+VFROT2+VFROT3)     !! Matrice complète
!      VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + DEUX*(VFROTS+VFROT3+VFROT3)     !! Matrice complète

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_V_DIF
C
C Description:
C     La subroutine privée SV2D_CMP_KE_V_DIF assemble dans la
C     matrice Ke la contribution de volume des termes de frottement de
C     diffusion des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T3
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VKE
C
C Notes:
C     La réorganisation de VKE par indice est plus efficace mais
C     par contre moins lisible.
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_V_DIF(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (EG_CMMN_NDJV)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRN  (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NO1, NO2, NO3
      INTEGER IKU1, IKU2, IKU3
      INTEGER IKV1, IKV2, IKV3
      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ, COEF
      REAL*8  C1, C2, C3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  TIIHSX, TIIHKX, TIIHEX, TIIHSY, TIIHKY, TIIHEY
      REAL*8  TIJHSX, TIJHKX, TIJHEX, TIJHSY, TIJHKY, TIJHEY
C-----------------------------------------------------------------------

C---     CONNECTIVITES
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     MÉTRIQUES
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     DETERMINANT
      DETJ = VDJE(5)

C---     PROFONDEUR NODALES
      H1 = VPRN(SV2D_IPRNO_H,NO1)
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1) ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     PROFONDEUR MOYENNE !HM n'est pas la profondeur moyenne
      C1 = VPRE(2) + C1    ! Visco totale élémentaire
      C2 = VPRE(2) + C2
      C3 = VPRE(2) + C3
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     COEFFICIENT
      COEF = UN_2*H_NU/DETJ
      TIJHSX = COEF*VSX/H1
      TIJHSY = COEF*VSY/H1
      TIJHKX = COEF*VKX/H2
      TIJHKY = COEF*VKY/H2
      TIJHEX = COEF*VEX/H3
      TIJHEY = COEF*VEY/H3
      TIIHSX = TIJHSX + TIJHSX
      TIIHKX = TIJHKX + TIJHKX
      TIIHEX = TIJHEX + TIJHEX
      TIIHSY = TIJHSY + TIJHSY
      TIIHKY = TIJHKY + TIJHKY
      TIIHEY = TIJHEY + TIJHEY

C---     INDICE DE DDL
      IKU1 = KLOCE(1)
      IKV1 = KLOCE(2)
      IKU2 = KLOCE(4)
      IKV2 = KLOCE(5)
      IKU3 = KLOCE(7)
      IKV3 = KLOCE(8)

C---     ASSEMBLAGE DE VKE
      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + (VSX*TIIHSX + VSY*TIJHSY)
      VKE(IKV1,IKU1) = VKE(IKV1,IKU1) + VSX*TIJHSY
      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) + (VKX*TIIHSX + VKY*TIJHSY)
      VKE(IKV2,IKU1) = VKE(IKV2,IKU1) + VKX*TIJHSY
      VKE(IKU3,IKU1) = VKE(IKU3,IKU1) + (VEX*TIIHSX + VEY*TIJHSY)
      VKE(IKV3,IKU1) = VKE(IKV3,IKU1) + VEX*TIJHSY
      VKE(IKU1,IKV1) = VKE(IKU1,IKV1) + VSY*TIJHSX
      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + (VSX*TIJHSX + VSY*TIIHSY)
      VKE(IKU2,IKV1) = VKE(IKU2,IKV1) + VKY*TIJHSX
      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) + (VKX*TIJHSX + VKY*TIIHSY)
      VKE(IKU3,IKV1) = VKE(IKU3,IKV1) + VEY*TIJHSX
      VKE(IKV3,IKV1) = VKE(IKV3,IKV1) + (VEX*TIJHSX + VEY*TIIHSY)

      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) + (VSX*TIIHKX + VSY*TIJHKY)
      VKE(IKV1,IKU2) = VKE(IKV1,IKU2) + VSX*TIJHKY
      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + (VKX*TIIHKX + VKY*TIJHKY)
      VKE(IKV2,IKU2) = VKE(IKV2,IKU2) + VKX*TIJHKY
      VKE(IKU3,IKU2) = VKE(IKU3,IKU2) + (VEX*TIIHKX + VEY*TIJHKY)
      VKE(IKV3,IKU2) = VKE(IKV3,IKU2) + VEX*TIJHKY
      VKE(IKU1,IKV2) = VKE(IKU1,IKV2) + VSY*TIJHKX
      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) + (VSX*TIJHKX + VSY*TIIHKY)
      VKE(IKU2,IKV2) = VKE(IKU2,IKV2) + VKY*TIJHKX
      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + (VKX*TIJHKX + VKY*TIIHKY)
      VKE(IKU3,IKV2) = VKE(IKU3,IKV2) + VEY*TIJHKX
      VKE(IKV3,IKV2) = VKE(IKV3,IKV2) + (VEX*TIJHKX + VEY*TIIHKY)

      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) + (VSX*TIIHEX + VSY*TIJHEY)
      VKE(IKV1,IKU3) = VKE(IKV1,IKU3) + VSX*TIJHEY
      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) + (VKX*TIIHEX + VKY*TIJHEY)
      VKE(IKV2,IKU3) = VKE(IKV2,IKU3) + VKX*TIJHEY
      VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + (VEX*TIIHEX + VEY*TIJHEY)
      VKE(IKV3,IKU3) = VKE(IKV3,IKU3) + VEX*TIJHEY
      VKE(IKU1,IKV3) = VKE(IKU1,IKV3) + VSY*TIJHEX
      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) + (VSX*TIJHEX + VSY*TIIHEY)
      VKE(IKU2,IKV3) = VKE(IKU2,IKV3) + VKY*TIJHEX
      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) + (VKX*TIJHEX + VKY*TIIHEY)
      VKE(IKU3,IKV3) = VKE(IKU3,IKV3) + VEY*TIJHEX
      VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + (VEX*TIJHEX + VEY*TIIHEY)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_S_DIF
C
C Description:
C     La fonction SV2D_CMP_KE_S_DIF calcule la partie de diffusion de la
C     matrice de rigidité élémentaire pour un élément de surface.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) On prend pour acquis que KNE et KLOCE sont pour un T3 et que
C        l'élément de contour L2 à calculer forme le premier côté (noeuds 1-2).
C
C     2) {NL2} <NT3> {H} < NT3_1,x / H1 ; NT3_2,x / H2 ; NT3_3,x / H3>
C
C     3) Pour les termes de contour, on ne tient compte que de la viscosité
C        physique plutôt que de la viscosité totale. On considère que la
C        viscosité numérique ne comprend que le terme de volume.
C        La viscosité totale mène à des résultats non physiques et une
C        convergence très ralentie.
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_S_DIF(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      INTEGER  KNE  (3)
      INTEGER  KLOCE(9)
      REAL*8   VDJV (EG_CMMN_NDJV)
      REAL*8   VDJS (EG_CMMN_NDJS)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPREV(2)
      REAL*8   VPRES(3)             ! 3 POUR CHAQUE L2
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NO1, NO2, NO3
      INTEGER IKU1, IKU2, IKU3, IKV1, IKV2, IKV3
      REAL*8  COEFX, COEFY
      REAL*8  C1, C2, C3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  VNX, VNY
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DJL2, DJT3
      REAL*8  TIIHSX, TIIHKX, TIIHEX, TIIHSY, TIIHKY, TIIHEY
      REAL*8  TIJHSX, TIJHKX, TIJHEX, TIJHSY, TIJHKY, TIJHEY
C-----------------------------------------------------------------------

C---     CONNECTIVITES DU T3
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     NORMALES ET METRIQUES DE L'ELEMENT L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     METRIQUES DU T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)
      DJT3= VDJV(5)

C---     PROFONDEURS NODALES
      H1 = VPRN(SV2D_IPRNO_H,NO1)
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1) ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     (H*nu) moyen
      C1 = VPREV(2) + C1    ! Visco totale élémentaire
      C2 = VPREV(2) + C2
      C3 = VPREV(2) + C3
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     COEFFICIENT
      COEFX = SV2D_CMULT_INTGCTRQX * H_NU * (DJL2/DJT3)
      COEFY = SV2D_CMULT_INTGCTRQY * H_NU * (DJL2/DJT3)
      TIJHSX = VSX/H1
      TIJHKX = VKX/H2
      TIJHEX = VEX/H3
      TIJHSY = VSY/H1
      TIJHKY = VKY/H2
      TIJHEY = VEY/H3
      TIIHSX = TIJHSX + TIJHSX
      TIIHKX = TIJHKX + TIJHKX
      TIIHEX = TIJHEX + TIJHEX
      TIIHSY = TIJHSY + TIJHSY
      TIIHKY = TIJHKY + TIJHKY
      TIIHEY = TIJHEY + TIJHEY

C---     INDICE DE DDL
      IKU1 = KLOCE(1)
      IKV1 = KLOCE(2)
      IKU2 = KLOCE(4)
      IKV2 = KLOCE(5)
      IKU3 = KLOCE(7)
      IKV3 = KLOCE(8)

C---     SOUS-MATRICE U.U
      VKE(IKU1,IKU1) = VKE(IKU1,IKU1) - COEFX*(TIIHSX*VNX + TIJHSY*VNY)
      VKE(IKU2,IKU1) = VKE(IKU2,IKU1) - COEFX*(TIIHSX*VNX + TIJHSY*VNY)
      VKE(IKU1,IKU2) = VKE(IKU1,IKU2) - COEFX*(TIIHKX*VNX + TIJHKY*VNY)
      VKE(IKU2,IKU2) = VKE(IKU2,IKU2) - COEFX*(TIIHKX*VNX + TIJHKY*VNY)
      VKE(IKU1,IKU3) = VKE(IKU1,IKU3) - COEFX*(TIIHEX*VNX + TIJHEY*VNY)
      VKE(IKU2,IKU3) = VKE(IKU2,IKU3) - COEFX*(TIIHEX*VNX + TIJHEY*VNY)

C---     SOUS-MATRICE U.V
      VKE(IKU1,IKV1) = VKE(IKU1,IKV1) - COEFX*(TIJHSX*VNY)
      VKE(IKU2,IKV1) = VKE(IKU2,IKV1) - COEFX*(TIJHSX*VNY)
      VKE(IKU1,IKV2) = VKE(IKU1,IKV2) - COEFX*(TIJHKX*VNY)
      VKE(IKU2,IKV2) = VKE(IKU2,IKV2) - COEFX*(TIJHKX*VNY)
      VKE(IKU1,IKV3) = VKE(IKU1,IKV3) - COEFX*(TIJHEX*VNY)
      VKE(IKU2,IKV3) = VKE(IKU2,IKV3) - COEFX*(TIJHEX*VNY)

C---     SOUS-MATRICE V.U
      VKE(IKV1,IKU1) = VKE(IKV1,IKU1) - COEFY*(TIJHSY*VNX)
      VKE(IKV2,IKU1) = VKE(IKV2,IKU1) - COEFY*(TIJHSY*VNX)
      VKE(IKV1,IKU2) = VKE(IKV1,IKU2) - COEFY*(TIJHKY*VNX)
      VKE(IKV2,IKU2) = VKE(IKV2,IKU2) - COEFY*(TIJHKY*VNX)
      VKE(IKV1,IKU3) = VKE(IKV1,IKU3) - COEFY*(TIJHEY*VNX)
      VKE(IKV2,IKU3) = VKE(IKV2,IKU3) - COEFY*(TIJHEY*VNX)

C---     SOUS-MATRICE V.V
      VKE(IKV1,IKV1) = VKE(IKV1,IKV1) - COEFY*(TIJHSX*VNX + TIIHSY*VNY)
      VKE(IKV2,IKV1) = VKE(IKV2,IKV1) - COEFY*(TIJHSX*VNX + TIIHSY*VNY)
      VKE(IKV1,IKV2) = VKE(IKV1,IKV2) - COEFY*(TIJHKX*VNX + TIIHKY*VNY)
      VKE(IKV2,IKV2) = VKE(IKV2,IKV2) - COEFY*(TIJHKX*VNX + TIIHKY*VNY)
      VKE(IKV1,IKV3) = VKE(IKV1,IKV3) - COEFY*(TIJHEX*VNX + TIIHEY*VNY)
      VKE(IKV2,IKV3) = VKE(IKV2,IKV3) - COEFY*(TIJHEX*VNX + TIIHEY*VNY)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_KE_S_CNT
C
C Description:
C     La fonction SV2D_CMP_KE_S_CNT calcule la partie de Darcy de la
C     continuité de la matrice de rigidité élémentaire
C     pour un élément de surface.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     {NL2} <NT3_1,x ; NT3_2,x ; NT3_3,x>
C
C************************************************************************
      SUBROUTINE SV2D_CMP_KE_S_CNT(VKE,
     &                             KNE,
     &                             KLOCE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      INTEGER  KNE  (3)
      INTEGER  KLOCE(9)
      REAL*8   VDJV (EG_CMMN_NDJV)
      REAL*8   VDJS (EG_CMMN_NDJS)
      REAL*8   VPRG (LM_CMMN_NPRGL)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8   VPREV(2)
      REAL*8   VPRES(3)             ! 3 POUR CHAQUE L2
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER NO1, NO3, NO5
      INTEGER IKH1, IKH2, IKH3
      REAL*8  C1, C3, C5, CM, C
      REAL*8  VNX, VNY
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DJL2, DJT3
      REAL*8  H1, H3, H5
      REAL*8  DHDX, DHDY, DHNRM
C-----------------------------------------------------------------------

C---     Connectivités des sommets du T6L
      NO1 = KNE(1)
      NO3 = KNE(2)
      NO5 = KNE(3)

C---     Normales et métriques de l'élément L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     Métriques du T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)
      DJT3= VDJV(5)

C---     Degrés de liberté
      H1 = VDLE(3,NO1)
      H3 = VDLE(3,NO3)
      H5 = VDLE(3,NO5)

C---     Coefficients de Lapidus
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)
      DHNRM = MAX(HYPOT(DHDX, DHDY), 1.0D-12)
      C = SV2D_STABI_LAPIDUS * DHNRM

C---     Coefficients de Darcy
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO5)
      CM = UN_3*(C1 + C3 + C5)
      C  = SV2D_CMULT_INTGCTRH * (C+CM) * (DJL2/DJT3)

C---     Indice de DDL
      IKH1 = KLOCE(3)
      IKH2 = KLOCE(6)
      IKH3 = KLOCE(9)

C---     Sous-matrice H.H
      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) - C*(VNX*VSX + VNY*VSY)
      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) - C*(VNX*VSX + VNY*VSY)
      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) - C*(VNX*VKX + VNY*VKY)
      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) - C*(VNX*VKX + VNY*VKY)
      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) - C*(VNX*VEX + VNY*VEY)
      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) - C*(VNX*VEX + VNY*VEY)

      RETURN
      END
