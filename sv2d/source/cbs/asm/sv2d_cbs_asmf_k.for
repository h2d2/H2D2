C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_CBS_ASMF_K
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C     NSOLR et NSOLC doivent être égaux à NDLN
C
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMF_K(VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLC,
     &                           VSOLR,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG,
     &                           VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMF_K
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Sollicitations concentrées
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLL, KLOCN, VSOLC, VFG)
      ENDIF

C---     Contribution de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& private(IERR)
!$omp& default(shared)
         CALL SV2D_CBS_ASMF_KV(VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         VDJV,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VSOLR,
     &                         VFG)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contribution des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMF (VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLC,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        VFG)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CBS_ASMF_KV
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Assemble le vent comme vent en absolu, sans tenir compte de la
C     vitesse du courant.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMF_KV(VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            VDJV,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VSOLR,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER IPH
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER KLOCE(3, 6)
      REAL*8  W, WX, WY, CW, COEFW, CMULW
      REAL*8  WU1, WU2, WU3, WU4, WU5, WU6
      REAL*8  WV1, WV2, WV3, WV4, WV5, WV6
      REAL*8  SU1, SU2, SU3, SU4, SU5, SU6
      REAL*8  SV1, SV2, SV3, SV4, SV5, SV6
      REAL*8  SH1,      SH3,      SH5
      REAL*8  SUE1, SUE2, SUE3, SUE4
      REAL*8  SVE1, SVE2, SVE3, SVE4
      REAL*8  SHE
      REAL*8  DETJ_T6, DETJ_T3
      REAL*8  VFE(3, 6)

      REAL*8, PARAMETER :: RHO_AIR = 1.2475D+00
      REAL*8, PARAMETER :: RHO_EAU = 1.0000D+03
C-----------------------------------------------------------------------
      REAL*8 F_ENEAU
      F_ENEAU(IN) = MERGE(UN,ZERO,(VPRNO(IPH,IN).GT.SV2D_DECOU_HMIN))
C-----------------------------------------------------------------------

      IPH = SV2D_IPRNO_H
      CMULW = SV2D_CMULT_VENT * RHO_AIR / RHO_EAU

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES
         NO1 = KNGV(1, IE)
         NO2 = KNGV(2, IE)
         NO3 = KNGV(3, IE)
         NO4 = KNGV(4, IE)
         NO5 = KNGV(5, IE)
         NO6 = KNGV(6, IE)

C---        CONNECTIVITES
         DETJ_T6 = UN_24*VDJV(5,IE)
         DETJ_T3 = UN_4*DETJ_T6      ! 1/4 POUR LE DJ SUR LE T3
         COEFW = CMULW * DETJ_T3

C---        Vent
         WX = VPRNO(SV2D_IPRNO_WND_X, NO1)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO1)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO1) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU1 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV1 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

         WX = VPRNO(SV2D_IPRNO_WND_X, NO2)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO2)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO2) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU2 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV2 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

         WX = VPRNO(SV2D_IPRNO_WND_X, NO3)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO3)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO3) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU3 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV3 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

         WX = VPRNO(SV2D_IPRNO_WND_X, NO4)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO4)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO4) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU4 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV4 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

         WX = VPRNO(SV2D_IPRNO_WND_X, NO5)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO5)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO5) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU5 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV5 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

         WX = VPRNO(SV2D_IPRNO_WND_X, NO6)
         WY = VPRNO(SV2D_IPRNO_WND_Y, NO6)
         W  = HYPOT(WX, WY)
         CW = F_ENEAU(NO6) * COEFW * SP_HDRO_CW(W, SV2D_FCT_CW_VENT) * W
         WU6 = VPRNO(SV2D_IPRNO_WND_X, NO1) * CW
         WV6 = VPRNO(SV2D_IPRNO_WND_Y, NO1) * CW

C---        SOLLICITATIONS NODALES
         SU1 = DETJ_T3 * VSOLR(1,NO1) + WU1
         SV1 = DETJ_T3 * VSOLR(2,NO1) + WV1
         SH1 = DETJ_T6 * VSOLR(3,NO1)
         SU2 = DETJ_T3 * VSOLR(1,NO2) + WU2
         SV2 = DETJ_T3 * VSOLR(2,NO2) + WV2
         SU3 = DETJ_T3 * VSOLR(1,NO3) + WU3
         SV3 = DETJ_T3 * VSOLR(2,NO3) + WV3
         SH3 = DETJ_T6 * VSOLR(3,NO3)
         SU4 = DETJ_T3 * VSOLR(1,NO4) + WU4
         SV4 = DETJ_T3 * VSOLR(2,NO4) + WV4
         SU5 = DETJ_T3 * VSOLR(1,NO5) + WU5
         SV5 = DETJ_T3 * VSOLR(2,NO5) + WV5
         SH5 = DETJ_T6 * VSOLR(3,NO5)
         SU6 = DETJ_T3 * VSOLR(1,NO6) + WU6
         SV6 = DETJ_T3 * VSOLR(2,NO6) + WV6

C---        SOMMATIONS POUR CHAQUE SOUS-ELEMENT
         SUE1 = SU1 + SU2 + SU6
         SVE1 = SV1 + SV2 + SV6
         SUE2 = SU2 + SU3 + SU4
         SVE2 = SV2 + SV3 + SV4
         SUE3 = SU6 + SU4 + SU5
         SVE3 = SV6 + SV4 + SV5
         SUE4 = SU4 + SU6 + SU2
         SVE4 = SV4 + SV6 + SV2
         SHE  = SH1 + SH3 + SH5

C---        CONTRIBUTIONS
         VFE(1, 1) = SUE1+SU1
         VFE(2, 1) = SVE1+SV1
         VFE(3, 1) = SHE +SH1
         VFE(1, 2) = SUE1+SU2 + SUE2+SU2 + SUE4+SU2
         VFE(2, 2) = SVE1+SV2 + SVE2+SV2 + SVE4+SV2
         VFE(3, 2) = ZERO
         VFE(1, 3) = SUE2+SU3
         VFE(2, 3) = SVE2+SV3
         VFE(3, 3) = SHE +SH3
         VFE(1, 4) = SUE2+SU4 + SUE3+SU4 + SUE4+SU4
         VFE(2, 4) = SVE2+SV4 + SVE3+SV4 + SVE4+SV4
         VFE(3, 4) = ZERO
         VFE(1, 5) = SUE3+SU5
         VFE(2, 5) = SVE3+SV5
         VFE(3, 5) = SHE +SH5
         VFE(1, 6) = SUE3+SU6 + SUE1+SU6 + SUE4+SU6
         VFE(2, 6) = SVE3+SV6 + SVE1+SV6 + SVE4+SV6
         VFE(3, 6) = ZERO

C---        TABLE KLOCE DE L'ÉLÉMENT T6L
!dir$ IVDEP, LOOP COUNT(6)
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1,IN) = KLOCN(1, KNGV(IN,IE))
            KLOCE(2,IN) = KLOCN(2, KNGV(IN,IE))
            KLOCE(3,IN) = KLOCN(3, KNGV(IN,IE))
         ENDDO

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END
