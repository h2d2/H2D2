C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_asmku_r.for,v 1.1 2013/10/08 19:44:21 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_CBS_ASMKU_R
C   Private:
C     SUBROUTINE SV2D_CBS_ASMRES_RV
C     SUBROUTINE SV2D_CBS_ASMKU_RS
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKU_R(VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)
CDEC$ATTRIBUTES DLLEXPORT :: SV2D_CBS_ASMKU_R

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl.fi'

      INTEGER NNELT, NDLNMAX, NDLEMAX
      PARAMETER (NNELT=6)
      PARAMETER (NDLNMAX=3)
      PARAMETER (NDLEMAX=NNELT*NDLNMAX)

      REAL*8  VKE  (NDLEMAX*NDLEMAX)
      REAL*8  VFE  (NDLEMAX)
      INTEGER KLOCE(NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. NDLNMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Sollicitations concentrées
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLL, KLOCN, VSOLC, VFG)
      ENDIF

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VFE, KLOCE)
         CALL SV2D_CBS_ASMRES_RV(KLOCE,
     &                           VFE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLR,
     &                           VDLG,
     &                           VFG)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         CALL SV2D_CBS_ASMKU_RS(KLOCE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          VDLG,
     &                          VFG)
      ENDIF

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_ASMKU(KLOCE,
     &                        VKE,
     &                        VFE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG,
     &                        VFG)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[K].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMRES_RV(KLOCE,
     &                              VFE,
     &                              VCORG,
     &                              KLOCN,
     &                              KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              VPREV,
     &                              VPRES,
     &                              VSOLR,
     &                              VDLG,
     &                              VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, II, NO

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  6)
      PARAMETER (NDLE_LCL = 18)
      PARAMETER (NPRN_LCL = 18)

      REAL*8  VDLE(NDLE_LCL)
      REAL*8  VPRN(NPRN_LCL * NNEL_LCL)
      INTEGER KNE (NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL

!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        Transfert des DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            II=II+3
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        R(u)
         CALL DINIT(LM_CMMN_NDLEV, ZERO, VFE, 1)
         CALL SV2D_CBS_RE_V(VFE,
     &                      VDJV(1,IE),
     &                      VPRGL,
     &                      VPRN,
     &                      VPREV(1,IE),
     &                      VDLE,
     &                      VDLE)

C---        Table KLOCE de l'élément T6L
         DO IN=1,EG_CMMN_NNELV
            KLOCE(1, IN) = KLOCN(1, KNE(IN))
            KLOCE(2, IN) = KLOCN(2, KNE(IN))
            KLOCE(3, IN) = KLOCN(3, KNE(IN))
         ENDDO

C---       Assemblage du vecteur global
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On transfert les propriétés du T6L et du L3L par simplicité de
C     gestion. Le L3L n'aurait besoin que de deux sous-triangles T3.
C     On assemble une matrice complète 18x18, car la matrice Ke doit
C     être carrée pour l'assemblage.
C************************************************************************
      SUBROUTINE SV2D_CBS_ASMKU_RS(KLOCE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IN, II, NO
      INTEGER IES, IEV, ICT

      INTEGER NNEL_LCL
      INTEGER NDLV_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  6)
      PARAMETER (NDLV_LCL = 18)
      PARAMETER (NPRN_LCL = 18)

      REAL*8  VDLEV(NDLV_LCL)
      REAL*8  VPRNV(NPRN_LCL * NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLV_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Element parent et côté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des DDL
         II=0
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN, IEV)
            VDLEV(II+1) = VDLG(1,NO)
            VDLEV(II+2) = VDLG(2,NO)
            VDLEV(II+3) = VDLG(3,NO)
            II=II+3
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN, IEV)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        R(u)
         CALL DINIT(LM_CMMN_NDLEV, ZERO, VFE, 1)
         CALL SV2D_CBS_RE_S(VFE,
     &                      VDJV(1,IEV),
     &                      VDJS(1,IES),
     &                      VPRGL,
     &                      VPRNV,
     &                      VPREV(1,IEV),
     &                      VPRES(1,IES),
     &                      VDLEV,
     &                      VDLEV,
     &                      ICT)

C---        Table KLOCE de l'élément T6L
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            KLOCE(1,IN) = KLOCN(1,NO)
            KLOCE(2,IN) = KLOCN(2,NO)
            KLOCE(3,IN) = KLOCN(3,NO)
         ENDDO

C---        Assemblage du vecteur global
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

      ENDDO

      RETURN
      END
