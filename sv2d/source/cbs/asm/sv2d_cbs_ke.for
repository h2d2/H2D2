C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_CBS
C         LOGICAL SV2D_CBS_KE_IPVALID
C         SUBROUTINE SV2D_CBS_KE_V
C         SUBROUTINE SV2D_CBS_KE_S
C
C************************************************************************

      LOGICAL FUNCTION SV2D_CBS_KE_IPVALID() RESULT(R)

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'

      LOGICAL, SAVE      :: DONE = .FALSE.
      INTEGER, PARAMETER :: NOTUSED = -1
C-----------------------------------------------------------------------

      R = .TRUE.
      IF (DONE) RETURN

      R = .TRUE.
      R = (R .AND. (SV2D_IPRGL_GRAVITE        .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_LATITUDE       .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_FCT_CW_VENT    .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_CST      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_LM       .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_SMGO     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_BINF     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_VISCO_BSUP     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_HTRG     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_HMIN     .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_DECOU_PENA_H   .EQ. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_DECOU_PENA_Q   .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_MAN      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_UMAX     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_PORO     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_AMORT    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_CON_FACT .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_GRA_FACT .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_DIF_NU   .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_DECOU_DIF_PE   .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_DECOU_DRC_NU   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_PECLET   .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_AMORT    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_DARCY    .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_STABI_LAPIDUS  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_CON      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_GRA      .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_CMULT_PDYN     .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_MAN      .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_VENT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CMULT_INTGCTR  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PENALITE .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_DELPRT   .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_PNUMR_DELMIN   .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_OMEGAKT  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PRTPREL  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_PNUMR_PRTPRNO  .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRGL_CORIOLIS       .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRGL_CMULT_VENT_REL .EQ. NOTUSED))

      R = (R .AND. (SV2D_IPRNO_Z              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_N              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_ICE_E          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_ICE_N          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_WND_X          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_WND_Y          .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_U              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_V              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_H              .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_CNVT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_GRVT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_FROT     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_DIFF     .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRNO_COEFF_PE       .EQ. NOTUSED))
         R = (R .AND. (SV2D_IPRNO_COEFF_DMPG     .EQ. NOTUSED))
         R = (R .AND. (SV2D_IPRNO_COEFF_VENT     .EQ. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_DRCY     .NE. NOTUSED))
      R = (R .AND. (SV2D_IPRNO_COEFF_PORO     .NE. NOTUSED))
         R = (R .AND. (SV2D_IPRNO_DECOU_PENA     .EQ. NOTUSED))

      DONE = .TRUE.
      RETURN
      END FUNCTION SV2D_CBS_KE_IPVALID

C************************************************************************
C Sommaire: SV2D_CBS_KE_V
C
C Description:
C     La fonction SV2D_CBS_ASMK_V calcul le matrice de rigidité
C     élémentaire due à un élément de volume.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C     VDLE        Table de Degrés de Libertés Élémentaires
C
C Sortie:
C     VKE         Matrice élémentaire
C
C Notes:
C
C************************************************************************
      SUBROUTINE SV2D_CBS_KE_V(VKE,
     &                         VDJE,
     &                         VPRG,
     &                         VPRN,
     &                         VPRE,
     &                         VDLE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRG (LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8  VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      REAL*8  VDJT3  (5, 4)
      INTEGER KNET3  (3, 4)
      INTEGER KLOCET3(9, 4)
      INTEGER IT3, ID

      DATA KNET3  / 1,2,6,  2,3,4,  6,4,5,  4,6,2/
      DATA KLOCET3/ 1, 2, 3,   4, 5, 6,  16,17,18,   ! NOEUDS 1,2,6,
     &              4, 5, 6,   7, 8, 9,  10,11,12,   ! NOEUDS 2,3,4,
     &             16,17,18,  10,11,12,  13,14,15,   ! NOEUDS 6,4,5,
     &             10,11,12,  16,17,18,   4, 5, 6/   ! NOEUDS 4,6,2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
C-----------------------------------------------------------------------

C---     Métriques des sous-élément
      DO IT3=1,4
         VDJT3(1,IT3) = UN_2*VDJE(1)
         VDJT3(2,IT3) = UN_2*VDJE(2)
         VDJT3(3,IT3) = UN_2*VDJE(3)
         VDJT3(4,IT3) = UN_2*VDJE(4)
         VDJT3(5,IT3) = UN_4*VDJE(5)
      ENDDO
      VDJT3(1:4,4) = -VDJT3(1:4,4)

C---        DIFFUSION
      DO IT3=1,4
         CALL SV2D_CMP_KE_V_DIF(VKE,
     &                       KNET3(1,IT3), KLOCET3(1,IT3), VDJT3(1,IT3),
     &                       VPRG, VPRN, VPRE(1,IT3), VDLE)
      ENDDO

C---        MANNING
      DO IT3=1,4
         CALL SV2D_CMP_KE_V_MAN(VKE,
     &                       KNET3(1,IT3), KLOCET3(1,IT3), VDJT3(1,IT3),
     &                       VPRG, VPRN, VPRE(1,IT3), VDLE)
      ENDDO

C---        CORIOLIS
      DO IT3=1,4
         CALL SV2D_CMP_KE_V_COR(VKE,
     &                       KNET3(1,IT3), KLOCET3(1,IT3), VDJT3(1,IT3),
     &                       VPRG, VPRN, VPRE(1,IT3), VDLE)
      ENDDO

C---        CONVECTION
      DO IT3=1,4
         CALL SV2D_CMP_KE_V_CNV(VKE,
     &                       KNET3(1,IT3), KLOCET3(1,IT3), VDJT3(1,IT3),
     &                       VPRG, VPRN, VPRE(1,IT3), VDLE)
      ENDDO

C---        GRAVITE
      CALL SV2D_CMP_KE_V_GRV(VKE, VDJE, VPRG, VPRN, VPRE, VDLE)

C---        CONTINUITÉ
      CALL SV2D_CMP_KE_V_CNT(VKE, VDJE, VPRG, VPRN, VPRE, VDLE)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CBS_KE_S
C
C Description:
C     La fonction SV2D_CBS_ASMK_S calcul le matrice de rigidité
C     élémentaire due à un élément de surface.
C     Les tables passées en paramètre sont des tables élémentaires,
C     à part la table VDLG.
C
C Entrée:
C     VDJV        Table du Jacobien Élémentaire de l'élément de volume
C     VDJS        Table du Jacobien Élémentaire de l'élément de surface
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales de Volume
C     VPREV       Table de PRopriétés Élémentaires de Volume
C     VPRES       Table de PRopriétés Élémentaires de Surface
C     VDLE        Table de Degrés de Libertés Élémentaires de Volume
C     ICOTE       L'élément de surface forme le Ième CÔTÉ du triangle
C
C Sortie:
C     VKE         Matrice élémentaire
C
C Notes:
C     On permute les métriques du T6L parent pour que le côté à traiter
C     soit le premier côté (noeuds 1-2-3).
C
C************************************************************************
      SUBROUTINE SV2D_CBS_KE_S(VKE,
     &                         VDJV,
     &                         VDJS,
     &                         VPRG,
     &                         VPRN,
     &                         VPREV,
     &                         VPRES,
     &                         VDLE,
     &                         ICOTE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VKE  (LM_CMMN_NDLEV, LM_CMMN_NDLEV)
      REAL*8  VDJV (EG_CMMN_NDJV)
      REAL*8  VDJS (EG_CMMN_NDJS)
      REAL*8  VPRG (LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8  VPREV(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VPRES(LM_CMMN_NPRES_D1, LM_CMMN_NPRES_D2)
      REAL*8  VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER ICOTE

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IL2, IT3
      INTEGER KNET6  (3, 3)       ! 3 noeuds sommets, 3 cotés
      INTEGER KLOCET6(9, 3)       ! 9 DDL, 3 cotés
      INTEGER KNET3  (3, 2, 3)    ! 3 noeuds, 2 sous-elem T3, 3 cotés
      INTEGER KLOCET3(9, 2, 3)    ! 9 DDL, 2 sous-elem T3, 3 cotés
      INTEGER KT3    (2, 3)       ! 2 sous-elem T3, 3 cotés
      REAL*8  VDJX(4), VDJY(4)
      REAL*8  VDJT6(5), VDJT3(5)
      REAL*8  VDJL2(3)

      DATA KNET6  / 1,3,5,  3,5,1,  5,1,3/           ! Les sommets du T6L
      DATA KLOCET6/ 1, 2, 3,   7, 8, 9,  13,14,15,   ! NOEUDS 1,3,5,
     &              7, 8, 9,  13,14,15,   1, 2, 3,   ! NOEUDS 3,5,1,
     &             13,14,15,   1, 2, 3,   7, 8, 9/   ! NOEUDS 5,1,3,

      DATA KNET3  / 1,2,6,  2,3,4,  3,4,2,  4,5,6,  5,6,4,  6,1,2/
      DATA KLOCET3/ 1, 2, 3,   4, 5, 6,  16,17,18,   ! NOEUDS 1,2,6,
     &              4, 5, 6,   7, 8, 9,  10,11,12,   ! NOEUDS 2,3,4,
     &              7, 8, 9,  10,11,12,   4, 5, 6,   ! NOEUDS 3,4,2,
     &             10,11,12,  13,14,15,  16,17,18,   ! NOEUDS 4,5,6,
     &             13,14,15,  16,17,18,  10,11,12,   ! NOEUDS 5,6,4,
     &             16,17,18,   1, 2, 3,   4, 5, 6/   ! NOEUDS 6,1,2,
      DATA KT3    / 1,2, 2,3, 3,1/
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
D     CALL ERR_PRE(LM_CMMN_NPRES_D1 .GE. 3)     ! Au moins 3 POUR CHAQUE L2
D     CALL ERR_PRE(LM_CMMN_NPRES_D2 .EQ. 2)     ! 2 L2
D     CALL ERR_PRE(ICOTE .GE. 1 .AND. ICOTE .LE. 3)
C-----------------------------------------------------------------------

C---     Monte les tables aux. pour permuter les métriques
      VDJX(1) = VDJV(1)
      VDJX(2) = VDJV(2)
      VDJX(3) = -(VDJX(1)+VDJX(2))
      VDJX(4) = VDJX(1)
      VDJY(1) = VDJV(3)
      VDJY(2) = VDJV(4)
      VDJY(3) = -(VDJY(1)+VDJY(2))
      VDJY(4) = VDJY(1)

C---     Métriques permutées
      VDJT6(1) = VDJX(ICOTE)
      VDJT6(2) = VDJX(ICOTE+1)
      VDJT6(3) = VDJY(ICOTE)
      VDJT6(4) = VDJY(ICOTE+1)
      VDJT6(5) = VDJV(5)

C---     Métriques du sous-élément T3
      VDJT3(1) = UN_2*VDJT6(1)
      VDJT3(2) = UN_2*VDJT6(2)
      VDJT3(3) = UN_2*VDJT6(3)
      VDJT3(4) = UN_2*VDJT6(4)
      VDJT3(5) = UN_4*VDJT6(5)

C---     Métriques du sous-élément L2
      VDJL2(1) = VDJS(1)
      VDJL2(2) = VDJS(2)
      VDJL2(3) = UN_2*VDJS(3)

C---        BOUCLE SUR LES SOUS-ÉLÉMENTS
C           ============================
      DO IL2=1,2
         IT3 = KT3(IL2, ICOTE)

C---        Diffusion
         CALL SV2D_CMP_KE_S_DIF(VKE,
     &                          KNET3  (1, IL2, ICOTE),
     &                          KLOCET3(1, IL2, ICOTE),
     &                          VDJT3,
     &                          VDJL2,
     &                          VPRG,
     &                          VPRN,
     &                          VPREV(1,IT3),
     &                          VPRES(1,IL2),
     &                          VDLE)

      ENDDO

C---        Continuité
      CALL SV2D_CMP_KE_S_CNT(VKE,
     &                       KNET6  (1, ICOTE),
     &                       KLOCET6(1, ICOTE),
     &                       VDJT6,
     &                       VDJS,
     &                       VPRG,
     &                       VPRN,
     &                       VPREV,
     &                       VPRES,
     &                       VDLE)

      RETURN
      END
