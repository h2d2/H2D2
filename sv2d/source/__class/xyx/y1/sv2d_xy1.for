C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xy1.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_XY1
C         FTN (Sub)Module: SV2D_XY1_M
C            Public:
C               SUBROUTINE SV2D_XY1_CTR
C            Private:
C            
C            FTN Type: SV2D_XY1_T
C               Public:
C                  INTEGER SV2D_XY1_DTR
C               Private:
C
C************************************************************************

      MODULE SV2D_XY1_M

      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---  La classe
      !========================================================================
      TYPE, EXTENDS(SV2D_XBS_T) :: SV2D_XY1_T
!        pass
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => SV2D_XY1_DTR
         PROCEDURE, PUBLIC :: CLCPRNO  => SV2D_XY1_CLCPRNO
         PROCEDURE, PUBLIC :: CLCPRNEV => SV2D_XY1_CLCPRNEV
         PROCEDURE, PUBLIC :: CLCPRNES => SV2D_XY1_CLCPRNES
      END TYPE SV2D_XY1_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: SV2D_XY1_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: SV2D_XY1_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         ! ---  Fonction CLC
         MODULE INTEGER FUNCTION SV2D_XY1_CLCPRNO(SELF)
            CLASS(SV2D_XY1_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY1_CLCPRNO

         MODULE INTEGER FUNCTION SV2D_XY1_CLCPRNEV(SELF, VDLE, VPRN)
            CLASS(SV2D_XY1_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION SV2D_XY1_CLCPRNEV

         MODULE INTEGER FUNCTION SV2D_XY1_CLCPRNES(SELF, VDLE, VPRN)
            CLASS(SV2D_XY1_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION SV2D_XY1_CLCPRNES
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est concrète, OKID peut ne pas exister.
C************************************************************************
      FUNCTION SV2D_XY1_CTR(OKID) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY1_CTR
CDEC$ ENDIF

      USE SV2D_XBS_M, ONLY: SV2D_XBS_CTR

      CLASS(SV2D_XY1_T), INTENT(INOUT), POINTER, OPTIONAL :: OKID

      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'

      INTEGER IERR, IRET
      TYPE (SV2D_XY1_T), POINTER :: SELF
      CLASS(SV2D_XBS_T), POINTER :: OPRNT, SELF_P
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      IF (PRESENT(OKID)) THEN
         CALL ERR_ASR(ASSOCIATED(OKID))
         SELF => OKID
      ELSE
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
      ENDIF

C---     Construis le parent
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         OPRNT => SV2D_XBS_CTR(SELF_P)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XY1_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XY1_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY1_DTR
CDEC$ ENDIF

      USE SV2D_XBS_M, ONLY : SV2D_XBS_DTR

      CLASS(SV2D_XY1_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(SV2D_XY1_T), POINTER :: SELF_P
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Détruis le parent
      IERR = SV2D_XBS_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      SV2D_XY1_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY1_DTR

      END MODULE SV2D_XY1_M
