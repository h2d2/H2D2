C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_prcprgl.for,v 1.5 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XY4_PRCPRGL
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XY4_M) SV2D_XY4_PRCPRGL_M

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XY4_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY4_PRCPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY4_PRCPRGL
CDEC$ ENDIF

      USE SV2D_IPRG_M, ONLY: SV2D_IPRG_T
      USE SV2D_XPRG_M, ONLY: SV2D_XPRG_T

      CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      REAL*8  V
      TYPE (LM_EDTA_T),   POINTER :: EDTA
      TYPE (SV2D_IPRG_T), POINTER :: IPRG
      TYPE (SV2D_XPRG_T), POINTER :: XPRG
      REAL*8,             POINTER :: VPRGL(:)
C-----------------------------------------------------------------------
      REAL*8 CMUL
      REAL*8 F
      CMUL(F) = MERGE(1.0D0, 0.0D0, F .LT. 0.0D0)
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      XPRG => SELF%XPRG
      VPRGL => EDTA%VPRGL
      
C---     Appel du parent
      IERR = SELF%SV2D_XY3_T%PRCPRGL()

C---     Les prop. locales
      V = VPRGL(IPRG%FCT_CW_VENT)
      XPRG%FCT_CW_VENT    = NINT( ABS(V) )   ! Écrase XY3
      XPRG%CMULT_VENT_REL = CMUL(V)
      XPRG%PNUMR_DELMIN   = VPRGL(IPRG%PNUMR_DELMIN)

      SV2D_XY4_PRCPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY4_PRCPRGL

      END SUBMODULE SV2D_XY4_PRCPRGL_M
