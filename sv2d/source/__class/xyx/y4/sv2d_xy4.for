C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xy4.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Functions:
C   Public:
C     INTEGER SV2D_XY4_000
C     INTEGER SV2D_XY4_999
C     INTEGER SV2D_XY4_CTR
C     INTEGER SV2D_XY4_DTR
C     INTEGER SV2D_XY4_INI
C     INTEGER SV2D_XY4_RST
C     INTEGER SV2D_XY4_REQHBASE
C     LOGICAL SV2D_XY4_HVALIDE
C   Private:
C     SUBROUTINE SV2D_XY4_REQSELF
C     INTEGER SV2D_XY4_INIVTBL
C     INTEGER SV2D_XY4_INIPRMS
C
C************************************************************************

      MODULE SV2D_XY4_M

      USE SV2D_XY3_M, ONLY: SV2D_XY3_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---  La classe
      !========================================================================
      TYPE, EXTENDS(SV2D_XY3_T) :: SV2D_XY4_T
!        pass
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => SV2D_XY4_DTR
         PROCEDURE, PUBLIC :: PSLPRGL  => SV2D_XY4_PSLPRGL
         PROCEDURE, PUBLIC :: PRCPRGL  => SV2D_XY4_PRCPRGL
         PROCEDURE, PUBLIC :: PSLPRNO  => SV2D_XY4_PSLPRNO
         PROCEDURE, PUBLIC :: CLCPRNO  => SV2D_XY4_CLCPRNO

         ! ---  Méthodes virtuelles SV2D
         PROCEDURE, PUBLIC :: PSLPRGL_CHK => SV2D_XY4_PSLPRGL_CHK
         PROCEDURE, PUBLIC :: PSLPRGL_IPG => SV2D_XY4_PSLPRGL_IPG
      END TYPE SV2D_XY4_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: SV2D_XY4_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: SV2D_XY4_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         ! ---  Fonction PSL
         MODULE INTEGER FUNCTION SV2D_XY4_PSLPRGL_CHK(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_PSLPRGL_CHK
         
         MODULE INTEGER FUNCTION SV2D_XY4_PSLPRGL_IPG(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_PSLPRGL_IPG

         MODULE INTEGER FUNCTION SV2D_XY4_PSLPRGL(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_PSLPRGL
         
         MODULE INTEGER FUNCTION SV2D_XY4_PSLPRNO(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_PSLPRNO

         ! ---  Fonction PRC
         MODULE INTEGER FUNCTION SV2D_XY4_PRCPRGL(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_PRCPRGL
         
         ! ---  Fonction CLC
         MODULE INTEGER FUNCTION SV2D_XY4_CLCPRNO(SELF)
            CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XY4_CLCPRNO
         
         MODULE INTEGER FUNCTION SV2D_XY4_CLCPRNEV(SELF, VDLE, VPRN)
            CLASS(SV2D_XY4_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION SV2D_XY4_CLCPRNEV

         MODULE INTEGER FUNCTION SV2D_XY4_CLCPRNES(SELF, VDLE, VPRN)
            CLASS(SV2D_XY4_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION SV2D_XY4_CLCPRNES
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est concrète, OKID peut ne pas exister.
C************************************************************************
      FUNCTION SV2D_XY4_CTR(OKID) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY4_CTR
CDEC$ ENDIF

      USE SV2D_XY3_M, ONLY: SV2D_XY3_CTR

      CLASS(SV2D_XY4_T), INTENT(INOUT), POINTER, OPTIONAL :: OKID

      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'

      INTEGER IERR, IRET
      TYPE (SV2D_XY4_T), POINTER :: SELF
      CLASS(SV2D_XY3_T), POINTER :: OPRNT, SELF_P
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      IF (PRESENT(OKID)) THEN
         CALL ERR_ASR(ASSOCIATED(OKID)) 
         SELF => OKID
      ELSE
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
      ENDIF

C---     Construis le parent
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         OPRNT => SV2D_XY3_CTR(SELF_P)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XY4_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XY4_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY4_DTR
CDEC$ ENDIF

      USE SV2D_XY2_M, ONLY: SV2D_XY2_DTR

      CLASS(SV2D_XY4_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(SV2D_XY4_T), POINTER :: SELF_P
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Détruis le parent
      IERR = SV2D_XY2_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      SV2D_XY4_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY4_DTR

      END MODULE SV2D_XY4_M
