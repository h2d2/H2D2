C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_prcprgl.for,v 1.5 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_GY4_PRCPRGL
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_GY4_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_PRCPRGL (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_PRCPRGL
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xy4.fi'

      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRG_T),    POINTER :: IPRG
      TYPE (SV2D_XPRG_T),    POINTER :: XPRG
      REAL*8,                POINTER :: VPRGL(:)
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SV2D_XBS_REQEDTA(HOBJ)
      IPRG => SV2D_XBS_REQIPRG(HOBJ)
      XPRG => SV2D_XBS_REQXPRG(HOBJ)
      VPRGL => EDTA%VPRGL
      
C---     Appel du parent
      IERR = SV2D_XY4_PRCPRGL(HOBJ)

      SV2D_GY4_PRCPRGL = ERR_TYP()
      RETURN
      END

