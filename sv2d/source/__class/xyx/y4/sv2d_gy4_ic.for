C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_gy4_ic.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Groupe:  Interface Commandes: SV2D
C Objet:   PRopriétés GLobales
C Type:    ---
C
C Functions:
C   Public:
C     INTEGER IC_SV2D_GY4_XEQCTR
C     INTEGER IC_SV2D_GY4_XEQMTH
C     CHARACTER*(32) IC_SV2D_GY4_REQCLS
C     INTEGER IC_SV2D_GY4_REQHDL
C   Private:
C     SUBROUTINE IC_SV2D_GY4_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_GY4_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_GY4_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_gy4_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_gy4.fi'

      INTEGER IERR, IRET
      INTEGER HOBJ
      INTEGER HVTBL
      INTEGER HCONF
      INTEGER HGRID, HDLIB, HCLIM, HSOLC, HSOLR, HPRGL, HPRNO, HPREL
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SV2D_GY4_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_SV2D_Y4_GPU'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the GPU configuration</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HCONF)
C     <comment>Handle on the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HGRID)
C     <comment>Handle on the degrees of freedom (unknowns)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HDLIB)
C     <comment>Handle on the boundary conditions</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HCLIM)
C     <comment>Handle on the concentrated solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HSOLC)
C     <comment>Handle on the distributed solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, HSOLR)
C     <comment>Handle on the global properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 7, HPRGL)
C     <comment>Handle on the nodal properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 8, HPRNO)
C     <comment>Handle on the elemental properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 9, HPREL)
      IF (IERR .NE. 0) GOTO 9901

C---     Construis l'objet
      HOBJ = 0
      HVTBL = 0
      IF (ERR_GOOD()) IERR = SV2D_GY4_CTR(HOBJ, HVTBL)
      IF (ERR_GOOD()) IERR = SV2D_GY4_INI(HOBJ,
     &                                    HCONF,
     &                                    HGRID,
     &                                    HDLIB,
     &                                    HCLIM,
     &                                    HSOLC,
     &                                    HSOLR,
     &                                    HPRGL,
     &                                    HPRNO,
     &                                    HPREL)

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the element</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>sv2d_xy4_gpu</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_GY4_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SV2D_GY4_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_GY4_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type SV2D_YS.
C     Le proxy résous directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_GY4_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_GY4_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_gy4_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem_ic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'sv2d_gy4.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
      REAL*8  RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_GY4_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Traitement des properties particulières à la classe</comment>
         IF (.FALSE.) THEN    !!! (PROP .EQ. 'xxxx') THEN

         ELSE
C           <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
            IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_GY4_DTR(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SV2D_GY4_AID()

      ELSE
C        <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_GY4_AID()

9999  CONTINUE
      IC_SV2D_GY4_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_SV2D_GY4_OPBDOT(...) exécute les méthodes statiques
C     sur la classe SV2D_GY4_CNN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_GY4_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_GY4_OPBDOT
CDEC$ ENDIF

      USE SV2D_GY4_M
      USE SV2D_GY4_PSLPRGL_M
      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_gy4_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xbs.fi'
      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'sv2d_gy4.fc'

      INTEGER       IERR
      INTEGER       IVAL
      INTEGER       LDIR, LVAL
      INTEGER       HLIST
      CHARACTER*256 KDIR
      CHARACTER*256 SVAL
      TYPE (SV2D_GY4_SELF_T) :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(HOBJ .EQ. IC_SV2D_GY4_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>get_mem_size_per_element</b> .</comment>
      IF (IMTH .EQ. 'opencl_get_mem_size_per_element') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         IVAL = SV2D_GY4_REQNBPE()
         WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

!C     <comment>The method <b>opencl_get_file_name</b> .</comment>
!      ELSEIF (IMTH .EQ. 'opencl_get_file_name') THEN
!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!
!         !!SVAL = SV2D_GY4_REQNBPE()
!         SVAL = 'E:/dev/H2D2/sv2d/source/__class/OpenCL_Kernel/' //
!     &          'sv2d_clcprno_gpu.cl'
!         WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))

!C     <comment>The method <b>opencl_get_kernel_name</b> .</comment>
!      ELSEIF (IMTH .EQ. 'opencl_get_kernel_name') THEN
!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!
!         !!SVAL = SV2D_GY4_REQNBPE()
!         SVAL = 'sv2d_clcprn_cl'
!         WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))

C     <comment>
C     The method <b>opencl_get_kernels</b> return a list of kernels as
C     strings of the form: kernel_name@kernel_file_name.
C     </comment>
      ELSEIF (IMTH .EQ. 'opencl_get_kernels') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         KDIR = 'E:/dev/H2D2/sv2d/source/__class/OpenCL_Kernel/'
         LDIR = SP_STRN_LEN(KDIR)
         IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLIST)
         IF (ERR_GOOD()) IERR = DS_LIST_INI(HLIST)
         SVAL = 'sv2d_clcprn_cl' // '@' //
     &          KDIR(1:LDIR) // 'sv2d_clcprno_gpu.cl'
         LVAL = SP_STRN_LEN(SVAL)
         IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HLIST, SVAL(1:LVAL))
!         SVAL = 'sv2d_asmxxx_cl' // '@' //
!     &          KDIR(1:LDIR) // 'sv2d_clcprno_gpu.cl'
!         LVAL = SP_STRN_LEN(SVAL)
!         IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HLIST, SVAL(1:LVAL))

         WRITE(IPRM, '(2A,I)') 'H', ',', HLIST
         
C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_GY4_INI_IPRGL(SELF)

         CALL IC_SV2D_GY4_AID()
         IERR = SV2D_XBS_HLPPRGL(SELF%SV2D_XBS_T)
         IERR = SV2D_XBS_HLPPRNO()
         IERR = SV2D_XBS_HLPCLIM()

C     <comment>The method <b>help_prgl</b> prints the global properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prgl') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_GY4_INI_IPRGL(SELF)
         IERR = SV2D_XBS_HLPPRGL(SELF%SV2D_XBS_T)

C     <comment>The method <b>help_prno</b> prints the nodal properties of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_prno') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_XBS_HLPPRNO()

C     <comment>The method <b>help_bc</b> prints the boundary conditions of the class.</comment>
      ELSEIF (IMTH .EQ. 'help_bc') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = SV2D_XBS_HLPCLIM()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_GY4_AID()

9999  CONTINUE
      IC_SV2D_GY4_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_GY4_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_GY4_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_gy4_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>sv2d_xy4_gpu</b> is a prototype for GPU computing (Status: Experimental).
C</comment>
      IC_SV2D_GY4_REQCLS = 'sv2d_xy4_gpu'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_GY4_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_GY4_REQHDL
CDEC$ ENDIF

      USE SV2D_GY4_M
      IMPLICIT NONE

      INCLUDE 'sv2d_gy4_ic.fi'
      INCLUDE 'sv2d_gy4.fi'
C-------------------------------------------------------------------------

      IC_SV2D_GY4_REQHDL = SV2D_GY4_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_HMCOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SV2D_GY4_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_gy4_ic.hlp')
      RETURN
      END

