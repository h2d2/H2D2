C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_pslprgl.for,v 1.7 2015/12/05 14:32:10 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_GY4_PSLPRGL
C   Private:
C     INTEGER SV2D_GY4_PSLPRGL_E
C     INTEGER SV2D_GY4_INI_IPRGL
C
C************************************************************************

      MODULE SV2D_GY4_PSLPRGL_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_PSLPRGL_E(SELF)

      USE SV2D_GY4_M
      USE SV2D_XY4_PSLPRGL_M
      IMPLICIT NONE

      INTEGER SV2D_GY4_PSLPRGL_E
      TYPE (SV2D_GY4_SELF_T), TARGET, INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER I, IE
      INTEGER IERR
      REAL*8, POINTER :: VPRGL(:)
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRG_T),    POINTER :: IPRG

      INTEGER CHKVAL
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      VPRGL=> EDTA%VPRGL

C---     Appel du parent
      IERR = SV2D_XY4_PSLPRGL_E(SELF%SV2D_XY4_SELF_T)

      SV2D_GY4_PSLPRGL_E = ERR_TYP()
      RETURN
      END FUNCTION SV2D_GY4_PSLPRGL_E

C************************************************************************
C Sommaire : SV2D_GY4_INI_IPRGL
C
C Description:
C     La fonction privée SV2D_GY4_INI_IPRGL initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_INI_IPRGL(SELF)

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER SV2D_GY4_INI_IPRGL
      TYPE (SV2D_GY4_SELF_T), TARGET, INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      TYPE (SV2D_IPRG_T), POINTER :: IPRG
C-----------------------------------------------------------------------

C---     Récupère les données
      IPRG => SELF%IPRG

C---     Assigne les indices      
      IPRG%GRAVITE         =  1
      IPRG%LATITUDE        =  2
      IPRG%FCT_CW_VENT     =  3
      IPRG%VISCO_CST       =  4
      IPRG%VISCO_LM        =  5
      IPRG%VISCO_SMGO      =  6
      IPRG%VISCO_BINF      = -1
      IPRG%VISCO_BSUP      = -1
      IPRG%DECOU_HTRG      =  7
      IPRG%DECOU_HMIN      =  8
      IPRG%DECOU_PENA_H    = -1
      IPRG%DECOU_PENA_Q    = -1
      IPRG%DECOU_MAN       =  9
      IPRG%DECOU_UMAX      = 10
      IPRG%DECOU_PORO      = -1
      IPRG%DECOU_AMORT     = 11
      IPRG%DECOU_CON_FACT  = -1
      IPRG%DECOU_GRA_FACT  = -1
      IPRG%DECOU_DIF_NU    = 12
      IPRG%DECOU_DRC_NU    = 13
      IPRG%STABI_PECLET    = 14
      IPRG%STABI_AMORT     = 15
      IPRG%STABI_DARCY     = 16
      IPRG%STABI_LAPIDUS   = 17
      IPRG%CMULT_CON       = -1
      IPRG%CMULT_GRA       = -1
      IPRG%CMULT_PDYN      = -1
      IPRG%CMULT_MAN       = 18
      IPRG%CMULT_VENT      = 19
      IPRG%CMULT_INTGCTR   = -1
      IPRG%PNUMR_PENALITE  = -1
      IPRG%PNUMR_DELPRT    = -1
      IPRG%PNUMR_DELMIN    = -1
      IPRG%PNUMR_OMEGAKT   = -1
      IPRG%PNUMR_PRTPREL   = -1
      IPRG%PNUMR_PRTPRNO   = -1

      SV2D_GY4_INI_IPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_GY4_INI_IPRGL

      END MODULE SV2D_GY4_PSLPRGL_M
      
C************************************************************************
C Sommaire : SV2D_GY4_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_PSLPRGL (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_PSLPRGL
CDEC$ ENDIF

      USE SV2D_GY4_M
      USE SV2D_GY4_PSLPRGL_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => SV2D_GY4_REQSELF(HOBJ)

C---     Fait le traitement
      IERR = SV2D_GY4_INI_IPRGL(SELF)
      IERR = SV2D_GY4_PSLPRGL_E(SELF)

      SV2D_GY4_PSLPRGL = ERR_TYP()
      RETURN
      END
