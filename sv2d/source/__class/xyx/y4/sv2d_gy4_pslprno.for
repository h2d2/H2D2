C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y4_cbs_pslprno.for,v 1.2 2015/11/26 22:23:33 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_GY4_PSLPRNO
C   Private:
C     INTEGER SV2D_GY4_INI_IPRNO
C
C************************************************************************

      MODULE SV2D_GY4_PSLPRNO_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_GY4_INI_IPRNO
C
C Description:
C     La fonction privée SV2D_GY4_INI_IPRNO initialise les indices
C     des propriétés nodales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_INI_IPRNO(SELF)

      USE SV2D_GY4_M
      IMPLICIT NONE

      INTEGER SV2D_GY4_INI_IPRNO
      TYPE (SV2D_GY4_SELF_T), TARGET, INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      TYPE (SV2D_IPRN_T), POINTER :: IPRN
C-----------------------------------------------------------------------

C---     Récupère les données
      IPRN => SELF%IPRN

C---     Assigne les indices      
      IPRN%Z          =  1
      IPRN%N          =  2
      IPRN%ICE_E      =  3
      IPRN%ICE_N      =  4
      IPRN%WND_X      =  5
      IPRN%WND_Y      =  6
      IPRN%P          = -1
      IPRN%U          = -1
      IPRN%V          = -1
      IPRN%H          = -1
      IPRN%COEFF_CNVT = -1
      IPRN%COEFF_GRVT = -1
      IPRN%COEFF_FROT = -1
      IPRN%COEFF_DIFF = -1
      IPRN%COEFF_PE   = -1
      IPRN%COEFF_DMPG = -1
      IPRN%COEFF_VENT = -1
      IPRN%DECOU_DRCY = -1
      IPRN%COEFF_PORO = -1
      IPRN%DECOU_PENA = -1

      SV2D_GY4_INI_IPRNO = ERR_TYP()
      RETURN
      END

      END MODULE SV2D_GY4_PSLPRNO_M
      
C************************************************************************
C Sommaire : SV2D_GY4_PSLPRNO
C
C Description:
C     Traitement post-lecture des propriétés nodales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_GY4_PSLPRNO (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_GY4_PSLPRNO
CDEC$ ENDIF

      USE SV2D_GY4_M
      USE SV2D_GY4_PSLPRNO_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sv2d_gy4.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SV2D_GY4_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

C---     Récupère les données
      SELF => SV2D_GY4_REQSELF(HOBJ)

C---     Fait le traitement
      IERR = SV2D_GY4_INI_IPRNO(SELF)

      SV2D_GY4_PSLPRNO = ERR_TYP()
      RETURN
      END
