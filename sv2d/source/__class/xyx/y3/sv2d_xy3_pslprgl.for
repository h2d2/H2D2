C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y3_cbs_pslprgl.for,v 1.10 2015/11/26 18:54:07 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XY3_PSLPRGL
C   Private:
C     INTEGER SV2D_XY3_PSLPRGL_E
C     INTEGER SV2D_XY3_INI_IPRGL
C
C************************************************************************

      SUBMODULE(SV2D_XY3_M) SV2D_XY3_PSLPRGL_M

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XY3_PSLPRGL_E
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY3_PSLPRGL_CHK(SELF)

      USE SV2D_XBS_M
      USE SV2D_IPRG_M, ONLY: SV2D_IPRG_T

      CLASS(SV2D_XY3_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_enum.fi'
      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER I, IE
      REAL*8  VVAL, VMIN, VMAX, VLATDEG, VLATRAD
      REAL*8, POINTER :: VPRGL(:)
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRG_T),POINTER :: IPRG

      INTEGER CHKVAL
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      VPRGL=> EDTA%VPRGL

C---     Appel du parent
      IERR = SV2D_XBS_PSLPRGL_CHK(SELF)

C---     Coefficient du vent
      IF (ERR_GOOD()) THEN
         I = IPRG%FCT_CW_VENT
         VVAL = NINT(VPRGL(I))
         VMIN = SV2D_CW_INDEFINI+1
         VMAX = SV2D_CW_TAG_LAST-1
         IE = CHKVAL(I, VVAL, VMIN, VMAX, 'MSG_FCT_CW_VENT')
      ENDIF

      SV2D_XY3_PSLPRGL_CHK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY3_PSLPRGL_CHK

C************************************************************************
C Sommaire : SV2D_XY3_PSLPRGL_IPRG
C
C Description:
C     La fonction privée SV2D_XY3_PSLPRGL_IPRG initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY3_PSLPRGL_IPG(SELF)

      USE SV2D_IPRG_M, ONLY: SV2D_IPRG_T

      CLASS(SV2D_XY3_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE (SV2D_IPRG_T), POINTER :: IPRG
C-----------------------------------------------------------------------

C---     Récupère les données
      IPRG => SELF%IPRG

C---     Assigne les indices      
      IPRG%GRAVITE         =  1
      IPRG%LATITUDE        =  2
      IPRG%FCT_CW_VENT     =  3
      IPRG%VISCO_CST       =  4
      IPRG%VISCO_LM        =  5
      IPRG%VISCO_SMGO      =  6
      IPRG%VISCO_BINF      =  7
      IPRG%VISCO_BSUP      =  8
      IPRG%DECOU_HTRG      =  9
      IPRG%DECOU_HMIN      = 10
      IPRG%DECOU_PENA_H    = -1
      IPRG%DECOU_PENA_Q    = -1
      IPRG%DECOU_MAN       = 11
      IPRG%DECOU_UMAX      = 12
      IPRG%DECOU_PORO      = 13
      IPRG%DECOU_AMORT     = 14
      IPRG%DECOU_CON_FACT  = 15
      IPRG%DECOU_GRA_FACT  = 16
      IPRG%DECOU_DIF_NU    = 17
      IPRG%DECOU_DRC_NU    = 18
      IPRG%STABI_PECLET    = 19
      IPRG%STABI_AMORT     = 20
      IPRG%STABI_DARCY     = 21
      IPRG%STABI_LAPIDUS   = 22
      IPRG%CMULT_CON       = 23
      IPRG%CMULT_GRA       = 24
      IPRG%CMULT_PDYN      = -1
      IPRG%CMULT_MAN       = 25
      IPRG%CMULT_VENT      = 26
      IPRG%CMULT_INTGCTR   = 27
      IPRG%PNUMR_PENALITE  = 28
      IPRG%PNUMR_DELPRT    = 29
      IPRG%PNUMR_DELMIN    = -1
      IPRG%PNUMR_OMEGAKT   = 30
      IPRG%PNUMR_PRTPREL   = 31
      IPRG%PNUMR_PRTPRNO   = 32

      SV2D_XY3_PSLPRGL_IPG = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY3_PSLPRGL_IPG

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY3_PSLPRGL (SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY3_PSLPRGL
CDEC$ ENDIF

      CLASS(SV2D_XY3_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Fait le traitement
      IERR = SELF%PSLPRGL_IPG()
      IERR = SELF%PSLPRGL_CHK()

      SV2D_XY3_PSLPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY3_PSLPRGL

      END SUBMODULE SV2D_XY3_PSLPRGL_M
