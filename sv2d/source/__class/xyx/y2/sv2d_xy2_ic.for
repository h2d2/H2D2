C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xy2_ic.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Groupe:  Interface Commandes: SV2D
C Objet:   PRopriétés GLobales
C Type:    ---
C Functions:
C   Public:
C     INTEGER IC_SV2D_XY2_XEQCTR
C     INTEGER IC_SV2D_XY2_XEQMTH
C     CHARACTER*(32) IC_SV2D_XY2_REQCLS
C     INTEGER IC_SV2D_XY2_REQHDL
C   Private:
C     SUBROUTINE IC_SV2D_XY2_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SV2D_XY2_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_XY2_XEQCTR
CDEC$ ENDIF

      USE SV2D_XY2_M
      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sv2d_xy2_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HOBJ
      INTEGER HVTBL
      INTEGER HCONF
      INTEGER HGRID, HDLIB, HCLIM, HSOLC, HSOLR, HPRGL, HPRNO, HPREL
      CHARACTER*(256) NOM
      CLASS(SV2D_XY2_T), POINTER :: OELE
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SV2D_XY2_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_SV2D_Y2'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRID)
C     <comment>Handle on the degrees of freedom (unknowns)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HDLIB)
C     <comment>Handle on the boundary conditions</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HCLIM)
C     <comment>Handle on the concentrated solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HSOLC)
C     <comment>Handle on the distributed solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HSOLR)
C     <comment>Handle on the global properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, HPRGL)
C     <comment>Handle on the nodal properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 7, HPRNO)
C     <comment>Handle on the elemental properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 8, HPREL)
      IF (IERR .NE. 0) GOTO 9901

C---     Construis l'objet
      IF (ERR_GOOD()) OELE => SV2D_XY2_CTR()
      IF (ERR_GOOD()) IERR = OELE%INI(HCONF,
     &                                HGRID,
     &                                HDLIB,
     &                                HCLIM,
     &                                HSOLC,
     &                                HSOLR,
     &                                HPRGL,
     &                                HPRNO,
     &                                HPREL)

C---     Construis le proxy     
      HOBJ = 0
      IERR = LM_HELE_CTR(HOBJ, OELE)
      
C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the element</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>sv2d_xy2</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SV2D_XY2_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SV2D_XY2_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_XY2_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_XY2_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_xy2_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C     The class <b>sv2d_xy2</b> is a finite element for the 2D St-Venant equations
C     (shallow water) written in conservation form.
C     <br>
C     The drying/wetting formulation is Y2 with a linear interpolation of
C     properties between wet and dry regions. A slight hysteresis is also build in.
C     <br>
C     The wind function is build-in and the wind is absolute (i.e. not relative to
C     the current).
C     <br>
C     The degree of freedom are node numbered.
C</comment>
      IC_SV2D_XY2_REQCLS = 'sv2d_xy2'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SV2D_XY2_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SV2D_XY2_REQHDL
CDEC$ ENDIF

      USE SV2D_XY2_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xy2_ic.fi'
!      INCLUDE 'sv2d_xy2.fi'
C-------------------------------------------------------------------------

      IC_SV2D_XY2_REQHDL = 0 ! SV2D_XY2_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_HMCOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SV2D_XY2_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xy2_ic.hlp')
      RETURN
      END

