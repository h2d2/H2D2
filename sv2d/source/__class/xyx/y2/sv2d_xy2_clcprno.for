C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y2_cbs_clcprno.for,v 1.26 2015/11/26 22:23:33 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XY2_CLCPRNO
C   Private:
C     INTEGER SV2D_XY2_CLCPRNEV
C     INTEGER SV2D_XY2_CLCPRNES
C     INTEGER SV2D_XY2_CLCPRN_1N
C
C************************************************************************

      SUBMODULE(SV2D_XY2_M) SV2D_XY2_CLCPRNO_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XY2_CLCPRN_1N
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRG        Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     En PRFA, un noeud milieu va se retrouver rehaussé si un noeud sommet
C     est découvert. Il y a un impact sur le coefficient de frottement
C     qui est diminué.
C     ULIM vise à "freiner" les fortes vitesses, celles au-delà de UMAX.
C     Jusqu'à UMAX, la loi est linéaire, puis exp 1/3
C************************************************************************
      INTEGER FUNCTION SV2D_XY2_CLCPRN_1N(VPRN,
     &                                    VDLN,
     &                                    IPRN,
     &                                    XPRG)

      USE SV2D_IPRN_M
      USE SV2D_XPRG_M

      REAL*8, INTENT(INOUT) :: VPRN (:)
      REAL*8, INTENT(IN)    :: VDLN (:)
      TYPE(SV2D_IPRN_T), INTENT(IN) :: IPRN
      TYPE(SV2D_XPRG_T), INTENT(IN) :: XPRG

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      REAL*8, PARAMETER :: R_4_3N = -4.0D0/3.0D0
      REAL*8, PARAMETER :: R_1_3  =  1.0D0/3.0D0

      REAL*8  ALFA
      REAL*8  H1, H2
      REAL*8  PRFE, PRFA, UN_PRFA
      REAL*8  PORO, VMAN, VFLN, FCVT, FGRA, VDIF, VDRC
      REAL*8  NTOT, FROTT
      REAL*8  VX, VY, VMOD
      REAL*8  STTEL
      REAL*8  VN1, VN2

      INTEGER NPRN
D     LOGICAL COUVERT, DECOUVERT, COUVRE
      LOGICAL DECOUVRE
C-----------------------------------------------------------------------
      REAL*8 ULIM, UEXP, U
      UEXP(U) = (U-XPRG%DECOU_UMAX+UN)**R_1_3 + XPRG%DECOU_UMAX-UN
      ULIM(U) = MIN(UEXP(MAX(U,XPRG%DECOU_UMAX)), U)
C-----------------------------------------------------------------------

      NPRN = SIZE(VPRN)

C---     Profondeur
      PRFE = VPRN(IPRN%V)                 ! Prof effective nodale
      PRFA = VPRN(IPRN%H)                 ! Prof absolue linéaire
      STTEL= VPRN(NPRN)                   ! État des éléments adhérents
      UN_PRFA = UN / PRFA                 ! Inverse prof absolue

C---     Alfa
D     COUVERT   = (PRFE .GE. XPRG%DECOU_HTRG .AND. STTEL .GE. 0.0D0)
D     COUVRE    = (PRFE .LT. XPRG%DECOU_HTRG .AND. STTEL .GE. 0.0D0)
D     DECOUVERT = (PRFE .LE. XPRG%DECOU_HMIN .AND. STTEL .LT. 0.0D0)
      DECOUVRE  = (PRFE .GT. XPRG%DECOU_HMIN .AND. STTEL .LT. 0.0D0)
      IF (DECOUVRE) THEN
         H1 = XPRG%DECOU_HMIN
         H2 = XPRG%DECOU_HTRG - XPRG%DECOU_DHST
      ELSE
         H1 = XPRG%DECOU_HMIN + XPRG%DECOU_DHST
         H2 = XPRG%DECOU_HTRG
      ENDIF
D     CALL ERR_ASR(H2 .GT. H1)
      ALFA = (PRFE-H1)/(H2-H1)
      ALFA = MIN(UN, MAX(ZERO, ALFA))
D     CALL ERR_ASR((COUVERT .AND. ALFA .EQ. UN) .OR.
D    &             (DECOUVERT .AND. ALFA .EQ. ZERO) .OR.
D    &             (ALFA .LE. UN .AND. ALFA .GE. ZERO))
      VN1  = UN - ALFA
      VN2  = ALFA

C---     Module de la vitesse
      VX = VDLN(1) * UN_PRFA
      VY = VDLN(2) * UN_PRFA
      VMOD = HYPOT(VX, VY)

C---     Manning global
      NTOT = HYPOT(VPRN(IPRN%N), VPRN(IPRN%ICE_N))

C---     Paramètres variables pour le découvrement
      VMOD = VN1*ULIM(VMOD)          + VN2*VMOD
      PORO = VN1*XPRG%DECOU_PORO     + VN2 !*UN
      VFLN = VN1*XPRG%DECOU_AMORT    + VN2*XPRG%STABI_AMORT
      VMAN = VN1*XPRG%DECOU_MAN      + VN2*XPRG%CMULT_MAN*NTOT
      FCVT = VN1*XPRG%DECOU_CON_FACT + VN2*XPRG%CMULT_CON
      FGRA = VN1*XPRG%DECOU_GRA_FACT + VN2*XPRG%CMULT_GRA
      VDIF = VN1*XPRG%DECOU_DIF_NU  !+ VN2*0.0
      VDRC = VN1*XPRG%DECOU_DRC_NU   + VN2*XPRG%STABI_DARCY

C---     Frottement
!      L'utilisation de la masse lumpée pour découpler le frottement
!      mène à des résultats très différents d'Hydrosim.
!      FROTT = XPRG%GRAVITE*VMAN*VMAN * UMOD * (UN_PRFA**R_4_3)
      FROTT = VFLN + XPRG%GRAVITE*VMAN*VMAN * VMOD * PRFA**R_4_3N    ! Hydrosim

C---     Valeurs nodales
      VPRN(IPRN%U)          = VDLN(1) * UN_PRFA ! U
      VPRN(IPRN%V)          = VDLN(2) * UN_PRFA ! V
      VPRN(IPRN%H)          = PRFA              ! Prof absolue
      VPRN(IPRN%COEFF_CNVT) = FCVT              ! Facteur de convection
      VPRN(IPRN%COEFF_GRVT) = FGRA              ! Facteur de gravité
      VPRN(IPRN%COEFF_FROT) = FROTT             ! g n2 |u| / H**(4/3)
      VPRN(IPRN%DECOU_DIFF) = VDIF              ! Diffusion de decou.
      VPRN(IPRN%DECOU_DRCY) = VDRC              ! Darcy
      VPRN(IPRN%COEFF_PORO) = PORO

      SV2D_XY2_CLCPRN_1N = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY2_CLCPRN_1N

C************************************************************************
C Sommaire: SV2D_XY2_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C     CALCUL DES PROPRIETES NODALES DÉPENDANT DE LA SOLUTION (DS)
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  cw(w).|w|
C                    DS    16)  POROSITÉ
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Par rapport à SV2D_YS_CBS_CLCPRNO, on ajoute l'hystérèse sur le
C     découvrement.
C
C     L'imposition des noeuds milieux devrait être faite dans une
C     fonction séparée, pour bien séparer les concepts
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY2_CLCPRNO(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XY2_CLCPRNO
CDEC$ ENDIF

      USE SV2D_IPRN_M, ONLY: SV2D_IPRN_T
      USE SV2D_XPRG_M, ONLY: SV2D_XPRG_T

      CLASS(SV2D_XY2_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'

      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA
      INTEGER IERR
      INTEGER IC, IE, IN, INC
      INTEGER IPV, IPH, IPF, IPC
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      LOGICAL COUVERT, DECOUVERT
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      INTEGER,POINTER :: KNGV(:,:)
      REAL*8, POINTER :: VPRG(:)
      REAL*8, POINTER :: VPRN(:,:)
      REAL*8, POINTER :: VDLG(:,:)
C-----------------------------------------------------------------------

      IERR = ERR_OK

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IC, IE, IN, INC)
!$omp& private(IPV, IPH, IPF, IPC)
!$omp& private(SELF, GDTA, EDTA, IPRN, XPRG)
!$omp& private(KNGV, VPRN, VDLG)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
!$omp& private(BATHY, EPAIGL, PROF)
!$omp& private(PRFE, PRFA)
!$omp& private(COUVERT, DECOUVERT)

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG
      KNGV => GDTA%KNGV
      VPRN => EDTA%VPRNO
      VDLG => EDTA%VDLG

C---     Indices dans VPRNO
      IPV = IPRN%V
      IPH = IPRN%H
      IPF = IPRN%COEFF_FROT
      IPC = IPRN%COEFF_CNVT

C---     Impose les niveaux d'eau sur les noeuds milieux
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VDLG(3,NO2) = (VDLG(3,NO1)+VDLG(3,NO3))*UN_2
         VDLG(3,NO4) = (VDLG(3,NO3)+VDLG(3,NO5))*UN_2
         VDLG(3,NO6) = (VDLG(3,NO5)+VDLG(3,NO1))*UN_2
      ENDDO
!$omp end do
      ENDDO

C---     Profondeurs effective et absolue
!$omp  do
      DO IN=1, GDTA%NNL
         BATHY  = VPRN(IPRN%Z,IN)
         EPAIGL = 0.9D0 * VPRN(IPRN%ICE_E,IN)
         PROF   = VDLG(3,IN) - BATHY
         EPAIGL = MIN(EPAIGL, PROF)
         EPAIGL = MAX(EPAIGL, ZERO)
         PRFE   = PROF - EPAIGL                 ! Prof effective
         PRFA   = MAX(PRFE, XPRG%DECOU_HMIN)    ! Prof absolue
         VPRN(IPRN%V,IN) = PRFE                 ! Prof effective temp.
         VPRN(IPRN%H,IN) = PRFA                 ! Prof absolue
         VPRN(IPRN%COEFF_FROT,IN) = 0
         VPRN(IPRN%COEFF_CNVT,IN) = 0
      ENDDO
!$omp end do

C---     Impose les profondeurs sur les noeuds milieux
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VPRN(IPV,NO2) = (VPRN(IPV,NO1)+VPRN(IPV,NO3))*UN_2
         VPRN(IPV,NO4) = (VPRN(IPV,NO3)+VPRN(IPV,NO5))*UN_2
         VPRN(IPV,NO6) = (VPRN(IPV,NO5)+VPRN(IPV,NO1))*UN_2

C           Profondeur linéaire
C           Noeud rehaussé si un noeud sommet est découvert
         VPRN(IPH,NO2) = (VPRN(IPH,NO1)+VPRN(IPH,NO3))*UN_2
         VPRN(IPH,NO4) = (VPRN(IPH,NO3)+VPRN(IPH,NO5))*UN_2
         VPRN(IPH,NO6) = (VPRN(IPH,NO5)+VPRN(IPH,NO1))*UN_2

C---        État de l'élément et assemblage sur les noeuds
         COUVERT  = (VPRN(IPRN%V,NO1) .GE. XPRG%DECOU_HTRG .AND.
     &               VPRN(IPRN%V,NO3) .GE. XPRG%DECOU_HTRG .AND.
     &               VPRN(IPRN%V,NO5) .GE. XPRG%DECOU_HTRG)
         DECOUVERT= (VPRN(IPRN%V,NO1) .LE. XPRG%DECOU_HMIN .AND.
     &               VPRN(IPRN%V,NO3) .LE. XPRG%DECOU_HMIN .AND.
     &               VPRN(IPRN%V,NO5) .LE. XPRG%DECOU_HMIN)
         INC = 0
         IF (COUVERT) THEN
            INC = 1
         ELSEIF (DECOUVERT) THEN
            INC = -1
         ENDIF
         VPRN(IPF,NO1) = VPRN(IPF,NO1) + INC
         VPRN(IPC,NO1) = VPRN(IPC,NO1) + 1
         VPRN(IPF,NO2) = VPRN(IPF,NO2) + INC
         VPRN(IPC,NO2) = VPRN(IPC,NO2) + 1
         VPRN(IPF,NO3) = VPRN(IPF,NO3) + INC
         VPRN(IPC,NO3) = VPRN(IPC,NO3) + 1
         VPRN(IPF,NO4) = VPRN(IPF,NO4) + INC
         VPRN(IPC,NO4) = VPRN(IPC,NO4) + 1
         VPRN(IPF,NO5) = VPRN(IPF,NO5) + INC
         VPRN(IPC,NO5) = VPRN(IPC,NO5) + 1
         VPRN(IPF,NO6) = VPRN(IPF,NO6) + INC
         VPRN(IPC,NO6) = VPRN(IPC,NO6) + 1
      ENDDO
!$omp end do
      ENDDO

C---     Boucle sur les noeuds
!$omp  do
      DO IN=1, GDTA%NNL
         VPRN(EDTA%NPRNO,IN) = VPRN(IPF,IN) / VPRN(IPC,IN)
         IERR = SV2D_XY2_CLCPRN_1N(VPRN(:,IN), VDLG(:,IN), IPRN, XPRG)
      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      SV2D_XY2_CLCPRNO = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY2_CLCPRNO

C************************************************************************
C Sommaire: SV2D_XY2_CLCPRNEV
C
C Description:
C     Calcul des propriétés nodales d'un élément de volume,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY2_CLCPRNEV(SELF, VDLE, VPRN)

      USE SV2D_IPRN_M, ONLY: SV2D_IPRN_T
      USE SV2D_XPRG_M, ONLY: SV2D_XPRG_T

      CLASS(SV2D_XY2_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT):: VDLE(:,:)
      REAL*8, INTENT(OUT)  :: VPRN(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG

C---     Indices dans VPRNO
      IPV = IPRN%V
      IPH = IPRN%H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2
      VDLE(3,NO4) = (VDLE(3,NO3)+VDLE(3,NO5))*UN_2
      VDLE(3,NO6) = (VDLE(3,NO5)+VDLE(3,NO1))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,GDTA%NNELV
         BATHY   = VPRN(IPRN%Z,IN)
         EPAIGL  = 0.9D0 * VPRN(IPRN%ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, XPRG%DECOU_HMIN)   ! Prof absolue
         VPRN(IPRN%V,IN) = PRFE           ! Prof effective temp.
         VPRN(IPRN%H,IN) = PRFA           ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRN(IPV,NO2) = (VPRN(IPV,NO1)+VPRN(IPV,NO3))*UN_2
      VPRN(IPV,NO4) = (VPRN(IPV,NO3)+VPRN(IPV,NO5))*UN_2
      VPRN(IPV,NO6) = (VPRN(IPV,NO5)+VPRN(IPV,NO1))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRN(IPH,NO2) = (VPRN(IPH,NO1)+VPRN(IPH,NO3))*UN_2
      VPRN(IPH,NO4) = (VPRN(IPH,NO3)+VPRN(IPH,NO5))*UN_2
      VPRN(IPH,NO6) = (VPRN(IPH,NO5)+VPRN(IPH,NO1))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,GDTA%NNELV
         IERR = SV2D_XY2_CLCPRN_1N(VPRN(:,IN), VDLE(:,IN), IPRN, XPRG)
      ENDDO

      SV2D_XY2_CLCPRNEV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY2_CLCPRNEV

C************************************************************************
C Sommaire: SV2D_XY2_CLCPRNES
C
C Description:
C     Calcul des propriétés nodales d'un élément de surface,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XY2_CLCPRNES(SELF, VDLE, VPRN)

      USE SV2D_IPRN_M, ONLY: SV2D_IPRN_T
      USE SV2D_XPRG_M, ONLY: SV2D_XPRG_T

      CLASS(SV2D_XY2_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT):: VDLE(:,:)
      REAL*8, INTENT(OUT)  :: VPRN(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      TYPE (SV2D_XPRG_T),POINTER :: XPRG

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      XPRG => SELF%XPRG

C---     Indices dans VPRNO
      IPV = IPRN%V
      IPH = IPRN%H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2

C---     Profondeurs effective et absolue
      DO IN=1,GDTA%NNELS
         BATHY   = VPRN(IPRN%Z,IN)
         EPAIGL  = 0.9D0 * VPRN(IPRN%ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, XPRG%DECOU_HMIN)   ! Prof absolue
         VPRN(IPRN%V,IN) = PRFE           ! Prof effective temp.
         VPRN(IPRN%H,IN) = PRFA           ! Prof absolue
      ENDDO

C---     Impose les profondeurs sur les noeuds milieux
      VPRN(IPV,NO2) = (VPRN(IPV,NO1) + VPRN(IPV,NO3))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRN(IPH,NO2) = (VPRN(IPH,NO1) + VPRN(IPH,NO3))*UN_2

C---     Boucle sur les noeuds
      DO IN=1,GDTA%NNELS
         IERR = SV2D_XY2_CLCPRN_1N(VPRN(:,IN), VDLE(:,IN), IPRN, XPRG)
      ENDDO

      SV2D_XY2_CLCPRNES = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XY2_CLCPRNES

      END SUBMODULE SV2D_XY2_CLCPRNO_M
