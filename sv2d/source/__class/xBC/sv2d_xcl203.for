C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C  Astronomical tide Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL203_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL203_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL203_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL203_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL203_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL203_HLP
      END TYPE SV2D_XCL203_T

      PUBLIC :: SV2D_XCL203_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL203_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL203_CTR</code> construit une C.L.
C     de type <code>203</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL203_CTR() RESULT(SELF)

      TYPE(SV2D_XCL203_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL203_CTR

C************************************************************************
C Sommaire:  SV2D_XCL203_COD
C
C Description:
C     La méthode <code>SV2D_XCL203_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL203_COD(SELF, KDIMP)

      CLASS(SV2D_XCL203_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl203.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl203.fc'

      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN
      INTEGER IVDEB, IVFIN
      INTEGER NVAL
      REAL*8  C
      TYPE (SV2D_XBS_SELF_T),POINTER :: SELF
      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
      TYPE (LM_EDTA_DATA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL203_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2, IL)

C---     Contrôles généraux des valeurs
      NVAL = EDTA%KCLCND(4, IC) - EDTA%KCLCND(3, IC) + 1
      IF (GDTA%KCLLIM(4,IL)-GDTA%KCLLIM(3,IL)+1 .LT. 1) GOTO 9900
      IF (MOD(NVAL, 3) .NE. 1) GOTO 9902
      IF (NVAL/3       .LT. 1) GOTO 9902

C---     Contrôles les composantes
      IVDEB = LBOUND(SELF%VCND, 1)
      IVFIN = UBOUND(SELF%VCND, 1)
      DO IV=IVDEB+1,IVFIN,3
         C = SELF%VCND(IV)       ! composante
         IF (.NOT. SP_TIDE_CMPEXIST(NINT(C))) GOTO 9904
      ENDDO

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTIENT', ': ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTEND', ': ', '(1 + N*3)'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A)') 'ERR_UNKNOWN_TIDE_COMPONENT', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL203_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL203_PRC
C
C Description:
C     La méthode <code>SV2D_XCL203_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        T0
C        (c,a,p),
C        (c,a,p), ...
C     )
C************************************************************************
      INTEGER FUNCTION SV2D_XCL203_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL203_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'eacdcl.fi'

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_DATA_T), INTENT(IN)    :: GDTA
      TYPE (LM_EDTA_DATA_T), INTENT(INOUT) :: EDTA

      INCLUDE 'sv2d_xcl203.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'
      INCLUDE 'sv2d_xcl203.fc'

      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
      REAL*8  H, C, A, D
      REAL*8  T, T0
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL203_TYP)
C-----------------------------------------------------------------------

      IVDEB = LBOUND(SELF%VCND, 1)
      IVFIN = UBOUND(SELF%VCND, 1)

      T0 = SELF%VCND(IVDEB)   ! Temps de ref.
      T  = ELEM%EDTA%TEMPS    ! Temps de la simu.
      IVDEB = IVDEB + 1

      H = 0.0D0
      DO IV=IVDEB,IVFIN,3
         C = SELF%VCND(IV+0)     ! composante
         A = SELF%VCND(IV+1)     ! amplitude
         D = SELF%VCND(IV+2)     ! déphasage
         H = H + SP_TIDE_TTDE_HD(NINT(C), A, D, T, T0, .TRUE.)
      ENDDO
      CALL ERR_ASR(.FALSE.)

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I=INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = H
      ENDDO

      SV2D_XCL203_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL203_PRC

C************************************************************************
C Sommaire: SV2D_XCL203_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL203_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL203_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl203.hlp')

      SV2D_XCL203_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL203_HLP

C************************************************************************
C Sommaire:  SV2D_XCL203_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL203_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL203_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>203</b>: <br>
C  Astronomical tide Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            T0
C            (c,a,p),
C            (c,a,p), ...
C         )</pre>
C  where T0 is the reference time,
C  c the code name of the tide constituent,
C  a the amplitude and
C  p the phase shift in degrees.
C  The water level H is the sum of all constituents contributions.
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 203</li>
C     <li>Values: T0, c, a, p, c, a, p, ... </li>
C     <li>Units: [s] ([-] [m] [deg])</li>
C     <li>Example:  203  1234567890.0   m2  2.0  -74.8</li>
C  </ul>
C</comment>

      SV2D_XCL203_CMD = 'bc_type_203'
      RETURN
      END FUNCTION SV2D_XCL203_CMD
      
      END MODULE SV2D_XCL203_M
