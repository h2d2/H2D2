C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xcl129.for,v 1.24 2016/01/28 17:49:08 secretyv Exp $
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau actuel.
C     Formulation Manning variable
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL129_000
C     INTEGER SV2D_XCL129_999
C     INTEGER SV2D_XCL129_COD
C     INTEGER SV2D_XCL129_ASMKU
C     INTEGER SV2D_XCL129_ASMM
C     INTEGER SV2D_XCL129_ASMMU
C   Private:
C     INTEGER SV2D_XCL129_INIVTBL
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL129_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl129.fc'

      DATA SV2D_XCL129_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL129_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL129_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl129.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL129_INIVTBL()

      SV2D_XCL129_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL129_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL129_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL129_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl129.fi'
!!!      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
!!!      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl129.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL129_HLP
      EXTERNAL SV2D_XCL129_COD
      EXTERNAL SV2D_XCL129_ASMKU
      EXTERNAL SV2D_XCL129_ASMM
      EXTERNAL SV2D_XCL129_ASMMU
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL129_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL129_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL129_TPN(1:LTPN), SV2D_XCL129_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL129_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL129_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL129_TYP,
     &                     SV2D_XCL129_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD, SV2D_XCL129_TYP,
     &                    SV2D_XCL129_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKU,SV2D_XCL129_TYP,
     &                    SV2D_XCL129_ASMKU)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMM,SV2D_XCL129_TYP,
     &                    SV2D_XCL129_ASMM)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMMU,SV2D_XCL129_TYP,
     &                    SV2D_XCL129_ASMMU)

      SV2D_XCL129_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL129_COD
C
C Description:
C     La fonction SV2D_XCL129_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_COD(IL, BELF)

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl129.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 2,-1,
     &                            1,-1,
     &                            1,-1,
     &                            2, 2/), (/2, 4/))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC   = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL129_TYP)
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, KCLDIM)

C---     Assigne les codes
      INDEB = GDTA%KCLLIM(3, IL)
      INFIN = GDTA%KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               SV2D_XCL_DEBIT)
      ENDDO

      SV2D_XCL129_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL129_ASMKU
C
C Description:
C     La fonction SV2D_XCL129_ASMKU assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C     L'idée est de prolonger par un élément Q4 rectangulaire,
C     normal à l'élément de frontière, et de longueur L. Ce Q4 est
C     splitté en 2 sous-éléments pour s'adapter au T6L. La continuité
C     est intégrée dans le repère normal-tangent.
C
C     Ici, u est la vitesse normale et v la tangentielle. On a u3, u4 qui
C     sont les valeurs imposées (calculées par répartition), et on a v3 = 0
C     et v4 = 0.
C
C     Pour répartir le débit total, il faut un niveau. Celui-ci est calculé
C     comme le niveau moyen sur la limite.
C************************************************************************
      FUNCTION SV2D_XCL129_ASMKU(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl129.fc'

      REAL*8  HQ4
      REAL*8  F1, F2, F3, P1, P2, P3
      REAL*8  H1, H3, HM, BH
      REAL*8  QTOT, QSPEC
      REAL*8  QS, QG
      REAL*8  QN1, QN2, QN3, QN4, QN5, QN6
      REAL*8  QN_E1, QN_E2
      REAL*8  QT1, QT2, QT3
      REAL*8  QT_L
      REAL*8  C, M(2)
      REAL*8  VNX, VNY, VTX, VTY, VKX, VEY
      REAL*8  VL(2), VG(2)
      INTEGER KLOCE (2)
      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IV
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO2, NO3
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),   POINTER :: IPRN

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL129_TYP)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .GE. 1)
      IV = EDTA%KCLCND(3,IC)
      QTOT = EDTA%VCLCNV(IV)
      HQ4  = EDTA%VCLCNV(IV+1)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

C---     Niveau moyen pour la répartition
      HM = ZERO
      BH = ZERO     ! Largeur
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         H1 = EDTA%VDLG(3, NO1)
         H3 = EDTA%VDLG(3, NO3)

C---        Intègre
         HM = HM +      GDTA%VDJS(3,IE)*(H1+H3)
         BH = BH + DEUX*GDTA%VDJS(3,IE)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL = (/ HM, BH /)
      CALL MPI_ALLREDUCE(VL, VG, 2, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      HM = VG(1)
      BH = VG(2)
D     CALL ERR_ASR(BH .GT. ZERO)
      HM = HM / BH

C---     Surface mouillée
      QS = 0.0D0
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO2 = GDTA%KNGS(2,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Valeurs nodales
         F1 = MAX(     EDTA%VPRNO(IPRN%N, NO1), PETIT)  ! Manning
         P1 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO1), PETIT)  ! Profondeur
         F2 = MAX(     EDTA%VPRNO(IPRN%N, NO2), PETIT)
         P2 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO2), PETIT)
         F3 = MAX(     EDTA%VPRNO(IPRN%N, NO3), PETIT)
         P3 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO3), PETIT)

C---        Règle de répartition d'après Manning
         QN1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         QN2 = (P2**CINQ_TIER) / F2
         QN3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + UN_2*GDTA%VDJS(3,IE)*(QN1+QN2+QN2+QN3)
299      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL = (/ QS, 0.0D0 /)
      CALL MPI_ALLREDUCE(VL, VG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS = VG(1)
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB, IEFIN
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO2 = GDTA%KNGS(2,IE)
         NO3 = GDTA%KNGS(3,IE)

C---        Coefficient
         VTX =  GDTA%VDJS(1,IE)
         VTY =  GDTA%VDJS(2,IE)
         VNY = -GDTA%VDJS(1,IE)    ! VNY = -VTX
         VNX =  GDTA%VDJS(2,IE)    ! VNX =  VTY
         VKX =  UN_2 * HQ4         ! kx * DETJ = (A*B) / A
         VEY =  GDTA%VDJS(3,IE)    ! ey * DETJ = (A*B) / B
         C = - UN_24

C---        Valeurs nodales
         F1 = MAX(     EDTA%VPRNO(IPRN%N, NO1), PETIT)  ! Manning
         P1 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO1), PETIT)  ! Profondeur
         F2 = MAX(     EDTA%VPRNO(IPRN%N, NO2), PETIT)
         P2 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO2), PETIT)
         F3 = MAX(     EDTA%VPRNO(IPRN%N, NO3), PETIT)
         P3 = MAX(HM - EDTA%VPRNO(IPRN%Z, NO3), PETIT)

C---        Règle de répartition d'après Manning
         QN6 = QSPEC * (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         QN5 = QSPEC * (P2**CINQ_TIER) / F2
         QN4 = QSPEC * (P3**CINQ_TIER) / F3

C---        DDL
         QN1 = EDTA%VDLG(1,NO1)*VNX + EDTA%VDLG(2,NO1)*VNY
         QN2 = EDTA%VDLG(1,NO2)*VNX + EDTA%VDLG(2,NO2)*VNY
         QN3 = EDTA%VDLG(1,NO3)*VNX + EDTA%VDLG(2,NO3)*VNY
         QT1 = EDTA%VDLG(1,NO1)*VTX + EDTA%VDLG(2,NO1)*VTY
         QT2 = EDTA%VDLG(1,NO2)*VTX + EDTA%VDLG(2,NO2)*VTY
         QT3 = EDTA%VDLG(1,NO3)*VTX + EDTA%VDLG(2,NO3)*VTY

C---        Assemblage du vecteur global (mixte Q4-Q6L)
         QN_E1 = QN1 + QN2 + QN5 + QN6
         QN_E2 = QN2 + QN3 + QN4 + QN5
         QT_L  = QT1 + QT2 + QT2 + QT3
         M(1) = C * (-4*VKX*(QT_L) - VEY*(5*QN_E1 +   QN_E2))
         M(2) = C * ( 4*VKX*(QT_L) - VEY*(  QN_E1 + 5*QN_E2))

         KLOCE(:) = (/ EDTA%KLOCN(3, NO1), EDTA%KLOCN(3, NO3) /)
         IERR = SP_ELEM_ASMFE(2, KLOCE, M, VFG)
!         WRITE(LOG_BUF,*) I, M(1), M(2)
!         CALL LOG_ECRIS(LOG_BUF)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL129_TYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_DEBIT: ', QTOT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_SURFACE: ', QS
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL129_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL129_ASMM
C
C Description:
C     La fonction SV2D_XCL129_ASMM assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C M = 1/9 [4, 2, 1, 2]
C         [2, 4, 2, 1]
C         [1, 2, 4, 2]
C         [2, 1, 2, 4]
C************************************************************************
      FUNCTION SV2D_XCL129_ASMM  (IL,
     &                            BELF,
     &                            HMTX,
     &                            F_ASM)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl129.fc'

      REAL*8  VKE   (2,2)
      REAL*8  HQ4, COEF
      INTEGER KLOCE (2)
      INTEGER IERR
      INTEGER I, IC, IV, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRN_T),   POINTER :: IPRN
C-----------------------------------------------------------------
D     CALL ERR_PRE(SELF%EDTA%KCLCND(2,SELF%GDTA%KCLLIM(2,IL)) .EQ.
D    &             SV2D_XCL129_TYP)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      IPRN => SELF%IPRN

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .GE. 1)
      IV = EDTA%KCLCND(3,IC)
      HQ4  = EDTA%VCLCNV(IV+1)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

      DO I = IEDEB, IEFIN
         IES = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)
         COEF = UN_18 * GDTA%VDJS(3,IES) * HQ4 ! 2 / (4*9)

C---        Matrice masse
         VKE(1,1) = COEF+COEF
         VKE(2,2) = COEF
         VKE(1,2) = COEF
         VKE(2,2) = COEF+COEF

C---       Assemblage de la matrice
         KLOCE(:) = (/ EDTA%KLOCN(3, NO1), EDTA%KLOCN(3, NO3) /)
         IERR = F_ASM(HMTX, 2, KLOCE, VKE)
      ENDDO

      SV2D_XCL129_ASMM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL129_ASMMU
C
C Description:
C     La fonction SV2D_XCL129_ASMMU assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     IL                Indice de la condition
C     GDTA              Données géométriques
C     EDTA              Données d'approximation
C
C Sortie:
C     VFG               Vecteur force global mis à jour
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_ASMMU(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG   (:, :)

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xcl129.fc'

      REAL*8  HQ4, COEF
      REAL*8  H1, H3
      REAL*8  VFE  (2)
      INTEGER KLOCE(2)
      INTEGER IERR
      INTEGER I, IC, IV, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------
D     CALL ERR_PRE(SELF%EDTA%KCLCND(2,SELF%GDTA%KCLLIM(2,IL)) .EQ.
D    &             SV2D_XCL129_TYP)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .GE. 1)
      IV = EDTA%KCLCND(3,IC)
      HQ4  = EDTA%VCLCNV(IV+1)

      IEDEB = GDTA%KCLLIM(5, IL)
      IEFIN = GDTA%KCLLIM(6, IL)

      DO I = IEDEB, IEFIN
         IES = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)
         COEF = UN_18 * GDTA%VDJS(3,IES) * HQ4 ! 2 / (4*9)

C---        Degrés de liberté
         H1 = EDTA%VDLG(3, NO1)
         H3 = EDTA%VDLG(3, NO3)

C---        Matrice masse
         VFE(1) = COEF * (H1+H1 + H3)
         VFE(2) = COEF * (H1 + H3+H3)

C---       Assemblage de la matrice
         KLOCE(:) = (/ EDTA%KLOCN(3, NO1), EDTA%KLOCN(3, NO3) /)
         IERR = SP_ELEM_ASMFE(2, KLOCE, VFE, VFG)
      ENDDO

      SV2D_XCL129_ASMMU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL129_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL129_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>129</b>: this is a pre-alfa state condition
C  <p>
C  L'idée est de prolonger par un élément Q4 rectangulaire,
C  normal à l'élément de frontière, et de longueur L. Ce Q4 est
C  scindé en 2 sous-éléments pour s'adapter au T6L. La continuité
C  est intégrée dans le repère normal-tangent.
C
C  Ici, u est la vitesse normale et v la tangentielle. On a u3, u4 qui
C  sont les valeurs imposées (calculées par répartition), et on a v3 = 0
C  et v4 = 0.
C
C  Pour répartir le débit total, il faut un niveau. Celui-ci est calculé
C  comme le niveau moyen sur la limite.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 129</li>
C     <li>Values: Q, HQ4???</li>
C     <li>Units: m^3/s m</li>
C     <li>Example:  129  -500  400</li>
C  </ul>
C  (Status: Very Experimental).
C</comment>

      SV2D_XCL129_REQHLP = 'bc_type_129'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL129_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL129_HLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl129.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl129.hlp')

      SV2D_XCL129_HLP = ERR_TYP()
      RETURN
      END
