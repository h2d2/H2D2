C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL012_000
C     INTEGER SV2D_XCL012_999
C   Private:
C     INTEGER SV2D_XCL012_INIVTBL
C     INTEGER SV2D_XCL012_COD
C     INTEGER SV2D_XCL012_ASMF
C     CHARACTER*(16) SV2D_XCL012_REQHLP
C     INTEGER SV2D_XCL012_HLP
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL012_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL012_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl012.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL012_INIVTBL()

      SV2D_XCL012_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL012_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL012_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL012_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_INIVTBL()

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl012.fc'

      INTEGER IERR
      INTEGER HVFT
      EXTERNAL SV2D_XCL012_HLP
      EXTERNAL SV2D_XCL012_COD
      EXTERNAL SV2D_XCL012_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL012_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL012_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL012_TYP,
     &                    SV2D_XCL012_HLP)
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_COD, SV2D_XCL012_TYP,
     &                    SV2D_XCL012_COD)
      IERR=SV2D_XCL_AJTFNC(HVFT, SV2D_XCL_FUNC_ASMF,SV2D_XCL012_TYP,
     &                    SV2D_XCL012_ASMF)

      SV2D_XCL012_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL012_COD
C
C Description:
C     La fonction SV2D_XCL012_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme sollicitation de débit.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_COD(IL, BELF)

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl012.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN, IVDEB, IVFIN
      INTEGER NNOD, NVAL
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,
     &                           -1,-1,
     &                           -1,-1,
     &                            1,-1/), (/2, 4/))
C-----------------------------------------------------------------------

      CALL LOG_TODO('XCL012: valider le contrôle des dimensions')
      CALL ERR_ASR(.FALSE.)

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC   = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL012_TYP)
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, KCLDIM)

C---     Assigne les codes
      IC = GDTA%KCLLIM(2, IL)
      INDEB = GDTA%KCLLIM(3,IL)
      INFIN = GDTA%KCLLIM(4,IL)
      DO I = INDEB, INFIN
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(3,IN),
     &                               SV2D_XCL_SLC_DEBIT)
      ENDDO

      SV2D_XCL012_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL012_ASMF
C
C Description:
C     La fonction SV2D_XCL012_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme sollicitation de débit.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des DDL imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_ASMF(IL, BELF, VFG)

      USE SV2D_XBS_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl012.fc'

      INTEGER IERR
      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
D     INTEGER NNOD, NVAL
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les attributs
      CALL C_F_POINTER(TRANSFER(LOC(BELF), C_NULL_PTR), SELF)
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Contrôles
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL012_TYP)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)
      INDEB = GDTA%KCLLIM(3,IL)
      INFIN = GDTA%KCLLIM(4,IL)
      IVDEB = EDTA%KCLCND(3,IC)
      IVFIN = EDTA%KCLCND(4,IC)
D     NNOD = INFIN - INDEB + 1
D     NVAL = IVFIN - IVDEB + 1
D     CALL ERR_ASR(NVAL .EQ. 1 .OR. NVAL .EQ. NNOD)

      IV = IVDEB - 1
      DO I = INDEB, INFIN
         IF (IV .LT. IVFIN) IV = IV + 1
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      IERR = SP_ELEM_ASMFE(1,
     &                           EDTA%KLOCN(3:,IN),
     &                           EDTA%VCLCNV(IV:),
     &                           VFG)
      ENDDO

      SV2D_XCL012_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL012_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL012_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>012</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The discharge sollicitation is imposed node by node.
C  Conceptually the condition is described by:
C  <pre>    (q0, q1, ..., qn)</pre>
C  where qi is the normal specific discharge at node i.
C  As a special case, if only 1 value is specified, it will be used
C  for all nodes.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 12</li>
C     <li>Values: q0  q1  q2 ... </li>
C     <li>Units: m^2/s</li>
C     <li>Example:  12   1.0 1.0 1.0</li>
C  </ul>
C</comment>

      SV2D_XCL012_REQHLP = 'bc_type_012'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL012_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL012_HLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl012.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl012.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl012.hlp')

      SV2D_XCL012_HLP = ERR_TYP()
      RETURN
      END
