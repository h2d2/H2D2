C************************************************************************
C --- Copyright (c) INRS 2011-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Condition de Dirichlet sur le débit spécifique qy
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL004_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL004_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD => SV2D_XCL004_COD
         PROCEDURE, PUBLIC :: PRC => SV2D_XCL004_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL004_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL004_HLP
      END TYPE SV2D_XCL004_T

      PUBLIC :: SV2D_XCL004_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL004_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL004_CTR</code> construit une C.L.
C     de type <code>004</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL004_CTR() RESULT(SELF)

      TYPE(SV2D_XCL004_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL004_CTR

C************************************************************************
C Sommaire:  SV2D_XCL004_COD
C
C Description:
C     La méthode <code>SV2D_XCL004_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL004_COD(SELF, KDIMP)

      CLASS(SV2D_XCL004_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            1, 1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(2,IN) = IBSET(KDIMP(2,IN), EA_TPCL_DIRICHLET)
      ENDDO

      SV2D_XCL004_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL004_COD

C************************************************************************
C Sommaire:  SV2D_XCL004_PRC
C
C Description:
C     La méthode <code>SV2D_XCL004_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL004_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL004_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'

      INTEGER I, IN, INDEB, INFIN
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0) VDIMP(2,IN) = SELF%VCND(1)
      ENDDO

      SV2D_XCL004_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL004_PRC

C************************************************************************
C Sommaire: SV2D_XCL004_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL004_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL004_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl004.hlp')

      SV2D_XCL004_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL004_HLP

C************************************************************************
C Sommaire:  SV2D_XCL004_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL004_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL004_CMD()

C------------------------------------------------------------------------

C<comment>
C *****<br>
C This boundary condition has been DEPRECATED. Please use condition <b>102</b> instead.<br>
C *****
C <p>
C  Boundary condition of type <b>004</b>: <br>
C  Dirichlet condition on the specific discharge in y direction.
C     <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 4</li>
C     <li>Values: qy</li>
C     <li>Units: m/s^2</li>
C     <li>Example:  4  0.0</li>
C     </ul>
C</comment>

      SV2D_XCL004_CMD = 'bc_type_004'
      RETURN
      END FUNCTION SV2D_XCL004_CMD

      END MODULE SV2D_XCL004_M
