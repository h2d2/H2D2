C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Impose le débit global d'une autre limite qui est redistribué
C     en fonction du niveau d'eau actuel.
C     Formulation Manning variable
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL138_000
C     INTEGER SV2D_XCL138_999
C     INTEGER SV2D_XCL138_COD
C     INTEGER SV2D_XCL138_ASMF
C     INTEGER SV2D_XCL138_HLP
C   Private:
C     INTEGER SV2D_XCL138_INIVTBL
C     CHARACTER*(16) SV2D_XCL138_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL138_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl138.fc'

      DATA SV2D_XCL138_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL138_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL138_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL138_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl138.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL138_INIVTBL()

      SV2D_XCL138_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL138_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL138_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL138_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL138_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL138_INIVTBL()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl138.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_XCL138_HLP
      EXTERNAL SV2D_XCL138_COD
      EXTERNAL SV2D_XCL138_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL138_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL138_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL138_TPN(1:LTPN), SV2D_XCL138_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL138_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL138_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL138_TYP,
     &                     SV2D_XCL138_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD, SV2D_XCL138_TYP,
     &                    SV2D_XCL138_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF,SV2D_XCL138_TYP,
     &                    SV2D_XCL138_ASMF)

      SV2D_XCL138_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL138_COD
C
C Description:
C     La fonction SV2D_XCL138_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose les valeurs de Q d'une
C     autre limite.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C     Valeurs lues: lim
C************************************************************************
      FUNCTION SV2D_XCL138_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL138_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl138.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_DST, ID_DST
      INTEGER IL_SRC, ID_SRC
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL138_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 1)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)
      IV = EDTA%KCLCND(3, IC)

C---     Assigne les codes
      IL_DST = IL
      ID_DST = 3        ! Sollicitation sur h
      IL_SRC = NINT( EDTA%VCLCNV(IV) )
      ID_SRC = 1        ! Devrait être 1&2
      IERR = SV2D_XCL_COD2L(IL_DST, ID_DST, SV2D_XCL_DEBIT,
     &                      IL_SRC, ID_SRC, SV2D_XCL_COPIE,
     &                      GDTA%KCLLIM,
     &                      GDTA%KCLNOD,
     &                      GDTA%KCLELE,
     &                      EDTA%KDIMP)
      IF (ERR_GOOD()) EDTA%VCLCNV(IV) = IL_SRC

C---     Contrôle la limite source
      IERR = SV2D_XCL_CHKDIM(IL_SRC, EDTA%KCLCND, GDTA%KCLLIM, -1)

9999  CONTINUE
      SV2D_XCL138_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL138_ASMF
C
C Description:
C     La fonction SV2D_XCL138_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C     D'après la loi de Manning:
C        u = H**(2/3) / n * pente
C     en posant la pente constante (==1), le débit spécifique est
C        q = H**(5/3) / n
C     qui est intégré.
C************************************************************************
      FUNCTION SV2D_XCL138_ASMF(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL138_ASMF
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl138.fc'

      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QX2, QX3
      REAL*8  QY1, QY2, QY3
      REAL*8  QXS, QYS
      REAL*8  QTOT, QSPEC
      REAL*8  F1, F3, P1, P3, Q1, Q3, QS
      REAL*8  C
      REAL*8  VL(2), VG(2)
      INTEGER NO1, NO2, NO3
      INTEGER IERR, I_ERROR
      INTEGER I, IE, IC, IV
      INTEGER IL_DST, IL_SRC
      INTEGER IEDEB_DST, IEFIN_DST
      INTEGER IEDEB_SRC, IEFIN_SRC
      INTEGER IN_DST, IN_SRC

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL138_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 1)
      IV = EDTA%KCLCND(3,IC)
      IL_SRC = NINT( EDTA%VCLCNV(IV) )

      IL_DST = IL
      IEDEB_DST = GDTA%KCLLIM(5, IL_DST)
      IEFIN_DST = GDTA%KCLLIM(6, IL_DST)
      IEDEB_SRC = GDTA%KCLLIM(5, IL_SRC)
      IEFIN_SRC = GDTA%KCLLIM(6, IL_SRC)

C---        Intègre le débit sur la limite source
      QTOT = 0.0D0
      DO I = IEDEB_SRC, IEFIN_SRC
         IE = GDTA%KCLELE(I)

C---        Connectivités du L3
         NO1 = GDTA%KNGS(1,IE)
         NO2 = GDTA%KNGS(2,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

         VNX =  GDTA%VDJS(2,IE)    ! VNX =  VTY
         VNY = -GDTA%VDJS(1,IE)    ! VNY = -VTX
         DJL3=  GDTA%VDJS(3,IE)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = EDTA%VDLG (1,NO1)   ! Qx
         QY1 = EDTA%VDLG (2,NO1)   ! Qy
         QX2 = EDTA%VDLG (1,NO2)
         QY2 = EDTA%VDLG (2,NO2)
         QX3 = EDTA%VDLG (1,NO3)
         QY3 = EDTA%VDLG (2,NO3)

C---        Débits normaux
         QXS = ((QX2+QX1) + (QX3+QX2))*VNX
         QYS = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         QTOT = QTOT + (QXS + QYS)*DJL2
199      CONTINUE
      ENDDO

C---        Intègre la surface sur la limite de sortie
      QS = 0.0D0
      DO I = IEDEB_DST, IEFIN_DST
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO3 = GDTA%KNGS(3,IE)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + GDTA%VDJS(3,IE)*(Q1+Q3)
299      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL(1) = QTOT
      VL(2) = QS
      CALL MPI_ALLREDUCE(VL, VG, 2, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QTOT = VG(1)
      QS   = VG(2)
      IF (ABS(QTOT) .GT. PETIT .AND. QS .LT. PETIT) GOTO 9900
      QS = MAX(QS, PETIT)

C---     Débit spécifique
      QTOT = - QTOT     ! Transforme sortant en entrant, et vice-versa
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB_DST, IEFIN_DST
         IE = GDTA%KCLELE(I)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IE)
         NO3 = GDTA%KNGS(3,IE)

C---        Coefficient
         C = -UN_3*QSPEC*GDTA%VDJS(3,IE)

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), C*(Q1+Q1 + Q3), VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), C*(Q3+Q3 + Q1), VFG)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_XCL138_TYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_DEBIT: ', QTOT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_SURFACE: ', QS
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL138_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL138_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL138_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL138_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>138</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The discharge Q taken from a source boundary is distributed on the wetted part of the boundary,
C  taking into account the current water level h.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth and Manning dependent).
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 138</li>
C     <li>Values: Name</li>
C     <li>Units: [-]</li>
C     <li>Example:  138  $__Amont__$</li>
C  </ul>
C</comment>

      SV2D_XCL138_REQHLP = 'bc_type_138'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL138_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL138_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL138_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl138.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl138.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl138.hlp')

      SV2D_XCL138_HLP = ERR_TYP()
      RETURN
      END
