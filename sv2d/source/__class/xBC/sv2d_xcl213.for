C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C  Harmonic Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL213_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL213_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL213_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL213_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL213_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL213_HLP
      END TYPE SV2D_XCL213_T

      PUBLIC :: SV2D_XCL213_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL213_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL213_CTR</code> construit une C.L.
C     de type <code>213</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL213_CTR() RESULT(SELF)

      TYPE(SV2D_XCL213_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL213_CTR

C************************************************************************
C Sommaire:  SV2D_XCL213_COD
C
C Description:
C     La méthode <code>SV2D_XCL213_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL213_COD(SELF, KDIMP)

      CLASS(SV2D_XCL213_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN
      INTEGER NVAL

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            3,-1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)
      NVAL = SIZE(SELF%VCND, 1)
      IF (MOD(NVAL, 3) .NE. 0) GOTO 9902   ! n multiple de 3
      IF (NVAL/3       .LT. 1) GOTO 9902   ! n >= 3

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTENU', ': ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTENDU', ': ', '(N*3)'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL213_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL213_COD

C************************************************************************
C Sommaire:  SV2D_XCL213_PRC
C
C Description:
C     La méthode <code>SV2D_XCL213_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        (                       ! Noeud début
C           (o,a,p),
C           (o,a,p), ...
C        ),
C        (                       ! Noeud fin
C           (o,a,p),
C           (o,a,p), ...
C        )
C     )
C     Les fréquences doivent être dans le même ordre
C     On interpole également les fréquences
C************************************************************************
      INTEGER FUNCTION SV2D_XCL213_PRC(SELF, ELEM, VDIMP)

      USE TRIGD_M, ONLY: COSD
      
      CLASS(SV2D_XCL213_T),INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),    INTENT(IN), TARGET :: ELEM
      REAL*8,              INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'

      INTEGER I, IN, INDEB, INFIN
      INTEGER IV, IVDEB, IVFIN
      REAL*8  H, O, A, D
      REAL*8  T
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      IVDEB = LBOUND(SELF%VCND, 1)
      IVFIN = UBOUND(SELF%VCND, 1)

      T  = ELEM%EDTA%TEMPS    ! Temps de la simu.

      H = 0.0D0
      DO IV=IVDEB,IVFIN,3
         O = SELF%VCND(IV+0)     ! omega
         A = SELF%VCND(IV+1)     ! amplitude
         D = SELF%VCND(IV+2)     ! phi
         H = H + A * COSD(O*T + D)
      ENDDO
         
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I=INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = H
      ENDDO

      SV2D_XCL213_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL213_PRC

C************************************************************************
C Sommaire: SV2D_XCL213_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL213_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL213_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl213.hlp')

      SV2D_XCL213_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL213_HLP

C************************************************************************
C Sommaire:  SV2D_XCL213_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL213_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL213_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>213</b>: <br>
C  Harmonic Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            (o,a,p),
C            (o,a,p), ...
C         )</pre>
C  where o is the period, a the amplitude and p the phase shift (in degrees).
C  The water level H is then:
C  <pre>
C     H = sum(a * cosd(o*t + p))</pre>
C  Period, amplitude and phase shift are not interpolated.
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 213</li>
C     <li>Values: o, a, p, o, a, p, ... </li>
C     <li>Units: ([deg/s] [m] [deg])</li>
C     <li>Example:  213   0.0314  2.0  0.0</li>
C  </ul>
C</comment>

      SV2D_XCL213_CMD = 'bc_type_213'
      RETURN
      END FUNCTION SV2D_XCL213_CMD
      
      END MODULE SV2D_XCL213_M