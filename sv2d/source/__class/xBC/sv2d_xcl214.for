C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C  Harmonic Dirichlet condition on the water level. The condition
C  is interpolated between boundary start and end node.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL214_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL214_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL214_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL214_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL214_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL214_HLP
      END TYPE SV2D_XCL214_T

      PUBLIC :: SV2D_XCL214_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL214_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL214_CTR</code> construit une C.L.
C     de type <code>214</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL214_CTR() RESULT(SELF)

      TYPE(SV2D_XCL214_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL214_CTR

C************************************************************************
C Sommaire:  SV2D_XCL214_COD
C
C Description:
C     La méthode <code>SV2D_XCL214_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL214_COD(SELF, KDIMP)

      CLASS(SV2D_XCL214_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN
      INTEGER NVAL

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                            1,-1,               ! Nb de limites (min, max)
     &                            6,-1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)
      NVAL = SIZE(SELF%VCND, 1)
      IF (SELF%IK .NE. EG_TPLMT_1SGMT) GOTO 9901
      IF (MOD(NVAL, 2)   .NE. 0) GOTO 9902   ! n*2 valeurs
      IF (MOD(NVAL/2, 3) .NE. 0) GOTO 9902   ! n multiple de 3
      IF ((NVAL/2)/3     .LT. 1) GOTO 9902   ! n >= 3

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_LIMITE_UN_SEGMENT_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTIENT', ': ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTEND', ': ', '2*(N*3)'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL214_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL214_COD

C************************************************************************
C Sommaire:  SV2D_XCL214_PRC
C
C Description:
C     La méthode <code>SV2D_XCL214_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        (                       ! Noeud début
C           (o,a,p),
C           (o,a,p), ...
C        ),
C        (                       ! Noeud fin
C           (o,a,p),
C           (o,a,p), ...
C        )
C     )
C     Les fréquences doivent être dans le même ordre
C     On interpole également les fréquences
C************************************************************************
      INTEGER FUNCTION SV2D_XCL214_PRC(SELF, ELEM, VDIMP)

      USE TRIGD_M, ONLY: COSD
      
      CLASS(SV2D_XCL214_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'

      INTEGER I, IN, INDEB, INFIN
      INTEGER IV1, IVDEB1, IVFIN1
      INTEGER IV2, IVDEB2, IVFIN2
      REAL*8  H, O, A, D
      REAL*8  O1, A1, D1
      REAL*8  O2, A2, D2
      REAL*8  T
      REAL*8  VN1, VN2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      IVDEB1 = LBOUND(SELF%VCND, 1)
      IVFIN1 = IVDEB1 + SIZE(SELF%VCND, 1)/2 - 1
      IVDEB2 = IVFIN1 + 1
      IVFIN2 = UBOUND(SELF%VCND, 1)

      T  = ELEM%EDTA%TEMPS     ! Temps de la simu.

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I=INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .LE. 0) GOTO 199

         VN1 = 1.0D0 - SELF%VDST(I)
         VN2 = SELF%VDST(I)

         H = 0.0D0
         IV2 = IVDEB2-3
         DO IV1=IVDEB1,IVFIN1,3
            IV2 = IV2 + 3
            O1 = SELF%VCND(IV1+0)     ! omega
            A1 = SELF%VCND(IV1+1)     ! amplitude
            D1 = SELF%VCND(IV1+2)     ! phi
            O2 = SELF%VCND(IV2+0)     ! omega
            A2 = SELF%VCND(IV2+1)     ! amplitude
            D2 = SELF%VCND(IV2+2)     ! phi
            O = VN1*O1 + VN2*O2
            A = VN1*A1 + VN2*A2
            D = VN1*D1 + VN2*D2
            H = H + A * COSD(O*T + D)
         ENDDO
D        CALL ERR_ASR(IV2 .EQ. IVFIN2)
         VDIMP(3,IN) = H

199      CONTINUE
      ENDDO

      SV2D_XCL214_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL214_PRC

C************************************************************************
C Sommaire: SV2D_XCL214_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL214_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL214_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl214.hlp')

      SV2D_XCL214_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL214_HLP

C************************************************************************
C Sommaire:  SV2D_XCL214_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL214_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL214_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>214</b>: <br>
C  Harmonic Dirichlet condition on the water level. The condition
C  is interpolated between boundary start and end node.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            (                     ! Start node
C               (o,a,p),
C               (o,a,p), ...
C            ),
C            (                     ! End node
C               (o,a,p),
C               (o,a,p), ...
C            )
C         )</pre>
C  where o is the period, a the amplitude and p the phase shift (in degrees).
C  The water level H is then:
C  <pre>
C     H = sum(a * cos(o*t + p))</pre>
C  Period, amplitude and phase shift are interpolated between
C  boundary start and end node.
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 214</li>
C     <li>Values: o, a, p, o, a, p, ... </li>
C     <li>Units: [deg/s]  [m]  [deg]</li>
C     <li>Example:  214   0.0314  2.0  0.0  0.0314  1.9  0.1</li>
C  </ul>
C</comment>

      SV2D_XCL214_CMD = 'bc_type_214'
      RETURN
      END FUNCTION SV2D_XCL214_CMD
      
      END MODULE SV2D_XCL214_M

      