C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C  Weak Dirichlet condition on the specific discharge in x.
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL111_M

      USE SV2D_XCL000_M
      USE LM_ELEM_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL111_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL111_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL111_PRC
         PROCEDURE, PUBLIC :: ASMF  => SV2D_XCL111_ASMF
         PROCEDURE, PUBLIC :: ASMKU => SV2D_XCL111_ASMKU
         PROCEDURE, PUBLIC :: ASMK  => SV2D_XCL111_ASMK
         PROCEDURE, PUBLIC :: ASMKT => SV2D_XCL111_ASMKT
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD  => SV2D_XCL111_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP  => SV2D_XCL111_HLP
      END TYPE SV2D_XCL111_T

      PUBLIC :: SV2D_XCL111_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL111_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL111_CTR</code> construit une C.L.
C     de type <code>111</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL111_CTR() RESULT(SELF)

      TYPE(SV2D_XCL111_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL111_CTR

C************************************************************************
C Sommaire:  SV2D_XCL111_COD
C
C Description:
C     La méthode <code>SV2D_XCL111_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_COD(SELF, KDIMP)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            1, 1/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(1,IN) = IBSET(KDIMP(1,IN), EA_TPCL_WEAKDIR)
      ENDDO

      SV2D_XCL111_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_COD

C************************************************************************
C Sommaire:  SV2D_XCL111_PRC
C
C Description:
C     La méthode <code>SV2D_XCL111_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'

      INTEGER I, IN, INDEB, INFIN
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0) VDIMP(1,IN) = SELF%VCND(1)
      ENDDO

      SV2D_XCL111_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_PRC

C************************************************************************
C Sommaire:  SV2D_XCL111_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_ASMF(SELF, ELEM, VFG)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER, PARAMETER :: NNEL_LCL = 3
      INTEGER, PARAMETER :: NDLE_LCL = 3

      REAL*8  VFE(NDLE_LCL)
      REAL*8  C
      INTEGER IERR
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER KLOCE(NDLE_LCL), KNE(NNEL_LCL)
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => ELEM%GDTA
      EDTA => ELEM%EDTA

C---     Indices des éléments
      IEDEB = LBOUND(SELF%KELE, 1)
      IEFIN = UBOUND(SELF%KELE, 1) 

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         KNE(:) = GDTA%KNGS(1:3,IES)

C---        Valeurs élémentaires
         C = UN_2 * GDTA%VDJS(3,IES)
         VFE(:) = C * EDTA%VDIMP(1, KNE(:))
         VFE(2) = VFE(2) + VFE(2)

C---        Assemblage du vecteur global lumped
         KLOCE(:) = EDTA%KLOCN(1, KNE(:))
         IERR = SP_ELEM_ASMFE(NDLE_LCL, KLOCE, VFE, VFG)
      ENDDO

      SV2D_XCL111_ASMF = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_ASMF

C************************************************************************
C Sommaire:  SV2D_XCL111_ASMKU
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_ASMKU(SELF, ELEM, VFG)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8,  INTENT(IN) :: VFG(:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER, PARAMETER :: NNEL_LCL = 3
      INTEGER, PARAMETER :: NDLE_LCL = 3

      REAL*8  VFE(NDLE_LCL)
      REAL*8  C
      INTEGER IERR
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER KLOCE(NDLE_LCL), KNE(NNEL_LCL)
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => ELEM%GDTA
      EDTA => ELEM%EDTA

C---     Indices des éléments
      IEDEB = LBOUND(SELF%KELE, 1)
      IEFIN = UBOUND(SELF%KELE, 1) 

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         KNE(:) = GDTA%KNGS(1:3,IES)

C---        Valeurs élémentaires
         C = UN_2*GDTA%VDJS(3,IES)
         VFE(:) = C * EDTA%VDLG(1, KNE(:))
         VFE(2) = VFE(2) + VFE(2)

C---        Assemblage du vecteur global lumped
         KLOCE(:) = EDTA%KLOCN(1, KNE(:))
         IERR = SP_ELEM_ASMFE(NDLE_LCL, KLOCE, VFE, VFG)
      ENDDO

      SV2D_XCL111_ASMKU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_ASMKU

C************************************************************************
C Sommaire: SV2D_XCL111_ASMK
C
C Description:
C     La méthode <code>SV2D_XCL111_ASMK</code> calcule le matrice
C     de rigidité élémentaire. L'assemblage de la matrice globale est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_ASMK(SELF, ELEM, HMTX, F_ASM)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER, PARAMETER :: NNEL_LCL = 3
      INTEGER, PARAMETER :: NDLE_LCL = 3

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IE, IES
      INTEGER KLOCE (NDLE_LCL), KNE(NNEL_LCL)
      REAL*8  VKE   (NDLE_LCL, NDLE_LCL)
      REAL*8  ML
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C----------------------------------------------------------------

C---     Récupère les données
      GDTA => ELEM%GDTA
      EDTA => ELEM%EDTA

C---     Initialise la matrice élémentaire
      VKE(:,:) = ZERO

C---     Indices des éléments
      IEDEB = LBOUND(SELF%KELE, 1)
      IEFIN = UBOUND(SELF%KELE, 1) 

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         KNE(:) = GDTA%KNGS(1:NNEL_LCL,IES)

C---        Matrice masse lumpded
         ML = UN_2 * GDTA%VDJS(3,IES)
         VKE(1,1) = ML
         VKE(2,2) = ML+ML
         VKE(3,3) = ML

C---       Assemblage de la matrice
         KLOCE(:) = EDTA%KLOCN(1, KNE(:))
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      SV2D_XCL111_ASMK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_ASMK

C************************************************************************
C Sommaire: SV2D_XCL111_ASMKT
C
C Description:
C     La méthode <code>SV2D_XCL111_ASMKT</code> calcule le matrice
C     de rigidité élémentaire. L'assemblage de la matrice globale est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_ASMKT(SELF, ELEM, HMTX, F_ASM)

      CLASS(SV2D_XCL111_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XCL111_ASMKT = SV2D_XCL111_ASMK(SELF, ELEM, HMTX, F_ASM)
      RETURN
      END FUNCTION SV2D_XCL111_ASMKT

C************************************************************************
C Sommaire: SV2D_XCL111_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL111_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL111_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl111.hlp')

      SV2D_XCL111_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL111_HLP

C************************************************************************
C Sommaire:  SV2D_XCL111_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL111_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL111_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>111</b>: <br>
C  Weak Dirichlet condition on the specific discharge in x.
C     <ul>
C     <li>Kind: Weak Dirichlet</li>
C     <li>Code: 111</li>
C     <li>Values: qx</li>
C     <li>Units: m^2/s</li>
C     <li>Example:  111  10.0</li>
C     </ul>
C</comment>

      SV2D_XCL111_CMD = 'bc_type_111'
      RETURN
      END FUNCTION SV2D_XCL111_CMD
      
      END MODULE SV2D_XCL111_M

      