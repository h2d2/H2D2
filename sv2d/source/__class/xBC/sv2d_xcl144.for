C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Ponceau MTQ.
C     Le débit calculé sur la différence des niveaux amont-aval est imposé
C     sous forme faible à l'amont et l'aval.
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL144_000
C     INTEGER SV2D_XCL144_999
C     INTEGER SV2D_XCL144_COD
C     INTEGER SV2D_XCL144_CLC
C     INTEGER SV2D_XCL144_ASMF
C     INTEGER SV2D_XCL144_ASMKT
C     INTEGER SV2D_XCL144_HLP
C   Private:
C     INTEGER SV2D_XCL144_INIVTBL
C     INTEGER SV2D_XCL144_CLC_
C     INTEGER SV2D_XCL144_ASMFE
C     INTEGER SV2D_XCL144_ASMKTL
C     CHARACTER*(16) SV2D_XCL144_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL144_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl144.fc'

      DATA SV2D_XCL144_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL144_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL144_INIVTBL()

      SV2D_XCL144_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL144_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL144_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_INIVTBL()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR !!!, IRET
      INTEGER HVFT
!!!      INTEGER LTPN
      EXTERNAL SV2D_XCL144_HLP
      EXTERNAL SV2D_XCL144_COD
      EXTERNAL SV2D_XCL144_CLC
      EXTERNAL SV2D_XCL144_ASMF
      EXTERNAL SV2D_XCL144_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL144_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL144_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL144_TPN(1:LTPN), SV2D_XCL144_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL144_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL144_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL144_TYP,
     &                     SV2D_XCL144_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL144_TYP,
     &                     SV2D_XCL144_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_CLC,  SV2D_XCL144_TYP,
     &                     SV2D_XCL144_CLC)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF, SV2D_XCL144_TYP,
     &                     SV2D_XCL144_ASMF)
!      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKT,SV2D_XCL144_TYP,
!     &                     SV2D_XCL144_ASMKT)

      SV2D_XCL144_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL144_COD
C
C Description:
C     La fonction SV2D_XCL144_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C     On ne contrôle pas le type de la CL amont
C************************************************************************
      FUNCTION SV2D_XCL144_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, ID_AMT
      INTEGER IL_AVL, ID_AVL
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL144_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 25)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)
      IV = EDTA%KCLCND(3, IC)

C---     Assigne les codes
      IL_AVL = IL
      ID_AVL = 3
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      ID_AMT = 3
      IERR = SV2D_XCL_COD2L(IL_AVL, ID_AVL, SV2D_XCL_DEBIT,     ! aval
     &                      IL_AMT, ID_AMT, EA_TPCL_WEAKDIR,    ! amont
     &                      GDTA%KCLLIM,
     &                      GDTA%KCLNOD,
     &                      GDTA%KCLELE,
     &                      EDTA%KDIMP)
      IF (ERR_GOOD()) EDTA%VCLCNV(IV) = IL_AMT

C---     Contrôle la limite amont
      IERR = SV2D_XCL_CHKDIM(IL_AMT, EDTA%KCLCND, GDTA%KCLLIM, -1)

      SV2D_XCL144_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL144_CLC_
C
C Description:
C     La fonction privée SV2D_XCL144_CLC_ assigne la valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C     Elle permet de simplifier les arguments et est appelée également par
C     ASMKTL
C************************************************************************
      FUNCTION SV2D_XCL144_CLC_(IL,
     &                         GDTA%KNGS,
     &                         GDTA%VDJS,
     &                         EDTA%VPRGL,
     &                         EDTA%VPRNO,
     &                         EDTA%KCLCND,
     &                         EDTA%VCLCNV,
     &                         GDTA%KCLLIM,
     &                         GDTA%KCLNOD,
     &                         GDTA%KCLELE,
     &                         GDTA%VCLDST,
     &                         EDTA%KDIMP,
     &                         EDTA%VDIMP,
     &                         EDTA%VDLG)

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spmtq.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IE, IES, I, IN
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT, INDEB_AMT, INFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL, INDEB_AVL, INFIN_AVL
      INTEGER NO1, NO2, NO3
      INTEGER NVAL
      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QY1, H1, P1, F1
      REAL*8  QX2, QY2, P2, F2
      REAL*8  QX3, QY3, H3, P3, F3
      REAL*8  QX, QY
      REAL*8  Q_US, H_US, L_US, S_US
      REAL*8  Q_DS, H_DS, L_DS, S_DS
      REAL*8  Q_Pct, Q_Med, Q_srf !, H_PctUs
      REAL*8  RS(8), RG(8)
      LOGICAL ENEAU

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
      REAL*8, PARAMETER :: DREL_MAX  = 0.15D0     ! Delta relatif max
-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
      REAL*8 V1, V2
      REAL*8 ERR_REL
      ERR_REL(V1, V2) = 0.5D0*(ABS(V1-V2) / ABS(V1+V2))
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL144_TYP)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.bc.cl144'

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      INDEB_AMT = GDTA%KCLLIM(3, IL_AMT)
      INFIN_AMT = GDTA%KCLLIM(4, IL_AMT)
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)
      INDEB_AVL = GDTA%KCLLIM(3, IL_AVL)
      INFIN_AVL = GDTA%KCLLIM(4, IL_AVL)
      IEDEB_AVL = GDTA%KCLLIM(5, IL_AVL)
      IEFIN_AVL = GDTA%KCLLIM(6, IL_AVL)

C---     Valeurs de la condition
      NVAL = EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)            ! +1 mais -la première
      IERR = SP_MTQ_ASGPRM(NVAL, EDTA%VCLCNV(IV+1))

C---     Débit et Niveau amont
      Q_US = 0.0D0
      H_US = 0.0D0
      S_US = 0.0D0
      L_US = 0.0D0
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités du L3
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Métriques
         VNX =  GDTA%VDJS(2,IES)    ! VNX =  VTY
         VNY = -GDTA%VDJS(1,IES)    ! VNY = -VTX
         DJL3=  GDTA%VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = EDTA%VDLG (1,NO1)   ! Qx
         QY1 = EDTA%VDLG (2,NO1)   ! Qy
         H1  = EDTA%VDLG (3,NO1)   ! h
         QX2 = EDTA%VDLG (1,NO2)
         QY2 = EDTA%VDLG (2,NO2)
         QX3 = EDTA%VDLG (1,NO3)
         QY3 = EDTA%VDLG (2,NO3)
         H3  = EDTA%VDLG (3,NO3)
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_US = Q_US + (QX+QY)*DJL2          ! Débit
         H_US = H_US + (H1+H3)*DJL3          ! h*L
         S_US = S_US + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_US = L_US + (UN+UN)*DJL3          ! Longueur
199      CONTINUE
      ENDDO

C---     Débit et Niveau aval
      Q_DS = 0.0D0
      H_DS = 0.0D0
      S_DS = 0.0D0
      L_DS = 0.0D0
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = GDTA%KCLELE(IE)

C---        Connectivités du L3
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Métriques
         VNX =  GDTA%VDJS(2,IES)    ! VNX =  VTY
         VNY = -GDTA%VDJS(1,IES)    ! VNY = -VTX
         DJL3=  GDTA%VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = EDTA%VDLG (1,NO1)   ! Qx
         QY1 = EDTA%VDLG (2,NO1)   ! Qy
         H1  = EDTA%VDLG (3,NO1)   ! h
         QX2 = EDTA%VDLG (1,NO2)
         QY2 = EDTA%VDLG (2,NO2)
         QX3 = EDTA%VDLG (1,NO3)
         QY3 = EDTA%VDLG (2,NO3)
         H3  = EDTA%VDLG (3,NO3)
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_DS = Q_DS + (QX+QY)*DJL2          ! Débit
         H_DS = H_DS + (H1+H3)*DJL3          ! h*Longueur
         S_DS = S_DS + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_DS = L_DS + (UN+UN)*DJL3          ! Longueur totale
299      CONTINUE
      ENDDO

C---     Réduction et contrôle
      RS(1) = Q_US
      RS(2) = H_US
      RS(3) = L_US
      RS(4) = S_US
      RS(5) = Q_DS
      RS(6) = H_DS
      RS(7) = L_DS
      RS(8) = S_DS
      CALL MPI_ALLREDUCE(RS, RG, 8, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      Q_US = RG(1)
      H_US = RG(2)
      L_US = RG(3)
      S_US = RG(4)
      Q_DS = -RG(5)           ! négatif, débit entrant
      H_DS = RG(6)
      L_DS = RG(7)
      S_DS = RG(8)

C---     Calcule les données hydrauliques du ponceau
      Q_Pct = 0.0D0
      ENEAU = .TRUE.
      IF (L_US .LT. PETIT .OR. L_DS .LT. PETIT) THEN
         S_US = 1.0D0
         S_DS = 1.0D0
         ENEAU = .FALSE.
      ENDIF
      IF (ENEAU) THEN
         H_US = (H_US / L_US)    ! Niveau moyen
         H_DS = (H_DS / L_DS)    ! Niveau moyen
         Q_Pct   = SP_MTQ_CLCQ(H_US, H_DS)
!      H_PctUs = SP_MTQ_CLCH(H_US, H_DS, 0.5D0*(Q_US+Q_DS))
      ENDIF

C---     Log
!      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
!     &      'Culvert ', IL,': H (up, down, clc): ', H_US, H_DS, H_PctUs
!      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
     &      'Culvert ', IL,': Q (up, down, clc): ', Q_US, Q_DS, Q_Pct
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Impose les valeurs (avec amortissement au besoin)
      IF (ENEAU) THEN
         Q_Med = UN_2*(Q_US+Q_DS)
         IF (ERR_REL(Q_Pct, Q_Med) .GT. DREL_MAX)
     &      Q_Pct = Q_Med + DREL_MAX*(Q_Pct-Q_Med)
      ENDIF
      Q_srf = Q_Pct / S_US                   ! Positif, débit sortant
      DO I = INDEB_AMT, INFIN_AMT
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0) EDTA%VDIMP(3,IN) = Q_srf  ! Débit spécifique (m/s)
      ENDDO
      Q_srf = - Q_Pct / S_DS                 ! Négatif, débit entrant
      DO I = INDEB_AVL, INFIN_AVL
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0) EDTA%VDIMP(3,IN) = Q_srf  ! Débit spécifique (m/s)
      ENDDO

      SV2D_XCL144_CLC_ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL144_CLC
C
C Description:
C     La fonction privée SV2D_XCL144_CLC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_CLC(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_CLC
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL144_TYP)
C-----------------------------------------------------------------------

      IERR = SV2D_XCL144_CLC_(IL,
     &                       GDTA%KNGS,
     &                       GDTA%VDJS,
     &                       EDTA%VPRGL,
     &                       EDTA%VPRNO,
     &                       EDTA%KCLCND,
     &                       EDTA%VCLCNV,
     &                       GDTA%KCLLIM,
     &                       GDTA%KCLNOD,
     &                       GDTA%KCLELE,
     &                       GDTA%VCLDST,
     &                       EDTA%KDIMP,
     &                       EDTA%VDIMP,
     &                       EDTA%VDLG)

      SV2D_XCL144_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL144_ASMFE
C
C Description:
C     La fonction SV2D_XCL144_ASMFE calcule le vecteur {F} élémentaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_ASMFE(VFE,
     &                          VDJE,
     &                          VPRN,
     &                          EDTA%VDIMP)

      USE SV2D_XBS_M
      IMPLICIT NONE


      REAL*8   VFE  (EDTA%NDLES)
      REAL*8   VDJE (GDTA%NDJS)
      REAL*8   VPRN (EDTA%NPRNO, GDTA%NNELS)
      REAL*8   EDTA%VDIMP(EDTA%NDLN,  GDTA%NNELS)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
      REAL*8  C
      REAL*8  P1, F1, Q1
      REAL*8  P2, F2, Q2
      REAL*8  P3, F3, Q3

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Coefficient
      C = -UN_12*VDJE(3)               ! UN_6*DJL2

C---     Valeurs nodales
      F1 = MAX(VPRN(SV2D_IPRNO_N,1), PETIT)       ! Manning
      P1 =     VPRN(SV2D_IPRNO_H,1)               ! Profondeur
      F2 = MAX(VPRN(SV2D_IPRNO_N,2), PETIT)
      P2 =     VPRN(SV2D_IPRNO_H,2)
      F3 = MAX(VPRN(SV2D_IPRNO_N,3), PETIT)
      P3 =     VPRN(SV2D_IPRNO_H,3)
      Q1 = EDTA%VDIMP(3,1)
      Q2 = Q1     !! EDTA%VDIMP(3,2) ! est-il assigné?
      Q3 = EDTA%VDIMP(3,3)

C---     Règle de répartition d'après Manning
      Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
      Q2 = Q2*(P2**CINQ_TIER) / F2
      Q3 = Q3*(P3**CINQ_TIER) / F3

C---     Assemble le vecteur élémentaire
      VFE(3) = VFE(3) + C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
      VFE(9) = VFE(9) + C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)

      SV2D_XCL144_ASMFE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL144_ASMF
C
C Description:
C     La fonction SV2D_XCL144_ASMF calcul le terme de sollicitation.
C     Sur la limite <code>IL</code>, ainsi que sur la limite associée,
C     elle impose une condition de type Ponceau via des sollicitations
C     réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_ASMF(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_ASMF
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
      INTEGER IE, IES
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL
      INTEGER NO1, NO2, NO3
      REAL*8  C
      REAL*8  P1, F1, Q1
      REAL*8  P2, F2, Q2
      REAL*8  P3, F3, Q3
      REAL*8  M(2)

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL144_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)
      IEDEB_AVL = GDTA%KCLLIM(5, IL_AVL)
      IEFIN_AVL = GDTA%KCLLIM(6, IL_AVL)

C---     Boucle sur la limite amont (sollicitation sur h)
C        ================================================
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Coefficient
         C = -UN_12*GDTA%VDJS(3,IES)              ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)
         Q1 = EDTA%VDIMP(3,NO1)
         Q2 = Q1     !! EDTA%VDIMP(3,NO2) ! est-il assigné?
         Q3 = EDTA%VDIMP(3,NO3)

C---        Règle de répartition d'après Manning
         Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = Q2*(P2**CINQ_TIER) / F2
         Q3 = Q3*(P3**CINQ_TIER) / F3

C---        Assemble le vecteur global
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), M(2), VFG)
199      CONTINUE
      ENDDO

C---     Boucle sur la limite aval (sollicitation sur h)
C        ===============================================
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Coefficient
         C = -UN_12*GDTA%VDJS(3,IES)              ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)
         Q1 = EDTA%VDIMP(3,NO1)
         Q2 = Q1            ! est-il assigné?
         Q3 = EDTA%VDIMP(3,NO3)

C---        Règle de répartition d'après Manning
         Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = Q1     !! EDTA%VDIMP(3,NO2) ! est-il assigné?
         Q3 = Q3*(P3**CINQ_TIER) / F3

C---        Assemble le vecteur global
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), M(2), VFG)
299      CONTINUE
      ENDDO

      SV2D_XCL144_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL144_ASMKTL
C
C Description:
C     La fonction SV2D_XCL144_ASMKTL calcule le matrice de rigidité
C     élémentaire pour une limite. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_ASMKTL(IL,
     &                           IEDEB,
     &                           IEFIN,
     &                           KLOCE,
     &                           VKE,
     &                           EDTA%KLOCN,
     &                           GDTA%KNGS,
     &                           GDTA%VDJS,
     &                           EDTA%VPRGL,
     &                           EDTA%VPRNO,
     &                           EDTA%KCLCND,
     &                           EDTA%VCLCNV,
     &                           GDTA%KCLLIM,
     &                           GDTA%KCLNOD,
     &                           GDTA%KCLELE,
     &                           GDTA%VCLDST,
     &                           EDTA%KDIMP,
     &                           EDTA%VDIMP,
     &                           EDTA%VDLG,
     &                           HMTX,
     &                           F_ASM)

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER IL
      INTEGER IEDEB
      INTEGER IEFIN
      INTEGER KLOCE (EDTA%NDLES)
      REAL*8  VKE   (EDTA%NDLES,EDTA%NDLES)
      INTEGER HOBJ
      TYPE (LM_EDTA_T) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL =  9)
      PARAMETER (NPRN_LCL = 18)

      REAL*8  VDLE (NDLE_LCL)
      REAL*8  VCLE (NDLE_LCL)
      REAL*8  VPRN (NPRN_LCL * NNEL_LCL)
      REAL*8  VFE  (NDLE_LCL), VFEP(NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IE, IES
      INTEGER IN, II, ID, NO
      INTEGER HVFT, HPRNE
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. GDTA%NNELS)
D     CALL ERR_PRE(NDLE_LCL .GE. EDTA%NDLES)
D     CALL ERR_PRE(NPRN_LCL .GE. EDTA%NPRNO)
C-----------------------------------------------------------------------

C---     Fonctions de calcul
      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNES, HPRNE)

C---     Boucle sur la limite
C        ====================
      DO IE = IEDEB, IEFIN
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Table KLOCE de l'élément
         II=0
         DO IN=1,GDTA%NNELS
            NO = GDTA%KNGS(IN,IES)
            KLOCE(II+1) = EDTA%KLOCN(1,NO)
            KLOCE(II+2) = EDTA%KLOCN(2,NO)
            KLOCE(II+3) = EDTA%KLOCN(3,NO)
            II=II+3
         ENDDO

C---        Transfert des DDL
         II=0
         DO IN=1,GDTA%NNELS
            NO = GDTA%KNGS(IN,IES)
            VDLE(II+1) = EDTA%VDLG(1,NO)
            VDLE(II+2) = EDTA%VDLG(2,NO)
            VDLE(II+3) = EDTA%VDLG(3,NO)
            II=II+3
         ENDDO

C---        Transfert des EDTA%VDIMP
         II=0
         DO IN=1,GDTA%NNELS
            NO = GDTA%KNGS(IN,IES)
            VCLE(II+1) = EDTA%VDIMP(1,NO)
            VCLE(II+2) = EDTA%VDIMP(2,NO)
            VCLE(II+3) = EDTA%VDIMP(3,NO)
            II=II+3
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,GDTA%NNELS
            NO = GDTA%KNGS(IN,IES)
            CALL DCOPY(EDTA%NPRNO, EDTA%VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+EDTA%NPRNO
         ENDDO

C---        Initialise le vecteur F
         CALL DINIT(EDTA%NDLES, ZERO, VFE, 1)

C---        {F}
         IERR = SV2D_XCL144_ASMFE(VFE,
     &                           GDTA%VDJS(1,IES),
     &                           VPRN,
     &                           VCLE)

C---        Initialise la matrice des dérivées
         CALL DINIT(EDTA%NDLES*EDTA%NDLES, ZERO, VKE, 1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,EDTA%NDLES
            IF (KLOCE(ID) .EQ. 0) GOTO 299

C---           Perturbe le ddl
            VDL_ORI = VDLE(ID)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLE(ID)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLE(ID))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID) = VDLE(ID) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,   ! PRN perturbées
     &                              SO_ALLC_CST2B(EDTA%VPRGL(1))),
     &                              SO_ALLC_CST2B(VPRN(1))),
     &                              SO_ALLC_CST2B(VDLE(1))),
     &                              SO_ALLC_CST2B(IERR)))
               IERR = SV2D_XCL144_CLC_(IL,    ! CL perturbé dans EDTA%VDIMP
     &                              GDTA%KNGS,
     &                              GDTA%VDJS,
     &                              EDTA%VPRGL,
     &                              EDTA%VPRNO,
     &                              EDTA%KCLCND,
     &                              EDTA%VCLCNV,
     &                              GDTA%KCLLIM,
     &                              GDTA%KCLNOD,
     &                              GDTA%KCLELE,
     &                              GDTA%VCLDST,
     &                              EDTA%KDIMP,
     &                              EDTA%VDIMP,
     &                              EDTA%VDLG)
               VCLE(3) = EDTA%VDIMP(3, GDTA%KNGS(1,IES))
               VCLE(6) = EDTA%VDIMP(3, GDTA%KNGS(2,IES))
               VCLE(9) = EDTA%VDIMP(3, GDTA%KNGS(3,IES))
            ENDIF

C---           Initialise la vecteur de travail
            CALL DINIT(EDTA%NDLES, ZERO, VFEP, 1)

C---           {F(u+du_id)}
            IERR = SV2D_XCL144_ASMFE(VFEP,
     &                              GDTA%VDJS(1,IES),
     &                              VPRN,
     &                              VCLE)

C---           Restaure la valeur originale
            VDLE(ID) = VDL_ORI

C---           - ({F(u+du_id)} - {F(u)})/du
            DO II=1,EDTA%NDLES
               VKE(II,ID) = (VFE(II) - VFEP(II)) * VDL_INV
            ENDDO

299         CONTINUE
         ENDDO

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, EDTA%NDLES, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

199      CONTINUE
      ENDDO

      SV2D_XCL144_ASMKTL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL144_ASMKT
C
C Description:
C     La fonction SV2D_XCL144_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_ASMKT(IL,
     &                           GDTA,
     &                           EDTA,
     &                           HMTX,
     &                           F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_ASMKT
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL144_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)
      IEDEB_AVL = GDTA%KCLLIM(5, IL_AVL)
      IEFIN_AVL = GDTA%KCLLIM(6, IL_AVL)

C---     Calcule chaque limite
      IERR = SV2D_XCL144_ASMKTL(IL, IEDEB_AMT, IEFIN_AMT,
     &                         KLOCE, VKE, EDTA%KLOCN, GDTA%KNGS, GDTA%VDJS,
     &                         EDTA%VPRGL, EDTA%VPRNO,
     &                         EDTA%KCLCND, EDTA%VCLCNV,
     &                         GDTA%KCLLIM, GDTA%KCLNOD, GDTA%KCLELE, GDTA%VCLDST,
     &                         EDTA%KDIMP, EDTA%VDIMP,
     &                         EDTA%VDLG,
     &                         HMTX, F_ASM)
      IERR = SV2D_XCL144_ASMKTL(IL, IEDEB_AVL, IEFIN_AVL,
     &                         KLOCE, VKE, EDTA%KLOCN, GDTA%KNGS, GDTA%VDJS,
     &                         EDTA%VPRGL, EDTA%VPRNO,
     &                         EDTA%KCLCND, EDTA%VCLCNV,
     &                         GDTA%KCLLIM, GDTA%KCLNOD, GDTA%KCLELE, GDTA%VCLDST,
     &                         EDTA%KDIMP, EDTA%VDIMP,
     &                         EDTA%VDLG,
     &                         HMTX, F_ASM)

      SV2D_XCL144_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL144_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL144_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>144</b>: <br>
C  Culvert model with MTQ rules and formulas.
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge in the culvert Q is calculated with the actual upstream-downstream water level difference.
C  It is distributed on the wetted part of both upstream and downstream boundaries.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth dependent).
C  <ul>
C     <li>Kind: Culvert</li>d
C     <li>Code: 144</li>
C     <li>Values: Upstream limit name, 24 parameters describing the culvert</li>
C     <li>Units: </li>
C     <li>Example:  144  $__Culvert_upstream__$ 24_required_parameters</li>
C  </ul>

      SV2D_XCL144_REQHLP = 'bc_type_144'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL144_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL144_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL144_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl144.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl144.hlp')

      SV2D_XCL144_HLP = ERR_TYP()
      RETURN
      END
