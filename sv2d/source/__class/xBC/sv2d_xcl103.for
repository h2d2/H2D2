C************************************************************************
C --- Copyright (c) INRS 2010-2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Condition de Dirichlet sur le niveau d'eau h
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL103_M

      USE SV2D_XCL000_M
      IMPLICIT NONE

      TYPE, EXTENDS(SV2D_XCL000_T) :: SV2D_XCL103_T
      CONTAINS
         PROCEDURE, PUBLIC :: COD => SV2D_XCL103_COD
         PROCEDURE, PUBLIC :: PRC => SV2D_XCL103_PRC
         
         ! ---  Méthodes statiques
         PROCEDURE, PUBLIC, NOPASS :: CMD => SV2D_XCL103_CMD
         PROCEDURE, PUBLIC, NOPASS :: HLP => SV2D_XCL103_HLP
      END TYPE SV2D_XCL103_T

      PUBLIC :: SV2D_XCL103_CTR

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XCL103_CTR
C
C Description:
C     Le constructeur <code>SV2D_XCL103_CTR</code> construit une C.L.
C     de type <code>103</code>.
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL103_CTR() RESULT(SELF)

      TYPE(SV2D_XCL103_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL103_CTR

C************************************************************************
C Sommaire:  SV2D_XCL103_COD
C
C Description:
C     La méthode <code>SV2D_XCL103_COD</code>  assigne les codes de
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     KDIMP       Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL103_COD(SELF, KDIMP)

      CLASS(SV2D_XCL103_T), INTENT(IN) :: SELF
      INTEGER, INTENT(INOUT) :: KDIMP(:,:)

      INCLUDE 'eacdcl.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IN, INDEB, INFIN
      INTEGER ITYP
      INTEGER NVAL

      INTEGER, PARAMETER, DIMENSION(2,4) :: KCLDIM =
     &                 RESHAPE((/ 1,-1,               ! Nb de noeuds  (min, max)
     &                           -1,-1,               ! Nb d'éléments (min, max)
     &                           -1,-1,               ! Nb de limites (min, max)
     &                            1, 2/), (/2, 4/))   ! Nb de valeurs (min, max)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SELF%CHK(KCLDIM)
      NVAL = SIZE(SELF%VCND, 1)
      ITYP = SELF%IK
      IF (NVAL .EQ. 2 .AND. ITYP .NE. EG_TPLMT_1SGMT) GOTO 9902

C---     Assigne les codes
      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         IF (IN .GT. 0)
     &      KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL103_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL103_COD

C************************************************************************
C Sommaire:  SV2D_XCL103_PRC
C
C Description:
C     La méthode <code>SV2D_XCL103_PRC</code> fait l'étape de pré-calcul,
C     résultats qui ne dépendent pas des DDL.
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C     VDIMP       Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL103_PRC(SELF, ELEM, VDIMP)

      CLASS(SV2D_XCL103_T), INTENT(IN) :: SELF
      CLASS(LM_ELEM_T),INTENT(IN), TARGET :: ELEM
      REAL*8, INTENT(INOUT) :: VDIMP(:,:)

      INCLUDE 'err.fi'

      INTEGER I, IN, INDEB, INFIN
      REAL*8  DS, DV, V1, V2
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SIZE(VDIMP, 1) .LE. SIZE(SELF%VCND, 1))
C-----------------------------------------------------------------------

      V1 = SELF%VCND(LBOUND(SELF%VCND, 1))
      V2 = SELF%VCND(UBOUND(SELF%VCND, 1))
      DV = V2 - V1

      INDEB = LBOUND(SELF%KNOD, 1) 
      INFIN = UBOUND(SELF%KNOD, 1) 
      DO I = INDEB, INFIN
         IN = SELF%KNOD(I)
         DS = SELF%VDST(I)
         IF (IN .GT. 0) VDIMP(3,IN) = V1 + DS*DV
      ENDDO

      SV2D_XCL103_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL103_PRC

C************************************************************************
C Sommaire: SV2D_XCL103_HLP
C
C Description:
C     La méthode statique <code>SV2D_XCL103_HLP</code> imprime l'aide
C     de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL103_HLP()

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl103.hlp')

      SV2D_XCL103_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL103_HLP

C************************************************************************
C Sommaire:  SV2D_XCL103_CMD
C
C Description:
C     La méthode statique <code>SV2D_XCL103_CMD</code> définit la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      CHARACTER*(16) FUNCTION SV2D_XCL103_CMD()

C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>103</b>: <br>
C  Dirichlet condition on the water level.<br>
C  For the case where the limit in made of one single segment, 
C  two values can be specified. The imposed water level will be
C  linearly interpolated between start and end node. The interpolation is based on
C  the cumulative distance to start node.
C     <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 103</li>
C     <li>Values: h</li>
C     <li>Units: m</li>
C     <li>Example with a constant value:  103  10.0</li>
C     <li>Example with linear interpolation:  103  10.0 12.0</li>
C     </ul>
C</comment>

      SV2D_XCL103_CMD = 'bc_type_103'
      RETURN
      END FUNCTION SV2D_XCL103_CMD
      
      END MODULE SV2D_XCL103_M

      