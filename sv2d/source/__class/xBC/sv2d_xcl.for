C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C     Toutes les conditions limites
C
C Note:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SV2D_XCL_M

      USE SV2D_XCL000_M, ONLY : SV2D_XCL000_T

      IMPLICIT NONE

      ! ---  Auxiliary type to hold the pointer
      TYPE :: SV2D_XCL_CLPTR
         CLASS(SV2D_XCL000_T), POINTER :: CLIM
      END TYPE SV2D_XCL_CLPTR
      
      TYPE :: SV2D_XCL_T
         INTEGER, PRIVATE :: NCLIM
         CLASS(SV2D_XCL_CLPTR), ALLOCATABLE, PRIVATE :: KCLIM(:)
      CONTAINS
         PROCEDURE, PUBLIC :: INI   => SV2D_XCL_INI
         
         PROCEDURE, PUBLIC :: COD   => SV2D_XCL_COD
         PROCEDURE, PUBLIC :: PRC   => SV2D_XCL_PRC
         PROCEDURE, PUBLIC :: CLC   => SV2D_XCL_CLC
         
         PROCEDURE, PUBLIC :: ASMF  => SV2D_XCL_ASMF
         PROCEDURE, PUBLIC :: ASMKU => SV2D_XCL_ASMKU
         PROCEDURE, PUBLIC :: ASMK  => SV2D_XCL_ASMK
         PROCEDURE, PUBLIC :: ASMKT => SV2D_XCL_ASMKT
         PROCEDURE, PUBLIC :: ASMMU => SV2D_XCL_ASMMU
         PROCEDURE, PUBLIC :: ASMM  => SV2D_XCL_ASMM
      END TYPE SV2D_XCL_T

      PUBLIC :: SV2D_XCL_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: SV2D_XCL_DTR
      END INTERFACE DEL
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     Le constructeur SV2D_XCL_CTR retourne une objet nouvellement
C     alloué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL_CTR() RESULT(SELF)
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      TYPE(SV2D_XCL_T), POINTER :: SELF
C-----------------------------------------------------------------------
      
C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      SELF%NCLIM =-1
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION SV2D_XCL_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur SV2D_XCL_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_DTR(SELF)
      
      USE SV2D_XCL000_M, ONLY : DEL
      
      CLASS(SV2D_XCL_T), INTENT(INOUT), POINTER :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET, IERR
      INTEGER IL
      CLASS(SV2D_XCL000_T), POINTER :: CLIM
C-----------------------------------------------------------------------

      IF (.NOT. ASSOCIATED(SELF)) GOTO 9999

C---     Vide la table
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = DEL(CLIM)
      ENDDO

C---     Désalloue la mémoire
      DEALLOCATE (SELF%KCLIM, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      DEALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      SELF => NULL()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SV2D_XCL_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     SELF        L'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_INI(SELF, ELEM)
      
      USE LM_ELEM_M
      USE SV2D_XCL000_M
      USE SV2D_XCL001_M, ONLY: SV2D_XCL001_CTR
      USE SV2D_XCL002_M, ONLY: SV2D_XCL002_CTR
      USE SV2D_XCL003_M, ONLY: SV2D_XCL003_CTR
      USE SV2D_XCL004_M, ONLY: SV2D_XCL004_CTR
      USE SV2D_XCL100_M, ONLY: SV2D_XCL100_CTR
      USE SV2D_XCL101_M, ONLY: SV2D_XCL101_CTR
      USE SV2D_XCL102_M, ONLY: SV2D_XCL102_CTR
      USE SV2D_XCL103_M, ONLY: SV2D_XCL103_CTR
      USE SV2D_XCL104_M, ONLY: SV2D_XCL104_CTR
      USE SV2D_XCL111_M, ONLY: SV2D_XCL111_CTR
      USE SV2D_XCL112_M, ONLY: SV2D_XCL112_CTR
      USE SV2D_XCL113_M, ONLY: SV2D_XCL113_CTR
      USE SV2D_XCL213_M, ONLY: SV2D_XCL213_CTR
      USE SV2D_XCL214_M, ONLY: SV2D_XCL214_CTR

      CLASS(SV2D_XCL_T),     INTENT(INOUT) :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      
      INCLUDE 'err.fi'
      
      INTEGER IL, IC, IT, IK
      INTEGER IERR, IRET
      INTEGER INDEB, INFIN
      INTEGER IEDEB, IEFIN
      INTEGER IVDEB, IVFIN
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      CLASS(SV2D_XCL000_T),  POINTER :: CL
C-----------------------------------------------------------------------
      
C---     Récupère les attributs
      GDTA => ELEM%GDTA
      EDTA => ELEM%EDTA

C---     Alloue la table
      ALLOCATE(SELF%KCLIM(1:GDTA%NCLLIM), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      SELF%NCLIM = GDTA%NCLLIM

C---     Boucle sur les limites
      DO IL=1, GDTA%NCLLIM
         IC = GDTA%KCLLIM(2, IL)
         IT = EDTA%KCLCND(2, IC)

         ! ---  Crée la C.L. (B.C. factory)
         SELECT CASE(IT)
            CASE (001); CL => SV2D_XCL001_CTR()
            CASE (002); CL => SV2D_XCL002_CTR()
            CASE (003); CL => SV2D_XCL003_CTR()
            CASE (004); CL => SV2D_XCL004_CTR()

            CASE (100); CL => SV2D_XCL100_CTR()
            CASE (101); CL => SV2D_XCL101_CTR()
            CASE (102); CL => SV2D_XCL102_CTR()
            CASE (103); CL => SV2D_XCL103_CTR()
            CASE (104); CL => SV2D_XCL104_CTR()

            CASE (111); CL => SV2D_XCL111_CTR()
            CASE (112); CL => SV2D_XCL112_CTR()
            CASE (113); CL => SV2D_XCL113_CTR()

            !CASE (121); CL => SV2D_XCL121_CTR()
            !CASE (122); CL => SV2D_XCL122_CTR()
            !CASE (123); CL => SV2D_XCL123_CTR()
            !CASE (124); CL => SV2D_XCL124_CTR()
            !CASE (125); CL => SV2D_XCL125_CTR()
            !CASE (126); CL => SV2D_XCL126_CTR()
            !CASE (129); CL => SV2D_XCL129_CTR()
            !
            !CASE (133); CL => SV2D_XCL133_CTR()
            !CASE (138); CL => SV2D_XCL138_CTR()
            !
            !CASE (141); CL => SV2D_XCL141_CTR()
            !CASE (142); CL => SV2D_XCL142_CTR()
            !CASE (143); CL => SV2D_XCL143_CTR()
            !CASE (144); CL => SV2D_XCL144_CTR()
            !
            !CASE (203); CL => SV2D_XCL203_CTR()
            !CASE (204); CL => SV2D_XCL204_CTR()
            CASE (213); CL => SV2D_XCL213_CTR()
            CASE (214); CL => SV2D_XCL214_CTR()
            CASE DEFAULT
               CALL ERR_ASR(.FALSE.)         
         END SELECT

         ! ---  Indice dans les tables
         INDEB = GDTA%KCLLIM(3, IL)
         INFIN = GDTA%KCLLIM(4, IL)
         IEDEB = GDTA%KCLLIM(5, IL)
         IEFIN = GDTA%KCLLIM(6, IL)
         IVDEB = EDTA%KCLCND(3, IC)
         IVFIN = EDTA%KCLCND(4, IC)
         
         ! ---  Initialise
         IK = GDTA%KCLLIM(7, IL)
         IF (ERR_GOOD()) 
     &      IERR = CL%INI(IL,
     &                    IT, 
     &                    IK, 
     &                    GDTA%KCLNOD(INDEB:INFIN),
     &                    GDTA%KCLELE(IEDEB:IEFIN),
     &                    GDTA%VCLDST(INDEB:INFIN),
     &                    EDTA%VCLCNV(IVDEB:IVFIN))

         ! ---  Stocke
         IF (ERR_GOOD()) SELF%KCLIM(IL)%CLIM => CL
         
         IF (.NOT. ERR_GOOD()) EXIT
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SV2D_XCL_INI = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_INI

C************************************************************************
C Sommaire:  SV2D_XCL_COD
C
C Description:
C     La fonction SV2D_XCL_COD assigne le code de condition limite.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C     ELEM        L'élément mis à jour
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_COD(SELF, ELEM)

      USE LM_ELEM_M

      CLASS(SV2D_XCL_T),INTENT(IN),    TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: ELEM

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: EDTA
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Récupère les attributs
      EDTA => ELEM%EDTA

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%COD(EDTA%KDIMP)
      ENDDO

      SV2D_XCL_COD = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_COD
      
C************************************************************************
C Sommaire:  SV2D_XCL_PRC
C
C Description:
C     La fonction SV2D_XCL_PRC fait l'étape de pré-calcul pour les
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_PRC(SELF, ELEM)

      USE LM_ELEM_M

      CLASS(SV2D_XCL_T),INTENT(IN),    TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: ELEM

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IL
      TYPE (LM_EDTA_T), POINTER :: EDTA
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Récupère les attributs
      EDTA => ELEM%EDTA

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%PRC(ELEM, EDTA%VDIMP)
      ENDDO

      SV2D_XCL_PRC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_PRC

C************************************************************************
C Sommaire:  SV2D_XCL_CLC
C
C Description:
C     La fonction SV2D_XCL_CLC fait l'étape de pré-calcul pour les
C     conditions limites.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_CLC(SELF, ELEM)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN),    TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: ELEM

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IL
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%CLC(ELEM)
      ENDDO

      SV2D_XCL_CLC = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_CLC
      
C************************************************************************
C Sommaire:  SV2D_XCL_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMF(SELF, ELEM, VFG)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      REAL*8,           INTENT(INOUT) :: VFG(:)

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMF(ELEM, VFG)
      ENDDO

      SV2D_XCL_ASMF = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMF

C************************************************************************
C Sommaire:  SV2D_XCL_ASMKU
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMKU(SELF, ELEM, VFG)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      REAL*8,           INTENT(INOUT) :: VFG(:)

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMKU(ELEM, VFG)
      ENDDO

      SV2D_XCL_ASMKU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMKU

C************************************************************************
C Sommaire: SV2D_XCL_ASMK
C
C Description:
C     La fonction SV2D_XCL_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMK(SELF, ELEM, HMTX, F_ASM)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM
      
      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMK(ELEM, HMTX, F_ASM)
      ENDDO

      SV2D_XCL_ASMK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMK

C************************************************************************
C Sommaire: SV2D_XCL_ASMKT
C
C Description:
C     La fonction SV2D_XCL_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMKT(SELF, ELEM, HMTX, F_ASM)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMKT(ELEM, HMTX, F_ASM)
      ENDDO

      SV2D_XCL_ASMKT = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMKT

C************************************************************************
C Sommaire: SV2D_XCL_ASMM
C
C Description:
C     La fonction SV2D_XCL_ASMM calcule le matrice masse
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMM (SELF, ELEM, HMTX, F_ASM)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMM(ELEM, HMTX, F_ASM)
      ENDDO

      SV2D_XCL_ASMM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMM

C************************************************************************
C Sommaire:  SV2D_XCL_ASMMU
C
C Description:
C
C Entrée:
C     SELF        L'objet
C     ELEM        L'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_ASMMU(SELF, ELEM, VFG)

      USE LM_ELEM_M, ONLY: LM_ELEM_T

      CLASS(SV2D_XCL_T),INTENT(IN), TARGET :: SELF
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: ELEM
      REAL*8,           INTENT(INOUT) :: VFG(:)

      INCLUDE 'err.fi'

      INTEGER IL
      INTEGER IERR
      CLASS(SV2D_XCL000_T),  POINTER :: CLIM
C-----------------------------------------------------------------------

C---     Boucle sur les limites
      DO IL=1, SELF%NCLIM
         CLIM => SELF%KCLIM(IL)%CLIM
         IERR = CLIM%ASMMU(ELEM, VFG)
      ENDDO

      SV2D_XCL_ASMMU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_ASMMU

C************************************************************************
C Sommaire:  SV2D_XCL_HLP
C
C Description:
C     La fonction SV2D_XCL_HLP
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_HLP()

      USE SV2D_XCL001_M, ONLY: SV2D_XCL001_HLP
      USE SV2D_XCL002_M, ONLY: SV2D_XCL002_HLP
      USE SV2D_XCL003_M, ONLY: SV2D_XCL003_HLP
      USE SV2D_XCL004_M, ONLY: SV2D_XCL004_HLP
      USE SV2D_XCL100_M, ONLY: SV2D_XCL100_HLP
      USE SV2D_XCL101_M, ONLY: SV2D_XCL101_HLP
      USE SV2D_XCL102_M, ONLY: SV2D_XCL102_HLP
      USE SV2D_XCL103_M, ONLY: SV2D_XCL103_HLP
      USE SV2D_XCL104_M, ONLY: SV2D_XCL104_HLP
      USE SV2D_XCL111_M, ONLY: SV2D_XCL111_HLP
      USE SV2D_XCL112_M, ONLY: SV2D_XCL112_HLP
      USE SV2D_XCL113_M, ONLY: SV2D_XCL113_HLP
      USE SV2D_XCL213_M, ONLY: SV2D_XCL213_HLP
      USE SV2D_XCL214_M, ONLY: SV2D_XCL214_HLP

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SV2D_XCL001_HLP()
      IERR = SV2D_XCL002_HLP()
      IERR = SV2D_XCL003_HLP()
      IERR = SV2D_XCL004_HLP()
      IERR = SV2D_XCL100_HLP()
      IERR = SV2D_XCL101_HLP()
      IERR = SV2D_XCL102_HLP()
      IERR = SV2D_XCL103_HLP()
      IERR = SV2D_XCL104_HLP()
      IERR = SV2D_XCL111_HLP()
      IERR = SV2D_XCL112_HLP()
      IERR = SV2D_XCL113_HLP()
      IERR = SV2D_XCL213_HLP()
      IERR = SV2D_XCL214_HLP()

      SV2D_XCL_HLP = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_HLP

C************************************************************************
C Sommaire:  SV2D_XCL_COD1L
C
C Description:
C     La fonction utilitaire SV2D_XCL_COD1L assigne le code de
C     condition limite pour une condition qui n'a qu'une limite,
C     la limite destination.
C
C Entrée:
C     IL_DST         Indice de la limite destination
C     ID_DST         Indice du DDL destination
C     IT_DST         Type de la limite destination
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Le contrôle comme dans COD2L est aussi coûteux que d'assigner
C     directement.
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_COD1L(IL_DST,
     &                                ID_DST,
     &                                IT_DST,
     &                                SELF)

      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      INTEGER, INTENT(IN) :: IL_DST, ID_DST, IT_DST
      CLASS(SV2D_XBS_T), TARGET :: SELF

      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'err.fi'

      INTEGER I, IN
      INTEGER INDEB_DST, INFIN_DST
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IT_DST .GT. 0)
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
D     CALL ERR_ASR(IL_DST .GE. 1 .AND. IL_DST .LE. GDTA%NCLLIM)
D     CALL ERR_ASR(ID_DST .GE. 1 .AND. ID_DST .LE. EDTA%NDLN)

C---     Indices
      INDEB_DST = GDTA%KCLLIM(3, IL_DST)
      INFIN_DST = GDTA%KCLLIM(4, IL_DST)

C---     Impose la limite DESTINATION (self)
      DO I = INDEB_DST, INFIN_DST
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0)
     &      EDTA%KDIMP(3,IN) = IBSET(EDTA%KDIMP(ID_DST,IN), IT_DST)
      ENDDO

      SV2D_XCL_COD1L = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_COD1L

C************************************************************************
C Sommaire:  SV2D_XCL_COD2L
C
C Description:
C     La fonction utilitaire SV2D_XCL_COD2L assigne le code de
C     condition limite pour une condition qui a deux limites,
C     la limite destination et la limite source. Si la limite
C     source n'est pas encore connectée (i.e contient encore
C     le CRC32) alors elle sera connectée et en sortie IL_SRC
C     contiendra l'indice de la limite.
C
C Entrée:
C     IL_DST         Indice de la limite destination
C     ID_DST         Indice du DDL destination
C     IT_DST         Type de la limite destination
C     IL_SRC         Indice ou CRC32 de la limite source
C     ID_SRC         Indice du DDL source
C     IT_SRC         Type de la limite source
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     IL_SRC         Indice de la limite connectée
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XCL_COD2L(IL_DST,
     &                                ID_DST,
     &                                IT_DST,
     &                                IL_SRC,
     &                                ID_SRC,
     &                                IT_SRC,
     &                                SELF)

      USE SV2D_XBS_M, ONLY: SV2D_XBS_T
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      INTEGER, INTENT(IN)    :: IL_DST, ID_DST, IT_DST
      INTEGER, INTENT(INOUT) :: IL_SRC, ID_SRC, IT_SRC
      CLASS(SV2D_XBS_T), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER I, IN
      INTEGER ICRC
      INTEGER INDEB_DST, INFIN_DST
      INTEGER INDEB_SRC, INFIN_SRC
      INTEGER NVU
      LOGICAL DEJAVU
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IT_DST .GT. 0)
D     CALL ERR_PRE(IL_SRC .NE. IL_DST)
D     CALL ERR_PRE(IT_SRC .GT. 0)
D     CALL ERR_PRE(IT_SRC .NE. IT_DST)
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
D     CALL ERR_ASR(IL_DST .GE. 1 .AND. IL_DST .LE. GDTA%NCLLIM)
D     CALL ERR_ASR(ID_DST .GE. 1 .AND. ID_DST .LE. EDTA%NDLN)
D     CALL ERR_ASR(ID_SRC .GE. 1 .AND. ID_SRC .LE. EDTA%NDLN)

C---     Indices
      INDEB_DST = GDTA%KCLLIM(3, IL_DST)
      INFIN_DST = GDTA%KCLLIM(4, IL_DST)
      IF (IL_SRC .GE. 1 .AND. IL_SRC .LE. GDTA%NCLLIM) THEN
         INDEB_SRC = GDTA%KCLLIM(3, IL_SRC)
         INFIN_SRC = GDTA%KCLLIM(4, IL_SRC)
         DEJAVU = .TRUE.
      ELSE
         INDEB_SRC =  1
         INFIN_SRC = -1
         DEJAVU = .FALSE.
      ENDIF

C---     Détecte si on a déjà passé
      IF (DEJAVU) THEN
         NVU = 0
         DO I = INDEB_DST, INFIN_DST
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0) THEN
               IF (BTEST(EDTA%KDIMP(ID_DST,IN), IT_DST)) NVU = NVU + 1
            ENDIF
         ENDDO
         DEJAVU = NVU .EQ. (INFIN_DST-INDEB_DST+1)
      ENDIF
      IF (DEJAVU) THEN
         NVU = 0
         DO I = INDEB_SRC, INFIN_SRC
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0) THEN
               IF (BTEST(EDTA%KDIMP(ID_SRC,IN), IT_SRC)) NVU = NVU + 1
            ENDIF
         ENDDO
         DEJAVU = NVU .EQ. (INFIN_SRC-INDEB_SRC+1)
      ENDIF

C---     Fait le lien avec la limite source
      IF (.NOT. DEJAVU) THEN
         ICRC = IL_SRC
         IL_SRC = -1
         DO I=1,GDTA%NCLLIM
            IF (GDTA%KCLLIM(1,I) .EQ. ICRC) IL_SRC = I
         ENDDO
         IF (IL_SRC .LE. 0) GOTO 9900
      ENDIF

C---     Impose la limite DESTINATION (self)
      IF (.NOT. DEJAVU) THEN
         INDEB_DST = GDTA%KCLLIM(3, IL_DST)
         INFIN_DST = GDTA%KCLLIM(4, IL_DST)
         DO I = INDEB_DST, INFIN_DST
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0) THEN
         EDTA%KDIMP(ID_DST,IN) = IBSET(EDTA%KDIMP(ID_DST,IN), IT_DST)
            ENDIF
         ENDDO
      ENDIF

C---     Impose la limite SOURCE
      IF (.NOT. DEJAVU) THEN
         INDEB_SRC = GDTA%KCLLIM(3, IL_SRC)
         INFIN_SRC = GDTA%KCLLIM(4, IL_SRC)
         DO I = INDEB_SRC, INFIN_SRC
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0) THEN
         EDTA%KDIMP(ID_SRC,IN) = IBSET(EDTA%KDIMP(ID_SRC,IN), IT_SRC)
            ENDIF
         ENDDO
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XCL_COD2L = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XCL_COD2L

      END MODULE SV2D_XCL_M

