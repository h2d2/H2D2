C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Ponceau MTQ.
C     Le débit calculé sur la différence des niveaux amont-aval est imposé
C     sous forme faible à l'aval. Le niveau amont, calculé à partir du
C     niveau est imposé comme condition de Weak Dirichlet
C
C Functions:
C   Public:
C     INTEGER SV2D_XCL143_000
C     INTEGER SV2D_XCL143_999
C     INTEGER SV2D_XCL143_COD
C     INTEGER SV2D_XCL143_CLC
C     INTEGER SV2D_XCL143_ASMF
C     INTEGER SV2D_XCL143_ASMKU
C     INTEGER SV2D_XCL143_ASMK
C     INTEGER SV2D_XCL143_ASMKT
C     INTEGER SV2D_XCL143_HLP
C   Private:
C     INTEGER SV2D_XCL143_INIVTBL
C     INTEGER SV2D_XCL143_CLC_
C     SUBROUTINE SV2D_XCL143_ASMKE_S
C     CHARACTER*(16) SV2D_XCL143_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_XCL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_XCL143_DATA_000

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl143.fc'

      DATA SV2D_XCL143_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL143_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_000
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_XCL143_INIVTBL()

      SV2D_XCL143_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_999
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_XCL143_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_XCL143_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_INIVTBL()

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR !!!, IRET
      INTEGER HVFT
!!!      INTEGER LTPN
      EXTERNAL SV2D_XCL143_HLP
      EXTERNAL SV2D_XCL143_COD
      EXTERNAL SV2D_XCL143_CLC
      EXTERNAL SV2D_XCL143_ASMF
      EXTERNAL SV2D_XCL143_ASMKU
      EXTERNAL SV2D_XCL143_ASMK
      EXTERNAL SV2D_XCL143_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_XCL143_TYP .LE. SV2D_XCL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_XCL143_TPN)
!!!      IRET = C_ST_CRC32(SV2D_XCL143_TPN(1:LTPN), SV2D_XCL143_TPC)
!!!      IERR = SV2D_XCL_INIVTBL2(HVFT, SV2D_XCL143_TPN)
      IERR = SV2D_XCL_INIVTBL(HVFT, SV2D_XCL143_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_HLP, SV2D_XCL143_TYP,
     &                     SV2D_XCL143_HLP)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_COD,  SV2D_XCL143_TYP,
     &                     SV2D_XCL143_COD)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_CLC,  SV2D_XCL143_TYP,
     &                     SV2D_XCL143_CLC)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMF, SV2D_XCL143_TYP,
     &                     SV2D_XCL143_ASMF)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKU,SV2D_XCL143_TYP,
     &                     SV2D_XCL143_ASMKU)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMK, SV2D_XCL143_TYP,
     &                     SV2D_XCL143_ASMK)
      IERR=SV2D_XCL_AJTFNC (HVFT, SV2D_XCL_FUNC_ASMKT,SV2D_XCL143_TYP,
     &                     SV2D_XCL143_ASMKT)

      SV2D_XCL143_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL143_COD
C
C Description:
C     La fonction SV2D_XCL143_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C
C Sortie:
C     EDTA%KDIMP          Codes des ddl imposés
C
C Notes:
C     On ne contrôle pas le type de la CL amont
C************************************************************************
      FUNCTION SV2D_XCL143_COD(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_COD
CDEC$ ENDIF

      USE SV2D_XBS_M
      USE SV2D_XCL_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      BYTE,    INTENT(IN) :: BELF(*)

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, ID_AMT
      INTEGER IL_AVL, ID_AVL
      TYPE (SV2D_XBS_T),POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_XCL_CHKDIM(IL, EDTA%KCLCND, GDTA%KCLLIM, 25)

C---     Les indices
      IC = GDTA%KCLLIM(2, IL)
      IV = EDTA%KCLCND(3, IC)

C---     Assigne les codes
      IL_AVL = IL
      ID_AVL = 3
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      ID_AMT = 3
      IERR = SV2D_XCL_COD2L(IL_AVL, ID_AVL, SV2D_XCL_DEBIT,     ! aval
     &                      IL_AMT, ID_AMT, EA_TPCL_WEAKDIR,    ! amont
     &                      GDTA%KCLLIM,
     &                      GDTA%KCLNOD,
     &                      GDTA%KCLELE,
     &                      EDTA%KDIMP)
      IF (ERR_GOOD()) EDTA%VCLCNV(IV) = IL_AMT

C---     Contrôle la limite amont
      IERR = SV2D_XCL_CHKDIM(IL_AMT, EDTA%KCLCND, GDTA%KCLLIM, -1)

      SV2D_XCL143_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL143_CLC_
C
C Description:
C     La fonction privée SV2D_XCL143_CLC_ assigne la valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C     Elle permet de simplifier les arguments et est appelée également par
C     ASMKTL
C************************************************************************
      FUNCTION SV2D_XCL143_CLC_(IL,
     &                         GDTA%KNGS,
     &                         GDTA%VDJS,
     &                         EDTA%VPRGL,
     &                         EDTA%VPRNO,
     &                         EDTA%KCLCND,
     &                         EDTA%VCLCNV,
     &                         GDTA%KCLLIM,
     &                         GDTA%KCLNOD,
     &                         GDTA%KCLELE,
     &                         GDTA%VCLDST,
     &                         EDTA%KDIMP,
     &                         EDTA%VDIMP,
     &                         EDTA%VDLG)

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spmtq.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IE, IES, I, IN
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT, INDEB_AMT, INFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL, INDEB_AVL, INFIN_AVL
      INTEGER NO1, NO2, NO3
      INTEGER NVAL
      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QY1, H1, P1, F1
      REAL*8  QX2, QY2, P2, F2
      REAL*8  QX3, QY3, H3, P3, F3
      REAL*8  QX, QY
      REAL*8  Q_US, H_US, L_US, S_US
      REAL*8  Q_DS, H_DS, L_DS, S_DS
      REAL*8  Q_Pct, Q_Med, Q_srf, H_PctUs
      REAL*8  RS(8), RG(8)
      LOGICAL ENEAU

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
      REAL*8, PARAMETER :: DREL_MAX  = 0.15D0     ! Delta relatif max
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
      REAL*8 V1, V2
      REAL*8 ERR_REL
      ERR_REL(V1, V2) = 0.5D0*(ABS(V1-V2) / ABS(V1+V2))
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.bc.cl143'

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      INDEB_AMT = GDTA%KCLLIM(3, IL_AMT)
      INFIN_AMT = GDTA%KCLLIM(4, IL_AMT)
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)
      INDEB_AVL = GDTA%KCLLIM(3, IL_AVL)
      INFIN_AVL = GDTA%KCLLIM(4, IL_AVL)
      IEDEB_AVL = GDTA%KCLLIM(5, IL_AVL)
      IEFIN_AVL = GDTA%KCLLIM(6, IL_AVL)

C---     Valeurs de la condition
      NVAL = EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)            ! +1 mais -la première
      IERR = SP_MTQ_ASGPRM(NVAL, EDTA%VCLCNV(IV+1))

C---     Débit et Niveau amont
      Q_US = 0.0D0
      H_US = 0.0D0
      S_US = 0.0D0
      L_US = 0.0D0
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités du L3
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Métriques
         VNX =  GDTA%VDJS(2,IES)    ! VNX =  VTY
         VNY = -GDTA%VDJS(1,IES)    ! VNY = -VTX
         DJL3=  GDTA%VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = EDTA%VDLG (1,NO1)   ! Qx
         QY1 = EDTA%VDLG (2,NO1)   ! Qy
         H1  = EDTA%VDLG (3,NO1)   ! h
         QX2 = EDTA%VDLG (1,NO2)
         QY2 = EDTA%VDLG (2,NO2)
         QX3 = EDTA%VDLG (1,NO3)
         QY3 = EDTA%VDLG (2,NO3)
         H3  = EDTA%VDLG (3,NO3)
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_US = Q_US + (QX+QY)*DJL2          ! Débit
         H_US = H_US + (H1+H3)*DJL3          ! h*L
         S_US = S_US + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_US = L_US + (UN+UN)*DJL3          ! Longueur
199      CONTINUE
      ENDDO

C---     Débit et Niveau aval
      Q_DS = 0.0D0
      H_DS = 0.0D0
      S_DS = 0.0D0
      L_DS = 0.0D0
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = GDTA%KCLELE(IE)

C---        Connectivités du L3
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Métriques
         VNX =  GDTA%VDJS(2,IES)    ! VNX =  VTY
         VNY = -GDTA%VDJS(1,IES)    ! VNY = -VTX
         DJL3=  GDTA%VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = EDTA%VDLG (1,NO1)   ! Qx
         QY1 = EDTA%VDLG (2,NO1)   ! Qy
         H1  = EDTA%VDLG (3,NO1)   ! h
         QX2 = EDTA%VDLG (1,NO2)
         QY2 = EDTA%VDLG (2,NO2)
         QX3 = EDTA%VDLG (1,NO3)
         QY3 = EDTA%VDLG (2,NO3)
         H3  = EDTA%VDLG (3,NO3)
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_DS = Q_DS + (QX+QY)*DJL2          ! Débit
         H_DS = H_DS + (H1+H3)*DJL3          ! h*Longueur
         S_DS = S_DS + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_DS = L_DS + (UN+UN)*DJL3          ! Longueur totale
299      CONTINUE
      ENDDO

C---     Réduction et contrôle
      RS(1) = Q_US
      RS(2) = H_US
      RS(3) = L_US
      RS(4) = S_US
      RS(5) = Q_DS
      RS(6) = H_DS
      RS(7) = L_DS
      RS(8) = S_DS
      CALL MPI_ALLREDUCE(RS, RG, 8, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      Q_US = RG(1)
      H_US = RG(2)
      L_US = RG(3)
      S_US = RG(4)
      Q_DS = -RG(5)           ! négatif, débit entrant
      H_DS = RG(6)
      L_DS = RG(7)
      S_DS = RG(8)

C---     Calcule les données hydrauliques du ponceau
      Q_Pct = 0.0D0
      ENEAU = .TRUE.
      IF (L_US .LT. PETIT .OR. L_DS .LT. PETIT) THEN
         S_US = 1.0D0
         S_DS = 1.0D0
         ENEAU = .FALSE.
      ENDIF
      IF (ENEAU) THEN
         H_US = (H_US / L_US)    ! Niveau moyen
         H_DS = (H_DS / L_DS)    ! Niveau moyen
         Q_Pct   = SP_MTQ_CLCQ(H_US, H_DS)
         H_PctUs = SP_MTQ_CLCH(H_US, H_DS, Q_US)
      ENDIF

C---     Log
!      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
!     &      'Culvert ', IL,': H (up, down, clc): ', H_US, H_DS, H_PctUs
!      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
     &      'Culvert ', IL,': Q (up, down, clc): ', Q_US, Q_DS, Q_Pct
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Impose le niveau en amont (avec amortissement au besoin)
      IF (ENEAU) THEN
         IF (ERR_REL(H_PctUs, H_US) .GT. 0.15D0)
     &      H_PctUs = 0.5D0*(H_PctUs + H_US)
         DO I = INDEB_AMT, INFIN_AMT
            IN = GDTA%KCLNOD(I)
            IF (IN .GT. 0) EDTA%VDIMP(3,IN) = H_PctUs
         ENDDO
      ENDIF
C---     Impose le débit en aval (avec amortissement au besoin)
      IF (ENEAU) THEN
         Q_Med = UN_2*(Q_US+Q_DS)
         IF (ERR_REL(Q_Pct, Q_Med) .GT. DREL_MAX)
     &      Q_Pct = Q_Med + DREL_MAX*(Q_Pct-Q_Med)
      ENDIF
      Q_srf = - Q_Pct / S_DS                 ! Négatif, débit entrant
      DO I = INDEB_AVL, INFIN_AVL
         IN = GDTA%KCLNOD(I)
         IF (IN .GT. 0) EDTA%VDIMP(3,IN) = Q_srf  ! Débit spécifique (m/s)
      ENDDO

      SV2D_XCL143_CLC_ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL143_CLC
C
C Description:
C     La fonction privée SV2D_XCL143_CLC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     EDTA%KCLCND         Liste des conditions
C     EDTA%VCLCNV         Valeurs associées aux conditions
C     GDTA%KCLLIM         Liste des limites
C     GDTA%KCLNOD         Noeuds des limites
C     GDTA%KCLELE         Éléments des limites
C     EDTA%KDIMP          Codes des ddl imposés
C
C Sortie:
C     EDTA%VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_CLC(IL, BELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_CLC
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
C-----------------------------------------------------------------------

      IERR = SV2D_XCL143_CLC_(IL,
     &                       GDTA%KNGS,
     &                       GDTA%VDJS,
     &                       EDTA%VPRGL,
     &                       EDTA%VPRNO,
     &                       EDTA%KCLCND,
     &                       EDTA%VCLCNV,
     &                       GDTA%KCLLIM,
     &                       GDTA%KCLNOD,
     &                       GDTA%KCLELE,
     &                       GDTA%VCLDST,
     &                       EDTA%KDIMP,
     &                       EDTA%VDIMP,
     &                       EDTA%VDLG)

      SV2D_XCL143_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL143_ASMF
C
C Description:
C     La fonction SV2D_XCL143_ASMF calcul le terme de sollicitation.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_ASMF(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_ASMF
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
      INTEGER IE, IES
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL
      INTEGER NO1, NO2, NO3
      REAL*8  C
      REAL*8  V1, V3
      REAL*8  P1, F1, Q1
      REAL*8  P2, F2, Q2
      REAL*8  P3, F3, Q3
      REAL*8  M(2)

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)
      IEDEB_AVL = GDTA%KCLLIM(5, IL_AVL)
      IEFIN_AVL = GDTA%KCLLIM(6, IL_AVL)

C---     Boucle sur la limite amont (Weak Dirichlet sur h)
C        =================================================
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Coefficient
         C = GDTA%VDJS(3,IES)

C---        Valeurs nodales
         V1 = EDTA%VDIMP(3,NO1)
         V3 = EDTA%VDIMP(3,NO3)

C---        Assemble le vecteur global lumped
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), C*V1, VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), C*V3, VFG)
199      CONTINUE
      ENDDO

C---     Boucle sur la limite aval (sollicitation sur h)
C        ===============================================
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 299

C---        Coefficient
         C = -UN_12*GDTA%VDJS(3,IES)              ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     EDTA%VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     EDTA%VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(EDTA%VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     EDTA%VPRNO(SV2D_IPRNO_H, NO3)
         Q1 = EDTA%VDIMP(3,NO1)
         Q2 = Q1            ! est-il assigné?
         Q3 = EDTA%VDIMP(3,NO3)

C---        Règle de répartition d'après Manning
         Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = Q1     !! EDTA%VDIMP(3,NO2) ! est-il assigné?
         Q3 = Q3*(P3**CINQ_TIER) / F3

C---        Assemble le vecteur global
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, EDTA%KLOCN(3,NO3), M(2), VFG)
299      CONTINUE
      ENDDO

      SV2D_XCL143_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XCL143_ASMKU
C
C Description:
C     La fonction SV2D_XCL143_ASMKU est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_ASMKU(IL, GDTA, EDTA, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_ASMKU
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER NDLE_LCL
      PARAMETER (NDLE_LCL = 9)

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IE, IES
      INTEGER NO1, NO2, NO3
      REAL*8  VDLE(NDLE_LCL)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
D     CALL ERR_PRE(EDTA%NDLEV .GE. NDLE_LCL)
C-----------------------------------------------------------------------

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)

C---     Initialise la matrice élémentaire
      CALL DINIT(NDLE_LCL*NDLE_LCL, ZERO, VKE, 1)

C---     Boucle sur la limite amont
C        ==========================
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Transfert des DDL
         VDLE(1) = EDTA%VDLG(1,NO1)
         VDLE(2) = EDTA%VDLG(2,NO1)
         VDLE(3) = EDTA%VDLG(3,NO1)
         VDLE(4) = EDTA%VDLG(1,NO2)
         VDLE(5) = EDTA%VDLG(2,NO2)
         VDLE(6) = EDTA%VDLG(3,NO2)
         VDLE(7) = EDTA%VDLG(1,NO3)
         VDLE(8) = EDTA%VDLG(2,NO3)
         VDLE(9) = EDTA%VDLG(3,NO3)

C---        Matrice [K(u)]
         CALL SV2D_XCL143_ASMKE_S(NDLE_LCL, VKE, GDTA%VDJS(1,IES))

C---        Produit matrice-vecteur [K(u)].{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              NDLE_LCL,          ! m rows
     &              NDLE_LCL,          ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              NDLE_LCL,          ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---        Table KLOCE de l'élément
         KLOCE(1) = EDTA%KLOCN(1,NO1)
         KLOCE(2) = EDTA%KLOCN(2,NO1)
         KLOCE(3) = EDTA%KLOCN(3,NO1)
         KLOCE(4) = EDTA%KLOCN(1,NO2)
         KLOCE(5) = EDTA%KLOCN(2,NO2)
         KLOCE(6) = EDTA%KLOCN(3,NO2)
         KLOCE(7) = EDTA%KLOCN(1,NO3)
         KLOCE(8) = EDTA%KLOCN(2,NO3)
         KLOCE(9) = EDTA%KLOCN(3,NO3)

C---        Assemblage du vecteur global
         IERR = SP_ELEM_ASMFE(NDLE_LCL, KLOCE, VFE, VFG)

199      CONTINUE
      ENDDO

      SV2D_XCL143_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL143_ASMK
C
C Description:
C     La fonction SV2D_XCL143_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_ASMK(IL,
     &                          GDTA,
     &                          EDTA,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_ASMK
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INTEGER NDLE_LCL
      PARAMETER (NDLE_LCL = 9)

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IE, IES
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
D     CALL ERR_PRE(EDTA%NDLEV .GE. NDLE_LCL)
C-----------------------------------------------------------------

      IC = GDTA%KCLLIM(2,IL)
D     CALL ERR_ASR(EDTA%KCLCND(4,IC)-EDTA%KCLCND(3,IC)+1 .EQ. 25)
      IV = EDTA%KCLCND(3,IC)

C---     Indices des limites
      IL_AMT = NINT( EDTA%VCLCNV(IV) )
      IEDEB_AMT = GDTA%KCLLIM(5, IL_AMT)
      IEFIN_AMT = GDTA%KCLLIM(6, IL_AMT)

C---     Initialise la matrice élémentaire
      CALL DINIT(NDLE_LCL*NDLE_LCL, ZERO, VKE, 1)

C---     Boucle sur la limite amont
C        ==========================
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = GDTA%KCLELE(IE)

C---        Connectivités
         NO1 = GDTA%KNGS(1,IES)
         NO2 = GDTA%KNGS(2,IES)
         NO3 = GDTA%KNGS(3,IES)
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(EDTA%KDIMP(3,NO3))) GOTO 199

C---        Matrice [K(u)]
         CALL SV2D_XCL143_ASMKE_S(NDLE_LCL, VKE, GDTA%VDJS(1,IES))

C---        Table KLOCE de l'élément
         KLOCE(1) = EDTA%KLOCN(1,NO1)
         KLOCE(2) = EDTA%KLOCN(2,NO1)
         KLOCE(3) = EDTA%KLOCN(3,NO1)
         KLOCE(4) = EDTA%KLOCN(1,NO2)
         KLOCE(5) = EDTA%KLOCN(2,NO2)
         KLOCE(6) = EDTA%KLOCN(3,NO2)
         KLOCE(7) = EDTA%KLOCN(1,NO3)
         KLOCE(8) = EDTA%KLOCN(2,NO3)
         KLOCE(9) = EDTA%KLOCN(3,NO3)

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

199      CONTINUE
      ENDDO

      SV2D_XCL143_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL143_ASMKT
C
C Description:
C     La fonction SV2D_XCL143_ASMKT calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_ASMKT(IL,
     &                           GDTA,
     &                           EDTA,
     &                           HMTX,
     &                           F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_ASMKT
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IL
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
      INTEGER, INTENT(IN) :: HMTX
      INTEGER, EXTERNAL   :: F_ASM

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER   IERR
C----------------------------------------------------------------
D     IC = GDTA%KCLLIM(2, IL)
D     CALL ERR_PRE(EDTA%KCLCND(2,IC) .EQ. SV2D_XCL143_TYP)
C-----------------------------------------------------------------

      IERR = SV2D_XCL143_ASMK(IL, GDTA, EDTA, HMTX, F_ASM)

      SV2D_XCL143_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_XCL143_ASMKE_S
C
C Description:
C     La fonction SV2D_XCL143_ASMKE_S calcule le matrice de rigidité
C     élémentaire pour un élément de surface.
C     Elle calcule:
C        2) la matrice masse pour une CL de weak-Dirichlet en h
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_XCL143_ASMKE_S(NDLE, VKE, GDTA%VDJS)

      USE SV2D_XBS_M
      IMPLICIT NONE


      INTEGER  NDLE
      REAL*8   VKE  (NDLE, NDLE)
      REAL*8   GDTA%VDJS (GDTA%NDJS)

      INCLUDE 'sv2d_subt3.fi'

      INTEGER IKH1, IKH2, IKH3
      REAL*8  ML
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Indices dans VKE
      IKH1 = 3
D     IKH2 = 6
      IKH3 = 9

C---     CL Weak-Dirichlet: Matrice masse lumped
      ML = GDTA%VDJS(3)
      VKE(IKH1, IKH1) = ML
      VKE(IKH3, IKH3) = ML

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL143_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_REQHLP()

      USE SV2D_XBS_M
      IMPLICIT NONE

      CHARACTER*(16) SV2D_XCL143_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>143</b>: <br>
C  Culvert model with MTQ rules and formulas.
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge in the culvert Q is calculated with the actual upstream-downstream water level difference.
C  It is distributed on the wetted part of the downstream boundary.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth dependent).
C  <p>
C  The upstream water level is also imposed as a weak Dirichlet condition.
C  <ul>
C     <li>Kind: Culvert</li>d
C     <li>Code: 143</li>
C     <li>Values: Upstream limit name, 24 parameters describing the culvert</li>
C     <li>Units: </li>
C     <li>Example:  143  $__Culvert_upstream__$ 24_required_parameters</li>
C  </ul>

      SV2D_XCL143_REQHLP = 'bc_type_143'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_XCL143_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XCL143_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XCL143_HLP
CDEC$ ENDIF

      USE SV2D_XBS_M
      IMPLICIT NONE

      INCLUDE 'sv2d_xcl143.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_xcl143.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_xcl143.hlp')

      SV2D_XCL143_HLP = ERR_TYP()
      RETURN
      END
