C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C
C Notes:
C
C Functions:
C************************************************************************

      MODULE SV2D_XPRG_M

      USE ISO_C_BINDING
      IMPLICIT NONE
      
!!!C---     Valeurs des PRGL
!!!      TYPE :: SV2D_XPRG_T
!!!         REAL*8  GRAVITE
!!!         REAL*8  CORIOLIS
!!!         REAL*8  VISCO_CST
!!!         REAL*8  VISCO_LM
!!!         REAL*8  VISCO_SMGO
!!!         REAL*8  VISCO_BINF
!!!         REAL*8  VISCO_BSUP
!!!         REAL*8  DECOU_HTRG
!!!         REAL*8  DECOU_HMIN
!!!         REAL*8  DECOU_DHST
!!!         REAL*8  DECOU_UN_HDIF
!!!         REAL*8  DECOU_PENA_H
!!!         REAL*8  DECOU_PENA_Q
!!!         REAL*8  DECOU_PORO
!!!         REAL*8  DECOU_MAN
!!!         REAL*8  DECOU_UMAX
!!!         REAL*8  DECOU_AMORT
!!!         REAL*8  DECOU_CON_FACT
!!!         REAL*8  DECOU_GRA_FACT
!!!         REAL*8  DECOU_DIF_NU
!!!         REAL*8  DECOU_DRC_NU
!!!         REAL*8  STABI_PECLET
!!!         REAL*8  STABI_AMORT
!!!         REAL*8  STABI_DARCY
!!!         REAL*8  STABI_LAPIDUS
!!!         REAL*8  CMULT_CON
!!!         REAL*8  CMULT_MAN
!!!         REAL*8  CMULT_GRA
!!!         REAL*8  CMULT_PDYN
!!!         REAL*8  CMULT_VENT
!!!         REAL*8  CMULT_INTGCTR
!!!         REAL*8  PNUMR_PENALITE
!!!         REAL*8  PNUMR_DELPRT
!!!         REAL*8  PNUMR_DELMIN
!!!         REAL*8  PNUMR_OMEGAKT
!!!         REAL*8  CMULT_VENT_REL
!!!         INTEGER FCT_CW_VENT
!!!         LOGICAL PNUMR_PRTPREL
!!!         LOGICAL PNUMR_PRTPRNO
!!!      END TYPE SV2D_XPRG_T

      
C---     Valeurs des PRGL
      TYPE, BIND(C) :: SV2D_XPRG_T
         REAL(C_DOUBLE) :: GRAVITE
         REAL(C_DOUBLE) :: CORIOLIS
         REAL(C_DOUBLE) :: VISCO_CST
         REAL(C_DOUBLE) :: VISCO_LM
         REAL(C_DOUBLE) :: VISCO_SMGO
         REAL(C_DOUBLE) :: VISCO_BINF
         REAL(C_DOUBLE) :: VISCO_BSUP
         REAL(C_DOUBLE) :: DECOU_HTRG
         REAL(C_DOUBLE) :: DECOU_HMIN
         REAL(C_DOUBLE) :: DECOU_DHST
         REAL(C_DOUBLE) :: DECOU_UN_HDIF
         REAL(C_DOUBLE) :: DECOU_PENA_H
         REAL(C_DOUBLE) :: DECOU_PENA_Q
         REAL(C_DOUBLE) :: DECOU_PORO
         REAL(C_DOUBLE) :: DECOU_MAN
         REAL(C_DOUBLE) :: DECOU_UMAX
         REAL(C_DOUBLE) :: DECOU_AMORT
         REAL(C_DOUBLE) :: DECOU_CON_FACT
         REAL(C_DOUBLE) :: DECOU_GRA_FACT
         REAL(C_DOUBLE) :: DECOU_DIF_NU
         REAL(C_DOUBLE) :: DECOU_DIF_PE
         REAL(C_DOUBLE) :: DECOU_DRC_NU
         REAL(C_DOUBLE) :: STABI_PECLET
         REAL(C_DOUBLE) :: STABI_AMORT
         REAL(C_DOUBLE) :: STABI_DARCY
         REAL(C_DOUBLE) :: STABI_LAPIDUS
         REAL(C_DOUBLE) :: CMULT_CON
         REAL(C_DOUBLE) :: CMULT_MAN
         REAL(C_DOUBLE) :: CMULT_GRA
         REAL(C_DOUBLE) :: CMULT_PDYN
         REAL(C_DOUBLE) :: CMULT_VENT
         REAL(C_DOUBLE) :: CMULT_INTGCTRQX
         REAL(C_DOUBLE) :: CMULT_INTGCTRQY
         REAL(C_DOUBLE) :: CMULT_INTGCTRH
         REAL(C_DOUBLE) :: PNUMR_PENALITE
         REAL(C_DOUBLE) :: PNUMR_DELPRT
         REAL(C_DOUBLE) :: PNUMR_DELMIN
         REAL(C_DOUBLE) :: PNUMR_OMEGAKT
         REAL(C_DOUBLE) :: CMULT_VENT_REL
         INTEGER(C_INT) :: FCT_CW_VENT
         LOGICAL(C_BOOL):: PNUMR_PRTPREL
         LOGICAL(C_BOOL):: PNUMR_PRTPRNO
      END TYPE SV2D_XPRG_T

      CONTAINS
      
C************************************************************************
C Sommaire: Retourne la dimension de la structure
C
C Description:
C     La fonction SV2D_XPRG_REQNRE8 retourne la dimension de la structure
C     en terme de REAL*8
C
C Entrée:
C      XPRG:      La structure
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XPRG_REQNRE8(XPRG) RESULT(N)
      
      TYPE(SV2D_XPRG_T), INTENT(IN) :: XPRG
C-----------------------------------------------------------------------
      N = SIZEOF(XPRG) / SIZEOF(XPRG%GRAVITE)
      IF (N*SIZEOF(XPRG%GRAVITE) .GT. SIZEOF(XPRG))
     &      N = N + 1

      RETURN 
      END FUNCTION SV2D_XPRG_REQNRE8
      
C************************************************************************
C Sommaire: Retourne la structure sous forme de pointeur REAL*8
C
C Description:
C     La fonction SV2D_XPRG_REQPRE8 retourne le début de la structure
C     comme pointeur à une table REAL*8
C
C Entrée:
C      XPRG:      La structure
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XPRG_REQPRE8(XPRG) RESULT(PTR)

      REAL*8, DIMENSION(:), POINTER :: PTR
      TYPE(SV2D_XPRG_T), INTENT(IN), TARGET :: XPRG

      TYPE (C_PTR) :: CELF
      INTEGER :: N
C-----------------------------------------------------------------------
      N = SV2D_XPRG_REQNRE8(XPRG)
      CELF = C_LOC(XPRG%GRAVITE)
      CALL C_F_POINTER(CELF, PTR, [N])

      RETURN
      END FUNCTION SV2D_XPRG_REQPRE8
      
      END MODULE SV2D_XPRG_M
