C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_XBS_HLPPRNO
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_HLPPRNO_M
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_HLPPRNO
C
C Description:
C     Aide sur les propriétés nodales
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_HLPPRNO(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_HLPPRNO
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, IERR
      INTEGER SV2D_XBS_HLP1
C-----------------------------------------------------------------------

      I = 0

C---     IMPRIME ENTETE
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     PRNO LUES
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_ZF')
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_MANNING')
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_GLACE_EPAISSEUR')
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_GLACE_COEF_MANNING')
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_VENT_X')
      I = I + 1
      IERR = SV2D_XBS_HLP1(I,'MSG_VENT_Y')

      CALL LOG_DECIND()

      SV2D_XBS_HLPPRNO = ERR_TYP()
      RETURN
      END

      END SUBMODULE SV2D_XBS_HLPPRNO_M
