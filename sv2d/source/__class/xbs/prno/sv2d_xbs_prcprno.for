C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_prcprno.for,v 1.23 2015/01/10 17:02:25 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCPRNO
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCPRNO_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XBS_PRCPRNO
C
C Description:
C     Pré-traitement des propriétés nodales. On force la topo linéaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCPRNO(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRCPRNO
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER IPZ
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN
      INTEGER,POINTER :: KNGV (:,:)
      REAL*8, POINTER :: VPRNO(:,:)

      REAL*8, PARAMETER :: EMIN = 1.0D-15
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IC, IE, IN)
!$omp& private(IPZ)
!$omp& private(SELF, GDTA, EDTA, IPRN)
!$omp& private(KNGV, VPRNO)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN
      KNGV => GDTA%KNGV
      VPRNO=> EDTA%VPRNO

C---     Indices dans VPRNO
      IPZ = IPRN%Z

C---     Force la topo linéaire
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)

         VPRNO(IPZ,NO2) = (VPRNO(IPZ,NO1) + VPRNO(IPZ,NO3)) * UN_2
         VPRNO(IPZ,NO4) = (VPRNO(IPZ,NO3) + VPRNO(IPZ,NO5)) * UN_2
         VPRNO(IPZ,NO6) = (VPRNO(IPZ,NO5) + VPRNO(IPZ,NO1)) * UN_2

      ENDDO
!$omp end do
      ENDDO

C---     Force le vent à 0 en présence de glace
!$omp  do
      DO IN=1, GDTA%NNL

         IF (VPRNO(IPRN%ICE_E,IN) .GT. EMIN) THEN
             VPRNO(IPRN%WND_X,IN) = ZERO
             VPRNO(IPRN%WND_Y,IN) = ZERO
         ENDIF

      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      SV2D_XBS_PRCPRNO = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRCPRNO

      END SUBMODULE SV2D_XBS_PRCPRNO_M

