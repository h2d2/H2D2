C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_prnprno.for,v 1.15 2014/12/10 15:44:50 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRNPRNO
C   Private:
C     INTEGER SV2D_XBS_PRN1
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRNPRNO_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_XBS_PRN1 imprime une seule
C     propriété nodale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_PRN1(IP, TXT)

      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_XBS_PRN1 = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRN1

C************************************************************************
C Sommaire: SV2D_XBS_PRNPRNO
C
C Description:
C     IMPRESSION DES PROPRIETES NODALES
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  COEF. COMP. D'AMORTISSEMENT
C                    DS    16)  POROSITÉ
C
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRNPRNO(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRNPRNO
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, IERR
C-----------------------------------------------------------------------

      I = 0

C---     IMPRIME ENTETE
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     PRNO LUES
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_ZF')
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_COEF_MANNING')
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_GLACE_EPAISSEUR')
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_GLACE_COEF_MANNING')
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_VENT_X')
      I = I + 1
      IERR = SV2D_XBS_PRN1(I,'MSG_VENT_Y')

C---     PRNO CALCULEES
      IF (LOG_REQNIV() .GE. LOG_LVL_INFO) THEN
         CALL LOG_ECRIS(' ')
         WRITE (LOG_BUF, '(A)') 'MSG_PRNO_ST_VENANT_CALCULEES:'
         CALL LOG_ECRIS(LOG_BUF)

         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_VITESSE_X')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_VITESSE_Y')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_PROF_POSITIVE')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_MANNING_EFF')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_CONVECTION')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_GRAVITE')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_DIFFUSION')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_DARCY')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_COEF_AMORTISSEMENT')
         I = I + 1
         IERR = SV2D_XBS_PRN1(I,'MSG_POROSITE')
      ENDIF

      CALL LOG_DECIND()

      SV2D_XBS_PRNPRNO = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRNPRNO

      END SUBMODULE SV2D_XBS_PRNPRNO_M
