C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_pscprno.for,v 1.7 2012/01/10 20:20:00 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PSCPRNO
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PSCPRNO_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_PSCPRNO
C
C Description:
C     Post-calcul des propriétés nodales dépendantes de VDLG
C
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSCPRNO(SELF, MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PSCPRNO
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
      LOGICAL, INTENT(OUT) :: MODIF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      MODIF = .FALSE.
      SV2D_XBS_PSCPRNO = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSCPRNO

      END SUBMODULE SV2D_XBS_PSCPRNO_M
