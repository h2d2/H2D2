C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cmp_re.for,v 1.9 2015/12/07 19:36:38 secretyv Exp $
C
C Functions:
C   Public:
C   Private:
C     SUBROUTINE SV2D_CMP_RE_V_PNA
C     SUBROUTINE SV2D_CMP_RE_V_CNT
C     SUBROUTINE SV2D_CMP_RE_V_GRV
C     SUBROUTINE SV2D_CMP_RE_V_CNV
C     SUBROUTINE SV2D_CMP_RE_V_COR
C     SUBROUTINE SV2D_CMP_RE_V_MAN
C     SUBROUTINE SV2D_CMP_RE_V_MAN_MC
C     SUBROUTINE SV2D_CMP_RE_V_VNT
C     SUBROUTINE SV2D_CMP_RE_V_DIF
C     SUBROUTINE SV2D_CMP_FE_V_SLR
C     SUBROUTINE SV2D_CMP_RE_S_DIF
C     SUBROUTINE SV2D_CMP_RE_S_CNT
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_PNA
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_PNA assemble dans le
C     résidu Re la contribution de volume des termes de pénalisation.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Version pour le calcul du résidu
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_PNA(VRES,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      INTEGER, PARAMETER :: NDLN =  3
      INTEGER, PARAMETER :: NNEL =  6
      INTEGER, PARAMETER :: NPRN = 18
      INTEGER, PARAMETER :: NPRE =  2
      
      REAL*8   VRES  (NDLN,NNEL)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (NPRN,NNEL)
      REAL*8   VPRE  (NPRE)
      REAL*8   VDLE  (NDLN,NNEL)
      REAL*8   VRHS  (NDLN,NNEL)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  DJT6, DJT3
      REAL*8  DU1, DU2, DU3, DU4, DU5, DU6
      REAL*8  DV1, DV2, DV3, DV4, DV5, DV6
      REAL*8  DH1, DH3, DH5
      REAL*8  W1, W2, W3, W4, W5, W6
      REAL*8  CU, CH
      REAL*8  HMIN

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6
      DJT6 = VDJE(5)
      DJT3 = UN_4 * DJT6

C---     Variables
      HMIN = VPRGL(SV2D_IPRG%DECOU_HMIN)
      DU1 = VDLE(1,NO1) !- 0.0D0
      DV1 = VDLE(2,NO1) !- 0.0D0
      DH1 = VDLE(3,NO1) - VPRN(SV2D_IPRNO_Z,NO1) - HMIN  ! zf+hmin
      DU2 = VDLE(1,NO2) !- 0.0D0
      DV2 = VDLE(2,NO2) !- 0.0D0
      DU3 = VDLE(1,NO3) !- 0.0D0
      DV3 = VDLE(2,NO3) !- 0.0D0
      DH3 = VDLE(3,NO3) - VPRN(SV2D_IPRNO_Z,NO3) - HMIN
      DU4 = VDLE(1,NO4) !- 0.0D0
      DV4 = VDLE(2,NO4) !- 0.0D0
      DU5 = VDLE(1,NO5) !- 0.0D0
      DV5 = VDLE(2,NO5) !- 0.0D0
      DH5 = VDLE(3,NO5) - VPRN(SV2D_IPRNO_Z,NO5) - HMIN
      DU6 = VDLE(1,NO6) !- 0.0D0
      DV6 = VDLE(2,NO6) !- 0.0D0

C---     Coefficients de pénalisation (Weak-Dirichlet lumped)
      CU = UN_6 * DJT3 * VPRGL(SV2D_IPRG%DECOU_PENA_Q)
      CH = UN_6 * DJT6 * VPRGL(SV2D_IPRG%DECOU_PENA_H)
      W1 = VPRN(SV2D_IPRNO_DECOU_PENA,NO1)
      W2 = VPRN(SV2D_IPRNO_DECOU_PENA,NO2) * 3
      W3 = VPRN(SV2D_IPRNO_DECOU_PENA,NO3)
      W4 = VPRN(SV2D_IPRNO_DECOU_PENA,NO4) * 3
      W5 = VPRN(SV2D_IPRNO_DECOU_PENA,NO5)
      W6 = VPRN(SV2D_IPRNO_DECOU_PENA,NO6) * 3

C---     Résidu
      VRES(1,NO1) = VRES(1,NO1) + CU * W1 * DU1
      VRES(2,NO1) = VRES(2,NO1) + CU * W1 * DV1
      VRES(3,NO1) = VRES(3,NO1) + CH * W1 * DH1

      VRES(1,NO2) = VRES(1,NO2) + CU * W2 * DU2
      VRES(2,NO2) = VRES(2,NO2) + CU * W2 * DV2

      VRES(1,NO3) = VRES(1,NO3) + CU * W3 * DU3
      VRES(2,NO3) = VRES(2,NO3) + CU * W3 * DV3
      VRES(3,NO3) = VRES(3,NO3) + CH * W3 * DH3

      VRES(1,NO4) = VRES(1,NO4) + CU * W4 * DU4
      VRES(2,NO4) = VRES(2,NO4) + CU * W4 * DV4

      VRES(1,NO5) = VRES(1,NO5) + CU * W5 * DU5
      VRES(2,NO5) = VRES(2,NO5) + CU * W5 * DV5
      VRES(3,NO5) = VRES(3,NO5) + CH * W5 * DH5

      VRES(1,NO6) = VRES(1,NO6) + CU * W6 * DU6
      VRES(2,NO6) = VRES(2,NO6) + CU * W6 * DV6

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_CNT
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_CNT assemble dans le
C     résidu Re la contribution de volume de l'équation de
C     continuité. La divergence du débit est intégrée par partie.
C
C Entrée:
C     VDJE        Métrique de l'élément
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Les coefficients sont pour les métriques du T6
C        - <NT6,x> {NT3}      ! continuité
C        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
C
C     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
C     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
C     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
C     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
C     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_CNT(VRES,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE
      INTEGER, PARAMETER :: NDLN = 3
      INTEGER, PARAMETER :: NNEL = 6
      INTEGER, PARAMETER :: NPRN = 18
      
      REAL*8   VRES  (NDLN,NNEL)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (NPRN,NNEL)
      REAL*8   VPRE  ( 2)
      REAL*8   VDLE  (NDLN,NNEL)
      REAL*8   VRHS  (NDLN,NNEL)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ
      REAL*8  QX1, QX2, QX3, QX4, QX5, QX6
      REAL*8  QY1, QY2, QY3, QY4, QY5, QY6
      REAL*8  H1, H3, H5
      REAL*8  DHDX, DHDY, QXS, QYS, DHNRM
      REAL*8  C1, C3, C5, C, CM

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)
      DETJ = VDJE(5)

C---     Degrés de liberté
      H1 = VDLE(3,NO1)
      H3 = VDLE(3,NO3)
      H5 = VDLE(3,NO5)

C---     Coefficients de Lapidus
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)
      DHNRM = MAX(HYPOT(DHDX, DHDY), 1.0D-12)
      C = UN_2 * SV2D_STABI_LAPIDUS * DHNRM / DETJ

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      H1  = VRHS(3,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)
      H3  = VRHS(3,NO3)
      QX4 = VRHS(1,NO4)
      QY4 = VRHS(2,NO4)
      QX5 = VRHS(1,NO5)
      QY5 = VRHS(2,NO5)
      H5  = VRHS(3,NO5)
      QX6 = VRHS(1,NO6)
      QY6 = VRHS(2,NO6)

C---     Dérivées
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)

C---     Valeurs élémentaires
      QXS = (QX1+QX2+QX6)+(QX2+QX3+QX4)+(QX6+QX4+QX5)+(QX4+QX6+QX2)
      QYS = (QY1+QY2+QY6)+(QY2+QY3+QY4)+(QY6+QY4+QY5)+(QY4+QY6+QY2)

C---     Coefficients de Darcy
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY,NO5)
      CM = UN_3*(C1 + C3 + C5)
      C = C + UN_2*CM/DETJ

C---     Résidu
      VRES(3,NO1) = VRES(3,NO1) - UN_24*(VSX*QXS + VSY*QYS)
     &                          + C*(VSX*DHDX + VSY*DHDY)
      VRES(3,NO3) = VRES(3,NO3) - UN_24*(VKX*QXS + VKY*QYS)
     &                          + C*(VKX*DHDX + VKY*DHDY)
      VRES(3,NO5) = VRES(3,NO5) - UN_24*(VEX*QXS + VEY*QYS)
     &                          + C*(VEX*DHDX + VEY*DHDY)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_GRV
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_GRV assemble dans le
C     résidu Re la contribution de volume des termes de gravité
C     des équations de mouvement pour un élément T6L.
C
C Entrée:
C     VDJE        Métrique de l'élément T6
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Le résidu est :
C        {N} < N1 ; N2 ; N3 > {H} < N1,x ; N2,x ; N3,x > {h}
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_GRV(VRES,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  H1, H3, H5, DHDX, DHDY
      REAL*8  P1, P2, P3, P4, P5, P6
      REAL*8  PE1, PE2, PE3, PE4
      REAL*8  CQH

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques du T6L
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      CQH = SV2D_GRAVITE * UN_96

C---     Inconnues
      H1 = VRHS(3,NO1)
      H3 = VRHS(3,NO3)
      H5 = VRHS(3,NO5)

C---     Dérivées
      DHDX = CQH * (VKX*(H3-H1) + VEX*(H5-H1))
      DHDY = CQH * (VKY*(H3-H1) + VEY*(H5-H1))

C---     Profondeur nodales
      P1 = VPRN(SV2D_IPRNO_H,NO1) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO1)   ! Prof absolue * lmtr de gravité
      P2 = VPRN(SV2D_IPRNO_H,NO2) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO2)
      P3 = VPRN(SV2D_IPRNO_H,NO3) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO3)
      P4 = VPRN(SV2D_IPRNO_H,NO4) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO4)
      P5 = VPRN(SV2D_IPRNO_H,NO5) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO5)
      P6 = VPRN(SV2D_IPRNO_H,NO6) * VPRN(SV2D_IPRNO_COEFF_GRVT,NO6)

C---     Profondeurs élémentaires
      PE1 = P1 + P2 + P6
      PE2 = P2 + P3 + P4
      PE3 = P6 + P4 + P5
      PE4 = P4 + P6 + P2

C!!!!!!!!!!!!! Ajouter la pression dynamique (SV2D_CMULT_PDYN) !!!!!!!!!!!!!!!!1

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) + (PE1+P1)*DHDX
      VRES(2,NO1) = VRES(2,NO1) + (PE1+P1)*DHDY
      VRES(1,NO2) = VRES(1,NO2) + (PE1+P2 + PE2+P2 + PE4+P2)*DHDX
      VRES(2,NO2) = VRES(2,NO2) + (PE1+P2 + PE2+P2 + PE4+P2)*DHDY
      VRES(1,NO3) = VRES(1,NO3) + (PE2+P3)*DHDX
      VRES(2,NO3) = VRES(2,NO3) + (PE2+P3)*DHDY
      VRES(1,NO4) = VRES(1,NO4) + (PE2+P4 + PE3+P4 + PE4+P4)*DHDX
      VRES(2,NO4) = VRES(2,NO4) + (PE2+P4 + PE3+P4 + PE4+P4)*DHDY
      VRES(1,NO5) = VRES(1,NO5) + (PE3+P5)*DHDX
      VRES(2,NO5) = VRES(2,NO5) + (PE3+P5)*DHDY
      VRES(1,NO6) = VRES(1,NO6) + (PE3+P6 + PE4+P6 + PE1+P6)*DHDX
      VRES(2,NO6) = VRES(2,NO6) + (PE3+P6 + PE4+P6 + PE1+P6)*DHDY

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_CNV
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_CNV assemble dans le
C     résidu Re la contribution de volume des termes de convection
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Le résidu :
C        {N} < N1,x ; N2,x ; N3,x > {U Qx}
C     développé donne
C        {N} ( N1,x*U1*Qx1 + N2,x*U2*Qx2 + N3,x*u3*Qx3)
C     qui est écrit ensuite sous la forme
C        {N} < U1*N1,x ; U2*N2,x ; U3*N3,x > {Qx}
C
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_CNV(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER NO1, NO2, NO3
      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  DUQXDX, DVQXDY
      REAL*8  DUQYDX, DVQYDY
      REAL*8  CNVX, CNVY
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Propriétés nodales
      U1 = VPRN(SV2D_IPRNO_U,NO1) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO1)   ! u * limiteur de convection
      V1 = VPRN(SV2D_IPRNO_V,NO1) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO1)   !
      U2 = VPRN(SV2D_IPRNO_U,NO2) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO2)
      V2 = VPRN(SV2D_IPRNO_V,NO2) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO2)
      U3 = VPRN(SV2D_IPRNO_U,NO3) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO3)
      V3 = VPRN(SV2D_IPRNO_V,NO3) * VPRN(SV2D_IPRNO_COEFF_CNVT,NO3)

C---     d/dx (u.qx), d/dy (v.qx)
      DUQXDX = VKX*(U2*QX2-U1*QX1) + VEX*(U3*QX3-U1*QX1)
      DVQXDY = VKY*(V2*QX2-V1*QX1) + VEY*(V3*QX3-V1*QX1)
      DUQYDX = VKX*(U2*QY2-U1*QY1) + VEX*(U3*QY3-U1*QY1)
      DVQYDY = VKY*(V2*QY2-V1*QY1) + VEY*(V3*QY3-V1*QY1)

C---     Termes de convection
      CNVX = UN_6*(DUQXDX + DVQXDY)
      CNVY = UN_6*(DUQYDX + DVQYDY)

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + CNVX
      VRES(2,NO1) = VRES(2,NO1) + CNVY

      VRES(1,NO2) = VRES(1,NO2) + CNVX
      VRES(2,NO2) = VRES(2,NO2) + CNVY

      VRES(1,NO3) = VRES(1,NO3) + CNVX
      VRES(2,NO3) = VRES(2,NO3) + CNVY

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_COR
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_COR assemble dans le
C     résidu Re la contribution de volume des termes de Coriolis
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     La matrice lumpée est trop dissipative
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_COR(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER NO1, NO2, NO3
      REAL*8  CX, CY
      REAL*8  DETJ
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques
      DETJ = VDJE(5)

C---     Coefficients de Coriolis
      CY = UN_24*SV2D_CORIOLIS*DETJ
      CX = -CY

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + CX*(QY1+QY1 + QY2 + QY3)
      VRES(2,NO1) = VRES(2,NO1) + CY*(QX1+QX1 + QX2 + QX3)

      VRES(1,NO2) = VRES(1,NO2) + CX*(QY1 + QY2+QY2 + QY3)
      VRES(2,NO2) = VRES(2,NO2) + CY*(QX1 + QX2+QX2 + QX3)

      VRES(1,NO3) = VRES(1,NO3) + CX*(QY1 + QY2 + QY3+QY3)
      VRES(2,NO3) = VRES(2,NO3) + CY*(QX1 + QX2 + QX3+QX3)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_MAN
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_MAN assemble dans le
C     résidu Re la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C     Version avec matrice lumpée.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     La matrice masse est lumpée. De la forme de base:
C        {N} <N> {fr} <N>
C     développée en:
C        [ N1.N1  N1.N2  N1.N3 ]
C        [ N2.N1  N2.N2  N2.N3 ] <N>{fr}
C        [ N3.N1  N3.N2  N3.N3 ]
C     lumpée en:
C        [ N1(N1+N2+N3)      0            0      ]
C        [       0      N2(N1+N2+N3)      0      ] <N>{fr}
C        [       0           0       N3(N1+N2+N3)]
C     ce qui revient à:
C        [ N1 0  0 ]
C        [ 0  N2 0 ] <N>{fr}
C        [ 0  0 N3 ]
C     qui à les mêmes composantes que la matrice:
C         {N}<N>
C
C     Masse non lumpée:
C        Dans Maxima
C        n1(x,y):= 1-x-y;
C        n2(x,y):=x;
C        n3(x,y):=y;
C        u(x,y):=n1(x,y)*u1 + n2(x,y)*u2 + n3(x,y)*u3;
C        integrate(integrate(n1(x,y)*n1(x,y)*u(x,y),y,0,1-x),x,0,1);
C
C      COEF = UN_120*DETJ     !! Matrice complète   (avec cette méthode, on arrive à des écarts
C                                                    par rapport à Hydrosim)
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_MAN(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  DETJ, COEF_HY
      REAL*8  VFROT1, VFROT2, VFROT3
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  FX1, FX2, FX3, FY1, FY2, FY3

      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF_HY = UN_24*DETJ   !! Hydrosim

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Coefficients de frottements
      VFROT1 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO1)
      VFROT2 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO2)
      VFROT3 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO3)

C---     Termes de frottement
      FX1 = VFROT1*QX1
      FX2 = VFROT2*QX2
      FX3 = VFROT3*QX3
      FY1 = VFROT1*QY1
      FY2 = VFROT2*QY2
      FY3 = VFROT3*QY3

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + (FX1+FX1 + FX2 + FX3)
      VRES(2,NO1) = VRES(2,NO1) + (FY1+FY1 + FY2 + FY3)

      VRES(1,NO2) = VRES(1,NO2) + (FX1 + FX2+FX2 + FX3)
      VRES(2,NO2) = VRES(2,NO2) + (FY1 + FY2+FY2 + FY3)

      VRES(1,NO3) = VRES(1,NO3) + (FX1 + FX2 + FX3+FX3)
      VRES(2,NO3) = VRES(2,NO3) + (FY1 + FY2 + FY3+FY3)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_MAN
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_MAN_MC assemble dans le
C     résidu Re la contribution de volume des termes de frottement de Manning
C     des équations de mouvement pour un sous-élément T3.
C     Version avec la matrice complète.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     La matrice masse est lumpée. De la forme de base:
C        {N} <N> {fr} <N>
C     développée en:
C        [ N1.N1  N1.N2  N1.N3 ]
C        [ N2.N1  N2.N2  N2.N3 ] <N>{fr}
C        [ N3.N1  N3.N2  N3.N3 ]
C     lumpée en:
C        [ N1(N1+N2+N3)      0            0      ]
C        [       0      N2(N1+N2+N3)      0      ] <N>{fr}
C        [       0           0       N3(N1+N2+N3)]
C     ce qui revient à:
C        [ N1 0  0 ]
C        [ 0  N2 0 ] <N>{fr}
C        [ 0  0 N3 ]
C     qui à les mêmes composantes que la matrice:
C         {N}<N>
C
C     Masse non lumpée:
C        Dans Maxima
C        n1(x,y):= 1-x-y;
C        n2(x,y):=x;
C        n3(x,y):=y;
C        u(x,y):=n1(x,y)*u1 + n2(x,y)*u2 + n3(x,y)*u3;
C        integrate(integrate(n1(x,y)*n1(x,y)*u(x,y),y,0,1-x),x,0,1);
C
C      COEF = UN_120*DETJ     !! Matrice complète   (avec cette méthode, on arrive à des écarts
C                                                    par rapport à Hydrosim)
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_MAN_MC(VRES,
     &                                KNE,
     &                                VDJE,
     &                                VPRGL,
     &                                VPRN,
     &                                VPRE,
     &                                VDLE,
     &                                VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  DETJ, COEF_HY
      REAL*8  F1, F2, F3, FS
      REAL*8  C11, C12, C13, C22, C23, C33
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3

      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF_HY = UN_120*DETJ

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Coefficients de frottements
      F1 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO1)
      F2 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO2)
      F3 = COEF_HY*VPRN(SV2D_IPRNO_COEFF_FROT,NO3)
      FS = F1 + F2 + F3

C---     Termes de frottement
      C11 = (FS + F1 + F1)*2
      C12 = (FS + F1 + F2)
      C13 = (FS + F1 + F3)
      C22 = (FS + F2 + F2)*2
      C23 = (FS + F2 + F3)
      C33 = (FS + F3 + F3)*2

C---     Assemblage de VRES
      VRES(1,NO1) = VRES(1,NO1) + (C11*QX1 + C12*QX2 + C13*QX3)
      VRES(2,NO1) = VRES(2,NO1) + (C11*QY1 + C12*QY2 + C13*QY3)
      VRES(1,NO2) = VRES(1,NO2) + (C12*QX1 + C22*QX2 + C23*QX3)
      VRES(2,NO2) = VRES(2,NO2) + (C12*QY1 + C22*QY2 + C23*QY3)
      VRES(1,NO3) = VRES(1,NO3) + (C13*QX1 + C23*QX2 + C33*QX3)
      VRES(2,NO3) = VRES(2,NO3) + (C13*QY1 + C23*QY2 + C33*QY3)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_VNT
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_VNT assemble dans le
C     résidu Re la contribution de volume des termes de vent
C     des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T6l
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     Cf. Manning
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_VNT(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      REAL*8  DETJ, COEF, CREL
      REAL*8  VVENT1, VVENT2, VVENT3
      REAL*8  WX1, WX2, WX3, WY1, WY2, WY3
      REAL*8  VX1, VX2, VX3, VY1, VY2, VY3

      INTEGER IPH, IPX, IPY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

      IPH = SV2D_IPRNO_H
      IPX = SV2D_IPRNO_WND_X
      IPY = SV2D_IPRNO_WND_Y

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Déterminant
      DETJ = VDJE(5)

C---     Coefficient
      COEF = UN_24*DETJ
      CREL = SV2D_CMULT_VENT_REL

C---     Coefficients de vent
      VVENT1 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO1)            ! cw |w| rho_air/rho_eau
      VVENT2 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO2)
      VVENT3 = COEF*VPRN(SV2D_IPRNO_COEFF_VENT,NO3)

C---     Inconnues (vent relatif)
      WX1 = VPRN(IPX,NO1) - CREL*VRHS(1,NO1)/VPRN(IPH,NO1)     ! wx - qx/h
      WY1 = VPRN(IPY,NO1) - CREL*VRHS(2,NO1)/VPRN(IPH,NO1)     ! Recalculé car issu de VRHS
      WX2 = VPRN(IPX,NO2) - CREL*VRHS(1,NO2)/VPRN(IPH,NO2)
      WY2 = VPRN(IPY,NO2) - CREL*VRHS(2,NO2)/VPRN(IPH,NO2)
      WX3 = VPRN(IPX,NO3) - CREL*VRHS(1,NO3)/VPRN(IPH,NO3)
      WY3 = VPRN(IPY,NO3) - CREL*VRHS(2,NO3)/VPRN(IPH,NO3)

C---     Termes de vent
      VX1 = VVENT1*WX1
      VX2 = VVENT2*WX2
      VX3 = VVENT3*WX3
      VY1 = VVENT1*WY1
      VY2 = VVENT2*WY2
      VY3 = VVENT3*WY3

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) - (VX1+VX1 + VX2 + VX3)
      VRES(2,NO1) = VRES(2,NO1) - (VY1+VY1 + VY2 + VY3)

      VRES(1,NO2) = VRES(1,NO2) - (VX1 + VX2+VX2 + VX3)
      VRES(2,NO2) = VRES(2,NO2) - (VY1 + VY2+VY2 + VY3)

      VRES(1,NO3) = VRES(1,NO3) - (VX1 + VX2 + VX3+VX3)
      VRES(2,NO3) = VRES(2,NO3) - (VY1 + VY2 + VY3+VY3)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_V_DIF
C
C Description:
C     La subroutine privée SV2D_CMP_RE_V_DIF assemble dans le
C     résidu Re la contribution de volume des termes de frottement de
C     diffusion des équations de mouvement pour un sous-élément T3.
C
C Entrée:
C     KNE         Connectivités du T3 au sein du T6L
C     KLOCE       Localisation élémentaire de l'élément T3
C     VDJE        Métrique de l'élément T3
C     VPRGL       Propriétés globales
C     VPRN        Propriétés nodales du T6l
C     VPRE        Propriétés élémentaires du T3
C     VDLE        Degrés de liberté du T6l
C
C Sortie:
C     VRES
C
C Notes:
C     {N,x} H_nu TXX
C     Le produit H_nu est la moyenne sur l'élément de volume.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_V_DIF(VRES,
     &                             KNE,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:)
      REAL*8   VRHS  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER NO1, NO2, NO3
      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DETJ, COEF
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  C1, C2, C3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  DUDX, DUDY, DVDX, DVDY
      REAL*8  TXX, TXY, TYY
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSX = -(VKX+VEX)
      VSY = -(VKY+VEY)

C---     Déterminant
      DETJ = VDJE(5)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Valeurs nodales
      H1 = VPRN(SV2D_IPRNO_H,NO1)          ! Profondeur
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1) ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     Visco totale
      C1 = VPRE(2) + C1       ! Visco totale + découvrement
      C2 = VPRE(2) + C2
      C3 = VPRE(2) + C3

C---     qx / h
      U1 = QX1/H1    ! Recalculé car issu de VRHS
      V1 = QY1/H1
      U2 = QX2/H2
      V2 = QY2/H2
      U3 = QX3/H3
      V3 = QY3/H3

C---     (H*nu) moyen
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     Coefficient
      COEF = UN_2*H_NU/DETJ

C---     Contraintes
      DUDX = VKX*(U2-U1) + VEX*(U3-U1)
      DUDY = VKY*(U2-U1) + VEY*(U3-U1)
      DVDX = VKX*(V2-V1) + VEX*(V3-V1)
      DVDY = VKY*(V2-V1) + VEY*(V3-V1)
      TXX = COEF*(DUDX + DUDX)
      TXY = COEF*(DUDY + DVDX)
      TYY = COEF*(DVDY + DVDY)

C---     Assemblage
      VRES(1,NO1) = VRES(1,NO1) + (VSX*TXX + VSY*TXY)
      VRES(2,NO1) = VRES(2,NO1) + (VSX*TXY + VSY*TYY)

      VRES(1,NO2) = VRES(1,NO2) + (VKX*TXX + VKY*TXY)
      VRES(2,NO2) = VRES(2,NO2) + (VKX*TXY + VKY*TYY)

      VRES(1,NO3) = VRES(1,NO3) + (VEX*TXX + VEY*TXY)
      VRES(2,NO3) = VRES(2,NO3) + (VEX*TXY + VEY*TYY)

      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CMP_FE_V_SLR
C
C Description:
C     La subroutine privée SV2D_CMP_FE_V_SLR assemble dans le
C     résidu Re la contribution de volume des sollicitations réparties.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CMP_FE_V_SLR(VRES,
     &                             VDJE,
     &                             VPRGL,
     &                             VPRN,
     &                             VPRE,
     &                             VDLE,
     &                             VSLR)

      IMPLICIT NONE

      REAL*8   VRES  (:,:)
      INTEGER  KLOCE (9)
      INTEGER  KNE   (3)
      REAL*8   VDJE  (:)
      REAL*8   VPRGL (:)
      REAL*8   VPRN  (:,:)
      REAL*8   VPRE  (2)
      REAL*8   VDLE  (:,:) ! Pas utilisé
      REAL*8   VSLR  (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER IERR
      INTEGER IC, IE, IN
      REAL*8  SU1, SU2, SU3, SU4, SU5, SU6
      REAL*8  SV1, SV2, SV3, SV4, SV5, SV6
      REAL*8  SH1,      SH3,      SH5
      REAL*8  SUE1, SUE2, SUE3, SUE4
      REAL*8  SVE1, SVE2, SVE3, SVE4
      REAL*8  SHE
      REAL*8  DETJ_T6, DETJ_T3

C---     Connectivités du T6 externe
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques
      DETJ_T6 = UN_24*VDJE(5)
      DETJ_T3 = UN_4*DETJ_T6      ! 1/4 POUR LE DJ SUR LE T3

C---     Sollicitations nodales
      SU1 = DETJ_T3 * VSLR(1,NO1)
      SV1 = DETJ_T3 * VSLR(2,NO1)
      SH1 = DETJ_T6 * VSLR(3,NO1)
      SU2 = DETJ_T3 * VSLR(1,NO2)
      SV2 = DETJ_T3 * VSLR(2,NO2)
      SU3 = DETJ_T3 * VSLR(1,NO3)
      SV3 = DETJ_T3 * VSLR(2,NO3)
      SH3 = DETJ_T6 * VSLR(3,NO3)
      SU4 = DETJ_T3 * VSLR(1,NO4)
      SV4 = DETJ_T3 * VSLR(2,NO4)
      SU5 = DETJ_T3 * VSLR(1,NO5)
      SV5 = DETJ_T3 * VSLR(2,NO5)
      SH5 = DETJ_T6 * VSLR(3,NO5)
      SU6 = DETJ_T3 * VSLR(1,NO6)
      SV6 = DETJ_T3 * VSLR(2,NO6)

C---     Sommations pour chaque sous-element
      SUE1 = SU1 + SU2 + SU6
      SVE1 = SV1 + SV2 + SV6
      SUE2 = SU2 + SU3 + SU4
      SVE2 = SV2 + SV3 + SV4
      SUE3 = SU6 + SU4 + SU5
      SVE3 = SV6 + SV4 + SV5
      SUE4 = SU4 + SU6 + SU2
      SVE4 = SV4 + SV6 + SV2
      SHE  = SH1 + SH3 + SH5

C---     Contributions
      VRES(1, NO1) = VRES(1, NO1) + SUE1+SU1
      VRES(2, NO1) = VRES(2, NO1) + SVE1+SV1
      VRES(3, NO1) = VRES(3, NO1) + SHE +SH1
      VRES(1, NO2) = VRES(1, NO2) + SUE1+SU2 + SUE2+SU2 + SUE4+SU2
      VRES(2, NO2) = VRES(2, NO2) + SVE1+SV2 + SVE2+SV2 + SVE4+SV2
!     VRES(3, NO2) = VRES(3, NO2) + ZERO
      VRES(1, NO3) = VRES(1, NO3) + SUE2+SU3
      VRES(2, NO3) = VRES(2, NO3) + SVE2+SV3
      VRES(3, NO3) = VRES(3, NO3) + SHE +SH3
      VRES(1, NO4) = VRES(1, NO4) + SUE2+SU4 + SUE3+SU4 + SUE4+SU4
      VRES(2, NO4) = VRES(2, NO4) + SVE2+SV4 + SVE3+SV4 + SVE4+SV4
!     VRES(3, NO4) = VRES(3, NO4) + ZERO
      VRES(1, NO5) = VRES(1, NO5) + SUE3+SU5
      VRES(2, NO5) = VRES(2, NO5) + SVE3+SV5
      VRES(3, NO5) = VRES(3, NO5) + SHE +SH5
      VRES(1, NO6) = VRES(1, NO6) + SUE3+SU6 + SUE1+SU6 + SUE4+SU6
      VRES(2, NO6) = VRES(2, NO6) + SVE3+SV6 + SVE1+SV6 + SVE4+SV6
!     VRES(3, NO6) = VRES(3, NO6) + ZERO

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_S_DIF
C
C Description:
C     La fonction SV2D_CMP_RE_S_DIF calcule la partie de diffusion du
C     résidu Re pour un élément de surface.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) On prend pour acquis que KNE et KLOCE sont pour un T3 et que
C        l'élément de contour L2 à calculer forme le premier côté (noeuds 1-2).
C
C     2) {NL2} H_nu < NT3,x > { q/H }
C        Le produit H.nu est la moyenne sur l'élément de volume.
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_S_DIF(VRES,
     &                             KNE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES (:,:)
      INTEGER  KNE  (3)
      REAL*8   VDJV (:)
      REAL*8   VDJS (:)
      REAL*8   VPRG (:)
      REAL*8   VPRN (:,:)
      REAL*8   VPREV(2)
      REAL*8   VPRES(3)             ! 3 POUR CHAQUE L2
      REAL*8   VDLE (:,:)
      REAL*8   VRHS (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER NO1, NO2, NO3
      REAL*8  COEF
      REAL*8  QX1, QX2, QX3, QY1, QY2, QY3
      REAL*8  H1, H2, H3, H_NU
      REAL*8  C1, C2, C3
      REAL*8  U1, U2, U3, V1, V2, V3
      REAL*8  VNX, VNY
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DJL2, DJT3
      REAL*8  DUDX, DUDY, DVDX, DVDY, TXX, TXY, TYY
C-----------------------------------------------------------------------

C---     Connectivités du T3
      NO1 = KNE(1)
      NO2 = KNE(2)
      NO3 = KNE(3)

C---     Normales et métriques de l'élément L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     Métriques du T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      DJT3= VDJV(5)

C---     Inconnues
      QX1 = VRHS(1,NO1)
      QY1 = VRHS(2,NO1)
      QX2 = VRHS(1,NO2)
      QY2 = VRHS(2,NO2)
      QX3 = VRHS(1,NO3)
      QY3 = VRHS(2,NO3)

C---     Valeurs nodales
      H1 = VPRN(SV2D_IPRNO_H,NO1)            ! Profondeur nodales
      C1 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO1)   ! Visco pour le découvrement
      H2 = VPRN(SV2D_IPRNO_H,NO2)
      C2 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO2)
      H3 = VPRN(SV2D_IPRNO_H,NO3)
      C3 = VPRN(SV2D_IPRNO_COEFF_DIFF,NO3)

C---     Visco totale
      C1 = VPREV(2) + C1      ! Visco totale + découvrement
      C2 = VPREV(2) + C2
      C3 = VPREV(2) + C3

C---     qx / h
      U1 = QX1/H1    ! Recalculé car issu de VRHS
      V1 = QY1/H1
      U2 = QX2/H2
      V2 = QY2/H2
      U3 = QX3/H3
      V3 = QY3/H3

C---     (H*nu) moyen
      H_NU = UN_3*(C1*H1 + C2*H2 + C3*H3)

C---     Coefficient
      COEF = SV2D_CMULT_INTGCTRQX*UN_4*H_NU*(DJL2/DJT3)

C---     Contraintes
      DUDX = VKX*(U2-U1) + VEX*(U3-U1)
      DUDY = VKY*(U2-U1) + VEY*(U3-U1)
      DVDX = VKX*(V2-V1) + VEX*(V3-V1)
      DVDY = VKY*(V2-V1) + VEY*(V3-V1)
      TXX = COEF*(DUDX + DUDX)
      TXY = COEF*(DUDY + DVDX)
      TYY = COEF*(DVDY + DVDY)

C---     Assemble
      VRES(1,NO1) = VRES(1,NO1) - (TXX*VNX + TXY*VNY)
      VRES(1,NO2) = VRES(1,NO2) - (TXX*VNX + TXY*VNY)

      VRES(2,NO1) = VRES(2,NO1) - (TXY*VNX + TYY*VNY)
      VRES(2,NO2) = VRES(2,NO2) - (TXY*VNX + TYY*VNY)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CMP_RE_S_CNT
C
C Description:
C     La fonction SV2D_CBS_RE_S_CNT calcule la partie de Darcy de la
C     continuité du résidu Re.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     {NL2} <NT3_1,x ; NT3_2,x ; NT3_3,x>
C
C************************************************************************
      SUBROUTINE SV2D_CMP_RE_S_CNT(VRES,
     &                             KNE,
     &                             VDJV,
     &                             VDJS,
     &                             VPRG,
     &                             VPRN,
     &                             VPREV,
     &                             VPRES,
     &                             VDLE,
     &                             VRHS)

      IMPLICIT NONE

      REAL*8   VRES (:,:)
      INTEGER  KNE  (3)
      REAL*8   VDJV (:)
      REAL*8   VDJS (:)
      REAL*8   VPRG (:)
      REAL*8   VPRN (:,:)
      REAL*8   VPREV(2)
      REAL*8   VPRES(3)             ! 3 POUR CHAQUE L2
      REAL*8   VDLE (:,:)
      REAL*8   VRHS (:,:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_xbs_idx.fc'

      INTEGER NO1, NO3, NO5
      INTEGER IKH1, IKH2, IKH3
      REAL*8  C1, C3, C5, CM, C
      REAL*8  VNX, VNY
      REAL*8  VSX, VKX, VEX
      REAL*8  VSY, VKY, VEY
      REAL*8  DJL2, DJT3
      REAL*8  H1, H3, H5
      REAL*8  DHDX, DHDY
C-----------------------------------------------------------------------

C---     Connectivités des sommets du T6L
      NO1 = KNE(1)
      NO3 = KNE(2)
      NO5 = KNE(3)

C---     Normales et métriques de l'élément L2
      VNX =  VDJS(2)     ! VNX =  VTY
      VNY = -VDJS(1)     ! VNY = -VTX
      DJL2=  VDJS(3)

C---     Métriques du T3
      VKX = VDJV(1)
      VEX = VDJV(2)
      VKY = VDJV(3)
      VEY = VDJV(4)
      DJT3= VDJV(5)

C---     Inconnues
      H1 = VRHS(3,NO1)
      H3 = VRHS(3,NO3)
      H5 = VRHS(3,NO5)

C---     Gradients
      DHDX = VKX*(H3-H1) + VEX*(H5-H1)
      DHDY = VKY*(H3-H1) + VEY*(H5-H1)

C---     Coefficients de Darcy
      C1 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO1)
      C3 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO3)
      C5 = VPRN(SV2D_IPRNO_COEFF_DRCY, NO5)
      CM = UN_3*(C1 + C3 + C5)
      C  = SV2D_CMULT_INTGCTRH*CM*(DJL2/DJT3)

C---     Sous-matrice H.H
      VRES(3, NO1) = VRES(3, NO1) - C*(VNX*DHDX + VNY*DHDY)
      VRES(3, NO3) = VRES(3, NO3) - C*(VNX*DHDX + VNY*DHDY)

      RETURN
      END
