C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_ASMMU
C   Private:
C     INTEGER SV2D_XBS_ASMMU_V
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMMU_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XBS_ASMMU_V
C
C Description: ASSEMBLAGE DU RESIDU:   "[M].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : EDTA%VDLG=C
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMMU_V(SELF, VFG)

      USE SV2D_XBS_ASMM_M, ONLY: SV2D_XBS_ASMME_V
      
      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IC, IE
      REAL*8, POINTER :: VKE  (:, :)
      REAL*8, POINTER :: VFE  (:)
      REAL*8, POINTER :: VDLE (:, :)
      REAL*8, POINTER :: VPRN (:)
      REAL*8, POINTER :: VDJE (:)
      INTEGER,POINTER :: KNE  (:)
      INTEGER,POINTER :: KLOCE(:, :)
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN

C---     Tables locales
      INTEGER, PARAMETER :: NDJV_LCL =  5
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLE_LCL = 18
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VKE_LCL  (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VFE_LCL  (NDLE_LCL)
      REAL*8,  TARGET :: VDJE_LCL (NDJV_LCL)
      REAL*8,  TARGET :: VDLE_LCL (NDLE_LCL)
      REAL*8,  TARGET :: VPRN_LCL (NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL(NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NDJV_LCL .GE. GDTA%NDJV)
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VKE  (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      VDLE (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      KLOCE(1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL
      VPRN (1:GDTA%NNELV) => VPRN_LCL
      VFE  (1:EDTA%NDLEV) => VFE_LCL
      VDJE (1:GDTA%NDJV)  => VDJE_LCL
      KNE  (1:GDTA%NCELV) => KNE_LCL

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL

C---     Initialise KLOCE
D     KLOCE(:, :) = 0

!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des valeurs élémentaires
         KNE (:) = GDTA%KNGV(:,IE)
         VDJE(:) = GDTA%VDJV(:,IE)
         VPRN(:) = EDTA%VPRNO(IPRN%COEFF_PORO, KNE(:))
         VDLE(:,:) = EDTA%VDLG(:, KNE(:))

C---        Initialise la matrice élémentaire
         VKE(:, :) = ZERO

C---        Calcul de la matrice élémentaire de volume
         IERR = SV2D_XBS_ASMME_V(VKE,
     &                           VDJE,
     &                           VPRN)

C---        Produit matrice-vecteur
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              EDTA%NDLEV,        ! m rows
     &              EDTA%NDLEV,        ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              EDTA%NDLEV,        ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VFE,               ! y
     &              1)                 ! incy

C---       Assemblage du vecteur global
         KLOCE(:,:) = EDTA%KLOCN(:, KNE(:))
         IERR = SP_ELEM_ASMFE(EDTA%NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMMU_V = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMMU_V


C************************************************************************
C Sommaire:
C
C Description: ASSEMBLAGE DU RESIDU:   "[M].{U}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMMU(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMMU
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(:)

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMMU_V(SELF, VRES)
      
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
!!      IF (ERR_GOOD()) THEN
!!         CALL SV2D_XBS_ASMMU_S(SELF, VRES)
!!      ENDIF

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMMU(SELF, VRES)
      ENDIF

      SV2D_XBS_ASMMU = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMMU

      END SUBMODULE SV2D_XBS_ASMMU_M
