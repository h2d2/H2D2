C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_ASMK_R
C   Private:
C     INTEGER SV2D_XBS_ASMK_RV
C     INTEGER SV2D_XBS_ASMK_RS
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMK_R_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_ASMK_RV
C
C Description:
C     La fonction SV2D_XBS_ASMK_RV calcule la matrice
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMK_RV(SELF, HMTX, F_ASM)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_K
      
      CLASS (SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      INTEGER HVFT
      REAL*8  PENA
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      REAL*8, POINTER :: VDLE  (:,:)
      REAL*8, POINTER :: VPRN  (:,:)
      REAL*8, POINTER :: VKE   (:,:,:)
      REAL*8, POINTER :: VRHS  (:,:)
      INTEGER,POINTER :: KLOCE (:,:)
      INTEGER,POINTER :: KNE   (:)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL  (NDLE_LCL)
      REAL*8,  TARGET :: VPRN_LCL  (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VKE_LCL   (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VRHS_LCL  (NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL (NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VDLE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRN  (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VKE   (1:EDTA%NDLN, 1:GDTA%NNELV, 1:EDTA%NDLEV) => VKE_LCL
      VRHS  (1:EDTA%NDLN, 1:GDTA%NNELV) => VRHS_LCL
      KNE   (1:GDTA%NNELV) => KNE_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C---     Initialise le RHS
      VRHS(:,:) = ZERO

C-------  BOUCLE SUR LES ELEMENTS DE VOLUME
C         =================================
      DO 10 IC=1,GDTA%NELCOL
!$omp  do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = GDTA%KNGV(:,IE)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:,KNE(:))

C---        Transfert des valeurs nodales T6L
         VDLE (:,:) = EDTA%VDLG (:,KNE(:))
         VPRN (:,:) = EDTA%VPRNO(:,KNE(:))

C---        Initialise la matrice élémentaire
         VKE(:,:,:) = ZERO

C---        Boucle de perturbation sur les DDL
C           ==================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VRHS(ID,IN) = UN

C---           R(u). i
            IERR = SV2D_XBS_RE_K(VKE(:,:,IDG),
     &                           GDTA%VDJV(:,IE),
     &                           VPRN,
     &                           EDTA%VPREV(:,:,IE),
     &                           VDLE,
     &                           VRHS,    ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG)

C---           Restaure la valeur originale
            VRHS(ID,IN) = ZERO

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = XPRG%PNUMR_PENALITE*GDTA%VDJV(5,IE)
         VKE( 3, 1, 3) = MAX(VKE( 3, 1, 3), PENA)
         VKE( 3, 3, 9) = MAX(VKE( 3, 3, 9), PENA)
         VKE( 3, 5,15) = MAX(VKE( 3, 5,15), PENA)

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMK_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMK_RV

C************************************************************************
C Sommaire: SV2D_CBS_ASMK_RS
C
C Description:
C     La fonction SV2D_CBS_ASMK_RS calcule la matrice élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) c.f. SV2D_XBS_ASMK_RV
C
C     2) Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C
C     3) Il faut perturber tous les dll de l'élément de volume car les
C     VPREV dépendent des VPRN. Les VPRES ne sont pas utilisées.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMK_RS(SELF, HMTX, F_ASM)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_S
      
      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      INTEGER HVFT
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      REAL*8, POINTER :: VDLEV (:,:)
      REAL*8, POINTER :: VPRNV (:,:)
      REAL*8, POINTER :: VKE   (:,:,:)
      REAL*8, POINTER :: VRHSV (:,:)
      INTEGER,POINTER :: KNEV  (:)
      INTEGER,POINTER :: KLOCE (:, :)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VKE_LCL   (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VRHSV_LCL (NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL (NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VDLEV (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLEV_LCL
      VPRNV (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRNV_LCL
      VKE   (1:EDTA%NDLN, 1:GDTA%NNELV, 1:EDTA%NDLEV) => VKE_LCL
      VRHSV (1:EDTA%NDLN, 1:GDTA%NNELV) => VRHSV_LCL
      KNEV  (1:GDTA%NNELV) => KNE_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C---     Initialise le RHS
      VRHSV(:,:) = ZERO

C-------  Boucle sur les éléments de surface
C         ==================================
      DO IES=1,GDTA%NELLS

C---        Élément parent et coté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
         KNEV(:) = GDTA%KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:,KNEV(:))

C---        Transfert des valeurs nodales T6L
         VDLEV(:,:) = EDTA%VDLG (:,KNEV(:))
         VPRNV(:,:) = EDTA%VPRNO(:,KNEV(:))

C---        Initialise la matrice élémentaire
         VKE(:,:,:) = ZERO

C---        Boucle de perturbation sur tous les ddl
C           =======================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VRHSV(ID,IN) = UN

C---           R(u)
            IERR = SV2D_XBS_RE_S(VKE(:,:,IDG),
     &                           GDTA%VDJV(:,IEV),
     &                           GDTA%VDJS(:,IES),
     &                           VPRNV,
     &                           EDTA%VPREV(:,:,IEV),
     &                           EDTA%VPRES(:,:,IES), ! Pas utilisé
     &                           VDLEV,
     &                           VRHSV,               ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG,
     &                           ICT)

C---           Restaure la valeur originale
            VRHSV(ID,IN) = ZERO

199         CONTINUE
         ENDDO
         ENDDO

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      SV2D_XBS_ASMK_RS = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMK_RS

C************************************************************************
C Sommaire: SV2D_XBS_ASMK_R
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C     La fonction SV2D_XBS_ASMK_R calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMK_R(SELF, HMTRX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMK_R
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER  IERR
C----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMK_RV(SELF, HMTRX, F_ASM)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF
      
C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         IERR = SV2D_XBS_ASMK_RS(SELF, HMTRX, F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMK(SELF, HMTRX, F_ASM)
      ENDIF

      SV2D_XBS_ASMK_R = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMK_R

      END SUBMODULE SV2D_XBS_ASMK_R_M
