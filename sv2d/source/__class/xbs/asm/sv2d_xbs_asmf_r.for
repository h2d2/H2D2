C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMF_R_M
      
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS
      
C************************************************************************
C Sommaire:  SV2D_XBS_ASMF_RV
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Assemble:
C        les sollicitations concentrées
C        les solicitations réparties.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMF_RV (SELF, VFG)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_FE_V
      
      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VFG(:)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IC, IE, NO
      REAL*8, POINTER :: VDLE (:, :)
      REAL*8, POINTER :: VSRE (:, :)
      REAL*8, POINTER :: VPRN (:, :)
      REAL*8, POINTER :: VFE  (:, :)
      INTEGER,POINTER :: KLOCE(:, :)
      INTEGER,POINTER :: KNE  (:)
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VSRE_LCL (NDLN_LCL * NNEL_LCL)  ! rank one
      REAL*8,  TARGET :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VFE_LCL  (NNEL_LCL * NDLN_LCL)
      INTEGER, TARGET :: KLOCE_LCL(NNEL_LCL * NDLN_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      !IPRN => SELF%IPRN

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VDLE (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VSRE (1:EDTA%NDLN, 1:GDTA%NNELV) => VSRE_LCL
      VPRN (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VFE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VFE_LCL
      KLOCE(1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL
      KNE  (1:GDTA%NCELV) => KNE_LCL

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,GDTA%NELCOL
!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = GDTA%KNGV(:,IE)

C---        Transfert des valeurs élémentaires
         VSRE(:,:) = EDTA%VSOLR(:, KNE(:))
         VPRN(:,:) = EDTA%VPRNO(:, KNE(:))

C---        F
         VFE(:,:) = ZERO
         IERR =  SV2D_XBS_FE_V(VFE, 
     &                         GDTA%VDJV(:,IE),
     &                         VPRN,
     &                         EDTA%VPREV(:,:,IE),
     &                         VDLE,         ! Pas utilisé
     &                         VSRE,
     &                         SELF%IPRN,
     &                         SELF%XPRG)

C---       Assemblage du vecteur global
         KLOCE(:,:) = EDTA%KLOCN(:, KNE(:))
         IERR = SP_ELEM_ASMFE(EDTA%NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMF_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMF_RV
      
C************************************************************************
C Sommaire:  SV2D_XBS_ASMF_R
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     NSOLR et NSOLC doivent être égaux à NDLN
C
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMF_R(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMF_R
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Sollicitations concentrées
      IF (ERR_GOOD()) THEN
         EDTA => SELF%EDTA
         IERR = SP_ELEM_ASMFE(EDTA%NDLL, EDTA%KLOCN, EDTA%VSOLC, VRES)
      ENDIF

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMF_RV (SELF, VRES)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF
         
C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMF(SELF, VRES)
      ENDIF

      SV2D_XBS_ASMF_R = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMF_R
      
      END SUBMODULE SV2D_XBS_ASMF_R_M
