C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_ASMM
C   Private:
C     INTEGER SV2D_XBS_ASMM_V
C     INTEGER SV2D_XBS_ASMME_V
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMM_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_ASMME_V
C
C Description:
C     La fonction SV2D_XBS_ASMME_V calcul la matrice masse élémentaire.
C     Les tables passées en paramètre sont des tables élémentaires.
C
C Entrée:
C     VDJE        Table du Jacobien Élémentaire
C     VPRG        Table de PRopriétés Globales
C     VPRN        Table de PRopriétés Nodales
C     VPRE        Table de PRopriétés Élémentaires
C
C Sortie:
C     VKE         Matrice élémentaire
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMME_V(VKE, VDJE, VPRN)

      REAL*8  VKE  (:, :)
      REAL*8  VDJE (:)
      REAL*8  VPRN (:)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmgeom_subt3.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IT3
      INTEGER IKU1, IKV1, IKH1
      INTEGER IKU2, IKV2
      INTEGER IKU3, IKV3, IKH2
      INTEGER IKH3

      REAL*8  COEF, COEF2
      REAL*8  PORO1, PORO2, PORO3, POROS
C-----------------------------------------------------------------------
!D     CALL ERR_PRE(LM_CMMN_NPREV_D1 .GE. 2)     ! Au moins 2 POUR CHAQUE T3
!D     CALL ERR_PRE(LM_CMMN_NPREV_D2 .EQ. 4)     ! 4 T3
C-----------------------------------------------------------------------

C---     Coefficient pour les sous-éléments
      COEF  = UN_24*UN_4*VDJE(5)
      COEF2 = COEF+COEF

C---     BOUCLE SUR LES SOUS-ÉLÉMENTS
C        ============================
      DO IT3=1,4

C---        Indice de DDL
         IKU1 = KLOCET3(1,IT3)
         IKV1 = KLOCET3(2,IT3)
         IKU2 = KLOCET3(4,IT3)
         IKV2 = KLOCET3(5,IT3)
         IKU3 = KLOCET3(7,IT3)
         IKV3 = KLOCET3(8,IT3)

C---        Assemblage de la sous-matrice U.U
         VKE(IKU1,IKU1) = VKE(IKU1,IKU1) + COEF2
         VKE(IKU2,IKU1) = VKE(IKU2,IKU1) + COEF
         VKE(IKU3,IKU1) = VKE(IKU3,IKU1) + COEF
         VKE(IKU1,IKU2) = VKE(IKU1,IKU2) + COEF
         VKE(IKU2,IKU2) = VKE(IKU2,IKU2) + COEF2
         VKE(IKU3,IKU2) = VKE(IKU3,IKU2) + COEF
         VKE(IKU1,IKU3) = VKE(IKU1,IKU3) + COEF
         VKE(IKU2,IKU3) = VKE(IKU2,IKU3) + COEF
         VKE(IKU3,IKU3) = VKE(IKU3,IKU3) + COEF2

C---        Assemblage de la sous-matrice V.V
         VKE(IKV1,IKV1) = VKE(IKV1,IKV1) + COEF2
         VKE(IKV2,IKV1) = VKE(IKV2,IKV1) + COEF
         VKE(IKV3,IKV1) = VKE(IKV3,IKV1) + COEF
         VKE(IKV1,IKV2) = VKE(IKV1,IKV2) + COEF
         VKE(IKV2,IKV2) = VKE(IKV2,IKV2) + COEF2
         VKE(IKV3,IKV2) = VKE(IKV3,IKV2) + COEF
         VKE(IKV1,IKV3) = VKE(IKV1,IKV3) + COEF
         VKE(IKV2,IKV3) = VKE(IKV2,IKV3) + COEF
         VKE(IKV3,IKV3) = VKE(IKV3,IKV3) + COEF2

      ENDDO

C---     Coefficients
      COEF = UN_120*VDJE(5)

C---     Porosité
      PORO1 = COEF*VPRN(1)
      PORO2 = COEF*VPRN(3)
      PORO3 = COEF*VPRN(5)
      POROS = PORO1 + PORO2 + PORO3

C---     Indice de DDL H
      IKH1 =  3   ! h sur les noeuds sommets
      IKH2 =  9
      IKH3 = 15

C---     Assemblage de la sous-matrice H.H
      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) + DEUX*(POROS+PORO1+PORO1)
      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) +      (POROS+PORO2+PORO1)
      VKE(IKH3,IKH1) = VKE(IKH3,IKH1) +      (POROS+PORO3+PORO1)
      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) +      (POROS+PORO1+PORO2)
      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) + DEUX*(POROS+PORO2+PORO2)
      VKE(IKH3,IKH2) = VKE(IKH3,IKH2) +      (POROS+PORO3+PORO2)
      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) +      (POROS+PORO1+PORO3)
      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) +      (POROS+PORO2+PORO3)
      VKE(IKH3,IKH3) = VKE(IKH3,IKH3) + DEUX*(POROS+PORO3+PORO3)
!      VKE(IKH1,IKH1) = VKE(IKH1,IKH1) + COEF2
!      VKE(IKH2,IKH1) = VKE(IKH2,IKH1) + COEF
!      VKE(IKH3,IKH1) = VKE(IKH3,IKH1) + COEF
!      VKE(IKH1,IKH2) = VKE(IKH1,IKH2) + COEF
!      VKE(IKH2,IKH2) = VKE(IKH2,IKH2) + COEF2
!      VKE(IKH3,IKH2) = VKE(IKH3,IKH2) + COEF
!      VKE(IKH1,IKH3) = VKE(IKH1,IKH3) + COEF
!      VKE(IKH2,IKH3) = VKE(IKH2,IKH3) + COEF
!      VKE(IKH3,IKH3) = VKE(IKH3,IKH3) + COEF2

      SV2D_XBS_ASMME_V = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMME_V

C************************************************************************
C Sommaire: SV2D_XBS_ASMM_V
C
C Description:
C     ASSEMBLE LA MATRICE MASSE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMM_V(SELF, HMTX, F_ASM)

      CLASS (SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR
      INTEGER IC, IE
      REAL*8, POINTER :: VKE  (:, :)
      REAL*8, POINTER :: VPRN (:)
      REAL*8, POINTER :: VDJE (:)
      INTEGER,POINTER :: KNE  (:)
      INTEGER,POINTER :: KLOCE(:, :)
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRN_T),POINTER :: IPRN

C---     Tables locales
      INTEGER, PARAMETER :: NDJV_LCL =  5
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLE_LCL = 18
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VKE_LCL  (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VPRN_LCL (NNEL_LCL)
      REAL*8,  TARGET :: VDJE_LCL (NDJV_LCL)
      INTEGER, TARGET :: KNE_LCL  (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL(NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      IPRN => SELF%IPRN

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)

C---     Reshape the arrays
      VKE  (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      KLOCE(1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL
      VPRN (1:GDTA%NNELV) => VPRN_LCL
      VDJE (1:GDTA%NDJV)  => VDJE_LCL
      KNE  (1:GDTA%NCELV) => KNE_LCL

C-------  Boucle sur les éléments de volume
C         =================================
      DO 10 IC=1,GDTA%NELCOL
!$omp do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Transfert des valeurs élémentaires
         KNE (:) = GDTA%KNGV(:,IE)
         VDJE(:) = GDTA%VDJV(:,IE)
         VPRN(:) = EDTA%VPRNO(IPRN%COEFF_PORO, KNE(:))

C---        Calcul de la matrice élémentaire de volume
         IERR = SV2D_XBS_ASMME_V(VKE, VDJE, VPRN)

C---       Assemblage de la matrice
         KLOCE(:,:) = EDTA%KLOCN(:,KNE(:))
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_M_ELEM', ': ', IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMM_V = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMM_V

C************************************************************************
C Sommaire: SV2D_XBS_ASMM
C
C Description:
C     ASSEMBLAGE DE LA MATRICE MASSE
C
C Entrée:
C
C Sortie: VKG
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMM(SELF, HMTRX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMM
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER IERR
C----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMM_V(SELF, HMTRX, F_ASM)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF
      
C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMK(SELF, HMTRX, F_ASM)
      ENDIF

      SV2D_XBS_ASMM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMM

      END SUBMODULE SV2D_XBS_ASMM_M
