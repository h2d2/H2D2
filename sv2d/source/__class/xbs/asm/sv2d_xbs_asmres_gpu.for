C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_asmres.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_XBS_ASMRES
C   Private:
C     INTEGER SV2D_XBS_ASMRES_RV
C     INTEGER SV2D_$_CLC_PRE
C
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_XBS_ASMRES
C
C Description:
C     Assemblage du résidu de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XBS_ASMRES_G(HOBJ, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMRES_G
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VRES(*)

      INCLUDE 'err.fi'
      INCLUDE 'gpclcl.fi'
      INCLUDE 'lmelem.fc'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HCLC
      INTEGER NBPE
      INTEGER*8 XARGS(2)
      REAL*8    VARGS(1)

      EXTERNAL SV2D_XBS_ASMRES_CB
      INTEGER  SV2D_XBS_ASMRES_CB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_XBS_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

!!!C---     le kernel
!!!      FNAM = 'E:/dev/H2D2/sv2d/source/__class/OpenCL Kernel/' //
!!!     &       'sv2d_xbs_asmres_gpu.cl'
!!!      KNAM = 'sv2d_xbs_asmres_cl'

C---     Construis le calculateur
      HCLC = 0
      IERR = LM_ELEM_CFGGPU(HOBJ, HCLC)

C---     L'appel
      IF (ERR_GOOD()) THEN
         XARGS(1) = HOBJ
         XARGS(2) = SO_ALLC_REQVHND(VRES)
         IERR = GP_CLCL_XEQ(HCLC, SV2D_XBS_ASMRES_CB, XARGS, VARGS)
      ENDIF

      SV2D_XBS_ASMRES_G = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble {F}
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     VRES     F
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XBS_ASMRES_CB(HPAGE, XARGS, VARGS)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER   SV2D_XBS_ASMRES_CB
      INTEGER   HPAGE
      INTEGER*8 XARGS(*)
      REAL*8    VARGS(*)

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gppage.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HVTBL, HMETH
      INTEGER HRES
      INTEGER SV2D_XBS_ASMRES_CLC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HPAGE))
C-----------------------------------------------------------------------

      HOBJ = XARGS(1)
D     CALL ERR_ASR(SV2D_XBS_HVALIDE(HOBJ))

C---     Fait l'appel de méthode
      HRES = XARGS(2)
      IF (ERR_GOOD())
     &   IERR = SV2D_XBS_ASMRES_CLC(HOBJ,
     &                          HPAGE,
     &                          VA(SO_ALLC_REQVIND(VA, HRES)))

      SV2D_XBS_ASMRES_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_XBS_ASMRES
C
C Description:
C     Assemblage du résidu de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_XBS_ASMRES_CLC(HOBJ, HPGE, VRES)

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER SV2D_XBS_ASMRES_CLC
      INTEGER HOBJ
      INTEGER HPGE
      REAL*8  VRES (*)

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gppage.fi'

      INTEGER IERR
      INTEGER HDEV
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
!C-----------------------------------------------------------------------
D     CALL ERR_PRE(SV2D_XBS_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les données
      PDTA => GP_PAGE_REQPDTA(HPGE)
      HDEV =  GP_PAGE_REQHDEV(HPGE)

C---     Assigne les arguments
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 1, PDTA%VRES_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 2, PDTA%VPRC_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 3, PDTA%VDJV_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 4, PDTA%VPRG_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 5, PDTA%VPRN_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 6, PDTA%VDLG_ID)
      IF (ERR_GOOD()) IERR = GP_DEVC_SETARG_BUF(HDEV, 7, PDTA%PRMS_ID)

C---     Lance le calcul
      IF (ERR_GOOD()) IERR = GP_DEVC_XEQK(HDEV, PDTA%NTSK)

      SV2D_XBS_ASMRES_CLC = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:  SV2D_XBS_ASMRES
!!!C
!!!C Description:
!!!C     Assemblage du résidu de volume
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SV2D_XBS_ASMRES_CL(GDTA, EDTA, PDTA)
!!!
!!!      USE SV2D_XBS_M
!!!      USE GP_PAGE_M
!!!      IMPLICIT NONE
!!!
!!!      TYPE (LM_GDTA_T) :: GDTA
!!!      TYPE (LM_EDTA_T) :: EDTA
!!!      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
!!!
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'spelem.fi'
!!!
!!!      INTEGER IERR
!!!      INTEGER IT
!!!
!!!      INTEGER SV2D_XBS_ASMRES_RV
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(SV2D_XBS_HVALIDE(HOBJ))
!!!C-----------------------------------------------------------------------
!!!
!!!!!!----- le kernel
!!!      
!!!!$omp do !! sur les ntask gpu
!!!      DO IT=1,PDTA%NTSK
!!!
!!!C---     Contribution de volume
!!!      IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_RV(IT, GDTA,EDTA,PDTA,VRES)
!!!
!!!C---     Contribution de surface
!!!!      IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_RV(GDTA,EDTA,PDTA,VRES)
!!!
!!!C---     Contribution des conditions limites
!!!!      IF (ERR_GOOD()) IERR = SV2D_XBS_ASMRES_RV(GDTA,EDTA,PDTA,VRES)
!!!
!!!      ENDDO
!!!!$omp end do
!!!
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SV2D_XBS_ASMRES_RV(IT,
!!!     &                            GDTA,
!!!     &                            EDTA,
!!!     &                            PDTA,
!!!     &                            VRES)
!!!
!!!      USE SV2D_XBS_M
!!!      USE GP_PAGE_M
!!!      IMPLICIT NONE
!!!
!!!      INCLUDE 'eacnst.fi'
!!!
!!!      INTEGER SV2D_XBS_ASMRES_RV
!!!      INTEGER IT
!!!      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
!!!      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA
!!!      TYPE (GP_PAGE_DATA_T), INTENT(IN) :: PDTA
!!!      REAL*8 , INTENT(INOUT) :: VRES(EDTA%NEQL)
!!!
!!!      INCLUDE 'spelem.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'log.fi'
!!!
!!!      INTEGER IERR
!!!      INTEGER IE
!!!      REAL*8  VFE(6*3)
!!!      REAL*8, DIMENSION(:,:) :: VPRN(EDTA%NPRNO, GDTA%NNELV)
!!!      REAL*8, DIMENSION(:)   :: VPRE(EDTA%NPREV)
!!!      INTEGER SV2D_$_CLC_PRE
!!!C-----------------------------------------------------------------------
!!!C-----------------------------------------------------------------------
!!!
!!!C-------  Boucle sur les éléments de la tâche
!!!C         ===================================
!!!      DO IE=1, PDTA%NELE
!!!         VPRN(:,:) = PDTA%VPRN(1:EDTA%NPRNOL, 1:GDTA%NNELV, IT, IE)
!!!         VPRE(:) = ZERO
!!!
!!!C---        Pré-traitement avant calcul
!!!         IERR = SV2D_$_CLC_PRE(EDTA%VPRGL,
!!!     &                         VPRN,
!!!     &                         VPRE,
!!!     &                         PDTA%VDLG(:, :, IT, IE))
!!!
!!!         CALL SV2D_XBS_FE_V(VRES,
!!!     &                      GDTA%VDJV(:,IE),
!!!     &                      EDTA%VPRGL,
!!!     &                      VPRN,
!!!     &                      VPRE,
!!!     &                      PDTA%VDLG(1, 1, IT, IE),
!!!     &                      EDTA%VSOLR(:,IE))  !! (1, 1, IT, IE))
!!!
!!!         CALL SV2D_XBS_RE_V(VRES,
!!!     &                      GDTA%VDJV(:,IE),
!!!     &                      EDTA%VPRGL,
!!!     &                      VPRN,
!!!     &                      VPRE,
!!!     &                      PDTA%VDLG(1, 1, IT, IE),
!!!     &                      PDTA%VDLG(1, 1, IT, IE))
!!!
!!!      ENDDO
!!!
!!!      SV2D_XBS_ASMRES_RV = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SV2D_$_CLC_PRE(VPRG, VPRN, VPRE, VDLE)
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER SV2D_$_CLC_PRE
!!!      REAL*8, INTENT(IN)    :: VPRG(:)
!!!      REAL*8, INTENT(INOUT) :: VPRN(:,:)
!!!      REAL*8, INTENT(INOUT) :: VPRE(:)
!!!      REAL*8, INTENT(IN)    :: VDLE(:,:)
!!!
!!!      INCLUDE 'spelem.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'sv2d_xy4.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER IE, IT
!!!      REAL*8  VFE(6*3)
!!!C-----------------------------------------------------------------------
!!!C-----------------------------------------------------------------------
!!!
!!!C---     Calcule les propriétés nodales
!!!       IERR = SV2D_XY4_CLCPRNEV(VPRG, VPRN, VDLE)
!!!
!!!C---     CALCULE LES PROP. ELEMENTAIRES (VOLUME ET SURFACE)
!!!!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPREV(HOBJ)
!!!!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRES(HOBJ)
!!!
!!!C---     CALCULE LES COND. LIMITES
!!!!      IF (ERR_GOOD()) IERR = LM_ELEM_CLCCLIM(HOBJ)
!!!
!!!      SV2D_$_CLC_PRE = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!
!!!
!!!!!clc_pst
!!!!!           SUBROUTINE SV2D_XBS_CLCDLIB(KNGV,
!!!!!     &                           KLOCN,
!!!!!     &                           KDIMP,
!!!!!     &                           VDIMP,
!!!!!     &                           VDLG,
!!!!!     &                           IERR)
