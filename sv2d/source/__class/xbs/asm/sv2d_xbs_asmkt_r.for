C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_XBS
C         FTN (Sub)Module: SV2D_XBS_ASMKT_R_M
C            Public:
C               MODULE INTEGER SV2D_XBS_ASMKT_R
C            Private:
C               INTEGER SV2D_XBS_ASMKT_RV
C               INTEGER SV2D_XBS_ASMKT_RS
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_ASMKT_R_M

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_ASMKT_RV
C
C Description:
C     La fonction SV2D_XBS_ASMKT_RV calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée: HMTX
C
C Sortie: KLOCE : Table de localisation élémentaire
C         VKE : Matrice élémentaire
C
C Notes:
C     1) La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     2) La version d'Hydrosim est plus efficace. Elle calcule la partie
C     tangente comme:
C        dK_il/du_j ~= ([K(u+delu_j)]{u+delu_j} - [K(u)]{u} ) / delu_j
C
C     3) La perturbation d'un DDL en h sur un noeud sommet entraîne une
C     modification du DDL pour le noeud milieu dans le calcul des prop. nodales.
C     La valeur originale du noeud sommet est restaurée à la fin de la boucle
C     de perturbation. Le noeud milieu lui n'est restauré qu'au prochain
C     calcul de prop. nodales. Comme le dernier DDL perturbé est en v
C     (noeud 6), il y a implicitement restauration.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMKT_RV(SELF, HMTX, F_ASM)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_V

      CLASS (SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'
!      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, IDG
      INTEGER HVFT
      REAL*8  PENA
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      REAL*8, POINTER :: VDLE  (:,:)
      REAL*8, POINTER :: VPRN  (:,:)
      REAL*8, POINTER :: VDJE  (:)
      REAL*8, POINTER :: VPRE  (:,:)
      REAL*8, POINTER :: VKE   (:,:)
      REAL*8, POINTER :: VRESE (:,:)
      REAL*8, POINTER :: VRESEP(:,:)
      INTEGER,POINTER :: KLOCE (:,:)
      INTEGER,POINTER :: KNE   (:)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8,  TARGET :: VDLE_LCL  (NDLE_LCL)
      REAL*8,  TARGET :: VPRN_LCL  (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VDJE_LCL  (NDJE_LCL)
      REAL*8,  TARGET :: VPRE_LCL  (NPRE_LCL)
      REAL*8,  TARGET :: VKE_LCL   (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VRESE_LCL (NDLE_LCL)
      REAL*8,  TARGET :: VRESEP_LCL(NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL (NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG
      !PDTA => GP_PAGE_REQPDTA(HPGE)

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NDJE_LCL .GE. GDTA%NDJV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_ASR(NPRE_LCL .GE. EDTA%NPREV)

C---     Reshape the arrays
      VDLE  (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRN  (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      VDJE  (1:GDTA%NDJV)  => VDJE_LCL
      VPRE  (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPRE_LCL
      VKE   (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      VRESE (1:EDTA%NDLN, 1:GDTA%NNELV) => VRESE_LCL
      VRESEP(1:EDTA%NDLN, 1:GDTA%NNELV) => VRESEP_LCL
      !VRESEP_F(1:EDTA%NDLEV) => VRESEP_LCL
      KNE   (1:GDTA%NNELV) => KNE_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C-------  BOUCLE SUR LES ELEMENTS DE VOLUME
C         =================================
      DO 10 IC=1,GDTA%NELCOL
!$omp  do
      DO 20 IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         KNE(:) = GDTA%KNGV(:, IE)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:, KNE(:))

C---        Transfert des valeurs nodales T6L
         VDLE (:,:) = EDTA%VDLG (:, KNE(:))
         VPRN (:,:) = EDTA%VPRNO(:, KNE(:))

C---        Transfert des valeurs élémentaires
         VDJE(:) = GDTA%VDJV (:, IE)
         VPRE(:,:) = EDTA%VPREV(:, :, IE)

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        R(u)
         VRESE(:,:) = ZERO
         IERR = SV2D_XBS_RE_V(VRESE,
     &                        VDJE,
     &                        VPRN,
     &                        VPRE,
     &                        VDLE,
     &                        VDLE,       ! VRHS
     &                        SELF%IPRN,
     &                        SELF%XPRG)

C---        Boucle de perturbation sur les DDL
C           ==================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL
            VDL_ORI = VDLE(ID,IN)
            VDL_DEL = XPRG%PNUMR_DELPRT * VDLE(ID,IN)
     &              + SIGN(XPRG%PNUMR_DELMIN, VDLE(ID,IN))
            VDL_INV = XPRG%PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID,IN) = VDLE(ID,IN) + VDL_DEL

C---           Propriétés nodales perturbées
            IF (XPRG%PNUMR_PRTPRNO) THEN
               IERR = SELF%CLCPRNEV(VDLE, VPRN)
            ENDIF

C---           Propriétés élémentaires perturbées
            IF (XPRG%PNUMR_PRTPREL) THEN
               IERR = SELF%CLCPREVE(VDJE, VPRN, VDLE, VPRE)
            ENDIF

C---           R(u + du_id)
            VRESEP(:,:) = ZERO
            IERR = SV2D_XBS_RE_V(VRESEP,
     &                           VDJE,
     &                           VPRN,
     &                           VPRE,
     &                           VDLE,
     &                           VDLE,  ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG)

C---           Restaure la valeur originale
            VDLE(ID,IN) = VDL_ORI

C---           (R(u + du_id) - R(u))/du
            VRESEP(:,:) = (VRESEP(:,:) - VRESE(:,:)) * VDL_INV
            VKE(:,IDG) = VRESEP_LCL(1:EDTA%NDLEV)  ! use the rank 1 array

199         CONTINUE
         ENDDO
         ENDDO

C---        Pénalisation en hh
         PENA = XPRG%PNUMR_PENALITE*VDJE(5)
         VKE( 3, 3) = MAX(VKE( 3, 3), PENA)
         VKE( 9, 9) = MAX(VKE( 9, 9), PENA)
         VKE(15,15) = MAX(VKE(15,15), PENA)

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      SV2D_XBS_ASMKT_RV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKT_RV

C************************************************************************
C Sommaire: SV2D_XBS_ASMKT_RS
C
C Description:
C     La fonction SV2D_XBS_ASMKT_RS calcule la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) c.f. SV2D_XBS_ASMKT_RV
C
C     2) Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C
C     3) Il faut perturber tous les dll de l'élément de volume car les
C     VPREV dépendent des VPRN. Les VPRES ne sont pas utilisées.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_ASMKT_RS(SELF, HMTX, F_ASM)

      USE SV2D_XBS_RE_M, ONLY: SV2D_XBS_RE_S
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_fnct.fi'

      INTEGER IERR
      INTEGER IN, ID, IDG
      INTEGER IES, IEV, ICT
      INTEGER HVFT
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_XPRG_T),     POINTER :: XPRG
      REAL*8, POINTER :: VDLEV (:,:)
      REAL*8, POINTER :: VPRNV (:,:)
      REAL*8, POINTER :: VDJEV (:)
      REAL*8, POINTER :: VPREV (:,:)
      REAL*8, POINTER :: VPRES (:,:)
      REAL*8, POINTER :: VKE   (:,:)
      REAL*8, POINTER :: VRESE (:,:)
      REAL*8, POINTER :: VRESEP(:,:)
      INTEGER,POINTER :: KNEV  (:)
      INTEGER,POINTER :: KLOCE (:, :)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NDLE_LCL = NDLN_LCL * NNEL_LCL
      INTEGER, PARAMETER :: NPRN_LCL = 18
      INTEGER, PARAMETER :: NDJE_LCL =  5
      INTEGER, PARAMETER :: NPRE_LCL =  8
      REAL*8,  TARGET :: VDLEV_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRNV_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VDJEV_LCL (NDJE_LCL)
      REAL*8,  TARGET :: VPREV_LCL (NPRE_LCL)
      REAL*8,  TARGET :: VPRES_LCL (NPRE_LCL)
      REAL*8,  TARGET :: VKE_LCL   (NDLE_LCL * NDLE_LCL)
      REAL*8,  TARGET :: VRESE_LCL (NDLE_LCL)
      REAL*8,  TARGET :: VRESEP_LCL(NDLE_LCL)
      INTEGER, TARGET :: KNE_LCL   (NNEL_LCL)
      INTEGER, TARGET :: KLOCE_LCL (NDLE_LCL)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA
      XPRG => SELF%XPRG
      !PDTA => GP_PAGE_REQPDTA(HPGE)

C---     Contrôles: Assertion sur les dimensions
D     CALL ERR_ASR(NNEL_LCL .GE. GDTA%NNELV)
D     CALL ERR_ASR(NDLN_LCL .GE. EDTA%NDLN)
D     CALL ERR_ASR(NDLE_LCL .GE. EDTA%NDLEV)
D     CALL ERR_ASR(NDJE_LCL .GE. GDTA%NDJV)
D     CALL ERR_ASR(NPRN_LCL .GE. EDTA%NPRNO)
D     CALL ERR_ASR(NPRE_LCL .GE. EDTA%NPREV)

C---     Reshape the arrays
      VDLEV (1:EDTA%NDLN, 1:GDTA%NNELV) => VDLEV_LCL
      VPRNV (1:EDTA%NPRNO,1:GDTA%NNELV) => VPRNV_LCL
      VDJEV (1:GDTA%NDJV)  => VDJEV_LCL
      VPREV (1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPREV_LCL
      VPRES (1:EDTA%NPRES_D1, 1:EDTA%NPRES_D2) => VPRES_LCL
      VKE   (1:EDTA%NDLEV,1:EDTA%NDLEV) => VKE_LCL
      VRESE (1:EDTA%NDLN, 1:GDTA%NNELV) => VRESE_LCL
      VRESEP(1:EDTA%NDLN, 1:GDTA%NNELV) => VRESEP_LCL
      KNEV  (1:GDTA%NNELV) => KNE_LCL
      KLOCE (1:EDTA%NDLN, 1:GDTA%NNELV) => KLOCE_LCL

C-------  Boucle sur les éléments de surface
C         ==================================
      DO IES=1,GDTA%NELLS

C---        Élément parent et coté
         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Transfert des connectivités de l'élément T6L
         KNEV(:) = GDTA%KNGV(:,IEV)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = EDTA%KLOCN(:, KNEV(:))

C---        Transfert des valeurs nodales T6L
         VDLEV(:,:) = EDTA%VDLG (:,KNEV(:))
         VPRNV(:,:) = EDTA%VPRNO(:,KNEV(:))

C---        Transfert des valeurs élémentaires
         VDJEV(:)   = GDTA%VDJV (:,IEV)
         VPREV(:,:) = EDTA%VPREV(:,:,IEV)
         VPRES(:,:) = EDTA%VPRES(:,:,IES)

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        R(u)
         VRESE(:,:) = ZERO
         IERR = SV2D_XBS_RE_S(VRESE,
     &                        GDTA%VDJV(:,IEV),
     &                        GDTA%VDJS(:,IES),
     &                        VPRNV,
     &                        VPREV,
     &                        VPRES,         ! Pas utilisé
     &                        VDLEV,
     &                        VDLEV,         ! VRHS
     &                        SELF%IPRN,
     &                        SELF%XPRG,
     &                        ICT)


C---        Boucle de perturbation sur tous les ddl
C           =======================================
         IDG = 0
         DO IN=1,GDTA%NNELV
         DO ID=1,EDTA%NDLN
            IDG = IDG + 1
            IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---           Perturbe le DDL du T6L
            VDL_ORI = VDLEV(ID,IN)
            VDL_DEL = XPRG%PNUMR_DELPRT * VDLEV(ID,IN)
     &              + SIGN(XPRG%PNUMR_DELMIN, VDLEV(ID,IN))
            VDL_INV = XPRG%PNUMR_OMEGAKT / VDL_DEL
            VDLEV(ID,IN) = VDLEV(ID,IN) + VDL_DEL

C---           Propriétés nodales perturbées
            IF (XPRG%PNUMR_PRTPRNO) THEN
               IERR = SELF%CLCPRNEV(VDLEV, VPRNV)
            ENDIF

C---           Propriétés élémentaires perturbées
            IF (XPRG%PNUMR_PRTPREL) THEN
               IERR = SELF%CLCPREVE(VDJEV,  VPRNV, VDLEV, VPREV)
            ENDIF

!!!C---           Propriétés élémentaires de surface perturbées (pas utilisées)
!!!            IF (XPRG%PNUMR_PRTPREL) THEN
!!!               IERR = SO_FUNC_CALL6(HPRESE,
!!!     &                              BA(XPRESE(1)),
!!!     &                              BA(XPRESE(2)),
!!!     &                              BA(XPRESE(3)),
!!!     &                              BA(XPRESE(4)),
!!!     &                              BA(XPRESE(5)),
!!!     &                              BA(XPRESE(6)))
!!!            ENDIF

C---           R(u)
            VRESEP(:,:) = ZERO
            IERR = SV2D_XBS_RE_S(VRESEP,
     &                           GDTA%VDJV(:,IEV),
     &                           GDTA%VDJS(:,IES),
     &                           VPRNV,
     &                           VPREV,
     &                           VPRES,         ! Pas utilisé
     &                           VDLEV,
     &                           VDLEV,         ! VRHS
     &                           SELF%IPRN,
     &                           SELF%XPRG,
     &                           ICT)

C---           Restaure la valeur originale
            VDLEV(ID,IN) = VDL_ORI

C---           (R(u + du_id) - R(u))/du
            VRESEP(:,:) = (VRESEP(:,:) - VRESE(:,:)) * VDL_INV
            VKE(:,IDG) = VRESEP_LCL(1:EDTA%NDLEV)  ! use the rank 1 array

199         CONTINUE
         ENDDO
         ENDDO

C---       Assemble la matrice
         IERR = F_ASM(HMTX, EDTA%NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      SV2D_XBS_ASMKT_RS = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKT_RS

C************************************************************************
C Sommaire: SV2D_XBS_ASMKT_R
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C     La fonction SV2D_XBS_ASMKT_R calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_ASMKT_R(SELF, HMTRX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_ASMKT_R
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER  IERR
C----------------------------------------------------------------

      IERR = ERR_OK

C---     Contributions de volume
      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
         IERR = SV2D_XBS_ASMKT_RV(SELF, HMTRX, F_ASM)

         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

C---     Contributions de surface
      IF (ERR_GOOD()) THEN
         IERR = SV2D_XBS_ASMKT_RS(SELF, HMTRX, F_ASM)
      ENDIF

C---     Contributions de conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%ASMKT(SELF, HMTRX, F_ASM)
      ENDIF

      SV2D_XBS_ASMKT_R = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_ASMKT_R

      END SUBMODULE SV2D_XBS_ASMKT_R_M
