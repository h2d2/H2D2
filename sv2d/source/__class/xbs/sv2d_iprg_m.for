C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Notes:
C
C Functions:
C
C************************************************************************

      MODULE SV2D_IPRG_M

C---     Indices dans PRGL
      TYPE :: SV2D_IPRG_T
         INTEGER GRAVITE
         INTEGER LATITUDE
         INTEGER FCT_CW_VENT
         INTEGER VISCO_CST
         INTEGER VISCO_LM
         INTEGER VISCO_SMGO
         INTEGER VISCO_BINF
         INTEGER VISCO_BSUP
         INTEGER DECOU_HTRG
         INTEGER DECOU_HMIN
         INTEGER DECOU_PENA_H
         INTEGER DECOU_PENA_Q
         INTEGER DECOU_MAN
         INTEGER DECOU_UMAX
         INTEGER DECOU_PORO
         INTEGER DECOU_AMORT
         INTEGER DECOU_CON_FACT
         INTEGER DECOU_GRA_FACT
         INTEGER DECOU_DIF_NU
         INTEGER DECOU_DIF_PE
         INTEGER DECOU_DRC_NU
         INTEGER STABI_PECLET
         INTEGER STABI_AMORT
         INTEGER STABI_DARCY
         INTEGER STABI_LAPIDUS
         INTEGER CMULT_CON
         INTEGER CMULT_GRA
         INTEGER CMULT_PDYN
         INTEGER CMULT_MAN
         INTEGER CMULT_VENT
         INTEGER CMULT_INTGCTR
         INTEGER PNUMR_PENALITE
         INTEGER PNUMR_DELPRT
         INTEGER PNUMR_DELMIN
         INTEGER PNUMR_OMEGAKT
         INTEGER PNUMR_PRTPREL
         INTEGER PNUMR_PRTPRNO
      END TYPE SV2D_IPRG_T

      END MODULE SV2D_IPRG_M
