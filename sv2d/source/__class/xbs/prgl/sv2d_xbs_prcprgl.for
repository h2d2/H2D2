C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_prcprgl.for,v 1.42 2015/11/26 18:54:07 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCPRGL
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCPRGL_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XBS_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRCPRGL
CDEC$ ENDIF

      USE SV2D_XPRG_M
      USE TRIGD_M

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cnst.fi'
      INCLUDE 'sv2d_enum.fi'

      REAL*8, PARAMETER :: VROTTERRE = 360.0D0/(24.0D0*3600.0D0)

      INTEGER IERR
      INTEGER HNDL
      REAL*8  DELH
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRG_T),POINTER :: IPRG
      TYPE (SV2D_XPRG_T),POINTER :: XPRG
      REAL*8, PARAMETER :: UNDEFINED_VALUE = -1.0D66
C-----------------------------------------------------------------------
      REAL*8  V, PRG_OR_DEF, CORIOLIS
      INTEGER I
      LOGICAL R2L
      R2L(V) = (NINT(V) .NE. 0)
      PRG_OR_DEF(I) = MERGE(EDTA%VPRGL(I), UNDEFINED_VALUE, I .GT. 0)
      CORIOLIS(V) = DEUX*DEG2RAD(VROTTERRE)*SIND(V)
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      XPRG => SELF%XPRG

C---     Affecte les valeurs aux propriétés globales
      XPRG%GRAVITE         = PRG_OR_DEF(IPRG%GRAVITE)
      XPRG%FCT_CW_VENT     = SV2D_CW_WU1969
      XPRG%VISCO_CST       = PRG_OR_DEF(IPRG%VISCO_CST)
      XPRG%VISCO_LM        = PRG_OR_DEF(IPRG%VISCO_LM)
      XPRG%VISCO_SMGO      = PRG_OR_DEF(IPRG%VISCO_SMGO)
      XPRG%VISCO_BINF      = PRG_OR_DEF(IPRG%VISCO_BINF)
      XPRG%VISCO_BSUP      = PRG_OR_DEF(IPRG%VISCO_BSUP)

      XPRG%DECOU_HTRG      = PRG_OR_DEF(IPRG%DECOU_HTRG)
      XPRG%DECOU_HMIN      = PRG_OR_DEF(IPRG%DECOU_HMIN)
      DELH                 = XPRG%DECOU_HTRG - XPRG%DECOU_HMIN
      XPRG%DECOU_DHST      = 0.2D+00 * DELH
      XPRG%DECOU_UN_HDIF   = UN / MAX(1.0D-32, DELH)
      XPRG%DECOU_PENA_H    = ZERO
      XPRG%DECOU_PENA_Q    = ZERO
      XPRG%DECOU_MAN       = PRG_OR_DEF(IPRG%DECOU_MAN)
      XPRG%DECOU_UMAX      = PRG_OR_DEF(IPRG%DECOU_UMAX)
      XPRG%DECOU_PORO      = PRG_OR_DEF(IPRG%DECOU_PORO)
      XPRG%DECOU_AMORT     = PRG_OR_DEF(IPRG%DECOU_AMORT)
      XPRG%DECOU_CON_FACT  = PRG_OR_DEF(IPRG%DECOU_CON_FACT)
      XPRG%DECOU_GRA_FACT  = PRG_OR_DEF(IPRG%DECOU_GRA_FACT)
      XPRG%DECOU_DIF_NU    = PRG_OR_DEF(IPRG%DECOU_DIF_NU)
      XPRG%DECOU_DRC_NU    = PRG_OR_DEF(IPRG%DECOU_DRC_NU)

      XPRG%STABI_PECLET    = PRG_OR_DEF(IPRG%STABI_PECLET)
      XPRG%STABI_AMORT     = PRG_OR_DEF(IPRG%STABI_AMORT)
      XPRG%STABI_DARCY     = PRG_OR_DEF(IPRG%STABI_DARCY)
      XPRG%STABI_LAPIDUS   = PRG_OR_DEF(IPRG%STABI_LAPIDUS)

      XPRG%CMULT_CON       = PRG_OR_DEF(IPRG%CMULT_CON)
      XPRG%CMULT_GRA       = PRG_OR_DEF(IPRG%CMULT_GRA)
      XPRG%CMULT_PDYN      = ZERO
      XPRG%CMULT_MAN       = PRG_OR_DEF(IPRG%CMULT_MAN)
      XPRG%CMULT_VENT      = PRG_OR_DEF(IPRG%CMULT_VENT)
      XPRG%CMULT_INTGCTRQX = PRG_OR_DEF(IPRG%CMULT_INTGCTR)
      XPRG%CMULT_INTGCTRQY = PRG_OR_DEF(IPRG%CMULT_INTGCTR)
      XPRG%CMULT_INTGCTRH  = PRG_OR_DEF(IPRG%CMULT_INTGCTR)

      XPRG%PNUMR_PENALITE  = PRG_OR_DEF(IPRG%PNUMR_PENALITE)
      XPRG%PNUMR_DELPRT    = PRG_OR_DEF(IPRG%PNUMR_DELPRT)
      XPRG%PNUMR_DELMIN    = 1.0D-07
      XPRG%PNUMR_OMEGAKT   = PRG_OR_DEF(IPRG%PNUMR_OMEGAKT)
      XPRG%PNUMR_PRTPREL   = R2L(EDTA%VPRGL(IPRG%PNUMR_PRTPREL))
      XPRG%PNUMR_PRTPRNO   = R2L(EDTA%VPRGL(IPRG%PNUMR_PRTPRNO))

C---     Propriétés calculées
      XPRG%CMULT_VENT_REL  = ZERO
      XPRG%CORIOLIS        = ZERO
      IF (IPRG%LATITUDE .GT. 0) THEN
         XPRG%CORIOLIS     = CORIOLIS(EDTA%VPRGL(IPRG%LATITUDE)) 
      ENDIF
      
C---     Fait le lien inverse
      EDTA%NPRGX =  SV2D_XPRG_REQNRE8(XPRG)
      EDTA%VPRGX => SV2D_XPRG_REQPRE8(XPRG)
      
      SV2D_XBS_PRCPRGL = ERR_TYP()
      RETURN
      END

      END SUBMODULE SV2D_XBS_PRCPRGL_M
