C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_pslprgl.for,v 1.40 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PSLPRGL
C   Private:
C     INTEGER SV2D_XBS_PSLPRGL_E
C     INTEGER SV2D_XBS_INI_IPRG
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PSLPRGL_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XBS_PSLPRGL_E
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL_CHK(SELF)

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER I, IE
      REAL*8  VVAL, VMIN, VMAX
      REAL*8, POINTER :: VPRGL(:)
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SV2D_IPRG_T),    POINTER :: IPRG

      REAL*8, PARAMETER :: DIX  = 1.0D+01
      REAL*8, PARAMETER :: CENT = 1.0D+02
      REAL*8, PARAMETER :: TGV  = 1.0D+99

      REAL*8, PARAMETER :: GRAMAX = 9.86605398D0
      REAL*8, PARAMETER :: GRAMIN = 9.75258282D0

      REAL*8, PARAMETER :: VISCOMAX =  1.0D0
      REAL*8, PARAMETER :: LMMAX    = 10.0D0
      INTEGER CHKVAL
C-----------------------------------------------------------------------
      REAL*8 V, V1, V2
      LOGICAL IS_OUT
      IS_OUT(V,V1,V2) = (V .LT. V1 .OR.  V .GT. V2)
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      VPRGL=> EDTA%VPRGL

C---     Gravité
      I = IPRG%GRAVITE
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_GRAVITE')

C---     Coriolis
      I = IPRG%LATITUDE
      IE = CHKVAL(I, VPRGL(I), -90.0D+00, 90.0D+00, 'MSG_LATITUDE')

C---     Viscosité
      I = IPRG%VISCO_CST
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_LAMINAIRE')
      I = IPRG%VISCO_LM
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_LM')
      I = IPRG%VISCO_SMGO
      IE = CHKVAL(I, VPRGL(I), ZERO, DIX, 'MSG_VISCO_SMGO')
      I = IPRG%VISCO_BINF
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_VISCO_BORNE_INF')
      I = IPRG%VISCO_BSUP
      IE = CHKVAL(I, VPRGL(I), VPRGL(I-1), TGV, 'MSG_VISCO_BORNE_SUP')

C---     Découvrement
      I = IPRG%DECOU_HTRG
      IE = CHKVAL(I, VPRGL(I), ZERO,CENT, 'MSG_DECOUVREMENT_PROF_TRIG')
      I = IPRG%DECOU_HMIN
      VMAX = (1.0D0-1.0D-12)*VPRGL(IPRG%DECOU_HTRG)
      IE = CHKVAL(I, VPRGL(I), ZERO,VMAX, 'MSG_DECOUVREMENT_PROF_MIN')
      I = IPRG%DECOU_MAN
      IE = CHKVAL(I, VPRGL(I), ZERO, DIX, 'MSG_DECOUVREMENT_MAN')
      I = IPRG%DECOU_UMAX
      IE = CHKVAL(I, VPRGL(I), ZERO,CENT, 'MSG_DECOUVREMENT_UMAX')
      I = IPRG%DECOU_PORO
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_PORO')
      I = IPRG%DECOU_AMORT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_AMORT')

      I = IPRG%DECOU_CON_FACT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_CON_FACT')
      I = IPRG%DECOU_GRA_FACT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_DECOUVREMENT_GRA_FACT')
      I = IPRG%DECOU_DIF_NU
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_DECOUVREMENT_DIF_NU')
      I = IPRG%DECOU_DRC_NU
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_DECOUVREMENT_DRC_NU')

C---     Stabilité
      I = IPRG%STABI_PECLET
      IE = CHKVAL(I, VPRGL(I),PETIT, TGV, 'MSG_STABI_PECLET')
      I = IPRG%STABI_AMORT
      IE = CHKVAL(I, VPRGL(I), ZERO,  UN, 'MSG_STABI_AMORT')
      I = IPRG%STABI_DARCY
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_STABI_DARCY')
      I = IPRG%STABI_LAPIDUS
      IE = CHKVAL(I, VPRGL(I), ZERO, TGV, 'MSG_STABI_LAPIDUS')

C---     Coefficients
      I = IPRG%CMULT_CON
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_CMULT_CON')
      I = IPRG%CMULT_GRA
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_CMULT_GRA')
      I = IPRG%CMULT_MAN
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_CMULT_MAN')
      I = IPRG%CMULT_VENT
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_CMULT_VENT')
      I = IPRG%CMULT_INTGCTR
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_CMULT_INTGCTR')

C---     Numérique
      I = IPRG%PNUMR_PENALITE
      IE = CHKVAL(I, VPRGL(I), ZERO, UN, 'MSG_PNUMR_PENALITE')
      I = IPRG%PNUMR_DELPRT
      IE = CHKVAL(I, VPRGL(I),PETIT, UN, 'MSG_PNUMR_DELPRT')
      I = IPRG%PNUMR_OMEGAKT
      IE = CHKVAL(I, VPRGL(I),PETIT, UN, 'MSG_PNUMR_OMEGAKT')
      I = IPRG%PNUMR_PRTPREL
      IE = CHKVAL(I, VPRGL(I),ZERO, UN, 'MSG_PNUMR_PRTPREL')
      I = IPRG%PNUMR_PRTPRNO
      IE = CHKVAL(I, VPRGL(I),ZERO, UN, 'MSG_PNUMR_PRTPRNO')

C---     Avertissements
      IF (ERR_BAD()) GOTO 9999
      IF (IS_OUT(VPRGL(IPRG%GRAVITE), GRAMIN, GRAMAX)) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3,A,1PE14.6E3)')
     &      'MSG_GRAVITE_NON_TERRESTRE: ', GRAMIN,' <= g <= ',GRAMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (IS_OUT(VPRGL(IPRG%VISCO_CST), ZERO, VISCOMAX)) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_VISCO_NON_PHYSIQUE: ', 'nu <= ',VISCOMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (IS_OUT(VPRGL(IPRG%VISCO_LM), ZERO, LMMAX)) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)')
     &      'MSG_LM_PEU_REALISTE: ', 'lm <= ', LMMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

9999  CONTINUE
      SV2D_XBS_PSLPRGL_CHK = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSLPRGL_CHK

C************************************************************************
C Sommaire : SV2D_XBS_INI_IPRG
C
C Description:
C     La fonction privée SV2D_XBS_PSLPRGL_IPG initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL_IPG(SELF)

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE (SV2D_IPRG_T), POINTER :: IPRG
C-----------------------------------------------------------------------

C---     Récupère les données
      IPRG => SELF%IPRG

C---     Assigne les indices      
      IPRG%GRAVITE         =  1
      IPRG%LATITUDE        =  2
      IPRG%FCT_CW_VENT     = -1
      IPRG%VISCO_CST       =  3
      IPRG%VISCO_LM        =  4
      IPRG%VISCO_SMGO      =  5
      IPRG%VISCO_BINF      =  6
      IPRG%VISCO_BSUP      =  7
      IPRG%DECOU_HTRG      =  8
      IPRG%DECOU_HMIN      =  9
      IPRG%DECOU_PENA_H    = -1
      IPRG%DECOU_PENA_Q    = -1
      IPRG%DECOU_MAN       = 10
      IPRG%DECOU_UMAX      = 11
      IPRG%DECOU_PORO      = 12
      IPRG%DECOU_AMORT     = 13
      IPRG%DECOU_CON_FACT  = 14
      IPRG%DECOU_GRA_FACT  = 15
      IPRG%DECOU_DIF_NU    = 16
      IPRG%DECOU_DRC_NU    = 17
      IPRG%STABI_PECLET    = 18
      IPRG%STABI_AMORT     = 19
      IPRG%STABI_DARCY     = 20
      IPRG%STABI_LAPIDUS   = 21
      IPRG%CMULT_CON       = 22
      IPRG%CMULT_GRA       = 23
      IPRG%CMULT_PDYN      = -1
      IPRG%CMULT_MAN       = 24
      IPRG%CMULT_VENT      = 25
      IPRG%CMULT_INTGCTR   = 26
      IPRG%PNUMR_PENALITE  = 27
      IPRG%PNUMR_DELPRT    = 28
      IPRG%PNUMR_DELMIN    = -1
      IPRG%PNUMR_OMEGAKT   = 29
      IPRG%PNUMR_PRTPREL   = 30
      IPRG%PNUMR_PRTPRNO   = 31

      SV2D_XBS_PSLPRGL_IPG = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSLPRGL_IPG

C************************************************************************
C Sommaire : SV2D_XBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PSLPRGL
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Fait le traitement
      IERR = SELF%PSLPRGL_IPG()
      IERR = SELF%PSLPRGL_CHK()

      SV2D_XBS_PSLPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSLPRGL 

      END SUBMODULE SV2D_XBS_PSLPRGL_M
      