C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_XBS_HLPPRGL
C   Private:
C     INTEGER SV2D_XBS_HLPPRGL
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_HLPPRGL_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_XBS_HLP1 imprime la description d'une
C     seule propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_HLP1(IP, TXT)

      INTEGER       IP
      CHARACTER*(*) TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_XBS_HLP1 = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_HLP1

C************************************************************************
C Sommaire: SV2D_XBS_HLPPRGL
C
C Description:
C     Aide sur les propriétés globales
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_HLPPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_HLPPRGL
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Groupe des propriétés physiques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SELF%IPRG%GRAVITE
      IERR = SV2D_XBS_HLP1(I,'MSG_GRAVITE')
      I = SELF%IPRG%LATITUDE
      IERR = SV2D_XBS_HLP1(I,'MSG_LATITUDE')
      I = SELF%IPRG%FCT_CW_VENT
      IF (I .GT. 0)
     &   IERR = SV2D_XBS_HLP1(I,'MSG_FCT_CW_VENT')
      I = SELF%IPRG%VISCO_CST
      IERR = SV2D_XBS_HLP1(I,'MSG_VISCOSITE_MOLECULAIRE')
      I = SELF%IPRG%VISCO_LM
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_LONGUEUR_MELANGE')
      I = SELF%IPRG%VISCO_SMGO
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_SMAGORINSKY')
      I = SELF%IPRG%VISCO_BINF
      IERR = SV2D_XBS_HLP1(I,'MSG_BORNE_INF_VISCOSITE')
      I = SELF%IPRG%VISCO_BSUP
      IERR = SV2D_XBS_HLP1(I,'MSG_BORNE_SUP_VISCOSITE')

C---     Groupe des propriétés du découvrement
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SELF%IPRG%DECOU_HTRG
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_PROF_TRIG')
      I = SELF%IPRG%DECOU_HMIN
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_PROF_MIN')
      I = SELF%IPRG%DECOU_PENA_H
      IF (I .GT. 0)
     &   IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_PENA_H')
      I = SELF%IPRG%DECOU_PENA_Q
      IF (I .GT. 0)
     &   IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_PENA_Q')
      I = SELF%IPRG%DECOU_MAN
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_MAN')
      I = SELF%IPRG%DECOU_UMAX
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_UMAX')
      I = SELF%IPRG%DECOU_PORO
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_PORO')
      I = SELF%IPRG%DECOU_AMORT
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_AMORT')
      I = SELF%IPRG%DECOU_CON_FACT
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_COEF_CONVECTION')
      I = SELF%IPRG%DECOU_GRA_FACT
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_COEF_GRAVITE')
      I = SELF%IPRG%DECOU_DIF_NU
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_NU_DIFFUSION')
      I = SELF%IPRG%DECOU_DRC_NU
      IERR = SV2D_XBS_HLP1(I,'MSG_DECOUVREMENT_NU_DARCY')

C---     Groupe des propriétés de stabilisation
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SELF%IPRG%STABI_PECLET
      IERR = SV2D_XBS_HLP1(I,'MSG_PECLET')
      I = SELF%IPRG%STABI_AMORT
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_AMORTISSEMENT')
      I = SELF%IPRG%STABI_DARCY
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_SURFACE_LIBRE_DARCY')
      I = SELF%IPRG%STABI_LAPIDUS
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_SURFACE_LIBRE_LAPIDUS')

C---     Groupe des prop d'activation-désactivation des termes
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SELF%IPRG%CMULT_CON
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_CONVECTION')
      I = SELF%IPRG%CMULT_GRA
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_GRAVITE')
      I = SELF%IPRG%CMULT_PDYN
      IF (I .GT. 0)
     &   IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PDYN')
      I = SELF%IPRG%CMULT_MAN
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_FROTTEMENT_FOND')
      I = SELF%IPRG%CMULT_VENT
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_FROTTEMENT_VENT')
      I = SELF%IPRG%CMULT_INTGCTR
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_INTGRL_CONTOUR')

C---     Groupe des propriétés numériques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = SELF%IPRG%PNUMR_PENALITE
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PENALITE')
      I = SELF%IPRG%PNUMR_DELPRT
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PERTURBATION_KT')
      I = SELF%IPRG%PNUMR_DELMIN
      IF (I .GT. 0)
     &   IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PERTURBATION_KT_MIN')
      I = SELF%IPRG%PNUMR_OMEGAKT
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_RELAXATION_KT')
      I = SELF%IPRG%PNUMR_PRTPREL
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PERTURB_PREL')
      I = SELF%IPRG%PNUMR_PRTPRNO
      IERR = SV2D_XBS_HLP1(I,'MSG_COEF_PERTURB_PRNO')

      CALL LOG_DECIND()

      SV2D_XBS_HLPPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_HLPPRGL

      END SUBMODULE SV2D_XBS_HLPPRGL_M
