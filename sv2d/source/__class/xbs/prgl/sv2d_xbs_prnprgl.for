C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs_prnprgl.for,v 1.32 2015/11/26 18:54:07 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRNPRGL
C   Private:
C     INTEGER SV2D_XBS_1RE8
C     INTEGER SV2D_XBS_1INT
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRNPRGL_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_XBS_1RE8 imprime une seule
C     propriété globale de type Real*8
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_1RE8(IP, TXT, V)

      IMPLICIT NONE

      INTEGER       IP
      CHARACTER*(*) TXT
      REAL*8        V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,1PE14.6E3)') IP, TXT, '#<60>#=', V
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_XBS_1RE8 = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_1RE8

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_XBS_1RE8 imprime une seule
C     propriété globale de type I8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_1INT(IP, TXT, IV)

      INTEGER       IP
      CHARACTER*(*) TXT
      INTEGER       IV

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,I12)') IP, TXT, '#<60>#=', IV
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_XBS_1INT = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_1INT

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SV2D_XBS_1STR imprime une seule
C     propriété globale de type STRing
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_1STR(IP, TXT, V)

      INTEGER       IP
      CHARACTER*(*) TXT
      CHARACTER*(*) V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE (LOG_BUF, '(I3,1X,2A,A)') IP, TXT, '#<60>#=', V
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_XBS_1STR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_1STR

C************************************************************************
C Sommaire: SV2D_XBS_PRNPRGL
C
C Description:
C     IMPRESSION DE PROPRIETES GLOBALES
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRNPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRNPRGL
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER I, ICW
      CHARACTER*(128) BUF
      REAL*8, POINTER :: VPRGL(:)
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      TYPE (SV2D_IPRG_T),POINTER :: IPRG
      
      INTEGER SV2D_XBS_1RE8
      INTEGER SV2D_XBS_1INT
C-----------------------------------------------------------------------
      INTEGER P_1RE8, P_1INT
      CHARACTER*(64) T
      P_1RE8(I,T)=SV2D_XBS_1RE8(I,T(1:SP_STRN_LEN(T)),VPRGL(I))
      P_1INT(I,T)=SV2D_XBS_1INT(I,T(1:SP_STRN_LEN(T)),NINT(VPRGL(I)))
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA
      IPRG => SELF%IPRG
      VPRGL=> EDTA%VPRGL

C---     Imprime l'entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_LUES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Groupe des propriétés physiques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = IPRG%GRAVITE
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_GRAVITE')
      I = IPRG%LATITUDE
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_LATITUDE')
      I = IPRG%FCT_CW_VENT
      IF (I .GT. 0) THEN
         ICW = NINT(VPRGL(I))
         T = SP_HDRO_CWNAME(ABS(ICW))
         IF (ICW .LT. 0) T = T(1:SP_STRN_LEN(T)) // '; MSG_VENT_RELATIF'
         IF (ICW .GT. 0) T = T(1:SP_STRN_LEN(T)) // '; MSG_VENT_ABSOLU'
         WRITE(BUF,'(I3,3A)') ICW, ' (', T(1:SP_STRN_LEN(T)), ')'
         IERR = SV2D_XBS_1STR(I, 'MSG_FCT_CW_VENT', BUF)
      ENDIF
      I = IPRG%VISCO_CST
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_VISCOSITE_MOLECULAIRE')
      I = IPRG%VISCO_LM
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_LONGUEUR_MELANGE')
      I = IPRG%VISCO_SMGO
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_SMAGORINSKY')
      I = IPRG%VISCO_BINF
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_BORNE_INF_VISCOSITE')
      I = IPRG%VISCO_BSUP
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_BORNE_SUP_VISCOSITE')

C---     Groupe des propriétés du découvrement
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = IPRG%DECOU_HTRG
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_PROF_TRIG')
      I = IPRG%DECOU_HMIN
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_PROF_MIN')
      I = IPRG%DECOU_PENA_H
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_PENA_H')
      I = IPRG%DECOU_PENA_Q
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_PENA_Q')
      I = IPRG%DECOU_MAN
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_MAN')
      I = IPRG%DECOU_UMAX
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_UMAX')
      I = IPRG%DECOU_PORO
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_PORO')
      I = IPRG%DECOU_AMORT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_AMORT')
      I = IPRG%DECOU_CON_FACT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_COEF_CONVECTION')
      I = IPRG%DECOU_GRA_FACT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_COEF_GRAVITE')
      I = IPRG%DECOU_DIF_NU
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_NU_DIFFUSION')
      I = IPRG%DECOU_DRC_NU
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_DECOUVREMENT_NU_DARCY')

C---     Groupe des propriétés de stabilisation
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = IPRG%STABI_PECLET
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_PECLET')
      I = IPRG%STABI_AMORT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_AMORTISSEMENT')
      I = IPRG%STABI_DARCY
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_SURFACE_LIBRE_DARCY')
      I = IPRG%STABI_LAPIDUS
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_SURFACE_LIBRE_LAPIDUS')

C---     Groupe des propriétés d'activation-désactivation des termes
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = IPRG%CMULT_CON
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_CONVECTION')
      I = IPRG%CMULT_GRA
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_GRAVITE')
      I = IPRG%CMULT_PDYN
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PDYN')
      I = IPRG%CMULT_MAN
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_FROTTEMENT_FOND')
      I = IPRG%CMULT_VENT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_FROTTEMENT_VENT')
      I = IPRG%CMULT_INTGCTR
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_INTGRL_CONTOUR')

C---     Groupe des propriétés numériques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PARAM_NUMERIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = IPRG%PNUMR_PENALITE
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PENALITE')
      I = IPRG%PNUMR_DELPRT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PERTURBATION_KT')
      I = IPRG%PNUMR_DELMIN
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PERTURBATION_KT_MIN')
      I = IPRG%PNUMR_OMEGAKT
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_RELAXATION_KT')
      I = IPRG%PNUMR_PRTPREL
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PERTURB_PREL')
      I = IPRG%PNUMR_PRTPRNO
      IF (I .GT. 0) IERR = P_1RE8(I,'MSG_COEF_PERTURB_PRNO')

      CALL LOG_DECIND()

C---     Imprime l'entête
      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_PRGL_ST_VENANT_CALCULEES:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()
      
      CALL LOG_DECIND()

      SV2D_XBS_PRNPRGL = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRNPRGL

      END SUBMODULE SV2D_XBS_PRNPRGL_M
