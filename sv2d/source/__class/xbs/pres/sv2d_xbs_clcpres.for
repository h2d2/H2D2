C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_clcpres.for,v 1.23 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_CLCPRES
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_CLCPRES_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_CBS_CLCPRESE
C
C Description:
C     Calcul des propriétés élémentaires de surface sur un élément
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On calcule les contraintes sur les 3 sous-éléments externes du T6L
C     avant d'assembler uniquement les deux de l'arête.
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_CLCPRES_1E(VPRES,
     &                                     VDJV,
     &                                     VPRNV,
     &                                     VPREV,
     &                                     VDLEV,
     &                                     SV2D_IPRN,
     &                                     SV2D_XPRG,
     &                                     ICT)

      REAL*8, INTENT(INOUT) :: VPRES(:,:)
      REAL*8, INTENT(IN) :: VDJV (:)
      REAL*8, INTENT(IN) :: VPRNV(:,:)
      REAL*8, INTENT(IN) :: VPREV(:,:)
      REAL*8, INTENT(IN) :: VDLEV(:,:)
      TYPE(SV2D_IPRN_T), INTENT(IN) :: SV2D_IPRN
      TYPE(SV2D_XPRG_T), INTENT(IN) :: SV2D_XPRG
      INTEGER ICT

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IET3_1, IET3_2

      REAL*8  VKX, VEX, VKY, VEY
      REAL*8  UN_DT3
      REAL*8  U1, V1, U2, V2, U3, V3, U4, V4, U5, V5, U6, V6
      REAL*8  UX1, UX2, UX3, UY1, UY2, UY3
      REAL*8  VX1, VX2, VX3, VY1, VY2, VY3
      REAL*8  VIS1, VIS2, VIS3
      REAL*8  TAUXX(3), TAUXY(3), TAUYY(3)

      INTEGER, PARAMETER, DIMENSION(2,3) :: KT3 =   ! 2 sous-elem T3, 3 cotés
     &                 RESHAPE((/1, 2,    
     &                           2, 3, 
     &                           3, 1/), (/2, 3/))
C---     Connectivités du T6
      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6
C-----------------------------------------------------------------------

C---     Métriques des T3 et T6L
      VKX    = UN_2*VDJV(1)
      VEX    = UN_2*VDJV(2)
      VKY    = UN_2*VDJV(3)
      VEY    = UN_2*VDJV(4)
      UN_DT3 = UN_4/VDJV(5)

C---     Variables nodales
      U1 = VPRNV(SV2D_IPRN%U,NO1)
      V1 = VPRNV(SV2D_IPRN%V,NO1)
      U2 = VPRNV(SV2D_IPRN%U,NO2)
      V2 = VPRNV(SV2D_IPRN%V,NO2)
      U3 = VPRNV(SV2D_IPRN%U,NO3)
      V3 = VPRNV(SV2D_IPRN%V,NO3)
      U4 = VPRNV(SV2D_IPRN%U,NO4)
      V4 = VPRNV(SV2D_IPRN%V,NO4)
      U5 = VPRNV(SV2D_IPRN%U,NO5)
      V5 = VPRNV(SV2D_IPRN%V,NO5)
      U6 = VPRNV(SV2D_IPRN%U,NO6)
      V6 = VPRNV(SV2D_IPRN%V,NO6)

C---     Dérivée en X de U sur les 3 T3 externes
      UX1 = VKX*(U2-U1)+VEX*(U6-U1)
      UX2 = VKX*(U3-U2)+VEX*(U4-U2)
      UX3 = VKX*(U4-U6)+VEX*(U5-U6)

C---     Dérivée en Y de U sur les 3 T3 externes
      UY1 = VKY*(U2-U1)+VEY*(U6-U1)
      UY2 = VKY*(U3-U2)+VEY*(U4-U2)
      UY3 = VKY*(U4-U6)+VEY*(U5-U6)

C---     Dérivée en X de V sur les 3 T3 externes
      VX1 = VKX*(V2-V1)+VEX*(V6-V1)
      VX2 = VKX*(V3-V2)+VEX*(V4-V2)
      VX3 = VKX*(V4-V6)+VEX*(V5-V6)

C---     Dérivée en Y de V sur les 3 T3 externes
      VY1 = VKY*(V2-V1)+VEY*(V6-V1)
      VY2 = VKY*(V3-V2)+VEY*(V4-V2)
      VY3 = VKY*(V4-V6)+VEY*(V5-V6)

C---        Viscosité
      VIS1 = VPREV(1,1)  ! visco physique = 1
      VIS2 = VPREV(1,2)  ! visco totale   = 2
      VIS3 = VPREV(1,3)

C---     TAUXX
      TAUXX(1) = UX1*VIS1*DEUX
      TAUXX(2) = UX2*VIS2*DEUX
      TAUXX(3) = UX3*VIS3*DEUX

C---     TAUXY
      TAUXY(1) = (UY1 + VX1)*VIS1
      TAUXY(2) = (UY2 + VX2)*VIS2
      TAUXY(3) = (UY3 + VX3)*VIS3

C---     TAUYY
      TAUYY(1) = VY1*VIS1*DEUX
      TAUYY(2) = VY2*VIS2*DEUX
      TAUYY(3) = VY3*VIS3*DEUX

C---     Sous-éléments
      IET3_1 = KT3(1, ICT)
      IET3_2 = KT3(2, ICT)

C---     Contraintes sur les deux sous-éléments
      VPRES(1,1) = TAUXX(IET3_1)*UN_DT3
      VPRES(2,1) = TAUYY(IET3_1)*UN_DT3
      VPRES(3,1) = TAUXY(IET3_1)*UN_DT3
      VPRES(1,2) = TAUXX(IET3_2)*UN_DT3
      VPRES(2,2) = TAUYY(IET3_2)*UN_DT3
      VPRES(3,2) = TAUXY(IET3_2)*UN_DT3

      SV2D_XBS_CLCPRES_1E = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCPRES_1E

C************************************************************************
C Sommaire:  SV2D_XBS_CLCPRES
C
C Description:
C     Calcul des propriétés élémentaires des éléments de surface
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On calcule les contraintes sur les 3 sous-éléments externes du T6L
C     avant d'assembler uniquement les deux de l'arête.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_CLCPRES(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_CLCPRES
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IES, IEV, ICT
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
      REAL*8, POINTER :: VDLE  (:, :)
      REAL*8, POINTER :: VPRN  (:, :)
      INTEGER,POINTER :: KNE   (:)

C---     Tables locales
      INTEGER, PARAMETER :: NNEL_LCL =  6
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8,  TARGET :: VDLE_LCL(NDLN_LCL * NNEL_LCL)
      REAL*8,  TARGET :: VPRN_LCL(NPRN_LCL * NNEL_LCL)
      INTEGER, TARGET :: KNE_LCL (NNEL_LCL)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IES, IEV, ICT)
!$omp& private(SELF, GDTA, EDTA)
!$omp& private(VDLE_LCL, VDLE)
!$omp& private(VPRN_LCL, VPRN)
!$omp& private(KNE_LCL, KNE)

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Reshape the arrays
      VDLE(1:EDTA%NDLN, 1:GDTA%NNELV) => VDLE_LCL
      VPRN(1:EDTA%NPRNO,1:GDTA%NNELV) => VPRN_LCL
      KNE (1:GDTA%NNELV) => KNE_LCL

C----- Boucle sur les éléments de surface
C      ==================================
!$omp  do
      DO 10 IES=1,GDTA%NELLS

         IEV = GDTA%KNGS(4,IES)
         ICT = GDTA%KNGS(5,IES)

C---        Connectivités du T6
         KNE(:) = GDTA%KNGV(:,IEV)
         VDLE (:,:) = EDTA%VDLG (:,KNE(:))
         VPRN (:,:) = EDTA%VPRNO(:,KNE(:))
         
         IERR = SV2D_XBS_CLCPRES_1E(EDTA%VPRES(1:,1:,IES),
     &                              GDTA%VDJV(1:,IEV),
     &                              VPRN,
     &                              EDTA%VPREV(1:,1:,IEV),
     &                              VDLE,
     &                              SELF%IPRN,
     &                              SELF%XPRG,
     &                              ICT)
         
10    CONTINUE
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      SV2D_XBS_CLCPRES = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCPRES

C************************************************************************
C Sommaire:  SV2D_XBS_CLCPRESE
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume pour
C     un élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     A cause de l'appel via les functors, ce ne sont pas des pointeurs F90
C     qui sont passé. Il faut donc un reshape.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_CLCPRESE(SELF,
     &                                          ICT, 
     &                                          VDJEV_F, 
     &                                          VPRNV_F, 
     &                                          VPREV_F,
     &                                          VDLEV_F, 
     &                                          VPRES_F)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_CLCPRESE
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: ICT
      REAL*8, TARGET :: VDJEV_F(*) ! cf note
      REAL*8, TARGET :: VPRNV_F(*) ! cf note
      REAL*8, TARGET :: VPREV_F(*) ! cf note
      REAL*8, TARGET :: VDLEV_F(*) ! cf note
      REAL*8, TARGET :: VPRES_F(*) ! cf note

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NPRNE
      REAL*8, POINTER :: VDJEV(:)
      REAL*8, POINTER :: VDLEV(:,:)
      REAL*8, POINTER :: VPRNV(:,:)
      REAL*8, POINTER :: VPREV(:,:)
      REAL*8, POINTER :: VPRES(:,:)
      TYPE (LM_GDTA_T),  POINTER :: GDTA
      TYPE (LM_EDTA_T),  POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Reshape the arrays
      NPRNE = EDTA%NPRNO * GDTA%NNELV
      VDJEV(1:GDTA%NDJV)  => VDJEV_F(1:GDTA%NDJV)
      VDLEV(1:EDTA%NDLN, 1:GDTA%NNELV) => VDLEV_F(1:EDTA%NDLEV)
      VPRNV(1:EDTA%NPRNO,1:GDTA%NNELV) => VPRNV_F(1:NPRNE)
      VPREV(1:EDTA%NPREV_D1, 1:EDTA%NPREV_D2) => VPREV_F(1:EDTA%NPREV)
      VPRES(1:EDTA%NPRES_D1, 1:EDTA%NPRES_D2) => VPRES_F(1:EDTA%NPRES)

C---     Calcule
      IERR = SV2D_XBS_CLCPRES_1E(VPRES,
     &                           VDJEV,
     &                           VPRNV,
     &                           VPREV,
     &                           VDLEV,
     &                           SELF%IPRN,
     &                           SELF%XPRG,
     &                           ICT)

      SV2D_XBS_CLCPRESE = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCPRESE

      END SUB MODULE SV2D_XBS_CLCPRES_M
