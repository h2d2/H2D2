C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier:
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_XBS
C         FTN (Sub)Module: SV2D_XBS_M
C            Public:
C               SUBROUTINE SV2D_XBS_CTR
C            Private:
C            
C            FTN Type: SV2D_XBS_T
C               Public:
C                  INTEGER SV2D_XBS_DTR
C               Private:
C                  INTEGER SV2D_XBS_INIPRMS
C
C************************************************************************

      MODULE SV2D_XBS_M

      USE LM_ELEM_M,   ONLY: LM_ELEM_T
      USE SV2D_XPRG_M, ONLY: SV2D_XPRG_T
      USE SV2D_IPRG_M, ONLY: SV2D_IPRG_T
      USE SV2D_IPRN_M, ONLY: SV2D_IPRN_T
      USE SV2D_XCL_M,  ONLY: SV2D_XCL_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---  La classe
      !========================================================================
      TYPE, ABSTRACT, EXTENDS(LM_ELEM_T) :: SV2D_XBS_T
         TYPE(SV2D_XPRG_T) :: XPRG
         TYPE(SV2D_IPRG_T) :: IPRG
         TYPE(SV2D_IPRN_T) :: IPRN
         TYPE(SV2D_XCL_T), POINTER :: CLIM
      CONTAINS
         ! ---  Méthodes virtuelles héritées
         PROCEDURE, PUBLIC :: DTR     => SV2D_XBS_DTR
         PROCEDURE, PUBLIC :: CLCF    => SV2D_XBS_ASMF_R
         PROCEDURE, PUBLIC :: CLCK    => SV2D_XBS_ASMK_R
         PROCEDURE, PUBLIC :: CLCKT   => SV2D_XBS_ASMKT_R
         PROCEDURE, PUBLIC :: CLCKU   => SV2D_XBS_ASMKU_R
         PROCEDURE, PUBLIC :: CLCM    => SV2D_XBS_ASMM
         PROCEDURE, PUBLIC :: CLCMU   => SV2D_XBS_ASMMU
         PROCEDURE, PUBLIC :: CLCRES  => SV2D_XBS_ASMRES
         PROCEDURE, PUBLIC :: CLCCLIM => SV2D_XBS_CLCCLIM
         PROCEDURE, PUBLIC :: CLCDLIB => SV2D_XBS_CLCDLIB
         PROCEDURE, PUBLIC :: CLCPRES => SV2D_XBS_CLCPRES
         PROCEDURE, PUBLIC :: CLCPREV => SV2D_XBS_CLCPREV
         !PROCEDURE, PUBLIC :: CLCPRNO => SV2D_XBS_CLCPRNO
         PROCEDURE, PUBLIC :: PRCCLIM => SV2D_XBS_PRCCLIM
         PROCEDURE, PUBLIC :: PRCDLIB => SV2D_XBS_PRCDLIB
         PROCEDURE, PUBLIC :: PRCPRES => SV2D_XBS_PRCPRES
         PROCEDURE, PUBLIC :: PRCPREV => SV2D_XBS_PRCPREV
         PROCEDURE, PUBLIC :: PRCPRGL => SV2D_XBS_PRCPRGL
         PROCEDURE, PUBLIC :: PRCPRLL => SV2D_XBS_PRCPRLL
         PROCEDURE, PUBLIC :: PRCPRNO => SV2D_XBS_PRCPRNO
         PROCEDURE, PUBLIC :: PRCSOLC => SV2D_XBS_PRCSOLC
         PROCEDURE, PUBLIC :: PRCSOLR => SV2D_XBS_PRCSOLR
         PROCEDURE, PUBLIC :: HLPCLIM => SV2D_XBS_HLPCLIM
         PROCEDURE, PUBLIC :: HLPPRGL => SV2D_XBS_HLPPRGL
         PROCEDURE, PUBLIC :: HLPPRNO => SV2D_XBS_HLPPRNO
         PROCEDURE, PUBLIC :: PRNCLIM => SV2D_XBS_PRNCLIM
         PROCEDURE, PUBLIC :: PRNPRGL => SV2D_XBS_PRNPRGL
         PROCEDURE, PUBLIC :: PRNPRNO => SV2D_XBS_PRNPRNO
         PROCEDURE, PUBLIC :: PSCPRNO => SV2D_XBS_PSCPRNO
         PROCEDURE, PUBLIC :: PSLCLIM => SV2D_XBS_PSLCLIM
         PROCEDURE, PUBLIC :: PSLDLIB => SV2D_XBS_PSLDLIB
         PROCEDURE, PUBLIC :: PSLPREV => SV2D_XBS_PSLPREV
         PROCEDURE, PUBLIC :: PSLPRGL => SV2D_XBS_PSLPRGL
         PROCEDURE, PUBLIC :: PSLPRNO => SV2D_XBS_PSLPRNO
         PROCEDURE, PUBLIC :: PSLSOLC => SV2D_XBS_PSLSOLC
         PROCEDURE, PUBLIC :: PSLSOLR => SV2D_XBS_PSLSOLR
         !PROCEDURE(PST_GEN),     PUBLIC, DEFERRED :: PST

         ! ---  Méthodes virtuelles SV2D
         PROCEDURE, PUBLIC :: PSLPRGL_CHK => SV2D_XBS_PSLPRGL_CHK
         PROCEDURE, PUBLIC :: PSLPRGL_IPG => SV2D_XBS_PSLPRGL_IPG
         PROCEDURE, PUBLIC :: CLCPREVE    => SV2D_XBS_CLCPREVE
         PROCEDURE, PUBLIC :: CLCPRESE    => SV2D_XBS_CLCPRESE
         PROCEDURE(CLCPRNEV_GEN), PUBLIC, DEFERRED :: CLCPRNEV
         PROCEDURE(CLCPRNES_GEN), PUBLIC, DEFERRED :: CLCPRNES

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: INIPRMS => SV2D_XBS_INIPRMS
      END TYPE SV2D_XBS_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: SV2D_XBS_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: SV2D_XBS_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Interface abstraite
      !========================================================================
      ABSTRACT INTERFACE
         INTEGER FUNCTION CLCPRNEV_GEN(SELF, VDLE, VPRN)
            IMPORT :: SV2D_XBS_T
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION CLCPRNEV_GEN

         INTEGER FUNCTION CLCPRNES_GEN(SELF, VDLE, VPRN)
            IMPORT :: SV2D_XBS_T
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT):: VDLE(:,:)
            REAL*8, INTENT(OUT)  :: VPRN(:,:)
         END FUNCTION CLCPRNES_GEN

      END INTERFACE

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         ! ---  Fonction ASM
         MODULE INTEGER FUNCTION SV2D_XBS_ASMF_R(SELF, VRES)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION SV2D_XBS_ASMF_R

         MODULE INTEGER FUNCTION SV2D_XBS_ASMK_R(SELF, HMTRX, F_ASM)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION SV2D_XBS_ASMK_R

         MODULE INTEGER FUNCTION SV2D_XBS_ASMKT_R(SELF, HMTRX, F_ASM)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION SV2D_XBS_ASMKT_R

         MODULE INTEGER FUNCTION SV2D_XBS_ASMKU_R(SELF, VRES)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION SV2D_XBS_ASMKU_R

         MODULE INTEGER FUNCTION SV2D_XBS_ASMM(SELF, HMTRX, F_ASM)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION SV2D_XBS_ASMM

         MODULE INTEGER FUNCTION SV2D_XBS_ASMMU(SELF, VRES)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION SV2D_XBS_ASMMU

         MODULE INTEGER FUNCTION SV2D_XBS_ASMRES(SELF, VRES)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION SV2D_XBS_ASMRES

         ! ---  Fonction CLC
         MODULE INTEGER FUNCTION SV2D_XBS_CLCCLIM(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_CLCCLIM

         MODULE INTEGER FUNCTION SV2D_XBS_CLCDLIB(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_CLCDLIB

         MODULE INTEGER FUNCTION SV2D_XBS_CLCPRES(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_CLCPRES

         MODULE INTEGER FUNCTION SV2D_XBS_CLCPREV(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_CLCPREV

         !MODULE INTEGER FUNCTION SV2D_XBS_CLCPRNO(SELF)
         !   CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         !END FUNCTION SV2D_XBS_CLCPRNO

         ! ---  Fonction HLP
         MODULE INTEGER FUNCTION SV2D_XBS_HLPCLIM(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_HLPCLIM

         MODULE INTEGER FUNCTION SV2D_XBS_HLPPRGL(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_HLPPRGL

         MODULE INTEGER FUNCTION SV2D_XBS_HLPPRNO(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_HLPPRNO

         ! ---  Fonction PRC
         MODULE INTEGER FUNCTION SV2D_XBS_PRCCLIM(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCCLIM

         MODULE INTEGER FUNCTION SV2D_XBS_PRCDLIB(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCDLIB

         MODULE INTEGER FUNCTION SV2D_XBS_PRCPRES(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCPRES

         MODULE INTEGER FUNCTION SV2D_XBS_PRCPREV(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCPREV

         MODULE INTEGER FUNCTION SV2D_XBS_PRCPRGL(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCPRGL

         MODULE INTEGER FUNCTION SV2D_XBS_PRCPRLL(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCPRLL

         MODULE INTEGER FUNCTION SV2D_XBS_PRCPRNO(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCPRNO

         MODULE INTEGER FUNCTION SV2D_XBS_PRCSOLC(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCSOLC

         MODULE INTEGER FUNCTION SV2D_XBS_PRCSOLR(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRCSOLR

         ! ---  Fonction PRN
         MODULE INTEGER FUNCTION SV2D_XBS_PRNCLIM(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRNCLIM

         MODULE INTEGER FUNCTION SV2D_XBS_PRNPRGL(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRNPRGL

         MODULE INTEGER FUNCTION SV2D_XBS_PRNPRNO(SELF)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
         END FUNCTION SV2D_XBS_PRNPRNO

         ! ---  Fonction PSC
         MODULE INTEGER FUNCTION SV2D_XBS_PSCPRNO(SELF, MODIF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
            LOGICAL, INTENT(OUT) :: MODIF
         END FUNCTION SV2D_XBS_PSCPRNO

         ! ---  Fonction PSL
         MODULE INTEGER FUNCTION SV2D_XBS_PSLCLIM(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLCLIM

         MODULE INTEGER FUNCTION SV2D_XBS_PSLDLIB(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLDLIB

         MODULE INTEGER FUNCTION SV2D_XBS_PSLPREV(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLPREV

         MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLPRGL

         MODULE INTEGER FUNCTION SV2D_XBS_PSLPRNO(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLPRNO

         MODULE INTEGER FUNCTION SV2D_XBS_PSLSOLC(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLSOLC

         MODULE INTEGER FUNCTION SV2D_XBS_PSLSOLR(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLSOLR

         ! ---  Fonction élémentaires
         MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL_CHK(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLPRGL_CHK
         
         MODULE INTEGER FUNCTION SV2D_XBS_PSLPRGL_IPG(SELF)
            CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION SV2D_XBS_PSLPRGL_IPG

         MODULE INTEGER
     &   FUNCTION SV2D_XBS_CLCPREVE(SELF,
     &                              VDJE,
     &                              VPRN,
     &                              VDLE,
     &                              VPRE)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(IN) :: VDJE(:)
            REAL*8, INTENT(IN) :: VPRN(:,:)
            REAL*8, INTENT(IN) :: VDLE(:,:)
            REAL*8, INTENT(OUT):: VPRE(:,:)
         END FUNCTION SV2D_XBS_CLCPREVE

         MODULE INTEGER
     &   FUNCTION SV2D_XBS_CLCPRESE(SELF,
     &                              ICT,
     &                              VDJEV,
     &                              VPRNV,
     &                              VPREV,
     &                              VDLEV,
     &                              VPRES)
            CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: ICT
            REAL*8, INTENT(IN) :: VDJEV(:,:)
            REAL*8, INTENT(IN) :: VPRNV(:,:)
            REAL*8, INTENT(IN) :: VPREV(:,:)
            REAL*8, INTENT(IN) :: VDLEV(:,:)
            REAL*8, INTENT(IN) :: VPRES(:,:)
         END FUNCTION SV2D_XBS_CLCPRESE

      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est abstraite, OKID doit exister.
C     OKID est passé par pointeur pour permettre la précondition
C************************************************************************
      FUNCTION SV2D_XBS_CTR(OKID) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_CTR
CDEC$ ENDIF

      USE LM_GOT6L_M, ONLY: LM_GOT6L_T, LM_GOT6L_CTR
      USE LM_ELEM_M,  ONLY: LM_ELEM_CTR

      CLASS(SV2D_XBS_T), INTENT(INOUT), POINTER :: OKID

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      CLASS(SV2D_XBS_T), POINTER :: SELF
      CLASS(LM_ELEM_T),  POINTER :: OPRNT
      TYPE(LM_GOT6L_T),  POINTER :: OGEOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(ASSOCIATED(OKID))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cast le pointeur
      SELF => OKID

C---     Construis les parents
      IF (ERR_GOOD()) OGEOM => LM_GOT6L_CTR()
      IF (ERR_GOOD()) OPRNT => LM_ELEM_CTR (OKID, OGEOM)

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      RETURN
      END FUNCTION SV2D_XBS_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_DTR
CDEC$ ENDIF

      USE LM_ELEM_M, ONLY: LM_ELEM_DTR
      USE LM_GEOM_M, ONLY: DEL

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Détruis les parents
      IF (ERR_GOOD()) IERR = DEL(SELF%OGEO)
      IF (ERR_GOOD()) IERR = LM_ELEM_DTR(SELF)

      SV2D_XBS_DTR = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode SV2D_XBS_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_INIPRMS(SELF)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA => SELF%EDTA

C---     Initialise les paramètres variables sujets à changement chez l'élément héritier
      EDTA%NPRGLL = 31                 ! NB DE PROPRIÉTÉS GLOBALES LUES
      EDTA%NPRGL  = EDTA%NPRGLL        ! NB DE PROPRIÉTÉS GLOBALES
      EDTA%NPRGX  = 38                 ! NB de propriétés équivalentes à SV2D_XPRG_T
      EDTA%NPRNO  = 16                 ! NB DE PROPRIÉTÉS NODALES
      EDTA%NPRNOL =  6                 ! NB DE PROPRIÉTÉS NODALES LUES
      EDTA%NPREV_D1 = 2                ! NB DE PRE par sous-élément
      EDTA%NPREV_D2 = 4
      EDTA%NPREV  =  2*4               ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
      EDTA%NPREVL =  0                 ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME LUES
      EDTA%NPRES_D1 = 3                ! NB DE PRE par sous-élément
      EDTA%NPRES_D2 = 2
      EDTA%NPRES  =  3*2               ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE
      EDTA%NSOLC  =  3                 ! NB DE SOLLICITATIONS CONCENTRÉES
      EDTA%NSOLCL =  3                 ! NB DE SOLLICITATIONS CONCENTRÉES LUES
      EDTA%NSOLR  =  3                 ! NB DE SOLLICITATIONS RÉPARTIES
      EDTA%NSOLRL =  3                 ! NB DE SOLLICITATIONS RÉPARTIES LUES
      EDTA%NDLN   =  3                 ! NB DE DDL PAR NOEUD
      EDTA%NDLEV  = 18                 ! NB DE DDL PAR ELEMENT DE VOLUME
      EDTA%NDLES  =  9                 ! NB DE DDL PAR ELEMENT DE SURFACE

!      EDTA%ASURFACE = .TRUE.
!      EDTA%ESTLIN   = .FALSE.

      SV2D_XBS_INIPRMS = ERR_TYP()
      RETURN
      END

      END MODULE SV2D_XBS_M

