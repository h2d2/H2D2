C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PSLDLIB
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PSLDLIB_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Fichier:  SV2D_XBS_PSLDLIB
C
C Description:
C     La sous-routine SV2D_XBS_PSLDLIB fait tout le traitement
C     post-lecture des degrés de liberté.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSLDLIB(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PSLDLIB
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XBS_PSLDLIB = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSLDLIB

      END SUBMODULE SV2D_XBS_PSLDLIB_M
