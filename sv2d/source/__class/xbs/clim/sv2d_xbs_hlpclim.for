C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_XBS_HLPCLIM
C   Private:
C     INTEGER SV2D_XBS_HLP1
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_HLPCLIM_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: SV2D_XBS_HLPCLIM
C
C Description:
C     Aide sur les conditions limites
C     EQUATION : SAINT-VENANT CONSERVATIF 2D
C     ELEMENT  : T6L
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_HLPCLIM(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_HLPCLIM
CDEC$ ENDIF

      USE SV2D_XCL_M, ONLY: SV2D_XCL_HLP
      
      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF
      
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_CLIM_ST_VENANT:')
      CALL LOG_INCIND()

      IERR = SV2D_XCL_HLP()

      CALL LOG_DECIND()

      SV2D_XBS_HLPCLIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_HLPCLIM

      END SUBMODULE SV2D_XBS_HLPCLIM_M
      