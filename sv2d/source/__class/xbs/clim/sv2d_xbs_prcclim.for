C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_prcclim.for,v 1.40 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCCLIM
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCCLIM_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XBS_PRCCLIM
C
C Description:
C     Traitement PRé-Calcul des Conditions LIMites
C
C Entrée:
C     HOBJ
C
C Sortie:
C     IERR
C     EDTA%KDIMP     Pour chaque DDL, contient les codes de CL
C     EDTA%VDIMP     Pour chaque DDL, contient la valeur imposée par la cl
C     EDTA%KEIMP     Pour chaque ELS, contient les codes des CL
C
C Notes:
C     Le test n'est pas bon. Les noeuds milieux en H sont flagués et donc
C     H sera toujours considéré comme ayant au moins un noeud imposé.
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCCLIM(SELF)

      USE SV2D_XCL_M, ONLY: SV2D_XCL_CTR
      
      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'eacdcl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER SV2D_XBS_PRCCLIM_VDIMP
      INTEGER SV2D_XBS_PRCCLIM_EC

      INTEGER IERR
      INTEGER IC, IE, ID, IN
      INTEGER NEQ, NCLT, NCLTI
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      INTEGER, POINTER :: KNGV (:,:)
      INTEGER, POINTER :: KDIMP(:,:)
      INTEGER, POINTER :: KEIMP(:)
      REAL*8,  POINTER :: VDIMP(:,:)
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      KNGV  => GDTA%KNGV
      KDIMP => EDTA%KDIMP
      KEIMP => EDTA%KEIMP
      VDIMP => EDTA%VDIMP

C---     Initialise
      NEQ  = EDTA%NDLL
      NCLT = 0

C---     Reset les C.L.
      KDIMP(:,:) = 0
      KEIMP(:)   = 0
      VDIMP(:,:) = ZERO

C---     Flag les DDL de H sur les noeuds milieux
!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IN)
      DO IC=1,GDTA%NELCOL
!$omp  do
      DO IE=GDTA%KELCOL(1,IC),GDTA%KELCOL(2,IC)
         IN = KNGV(2,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
         IN = KNGV(4,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
         IN = KNGV(6,IE)
         KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_PHANTOME)
      ENDDO
!$omp  end do
      ENDDO
!$omp end parallel

C---     Construis les objets C.L.
      IF (ERR_GOOD() .AND. .NOT. ASSOCIATED(SELF%CLIM)) THEN
         SELF%CLIM => SV2D_XCL_CTR()
         IERR = SELF%CLIM%INI(SELF)
      ENDIF
      
C---     Assigne les codes de condition
      IF (ERR_GOOD()) IERR = SELF%CLIM%COD(SELF)

C---     Pré-calcul
      IF (ERR_GOOD()) IERR = SELF%CLIM%PRC(SELF)

C---     Contrôle les C.L. pour chaque type de DDL
C---     S'assure que chaque DDL a au moins une CL (cf. note)
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         DO ID=1,EDTA%NDLN
            NCLTI = 0
            DO IN=1, GDTA%NNL
               IF (KDIMP(ID,IN) .NE. 0) NCLTI = NCLTI + 1
            ENDDO
            IF (NCLTI .EQ. 0) THEN
               WRITE(LOG_BUF,'(2A,I12)') 'MSG_DDL_SANS_CLIM',': ',ID
               CALL LOG_ECRIS(LOG_BUF)
            ENDIF
         ENDDO
      ENDIF

C---     Compte le nombre de C.L.
      NCLT = 0
      DO IN = 1, GDTA%NNL
         DO ID=1,EDTA%NDLN
            IF (BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)) THEN
               NCLT = NCLT + 1
            ENDIF
         ENDDO
      ENDDO

C---     Met à jour NEQ
      NEQ = NEQ - NCLT

C---     Contrôle NEQ
      IF (NEQ. LE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_CLIM_NBR_EQUATIONS',': ',NEQ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_XBS_PRCCLIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRCCLIM

      END SUBMODULE SV2D_XBS_PRCCLIM_M
      