C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_clcclim.for,v 1.35 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_CLCCLIM
C   Private:
C     INTEGER SV2D_XBS_CLCCLIM_S
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_CLCCLIM_M
            
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XBS_CLCCLIM_S
C
C Description:
C     Calcul des conditions aux limites sur les éléments de surface
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SV2D_XBS_CLCCLIM_S(SELF)

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'eacdcl.fi'  ! Pour les types de CL
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cnst.fi'

      INTEGER IERR

      REAL*8 VNX, VNY
      REAL*8 QX1, QY1, QX2, QY2
      REAL*8 QN1, QN2
      INTEGER IEP, ID, IN, NP1, NP2, IE
      LOGICAL ESTCAUCHY
      LOGICAL ESTOUVERT
      LOGICAL ESTENTRANT
      CHARACTER*(48) TYPCL
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      INTEGER, POINTER :: KNGV (:,:)
      INTEGER, POINTER :: KNGS (:,:)
      INTEGER, POINTER :: KDIMP(:,:)
      INTEGER, POINTER :: KEIMP(:)
      REAL*8,  POINTER :: VDJS (:,:)
      REAL*8,  POINTER :: VDIMP(:,:)
      REAL*8,  POINTER :: VDLG (:,:)
C-----------------------------------------------------------------------
      LOGICAL DDL_ESTCAUCHY
      LOGICAL DDL_ESTOUVERT
      DDL_ESTCAUCHY(ID,IN) = BTEST(EDTA%KDIMP(ID,IN), EA_TPCL_CAUCHY)
      DDL_ESTOUVERT(ID,IN) = BTEST(EDTA%KDIMP(ID,IN), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.bc.clc'
      
C---     Récupère les données
      GDTA  => SELF%GDTA
      EDTA  => SELF%EDTA
      KNGV  => GDTA%KNGV
      KNGS  => GDTA%KNGS
      VDJS  => GDTA%VDJS
      KDIMP => EDTA%KDIMP
      KEIMP => EDTA%KEIMP
      VDLG  => EDTA%VDLG

      EDTA%KEIMP(:) = 0

C---     Boucle sur les éléments de surface
C        ==================================
      DO IEP=1,GDTA%NELLS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)

C---        Métriques
         VNX =  VDJS(2,IEP)
         VNY = -VDJS(2,IEP)

C---        Débits normaux
         QX1 = VDLG(1,NP1)
         QY1 = VDLG(2,NP1)
         QX2 = VDLG(1,NP2)
         QY2 = VDLG(2,NP2)
         QN1 = QX1*VNX + QY1*VNY
         QN2 = QX2*VNX + QY2*VNY

C---        Les limites de Cauchy
         ESTCAUCHY  = (DDL_ESTCAUCHY(3,NP1) .AND. DDL_ESTCAUCHY(3,NP2))
         IF (ESTCAUCHY) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_CAUCHY)
            ESTENTRANT = (QN1 .LE. ZERO .OR. QN2 .LE. ZERO)
            IF (ESTENTRANT) THEN
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
            ELSE
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_SORTANT)
            ENDIF
         ENDIF

C---        Les limites "OUVERT"
         ESTOUVERT  = (DDL_ESTOUVERT(3,NP1) .AND. DDL_ESTOUVERT(3,NP2))
         IF (ESTOUVERT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_OUVERT)
            ESTENTRANT = (QN1 .LT. ZERO .AND. QN2 .LT. ZERO)
            IF (ESTENTRANT) THEN
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_ENTRANT)
            ELSE
               KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_SORTANT)
            ENDIF
         ENDIF

      ENDDO

C---   IMPRESSION DES CONNECTIVITÉS DE LA PEAU DES FLUX DE
C      CAUCHY(ENTRE), OUVERT(NUL+SORT), DIRICHLET+NEUMAN(DÉFAUT)
      IF (LOG_REQNIVZ(LOG_ZNE) .GE. LOG_LVL_DEBUG) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,'(A)') 'MSG_TYPE_CLIM_ELEMENTS_NOEUDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         DO IEP=1,GDTA%NELLS

            IF     (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_SORTANT)) THEN
               IE = KNGS(3,IEP)
               WRITE(LOG_BUF,2050) IEP,NP1,NP2,
     &                          IE,KNGV(1,IE),KNGV(2,IE),KNGV(3,IE)
               CALL LOG_ECRIS(LOG_BUF)
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_CAUCHY_ENTRANT#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_ENTRANT#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IEP), EA_TPCL_SORTANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_SORTANT#<40>#'
            ELSE
               TYPCL = 'MSG_CL_DEFAUT#<40>#'
            ENDIF
            WRITE(LOG_BUF,'(A,2X,I9,3X,2(1X,I9))')
     &            TYPCL(1:SP_STRN_LEN(TYPCL)),
     &            IEP, KNGS(1,IEP), KNGS(2,IEP)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
         CALL LOG_DECIND()
      ENDIF

C---  IMPRESSION DE L'INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'TRAITEMENT DES ELEMENTS DE CONTOUR:'
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DE CAUCHY       =',
C     &               NELC
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. OUVERT          =',
C     &               NELS
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DIRICHLET-NEUMAN=',
C     &               GDTA%NELLS-NELC-NELS
C      WRITE(MP,1000)

C---------------------------------------------------------------
1000  FORMAT(/)
1001  FORMAT(15X,A)
1002  FORMAT(15X,A,I12)
2050  FORMAT(2X,'AVERTISSEMENT ---  APPLICATION D''UNE CONDITION DE',/,
     &       2X,'   NEUMAN PAR DEFAUT; LA CONDITION DE CAUCHY-ENTRANT',
     &          '   N''EST PAS APPLICABLE SUR UN FLUX SORTANT',/,
     &       2X,'ELEMENT DE CONTOUR',I12,/,
     &       2X,'NOEUDS DE CONTOUR ',2(1X,I12),/,
     &       2X,'ELEMENT PARENT',I12,/,
     &       2X,'CONNECTIVITE DU PARENT',3(1X,I12),/)
C---------------------------------------------------------------

      SV2D_XBS_CLCCLIM_S = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCCLIM_S

C************************************************************************
C Sommaire:  SV2D_XBS_CLCCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_CLCCLIM(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_CLCCLIM
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER  IERR
C-----------------------------------------------------------------------

C---     Contributions de éléments de surface
      IF (ERR_GOOD()) THEN
         IERR = SV2D_XBS_CLCCLIM_S(SELF)
      ENDIF

C---     Contributions des conditions limites
      IF (ERR_GOOD()) THEN
         IERR = SELF%CLIM%CLC(SELF)
      ENDIF

      SV2D_XBS_CLCCLIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_CLCCLIM

      END SUBMODULE SV2D_XBS_CLCCLIM_M
      