C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_pslclim.for,v 1.14 2013/09/09 15:00:21 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PSLCLIM
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PSLCLIM_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Fichier:  SV2D_XBS_PSLCLIM
C
C Description:
C     La sous-routine SV2D_XBS_PSLCLIM fait tout le traitement
C     post-lecture des conditions limites.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PSLCLIM(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PSLCLIM
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XBS_PSLCLIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PSLCLIM

      END SUBMODULE SV2D_XBS_PSLCLIM_M
