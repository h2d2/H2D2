C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_prnclim.for,v 1.12 2012/01/10 20:20:00 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRNCLIM
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRNCLIM_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:  SV2D_XBS_PRNCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRNCLIM(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRNCLIM
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      CALL LOG_INCIND()

      CALL LOG_ECRIS('DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:')
!!      CALL LOG_ECRIS('CODE C.L.=1 CONDITION DE DIRICHLET;')
!!      CALL LOG_ECRIS('CODE C.L.=2 CONDITION DE CAUCHY;')
!!      CALL LOG_ECRIS('CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.')

      CALL LOG_DECIND()

      SV2D_XBS_PRNCLIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRNCLIM

      END SUBMODULE SV2D_XBS_PRNCLIM_M
      