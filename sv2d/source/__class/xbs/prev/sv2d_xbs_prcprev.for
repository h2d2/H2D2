C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_prcprev.for,v 1.13 2012/01/10 20:20:00 secretyv Exp $
C
C Functions:
C   Public:
C     INTEGER SV2D_XBS_PRCPREV
C   Private:
C
C************************************************************************

      SUBMODULE(SV2D_XBS_M) SV2D_XBS_PRCPREV_M
            
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire : SV2D_XBS_PRCPREV
C
C Description:
C     Pré-traitement des propriétés élémentaires de volume
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION SV2D_XBS_PRCPREV(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_XBS_PRCPREV
CDEC$ ENDIF

      CLASS(SV2D_XBS_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_XBS_PRCPREV = ERR_TYP()
      RETURN
      END FUNCTION SV2D_XBS_PRCPREV

      END SUBMODULE SV2D_XBS_PRCPREV_M
