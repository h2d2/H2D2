C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_xbs.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
C
C Notes:
C
C Functions:
C
C************************************************************************

      MODULE SV2D_IPRN_M

C---     Indices dans PRNO
      TYPE :: SV2D_IPRN_T
         INTEGER Z
         INTEGER N
         INTEGER ICE_E
         INTEGER ICE_N
         INTEGER WND_X
         INTEGER WND_Y
         INTEGER U
         INTEGER V
         INTEGER H
         INTEGER COEFF_CNVT
         INTEGER COEFF_GRVT
         INTEGER COEFF_FROT
         INTEGER COEFF_DIFF
         INTEGER COEFF_PE
         INTEGER COEFF_DMPG
         INTEGER COEFF_VENT
         INTEGER COEFF_DRCY
         INTEGER COEFF_PORO
         INTEGER DECOU_PENA
      END TYPE SV2D_IPRN_T

      END MODULE SV2D_IPRN_M
