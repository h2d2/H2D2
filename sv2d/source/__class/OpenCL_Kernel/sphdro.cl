//************************************************************************
// --- Copyright (c) INRS 2003-2013
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SPHDRO_CL_ALLREADY_DEFINED
#define SPHDRO_CL_ALLREADY_DEFINED

#include "gpu_config.cl"

#define SP_HDRO_GRA  9.81e+00
#define SP_HDRO_HMIN 1.00e-04
#define SP_HDRO_VMIN 1.00e-08

//************************************************************************
// Sommaire: SP_HDRO_TEZDUYAR
//
// Description:
//     La fonctions <code>SP_HDRO_TEZDUYAR(...)</code> calcule le taille de
//     maille directionnelle sur l'élément de référence.
//
// Entrée:
//     REAL*8 V1         ! Projection normalisée de la vitesse sur le coté 2-1
//     REAL*8 V2         ! Projection normalisée de la vitesse sur le coté 3-1
//
// Sortie:
//     REAL*8 V1         ! Composante de la taille de maille sur le coté 2-1
//     REAL*8 V2         !
//
// Notes:
//     T.E. Tezduyar, Y. Osawa. Finite element stabilization parameters
//     computed from element matrices and vectors.
//     Comput. Methods Appl. Mech. Engrg. 190 (200) 411-430
//************************************************************************
double
sp_hdro_tezduyar(double v1, double v2)
{
   double rx = 0.0e0;
   double ry = 0.0e0;
   if (v1 > 0.0e0) {
      if (v2 > 0.0e0) {
         rx = v1 / (v1 + v2);
         ry = 1.0e0 - rx;
      }
      else {
         if (v1 > - v2) {
            rx = 1.0e0;
            ry = v2 / v1;
         }
         else {
            rx = -v1 / v2;
            ry = -1.0e0;
         }
      }
   }
   else {
      if (v2 < 0.0f) {
         rx = -v1 / (v1 + v2);
         ry = -1.0e0 - rx;
      }
      else {
         if (v2 > fabs(v1)) {
            rx = v1 / v2;
            ry = 1.0e0;
         }
         else {
            rx = -1.0e0;
            ry = -v2 / v1;
         }
      }
   }
   v1 = 0.5e0 * rx;
   v2 = 0.5e0 * ry;

  return 0.5e0 * sqrt(rx * rx + ry * ry);
}

//************************************************************************
// Sommaire: SP_HDRO_TMAILD
//
// Description:
//     La fonctions <code>SP_HDRO_TMAILD(...)</code> calcule le taille de
//     maille directionnelle.
//
// Entrée:
//     REAL*8 U          ! Composantes de la vitesse
//     REAL*8 V
//     REAL*8 H          ! Profondeur
//     REAL*8 T1X        ! Métriques tangentes
//     REAL*8 T1Y
//     REAL*8 T2X
//     REAL*8 T2Y
//
// Sortie:
//
// Notes:
//************************************************************************
double
sp_hdro_tmaild(
  double u,
  double v,
  double h,
  double t1x,
  double t1y,
  double t2x,
  double t2y)
{
   const int icod = 2; // Hydrosim
   const double RAC2    = 1.4142135623730950488e0;
   const double UN_RAC2 = 0.7071067811865475244e0;

   // ---  Normalise les vitesses petites
   double vmodl = sqrt(u * u + v * v);
   if (h < SP_HDRO_HMIN) {
      double vonde = sqrt(SP_HDRO_GRA * h);
      vmodl = fmax(vmodl, vonde);
   }
   double velocx, velocy, veloc;
   if (vmodl < SP_HDRO_VMIN) {
      veloc = SP_HDRO_VMIN;
      velocx = veloc * UN_RAC2;
      velocy = velocx;
   }
   else {
      veloc = vmodl;
      velocx = u;
      velocy = v;
   }

   // ---  Taille de maille sur chaque côté
   double tm1 = (t1x * velocx + t1y * velocy) / veloc;
   double tm2 = (t2x * velocx + t2y * velocy) / veloc;
   double tm3 = ((t1x - t2x) * velocx + (t1y - t2y) * velocy) / veloc;

   // ---  Taille de maille directionnelle
   double tmaild = 0.0e0;
   switch (icod) {
      case 0:  // detj
         tmaild = sqrt(t1x * t2y - t2x * t1y);
         break;
      case 1:   // Géométrique
         tmaild = fmax(sqrt(tm1*tm1), sqrt(tm2*tm2));
         break;
      case 2:   // Hydrosim
         tmaild = RAC2 * sqrt(tm1*tm1 + tm2*tm2 + tm3*tm3) / 3.0e0;
         break;
      case 3:   // Atkin
         tmaild = (sqrt(tm1*tm1) + sqrt(tm2*tm2) + sqrt(tm3*tm3)) / 4.0e0;
         break;
      case 4:   // Tezduyar
      {
         // ---  Normalise
         double v1 = tm1 / sqrt(t1x*t1x + t1y*t1y);
         double v2 = tm2 / sqrt(t2x*t2x + t2y*t2y);
         tmaild = sp_hdro_tezduyar(v1, v2);
         // ---  Dé-normalise
         v1 = v1 * sqrt(t1x*t1x + t1y*t1y);
         v2 = v2 * sqrt(t2x*t2x + t2y*t2y);
         tmaild = sqrt(v1 * v1 + v2 * v2);
         break;
      }
      default: break;
   }

   return tmaild;
}

//************************************************************************
// Sommaire: SP_HDRO_PECLET
//
// Description:
//     La fonctions <code>SP_HDRO_PECLET(...)</code> calcule le coefficient
//     de Peclet (v * L ) / nu.
//
// Entrée:
//     REAL*8 U          ! Composantes de la vitesse
//     REAL*8 V
//     REAL*8 H          ! Profondeur
//     REAL*8 T1X        ! Métriques tangentes
//     REAL*8 T1Y
//     REAL*8 T2X
//     REAL*8 T2Y
//
// Sortie:
//
// Notes:
//************************************************************************
double
sp_hdro_peclet(
   double nu,
   double u,
   double v,
   double h,
   double t1x,
   double t1y,
   double t2x,
   double t2y)
{
   // ---  Taille de maille directionnelle
   double tmail = sp_hdro_tmaild(u, v, h, t1x, t1y, t2x, t2y);

   // ---  Norme de la vitesse
   double vmod = sqrt(u*u + v*v);
   //!      VMOD = MAX(VMOD, SP_HDRO_VMIN)     ???

   // ---  Le Peclet ( v * L / nu)
   return (vmod * tmail / nu);
}

//************************************************************************
// Sommaire: SP_HDRO_CFL
//
// Description:
//     La fonctions <code>SP_HDRO_CFL(...)</code> calcule le CFL.
//
// Entrée:
//     REAL*8 U          ! Composantes de la vitesse
//     REAL*8 V
//     REAL*8 H          ! Profondeur
//     REAL*8 T1X        ! Métriques tangentes
//     REAL*8 T1Y
//     REAL*8 T2X
//     REAL*8 T2Y
//
// Sortie:
//
// Notes:
//************************************************************************
double
sp_hdro_cfl(
  double dt,
  double u,
  double v,
  double h,
  double t1x,
  double t1y,
  double t2x,
  double t2y)
{
   // ---  Taille de maille directionnelle
   double tmail = sp_hdro_tmaild(u, v, h, t1x, t1y, t2x, t2y);

   // ---  Norme de la vitesse
   double vmod = sqrt(u * u + v * v);
   //!      VMOD = MAX(VMOD, SP_HDRO_VMIN)     ???

   // ---  Le CFL ( v * DT / DL)
   return (vmod * dt / tmail);
}

//**************************************************************
// Sommaire:
//
// Description:
//     COEFFICIENT DE FRICTION DU VENT, FORMULE DE WU (1969).
//
// Entrée:
//     W     Module de la vitesse du vent par rapport à l'eau
//
// Sortie:
//
// Notes:
//
//****************************************************************
double
sp_hdro_wu1969(double w)
{
   //-----  CW = 1.25E-3*W^(-1/5) POUR    W<1  EN m/s
   //-----  CW = 0.50E-3*W^(1/2)  POUR  1<W<15 EN m/s
   //-----  CW = 2.60E-3          POUR 15<W    EN m/s
   if (w ==  0.0e0) return  1.25e-3;
   if (w <=  1.0e0) return (1.25e-3 * pow(w, -0.2e0));
   if (w <= 15.0e0) return (0.50e-3 * sqrt(w));
   return 2.60e-3;
}

//**************************************************************
// Sommaire:
//
// Description:
//     COEFFICIENT DE FRICTION DU VENT, FORMULE DE WU (1980).
//
// Entrée:
//     W     Module de la vitesse du vent par rapport à l'eau
//
// Sortie:
//
// Notes:
//
//****************************************************************
double8
sp_hdro_wu1980(double8 w)
{
   return (0.8000e-03 + 0.0650e-03*w);
}

//**************************************************************
// Sommaire:
//
// Description:
//     COEFFICIENT DE FRICTION DU VENT, FORMULE DE SMITH (1980).
//
// Entrée:
//     W     Module de la vitesse du vent par rapport à l'eau
//
// Sortie:
//
// Notes:
//
//****************************************************************
double8
sp_hdro_smith1980(double8 w)
{
   return (0.6100e-03 + 0.0630e-03*w);
}

//**************************************************************
// Sommaire:
//
// Description:
//     Coefficient de friction du vent.
//
// Entrée:
//     W        Module de la vitesse du vent par rapport à l'eau
//     ICOD     Code de la formule à appliquer
//
// Sortie:
//
// Notes:
//
//****************************************************************
double8
sp_hdro_cw(double8 w, int icod)
{
   switch (icod)
   {
      case 0: return 0.0e0;
      case 1: return INFINITY;   //
      case 2: return sp_hdro_wu1980(w);
      case 3: return sp_hdro_smith1980(w);
      default: return 0.0e0;
   }
}

#endif   // define SPHDRO_CL_ALLREADY_DEFINED
