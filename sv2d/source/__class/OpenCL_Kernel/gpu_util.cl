//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef GPU_UTIL_CL_ALLREADY_DEFINED
#define GPU_UTIL_CL_ALLREADY_DEFINED

#include "gpu_types.cl"

/*
   Initialize an double array to 0
*/
void __attribute__((overloadable)) initArray(TTGlobalDouble* x, size_t n)
{
   TTGlobalDouble *end = x + n;
   while (x != end) *x++ = 0.0e0;
}
void __attribute__((overloadable)) initArray(TTLocalDouble* x, size_t n)
{
   TTLocalDouble *end = x + n;
   while (x != end) *x++ = 0.0e0;
}
void __attribute__((overloadable)) initArray(TTPrivateDouble* x, size_t n)
{
   TTPrivateDouble *end = x + n;
   while (x != end) *x++ = 0.0e0;
}

/*
   Copy a double array src to dst
*/
void __attribute__((overloadable)) copyArray(TTGlobalConstDouble* src, TTPrivateDouble* dst, size_t n)
{
   TTGlobalConstDouble *end = src + n;
   while (src != end) *dst++ = *src++;
}
void __attribute__((overloadable)) copyArray(TTGlobalConstDouble* src, TTLocalDouble* dst, size_t n)
{
   TTGlobalConstDouble *end = src + n;
   while (src != end) *dst++ = *src++;
}
void __attribute__((overloadable)) copyArray(TTPrivateDouble* src, TTGlobalDouble* dst, size_t n)
{
   TTPrivateDouble *end = src + n;
   while (src != end) *dst++ = *src++;
}
void __attribute__((overloadable)) copyArray(TTPrivateDouble* src, TTLocalDouble* dst, size_t n)
{
   TTPrivateDouble *end = src + n;
   while (src != end) *dst++ = *src++;
}
void __attribute__((overloadable)) copyArray(TTPrivateDouble* src, TTPrivateDouble* dst, size_t n)
{
   TTPrivateDouble *end = src + n;
   while (src != end) *dst++ = *src++;
}

/*
   Swap a double array from src to dst
*/
void __attribute__((overloadable)) swapArray(TTPrivateDouble* src, TTPrivateDouble* dst, size_t n)
{
   TTPrivateDouble *end = src + n;
   while (src != end){
     double tmp = *dst;
     *dst++ = *src;
     *src++ = tmp;
   }
}

/*
   Add a double array src to dst
*/
void __attribute__((overloadable)) addArray(TTPrivateDouble* src, TTPrivateDouble* dst, size_t n)
{
   TTPrivateDouble *end = src + n;
   while (src != end) *dst++ += *src++;
}


// ---  Avec la carte NVIDIA GTX970 (OpencCL 1.2)
//      la fonction norm2 consomme moins de registres que hypot
#ifdef IS_NVIDIA
double  __attribute__((overloadable)) norm2(double  x, double  y) { return sqrt(x*x + y*y); }
double8 __attribute__((overloadable)) norm2(double8 x, double8 y) { return sqrt(x*x + y*y); }
#else
#  define norm2(x,y) (hypot(x,y))
#endif

double __attribute__((overloadable)) sum (double3 x) { return x.s0+x.s1+x.s2; }
double __attribute__((overloadable)) sum (double8 x) { return x.s0+x.s1+x.s2+x.s3+x.s4+x.s5+x.s6+x.s7; }

double __attribute__((overloadable)) mean(double3 x) { return sum(x) / 3; }
double __attribute__((overloadable)) mean(double8 x) { return sum(x) / 8; }

double8 pow2 (double8 x) { return x*x; }
double8 pow3 (double8 x) { return x*x*x; }
double8 pow4 (double8 x) { double8 x2=x*x; return x2*x2; }

#endif // define GPU_UTIL_CL_ALLREADY_DEFINED
