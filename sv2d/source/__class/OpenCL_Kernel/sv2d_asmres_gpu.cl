//************************************************************************
// --- Copyright (c) INRS 2003-2013
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************

//************************************************************************
// Sommaire:  SV2D_XBS_ASMRES
//
// Description:
//     Residuum due to volume contributions
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#include "gpu_config.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"

#include "sv2d_clcres.cl"

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Les cast sont nécessaire parce qu'on passe d'une table flat à
//    des tables à 2 dim
//************************************************************************
void sv2d_asmres(int it,
                 TTGridData*  gridDta,
                 TTConstantParams* dims)
{
   TTDouble vres_l[NNEL][NDLN];
   //-----------------------------------------------------------------------

   // ---  Loop on task elements
   //      =====================
   for (int ie = 0; ie < dims->nele; ++ie)
   {
      //if (it == 0) printf("itask: %i, ie: %i / %i\n", it, ie, dims->nele);

      const size_t idjv = (it + ie*dims->ntsk) * (NDJV);
      const size_t iprc = (it + ie*dims->ntsk) * (NPRNC*NNEL);
      const size_t idlg = (it + ie*dims->ntsk) * (NDLN *NNEL);

      TTElemData elemDta;
      elemDta.vprg = gridDta->vprg;
      elemDta.vdjv = gridDta->vdjv+idjv;
      elemDta.vprn = NULL;
      elemDta.vres = vres_l;

      // ---  Les ddl
      TTElemVdlg* vdlg = (TTElemVdlg*) (gridDta->vdlg+idlg);
      elemDta.h  = vloadStrideT6(NDLN, &vdlg[0][IDLG_H]);
      elemDta.qx = vloadStrideT6(NDLN, &vdlg[0][IDLG_QX]);
      elemDta.qy = vloadStrideT6(NDLN, &vdlg[0][IDLG_QX]);

      // ---  Les VPRC
      TTElemVprc* vprc = (TTElemVprc*) (gridDta->vprc+iprc);
      elemDta.U  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_U]);
      elemDta.V  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_V]);
      elemDta.H  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_H]);
      elemDta.N  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_N]);
      elemDta.NU = vloadStrideT6(NPRNC, &vprc[0][IPRNC_NU]);
      elemDta.WX = vloadStrideT6(NPRNC, &vprc[0][IPRNC_WX]);
      elemDta.WX = vloadStrideT6(NPRNC, &vprc[0][IPRNC_WX]);

      // ---  Calcul
      initArray(&elemDta.vres[0][0], NDLN*NNEL);
      //sv2d_xbs_fe_v(&elemDta, vsolr, vsolc);
      //sv2d_xbs_re_v1(&elemDta);
      sv2d_xbs_re_v2(&elemDta);

      // ---  Copy result to global memory
      copyArray(&elemDta.vres[0][0], gridDta->vres+idlg, NDLN*NNEL);
   }
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
KERNEL
void sv2d_asmres_cl(TTGlobalDouble*      vres,
                    TTGlobalConstDouble* vprc,
                    TTGlobalConstDouble* vdjv,
                    TTConstantVprg*      vprg,
                    TTGlobalConstDouble* vprn,
                    TTGlobalConstDouble* vdlg,
                    TTConstantParams*    dims
                    )
{
   // ---  Global task index
   const int it = get_global_id(0);
//   if (it == 0) printf("itask: %i / %i\n", it, dims->ntsk);

   // ---  Transfer to the global structure
   TTGridData gridDta;
   gridDta.vprg = vprg;
   gridDta.vdjv = vdjv;
   gridDta.vprn = vprn;
   gridDta.vprc = vprc;
   gridDta.vdlg = vdlg;
   gridDta.vres = vres;

   // ---  Nodal properties
   sv2d_asmres(it, &gridDta, dims);
}
