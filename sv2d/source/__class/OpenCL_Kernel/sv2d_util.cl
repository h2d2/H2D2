//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_UTIL_CL_ALLREADY_DEFINED
#define SV2D_UTIL_CL_ALLREADY_DEFINED

#include "gpu_types.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"


#define VACCESS_HLPR(x, i) (x.s ## i)
#define VACCESS(x, i) VACCESS_HLPR(x, i)

#define VACCESS_T3X(x)  x.s024
#define VACCESS_T3_1(x) x.s015
#define VACCESS_T3_2(x) x.s123
#define VACCESS_T3_3(x) x.s534
#define VACCESS_T3_4(x) x.s351

#define VSTORE_T3X(r,x)  { r.s0=x.s0; r.s2=x.s1; r.s4=x.s2; }
#define VSTORE_T3_1(r,x) { r.s0=x.s0; r.s1=x.s1; r.s5=x.s2; }
#define VSTORE_T3_2(r,x) { r.s1=x.s0; r.s2=x.s1; r.s3=x.s2; }
#define VSTORE_T3_3(r,x) { r.s5=x.s0; r.s3=x.s1; r.s4=x.s2; }
#define VSTORE_T3_4(r,x) { r.s3=x.s0; r.s5=x.s1; r.s1=x.s2; }

double3 VACCESS_T3_n(double8 x, int i)
{
   switch(i)
   {
      case 0 : return VACCESS_T3_1(x);
      case 1 : return VACCESS_T3_2(x);
      case 2 : return VACCESS_T3_3(x);
      case 3 : return VACCESS_T3_4(x);
      default : return INFINITY;
   }
}

double8 VSTORE_T3_n(double3 x, int i)
{
   double8 r = 0.0e0;
   switch(i)
   {
      case 0 : VSTORE_T3_1(r, x); break;
      case 1 : VSTORE_T3_2(r, x); break;
      case 2 : VSTORE_T3_3(r, x); break;
      case 3 : VSTORE_T3_4(r, x); break;
      default : r = INFINITY; break;
   }
   return r;
}

/*
   Vector load from a double array p with stride s.
   Only 6 values are copied.
*/
double8 __attribute__((overloadable)) vloadStrideT6(size_t s, TTGlobalConstDouble* p)
{
   double tmp[8];
   tmp[0] = *p; p += s;
   tmp[1] = *p; p += s;
   tmp[2] = *p; p += s;
   tmp[3] = *p; p += s;
   tmp[4] = *p; p += s;
   tmp[5] = *p;
   tmp[6] = 0.0e0;
   tmp[7] = 0.0e0;
   return vload8(0, tmp);
}

double8 __attribute__((overloadable)) vloadStrideT6(size_t s, TTPrivateConstDouble* p)
{
   double tmp[8];
   tmp[0] = *p; p += s;
   tmp[1] = *p; p += s;
   tmp[2] = *p; p += s;
   tmp[3] = *p; p += s;
   tmp[4] = *p; p += s;
   tmp[5] = *p;
   tmp[6] = 0.0e0;
   tmp[7] = 0.0e0;
   return vload8(0, tmp);
}


double3 __attribute__((overloadable)) vloadStrideT3X(size_t s, TTGlobalConstDouble* p)
{
   double tmp[3] = {*p, *(p+2*s), *(p+4*s) };
   return vload3(0, tmp);
}

double3 __attribute__((overloadable)) vloadStrideT3X(size_t s, TTPrivateConstDouble* p)
{
   double tmp[3] = {*p, *(p+2*s), *(p+4*s) };
   return vload3(0, tmp);
}


void __attribute__((overloadable)) vstoreStrideT6(double8 data, size_t s, TTGlobalDouble* p)
{
   *p = data.s0; p += s;
   *p = data.s1; p += s;
   *p = data.s2; p += s;
   *p = data.s3; p += s;
   *p = data.s4; p += s;
   *p = data.s5; p += s;
   return;
}

void __attribute__((overloadable)) vstoreStrideT6(double8 data, size_t s, TTPrivateDouble* p)
{
   *p = data.s0; p += s;
   *p = data.s1; p += s;
   *p = data.s2; p += s;
   *p = data.s3; p += s;
   *p = data.s4; p += s;
   *p = data.s5; p += s;
   return;
}

// ---  Finite element derivative on a T3 element
double __attribute__((overloadable)) ddx(double3 u, TTConstDouble* vdj)
{
   return vdj[0]*(u.s1-u.s0) + vdj[1]*(u.s2-u.s0);
}
double __attribute__((overloadable)) ddx(double3 u, TTGlobalConstDouble* vdj)
{
   return vdj[0]*(u.s1-u.s0) + vdj[1]*(u.s2-u.s0);
}
double __attribute__((overloadable)) ddy(double3 u, TTConstDouble* vdj)
{
   return vdj[2]*(u.s1-u.s0) + vdj[3]*(u.s2-u.s0);
}
double __attribute__((overloadable)) ddy(double3 u, TTGlobalConstDouble* vdj)
{
   return vdj[2]*(u.s1-u.s0) + vdj[3]*(u.s2-u.s0);
}

#ifdef H2D2_GPU_PRINT_DEBUG
void print_vprn(TTElemData* elemDta)
{
   printf("   vdlg(:,ie): ");
   for (int i=0; i < NDLN; ++i)
      printf(" %f", elemDta.vdlg[0][i]);
   printf("\n");
   printf("   vprn(:,ie): ");
   for (int i=0; i < NPRNL; ++i)
      printf(" %f", elemDta.vprn[0][i]);
   printf("\n");
   printf("   vprc(:,ie): ");
   for (int i=0; i < NPRNC; ++i)
      printf(" %f", elemDta.vprc[0][i]);
   printf("\n");
}
#else
void print_vprn(TTElemData* elemDta) {}
#endif   // H2D2_GPU_PRINT_DEBUG

#ifdef H2D2_GPU_PRINT_DEBUG
void print_vpre(TTElemData* elemDta)
{
   printf("   vpre(:,ie): ");
   for (int i=0; i < NPRE; ++i)
      printf(" %f", elemDta.vpre[i]);
   printf("\n");
}
#else
void print_vpre(TTElemData* elemDta) {}
#endif   // H2D2_GPU_PRINT_DEBUG

#endif // define SV2D_UTIL_CL_ALLREADY_DEFINED
