//************************************************************************
// --- Copyright (c) INRS 2003-2013
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id: sv2d_cbs_asmres.for,v 1.1 2015/11/13 17:33:30 secretyv Exp $
//
// Functions:
//   Public:
//     SUBROUTINE SV2D_XBS_ASMRES
//   Private:
//     INTEGER SV2D_XBS_ASMRES_RV
//     INTEGER SV2D_$_CLC_PRE
//
//************************************************************************

#include "gpu_config.cl"
#include "gpu_util.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"
#include "sv2d_util.cl"
#include "sv2d_const.cl"

#include "sphdro.cl"
#include "sv2d_clcprev.cl"
#include "sv2d_clcprno.cl"
#include "sv2d_clcres.cl"

#ifdef  __COMPILE_VPRN_ASM
//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Les cast sont nécessaire parce qu'on passe d'une table flat à
//    des tables à 2 dim
//************************************************************************
void sv2d_xbs_asmres_rv3(int it,
                        TTGridData*  gridDta,
                        TTConstantParams* dims)
{
   TTDouble vres_l[NNEL][NDLN];
   //-----------------------------------------------------------------------

   // ---  Loop on task elements
   //      =====================
   for (int ie = 0; ie < dims->nele; ++ie)
   {
      //if (it == 0) printf("itask: %i, ie: %i / %i\n", it, ie, dims->nele);

      const size_t idjv = (it + ie*dims->ntsk) * (NDJV);
      const size_t iprn = (it + ie*dims->ntsk) * (NPRNL*NNEL);
      const size_t idlg = (it + ie*dims->ntsk) * (NDLN *NNEL);

      TTElemData elemDta;
      elemDta.vprg = gridDta->vprg;
      elemDta.vdjv = gridDta->vdjv+idjv;
      elemDta.vprn = (TTElemVprn*) (gridDta->vprn+iprn);
      elemDta.vdlg = (TTElemVdlg*) (gridDta->vdlg+idlg);    // doit disparaitre
      elemDta.vprc = NULL;
      elemDta.vres = vres_l;
      elemDta.h  = vloadStrideT6(NDLN, &elemDta.vdlg[0][IDLG_H]);
      elemDta.qx = vloadStrideT6(NDLN, &elemDta.vdlg[0][IDLG_QX]);
      elemDta.qy = vloadStrideT6(NDLN, &elemDta.vdlg[0][IDLG_QX]);

      // ---  Initialize arrays
      initArray(elemDta.vres[0], NDLN*NNEL);

      // ---  Nodal properties
      elemDta->H = vloadStrideT6(NPRNC, &elemDta.vprc[0][IDLG_H])
      //sv2d_y4_cbs_clcprnev (&elemDta);
      //if (it == 0) print_vprn(&elemDta);

      // ---  Calcul
      //sv2d_xbs_fe_v(&elemDta, vsolr, vsolc);
      sv2d_xbs_re_v(&elemDta);

      // ---  Copy result to global memory
      copyPrivateToGlobal(elemDta.vres[0], gridDta->vres+idlg, NDLN*NNEL);
   }
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
KERNEL
void sv2d_xbs_asmres_cl(TTGlobalDouble*      vres,
                        TTGlobalConstDouble* vdjv,
                        TTGlobalConstDouble* vprg,
                        TTGlobalConstDouble* vprn,
                        //TTGlobalConstDouble* vprc,
                        TTGlobalConstDouble* vdlg,
                        TTConstantParams*    dims
                        )
{
   // ---  Global task index
   const int it = get_global_id(0);
//   if (it == 0) printf("itask: %i / %i\n", it, dims->ntsk);

   //TTLocalDouble vprg_l[NPRG];
   //copyGlobalToLocal(vprg, vprg_l, NPRG);

   // ---  Transfer to the global structure
   TTGridData gridDta;
   gridDta.vprg = vprg;
   gridDta.vdjv = vdjv;
   gridDta.vprn = vprn;
   //gridDta.vprc = vprc;
   gridDta.vdlg = vdlg;
   gridDta.vres = vres;

   // ---  Volume contribution
   sv2d_xbs_asmres_rv3(it, &gridDta, dims);

   /*
   // ---   Contribution de surface
   //IERR = SV2D_XBS_ASMRES_RV(GDTA,EDTA,PDTA,VRES)

   // ---  Contribution des conditions limites
   //IERR = SV2D_XBS_ASMRES_RV(GDTA,EDTA,PDTA,VRES)
   */
}
#endif // __COMPILE_VPRN_ASM

#ifdef  __COMPILE_ASM
//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Les cast sont nécessaire parce qu'on passe d'une table flat à
//    des tables à 2 dim
//************************************************************************
void sv2d_asmres(int it,
                 TTGridData*  gridDta,
                 TTConstantParams* dims)
{
   TTDouble vres_l[NNEL][NDLN];
   //-----------------------------------------------------------------------

   // ---  Loop on task elements
   //      =====================
   for (int ie = 0; ie < dims->nele; ++ie)
   {
      //if (it == 0) printf("itask: %i, ie: %i / %i\n", it, ie, dims->nele);

      const size_t idjv = (it + ie*dims->ntsk) * (NDJV);
      const size_t iprc = (it + ie*dims->ntsk) * (NPRNC*NNEL);
      const size_t idlg = (it + ie*dims->ntsk) * (NDLN *NNEL);

      TTElemData elemDta;
      elemDta.vprg = gridDta->vprg;
      elemDta.vdjv = gridDta->vdjv+idjv;
      elemDta.vprn = NULL;
      elemDta.vres = vres_l;

      // ---  Les ddl
      TTElemVdlg* vdlg = (TTElemVdlg*) (gridDta->vdlg+idlg);
      elemDta.h  = vloadStrideT6(NDLN, &vdlg[0][IDLG_H]);
      elemDta.qx = vloadStrideT6(NDLN, &vdlg[0][IDLG_QX]);
      elemDta.qy = vloadStrideT6(NDLN, &vdlg[0][IDLG_QX]);

      // ---  Les VPRC
      TTElemVprc* vprc = (TTElemVprc*) (gridDta->vprc+iprc);
      elemDta.U  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_U]);
      elemDta.V  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_V]);
      elemDta.H  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_H]);
      elemDta.N  = vloadStrideT6(NPRNC, &vprc[0][IPRNC_N]);
      elemDta.NU = vloadStrideT6(NPRNC, &vprc[0][IPRNC_NU]);
      elemDta.WX = vloadStrideT6(NPRNC, &vprc[0][IPRNC_WX]);
      elemDta.WX = vloadStrideT6(NPRNC, &vprc[0][IPRNC_WX]);

      // ---  Calcul
      initArray(&elemDta.vres[0][0], NDLN*NNEL);
      //sv2d_xbs_fe_v(&elemDta, vsolr, vsolc);
      //sv2d_xbs_re_v1(&elemDta);
      sv2d_xbs_re_v2(&elemDta);

      // ---  Copy result to global memory
      copyArray(&elemDta.vres[0][0], gridDta->vres+idlg, NDLN*NNEL);
   }
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
KERNEL
void sv2d_asmres_cl(TTGlobalDouble*      vres,
                    TTGlobalConstDouble* vdjv,
                    TTGlobalConstDouble* vprg,
                    TTGlobalConstDouble* vprn,
                    TTGlobalConstDouble* vprc,
                    TTGlobalConstDouble* vdlg,
                    TTConstantParams*    dims
                    )
{
   // ---  Global task index
   const int it = get_global_id(0);
//   if (it == 0) printf("itask: %i / %i\n", it, dims->ntsk);

   // ---  Transfer to the global structure
   TTGridData gridDta;
   gridDta.vprg = vprg;
   gridDta.vdjv = vdjv;
   gridDta.vprn = vprn;
   gridDta.vprc = vprc;
   gridDta.vdlg = vdlg;
   gridDta.vres = vres;

   // ---  Nodal properties
   sv2d_asmres(it, &gridDta, dims);
}

#endif // __COMPILE_ASM
