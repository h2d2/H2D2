//************************************************************************
// --- Copyright (c) INRS 2003-2013
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_CONST_CL_ALLREADY_DEFINED
#define SV2D_CONST_CL_ALLREADY_DEFINED

#include "sv2d_types.cl"

TTConstantConstDouble ZERO   = 0.0e0;
TTConstantConstDouble UN     = 1.0e0;
TTConstantConstDouble DEUX   = 2.0e0;
TTConstantConstDouble TROIS  = 3.0e0;
TTConstantConstDouble QUATRE = 4.0e0;
TTConstantConstDouble UN_2   = 1.0e0 /   2.0e0;
TTConstantConstDouble UN_3   = 1.0e0 /   3.0e0;
TTConstantConstDouble UN_4   = 1.0e0 /   4.0e0;
TTConstantConstDouble UN_6   = 1.0e0 /   6.0e0;
TTConstantConstDouble UN_24  = 1.0e0 /  24.0e0;
TTConstantConstDouble UN_96  = 1.0e0 /  96.0e0;
TTConstantConstDouble UN_120 = 1.0e0 / 120.0e0;

TTConstantConstDouble RHO_AIR = 1.2475e+00;
TTConstantConstDouble RHO_EAU = 1.0000e+03;
TTConstantConstDouble RHO_REL = 1.2475e+00 / 1.0000e+03;

TTConstantConstDouble R_4_3     =  4.0e0 / 3.0e0;
TTConstantConstDouble R_4_3_NEG = -4.0e0 / 3.0e0;

TTConstantConstUChar KNE_T3[4][3] = { {0,1,5}, {1,2,3}, {5,3,4}, {3,5,1}};

#endif   // SV2D_CONST_CL_ALLREADY_DEFINED
