//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_CBS_RE_CL_ALLREADY_DEFINED
#define SV2D_CBS_RE_CL_ALLREADY_DEFINED

#include "gpu_config.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"

#include "sv2d_const.cl"
#include "sv2d_clcres_cmp.cl"

//************************************************************************
// Sommaire: SV2D_XBS_FE_V
//
// Description:
//     La fonction SV2D_XBS_FE_V calcule le résidu élémentaire dû à un
//     élément de volume.
//     Les tables passées en paramètre sont des tables élémentaires.
//
// Entrée:
//     VDJE        Table du Jacobien Élémentaire
//     VPRG        Table de PRopriétés Globales
//     VPRN        Table de PRopriétés Nodales
//     VPRE        Table de PRopriétés Élémentaires
//     VDLE        Table de Degrés de Libertés Élémentaires
//     VRHS        Table du Membre de Droite
//
// Sortie:
//     VRES        Résidu élémentaire
//
// Notes:
//
//************************************************************************
void sv2d_xbs_fe_v(TTElemData* elemDta)
{
  // ---     Sollicitations
  //sv2d_cmp_fe_v_slr(vres, vdje, vprg, vprn, vpre, vdle, vslr);

  return;
}

//************************************************************************
// Sommaire: SV2D_XBS_RE_V
//
// Description:
//     La fonction SV2D_XBS_ASMR_V calcule le résidu élémentaire dû à un
//     élément de volume.
//     Les tables passées en paramètre sont des tables élémentaires.
//
// Entrée:
//     VDJE        Table du Jacobien Élémentaire
//     VPRG        Table de PRopriétés Globales
//     VPRN        Table de PRopriétés Nodales
//     VPRE        Table de PRopriétés Élémentaires
//     VDLE        Table de Degrés de Libertés Élémentaires
//     VRHS        Table du Membre de Droite
//
// Sortie:
//     VRES        Résidu élémentaire
//
// Notes:
//
//************************************************************************
void sv2d_xbs_re_v2(TTElemData* elemDta)
{
   sv2d_cmp_T6v_cnv(elemDta);       // Convection
   sv2d_cmp_T6v_dif(elemDta);       // Diffusion
   //sv2d_cmp_T6v_man(elemDta);       // Manning
   //sv2d_cmp_T6v_vnt(elemDta);       // Vent
   //sv2d_cmp_T6v_cor(elemDta);       // Coriolis
   //sv2d_cmp_T6v_grv(elemDta);       // Gravité
}

//************************************************************************
// Sommaire: SV2D_XBS_RE_V1
//
// Description:
//     La fonction SV2D_XBS_ASMR_V calcule le résidu élémentaire dû à un
//     élément de volume.
//     Les tables passées en paramètre sont des tables élémentaires.
//
// Entrée:
//     VDJE        Table du Jacobien Élémentaire
//     VPRG        Table de PRopriétés Globales
//     VPRN        Table de PRopriétés Nodales
//     VPRE        Table de PRopriétés Élémentaires
//     VDLE        Table de Degrés de Libertés Élémentaires
//     VRHS        Table du Membre de Droite
//
// Sortie:
//     VRES        Résidu élémentaire
//
// Notes:
//
//************************************************************************
void sv2d_xbs_re_v1(TTElemData* elemDta)
{
   sv2d_cmp_T6v_cnt(elemDta);   // Continuité
   sv2d_cmp_T6v_lpd(elemDta);   // Lapidus
   sv2d_cmp_T6v_drc(elemDta);   // Darcy
}

//************************************************************************
// Sommaire: SV2D_XBS_RE_V
//
// Description:
//     La fonction SV2D_XBS_ASMR_V calcule le résidu élémentaire dû à un
//     élément de volume.
//     Les tables passées en paramètre sont des tables élémentaires.
//
// Entrée:
//     VDJE        Table du Jacobien Élémentaire
//     VPRG        Table de PRopriétés Globales
//     VPRN        Table de PRopriétés Nodales
//     VPRE        Table de PRopriétés Élémentaires
//     VDLE        Table de Degrés de Libertés Élémentaires
//     VRHS        Table du Membre de Droite
//
// Sortie:
//     VRES        Résidu élémentaire
//
// Notes:
//
//************************************************************************
void sv2d_xbs_re_v0(TTElemData* elemDta)
{
   for (int inod=0; inod < NNEL; ++inod)
      for (int idln=0; idln < NDLN; ++idln)
         elemDta->vres[inod][idln] = inod*NNEL + idln;
}

#ifdef ZZZZZZZZZZZZZZZZZ
//************************************************************************
// Sommaire: SV2D_XBS_RE_S
//
// Description:
//     La fonction SV2D_XBS_ASMR_S calcule le matrice de rigidité
//     élémentaire due à un élément de surface.
//     Les tables passées en paramètre sont des tables élémentaires,
//     à part la table VDLG.
//
// Entrée:
//     VDJV        Table du Jacobien Élémentaire de l'élément de volume
//     VDJS        Table du Jacobien Élémentaire de l'élément de surface
//     VPRG        Table de PRopriétés Globales
//     VPRN        Table de PRopriétés Nodales de Volume
//     VPREV       Table de PRopriétés Élémentaires de Volume
//     VPRES       Table de PRopriétés Élémentaires de Surface
//     VDLE        Table de Degrés de Libertés Élémentaires de Volume
//     ICOTE       L'élément de surface forme le ième CÔTÉ du triangle
//
// Sortie:
//     VRES        Résidu élémentaire
//
// Notes:
//     On permute les métriques du T6L parent pour que le côté à traiter
//     soit le premier côté (noeuds 1-2-3).
//
//************************************************************************
int
sv2d_xbs_re_s(
  common& cmn,
  arr_cref<double, 2> vres,
  arr_cref<double> vdjv,
  arr_cref<double> vdjs,
  arr_cref<double> vprg,
  arr_cref<double, 2> vprn,
  arr_cref<double, 2> vprev,
  arr_cref<double, 2> vpres,
  arr_cref<double, 2> vdle,
  arr_cref<double, 2> vrhs,
  int const& icote)
{
  int return_value = fem::int0;
  FEM_CMN_SVE(sv2d_xbs_re_s);
  vres(dim1(1, ).dim2(1, ));
  vdjv(dim1(1, ));
  vdjs(dim1(1, ));
  vprg(dim1(1, ));
  vprn(dim1(1, ).dim2(1, ));
  vprev(dim1(1, ).dim2(1, ));
  vpres(dim1(1, ).dim2(1, ));
  vdle(dim1(1, ).dim2(1, ));
  vrhs(dim1(1, ).dim2(1, ));
  // SAVE
  arr_ref<int, 3> knet3(sve.knet3, dimension(3, 2, 3));
  arr_ref<int, 2> knet6(sve.knet6, dimension(3, 3));
  arr_ref<int, 2> kt3(sve.kt3, dimension(2, 3));
  //
  if (is_called_first_time) {
    {
      static const int values[] = {
        1, 3, 5, 3, 5, 1, 5, 1, 3
      };
      fem::data_of_type<int>(FEM_VALUES_AND_SIZE),
        knet6;
    }
    {
      static const int values[] = {
        1, 2, 6, 2, 3, 4, 3, 4, 2, 4, 5, 6, 5, 6, 4, 6, 1, 2
      };
      fem::data_of_type<int>(FEM_VALUES_AND_SIZE),
        knet3;
    }
    {
      static const int values[] = {
        1, 2, 2, 3, 3, 1
      };
      fem::data_of_type<int>(FEM_VALUES_AND_SIZE),
        kt3;
    }
  }
  //
  // 3 noeuds sommets, 3 cotés
  // 3 noeuds, 2 sous-elem T3, 3 cotés
  // 2 sous-elem T3, 3 cotés
  //
  // Les sommets du T6
  //-----------------------------------------------------------------------
  //     CALL ERR_PRE(ICOTE .GE. 1 .AND. ICOTE .LE. 3)
  //-----------------------------------------------------------------------
  //
  // ---  MONTE LES TABLES AUX. POUR PERMUTER LES MÉTRIQUES
  arr_1d<4, double> vdjx(fem::fill0);
  vdjx(1) = vdjv(1);
  vdjx(2) = vdjv(2);
  vdjx(3) = -(vdjx(1) + vdjx(2));
  vdjx(4) = vdjx(1);
  arr_1d<4, double> vdjy(fem::fill0);
  vdjy(1) = vdjv(3);
  vdjy(2) = vdjv(4);
  vdjy(3) = -(vdjy(1) + vdjy(2));
  vdjy(4) = vdjy(1);
  //
  // ---  Métriques permutées du sous-élément T3
  double UN_2 = fem::zero<double>();
  arr_1d<5, double> vdjt3(fem::fill0);
  vdjt3(1) = UN_2 * vdjx(icote);
  vdjt3(2) = UN_2 * vdjx(icote + 1);
  vdjt3(3) = UN_2 * vdjy(icote);
  vdjt3(4) = UN_2 * vdjy(icote + 1);
  double UN_4 = fem::zero<double>();
  vdjt3(5) = UN_4 * vdjv(5);
  //
  // ---  Métriques du sous-élément L2
  arr_1d<3, double> vdjl2(fem::fill0);
  vdjl2(1) = vdjs(1);
  vdjl2(2) = vdjs(2);
  vdjl2(3) = UN_2 * vdjs(3);
  //
  // ---     BOUCLE SUR LES SOUS-ÉLÉMENTS
  //           ============================
  int il2 = fem::int0;
  int it3 = fem::int0;
  FEM_DO_SAFE(il2, 1, 2) {
    it3 = kt3(il2, icote);
    //
    // ---     DIFFUSION
    sv2d_cmp_s_dif(vres, knet3(1, , il2, icote), vdjt3, vdjl2,
      vprg, vprn, vprev(1, , it3), vpres(1, , il2), vdle, vrhs);
    //
  }
  //
  // ---     CONTINUITE
  sv2d_cmp_s_cnt(vres, knet6(1, , icote), vdjt3, vdjl2, vprg,
    vprn, vprev, vpres, vdle, vrhs);
  //
  return;
}
#endif

#endif   // define SV2D_CBS_RE_CL_ALLREADY_DEFINED
