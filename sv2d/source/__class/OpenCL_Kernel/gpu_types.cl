//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef GPU_TYPES_CL_ALLREADY_DEFINED
#define GPU_TYPES_CL_ALLREADY_DEFINED

#include "gpu_config.cl"

typedef GLOBAL   const double          TTGlobalConstDouble;
typedef GLOBAL         double          TTGlobalDouble;
typedef CONSTANT const double          TTConstantConstDouble;
typedef CONSTANT const int             TTConstantConstInt;
typedef CONSTANT const char            TTConstantConstChar;
typedef CONSTANT const unsigned int    TTConstantConstUInt;
typedef CONSTANT const unsigned char   TTConstantConstUChar;

typedef PRIVATE  const double          TTPrivateConstDouble;
typedef PRIVATE        double          TTPrivateDouble;
typedef LOCAL    const double          TTLocalConstDouble;
typedef LOCAL          double          TTLocalDouble;
typedef          const double          TTConstDouble;
typedef                double          TTDouble;

#endif   // GPU_TYPES_CL_ALLREADY_DEFINED
