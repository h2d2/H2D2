//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_CLCPRNO_CL_ALLREADY_DEFINED
#define SV2D_CLCPRNO_CL_ALLREADY_DEFINED

#include "gpu_config.cl"
#include "gpu_util.cl"

#include "sphdro.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"
#include "sv2d_const.cl"
#include "sv2d_util.cl"
#include "sv2d_clcprev.cl"


double8 uexp(const double8 u, double umax)
{
   double8 ul = fmax(u, umax) + (umax - UN);
   return cbrt(ul) + (umax - UN);
}
double8 ulim(const double8 u, double umax)
{
   return fmin(u, uexp(u, umax));
}

//************************************************************************
// Sommaire: Darcy coefficient on the external T3 element
//
// Description:
//    The function clc_prnev_DRCY_T3X compute the Darcy coefficient
//    fro the external T3 element
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double3
clc_prnev_DRCY_T3X(const TTElemData* elemDta, double3 vn1, double3 vn2)
{
   const double DECOU_DRC_NU = elemDta->vprg->decou_drc_nu;
   const double STABI_DARCY  = elemDta->vprg->stabi_darcy;

   return (vn1*DECOU_DRC_NU + vn2*STABI_DARCY);   // Darcy
}

//************************************************************************
// Sommaire: clc_prnev_DIFF_T3X
//
// Description:
//    Calcul des autres valeurs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double3
clc_prnev_DIFF_T3X(const TTElemData* elemDta, double3 vn1, double3 vn2)
{
   const double DECOU_DIF_NU = elemDta->vprg->decou_dif_nu;

   return (vn1*DECOU_DIF_NU);   // Dry diffusion
}

//************************************************************************
// Sommaire: clc_prnev_DIFF_T3
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double3
clc_prnev_DIFF_T3(const TTSubT3Data* elemDta, double3 vn1, double3 vn2)
{
   const double DECOU_DIF_NU = elemDta->vprg->decou_dif_nu;

   return (vn1*DECOU_DIF_NU);   // Dry diffusion
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev_N
//
// Description:
//    Calcul du frottement.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Frottement de Manning : g n2 |u| / H**[4/3]
//    ???  pour Manning, il y 2 interpolations linéaires:
//       1) le coeef de Manning (bizarre!!)
//       20 le module de la vitesse
//    Pourquoi ne pas interpoler la contrainte??
//************************************************************************
double8
clc_prnev_N_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   const double GRAVITE     = elemDta->vprg->gravite;

   const double8 H4_3 = cbrt( pow4(elemDta->H) );

   // ---  La partie découverte
   double8 fdry;
   //if (false)
   {
      const double DECOU_AMORT = elemDta->vprg->decou_amort;
      const double DECOU_MAN   = elemDta->vprg->decou_man;
      const double DECOU_UMAX  = elemDta->vprg->decou_umax;

      const double8 vman = DECOU_MAN;
            double8 vmod = norm2(elemDta->U, elemDta->V);
                    vmod = ulim(vmod, DECOU_UMAX);
      const double8 vfln = DECOU_AMORT;
                    fdry = vfln + GRAVITE * pow2(vman) * vmod / H4_3;
   }

   // ---  La partie mouillée
   double8 fwet;
   //if (false)
   {
      const double STABI_AMORT = elemDta->vprg->stabi_amort;
      const double CMULT_MAN   = elemDta->vprg->cmult_man;
      const double8 nbtm = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_N]);
      const double8 nice = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_ICE_N]);
      const double8 ntot = norm2(nbtm, nice);
      const double8 vman = CMULT_MAN * ntot;
      const double8 vmod = norm2(elemDta->U, elemDta->V);
      const double8 vfln = STABI_AMORT;
                    fwet = vfln + GRAVITE * pow2(vman) * vmod / H4_3;
   }

   return (vn1 * fdry + vn2 * fwet);  // Total friction
}

//************************************************************************
// Sommaire: clc_prnev_NU_T6
//
// Description:
//    Calcul de la viscosité totale.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_NU_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   double8 nu = 0.0e0;
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h, it3);
      //t3Dta.qx = VACCESS_T3_n(elemDta->qx,it3);
      //t3Dta.qy = VACCESS_T3_n(elemDta->qy,it3);
      t3Dta.U  = VACCESS_T3_n(elemDta->U, it3);
      t3Dta.V  = VACCESS_T3_n(elemDta->V, it3);
      t3Dta.H  = VACCESS_T3_n(elemDta->H, it3);
      //t3Dta.N  = VACCESS_T3_n(elemDta->N, it3);
      //t3Dta.A  = VACCESS_T3_n(elemDta->A, it3);
      double3 vn1T3 = VACCESS_T3_n(vn1, it3);
      double3 vn2T3 = VACCESS_T3_n(vn2, it3);

      // ---  Viscosité totale + découvrement
      const double nutot = clc_preev_T3(&t3Dta).s1;
      const double3 c = clc_prnev_DIFF_T3(&t3Dta, vn1T3, vn2T3) + nutot;

      // ---  (H*nu) moyen
      const double3 h_nu = t3Dta.H * c;

      // ---  Accumule
      nu = nu + VSTORE_T3_n(h_nu, it3);
   }
   
   // ---  Moyenne les noeuds médiants
   nu.s1 = UN_3 * nu.s1;
   nu.s3 = UN_3 * nu.s3;
   nu.s5 = UN_3 * nu.s5;

   return nu;  
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev_W
//
// Description:
//    Calcul du vent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_WX_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   const double CMULT_VENT_REL = elemDta->vprg->cmult_vent_rel;
   const int    FCT_CW_VENT    = elemDta->vprg->fct_cw_vent;

   // ---  Module du vent relatif
   const double8 vx = elemDta->U * CMULT_VENT_REL;
   const double8 vy = elemDta->V * CMULT_VENT_REL;
   const double8 wx = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_X]) - vx;
   const double8 wy = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_Y]) - vy;
   const double8 wmod = norm2(wx, wy);
         double8 cw = FCT_CW_VENT; //sp_hdro_cw(wmod, FCT_CW_VENT);

   // ---  cw |w| rho_air/r
   //return (RHO_REL * cw  * wmod * wx);
   return cw;
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev_W
//
// Description:
//    Calcul du vent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_WY_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   const double CMULT_VENT_REL = elemDta->vprg->cmult_vent_rel;
   const int    FCT_CW_VENT    = elemDta->vprg->fct_cw_vent;

   // ---  Module du vent relatif
   const double8 vx = elemDta->U * CMULT_VENT_REL;
   const double8 vy = elemDta->V * CMULT_VENT_REL;
   const double8 wx = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_X]) - vx;
   const double8 wy = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_Y]) - vy;
   const double8 wmod = norm2(wx, wy);
         //double8 cw = sp_hdro_cw(wmod, FCT_CW_VENT);
         double8 cw = wmod;

   // ---  cw |w| rho_air/r
   //return (RHO_REL * cw  * wmod * wy);
   return cw;
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev_W
//
// Description:
//    Calcul du vent.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double16
clc_prnev_W_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   const double CMULT_VENT_REL = elemDta->vprg->cmult_vent_rel;
   const int    FCT_CW_VENT    = elemDta->vprg->fct_cw_vent;

   // ---  Module du vent relatif
   const double8 vx = elemDta->U * CMULT_VENT_REL;
   const double8 vy = elemDta->V * CMULT_VENT_REL;
   const double8 wx = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_X]) - vx;
   const double8 wy = vloadStrideT6(NPRNL, &elemDta->vprn[0][IPRNO_WND_Y]) - vy;
   const double8 wmod = norm2(wx, wy);
         double8 cw = sp_hdro_cw(wmod, FCT_CW_VENT);

   // ---  cw |w| rho_air/r
   cw = (RHO_REL * cw  * wmod);
   return (double16) (cw*wx, cw*wy);
}

//************************************************************************
// Sommaire: clc_prnev_DRCY_V_T6
//
// Description:
//    Calcul des vitesses.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_V_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   return (elemDta->qy / elemDta->H);
}

//************************************************************************
// Sommaire: clc_prnev_DRCY_U_T6
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_U_T6(const TTElemData* elemDta, double8 vn1, double8 vn2)
{
   return (elemDta->qx / elemDta->H);
}

//************************************************************************
// Sommaire: clc_prnev_A_T6
//
// Description:
//    Calcul des profondeurs
//       H is in place
//       A is squatted and holds H effectiv
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_A_T6(const TTElemData* elemDta)
{
   const double DECOU_HMIN = elemDta->vprg->decou_hmin;
   const double DECOU_HDEL = elemDta->vprg->decou_dhst;

   // ---  Profondeurs des sommets
   const double3 zf = vloadStrideT3X(NPRNL, &elemDta->vprn[0][IPRNO_Z]);
         double3 P  = VACCESS_T3X(elemDta->h) - zf;

   // ---  Epaisseur de glace
   double3 epaigl = 0.9e0 * vloadStrideT3X(NPRNL, &elemDta->vprn[0][IPRNO_ICE_E]);
           epaigl = fmin(epaigl, P);
           epaigl = fmax(epaigl, ZERO);

   // ---  Profondeurs effective des sommets
   P = P - epaigl;

   // ---  Impose les profondeurs sur les noeuds milieux
   double8 A;
   A.s0 = P.s0;
   A.s2 = P.s1;
   A.s4 = P.s2;
   A.s1 = (P.s0 + P.s1) * UN_2;
   A.s3 = (P.s1 + P.s2) * UN_2;
   A.s5 = (P.s2 + P.s0) * UN_2;

   // ---  Alfa de découvrement
   A = (A - DECOU_HMIN) / DECOU_HDEL;
   A = fmax(ZERO, A);
   A = fmin(UN,   A);

   return A;
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev_H
//
// Description:
//    Calcul des profondeurs
//       H is in place
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double8
clc_prnev_H_T6(const TTElemData* elemDta)
{
   const double DECOU_HMIN = elemDta->vprg->decou_hmin;
   const double DECOU_HDEL = elemDta->vprg->decou_dhst;

   // ---  Profondeurs des sommets
   const double3 zf = vloadStrideT3X(NPRNL, &elemDta->vprn[0][IPRNO_Z]);
         double3 P  = VACCESS_T3X(elemDta->h) - zf;

   // ---  Epaisseur de glace
   double3 epaigl = 0.9e0 * vloadStrideT3X(NPRNL, &elemDta->vprn[0][IPRNO_ICE_E]);
           epaigl = fmin(epaigl, P);
           epaigl = fmax(epaigl, ZERO);

   // ---  Profondeurs absolue des sommets
   P = fmax(P - epaigl, DECOU_HMIN);

   // ---  Impose les profondeurs sur les noeuds milieux
   //      Noeud rehaussé si un noeud sommet est découvert
   double8 H;
   H.s0 = P.s0;
   H.s2 = P.s1;
   H.s4 = P.s2;
   H.s1 = (P.s0 + P.s1) * UN_2;
   H.s3 = (P.s1 + P.s2) * UN_2;
   H.s5 = (P.s2 + P.s0) * UN_2;

   return H;
}

//************************************************************************
// Sommaire: sv2d_y4_cbs_clcprnev
//
// Description:
//     Calcul des propriétés nodales d'un élément de volume.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void
sv2d_clcprnev(TTElemData* elemDta)
{
   /*
   // ---  Impose les niveaux d'eau sur les noeuds milieux
   VACCESS(elemDta->h, NO2) = (vdle[NO1] + vdle[NO3]) * UN_2;
   VACCESS(elemDta->h, NO4) = (vdle[NO3] + vdle[NO5]) * UN_2;
   VACCESS(elemDta->h, NO6) = (vdle[NO5] + vdle[NO1]) * UN_2;
   */

/*
   if (get_global_id(0) == 0) printf("   In sv2d_y4_cbs_clcprnev\n");
*/

   elemDta->H  = clc_prnev_H_T6(elemDta);
   
   double8 vn2 = clc_prnev_A_T6(elemDta);
   double8 vn1 = vn1 = UN - vn2;
   
   elemDta->U  = clc_prnev_U_T6 (elemDta, vn1, vn2);
   elemDta->V  = clc_prnev_V_T6 (elemDta, vn1, vn2);
   elemDta->N  = clc_prnev_N_T6 (elemDta, vn1, vn2);
   elemDta->NU = clc_prnev_NU_T6(elemDta, vn1, vn2);
   const double16 WXY = clc_prnev_W_T6(elemDta, vn1, vn2);
   elemDta->WX = WXY.lo;
   elemDta->WX = WXY.hi;

/*
   if (get_global_id(0) == 0) printf("   End sv2d_y4_cbs_clcprnev\n");
*/
}

#endif // define SV2D_CLCPRNO_CL_ALLREADY_DEFINED
