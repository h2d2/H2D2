//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************

//************************************************************************
// Sommaire:  Kernel de calcul de prno
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#define H2D2_GPU_SV2D_COMPILE_VPRN

#include "gpu_config.cl"
#include "gpu_util.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"

#include "sv2d_clcprno.cl"

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Les cast sont nécessaire parce qu'on passe d'une table flat à
//    des tables à 2 dim
//************************************************************************
void sv2d_clcprn(int it,
                 TTGridData*  gridDta,
                 TTConstantParams* dims)
{
   TTDouble vprc_l[NNEL][NPRNC];
   //-----------------------------------------------------------------------

   // ---  Loop on task elements
   //      =====================
   for (int ie = 0; ie < dims->nele; ++ie)
   {
      if (it == 0) printf("itask: %i, ie: %i / %i\n", it, ie, dims->nele);

      const size_t idjv = (it + ie*dims->ntsk) * (NDJV);
      const size_t iprn = (it + ie*dims->ntsk) * (NPRNL*NNEL);
      const size_t idlg = (it + ie*dims->ntsk) * (NDLN *NNEL);

      TTElemData elemDta;
      elemDta.vprg = gridDta->vprg;
      elemDta.vdjv = gridDta->vdjv+idjv;
      elemDta.vprn = (TTElemVprn*) (gridDta->vprn+iprn);
      elemDta.vres = NULL;

      // ---  Extract DOF
      TTElemVdlg* vdlg = (TTElemVdlg*) (gridDta->vdlg+idlg);
      elemDta.qx = vloadStrideT6(NDLN, &vdlg[0][IDLG_QX]);
      elemDta.qy = vloadStrideT6(NDLN, &vdlg[0][IDLG_QY]);
      elemDta.h  = vloadStrideT6(NDLN, &vdlg[0][IDLG_H]);

      // ---  Compute depth H
      elemDta.H = clc_prnev_H_T6(&elemDta);

      // ---  Dry/wet coefficients
      //double8 vn2 = clc_prnev_A_T6(&elemDta);
      //double8 vn1 = UN - vn2;
      double8 vn2 = UN;
      double8 vn1 = ZERO;

      // ---  Compute PRN
      elemDta.U  = clc_prnev_U_T6 (&elemDta, vn1, vn2);
      elemDta.V  = clc_prnev_V_T6 (&elemDta, vn1, vn2);
      elemDta.N  = clc_prnev_N_T6 (&elemDta, vn1, vn2);
      elemDta.NU = clc_prnev_NU_T6(&elemDta, vn1, vn2);
      elemDta.WX = clc_prnev_WX_T6(&elemDta, vn1, vn2);
      elemDta.WY = clc_prnev_WY_T6(&elemDta, vn1, vn2);

      // ---  Transfer to private array vprc_l
      initArray(&vprc_l[0][IPRNC_U], NPRNC*NNEL);
      vstoreStrideT6(elemDta.U, NPRNC, &vprc_l[0][IPRNC_U]);
      vstoreStrideT6(elemDta.V, NPRNC, &vprc_l[0][IPRNC_V]);
      vstoreStrideT6(elemDta.H, NPRNC, &vprc_l[0][IPRNC_H]);
      vstoreStrideT6(elemDta.N, NPRNC, &vprc_l[0][IPRNC_N]);
      vstoreStrideT6(elemDta.NU,NPRNC, &vprc_l[0][IPRNC_NU]);
      vstoreStrideT6(elemDta.WX,NPRNC, &vprc_l[0][IPRNC_WX]);
      vstoreStrideT6(elemDta.WY,NPRNC, &vprc_l[0][IPRNC_WY]);

      // ---  Copy result to global memory
      const size_t iprc = (it + ie*dims->ntsk) * (NPRNC*NNEL);
      copyArray(&vprc_l[0][0], gridDta->vprc+iprc, NPRNC*NNEL);
   }
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
KERNEL
void sv2d_clcprn_cl(TTGlobalDouble*      vres,
                    TTGlobalDouble*      vprc,
                    TTGlobalConstDouble* vdjv,
                    TTConstantVprg*      vprg,
                    TTGlobalConstDouble* vprn,
                    TTGlobalConstDouble* vdlg,
                    TTConstantParams*    dims
                    )
{
   // ---  Global task index
   const int it = get_global_id(0);
   if (it == 0) printf("itask: %i / %i\n", it, dims->ntsk);

   // ---  Transfer to the global structure
   TTGridData gridDta;
   gridDta.vprg = vprg;
   gridDta.vdjv = vdjv;
   gridDta.vprn = vprn;
   gridDta.vprc = vprc;
   gridDta.vdlg = vdlg;
   gridDta.vres = NULL;

   // ---  Nodal properties
   sv2d_clcprn(it, &gridDta, dims);
}
