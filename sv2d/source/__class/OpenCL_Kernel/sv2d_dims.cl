//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_XY4_DIMS_CL_ALLREADY_DEFINED
#define SV2D_XY4_DIMS_CL_ALLREADY_DEFINED

#define NO1 0
#define NO2 1
#define NO3 2
#define NO4 3
#define NO5 4
#define NO6 5

#define NDJV    5
#define NNEL    6
#define NET3    4
#define NDLN    3
#define NPRNL   6
#define NPRNC   7 // NPRN-NPRNL
#define NPRE    2
#define NPRG   21

#define IDLG_QX 0
#define IDLG_QY 1
#define IDLG_H  2

#define IPRGL_GRAVITE          0
#define IPRGL_LATITUDE         1
#define IPRGL_FCT_CW_VENT      2
#define IPRGL_VISCO_CST        3
#define IPRGL_VISCO_LM         4
#define IPRGL_VISCO_SMGO       5
#define IPRGL_DECOU_HTRG       6
#define IPRGL_DECOU_HMIN       7
#define IPRGL_DECOU_MAN        8
#define IPRGL_DECOU_UMAX       9
#define IPRGL_DECOU_AMORT     10
#define IPRGL_DECOU_DIF_NU    11
#define IPRGL_DECOU_DRC_NU    12
#define IPRGL_STABI_PECLET    13
#define IPRGL_STABI_AMORT     14
#define IPRGL_STABI_DARCY     15
#define IPRGL_STABI_LAPIDUS   16
#define IPRGL_CMULT_MAN       17
#define IPRGL_CMULT_VENT      10
#define IPRGL_CORIOLIS        19
#define IPRGL_CMULT_VENT_REL  20

#define IPRNO_Z                0
#define IPRNO_N                1
#define IPRNO_ICE_E            2
#define IPRNO_ICE_N            3
#define IPRNO_WND_X            4
#define IPRNO_WND_Y            5

#define IPRNC_U                0
#define IPRNC_V                1
#define IPRNC_H                2
#define IPRNC_N                3
#define IPRNC_NU               4
#define IPRNC_WX               5
#define IPRNC_WY               6

#endif   // SV2D_XY4_DIMS_CL_ALLREADY_DEFINED
