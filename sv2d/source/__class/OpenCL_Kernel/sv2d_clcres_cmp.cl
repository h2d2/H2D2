//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_CLCRES_CMP_CL_ALLREADY_DEFINED
#define SV2D_CLCRES_CMP_CL_ALLREADY_DEFINED

#include "gpu_config.cl"
#include "gpu_util.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"
#include "sv2d_const.cl"
#include "sv2d_util.cl"

//************************************************************************
// Sommaire: sv2d_cmp_T3v_cnv
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_cnv assemble dans
//     la contribution de volume des termes de convection
//     des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     Le résidu :
//        {N} < N1,x ; N2,x ; N3,x > {U Qx}
//     développé donne
//        {N} ( N1,x*U1*Qx1 + N2,x*U2*Qx2 + N3,x*u3*Qx3)
//     qui est écrit ensuite sous la forme
//        {N} < U1*N1,x ; U2*N2,x ; U3*N3,x > {Qx}
//
//************************************************************************
void sv2d_cmp_T3v_cnv(TTSubT3Data* elemDta)
{
   // ---  Termes de convection en X
   const double3 uqx = elemDta->U * elemDta->qx;
   const double3 vqx = elemDta->V * elemDta->qx;
   const double duqxdx = ddx(uqx, elemDta->vdjv);
   const double dvqxdy = ddx(vqx, elemDta->vdjv);
   const double cnvx = UN_6 * (duqxdx + dvqxdy);
   elemDta->vres[NO1][0] = cnvx;
   elemDta->vres[NO2][0] = cnvx;
   elemDta->vres[NO3][0] = cnvx;

   // ---  Termes de convection en Y
   const double3 uqy = elemDta->U * elemDta->qy;
   const double3 vqy = elemDta->V * elemDta->qy;
   const double duqydx = ddx(uqy, elemDta->vdjv);
   const double dvqydy = ddx(vqy, elemDta->vdjv);
   const double cnvy = UN_6 * (duqydx + dvqydy);
   elemDta->vres[NO1][1] = cnvy;
   elemDta->vres[NO2][1] = cnvy;
   elemDta->vres[NO3][1] = cnvy;
}

//************************************************************************
// Sommaire: sv2d_cmp_T3v_cor
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_cor assemble
//     la contribution de volume des termes de Coriolis
//     des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     La matrice lumpée est trop dissipative
//************************************************************************
void sv2d_cmp_T3v_cor(TTSubT3Data* elemDta)
{
   const double CORIOLIS = elemDta->vprg->coriolis;

   // ---  Métriques
   const double detj = elemDta->vdjv[4];

   // ---  Coefficients de Coriolis
   const double cy = UN_24 * CORIOLIS * detj;
   const double cx = -cy;

   // ---  Inconnues
   const double qx1 = VACCESS(elemDta->qx, NO1);
   const double qx2 = VACCESS(elemDta->qx, NO2);
   const double qx3 = VACCESS(elemDta->qx, NO3);
   const double qy1 = VACCESS(elemDta->qy, NO1);
   const double qy2 = VACCESS(elemDta->qy, NO2);
   const double qy3 = VACCESS(elemDta->qy, NO3);

   // ---  Assemblage de VRES
   elemDta->vres[NO1][IDLG_QX] = cx * (qy1 + qy1 + qy2 + qy3);
   elemDta->vres[NO1][IDLG_QY] = cy * (qx1 + qx1 + qx2 + qx3);
   elemDta->vres[NO2][IDLG_QX] = cx * (qy1 + qy2 + qy2 + qy3);
   elemDta->vres[NO2][IDLG_QY] = cy * (qx1 + qx2 + qx2 + qx3);
   elemDta->vres[NO3][IDLG_QX] = cx * (qy1 + qy2 + qy3 + qy3);
   elemDta->vres[NO3][IDLG_QY] = cy * (qx1 + qx2 + qx3 + qx3);
}

//************************************************************************
// Sommaire: SV2D_CMP_RE_V_MAN
//
// Description:
//     La subroutine privée SV2D_CMP_RE_V_MAN assemble
//     la contribution de volume des termes de frottement de Manning
//     des équations de mouvement pour un sous-élément T3.
//     Version avec matrice lumpée.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     La matrice masse est lumpée. De la forme de base:
//        {N} <N> {fr} <N>
//     développée en:
//        [ N1.N1  N1.N2  N1.N3 ]
//        [ N2.N1  N2.N2  N2.N3 ] <N>{fr}
//        [ N3.N1  N3.N2  N3.N3 ]
//     lumpée en:
//        [ N1(N1+N2+N3)      0            0      ]
//        [       0      N2(N1+N2+N3)      0      ] <N>{fr}
//        [       0           0       N3(N1+N2+N3)]
//     ce qui revient à:
//        [ N1 0  0 ]
//        [ 0  N2 0 ] <N>{fr}
//        [ 0  0 N3 ]
//     qui à les mêmes composantes que la matrice:
//         {N}<N>
//
//     Masse non lumpée:
//        Dans Maxima
//        n1(x,y):= 1-x-y;
//        n2(x,y):=x;
//        n3(x,y):=y;
//        u(x,y):=n1(x,y)*u1 + n2(x,y)*u2 + n3(x,y)*u3;
//        integrate(integrate(n1(x,y)*n1(x,y)*u(x,y),y,0,1-x),x,0,1);
//
//      COEF = UN_120*DETJ     !! Matrice complète   (avec cette méthode, on arrive à des écarts
//                                                    par rapport à Hydrosim)
//************************************************************************
void sv2d_cmp_T3v_man(TTSubT3Data* elemDta)
{
   // ---  Coefficient
   const double detj = elemDta->vdjv[4];
   const double coef_hy = UN_24 * detj;   // ! Hydrosim

   // ---  Coefficients de frottements
   const double vfrot1 = coef_hy * VACCESS(elemDta->N, NO1);
   const double vfrot2 = coef_hy * VACCESS(elemDta->N, NO2);
   const double vfrot3 = coef_hy * VACCESS(elemDta->N, NO3);

   // ---  Inconnues
   const double qx1 = VACCESS(elemDta->qx, NO1);
   const double qx2 = VACCESS(elemDta->qx, NO2);
   const double qx3 = VACCESS(elemDta->qx, NO3);
   const double qy1 = VACCESS(elemDta->qy, NO1);
   const double qy2 = VACCESS(elemDta->qy, NO2);
   const double qy3 = VACCESS(elemDta->qy, NO3);

   // ---  Termes de frottement
   const double fx1 = vfrot1 * qx1;
   const double fx2 = vfrot2 * qx2;
   const double fx3 = vfrot3 * qx3;
   const double fy1 = vfrot1 * qy1;
   const double fy2 = vfrot2 * qy2;
   const double fy3 = vfrot3 * qy3;

   // ---  Assemblage de VRES
   elemDta->vres[NO1][IDLG_QX] = (fx1 + fx1 + fx2 + fx3);
   elemDta->vres[NO1][IDLG_QY] = (fy1 + fy1 + fy2 + fy3);
   elemDta->vres[NO2][IDLG_QX] = (fx1 + fx2 + fx2 + fx3);
   elemDta->vres[NO2][IDLG_QY] = (fy1 + fy2 + fy2 + fy3);
   elemDta->vres[NO3][IDLG_QX] = (fx1 + fx2 + fx3 + fx3);
   elemDta->vres[NO3][IDLG_QY] = (fy1 + fy2 + fy3 + fy3);
}

//************************************************************************
// Sommaire: sv2d_cmp_T3v_vnt
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_vnt assemble
//     la contribution de volume des termes de vent
//     des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     Cf. Manning
//************************************************************************
void sv2d_cmp_T3v_vnt(TTSubT3Data* elemDta)
{
   const double CMULT_VENT_REL = elemDta->vprg->cmult_vent_rel;

   // ---  Coefficient
   const double detj = elemDta->vdjv[4];
   const double coef = UN_24 * detj;

   // ---  Termes de vent
   const double3 wx = coef * elemDta->WX;   // cw |w| wx
   const double3 wy = coef * elemDta->WY;   // cw |w| wy

   // ---  Assemblage
   double wxs = sum(wx);
   elemDta->vres[NO1][IDLG_QX] = (wxs + wx.s0);
   elemDta->vres[NO2][IDLG_QX] = (wxs + wx.s1);
   elemDta->vres[NO3][IDLG_QX] = (wxs + wx.s2);
   double wys = sum(wy);
   elemDta->vres[NO1][IDLG_QY] = (wys + wy.s0);
   elemDta->vres[NO2][IDLG_QY] = (wys + wy.s1);
   elemDta->vres[NO3][IDLG_QY] = (wys + wy.s2);
}

//************************************************************************
// Sommaire: sv2d_cmp_T3v_difX
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_difX assemble
//     la contribution de volume des termes de frottement de
//     diffusion des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//     KNE         Connectivités du T3 au sein du T6L
//     KLOCE       Localisation élémentaire de l'élément T3
//     VDJE        Métrique de l'élément T3
//     VPRGL       Propriétés globales
//     VPRN        Propriétés nodales du T6l
//     VPRE        Propriétés élémentaires du T3
//     VDLE        Degrés de liberté du T6l
//
// Sortie:
//     VRES
//
// Notes:
//     {N,x} H_nu TXX
//     Le produit H_nu est la moyenne sur l'élément de volume.
//************************************************************************
void sv2d_cmp_T3v_difX(TTSubT3Data* elemDta)
{
   // ---  Contraintes en x
   const double dudx = ddx(elemDta->U, elemDta->vdjv);
   const double dudy = ddy(elemDta->U, elemDta->vdjv);
   const double dvdx = ddx(elemDta->V, elemDta->vdjv);
   const double txx = (dudx + dudx);
   const double txy = (dudy + dvdx);

   // ---  (H*nu) moyen
   const double h_nu = mean(elemDta->NU);

   // ---  Coefficient
   const double detj = elemDta->vdjv[4];
   const double coef = UN_2 * h_nu / detj;

   // ---  Métriques
   const double vkx = elemDta->vdjv[0];
   const double vex = elemDta->vdjv[1];
   const double vky = elemDta->vdjv[2];
   const double vey = elemDta->vdjv[3];
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);

   // ---  Assemblage
   elemDta->vres[NO1][IDLG_QX] = coef * (vsx * txx + vsy * txy);
   elemDta->vres[NO2][IDLG_QX] = coef * (vkx * txx + vky * txy);
   elemDta->vres[NO3][IDLG_QX] = coef * (vex * txx + vey * txy);
}

//************************************************************************
// Sommaire: sv2d_cmp_T3v_difY
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_difY assemble
//     la contribution de volume des termes de frottement de
//     diffusion des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//     KNE         Connectivités du T3 au sein du T6L
//     KLOCE       Localisation élémentaire de l'élément T3
//     VDJE        Métrique de l'élément T3
//     VPRGL       Propriétés globales
//     VPRN        Propriétés nodales du T6l
//     VPRE        Propriétés élémentaires du T3
//     VDLE        Degrés de liberté du T6l
//
// Sortie:
//     VRES
//
// Notes:
//     {N,x} H_nu TXX
//     Le produit H_nu est la moyenne sur l'élément de volume.
//************************************************************************
void sv2d_cmp_T3v_difY(TTSubT3Data* elemDta)
{
   // ---  Contraintes en y
   const double dudy = ddy(elemDta->U, elemDta->vdjv);
   const double dvdx = ddx(elemDta->V, elemDta->vdjv);
   const double dvdy = ddy(elemDta->V, elemDta->vdjv);
   const double txy = (dvdx + dudy);
   const double tyy = (dvdy + dvdy);

   // ---  (H*nu) moyen
   const double h_nu = mean(elemDta->NU);

   // ---  Coefficient
   const double detj = elemDta->vdjv[4];
   const double coef = UN_2 * h_nu / detj;

   // ---  Métriques
   const double vkx = elemDta->vdjv[0];
   const double vex = elemDta->vdjv[1];
   const double vky = elemDta->vdjv[2];
   const double vey = elemDta->vdjv[3];
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);

   // ---  Assemblage
   elemDta->vres[NO1][IDLG_QY] = coef * (vsx * txy + vsy * tyy);
   elemDta->vres[NO2][IDLG_QY] = coef * (vkx * txy + vky * tyy);
   elemDta->vres[NO3][IDLG_QY] = coef * (vex * txy + vey * tyy);
}

//************************************************************************
// Sommaire: sv2d_cmp_T3v_dif
//
// Description:
//     La subroutine privée sv2d_cmp_T3v_dif assemble dans le
//     résidu Re la contribution de volume des termes de frottement de
//     diffusion des équations de mouvement pour un sous-élément T3.
//
// Entrée:
//     KNE         Connectivités du T3 au sein du T6L
//     KLOCE       Localisation élémentaire de l'élément T3
//     VDJE        Métrique de l'élément T3
//     VPRGL       Propriétés globales
//     VPRN        Propriétés nodales du T6l
//     VPRE        Propriétés élémentaires du T3
//     VDLE        Degrés de liberté du T6l
//
// Sortie:
//     VRES
//
// Notes:
//     {N,x} H_nu TXX
//     Le produit H_nu est la moyenne sur l'élément de volume.
//************************************************************************
void sv2d_cmp_T3v_dif(TTSubT3Data* elemDta)
{
   double txx = 0.0e0;
   double txy = 0.0e0;
   // ---  Contraintes en x
   {
      const double dudx = ddx(elemDta->U, elemDta->vdjv);
      const double dudy = ddy(elemDta->U, elemDta->vdjv);
      txx += (dudx + dudx);
      txy += (dudy);
   }
   // ---  Contraintes en y
   double tyy = 0.0e0;
   {
      const double dvdx = ddx(elemDta->V, elemDta->vdjv);
      const double dvdy = ddy(elemDta->V, elemDta->vdjv);
      txy += (dvdx);
      tyy += (dvdy + dvdy);
   }

   // ---  Coefficient
   double coef = 1.0e0;
   if (true)
   {
      // ---  (H*nu) moyen
      const double h_nu = mean(elemDta->NU);

      // ---  Coefficient
      const double detj = elemDta->vdjv[4];
      coef = UN_2 * h_nu / detj;
   }

   // ---  Métriques
   const double vkx = elemDta->vdjv[0];
   const double vex = elemDta->vdjv[1];
   const double vky = elemDta->vdjv[2];
   const double vey = elemDta->vdjv[3];
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);

   // ---  Assemblage
   elemDta->vres[NO1][IDLG_QX] = coef * (vsx * txx + vsy * txy);
   elemDta->vres[NO1][IDLG_QY] = coef * (vsx * txy + vsy * tyy);
   elemDta->vres[NO2][IDLG_QX] = coef * (vkx * txx + vky * txy);
   elemDta->vres[NO2][IDLG_QY] = coef * (vkx * txy + vky * tyy);
   elemDta->vres[NO3][IDLG_QX] = coef * (vex * txx + vey * txy);
   elemDta->vres[NO3][IDLG_QY] = coef * (vex * txy + vey * tyy);
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_cnt
//
// Description:
//     La subroutine privée sv2d_cmp_T6v_cnt assemble dans le
//     résidu Re la contribution de volume de l'équation de
//     continuité. La divergence du débit est intégrée par partie.
//
// Entrée:
//     VDJE        Métrique de l'élément
//     VPRGL       Propriétés globales
//     VPRN        Propriétés nodales du T6l
//     VPRE        Propriétés élémentaires du T6l
//     VDLE        Degrés de liberté du T6l
//
// Sortie:
//     VRES
//
// Notes:
//     Les coefficients sont pour les métriques du T6
//        - <NT6,x> {NT3}      ! continuité
//        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
//
//     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
//     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
//     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
//     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
//     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
//************************************************************************
void sv2d_cmp_T6v_cnt(TTElemData* elemDta)
{
   TTElemVdjv *vdjv = elemDta->vdjv;
   TTElemVres *vres = elemDta->vres;

   // ---  Valeurs élémentaires moyennes
   double qxs;
   {
      const double qx1 = sum( VACCESS_T3_1(elemDta->qx) );
      const double qx2 = sum( VACCESS_T3_2(elemDta->qx) );
      const double qx3 = sum( VACCESS_T3_3(elemDta->qx) );
      const double qx4 = sum( VACCESS_T3_4(elemDta->qx) );
      qxs = qx1 + qx2 + qx3 + qx4;
   }
   double qys;
   {
      const double qy1 = sum( VACCESS_T3_1(elemDta->qy) );
      const double qy2 = sum( VACCESS_T3_2(elemDta->qy) );
      const double qy3 = sum( VACCESS_T3_3(elemDta->qy) );
      const double qy4 = sum( VACCESS_T3_4(elemDta->qy) );
      qys = qy1 + qy2 + qy3 + qy4;
   }

   // ---  Métriques du T6L
   const double vkx = vdjv[0];
   const double vex = vdjv[1];
   const double vky = vdjv[2];
   const double vey = vdjv[3];
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);

   // ---  Résidu
   vres[NO1][IDLG_H] -= UN_24 * (vsx*qxs + vsy*qys);
   vres[NO3][IDLG_H] -= UN_24 * (vkx*qxs + vky*qys);
   vres[NO5][IDLG_H] -= UN_24 * (vex*qxs + vey*qys);
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_lpd
//
// Description:
//     La subroutine privée sv2d_cmp_T6v_lpd assemble dans le
//     résidu Re la contribution de volume du Lapidus sur le niveau d'eau.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     Les coefficients sont pour les métriques du T6
//        - <NT6,x> {NT3}      ! continuité
//        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
//
//     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
//     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
//     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
//     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
//     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
//************************************************************************
void sv2d_cmp_T6v_lpd(TTElemData* elemDta)
{
   TTElemVdjv *vdjv = elemDta->vdjv;
   TTElemVres *vres = elemDta->vres;

   const double STABI_LAPIDUS = elemDta->vprg->stabi_lapidus;

   // ---  Métriques du T6L
   const double vkx = vdjv[0];
   const double vex = vdjv[1];
   const double vky = vdjv[2];
   const double vey = vdjv[3];
   const double detj = vdjv[4];

   // ---  Degrés de liberté
   const double h1 = VACCESS(elemDta->h, NO1);
   const double h3 = VACCESS(elemDta->h, NO3);
   const double h5 = VACCESS(elemDta->h, NO5);

   // ---  Gradient du niveau d'eau
   const double dhdx = vkx * (h3-h1) + vex * (h5-h1);
   const double dhdy = vky * (h3-h1) + vey * (h5-h1);

   // ---  Coefficients de Lapidus
   const double dhnrm = fmax(norm2(dhdx, dhdy), 1.0e-12);
   const double c = UN_2 * STABI_LAPIDUS * dhnrm / detj;

   // ---  Résidu
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);
   vres[NO1][IDLG_H] += c * (vsx*dhdx + vsy*dhdy);
   vres[NO3][IDLG_H] += c * (vkx*dhdx + vky*dhdy);
   vres[NO5][IDLG_H] += c * (vex*dhdx + vey*dhdy);
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_drc
//
// Description:
//     La subroutine privée sv2d_cmp_T6v_drc assemble dans le
//     résidu Re la contribution de volume du Lapidus sur le niveau d'eau.
//
// Entrée:
//
// Sortie:
//     VRES
//
// Notes:
//     Les coefficients sont pour les métriques du T6
//        - <NT6,x> {NT3}      ! continuité
//        + <NT6,x> {NT6,x}    ! stabilisation Lapidus
//
//     Le calcul de la sous-matrice H-H (lissage) mène à des résultats 4 fois plus
//     grands que le calcul effectué par Hydrosim. Dans le calcul de la variable
//     CLISSEH dans Hydrosim, on multiplie le module calculé par un facteur DEMI
//     alors qu'on devrait retrouver un facteur DEUX. Cette erreur dans Hydrosim
//     explique l'écart observé entre les résultats de H2D2 et Hydrosim.
//************************************************************************
void sv2d_cmp_T6v_drc(TTElemData* elemDta)
{
   TTElemVdjv *vdjv = elemDta->vdjv;
   TTElemVres *vres = elemDta->vres;

   // ---  Coefficient de Darcy
   double c;
   {
      const double3 vn1  = 0; // TODO
      const double3 vn2  = 1;
      const double3 drcy = 0.0; //clc_prnev_DRCY_T3X(elemDta, vn1, vn2);
      const double cm = mean(drcy);
      const double detj = vdjv[4];
      c = UN_2 * cm / detj;
   }

   // ---  Degrés de liberté
   const double h1 = VACCESS(elemDta->h, NO1);
   const double h3 = VACCESS(elemDta->h, NO3);
   const double h5 = VACCESS(elemDta->h, NO5);

   // ---  Métriques du T6L
   const double vkx = vdjv[0];
   const double vex = vdjv[1];
   const double vky = vdjv[2];
   const double vey = vdjv[3];
   const double vsx = -(vkx + vex);
   const double vsy = -(vky + vey);

   // ---  Gradient du niveau d'eau
   const double dhdx = vkx * (h3-h1) + vex * (h5-h1);
   const double dhdy = vky * (h3-h1) + vey * (h5-h1);

   // ---  Résidu
   vres[NO1][IDLG_H] += c * (vsx*dhdx + vsy*dhdy);
   vres[NO3][IDLG_H] += c * (vkx*dhdx + vky*dhdy);
   vres[NO5][IDLG_H] += c * (vex*dhdx + vey*dhdy);
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_grv
//
// Description:
//     La subroutine privée sv2d_cmp_T6v_grv assemble dans le
//     résidu Re la contribution de volume des termes de gravité
//     des équations de mouvement pour un élément T6L.
//
// Entrée:
//     VDJE        Métrique de l'élément T6
//     VPRGL       Propriétés globales
//     VPRN        Propriétés nodales du T6l
//     VPRE        Propriétés élémentaires du T6l
//     VDLE        Degrés de liberté du T6l
//
// Sortie:
//     VRES
//
// Notes:
//     Le résidu est :
//        {N} < N1 ; N2 ; N3 > {H} < N1,x ; N2,x ; N3,x > {h}
//************************************************************************
void sv2d_cmp_T6v_grv(TTElemData* elemDta)
{
   TTElemVdjv *vdjv = elemDta->vdjv;
   TTElemVres *vres = elemDta->vres;

   const double GRAVITE = elemDta->vprg->gravite;

   // ---  Dérivées
   const double cqh = GRAVITE * UN_96;
   const double dhdx = cqh * ddx(VACCESS_T3X(elemDta->h), elemDta->vdjv);
   const double dhdy = cqh * ddy(VACCESS_T3X(elemDta->h), elemDta->vdjv);

   // ---  Profondeur nodales
   double p1 = VACCESS(elemDta->H, NO1);
   double p2 = VACCESS(elemDta->H, NO2);
   double p3 = VACCESS(elemDta->H, NO3);
   double p4 = VACCESS(elemDta->H, NO4);
   double p5 = VACCESS(elemDta->H, NO5);
   double p6 = VACCESS(elemDta->H, NO6);

   // ---  Profondeurs élémentaires
   const double pe1 = p1 + p2 + p6;
   const double pe2 = p2 + p3 + p4;
   const double pe3 = p6 + p4 + p5;
   const double pe4 = p4 + p6 + p2;

   //!!!!!!!!!!!!! Ajouter la pression dynamique (SV2D_CMULT_PDYN) !!!!!!!!!!!!!!!!1

   // ---  Métriques du T6L
   const double vkx = vdjv[0];
   const double vex = vdjv[1];
   const double vky = vdjv[2];
   const double vey = vdjv[3];

   // ---  Assemblage
   vres[NO1][IDLG_QX] = (pe1 + p1) * dhdx;
   vres[NO1][IDLG_QY] = (pe1 + p1) * dhdy;
   vres[NO2][IDLG_QX] = (pe1 + p2 + pe2 + p2 + pe4 + p2) * dhdx;
   vres[NO2][IDLG_QY] = (pe1 + p2 + pe2 + p2 + pe4 + p2) * dhdy;
   vres[NO3][IDLG_QX] = (pe2 + p3) * dhdx;
   vres[NO3][IDLG_QY] = (pe2 + p3) * dhdy;
   vres[NO4][IDLG_QX] = (pe2 + p4 + pe3 + p4 + pe4 + p4) * dhdx;
   vres[NO4][IDLG_QY] = (pe2 + p4 + pe3 + p4 + pe4 + p4) * dhdy;
   vres[NO5][IDLG_QX] = (pe3 + p5) * dhdx;
   vres[NO5][IDLG_QY] = (pe3 + p5) * dhdy;
   vres[NO6][IDLG_QX] = (pe3 + p6 + pe4 + p6 + pe1 + p6) * dhdx;
   vres[NO6][IDLG_QY] = (pe3 + p6 + pe4 + p6 + pe1 + p6) * dhdy;
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_man
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void sv2d_cmp_T6v_cnv(TTElemData* elemDta)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h,  it3);
      t3Dta.qx = VACCESS_T3_n(elemDta->qx, it3);
      t3Dta.qy = VACCESS_T3_n(elemDta->qy, it3);
      t3Dta.U  = VACCESS_T3_n(elemDta->N,  it3);
      t3Dta.V  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.H  = VACCESS_T3_n(elemDta->H,  it3);
      //t3Dta.N  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.NU = VACCESS_T3_n(elemDta->NU, it3);
      //t3Dta.WX = VACCESS_T3_n(elemDta->WX, it3);
      //t3Dta.WY = VACCESS_T3_n(elemDta->WY, it3);

      // ---  Initialise
      initArray (&t3Dta.vres[0][0], 3*NDLN);

      // ---  Calcule
      sv2d_cmp_T3v_cnv(&t3Dta);        // Convection

      // ---  Scatter
      addArray(&t3Dta.vres[0][0], &elemDta->vres[KNE_T3[it3][0]][0], NDLN);
      addArray(&t3Dta.vres[1][0], &elemDta->vres[KNE_T3[it3][1]][0], NDLN);
      addArray(&t3Dta.vres[2][0], &elemDta->vres[KNE_T3[it3][2]][0], NDLN);
   }
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_dif
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void sv2d_cmp_T6v_dif(TTElemData* elemDta)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h, it3);
      //t3Dta.qx = VACCESS_T3_n(elemDta->qx,it3);
      //t3Dta.qy = VACCESS_T3_n(elemDta->qy,it3);
      t3Dta.U  = VACCESS_T3_n(elemDta->U, it3);
      t3Dta.V  = VACCESS_T3_n(elemDta->V, it3);
      t3Dta.H  = VACCESS_T3_n(elemDta->H, it3);
      //t3Dta.N  = VACCESS_T3_n(elemDta->N, it3);
      //t3Dta.NU = VACCESS_T3_n(elemDta->NU, it3);
      //t3Dta.WX = VACCESS_T3_n(elemDta->WX, it3);
      //t3Dta.WY = VACCESS_T3_n(elemDta->WY, it3);

      // ---  Initialise
      initArray (&t3Dta.vres[0][0], 3*NDLN);

      // ---  Calcule
      sv2d_cmp_T3v_difX(&t3Dta);        // Diffusion
      sv2d_cmp_T3v_difY(&t3Dta);        // Diffusion

      // ---  Scatter
      addArray(&t3Dta.vres[0][0], &elemDta->vres[KNE_T3[it3][0]][0], NDLN);
      addArray(&t3Dta.vres[1][0], &elemDta->vres[KNE_T3[it3][1]][0], NDLN);
      addArray(&t3Dta.vres[2][0], &elemDta->vres[KNE_T3[it3][2]][0], NDLN);
   }
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_man
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void sv2d_cmp_T6v_man(TTElemData* elemDta)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h,  it3);
      t3Dta.qx = VACCESS_T3_n(elemDta->qx, it3);
      t3Dta.qy = VACCESS_T3_n(elemDta->qy, it3);
      //t3Dta.U  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.V  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.H  = VACCESS_T3_n(elemDta->H,  it3);
      t3Dta.N  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.NU = VACCESS_T3_n(elemDta->NU, it3);
      //t3Dta.WX = VACCESS_T3_n(elemDta->WX, it3);
      //t3Dta.WY = VACCESS_T3_n(elemDta->WY, it3);

      // ---  Initialise
      initArray (&t3Dta.vres[0][0], 3*NDLN);

      // ---  Calcule
      sv2d_cmp_T3v_man(&t3Dta);        // Manning

      // ---  Scatter
      addArray(&t3Dta.vres[0][0], &elemDta->vres[KNE_T3[it3][0]][0], NDLN);
      addArray(&t3Dta.vres[1][0], &elemDta->vres[KNE_T3[it3][1]][0], NDLN);
      addArray(&t3Dta.vres[2][0], &elemDta->vres[KNE_T3[it3][2]][0], NDLN);
   }
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_cor
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void sv2d_cmp_T6v_cor(TTElemData* elemDta)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h,  it3);
      t3Dta.qx = VACCESS_T3_n(elemDta->qx, it3);
      t3Dta.qy = VACCESS_T3_n(elemDta->qy, it3);
      //t3Dta.U  = VACCESS_T3_n(elemDta->U,  it3);
      //t3Dta.V  = VACCESS_T3_n(elemDta->V,  it3);
      //t3Dta.H  = VACCESS_T3_n(elemDta->H,  it3);
      //t3Dta.N  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.NU = VACCESS_T3_n(elemDta->NU, it3);
      //t3Dta.WX = VACCESS_T3_n(elemDta->WX, it3);
      //t3Dta.WY = VACCESS_T3_n(elemDta->WY, it3);

      // ---  Initialise
      initArray (&t3Dta.vres[0][0], 3*NDLN);

      // ---  Calcule
      sv2d_cmp_T3v_cor(&t3Dta);        // Coriolis

      // ---  Scatter
      addArray(&t3Dta.vres[0][0], &elemDta->vres[KNE_T3[it3][0]][0], NDLN);
      addArray(&t3Dta.vres[1][0], &elemDta->vres[KNE_T3[it3][1]][0], NDLN);
      addArray(&t3Dta.vres[2][0], &elemDta->vres[KNE_T3[it3][2]][0], NDLN);
   }
}

//************************************************************************
// Sommaire: sv2d_cmp_T6v_cor
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void sv2d_cmp_T6v_vnt(TTElemData* elemDta)
{
   TTSubT3Data t3Dta;
   t3Dta.vprg = elemDta->vprg;

   // ---  Métriques du sous-élément
   t3Dta.vdjv[0] = UN_2 * elemDta->vdjv[0];
   t3Dta.vdjv[1] = UN_2 * elemDta->vdjv[1];
   t3Dta.vdjv[2] = UN_2 * elemDta->vdjv[2];
   t3Dta.vdjv[3] = UN_2 * elemDta->vdjv[3];
   t3Dta.vdjv[4] = UN_4 * elemDta->vdjv[4];

   // ---  Boucle sur les sous-éléments
   //      ============================
   for (int it3 = 0; it3 < 4; ++it3)
   {
      // ---  Le T3 interne est inversé
      if (it3 == 3) {
         t3Dta.vdjv[0] = -t3Dta.vdjv[0];
         t3Dta.vdjv[1] = -t3Dta.vdjv[1];
         t3Dta.vdjv[2] = -t3Dta.vdjv[2];
         t3Dta.vdjv[3] = -t3Dta.vdjv[3];
      }

      // ---  Données du sous-T3
      //t3Dta.h  = VACCESS_T3_n(elemDta->h,  it3);
      //t3Dta.qx = VACCESS_T3_n(elemDta->qx, it3);
      //t3Dta.qy = VACCESS_T3_n(elemDta->qy, it3);
      t3Dta.U = VACCESS_T3_n(elemDta->U, it3);
      t3Dta.V = VACCESS_T3_n(elemDta->V, it3);
      //t3Dta.H  = VACCESS_T3_n(elemDta->H,  it3);
      //t3Dta.N  = VACCESS_T3_n(elemDta->N,  it3);
      //t3Dta.NU = VACCESS_T3_n(elemDta->NU, it3);
      t3Dta.WX = VACCESS_T3_n(elemDta->WX, it3);
      t3Dta.WY = VACCESS_T3_n(elemDta->WY, it3);

      // ---  Initialise
      initArray (&t3Dta.vres[0][0], 3*NDLN);

      // ---  Calcule
      sv2d_cmp_T3v_vnt(&t3Dta);        // Coriolis

      // ---  Scatter
      addArray(&t3Dta.vres[0][0], &elemDta->vres[KNE_T3[it3][0]][0], NDLN);
      addArray(&t3Dta.vres[1][0], &elemDta->vres[KNE_T3[it3][1]][0], NDLN);
      addArray(&t3Dta.vres[2][0], &elemDta->vres[KNE_T3[it3][2]][0], NDLN);
   }
}

#ifdef JJJJJJJJJJJJJJJJJJJJJJJJJJJJJ
//
//************************************************************************
// Sommaire:  SV2D_CMP_FE_V_SLR
//
// Description:
//     La subroutine privée SV2D_CMP_FE_V_SLR assemble dans le
//     résidu Re la contribution de volume des sollicitations réparties.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
void
sv2d_cmp_fe_v_slr(
  common& cmn,
  arr_ref<double, 2> vres,
  arr_cref<double> vdjv,
  arr_cref<double> /* vprgl */,
  double* /* vprn */,
  arr_cref<double> /* vpre */,
  double* /* vdlg */,
  double* vslr)
{
  vres(dim1(1, ).dim2(1, ));
  vdjv(dim1(1, ));
  vslr(dim1(1, ).dim2(1, ));
  //
  // Pas utilisé
  //
  // ---  Connectivités du T6 externe
  //-----------------------------------------------------------------------
  //
  // ---  Métriques
  double detj_t6 = UN_24 * vdjv(5);
  // 1/4 POUR LE DJ SUR LE T3
  double detj_t3 = UN_4 * detj_t6;
  //
  // ---  Sollicitations nodales
  const int no1 = 1;
  double su1 = detj_t3 * vslr[NO1][1];
  double sv1 = detj_t3 * vslr[NO1][2];
  double sh1 = detj_t6 * vslr[NO1][3];
  const int no2 = 2;
  double su2 = detj_t3 * vslr[NO2][1];
  double sv2 = detj_t3 * vslr[NO2][2];
  const int no3 = 3;
  double su3 = detj_t3 * vslr[NO3][1];
  double sv3 = detj_t3 * vslr[NO3][2];
  double sh3 = detj_t6 * vslr[NO3][3];
  const int no4 = 4;
  double su4 = detj_t3 * vslr[NO4][1];
  double sv4 = detj_t3 * vslr[NO4][2];
  const int no5 = 5;
  double su5 = detj_t3 * vslr[NO5][1];
  double sv5 = detj_t3 * vslr[NO5][2];
  double sh5 = detj_t6 * vslr[NO5][3];
  const int no6 = 6;
  double su6 = detj_t3 * vslr[NO6][1];
  double sv6 = detj_t3 * vslr[NO6][2];
  //
  // ---  Sommations pour chaque sous-element
  double sue1 = su1 + su2 + su6;
  double sve1 = sv1 + sv2 + sv6;
  double sue2 = su2 + su3 + su4;
  double sve2 = sv2 + sv3 + sv4;
  double sue3 = su6 + su4 + su5;
  double sve3 = sv6 + sv4 + sv5;
  double sue4 = su4 + su6 + su2;
  double sve4 = sv4 + sv6 + sv2;
  double she = sh1 + sh3 + sh5;
  //
  // ---  Contributions
  vres[NO1][1] += sue1 + su1;
  vres[NO1][2] += sve1 + sv1;
  vres[NO1][3] += she + sh1;
  vres[NO2][1] += sue1 + su2 + sue2 + su2 + sue4 + su2;
  vres[NO2][2] += sve1 + sv2 + sve2 + sv2 + sve4 + sv2;
  //     VRES(3[NO2] = VRES(3[NO2] + ZERO
  vres[NO3][1] += sue2 + su3;
  vres[NO3][2] += sve2 + sv3;
  vres[NO3][3] += she + sh3;
  vres[NO4][1] += sue2 + su4 + sue3 + su4 + sue4 + su4;
  vres[NO4][2] += sve2 + sv4 + sve3 + sv4 + sve4 + sv4;
  //     VRES(3[NO4] = VRES(3[NO4] + ZERO
  vres[NO5][1] += sue3 + su5;
  vres[NO5][2] += sve3 + sv5;
  vres[NO5][3] += she + sh5;
  vres[NO6][1] += sue3 + su6 + sue1 + su6 + sue4 + su6;
  vres[NO6][2] += sve3 + sv6 + sve1 + sv6 + sve4 + sv6;
}

//
//************************************************************************
// Sommaire: SV2D_CMP_RE_S_DIF
//
// Description:
//     La fonction SV2D_CMP_RE_S_DIF calcule la partie de diffusion du
//     résidu Re pour un élément de surface.
//
// Entrée:
//
// Sortie:
//
// Notes:
//     1) On prend pour acquis que KNE et KLOCE sont pour un T3 et que
//        l’élément de contour L2 à calculer forme le premier côté (noeuds 1-2).
//
//     2) {NL2} H_nu < NT3,x > { q/H }
//        Le produit H.nu est la moyenne sur l'élément de volume.
//************************************************************************
void
sv2d_cmp_s_dif(
  common& cmn,
  arr_ref<double, 2> vres,
  arr_cref<int> kne,
  arr_cref<double> vdjv,
  arr_cref<double> vdjs,
  arr_cref<double> /* vprg */,
  double* vprn,
  arr_cref<double> vprev,
  arr_cref<double> /* vpres */,
  double* /* vdlg */,
  double* vdlg)
{
  vres(dim1(1, ).dim2(1, ));
  kne(dimension(3));
  vdjv(dim1(1, ));
  vdjs(dim1(1, ));
  vprn(dim1(1, ).dim2(1, ));
  vprev(dimension(2));
  vdlg(dim1(1, ).dim2(1, ));
  // COMMON sv2d_cbs_cdt1
  int& sv2d_iprno_h = cmn.sv2d_iprno_h;
  int& sv2d_iprno_decou_diff = cmn.sv2d_iprno_decou_diff;
  //
  //
  // 3 POUR CHAQUE L2
  //
  //-----------------------------------------------------------------------
  //
  // ---  Connectivités du T3
  int no1 = kne(1);
  int no2 = kne(2);
  int no3 = kne(3);
  //
  // ---  Normales et métriques de l'élément L2
  // VNX =  VTY
  double vnx = vdjs(2);
  // VNY = -VTX
  double vny = -vdjs(1);
  double djl2 = vdjs(3);
  //
  // ---  Métriques du T3
  double vkx = vdjv(1);
  double vex = vdjv(2);
  double vky = vdjv(3);
  double vey = vdjv(4);
  double djt3 = vdjv(5);
  //
  // ---  Inconnues
  double qx1 = vdlg[NO1][1];
  double qy1 = vdlg[NO1][2];
  double qx2 = vdlg[NO2][1];
  double qy2 = vdlg[NO2][2];
  double qx3 = vdlg[NO3][1];
  double qy3 = vdlg[NO3][2];
  //
  // ---  Valeurs nodales
  // Profondeur nodales
  double h1 = vprn[IPRNO_H][NO1];
  // Visco pour le découvreme
  double c1 = vprn(sv2d_iprno_decou_diff[NO1];
  double h2 = vprn[IPRNO_H][NO2];
  double c2 = vprn(sv2d_iprno_decou_diff[NO2];
  double h3 = vprn[IPRNO_H][NO3];
  double c3 = vprn(sv2d_iprno_decou_diff[NO3];
  //
  // ---  Visco totale
  // Visco totale + découvrement
  c1 += vprev(2);
  c2 += vprev(2);
  c3 += vprev(2);
  //
  // ---  qx / h
  // Recalculé car issu de VRHS
  double u1 = qx1 / h1;
  double v1 = qy1 / h1;
  double u2 = qx2 / h2;
  double v2 = qy2 / h2;
  double u3 = qx3 / h3;
  double v3 = qy3 / h3;
  //
  // ---  (H*nu) moyen
  double h_nu = UN_3 * (c1 * h1 + c2 * h2 + c3 * h3);
  //
  // ---  Coefficient
  double coef = cmn.sv2d_cmult_intgctr * UN_4 * h_nu * (djl2 / djt3);
  //
  // ---  Contraintes
  double dudx = vkx * (u2 - u1) + vex * (u3 - u1);
  double dudy = vky * (u2 - u1) + vey * (u3 - u1);
  double dvdx = vkx * (v2 - v1) + vex * (v3 - v1);
  double dvdy = vky * (v2 - v1) + vey * (v3 - v1);
  double txx = coef * (dudx + dudx);
  double txy = coef * (dudy + dvdx);
  double tyy = coef * (dvdy + dvdy);
  //
  // ---  Assemble
  vres[NO1][1] = vres[NO1][1] - (txx * vnx + txy * vny);
  vres[NO2][1] = vres[NO2][1] - (txx * vnx + txy * vny);
  //
  vres[NO1][2] = vres[NO1][2] - (txy * vnx + tyy * vny);
  vres[NO2][2] = vres[NO2][2] - (txy * vnx + tyy * vny);
}

//
//************************************************************************
// Sommaire: SV2D_CMP_RE_S_CNT
//
// Description:
//     La fonction SV2D_CBS_RE_S_CNT calcule la partie de Darcy de la
//     continuité du résidu Re.
//
// Entrée:
//
// Sortie:
//
// Notes:
//     {NL2} <NT3_1,x ; NT3_2,x ; NT3_3,x>
//
//************************************************************************
void
sv2d_cmp_s_cnt(
  common& cmn,
  arr_ref<double, 2> vres,
  arr_cref<int> kne,
  arr_cref<double> vdjv,
  arr_cref<double> vdjs,
  arr_cref<double> /* vprg */,
  double* vprn,
  arr_cref<double> /* vprev */,
  arr_cref<double> /* vpres */,
  double* /* vdlg */,
  double* vdlg)
{
  vres(dim1(1, ).dim2(1, ));
  kne(dimension(3));
  vdjv(dim1(1, ));
  vdjs(dim1(1, ));
  vprn(dim1(1, ).dim2(1, ));
  vdlg(dim1(1, ).dim2(1, ));
  // COMMON sv2d_cbs_cdt1
  int& iprno_decou_drcy = cmn.iprno_decou_drcy;
  //
  //
  // 3 POUR CHAQUE L2
  //
  //-----------------------------------------------------------------------
  //
  // ---  Connectivités des sommets du T6L
  int no1 = kne(1);
  int no3 = kne(2);
  int no5 = kne(3);
  //
  // ---  Normales et métriques de l'élément L2
  // VNX =  VTY
  double vnx = vdjs(2);
  // VNY = -VTX
  double vny = -vdjs(1);
  double djl2 = vdjs(3);
  //
  // ---  Métriques du T3
  double vkx = vdjv(1);
  double vex = vdjv(2);
  double vky = vdjv(3);
  double vey = vdjv(4);
  double djt3 = vdjv(5);
  //
  // ---  Inconnues
  double h1 = vdlg[NO1][3];
  double h3 = vdlg[NO3][3];
  double h5 = vdlg[NO5][3];
  //
  // ---  Gradients
  double dhdx = vkx * (h3 - h1) + vex * (h5 - h1);
  double dhdy = vky * (h3 - h1) + vey * (h5 - h1);
  //
  // ---  Coefficients de Darcy
  double c1 = vprn(iprno_decou_drcy[NO1];
  double c3 = vprn(iprno_decou_drcy[NO3];
  double c5 = vprn(iprno_decou_drcy[NO5];
  double cm = UN_3 * (c1 + c3 + c5);
  double c = cmn.sv2d_cmult_intgctr * cm * (djl2 / djt3);
  //
  // ---  Sous-matrice H.H
  vres[NO1][3] = vres[NO1][3] - c * (vnx * dhdx + vny * dhdy);
  vres[NO3][3] = vres[NO3][3] - c * (vnx * dhdx + vny * dhdy);
}

#endif

#endif // define SV2D_CLCRES_CMP_CL_ALLREADY_DEFINED
