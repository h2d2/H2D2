//************************************************************************
// --- Copyright (c) INRS 2003-2013
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_TYPES_CL_ALLREADY_DEFINED
#define SV2D_TYPES_CL_ALLREADY_DEFINED

#include "gpu_config.cl"
#include "gpu_types.cl"

#include "sv2d_dims.cl"

typedef struct TTParams
{
   int nnel;
   int ndjv;
   int ndln;
   int nprg;
   int nprn;
   int nprc;
   int npre;
   int nele;
   int ntsk;
} TTParams;
typedef CONSTANT struct TTParams TTConstantParams;

typedef struct TTPrg
{
   double gravite;
   double coriolis;
   double visco_cst;
   double visco_lm;
   double visco_smgo;
   double visco_binf;
   double visco_bsup;
   double decou_htrg;
   double decou_hmin;
   double decou_dhst;
   double decou_un_hdif;
   double decou_pena_h;
   double decou_pena_q;
   double decou_poro;
   double decou_man;
   double decou_umax;
   double decou_amort;
   double decou_con_fact;
   double decou_gra_fact;
   double decou_dif_nu;
   double decou_drc_nu;
   double stabi_peclet;
   double stabi_amort;
   double stabi_darcy;
   double stabi_lapidus;
   double cmult_con;
   double cmult_man;
   double cmult_gra;
   double cmult_pdyn;
   double cmult_vent;
   double cmult_intgctr;
   double pnumr_penalite;
   double pnumr_delprt;
   double pnumr_delmin;
   double pnumr_omegakt;
   double cmult_vent_rel;
   int    fct_cw_vent;
   bool   pnumr_prtprel;
   bool   pnumr_prtprno;
} TTPrg;
typedef CONSTANT struct TTPrg TTConstantVprg;
typedef GLOBAL   struct TTPrg TTGlobalVprg;

typedef TTConstantVprg      TTGridVprg;

typedef TTGlobalConstDouble TTGridVdjv;
typedef TTGlobalConstDouble TTElemVdjv;

typedef TTGlobalConstDouble TTGridVprn;
typedef TTGlobalConstDouble TTElemVprn[NPRNL];

#ifdef H2D2_GPU_SV2D_COMPILE_VPRN
typedef TTGlobalDouble      TTGridVprc;
typedef TTDouble            TTElemVprc[NPRNC];
#else
typedef TTGlobalConstDouble TTGridVprc;
typedef TTGlobalConstDouble TTElemVprc[NPRNC];
#endif

typedef TTDouble            TTElemVpre[NPRE];

typedef TTGlobalConstDouble TTGridVdlg;
typedef TTGlobalConstDouble TTElemVdlg[NDLN];

typedef TTGlobalDouble      TTGridVres;
typedef TTDouble            TTElemVres[NDLN];


typedef struct TTGridData
{
   TTGridVprg *vprg;
   TTGridVdjv *vdjv;
   TTGridVprn *vprn;
   TTGridVprc *vprc;
   TTGridVdlg *vdlg;
   TTGridVres *vres;
} TTGridData;

typedef struct TTElemData
{
   TTGridVprg *vprg;
   TTElemVdjv *vdjv;
   TTElemVprn *vprn;
   TTElemVres *vres;
   double8 h;
   double8 qx;
   double8 qy;
   double8 U;
   double8 V;
   double8 H;
   double8 N;
   double8 NU;
   double8 WX;
   double8 WY;
} TTElemData;

/*
typedef struct TTElemDataDif
{
   double8 U;
   double8 V;
   double8 H;
   double8 A;
   TTGridVprg *vprg;
   TTElemVdjv *vdjv;
   TTElemVres *vres;
} TTElemDataDif;
*/

typedef struct TTSubT3Data
{
   TTGridVprg *vprg;
   TTDouble    vdjv[5];
   TTDouble    vres[3][NDLN];
   double3 h;
   double3 qx;
   double3 qy;
   double3 U;
   double3 V;
   double3 H;
   double3 N;
   double3 NU;
   double3 WX;
   double3 WY;
} TTSubT3Data;

#endif   // SV2D_TYPES_CL_ALLREADY_DEFINED
