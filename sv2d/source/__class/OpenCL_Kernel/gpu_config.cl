//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef GPU_CONFIG_CL_ALLREADY_DEFINED
#define GPU_CONFIG_CL_ALLREADY_DEFINED

#pragma OPENCL EXTENSION cl_khr_fp64 : enable

/*
#undef IS_CPLR_NDIVIA
#undef IS_CPLR_AMD
#undef IS_CPLR_INTEL
#ifdef cl_nv_compiler_options
#  define IS_NVIDIA 1
#else
#  error Unknown compiler
#endif
*/
//#define IS_NVIDIA 1


//https://github.com/savage309/GPAPI/blob/master/kernel.cl
#ifdef __OPENCL_C_VERSION__
    #define KERNEL   __kernel
    #define GLOBAL   __global
    #define CONSTANT __constant
    #define DEVICE
    #define LOCAL    __local
    #define SHARED   __local
    #define PRIVATE  __private
    #define RESTRICT restrict
    #define MEMORY_BARRIER barrier(CLK_LOCAL_MEM_FENCE)
#endif

#ifdef __CUDACC__
    #define KERNEL extern "C" __global__
    #define GLOBAL
    #define CONSTANT
    #define DEVICE   __device__
    #define LOCAL    __shared__
    #define SHARED   __shared__
    #define PRIVATE  "Equivalent of OpenCL __private: Must be Adapted For CUDA""
    #define RESTRICT __restrict__
    #define MEMORY_BARRIER __syncthreads
#endif

#if (!defined __OPENCL_C_VERSION__) && (!defined __CUDACC__)
    #include <math.h>
    #define KERNEL   inline
    #define GLOBAL
    #define CONSTANT
    #define DEVICE   inline
    #define SHARED
    #define PRIVATE
    #define RESTRICT
    #define MEMORY_BARRIER
int get_global_id(int i) { return i; }
#endif

#ifndef NULL
#  define NULL 0
#endif
         
#endif   // GPU_CONFIG_CL_ALLREADY_DEFINED
