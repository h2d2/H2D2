//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SPMATH_CL_ALLREADY_DEFINED
#define SPMATH_CL_ALLREADY_DEFINED

double norm2(double x, double y) { return sqrt(x*x + y*y); }
double pow2 (double x) { return x*x; }
double pow3 (double x) { return x*x*x; }

/*
   Iterative cube root with precision
*/
double root3(double x, double prec) {
    if (!x) return 0.0e0;

    const int N = 3;
    double d, r = 1.0e0;
    do {
        d = (x / pow2(r) - r) / N;
        r += d;
    }
    while (d >= prec || d <= -prec);
    return r;
}

#endif // define SPMATH_CL_ALLREADY_DEFINED

#ifdef SPMATH_CL_UNIT_TEST
#include "gpu_config.cl"
 
#ifdef DEBUG
#define assert(x) \
		if (! (x)) \
		{ \
			printf("Assert(%s) failed in %s:%d", #x, __FUNCTION__, __LINE__); \
			exit(-1); \
		}
#else
	#define assert(X)
#endif

// http://cas.ee.ic.ac.uk/people/dt10/research/rngs-gpu-mwc64x.html
uint MWC64X(uint2 *state)
{
    enum {A = 4294883355U};
    uint x = (*state).x, c=(*state).y;    // Unpack the state
    uint res = x^c;                       // Calculate the result
    uint hi  = mul_hi(x,A);               // Step the RNG
    x = x*A+c;
    c = hi+(x<c);
    *state = (uint2)(x,c);                // Pack the state back up
    return res;                           // Return the next result
}

#define MAX_LOOP 10000
KERNEL
void sv2d_xbs_asmres_cl()
{
   const uint2 state = (get_global_id(0), get_global_id(0)+1);
   for (int i = 0; i < MAX_LOOP; ++i)
   {
      uint j = MWC64X(&state);
      
      assert (pow2(j) == (j*j));
      assert (pow3(j) == (j*j*j));
      assert (root3(pow3(j)) == j);
   }
}

#endif // define SPMATH_CL_UNIT_TEST
