//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************
//
//************************************************************************
// Description:
//
// Functions:
//************************************************************************
#ifndef SV2D_CLCPREV_CL_ALLREADY_DEFINED
#define SV2D_CLCPREV_CL_ALLREADY_DEFINED

#include "gpu_config.cl"
#include "gpu_util.cl"

#include "sv2d_dims.cl"
#include "sv2d_types.cl"
#include "sv2d_const.cl"
#include "sv2d_util.cl"

#include "sphdro.cl"

//************************************************************************
// Sommaire:  SV2D_CBS_CLCPREVE
//
// Description:
//     Calcul des propriétés élémentaires des éléments de volume pour
//     un sous-élément T3.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double
clc_preev_T3_vis(const TTSubT3Data* elemDta)
{
   const double VISCO_CST    = elemDta->vprg->visco_cst;
   const double VISCO_LM     = elemDta->vprg->visco_lm;
   const double VISCO_SMGO   = elemDta->vprg->visco_smgo;

   const double detjt3 = elemDta->vdjv[4];

   // ---  Coefficients finaux (longueur de mélange & Smagorinsky)
   const double clmfin = VISCO_LM;
   const double csmfin = VISCO_SMGO * sqrt(detjt3);

   // ---  Viscosité turbulente finale sur les T3
   const double pm = mean(elemDta->H);
   const double nut = clmfin*pm + csmfin;

   // ---  Calcul de la viscosité physique
   const double dudx = ddx(elemDta->U, elemDta->vdjv);
   const double dudy = ddy(elemDta->U, elemDta->vdjv);
   const double dvdx = ddx(elemDta->V, elemDta->vdjv);
   const double dvdy = ddy(elemDta->V, elemDta->vdjv);
   double vis = (nut*nut * sqrt(DEUX*dudx*dudx +
                                DEUX*dvdy*dvdy +
                                (dudy+dvdx) * (dudy+dvdx))) / detjt3;
   vis += VISCO_CST;

   return vis;        // Visco physique
}

//************************************************************************
// Sommaire:  SV2D_CBS_CLCPREVE
//
// Description:
//     Calcul des propriétés élémentaires des éléments de volume pour
//     un sous-élément T3.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double
clc_preev_T3_vnm(const TTSubT3Data* elemDta)
{
   const double STABI_PECLET = elemDta->vprg->stabi_peclet;

   // ---  Calcul de la viscosité numérique
   const double um  = mean(elemDta->U);
   const double vm  = mean(elemDta->V);
   const double pm  = mean(elemDta->H);
   const double vkx = elemDta->vdjv[0];
   const double vex = elemDta->vdjv[1];
   const double vky = elemDta->vdjv[2];
   const double vey = elemDta->vdjv[3];
   double vnm = sp_hdro_peclet(UN, um, vm, pm, vey, -vex, -vky, vkx) / STABI_PECLET;

   return vnm;
}

//************************************************************************
// Sommaire:  clc_preev_T3
//
// Description:
//     Calcul des propriétés élémentaires des éléments de volume pour
//     un sous-élément T3.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
double2
clc_preev_T3(const TTSubT3Data* elemDta)
{
   const double vis = clc_preev_T3_vis(elemDta);   // Visco physique
   const double vnm = clc_preev_T3_vnm(elemDta);   // Visco numérique

   return (double2) (vis, vis + vnm);
}

#endif   // SV2D_CLCPREV_CL_ALLREADY_DEFINED


#ifdef SV2D_CLCPREV_CL_UNIT_TEST
 
#ifdef DEBUG
#define assert(x) \
		if (! (x)) \
		{ \
			printf("Assert(%s) failed in %s:%d", #x, __FUNCTION__, __LINE__); \
			exit(-1); \
		}
#else
	#define assert(X)
#endif

KERNEL
void sv2d_clc_preev_T3_gpu()
{
   TTSubT3Data elemDta;
   double2 r = clc_preev_T3(&elemDta);
}

#endif // define SV2D_CLCPREV_CL_UNIT_TEST
