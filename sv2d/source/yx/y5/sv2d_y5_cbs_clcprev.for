C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cbs_clcprev.for,v 1.34 2016/05/01 17:43:53 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_CLCPREV
C     SUBROUTINE SV2D_Y5_CBS_CLCPREVE
C   Private:
C     SUBROUTINE SV2D_Y5_CBS_CLCPREV_1E
C
C************************************************************************

C************************************************************************
C Sommaire:  SV2D_Y5_CBS_CLCPREV
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPREV(VCORG,
     &                               KNGV,
     &                               VDJV,
     &                               VPRGL,
     &                               VPRNO,
     &                               VPREV,
     &                               VDLG,
     &                               IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_CLCPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2, EG_CMMN_NELV)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IC, IE
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Appel du parent
      CALL SV2D_CBS_CLCPREV(VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VDLG,
     &                      IERR)

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(IC, IE)

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         CALL SV2D_Y5_CBS_CLCPREV_1E(VCORG,
     &                               KNGV(:,IE),
     &                               VDJV(:,IE),
     &                               VPRGL,
     &                               VPRNO,
     &                               VPREV(:,:,IE),
     &                               VDLG,
     &                               IERR)

20    CONTINUE
!$omp end do
10    CONTINUE

      IERR = ERR_OMP_RDC()
!$omp end parallel

      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_Y5_CBS_CLCPREVE
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume pour
C     un élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPREVE(VCORG,
     &                                KNE,
     &                                VDJE,
     &                                VPRGL,
     &                                VPRNO,
     &                                VPRE,
     &                                VDLG,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_CLCPREVE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNE  (EG_CMMN_NCELV)
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

C---     Appel du parent
      CALL SV2D_CBS_CLCPREVE(VCORG,
     &                       KNE,
     &                       VDJE,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPRE,
     &                       VDLG,
     &                       IERR)

C---     Traitement Y5
      CALL SV2D_Y5_CBS_CLCPREV_1E(VCORG,
     &                            KNE,
     &                            VDJE,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPRE,
     &                            VDLG,
     &                            IERR)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_Y5_CBS_CLSV2D_Y5_CBS_CLCPREV_1ECPREVE
C
C Description:
C     Calcul des propriétés élémentaires des éléments de volume pour
C     un élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Paul D. Bates, Jean–Michel Hervouet. "A new method for
C        moving–boundary hydrodynamic problems in shallow water"
C        Proc. R. Soc. Lond. A 1999 455 3107-3128; 
C        DOI: 10.1098/rspa.1999.0442. Published 8 August 1999.
C        Href est considéré représentatif de l'élément
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_CLCPREV_1E(VCORG,
     &                                  KNE,
     &                                  VDJE,
     &                                  VPRGL,
     &                                  VPRNO,
     &                                  VPRE,
     &                                  VDLG,
     &                                  IERR)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNE  (EG_CMMN_NCELV)
      REAL*8  VDJE (EG_CMMN_NDJV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPRE (LM_CMMN_NPREV_D1, LM_CMMN_NPREV_D2)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sphdro.fi'

      REAL*8  H(3), HMIN, HMAX, HREF
      REAL*8  P(3)
      REAL*8  Z(3), ZMIN, ZMAX, ZEPS
      REAL*8  KSI, ETA, SREL
      INTEGER I !, I1, I2, I3
      INTEGER STTS
C-----------------------------------------------------------------------

C---     Valeurs nodales
      H(:) = VDLG (           3, KNE(1::2))
      Z(:) = VPRNO(SV2D_IPRNO_Z, KNE(1::2))
      P(:) = VPRNO(SV2D_IPRNO_H, KNE(1::2))

C---     Niveau du noeud de profondeur max
      I = 1
      IF (P(2) > P(I)) I = 2
      IF (P(3) > P(I)) I = 3
      HREF = H(I)

C---     Détermine l'état
      HMIN = MIN(H(1), H(2), H(3))
      HMAX = MAX(H(1), H(2), H(3))
      ZMIN = MIN(Z(1), Z(2), Z(3))
      ZMAX = MAX(Z(1), Z(2), Z(3))
      ZEPS = SV2D_DECOU_HMIN * (1.0D0 + 1.0D-3)
      IF     (HMIN .GE. ZMAX+ZEPS) THEN
         STTS = SV2D_LM_COUVERT
      ELSEIF (HMAX .LE. ZMIN+ZEPS) THEN
         STTS = SV2D_LM_DECOUVERT
      ELSEIF (HREF .GE. ZMIN .AND. HREF .LE. ZMAX) THEN
         STTS = SV2D_LM_PARTIEL_1   ! Flooding from below
      ELSE
         STTS = SV2D_LM_PARTIEL_2   ! Flooding from above
      ENDIF

C---     Calcul du taux de couverture
      SREL = 0.0D0
!!!      SELECT  CASE (STTS)
!!!         CASE (SV2D_LM_COUVERT)
!!!            SREL = 1.0D0
!!!         CASE (SV2D_LM_DECOUVERT)
!!!            SREL = 0.0D0
!!!         CASE (SV2D_LM_PARTIEL_1)
!!!            I1 = 1          ! Ordonne les Z
!!!            IF (Z(2) < Z(I1)) I1 = 2
!!!            IF (Z(3) < Z(I1)) I1 = 3
!!!            I3 = 1
!!!            IF (Z(2) > Z(I3)) I3 = 2
!!!            IF (Z(3) > Z(I3)) I3 = 3
!!!            I2 = 6 - I1 - I3
!!!            IF     (HREF .LT. Z(I1)) THEN
!!!               SREL = 0.0D0
!!!            ELSEIF (HREF .LT. Z(I2)) THEN
!!!               ETA = (HREF-Z(I1)) / (Z(I3)-Z(I1))   ! triangle 1-2-3
!!!               KSI = (HREF-Z(I1)) / (Z(I2)-Z(I1))   ! rectangle en 1
!!!               SREL = KSI*ETA
!!!            ELSEIF (HREF .LT. Z(I3)) THEN
!!!               ETA = (HREF-Z(I3)) / (Z(I1)-Z(I3))   ! triangle 3-1-2
!!!               KSI = (HREF-Z(I3)) / (Z(I2)-Z(I3))   ! rectangle en 3
!!!               SREL = 1.0D0 - KSI*ETA
!!!            ELSE
!!!               SREL = 1.0D0
!!!            ENDIF
!!!         CASE (SV2D_LM_PARTIEL_2)
!!!            I1 = 1          ! Ordonne les Z
!!!            IF (Z(2) < Z(I1)) I1 = 2
!!!            IF (Z(3) < Z(I1)) I1 = 3
!!!            I3 = 1
!!!            IF (Z(2) > Z(I3)) I3 = 2
!!!            IF (Z(3) > Z(I3)) I3 = 3
!!!            I2 = 6 - I1 - I3
!!!            IF     (HMIN .LT. Z(I1)) THEN
!!!               SREL = 0.0D0
!!!            ELSEIF (HMIN .LT. Z(I2)) THEN
!!!               ETA = (HMIN-Z(I1)) / (Z(I3)-Z(I1))   ! triangle 1-2-3
!!!               KSI = (HMIN-Z(I1)) / (Z(I2)-Z(I1))   ! rectangle en 1
!!!               SREL = KSI*ETA
!!!            ELSEIF (HMIN .LE. Z(I3)) THEN
!!!               ETA = (HMIN-Z(I3)) / (Z(I1)-Z(I3))   ! triangle 3-1-2
!!!               KSI = (HMIN-Z(I3)) / (Z(I2)-Z(I3))   ! rectangle en 3
!!!               SREL = 1.0D0 - KSI*ETA
!!!            ELSE
!!!               SREL = 1.0D0
!!!            ENDIF
!!!D        CASE DEFAULT
!!!D           CALL ERR_ASR(STTS .GE. SV2D_LM_COUVERT .AND.
!!!D    &                   STTS .LE. SV2D_LM_PARTIEL_2)
!!!      END SELECT
!!!D     CALL ERR_ASR(SREL .GE. 0.0D0)
!!!D     CALL ERR_ASR(SREL .LE. 1.0D0)

!!!C---     Modifie les PRNO
!!!      SELECT  CASE (STTS)
!!!         CASE (SV2D_LM_COUVERT)
!!!            CONTINUE
!!!         CASE (SV2D_LM_DECOUVERT)
!!!!!!            VFLN = SV2D_DECOU_AMORT
!!!!!!            VMAN = SV2D_DECOU_MAN
!!!!!!            FROTT = VFLN + SV2D_GRAVITE*VMAN*VMAN * VMOD * PRFA**R_4_3_NEG ! Hydrosim
!!!!!!            VPRNO(SV2D_IPRNO_COEFF_FROT, KNE(:)) = FROTT                ! g n2 |u| / H**(4/3)
!!!            VPRNO(SV2D_IPRNO_COEFF_CNVT, KNE(:)) = SV2D_DECOU_CON_FACT  ! Facteur de convection
!!!            VPRNO(SV2D_IPRNO_COEFF_GRVT, KNE(:)) = SV2D_DECOU_GRA_FACT  ! Facteur de gravité
!!!            VPRNO(SV2D_IPRNO_COEFF_DIFF, KNE(:)) = SV2D_DECOU_DIF_NU    ! Diffusion de decou.
!!!            VPRNO(SV2D_IPRNO_COEFF_DRCY, KNE(:)) = SV2D_DECOU_DRC_NU    ! Darcy
!!!            VPRNO(SV2D_IPRNO_COEFF_VENT, KNE(:)) = 0.0D0                ! cw |w| rho_air/rho_eau
!!!            VPRNO(SV2D_IPRNO_COEFF_PORO, KNE(:)) = SV2D_DECOU_PORO      ! Porosité de décou.
!!!            VPRNO(SV2D_IPRNO_DECOU_PENA, KNE(:)) = 1.0D0                ! Facteur de pénalité
!!!         CASE DEFAULT
!!!            VPRNO(SV2D_IPRNO_COEFF_GRVT, KNE(:)) = SV2D_DECOU_GRA_FACT
!!!      END SELECT

C---     Propriété élémentaire
      VPRE(SV2D_IPREV_STTS,1) = STTS
      VPRE(SV2D_IPREV_STTS,2) = SREL
      VPRE(SV2D_IPREV_STTS,3) =  -1
      VPRE(SV2D_IPREV_STTS,4) =  -1

      IERR = ERR_TYP()
      RETURN
      END
