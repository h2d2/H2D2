C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cbs_reqprm.for,v 1.1 2016/02/12 17:42:42 secretyv Exp $
C
C Notes:
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_REQPRM
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_Y5_CBS_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C   - 3 PRNO de plus que YS mais 2 de plus que Y3
C   - Les PRE sont encodée suivant leurs deux dimensions:
C     3 PRE pour chacun des 4 sous-éléments: 2*1000 + 4
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_REQPRM(TGELV,
     &                              NPRGL,
     &                              NPRGLL,
     &                              NPRNO,
     &                              NPRNOL,
     &                              NPREV,
     &                              NPREVL,
     &                              NPRES,
     &                              NSOLC,
     &                              NSOLCL,
     &                              NSOLR,
     &                              NSOLRL,
     &                              NDLN,
     &                              NDLEV,
     &                              NDLES,
     &                              ASURFACE,
     &                              ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------

C---     Initialise les paramètres de l'élément parent
      CALL SV2D_Y4_CBS_REQPRM(TGELV,
     &                        NPRGL,
     &                        NPRGLL,
     &                        NPRNO,
     &                        NPRNOL,
     &                        NPREV,
     &                        NPREVL,
     &                        NPRES,
     &                        NSOLC,
     &                        NSOLCL,
     &                        NSOLR,
     &                        NSOLRL,
     &                        NDLN,
     &                        NDLEV,
     &                        NDLES,
     &                        ASURFACE,
     &                        ESTLIN)

      NPRGLL = NPRGLL + 2             ! Nb de propriétés globales lues
      NPRGL  = NPRGL  + 2             ! Nb de propriétés globales
      NPRNO  = NPRNO  + 1             ! Nb de propriétés nodales
      NPREV  = 3004                   ! Nb de propriétés élémentaires de volume

      RETURN
      END
