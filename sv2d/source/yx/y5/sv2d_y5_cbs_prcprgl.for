C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cbs_prcprgl.for,v 1.5 2015/11/16 20:52:45 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_PRCPRGL
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_Y5_CBS_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_PRCPRGL (VPRGL,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      CALL SV2D_Y4_CBS_PRCPRGL(VPRGL, IERR)

      SV2D_DECOU_PENA_H = VPRGL(SV2D_IPRGL_DECOU_PENA_H)
      SV2D_DECOU_PENA_Q = VPRGL(SV2D_IPRGL_DECOU_PENA_Q)

      IERR = ERR_TYP()
      RETURN
      END

