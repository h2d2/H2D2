C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cbs_pslprgl.for,v 1.8 2016/04/15 18:29:14 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y5_CBS_PSLPRGL
C   Private:
C     SUBROUTINE SV2D_Y5_PSLPRGL_E
C     SUBROUTINE SV2D_Y5_INI_IPRGL
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_Y5_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_CBS_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CBS_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      CALL SV2D_Y5_INI_IPRGL()
      CALL SV2D_Y5_PSLPRGL_E(VPRGL, IERR)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_PSLPRGL_E(VPRGL, IERR)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'eacnst.fi'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I, IE

      INTEGER CHKVAL
C-----------------------------------------------------------------------

      CALL SV2D_Y4_PSLPRGL_E(VPRGL, IERR)

C---     Coefficient de pénalité de découvrement
      I = SV2D_IPRGL_DECOU_PENA_H
      IE = CHKVAL(I, VPRGL(I), ZERO, GRAND, 'MSG_DECOUVREMENT_PENA_H')
      I = SV2D_IPRGL_DECOU_PENA_Q
      IE = CHKVAL(I, VPRGL(I), ZERO, GRAND, 'MSG_DECOUVREMENT_PENA_Q')

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_Y5_CBS_INI_IPRGL
C
C Description:
C     La fonction privée SV2D_Y5_CBS_INI_IPRGL initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y5_INI_IPRGL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      SV2D_IPRGL_GRAVITE         =  1
      SV2D_IPRGL_LATITUDE        =  2
      SV2D_IPRGL_FCT_CW_VENT     =  3
      SV2D_IPRGL_VISCO_CST       =  4
      SV2D_IPRGL_VISCO_LM        =  5
      SV2D_IPRGL_VISCO_SMGO      =  6
      SV2D_IPRGL_VISCO_BINF      =  7
      SV2D_IPRGL_VISCO_BSUP      =  8
      SV2D_IPRGL_DECOU_HTRG      =  9
      SV2D_IPRGL_DECOU_HMIN      = 10
      SV2D_IPRGL_DECOU_PENA_H    = 11
      SV2D_IPRGL_DECOU_PENA_Q    = 12
      SV2D_IPRGL_DECOU_MAN       = 13
      SV2D_IPRGL_DECOU_UMAX      = 14
      SV2D_IPRGL_DECOU_PORO      = 15
      SV2D_IPRGL_DECOU_AMORT     = 16
      SV2D_IPRGL_DECOU_CON_FACT  = 17
      SV2D_IPRGL_DECOU_GRA_FACT  = 18
      SV2D_IPRGL_DECOU_DIF_NU    = 19
      SV2D_IPRGL_DECOU_DIF_PE    = 20
      SV2D_IPRGL_DECOU_DRC_NU    = 21
      SV2D_IPRGL_STABI_PECLET    = 22
      SV2D_IPRGL_STABI_AMORT     = 23
      SV2D_IPRGL_STABI_DARCY     = 24
      SV2D_IPRGL_STABI_LAPIDUS   = 25
      SV2D_IPRGL_CMULT_CON       = 26
      SV2D_IPRGL_CMULT_GRA       = 27
      SV2D_IPRGL_CMULT_PDYN      = -1
      SV2D_IPRGL_CMULT_MAN       = 28
      SV2D_IPRGL_CMULT_VENT      = 29
      SV2D_IPRGL_CMULT_INTGCTR   = 30
      SV2D_IPRGL_PNUMR_PENALITE  = 31
      SV2D_IPRGL_PNUMR_DELPRT    = 32
      SV2D_IPRGL_PNUMR_DELMIN    = 33
      SV2D_IPRGL_PNUMR_OMEGAKT   = 34
      SV2D_IPRGL_PNUMR_PRTPREL   = 35
      SV2D_IPRGL_PNUMR_PRTPRNO   = 36
      SV2D_IPRGL_CORIOLIS        = 37
      SV2D_IPRGL_CMULT_VENT_REL  = 38

      RETURN
      END
