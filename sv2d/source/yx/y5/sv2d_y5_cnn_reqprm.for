C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_y5_cnn_reqprm.for,v 1.8 2016/02/12 17:42:42 secretyv Exp $
C
C Élément:
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_Y5_CNN_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Redirige sur CBS
C************************************************************************
      SUBROUTINE SV2D_Y5_CNN_REQPRM(TGELV,
     &                              NPRGL,
     &                              NPRGLL,
     &                              NPRNO,
     &                              NPRNOL,
     &                              NPREV,
     &                              NPREVL,
     &                              NPRES,
     &                              NSOLC,
     &                              NSOLCL,
     &                              NSOLR,
     &                              NSOLRL,
     &                              NDLN,
     &                              NDLEV,
     &                              NDLES,
     &                              ASURFACE,
     &                              ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y5_CNN_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------

      CALL SV2D_Y5_CBS_REQPRM(TGELV,
     &                        NPRGL,
     &                        NPRGLL,
     &                        NPRNO,
     &                        NPRNOL,
     &                        NPREV,
     &                        NPREVL,
     &                        NPRES,
     &                        NSOLC,
     &                        NSOLCL,
     &                        NSOLR,
     &                        NSOLRL,
     &                        NDLN,
     &                        NDLEV,
     &                        NDLES,
     &                        ASURFACE,
     &                        ESTLIN)

      RETURN
      END
