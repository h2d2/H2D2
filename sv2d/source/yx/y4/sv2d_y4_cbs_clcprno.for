C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y4_CBS_CLCPRNO
C     SUBROUTINE SV2D_Y4_CBS_CLCPRNEV
C     SUBROUTINE SV2D_Y4_CBS_CLCPRNES
C   Private:
C     SUBROUTINE SV2D_Y4_CBS_CLCPRN_1N
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_Y4_CBS_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C     CALCUL DES PROPRIETES NODALES DÉPENDANT DE LA SOLUTION (DS)
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  cw(w).|w|
C                    DS    16)  POROSITÉ
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Par rapport à SV2D_YS_CBS_CLCPRNO, on ajoute le vent relatif.
C     Par rapport à SV2D_Y2_CBS_CLCPRNO, on enlève l'hystérèse.
C     Par rapport à SV2D_Y2_CBS_CLCPRNO, on ajoute le Pe de découvrement
C
C     L'imposition des noeuds milieux devrait être faite dans une
C     fonction séparée, pour bien séparer les concepts
C************************************************************************
      SUBROUTINE SV2D_Y4_CBS_CLCPRNO (VCORG,
     &                                KNGV,
     &                                VDJV,
     &                                VPRGL,
     &                                VPRNO,
     &                                VDLG,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y4_CBS_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  BATHY, EPAIGL
      REAL*8  PRFE, PRFA
      INTEGER IC, IE, IN
      INTEGER IPV, IPH
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6

      INTEGER SV2D_Y4_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
!$omp& private(BATHY, EPAIGL)
!$omp& private(PRFE, PRFA)

C---     Impose les niveaux d'eau sur les noeuds milieux
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VDLG(3,NO2) = (VDLG(3,NO1)+VDLG(3,NO3))*UN_2
         VDLG(3,NO4) = (VDLG(3,NO3)+VDLG(3,NO5))*UN_2
         VDLG(3,NO6) = (VDLG(3,NO5)+VDLG(3,NO1))*UN_2
      ENDDO
!$omp end do
      ENDDO

C---     Profondeurs effective et absolue
!$omp  do
      DO IN=1,EG_CMMN_NNL
         BATHY  = VPRNO(SV2D_IPRNO_Z,IN)
         EPAIGL = 0.9D0 * VPRNO(SV2D_IPRNO_ICE_E,IN)
         PRFE   = VDLG(3,IN) - (BATHY + EPAIGL) ! Prof effective
         PRFA   = MAX(PRFE, SV2D_DECOU_HMIN)    ! Prof absolue
         VPRNO(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNO(IPH,IN) = PRFA                   ! Prof absolue
      ENDDO
!$omp end do

!!!C---     Impose les profondeurs sur les noeuds milieux
!!!      DO IC=1,EG_CMMN_NELCOL
!!!!$omp  do
!!!      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
!!!         NO1  = KNGV(1,IE)
!!!         NO2  = KNGV(2,IE)
!!!         NO3  = KNGV(3,IE)
!!!         NO4  = KNGV(4,IE)
!!!         NO5  = KNGV(5,IE)
!!!         NO6  = KNGV(6,IE)
!!!
!!!         VPRNO(IPV,NO2) = (VPRNO(IPV,NO1)+VPRNO(IPV,NO3))*UN_2
!!!         VPRNO(IPV,NO4) = (VPRNO(IPV,NO3)+VPRNO(IPV,NO5))*UN_2
!!!         VPRNO(IPV,NO6) = (VPRNO(IPV,NO5)+VPRNO(IPV,NO1))*UN_2
!!!
!!!!!!C           Profondeur linéaire
!!!!!!C           Noeud abaissé si un noeud sommet est découvert
!!!!!!         VPRNO(IPH,NO2) = (VPRNO(IPH,NO1)+VPRNO(IPH,NO3))*UN_2
!!!!!!         VPRNO(IPH,NO4) = (VPRNO(IPH,NO3)+VPRNO(IPH,NO5))*UN_2
!!!!!!         VPRNO(IPH,NO6) = (VPRNO(IPH,NO5)+VPRNO(IPH,NO1))*UN_2
!!!      ENDDO
!!!!$omp end do
!!!      ENDDO

C---     Boucle sur les noeuds
!$omp  do
      DO IN=1,EG_CMMN_NNL
         IERR = SV2D_Y4_CBS_CLCPRN_1N(VPRGL, VPRNO(1,IN), VDLG(1,IN))
      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y4_CBS_CLCPRNEV
C
C Description:
C     Calcul des propriétés nodales d'un élément de volume,
C     dépendantes de VDLE.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y4_CBS_CLCPRNEV(VPRG,
     &                                VPRN,
     &                                VDLE,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y4_CBS_CLCPRNEV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRG(LM_CMMN_NPRGL)
      REAL*8  VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8  VDLE(LM_CMMN_NDLN,  EG_CMMN_NNELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL
      REAL*8  PRFE, PRFA

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6

      INTEGER SV2D_Y4_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2
      VDLE(3,NO4) = (VDLE(3,NO3)+VDLE(3,NO5))*UN_2
      VDLE(3,NO6) = (VDLE(3,NO5)+VDLE(3,NO1))*UN_2

C---     Profondeurs effective et absolue
!$omp simd
!$omp& safelen(6)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(BATHY, EPAIGL)
!$omp& private(PRFE, PRFA)
      DO IN=1,EG_CMMN_NNELV
         BATHY  = VPRN(SV2D_IPRNO_Z,IN)
         EPAIGL = 0.9D0 * VPRN(SV2D_IPRNO_ICE_E,IN)
         PRFE   = VDLE(3,IN) - (BATHY + EPAIGL) ! Prof effective
         PRFA   = MAX(PRFE, SV2D_DECOU_HMIN)    ! Prof absolue
         VPRN(IPV,IN) = PRFE                    ! Prof effective temp.
         VPRN(IPH,IN) = PRFA                    ! Prof absolue
      ENDDO
!$omp end simd

!!!C---     Impose les profondeurs sur les noeuds milieux
!!!      VPRN(IPV,NO2) = (VPRN(IPV,NO1)+VPRN(IPV,NO3))*UN_2
!!!      VPRN(IPV,NO4) = (VPRN(IPV,NO3)+VPRN(IPV,NO5))*UN_2
!!!      VPRN(IPV,NO6) = (VPRN(IPV,NO5)+VPRN(IPV,NO1))*UN_2
!!!
!!!!!!C        Profondeur linéaire
!!!!!!C        Noeud abaissé si un noeud sommet est découvert
!!!!!!      VPRN(IPH,NO2) = (VPRN(IPH,NO1)+VPRN(IPH,NO3))*UN_2
!!!!!!      VPRN(IPH,NO4) = (VPRN(IPH,NO3)+VPRN(IPH,NO5))*UN_2
!!!!!!      VPRN(IPH,NO6) = (VPRN(IPH,NO5)+VPRN(IPH,NO1))*UN_2

C---     Boucle sur les noeuds
!$omp simd
!$omp& safelen(6)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(IERR)
      DO IN=1,EG_CMMN_NNELV
         IERR = SV2D_Y4_CBS_CLCPRN_1N(VPRG, VPRN(1,IN), VDLE(1,IN))
      ENDDO
!$omp end simd

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y4_CBS_CLCPRNES
C
C Description:
C     Calcul des propriétés nodales d'un élément de surface,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y4_CBS_CLCPRNES(VPRG,
     &                                VPRN,
     &                                VDLE,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y4_CBS_CLCPRNES
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRG(LM_CMMN_NPRGL)
      REAL*8  VPRN(LM_CMMN_NPRNO, EG_CMMN_NNELS)
      REAL*8  VDLE(LM_CMMN_NDLN,  EG_CMMN_NNELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  BATHY, EPAIGL
      REAL*8  PRFE, PRFA
      INTEGER IN
      INTEGER IPV, IPH

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3

      INTEGER SV2D_Y4_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2

C---     Profondeurs effective et absolue
!$omp simd
!$omp& safelen(3)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(BATHY, EPAIGL)
!$omp& private(PRFE, PRFA)
      DO IN=1,EG_CMMN_NNELS
         BATHY  = VPRN(SV2D_IPRNO_Z,IN)
         EPAIGL = 0.9D0 * VPRN(SV2D_IPRNO_ICE_E,IN)
         PRFE   = VDLE(3,IN) - (BATHY + EPAIGL) ! Prof effective
         PRFA   = MAX(PRFE, SV2D_DECOU_HMIN)    ! Prof absolue
         VPRN(IPV,IN) = PRFE                    ! Prof effective temp.
         VPRN(IPH,IN) = PRFA                    ! Prof absolue
      ENDDO
!$omp end simd

!!!C---     Impose les profondeurs sur les noeuds milieux
!!!      VPRN(IPV,NO2) = (VPRN(IPV,NO1) + VPRN(IPV,NO3))*UN_2
!!!
!!!!!!C        Profondeur linéaire
!!!!!!C        Noeud abaissé si un noeud sommet est découvert
!!!!!!      VPRN(IPH,NO2) = (VPRN(IPH,NO1) + VPRN(IPH,NO3))*UN_2

C---     Boucle sur les noeuds
!$omp simd
!$omp& safelen(3)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(IERR)
      DO IN=1,EG_CMMN_NNELS
         IERR = SV2D_Y4_CBS_CLCPRN_1N(VPRG, VPRN(1,IN), VDLE(1,IN))
      ENDDO
!$omp end simd

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y4_CBS_CLCPRN1N
C
C Description:
C     Calcul des propriétés nodales pour un noeud,
C     dépendantes de VDLN
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y4_CBS_CLCPRN1N(VPRG,
     &                                VPRN,
     &                                VDLN,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y4_CBS_CLCPRN1N
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRG(LM_CMMN_NPRGL)
      REAL*8  VPRN(LM_CMMN_NPRNO)
      REAL*8  VDLN(LM_CMMN_NDLN)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL
      REAL*8  PRFE, PRFA

      INTEGER SV2D_Y4_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Profondeurs effective et absolue
      BATHY  = VPRN(SV2D_IPRNO_Z)
      EPAIGL = 0.9D0 * VPRN(SV2D_IPRNO_ICE_E)
      PRFE   = VDLN(3) - (BATHY + EPAIGL)    ! Prof effective
      PRFA   = MAX(PRFE, SV2D_DECOU_HMIN)    ! Prof absolue
      VPRN(IPV) = PRFE                       ! Prof effective temp.
      VPRN(IPH) = PRFA                       ! Prof absolue

      IERR = SV2D_Y4_CBS_CLCPRN_1N(VPRG, VPRN, VDLN)

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y4_CBS_CLCPRN_1N
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRG        Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     En PRFA, un noeud milieu va se retrouver rehaussé si un noeud sommet
C     est découvert. Il y a un impact sur le coefficient de frottement
C     qui est diminué.
C     ULIM vise à "freiner" les fortes vitesses, celles au-delà de UMAX.
C     Jusqu'à UMAX, la loi est linéaire, puis exp 1/3
C     Le vent est mis à 0 en présence de glace. Mais si le vent est relatif,
C     il y aura quand même la contribution du courant.
C
C     La version de calcul VN1/VN2 avec ERF n'apporte rien en terme de précision
C     ou de convergence de Newton. Elle est laissée en commentaire pour ref.
C************************************************************************
      INTEGER 
     &FUNCTION SV2D_Y4_CBS_CLCPRN_1N(VPRG, 
     &                               VPRN,
     &                               VDLN)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRG(LM_CMMN_NPRGL)
      REAL*8  VPRN(LM_CMMN_NPRNO)
      REAL*8  VDLN(LM_CMMN_NDLN)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sphdro.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8, PARAMETER :: RHO_AIR = 1.2475D+00
      REAL*8, PARAMETER :: RHO_EAU = 1.0000D+03
      REAL*8, PARAMETER :: RHO_REL = RHO_AIR / RHO_EAU

      REAL*8, PARAMETER :: R_4_3_NEG = -4.0D0/3.0D0
      REAL*8, PARAMETER :: R_1_3     =  1.0D0/3.0D0

      REAL*8  ALFA
      REAL*8  SG, MU
      REAL*8  H1, H2
      REAL*8  PRFE, PRFA, UN_PRFA
      REAL*8  PORO, VFLN, FCVT, FGRA, VDIF, VPEC, VDRC
      REAL*8  NWet, NDry, VBTMWet, VBTMDry, VBTM, VMAN, VFRO
      REAL*8  VX, VY, VMOD
      REAL*8  WX, WY, WMOD, VENT
      REAL*8  VN1, VN2
C-----------------------------------------------------------------------
      REAL*8 ULIM, UEXP, U
      UEXP(U) = (U-SV2D_DECOU_UMAX+UN)**R_1_3 + SV2D_DECOU_UMAX-UN
      ULIM(U) = MIN(UEXP(MAX(U,SV2D_DECOU_UMAX)), U)
C-----------------------------------------------------------------------

C---     Profondeur
      PRFE = VPRN(SV2D_IPRNO_V)           ! Prof effective nodale
      PRFA = VPRN(SV2D_IPRNO_H)           ! Prof absolue linéaire
      UN_PRFA = UN / PRFA                 ! Inverse prof absolue

      H1 = SV2D_DECOU_HMIN
      H2 = SV2D_DECOU_HTRG
C---     Alfa - version linéaire
!!!      ALFA = (PRFE-H1)/(H2-H1)
!!!      ALFA = MIN(UN, MAX(ZERO, ALFA))
!!!      VN2  = ALFA
!!!      VN1  = UN - VN2
C---     Alfa - version continue ERF (c.f. note)
      SG = (H2-H1) / 4.0D0    ! 2*Sigma - Écart type
      MU = (H2+H1) / 2.0D0    ! Moyenne
      VN2 = UN_2 * (UN + ERF((PRFE-MU)/(SG*SQRT(2.0D0))))
      VN1 = UN - VN2

C---     Module de la vitesse
      VX = VDLN(1) * UN_PRFA
      VY = VDLN(2) * UN_PRFA
      VMOD = HYPOT(VX, VY)

C---     Module du vent relatif
      WX = VPRN(SV2D_IPRNO_WND_X) - SV2D_CMULT_VENT_REL*VX
      WY = VPRN(SV2D_IPRNO_WND_Y) - SV2D_CMULT_VENT_REL*VY
      WMOD = HYPOT(WX, WY)
      VENT = RHO_REL * SP_HDRO_CW(WMOD, SV2D_FCT_CW_VENT) * WMOD

C---     Manning partiel : wet & dry
      NWet = HYPOT(VPRN(SV2D_IPRNO_N), VPRN(SV2D_IPRNO_ICE_N)) ! Manning global
      NDry = SV2D_DECOU_MAN                                    ! Manning découvrement
      VBTMWet = NWet*NWet * PRFA**R_4_3_NEG                    ! Hydrosim
      VBTMDry = NDry*NDry * PRFA**R_4_3_NEG                    ! Hydrosim

C---     Paramètres variables pour le découvrement
      VMOD = VN1*ULIM(VMOD)          + VN2*VMOD
      PORO = VN1*SV2D_DECOU_PORO     + VN2 !*UN
      VFLN = VN1*SV2D_DECOU_AMORT    + VN2*SV2D_STABI_AMORT
      VBTM = VN1*VBTMDry             + VN2*SV2D_CMULT_MAN*VBTMWet
      FCVT = VN1*SV2D_DECOU_CON_FACT + VN2*SV2D_CMULT_CON
      FGRA = VN1*SV2D_DECOU_GRA_FACT + VN2*SV2D_CMULT_GRA
      VDIF = VN1*SV2D_DECOU_DIF_NU  !+ VN2*0.0
      VPEC = VN1*SV2D_DECOU_DIF_PE   + VN2*SV2D_STABI_PECLET
      VDRC = VN1*SV2D_DECOU_DRC_NU   + VN2*SV2D_STABI_DARCY
      VENT =                         + VN2*SV2D_CMULT_VENT*VENT

C---     Frottement
      ! L'utilisation de la masse lumped pour découpler le frottement
      ! mène à des résultats très différents d'Hydrosim.
      ! FROTT = SV2D_GRAVITE*VMAN*VMAN * UMOD * (UN_PRFA**R_4_3)
      ! FROTT = VFLN + SV2D_GRAVITE*VMAN*VMAN * VMOD * PRFA**R_4_3_NEG ! Hydrosim
      VFRO = SV2D_GRAVITE*VBTM*VMOD

C---     Valeurs nodales
      VPRN(SV2D_IPRNO_U)          = VDLN(1) * UN_PRFA ! U
      VPRN(SV2D_IPRNO_V)          = VDLN(2) * UN_PRFA ! V
      VPRN(SV2D_IPRNO_H)          = PRFA              ! Prof absolue
      VPRN(SV2D_IPRNO_COEFF_CNVT) = FCVT              ! Facteur de convection
      VPRN(SV2D_IPRNO_COEFF_GRVT) = FGRA              ! Facteur de gravité
      VPRN(SV2D_IPRNO_COEFF_FROT) = VFRO              ! g n2 |u| / H**(4/3)
      VPRN(SV2D_IPRNO_COEFF_DIFF) = VDIF              ! Nu laminaire de découvrement
      VPRN(SV2D_IPRNO_COEFF_PE)   = VPEC              ! Peclet
      VPRN(SV2D_IPRNO_COEFF_DMPG) = VFLN              ! Damping
      VPRN(SV2D_IPRNO_COEFF_VENT) = VENT              ! cw |w| rho_air/rho_eau
      VPRN(SV2D_IPRNO_COEFF_DRCY) = VDRC              ! Darcy
      VPRN(SV2D_IPRNO_COEFF_PORO) = PORO

      SV2D_Y4_CBS_CLCPRN_1N = ERR_OK
      RETURN
      END FUNCTION SV2D_Y4_CBS_CLCPRN_1N
