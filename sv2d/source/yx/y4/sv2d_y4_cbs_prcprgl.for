C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire : SV2D_Y4_CBS_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y4_CBS_PRCPRGL (VPRGL,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y4_CBS_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      
      REAL*8  V
      INTEGER IV, IVX, IVY, IVH
C-----------------------------------------------------------------------
      REAL*8 CMUL
      REAL*8 F
      CMUL(F) = MERGE(1.0D0, 0.0D0, F .LT. 0.0D0)
C-----------------------------------------------------------------------

C---     Appel du parent
      CALL SV2D_Y3_CBS_PRCPRGL(VPRGL, IERR)

C---     le Peclet de découvrement
      SV2D_DECOU_DIF_PE    = VPRGL(SV2D_IPRGL_DECOU_DIF_PE) 

C---     Fonction du vent et vent relatif
      V = VPRGL(SV2D_IPRGL_FCT_CW_VENT)
      SV2D_FCT_CW_VENT    = ABS( NINT(V) )   ! Écrase Y3
      SV2D_CMULT_VENT_REL = CMUL(V)

C---     Delta min de perturbation
      SV2D_PNUMR_DELMIN   = VPRGL(SV2D_IPRGL_PNUMR_DELMIN)

C---     Les coefficients pour les intégrales de contour
      V  = VPRGL(SV2D_IPRGL_CMULT_INTGCTR)
      IV = NINT((V/1000.0D0)*1000.0D0)
      IVX = IV / 100
      IV  = IV - IVX*100
      IVY = IV / 10
      IVH = IV - IVY*10
      SV2D_CMULT_INTGCTRQX = MAX(MIN(IVX, 1), 0)
      SV2D_CMULT_INTGCTRQY = MAX(MIN(IVY, 1), 0)
      SV2D_CMULT_INTGCTRH  = MAX(MIN(IVH, 1), 0)

      IERR = ERR_TYP()
      RETURN
      END

