C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y2_CBS_CLCPRNO
C     SUBROUTINE SV2D_Y2_CBS_CLCPRNEV
C     SUBROUTINE SV2D_Y2_CBS_CLCPRNES
C   Private:
C     SUBROUTINE SV2D_Y2_CBS_CLCPRN_1N
C
C************************************************************************

C************************************************************************
C Sommaire: SV2D_Y2_CBS_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C     CALCUL DES PROPRIETES NODALES DÉPENDANT DE LA SOLUTION (DS)
C                    CST    1)  Z fond
C                    CST    2)  MANNING NODAL
C                    CST    3)  EPAISSEUR DE LA GLACE
C                    CST    4)  MANNING GLACE
C                    CST    5)  COMPOSANTE X DU VENT
C                    CST    6)  COMPOSANTE Y DU VENT
C                    DS     7)  VITESSE EN X => U (QX/PROF)
C                    DS     8)  VITESSE EN Y => V (QY/PROF)
C                    DS     9)  PROFONDEUR
C                    DS    10)  COEF. COMP. DE FROTTEMENT DE MANNING
C                    DS    11)  COEF. COMP. DE CONVECTION
C                    DS    12)  COEF. COMP. DE GRAVITE
C                    DS    13)  COEF. COMP. DE DIFFUSION (DISSIPATION)
C                    DS    14)  COEF. COMP. DE DARCY
C                    DS    15)  cw(w).|w|
C                    DS    16)  POROSITÉ
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Par rapport à SV2D_YS_CBS_CLCPRNO, on ajoute l'hystérèse sur le
C     découvrement.
C
C     L'imposition des noeuds milieux devrait être faite dans une
C     fonction séparée, pour bien séparer les concepts
C************************************************************************
      SUBROUTINE SV2D_Y2_CBS_CLCPRNO (VCORG,
     &                                KNGV,
     &                                VDJV,
     &                                VPRGL,
     &                                VPRNO,
     &                                VDLG,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y2_CBS_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA
      INTEGER IC, IE, IN, INC
      INTEGER IPV, IPH, IPF, IPC
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      LOGICAL COUVERT, DECOUVERT

      INTEGER SV2D_Y2_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H
      IPF = SV2D_IPRNO_COEFF_FROT
      IPC = SV2D_IPRNO_COEFF_CNVT

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(INC)
!$omp& private(IERR)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
!$omp& private(BATHY, EPAIGL, PROF)
!$omp& private(PRFE, PRFA)
!$omp& private(COUVERT, DECOUVERT)

C---     Impose les niveaux d'eau sur les noeuds milieux
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VDLG(3,NO2) = (VDLG(3,NO1)+VDLG(3,NO3))*UN_2
         VDLG(3,NO4) = (VDLG(3,NO3)+VDLG(3,NO5))*UN_2
         VDLG(3,NO6) = (VDLG(3,NO5)+VDLG(3,NO1))*UN_2
      ENDDO
!$omp end do
      ENDDO

C---     Profondeurs effective et absolue
!$omp  do
      DO IN=1,EG_CMMN_NNL
         BATHY  = VPRNO(SV2D_IPRNO_Z,IN)
         EPAIGL = 0.9D0 * VPRNO(SV2D_IPRNO_ICE_E,IN)
         PROF   = VDLG(3,IN) - BATHY
         EPAIGL = MIN(EPAIGL, PROF)
         EPAIGL = MAX(EPAIGL, ZERO)
         PRFE   = PROF - EPAIGL                 ! Prof effective
         PRFA   = MAX(PRFE, SV2D_DECOU_HMIN)    ! Prof absolue
         VPRNO(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNO(IPH,IN) = PRFA                   ! Prof absolue
         VPRNO(IPF,IN) = 0
         VPRNO(IPC,IN) = 0
      ENDDO
!$omp end do

C---     Impose les profondeurs sur les noeuds milieux
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         NO4  = KNGV(4,IE)
         NO5  = KNGV(5,IE)
         NO6  = KNGV(6,IE)

         VPRNO(IPV,NO2) = (VPRNO(IPV,NO1)+VPRNO(IPV,NO3))*UN_2
         VPRNO(IPV,NO4) = (VPRNO(IPV,NO3)+VPRNO(IPV,NO5))*UN_2
         VPRNO(IPV,NO6) = (VPRNO(IPV,NO5)+VPRNO(IPV,NO1))*UN_2

C           Profondeur linéaire
C           Noeud rehaussé si un noeud sommet est découvert
         VPRNO(IPH,NO2) = (VPRNO(IPH,NO1)+VPRNO(IPH,NO3))*UN_2
         VPRNO(IPH,NO4) = (VPRNO(IPH,NO3)+VPRNO(IPH,NO5))*UN_2
         VPRNO(IPH,NO6) = (VPRNO(IPH,NO5)+VPRNO(IPH,NO1))*UN_2

C---        État de l'élément et assemblage sur les noeuds
         COUVERT  = (VPRNO(SV2D_IPRNO_V,NO1) .GE. SV2D_DECOU_HTRG .AND.
     &               VPRNO(SV2D_IPRNO_V,NO3) .GE. SV2D_DECOU_HTRG .AND.
     &               VPRNO(SV2D_IPRNO_V,NO5) .GE. SV2D_DECOU_HTRG)
         DECOUVERT= (VPRNO(SV2D_IPRNO_V,NO1) .LE. SV2D_DECOU_HMIN .AND.
     &               VPRNO(SV2D_IPRNO_V,NO3) .LE. SV2D_DECOU_HMIN .AND.
     &               VPRNO(SV2D_IPRNO_V,NO5) .LE. SV2D_DECOU_HMIN)
         INC = 0
         IF (COUVERT) THEN
            INC = 1
         ELSEIF (DECOUVERT) THEN
            INC = -1
         ENDIF
         VPRNO(IPF,NO1) = VPRNO(IPF,NO1) + INC
         VPRNO(IPC,NO1) = VPRNO(IPC,NO1) + 1
         VPRNO(IPF,NO2) = VPRNO(IPF,NO2) + INC
         VPRNO(IPC,NO2) = VPRNO(IPC,NO2) + 1
         VPRNO(IPF,NO3) = VPRNO(IPF,NO3) + INC
         VPRNO(IPC,NO3) = VPRNO(IPC,NO3) + 1
         VPRNO(IPF,NO4) = VPRNO(IPF,NO4) + INC
         VPRNO(IPC,NO4) = VPRNO(IPC,NO4) + 1
         VPRNO(IPF,NO5) = VPRNO(IPF,NO5) + INC
         VPRNO(IPC,NO5) = VPRNO(IPC,NO5) + 1
         VPRNO(IPF,NO6) = VPRNO(IPF,NO6) + INC
         VPRNO(IPC,NO6) = VPRNO(IPC,NO6) + 1
      ENDDO
!$omp end do
      ENDDO

C---     Boucle sur les noeuds
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VPRNO(LM_CMMN_NPRNO,IN) = VPRNO(IPF,IN) / VPRNO(IPC,IN)
         IERR = SV2D_Y2_CBS_CLCPRN_1N(VPRGL, VPRNO(1,IN), VDLG(1,IN))
      ENDDO
!$omp end do

      IERR = ERR_OMP_RDC()
!$omp end parallel

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y2_CBS_CLCPRNEV
C
C Description:
C     Calcul des propriétés nodales d'un élément de volume,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y2_CBS_CLCPRNEV(VPRGL,
     &                                VPRNE,
     &                                VDLE,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y2_CBS_CLCPRNEV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNE(LM_CMMN_NPRNO, EG_CMMN_NNELV)
      REAL*8  VDLE (LM_CMMN_NDLN,  EG_CMMN_NNELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
      INTEGER, PARAMETER :: NO4 = 4
      INTEGER, PARAMETER :: NO5 = 5
      INTEGER, PARAMETER :: NO6 = 6

      INTEGER SV2D_Y2_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2
      VDLE(3,NO4) = (VDLE(3,NO3)+VDLE(3,NO5))*UN_2
      VDLE(3,NO6) = (VDLE(3,NO5)+VDLE(3,NO1))*UN_2

C---     Profondeurs effective et absolue
!$omp simd
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(BATHY, EPAIGL, PROF)
!$omp& private(PRFE, PRFA)
      DO IN=1,EG_CMMN_NNELV
         BATHY   = VPRNE(SV2D_IPRNO_Z,IN)
         EPAIGL  = 0.9D0 * VPRNE(SV2D_IPRNO_ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, SV2D_DECOU_HMIN)   ! Prof absolue
         VPRNE(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNE(IPH,IN) = PRFA                   ! Prof absolue
      ENDDO
!$omp end simd

C---     Impose les profondeurs sur les noeuds milieux
      VPRNE(IPV,NO2) = (VPRNE(IPV,NO1)+VPRNE(IPV,NO3))*UN_2
      VPRNE(IPV,NO4) = (VPRNE(IPV,NO3)+VPRNE(IPV,NO5))*UN_2
      VPRNE(IPV,NO6) = (VPRNE(IPV,NO5)+VPRNE(IPV,NO1))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRNE(IPH,NO2) = (VPRNE(IPH,NO1)+VPRNE(IPH,NO3))*UN_2
      VPRNE(IPH,NO4) = (VPRNE(IPH,NO3)+VPRNE(IPH,NO5))*UN_2
      VPRNE(IPH,NO6) = (VPRNE(IPH,NO5)+VPRNE(IPH,NO1))*UN_2

C---     Boucle sur les noeuds
!$omp simd
!$omp& safelen(6)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(IERR)
      DO IN=1,EG_CMMN_NNELV
         IERR = SV2D_Y2_CBS_CLCPRN_1N(VPRGL, VPRNE(1,IN), VDLE(1,IN))
      ENDDO
!$omp end simd

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y2_CBS_CLCPRNES
C
C Description:
C     Calcul des propriétés nodales d'un élément de surface,
C     dépendantes de VDLE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Fonction appelée lors du calcul des propriétés nodales perturbées dans
C     ASMKT.
C************************************************************************
      SUBROUTINE SV2D_Y2_CBS_CLCPRNES(VPRGL,
     &                                VPRNE,
     &                                VDLE,
     &                                IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y2_CBS_CLCPRNES
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNE(LM_CMMN_NPRNO, EG_CMMN_NNELS)
      REAL*8  VDLE (LM_CMMN_NDLN,  EG_CMMN_NNELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER IN
      INTEGER IPV, IPH
      REAL*8  BATHY, EPAIGL, PROF
      REAL*8  PRFE, PRFA

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3

      INTEGER SV2D_Y2_CBS_CLCPRN_1N
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPV = SV2D_IPRNO_V
      IPH = SV2D_IPRNO_H

C---     Impose les niveaux d'eau sur les noeuds milieux
      VDLE(3,NO2) = (VDLE(3,NO1)+VDLE(3,NO3))*UN_2

C---     Profondeurs effective et absolue
!$omp simd
!$omp& safelen(3)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(BATHY, EPAIGL, PROF)
!$omp& private(PRFE, PRFA)
      DO IN=1,EG_CMMN_NNELS
         BATHY   = VPRNE(SV2D_IPRNO_Z,IN)
         EPAIGL  = 0.9D0 * VPRNE(SV2D_IPRNO_ICE_E,IN)
         PROF    = VDLE(3,IN) - BATHY
         EPAIGL  = MIN(EPAIGL, PROF)
         EPAIGL  = MAX(EPAIGL, ZERO)
         PRFE    = PROF - EPAIGL                ! Prof effective
         PRFA    = MAX(PRFE, SV2D_DECOU_HMIN)   ! Prof absolue
         VPRNE(IPV,IN) = PRFE                   ! Prof effective temp.
         VPRNE(IPH,IN) = PRFA                   ! Prof absolue
      ENDDO
!$omp end simd

C---     Impose les profondeurs sur les noeuds milieux
      VPRNE(IPV,NO2) = (VPRNE(IPV,NO1)+VPRNE(IPV,NO3))*UN_2

C        Profondeur linéaire
C        Noeud rehaussé si un noeud sommet est découvert
      VPRNE(IPH,NO2) = (VPRNE(IPH,NO1)+VPRNE(IPH,NO3))*UN_2

C---     Boucle sur les noeuds
!$omp simd
!$omp& safelen(3)
C!$omp& private(IN)  !!Intel 16.0 No loop control variable in a private clause
!$omp& private(IERR)
      DO IN=1,EG_CMMN_NNELS
         IERR = SV2D_Y2_CBS_CLCPRN_1N(VPRGL, VPRNE(1,IN), VDLE(1,IN))
      ENDDO
!$omp end simd

      RETURN
      END

C************************************************************************
C Sommaire: SV2D_Y2_CBS_CLCPRN_1N
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG. Le calcul est
C     fait sur un noeud.
C
C Entrée:
C      REAL*8  VPRGL       Les PRopriétés GLobales
C      REAL*8  VDLN        Le Degré de Liberté Nodaux
C
C Sortie:
C      REAL*8  VPRN        Les PRopriétés Nodales
C      INTEGER IERR
C
C Notes:
C     En PRFA, un noeud milieu va se retrouver rehaussé si un noeud sommet
C     est découvert. Il y a un impact sur le coefficient de frottement
C     qui est diminué.
C     ULIM vise à "freiner" les fortes vitesses, celles au-delà de UMAX.
C     Jusqu'à UMAX, la loi est linéaire, puis exp 1/3
C************************************************************************
      FUNCTION SV2D_Y2_CBS_CLCPRN_1N(VPRGL, VPRN, VDLN)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SV2D_Y2_CBS_CLCPRN_1N
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRN (LM_CMMN_NPRNO)
      REAL*8  VDLN (LM_CMMN_NDLN)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'

      REAL*8  R_4_3
      PARAMETER (R_4_3 = 4.0D0/3.0D0)
      REAL*8  R_1_3
      PARAMETER (R_1_3 = 1.0D0/3.0D0)

      REAL*8  ALFA
      REAL*8  H1, H2
      REAL*8  PRFE, PRFA, UN_PRFA
      REAL*8  PORO, VMAN, VFLN, FCVT, FGRA, VDIF, VDRC
      REAL*8  NTOT, FROTT
      REAL*8  VX, VY, VMOD
      REAL*8  STTEL
      REAL*8  VN1, VN2

D     LOGICAL COUVERT, DECOUVERT, COUVRE
      LOGICAL DECOUVRE
C-----------------------------------------------------------------------
      REAL*8 ULIM, UEXP, U
      UEXP(U) = (U-SV2D_DECOU_UMAX+UN)**R_1_3 + SV2D_DECOU_UMAX-UN
      ULIM(U) = MIN(UEXP(MAX(U,SV2D_DECOU_UMAX)), U)
C-----------------------------------------------------------------------

C---     Profondeur
      PRFE = VPRN(SV2D_IPRNO_V)           ! Prof effective nodale
      PRFA = VPRN(SV2D_IPRNO_H)           ! Prof absolue linéaire
      STTEL= VPRN(LM_CMMN_NPRNO)          ! État des éléments adhérents
      UN_PRFA = UN / PRFA                 ! Inverse prof absolue

C---     ALFA
D     COUVERT   = (PRFE .GE. SV2D_DECOU_HTRG .AND. STTEL .GE. 0.0D0)
D     COUVRE    = (PRFE .LT. SV2D_DECOU_HTRG .AND. STTEL .GE. 0.0D0)
D     DECOUVERT = (PRFE .LE. SV2D_DECOU_HMIN .AND. STTEL .LT. 0.0D0)
      DECOUVRE  = (PRFE .GT. SV2D_DECOU_HMIN .AND. STTEL .LT. 0.0D0)
      IF (DECOUVRE) THEN
         H1 = SV2D_DECOU_HMIN
         H2 = SV2D_DECOU_HTRG - SV2D_DECOU_DHST
      ELSE
         H1 = SV2D_DECOU_HMIN + SV2D_DECOU_DHST
         H2 = SV2D_DECOU_HTRG
      ENDIF
D     CALL ERR_ASR(H2 .GT. H1)
      ALFA = (PRFE-H1)/(H2-H1)
      ALFA = MIN(UN, MAX(ZERO, ALFA))
D     CALL ERR_ASR((COUVERT .AND. ALFA .EQ. UN) .OR.
D    &             (DECOUVERT .AND. ALFA .EQ. ZERO) .OR.
D    &             (ALFA .LE. UN .AND. ALFA .GE. ZERO))
      VN1 = UN - ALFA
      VN2 = ALFA

C---     Module de la vitesse
      VX = VDLN(1) * UN_PRFA
      VY = VDLN(2) * UN_PRFA
      VMOD = HYPOT(VX, VY)

C---     Manning global
      NTOT = HYPOT(VPRN(SV2D_IPRNO_N), VPRN(SV2D_IPRNO_ICE_N))

C---     Paramètres variables pour le découvrement
      VMOD = VN1*ULIM(VMOD)          + VN2*VMOD
      PORO = VN1*SV2D_DECOU_PORO     + VN2 !*UN
      VFLN = VN1*SV2D_DECOU_AMORT    + VN2*SV2D_STABI_AMORT
      VMAN = VN1*SV2D_DECOU_MAN      + VN2*SV2D_CMULT_MAN*NTOT
      FCVT = VN1*SV2D_DECOU_CON_FACT + VN2*SV2D_CMULT_CON
      FGRA = VN1*SV2D_DECOU_GRA_FACT + VN2*SV2D_CMULT_GRA
      VDIF = VN1*SV2D_DECOU_DIF_NU  !+ VN2*0.0
      VDRC = VN1*SV2D_DECOU_DRC_NU   + VN2*SV2D_STABI_DARCY

C---     Frottement
!      L'utilisation de la masse lumped pour découpler le frottement
!      mène à des résultats très différents d'Hydrosim.
!      FROTT = SV2D_GRAVITE*VMAN*VMAN * UMOD * (UN_PRFA**R_4_3)
      FROTT = VFLN + SV2D_GRAVITE*VMAN*VMAN * VMOD * (UN_PRFA**R_4_3) ! Hydrosim

C---     Valeurs nodales
      VPRN(SV2D_IPRNO_U)          = VDLN(1) * UN_PRFA ! U
      VPRN(SV2D_IPRNO_V)          = VDLN(2) * UN_PRFA ! V
      VPRN(SV2D_IPRNO_H)          = PRFA              ! Prof absolue
      VPRN(SV2D_IPRNO_COEFF_CNVT) = FCVT              ! Facteur de convection
      VPRN(SV2D_IPRNO_COEFF_GRVT) = FGRA              ! Facteur de gravité
      VPRN(SV2D_IPRNO_COEFF_FROT) = FROTT             ! g n2 |u| / H**(4/3)
      VPRN(SV2D_IPRNO_COEFF_DIFF) = VDIF              ! Diffusion de decou.
      VPRN(SV2D_IPRNO_COEFF_DRCY) = VDRC              ! Darcy
      VPRN(SV2D_IPRNO_COEFF_PORO) = PORO

      SV2D_Y2_CBS_CLCPRN_1N = ERR_OK
      RETURN
      END
