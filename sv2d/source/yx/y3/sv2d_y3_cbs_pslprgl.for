C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE SV2D_Y3_CBS_PSLPRGL
C   Private:
C     SUBROUTINE SV2D_Y3_PSLPRGL_E
C     SUBROUTINE SV2D_Y3_INI_IPRGL
C
C************************************************************************

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y3_CBS_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_Y3_CBS_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      CALL SV2D_Y3_INI_IPRGL()
      CALL SV2D_Y3_PSLPRGL_E(VPRGL, IERR)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_CBS_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y3_PSLPRGL_E(VPRGL, IERR)

      IMPLICIT NONE
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'

      INTEGER I, IE
      REAL*8  VVAL, VMIN, VMAX, VLATDEG, VLATRAD

      INTEGER CHKVAL
C-----------------------------------------------------------------------

      CALL SV2D_CBS_PSLPRGL_E(VPRGL, IERR)

C---     Coefficient du vent
      I = SV2D_IPRGL_FCT_CW_VENT
      VVAL = NINT(VPRGL(I))
      VMIN = SV2D_CW_INDEFINI+1
      VMAX = SV2D_CW_TAG_LAST-1
      IE = CHKVAL(I, VVAL, VMIN, VMAX, 'MSG_FCT_CW_VENT')

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SV2D_Y3_CBS_INI_IPRGL
C
C Description:
C     La fonction privée SV2D_Y3_CBS_INI_IPRGL initialise les indices
C     des propriétés globales.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_Y3_INI_IPRGL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cbs.fc'
C-----------------------------------------------------------------------

      SV2D_IPRGL_GRAVITE         =  1
      SV2D_IPRGL_LATITUDE        =  2
      SV2D_IPRGL_FCT_CW_VENT     =  3
      SV2D_IPRGL_VISCO_CST       =  4
      SV2D_IPRGL_VISCO_LM        =  5
      SV2D_IPRGL_VISCO_SMGO      =  6
      SV2D_IPRGL_VISCO_BINF      =  7
      SV2D_IPRGL_VISCO_BSUP      =  8
      SV2D_IPRGL_DECOU_HTRG      =  9
      SV2D_IPRGL_DECOU_HMIN      = 10
      SV2D_IPRGL_DECOU_PENA_H    = -1
      SV2D_IPRGL_DECOU_PENA_Q    = -1
      SV2D_IPRGL_DECOU_MAN       = 11
      SV2D_IPRGL_DECOU_UMAX      = 12
      SV2D_IPRGL_DECOU_PORO      = 13
      SV2D_IPRGL_DECOU_AMORT     = 14
      SV2D_IPRGL_DECOU_CON_FACT  = 15
      SV2D_IPRGL_DECOU_GRA_FACT  = 16
      SV2D_IPRGL_DECOU_DIF_NU    = 17
      SV2D_IPRGL_DECOU_DIF_PE    = -1
      SV2D_IPRGL_DECOU_DRC_NU    = 18
      SV2D_IPRGL_STABI_PECLET    = 19
      SV2D_IPRGL_STABI_AMORT     = 20
      SV2D_IPRGL_STABI_DARCY     = 21
      SV2D_IPRGL_STABI_LAPIDUS   = 22
      SV2D_IPRGL_CMULT_CON       = 23
      SV2D_IPRGL_CMULT_GRA       = 24
      SV2D_IPRGL_CMULT_PDYN      = -1
      SV2D_IPRGL_CMULT_MAN       = 25
      SV2D_IPRGL_CMULT_VENT      = 26
      SV2D_IPRGL_CMULT_INTGCTR   = 27
      SV2D_IPRGL_PNUMR_PENALITE  = 28
      SV2D_IPRGL_PNUMR_DELPRT    = 29
      SV2D_IPRGL_PNUMR_DELMIN    = -1
      SV2D_IPRGL_PNUMR_OMEGAKT   = 30
      SV2D_IPRGL_PNUMR_PRTPREL   = 31
      SV2D_IPRGL_PNUMR_PRTPRNO   = 32
      SV2D_IPRGL_CORIOLIS        = 33
      SV2D_IPRGL_CMULT_VENT_REL  = -1

      RETURN
      END
