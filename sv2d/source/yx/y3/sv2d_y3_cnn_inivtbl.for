C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction SV2D_Y3_CNN_INIVTBL initialise et remplis la table
C     virtuelle (VTABLE) qui contient les fonctions de la classe.
C     SV2D_Y3 spécialise SV2D_Y2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne gère pas l'héritage multiple en diamant.
C************************************************************************
      FUNCTION SV2D_Y3_CNN_INIVTBL(H)

      IMPLICIT NONE

      INTEGER H

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_y2_cnn.fc'
      INCLUDE 'sv2d_y3_cnn.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SO_VTBL_HVALIDE(H))
C-----------------------------------------------------------------------

C---     Appelle le parent
      IF (ERR_GOOD()) IERR = SV2D_Y2_CNN_INIVTBL(H)

C---     Redéfinis les fonctions de l'interface
      IF (ERR_GOOD()) THEN
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_PSLPRGL, 'SV2D_Y3_CBS_PSLPRGL')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_PRCPRGL, 'SV2D_Y3_CBS_PRCPRGL')
         IERR=SV2D_CBS_AJTFSO(H,EA_FUNC_REQPRM,  'SV2D_Y3_CBS_REQPRM')
      ENDIF

      SV2D_Y3_CNN_INIVTBL = ERR_TYP()
      RETURN
      END
