C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Ponceau MTQ.
C     Le débit calculé sur la différence des niveaux amont-aval est imposé
C     sous forme faible à l'amont et l'aval.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL144_000
C     INTEGER SV2D_CL144_999
C     INTEGER SV2D_CL144_COD
C     INTEGER SV2D_CL144_CLC
C     INTEGER SV2D_CL144_ASMF
C     INTEGER SV2D_CL144_ASMKT
C     INTEGER SV2D_CL144_HLP
C   Private:
C     INTEGER SV2D_CL144_INIVTBL
C     INTEGER SV2D_CL144_CLC_
C     INTEGER SV2D_CL144_ASMFE
C     INTEGER SV2D_CL144_ASMKTL
C     CHARACTER*(16) SV2D_CL144_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL144_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl144.fc'

      DATA SV2D_CL144_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL144_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL144_INIVTBL()

      SV2D_CL144_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL144_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL144_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR !!!, IRET
      INTEGER HVFT
!!!      INTEGER LTPN
      EXTERNAL SV2D_CL144_HLP
      EXTERNAL SV2D_CL144_COD
      EXTERNAL SV2D_CL144_CLC
      EXTERNAL SV2D_CL144_ASMF
      EXTERNAL SV2D_CL144_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL144_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL144_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL144_TPN(1:LTPN), SV2D_CL144_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL144_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL144_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL144_TYP,
     &                     SV2D_CL144_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD,  SV2D_CL144_TYP,
     &                     SV2D_CL144_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_CLC,  SV2D_CL144_TYP,
     &                     SV2D_CL144_CLC)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF, SV2D_CL144_TYP,
     &                     SV2D_CL144_ASMF)
!      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKT,SV2D_CL144_TYP,
!     &                     SV2D_CL144_ASMKT)

      SV2D_CL144_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL144_COD
C
C Description:
C     La fonction SV2D_CL144_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     On ne contrôle pas le type de la CL amont
C************************************************************************
      FUNCTION SV2D_CL144_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, ID_AMT
      INTEGER IL_AVL, ID_AVL
      INTEGER, PARAMETER :: KVAL(1) = (/25/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(1) = (/EG_TPLMT_1SGMT/)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL144_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)
      IV = KCLCND(3, IC)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         IL_AVL = IL
         ID_AVL = 3
         IL_AMT = NINT( VCLCNV(IV) )
         ID_AMT = 3
         IERR = SV2D_CL_COD2L(IL_AVL, ID_AVL, SV2D_CL_DEBIT,      ! aval
     &                        IL_AMT, ID_AMT, SV2D_CL_DEBIT,      ! amont
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
         IF (ERR_GOOD()) VCLCNV(IV) = IL_AMT
      ENDIF

C---     Contrôle la limite amont
      IF (ERR_GOOD()) THEN
         IERR = SV2D_CL_CHKDIM(IL_AMT, KCLCND, KCLLIM,
     &                         KVAL(0:1),    ! Force empty array
     &                         KRNG, KKND)
      ENDIF

      SV2D_CL144_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL144_CLC_
C
C Description:
C     La fonction privée SV2D_CL144_CLC_ assigne la valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     Elle permet de simplifier les arguments et est appelée également par
C     ASMKTL
C************************************************************************
      FUNCTION SV2D_CL144_CLC_(IL,
     &                         KNGS,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         VDLG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spmtq.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IE, IES, I, IN
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT, INDEB_AMT, INFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL, INDEB_AVL, INFIN_AVL
      INTEGER NO1, NO2, NO3
      INTEGER NVAL
      REAL*8  VNX, VNY, DJL3, DJL2
      REAL*8  QX1, QY1, H1, P1, F1
      REAL*8  QX2, QY2, P2, F2
      REAL*8  QX3, QY3, H3, P3, F3
      REAL*8  QX, QY
      REAL*8  Q_US, H_US, L_US, S_US
      REAL*8  Q_DS, H_DS, L_DS, S_DS
      REAL*8  Q_Pct, Q_Med, Q_srf !, H_PctUs
      REAL*8  RS(8), RG(8)
      LOGICAL ENEAU

      REAL*8  CINQ_TIER
      PARAMETER (CINQ_TIER = 5.0D0/3.0D0)
      REAL*8  DREL_MAX     ! Delta relatif max
      PARAMETER (DREL_MAX  = 0.15D0)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
      REAL*8 V1, V2
      REAL*8 ERR_REL
      ERR_REL(V1, V2) = 0.5D0*(ABS(V1-V2) / ABS(V1+V2))
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL144_TYP)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.bc.cl144'

      IC = KCLLIM(2,IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 25)
      IV = KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( VCLCNV(IV) )
      INDEB_AMT = KCLLIM(3, IL_AMT)
      INFIN_AMT = KCLLIM(4, IL_AMT)
      IEDEB_AMT = KCLLIM(5, IL_AMT)
      IEFIN_AMT = KCLLIM(6, IL_AMT)
      INDEB_AVL = KCLLIM(3, IL_AVL)
      INFIN_AVL = KCLLIM(4, IL_AVL)
      IEDEB_AVL = KCLLIM(5, IL_AVL)
      IEFIN_AVL = KCLLIM(6, IL_AVL)

C---     Valeurs de la condition
      NVAL = KCLCND(4,IC)-KCLCND(3,IC)            ! +1 mais -la première
      IERR = SP_MTQ_ASGPRM(NVAL, VCLCNV(IV+1))

C---     Débit et Niveau amont
      Q_US = 0.0D0
      H_US = 0.0D0
      S_US = 0.0D0
      L_US = 0.0D0
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = KCLELE(IE)

C---        Connectivités du L3
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Métriques
         VNX =  VDJS(2,IES)    ! VNX =  VTY
         VNY = -VDJS(1,IES)    ! VNY = -VTX
         DJL3=  VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = VDLG (1,NO1)   ! Qx
         QY1 = VDLG (2,NO1)   ! Qy
         H1  = VDLG (3,NO1)   ! h
         QX2 = VDLG (1,NO2)
         QY2 = VDLG (2,NO2)
         QX3 = VDLG (1,NO3)
         QY3 = VDLG (2,NO3)
         H3  = VDLG (3,NO3)
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_US = Q_US + (QX+QY)*DJL2          ! Débit
         H_US = H_US + (H1+H3)*DJL3          ! h*L
         S_US = S_US + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_US = L_US + (UN+UN)*DJL3          ! Longueur
199      CONTINUE
      ENDDO

C---     Débit et Niveau aval
      Q_DS = 0.0D0
      H_DS = 0.0D0
      S_DS = 0.0D0
      L_DS = 0.0D0
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = KCLELE(IE)

C---        Connectivités du L3
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 299

C---        Métriques
         VNX =  VDJS(2,IES)    ! VNX =  VTY
         VNY = -VDJS(1,IES)    ! VNY = -VTX
         DJL3=  VDJS(3,IES)
         DJL2=  UN_2*DJL3

C---        Valeurs nodales
         QX1 = VDLG (1,NO1)   ! Qx
         QY1 = VDLG (2,NO1)   ! Qy
         H1  = VDLG (3,NO1)   ! h
         QX2 = VDLG (1,NO2)
         QY2 = VDLG (2,NO2)
         QX3 = VDLG (1,NO3)
         QY3 = VDLG (2,NO3)
         H3  = VDLG (3,NO3)
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         P1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         P2 = (P2**CINQ_TIER) / F2
         P3 = (P3**CINQ_TIER) / F3

C---        Débits normaux
         QX = ((QX2+QX1) + (QX3+QX2))*VNX
         QY = ((QY2+QY1) + (QY3+QY2))*VNY

C---        Intègre
         Q_DS = Q_DS + (QX+QY)*DJL2          ! Débit
         H_DS = H_DS + (H1+H3)*DJL3          ! h*Longueur
         S_DS = S_DS + (P1+P2+P2+P3)*DJL2    ! Surface à la Manning
         L_DS = L_DS + (UN+UN)*DJL3          ! Longueur totale
299      CONTINUE
      ENDDO

C---     Réduction et contrôle
      RS(1) = Q_US
      RS(2) = H_US
      RS(3) = L_US
      RS(4) = S_US
      RS(5) = Q_DS
      RS(6) = H_DS
      RS(7) = L_DS
      RS(8) = S_DS
      CALL MPI_ALLREDUCE(RS, RG, 8, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      Q_US = RG(1)
      H_US = RG(2)
      L_US = RG(3)
      S_US = RG(4)
      Q_DS = -RG(5)           ! négatif, débit entrant
      H_DS = RG(6)
      L_DS = RG(7)
      S_DS = RG(8)

C---     Calcule les données hydrauliques du ponceau
      Q_Pct = 0.0D0
      ENEAU = .TRUE.
      IF (L_US .LT. PETIT .OR. L_DS .LT. PETIT) THEN
         S_US = 1.0D0
         S_DS = 1.0D0
         ENEAU = .FALSE.
      ENDIF
      IF (ENEAU) THEN
         H_US = (H_US / L_US)    ! Niveau moyen
         H_DS = (H_DS / L_DS)    ! Niveau moyen
         Q_Pct   = SP_MTQ_CLCQ(H_US, H_DS)
!      H_PctUs = SP_MTQ_CLCH(H_US, H_DS, 0.5D0*(Q_US+Q_DS))
      ENDIF

C---     Log
!      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
!     &      'Culvert ', IL,': H (up, down, clc): ', H_US, H_DS, H_PctUs
!      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I3,A,3(1PE14.6E3))')
     &      'Culvert ', IL,': Q (up, down, clc): ', Q_US, Q_DS, Q_Pct
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Impose les valeurs (avec amortissement au besoin)
      IF (ENEAU) THEN
         Q_Med = UN_2*(Q_US+Q_DS)
         IF (ERR_REL(Q_Pct, Q_Med) .GT. DREL_MAX)
     &      Q_Pct = Q_Med + DREL_MAX*(Q_Pct-Q_Med)
      ENDIF
      Q_srf = Q_Pct / S_US                   ! Positif, débit sortant
      DO I = INDEB_AMT, INFIN_AMT
         IN = KCLNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = Q_srf  ! Débit spécifique (m/s)
      ENDDO
      Q_srf = - Q_Pct / S_DS                 ! Négatif, débit entrant
      DO I = INDEB_AVL, INFIN_AVL
         IN = KCLNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = Q_srf  ! Débit spécifique (m/s)
      ENDDO

      SV2D_CL144_CLC_ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL144_CLC
C
C Description:
C     La fonction privée SV2D_CL144_CLC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose une condition de type
C     Ponceau via des sollicitations réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_CLC(IL,
     &                        VCORG,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL144_TYP)
C-----------------------------------------------------------------------

      IERR = SV2D_CL144_CLC_(IL,
     &                       KNGS,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       VDLG)

      SV2D_CL144_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL144_ASMFE
C
C Description:
C     La fonction SV2D_CL144_ASMFE calcule le vecteur {F} élémentaire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_ASMFE(VFE,
     &                          VDJE,
     &                          VPRN,
     &                          VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VFE  (LM_CMMN_NDLES)
      REAL*8   VDJE (EG_CMMN_NDJS)
      REAL*8   VPRN (LM_CMMN_NPRNO, EG_CMMN_NNELS)
      REAL*8   VDIMP(LM_CMMN_NDLN,  EG_CMMN_NNELS)

      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
      REAL*8  C
      REAL*8  P1, F1, Q1
      REAL*8  P2, F2, Q2
      REAL*8  P3, F3, Q3

      REAL*8  CINQ_TIER
      PARAMETER (CINQ_TIER = 5.0D0/3.0D0)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Coefficient
      C = -UN_12*VDJE(3)               ! UN_6*DJL2

C---     Valeurs nodales
      F1 = MAX(VPRN(SV2D_IPRNO_N,1), PETIT)       ! Manning
      P1 =     VPRN(SV2D_IPRNO_H,1)               ! Profondeur
      F2 = MAX(VPRN(SV2D_IPRNO_N,2), PETIT)
      P2 =     VPRN(SV2D_IPRNO_H,2)
      F3 = MAX(VPRN(SV2D_IPRNO_N,3), PETIT)
      P3 =     VPRN(SV2D_IPRNO_H,3)
      Q1 = VDIMP(3,1)
      Q2 = Q1     !! VDIMP(3,2) ! est-il assigné?
      Q3 = VDIMP(3,3)

C---     Règle de répartition d'après Manning
      Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
      Q2 = Q2*(P2**CINQ_TIER) / F2
      Q3 = Q3*(P3**CINQ_TIER) / F3

C---     Assemble le vecteur élémentaire
      VFE(3) = VFE(3) + C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
      VFE(9) = VFE(9) + C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)

      SV2D_CL144_ASMFE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL144_ASMF
C
C Description:
C     La fonction SV2D_CL144_ASMF calcul le terme de sollicitation.
C     Sur la limite <code>IL</code>, ainsi que sur la limite associée,
C     elle impose une condition de type Ponceau via des sollicitations
C     réparties en débit (sollicitation sur h).
C     Le débit du ponceau est calculé par la formule du MTQ.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
      INTEGER IE, IES
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL
      INTEGER NO1, NO2, NO3
      REAL*8  C
      REAL*8  P1, F1, Q1
      REAL*8  P2, F2, Q2
      REAL*8  P3, F3, Q3
      REAL*8  M(2)

      REAL*8  CINQ_TIER
      PARAMETER (CINQ_TIER = 5.0D0/3.0D0)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL144_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2,IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 25)
      IV = KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( VCLCNV(IV) )
      IEDEB_AMT = KCLLIM(5, IL_AMT)
      IEFIN_AMT = KCLLIM(6, IL_AMT)
      IEDEB_AVL = KCLLIM(5, IL_AVL)
      IEFIN_AVL = KCLLIM(6, IL_AVL)

C---     Boucle sur la limite amont (sollicitation sur h)
C        ================================================
      DO IE = IEDEB_AMT, IEFIN_AMT
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Coefficient
         C = -UN_12*VDJS(3,IES)              ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)
         Q1 = VDIMP(3,NO1)
         Q2 = Q1     !! VDIMP(3,NO2) ! est-il assigné?
         Q3 = VDIMP(3,NO3)

C---        Règle de répartition d'après Manning
         Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = Q2*(P2**CINQ_TIER) / F2
         Q3 = Q3*(P3**CINQ_TIER) / F3

C---        Assemble le vecteur global
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), M(2), VFG)
199      CONTINUE
      ENDDO

C---     Boucle sur la limite aval (sollicitation sur h)
C        ===============================================
      DO IE = IEDEB_AVL, IEFIN_AVL
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 299

C---        Coefficient
         C = -UN_12*VDJS(3,IES)              ! UN_6*DJL2

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)     ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)             ! Profondeur
         F2 = MAX(VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 =     VPRNO(SV2D_IPRNO_H, NO2)
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)
         Q1 = VDIMP(3,NO1)
         Q2 = Q1            ! est-il assigné?
         Q3 = VDIMP(3,NO3)

C---        Règle de répartition d'après Manning
         Q1 = Q1*(P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q2 = Q1     !! VDIMP(3,NO2) ! est-il assigné?
         Q3 = Q3*(P3**CINQ_TIER) / F3

C---        Assemble le vecteur global
         M(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
         M(2) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), M(1), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), M(2), VFG)
299      CONTINUE
      ENDDO

      SV2D_CL144_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL144_ASMKTL
C
C Description:
C     La fonction SV2D_CL144_ASMKTL calcule le matrice de rigidité
C     élémentaire pour une limite. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_ASMKTL(IL,
     &                           IEDEB,
     &                           IEFIN,
     &                           KLOCE,
     &                           VKE,
     &                           KLOCN,
     &                           KNGS,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER IEDEB
      INTEGER IEFIN
      INTEGER KLOCE (LM_CMMN_NDLES)
      REAL*8  VKE   (LM_CMMN_NDLES,LM_CMMN_NDLES)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL =  9)
      PARAMETER (NPRN_LCL = 18)

      REAL*8  VDLE (NDLE_LCL)
      REAL*8  VCLE (NDLE_LCL)
      REAL*8  VPRN (NPRN_LCL * NNEL_LCL)
      REAL*8  VFE  (NDLE_LCL), VFEP(NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IE, IES
      INTEGER IN, II, ID, NO
      INTEGER HVFT, HPRNE
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELS)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLES)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Fonctions de calcul
      HVFT = 0    ! vtable par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNES, HPRNE)

C---     Boucle sur la limite
C        ====================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO2))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Table KLOCE de l'élément
         II=0
         DO IN=1,EG_CMMN_NNELS
            NO = KNGS(IN,IES)
            KLOCE(II+1) = KLOCN(1,NO)
            KLOCE(II+2) = KLOCN(2,NO)
            KLOCE(II+3) = KLOCN(3,NO)
            II=II+3
         ENDDO

C---        Transfert des DDL
         II=0
         DO IN=1,EG_CMMN_NNELS
            NO = KNGS(IN,IES)
            VDLE(II+1) = VDLG(1,NO)
            VDLE(II+2) = VDLG(2,NO)
            VDLE(II+3) = VDLG(3,NO)
            II=II+3
         ENDDO

C---        Transfert des VDIMP
         II=0
         DO IN=1,EG_CMMN_NNELS
            NO = KNGS(IN,IES)
            VCLE(II+1) = VDIMP(1,NO)
            VCLE(II+2) = VDIMP(2,NO)
            VCLE(II+3) = VDIMP(3,NO)
            II=II+3
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELS
            NO = KNGS(IN,IES)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Initialise le vecteur F
         CALL DINIT(LM_CMMN_NDLES, ZERO, VFE, 1)

C---        {F}
         IERR = SV2D_CL144_ASMFE(VFE,
     &                           VDJS(1,IES),
     &                           VPRN,
     &                           VCLE)

C---        Initialise la matrice des dérivées
         CALL DINIT(LM_CMMN_NDLES*LM_CMMN_NDLES, ZERO, VKE, 1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,LM_CMMN_NDLES
            IF (KLOCE(ID) .EQ. 0) GOTO 299

C---           Perturbe le DDL
            VDL_ORI = VDLE(ID)
            VDL_DEL = SV2D_PNUMR_DELPRT * VDLE(ID)
     &              + SIGN(SV2D_PNUMR_DELMIN, VDLE(ID))
            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
            VDLE(ID) = VDLE(ID) + VDL_DEL

C---           Calcule les propriétés nodales perturbées
            IF (SV2D_PNUMR_PRTPRNO) THEN
               IERR = SO_FUNC_CALL4(HPRNE,   ! PRN perturbées
     &                              SO_ALLC_CST2B(VPRGL(1)),
     &                              SO_ALLC_CST2B(VPRN(1)),
     &                              SO_ALLC_CST2B(VDLE(1)),
     &                              SO_ALLC_CST2B(IERR))
               IERR = SV2D_CL144_CLC_(IL,    ! CL perturbé dans VDIMP
     &                              KNGS,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP,
     &                              VDLG)
               VCLE(3) = VDIMP(3, KNGS(1,IES))
               VCLE(6) = VDIMP(3, KNGS(2,IES))
               VCLE(9) = VDIMP(3, KNGS(3,IES))
            ENDIF

C---           Initialise la vecteur de travail
            CALL DINIT(LM_CMMN_NDLES, ZERO, VFEP, 1)

C---           {F(u+du_id)}
            IERR = SV2D_CL144_ASMFE(VFEP,
     &                              VDJS(1,IES),
     &                              VPRN,
     &                              VCLE)

C---           Restaure la valeur originale
            VDLE(ID) = VDL_ORI

C---           - ({F(u+du_id)} - {F(u)})/du
            DO II=1,LM_CMMN_NDLES
               VKE(II,ID) = (VFE(II) - VFEP(II)) * VDL_INV
            ENDDO

299         CONTINUE
         ENDDO

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLES, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

199      CONTINUE
      ENDDO

      SV2D_CL144_ASMKTL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL144_ASMKT
C
C Description:
C     La fonction SV2D_CL144_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_ASMKT(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_ASMKT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLES)
      REAL*8   VKE   (LM_CMMN_NDLES,LM_CMMN_NDLES)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_AMT, IEDEB_AMT, IEFIN_AMT
      INTEGER IL_AVL, IEDEB_AVL, IEFIN_AVL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL144_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2,IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 25)
      IV = KCLCND(3,IC)

C---     Indices des limites
      IL_AVL = IL
      IL_AMT = NINT( VCLCNV(IV) )
      IEDEB_AMT = KCLLIM(5, IL_AMT)
      IEFIN_AMT = KCLLIM(6, IL_AMT)
      IEDEB_AVL = KCLLIM(5, IL_AVL)
      IEFIN_AVL = KCLLIM(6, IL_AVL)

C---     Calcule chaque limite
      IERR = SV2D_CL144_ASMKTL(IL, IEDEB_AMT, IEFIN_AMT,
     &                         KLOCE, VKE, KLOCN, KNGS, VDJS,
     &                         VPRGL, VPRNO,
     &                         KCLCND, VCLCNV,
     &                         KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                         KDIMP, VDIMP,
     &                         VDLG,
     &                         HMTX, F_ASM)
      IERR = SV2D_CL144_ASMKTL(IL, IEDEB_AVL, IEFIN_AVL,
     &                         KLOCE, VKE, KLOCN, KNGS, VDJS,
     &                         VPRGL, VPRNO,
     &                         KCLCND, VCLCNV,
     &                         KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                         KDIMP, VDIMP,
     &                         VDLG,
     &                         HMTX, F_ASM)

      SV2D_CL144_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL144_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL144_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>144</b>: <br>
C  Culvert model with MTQ rules and formulas.
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge in the culvert Q is calculated with the actual upstream-downstream water level difference.
C  It is distributed on the wetted part of both upstream and downstream boundaries.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth dependent).
C  <ul>
C     <li>Kind: Culvert</li>d
C     <li>Code: 144</li>
C     <li>Values: Upstream limit name, 24 parameters describing the culvert</li>
C     <li>Units: </li>
C     <li>Example:  144  $__Culvert_upstream__$ 24_required_parameters</li>
C  </ul>

      SV2D_CL144_REQHLP = 'bc_type_144'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL144_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL144_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL144_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl144.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl144.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl144.hlp')

      SV2D_CL144_HLP = ERR_TYP()
      RETURN
      END
