C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C  Les CL ne possèdent pas d'attributs. Elles représentes alors une
C  hiérarchie de fonctions, et les héritiers réimplantent au besoin des
C  versions spécialisées des fonctions de l'interface. Les héritiers
C  (CL spécialisées) s'enregistrent static dans 000. Les fonctions sont
C  identifiées de manière unique par le nom et par le type de CL.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL_INIVTBL2
C     INTEGER SV2D_CL_INIVTBL
C     INTEGER SV2D_CL_HLP
C     INTEGER SV2D_CL_COD
C     INTEGER SV2D_CL_PRC
C     INTEGER SV2D_CL_CLC
C     INTEGER SV2D_CL_ASMF
C     INTEGER SV2D_CL_ASMKU
C     INTEGER SV2D_CL_ASMK
C     INTEGER SV2D_CL_ASMKT
C     INTEGER SV2D_CL_ASMM
C     INTEGER SV2D_CL_ASMMU
C   Private:
C     INTEGER SV2D_CL_ENCFNC2
C     INTEGER SV2D_CL_ENCFNC
C     INTEGER SV2D_CL_AJTFNC2
C     INTEGER SV2D_CL_AJTFNC
C     INTEGER SV2D_CL_REQFNC
C     INTEGER SV2D_CL_HLP_DUMMY
C     INTEGER SV2D_CL_COD_DUMMY
C     INTEGER SV2D_CL_PRC_CB
C     INTEGER SV2D_CL_PRC_DUMMY
C     INTEGER SV2D_CL_CLC_CB
C     INTEGER SV2D_CL_CLC_DUMMY
C     INTEGER SV2D_CL_ASMF_CB
C     INTEGER SV2D_CL_ASMF_DUMMY
C     INTEGER SV2D_CL_ASMKU_CB
C     INTEGER SV2D_CL_ASMKU_DUMMY
C     INTEGER SV2D_CL_ASMK_CB
C     INTEGER SV2D_CL_ASMK_DUMMY
C     INTEGER SV2D_CL_ASMKT_CB
C     INTEGER SV2D_CL_ASMKT_DUMMY
C     INTEGER SV2D_CL_ASMM_CB
C     INTEGER SV2D_CL_ASMM_DUMMY
C     INTEGER SV2D_CL_ASMMU_CB
C     INTEGER SV2D_CL_ASMMU_DUMMY
C     INTEGER SV2D_CL_CHKDIM
C     INTEGER SV2D_CL_COD1L
C     INTEGER SV2D_CL_COD2L
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl.fc'

      DATA SV2D_CL_HVFT /0/

      END

C************************************************************************
C Sommaire: Encode l'identifiant de fonction de condition limite
C
C Description:
C     La fonction SV2D_CL_ENCFNC encode l'identifiant de la fonction ID et
C     l'identifiant de la condition limite IT en un seul code IC.
C
C Entrée:
C     ID    Identifiant de la fonction
C     IT    Identifiant de la condition limite
C
C Sortie:
C     IC    Identifiant de la fonction
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ENCFNC2(ID, IT, IC)

      IMPLICIT NONE

      INTEGER       ID
      CHARACTER*(*) IT
      INTEGER       IC

!      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR, IRET
      INTEGER IT_
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IRET = C_ST_CRC32(IT(1:SP_STRN_LEN(IT)), IT_)
      IF (IRET .NE. 0) GOTO 9900
      IERR = SV2D_CL_ENCFNC(ID, IT_, IC)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ECRITURE_CODE_CL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,A)') 'MSG_CODE_CL', ': ', IT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12)') 'MSG_CODE_FUNC', ': ', ID
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL_ENCFNC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Encode l'identifiant de fonction de condition limite
C
C Description:
C     La fonction SV2D_CL_ENCFNC encode l'identifiant de la fonction ID et
C     l'identifiant de la condition limite IT en un seul code IC.
C
C Entrée:
C     ID    Identifiant de la fonction
C     IT    Identifiant de la condition limite
C
C Sortie:
C     IC    Identifiant de la fonction
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ENCFNC(ID, IT, IC)

      IMPLICIT NONE

      INTEGER ID
      INTEGER IT
      INTEGER IC

!      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fc'

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IC = ID + IT

      SV2D_CL_ENCFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction à la table virtuelle
C
C Description:
C     La fonction SV2D_CL_AJTFSO ajoute la fonction d'identifiant ID, de
C     condition limite IT et de fonction FN à la table virtuelle de
C     handle HVFT.
C
C Entrée:
C     HVFT  Handle sur la table virtuelle
C     ID    Identifiant de la fonction
C     IT    Identifiant de la condition limite
C     FN    La fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_AJTFNC2(HVFT, ID, IT, FN)

      IMPLICIT NONE

      INTEGER       HVFT
      INTEGER       ID
      CHARACTER*(*) IT
      INTEGER       FN
      EXTERNAL      FN

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER ICOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HVFT))
C-----------------------   ------------------------------------------------

      IF (ERR_GOOD()) IERR = SV2D_CL_ENCFNC2(ID, IT, ICOD)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTFNC(HVFT, ICOD, FN)

      SV2D_CL_AJTFNC2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction à la table virtuelle
C
C Description:
C     La fonction SV2D_CL_AJTFSO ajoute la fonction d'identifiant ID, de
C     condition limite IT et de fonction FN à la table virtuelle de
C     handle HVFT.
C
C Entrée:
C     HVFT  Handle sur la table virtuelle
C     ID    Identifiant de la fonction
C     IT    Identifiant de la condition limite
C     FN    La fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_AJTFNC(HVFT, ID, IT, FN)

      IMPLICIT NONE

      INTEGER  HVFT
      INTEGER  ID
      INTEGER  IT
      INTEGER  FN
      EXTERNAL FN

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER ICOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HVFT))
C-----------------------   ------------------------------------------------

      IF (ERR_GOOD()) IERR = SV2D_CL_ENCFNC(ID, IT, ICOD)
      IF (ERR_GOOD()) IERR = SO_VTBL_AJTFNC(HVFT, ICOD, FN)

      SV2D_CL_AJTFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une fonction de la table virtuelle
C
C Description:
C     La fonction SV2D_CL_REQFNC retourne la fonction d'identifiant ID, de
C     condition limite IT.
C
C Entrée:
C     HVFT  Handle sur la table virtuelle
C     ID    Identifiant de la fonction
C     IT    Identifiant de la condition limite
C
C Sortie:
C     HFNC  Handle sur la function
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_REQFNC(HVFT, ID, IT, HFNC)

      IMPLICIT NONE

      INTEGER HVFT
      INTEGER ID
      INTEGER IT
      INTEGER HFNC

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER ICOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_VTBL_HVALIDE(HVFT))
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = SV2D_CL_ENCFNC(ID, IT, ICOD)
      IF (ERR_GOOD()) IERR = SO_VTBL_REQFNC(HVFT, ICOD, HFNC)

      SV2D_CL_REQFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_INIVTBL
C
C Description:
C     La fonction SV2D_CL_INIVTBL initialise la table virtuelle pour le type IT.
C     Elle charge les fonctions vides par défaut et retourne le handle sur la
C     table virtuelle.
C
C Entrée:
C     INTEGER       IT        Type de condition limite
C
C Sortie:
C     INTEGER       HVFT      Handle sur la table virtuelle
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_INIVTBL2(HVFT, IT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_INIVTBL2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HVFT
      CHARACTER*(*) IT

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR

      EXTERNAL SV2D_CL_HLP_DUMMY
      EXTERNAL SV2D_CL_COD_DUMMY
      EXTERNAL SV2D_CL_PRC_DUMMY
      EXTERNAL SV2D_CL_CLC_DUMMY
      EXTERNAL SV2D_CL_ASMF_DUMMY
      EXTERNAL SV2D_CL_ASMKU_DUMMY
      EXTERNAL SV2D_CL_ASMK_DUMMY
      EXTERNAL SV2D_CL_ASMKT_DUMMY
      EXTERNAL SV2D_CL_ASMM_DUMMY
      EXTERNAL SV2D_CL_ASMMU_DUMMY
C-----------------------------------------------------------------------

C---     Au besoin crée la VTBL
      IF (SV2D_CL_HVFT .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_VTBL_CTR(SV2D_CL_HVFT)
         IF (ERR_GOOD()) IERR = SO_VTBL_INI(SV2D_CL_HVFT)
      ENDIF
      HVFT = SV2D_CL_HVFT

C---     Remplis la table virtuelle avec les fonctions dummy
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_HLP,
     &                                     IT, SV2D_CL_HLP_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_COD,
     &                                     IT, SV2D_CL_COD_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_PRC,
     &                                     IT, SV2D_CL_PRC_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_CLC,
     &                                     IT, SV2D_CL_CLC_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMF,
     &                                     IT, SV2D_CL_ASMF_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMKU,
     &                                     IT, SV2D_CL_ASMKU_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMK,
     &                                     IT, SV2D_CL_ASMK_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMKT,
     &                                     IT, SV2D_CL_ASMKT_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMM,
     &                                     IT, SV2D_CL_ASMM_DUMMY)
      IF (ERR_GOOD()) IERR=SV2D_CL_AJTFNC2(HVFT, SV2D_CL_FUNC_ASMMU,
     &                                     IT, SV2D_CL_ASMMU_DUMMY)

      SV2D_CL_INIVTBL2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_INIVTBL
C
C Description:
C     La fonction SV2D_CL_INIVTBL initialise la table virtuelle pour le type IT.
C     Elle charge les fonctions vides par défaut et retourne le handle sur la
C     table virtuelle.
C
C Entrée:
C     INTEGER       IT        Type de condition limite
C
C Sortie:
C     INTEGER       HVFT      Handle sur la table virtuelle
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_INIVTBL(HVFT, IT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_INIVTBL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HVFT
      INTEGER IT

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR

      EXTERNAL SV2D_CL_HLP_DUMMY
      EXTERNAL SV2D_CL_COD_DUMMY
      EXTERNAL SV2D_CL_PRC_DUMMY
      EXTERNAL SV2D_CL_CLC_DUMMY
      EXTERNAL SV2D_CL_ASMF_DUMMY
      EXTERNAL SV2D_CL_ASMKU_DUMMY
      EXTERNAL SV2D_CL_ASMK_DUMMY
      EXTERNAL SV2D_CL_ASMKT_DUMMY
      EXTERNAL SV2D_CL_ASMM_DUMMY
      EXTERNAL SV2D_CL_ASMMU_DUMMY
C-----------------------------------------------------------------------

C---     Au besoin crée la VTBL
      IF (SV2D_CL_HVFT .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_VTBL_CTR(SV2D_CL_HVFT)
         IF (ERR_GOOD()) IERR = SO_VTBL_INI(SV2D_CL_HVFT)
      ENDIF
      HVFT = SV2D_CL_HVFT

C---     Remplis la table virtuelle avec les fonctions dummy
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_HLP,  IT,
     &                                      SV2D_CL_HLP_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_COD,  IT,
     &                                      SV2D_CL_COD_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_PRC,  IT,
     &                                      SV2D_CL_PRC_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_CLC,  IT,
     &                                      SV2D_CL_CLC_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMF, IT,
     &                                      SV2D_CL_ASMF_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMKU,IT,
     &                                      SV2D_CL_ASMKU_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMK, IT,
     &                                      SV2D_CL_ASMK_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMKT,IT,
     &                                      SV2D_CL_ASMKT_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMM,IT,
     &                                      SV2D_CL_ASMM_DUMMY)
      IF (ERR_GOOD()) IERR = SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMMU,IT,
     &                                      SV2D_CL_ASMMU_DUMMY)

      SV2D_CL_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_HLP
C
C Description:
C     La fonction SV2D_CL_HLP
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_HLP
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IT
      INTEGER HFNC
      BYTE, POINTER :: IT_B(:)
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IT_B => SO_ALLC_CST2B(IT)

C---     Boucle sur les types
      DO IT=1, SV2D_CL_TYP_MAX
         HFNC = 0

         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_HLP, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CALL1 (HFNC,
     &                            IT_B)

         IF (ERR_BAD()) CALL ERR_RESET()
      ENDDO

      SV2D_CL_HLP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_HLP_DUMMY
C
C Description:
C     La fonction SV2D_CL_HLP_DUMMY
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_HLP_DUMMY(IT)

      IMPLICIT NONE

      INTEGER IT

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      WRITE(LOG_BUF, '(A,I3)') 'Boundary condition: type = ', IT
      CALL LOG_ECRIS(LOG_BUF)

      SV2D_CL_HLP_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_COD
C
C Description:
C     La fonction SV2D_CL_COD assigne le code de condition limite.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_COD(KCLCND,
     &                     VCLCNV,
     &                     KCLLIM,
     &                     KCLNOD,
     &                     KCLELE,
     &                     KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_COD
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC

      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KCLCND_B(:)
      BYTE, POINTER :: VCLCNV_B(:)
      BYTE, POINTER :: KCLLIM_B(:)
      BYTE, POINTER :: KCLNOD_B(:)
      BYTE, POINTER :: KCLELE_B(:)
      BYTE, POINTER :: KDIMP_B (:)
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B     => SO_ALLC_CST2B(IL)
      KCLCND_B => SO_ALLC_CST2B(KCLCND(:,1))
      VCLCNV_B => SO_ALLC_CST2B(VCLCNV(:))
      KCLLIM_B => SO_ALLC_CST2B(KCLLIM(:,1))
      KCLNOD_B => SO_ALLC_CST2B(KCLNOD(:))
      KCLELE_B => SO_ALLC_CST2B(KCLELE(:))
      KDIMP_B  => SO_ALLC_CST2B(KDIMP (:,1))

C---     Boucle sur les limites
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         HFNC = 0

         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_COD, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CALL7 (HFNC,
     &                            IL_B,
     &                            KCLCND_B,
     &                            VCLCNV_B,
     &                            KCLLIM_B,
     &                            KCLNOD_B,
     &                            KCLELE_B,
     &                            KDIMP_B)
      ENDDO

      SV2D_CL_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_COD
C
C Description:
C     La fonction SV2D_CL_COD assigne le code de condition limite.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_COD_DUMMY(IL,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           KDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_COD_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_PRC
C
C Description:
C     La fonction SV2D_CL_PRC fait l'étape de pré-calcul pour les
C     conditions limites.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_PRC(KNGV,
     &                     KNGS,
     &                     VDJV,
     &                     VDJS,
     &                     VPRGL,
     &                     VPRNO,
     &                     VPREV,
     &                     VPRES,
     &                     KCLCND,
     &                     VCLCNV,
     &                     KCLLIM,
     &                     KCLNOD,
     &                     KCLELE,
     &                     VCLDST,
     &                     KDIMP,
     &                     VDIMP,
     &                     KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_PRC
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(17)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_PRC_CB
      EXTERNAL SV2D_CL_PRC_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KNGV)
      KI( 2) = SO_ALLC_REQKHND(KNGS)
      KI( 3) = SO_ALLC_REQVHND(VDJV)
      KI( 4) = SO_ALLC_REQVHND(VDJS)
      KI( 5) = SO_ALLC_REQVHND(VPRGL)
      KI( 6) = SO_ALLC_REQVHND(VPRNO)
      KI( 7) = SO_ALLC_REQVHND(VPREV)
      KI( 8) = SO_ALLC_REQVHND(VPRES)
      KI( 9) = SO_ALLC_REQKHND(KCLCND)
      KI(10) = SO_ALLC_REQVHND(VCLCNV)
      KI(11) = SO_ALLC_REQKHND(KCLLIM)
      KI(12) = SO_ALLC_REQKHND(KCLNOD)
      KI(13) = SO_ALLC_REQKHND(KCLELE)
      KI(14) = SO_ALLC_REQVHND(VCLDST)
      KI(15) = SO_ALLC_REQKHND(KDIMP)
      KI(16) = SO_ALLC_REQVHND(VDIMP)
      KI(17) = SO_ALLC_REQKHND(KEIMP)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         HFNC = 0
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_PRC, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC2(HFNC,
     &                            SV2D_CL_PRC_CB,
     &                            IL_B,
     &                            KI_B)
      ENDDO

      SV2D_CL_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_PRC_CB
C
C Description:
C     La fonction SV2D_CL_PRC_CB est le call-back pour le pré-calcul des CL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_PRC_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  SV2D_CL_PRC_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(17)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))))

      SV2D_CL_PRC_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_PRC_DUMMY
C
C Description:
C     La fonction SV2D_CL_PRC_DUMMY impose le degré de
C     vitesse en x comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_PRC_DUMMY(IL,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_PRC_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_CLC
C
C Description:
C     La fonction SV2D_CL_CLC impose le degré de
C     vitesse en x comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_CLC(VCORG,
     &                     KNGV,
     &                     KNGS,
     &                     VDJV,
     &                     VDJS,
     &                     VPRGL,
     &                     VPRNO,
     &                     VPREV,
     &                     VPRES,
     &                     KCLCND,
     &                     VCLCNV,
     &                     KCLLIM,
     &                     KCLNOD,
     &                     KCLELE,
     &                     VCLDST,
     &                     KDIMP,
     &                     VDIMP,
     &                     KEIMP,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_CLC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(19)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_CLC_CB
      EXTERNAL SV2D_CL_CLC_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQVHND(VCORG)
      KI( 2) = SO_ALLC_REQKHND(KNGV)
      KI( 3) = SO_ALLC_REQKHND(KNGS)
      KI( 4) = SO_ALLC_REQVHND(VDJV)
      KI( 5) = SO_ALLC_REQVHND(VDJS)
      KI( 6) = SO_ALLC_REQVHND(VPRGL)
      KI( 7) = SO_ALLC_REQVHND(VPRNO)
      KI( 8) = SO_ALLC_REQVHND(VPREV)
      KI( 9) = SO_ALLC_REQVHND(VPRES)
      KI(10) = SO_ALLC_REQKHND(KCLCND)
      KI(11) = SO_ALLC_REQVHND(VCLCNV)
      KI(12) = SO_ALLC_REQKHND(KCLLIM)
      KI(13) = SO_ALLC_REQKHND(KCLNOD)
      KI(14) = SO_ALLC_REQKHND(KCLELE)
      KI(15) = SO_ALLC_REQVHND(VCLDST)
      KI(16) = SO_ALLC_REQKHND(KDIMP)
      KI(17) = SO_ALLC_REQVHND(VDIMP)
      KI(18) = SO_ALLC_REQKHND(KEIMP)
      KI(19) = SO_ALLC_REQVHND(VDLG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         HFNC = 0
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_CLC, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC2(HFNC,
     &                            SV2D_CL_CLC_CB,
     &                            IL_B,
     &                            KI_B)
      ENDDO

      SV2D_CL_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_CLC_CB
C
C Description:
C     La fonction SV2D_CL_CLC_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_CLC_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  SV2D_CL_CLC_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(19)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))))

      SV2D_CL_CLC_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_CLC
C
C Description:
C     La fonction SV2D_CL_CLC impose le degré de
C     vitesse en x comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_CLC_DUMMY(IL,
     &                           VCORG,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_CLC_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMF(VCORG,
     &                      KLOCN,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      VSOLC,
     &                      VSOLR,
     &                      KCLCND,
     &                      VCLCNV,
     &                      KCLLIM,
     &                      KCLNOD,
     &                      KCLELE,
     &                      VCLDST,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP,
     &                      VDLG,
     &                      VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMF
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(23)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMF_CB
      EXTERNAL SV2D_CL_ASMF_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQVHND(VCORG)
      KI( 2) = SO_ALLC_REQKHND(KLOCN)
      KI( 3) = SO_ALLC_REQKHND(KNGV)
      KI( 4) = SO_ALLC_REQKHND(KNGS)
      KI( 5) = SO_ALLC_REQVHND(VDJV)
      KI( 6) = SO_ALLC_REQVHND(VDJS)
      KI( 7) = SO_ALLC_REQVHND(VPRGL)
      KI( 8) = SO_ALLC_REQVHND(VPRNO)
      KI( 9) = SO_ALLC_REQVHND(VPREV)
      KI(10) = SO_ALLC_REQVHND(VPRES)
      KI(11) = SO_ALLC_REQVHND(VSOLC)
      KI(12) = SO_ALLC_REQVHND(VSOLR)
      KI(13) = SO_ALLC_REQKHND(KCLCND)
      KI(14) = SO_ALLC_REQVHND(VCLCNV)
      KI(15) = SO_ALLC_REQKHND(KCLLIM)
      KI(16) = SO_ALLC_REQKHND(KCLNOD)
      KI(17) = SO_ALLC_REQKHND(KCLELE)
      KI(18) = SO_ALLC_REQVHND(VCLDST)
      KI(19) = SO_ALLC_REQKHND(KDIMP)
      KI(20) = SO_ALLC_REQVHND(VDIMP)
      KI(21) = SO_ALLC_REQKHND(KEIMP)
      KI(22) = SO_ALLC_REQVHND(VDLG)
      KI(23) = SO_ALLC_REQVHND(VFG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         HFNC = 0
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMF, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC2(HFNC,
     &                            SV2D_CL_ASMF_CB,
     &                            IL_B,
     &                            KI_B)
      ENDDO

      SV2D_CL_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMF_CB
C
C Description:
C     La fonction SV2D_CL_ASMF_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMF_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMF_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(23)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))))

      SV2D_CL_ASMF_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMF_DUMMY(IL,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_ASMF_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMKU
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKU(KLOCE,
     &                       VKE,
     &                       VFE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMKU
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(25)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMKU_CB
      EXTERNAL SV2D_CL_ASMKU_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KLOCE)
      KI( 2) = SO_ALLC_REQVHND(VKE)
      KI( 3) = SO_ALLC_REQVHND(VFE)
      KI( 4) = SO_ALLC_REQVHND(VCORG)
      KI( 5) = SO_ALLC_REQKHND(KLOCN)
      KI( 6) = SO_ALLC_REQKHND(KNGV)
      KI( 7) = SO_ALLC_REQKHND(KNGS)
      KI( 8) = SO_ALLC_REQVHND(VDJV)
      KI( 9) = SO_ALLC_REQVHND(VDJS)
      KI(10) = SO_ALLC_REQVHND(VPRGL)
      KI(11) = SO_ALLC_REQVHND(VPRNO)
      KI(12) = SO_ALLC_REQVHND(VPREV)
      KI(13) = SO_ALLC_REQVHND(VPRES)
      KI(14) = SO_ALLC_REQVHND(VSOLR)
      KI(15) = SO_ALLC_REQKHND(KCLCND)
      KI(16) = SO_ALLC_REQVHND(VCLCNV)
      KI(17) = SO_ALLC_REQKHND(KCLLIM)
      KI(18) = SO_ALLC_REQKHND(KCLNOD)
      KI(19) = SO_ALLC_REQKHND(KCLELE)
      KI(20) = SO_ALLC_REQVHND(VCLDST)
      KI(21) = SO_ALLC_REQKHND(KDIMP)
      KI(22) = SO_ALLC_REQVHND(VDIMP)
      KI(23) = SO_ALLC_REQKHND(KEIMP)
      KI(24) = SO_ALLC_REQVHND(VDLG)
      KI(25) = SO_ALLC_REQVHND(VFG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMKU, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC2(HFNC,
     &                            SV2D_CL_ASMKU_CB,
     &                            IL_B,
     &                            KI_B)
      ENDDO

      SV2D_CL_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMKU_CB
C
C Description:
C     La fonction SV2D_CL_ASMKU_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKU_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMKU_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(25)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))),
     &         KA(SO_ALLC_REQKIND(KA, KI(24))),
     &         KA(SO_ALLC_REQKIND(KA, KI(25))))

      SV2D_CL_ASMKU_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMKU
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKU_DUMMY(IL,
     &                             KLOCE,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             VCLDST,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_ASMKU_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL_ASMK
C
C Description:
C     La fonction SV2D_CL_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMK (KLOCE,
     &                       VKE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMK
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(23)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMK_CB
      EXTERNAL SV2D_CL_ASMK_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KLOCE)
      KI( 2) = SO_ALLC_REQVHND(VKE)
      KI( 3) = SO_ALLC_REQVHND(VCORG)
      KI( 4) = SO_ALLC_REQKHND(KLOCN)
      KI( 5) = SO_ALLC_REQKHND(KNGV)
      KI( 6) = SO_ALLC_REQKHND(KNGS)
      KI( 7) = SO_ALLC_REQVHND(VDJV)
      KI( 8) = SO_ALLC_REQVHND(VDJS)
      KI( 9) = SO_ALLC_REQVHND(VPRGL)
      KI(10) = SO_ALLC_REQVHND(VPRNO)
      KI(11) = SO_ALLC_REQVHND(VPREV)
      KI(12) = SO_ALLC_REQVHND(VPRES)
      KI(13) = SO_ALLC_REQVHND(VSOLR)
      KI(14) = SO_ALLC_REQKHND(KCLCND)
      KI(15) = SO_ALLC_REQVHND(VCLCNV)
      KI(16) = SO_ALLC_REQKHND(KCLLIM)
      KI(17) = SO_ALLC_REQKHND(KCLNOD)
      KI(18) = SO_ALLC_REQKHND(KCLELE)
      KI(19) = SO_ALLC_REQVHND(VCLDST)
      KI(20) = SO_ALLC_REQKHND(KDIMP)
      KI(21) = SO_ALLC_REQVHND(VDIMP)
      KI(22) = SO_ALLC_REQKHND(KEIMP)
      KI(23) = SO_ALLC_REQVHND(VDLG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMK, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC4(HFNC,
     &                            SV2D_CL_ASMK_CB,
     &                            IL_B,
     &                            KI_B,
     &                            SO_ALLC_CST2B(HMTX),
     &                            SO_ALLC_CST2B(F_ASM,0_1))
      ENDDO

      SV2D_CL_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMK_CB
C
C Description:
C     La fonction SV2D_CL_ASMK_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMK_CB(F, IL, KI, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMK_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(23)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))),
     &         HMTX,
     &         F_ASM)

      SV2D_CL_ASMK_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2DASMK
C
C Description:
C     La fonction SV2DASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMK_DUMMY(IL,
     &                            KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_ASMK_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL_ASMKT
C
C Description:
C     La fonction SV2D_CL_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKT(KLOCE,
     &                       VKE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMKT
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(23)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMKT_CB
      EXTERNAL SV2D_CL_ASMKT_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KLOCE)
      KI( 2) = SO_ALLC_REQVHND(VKE)
      KI( 3) = SO_ALLC_REQVHND(VCORG)
      KI( 4) = SO_ALLC_REQKHND(KLOCN)
      KI( 5) = SO_ALLC_REQKHND(KNGV)
      KI( 6) = SO_ALLC_REQKHND(KNGS)
      KI( 7) = SO_ALLC_REQVHND(VDJV)
      KI( 8) = SO_ALLC_REQVHND(VDJS)
      KI( 9) = SO_ALLC_REQVHND(VPRGL)
      KI(10) = SO_ALLC_REQVHND(VPRNO)
      KI(11) = SO_ALLC_REQVHND(VPREV)
      KI(12) = SO_ALLC_REQVHND(VPRES)
      KI(13) = SO_ALLC_REQVHND(VSOLR)
      KI(14) = SO_ALLC_REQKHND(KCLCND)
      KI(15) = SO_ALLC_REQVHND(VCLCNV)
      KI(16) = SO_ALLC_REQKHND(KCLLIM)
      KI(17) = SO_ALLC_REQKHND(KCLNOD)
      KI(18) = SO_ALLC_REQKHND(KCLELE)
      KI(19) = SO_ALLC_REQVHND(VCLDST)
      KI(20) = SO_ALLC_REQKHND(KDIMP)
      KI(21) = SO_ALLC_REQVHND(VDIMP)
      KI(22) = SO_ALLC_REQKHND(KEIMP)
      KI(23) = SO_ALLC_REQVHND(VDLG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMKT, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC4(HFNC,
     &                            SV2D_CL_ASMKT_CB,
     &                            IL_B,
     &                            KI_B,
     &                            SO_ALLC_CST2B(HMTX),
     &                            SO_ALLC_CST2B(F_ASM,0_1))
      ENDDO

      SV2D_CL_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMKT_CB
C
C Description:
C     La fonction SV2D_CL_ASMKT_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKT_CB(F, IL, KI, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMKT_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(23)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))),
     &         HMTX,
     &         F_ASM)

      SV2D_CL_ASMKT_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2DASMKT
C
C Description:
C     La fonction SV2DASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMKT_DUMMY(IL,
     &                            KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_ASMKT_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL_ASMM
C
C Description:
C     La fonction SV2D_CL_ASMM calcule le matrice masse
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMM (KLOCE,
     &                       VKE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMM
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(23)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMM_CB
      EXTERNAL SV2D_CL_ASMM_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KLOCE)
      KI( 2) = SO_ALLC_REQVHND(VKE)
      KI( 3) = SO_ALLC_REQVHND(VCORG)
      KI( 4) = SO_ALLC_REQKHND(KLOCN)
      KI( 5) = SO_ALLC_REQKHND(KNGV)
      KI( 6) = SO_ALLC_REQKHND(KNGS)
      KI( 7) = SO_ALLC_REQVHND(VDJV)
      KI( 8) = SO_ALLC_REQVHND(VDJS)
      KI( 9) = SO_ALLC_REQVHND(VPRGL)
      KI(10) = SO_ALLC_REQVHND(VPRNO)
      KI(11) = SO_ALLC_REQVHND(VPREV)
      KI(12) = SO_ALLC_REQVHND(VPRES)
      KI(13) = SO_ALLC_REQVHND(VSOLR)
      KI(14) = SO_ALLC_REQKHND(KCLCND)
      KI(15) = SO_ALLC_REQVHND(VCLCNV)
      KI(16) = SO_ALLC_REQKHND(KCLLIM)
      KI(17) = SO_ALLC_REQKHND(KCLNOD)
      KI(18) = SO_ALLC_REQKHND(KCLELE)
      KI(19) = SO_ALLC_REQVHND(VCLDST)
      KI(20) = SO_ALLC_REQKHND(KDIMP)
      KI(21) = SO_ALLC_REQVHND(VDIMP)
      KI(22) = SO_ALLC_REQKHND(KEIMP)
      KI(23) = SO_ALLC_REQVHND(VDLG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMM, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC4(HFNC,
     &                            SV2D_CL_ASMM_CB,
     &                            IL_B,
     &                            KI_B,
     &                            SO_ALLC_CST2B(HMTX),
     &                            SO_ALLC_CST2B(F_ASM,0_1))
      ENDDO

      SV2D_CL_ASMM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMM_CB
C
C Description:
C     La fonction SV2D_CL_ASMM_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMM_CB(F, IL, KI, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMM_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(23)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))),
     &         HMTX,
     &         F_ASM)

      SV2D_CL_ASMM_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL_ASMM_DUMMY
C
C Description:
C     ASSEMBLAGE DE LA MATRICE MASSE
C
C Entrée:
C
C Sortie: VKG
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMM_DUMMY (IL,
     &                            KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie ou pas vraiment utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------

      SV2D_CL_ASMM_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMMU
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMMU(KLOCE,
     &                       VKE,
     &                       VFE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL_ASMMU
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER IERR
      INTEGER IL, IC, IT
      INTEGER HFNC
      INTEGER KI(25)
      BYTE, POINTER :: IL_B(:)
      BYTE, POINTER :: KI_B(:)

      INTEGER  SV2D_CL_ASMMU_CB
      EXTERNAL SV2D_CL_ASMMU_CB
C-----------------------------------------------------------------------

C---     Cast en pointeur à byte
      IL_B => SO_ALLC_CST2B(IL)
      KI_B => SO_ALLC_CST2B(KI(:))

C---     Table des handle
      KI( 1) = SO_ALLC_REQKHND(KLOCE)
      KI( 2) = SO_ALLC_REQVHND(VKE)
      KI( 3) = SO_ALLC_REQVHND(VFE)
      KI( 4) = SO_ALLC_REQVHND(VCORG)
      KI( 5) = SO_ALLC_REQKHND(KLOCN)
      KI( 6) = SO_ALLC_REQKHND(KNGV)
      KI( 7) = SO_ALLC_REQKHND(KNGS)
      KI( 8) = SO_ALLC_REQVHND(VDJV)
      KI( 9) = SO_ALLC_REQVHND(VDJS)
      KI(10) = SO_ALLC_REQVHND(VPRGL)
      KI(11) = SO_ALLC_REQVHND(VPRNO)
      KI(12) = SO_ALLC_REQVHND(VPREV)
      KI(13) = SO_ALLC_REQVHND(VPRES)
      KI(14) = SO_ALLC_REQVHND(VSOLR)
      KI(15) = SO_ALLC_REQKHND(KCLCND)
      KI(16) = SO_ALLC_REQVHND(VCLCNV)
      KI(17) = SO_ALLC_REQKHND(KCLLIM)
      KI(18) = SO_ALLC_REQKHND(KCLNOD)
      KI(19) = SO_ALLC_REQKHND(KCLELE)
      KI(20) = SO_ALLC_REQVHND(VCLDST)
      KI(21) = SO_ALLC_REQKHND(KDIMP)
      KI(22) = SO_ALLC_REQVHND(VDIMP)
      KI(23) = SO_ALLC_REQKHND(KEIMP)
      KI(24) = SO_ALLC_REQVHND(VDLG)
      KI(25) = SO_ALLC_REQVHND(VFG)

C---     BOUCLE SUR LES LIMITES
      DO IL=1, EG_CMMN_NCLLIM
         IC = KCLLIM(2, IL)
         IT = KCLCND(2, IC)
         IF (ERR_GOOD())
     &      IERR = SV2D_CL_REQFNC(SV2D_CL_HVFT,
     &                            SV2D_CL_FUNC_ASMMU, IT,
     &                            HFNC)
         IF (ERR_GOOD())
     &      IERR = SO_FUNC_CBFNC2(HFNC,
     &                            SV2D_CL_ASMMU_CB,
     &                            IL_B,
     &                            KI_B)
      ENDDO

      SV2D_CL_ASMMU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMMU_CB
C
C Description:
C     La fonction SV2D_CL_ASMMU_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMMU_CB(F, IL, KI)

      IMPLICIT NONE

      INTEGER  SV2D_CL_ASMMU_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(25)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(IL,
     &         KA(SO_ALLC_REQKIND(KA, KI( 1))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 2))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 3))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 4))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 5))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 6))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 7))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 8))),
     &         KA(SO_ALLC_REQKIND(KA, KI( 9))),
     &         KA(SO_ALLC_REQKIND(KA, KI(10))),
     &         KA(SO_ALLC_REQKIND(KA, KI(11))),
     &         KA(SO_ALLC_REQKIND(KA, KI(12))),
     &         KA(SO_ALLC_REQKIND(KA, KI(13))),
     &         KA(SO_ALLC_REQKIND(KA, KI(14))),
     &         KA(SO_ALLC_REQKIND(KA, KI(15))),
     &         KA(SO_ALLC_REQKIND(KA, KI(16))),
     &         KA(SO_ALLC_REQKIND(KA, KI(17))),
     &         KA(SO_ALLC_REQKIND(KA, KI(18))),
     &         KA(SO_ALLC_REQKIND(KA, KI(19))),
     &         KA(SO_ALLC_REQKIND(KA, KI(20))),
     &         KA(SO_ALLC_REQKIND(KA, KI(21))),
     &         KA(SO_ALLC_REQKIND(KA, KI(22))),
     &         KA(SO_ALLC_REQKIND(KA, KI(23))),
     &         KA(SO_ALLC_REQKIND(KA, KI(24))),
     &         KA(SO_ALLC_REQKIND(KA, KI(25))))

      SV2D_CL_ASMMU_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_ASMMU_DUMMY
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_ASMMU_DUMMY(IL,
     &                             KLOCE,
     &                             VKE,
     &                             VFE,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLR,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             VCLDST,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             VDLG,
     &                             VFG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl.fc'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SV2D_CL_ASMMU_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_CHKDIM
C
C Description:
C     La fonction utilitaire SV2D_CL_CHKDIM contrôle les dimensions
C     d'une condition limite. Le nombre de valeurs est comparé aux items
C     de KVAL (qui peut être vide), contrôlé qu'il est dans les bornes
C     (si celles-ci sont non négatives).
C     alors le nombre de valeurs sera également contrôlé.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     KCLLIM         Liste des limites
C     KVAL           Liste de nombre de valeurs de la condition
C     KRNG           Bornes de nombre de valeurs de la condition
C     KKND           Type(s) de CL
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE SV2D_CL_CHKDIM_M
      
      CONTAINS
      
      INTEGER
     &FUNCTION SV2D_CL_CHKDIM(IL,
     &                        KCLCND,
     &                        KCLLIM,
     &                        KVAL,
     &                        KRNG,
     &                        KKND)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER, INTENT(IN) :: IL
      INTEGER, INTENT(IN) :: KCLCND( 4, LM_CMMN_NCLCND)
      INTEGER, INTENT(IN) :: KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER, INTENT(IN) :: KVAL(:)
      INTEGER, INTENT(IN) :: KRNG(2)
      INTEGER, INTENT(IN) :: KKND(:)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER I_ERROR

      INTEGER I, IC
      INTEGER ISZ_L(2), ISZ_G(2)
      INTEGER ITYP
      INTEGER NVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IL .GE. 1 .AND. IL .LE. EG_CMMN_NCLLIM)
D     CALL ERR_PRE(KRNG(1) .LE. KRNG(2))
C-----------------------------------------------------------------------

      ! ---  Extrait ici en cas d'erreur
      IC = KCLLIM(2, IL)
      NVAL = KCLCND(4,IC)-KCLCND(3,IC)+1
      ITYP = KCLLIM(7, IL)

      ! ---  Contrôle primaire du nombre de noeuds et d'éléments
      ISZ_L(1) = KCLLIM(4,IL)-KCLLIM(3,IL)+1    ! Nombre de noeuds
      ISZ_L(2) = KCLLIM(6,IL)-KCLLIM(5,IL)+1    ! Nombre d'éléments
      CALL MPI_ALLREDUCE(ISZ_L, ISZ_G, 2, MP_TYPE_INT(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)

      IF (ISZ_G(1) .LT. 1) GOTO 9900
      IF (ISZ_G(2) .LT. 1) GOTO 9901

      ! ---  Contrôle du nombre de valeurs
      IF (SIZE(KVAL) .GT. 0 .AND.
     &    .NOT. ANY(KVAL .EQ. NVAL)) GOTO 9902

      ! ---  Contrôle des bornes
      IF (KRNG(1) .GE. 0 .AND. 
     &    (NVAL .LT. KRNG(1) .OR. NVAL .GT. KRNG(2))) GOTO 9903
      
      ! ---  Contrôle du type
      IF (SIZE(KKND) .GT. 0 .AND.
     &    .NOT. ANY(KKND .EQ. ITYP)) GOTO 9904
      
      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_NBR_ELE_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_TYP_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  WRITE(ERR_BUF, '(2A,I6)') 'MSG_LIMITE#<30>#', '= ', IL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_TYPE#<30>#', '= ', KCLCND(2,IC)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_NBR_NOEUDS#<30>#', '= ', ISZ_G(1)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_NBR_ELEMENTS#<30>#', '= ', ISZ_G(2)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_NBR_VAL#<30>#', '= ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_TYPE_LIM#<30>#', '= ', ITYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_NBR_VAL_VALIDES', ':'
      CALL ERR_AJT(ERR_BUF)
      IF (SIZE(KVAL) .LE. 0) THEN
         WRITE(ERR_BUF, '(A,I6)') '   ', 0
         CALL ERR_AJT(ERR_BUF)
      ELSE
         WRITE(ERR_BUF, '(A,5I6)') '   ',(KVAL(I),I=1,MIN(SIZE(KVAL),5))
         CALL ERR_AJT(ERR_BUF)
      ENDIF      
      IF (KRNG(1) .GT. 0) THEN
         WRITE(ERR_BUF, '(2A,2I6,A)') 
     &      'MSG_RANGE_VALIDE', ': [', KRNG(1), KRNG(2), ']'
         CALL ERR_AJT(ERR_BUF)
      ENDIF      
      IF (SIZE(KKND) .GT. 0) THEN
         WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_LIM_VALIDES', ':'
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF, '(A,5I6)') '   ',(KKND(I),I=1,MIN(SIZE(KKND),5))
         CALL ERR_AJT(ERR_BUF)
      ENDIF      
      GOTO 9999
      
9999  CONTINUE
      SV2D_CL_CHKDIM = ERR_TYP()
      RETURN
      END FUNCTION SV2D_CL_CHKDIM

      END MODULE SV2D_CL_CHKDIM_M

C************************************************************************
C Sommaire:  SV2D_CL_COD1L
C
C Description:
C     La fonction utilitaire SV2D_CL_COD1L assigne le code de
C     condition limite pour une condition qui n'a qu'une limite,
C     la limite destination.
C
C Entrée:
C     IL_DST         Indice de la limite destination
C     ID_DST         Indice du DDL destination
C     IT_DST         Type de la limite destination
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Le contrôle comme dans COD2L est aussi coûteux que d'assigner
C     directement.
C************************************************************************
      FUNCTION SV2D_CL_COD1L(IL_DST,
     &                       ID_DST,
     &                       IT_DST,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       KDIMP)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL_DST, ID_DST, IT_DST
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER I, IN
      INTEGER INDEB_DST, INFIN_DST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IL_DST .GE. 1 .AND. IL_DST .LE. EG_CMMN_NCLLIM)
D     CALL ERR_PRE(ID_DST .GE. 1 .AND. ID_DST .LE. LM_CMMN_NDLN)
D     CALL ERR_PRE(IT_DST .GT. 0)
C-----------------------------------------------------------------------

C---     Indices
      INDEB_DST = KCLLIM(3, IL_DST)
      INFIN_DST = KCLLIM(4, IL_DST)

C---     Impose la limite DESTINATION (self)
      DO I = INDEB_DST, INFIN_DST
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            KDIMP(ID_DST,IN) = IBSET(KDIMP(ID_DST,IN), IT_DST)
         ENDIF
      ENDDO

      SV2D_CL_COD1L = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL_COD2L
C
C Description:
C     La fonction utilitaire SV2D_CL_COD2L assigne le code de
C     condition limite pour une condition qui a deux limites,
C     la limite destination et la limite source. Si la limite
C     source n'est pas encore connectée (i.e contient encore
C     le CRC32) alors elle sera connectée et en sortie IL_SRC
C     contiendra l'indice de la limite.
C
C Entrée:
C     IL_DST         Indice de la limite destination
C     ID_DST         Indice du DDL destination
C     IT_DST         Type de la limite destination
C     IL_SRC         Indice ou CRC32 de la limite source
C     ID_SRC         Indice du DDL source
C     IT_SRC         Type de la limite source
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     IL_SRC         Indice de la limite connectée
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL_COD2L(IL_DST,
     &                       ID_DST,
     &                       IT_DST,
     &                       IL_SRC,
     &                       ID_SRC,
     &                       IT_SRC,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       KDIMP)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL_DST, ID_DST, IT_DST
      INTEGER IL_SRC, ID_SRC, IT_SRC
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fc'

      INTEGER I, IN
      INTEGER ICRC
      INTEGER INDEB_DST, INFIN_DST
      INTEGER INDEB_SRC, INFIN_SRC
      INTEGER NVU
      LOGICAL DEJAVU
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IL_DST .GE. 1 .AND. IL_DST .LE. EG_CMMN_NCLLIM)
D     CALL ERR_PRE(ID_DST .GE. 1 .AND. ID_DST .LE. LM_CMMN_NDLN)
D     CALL ERR_PRE(IT_DST .GT. 0)
D     CALL ERR_PRE(IL_SRC .NE. IL_DST)
D     CALL ERR_PRE(ID_SRC .GE. 1 .AND. ID_SRC .LE. LM_CMMN_NDLN)
D     CALL ERR_PRE(IT_SRC .GT. 0)
D     CALL ERR_PRE(IT_SRC .NE. IT_DST)
C-----------------------------------------------------------------------

C---     Indices
      INDEB_DST = KCLLIM(3, IL_DST)
      INFIN_DST = KCLLIM(4, IL_DST)
      IF (IL_SRC .GE. 1 .AND. IL_SRC .LE. EG_CMMN_NCLLIM) THEN
         INDEB_SRC = KCLLIM(3, IL_SRC)
         INFIN_SRC = KCLLIM(4, IL_SRC)
         DEJAVU = .TRUE.
      ELSE
         INDEB_SRC =  1
         INFIN_SRC = -1
         DEJAVU = .FALSE.
      ENDIF

C---     Détecte si on a déjà passé
      IF (DEJAVU) THEN
         NVU = 0
         DO I = INDEB_DST, INFIN_DST
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               IF (BTEST(KDIMP(ID_DST,IN), IT_DST)) NVU = NVU + 1
            ENDIF
         ENDDO
         DEJAVU = NVU .EQ. (INFIN_DST-INDEB_DST+1)
      ENDIF
      IF (DEJAVU) THEN
         NVU = 0
         DO I = INDEB_SRC, INFIN_SRC
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               IF (BTEST(KDIMP(ID_SRC,IN), IT_SRC)) NVU = NVU + 1
            ENDIF
         ENDDO
         DEJAVU = NVU .EQ. (INFIN_SRC-INDEB_SRC+1)
      ENDIF

C---     Fait le lien avec la limite source
      IF (.NOT. DEJAVU) THEN
         ICRC = IL_SRC
         IL_SRC = -1
         DO I=1,EG_CMMN_NCLLIM
            IF (KCLLIM(1,I) .EQ. ICRC) IL_SRC = I
         ENDDO
         IF (IL_SRC .LE. 0) GOTO 9900
      ENDIF

C---     Impose la limite DESTINATION (self)
      IF (.NOT. DEJAVU) THEN
         INDEB_DST = KCLLIM(3, IL_DST)
         INFIN_DST = KCLLIM(4, IL_DST)
         DO I = INDEB_DST, INFIN_DST
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(ID_DST,IN) = IBSET(KDIMP(ID_DST,IN), IT_DST)
            ENDIF
         ENDDO
      ENDIF

C---     Impose la limite SOURCE
      IF (.NOT. DEJAVU) THEN
         INDEB_SRC = KCLLIM(3, IL_SRC)
         INFIN_SRC = KCLLIM(4, IL_SRC)
         DO I = INDEB_SRC, INFIN_SRC
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(ID_SRC,IN) = IBSET(KDIMP(ID_SRC,IN), IT_SRC)
            ENDIF
         ENDDO
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL_COD2L = ERR_TYP()
      RETURN
      END
