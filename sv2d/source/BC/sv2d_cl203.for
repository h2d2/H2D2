C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Condition astronomiques de marée (Dirichlet) sur le niveau d'eau h
C
C Functions:
C   Public:
C     INTEGER SV2D_CL203_000
C     INTEGER SV2D_CL203_999
C     INTEGER SV2D_CL203_COD
C     INTEGER SV2D_CL203_PRC
C     INTEGER SV2D_CL203_HLP
C   Private:
C     INTEGER SV2D_CL203_INIVTBL
C     CHARACTER*(16) SV2D_CL203_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL203_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl203.fc'

      DATA SV2D_CL203_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL203_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL203_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'
      INCLUDE 'sv2d_cl203.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL203_INIVTBL()
      IF (ERR_GOOD()) IERR = SP_TIDE_CFGTTD()

      SV2D_CL203_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL203_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL203_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL203_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl203.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL203_HLP
      EXTERNAL SV2D_CL203_COD
      EXTERNAL SV2D_CL203_PRC
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL203_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL203_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL203_TPN(1:LTPN), SV2D_CL203_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL203_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL203_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL203_TYP,
     &                     SV2D_CL203_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL203_TYP,
     &                     SV2D_CL203_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_PRC, SV2D_CL203_TYP,
     &                     SV2D_CL203_PRC)

      SV2D_CL203_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL203_COD
C
C Description:
C     La fonction SV2D_CL203_COD assigne les codes de conditions limites.
C     Sur la limite <code>IL</code>, elle impose le degré de niveau d'eau
C     comme condition de Dirichlet.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL203_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'
      INCLUDE 'sv2d_cl203.fc'

      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN
      INTEGER IVDEB, IVFIN
      INTEGER NVAL
      REAL*8  C
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL203_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)

C---     Contrôles généraux des valeurs
      NVAL = KCLCND(4, IC) - KCLCND(3, IC) + 1
      IF (KCLLIM(4,IL)-KCLLIM(3,IL)+1 .LT. 1) GOTO 9900
      IF (MOD(NVAL, 3) .NE. 1) GOTO 9902
      IF (NVAL/3       .LT. 1) GOTO 9902

C---     Contrôles les composantes
      IVDEB = KCLCND(3, IC)
      IVFIN = KCLCND(4, IC)
      DO IV=IVDEB+1,IVFIN,3
         C = VCLCNV(IV)       ! composante
         IF (.NOT. SP_TIDE_CMPEXIST(NINT(C))) GOTO 9904
      ENDDO

C---     Assigne les codes
      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_DIRICHLET)
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTIENT', ': ', NVAL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTEND', ': ', '(1 + N*3)'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A)') 'ERR_UNKNOWN_TIDE_COMPONENT', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL203_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL203_PRC
C
C Description:
C     La fonction SV2D_CL203_PRC impose le degré de niveau d'eau
C     comme condition de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     IL             Indice de la limite
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     Conceptuellement
C     (
C        T0
C        (c,a,p),
C        (c,a,p), ...
C     )
C************************************************************************
      FUNCTION SV2D_CL203_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL203_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fi'
      INCLUDE 'sv2d_cl203.fc'

      INTEGER I, IC, IN, IV
      INTEGER INDEB, INFIN, IVDEB, IVFIN
      REAL*8  H, C, A, D
      REAL*8  T, T0
      LOGICAL, PARAMETER :: DONODCOR = .TRUE.
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL203_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 4)
      IVDEB = KCLCND(3, IC)
      IVFIN = KCLCND(4, IC)

      T0 = VCLCNV(IVDEB)   ! Temps de ref.
      T  = LM_CMMN_TSIM    ! Temps de la simu.
      IVDEB = IVDEB + 1

      H = 0.0D0
      DO IV=IVDEB,IVFIN,3
         C = VCLCNV(IV+0)     ! composante
         A = VCLCNV(IV+1)     ! amplitude
         D = VCLCNV(IV+2)     ! déphasage
         H = H + SP_TIDE_TTDE_HD(NINT(C), A, D, T, T0, DONODCOR)
      ENDDO
      CALL ERR_ASR(.FALSE.)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I=INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) VDIMP(3,IN) = H
      ENDDO

      SV2D_CL203_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL203_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL203_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>203</b>: <br>
C  Astronomical tide Dirichlet condition on the water level.
C  The water level is applied on the whole boundary.
C  <p>
C  Conceptually the condition is described by:
C  <pre>
C         (
C            T0
C            (c,a,p),
C            (c,a,p), ...
C         )</pre>
C  where T0 is the reference time,
C  c the tide constituent,
C  a the amplitude and
C  p the phase shift.
C  The water level H is the sum of all constituents contributions.
C  <p>
C  <ul>
C     <li>Kind: Dirichlet</li>
C     <li>Code: 203</li>
C     <li>Values: T0  c  a  p  ... </li>
C     <li>Units: s  [-]  m  o</li>
C     <li>Example:  203  10   m2  2.0  0.0</li>
C  </ul>
C</comment>

      SV2D_CL203_REQHLP = 'bc_type_203'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL203_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL203_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL203_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl203.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl203.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl203.hlp')

      SV2D_CL203_HLP = ERR_TYP()
      RETURN
      END
