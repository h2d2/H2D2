C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: sv2d_cl129.for,v 1.24 2016/01/28 17:49:08 secretyv Exp $
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau actuel.
C     Formulation Manning variable
C
C Functions:
C   Public:
C     INTEGER SV2D_CL129_000
C     INTEGER SV2D_CL129_999
C     INTEGER SV2D_CL129_COD
C     INTEGER SV2D_CL129_ASMKU
C     INTEGER SV2D_CL129_ASMM
C     INTEGER SV2D_CL129_ASMMU
C   Private:
C     INTEGER SV2D_CL129_INIVTBL
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL129_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl129.fc'

      DATA SV2D_CL129_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL129_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl129.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL129_INIVTBL()

      SV2D_CL129_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL129_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL129_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl129.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL129_HLP
      EXTERNAL SV2D_CL129_COD
      EXTERNAL SV2D_CL129_ASMKU
!!!      EXTERNAL SV2D_CL129_ASMM
!!!      EXTERNAL SV2D_CL129_ASMMU
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL129_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL129_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL129_TPN(1:LTPN), SV2D_CL129_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL129_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL129_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL129_TYP,
     &                     SV2D_CL129_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD, SV2D_CL129_TYP,
     &                    SV2D_CL129_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKU,SV2D_CL129_TYP,
     &                    SV2D_CL129_ASMKU)
     !!! IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMM,SV2D_CL129_TYP,
     !!!&                    SV2D_CL129_ASMM)
     !!! IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMMU,SV2D_CL129_TYP,
     !!!&                    SV2D_CL129_ASMMU)

      SV2D_CL129_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL129_COD
C
C Description:
C     La fonction SV2D_CL129_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl129.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER, PARAMETER :: KVAL(1) = (/2/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(1) = (/EG_TPLMT_1SGMT/) ! c.f. note
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL129_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_CL_DEBIT)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL129_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL129_ASMKU
C
C Description:
C     La fonction SV2D_CL129_ASMKU assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     L'idée est de prolonger par un élément Q4 rectangulaire,
C     normal à l'élément de frontière, et de longueur L. Ce Q4 est
C     splitté en 2 sous-éléments pour s'adapter au T6L. La continuité
C     est intégrée dans le repère normal-tangent.
C
C     Ici, u est la vitesse normale et v la tangentielle. On a u3, u4 qui
C     sont les valeurs imposées (calculées par répartition), et on a v3 = 0
C     et v4 = 0.
C
C     Pour répartir le débit total, il faut un niveau. Celui-ci est calculé
C     comme le niveau moyen sur la limite.
C************************************************************************
      FUNCTION SV2D_CL129_ASMKU(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (2)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl129.fc'

      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IV
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO2, NO3
      REAL*8  HQ4
      REAL*8  F1, F2, F3, P1, P2, P3
      REAL*8  H1, H3, HM, BH
      REAL*8  QTOT, QSPEC
      REAL*8  QS, QG
      REAL*8  QN1, QN2, QN3, QN4, QN5, QN6
      REAL*8  QN_E1, QN_E2
      REAL*8  QT1, QT2, QT3
      REAL*8  QT_L
      REAL*8  C, M(2)
      REAL*8  VNX, VNY, VTX, VTY, VKX, VEY
      REAL*8  VL(2), VG(2)

      REAL*8, PARAMETER :: CINQ_TIER = 5.0D0/3.0D0
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL129_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3,IC)
      QTOT = VCLCNV(IV)
      HQ4  = VCLCNV(IV+1)

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

C---     Niveau moyen pour la répartition
      HM = ZERO
      BH = ZERO     ! Largeur
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO3 = KNGS(3,IE)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         H1 = VDLG(3, NO1)
         H3 = VDLG(3, NO3)

C---        Intègre
         HM = HM +      VDJS(3,IE)*(H1+H3)
         BH = BH + DEUX*VDJS(3,IE)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL = (/ HM, BH /)
      CALL MPI_ALLREDUCE(VL, VG, 2, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      HM = VG(1)
      BH = VG(2)
D     CALL ERR_ASR(BH .GT. ZERO)
      HM = HM / BH

C---     Surface mouillée
      QS = 0.0D0
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO2 = KNGS(2,IE)
         NO3 = KNGS(3,IE)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 299
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 299

C---        Valeurs nodales
         F1 = MAX(     VPRNO(SV2D_IPRNO_N, NO1), PETIT)  ! Manning
         P1 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO1), PETIT)  ! Profondeur
         F2 = MAX(     VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO2), PETIT)
         F3 = MAX(     VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO3), PETIT)

C---        Règle de répartition d'après Manning
         QN1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         QN2 = (P2**CINQ_TIER) / F2
         QN3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + UN_2*VDJS(3,IE)*(QN1+QN2+QN2+QN3)
299      CONTINUE
      ENDDO

C---    Sync et contrôle
      VL = (/ QS, 0.0D0 /)
      CALL MPI_ALLREDUCE(VL, VG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS = VG(1)
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO2 = KNGS(2,IE)
         NO3 = KNGS(3,IE)

C---        Coefficient
         VTX =  VDJS(1,IE)
         VTY =  VDJS(2,IE)
         VNY = -VDJS(1,IE)    ! VNY = -VTX
         VNX =  VDJS(2,IE)    ! VNX =  VTY
         VKX =  UN_2 * HQ4    ! kx * DETJ = (A*B) / A
         VEY =  VDJS(3,IE)    ! ey * DETJ = (A*B) / B
         C = - UN_24

C---        Valeurs nodales
         F1 = MAX(     VPRNO(SV2D_IPRNO_N, NO1), PETIT)  ! Manning
         P1 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO1), PETIT)  ! Profondeur
         F2 = MAX(     VPRNO(SV2D_IPRNO_N, NO2), PETIT)
         P2 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO2), PETIT)
         F3 = MAX(     VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 = MAX(HM - VPRNO(SV2D_IPRNO_Z, NO3), PETIT)

C---        Règle de répartition d'après Manning
         QN6 = QSPEC * (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         QN5 = QSPEC * (P2**CINQ_TIER) / F2
         QN4 = QSPEC * (P3**CINQ_TIER) / F3

C---        DDL
         QN1 = VDLG(1,NO1)*VNX + VDLG(2,NO1)*VNY
         QN2 = VDLG(1,NO2)*VNX + VDLG(2,NO2)*VNY
         QN3 = VDLG(1,NO3)*VNX + VDLG(2,NO3)*VNY
         QT1 = VDLG(1,NO1)*VTX + VDLG(2,NO1)*VTY
         QT2 = VDLG(1,NO2)*VTX + VDLG(2,NO2)*VTY
         QT3 = VDLG(1,NO3)*VTX + VDLG(2,NO3)*VTY

C---        Assemblage du vecteur global (mixte Q4-Q6L)
         QN_E1 = QN1 + QN2 + QN5 + QN6
         QN_E2 = QN2 + QN3 + QN4 + QN5
         QT_L  = QT1 + QT2 + QT2 + QT3
         M(1) = C * (-4*VKX*(QT_L) - VEY*(5*QN_E1 +   QN_E2))
         M(2) = C * ( 4*VKX*(QT_L) - VEY*(  QN_E1 + 5*QN_E2))

         KLOCE(:) = (/ KLOCN(3, NO1), KLOCN(3, NO3) /)
         IERR = SP_ELEM_ASMFE(2, KLOCE, M, VFG)
!         WRITE(LOG_BUF,*) I, M(1), M(2)
!         CALL LOG_ECRIS(LOG_BUF)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_CL129_TYP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_DEBIT: ', QTOT
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_SURFACE: ', QS
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL129_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL129_ASMM
C
C Description:
C     La fonction SV2D_CL129_ASMM assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C M = 1/9 [4, 2, 1, 2]
C         [2, 4, 2, 1]
C         [1, 2, 4, 2]
C         [2, 1, 2, 4]
C************************************************************************
      FUNCTION SV2D_CL129_ASMM   (IL,
     &                            KLOCE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_ASMM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (:)
      REAL*8   VKE   (:)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! sortie ou pas vraiment utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER I, IC, IV, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8 HQ4, COEF
C-----------------------------------------------------------------


      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3,IC)
      HQ4  = VCLCNV(IV+1)

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

      DO I = IEDEB, IEFIN
         IES = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)
         COEF = UN_18 * VDJS(3,IES) * HQ4 ! 2 / (4*9)

C---        Matrice masse
         VKE(1) = COEF+COEF   ! (1,1)
         VKE(2) = COEF        ! (2,1)
         VKE(3) = COEF        ! (1,2)
         VKE(4) = COEF+COEF   ! (2,2)

C---       Assemblage de la matrice
         KLOCE(:) = (/ KLOCN(3, NO1), KLOCN(3, NO3) /)
         IERR = F_ASM(HMTX, 2, KLOCE, VKE)
      ENDDO

      SV2D_CL129_ASMM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL129_ASMMU
C
C Description:
C     La fonction SV2D_CL129_ASMMU assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_ASMMU(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_ASMMU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (:)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (:)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER I, IC, IV, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8 HQ4, COEF
      REAL*8 H1, H3
      REAL*8 VDLE(2)
C-----------------------------------------------------------------


      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3,IC)
      HQ4  = VCLCNV(IV+1)

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

      DO I = IEDEB, IEFIN
         IES = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)
         COEF = UN_18 * VDJS(3,IES) * HQ4 ! 2 / (4*9)

C---        VDL
         H1 = VDLG(3, NO1)
         H3 = VDLG(3, NO3)

C---        Matrice masse
         VFE(1) = COEF * (H1+H1 + H3)
         VFE(2) = COEF * (H1 + H3+H3)

C---       Assemblage de la matrice
         KLOCE(:) = (/ KLOCN(3, NO1), KLOCN(3, NO3) /)
         IERR = SP_ELEM_ASMFE(2, KLOCE, VFE, VFG)
      ENDDO

      SV2D_CL129_ASMMU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL129_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL129_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>129</b>: this is a pre-alfa state condition
C  <p>
C  L'idée est de prolonger par un élément Q4 rectangulaire,
C  normal à l'élément de frontière, et de longueur L. Ce Q4 est
C  splitté en 2 sous-éléments pour s'adapter au T6L. La continuité
C  est intégrée dans le repère normal-tangent.
C
C  Ici, u est la vitesse normale et v la tangentielle. On a u3, u4 qui
C  sont les valeurs imposées (calculées par répartition), et on a v3 = 0
C  et v4 = 0.
C
C  Pour répartir le débit total, il faut un niveau. Celui-ci est calculé
C  comme le niveau moyen sur la limite.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 129</li>
C     <li>Values: Q  HQ4???</li>
C     <li>Units: m^3/s  m</li>
C     <li>Example:  129  -500  400</li>
C  </ul>
C  (Status: Very Experimental).
C</comment>

      SV2D_CL129_REQHLP = 'bc_type_129'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL129_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL129_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL129_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl129.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl129.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl129.hlp')

      SV2D_CL129_HLP = ERR_TYP()
      RETURN
      END
