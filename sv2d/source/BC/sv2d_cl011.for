C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SV2D_CL011_000
C     INTEGER SV2D_CL011_999
C     INTEGER SV2D_CL011_COD
C     INTEGER SV2D_CL011_ASMF
C     INTEGER SV2D_CL011_HLP
C   Private:
C     INTEGER SV2D_CL011_INIVTBL
C     CHARACTER*(16) SV2D_CL011_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL011_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL011_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl011.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL011_INIVTBL()

      SV2D_CL011_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL011_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL011_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL011_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl011.fc'

      INTEGER IERR
      INTEGER HVFT
      EXTERNAL SV2D_CL011_HLP
      EXTERNAL SV2D_CL011_COD
      EXTERNAL SV2D_CL011_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL011_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL011_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_HLP, SV2D_CL011_TYP,
     &                    SV2D_CL011_HLP)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_COD, SV2D_CL011_TYP,
     &                    SV2D_CL011_COD)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMF,SV2D_CL011_TYP,
     &                    SV2D_CL011_ASMF)

      SV2D_CL011_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL011_COD
C
C Description:
C     La fonction SV2D_CL011_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL011_COD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl011.fc'

      INTEGER I, IC, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL011_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      IF (KCLCND(4,IC)-KCLCND(3,IC)+1 .LT. 1) GOTO 9900

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_CL_DEBIT)
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL011_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL011_ASMF
C
C Description:
C     La fonction SV2D_CL011_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C     D'après la loi de Manning:
C        u = H**(2/3) / n * pente
C     en posant la pente constante (==1), le débit spécifique est
C        q = H**(5/3) / n
C     qui est intégré.
C************************************************************************
      FUNCTION SV2D_CL011_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL011_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS) !Pas utilisé
      REAL*8  VSOLC (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl011.fc'

      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IV
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  F1, F3, P1, P3
      REAL*8  QTOT, QSPEC
      REAL*8  Q1, Q3, QS, QG
      REAL*8  C

      REAL*8  CINQ_TIER
      PARAMETER (CINQ_TIER = 5.0D0/3.0D0)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL011_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3,IC)
      QTOT = VCLCNV(IV)

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

C---     Intègre sur la limite
      QS = 0.0D0
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO3 = KNGS(3,IE)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        Intègre
         QS = QS + VDJS(3,IE)*(Q1+Q3)
199      CONTINUE
      ENDDO

C---    Sync et contrôle
      CALL MPI_ALLREDUCE(QS, QG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      QS = QG
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Assemble
      DO I = IEDEB, IEFIN
         IE = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IE)
         NO3 = KNGS(3,IE)

C---        Coefficient
         C = -UN_3*QSPEC*VDJS(3,IE)

C---        Valeurs nodales
         F1 = MAX(VPRNO(SV2D_IPRNO_N, NO1), PETIT)    ! Manning
         P1 =     VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         F3 = MAX(VPRNO(SV2D_IPRNO_N, NO3), PETIT)
         P3 =     VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition d'après Manning
         Q1 = (P1**CINQ_TIER) / F1           ! (H**5/3) / n
         Q3 = (P3**CINQ_TIER) / F3

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), C*(Q1+Q1 + Q3), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), C*(Q3+Q3 + Q1), VFG)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_CL011_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL011_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL011_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL011_REQHLP
C------------------------------------------------------------------------

C<comment>
C *****<br>
C This boundary condition has been DEPRECATED. Please use condition <b>125</b> instead.<br>
C *****
C <p>
C  Boundary condition of type <b>011</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, taking into account the current water level h.
C  The velocity distribution is based on a Manning formulation with varying coefficient (i.e. depth and Manning dependent).
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 11</li>
C     <li>Values: Q</li>
C     <li>Units: m^3/s</li>
C     <li>Example:  11   -500</li>
C  </ul>
C</comment>

      SV2D_CL011_REQHLP = 'bc_type_011'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL011_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL011_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL011_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl011.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl011.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl011.hlp')

      SV2D_CL011_HLP = ERR_TYP()
      RETURN
      END
