C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Impose les valeurs de h d'une autre limite. Les valeurs sont prises
C     dans l'ordre, noeud par noeud.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL133_000
C     INTEGER SV2D_CL133_999
C     INTEGER SV2D_CL133_COD
C     INTEGER SV2D_CL133_CLC
C     INTEGER SV2D_CL133_ASMF
C     INTEGER SV2D_CL133_ASMKU
C     INTEGER SV2D_CL133_ASMK
C     INTEGER SV2D_CL133_ASMKT
C     INTEGER SV2D_CL133_HLP
C   Private:
C     INTEGER SV2D_CL133_INIVTBL
C     CHARACTER*(16) SV2D_CL133_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL133_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl133.fc'

      DATA SV2D_CL133_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL133_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL133_INIVTBL()

      SV2D_CL133_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL133_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL133_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL133_HLP
      EXTERNAL SV2D_CL133_COD
      EXTERNAL SV2D_CL133_CLC
      EXTERNAL SV2D_CL133_ASMF
      EXTERNAL SV2D_CL133_ASMKU
      EXTERNAL SV2D_CL133_ASMK
      EXTERNAL SV2D_CL133_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL133_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL133_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL133_TPN(1:LTPN), SV2D_CL133_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL133_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL133_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL133_TYP,
     &                     SV2D_CL133_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD,  SV2D_CL133_TYP,
     &                    SV2D_CL133_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_CLC,  SV2D_CL133_TYP,
     &                    SV2D_CL133_CLC)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF, SV2D_CL133_TYP,
     &                    SV2D_CL133_ASMF)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKU,SV2D_CL133_TYP,
     &                    SV2D_CL133_ASMKU)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMK, SV2D_CL133_TYP,
     &                    SV2D_CL133_ASMK)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKT,SV2D_CL133_TYP,
     &                    SV2D_CL133_ASMKT)

      SV2D_CL133_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL133_COD
C
C Description:
C     La fonction SV2D_CL133_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Valeurs lues: il_src, a0, a1
C************************************************************************
      FUNCTION SV2D_CL133_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IL_DST, ID_DST
      INTEGER IL_SRC, ID_SRC
      INTEGER, PARAMETER :: KVAL(1) = (/3/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(0) = (/INTEGER::/) ! Zero-sized array
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)
      IV = KCLCND(3, IC)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         IL_DST = IL
         ID_DST = 3        ! h
         IL_SRC = NINT( VCLCNV(IV) )
         ID_SRC = 3        ! h
         IERR = SV2D_CL_COD2L(IL_DST, ID_DST, EA_TPCL_WEAKDIR,
     &                        IL_SRC, ID_SRC, SV2D_CL_COPIE,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
         IF (ERR_GOOD()) VCLCNV(IV) = IL_SRC
      ENDIF

      SV2D_CL133_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL133_CLC
C
C Description:
C     La fonction SV2D_CL133_CLC assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_CLC(IL,
     &                        VCORG,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl133.fc'

      REAL*8  A0, A1
      INTEGER I, IC, IV
      INTEGER IL_DST, IL_SRC
      INTEGER INDEB_DST, INFIN_DST
      INTEGER INDEB_SRC, INFIN_SRC
      INTEGER IN_DST, IN_SRC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 3)
      IV = KCLCND(3,IC)
      IL_SRC = NINT( VCLCNV(IV) )
      A0 = VCLCNV(IV+1)
      A1 = VCLCNV(IV+2)

      IL_DST = IL
      INDEB_DST = KCLLIM(3, IL_DST)
      INFIN_DST = KCLLIM(4, IL_DST)
      INDEB_SRC = KCLLIM(3, IL_SRC)
D     INFIN_SRC = KCLLIM(4, IL_SRC)
D     CALL ERR_ASR((INFIN_DST-INDEB_DST) .EQ. (INFIN_SRC-INDEB_SRC))

      DO I = 0, (INFIN_DST-INDEB_DST)
         IN_DST = KCLNOD(INDEB_DST+I)
         IN_SRC = KCLNOD(INDEB_SRC+I)

         VDIMP(3, IN_DST) = A0 + A1*VDLG(3, IN_SRC)
      ENDDO

      SV2D_CL133_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL133_ASMF
C
C Description:
C     La fonction SV2D_CL133_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose les valeurs de h d'une
C     autre limite. Les valeurs sont prises dans l'ordre, noeud par noeud.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
      INTEGER I, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  C, V1, V3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
C-----------------------------------------------------------------------

      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Assemble
      DO I = IEDEB, IEFIN
         IES = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)

C---        Valeurs nodales
         V1 = VDIMP(3,NO1)
         V3 = VDIMP(3,NO3)

C---        Assemblage du vecteur global lumped
         C = VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), C*V1, VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), C*V3, VFG)

      ENDDO

      SV2D_CL133_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL133_ASMKU
C
C Description:
C     La fonction SV2D_CL133_ASMKU est vide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_ASMKU(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
      INTEGER I, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  C, V1, V3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
C-----------------------------------------------------------------------

      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

      CALL LOG_TODO('AJOUTER INTEGRALE CONTOUR CONTINUITÉ')
      CALL ERR_ASR(.FALSE.)
      
C---     Assemble
      DO I = IEDEB, IEFIN
         IES = KCLELE(I)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)

C---        Valeurs nodales
         V1 = VDLG(3,NO1)
         V3 = VDLG(3,NO3)

C---        Assemblage du vecteur global lumped
         C = VDJS(3,IES)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), C*V1, VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), C*V3, VFG)

      ENDDO

      SV2D_CL133_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2DASMK
C
C Description:
C     La fonction SV2DASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_ASMK(IL,
     &                         KLOCE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER NDLE_LCL
      PARAMETER (NDLE_LCL = 2)

      INTEGER  IL
      INTEGER  KLOCE (NDLE_LCL)
      REAL*8   VKE   (NDLE_LCL, LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IE, IES, NO1, NO3
      REAL*8  ML
C----------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
D     CALL ERR_PRE(LM_CMMN_NDLEV .GE. NDLE_LCL)
C-----------------------------------------------------------------

C---     Initialise la matrice élémentaire
      CALL DINIT(NDLE_LCL*LM_CMMN_NDLEV, ZERO, VKE, 1)

C---     Indices des éléments
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)

C---        Matrice masse lumpded
         ML = VDJS(3,IES)
         VKE(1,1) = ML
         VKE(2,2) = ML

C---        Table KLOCE
         KLOCE(1) = KLOCN(3,NO1)
         KLOCE(2) = KLOCN(3,NO3)

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, NDLE_LCL, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      SV2D_CL133_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL133_ASMKT
C
C Description:
C     La fonction SV2D_CL133_ASMKT calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_ASMKT(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_ASMKT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER   IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL133_TYP)
C-----------------------------------------------------------------

      IERR = SV2D_CL133_ASMK(IL,
     &                       KLOCE,
     &                       VKE,
     &                       VCORG,
     &                       KLOCN,
     &                       KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       VPRES,
     &                       VSOLR,
     &                       KCLCND,
     &                       VCLCNV,
     &                       KCLLIM,
     &                       KCLNOD,
     &                       KCLELE,
     &                       VCLDST,
     &                       KDIMP,
     &                       VDIMP,
     &                       KEIMP,
     &                       VDLG,
     &                       HMTX,
     &                       F_ASM)

      SV2D_CL133_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL133_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL133_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>133</b>: <br>
C  Weak Dirichlet condition on the water level.
C  The condition imposes on the limit the water level of another limit.
C  The values are taken in order, node by node.
C  <p>
C  Parameters (a0, a1) can be used to transform the source water level to the final form
C  <pre>h_dst = a0 + a1*h_src</pre>
C  <ul>
C     <li>Kind: Weak Dirichlet</li>
C     <li>Code: 133</li>
C     <li>Values: name a0 a1</li>
C     <li>Units: [-]  m  [-]</li>
C     <li>Example:  125 $__Amont_deversoir__$ 0.0 1.0</li>
C  </ul>
C</comment>

      SV2D_CL133_REQHLP = 'bc_type_133'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL133_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL133_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL133_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl133.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl133.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl133.hlp')

      SV2D_CL133_HLP = ERR_TYP()
      RETURN
      END
