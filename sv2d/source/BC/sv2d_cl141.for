C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Frontière ouverte en débit.
C
C Functions:
C   Public:
C     INTEGER SV2D_CL141_000
C     INTEGER SV2D_CL141_999
C     INTEGER SV2D_CL141_COD
C     INTEGER SV2D_CL141_ASMK
C     INTEGER SV2D_CL141_ASMKT
C     INTEGER SV2D_CL141_ASMKU
C     INTEGER SV2D_CL141_HLP
C   Private:
C     INTEGER SV2D_CL141_INIVTBL
C     SUBROUTINE SV2D_CL141_ASMRE_S
C     SUBROUTINE SV2D_CL141_ASMKE_S
C     CHARACTER*(16) SV2D_CL141_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL141_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl141.fc'

      DATA SV2D_CL141_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL141_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL141_INIVTBL()

      SV2D_CL141_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL141_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL141_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL141_HLP
      EXTERNAL SV2D_CL141_COD
      EXTERNAL SV2D_CL141_ASMKU
      EXTERNAL SV2D_CL141_ASMK
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL141_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL141_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL141_TPN(1:LTPN), SV2D_CL141_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL141_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL141_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL141_TYP,
     &                     SV2D_CL141_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD,  SV2D_CL141_TYP,
     &                    SV2D_CL141_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKU,SV2D_CL141_TYP,
     &                    SV2D_CL141_ASMKU)
!      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMK, SV2D_CL141_TYP,
!     &                    SV2D_CL141_ASMK)
!      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMKT,SV2D_CL141_TYP,
!     &                    SV2D_CL141_ASMK)

      SV2D_CL141_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL141_COD
C
C Description:
C     La fonction SV2D_CL141_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose une limite ouverte en débit.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Est-ce qu'on veut vraiment avoir plus qu'un segment
C************************************************************************
      FUNCTION SV2D_CL141_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER, PARAMETER :: KVAL(1) = (/0/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(2) = (/EG_TPLMT_1SGMT,
     &                                  EG_TPLMT_nSGMT/) ! c.f. note
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL141_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(3,IN) = IBSET(KDIMP(3,IN), EA_TPCL_OUVERT)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL141_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL141_ASMK
C
C Description:
C     La fonction SV2D_CL141_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La matrice est une matrice élémentaire pour le T6L
C     La matrice Ke peut être nulle.
C************************************************************************
      FUNCTION SV2D_CL141_ASMK(IL,
     &                         KLOCE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  IL
      INTEGER  KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmgeom_subt3.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NNEL_LCL = 6

      INTEGER IERR
      INTEGER IEDEB, IEFIN
      INTEGER IE, IES, IEV, ICT
      INTEGER II, IN, ID, IDG
      REAL*8  VDLE (NDLN_LCL, NNEL_LCL)
      REAL*8  VRHS (NDLN_LCL, NNEL_LCL)
C----------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL141_TYP)
D     CALL ERR_PRE(LM_CMMN_NDLN  .EQ. NDLN_LCL)
C-----------------------------------------------------------------

      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Initialise le RHS
      VRHS(:,:) = ZERO

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Element parent et côté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Table KLOCE de l'élément T6L
         KLOCE(:,:) = KLOCN(:,KNGV(:,IEV))

C---        Transfert des DDL
         VDLE(:,:) = VDLG (:,KNGV(:,IEV))

C---        Initialise la matrice élémentaire
         VKE(:,:) = ZERO

C---        Boucle de perturbation sur les DDL
C           ==================================
         DO II=1,LM_CMMN_NNELS
            IN = KNEL3L(II, ICT)
            IDG = (IN-1)*LM_CMMN_NDLN

            DO ID=1,LM_CMMN_NDLN
               IDG = IDG + 1
               IF (KLOCE(ID,IN) .EQ. 0) GOTO 199

C---              Perturbe le DDL
               VRHS(ID,IN) = UN

C---              R(u). i
               CALL SV2D_CL141_ASMRE_S(VKE(1,IDG),
     &                                 VDJV(1,IEV),
     &                                 VDJS(1,IES),
     &                                 VDLE,
     &                                 VRHS,
     &                                 ICT)

C---              Restaure la valeur originale
               VRHS(ID,IN) = ZERO

199            CONTINUE
            ENDDO
         ENDDO

C---       Assemblage de la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF
      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      SV2D_CL141_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL141_ASMKU
C
C Description:
C     La fonction SV2D_CL141_ASMKU calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_ASMKU(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VFE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_ASMKU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KLOCE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8  VFE   (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER, PARAMETER :: NDLN_LCL = 3
      INTEGER, PARAMETER :: NNEL_LCL = 6

      INTEGER   IERR
      INTEGER   IEDEB, IEFIN
      INTEGER   IE, IES, IEV, ICT
      REAL*8    VDLE (NDLN_LCL, NNEL_LCL)
C----------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL141_TYP)
D     CALL ERR_PRE(LM_CMMN_NDLN  .EQ. NDLN_LCL)
D     CALL ERR_PRE(LM_CMMN_NDLEV .EQ. NDLN_LCL*NNEL_LCL)
C-----------------------------------------------------------------

      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Boucle sur les éléments de la limite
C        ====================================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Élément parent et coté
         IEV = KNGS(4,IES)
         ICT = KNGS(5,IES)

C---        Transfert des DDL
         VDLE(:,:) = VDLG(:,KNGV(:,IEV))

C---        Initialise le vecteur élémentaire
         VFE(:,:) = ZERO

C---        [K(u)].{u}
         CALL SV2D_CL141_ASMRE_S(VFE,
     &                           VDJV(1,IEV),
     &                           VDJS(1,IES),
     &                           VDLE,
     &                           VDLE,
     &                           ICT)

C---       Assemblage du vecteur global
         KLOCE(:,:) = KLOCN(:,KNGV(:,IEV))
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

      ENDDO

      SV2D_CL141_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL141_ASMRE_S
C
C Description:
C     La fonction SV2D_CL141_ASMRE_S
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SV2D_CL141_ASMRE_S(VRES,
     &                              VDJV,
     &                              VDJS,
     &                              VDLE,
     &                              VRHS,
     &                              ICOTE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VRES (LM_CMMN_NDLEV)
      REAL*8   VDJV (EG_CMMN_NDJV)
      REAL*8   VDJS (EG_CMMN_NDJS)
      REAL*8   VDLE (LM_CMMN_NDLN, EG_CMMN_NNELV)
      REAL*8   VRHS (LM_CMMN_NDLN, EG_CMMN_NNELV)
      INTEGER  ICOTE

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmgeom_subt3.fi'
      INCLUDE 'sv2d_subt3.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER IKH1, IKH3
      INTEGER NO1, NO2, NO3
      REAL*8  Q1, Q2, Q3
      REAL*8  VNX, VNY, C, M(2)
C-----------------------------------------------------------------------

C---     Connectivités
      NO1 = KNEL3L(1, ICOTE)
      NO2 = KNEL3L(2, ICOTE)
      NO3 = KNEL3L(3, ICOTE)

C---     Indices dans VKE
      IKH1 = KLOCET3(3, 1, ICOTE)
      IKH3 = KLOCET3(6, 2, ICOTE)

C---     Coefficient
      VNX =  VDJS(2)       ! VNX =  VTY
      VNY = -VDJS(1)       ! VNY = -VTX
      C = UN_12*VDJS(3)    ! UN_6 * DJL2

C---     Débits normaux
      Q1 = VRHS(1,NO1)*VNX + VRHS(2,NO1)*VNY
      Q2 = VRHS(1,NO2)*VNX + VRHS(2,NO2)*VNY
      Q3 = VRHS(1,NO3)*VNX + VRHS(2,NO3)*VNY

C---     Assemblage du vecteur global (mixte L2-L3L)
      VRES(IKH1) = VRES(IKH1) + C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
      VRES(IKH3) = VRES(IKH3) + C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL141_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL141_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>141</b>: <br>
C  Open boundary condition for discharge.<br>
C  It amounts to computing the boundary integral arising
C  from the weak form of the continuity equation.
C  <ul>
C     <li>Kind: Open boundary</li>
C     <li>Code: 141</li>
C     <li>Values: </li>
C     <li>Units: </li>
C     <li>Example:  141</li>
C  </ul>
C</comment>

      SV2D_CL141_REQHLP = 'bc_type_141'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL141_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL141_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL141_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl141.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl141.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl141.hlp')

      SV2D_CL141_HLP = ERR_TYP()
      RETURN
      END
