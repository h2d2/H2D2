C************************************************************************
C --- Copyright (c) INRS 2012-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau actuel.
C     Formulation HYDROSIM (vitesse constante).
C
C Interface:
C   H2D2 Module: SV2D
C      H2D2 Class: SV2D_CL126
C         INTEGER SV2D_CL126_000
C         INTEGER SV2D_CL126_999
C         INTEGER SV2D_CL126_INIVTBL
C         INTEGER SV2D_CL126_COD
C         INTEGER SV2D_CL126_QSPEC
C         INTEGER SV2D_CL126_CLC
C         INTEGER SV2D_CL126_ASMFE
C         INTEGER SV2D_CL126_ASMF
C         INTEGER SV2D_CL126_ASMKT
C         CHARACTER*(16) SV2D_CL126_REQHLP
C         INTEGER SV2D_CL126_HLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL126_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl126.fc'

      DATA SV2D_CL126_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL126_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL126_INIVTBL()

      SV2D_CL126_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL126_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL126_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL126_HLP
      EXTERNAL SV2D_CL126_COD
      EXTERNAL SV2D_CL126_CLC
      EXTERNAL SV2D_CL126_ASMF
      EXTERNAL SV2D_CL126_ASMKT
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL126_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL126_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL126_TPN(1:LTPN), SV2D_CL126_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL126_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL126_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_HLP,  SV2D_CL126_TYP,
     &                    SV2D_CL126_HLP)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_COD,  SV2D_CL126_TYP,
     &                    SV2D_CL126_COD)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_CLC,  SV2D_CL126_TYP,
     &                    SV2D_CL126_CLC)
      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMF, SV2D_CL126_TYP,
     &                    SV2D_CL126_ASMF)
!!!      IERR=SV2D_CL_AJTFNC(HVFT, SV2D_CL_FUNC_ASMKT,SV2D_CL126_TYP,
!!!     &                    SV2D_CL126_ASMKT)

      SV2D_CL126_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL126_COD
C
C Description:
C     La fonction SV2D_CL126_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Est-ce qu'on veut vraiment avoir plus qu'un segment
C************************************************************************
      FUNCTION SV2D_CL126_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR
      INTEGER I, IC, IV, IN, INDEB, INFIN
      INTEGEr IORD, NVAL
      INTEGER, PARAMETER :: KVAL(2) = (/1, 2/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(2) = (/EG_TPLMT_1SGMT,
     &                                  EG_TPLMT_nSGMT/) ! c.f. note
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
C-----------------------------------------------------------------------

C---     Les indices
      IC = KCLLIM(2, IL)
      IV = KCLCND(3, IC)

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)
      NVAL = KCLCND(4,IC) - KCLCND(3,IC) + 1
      IF (NVAL .EQ. 2) THEN
         IORD = NINT( VCLCNV(IV+1) )
         IF ( .NOT. ANY((/-1, 0, 1/) .EQ. IORD)) GOTO 9900
         IF (IORD .EQ. 1 .AND.
     &       ANY(KCLNOD(INDEB:INFIN) .LE. 0)) GOTO 9901
      ENDIF

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
D              CALL ERR_ASR(IN .LE. EG_CMMN_NNL)
               KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_CL_DEBIT)
            ENDIF
         ENDDO
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ORDRE_REGRESSION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I6)') 'MSG_OBTIENT', ': ', IORD
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_ATTEND', ': ', '[0, 1]' 
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_LIMITE_COUPEE_PAR_DISTRIBUTION'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_TRAITEMENT_NON_IMPLANTE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_UTILISER_REGRESSION_0'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
      
9999  CONTINUE
      SV2D_CL126_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL126_QSPEC
C
C Description:
C     La fonction privée SV2D_CL126_QSPEC calcule le débit spécifique
C     utilisé dans le calcul les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_QSPEC(IL,
     &                          KNGS,
     &                          VDJS,
     &                          VPRNO,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          VDLG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'splsqr.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl126.fc'

      REAL*8,  PARAMETER :: TGV     = 1.0D+99
      REAL*8,  PARAMETER :: TGV_TST = 0.9D+99
      INTEGER, PARAMETER :: MAX_HTMP  = 64

      INTEGER IERR, I_ERROR
      INTEGER I, IC, IE, IES, IV, IN
      INTEGER IEDEB, IEFIN
      INTEGER INDEB, INFIN
      INTEGER IORD
      INTEGER N
      INTEGER NO1, NO2, NO3
      REAL*8  F1, F2, F3, P1, P2, P3
      REAL*8  Q1, Q2, Q3, QS, QG
      REAL*8  QTOT, QSPEC
      REAL*8  A, B, R
      REAL*8  H1, H2
      REAL*8, TARGET  :: HTMP_LCL(MAX_HTMP)
      REAL*8, POINTER :: HTMP(:)
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
      REAL*8 PRFE, PRFA, H_
      PRFE(H_, I) = H_ - (VPRNO(SV2D_IPRNO_Z,I) + 
     &                    0.9D0*VPRNO(SV2D_IPRNO_ICE_E, I))
      PRFA(H_, I) = MAX(PRFE(H_,I), SV2D_DECOU_HMIN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. 1)
      IV = KCLCND(3,IC)
      QTOT = VCLCNV(IV)
      IORD = -1
      IF (KCLCND(4,IC) .GT. KCLCND(3,IC)) THEN
         IORD = NINT( VCLCNV(IV+1) )
D        CALL ERR_ASR(ANY((/-1, 0, 1/) .EQ. IORD))
      ENDIF

C---     Limites d'itération
      INDEB = KCLLIM(3,IL)
      INFIN = KCLLIM(4,IL)
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Intègre sur la limite
      QS = 0.0D0
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) CYCLE
         IF (EST_PARALLEL(KDIMP(3,NO3))) CYCLE

C---        Valeurs nodales
         P1 = VPRNO(SV2D_IPRNO_H, NO1)            ! Profondeur
         P2 = VPRNO(SV2D_IPRNO_H, NO2)
         P3 = VPRNO(SV2D_IPRNO_H, NO3)

C---        Règle de répartition
         Q1 = P1
         Q2 = P2
         Q3 = P3

C---        Intègre
         QS = QS + UN_2*VDJS(3,IES)*(Q1+Q2+Q2+Q3)
      ENDDO

C---    Sync et contrôle
      CALL MPI_ALLREDUCE(QS, QG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS   = QG
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Les niveaux de la limite
      ! ---  Alloue la table de travail
      N = INFIN-INDEB+1
      IF (N .GT. MAX_HTMP) THEN
         ALLOCATE(HTMP(INDEB:INFIN))
      ELSE
         HTMP(INDEB:INFIN) => HTMP_LCL(1:N)
      ENDIF
      ! ---  Les niveaux locaux
      DO I=INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            HTMP(I) = VDLG(3,IN)
         ELSE
            HTMP(I) = -TGV
         ENDIF
      ENDDO
      ! ---  Réduis le niveau
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, HTMP(INDEB:), N, MP_TYPE_RE8(),
     &                   MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      
      ! ---  Regression du niveau d'eau
      A = 0.0D0
      B = 0.0D0
      SELECT CASE(IORD)
         CASE(-1)    ! Pas de regression
!           PASS
         CASE( 0)    ! Regression constante
            B = SUM(HTMP(:)) / N
         CASE( 1)    ! Regression linéaire
            R = SP_LSQR_LINREG(N, VCLDST(INDEB:), HTMP(INDEB:), A, B)
D        CASE DEFAULT
D           CALL ERR_ASR(.FALSE.)
      END SELECT
      ! ---  Désalloue la table de travail
      IF (N .GT. MAX_HTMP) THEN
         DEALLOCATE(HTMP)
      ENDIF
      
C---     Les niveaux des noeuds de la limite
      DO I = INDEB, INFIN                          ! Flag les noeuds pour
         IN = KCLNOD(I)                            ! détecter les noeuds milieux
         IF (IN .GT. 0) VDIMP(3, IN) = TGV
      ENDDO
      DO I = INDEB, INFIN                          ! détecter les noeuds milieux
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            SELECT CASE (IORD)
               CASE (-1)
                  H1 = VDLG(3,IN)
               CASE (0)
                  H1 = B
               CASE (1)
                  H1 = A*VCLDST(I) + B
            END SELECT
            VDIMP(3,IN) = H1                       ! Squat VDIMP
         ENDIF
      ENDDO

C---     Impose les valeurs des noeuds milieux
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

         NO1 = KNGS(1,IES)
         NO2 = KNGS(2,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) CYCLE
         IF (EST_PARALLEL(KDIMP(3,NO3))) CYCLE
         IF (VDIMP(3,NO2) .LT. TGV_TST) CYCLE         ! Test pour le flag
         
         H2 = 0.5D0 * (VDIMP(3,NO1) + VDIMP(3,NO3))   ! Niveau
         P2 = PRFA(H2, NO2)                           ! Prof absolue
         Q2 = P2
         VDIMP(3,NO2) = Q2*QSPEC
      ENDDO

C---     Impose les valeurs des noeuds sommets
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            H1 = VDIMP(3,IN)           ! Niveau
            P1 = PRFA(H1, IN)          ! Profondeur
            Q1 = P1
            VDIMP(3,IN) = Q1*QSPEC
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_CL126_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL126_QSPEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL126_CLC
C
C Description:
C     La fonction privée SV2D_CL126_CLC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_CLC(IL,
     &                        VCORG,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP,
     &                        VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_CLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
C-----------------------------------------------------------------------

      IERR = SV2D_CL126_QSPEC(IL,
     &                        KNGS,
     &                        VDJS,
     &                        VPRNO,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        VDLG)

      SV2D_CL126_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL126_ASMFE
C
C Description:
C     La fonction SV2D_CL126_ASMFE assemble le vecteur force élémentaire
C     {f}.
C     La fonction ne retourne que les 3 valeurs pour les DDL en h.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_ASMFE(VFE,
     &                          VDJES,
     &                          VPRNO,
     &                          VDIMP,
     &                          VDLES)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VFE  (EG_CMMN_NNELS)
      REAL*8  VDJES(EG_CMMN_NDJS)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNELS)
      REAL*8  VDIMP(LM_CMMN_NDLN,  EG_CMMN_NNELS)
      REAL*8  VDLES(LM_CMMN_NDLN,  EG_CMMN_NNELS)

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl126.fc'

      REAL*8  Q1, Q2, Q3
      REAL*8  C

      INTEGER, PARAMETER :: NO1 = 1
      INTEGER, PARAMETER :: NO2 = 2
      INTEGER, PARAMETER :: NO3 = 3
C-----------------------------------------------------------------------

C---     Coefficient
      C = -UN_12*VDJES(3)     ! UN_6*DJL2

C---     Débits spécifiques
      Q1 = VDIMP(3, NO1)
      Q2 = VDIMP(3, NO2)
      Q3 = VDIMP(3, NO3)

C---     Assemblage du vecteur global (mixte L2-L3L)
      VFE(1) = C*(5.0D0*Q1 + 6.0D0*Q2 +       Q3)
      VFE(2) = 0.0D0
      VFE(3) = C*(      Q1 + 6.0D0*Q2 + 5.0D0*Q3)

      SV2D_CL126_ASMFE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL126_ASMF
C
C Description:
C     La fonction SV2D_CL126_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau actuel.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR
      INTEGER IC, IEDEB, IEFIN
      INTEGER IE, IES

      REAL*8, POINTER :: VDLE (:,:)
      REAL*8, POINTER :: VCLE (:,:)
      REAL*8, POINTER :: VPRN (:,:)
      REAL*8, POINTER :: VFE  (:)
      INTEGER,POINTER :: KNE  (:)
      INTEGER,POINTER :: KLOCE(:)

C---     Tables locales
      INTEGER, PARAMETER :: NDLN_LCL =  3
      INTEGER, PARAMETER :: NNEL_LCL =  3
      INTEGER, PARAMETER :: NPRN_LCL = 18
      REAL*8, TARGET :: VDLE_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET :: VCLE_LCL (NDLN_LCL * NNEL_LCL)
      REAL*8, TARGET :: VPRN_LCL (NPRN_LCL * NNEL_LCL)
      REAL*8, TARGET :: VFE_LCL  (NNEL_LCL)
      INTEGER,TARGET :: KNE_LCL  (NNEL_LCL)
      INTEGER,TARGET :: KLOCE_LCL(NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C---     Reshape the arrays
      VDLE (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VDLE_LCL
      VCLE (1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VCLE_LCL
      VPRN (1:LM_CMMN_NPRNO,1:EG_CMMN_NNELS) => VPRN_LCL
      VFE  (1:EG_CMMN_NNELS) => VFE_LCL
      KNE  (1:EG_CMMN_NNELS) => KNE_LCL
      KLOCE(1:EG_CMMN_NNELS) => KLOCE_LCL

C---     Limites d'itération
      IC = KCLLIM(2,IL)
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Boucle sur la limite
C        ====================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         KNE(:) = KNGS(1:3, IES)

C---        Transfert des valeurs nodales
         VDLE(:,:) = VDLG (:, KNE(:))
         VCLE(:,:) = VDIMP(:, KNE(:))
         VPRN(:,:) = VPRNO(:, KNE(:))

C---        {f}
         IERR = SV2D_CL126_ASMFE(VFE,
     &                           VDJS(1,IES),
     &                           VPRN,
     &                           VCLE,
     &                           VDLE)

C---        Assemblage du vecteur global
         KLOCE(:) = KLOCN(3, KNE(:))
         IERR = SP_ELEM_ASMFE(EG_CMMN_NNELS, KLOCE, VFE, VFG)
      ENDDO

      SV2D_CL126_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SV2D_CL126_ASMKT
C
C Description:
C     La fonction SV2D_CL126_ASMKT calcule le matrice tangente
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_ASMKT(IL,
     &                          KLOCE,
     &                          VKE,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_ASMKT
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER, PARAMETER :: NNELS_LCL =  3    ! Dim ici à cause des param
      INTEGER, PARAMETER :: NDLES_LCL =  3
      INTEGER, PARAMETER :: NDLN_LCL  =  3
      INTEGER, PARAMETER :: NPRN_LCL  = 18

      INTEGER  IL
      INTEGER  KLOCE (0:NDLES_LCL)
      REAL*8   VKE   (NDLES_LCL, NDLES_LCL)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl126.fc'

      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IC, IEDEB, IEFIN, INDEB, INFIN
      INTEGER IE, IES
      INTEGER I, IN, ID
      INTEGER HVFT, HPRNE

      REAL*8, POINTER :: VDLES(:,:)
      REAL*8, POINTER :: VCLES(:,:)
      REAL*8, POINTER :: VPRNS(:,:)
      INTEGER,POINTER :: KNES(:)

C---     Tables locales
      REAL*8, TARGET :: VDLES_LCL(NDLN_LCL * NNELS_LCL)
      REAL*8, TARGET :: VCLES_LCL(NDLN_LCL * NNELS_LCL)
      REAL*8, TARGET :: VPRNS_LCL(NPRN_LCL * NNELS_LCL)
      INTEGER,TARGET :: KNES_LCL (NNELS_LCL)
      REAL*8, ALLOCATABLE :: VFE(:,:)
      REAL*8, ALLOCATABLE :: VFP(:,:)

!!!      BYTE, POINTER :: VPRGL_B(:)
!!!      BYTE, POINTER :: VPRNS_B(:)
!!!      BYTE, POINTER :: VDLES_B(:)
!!!      BYTE, POINTER :: IERR_B (:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

      GOTO 9999
      
C---     Fonctions de calcul
      HVFT = 0    ! VTABLE par défaut
      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNES, HPRNE)

C---     Reshape the arrays
      VDLES(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VDLES_LCL
      VCLES(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VCLES_LCL
      VPRNS(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELS) => VPRNS_LCL
      KNES (1:EG_CMMN_NNELS) => KNES_LCL

!!!C---     Byte pointeurs pour le calcul des propriétés perturbées
!!!      VDLES_B => SO_ALLC_CST2B(VDLES(:,1))
!!!      VPRGL_B => SO_ALLC_CST2B(VPRGL(:))
!!!      VPRNS_B => SO_ALLC_CST2B(VPRNS(:,1))
!!!      IERR_B  => SO_ALLC_CST2B(IERR)

C---     Limites d'itération
      IC = KCLLIM(2,IL)
      INDEB = KCLLIM(3,IL)
      INFIN = KCLLIM(4,IL)
      IEDEB = KCLLIM(5,IL)
      IEFIN = KCLLIM(6,IL)

C---     Alloue les tables       
      ALLOCATE(VFE(1:EG_CMMN_NNELS, IEDEB:IEFIN))
      ALLOCATE(VFP(1:EG_CMMN_NNELS, IEDEB:IEFIN))

C---     Initialise
      VKE(:,:) = 0.0D0
      VFE(:,:) = 0.0D0
      VFP(:,:) = 0.0D0
      
C---     Calcul de la partie non perturbée
C        =================================
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

         ! ---  Transfert des valeurs nodales de surface
         KNES(:) = KNGS(1:3, IES)
         VDLES(:,:) = VDLG (:, KNES(:))
         VCLES(:,:) = VDIMP(:, KNES(:))
         VPRNS(:,:) = VPRNO(:, KNES(:))

         ! ---  {R(u)} = {f}
         IERR = SV2D_CL126_ASMFE(VFE (1,IE),
     &                           VDJS(1,IES),
     &                           VPRNS,
     &                           VCLES,
     &                           VDLES)
      ENDDO
      
C---     Coefficient de perturbation (moyen)
      VDL_ORI = SUM( VDLG(3, KCLNOD(INDEB:INFIN)) )
      VDL_ORI = VDL_ORI / (INFIN-INDEB+1)
      VDL_DEL = SV2D_PNUMR_DELPRT * VDL_ORI
     &        + SIGN(SV2D_PNUMR_DELMIN, VDL_ORI)
      VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
      VDL_INV = 0.0D0

C---     Perturbe les noeuds un à un 
C        ===========================
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (KLOCN(3, IN) .EQ. 0) CYCLE
         
         ! ---  Perturbe le DDL
         VDL_ORI = VDLG(3, IN)
         VDLG(3, IN) = VDLG(3, IN) + VDL_DEL

         ! ---  Calcule les propriétés perturbées
         IERR = SV2D_CL126_QSPEC(IL,
     &                           KNGS,
     &                           VDJS,
     &                           VPRNO,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           VDLG)

         ! ---  Perturbation due au noeud dans VFP
D        VFP(:,:) = 0.0D0
         DO IE = IEDEB, IEFIN
            IES = KCLELE(IE)

            ! ---  Transfert des valeurs nodales de surface
            KNES(:) = KNGS(1:3, IES)
            VDLES(:,:) = VDLG (:, KNES(:))
            VCLES(:,:) = VDIMP(:, KNES(:))
            VPRNS(:,:) = VPRNO(:, KNES(:))

!!!            ! ---  Calcule les propriétés perturbées
!!!            IF (SV2D_PNUMR_PRTPRNO) THEN     ! Pas utilisées 
!!!               IERR = SO_FUNC_CALL4(HPRNE,
!!!     &                              VPRGL_B,
!!!     &                              VPRNS_B,
!!!     &                              VDLES_B,
!!!     &                              IERR_B)
!!!            ENDIF

            ! ---  {R(u+du_id)}
            IERR = SV2D_CL126_ASMFE(VFP (1,IE),
     &                              VDJS(1,IES),
     &                              VPRNS,
     &                              VCLES,
     &                              VDLES)
         ENDDO

         ! ---  Restaure la valeur originale du DDL
         VDLG(3, IN) = VDL_ORI
         
         ! ---  Finalise la perturbation: - ({R(u+du_id)} - {R(u)})/du
         VFP(:,:) = - (VFP(:,:) - VFE(:,:)) * VDL_INV
      
         ! ---  Assemble dans la matrice
         KLOCE(0) = KLOCN(3,IN)
         DO IE = IEDEB, IEFIN
            IES = KCLELE(IE)

            ! ---  Table KLOCE de l'élément de surface
            KNES(:)  = KNGS(1:3, IES)
            KLOCE(1:) = KLOCN(3, KNES(:))

            ! ---   Assemble le vecteur
            IERR = F_ASM(HMTX, -EG_CMMN_NNELS, KLOCE, VFP(:,IE))
            IF (ERR_BAD()) CALL ERR_RESET()
         ENDDO
      ENDDO

      ! ---  Restaure les propriétés nodales
      IF (SV2D_PNUMR_PRTPRNO) THEN
!!!         IERR = SO_FUNC_CALL4(HPRNE,
!!!     &                        VPRGL_B,
!!!     &                        VPRNS_B,
!!!     &                        VDLES_B,
!!!     &                        IERR_B)
!!!         VPRNO(:, KNES(:)) = VPRNS(:,:)
         IERR = SV2D_CL126_QSPEC(IL,
     &                           KNGS,
     &                           VDJS,
     &                           VPRNO,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           VCLDST,
     &                           KDIMP,
     &                           VDIMP,
     &                           VDLG)
      ENDIF

      ! ---  Récupère la mémoire
      DEALLOCATE(VFE)
      DEALLOCATE(VFP)
      
9999  CONTINUE      
      SV2D_CL126_ASMKT = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: SV2D_CL126_ASMKT
!!!C
!!!C Description:
!!!C     La fonction SV2D_CL126_ASMKT calcule le matrice tangente
!!!C     élémentaire. L'assemblage de la matrice globale est fait
!!!C     par call-back à la fonction paramètre F_ASM.
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION SV2D_CL126_ASMKT(IL,
!!!     &                          KLOCE,
!!!     &                          VKE,
!!!     &                          VCORG,
!!!     &                          KLOCN,
!!!     &                          KNGV,
!!!     &                          KNGS,
!!!     &                          VDJV,
!!!     &                          VDJS,
!!!     &                          VPRGL,
!!!     &                          VPRNO,
!!!     &                          VPREV,
!!!     &                          VPRES,
!!!     &                          VSOLR,
!!!     &                          KCLCND,
!!!     &                          VCLCNV,
!!!     &                          KCLLIM,
!!!     &                          KCLNOD,
!!!     &                          KCLELE,
!!!     &                          VCLDST,
!!!     &                          KDIMP,
!!!     &                          VDIMP,
!!!     &                          KEIMP,
!!!     &                          VDLG,
!!!     &                          HMTX,
!!!     &                          F_ASM)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_ASMKT
!!!CDEC$ ENDIF
!!!
!!!      USE SO_ALLC_M
!!!      USE SO_FUNC_M
!!!      IMPLICIT NONE
!!!
!!!      INCLUDE 'eacnst.fi'
!!!      INCLUDE 'eacdcl.fi'
!!!      INCLUDE 'eacmmn.fc'
!!!      INCLUDE 'egcmmn.fc'
!!!
!!!      INTEGER, PARAMETER :: NNELS_LCL =  3    ! Dim ici à cause des param
!!!      INTEGER, PARAMETER :: NDLES_LCL =  3
!!!      INTEGER, PARAMETER :: NDLN_LCL  =  3
!!!      INTEGER, PARAMETER :: NPRN_LCL  = 18
!!!
!!!      INTEGER  IL
!!!      INTEGER  KLOCE (NDLES_LCL)
!!!      REAL*8   VKE   (NDLES_LCL, NDLES_LCL)
!!!      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
!!!      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
!!!      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
!!!      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
!!!      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
!!!      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
!!!      REAL*8   VPRGL (LM_CMMN_NPRGL)
!!!      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
!!!      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
!!!      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
!!!      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
!!!      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
!!!      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
!!!      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
!!!      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
!!!      INTEGER  KCLELE(    EG_CMMN_NCLELE)
!!!      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
!!!      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
!!!      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
!!!      INTEGER  KEIMP (EG_CMMN_NELS)
!!!      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
!!!      INTEGER  HMTX
!!!      INTEGER  F_ASM
!!!      EXTERNAL F_ASM
!!!
!!!      INCLUDE 'sv2d_cl126.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'log.fi'
!!!      INCLUDE 'sofunc.fi'
!!!      INCLUDE 'sv2d_cbs.fc'
!!!      INCLUDE 'sv2d_cl126.fc'
!!!
!!!      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
!!!      INTEGER IERR
!!!      INTEGER IC, IEDEB, IEFIN
!!!      INTEGER IE, IES
!!!      INTEGER IN, II, ID
!!!      INTEGER HVFT, HPRNE
!!!
!!!      REAL*8, POINTER :: VDLES(:,:)
!!!      REAL*8, POINTER :: VCLES(:,:)
!!!      REAL*8, POINTER :: VPRNS(:,:)
!!!      REAL*8, POINTER :: VFE (:)
!!!      REAL*8, POINTER :: VFEP(:)
!!!      INTEGER,POINTER :: KNES(:)
!!!
!!!C---     Tables locales
!!!      REAL*8, TARGET :: VDLES_LCL(NDLN_LCL * NNELS_LCL)
!!!      REAL*8, TARGET :: VCLES_LCL(NDLN_LCL * NNELS_LCL)
!!!      REAL*8, TARGET :: VPRNS_LCL(NPRN_LCL * NNELS_LCL)
!!!      REAL*8, TARGET :: VFE_LCL  (NDLES_LCL)
!!!      REAL*8, TARGET :: VFEP_LCL (NDLES_LCL)
!!!      INTEGER,TARGET :: KNES_LCL (NNELS_LCL)
!!!
!!!      BYTE, POINTER :: VPRGL_B(:)
!!!      BYTE, POINTER :: VPRNS_B(:)
!!!      BYTE, POINTER :: VDLES_B(:)
!!!      BYTE, POINTER :: IERR_B (:)
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL126_TYP)
!!!D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
!!!C-----------------------------------------------------------------------
!!!
!!!C---     Fonctions de calcul
!!!      HVFT = 0    ! VTABLE par défaut
!!!      IERR = SV2D_CBS_REQFNC(HVFT, SV2D_VT_CLCPRNES, HPRNE)
!!!
!!!C---     Reshape the arrays
!!!      VDLES(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VDLES_LCL
!!!      VCLES(1:LM_CMMN_NDLN, 1:EG_CMMN_NNELS) => VCLES_LCL
!!!      VPRNS(1:LM_CMMN_NPRNO,1:EG_CMMN_NNELS) => VPRNS_LCL
!!!      VFE  (1:EG_CMMN_NNELS) => VFE_LCL
!!!      VFEP (1:EG_CMMN_NNELS) => VFEP_LCL
!!!      KNES (1:EG_CMMN_NNELS) => KNES_LCL
!!!
!!!C---     Byte pointeurs pour le calcul des propriétés perturbées
!!!      VDLES_B => SO_ALLC_CST2B(VDLES(:,1))
!!!      VPRGL_B => SO_ALLC_CST2B(VPRGL(:))
!!!      VPRNS_B => SO_ALLC_CST2B(VPRNS(:,1))
!!!      IERR_B  => SO_ALLC_CST2B(IERR)
!!!
!!!C---     Limites d'itération
!!!      IC = KCLLIM(2,IL)
!!!D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 1)
!!!      IEDEB = KCLLIM(5,IL)
!!!      IEFIN = KCLLIM(6,IL)
!!!
!!!C---     Boucle sur la limite
!!!C        ====================
!!!      DO IE = IEDEB, IEFIN
!!!         IES = KCLELE(IE)
!!!
!!!C---        Connectivités
!!!         KNES(:) = KNGS(1:3, IES)
!!!
!!!C---        Table KLOCE de l'élément de surface
!!!         KLOCE(:) = KLOCN(3, KNES(:))
!!!
!!!C---        Transfert des valeurs nodales de surface
!!!         VDLES(:,:) = VDLG (:, KNES(:))
!!!         VCLES(:,:) = VDIMP(:, KNES(:))
!!!         VPRNS(:,:) = VPRNO(:, KNES(:))
!!!
!!!C---        {R(u)} = {f}
!!!         IERR = SV2D_CL126_ASMFE(VFE,
!!!     &                           VDJS(1,IES),
!!!     &                           VPRNS,
!!!     &                           VCLES,
!!!     &                           VDLES)
!!!
!!!C---        Initialise la matrice élémentaire
!!!         VKE(:,:) = ZERO
!!!
!!!C---        BOUCLE DE PERTURBATION SUR LES DDL
!!!C           ==================================
!!!         DO ID=1,EG_CMMN_NNELS
!!!            IF (KLOCE(ID) .EQ. 0) CYCLE
!!!
!!!C---           Perturbe le DDL
!!!            VDL_ORI = VDLES(3, ID)
!!!            VDL_DEL = SV2D_PNUMR_DELPRT * VDL_ORI
!!!     &              + SIGN(SV2D_PNUMR_DELMIN, VDL_ORI)
!!!            VDL_INV = SV2D_PNUMR_OMEGAKT / VDL_DEL
!!!            VDLES(3,ID) = VDLES(3,ID) + VDL_DEL
!!!
!!!C---           Calcule les propriétés nodales perturbées
!!!            IF (SV2D_PNUMR_PRTPRNO) THEN
!!!               IERR = SO_FUNC_CALL4(HPRNE,
!!!     &                              VPRGL_B,
!!!     &                              VPRNS_B,
!!!     &                              VDLES_B,
!!!     &                              IERR_B)
!!!!!!               VPRNO(:, KNES(:)) = VPRNS(:,:)
!!!!!!               IERR = SV2D_CL126_QSPEC(IL,    ! CL perturbé dans VDIMP
!!!!!!     &                                 KNGS,
!!!!!!     &                                 VDJS,
!!!!!!     &                                 VPRNO,
!!!!!!     &                                 KCLCND,
!!!!!!     &                                 VCLCNV,
!!!!!!     &                                 KCLLIM,
!!!!!!     &                                 KCLNOD,
!!!!!!     &                                 KCLELE,
!!!!!!     &                                 VCLDST,
!!!!!!     &                                 KDIMP,
!!!!!!     &                                 VDIMP)
!!!!!!               VCLES(:,:) = VDIMP(:,KNES(:))
!!!            ENDIF
!!!
!!!C---           {R(u+du_id)}
!!!            IERR = SV2D_CL126_ASMFE(VFEP,
!!!     &                              VDJS(1,IES),
!!!     &                              VPRNS,
!!!     &                              VCLES,
!!!     &                              VDLES)
!!!
!!!C---           Restaure la valeur originale
!!!            VDLES(3,ID) = VDL_ORI
!!!
!!!C---           - ({R(u+du_id)} - {R(u)})/du
!!!            VKE(:,ID) = - (VFEP(:) - VFE(:)) * VDL_INV  !! * 1.0D-3
!!!         ENDDO
!!!
!!!C---       Assemble la matrice
!!!         IERR = F_ASM(HMTX, EG_CMMN_NNELS, KLOCE, VKE)
!!!         IF (ERR_BAD()) CALL ERR_RESET()
!!!
!!!      ENDDO
!!!
!!!C---     Restaure les propriétés nodales perturbées
!!!!!!      IF (SV2D_PNUMR_PRTPRNO) THEN
!!!!!!         IERR = SO_FUNC_CALL4(HPRNE,
!!!!!!     &                        VPRGL_B,
!!!!!!     &                        VPRNS_B,
!!!!!!     &                        VDLES_B,
!!!!!!     &                        IERR_B)
!!!!!!         VPRNO(:, KNES(:)) = VPRNS(:,:)
!!!!!!         IERR = SV2D_CL126_QSPEC(IL,
!!!!!!     &                           KNGS,
!!!!!!     &                           VDJS,
!!!!!!     &                           VPRNO,
!!!!!!     &                           KCLCND,
!!!!!!     &                           VCLCNV,
!!!!!!     &                           KCLLIM,
!!!!!!     &                           KCLNOD,
!!!!!!     &                           KCLELE,
!!!!!!     &                           VCLDST,
!!!!!!     &                           KDIMP,
!!!!!!     &                           VDIMP)
!!!!!!      ENDIF
!!!
!!!      SV2D_CL126_ASMKT = ERR_TYP()
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL126_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL126_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>126</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, taking into account the current water level h.
C  The distribution is based on a constant velocity on the whole section.
C  <p>
C  If <code>regLvl</code> is specified, a regression will be applied on the water level to help in cases where
C  the water level is very irregular which might slow convergence.
C  Valid values are 0 for a constant regression (i.e. a mean water level) and 1 for a linear regression.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 126</li>
C     <li>Values: Q [regLvl]</li>
C     <li>Units: m^3/s [-]</li>
C     <li>Example:  126   -500  [-1]</li>
C  </ul>
C</comment>

      SV2D_CL126_REQHLP = 'bc_type_126'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL126_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL126_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL126_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl126.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl126.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl126.hlp')

      SV2D_CL126_HLP = ERR_TYP()
      RETURN
      END
