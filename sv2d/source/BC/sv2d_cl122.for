C************************************************************************
C --- Copyright (c) INRS 2009-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Sollicitation de débit via la forme faible de la continuité.
C     Débit global redistribué en fonction du niveau d'eau fixe spécifié.
C     Formulation HYDROSIM
C
C Functions:
C   Public:
C     INTEGER SV2D_CL122_000
C     INTEGER SV2D_CL122_999
C     INTEGER SV2D_CL122_COD
C     INTEGER SV2D_CL122_PRC
C     INTEGER SV2D_CL122_ASMF
C     INTEGER SV2D_CL122_HLP
C   Private:
C     INTEGER SV2D_CL122_INIVTBL
C     CHARACTER*(16) SV2D_CL122_REQHLP
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SV2D_CL122_DATA_000

      IMPLICIT NONE

      INCLUDE 'sv2d_cl122.fc'

      DATA SV2D_CL122_TPC /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL122_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = SV2D_CL122_INIVTBL()

      SV2D_CL122_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SV2D_CL122_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la table virtuelle
C
C Description:
C     La fonction statique privée SV2D_CL122_INIVTBL initialise et remplis
C     la table virtuelle (VTABLE) qui contient les fonctions de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_INIVTBL()

      IMPLICIT NONE

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR, IRET
      INTEGER HVFT
      INTEGER LTPN
      EXTERNAL SV2D_CL122_HLP
      EXTERNAL SV2D_CL122_COD
      EXTERNAL SV2D_CL122_PRC
      EXTERNAL SV2D_CL122_ASMF
C-----------------------------------------------------------------------
D     CALL ERR_ASR(SV2D_CL122_TYP .LE. SV2D_CL_TYP_MAX)
C-----------------------------------------------------------------------

!!!      LTPN = SP_STRN_LEN(SV2D_CL122_TPN)
!!!      IRET = C_ST_CRC32(SV2D_CL122_TPN(1:LTPN), SV2D_CL122_TPC)
!!!      IERR = SV2D_CL_INIVTBL2(HVFT, SV2D_CL122_TPN)
      IERR = SV2D_CL_INIVTBL(HVFT, SV2D_CL122_TYP)

C---     Remplis la table virtuelle
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_HLP, SV2D_CL122_TYP,
     &                     SV2D_CL122_HLP)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_COD,  SV2D_CL122_TYP,
     &                    SV2D_CL122_COD)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_PRC,  SV2D_CL122_TYP,
     &                    SV2D_CL122_PRC)
      IERR=SV2D_CL_AJTFNC (HVFT, SV2D_CL_FUNC_ASMF, SV2D_CL122_TYP,
     &                    SV2D_CL122_ASMF)
      SV2D_CL122_INIVTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL122_COD
C
C Description:
C     La fonction SV2D_CL122_COD assigne le code de condition limite.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C     <p>
C     On contrôle la cohérence de la CL puis on impose les codes. Un débit
C     est compté positif s'il est dans le sens de la normale orientée vers
C     l'extérieur dans le sens de parcours trigonométrique.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C
C Notes:
C     Est-ce qu'on veut vraiment avoir plus qu'un segment
C************************************************************************
      FUNCTION SV2D_CL122_COD(IL,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        KDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_COD
CDEC$ ENDIF

      USE SV2D_CL_CHKDIM_M
      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sv2d_cl.fi'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR
      INTEGER I, IC, IN, INDEB, INFIN
      INTEGER, PARAMETER :: KVAL(1) = (/2/)
      INTEGER, PARAMETER :: KRNG(2) = (/-1, -1/)
      INTEGER, PARAMETER :: KKND(2) = (/EG_TPLMT_1SGMT,
     &                                  EG_TPLMT_nSGMT/) ! c.f. note
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL122_TYP)
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = SV2D_CL_CHKDIM(IL, KCLCND, KCLLIM, KVAL, KRNG, KKND)

C---     Les indices
      IC = KCLLIM(2, IL)

C---     Assigne les codes
      IF (ERR_GOOD()) THEN
         INDEB = KCLLIM(3, IL)
         INFIN = KCLLIM(4, IL)
         DO I = INDEB, INFIN
            IN = KCLNOD(I)
            IF (IN .GT. 0) THEN
               KDIMP(3,IN) = IBSET(KDIMP(3,IN), SV2D_CL_DEBIT_H)
            ENDIF
         ENDDO
      ENDIF

      SV2D_CL122_COD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL122_PRC
C
C Description:
C     La fonction privée SV2D_CL122_PRC calcule les valeurs de condition
C     limite en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_PRC(IL,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        KCLCND,
     &                        VCLCNV,
     &                        KCLLIM,
     &                        KCLNOD,
     &                        KCLELE,
     &                        VCLDST,
     &                        KDIMP,
     &                        VDIMP,
     &                        KEIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR, I_ERROR
      INTEGER IC, IV
      INTEGER I, IN, IE, IES
      INTEGER INDEB, INFIN
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  P1, P3
      REAL*8  H
      REAL*8  QTOT, QSPEC
      REAL*8  QS, QG
C-----------------------------------------------------------------------
      INTEGER ID
      LOGICAL EST_PARALLEL
      EST_PARALLEL(ID) = BTEST(ID, EA_TPCL_PARALLEL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL122_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 2)
      IV = KCLCND(3,IC)
      QTOT = VCLCNV(IV)
      H    = VCLCNV(IV+1)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

C---     Intègre la surface mouillée
      QS = 0.0D0
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)
         IF (EST_PARALLEL(KDIMP(3,NO1))) GOTO 199
         IF (EST_PARALLEL(KDIMP(3,NO3))) GOTO 199

C---        Valeurs nodales
         P1 = MAX(H-VPRNO(SV2D_IPRNO_Z, NO1), ZERO)     ! Profondeur
         P3 = MAX(H-VPRNO(SV2D_IPRNO_Z, NO3), ZERO)

C---        Intègre
         QS = QS + VDJS(3,IES)*(P1+P3)
199      CONTINUE
      ENDDO

C---     Sync et contrôle
      CALL MPI_ALLREDUCE(QS, QG, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_ASGERR(I_ERROR)
      QS   = QG
      IF (QS .LT. PETIT) GOTO 9900

C---     Débit spécifique
      QSPEC = QTOT / QS

C---     Impose les valeurs
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            IF ((H-VPRNO(SV2D_IPRNO_Z,IN)) .GT. ZERO) THEN
               VDIMP(3,IN) = QSPEC
            ELSE
               VDIMP(3,IN) = ZERO
            ENDIF
         ENDIF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SURFACE_DE_REPARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I3)') 'MSG_TYPE_CL=', SV2D_CL122_TYP
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SV2D_CL122_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SV2D_CL122_ASMF
C
C Description:
C     La fonction SV2D_CL122_ASMF assigne le valeur de condition limite
C     en calcul.
C     Sur la limite <code>IL</code>, elle impose le débit global qui sera
C     redistribué en fonction du niveau d'eau fixe spécifié.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C     KDIMP          Codes des DDL imposés
C
C Sortie:
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_ASMF(IL,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         VCLDST,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER IL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      REAL*8  VSOLC (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sv2d_cbs.fc'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR
      INTEGER IC, IV
      INTEGER IE, IES
      INTEGER IEDEB, IEFIN
      INTEGER NO1, NO3
      REAL*8  H
      REAL*8  C
      REAL*8  P1, P3, Q1, Q3, C1, C3
      REAL*8  VFE(2)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. SV2D_CL122_TYP)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 2)
      IV = KCLCND(3,IC)
      H  = VCLCNV(IV+1)

      IEDEB = KCLLIM(5, IL)
      IEFIN = KCLLIM(6, IL)

C---     Assemble
      DO IE = IEDEB, IEFIN
         IES = KCLELE(IE)

C---        Connectivités
         NO1 = KNGS(1,IES)
         NO3 = KNGS(3,IES)

C---        Coefficient
         C = -UN_3*VDJS(3,IES)

C---        Valeurs nodales
         P1 = MAX(H-VPRNO(SV2D_IPRNO_Z,NO1), ZERO) ! Profondeur
         P3 = MAX(H-VPRNO(SV2D_IPRNO_Z,NO3), ZERO)
         Q1 = VDIMP(3,NO1)                         ! Débit spec.
         Q3 = VDIMP(3,NO3)
         C1 = C*Q1*P1
         C3 = C*Q3*P3

C---        Assemblage du vecteur global
         VFE = (/ (C1+C1 + C3), (C3+C3 + C1) /)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO1), VFE(1), VFG)
         IERR = SP_ELEM_ASMFE(1, KLOCN(3,NO3), VFE(2), VFG)
      ENDDO

      SV2D_CL122_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL122_REQHLP défini l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_REQHLP()

      IMPLICIT NONE

      CHARACTER*(16) SV2D_CL122_REQHLP
C------------------------------------------------------------------------

C<comment>
C  Boundary condition of type <b>122</b>: <br>
C  It makes use of the natural boundary condition arising from the weak form of the continuity equation.
C  <p>
C  The total discharge Q is distributed on the wetted part of the boundary, assuming a constant water level h.
C  The distribution is based on a constant velocity on the whole section.
C  <p>
C  Inflow is negative, outflow is positive.
C  <ul>
C     <li>Kind: Discharge sollicitation</li>
C     <li>Code: 122</li>
C     <li>Values: Q  h</li>
C     <li>Units: m^3/s  m</li>
C     <li>Example:  122   -500 10</li>
C  </ul>
C</comment>

      SV2D_CL122_REQHLP = 'bc_type_122'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SV2D_CL122_HLP imprime l'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SV2D_CL122_HLP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SV2D_CL122_HLP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sv2d_cl122.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sv2d_cl122.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sv2d_cl122.hlp')

      SV2D_CL122_HLP = ERR_TYP()
      RETURN
      END
