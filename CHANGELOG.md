[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 1904p1

Version **19.04p1** adds a function in the remeshing part.. It also correct some bugs.

#### H2D2:
    - Error management:
        Fix: improve 
    - Boundary condition:
      - Fix: Limit name is still case insensitive, bat all other parts are case sensitive
      - Fix: Reading condition on multiple lines was broken.
    
#### Remesh:
    - Add: Remesh operation to cure "bridges", elements joining two boundaries

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 1904

Version **19.04** is mostly the port of the code to gitlab, with the development of automated compilation/installation scripts. It also correct some bugs.

- Migrate all the code to giltab.com
- Add compilation/installation scripts
- Port all python code to Python3
- Bug correction

#### Libraries versions:
   - boost 1.69
   - geos 3.7.1
   - scotch 6.0.4
   - MUMPS-5.1.2
   - SuperLU 5.2.1

#### H2D2:
   - Command interface:
      - Fix: Improve the treatment of string variables.
      - Fix: Uninitialized variable

   - Algorithms:
      - Fix: Virtual calls are now active, giving access to specialized methods.

   - Solvers:
      - Fix: Virtual calls are now active, giving access to specialized methods.

   - Probe on lines both scalar and vector:
      - Fix: Under MPI, local number of points can be empty

   - post_dlib:
      - Fix: Wrong index for value retrieval

#### sv2d: 
   - sv2d_pst_components:
      - Fix: Adapt to changes in dry/wet parameters

   - Boundary condition:
      - Fix: Tighten the controls
      - Fix: Clarify the error message

   - Condition 125:
   - Condition 126:
      - Fix: Wrong indices in allocate statement.

   - Condition 138:
      - Fix: Separate control specification for source and target boundary

#### Remesh:
   - Add: Add minimal and maximal refinement levels change

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 18.10p1
La version **18.10p1** corrige deux bugs:

#### H2D2:
   - bug dans l'allocation de mémoire du calcul de résidu

#### sv2d:
   - les contrôles de validité de la condition 138 n'étaient pas valides, rejetant du coup toutes les conditions de ce type.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 18.10
La version **18.10** améliore la robustesse de la résolution et la cohérence interne des algorithmes.
Diverses optimisations ont été faites ainsi que des correction de bugs.

#### Version de librairies:
   - boost 1.63
   - geos 3.6.2
   - scotch 6.0.4
   - MUMPS-5.1.1 / MUMPS-5.1.2
   - SuperLU 5.2.1

#### H2D2:
   - Général:
      - Sépare H2D2/tools du projet H2D2 pour en faire le projet indépendant H2D2-tools
      - Lecture de dates UTC: Ajoute la détection des décalages horaires

   - Aide:
      - Affiche les modules, classes et commandes.

   - Propriétés globales:
      - Lecture d'un fichier: Inverse les lignes/colonnes pour que les PRGL représentent N valeurs en 1 point par analogie aux propriétés nodales

   - Propriétés nodales:
      - Ajoute l'initialisation à partir d'un répertoire et d'un patron de recherche pour les fichiers
      
   - Maillages:
      - Ajoute la détection des éléments ayant deux côtés ou plus sur la peau. Ce type d'éléments lorsque utilisé dans une condition limite peut impacter sur la convergence.

   - Algorithmes:
      - Moule l'homotopie sur Euler. Le pas de chargement est pris comme un pas de temps.
      - La cohérence entre les algos a été améliorée ce qui permet de mieux gérer le comportement en cas de non convergence. Les algos temporels retourne toujours le résultat du dernier pas convergé.
      - Ajout de count-down timer. Lorsque le temps est épuisé, la résolution est stoppée.
      - Les algos affichent plus d'information sur leur état
   
   - Contrôleur d'incréments:
      - Limite le pas à dtmin
      - Branche la composition des contrôleurs d'incrément
      - Ajoute le contrôleur d'incrément sur CFL

   - Contrôleur de convergence:
      - Utilise la norme du résidu pour le critère de convergence
      - Révise la logique de fin de pas: le critère d'arrêt a maintenant préséance

   - Solveur Pardiso:
      - Modifie les paramètres pour augmenter la reproductibilité des calculs

#### sv2d:
   - Améliore l'affichage de la fonction de vent (nom + relatif/absolu)
   - Aide sur les PRGL, le PRNO et les CL

   - Élément sv2d_y4:
      - Améliore la robustesse du couvrant-découvrant
         - Supprime la pénalisation
         - Ajoute un Peclet pour le découvrement
         - Utilise une interpolation plus lisse entre l'état couvert et l'état découvert
      - Les intégrales de contour sont spécialisés pour chaque degré de liberté. Le coefficient devient un flag pour chaque DDL dans l'ordre qx, qy, h, i.e 111 pour les 3, 001 uniquement pour h

   - Conditions limites:
      - Resserre les contrôles de cohérence
      - CL 125 et 126
         - Ajoute l'option de lissage du niveau d'eau utilisé dans la répartition du début, soit par régression constante ou linéaire. Le lissage améliore la robustesse au détriment possible de la variabilité.
      - CL 113 
         - Ajoute la pondération comme paramètre

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.10p4

La version **15.10p4** corrige deux bugs dans le calcul distribué.

### H2D2:
   - Bugs:
      - glob_limiter: le test de convergence était fait sur le nombre de variables limités
        localement, alors qu'il doit être fait sur le nombre global. Ceci provoquait une erreur MPI.
      - euler: une variable n'était pas initialisée en mode release, provoquant une convergence
        au mieux ralentie, au pire provoquant une non-convergence. 

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.10p3

La version **15.10p3** ajoute une fonctionnalité.

#### H2D2:
   - Ajout:
      - L'algorithme "solution_reader" permet la relecture des ddl
        et ouvre la possibilité d'appels à des post-traitements.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.10p2

La version **15.10p2** est une correction de bugs.

#### H2D2:
   - Améliorations:
      - Adaptation de maillage: Lorsque possible, préserve la numérotation originale du maillage.

#### SV2D:   
   - Bugs:
      - Élément Y4: Corrige la pénalisation dans le couvrant/découvrant
   - Améliorations:
      - Ajout de fonctions de vent.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.10p1

La version **15.10p1** est une correction de bugs.

#### H2D2:
   - Améliorations:
      - MUMPS: Ajout du nombre d'itération de raffinement comme paramètre 

#### SV2D:   
   - Améliorations:
      - Condition limite de type 142 (seuil): la loi de seuil est exprimée en m3/s.
        Le niveau sur le seuil est le niveau moyen de la limite. Le débit calculé à
        partir de la loi de seuil est réparti en tenant compte du Manning.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.10

La version **15.10** est une correction de bugs.

#### H2D2:
   - Bugs:
      - Révise la détection des erreurs à la lecture de fichiers de limites
      - En calcul distribué, le nombre de noeuds limités par glob_limiter n'était pas rapporté correctement
      - ParMetis:  Révise la renumérotation suite au passage à ParMetis 4
      - Calcul d'erreur par recovery: gère les erreurs de débordement de tableaux
      - Calcul d'erreur: Révise l'appel du T3 depuis le T6L
      - Post-traitement: Les tables n'étaient pas initialisées correctement pour les opérations de réduction.
      - Calcul des erreurs par 'recovery': Augmente la taille du buffer des voisins et gère les erreurs de débordement.
   - Améliorations:
      - Révise le calcul des valeurs propres des ellipses d'erreur
      - Transforme le format d'impression des réels de 1PE12.5 à 1PE14.6E3
      - Uniformise l'affichage de noms de fichier
      - Révise la détection des erreurs à la lecture de fichiers de limites
      - Synchronise entre les process le nombre de noeuds limités par les limiteurs
      - Imprime le critère de convergence dans 'newton' et 'picard'
      - Ajoute des properties dans 'prno' et 'vnod'
      - Complète les paramètres imprimés dans 'algo_node_for'
      - Complète des messages d'erreur dans le lecture des limites
      - Ajoute les niveaux de log manquants dans 'log'
   - Ajouts:
      - Ajoute le trigger sur des réels
      - Retire les commandes obsolètes
      - Désactive la commande checkpoint_restart
      - Permet la réduction du hessien et de la norme du hessien
      - Active les opérations de réductions pour la norme du hessien

#### SV2D:   
   - Bugs:
      - Dans le calcul de Kt, la valeur min de perturbation était trop faible. Elle est maintenant fixée à 1.0D-07 et elle est rendue explicitement accessible
        comme propriété globale dans Y4.
      - Le calcul de l'intégrale de surface du terme de dissipation avait deux erreurs:
         1) C'est la viscosité totale qui doit être prise en compte, et non seulement la viscosité physique, pour vraiment éliminer
            la condition limite naturelle crée par l'intégration par partie.
         2) l'intégrale de surface n'était pas compatible avec celle de volume.
   - Améliorations:
   - Ajouts:

#### CD2D   
   - Bugs:
   - Améliorations:
      - Révise le calcul interne
   - Ajouts:
      - Ajoute les CL par défaut
      - Amortissement lorsque la concentration dépasse [-0, 1], d'abord faible puis à pleine puissance à partir de [-0.1, 1.1]
      - Ajoute les propriétés globales pour la stabilisation par amortissement
      - Amortissement de Weak Dirichlet pour les noeuds découverts
      - Pour la condition de Dirichlet et Weak Dirichlet, impose la valeur de découvrement sur les noeuds découverts
   
#### Outils:
   - Bugs:
      - DADataAnalizer: Bug dans la détection des fichiers à précision limitée
      - MeshAdaptation: Correction d'un bug sous Linux liés à un item de menu.
   - Améliorations:
      - MeshAdaptation: Ajoute la génération des limites
   - Ajouts:


[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 15.04

La version **15.04** est principalement une préparation du code en vue d'une modification majeur de l'assemblage. 
Les librairies externes ont été mises à jour. Sous Windows, la librairie MPI est changée pour MS-MPI car MPICH2
n'est plus supporté.

#### H2D2:
   - Bugs:
      - Divers bugs et limitations mineures
   - Améliorations:
      - Supprime les post-traitements obsolètes
      - Remplace la commande "limiter" par "glob_limiter" qui prend les mêmes paramètres
      - Critère de convergence:
         . Base le critère de divergence sur les pentes moyennes calculées sur 2, 3, 4 et 5 itérations
         . Introduit un nombre d'itérations min obligatoires
      - Contrôleurs d'incréments:
         . Révise la logique pour les pas non convergés et les pas tronqués
   - Ajouts:
      - Ajoute du calcul du Hessien par 'recovery'
      - Projection de solution initiale dans le chemin d'une séquence de solutions

#### SV2D:
   - Bugs:
   - Améliorations:
      - Les éléments disponibles se différencie par le traitement du couvrant-découvrant et du vent:
         - YS: sans hystérèse et vent absolu (1 fonction)
         - Y2: hystérèse et vent absolu (1 fonction)
         - Y3: hystérèse et vent absolu (multiples fonctions)
         - Y4: sans hystérèse et vent relatif (multiples fonctions), pénalisation de H positif (prototype)
      - Transforme la viscosité turbulente basée sur la taille de maille en modèle de Smagorinsky
   - Ajouts:
      - Post-traitement de calcul des composantes (termes) de sv2d

#### Outils:
   - Bugs:
   - Amélioration:
      - Les outils ConvergenceTracer et ProbeTracer supportent le drag & drop, et des fichiers sur la ligne de commande
      - Révision de ImageProcessor pour la publication vers Geoserver et WMS
   - Ajouts:
      - Ajout de l'outil "Data Analyzer", qui permet d'analyser des données et de les visualiser (Prototype)

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 14.0402

La version 14.04.p2 est une correction de bugs.

#### H2D2:
   - Les modules pouvaient ne pas être chargés correctement.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 14.0402

La version 14.04.p1 est une correction de bugs.

#### SV2D:
   - Le terme de contour résultant de l'intégration par partie du terme de Darcy était mal discrétisé ce qui
   pouvait provoquer des résultats erronés dans la partie découverte.

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 14.04

La version **14.04** est une évolution importante de H2D2.
Elle ajoute de nouvelles fonctionnalités, des améliorations ainsi que correctifs. Elle
ajoute les conditions limites astronomiques et harmoniques, l'interpolation des conditions limites.

#### H2D2:
   - Bugs:
        - Écriture de fichiers ASCII:
            - Gère les erreurs d'écriture de l'entête
        - Contrôleur d'incrément:
            - Simple: Révise l'algorithmique de calcul de l'incrément
            - Prédictif: Corrige le calcul du pas calculé
        - Système de log: En cas d'erreur d'ouverture du fichier, reconnecte à l'unité 6 et retourne un message d'erreur
        - Interpréteur de commande:
            - Fait la différence entre le numéro de ligne de lecture est celui du fichier source. Les 2 sont
              différents pour les fonctions.
            - Corrige un bug dans la détection des handle retournés par les fonctions.
   - Améliorations:
        - Sondes 0d avec points: Les points peuvent être spécifiés avec un handle sur une liste.
        - Lecture de données ASCII: Ajoute la détection de "buffer overflow"
        - Modules: Transforme les classes les plus utilisées en modules F90 avec allocation dynamique
          des objets, ce qui permet de se libérer des limites sur le nombre d'objets.
        - Classe de liste: Ajoute la méthode extend
   - Ajouts:
        - Port i8: H2D2 est prêt pour tourner avec des INTEGER en configuration INTEGER*8.
        - Module os: fonctions reliées au système d'exploitation
        - Ajoute l'interpolation linéaire de certaines conditions le long d'une limite
   - Développement en cours:
         - Contrôleur d'incrément basé sur le CFL

#### SV2D
   - Bugs:
        - Le Manning de glace n'était pas pris en considération!!
   - Ajouts:
        - Ajoute l'élément y3 comme héritier de y2 avec fonction de calcul du cw
        - Ajoute l'élément y4 comme héritier de y3 avec assemblage via le résidu
        - Ajoute le calcul en post-traitement du CFL et autre paramètres numériques comme le Peclet
        - Ajoute les conditions limites harmoniques:
            - sans interpolation (203) et
            - avec interpolation ()
      - Ajoute les conditions limites astronomiques:
            - sans interpolation (203) et
            - avec interpolation ()

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 13.04
La version **13.04** est une évolution importante de H2D2.
Elle ajoute de nouvelles fonctionnalités, des améliorations ainsi que correctifs. Elle
ajoute une nouvelle méthode de résolution, l'homotopie ou avancée par pas de chargement,
Les critères de convergence pour décider quand les itérations convergent ou divergent.
Elle implante également l'adaptation de maillage.

#### H2D2:
   - Bugs:
        - Contrôle la monotonie (croissante) des temps des fichiers lus
   - Améliorations:
        - Révise la logique de détection de succès et de rejet d'itération
        - Les sondes sur des points sont maintenant fonctionnelles sur des maillages distribués
        - Optimise la lecture des fichiers ASCII en lisant par blocs
   - Ajouts:
        - Adaptation de maillage:
            - Ajoute le calcul du hessien par deux méthodes (green, dxdx), pour les éléments T3 et T6L
            - Ajoute les post-traitements pour trois types de calcul: classes post_hessian, post_ellipse et post_hessian_norm
            - Implante l'adaptation de maillage: class remesh
            - Ajoute un outil externe (MeshAdaptation) pour l'adaptation interactive de maillage
        - Homotopie:
            - Lecture de PRGL à partir d'un fichier ce qui en permet l'interpolation
            - Définition d'un chemin de chargement pour les propriétés globales: classes homotopy_prgl et homotopy_compose
            - Ajoute l'algorithme d'homotopie: classe homotopy
        - Algorithmes:
            - Ajoute la globalisation par mise à l'échelle: classe glob_scaler
            - Ajoute les critères de convergence: classe convergence_criterion
        - Rends le timer disponible comme commande
   - Développements en cours:
        - Checkpoint-restart pour pouvoir redémarrer un calcul.
        - Support des integer*8, nécessaires dans les indices de grosses matrices (>2GB).

#### SV2D:
   - Bugs:
      Corrige un mauvais indice (sans incidence sur le calcul) dans l'accès des coefficient de Coriolis
   - Améliorations:
   - Ajouts:
      - Ajoute les lois de calcul de CW, coefficient de frottement du au vent
      - Ajoute l'élément SV2D_Conservatif_CDY3_NN qui donne accès au lois de calcul du vent par une PRGL
      - Ajoute la condition limite de type 126, débit en format Hydrosim mais avec niveau variable

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 20110831

La version **20110831** est une évolution importante de H2D2.
Elle ajoute de nouvelles fonctionnalités, des améliorations ainsi que correctifs. Elle
valide principalement le couplage entre les modèles ainsi que le traitement des bancs
découverts dans CD2D. Différentes fonctionnalités ont un statut expérimental.

#### H2D2:
   - Bugs:
       - Les vno avec valeurs initiales ne sont chargés que sur changement de renumérotation
       - La gestion des erreurs sous OMP n'étais pas thread-safe:
           - Révision de la gestion des erreurs sous OMP.
           - Réduction des erreurs en fin de boucle.
   - Améliorations:
       - Migre le calcul des prgl dans post-lecture afin qu'elles soient affichées correctement
       - Dans la commande split, calcule et assigne le nombre d'éléments total
       - Transforme les pst_ddl en pst_sim
       - Améliore l'impression, rend Picard compatible avec Newton
       - Rajoute l'affichage des item dans la résolution stationnaire
   - Ajouts:
       - Commande post_compose: Ajoute la requête d'un item via l'indice
       - Algorithme de Newton: Détecte les problèmes linéaires et limite le nombre d'itérations:
         copie la logique de Picard
       - Algorithme de Picard: L'algorithme Picard qui résout directement K.u=F n'est pas
         compatible avec les schémas d'intégration en temps qui résolvent pour un dU.
         L'algorithme est réécris sous la forme K dU = F- Ku.
       - Pour afficher tous les chiffres, change le format d'écriture du temps pour F25.6
       - Supporte les redimensionnements de matrice en cours de résolution
       - Ajoute l'arborescence des contrôleurs d'incrément pour des algorithmes adaptatifs:
           - contrôleur trivial
           - contrôleur simple
           - contrôleur prédictif
           - contrôleur PID
       - Ajoute les algorithmes implicites d'intégration dans le temps de la famille ESDIRK
           - ESDIRK 12
           - ESDIRK 23
           - ESDIRK 34

#### CD2D:
   - Bugs:
        - Corrige le calcul de Peclet et Lapidus
        - Uniformise les calculs entre K et KU. La principale modification est dans le calcul
          des dérivées dans les termes de contour
        - Reformatte l'écriture des CLIM car on ne peut pas avoir de saut de ligne en format interne
   - Amélioration:
        - Précise les messages de changement de type de CL
   - Ajouts:
        - Post-traitement sur les diffusivités
        - Implantation d'un modèle de découvrement
            - Limite la profondeur lue à HMIN
            - Remanie les PRGL pour permettre divers types de CL pour la partie découverte
            - Facteur multiplicatif de convection constant
            - PROFMIN est remplacé par une PRGL
        - Ajout des propriétés globales pour contrôler le calcul de l'advection et le calcul de div(q)

#### CD2D_TMP
   - Amélioration: Borne l'humidité relative

#### SV2D
   - Bugs:
        - Le test sur la présence de glace était inversé
        - Les fonctions de pondération VN1 et VN2 entre le couvert et le découvert n'allaient
          jamais jusqu'au découvert complet mais s'arrêtaient à  1/2
   - Améliorations:
        - Dans les CL avec un débit, fait l'intégration sur les L2 pour tenir compte
          de la variation du Manning sur l'élément.
   - Ajouts:
        - Post-traitment sv2d_pst_cd2d qui fait le lien pour cd2d
        - Condition limite de type 143, condition limite de ponceau sous contrôle aval (MTQ) (Prototype)

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 20110331
La version **20110331** est une évolution importante de H2D2.
Elle ajoute de nouvelles fonctionnalités, des améliorations ainsi que correctifs.
Différentes fonctionnalités ont un statut expérimental.

#### Ajouts, nouvelles fonctionnalités
   - Minidump en cas d'erreur logiciel (Google breakpad crash reporter). Le dump est contrôlé par
     la variable d'environnement H2D2_DUMP
   - Estimation de l'erreur d'approximation, métrique riemanienne sous forme d'ellipse d'erreur.
   - Adaptation de maillage (Statut expérimental)
   - Algorithme d'homotopie simple (Statut expérimental)
   - Globalisation par relaxation adaptative (adaptative_relaxation)
   - Globalisation par relaxation constante  (constant_relaxation)
   - Globalisation de Bank & Rose (rose_bank)
   - Interface de commande:
      - Structure de contrôle while-endwhile
      - Module de commandes sys et ses composantes sys.math, sys.date, sys.mpi, sys.omp
      - Module de commandes algo regroupant tous les algorithmes de résolution et les noeuds
      - Module de commandes prec regroupant les préconditonnements
      - Module de commandes glob regroupant les algorithmes de globalisation
   - SV2D: Nouveaux types de conditions limites (Statut expérimental)
      - Type 101:   Condition de Dirichlet sur le débit spécifique qx
      - Type 102:   Condition de Dirichlet sur le débit spécifique qy
      - Type 103:   Condition de Dirichlet sur le niveau d'eau h
      - Type 104:   Condition de Dirichlet sur tous les ddl
      - Type 111:   Condition de Cauchy (weak Dirichlet) sur le débit spécifique qx
      - Type 112:   Condition de Cauchy (weak Dirichlet) sur le débit spécifique qy
      - Type 113:   Condition de Cauchy (weak Dirichlet) sur le niveau d'eau h
      - Type 121:   Sollicitation de débit via la forme faible de la continuité.
      - Type 122:   Sollicitation de débit via la forme faible de la continuité.
                    Débit global redistribué en fonction du niveau d'eau fixe spécifié.
                    Formulation hydrosim
      - Type 123:   Sollicitation de débit via la forme faible de la continuité.
                    Débit global redistribué en fonction du niveau d'eau fixe spécifié.
                    Formulation Manning constant
      - Type 124:   Sollicitation de débit via la forme faible de la continuité.
                    Débit global redistribué en fonction du niveau d'eau fixe spécifié.
                    Formulation Manning variable
      - Type 125:   Sollicitation de débit via la forme faible de la continuité.
                    Débit global redistribué en fonction du niveau d'eau actuel.
      - Type 133:   Impose les valeurs de h d'une autre limite. Les valeurs sont prises
                    dans l'ordre, noeud par noeud.
      - Type 138:   Impose le débit global d'une autre limite qui est redistribué
                    en fonction du niveau d'eau actuel.
      - Type 141:   Frontière ouverte en débit.
      - Type 142:   Impose une loi de seuil

#### Améliorations
   - Optimisation du code
   - Nouvelles versions des librairies externes:
      - mpich2:     version 1.3.2p1
      - deino_mpi:  version 2.0.1
      - openmpi:    version 1.4.3
      - mumps:      version 4.9.2
      - scotch:     version 5.1.11
      - boost:      version 1.46.0
      - geosL       version 3.2.2
      - SuperLU     version 4.0
   - Interface de commande:
      - Renforce le contrôle de syntaxe préliminaire à l'exécution des instructions
   - Les critères d'arrêt et l'algorithme de globalisation sont des paramètres des algorithmes de résolution.
     Ils sont migrés des méthodes SOLVE vers les méthodes INI des algorithmes.
   - Ajoute le limiteur à la globalisation
   - Spécialise les timers des fichiers pour discriminer les temps de lecture/écriture et ascii/binaire.
   - prgl: Les propriétés globales peuvent être lues d'un fichier et donc variables dans le temps.
   - scotch: Active le partitionnement distribué de ptscotch
   - node-bi:
      - Implante la bissection du pas de temps avec ré-augmentation du pas
   - euler:
      - Ajoute le paramètre de type de continuation en cas de non convergence d'un pas de temps
      - Le calcul de F dans le second membre ne fais pas la différence entre t et t+dt
      - Implante le calcul complet de F avec la partie à t+dt
      - Implante la bissection du pas de temps avec ré-augmentation du pas
   - rk4l:
      - Corrige l'appel à la résolution par matrice masse lumpée.
   - newton_damped:
      - Utilisation des critères de convergence pour les sous-itérations
      - Tiens compte du résultat de la globalisation dans les tests de convergence
   - newton:
      - Utilisation des critères de convergence pour les sous-itérations
      - Tiens compte du résultat de la globalisation dans les tests de convergence
   - picard:
      - Tiens compte du résultat de la globalisation dans les tests de convergence
   - damper:
      - Modifie les valeurs par défaut
   - SV2D:
      - Retravaille le calcul du Peclet qui montre une très forte discontinuité à vitesses quasi nulles
      - Force la vent à 0 en présence de glace
      - Prof trig. doit être strictement supérieur à prof min.
   - SED2D:
      - Prise en compte de la pente longitudinale
      - Ajoute un Lapidus sur la viscosité numérique en deltaZ
      - Ajoute la viscosité numérique en FAI
      - Ajoute l'amortissement en Z et en FAI
      - Intègre par partie l'opérateur de divergence
      - Révise les conditions limites de type OUVERT
      - Transforme l'élément SED2D en une géométrie T6L, et une approximation T3 sur les noeuds sommets.
      - Force vdlg linéaire sur les noeuds milieux
      - Force ZPRE linéaire sur les noeuds milieux
      - Utilise la zone de log h2d2.element.sed2d
   - ProbeTracer:
      - Corrige la sélection avec CTRL+A
      - Ajoute les accélérateurs
      - Ajoute le texte et la couleur dans le display list
      - Ajout de la boite de dialogue pour les listes d'affichage
      - Pop-up de contrôle de l'affichage
      - Ajustement automatique à la lecture des données
      - Ajoute des entrées de menu Resetxx
      - Les boite de dialogues retournes des expressions qui sont évaluées, plutôt que des valeurs

#### Bug
   - Correction de nombreux bugs, imprécisions, messages d'erreur, etc...
   - Contrôle mémoire (valgrind)
   - Écriture des fichiers de partitionnement: avec 1 processus, les nombre de noeuds de chaque processus
     n'étaient pas transférés
   - pardiso: En cas d'erreur, désallocation possible de tables non allouées
   - Fichier binaire de propriétés nodales: Les nombres de lignes et de colonnes sont inversées
   - La lecture des valeurs entières ne prenait pas en compte le signe
   - SV2D:
      - La profondeur seuil doit être strictement supérieure à la profondeur min.
      - Prend en compte la porosité qui n'était pas calculée de la même façon que pour les autres modèles.
        La porosité, par définition, représente la proportion d'eau dans la partie découverte. Dans Hydrosim
        elle représentait faussement la proportion non aqueuse, au travers d'une opération (1-poro). La porosité
        joue sur la matrice masse de la continuité. Une porosité autre que 1 ralentie la convergence.
      - Mauvais indice pour accéder à la porosité


[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 20100803
La version **20100803** corrige certains problèmes et ajoute de nouvelles fonctionnalités.

#### Nouvelles fonctionnalités
   - Limiteurs: En plus de limite sur l'incrément, le limiteur prend maintenant
     les valeurs min et max à ne pas dépasser pour chaque degré de liberté.
   - Outils ConvergenceCompare, ConvergenceTracer et ProbeTracer: Les boites
      de dialogue retournent des expressions qui sont évaluées, plutôt que des valeurs.

#### Bugs et améliorations
   - CD2D: Prototype du modèle de température basé sur les mesures de la piscine
   - SV2D: Calcul des termes de Coriolis: l'opérateur lumped est trop diffusif
     et ne donne pas les même résultats qu'Hydrosim. Implante l'opérateur complet.
   - Supporte les fichiers de conditions/limites avec 0 limites/conditions
   - Ajoute la méthode solve aux noeuds d'algo qui peuvent maintenant être directement
     exécutés.
   - Ajoute de nombreuses impressions des handles dans le fichier de log
   - Révise la gestion des objects dans les séquences
   - GMRes: Ressèrre la gestion des erreur

[//]: # (----------------------------------------------------------------------)
[//]: # (----------------------------------------------------------------------)
## 20100623
La version **20100623** corrige certains problèmes et ajoute de nouvelles fonctionnalités.

#### Nouvelles fonctionnalités
   - Ajout d'un module "sys" dans l'interface utilisateur, module qui
     donne accès à des fonction mathématiques (sous-module "math"),
     omp (sous-module "omp") et mpiu (sous-module "mpi").
     Status: experimental.
   - Ajout d'une sonde 0D sur des points. La source peut être n'importe
     quel vno et non seulement les ddl. Status: experimental.
   - Ajout de sondes 1D soit sur les scalaires soit sur des vecteurs. La
     valeur scalaire sondée est intégrée le long de la droite. La valeur
     vectorielle est projetée sur la normale puis intégrée. La source peut
     être n'importe quel vno. Status: experimental.

#### Bugs et améliorations
   - Supprime les TAB des chaînes lues
   - Ajuste le format d'écriture pour OMP
   - Force d'avoir le même nombre de conditions que de limites
   - Imprime le nombre de conditions et le nombre de limites
   - Interface de commande: Les séquences ressemblant à des nombres
      (p.ex. slvr2d+) étaient encodées comme des nombres.
   - Fichier de vno binaires: L'écriture ne repositionnait
      pas correctement le fichier après l'écriture de l'entête
   - Fichier de vno binaires: Révise la detection d'erreur et les
      messages remontés.
   - Fichier de valeur nodales: Remplace des assertions par des
      tests au run-time
   - Log: Ajoute l'impression de RANK et de HOSTNAME en niveau de log > 4
   - MUMPS: Révise l'affichage des messages d'erreur
   - MUMPS: Permet de piloter l'impression et le dump via des
     variables d'environnement
   - MUMPS: La méthode finalize de mumps était appelée 2 fois.
   - Sequences: Les triggers étaient incrémentés 2 fois.
   - Sequences: Ne permet pas les handles de post-traitement nul
   - Séquences: Si on a uniquement du post-traitement, les données n'étaient pas chargées.
   - CD2D: Le calcul distribué n'était pas fonctionnel.
   - SED2D: post_Z n'avait pas suivi les modifications aux post-traitements

#### Nouvelles libraires, compilateurs
   - Port GCC-4.4
   - SuperLU Version 4.0
