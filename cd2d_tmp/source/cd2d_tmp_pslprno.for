C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PSLPRNO
C
C Description:
C     Traitement post-lecture des propriétés nodales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PSLPRNO (VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PSLPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INTEGER IN
      REAL*8  V
C-----------------------------------------------------------------------

C---     Borne la fonction d'extinction
      DO IN=1,EG_CMMN_NNL
         V = VPRNO(15, IN)
         V = MIN(V, UN)
         V = MAX(V, ZERO)
         VPRNO(15, IN) = V
      ENDDO

C---     Borne l'humidité relative
      DO IN=1,EG_CMMN_NNL
         V = VPRNO(12, IN)
         V = MIN(V, 100.0D0)  ! en %
         V = MAX(V, PETIT)
         VPRNO(12, IN) = V
      ENDDO

C---     APPELLE LE PARENT
      CALL CD2D_BSE_PSLPRNO (VPRNO,
     &                       IERR)

      RETURN
      END

