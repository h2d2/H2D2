C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_HLPPRGL
C
C Description:
C     Aide sur les propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_HLPPRGL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_HLPPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER I
C-----------------------------------------------------------------------

C---     APPEL LE PARENT
      CALL CD2D_BSE_HLPPRGL()

C---     IMPRESSION DE L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_TEMPERATURE_LUES (SI):')
      CALL LOG_INCIND()

C---     IMPRESSION DE L'INFO
      I = CD2D_BSE_NPRGLL
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_RHO_EAU')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CP_EAU')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_RHO_GLACE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CP_GLACE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CHALEUR_FUSION_GLACE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_PRESSION_ATMOSPHERIQUE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_MODELE_EVAPORATION')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_COEFF_BOWEN')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_TEMPERATURE_FOND')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_COEF_LINEARISATION_A')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_COEF_LINEARISATION_B')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_RADIATION_SOLAIRE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_OL_ATMOSPHERE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_OL_SURFACE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_EVAPORATION')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_CONVECTION')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_PRECIPITATION')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_GLACE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_LIT')

      CALL LOG_DECIND()

      RETURN
      END

