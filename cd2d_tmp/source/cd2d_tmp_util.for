C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire :  CD2D_TMP_XTRPRN
C
C Description:
C     Extrait de VPRNO les paramètres météorologiques.
C
C Entrée:
C     VPRN
C
C Sortie:
C     HSOLE, SF, TURB, TAIR, EMSVA, FCTW, RH, PRCP, VKFND, EXTNC, VKGLC
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_TMP_XTRPRN(VPRN,
     &                           HSOLE, SF, TURB, TAIR, EMSVA, FCTW,
     &                           RH, PRCP, VKFND, EXTNC, VKGLC)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8 VPRN(LM_CMMN_NPRNO)
      REAL*8 HSOLE
      REAL*8 SF
      REAL*8 TURB
      REAL*8 TAIR
      REAL*8 EMSVA
      REAL*8 FCTW
      REAL*8 RH
      REAL*8 PRCP
      REAL*8 VKFND
      REAL*8 EXTNC
      REAL*8 VKGLC
C----------------------------------------------------------------

      HSOLE = VPRN( 6)    ! FLUX DE RADIATION SOLAIRE EFFECTIVE
      SF    = VPRN( 7)    ! FRACTION DU COUVERT VEGETAL
      TURB  = VPRN( 8)    ! COEFFICIENT DE TURBIDITÉ DE LA COLONNE D'EAU
      TAIR  = VPRN( 9)    ! TEMPERATURE DE L'AIR
      EMSVA = VPRN(10)    ! EMISSIVITE DE L'ATMOSPHERE
      FCTW  = VPRN(11)    ! FONCTION DU VENT
      RH    = VPRN(12)    ! HUMIDITE RELATIVE
      PRCP  = VPRN(13)    ! INTENSITE DE PRECIPITATION DE NEIGE OU PLUIE
      VKGLC = VPRN(14)    ! CONDUCTIVITE DE LA GLACE
      EXTNC = VPRN(15)    ! FONCTION D'EXTINCTION
      VKFND = VPRN(16)    ! CONDUCTIVITE DU FOND

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_TMP_CLCFLX
C
C Description:
C     Evaluation des coefficients transitoires A(i) et B(i) qui entrent
C     dans l'expression du flux total S = A(i)*T + B(i). Pour obtenir les
C     vrais flux, il faut diviser par rho*cp.
C
C Entrée:
C     TEAU     ! Température de l'eau
C     VPRNO    ! Propriétés nodales
C
C Sortie: AT(8), BT(8)
C     nT(1) = Hs: Solar short wave radiation
C     nT(2) = Ha: Longwave radiation
C     nT(3) = Hbr: Body reemitted longwave radiation
C     nT(4) = He: Evaporation
C     nT(5) = Hc: Convection
C     nT(6) = Hi: Ice
C     nT(7) = Hbd: Échange avec le fond
C     nT(8) = Hp: Precipitation
C
C Notes:
C     Bowen est calculé avec un deltaT de 1 pour pouvoir expliciter
C     Teau et Tair dans les facteur A et B.
C************************************************************************
      SUBROUTINE CD2D_TMP_CLCFLX(VPRNO, AT, BT)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRNO(LM_CMMN_NPRNO)
      REAL*8 AT(8)
      REAL*8 BT(8)

      INCLUDE 'cd2d_bse.fc'
      INCLUDE 'cd2d_tmp.fc'

      INTEGER IN
      INTEGER IPH
      PARAMETER (IPH = 3)

      REAL*8 ALFA
      REAL*8 RCP
      REAL*8 HSOLE, SF, TURB, TAIR, EMSVA, FCTW
      REAL*8 RH, PRCP, VKFND, EXTNC, VKGLC
      REAL*8 EVAP, AEVAP, BEVAP
      REAL*8 TDEW
      REAL*8 BOWN, TA4

      REAL*8 EMSVE
      REAL*8 STFBLZ
      REAL*8 TABS
      PARAMETER(EMSVE  = 0.97D0    )   ! emissivité de l'eau;
      PARAMETER(STFBLZ = 5.67051D-8)   ! constante de Stefan-Boltzman;
      PARAMETER(TABS   = 273.15D0  )   ! facteur de conversion T(C) en T(K).

      REAL*8 BOWEN
      REAL*8 ES_EA
      REAL*8 T_ROSEE
C-----------------------------------------------------------------------

C---     RHO * CP
      RCP = CD2D_TMP_RHOE * CD2D_TMP_CP

C---     Extraction des paramètres météorologiques
      CALL CD2D_TMP_XTRPRN(VPRNO,
     &                     HSOLE, SF, TURB, TAIR, EMSVA, FCTW,
     &                     RH, PRCP, VKFND, EXTNC, VKGLC)
      TDEW = T_ROSEE(TAIR, RH)
      EVAP = FCTW * ES_EA(TAIR, RH)
      AEVAP=                        (1.0D0-CD2D_TMP_CEVAPO)*FCTW
      BEVAP= CD2D_TMP_CEVAPO*EVAP - (1.0D0-CD2D_TMP_CEVAPO)*FCTW*TDEW
      BOWN = FCTW * BOWEN(TAIR+1.0D0, TAIR, CD2D_TMP_PATM, RH) ! cf note.
      SF  = (UN - SF) * (UN - TURB)
      TA4 = (TABS+TAIR)**4

C---     Modèle de découvrement via l'extinction
      ALFA = (VPRNO(IPH)       - CD2D_DECOU_HMIN) /
     &       (CD2D_DECOU_HTRIG - CD2D_DECOU_HMIN)
      ALFA = MIN(UN, MAX(ZERO, ALFA))
      EXTNC = ALFA*EXTNC

C---     CALCUL DES COEFFICIENTS AT ET BT (AT*T + BT)
      AT(1) = - CD2D_TMP_CMULT_RAD*( 0.0D0 )
      AT(2) = - CD2D_TMP_CMULT_LWA*( 0.0D0 )
      AT(3) = - CD2D_TMP_CMULT_LWS*( STFBLZ*EMSVE*CD2D_TMP_A4*EXTNC )
      AT(4) = - CD2D_TMP_CMULT_EVA*( AEVAP*EXTNC )
      AT(5) = - CD2D_TMP_CMULT_CNV*( CD2D_TMP_CBOWEN*BOWN*EXTNC )
      AT(6) = - CD2D_TMP_CMULT_GLC*( VKGLC )
      AT(7) = - CD2D_TMP_CMULT_LIT*( VKFND )

      BT(1) = + CD2D_TMP_CMULT_RAD*( HSOLE*SF*EXTNC )
      BT(2) = + CD2D_TMP_CMULT_LWA*( STFBLZ*SQRT(EMSVA)*TA4*EXTNC )
      BT(3) = - CD2D_TMP_CMULT_LWS*( STFBLZ*EMSVE*CD2D_TMP_B4*EXTNC )
      BT(4) = - CD2D_TMP_CMULT_EVA*( BEVAP*EXTNC )
      BT(5) = + CD2D_TMP_CMULT_CNV*( CD2D_TMP_CBOWEN*BOWN*TAIR*EXTNC )
      BT(6) = + CD2D_TMP_CMULT_GLC*( 0.0D0 )
      BT(7) = + CD2D_TMP_CMULT_LIT*( VKFND*CD2D_TMP_TFOND )

C---     TRAITEMENT DE LA PRECIPITATION
      IF (TAIR .LT. ZERO) THEN      ! FLUX DE PRECIPITATION DE NEIGE
         AT(8) = - CD2D_TMP_CMULT_PRC*( CD2D_TMP_CP*PRCP*EXTNC )
         BT(8) = + CD2D_TMP_CMULT_PRC*( CD2D_TMP_CPG*TAIR
     &                                - CD2D_TMP_CLG)*PRCP*EXTNC
      ELSE                          ! FLUX DE PRECIPITATION DE PLUIE
         AT(8) = - CD2D_TMP_CMULT_PRC*( RCP*PRCP*EXTNC )
         BT(8) = + CD2D_TMP_CMULT_PRC*( RCP*PRCP*TAIR*EXTNC )
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire :  T_ROSEE
C
C Description:
C     La fonction T_ROSEE calcule la température du point de rosée
C     (dew point). Elle est basée sur la formule de Magnus-Tetens.
C
C Entrée:
C     TAIR, RH
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION T_ROSEE (TAIR, RH)

      IMPLICIT NONE

      REAL*8 T_ROSEE
      REAL*8 TAIR
      REAL*8 RH

      REAL*8 G

      REAL*8 A, B
      PARAMETER(A = 17.26939D0)
      PARAMETER(B = 237.2900D0)
C-----------------------------------------------------------------------
      G = (A*TAIR)/(B+TAIR) + LOG(RH*0.01D0)
      T_ROSEE = (B*G) / (A-G)
      RETURN
      END

C************************************************************************
C Sommaire :  ES
C
C Description:
C     La fonction ES calcule la pression de vapeur à saturation
C     à la température T. Elle est basée sur la formule de Magnus-Tetens:
C        pw = c * exp (aT / (b+T))
C
C Entrée:
C     T
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ES(T)

      IMPLICIT NONE

      REAL*8 ES
      REAL*8 T

      REAL*8 A, B, C
      PARAMETER(A = 17.26939D0)
      PARAMETER(B = 237.2900D0)
      PARAMETER(C = 610.7800D0)
C-----------------------------------------------------------------------

      ES = C * EXP((A*T)/(B+T))
      RETURN
      END

C************************************************************************
C Sommaire :  EA
C
C Description:
C     La fonction EA calcule la pression de vapeur à RH.
C
C Entrée:
C     TAIR     Température de l'air
C     RH       Humidité relative
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION EA(TAIR, RH)

      IMPLICIT NONE

      REAL*8 EA
      REAL*8 TAIR
      REAL*8 RH

      REAL*8 ES
      REAL*8 T_ROSEE
C-----------------------------------------------------------------------

      EA = ES( T_ROSEE(TAIR, RH) )
      RETURN
      END

C************************************************************************
C Sommaire :  ES_EA
C
C Description:
C     La fonction ES_EA calcule la différence entre les pressions de
C     vapeur à saturation et celle à RH.
C
C Entrée:
C     TAIR     Température de l'air
C     RH       Humidité relative
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION ES_EA(TAIR, RH)

      IMPLICIT NONE

      REAL*8 ES_EA
      REAL*8 TAIR
      REAL*8 RH

      REAL*8 ES, EA
C-----------------------------------------------------------------------

      ES_EA = ES(TAIR) - EA(TAIR, RH)
      RETURN
      END

C************************************************************************
C Sommaire :  BOWEN
C
C Description:
C     La fonction BOWEN calcule le rapport de Bowen,
C        (Teau - Tair) / (Es - Ea) * (Pair / Pnrm)
C
C Entrée:
C     TEAU     Température de l'eau
C     TAIR     Température de l'air
C     RH       Humidité relative
C     Pair     Pression de l'air
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION BOWEN(TEAU, TAIR, PAIR, RH)

      IMPLICIT NONE

      REAL*8 BOWEN
      REAL*8 TEAU
      REAL*8 TAIR
      REAL*8 PAIR
      REAL*8 RH

      REAL*8 DELP

      REAL*8 ES_EA

      REAL*8 P_NRM
      PARAMETER (P_NRM = 101.325D+03)
C-----------------------------------------------------------------------
      DELP = ES_EA(TAIR, RH)
      BOWEN= ((TEAU-TAIR) * PAIR) / (DELP * P_NRM)
      RETURN
      END
