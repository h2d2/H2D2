C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction CD2D_TMP_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION CD2D_TMP_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_TMP_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        CD2D_B1LN_REQNFN
      INTEGER        I
      INTEGER        IERR
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
      LOGICAL        SAUTE
C-----------------------------------------------------------------------

C---     Pas d'héritage en diamant
C      IERR = CD2D_B1LN_REQNFN(KFNC, IFNC)

      SAUTE = .FALSE.
      IF     (IFNC .EQ. EA_FUNC_PRCPRGL) THEN
                                    NF = 'CD2D_TMP_PRCPRGL@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRNO) THEN
                                    NF = 'CD2D_TMP_PRCPRNO@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_HLPPRGL) THEN
                                    NF = 'CD2D_TMP_HLPPRGL@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_HLPPRNO) THEN
                                    NF = 'CD2D_TMP_HLPPRNO@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRGL) THEN
                                    NF = 'CD2D_TMP_PRNPRGL@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
                                    NF = 'CD2D_TMP_PRNPRNO@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRGL) THEN
                                    NF = 'CD2D_TMP_PSLPRGL@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRNO) THEN
                                    NF = 'CD2D_TMP_PSLPRNO@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'CD2D_TMP_REQNFN@cd2d_tmp'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'CD2D_TMP_REQPRM@cd2d_tmp'
      ELSE
         SAUTE = .TRUE.
      ENDIF

      IF (.NOT. SAUTE) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
         CALL ERR_RESET()
      ENDIF

9999  CONTINUE
      CD2D_TMP_REQNFN = ERR_TYP()
      RETURN
      END
