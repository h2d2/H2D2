//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_tmp
// Entry point: extern "C" void fake_dll_cd2d_tmp()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:56.563133
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_TMP
F_PROT(CD2D_TMP_ASGCMN, cd2d_tmp_asgcmn);
F_PROT(CD2D_TMP_HLPPRGL, cd2d_tmp_hlpprgl);
F_PROT(CD2D_TMP_HLPPRNO, cd2d_tmp_hlpprno);
F_PROT(CD2D_TMP_PRCPRGL, cd2d_tmp_prcprgl);
F_PROT(CD2D_TMP_PRCPRNO, cd2d_tmp_prcprno);
F_PROT(CD2D_TMP_PRNPRGL, cd2d_tmp_prnprgl);
F_PROT(CD2D_TMP_PRNPRNO, cd2d_tmp_prnprno);
F_PROT(CD2D_TMP_PSLPRGL, cd2d_tmp_pslprgl);
F_PROT(CD2D_TMP_PSLPRNO, cd2d_tmp_pslprno);
F_PROT(CD2D_TMP_REQNFN, cd2d_tmp_reqnfn);
F_PROT(CD2D_TMP_REQPRM, cd2d_tmp_reqprm);
F_PROT(CD2D_TMP_PST_SIM_000, cd2d_tmp_pst_sim_000);
F_PROT(CD2D_TMP_PST_SIM_999, cd2d_tmp_pst_sim_999);
F_PROT(CD2D_TMP_PST_SIM_CTR, cd2d_tmp_pst_sim_ctr);
F_PROT(CD2D_TMP_PST_SIM_DTR, cd2d_tmp_pst_sim_dtr);
F_PROT(CD2D_TMP_PST_SIM_INI, cd2d_tmp_pst_sim_ini);
F_PROT(CD2D_TMP_PST_SIM_RST, cd2d_tmp_pst_sim_rst);
F_PROT(CD2D_TMP_PST_SIM_REQHBASE, cd2d_tmp_pst_sim_reqhbase);
F_PROT(CD2D_TMP_PST_SIM_HVALIDE, cd2d_tmp_pst_sim_hvalide);
F_PROT(CD2D_TMP_PST_SIM_ACC, cd2d_tmp_pst_sim_acc);
F_PROT(CD2D_TMP_PST_SIM_FIN, cd2d_tmp_pst_sim_fin);
F_PROT(CD2D_TMP_PST_SIM_XEQ, cd2d_tmp_pst_sim_xeq);
F_PROT(CD2D_TMP_PST_SIM_ASGHSIM, cd2d_tmp_pst_sim_asghsim);
F_PROT(CD2D_TMP_PST_SIM_REQHVNO, cd2d_tmp_pst_sim_reqhvno);
F_PROT(CD2D_TMP_PST_SIM_REQNOMF, cd2d_tmp_pst_sim_reqnomf);
F_PROT(CD2D_TMP_PST_SIM_CLC, cd2d_tmp_pst_sim_clc);
 
// ---  class CD2D_TMPC
F_PROT(CD2D_TMPC_000, cd2d_tmpc_000);
F_PROT(CD2D_TMPC_REQHBASE, cd2d_tmpc_reqhbase);
F_PROT(CD2D_TMPC_ASGCMN, cd2d_tmpc_asgcmn);
F_PROT(CD2D_TMPC_REQNFN, cd2d_tmpc_reqnfn);
F_PROT(CD2D_TMPC_REQPRM, cd2d_tmpc_reqprm);
 
// ---  class CD2D_TMPN
F_PROT(CD2D_TMPN_000, cd2d_tmpn_000);
F_PROT(CD2D_TMPN_REQHBASE, cd2d_tmpn_reqhbase);
F_PROT(CD2D_TMPN_ASGCMN, cd2d_tmpn_asgcmn);
F_PROT(CD2D_TMPN_REQNFN, cd2d_tmpn_reqnfn);
F_PROT(CD2D_TMPN_REQPRM, cd2d_tmpn_reqprm);
 
// ---  class IC_CD2D
F_PROT(IC_CD2D_TMP_PST_SIM_CMD, ic_cd2d_tmp_pst_sim_cmd);
F_PROT(IC_CD2D_TMP_PST_SIM_REQCMD, ic_cd2d_tmp_pst_sim_reqcmd);
F_PROT(IC_CD2D_TMPC_XEQCTR, ic_cd2d_tmpc_xeqctr);
F_PROT(IC_CD2D_TMPC_XEQMTH, ic_cd2d_tmpc_xeqmth);
F_PROT(IC_CD2D_TMPC_OPBDOT, ic_cd2d_tmpc_opbdot);
F_PROT(IC_CD2D_TMPC_REQCLS, ic_cd2d_tmpc_reqcls);
F_PROT(IC_CD2D_TMPC_REQHDL, ic_cd2d_tmpc_reqhdl);
F_PROT(IC_CD2D_TMPN_XEQCTR, ic_cd2d_tmpn_xeqctr);
F_PROT(IC_CD2D_TMPN_XEQMTH, ic_cd2d_tmpn_xeqmth);
F_PROT(IC_CD2D_TMPN_OPBDOT, ic_cd2d_tmpn_opbdot);
F_PROT(IC_CD2D_TMPN_REQCLS, ic_cd2d_tmpn_reqcls);
F_PROT(IC_CD2D_TMPN_REQHDL, ic_cd2d_tmpn_reqhdl);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_tmp()
{
   static char libname[] = "cd2d_tmp";
 
   // ---  class CD2D_TMP
   F_RGST(CD2D_TMP_ASGCMN, cd2d_tmp_asgcmn, libname);
   F_RGST(CD2D_TMP_HLPPRGL, cd2d_tmp_hlpprgl, libname);
   F_RGST(CD2D_TMP_HLPPRNO, cd2d_tmp_hlpprno, libname);
   F_RGST(CD2D_TMP_PRCPRGL, cd2d_tmp_prcprgl, libname);
   F_RGST(CD2D_TMP_PRCPRNO, cd2d_tmp_prcprno, libname);
   F_RGST(CD2D_TMP_PRNPRGL, cd2d_tmp_prnprgl, libname);
   F_RGST(CD2D_TMP_PRNPRNO, cd2d_tmp_prnprno, libname);
   F_RGST(CD2D_TMP_PSLPRGL, cd2d_tmp_pslprgl, libname);
   F_RGST(CD2D_TMP_PSLPRNO, cd2d_tmp_pslprno, libname);
   F_RGST(CD2D_TMP_REQNFN, cd2d_tmp_reqnfn, libname);
   F_RGST(CD2D_TMP_REQPRM, cd2d_tmp_reqprm, libname);
   F_RGST(CD2D_TMP_PST_SIM_000, cd2d_tmp_pst_sim_000, libname);
   F_RGST(CD2D_TMP_PST_SIM_999, cd2d_tmp_pst_sim_999, libname);
   F_RGST(CD2D_TMP_PST_SIM_CTR, cd2d_tmp_pst_sim_ctr, libname);
   F_RGST(CD2D_TMP_PST_SIM_DTR, cd2d_tmp_pst_sim_dtr, libname);
   F_RGST(CD2D_TMP_PST_SIM_INI, cd2d_tmp_pst_sim_ini, libname);
   F_RGST(CD2D_TMP_PST_SIM_RST, cd2d_tmp_pst_sim_rst, libname);
   F_RGST(CD2D_TMP_PST_SIM_REQHBASE, cd2d_tmp_pst_sim_reqhbase, libname);
   F_RGST(CD2D_TMP_PST_SIM_HVALIDE, cd2d_tmp_pst_sim_hvalide, libname);
   F_RGST(CD2D_TMP_PST_SIM_ACC, cd2d_tmp_pst_sim_acc, libname);
   F_RGST(CD2D_TMP_PST_SIM_FIN, cd2d_tmp_pst_sim_fin, libname);
   F_RGST(CD2D_TMP_PST_SIM_XEQ, cd2d_tmp_pst_sim_xeq, libname);
   F_RGST(CD2D_TMP_PST_SIM_ASGHSIM, cd2d_tmp_pst_sim_asghsim, libname);
   F_RGST(CD2D_TMP_PST_SIM_REQHVNO, cd2d_tmp_pst_sim_reqhvno, libname);
   F_RGST(CD2D_TMP_PST_SIM_REQNOMF, cd2d_tmp_pst_sim_reqnomf, libname);
   F_RGST(CD2D_TMP_PST_SIM_CLC, cd2d_tmp_pst_sim_clc, libname);
 
   // ---  class CD2D_TMPC
   F_RGST(CD2D_TMPC_000, cd2d_tmpc_000, libname);
   F_RGST(CD2D_TMPC_REQHBASE, cd2d_tmpc_reqhbase, libname);
   F_RGST(CD2D_TMPC_ASGCMN, cd2d_tmpc_asgcmn, libname);
   F_RGST(CD2D_TMPC_REQNFN, cd2d_tmpc_reqnfn, libname);
   F_RGST(CD2D_TMPC_REQPRM, cd2d_tmpc_reqprm, libname);
 
   // ---  class CD2D_TMPN
   F_RGST(CD2D_TMPN_000, cd2d_tmpn_000, libname);
   F_RGST(CD2D_TMPN_REQHBASE, cd2d_tmpn_reqhbase, libname);
   F_RGST(CD2D_TMPN_ASGCMN, cd2d_tmpn_asgcmn, libname);
   F_RGST(CD2D_TMPN_REQNFN, cd2d_tmpn_reqnfn, libname);
   F_RGST(CD2D_TMPN_REQPRM, cd2d_tmpn_reqprm, libname);
 
   // ---  class IC_CD2D
   F_RGST(IC_CD2D_TMP_PST_SIM_CMD, ic_cd2d_tmp_pst_sim_cmd, libname);
   F_RGST(IC_CD2D_TMP_PST_SIM_REQCMD, ic_cd2d_tmp_pst_sim_reqcmd, libname);
   F_RGST(IC_CD2D_TMPC_XEQCTR, ic_cd2d_tmpc_xeqctr, libname);
   F_RGST(IC_CD2D_TMPC_XEQMTH, ic_cd2d_tmpc_xeqmth, libname);
   F_RGST(IC_CD2D_TMPC_OPBDOT, ic_cd2d_tmpc_opbdot, libname);
   F_RGST(IC_CD2D_TMPC_REQCLS, ic_cd2d_tmpc_reqcls, libname);
   F_RGST(IC_CD2D_TMPC_REQHDL, ic_cd2d_tmpc_reqhdl, libname);
   F_RGST(IC_CD2D_TMPN_XEQCTR, ic_cd2d_tmpn_xeqctr, libname);
   F_RGST(IC_CD2D_TMPN_XEQMTH, ic_cd2d_tmpn_xeqmth, libname);
   F_RGST(IC_CD2D_TMPN_OPBDOT, ic_cd2d_tmpn_opbdot, libname);
   F_RGST(IC_CD2D_TMPN_REQCLS, ic_cd2d_tmpn_reqcls, libname);
   F_RGST(IC_CD2D_TMPN_REQHDL, ic_cd2d_tmpn_reqhdl, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
