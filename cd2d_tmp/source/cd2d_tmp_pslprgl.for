C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     CONTROLE DE POSITIVITE
      IF (ERR_GOOD()) THEN
         DO I=CD2D_BSE_NPRGLL+1,CD2D_BSE_NPRGLL+19
            IF (VPRGL(I) .LT. ZERO) THEN
               WRITE(LOG_BUF,*) 'ERR_PROP_GLOB_NEGATIVE:',
     &                       'V(', I, ')=',VPRGL(I)
               CALL LOG_ECRIS(LOG_BUF)
               CALL ERR_ASG(ERR_ERR, 'ERR_PROP_GLOB_NEGATIVE')
            ENDIF
         ENDDO
      ENDIF

C---     PROPRIÉTÉS EGALES A PETIT SI ZERO
      IF (ERR_GOOD()) THEN
         DO I=CD2D_BSE_NPRGLL+1,CD2D_BSE_NPRGLL+5
            VPRGL(I) = MAX(VPRGL(I), PETIT)
         ENDDO
      ENDIF

C---     APPEL L'ELEMENT DE BASE
      IF (ERR_GOOD()) THEN
         CALL CD2D_BSE_PSLPRGL (VPRGL, IERR)
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END

