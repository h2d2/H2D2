C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction CD2D_TMP_ASGCMN
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CD2D_TMP_ASGCMN(EG_KA, EA_KA, EA_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER CD2D_TMP_ASGCMN
      INTEGER EG_KA(*)
      INTEGER EA_KA(*)
      REAL*8  EA_VA(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER I
      INTEGER IERR
      INTEGER CD2D_B1L_ASGCMN
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = EG_KA(I)
      ENDDO

      DO I=1, LM_CMMN_KA_DIM
         LM_CMMN_KA(I) = EA_KA(I)
      ENDDO

      DO I=1, LM_CMMN_VA_DIM
         LM_CMMN_VA(I) = EA_VA(I)
      ENDDO

      IERR = CD2D_B1L_ASGCMN(EG_KA, EA_KA, EA_VA)

      CD2D_TMP_ASGCMN = ERR_TYP()
      RETURN
      END
