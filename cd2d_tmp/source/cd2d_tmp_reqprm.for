C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Température (TMP)
C     Formulation de base.
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_TMP_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_TMP_REQPRM(TGELV,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_TMP_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INTEGER NTMPRN

      INTEGER, PARAMETER :: NNELV = 3
      INTEGER, PARAMETER :: NNELS = 2
      INTEGER, PARAMETER :: NTMPRNC = 0   ! nombre de prop. nod. conduction eau-air
      INTEGER, PARAMETER :: NTMPRNE = 2   ! nombre de prop. nod. evaporation
      INTEGER, PARAMETER :: NTMPRNF = 1   ! nombre de prop. nod. conduction eau-fond
      INTEGER, PARAMETER :: NTMPRNG = 2   ! nombre de prop. nod. conduction eau-glace
      INTEGER, PARAMETER :: NTMPRNI = 2   ! nombre de prop. nod. radiation infrarouge
      INTEGER, PARAMETER :: NTMPRNP = 1   ! nombre de prop. nod. precipitation de neige ou pluie
      INTEGER, PARAMETER :: NTMPRNS = 3   ! nombre de prop. nod. radiation solaire
C-----------------------------------------------------------------------

C---     INITIALISE LES PARAMETRES DE L'ÉLÉMENT PARENT VIRTUEL
      CALL CD2D_B1LN_REQPRM(TGELV,
     &                      NPRGL,
     &                      NPRGLL,
     &                      NPRNO,
     &                      NPRNOL,
     &                      NPREV,
     &                      NPREVL,
     &                      NPRES,
     &                      NSOLC,
     &                      NSOLCL,
     &                      NSOLR,
     &                      NSOLRL,
     &                      NDLN,
     &                      NDLEV,
     &                      NDLES,
     &                      ASURFACE,
     &                      ESTLIN)

C---     NOMBRE TOTAL DE PROPRIÉTÉS NODALES ASSOCIÉES AU TERME SOURCE/PUITS
      NTMPRN = NTMPRNC + NTMPRNE + NTMPRNF + NTMPRNG +
     &         NTMPRNI + NTMPRNP + NTMPRNS

C---     INITIALISE LES PARAMETRES DE L'HERITIER
      NDLN  = 1                                 ! NB DE DDL/NOEUD
      NDLEV = NDLN * NNELV                      ! NB DE DDL PAR ELEMENT DE VOLUME
      NDLES = NDLN * NNELS                      ! NB DE DDL PAR ELEMENT DE SURFACE
      NPRGL = NPRGL + 19                        ! NB DE PROPRIÉTÉS GLOBALES
      NPRGLL= NPRGLL+ 19                        ! NB DE PROPRIÉTÉS GLOBALES LUES
      NPRNO = NPRNO + NTMPRN                    ! NB DE PROPRIÉTÉS NODALES
      NPRNOL= NPRNOL+ NTMPRN                    ! NB DE PROPRIÉTÉS NODALES LUES

      RETURN
      END

