//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_gpu
// Entry point: extern "C" void fake_dll_h2d2_gpu()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:02.003355
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class GP_CLC1
F_PROT(GP_CLC1_000, gp_clc1_000);
F_PROT(GP_CLC1_999, gp_clc1_999);
F_PROT(GP_CLC1_CTR, gp_clc1_ctr);
F_PROT(GP_CLC1_DTR, gp_clc1_dtr);
F_PROT(GP_CLC1_INI, gp_clc1_ini);
F_PROT(GP_CLC1_RST, gp_clc1_rst);
F_PROT(GP_CLC1_REQHBASE, gp_clc1_reqhbase);
F_PROT(GP_CLC1_HVALIDE, gp_clc1_hvalide);
F_PROT(GP_CLC1_XEQ, gp_clc1_xeq);
 
// ---  class GP_CLCL
F_PROT(GP_CLCL_000, gp_clcl_000);
F_PROT(GP_CLCL_999, gp_clcl_999);
F_PROT(GP_CLCL_CTR, gp_clcl_ctr);
F_PROT(GP_CLCL_DTR, gp_clcl_dtr);
F_PROT(GP_CLCL_INI, gp_clcl_ini);
F_PROT(GP_CLCL_RST, gp_clcl_rst);
F_PROT(GP_CLCL_REQHBASE, gp_clcl_reqhbase);
F_PROT(GP_CLCL_HVALIDE, gp_clcl_hvalide);
F_PROT(GP_CLCL_XEQ, gp_clcl_xeq);
 
// ---  class GP_CONF
F_PROT(GP_CONF_000, gp_conf_000);
F_PROT(GP_CONF_999, gp_conf_999);
F_PROT(GP_CONF_CTR, gp_conf_ctr);
F_PROT(GP_CONF_DTR, gp_conf_dtr);
F_PROT(GP_CONF_INI, gp_conf_ini);
F_PROT(GP_CONF_RST, gp_conf_rst);
F_PROT(GP_CONF_REQHBASE, gp_conf_reqhbase);
F_PROT(GP_CONF_HVALIDE, gp_conf_hvalide);
F_PROT(GP_CONF_DISDEV, gp_conf_disdev);
F_PROT(GP_CONF_REQNCPU, gp_conf_reqncpu);
F_PROT(GP_CONF_REQNDEV, gp_conf_reqndev);
F_PROT(GP_CONF_REQNTSK, gp_conf_reqntsk);
F_PROT(GP_CONF_REQHDEV, gp_conf_reqhdev);
 
// ---  class GP_DEVC
F_PROT(GP_DEVC_000, gp_devc_000);
F_PROT(GP_DEVC_999, gp_devc_999);
F_PROT(GP_DEVC_CTR, gp_devc_ctr);
F_PROT(GP_DEVC_DTR, gp_devc_dtr);
F_PROT(GP_DEVC_INI, gp_devc_ini);
F_PROT(GP_DEVC_RST, gp_devc_rst);
F_PROT(GP_DEVC_REQHBASE, gp_devc_reqhbase);
F_PROT(GP_DEVC_HVALIDE, gp_devc_hvalide);
F_PROT(GP_DEVC_REQITYP, gp_devc_reqityp);
F_PROT(GP_DEVC_REQMIN_CU, gp_devc_reqmin_cu);
F_PROT(GP_DEVC_REQMAX_CU, gp_devc_reqmax_cu);
F_PROT(GP_DEVC_REQMIN_WGSZ, gp_devc_reqmin_wgsz);
F_PROT(GP_DEVC_REQMAX_WGSZ, gp_devc_reqmax_wgsz);
F_PROT(GP_DEVC_REQGBL_MMSZ, gp_devc_reqgbl_mmsz);
F_PROT(GP_DEVC_ASGNCPU, gp_devc_asgncpu);
F_PROT(GP_DEVC_REQNCPU, gp_devc_reqncpu);
F_PROT(GP_DEVC_ASGNTSK, gp_devc_asgntsk);
F_PROT(GP_DEVC_ASGITSK, gp_devc_asgitsk);
F_PROT(GP_DEVC_REQNTSK, gp_devc_reqntsk);
F_PROT(GP_DEVC_ASGNWGP, gp_devc_asgnwgp);
F_PROT(GP_DEVC_REQNWGP, gp_devc_reqnwgp);
F_PROT(GP_DEVC_REQGWI, gp_devc_reqgwi);
F_PROT(GP_DEVC_ASGGMEM, gp_devc_asggmem);
F_PROT(GP_DEVC_REQGMEM, gp_devc_reqgmem);
F_PROT(GP_DEVC_START, gp_devc_start);
F_PROT(GP_DEVC_ALLOC, gp_devc_alloc);
F_PROT(GP_DEVC_ALLINT, gp_devc_allint);
F_PROT(GP_DEVC_ALLRE8, gp_devc_allre8);
F_PROT(GP_DEVC_B2CPU, gp_devc_b2cpu);
F_PROT(GP_DEVC_K2CPU, gp_devc_k2cpu);
F_PROT(GP_DEVC_V2CPU, gp_devc_v2cpu);
F_PROT(GP_DEVC_B2DEV, gp_devc_b2dev);
F_PROT(GP_DEVC_K2DEV, gp_devc_k2dev);
F_PROT(GP_DEVC_V2DEV, gp_devc_v2dev);
F_PROT(GP_DEVC_SETARG_BUF, gp_devc_setarg_buf);
F_PROT(GP_DEVC_LOADK, gp_devc_loadk);
F_PROT(GP_DEVC_XEQK, gp_devc_xeqk);
F_PROT(GP_DEVC_PRINT, gp_devc_print);
F_PROT(GP_DEVC_PRNALL, gp_devc_prnall);
 
// ---  class GP_DIST
F_PROT(GP_DIST_000, gp_dist_000);
F_PROT(GP_DIST_999, gp_dist_999);
F_PROT(GP_DIST_CTR, gp_dist_ctr);
F_PROT(GP_DIST_DTR, gp_dist_dtr);
F_PROT(GP_DIST_INI, gp_dist_ini);
F_PROT(GP_DIST_RST, gp_dist_rst);
F_PROT(GP_DIST_REQHBASE, gp_dist_reqhbase);
F_PROT(GP_DIST_HVALIDE, gp_dist_hvalide);
F_PROT(GP_DIST_REQNBLCS, gp_dist_reqnblcs);
F_PROT(GP_DIST_REQNNOD, gp_dist_reqnnod);
F_PROT(GP_DIST_REQNELE, gp_dist_reqnele);
F_PROT(GP_DIST_REQIELE, gp_dist_reqiele);
 
// ---  class GP_PAGE
M_PROT(GP_PAGE_M, gp_page_m, GP_PAGE_REQPDTA, gp_page_reqpdta);
F_PROT(GP_PAGE_000, gp_page_000);
F_PROT(GP_PAGE_999, gp_page_999);
F_PROT(GP_PAGE_CTR, gp_page_ctr);
F_PROT(GP_PAGE_DTR, gp_page_dtr);
F_PROT(GP_PAGE_INI, gp_page_ini);
F_PROT(GP_PAGE_RST, gp_page_rst);
F_PROT(GP_PAGE_REQHBASE, gp_page_reqhbase);
F_PROT(GP_PAGE_HVALIDE, gp_page_hvalide);
F_PROT(GP_PAGE_GATHER, gp_page_gather);
F_PROT(GP_PAGE_SCATTER, gp_page_scatter);
F_PROT(GP_PAGE_CP2DEV, gp_page_cp2dev);
F_PROT(GP_PAGE_CP2PGE, gp_page_cp2pge);
F_PROT(GP_PAGE_REQHDEV, gp_page_reqhdev);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_gpu()
{
   static char libname[] = "h2d2_gpu";
 
   // ---  class GP_CLC1
   F_RGST(GP_CLC1_000, gp_clc1_000, libname);
   F_RGST(GP_CLC1_999, gp_clc1_999, libname);
   F_RGST(GP_CLC1_CTR, gp_clc1_ctr, libname);
   F_RGST(GP_CLC1_DTR, gp_clc1_dtr, libname);
   F_RGST(GP_CLC1_INI, gp_clc1_ini, libname);
   F_RGST(GP_CLC1_RST, gp_clc1_rst, libname);
   F_RGST(GP_CLC1_REQHBASE, gp_clc1_reqhbase, libname);
   F_RGST(GP_CLC1_HVALIDE, gp_clc1_hvalide, libname);
   F_RGST(GP_CLC1_XEQ, gp_clc1_xeq, libname);
 
   // ---  class GP_CLCL
   F_RGST(GP_CLCL_000, gp_clcl_000, libname);
   F_RGST(GP_CLCL_999, gp_clcl_999, libname);
   F_RGST(GP_CLCL_CTR, gp_clcl_ctr, libname);
   F_RGST(GP_CLCL_DTR, gp_clcl_dtr, libname);
   F_RGST(GP_CLCL_INI, gp_clcl_ini, libname);
   F_RGST(GP_CLCL_RST, gp_clcl_rst, libname);
   F_RGST(GP_CLCL_REQHBASE, gp_clcl_reqhbase, libname);
   F_RGST(GP_CLCL_HVALIDE, gp_clcl_hvalide, libname);
   F_RGST(GP_CLCL_XEQ, gp_clcl_xeq, libname);
 
   // ---  class GP_CONF
   F_RGST(GP_CONF_000, gp_conf_000, libname);
   F_RGST(GP_CONF_999, gp_conf_999, libname);
   F_RGST(GP_CONF_CTR, gp_conf_ctr, libname);
   F_RGST(GP_CONF_DTR, gp_conf_dtr, libname);
   F_RGST(GP_CONF_INI, gp_conf_ini, libname);
   F_RGST(GP_CONF_RST, gp_conf_rst, libname);
   F_RGST(GP_CONF_REQHBASE, gp_conf_reqhbase, libname);
   F_RGST(GP_CONF_HVALIDE, gp_conf_hvalide, libname);
   F_RGST(GP_CONF_DISDEV, gp_conf_disdev, libname);
   F_RGST(GP_CONF_REQNCPU, gp_conf_reqncpu, libname);
   F_RGST(GP_CONF_REQNDEV, gp_conf_reqndev, libname);
   F_RGST(GP_CONF_REQNTSK, gp_conf_reqntsk, libname);
   F_RGST(GP_CONF_REQHDEV, gp_conf_reqhdev, libname);
 
   // ---  class GP_DEVC
   F_RGST(GP_DEVC_000, gp_devc_000, libname);
   F_RGST(GP_DEVC_999, gp_devc_999, libname);
   F_RGST(GP_DEVC_CTR, gp_devc_ctr, libname);
   F_RGST(GP_DEVC_DTR, gp_devc_dtr, libname);
   F_RGST(GP_DEVC_INI, gp_devc_ini, libname);
   F_RGST(GP_DEVC_RST, gp_devc_rst, libname);
   F_RGST(GP_DEVC_REQHBASE, gp_devc_reqhbase, libname);
   F_RGST(GP_DEVC_HVALIDE, gp_devc_hvalide, libname);
   F_RGST(GP_DEVC_REQITYP, gp_devc_reqityp, libname);
   F_RGST(GP_DEVC_REQMIN_CU, gp_devc_reqmin_cu, libname);
   F_RGST(GP_DEVC_REQMAX_CU, gp_devc_reqmax_cu, libname);
   F_RGST(GP_DEVC_REQMIN_WGSZ, gp_devc_reqmin_wgsz, libname);
   F_RGST(GP_DEVC_REQMAX_WGSZ, gp_devc_reqmax_wgsz, libname);
   F_RGST(GP_DEVC_REQGBL_MMSZ, gp_devc_reqgbl_mmsz, libname);
   F_RGST(GP_DEVC_ASGNCPU, gp_devc_asgncpu, libname);
   F_RGST(GP_DEVC_REQNCPU, gp_devc_reqncpu, libname);
   F_RGST(GP_DEVC_ASGNTSK, gp_devc_asgntsk, libname);
   F_RGST(GP_DEVC_ASGITSK, gp_devc_asgitsk, libname);
   F_RGST(GP_DEVC_REQNTSK, gp_devc_reqntsk, libname);
   F_RGST(GP_DEVC_ASGNWGP, gp_devc_asgnwgp, libname);
   F_RGST(GP_DEVC_REQNWGP, gp_devc_reqnwgp, libname);
   F_RGST(GP_DEVC_REQGWI, gp_devc_reqgwi, libname);
   F_RGST(GP_DEVC_ASGGMEM, gp_devc_asggmem, libname);
   F_RGST(GP_DEVC_REQGMEM, gp_devc_reqgmem, libname);
   F_RGST(GP_DEVC_START, gp_devc_start, libname);
   F_RGST(GP_DEVC_ALLOC, gp_devc_alloc, libname);
   F_RGST(GP_DEVC_ALLINT, gp_devc_allint, libname);
   F_RGST(GP_DEVC_ALLRE8, gp_devc_allre8, libname);
   F_RGST(GP_DEVC_B2CPU, gp_devc_b2cpu, libname);
   F_RGST(GP_DEVC_K2CPU, gp_devc_k2cpu, libname);
   F_RGST(GP_DEVC_V2CPU, gp_devc_v2cpu, libname);
   F_RGST(GP_DEVC_B2DEV, gp_devc_b2dev, libname);
   F_RGST(GP_DEVC_K2DEV, gp_devc_k2dev, libname);
   F_RGST(GP_DEVC_V2DEV, gp_devc_v2dev, libname);
   F_RGST(GP_DEVC_SETARG_BUF, gp_devc_setarg_buf, libname);
   F_RGST(GP_DEVC_LOADK, gp_devc_loadk, libname);
   F_RGST(GP_DEVC_XEQK, gp_devc_xeqk, libname);
   F_RGST(GP_DEVC_PRINT, gp_devc_print, libname);
   F_RGST(GP_DEVC_PRNALL, gp_devc_prnall, libname);
 
   // ---  class GP_DIST
   F_RGST(GP_DIST_000, gp_dist_000, libname);
   F_RGST(GP_DIST_999, gp_dist_999, libname);
   F_RGST(GP_DIST_CTR, gp_dist_ctr, libname);
   F_RGST(GP_DIST_DTR, gp_dist_dtr, libname);
   F_RGST(GP_DIST_INI, gp_dist_ini, libname);
   F_RGST(GP_DIST_RST, gp_dist_rst, libname);
   F_RGST(GP_DIST_REQHBASE, gp_dist_reqhbase, libname);
   F_RGST(GP_DIST_HVALIDE, gp_dist_hvalide, libname);
   F_RGST(GP_DIST_REQNBLCS, gp_dist_reqnblcs, libname);
   F_RGST(GP_DIST_REQNNOD, gp_dist_reqnnod, libname);
   F_RGST(GP_DIST_REQNELE, gp_dist_reqnele, libname);
   F_RGST(GP_DIST_REQIELE, gp_dist_reqiele, libname);
 
   // ---  class GP_PAGE
   M_RGST(GP_PAGE_M, gp_page_m, GP_PAGE_REQPDTA, gp_page_reqpdta, libname);
   F_RGST(GP_PAGE_000, gp_page_000, libname);
   F_RGST(GP_PAGE_999, gp_page_999, libname);
   F_RGST(GP_PAGE_CTR, gp_page_ctr, libname);
   F_RGST(GP_PAGE_DTR, gp_page_dtr, libname);
   F_RGST(GP_PAGE_INI, gp_page_ini, libname);
   F_RGST(GP_PAGE_RST, gp_page_rst, libname);
   F_RGST(GP_PAGE_REQHBASE, gp_page_reqhbase, libname);
   F_RGST(GP_PAGE_HVALIDE, gp_page_hvalide, libname);
   F_RGST(GP_PAGE_GATHER, gp_page_gather, libname);
   F_RGST(GP_PAGE_SCATTER, gp_page_scatter, libname);
   F_RGST(GP_PAGE_CP2DEV, gp_page_cp2dev, libname);
   F_RGST(GP_PAGE_CP2PGE, gp_page_cp2pge, libname);
   F_RGST(GP_PAGE_REQHDEV, gp_page_reqhdev, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
