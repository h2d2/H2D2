C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER GP_PAGE_000
C     INTEGER GP_PAGE_999
C     INTEGER GP_PAGE_CTR
C     INTEGER GP_PAGE_DTR
C     INTEGER GP_PAGE_INI
C     INTEGER GP_PAGE_RST
C     INTEGER GP_PAGE_REQHBASE
C     LOGICAL GP_PAGE_HVALIDE
C     INTEGER GP_PAGE_GATHER
C     INTEGER GP_PAGE_SCATTER
C     INTEGER GP_PAGE_CP2DEV
C     INTEGER GP_PAGE_CP2PGE
C   Private:
C     SUBROUTINE GP_PAGE_REQSELF
C     SUBROUTINE GP_PAGE_REQPDTA
C     INTEGER GP_PAGE_RAZ
C
C************************************************************************

! Une page correspond à un certain nombre d'éléments pour toutes les tâches.
! Il faut paginer lorsque le nombre d'éléments à assembler par tâche et
! supérieur au nombre d'éléments que la mémoire peut accommoder.

      MODULE GP_PAGE_M

      USE LM_GDTA_M
      USE LM_EDTA_M
      USE GP_PRMS_M
      IMPLICIT NONE

      PUBLIC

C---     Type de donnée associé à la classe
      TYPE :: GP_PAGE_DATA_T
         INTEGER NTSK      ! Nombre de tâches de la page
         INTEGER NELE      ! Nombre d'éléments de la page
         INTEGER*8 VRES_ID ! Table sur le device
         INTEGER*8 VPRC_ID ! Table sur le device
         INTEGER*8 VDJV_ID ! Table sur le device
         INTEGER*8 VPRG_ID ! Table sur le device
         INTEGER*8 VPRN_ID ! Table sur le device
         INTEGER*8 VDLG_ID ! Table sur le device
         INTEGER*8 PRMS_ID ! Table sur le device
!!         INTEGER*8 KLOC_ID ! Table sur le device
         REAL*8, ALLOCATABLE :: VRES(:,:,:,:)   ! NDLN, NNEL, NTSK, NELE
         REAL*8, ALLOCATABLE :: VPRC(:,:,:,:)   ! NPRC, NNEL, NTSK, NELE
         REAL*8, ALLOCATABLE :: VDJV(:,:,:)     ! NDJV,       NTSK, NELE
         REAL*8, ALLOCATABLE :: VPRG(:)         ! NPRG
         REAL*8, ALLOCATABLE :: VPRN(:,:,:,:)   ! NPRN, NNEL, NTSK, NELE
         REAL*8, ALLOCATABLE :: VDLG(:,:,:,:)   ! NDLN, NNEL, NTSK, NELE
         TYPE(GP_PRMS_RES_T) :: PRMS
         INTEGER,ALLOCATABLE :: KLOC(:,:,:,:)
      END TYPE GP_PAGE_DATA_T

!      PRIVATE

C---     Attributs statiques
      INTEGER, SAVE :: GP_PAGE_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: GP_PAGE_SELF_T
         INTEGER HDEV      ! Handle sur le device
         TYPE (LM_GDTA_T), POINTER :: GDTA
         TYPE (LM_EDTA_T), POINTER :: EDTA
         TYPE (GP_PAGE_DATA_T) :: PDTA
      END TYPE GP_PAGE_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Assigne les données géométriques.
C
C Description:
C     La fonction privée GP_PAGE_ASGGDTA(...) permet d'assigner les
C     données géométrique à l'Objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'assignation directe n'est pas possible, car cela demanderait une
C     interface explicite de la fonction qui fait l'assignation.
C************************************************************************
      FUNCTION GP_PAGE_ASGGDTA(HOBJ, GDTA)

      IMPLICIT NONE

      INTEGER :: GP_PAGE_ASGGDTA
      INTEGER, INTENT(IN):: HOBJ
      TYPE (LM_GDTA_T), INTENT(IN), TARGET :: GDTA

      INCLUDE 'err.fi'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => GP_PAGE_REQSELF(HOBJ)
      SELF%GDTA => GDTA
      GP_PAGE_ASGGDTA = ERR_TYP()
      RETURN
      END FUNCTION GP_PAGE_ASGGDTA

C************************************************************************
C Sommaire:    Assigne les données d'approximation.
C
C Description:
C     La fonction privée GP_PAGE_ASGEDTA(...) permet d'assigner les
C     données d'approximation à l'Objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'assignation directe n'est pas possible, car cela demanderait une
C     interface explicite de la fonction qui fait l'assignation.
C************************************************************************
      FUNCTION GP_PAGE_ASGEDTA(HOBJ, EDTA)

      IMPLICIT NONE

      INTEGER :: GP_PAGE_ASGEDTA
      INTEGER, INTENT(IN):: HOBJ
      TYPE (LM_EDTA_T), INTENT(IN), TARGET :: EDTA

      INCLUDE 'err.fi'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => GP_PAGE_REQSELF(HOBJ)
      SELF%EDTA => EDTA
      GP_PAGE_ASGEDTA = ERR_TYP()
      RETURN
      END FUNCTION GP_PAGE_ASGEDTA

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_PAGE_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_PAGE_SELF_T), POINTER :: GP_PAGE_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_PAGE_REQSELF => SELF
      RETURN
      END FUNCTION GP_PAGE_REQSELF

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_PAGE_REQPDTA(...) retourne le pointeur aux
C     données d'échange publique de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_REQPDTA(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_REQPDTA
CDEC$ ENDIF

      IMPLICIT NONE

      TYPE (GP_PAGE_DATA_T), POINTER :: GP_PAGE_REQPDTA
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => GP_PAGE_REQSELF(HOBJ)
      GP_PAGE_REQPDTA => SELF%PDTA
      RETURN
      END FUNCTION GP_PAGE_REQPDTA

      END MODULE GP_PAGE_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_PAGE_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_000
CDEC$ ENDIF

      USE GP_PAGE_M

      IMPLICIT NONE

      INCLUDE 'gppage.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_PAGE_HBASE, 'GPU - Data page')

      GP_PAGE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_PAGE_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_999
CDEC$ ENDIF

      USE GP_PAGE_M

      IMPLICIT NONE

      INCLUDE 'gppage.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_PAGE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_PAGE_HBASE, GP_PAGE_DTR)

      GP_PAGE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_PAGE_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_CTR
CDEC$ ENDIF

      USE GP_PAGE_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gppage.fc'

      INTEGER IERR, IRET
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_PAGE_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_PAGE_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_PAGE_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_PAGE_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_PAGE_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_DTR
CDEC$ ENDIF

      USE GP_PAGE_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_PAGE_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_PAGE_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_PAGE_HBASE)

      GP_PAGE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_PAGE_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_RAZ(HOBJ)

      USE GP_PAGE_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gppage.fc'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA
      SELF%HDEV = 0
      SELF%GDTA => NULL()
      SELF%EDTA => NULL()
      PDTA%NTSK = 0
      PDTA%NELE = 0
      PDTA%VRES_ID = 0
      PDTA%VPRC_ID = 0
      PDTA%VDJV_ID = 0
      PDTA%VPRG_ID = 0
      PDTA%VPRN_ID = 0
      PDTA%VDLG_ID = 0
      PDTA%PRMS_ID = 0
!!      PDTA%KLOC_ID = 0
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VRES))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VPRC))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VDJV))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VPRG))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VPRN))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%VDLG))
D     CALL ERR_ASR(.NOT. ALLOCATED(PDTA%KLOC))

      GP_PAGE_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_PAGE_INI initialise l'objet à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HDEV        Handel sur le device
C     NTSK        Nombre de tâches de la page
C     NELE        Nombre d'éléments par tâche de la page
C     NNEL        Nombre de noeuds par élément
C     NDLN        Nombre de degré de liberté par noeud
C     NPRN        Nombre de propriétés nodales par noeud
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_INI(HOBJ, HDEV, NTSK, NELE, GDTA, EDTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_INI
CDEC$ ENDIF

      USE GP_PAGE_M
      USE GP_PRMS_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HDEV   ! Handle sur le device
      INTEGER, INTENT(IN) :: NTSK   ! Nombre de tâches de la page
      INTEGER, INTENT(IN) :: NELE   ! Nombre d'éléments par tâche
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER IERR, IRET
      INTEGER ILEN
      INTEGER NDLN, NPRG, NPRN, NPRC, NPRE, NDJV, NNEL
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HDEV))
D     CALL ERR_PRE(NELE .GT. 0)
D     CALL ERR_PRE(NTSK .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_PAGE_RST(HOBJ)

C---     Récupère les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA

C---     Récupère les dimensions
      NDLN = EDTA%NDLN
      NPRC = EDTA%NPRNOC
      NPRN = EDTA%NPRNOL
      NPRG = EDTA%NPRGX
      NPRE = EDTA%NPREVL
      NDJV = GDTA%NDJV
      NNEL = GDTA%NNELV

C---     Alloue la mémoire CPU
      IF (ERR_GOOD()) THEN
         ALLOCATE(PDTA%VRES(NDLN, NNEL, NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%VPRC(NPRC, NNEL, NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%VDJV(NDJV,       NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%VPRG(NPRG), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%VPRN(NPRN, NNEL, NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%VDLG(NDLN, NNEL, NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         ALLOCATE(PDTA%KLOC(NDLN, NNEL, NTSK, NELE), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
      ENDIF

C---     Alloue la mémoire device
      IF (ERR_GOOD()) THEN
         IERR = GP_DEVC_ALLRE8(HDEV, NDLN*NNEL*NTSK*NELE, PDTA%VRES_ID)
         IERR = GP_DEVC_ALLRE8(HDEV, NPRC*NNEL*NTSK*NELE, PDTA%VPRC_ID)
         IERR = GP_DEVC_ALLRE8(HDEV, NDJV*     NTSK*NELE, PDTA%VDJV_ID)
         IERR = GP_DEVC_ALLRE8(HDEV, NPRG,                PDTA%VPRG_ID)
         IERR = GP_DEVC_ALLRE8(HDEV, NPRN*NNEL*NTSK*NELE, PDTA%VPRN_ID)
         IERR = GP_DEVC_ALLRE8(HDEV, NDLN*NNEL*NTSK*NELE, PDTA%VDLG_ID)
         ILEN = SIZEOF(PDTA%PRMS)
         IERR = GP_DEVC_ALLOC (HDEV, ILEN, PDTA%PRMS_ID)
!!         IERR = GP_DEVC_ALLINT(HDEV, NDLN*NNEL*NTSK*NELE, PDTA%KLOC_ID)
      ENDIF

C---     Assigne les paramètres
      IF (ERR_GOOD()) THEN
         PDTA%PRMS%NNEL = NNEL
         PDTA%PRMS%NDJV = NDJV
         PDTA%PRMS%NDLN = NDLN
         PDTA%PRMS%NPRG = NPRG
         PDTA%PRMS%NPRN = NPRN
         PDTA%PRMS%NPRC = NPRC
         PDTA%PRMS%NPRE = NPRE
         PDTA%PRMS%NELE = NELE
         PDTA%PRMS%NTSK = NTSK
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%HDEV = HDEV
         PDTA%NTSK = NTSK
         PDTA%NELE = NELE
         IERR = GP_PAGE_ASGGDTA(HOBJ, GDTA)
         IERR = GP_PAGE_ASGEDTA(HOBJ, EDTA)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_PAGE_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_RST
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gppage.fc'

      INTEGER IERR
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA

C---     Désalloue la mémoire
      IF (ALLOCATED(PDTA%VRES)) DEALLOCATE(PDTA%VRES)
      IF (ALLOCATED(PDTA%VPRC)) DEALLOCATE(PDTA%VPRC)
      IF (ALLOCATED(PDTA%VDJV)) DEALLOCATE(PDTA%VDJV)
      IF (ALLOCATED(PDTA%VPRG)) DEALLOCATE(PDTA%VPRG)
      IF (ALLOCATED(PDTA%VPRN)) DEALLOCATE(PDTA%VPRN)
      IF (ALLOCATED(PDTA%VDLG)) DEALLOCATE(PDTA%VDLG)
      IF (ALLOCATED(PDTA%KLOC)) DEALLOCATE(PDTA%KLOC)

C---     Reset les paramètres
      IERR = GP_PAGE_RAZ(HOBJ)

      GP_PAGE_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_PAGE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_REQHBASE
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER GP_PAGE_REQHBASE
C------------------------------------------------------------------------

      GP_PAGE_REQHBASE = GP_PAGE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_PAGE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_HVALIDE
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_PAGE_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_PAGE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: GP_PAGE_GATHER
C
C Description:
C     La méthode <code>GP_PAGE_GATHER(...)</code> transfert
C     les données externes dans les tables de la page.
C     Les données des tables nodales sont copiées.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HDST     Handle sur la distribution des éléments
C     IEMIN    Élément min à assembler, par rapport au début du bloc
C
C Sortie:
C
C Notes:
C     Les données copiées sont en mémoire CPU
C************************************************************************
      FUNCTION GP_PAGE_GATHER(HOBJ, HDST, IEMIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_GATHER
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HDST
      INTEGER, INTENT(IN) :: IEMIN

      INCLUDE 'gppage.fi'
      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IEMAX
      INTEGER IE, IT, IN, IEL
      INTEGER NNEL, NELE, NPRN
      INTEGER, DIMENSION(:),   POINTER :: KNE
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      INTEGER, DIMENSION(:,:), POINTER :: KLOC
      REAL*8,  DIMENSION(:,:), POINTER :: VDJV
      REAL*8,  DIMENSION(:),   POINTER :: VPRG
      REAL*8,  DIMENSION(:,:), POINTER :: VDLG
      REAL*8,  DIMENSION(:,:), POINTER :: VPRN
      LOGICAL ESTVALIDE
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
C     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C     CALL ERR_PRE(GP_DIST_HVALIDE(HDST))
C     CALL ERR_PRE(IEMIN .GT. 0)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA
      NELE = PDTA%NELE
      NNEL = SELF%GDTA%NNELV
      NPRN = PDTA%PRMS%NPRN
      KNGV => SELF%GDTA%KNGV
      VDJV => SELF%GDTA%VDJV
      VDLG => SELF%EDTA%VDLG
      VPRG => SELF%EDTA%VPRGX
      VPRN => SELF%EDTA%VPRNO
      KLOC => SELF%EDTA%KLOCN

C---     Limit sup des boucles
      IEMAX = IEMIN + PDTA%NELE - 1

C---     Les PRGL
      PDTA%VPRG(:) = VPRG(:)

C---     Boucle sur les éléments
      ESTVALIDE = .TRUE.
      DO IE=IEMIN,IEMAX

C---        Boucle sur toutes les tâches
         DO IT=1,PDTA%NTSK
            IEL = GP_DIST_REQIELE(HDST, IT, IE)
            IF (IEL .LE. 0) CYCLE

C---           Les connectivités
            KNE => KNGV(:, IEL)

C---           Transfert des métriques
            PDTA%VDJV(:, IT, IE) = VDJV(:, IEL)

C---           Transfert des PRNO
            DO IN=1,NNEL
               PDTA%VPRN(:, IN, IT, IE) = VPRN(1:NPRN, KNE(IN))
            ENDDO

C---           Transfert des DDL
            DO IN=1,NNEL
               PDTA%VDLG(:, IN, IT, IE) = VDLG(:, KNE(IN))
            ENDDO

C---           Table KLOCE de l'élément
            DO IN=1,NNEL
               PDTA%KLOC(:, IN, IT, IE) = KLOC(:, KNE(IN))
            ENDDO

         ENDDO
      ENDDO

      GP_PAGE_GATHER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GP_PAGE_SCATTER
C
C Description:
C     La méthode <code>GP_PAGE_SCATTER(...)</code> transfert les données
C     de la page vers l'externe.
C     Les données des tables nodales sont copiés.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_SCATTER(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_SCATTER
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IEMAX, ITMAX
      INTEGER IEDEB, IEFIN
      INTEGER IE, IT, IN
      INTEGER IEG
      INTEGER KNE(6), KNA(6)
      LOGICAL ESTVALIDE
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
C     CALL ERR_PRE(GP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)

      GP_PAGE_SCATTER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie les données vers le device.
C
C Description:
C     La fonction GP_PAGE_CP2DEV copie les données vers le device.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_CP2DEV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_CP2DEV
CDEC$ ENDIF

      USE GP_PAGE_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      INTEGER HDEV
      INTEGER ISIZ
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

!$ omp data :: connecter les données de la page au device

C---     Les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA
      HDEV = SELF%HDEV
D     CALL ERR_ASR(ALLOCATED(PDTA%VRES))
D     CALL ERR_ASR(ALLOCATED(PDTA%VPRC))
D     CALL ERR_ASR(ALLOCATED(PDTA%VDJV))
D     CALL ERR_ASR(ALLOCATED(PDTA%VPRG))
D     CALL ERR_ASR(ALLOCATED(PDTA%VPRN))
D     CALL ERR_ASR(ALLOCATED(PDTA%VDLG))
!!D     CALL ERR_ASR(ALLOCATED(PDTA%KLOC))
D     CALL ERR_ASR(PDTA%VRES_ID .NE. 0)
D     CALL ERR_ASR(PDTA%VPRC_ID .NE. 0)
D     CALL ERR_ASR(PDTA%VDJV_ID .NE. 0)
D     CALL ERR_ASR(PDTA%VPRG_ID .NE. 0)
D     CALL ERR_ASR(PDTA%VPRN_ID .NE. 0)
D     CALL ERR_ASR(PDTA%VDLG_ID .NE. 0)
!!D     CALL ERR_ASR(PDTA%KLOC_ID .NE. 0)
D     CALL ERR_ASR(PDTA%PRMS_ID .NE. 0)

C---     Copie
D     IF (ERR_GOOD()) THEN          !! Inutile - peut-être initialisé sur le device
D        ISIZ = SIZE(PDTA%VRES)
D        IERR = GP_DEVC_V2DEV(HDEV,
D    &                        ISIZ,
D    &                        PDTA%VRES(:,1,1,1),
D    &                        PDTA%VRES_ID)
D     ENDIF
D     IF (ERR_GOOD()) THEN          !! Inutile - peut-être initialisé sur le device
D        ISIZ = SIZE(PDTA%VPRC)
D        IERR = GP_DEVC_V2DEV(HDEV,
D    &                        ISIZ,
D    &                        PDTA%VPRC(:,1,1,1),
D    &                        PDTA%VPRC_ID)
D     ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VDJV)
         IERR = GP_DEVC_V2DEV(HDEV,
     &                        ISIZ,
     &                        PDTA%VDJV(:,1,1),
     &                        PDTA%VDJV_ID)
      ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VPRG)
         IERR = GP_DEVC_V2DEV(HDEV,
     &                        ISIZ,
     &                        PDTA%VPRG(:),
     &                        PDTA%VPRG_ID)
      ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VPRN)
         IERR = GP_DEVC_V2DEV(HDEV,
     &                        ISIZ,
     &                        PDTA%VPRN(:,1,1,1),
     &                        PDTA%VPRN_ID)
      ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VDLG)
         IERR = GP_DEVC_V2DEV(HDEV,
     &                        ISIZ,
     &                        PDTA%VDLG(:,1,1,1),
     &                        PDTA%VDLG_ID)
      ENDIF
     !! IF (ERR_GOOD()) THEN
     !!    ISIZ = SIZE(PDTA%KLOC)
     !!    IERR = GP_DEVC_K2DEV(HDEV,
     !!&                        ISIZ,
     !!&                        PDTA%KLOC(:,1,1,1),
     !!&                        PDTA%KLOC_ID)
     !! ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZEOF(PDTA%PRMS)
         IERR = GP_DEVC_B2DEV(HDEV,
     &                        ISIZ,
     &                        SO_ALLC_CST2B(C_LOC(PDTA%PRMS)),
     &                        PDTA%PRMS_ID)
      ENDIF

      GP_PAGE_CP2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie les données du device.
C
C Description:
C     La fonction GP_PAGE_CP2PGE copie les données du device vers l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_CP2PGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_CP2PGE
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      INTEGER ISIZ
      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_PAGE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)
      PDTA => SELF%PDTA
D     CALL ERR_ASR(ALLOCATED(PDTA%VRES))
D     CALL ERR_ASR(PDTA%VRES_ID .NE. 0)

C---     Copie VRES
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VRES)
         IERR = GP_DEVC_V2CPU(SELF%HDEV,
     &                        ISIZ,
     &                        PDTA%VRES(:,1,1,1),
     &                        PDTA%VRES_ID)
      ENDIF
      IF (ERR_GOOD()) THEN
         ISIZ = SIZE(PDTA%VPRC)
         IERR = GP_DEVC_V2CPU(SELF%HDEV,
     &                        ISIZ,
     &                        PDTA%VPRC(:,1,1,1),
     &                        PDTA%VPRC_ID)
      ENDIF

      GP_PAGE_CP2PGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: GP_PAGE_REQHDEV
C
C Description:
C     La méthode <code>GP_PAGE_REQHDEV(...)</code> retourne le handle
C     sur le device GPU associé à la page.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_PAGE_REQHDEV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_PAGE_REQHDEV
CDEC$ ENDIF

      USE GP_PAGE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gppage.fi'
      INCLUDE 'err.fi'

      TYPE (GP_PAGE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
C     CALL ERR_PRE(GP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_PAGE_REQSELF(HOBJ)

      GP_PAGE_REQHDEV = SELF%HDEV
      RETURN
      END


