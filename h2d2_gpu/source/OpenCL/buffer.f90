!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class buffer
! \brief A memory buffer on a compute device.
!
! The buffer class represents a memory buffer on a compute device.
!
! Buffers are allocated within a compute context. For example, to allocate
! a memory buffer for 32 float's:
!
! \snippet test/test_buffer.cpp constructor
!
! Once created, data can be copied to and from the buffer using the
! \c enqueue_*_buffer() methods in the command_queue class. For example, to
! copy a set of \c int values from the host to the device:
! \code
! int data[] = { 1, 2, 3, 4 };
!
! queue.enqueue_write_buffer(buf, 0, 4 * sizeof(int), data);
! \endcode
!
! Also see the copy() algorithm for a higher-level interface to copying data
! between the host and the device. For a higher-level, dynamically-resizable,
! type-safe container for data on a compute device, use the vector<T> class.
!
! Buffer objects have reference semantics. Creating a copy of a buffer
! object simply creates another reference to the underlying OpenCL memory
! object. To create an actual copy use the buffer::clone() method.
!
! \see context, command_queue

MODULE OCL_BUFFER_M

   USE clfortran
   USE ISO_C_BINDING

   IMPLICIT NONE

   TYPE :: OCL_BUFFER_T
      INTEGER(C_INTPTR_T), PRIVATE :: M_BUFFER = 0
      INTEGER(C_SIZE_T),   PRIVATE :: M_SIZE = 0

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: GET_SIZE
   END TYPE OCL_BUFFER_T

   INTERFACE OCL_BUFFER_CTR
      MODULE PROCEDURE OCL_BUFFER_CTR_NULL, &
                       OCL_BUFFER_CTR_BUF
   END INTERFACE


CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
!    ! Create a new memory buffer in of \p size with \p flags in
!    ! \p context.
!    !
!    ! \see_opencl_ref{clCreateBuffer}
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_BUFFER_CTR_NULL(SELF, CTXT, FLAGS, SIZE)

   USE OCL_UTIL_M
   USE OCL_CONTEXT_M

   TYPE(OCL_BUFFER_T),  INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTXT
   INTEGER(8), INTENT(IN) :: SIZE
   INTEGER(8), INTENT(IN) :: FLAGS

   INTEGER :: IRET
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INTPTR_T):: BID
   TYPE(OCL_BUFFER_T) :: SELF0
!---------------------------------------------------------------------

   CLRET = CL_SUCCESS
   BID = clCreateBuffer(CTXT.get(), &
                        FLAGS, &
                        MAX(SIZE, 1), &
                        C_NULL_PTR, &
                        CLRET)

   ! ---  Assigne les attributs
   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_buffer = BID
      SELF%m_size   = size
   ENDIF

   OCL_BUFFER_CTR_NULL = CLRET
   RETURN
   END FUNCTION OCL_BUFFER_CTR_NULL

!************************************************************************
! Sommaire:
!
! Description:
!    ! Create a new memory buffer in of \p size with \p flags in
!    ! \p context.
!    !
!    ! \see_opencl_ref{clCreateBuffer}
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_BUFFER_CTR_BUF(SELF, CTXT, FLAGS, SIZE, BUF)

   USE OCL_UTIL_M
   USE OCL_CONTEXT_M

   TYPE(OCL_BUFFER_T),  INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTXT
   INTEGER(8), INTENT(IN) :: SIZE
   INTEGER(8), INTENT(IN) :: FLAGS
   BYTE,       INTENT(IN) :: BUF(:)

   INTEGER :: IRET
   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INTPTR_T):: BID
   TYPE(OCL_BUFFER_T) :: SELF0
!---------------------------------------------------------------------

   CLRET = CL_SUCCESS
   BID = clCreateBuffer(CTXT.get(), &
                        FLAGS, &
                        MAX(SIZE, 1), &
                        C_LOC(BUF), &
                        CLRET)

   ! ---  Assigne les attributs
   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_buffer = BID
      SELF%m_size   = size
   ENDIF

   OCL_BUFFER_CTR_BUF = CLRET
   RETURN
   END FUNCTION OCL_BUFFER_CTR_BUF

!************************************************************************
! Sommaire:
!
! Description:
!    ! Destroys the kernel object.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_BUFFER_DTR(SELF)

   TYPE(OCL_BUFFER_T), INTENT(INOUT) :: SELF
!---------------------------------------------------------------------

   SELF%m_buffer = 0

   OCL_BUFFER_DTR = CL_SUCCESS
   RETURN
   END FUNCTION OCL_BUFFER_DTR

!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL device (ID).
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_BUFFER_T) :: SELF
!---------------------------------------------------------------------

   GET = SELF%m_buffer
   RETURN
   END FUNCTION GET

!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL device (ID).
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET_SIZE(SELF)

   CLASS(OCL_BUFFER_T) :: SELF
!---------------------------------------------------------------------

   GET_SIZE = SELF%m_size
   RETURN
   END FUNCTION GET_SIZE


!!!class buffer : public memory_object
!!!{
!!!public:
!!!
!!!    ! Create a new memory buffer in of \p size with \p flags in
!!!    ! \p context.
!!!    !
!!!    ! \see_opencl_ref{clCreateBuffer}
!!!    buffer(const context &context,
!!!           size_t size,
!!!           cl_mem_flags flags = read_write,
!!!           void *host_ptr = 0)
!!!    {
!!!        cl_int error = 0;
!!!        m_mem = clCreateBuffer(context,
!!!                               flags,
!!!                               (std::max)(size, size_t(1)),
!!!                               host_ptr,
!!!                               &error);
!!!        if(!m_mem){
!!!            BOOST_THROW_EXCEPTION(opencl_error(error));
!!!        }
!!!    }
!!!
!!!
!!!    ! Returns the size of the buffer in bytes.
!!!    size_t size() const
!!!    {
!!!        return get_memory_size();
!!!    }
!!!
!!!    ! \internal_
!!!    size_t max_size() const
!!!    {
!!!        return get_context().get_device().max_memory_alloc_size();
!!!    }
!!!
!!!    ! Returns information about the buffer.
!!!    !
!!!    ! \see_opencl_ref{clGetMemObjectInfo}
!!!    template<class T>
!!!    T get_info(cl_mem_info info) const
!!!    {
!!!        return get_memory_info<T>(info);
!!!    }
!!!
!!!    ! \overload
!!!    template<int Enum>
!!!    typename detail::get_object_info_type<buffer, Enum>::type
!!!    get_info() const;
!!!
!!!    ! Creates a new buffer with a copy of the data in \c *this. Uses
!!!    ! \p queue to perform the copy.
!!!    buffer clone(command_queue &queue) const;
!!!
!!!};
!!!
END MODULE OCL_BUFFER_M
