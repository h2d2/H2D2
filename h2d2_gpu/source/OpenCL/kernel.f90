!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class kernel
! \brief A compute kernel.
!

! \see command_queue, program
MODULE OCL_KERNEL_M

   USE clfortran
   USE ISO_C_BINDING

   IMPLICIT NONE

   TYPE, PUBLIC :: OCL_KERNEL_T
      INTEGER(C_INTPTR_T), PRIVATE :: M_KERNEL = 0

      CONTAINS
      PROCEDURE, PUBLIC :: GET

      PROCEDURE, PUBLIC  :: GET_WORK_GROUP_SIZE
      !PROCEDURE, PUBLIC  :: GET_COMPILE_WORK_GROUP_SIZE
      PROCEDURE, PUBLIC  :: GET_LOCAL_MEM_SIZE
      PROCEDURE, PUBLIC  :: GET_PREFERRED_WORK_GROUP_SIZE_MULTIPLE
      PROCEDURE, PUBLIC  :: GET_PRIVATE_MEM_SIZE
      !PROCEDURE, PUBLIC  :: GET_GLOBAL_WORK_SIZE
      PROCEDURE, PUBLIC  :: SET_ARG_BUF

      PROCEDURE, PRIVATE :: SET_ARG_PRV
      PROCEDURE, PRIVATE :: SET_ARG_I4
      PROCEDURE, PRIVATE :: SET_ARG_I8
      PROCEDURE, PRIVATE :: SET_ARG_R8

      !GENERIC, PUBLIC :: SET_ARG => SET_ARG_I4, SET_ARG_I8, SET_ARG_R8, SET_ARG_BUF
      !GENERIC, PUBLIC :: SET_ARG => SET_ARG_R8, SET_ARG_BUF

   END TYPE OCL_KERNEL_T

   CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
! Creates a new kernel object with \p name from \p program.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_KERNEL_CTR(SELF, PRGM, NAME)

   USE OCL_UTIL_M
   USE OCL_PROGRAM_M

   TYPE(OCL_KERNEL_T),  INTENT(OUT) :: SELF
   TYPE(OCL_PROGRAM_T), INTENT(IN)  :: PRGM
   CHARACTER*(*),       INTENT(IN)  :: NAME

   INTEGER :: LNAME
   INTEGER(C_INT32_T) :: CLRET
   TYPE(OCL_KERNEL_T) :: SELF0
   CHARACTER(LEN=:,KIND=C_CHAR), ALLOCATABLE :: NAME_C
!------------------------------------------------------------------------------

   LNAME = LEN_TRIM(NAME)
   ALLOCATE(CHARACTER(LEN=LNAME+1) :: NAME_C)
   NAME_C = NAME(1:LNAME) // C_NULL_CHAR

   CLRET = CL_SUCCESS
   SELF = SELF0
   SELF%m_kernel = clCreateKernel(PRGM%GET(),    &
                                  C_LOC(NAME_C), &
                                  CLRET)

   IF (ALLOCATED(NAME_C)) DEALLOCATE(NAME_C)
   OCL_KERNEL_CTR = CLRET
   RETURN
   END FUNCTION OCL_KERNEL_CTR

!************************************************************************
! Sommaire:
!
! Description:
!    ! Destroys the kernel object.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_KERNEL_DTR(SELF)

   TYPE(OCL_KERNEL_T), INTENT(INOUT) :: SELF

   integer(c_int32_t) :: CLRET
!---------------------------------------------------------------------

   ! ---  Relâche le kernel
   CLRET = CL_SUCCESS
   if (SELF%m_kernel .NE. 0) THEN
      CLRET = clReleaseKernel(SELF%m_kernel)
   ENDIF
   SELF%m_kernel = 0

   OCL_KERNEL_DTR = CLRET
   RETURN
   END FUNCTION OCL_KERNEL_DTR





!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL program.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF

   GET = SELF%M_KERNEL
   RETURN
   END FUNCTION GET

!************************************************************************
! Sommaire:
!
! Description:
   ! Returns a reference to the underlying OpenCL kernel object.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   CHARACTER(256) FUNCTION NAME(SELF)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   !!    return get_info<std::string>(CL_KERNEL_FUNCTION_NAME);
   NAME = "" !SELF%m_kernel

   RETURN
   END FUNCTION NAME

!************************************************************************
! Sommaire:
!
! Description:
!     Set an argument.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION SET_ARG_PRV(SELF, INDEX, SIZE, VALUE)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   INTEGER,             INTENT(IN) :: INDEX
   INTEGER(C_SIZE_T),   INTENT(IN) :: SIZE
   TYPE(C_PTR),         INTENT(IN) :: VALUE

   INTEGER(C_INT32_T) :: CLRET
   INTEGER(C_INT32_T) :: INDEX_C
!---------------------------------------------------------------------

   INDEX_C = INDEX - 1
   CLRET = clSetKernelArg(SELF%M_KERNEL, &
                          INDEX_C, &
                          SIZE, &
                          VALUE)

   SET_ARG_PRV = CLRET
   RETURN
   END FUNCTION SET_ARG_PRV

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION SET_ARG_I4(SELF, INDEX, VALUE)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   INTEGER,    INTENT(IN) :: INDEX
   INTEGER(4), INTENT(IN) :: VALUE

   INTEGER(C_SIZE_T) :: SIZE
!---------------------------------------------------------------------

   SIZE = SIZEOF(VALUE)
   SET_ARG_I4 = SET_ARG_PRV(SELF, INDEX, SIZE, C_LOC(VALUE))
   RETURN
   END FUNCTION SET_ARG_I4

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION SET_ARG_I8(SELF, INDEX, VALUE)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   INTEGER,    INTENT(IN) :: INDEX
   INTEGER(8), INTENT(IN) :: VALUE

   INTEGER(C_SIZE_T) :: SIZE
!---------------------------------------------------------------------

   SIZE = SIZEOF(VALUE)
   SET_ARG_I8 = SET_ARG_PRV(SELF, INDEX, SIZE, C_LOC(VALUE))
   RETURN
   END FUNCTION SET_ARG_I8

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION SET_ARG_R8(SELF, INDEX, VALUE)

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   INTEGER, INTENT(IN) :: INDEX
   REAL(8), INTENT(IN) :: VALUE

   INTEGER(C_SIZE_T) :: SIZE
!---------------------------------------------------------------------

   SIZE = SIZEOF(VALUE)
   SET_ARG_R8 = SET_ARG_PRV(SELF, INDEX, SIZE, C_LOC(VALUE))
   RETURN
   END FUNCTION SET_ARG_R8

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION SET_ARG_BUF(SELF, INDEX, VALUE)

   USE OCL_BUFFER_M, ONLY : OCL_BUFFER_T

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   INTEGER(C_INT32_T),  INTENT(IN) :: INDEX
   TYPE(OCL_BUFFER_T),  INTENT(IN) :: VALUE

   INTEGER(C_SIZE_T)   :: SIZE
   INTEGER(C_INTPTR_T) :: BUFF
!---------------------------------------------------------------------

   BUFF = VALUE%GET()
   SIZE = SIZEOF(BUFF)
   SET_ARG_BUF = SET_ARG_PRV(SELF, INDEX, SIZE, C_LOC(BUFF))
   RETURN
   END FUNCTION SET_ARG_BUF

   !!!template<class T>
   !!!void set_arg(size_t index, const T &value)
   !!!{
   !!!    detail::set_kernel_arg<T>()(*this, index, value);
   !!!}

   !!!! Returns the function name for the kernel.
   !!!std::string name() const
   !!!{
   !!!    return get_info<std::string>(CL_KERNEL_FUNCTION_NAME);
   !!!}
   !!!
   !!!! Returns the number of arguments for the kernel.
   !!!size_t arity() const
   !!!{
   !!!    return get_info<cl_uint>(CL_KERNEL_NUM_ARGS);
   !!!}
   !!!
   !!!! Returns the program for the kernel.
   !!!program get_program() const
   !!!{
   !!!    return program(get_info<cl_program>(CL_KERNEL_PROGRAM));
   !!!}
   !!!
   !!!! Returns the context for the kernel.
   !!!context get_context() const
   !!!{
   !!!    return context(get_info<cl_context>(CL_KERNEL_CONTEXT));
   !!!}
   !!!
   !!!! Returns information about the kernel.
   !!!!
   !!!! \see_opencl_ref{clGetKernelInfo}
   !!!template<class T>
   !!!T get_info(cl_kernel_info info) const
   !!!{
   !!!    return detail::get_object_info<T>(clGetKernelInfo, m_kernel, info);
   !!!}
   !!!

!************************************************************************
! Sommaire:
!
! Description:
!!!! Returns work-group information for the kernel with \p device.
!!!!
!!!! \see_opencl_ref{clGetKernelWorkGroupInfo}
!
! Entrée:
!
! Sortie:
!
! Notes:
!  Manquent
!      INTEGER(C_INT32_T), PRIVATE :: CL_KERNEL_COMPILE_WORK_GROUP_SIZE
!      INTEGER(C_INT32_T), PRIVATE :: CL_KERNEL_GLOBAL_WORK_SIZE
!************************************************************************
   INTEGER(C_SIZE_T) &
      FUNCTION GET_WORK_GROUP_SIZE(SELF, DEVC) &
      RESULT(VAL)

   USE OCL_DEVICE_M
   USE OCL_UTIL_M, ONLY : OCL_INFO_KERNEL_WG, GET_INFO

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   TYPE(OCL_DEVICE_T)  :: DEVC

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: OBJS(2)
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_KERNEL_WORK_GROUP_SIZE
!---------------------------------------------------------------------

   OBJS(1) = SELF%GET()
   OBJS(2) = DEVC%GET()
   CLRET = GET_INFO(OBJS, OCL_INFO_KERNEL_WG, INFO, VAL)
   RETURN
   END FUNCTION GET_WORK_GROUP_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!! Returns work-group information for the kernel with \p device.
!!!!
!!!! \see_opencl_ref{clGetKernelWorkGroupInfo}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_SIZE_T) &
      FUNCTION GET_LOCAL_MEM_SIZE(SELF, DEVC) &
      RESULT(VAL)

   USE OCL_DEVICE_M
   USE OCL_UTIL_M, ONLY : OCL_INFO_KERNEL_WG, GET_INFO

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   TYPE(OCL_DEVICE_T)  :: DEVC

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: OBJS(2)
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_KERNEL_LOCAL_MEM_SIZE
!---------------------------------------------------------------------

   OBJS(1) = SELF%GET()
   OBJS(2) = DEVC%GET()
   CLRET = GET_INFO(OBJS, OCL_INFO_KERNEL_WG, INFO, VAL)
   RETURN
   END FUNCTION GET_LOCAL_MEM_SIZE

!************************************************************************
! Sommaire:
!
! Description:
!!!! Returns work-group information for the kernel with \p device.
!!!!
!!!! \see_opencl_ref{clGetKernelWorkGroupInfo}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_SIZE_T) &
      FUNCTION GET_PREFERRED_WORK_GROUP_SIZE_MULTIPLE(SELF, DEVC) &
      RESULT(VAL)

   USE OCL_DEVICE_M
   USE OCL_UTIL_M, ONLY : OCL_INFO_KERNEL_WG, GET_INFO

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   TYPE(OCL_DEVICE_T)  :: DEVC

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: OBJS(2)
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE
!---------------------------------------------------------------------

   OBJS(1) = SELF%GET()
   OBJS(2) = DEVC%GET()
   CLRET = GET_INFO(OBJS, OCL_INFO_KERNEL_WG, INFO, VAL)
   RETURN
   END FUNCTION GET_PREFERRED_WORK_GROUP_SIZE_MULTIPLE

!************************************************************************
! Sommaire:
!
! Description:
!!!! Returns work-group information for the kernel with \p device.
!!!!
!!!! \see_opencl_ref{clGetKernelWorkGroupInfo}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_SIZE_T) &
      FUNCTION GET_PRIVATE_MEM_SIZE(SELF, DEVC) &
      RESULT(VAL)

   USE OCL_DEVICE_M
   USE OCL_UTIL_M, ONLY : OCL_INFO_KERNEL_WG, GET_INFO

   CLASS(OCL_KERNEL_T), INTENT(IN) :: SELF
   TYPE(OCL_DEVICE_T)  :: DEVC

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: OBJS(2)
   INTEGER(C_INT32_T), PARAMETER :: INFO = CL_KERNEL_PRIVATE_MEM_SIZE
!---------------------------------------------------------------------

   OBJS(1) = SELF%GET()
   OBJS(2) = DEVC%GET()
   CLRET = GET_INFO(OBJS, OCL_INFO_KERNEL_WG, INFO, VAL)
   RETURN
   END FUNCTION GET_PRIVATE_MEM_SIZE

   !!!! Sets the argument at \p index to \p value with \p size.
   !!!!
   !!!! \see_opencl_ref{clSetKernelArg}
   !!!void set_arg(size_t index, size_t size, const void *value)
   !!!{
   !!!    BOOST_ASSERT(index < arity());
   !!!
   !!!    cl_int ret = clSetKernelArg(m_kernel,
   !!!                                static_cast<cl_uint>(index),
   !!!                                size,
   !!!                                value);
   !!!    if(ret != CL_SUCCESS){
   !!!        BOOST_THROW_EXCEPTION(opencl_error(ret));
   !!!    }
   !!!}
   !!!
   !!!! Sets the argument at \p index to \p value.
   !!!!
   !!!! For built-in types (e.g. \c float, \c int4_), this is equivalent to
   !!!! calling set_arg(index, sizeof(type), &value).
   !!!!
   !!!! Additionally, this method is specialized for device memory objects
   !!!! such as buffer and image2d. This allows for them to be passed directly
   !!!! without having to extract their underlying cl_mem object.
   !!!!
   !!!! This method is also specialized for device container types such as
   !!!! vector<T> and array<T, N>. This allows for them to be passed directly
   !!!! as kernel arguments without having to extract their underlying buffer.
   !!!!
   !!!! For setting local memory arguments (e.g. "__local float *buf"), the
   !!!! local_buffer<T> class may be used:
   !!!! \code
   !!!! ! set argument to a local buffer with storage for 32 float's
   !!!! kernel.set_arg(0, local_buffer<float>(32));
   !!!! \endcode
   !!!template<class T>
   !!!void set_arg(size_t index, const T &value)
   !!!{
   !!!    detail::set_kernel_arg<T>()(*this, index, value);
   !!!}
   !!!
   !!!! \internal_
   !!!void set_arg(size_t index, const cl_mem mem)
   !!!{
   !!!    set_arg(index, sizeof(cl_mem), static_cast<const void *>(&mem));
   !!!}
   !!!
   !!!! \internal_
   !!!void set_arg(size_t index, const cl_sampler sampler)
   !!!{
   !!!    set_arg(index, sizeof(cl_sampler), static_cast<const void *>(&sampler));
   !!!}
   !!!
   !!!! \internal_
   !!!template<class T>
   !!!void set_arg(size_t index, const svm_ptr<T> ptr)
   !!!{
   !!!    #ifdef CL_VERSION_2_0
   !!!    cl_int ret = clSetKernelArgSVMPointer(m_kernel, index, ptr.get());
   !!!    if(ret != CL_SUCCESS){
   !!!        BOOST_THROW_EXCEPTION(opencl_error(ret));
   !!!    }
   !!!    #else
   !!!    BOOST_THROW_EXCEPTION(opencl_error(CL_INVALID_ARG_VALUE));
   !!!    #endif
   !!!}
   !!!
   !!!#if defined(CL_VERSION_2_0) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
   !!!! Sets additional execution information for the kernel.
   !!!!
   !!!! \opencl_version_warning{2,0}
   !!!!
   !!!! \see_opencl2_ref{clSetKernelExecInfo}
   !!!void set_exec_info(cl_kernel_exec_info info, size_t size, const void *value)
   !!!{
   !!!    cl_int ret = clSetKernelExecInfo(m_kernel, info, size, value);
   !!!    if(ret != CL_SUCCESS){
   !!!        BOOST_THROW_EXCEPTION(opencl_error(ret));
   !!!    }
   !!!}
   !!!#endif ! CL_VERSION_2_0
   !!!
   !!!! \internal_
   !!!operator cl_kernel() const
   !!!{
   !!!    return m_kernel;
   !!!}
   !!!

END MODULE OCL_KERNEL_M
