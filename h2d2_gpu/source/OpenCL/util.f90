!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class system
! \brief Provides access to platforms and devices on the system.
!
! The system class contains a set of static functions which provide access to
! the OpenCL platforms and compute devices on the host system.
!
! The default_device() convenience method automatically selects and returns
! the "best" compute device for the system following a set of heuristics and
! environment variables. This simplifies setup of the OpenCL environment.
!
! \see platform, device, context

      MODULE OCL_UTIL_M

      USE clfortran

      IMPLICIT NONE

      INTEGER, PARAMETER :: OCL_MEMORY_ALLOCATION       = 1
      INTEGER, PARAMETER :: OCL_INVALID_PLATFORM_COUNT  = 2
      INTEGER, PARAMETER :: OCL_INVALID_PLATFORM_INDEX  = 3
      INTEGER, PARAMETER :: OCL_INVALID_DEVICE_COUNT    = 4
      INTEGER, PARAMETER :: OCL_INVALID_DEVICE_INDEX    = 5

      INTEGER, PARAMETER :: OCL_INFO_PLATFORM   = 1
      INTEGER, PARAMETER :: OCL_INFO_DEVICE     = 2
      INTEGER, PARAMETER :: OCL_INFO_MEMOBJECT  = 3
      INTEGER, PARAMETER :: OCL_INFO_KERNEL     = 4
      INTEGER, PARAMETER :: OCL_INFO_KERNEL_WG  = 5

      INTERFACE GET_INFO
         MODULE PROCEDURE GET_INFO_I32, GET_INFO_I32S, &
                          GET_INFO_I64, GET_INFO_I64S, &
                          GET_INFO_STR
      END INTERFACE

      CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
      INTEGER(C_INT32_T) FUNCTION GET_INFO_I32S(OBJ, ITYP, INFO, VAL)

      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ
      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
      INTEGER(C_INT32_T), TARGET, INTENT(OUT):: VAL

      INTEGER(C_INTPTR_T) :: OBJS(1)
!---------------------------------------------------------------------

      OBJS(1) = OBJ
      GET_INFO_I32S = GET_INFO_I32(OBJS, ITYP, INFO, VAL)
      RETURN
      END FUNCTION GET_INFO_I32S

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
      INTEGER(C_INT32_T) FUNCTION GET_INFO_I32(OBJ, ITYP, INFO, VAL)

      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ(:)
      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
      INTEGER(C_INT32_T), TARGET, INTENT(OUT):: VAL

      INTEGER(C_INT32_T) :: CLRET
      INTEGER(C_SIZE_T)  :: SZE, LRET
!---------------------------------------------------------------------

      VAL = -1
      SZE = SIZEOF(VAL)
      SELECT CASE (ITYP)
         CASE (OCL_INFO_PLATFORM)
            CLRET = clGetPlatformInfo (OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_DEVICE)
            CLRET = clGetDeviceInfo   (OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_MEMOBJECT)
            CLRET = clGetMemObjectInfo(OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_KERNEL)
            CLRET = clGetKernelInfo   (OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_KERNEL_WG)
            CLRET = clGetKernelWorkGroupInfo(OBJ(1), OBJ(2), INFO, SZE, C_LOC(VAL), LRET)
         CASE DEFAULT
            CLRET = CL_INVALID_VALUE
      END SELECT

      GET_INFO_I32 = CLRET
      RETURN
      END FUNCTION GET_INFO_I32

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
      INTEGER(C_INT32_T) FUNCTION GET_INFO_I64S(OBJ, ITYP, INFO, VAL)

      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ
      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
      INTEGER(C_INT64_T), TARGET, INTENT(OUT):: VAL

      INTEGER(C_INTPTR_T) :: OBJS(1)
!---------------------------------------------------------------------

      OBJS(1) = OBJ
      GET_INFO_I64S = GET_INFO_I64(OBJS, ITYP, INFO, VAL)
      RETURN
      END FUNCTION GET_INFO_I64S

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
      INTEGER(C_INT32_T) FUNCTION GET_INFO_I64(OBJ, ITYP, INFO, VAL)

      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ(:)
      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
      INTEGER(C_INT64_T), TARGET, INTENT(OUT):: VAL

      INTEGER(C_INT32_T) :: CLRET
      INTEGER(C_SIZE_T)  :: SZE, LRET
!---------------------------------------------------------------------

      VAL = -1
      SZE = SIZEOF(VAL)
      SELECT CASE (ITYP)
         CASE (OCL_INFO_PLATFORM)
            CLRET = clGetPlatformInfo(OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_DEVICE)
            CLRET = clGetDeviceInfo  (OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_MEMOBJECT)
            CLRET = clGetMemObjectInfo(OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_KERNEL)
            CLRET = clGetKernelInfo   (OBJ(1), INFO, SZE, C_LOC(VAL), LRET)
         CASE (OCL_INFO_KERNEL_WG)
            CLRET = clGetKernelWorkGroupInfo(OBJ(1), OBJ(2), INFO, SZE, C_LOC(VAL), LRET)
         CASE DEFAULT
            CLRET = CL_INVALID_VALUE
      END SELECT

      GET_INFO_I64 = CLRET
      RETURN
      END FUNCTION GET_INFO_I64

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!      Redondante avec I32 et I64
!!!C************************************************************************
!!!      INTEGER(C_INT32_T) FUNCTION GET_INFO_ISZ(OBJ, ITYP, INFO, VAL)
!!!
!!!      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ
!!!      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
!!!      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
!!!      INTEGER(C_SIZE_T),   INTENT(OUT):: VAL
!!!
!!!      INTEGER(C_INT32_T) :: CLRET
!!!      INTEGER(C_SIZE_T)  :: SZE, LRET
!!!C---------------------------------------------------------------------
!!!
!!!      VAL = -1
!!!      SZE = SIZEOF(VAL)
!!!      SELECT CASE (ITYP)
!!!         CASE (OCL_INFO_PLATFORM)
!!!            CLRET = clGetPlatformInfo(OBJ, INFO, SZE, C_LOC(VAL), LRET)
!!!         CASE (OCL_INFO_DEVICE)
!!!            CLRET = clGetDeviceInfo  (OBJ, INFO, SZE, C_LOC(VAL), LRET)
!!!         CASE DEFAULT
!!!            CLRET = CL_INVALID_VALUE
!!!      END SELECT
!!!
!!!      GET_INFO_ISZ = CLRET
!!!      RETURN
!!!      END FUNCTION GET_INFO_ISZ

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!     Écriture inhabituelle pour des chaînes FTN à cause du passage au C.
!************************************************************************
      INTEGER(C_INT32_T) FUNCTION GET_INFO_STR(OBJ, ITYP, INFO, VAL)

      INTEGER(C_INTPTR_T), INTENT(IN) :: OBJ
      INTEGER(C_INT32_T),  INTENT(IN) :: ITYP
      INTEGER(C_INT32_T),  INTENT(IN) :: INFO
      CHARACTER*(*), TARGET, INTENT(OUT):: VAL

      INTEGER(C_INT32_T) :: CLRET
      INTEGER(C_SIZE_T)  :: SZE, LRET
      CHARACTER(KIND=C_CHAR), POINTER:: PVAL
!---------------------------------------------------------------------

      PVAL => VAL

      VAL = ' '
      SZE = LEN(VAL)
      LRET = -1
      SELECT CASE (ITYP)
         CASE (OCL_INFO_PLATFORM)
            CLRET = clGetPlatformInfo(OBJ, INFO,   0, C_NULL_PTR, LRET)
            IF (CLRET.NE. CL_SUCCESS) GOTO 119
            IF (LRET .LE.   0) GOTO 119
            IF (LRET .GE. SZE) GOTO 119
            CLRET = clGetPlatformInfo(OBJ, INFO, SZE, C_LOC(PVAL), LRET)
119         CONTINUE
         CASE (OCL_INFO_DEVICE)
            CLRET = clGetDeviceInfo  (OBJ, INFO,   0, C_NULL_PTR, LRET)
            IF (CLRET.NE. CL_SUCCESS) GOTO 129
            IF (LRET .LE.   0) GOTO 129
            IF (LRET .GE. SZE) GOTO 129
            CLRET = clGetDeviceInfo(OBJ, INFO, SZE, C_LOC(PVAL), LRET)
129         CONTINUE
         CASE DEFAULT
            CLRET = CL_INVALID_VALUE
      END SELECT
      IF (LRET .GE. SZE) CLRET = CL_INVALID_ARG_SIZE
      IF (CLRET .EQ. CL_SUCCESS)  THEN
         IF (LRET .GT. 0) VAL(LRET:SZE) = ' '
         VAL = ADJUSTL(VAL)
      ENDIF

      GET_INFO_STR = CLRET
      RETURN
      END FUNCTION GET_INFO_STR



!!!class system
!!!{
!!!public:
!!!    ! Returns the default compute device for the system.
!!!    !
!!!    ! The default device is selected based on a set of heuristics and can be
!!!    ! influenced using one of the following environment variables:
!!!    !
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_DEVICE -
!!!    !        name of the compute device (e.g. "GTX TITAN")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_DEVICE_TYPE
!!!    !        type of the compute device (e.g. "GPU" or "CPU")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_PLATFORM -
!!!    !        name of the platform (e.g. "NVIDIA CUDA")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_VENDOR -
!!!    !        name of the device vendor (e.g. "NVIDIA")
!!!    !
!!!    ! The default device is determined once on the first time this function
!!!    ! is called. Calling this function multiple times will always result in
!!!    ! the same device being returned.
!!!    !
!!!    ! If no OpenCL device is found on the system, a no_device_found exception
!!!    ! is thrown.
!!!    !
!!!    ! For example, to print the name of the default compute device on the
!!!    ! system:
!!!    ! \code
!!!    ! ! get the default compute device
!!!    ! boost::compute::device device = boost::compute::system::default_device();
!!!    !
!!!    ! ! print the name of the device
!!!    ! std::cout << "default device: " << device.name() << std::endl;
!!!    ! \endcode
!!!    static device default_device()
!!!    {
!!!        static device default_device = find_default_device();
!!!
!!!        return default_device;
!!!    }
!!!
!!!    ! Returns the device with \p name.
!!!    !
!!!    ! \throws no_device_found if no device with \p name is found.
!!!    static device find_device(const std::string &name)
!!!    {
!!!        const std::vector<device> devices = system::devices();
!!!        for(size_t i = 0; i < devices.size(); i++){
!!!            const device& device = devices[i];
!!!
!!!            if(device.name() == name){
!!!                return device;
!!!            }
!!!        }
!!!
!!!        BOOST_THROW_EXCEPTION(no_device_found());
!!!    }
!!!
!!!    ! Returns a vector containing all of the compute devices on
!!!    ! the system.
!!!    !
!!!    ! For example, to print out the name of each OpenCL-capable device
!!!    ! available on the system:
!!!    ! \code
!!!    ! for(const auto &device : boost::compute::system::devices()){
!!!    !     std::cout << device.name() << std::endl;
!!!    ! }
!!!    ! \endcode
!!!    static std::vector<device> devices()
!!!    {
!!!        std::vector<device> devices;
!!!
!!!        const std::vector<platform> platforms = system::platforms();
!!!        for(size_t i = 0; i < platforms.size(); i++){
!!!            const std::vector<device> platform_devices = platforms[i].devices();
!!!
!!!            devices.insert(
!!!                devices.end(), platform_devices.begin(), platform_devices.end()
!!!            );
!!!        }
!!!
!!!        return devices;
!!!    }
!!!
!!!    ! Returns the number of compute devices on the system.
!!!    static size_t device_count()
!!!    {
!!!        size_t count = 0;
!!!
!!!        const std::vector<platform> platforms = system::platforms();
!!!        for(size_t i = 0; i < platforms.size(); i++){
!!!            count += platforms[i].device_count();
!!!        }
!!!
!!!        return count;
!!!    }
!!!
!!!    ! Returns the default context for the system.
!!!    !
!!!    ! The default context is created for the default device on the system
!!!    ! (as returned by default_device()).
!!!    !
!!!    ! The default context is created once on the first time this function is
!!!    ! called. Calling this function multiple times will always result in the
!!!    ! same context object being returned.
!!!    static context default_context()
!!!    {
!!!        static context default_context(default_device());
!!!
!!!        return default_context;
!!!    }
!!!
!!!    ! Returns the default command queue for the system.
!!!    static command_queue& default_queue()
!!!    {
!!!        static command_queue queue(default_context(), default_device());
!!!
!!!        return queue;
!!!    }
!!!
!!!    ! Blocks until all outstanding computations on the default
!!!    ! command queue are complete.
!!!    !
!!!    ! This is equivalent to:
!!!    ! \code
!!!    ! system::default_queue().finish();
!!!    ! \endcode
!!!    static void finish()
!!!    {
!!!        default_queue().finish();
!!!    }

!!!
!!!private:
!!!    ! \internal_
!!!    static device find_default_device()
!!!    {
!!!        ! get a list of all devices on the system
!!!        const std::vector<device> devices_ = devices();
!!!        if(devices_.empty()){
!!!            BOOST_THROW_EXCEPTION(no_device_found());
!!!        }
!!!
!!!        ! check for device from environment variable
!!!        const char *name     = detail::getenv("BOOST_COMPUTE_DEFAULT_DEVICE");
!!!        const char *type     = detail::getenv("BOOST_COMPUTE_DEFAULT_DEVICE_TYPE");
!!!        const char *platform = detail::getenv("BOOST_COMPUTE_DEFAULT_PLATFORM");
!!!        const char *vendor   = detail::getenv("BOOST_COMPUTE_DEFAULT_VENDOR");
!!!
!!!        if(name || type || platform || vendor){
!!!            for(size_t i = 0; i < devices_.size(); i++){
!!!                const device& device = devices_[i];
!!!                if (name && !matches(device.name(), name))
!!!                    continue;
!!!
!!!                if (type && matches(std::string("GPU"), type))
!!!                    if (!(device.type() & device::gpu))
!!!                        continue;
!!!
!!!                if (type && matches(std::string("CPU"), type))
!!!                    if (!(device.type() & device::cpu))
!!!                        continue;
!!!
!!!                if (platform && !matches(device.platform().name(), platform))
!!!                    continue;
!!!
!!!                if (vendor && !matches(device.vendor(), vendor))
!!!                    continue;
!!!
!!!                return device;
!!!            }
!!!        }
!!!
!!!        ! find the first gpu device
!!!        for(size_t i = 0; i < devices_.size(); i++){
!!!            const device& device = devices_[i];
!!!
!!!            if(device.type() & device::gpu){
!!!                return device;
!!!            }
!!!        }
!!!
!!!        ! find the first cpu device
!!!        for(size_t i = 0; i < devices_.size(); i++){
!!!            const device& device = devices_[i];
!!!
!!!            if(device.type() & device::cpu){
!!!                return device;
!!!            }
!!!        }
!!!
!!!        ! return the first device found
!!!        return devices_[0];
!!!    }
!!!
!!!    ! \internal_
!!!    static bool matches(const std::string &str, const std::string &pattern)
!!!    {
!!!        return str.find(pattern) != std::string::npos;
!!!    }
!!!};
!!!
!!!} ! end compute namespace
!!!} ! end boost namespace
!!!
!!!#endif ! BOOST_COMPUTE_SYSTEM_HPP

      END MODULE OCL_UTIL_M
