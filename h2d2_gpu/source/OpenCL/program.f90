!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!


! \class program
! \brief A compute program.
!
! The program class represents an OpenCL program.
!
! Program objects are created with one of the static \c create_with_*
! functions. For example, to create a program from a source string:
!
! \snippet test/test_program.cpp create_with_source
!
! And to create a program from a source file:
! \code
! boost::compute::program bar_program =
!     boost::compute::program::create_with_source_file("/path/to/bar.cl", context);
! \endcode
!
! Once a program object has been successfully created, it can be compiled
! using the \c build() method:
! \code
! ! build the program
! foo_program.build();
! \endcode
!
! Once the program is built, \ref kernel objects can be created using the
! \c create_kernel() method by passing their name:
! \code
! ! create a kernel from the compiled program
! boost::compute::kernel foo_kernel = foo_program.create_kernel("foo");
! \endcode
!
MODULE OCL_PROGRAM_M

   USE clfortran
   USE ISO_C_BINDING

   IMPLICIT NONE

   TYPE :: OCL_PROGRAM_T
      INTEGER(C_INTPTR_T), PRIVATE :: M_PROGRAM = 0

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: BUILD
      PROCEDURE :: COMPILE
!         PROCEDURE :: LINK
   END TYPE OCL_PROGRAM_T

CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_CTR(SELF)

   USE OCL_UTIL_M

   TYPE(OCL_PROGRAM_T), INTENT(OUT) :: SELF

   INTEGER :: IRET
   INTEGER(C_INT32_T) :: CLRET
   TYPE(OCL_PROGRAM_T):: SELF0
!---------------------------------------------------------------------

   SELF = SELF0

   OCL_PRGM_CTR = CL_SUCCESS
   RETURN
   END FUNCTION OCL_PRGM_CTR

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_DTR(SELF)

   TYPE(OCL_PROGRAM_T), INTENT(INOUT) :: SELF
!---------------------------------------------------------------------

   OCL_PRGM_DTR = CL_SUCCESS
   RETURN
   END FUNCTION OCL_PRGM_DTR

!************************************************************************
! Sommaire:
!
! Description:
!!    ! Creates a new program with \p source in \p context.
!!    !
!!    ! \see_opencl_ref{clCreateProgramWithSource}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_CTR_WITH_STRING(SELF, CTX, SRC)

   USE OCL_CONTEXT_M

   TYPE(OCL_PROGRAM_T), INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTX
   CHARACTER*(*),       INTENT(IN)  :: SRC

   INTEGER(C_INTPTR_T) :: PGM
   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_SIZE_T), DIMENSION(:) :: SRCS_L(1)
   TYPE(C_PTR),       DIMENSION(:) :: SRCS_C(1)
!---------------------------------------------------------------------

   CLRET = CL_SUCCESS
   SRCS_L(1) = LEN_TRIM(SRC)
   SRCS_C(1) = C_LOC(SRC)
   PGM = clCreateProgramWithSource(CTX%GET(), &
                                   1, &
                                   C_LOC(SRCS_C), &
                                   C_LOC(SRCS_L), &
                                   CLRET)

   IF (PGM .NE. 0) THEN
      CLRET = OCL_PRGM_CTR(SELF)
      SELF%M_PROGRAM = PGM
   ENDIF

   OCL_PRGM_CTR_WITH_STRING = CLRET
   RETURN
   END FUNCTION OCL_PRGM_CTR_WITH_STRING

!************************************************************************
! Sommaire:
!
! Description:
!!    ! Creates a new program with \p source in \p context.
!!    !
!!    ! \see_opencl_ref{clCreateProgramWithSource}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_CTR_WITH_FILE(SELF, CTX, FIC)

   USE OCL_CONTEXT_M

   TYPE(OCL_PROGRAM_T), INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTX
   CHARACTER*(*),       INTENT(IN)  :: FIC

   INTEGER, PARAMETER :: IUNIT = 55

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER :: LBUF
   CHARACTER(LEN=:), POINTER :: BUF
   LOGICAL :: L_EXISTS
   INTEGER :: IT
!---------------------------------------------------------------------

   CLRET = CL_SUCCESS

   ! ---  Taille du fichier
   LBUF = -1
   INQUIRE(FILE  = FIC(1:LEN_TRIM(FIC)), &
           EXIST = L_EXISTS, &
           SIZE  = LBUF, &
           ERR   =  9900)
   IF (.NOT. L_EXISTS) CLRET = -1
   IF (LBUF .LE. 0)    CLRET = -1
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999

   ! ---  Alloue le buffer
   ALLOCATE(CHARACTER(LEN=LBUF) :: BUF)

   ! ---  Ouvre le fichier
   OPEN(UNIT   = IUNIT, &
        FILE   =  FIC(1:LEN_TRIM(FIC)), &
        ACCESS = 'STREAM', &
        FORM   = 'UNFORMATTED',  &
        STATUS = 'OLD')

   ! ---  Lis le programme
   READ(IUNIT, ERR=9900) BUF(1:LBUF)

   ! ---  Crée le programme
   CLRET = OCL_PRGM_CTR_WITH_STRING(SELF, CTX, BUF)

   GOTO 9999
!------------------------------------------------------------------------------
9900  CLRET = -1
   GOTO 9999

9999  CONTINUE
   !IF (ALLOCATED(BUF)) DEALLOCATE(BUF)
   CLOSE(IUNIT)
   OCL_PRGM_CTR_WITH_FILE = CLRET
   RETURN
   END FUNCTION OCL_PRGM_CTR_WITH_FILE

!************************************************************************
! Sommaire:
!
! Description:
!!    ! Creates a new program with \p source in \p context.
!!    !
!!    ! \see_opencl_ref{clCreateProgramWithSource}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_CTR_WITH_BINARY(SELF, CTX, SRC)

   USE OCL_CONTEXT_M

   TYPE(OCL_PROGRAM_T), INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTX
   CHARACTER*(*),       INTENT(IN)  :: SRC

   TYPE(OCL_DEVICE_T)  :: DEVC
   INTEGER(C_INTPTR_T) :: PGM
   INTEGER(C_INTPTR_T) :: DEV_ID
   INTEGER(C_INT32_T)  :: BSTTS
   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_SIZE_T), DIMENSION(:) :: SRCS_L(1)
   TYPE(C_PTR),       DIMENSION(:) :: SRCS_C(1)
!---------------------------------------------------------------------

   BSTTS = 0
   CLRET = CL_SUCCESS

   DEVC = CTX%GET_DEVICE()
   DEV_ID = DEVC%GET()
   SRCS_L(1) = LEN_TRIM(SRC)
   SRCS_C(1) = C_LOC(SRC)
   PGM = clCreateProgramWithBinary(CTX%GET(), &
                                   1, &
                                   C_LOC(DEV_ID), &
                                   C_LOC(SRCS_L), &
                                   C_LOC(SRCS_C), &     !
                                   C_LOC(BSTTS),  &   ! binary status
                                   CLRET)

   IF (PGM .NE. 0) THEN
      CLRET = OCL_PRGM_CTR(SELF)
      SELF%M_PROGRAM = PGM
   ENDIF

   OCL_PRGM_CTR_WITH_BINARY = CLRET
   RETURN
   END FUNCTION OCL_PRGM_CTR_WITH_BINARY

!************************************************************************
! Sommaire:
!
! Description:
!!    ! Creates a new program with \p source in \p context.
!!    !
!!    ! \see_opencl_ref{clCreateProgramWithSource}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_PRGM_CTR_WITH_BINARY_FILE(SELF, CTX, FIC)

   USE OCL_CONTEXT_M

   TYPE(OCL_PROGRAM_T), INTENT(OUT) :: SELF
   TYPE(OCL_CONTEXT_T), INTENT(IN)  :: CTX
   CHARACTER*(*),       INTENT(IN)  :: FIC

   INTEGER, PARAMETER :: IUNIT = 55

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER :: LBUF
   CHARACTER(LEN=:), ALLOCATABLE :: BUF
   LOGICAL :: L_EXISTS
!---------------------------------------------------------------------

   CLRET = CL_SUCCESS

   ! ---  Taille du fichier
   LBUF = -1
   INQUIRE(FILE  = FIC(1:LEN_TRIM(FIC)), &
           EXIST = L_EXISTS, &
           SIZE  = LBUF, &
           ERR   =  9900)
   IF (.NOT. L_EXISTS) CLRET = -1
   IF (LBUF .LE. 0)    CLRET = -1
   IF (CLRET .NE. CL_SUCCESS) GOTO 9999

   ! ---  Alloue le buffer
   ALLOCATE(CHARACTER(LEN=LBUF) :: BUF)

   ! ---  Ouvre le fichier
   OPEN(UNIT   = IUNIT, &
        FILE   =  FIC(1:LEN_TRIM(FIC)), &
        ACCESS = 'STREAM', &
        FORM   = 'UNFORMATTED',  &
        STATUS = 'OLD')

   ! ---  Lis le programme
   READ(IUNIT, ERR=9900) BUF

   CLRET = OCL_PRGM_CTR_WITH_BINARY(SELF, CTX, BUF)

   GOTO 9999
!------------------------------------------------------------------------------
9900  CLRET = -1
   GOTO 9999

9999  CONTINUE
   IF (ALLOCATED(BUF)) DEALLOCATE(BUF)
   CLOSE(IUNIT)
   OCL_PRGM_CTR_WITH_BINARY_FILE = CLRET
   RETURN
   END FUNCTION OCL_PRGM_CTR_WITH_BINARY_FILE

!************************************************************************
! Sommaire:
!
! Description:
!    ! Returns the underlying OpenCL program.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_PROGRAM_T), INTENT(IN) :: SELF

   GET = SELF%M_PROGRAM
   RETURN
   END FUNCTION GET

   !!    ! Returns the source code for the program.
!!    std::string source() const
!!    {
!!        return get_infoS(CL_PROGRAM_SOURCE);
!!    }
!!
!!    ! Returns the binary for the program.
!!    std::vector<unsigned char> binary() const
!!    {
!!        size_t binary_size = get_info<size_t>(CL_PROGRAM_BINARY_SIZES);
!!        std::vector<unsigned char> binary(binary_size);
!!
!!        unsigned char *binary_ptr = &binary[0];
!!        cl_int error = clGetProgramInfo(m_program,
!!                                        CL_PROGRAM_BINARIES,
!!                                        sizeof(unsigned char **),
!!                                        &binary_ptr,
!!                                        0);
!!        if(error != CL_SUCCESS){
!!            BOOST_THROW_EXCEPTION(opencl_error(error));
!!        }
!!
!!        return binary;
!!    }

!!    std::vector<device> get_devices() const
!!    {
!!        std::vector<cl_device_id> device_ids =
!!            get_info<std::vector<cl_device_id> >(CL_PROGRAM_DEVICES);
!!
!!        std::vector<device> devices;
!!        for(size_t i = 0; i < device_ids.size(); i++){
!!            devices.push_back(device(device_ids[i]));
!!        }
!!
!!        return devices;
!!    }

!!    ! Returns the context for the program.
!!    context get_context() const
!!    {
!!        return context(get_info<cl_context>(CL_PROGRAM_CONTEXT));
!!    }

!!    ! Returns information about the program.
!!    !
!!    ! \see_opencl_ref{clGetProgramInfo}
!!    template<class T>
!!    T get_info(cl_program_info info) const
!!    {
!!        return detail::get_object_info<T>(clGetProgramInfo, m_program, info);
!!    }


!************************************************************************
! Sommaire:
!
! Description:
!!    ! Builds the program with \p options.
!!    !
!!    ! If the program fails to compile, this function will set an error
!!
!!    ! \see_opencl_ref{clBuildProgram}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION BUILD(SELF, OPTS)

   CLASS(OCL_PROGRAM_T), INTENT(IN) :: SELF
   CHARACTER*(*),        INTENT(IN) :: OPTS

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER OPTS_L
   CHARACTER(LEN=:,KIND=C_CHAR), ALLOCATABLE :: OPTS_C
!------------------------------------------------------------------------------

   OPTS_L = LEN_TRIM(OPTS)
   ALLOCATE(CHARACTER(LEN=OPTS_L+1) :: OPTS_C)
   OPTS_C = OPTS(1:OPTS_L) // C_NULL_CHAR

   CLRET = clBuildProgram(self%m_program, &
                          0,              &  ! num_devices
                          C_NULL_PTR,     &  ! device_list
                          C_LOC(OPTS_C),  &  ! options
                          C_NULL_FUNPTR,  &  ! pfn notify
                          C_NULL_PTR)        ! user data

   IF (ALLOCATED(OPTS_C)) DEALLOCATE(OPTS_C)
   BUILD = CLRET
   RETURN
   END FUNCTION BUILD


!************************************************************************
! Sommaire:
!
! Description:
!!    ! Compiles the program with \p options.
!!    !
!!    ! \see_opencl_ref{clCompileProgram}
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION COMPILE(SELF, OPTS)

   CLASS(OCL_PROGRAM_T), INTENT(IN) :: SELF
   CHARACTER*(*),        INTENT(IN) :: OPTS

   INTEGER(C_INT32_T)  :: CLRET
   INTEGER OPTS_L
   CHARACTER(LEN=:,KIND=C_CHAR), ALLOCATABLE :: OPTS_C
!------------------------------------------------------------------------------

   CALL LOG_TODO('OCL_PROGRAM%COMPILE UNTESTED')
   CALL ERR_ASR(.FALSE.)

   OPTS_L = LEN_TRIM(OPTS)
   ALLOCATE(CHARACTER(LEN=OPTS_L+1) :: OPTS_C)
   OPTS_C = OPTS(1:OPTS_L) // C_NULL_CHAR

   CLRET = clCompileProgram(SELF%m_program, &
                            0, &
                            C_NULL_PTR,    &
                            C_LOC(OPTS_C), &
                            0,             &
                            C_NULL_PTR,    &
                            C_NULL_PTR,    &
                            C_NULL_FUNPTR, &
                            C_NULL_PTR)

   IF (ALLOCATED(OPTS_C)) DEALLOCATE(OPTS_C)
   COMPILE = CLRET
   RETURN
   END FUNCTION COMPILE

!!
!!    ! Returns the build log.
!!    std::string build_log() const
!!    {
!!        return get_build_info<std::string>(CL_PROGRAM_BUILD_LOG, get_devices().front());
!!    }
!!

!!    #if defined(CL_VERSION_1_2) || defined(BOOST_COMPUTE_DOXYGEN_INVOKED)
!!    ! Creates a new program with the built-in kernels listed in
!!    ! \p kernel_names for \p devices in \p context.
!!    !
!!    ! \opencl_version_warning{1,2}
!!    !
!!    ! \see_opencl_ref{clCreateProgramWithBuiltInKernels}
!!    static program create_with_builtin_kernels(const context &context,
!!                                               const std::vector<device> &devices,
!!                                               const std::string &kernel_names)
!!    {
!!        cl_int error = 0;
!!
!!        cl_program program_ = clCreateProgramWithBuiltInKernels(
!!            context.get(),
!!            static_cast<uint_>(devices.size()),
!!            reinterpret_cast<const cl_device_id *>(&devices[0]),
!!            kernel_names.c_str(),
!!            &error
!!        );
!!
!!        if(!program_){
!!            BOOST_THROW_EXCEPTION(opencl_error(error));
!!        }
!!
!!        return program(program_, false);
!!    }
!!    #endif ! CL_VERSION_1_2
!!
!!    ! Create a new program with \p source in \p context and builds it with \p options.
!!    /**
!!     * In case BOOST_COMPUTE_USE_OFFLINE_CACHE macro is defined,
!!     * the compiled binary is stored for reuse in the offline cache located in
!!     * $HOME/.boost_compute on UNIX-like systems and in %APPDATA%/boost_compute
!!     * on Windows.
!!     */
!!    static program build_with_source(
!!            const std::string &source,
!!            const context     &context,
!!            const std::string &options = std::string()
!!            )
!!    {
!!#ifdef BOOST_COMPUTE_USE_OFFLINE_CACHE
!!        ! Get hash string for the kernel.
!!        device   d = context.get_device();
!!        platform p = d.platform();
!!
!!        detail::sha1 hash;
!!        hash.process( p.name()    )
!!            .process( p.version() )
!!            .process( d.name()    )
!!            .process( options     )
!!            .process( source      )
!!            ;
!!
!!        ! Try to get cached program binaries:
!!        try {
!!            boost::optional<program> prog = load_program_binary(hash, context);
!!
!!            if (prog) {
!!                prog->build(options);
!!                return *prog;
!!            }
!!        } catch (...) {
!!            ! Something bad happened. Fallback to normal compilation.
!!        }
!!
!!        ! Cache is apparently not available. Just compile the sources.
!!#endif
!!        const char *source_string = source.c_str();
!!
!!        cl_int error = 0;
!!        cl_program program_ = clCreateProgramWithSource(context,
!!                                                        uint_(1),
!!                                                        &source_string,
!!                                                        0,
!!                                                        &error);
!!        if(!program_){
!!            BOOST_THROW_EXCEPTION(opencl_error(error));
!!        }
!!
!!        program prog(program_, false);
!!        prog.build(options);
!!
!!#ifdef BOOST_COMPUTE_USE_OFFLINE_CACHE
!!        ! Save program binaries for future reuse.
!!        save_program_binary(hash, prog);
!!#endif
!!
!!        return prog;
!!    }
!!
!!private:
!!#ifdef BOOST_COMPUTE_USE_OFFLINE_CACHE
!!    ! Saves program binaries for future reuse.
!!    static void save_program_binary(const std::string &hash, const program &prog)
!!    {
!!        std::string fname = detail::program_binary_path(hash, true) + "kernel";
!!        std::ofstream bfile(fname.c_str(), std::ios::binary);
!!        if (!bfile) return;
!!
!!        std::vector<unsigned char> binary = prog.binary();
!!
!!        size_t binary_size = binary.size();
!!        bfile.write((char*)&binary_size, sizeof(size_t));
!!        bfile.write((char*)binary.data(), binary_size);
!!    }
!!
!!    ! Tries to read program binaries from file cache.
!!    static boost::optional<program> load_program_binary(
!!            const std::string &hash, const context &ctx
!!            )
!!    {
!!        std::string fname = detail::program_binary_path(hash) + "kernel";
!!        std::ifstream bfile(fname.c_str(), std::ios::binary);
!!        if (!bfile) return boost::optional<program>();
!!
!!        size_t binary_size;
!!        std::vector<unsigned char> binary;
!!
!!        bfile.read((char*)&binary_size, sizeof(size_t));
!!
!!        binary.resize(binary_size);
!!        bfile.read((char*)binary.data(), binary_size);
!!
!!        return boost::optional<program>(
!!                program::create_with_binary(
!!                    binary.data(), binary_size, ctx
!!                    )
!!                );
!!    }
!!#endif ! BOOST_COMPUTE_USE_OFFLINE_CACHE
!!
!!private:
!!    cl_program m_program;
!!};
!!
!!! \internal_ define get_info() specializations for program
!!BOOST_COMPUTE_DETAIL_DEFINE_GET_INFO_SPECIALIZATIONS(program,
!!    ((cl_uint, CL_PROGRAM_REFERENCE_COUNT))
!!    ((cl_context, CL_PROGRAM_CONTEXT))
!!    ((cl_uint, CL_PROGRAM_NUM_DEVICES))
!!    ((std::vector<cl_device_id>, CL_PROGRAM_DEVICES))
!!    ((std::string, CL_PROGRAM_SOURCE))
!!    ((std::vector<size_t>, CL_PROGRAM_BINARY_SIZES))
!!    ((std::vector<unsigned char *>, CL_PROGRAM_BINARIES))
!!)
!!
!!#ifdef CL_VERSION_1_2
!!BOOST_COMPUTE_DETAIL_DEFINE_GET_INFO_SPECIALIZATIONS(program,
!!    ((size_t, CL_PROGRAM_NUM_KERNELS))
!!    ((std::string, CL_PROGRAM_KERNEL_NAMES))
!!)
!!#endif ! CL_VERSION_1_2
!!
END MODULE OCL_PROGRAM_M
