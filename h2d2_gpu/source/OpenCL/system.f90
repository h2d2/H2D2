!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!

! \class system
! \brief Provides access to platforms and devices on the system.
!
! The system class contains a set of static functions which provide access to
! the OpenCL platforms and compute devices on the host system.
!
! The default_device() convenience method automatically selects and returns
! the "best" compute device for the system following a set of heuristics and
! environment variables. This simplifies setup of the OpenCL environment.
!
! \see platform, device, context

MODULE OCL_SYSTEM_M

   USE clfortran

   PRIVATE :: FIND_DEFAULT_DEVICE

CONTAINS

!!!    ! Returns the default compute device for the system.
!!!    !
!!!    ! The default device is selected based on a set of heuristics and can be
!!!    ! influenced using one of the following environment variables:
!!!    !
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_DEVICE -
!!!    !        name of the compute device (e.g. "GTX TITAN")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_DEVICE_TYPE
!!!    !        type of the compute device (e.g. "GPU" or "CPU")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_PLATFORM -
!!!    !        name of the platform (e.g. "NVIDIA CUDA")
!!!    ! \li \c BOOST_COMPUTE_DEFAULT_VENDOR -
!!!    !        name of the device vendor (e.g. "NVIDIA")
!!!    !
!!!    ! The default device is determined once on the first time this function
!!!    ! is called. Calling this function multiple times will always result in
!!!    ! the same device being returned.
!!!    !
!!!    ! If no OpenCL device is found on the system, a no_device_found exception
!!!    ! is thrown.
!!!    !
!!!    ! For example, to print the name of the default compute device on the
!!!    ! system:
!!!    ! \code
!!!    ! ! get the default compute device
!!!    ! boost::compute::device device = boost::compute::system::default_device();
!!!    !
!!!    ! ! print the name of the device
!!!    ! std::cout << "default device: " << device.name() << std::endl;
!!!    ! \endcode

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns a table containing all of the platforms on
!!!    ! the system.
!     The caller must destroy each platform with a call to OCL_PLATFOR_DTR
!     and de-allocate the table.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION DEFAULT_DEVICE

   USE OCL_DEVICE_M

   TYPE(OCL_DEVICE_T) :: DEFAULT_DEVICE
!------------------------------------------------------------------------------

   DEFAULT_DEVICE = FIND_DEFAULT_DEVICE()
   RETURN
   END FUNCTION DEFAULT_DEVICE

!!!    ! Returns the device with \p name.
!!!    !
!!!    ! \throws no_device_found if no device with \p name is found.
!!!    static device find_device(const std::string &name)
!!!    {
!!!        const std::vector<device> devices = system::devices();
!!!        for(size_t i = 0; i < devices.size(); i++){
!!!            const device& device = devices[i];
!!!
!!!            if(device.name() == name){
!!!                return device;
!!!            }
!!!        }
!!!
!!!        BOOST_THROW_EXCEPTION(no_device_found());
!!!    }

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns a table containing all of the platforms on
!!!    ! the system.
!     The caller must destroy each platform with a call to OCL_PLATFOR_DTR
!     and de-allocate the table.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION PLATFORMS()

   USE OCL_PLATFORM_M

   TYPE(OCL_PLATFORM_T), POINTER :: PLATFORMS(:)

   INTEGER :: IP, NPTFS
   INTEGER :: CLRET
   TYPE(OCL_PLATFORM_T), POINTER :: PTFS(:)
!------------------------------------------------------------------------------

   NPTFS = OCL_PLATFORM_COUNT()
   ALLOCATE( PTFS(NPTFS) )

   DO IP=1, NPTFS
      CLRET = OCL_PLATFORM_CTR(PTFS(IP), IP)
   ENDDO

   PLATFORMS => PTFS
   RETURN
   END FUNCTION PLATFORMS

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns a vector containing all of the compute devices on
!!!    ! the system.
!!!    !
!!!    ! For example, to print out the name of each OpenCL-capable device
!!!    ! available on the system:
!!!    ! \code
!!!    ! for(const auto &device : boost::compute::system::devices()){
!!!    !     std::cout << device.name() << std::endl;
!!!    ! }
!!!    ! \endcode
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION DEVICES()

   USE OCL_PLATFORM_M
   USE OCL_DEVICE_M

   TYPE(OCL_DEVICE_T), DIMENSION(:), POINTER :: DEVICES

   INTEGER :: IP, ID, NDVCS
   INTEGER(C_INT32_T)  :: CLRET
   TYPE(OCL_PLATFORM_T) :: PTF
   TYPE(OCL_PLATFORM_T), POINTER :: PTFS(:)
   TYPE(OCL_DEVICE_T), POINTER :: DEVCS(:)
!------------------------------------------------------------------------------

   PTFS => PLATFORMS()
   NDVCS = 0
   DO IP=1, SIZE(PTFS)
      PTF = PTFS(IP)
      NDVCS = NDVCS + PTF%GET_DEVICE_COUNT()
   ENDDO

   ALLOCATE( DEVCS(NDVCS) )
   NDVCS = 0
   DO IP=1, SIZE(PTFS)
      PTF = PTFS(IP)
      DO ID=1, PTF%GET_DEVICE_COUNT()
         NDVCS = NDVCS + 1
         CLRET = OCL_DEVICE_CTR(DEVCS(NDVCS), PTF, ID)
      ENDDO
   ENDDO

   DO IP=1, SIZE(PTFS)
      CLRET = OCL_PLATFORM_DTR( PTFS(IP) )
   ENDDO
   DEALLOCATE(PTFS)

   DEVICES => DEVCS
   RETURN
   END FUNCTION DEVICES

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the number of compute devices on the system.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION DEVICE_COUNT()

   USE OCL_PLATFORM_M

   INTEGER :: IP, NDVCS
   TYPE(OCL_PLATFORM_T) :: PTF
   TYPE(OCL_PLATFORM_T), POINTER :: PTFS(:)
!------------------------------------------------------------------------------

   NDVCS = 0

   PTFS => PLATFORMS()
   DO IP=1, SIZE(PTFS)
      PTF = PTFS(IP)
      NDVCS = NDVCS + PTF%GET_DEVICE_COUNT()
   ENDDO
   DEALLOCATE(PTFS)

   DEVICE_COUNT = NDVCS
   RETURN
   END FUNCTION DEVICE_COUNT

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the default context for the system.
!!!    !
!!!    ! The default context is created for the default device on the system
!!!    ! (as returned by default_device()).
!!!    !
!!!    ! The default context is created once on the first time this function is
!!!    ! called. Calling this function multiple times will always result in the
!!!    ! same context object being returned.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION DEFAULT_CONTEXT()

   USE OCL_CONTEXT_M

   TYPE(OCL_CONTEXT_T) :: DEFAULT_CONTEXT

   INTEGER :: CLRET
   TYPE(OCL_CONTEXT_T), SAVE :: CONTEXT
   LOGICAL, SAVE :: CONTEXT_EXIST = .FALSE.
!------------------------------------------------------------------------------

   IF (.NOT. CONTEXT_EXIST) THEN
      CLRET = OCL_CONTEXT_CTR(CONTEXT, DEFAULT_DEVICE())
      CONTEXT_EXIST = .TRUE.
   ENDIF

   DEFAULT_CONTEXT = CONTEXT
   RETURN
   END FUNCTION DEFAULT_CONTEXT

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the default command queue for the system.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION DEFAULT_QUEUE()

   USE OCL_CMDQUEUE_M

   TYPE(OCL_CMDQUEUE_T) :: DEFAULT_QUEUE

   INTEGER :: CLRET
   TYPE(OCL_CMDQUEUE_T), SAVE :: QUEUE
   LOGICAL, SAVE :: QUEUE_EXIST = .FALSE.
!------------------------------------------------------------------------------

   IF (.NOT. QUEUE_EXIST) THEN
      CLRET = OCL_CMDQUEUE_CTR(QUEUE, DEFAULT_CONTEXT(), DEFAULT_DEVICE())
      QUEUE_EXIST = .TRUE.
   ENDIF

   DEFAULT_QUEUE = QUEUE
   RETURN
   END FUNCTION DEFAULT_QUEUE

!!!    ! Blocks until all outstanding computations on the default
!!!    ! command queue are complete.
!!!    !
!!!    ! This is equivalent to:
!!!    ! \code
!!!    ! system::default_queue().finish();
!!!    ! \endcode
!!!    static void finish()
!!!    {
!!!        default_queue().finish();
!!!    }

!************************************************************************
! Sommaire: Imprime OpenCL
!
! Description:
!     La fonction GP_OPNCL_PRN imprime les plates-formes OpenCL et leurs
!     devices.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION PRINT_ALL(IUNIT)

   USE OCL_PLATFORM_M
   USE OCL_DEVICE_M
   IMPLICIT NONE

   INTEGER, INTENT(IN) :: IUNIT

   INTEGER CLRET
   INTEGER IP, ID
   INTEGER NPTF, NDEV
   INTEGER, PARAMETER  :: INDNT = 3
   TYPE(OCL_PLATFORM_T):: PTF
   TYPE(OCL_DEVICE_T)  :: DEV
   CHARACTER(256) :: BUF
!------------------------------------------------------------------------------

   NPTF = OCL_PLATFORM_COUNT()
   DO IP = 1, NPTF
      WRITE(IUNIT,'(A,I6)') 'Platform: ', IP
      WRITE(IUNIT,'(A)')    '================'
      
      CLRET = OCL_PLATFORM_CTR(PTF, IP)
      CLRET = PTF%PRINT_INFO(IUNIT)

      !---  Get devices
      NDEV = PTF%GET_DEVICE_COUNT()

      !---  Loop over devices
      DO ID=1,NDEV
         WRITE(IUNIT,'(A,I6)') 'device: ', ID
         WRITE(IUNIT,'(A,I6)') '--------------'
         CLRET = OCL_DEVICE_CTR(DEV, PTF, ID)
         CLRET = DEV%PRINT_DEV(IUNIT, INDNT)
         CLRET = OCL_DEVICE_DTR(DEV)
      ENDDO
      WRITE(IUNIT,*)

      CLRET = OCL_PLATFORM_DTR(PTF)
   ENDDO

   PRINT_ALL = CLRET
   RETURN
   END

!************************************************************************
! Sommaire:
!
! Description:
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION FIND_DEFAULT_DEVICE()

   USE OCL_PLATFORM_M
   USE OCL_DEVICE_M

   TYPE(OCL_DEVICE_T) :: FIND_DEFAULT_DEVICE

   INTEGER :: I, ID
   INTEGER(C_INT32_T)   :: CLRET
   TYPE(OCL_PLATFORM_T) :: PLTF
   TYPE(OCL_DEVICE_T)   :: DEVC
   TYPE(OCL_DEVICE_T), POINTER :: DEVCS(:)
   CHARACTER(LEN=256) :: ENV_NAME, ENV_TYPE, ENV_PLTF, ENV_VNDR
   LOGICAL :: FOUND
!------------------------------------------------------------------------------

   ! ---  Get a list of all devices on the system
   DEVCS => DEVICES()

   ! ---  Check for device from environment variable
   CALL GET_ENVIRONMENT_VARIABLE('H2D2_DEFAULT_DEVICE',     ENV_NAME)
   CALL GET_ENVIRONMENT_VARIABLE('H2D2_DEFAULT_DEVICE_TYPE',ENV_TYPE)
   CALL GET_ENVIRONMENT_VARIABLE('H2D2_DEFAULT_PLATFORM',   ENV_PLTF)
   CALL GET_ENVIRONMENT_VARIABLE('H2D2_DEFAULT_VENDOR',     ENV_VNDR)

   ID = 0
   FOUND = .FALSE.
   IF (ENV_NAME .NE. '' .OR. ENV_TYPE .NE. '' .OR. &
       ENV_PLTF .NE. '' .OR. ENV_VNDR .NE. '') THEN
      DO ID=1, SIZE(DEVCS)
         DEVC = DEVCS(ID)
         PLTF = DEVC%GET_PLATFORM()
         IF (INDEX(DEVC%GET_NAME(), ENV_NAME) .GT. 0) FOUND = .TRUE.

         IF (INDEX('GPU', ENV_TYPE) .GT. 0) THEN
            IF (IAND(DEVC%GET_TYPE(), CL_DEVICE_TYPE_GPU)) FOUND = .TRUE.
         ENDIF
         IF (INDEX('CPU', ENV_TYPE) .GT. 0) THEN
            IF (IAND(DEVC%GET_TYPE(), CL_DEVICE_TYPE_CPU)) FOUND = .TRUE.
         ENDIF

         IF (INDEX(PLTF%GET_NAME(),   ENV_PLTF) .GT. 0) FOUND = .TRUE.
         IF (INDEX(DEVC%GET_VENDOR(), ENV_VNDR) .GT. 0) FOUND = .TRUE.

         IF (FOUND) EXIT
      ENDDO
   ENDIF

   ! ---   Find the first gpu device
   IF (.NOT. FOUND) THEN
      DO ID=1, SIZE(DEVCS)
         DEVC = DEVCS(ID)
         IF (IAND(DEVC%GET_TYPE(), CL_DEVICE_TYPE_GPU) .NE. 0) FOUND = .TRUE.
         IF (FOUND) EXIT
      ENDDO
   ENDIF

   ! ---   Find the first cpu device
   IF (.NOT. FOUND) THEN
      DO ID=1, SIZE(DEVCS)
         DEVC = DEVCS(ID)
         IF (IAND(DEVC%GET_TYPE(), CL_DEVICE_TYPE_CPU) .NE. 0) FOUND = .TRUE.
         IF (FOUND) EXIT
      ENDDO
   ENDIF

   ! ---   Return the first device found
   IF (.NOT. FOUND) THEN
      ID = 1
      DEVC = DEVCS(ID)
   ENDIF

   ! ---
   DO I=1, SIZE(DEVCS)
      IF (I .NE. ID) CLRET = OCL_DEVICE_DTR( DEVCS(I) )
   ENDDO
   DEALLOCATE(DEVCS)

   FIND_DEFAULT_DEVICE = DEVC
   RETURN
   END FUNCTION FIND_DEFAULT_DEVICE

END MODULE OCL_SYSTEM_M
