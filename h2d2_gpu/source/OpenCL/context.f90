!---------------------------------------------------------------------------!
! Copyright (c) 2013 Kyle Lutz <kyle.r.lutz@gmail.com>
!
! Distributed under the Boost Software License, Version 1.0
! See accompanying file LICENSE_1_0.txt or copy at
! http:!www.boost.org/LICENSE_1_0.txt
!
! See http:!boostorg.github.com/compute for more information.
!---------------------------------------------------------------------------!


! \class context
! \brief A compute context.
!
! The context class represents a compute context.
!
! A context object manages a set of OpenCL resources including memory
! buffers and program objects. Before allocating memory on the device or
! executing kernels you must set up a context object.
!
! To create a context for the default device on the system:
! \code
! ! get the default compute device
! boost::compute::device gpu = boost::compute::system::default_device();
!
! ! create a context for the device
! boost::compute::context context(gpu);
! \endcode
!
! Once a context is created, memory can be allocated using the buffer class
! and kernels can be executed using the command_queue class.
!
! \see device, command_queue
MODULE OCL_CONTEXT_M

   USE clfortran
   USE ISO_C_BINDING
   USE OCL_DEVICE_M, ONLY : OCL_DEVICE_T

   IMPLICIT NONE

   TYPE :: OCL_CONTEXT_T
      INTEGER(C_INTPTR_T), PRIVATE :: M_CONTEXT = 0
      TYPE(OCL_DEVICE_T),  PRIVATE :: DEVC

      CONTAINS
      PROCEDURE :: GET
      PROCEDURE :: GET_DEVICE
   END TYPE OCL_CONTEXT_T

   INTERFACE OCL_CONTEXT_CTR
      MODULE PROCEDURE OCL_CONTEXT_CTR_ONE, &
                       OCL_CONTEXT_CTR_MANY
   END INTERFACE

CONTAINS

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Creates a new context for 1 \p device with \p properties.
!
! Entrée:
!     DEVC     Device
!     PROPS    Properties
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_CONTEXT_CTR_ONE(SELF, DEVC)

   USE OCL_UTIL_M
   USE OCL_PLATFORM_M, ONLY : OCL_PLATFORM_T
   USE OCL_DEVICE_M,   ONLY : OCL_DEVICE_T

   TYPE(OCL_CONTEXT_T), INTENT(OUT):: SELF
   TYPE(OCL_DEVICE_T),  INTENT(IN) :: DEVC

   INTEGER :: ID
   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: CID
   INTEGER(C_INTPTR_T) :: DIDS(1)
   integer(c_intptr_t) :: PROPS(3)
   TYPE(OCL_PLATFORM_T):: PLTF
   TYPE(OCL_CONTEXT_T) :: SELF0
!---------------------------------------------------------------------

   ! ---  OpenCL device list
   DIDS(1) = DEVC%GET()

   ! ---  Properties
   PLTF = DEVC%GET_PLATFORM()
   PROPS(1) = CL_CONTEXT_PLATFORM
   PROPS(2) = PLTF%GET()
   PROPS(3) = 0

   CLRET = CL_SUCCESS
   CID   = clCreateContext(C_LOC(PROPS), &
                           SIZE(DIDS), &
                           C_LOC(DIDS), &
                           C_NULL_FUNPTR, &
                           C_NULL_PTR, &
                           CLRET)

   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_context = CID
      SELF%DEVC = DEVC
   ENDIF

   OCL_CONTEXT_CTR_ONE = CLRET
   RETURN
   END FUNCTION OCL_CONTEXT_CTR_ONE

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Creates a new context for n \p devices with \p properties.
!
! Entrée:
!     DEVC     Table of devices
!     PROPS    Properties
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_CONTEXT_CTR_MANY(SELF, DEVC)

   USE OCL_UTIL_M
   USE OCL_PLATFORM_M, ONLY : OCL_PLATFORM_T
   USE OCL_DEVICE_M,   ONLY : OCL_DEVICE_T

   TYPE(OCL_CONTEXT_T), INTENT(OUT):: SELF
   TYPE(OCL_DEVICE_T),  INTENT(IN) :: DEVC(:)

   INTEGER :: ID
   INTEGER(C_INT32_T)  :: CLRET
   INTEGER(C_INTPTR_T) :: CID
   INTEGER(C_INTPTR_T) :: DIDS( SIZE(DEVC) )
   integer(c_intptr_t) :: PROPS(3)
   TYPE(OCL_PLATFORM_T):: PLTF
   TYPE(OCL_CONTEXT_T) :: SELF0
!---------------------------------------------------------------------

   ! ---  OpenCL device list
   DO ID=1, size(DEVC)
      DIDS(ID) = DEVC(ID)%GET()
   ENDDO

   ! ---  Properties
   PLTF = DEVC(1)%GET_PLATFORM()
   PROPS(1) = CL_CONTEXT_PLATFORM
   PROPS(2) = PLTF%GET()
   PROPS(3) = 0

   CLRET = CL_SUCCESS
   CID   = clCreateContext(C_LOC(PROPS), &
                           SIZE(DIDS), &
                           C_LOC(DIDS), &
                           C_NULL_FUNPTR, &
                           C_NULL_PTR, &
                           CLRET)

   IF (CLRET .EQ. CL_SUCCESS) THEN
      SELF = SELF0
      SELF%m_context = CID
      SELF%DEVC = DEVC(1)
   ENDIF

   OCL_CONTEXT_CTR_MANY = CLRET
   RETURN
   END FUNCTION OCL_CONTEXT_CTR_MANY

!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Creates a new context for n \p devices with \p properties.
!
! Entrée:
!     DEVC     Table of devices
!     PROPS    Properties
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER FUNCTION OCL_CONTEXT_DTR(SELF)

   TYPE(OCL_CONTEXT_T), INTENT(INOUT) :: SELF

   INTEGER(C_INT32_T)  :: CLRET
!---------------------------------------------------------------------

   ! ---  Relâche le context
   CLRET = CL_SUCCESS
   IF (SELF%m_context .NE. 0) THEN
      CLRET = clReleaseContext(SELF%GET())
      SELF%m_context = 0
   ENDIF

   OCL_CONTEXT_DTR = CLRET
   RETURN
   END FUNCTION OCL_CONTEXT_DTR


!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the underlying OpenCL context.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   INTEGER(C_INTPTR_T) FUNCTION GET(SELF)

   CLASS(OCL_CONTEXT_T) :: SELF

   GET = SELF%M_CONTEXT
   RETURN
   END FUNCTION GET


!************************************************************************
! Sommaire:
!
! Description:
!!!    ! Returns the device for the context. If the context contains multiple
!!!    ! devices, the first is returned.
!
! Entrée:
!
! Sortie:
!
! Notes:
!************************************************************************
   FUNCTION GET_DEVICE(SELF)

   TYPE(OCL_DEVICE_T) :: GET_DEVICE
   CLASS(OCL_CONTEXT_T) :: SELF

   GET_DEVICE = SELF%DEVC
   RETURN
   END FUNCTION GET_DEVICE

!!!    ! Returns a vector of devices for the context.
!!!    std::vector<device> get_devices() const
!!!    {
!!!        return get_info<std::vector<device> >(CL_CONTEXT_DEVICES);
!!!    }
!!!
!!!    ! Returns information about the context.
!!!    !
!!!    ! \see_opencl_ref{clGetContextInfo}
!!!    template<class T>
!!!    T get_info(cl_context_info info) const
!!!    {
!!!        return detail::get_object_info<T>(clGetContextInfo, m_context, info);
!!!    }
!!!
!!!    ! \overload
!!!    template<int Enum>
!!!    typename detail::get_object_info_type<context, Enum>::type
!!!    get_info() const;
!!!
!!!    ! Returns \c true if the context is the same at \p other.
!!!    bool operator==(const context &other) const
!!!    {
!!!        return m_context == other.m_context;
!!!    }
!!!
!!!    ! Returns \c true if the context is different from \p other.
!!!    bool operator!=(const context &other) const
!!!    {
!!!        return m_context != other.m_context;
!!!    }
!!!
!!!    ! \internal_
!!!    operator cl_context() const
!!!    {
!!!        return m_context;
!!!    }

END MODULE OCL_CONTEXT_M
