C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_CONF_000
C     INTEGER GP_CONF_999
C     INTEGER GP_CONF_CTR
C     INTEGER GP_CONF_DTR
C     INTEGER GP_CONF_INI
C     INTEGER GP_CONF_RST
C     INTEGER GP_CONF_REQHBASE
C     LOGICAL GP_CONF_HVALIDE
C     INTEGER GP_CONF_REQNCPU
C     INTEGER GP_CONF_REQNDEV
C     INTEGER GP_CONF_REQNTSK
C     INTEGER GP_CONF_REQHDEV
C   Private:
C     SUBROUTINE GP_CONF_REQSELF
C     INTEGER GP_CONF_RAZ
C     INTEGER GP_CONF_INI_OMP
C     INTEGER GP_CONF_INI_CL
C     INTEGER GP_CONF_INI_CU
C     INTEGER GP_CONF_AJTDEV
C     INTEGER GP_CONF_SRTDEV
C     INTEGER GP_CONF_DISDEV
C
C************************************************************************

!  Configuration globale des GPU.
!  La configuration est faite de n devices.
!  Pour OMP, OpenCL & CUDA

      MODULE GP_CONF_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_CONF_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: GP_CONF_SELF_T
         INTEGER NDEVMAX
         INTEGER NDEV      ! Nombre de devices de la config
         INTEGER NBPE      ! Nombre de byte par élément
         INTEGER, ALLOCATABLE :: DEVICES(:)
      END TYPE GP_CONF_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_CONF_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_CONF_SELF_T), POINTER :: GP_CONF_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_CONF_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_CONF_REQSELF => SELF
      RETURN
      END FUNCTION GP_CONF_REQSELF

      END MODULE GP_CONF_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_CONF_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_000
CDEC$ ENDIF

      USE GP_CONF_M

      IMPLICIT NONE

      INCLUDE 'gpconf.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_CONF_HBASE, 'GPU - Configuration')

      GP_CONF_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_CONF_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_999
CDEC$ ENDIF

      USE GP_CONF_M

      IMPLICIT NONE

      INCLUDE 'gpconf.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_CONF_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_CONF_HBASE, GP_CONF_DTR)

      GP_CONF_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_CONF_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_CTR
CDEC$ ENDIF

      USE GP_CONF_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR, IRET
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_CONF_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_CONF_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_CONF_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_CONF_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_CONF_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_DTR
CDEC$ ENDIF

      USE GP_CONF_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_CONF_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_CONF_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_CONF_HBASE)

      GP_CONF_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_CONF_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_RAZ(HOBJ)

      USE GP_CONF_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fc'

      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_CONF_REQSELF(HOBJ)
      SELF%NDEV =  0
      SELF%NBPE =  0

      GP_CONF_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction privée GP_CONF_INI_CL initialise l'objet pour
C     une plate-forme OMP.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_INI_OMP(HOBJ, HLST)

      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: HOBJ
      INTEGER, INTENT(IN)    :: HLST

      INCLUDE 'gpconf.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
      INTEGER IT, IP, ID
      INTEGER HDEV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLST))
D     CALL ERR_PRE(DS_LIST_REQDIM(HLST) .GT. 0)
C------------------------------------------------------------------------

      IT = GP_DEVC_TYP_OMP
      IP = 1 ! Première plate-forme

C---     Reset les données
      IERR = GP_CONF_RST(HOBJ)

C---     Ajoute les devices
      ID = 0
100   CONTINUE
         ID = ID + 1
         HDEV = 0
         IF (ERR_GOOD()) IERR = GP_DEVC_CTR(HDEV, IT)
         IF (ERR_GOOD()) IERR = GP_DEVC_INI(HDEV, IP, ID)
         IF (ERR_GOOD()) IERR = GP_CONF_AJTDEV(HOBJ, HDEV)
      IF (ERR_GOOD()) GOTO 100

C---     Gestion de la sortie
      IF (ERR_BAD() .AND. GP_DEVC_HVALIDE(HDEV)) THEN
         CALL ERR_PUSH()
         IF (ERR_GOOD()) IERR = GP_DEVC_DTR(HDEV)
         CALL ERR_POP()
      ENDIF

C---     Gestion d'erreur
      IF (ERR_ESTMSG('ERR_OMP_INVALID_DEVICE_INDEX')) CALL ERR_RESET()

      GP_CONF_INI_OMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction privée GP_CONF_INI_CL initialise l'objet pour
C     une plate-forme OpenCL.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_INI_CL(HOBJ, HLST)

      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: HOBJ
      INTEGER, INTENT(IN)    :: HLST

      INCLUDE 'gpconf.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
      INTEGER ID, NDEV
      INTEGER HDEV
      CHARACTER*32  BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLST))
D     CALL ERR_PRE(DS_LIST_REQDIM (HLST) .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_CONF_RST(HOBJ)

C---     Ajoute les devices de la liste
      NDEV = DS_LIST_REQDIM(HLST)
      DO ID=1,NDEV
C---        Récupère le device
         HDEV = 0
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLST, ID, BUF)
         IF (ERR_GOOD()) READ(BUF, *) HDEV
C---        Contrôles
         IF (ERR_GOOD()) THEN
            IF (.NOT. GP_DEVC_HVALIDE(HDEV)) GOTO 9900
            IF (GP_DEVC_REQITYP(HDEV) .NE. GP_DEVC_TYP_OPENCL) GOTO 9901
         ENDIF
C---        Ajoute au context
         IF (ERR_GOOD()) IERR = GP_CONF_AJTDEV(HOBJ, HDEV)
         IF (ERR_GOOD()) IERR = GP_DEVC_START (HDEV)
         IF (ERR_GOOD()) IERR = GP_DEVC_PRINT (HDEV)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HDEV
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HDEV, 'MSG_OPENCL_DEVICE')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_TYPE_DEVICE_INCOHERENT')
      CALL ERR_AJT('MSG_TYPE_NON_IDENTIQUES')
      CALL ERR_AJT('MSG_OPENCL_DEVICE')
      GOTO 9999
      
9999  CONTINUE
      GP_CONF_INI_CL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction privée GP_CONF_INI_CU initialise l'objet pour
C     une plate-forme CUDA.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_INI_CU(HOBJ, HLST)

      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: HOBJ
      INTEGER, INTENT(IN)    :: HLST

      INCLUDE 'gpconf.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
      INTEGER IT, IP, ID
      INTEGER HDEV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLST))
D     CALL ERR_PRE(DS_LIST_REQDIM(HLST) .GT. 0)
C------------------------------------------------------------------------

      IT = GP_DEVC_TYP_CUDA
      IP = 1 ! Première plate-forme

C---     Reset les données
      IERR = GP_CONF_RST(HOBJ)

C---     Ajoute les devices
      ID = 0
100   CONTINUE
         ID = ID + 1
         HDEV = 0
         IF (ERR_GOOD()) IERR = GP_DEVC_CTR(HDEV, IT)
         IF (ERR_GOOD()) IERR = GP_DEVC_INI(HDEV, IP, ID)
         IF (ERR_GOOD()) IERR = GP_CONF_AJTDEV(HOBJ, HDEV)
      IF (ERR_GOOD()) GOTO 100

C---     Gestion de la sortie
      IF (ERR_BAD() .AND. GP_DEVC_HVALIDE(HDEV)) THEN
         CALL ERR_PUSH()
         IF (ERR_GOOD()) IERR = GP_DEVC_DTR(HDEV)
         CALL ERR_POP()
      ENDIF

C---     Gestion d'erreur
      IF (ERR_ESTMSG('ERR_CU_INVALID_DEVICE_INDEX')) CALL ERR_RESET()

      GP_CONF_INI_CU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_CONF_INI initialise l'objet à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     ITYP        Type de plate-forme
C     NBPT        Nombre de byte par task
C     FNAM        Path du kernel
C     KNAM        Kernel name
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_INI(HOBJ, HLST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_INI
CDEC$ ENDIF

      USE GP_CONF_M

      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: HOBJ
      INTEGER, INTENT(IN)    :: HLST

      INCLUDE 'gpconf.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
      INTEGER ITYP
      INTEGER HDEV
      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLST))
D     CALL ERR_PRE(DS_LIST_REQDIM(HLST) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère le type
      ITYP = GP_DEVC_TYP_INDEFINI
      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLST, 1, BUF)
      IF (ERR_GOOD()) READ(BUF, *) HDEV
      IF (ERR_GOOD()) ITYP = GP_DEVC_REQITYP(HDEV)
      
C---     Dispatch
      IF     (ITYP .EQ. GP_DEVC_TYP_OMP) THEN
         IERR = GP_CONF_INI_OMP(HOBJ, HLST)
      ELSEIF (ITYP .EQ. GP_DEVC_TYP_OPENCL) THEN
         IERR = GP_CONF_INI_CL(HOBJ, HLST)
      ELSEIF (ITYP .EQ. GP_DEVC_TYP_CUDA) THEN
         IERR = GP_CONF_INI_CU(HOBJ, HLST)
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_INVALID_GPU_TYPE')
      ENDIF

      GP_CONF_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_RST
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les paramètres
      IERR = GP_CONF_RAZ(HOBJ)

      GP_CONF_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_CONF_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_REQHBASE
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER GP_CONF_REQHBASE
C------------------------------------------------------------------------

      GP_CONF_REQHBASE = GP_CONF_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_CONF_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_HVALIDE
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_CONF_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_CONF_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_CONF_AJTDEV
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_AJTDEV(HOBJ, HDEV)

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HDEV

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fc'

      INTEGER IERR
      INTEGER NDEV
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
      INTEGER, ALLOCATABLE :: TBUF(:)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_CONF_REQSELF(HOBJ)

C---     Gère l'allocation de la table
      IF (.NOT. ALLOCATED(SELF%DEVICES)) THEN
         SELF%NDEVMAX = 4
         ALLOCATE( SELF%DEVICES(SELF%NDEVMAX) )
      ENDIF
      IF (SELF%NDEV .EQ. SELF%NDEVMAX) THEN
         SELF%NDEVMAX = 2 * SELF%NDEVMAX
         ALLOCATE( TBUF(SELF%NDEVMAX) )
         TBUF = SELF%DEVICES
         CALL MOVE_ALLOC(TBUF, SELF%DEVICES)
      ENDIF

C---     Enregistre le device
      SELF%NDEV = SELF%NDEV + 1
      SELF%DEVICES(SELF%NDEV) = HDEV

      GP_CONF_AJTDEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_CONF_SRTDEV trie les devices en ordre de "force"
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C  trier les devices en ordre:
C     ntask
C     clock
C     mem per task
C     ordre des architectures
C************************************************************************
      FUNCTION GP_CONF_SRTDEV(HOBJ)

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gpconf.fc'

      INTEGER   IERR
      INTEGER   I, J
      INTEGER   HDEV1, HDEV2
      INTEGER*8 NCPU1, NCPU2
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_CONF_REQSELF(HOBJ)

C---     Insertion sort
      DO I=1,SELF%NDEV-1
         HDEV1 = SELF%DEVICES(I)
         NCPU1 = GP_DEVC_REQNCPU(HDEV1)
         DO J=I+1,SELF%NDEV
            HDEV2 = SELF%DEVICES(J)
            NCPU2 = GP_DEVC_REQNCPU(HDEV2)
            IF (NCPU2 .GT. NCPU1) THEN
               HDEV1 = HDEV2
               NCPU1 = NCPU2
            ENDIF
         ENDDO
         SELF%DEVICES(I) = HDEV1
      ENDDO

      GP_CONF_SRTDEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction privée GP_CONF_DISDEV distribue les CPU sur les devices.
C     Le nombre de tâches peut être limité par le kernel, par la mémoire ou
C     par le paramètre NTSK. On retient le minimum disponible.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NBPE        Nombre de byte par élément
C     NEPT        Nombre d'élément par tâche (-1 si pas de limite)
C     NTSK        Nombre de tâches  (-1 si pas de limite)
C     HKRN        Liste des kernels
C
C Sortie:
C
C Notes:
!  si plusieurs devices:      2 x 2048 , 3 omp
!     ntask_per_omp_tgt    4096 / 3 ~ 1365.333
!
!     device 1:
!     reste 2 x 2048 , 3 omp
!     ntask_per_omp_tgt    4096 / 3 ~ 1365.333
!     nint(2048/1365.3) = nint(1.5) ==> 2? OMP
!
!     reste 1 x 2048 , 1 omp
!     ntask_per_omp_tgt    2048 / 1 ~ 2048
!     device 1: nint(2048/2048) = nint(1) ==> 1?
!
!  Le  nombre de tâches peut aussi être limité par la
!  mémoire.
!  Pour un kernel, le nombre de tâches peut aussi être limité
!  par la mémoire local ou les registres.
!
!  Pour un device qui a plusieurs OMP, la charge de chaque OMP
!  est identique.
C************************************************************************
      FUNCTION GP_CONF_DISDEV(HOBJ, NBPE, NEPT, NTSK, HKRN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_DISDEV
CDEC$ ENDIF

      USE GP_CONF_M

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NBPE      ! Nombre de bytes par élément
      INTEGER, INTENT(IN) :: NEPT      ! Nombre d'élément par task
      INTEGER, INTENT(IN) :: NTSK      ! Nombre de tâches requises (global work group size)
      INTEGER, INTENT(IN) :: HKRN      ! Handle sur la liste des kernels

      INCLUDE 'gpconf.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dslsti.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpconf.fc'

!$    INTEGER OMP_GET_MAX_THREADS

      INTEGER IERR, IRET
      INTEGER ID, ID0
      INTEGER INFO(4)
      INTEGER HDEV, HITR
      INTEGER NWI
      INTEGER NBPT, NTSK_EF
      INTEGER NTEFF
      INTEGER NDMIN, NOMP
      INTEGER NDIV, NPAG
      INTEGER NWITOT
      INTEGER ITSK_DEV, NOMP_DEV, NTSK_DEV, NWRP_DEV
      INTEGER*8 NMEM_DEV
      INTEGER UNBALANCE0, UNBALANCE1
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
      LOGICAL MODIF
      CHARACTER*(256) KITM      ! Kernel item
      CHARACTER*(256) FNAM      ! Path du fichier
      CHARACTER*(256) KNAM      ! Kernel name
      
      INTEGER, PARAMETER :: NDEV_MAX = 8
      INTEGER DEV_INFO(5, NDEV_MAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(NBPE .GT. 0)
D     CALL ERR_PRE(NEPT .GT. 0 .OR. NEPT .EQ. -1)
D     CALL ERR_PRE(NTSK .GT. 0 .OR. NTSK .EQ. -1)
D     CALL ERR_PRE(DS_LIST_HVALIDE(HKRN))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_CONF_REQSELF(HOBJ)

C---     Trie les devices
      IERR = GP_CONF_SRTDEV(HOBJ)

C---     OMP
      NOMP = 1
!$    NOMP = OMP_GET_MAX_THREADS()

C---     Monte les caractéristiques des devices
      NBPT = NBPE * MAX(1, NEPT)
      NDMIN = MIN(NOMP, SELF%NDEV)  !
      DO ID=1,NDMIN
         HDEV = SELF%DEVICES(ID)
         NWI  = MIN(GP_DEVC_REQGWI(HDEV),             ! Nb max de work items
     &              GP_DEVC_REQGBL_MMSZ(HDEV) / NBPT) ! Limite par la mémoire
C---        Boucle sur les kernels
         HITR = 0
         IERR = DS_LSTI_CTR(HITR)
         IERR = DS_LSTI_INI(HITR, HKRN)
         DO WHILE (ERR_GOOD())
            IERR = DS_LSTI_REQNXT(HITR, KITM)
            IF (ERR_GOOD()) THEN
               IRET = SP_STRN_TKS(KITM, '@', 1, KNAM)
               IRET = SP_STRN_TKS(KITM, '@', 2, FNAM)
               IERR = GP_DEVC_LOADK(HDEV, FNAM, KNAM, INFO)
            ENDIF
            IF (ERR_GOOD()) THEN
               NWI = MIN(NWI, INFO(4))                ! Limite par le kernel

               write(log_buf,'(a,a)') 'MSG_KERNEL: ', 
     &                                 KNAM(1:SP_STRN_LEN(KNAM))
               call log_ecris(log_buf)
               write(log_buf,'(a,i6)') '#<3>#MSG_LOCAL_MEM_SIZE: ',
     &                                 INFO(1)
               call log_ecris(log_buf)
               write(log_buf,'(a,i6)') '#<3>#MSG_PRIVATE_MEM_SIZE: ',
     &                                 INFO(2)
               call log_ecris(log_buf)
               write(log_buf,'(a,i6)') '#<3>#MSG_WORK_GROUP_SIZE: ',
     &                                 INFO(4)
               call log_ecris(log_buf)
            ENDIF
         ENDDO
         IF (DS_LSTI_HVALIDE(HITR)) IERR = DS_LSTI_DTR(HITR)

         IF (ERR_GOOD()) THEN
            DEV_INFO(1,ID) = NWI                            ! Available WI
            DEV_INFO(2,ID) = GP_DEVC_REQMIN_WGSZ(HDEV)      ! Warp size
            DEV_INFO(3,ID) = DEV_INFO(1,ID) / DEV_INFO(2,ID)! nb warp
            DEV_INFO(4,ID) = 0                              ! Eff Wi
            DEV_INFO(5,ID) = 0                              ! Eff nb warp
         ENDIF
      ENDDO
      IF (ERR_BAD()) GOTO 9999

C---     Diviseur et nombre de pages
      NWITOT  = SUM(DEV_INFO(1,:NDMIN))
      NTSK_EF = MERGE(NTSK, NWITOT, NTSK .GT. 0)      ! Limite par la nb. de tâches demandé
      NTSK_EF = MIN(NTSK_EF, NWITOT)
      NDIV = 1
      DO WHILE (INT(NTSK_EF/NDIV) > NWITOT)
         NDIV = NDIV + 1
      END DO
      NPAG = NDIV
      IF (INT(NTSK_EF/NDIV)*NDIV < NTSK_EF) NPAG = NPAG + 1

C---     Nombre de WI à distribuer
      NTEFF = INT(NTSK_EF/NDIV)

C---     Boucle de distribution
C        ======================
      DO WHILE (SUM(DEV_INFO(4,:NDMIN)) .LT. NTEFF)   ! LOAD < NTEFF
C---        Premier device qui n'est pas plein
         DO ID0=1,NDMIN
            IF (DEV_INFO(5,ID0) .LT. DEV_INFO(3,ID0)) EXIT
         ENDDO
C---        Incrémente ce device
         DEV_INFO(4,ID0) = DEV_INFO(4,ID0) + DEV_INFO(2,ID0)
         DEV_INFO(5,ID0) = DEV_INFO(5,ID0) + 1

C---        Incrémente les autres devices tant qu'ils sont dé-balancés
         MODIF = .TRUE.
         DO WHILE (MODIF .AND. (SUM(DEV_INFO(4,:NDMIN)) .LT. NTEFF))
            MODIF = .FALSE.
            DO ID=1,NDMIN
               IF (ID .EQ. ID0) CYCLE
               IF (DEV_INFO(5,ID) .GE. DEV_INFO(3,ID)) CYCLE   ! Device plein
               IF (DEV_INFO(4,ID) .GE. DEV_INFO(4,ID0)) CYCLE  ! Device déjà en surcharge
               UNBALANCE0 = DEV_INFO(4,ID0) - DEV_INFO(4,ID)
               UNBALANCE1 = ABS(UNBALANCE0 - DEV_INFO(5,ID))
               IF (UNBALANCE1 .LT. UNBALANCE0) THEN
                  DEV_INFO(4,ID) = DEV_INFO(4,ID) + DEV_INFO(2,ID0)
                  DEV_INFO(5,ID) = DEV_INFO(5,ID) + 1
                  MODIF = .TRUE.
               ENDIF
            ENDDO
         ENDDO
      ENDDO

C---     Assigne les charges
      ITSK_DEV = 1
      DO ID=1,NDMIN
         HDEV = SELF%DEVICES(ID)
         NOMP_DEV = 1
         NTSK_DEV = DEV_INFO(4,ID)
         NWRP_DEV = DEV_INFO(5,ID)
         NMEM_DEV = NTSK_DEV * NBPE
         IERR = GP_DEVC_ASGNCPU(HDEV, NOMP_DEV)
         IERR = GP_DEVC_ASGNTSK(HDEV, NTSK_DEV)
         IERR = GP_DEVC_ASGNWGP(HDEV, NWRP_DEV)
         IERR = GP_DEVC_ASGGMEM(HDEV, NMEM_DEV)    ! Mem par élém
         IERR = GP_DEVC_ASGITSK(HDEV, ITSK_DEV)    ! Indice de la 1ere tâche
         ITSK_DEV = ITSK_DEV + NTSK_DEV
      ENDDO

C---     Conserve le nombre de bytes par élément
      SELF%NBPE = NBPE

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_CONF_DISDEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de CPU.
C
C Description:
C     La fonction GP_CONF_REQNCPU retourne le nombre de CPU de
C     l'objet courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQNCPU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_REQNCPU
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER ID
      INTEGER HDEV
      INTEGER NCPU
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_CONF_REQSELF(HOBJ)

      NCPU = 0
      DO ID=1,SELF%NDEV
         HDEV = SELF%DEVICES(ID)
         NCPU = NCPU + GP_DEVC_REQNCPU(HDEV)
      ENDDO

      GP_CONF_REQNCPU = NCPU
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de devices.
C
C Description:
C     La fonction GP_CONF_REQNDEV retourne le nombre de devices de
C     l'objet courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQNDEV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_REQNDEV
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'

      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_CONF_REQSELF(HOBJ)
      GP_CONF_REQNDEV = SELF%NDEV
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de CPU.
C
C Description:
C     La fonction GP_CONF_REQNCPU retourne le nombre de CPU de
C     l'objet courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQNTSK(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_REQNTSK
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER ID
      INTEGER HDEV
      INTEGER NTSK
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_CONF_REQSELF(HOBJ)

      NTSK = 0
      DO ID=1,SELF%NDEV
         HDEV = SELF%DEVICES(ID)
         NTSK = NTSK + GP_DEVC_REQNTSK(HDEV)
      ENDDO

      GP_CONF_REQNTSK = NTSK
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur un device.
C
C Description:
C     La fonction GP_CONF_REQHDEV retourne le handle sur le device
C     d'indice ID, ou le device qui contient le CPU d'indice ICPU.
C
C Entrée:
C     ID
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CONF_REQHDEV(HOBJ, IDEV, ICPU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CONF_REQHDEV
CDEC$ ENDIF

      USE GP_CONF_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IDEV
      INTEGER ICPU

      INCLUDE 'gpconf.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      INTEGER ID
      INTEGER HDEV
      INTEGER NCPU
      TYPE (GP_CONF_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CONF_HVALIDE(HOBJ))
D     CALL ERR_PRE(IDEV .GT. 0 .OR. IDEV .EQ. -1)
C------------------------------------------------------------------------

      SELF => GP_CONF_REQSELF(HOBJ)
      HDEV = 0
      IF (IDEV .GT. 0) THEN
D        CALL ERR_ASR(IDEV .LE. SELF%NDEV)
         HDEV = SELF%DEVICES(IDEV)
      ELSE
         NCPU = 0
         DO ID=1,SELF%NDEV
            HDEV = SELF%DEVICES(ID)
            NCPU = NCPU + GP_DEVC_REQNCPU(HDEV)
            IF (NCPU .GE. ICPU) GOTO 199
         ENDDO
199      CONTINUE
         IF (ICPU .GT. NCPU) GOTO 9900
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_')
      GOTO 9999

9999  CONTINUE
      GP_CONF_REQHDEV = HDEV
      RETURN
      END
