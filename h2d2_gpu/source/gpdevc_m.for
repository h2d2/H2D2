C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     SUBROUTINE GP_DEVC_REQSELF
C
C************************************************************************

! La classe GP_DEVC représente un device physique (carte GPU) avec ses capacités.
! Un device à plusieurs groupes de calcul (work_group).
!Cuda:
!   taks
!   warp: min group of tasks
!opencl:
!   work_item
!   work_group
!   compute_unit

! Au niveau logique, un device a des tâches (i.e. des noeuds) qui se suivent

      MODULE GP_DEVC_M

      USE GP_DEVICE_INFO_M
      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_DEVC_HBASE = 0

C---     Pointeur à la structure virtuelle
      TYPE, ABSTRACT :: GP_DEVC_PTR_T
         CONTAINS
         PROCEDURE(GP_DEVC_INI_GEN),   DEFERRED :: GP_DEVC_INI_VIRT
         PROCEDURE(GP_DEVC_PRN_GEN),   DEFERRED :: GP_DEVC_PRN_VIRT
         PROCEDURE(GP_DEVC_START_GEN), DEFERRED :: GP_DEVC_START_VIRT
         !PROCEDURE(GP_DEVC_STOP_GEN),  DEFERRED :: GP_DEVC_STOP_VIRT

         PROCEDURE(GP_DEVC_ALLOC_GEN), DEFERRED :: GP_DEVC_ALLOC_VIRT
         PROCEDURE(GP_DEVC_CP2CPU_GEN),DEFERRED :: GP_DEVC_CP2CPU_VIRT
         PROCEDURE(GP_DEVC_CP2DEV_GEN),DEFERRED :: GP_DEVC_CP2DEV_VIRT

         PROCEDURE(GP_DEVC_ARGBUF_GEN),DEFERRED :: GP_DEVC_ARGBUF_VIRT
         PROCEDURE(GP_DEVC_LOADK_GEN), DEFERRED :: GP_DEVC_LOADK_VIRT
         PROCEDURE(GP_DEVC_XEQK_GEN),  DEFERRED :: GP_DEVC_XEQK_VIRT

      END TYPE GP_DEVC_PTR_T

      ABSTRACT INTERFACE

         INTEGER FUNCTION GP_DEVC_INI_GEN(SELF, INFO, IP, ID)
            IMPORT :: GP_DEVC_PTR_T
            IMPORT :: GP_DEVICE_INFO_T
            CLASS(GP_DEVC_PTR_T),   INTENT(INOUT) :: SELF
            TYPE(GP_DEVICE_INFO_T), INTENT(INOUT) :: INFO
            INTEGER, INTENT(IN) :: IP
            INTEGER, INTENT(IN) :: ID
         END FUNCTION GP_DEVC_INI_GEN

         INTEGER FUNCTION GP_DEVC_PRN_GEN(SELF, IUNIT)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: IUNIT
         END FUNCTION GP_DEVC_PRN_GEN

         INTEGER FUNCTION GP_DEVC_START_GEN(SELF)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(INOUT) :: SELF
         END FUNCTION GP_DEVC_START_GEN

         INTEGER FUNCTION GP_DEVC_ALLOC_GEN(SELF, ISIZ, BID)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(INOUT) :: SELF
            INTEGER(8), INTENT(IN)  :: ISIZ
            INTEGER(8), INTENT(OUT) :: BID
         END FUNCTION GP_DEVC_ALLOC_GEN

         INTEGER FUNCTION GP_DEVC_CP2CPU_GEN(SELF, ISIZ, BUFF, BID)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(IN) :: SELF
            INTEGER(8), INTENT(IN)    :: ISIZ
            BYTE,       INTENT(INOUT) :: BUFF(:)
            INTEGER(8), INTENT(IN)    :: BID
         END FUNCTION GP_DEVC_CP2CPU_GEN

         INTEGER FUNCTION GP_DEVC_CP2DEV_GEN(SELF, ISIZ, BUFF, BID)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(IN) :: SELF
            INTEGER(8), INTENT(IN) :: ISIZ
            BYTE,       INTENT(IN) :: BUFF(:)
            INTEGER(8), INTENT(IN) :: BID
         END FUNCTION GP_DEVC_CP2DEV_GEN

         INTEGER FUNCTION GP_DEVC_ARGBUF_GEN(SELF, I, V)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(IN) :: SELF
            INTEGER,    INTENT(IN) :: I
            INTEGER(8), INTENT(IN) :: V
         END FUNCTION GP_DEVC_ARGBUF_GEN

         INTEGER FUNCTION GP_DEVC_LOADK_GEN(SELF, FNAM, KNAM, INFO)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(INOUT) :: SELF
            CHARACTER*(*), INTENT(IN) :: FNAM
            CHARACTER*(*), INTENT(IN) :: KNAM
            INTEGER,       INTENT(OUT):: INFO(4)
         END FUNCTION GP_DEVC_LOADK_GEN

         INTEGER FUNCTION GP_DEVC_XEQK_GEN(SELF, NTSK)
            IMPORT :: GP_DEVC_PTR_T
            CLASS(GP_DEVC_PTR_T), INTENT(IN) :: SELF
            INTEGER,              INTENT(IN) :: NTSK
         END FUNCTION GP_DEVC_XEQK_GEN

      END INTERFACE

C---     Type de donnée associé à un device
      TYPE :: GP_DEVC_SELF_T
         CLASS(GP_DEVC_PTR_T), POINTER :: DEVP
         INTEGER :: ITYP
C---        Hardware data
         TYPE(GP_DEVICE_INFO_T) :: INFO
C---        Used
         INTEGER*8 NCPU_ALL
         INTEGER*8 NTSK_ALL
         INTEGER*8 NWGP_ALL
         INTEGER*8 GMEM_ALL
         INTEGER*8 ITSK_ALL
      END TYPE GP_DEVC_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_DEVC_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE(GP_DEVC_SELF_T), POINTER :: GP_DEVC_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_DEVC_REQSELF => SELF
      RETURN
      END FUNCTION GP_DEVC_REQSELF

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction privée GP_DEVC_CP2CPU copie la table depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CP2CPU(HOBJ, N, B, BID)

      IMPLICIT NONE

      INTEGER :: GP_DEVC_CP2CPU
      INTEGER,   INTENT(IN)    :: HOBJ
      INTEGER,   INTENT(IN)    :: N
      BYTE,      INTENT(INOUT) :: B(:)
      INTEGER(8),INTENT(IN)    :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER(8) :: ISIZ
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      ISIZ = N
      IERR = SELF%DEVP%GP_DEVC_CP2CPU_VIRT(ISIZ, B, BID)

      GP_DEVC_CP2CPU = ERR_TYP()
      RETURN
      END FUNCTION GP_DEVC_CP2CPU

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction privée GP_DEVC_CP2DEV copie la table BYTE vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CP2DEV(HOBJ, N, B, BID)

      IMPLICIT NONE

      INTEGER :: GP_DEVC_CP2DEV
      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      BYTE,      INTENT(IN) :: B(:)
      INTEGER(8),INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER(8) :: ISIZ
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      ISIZ = N
      IERR = SELF%DEVP%GP_DEVC_CP2DEV_VIRT(ISIZ, B, BID)

      GP_DEVC_CP2DEV = ERR_TYP()
      RETURN
      END FUNCTION GP_DEVC_CP2DEV

      END MODULE GP_DEVC_M
