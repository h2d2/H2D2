C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_DIST_000
C     INTEGER GP_DIST_999
C     INTEGER GP_DIST_CTR
C     INTEGER GP_DIST_DTR
C     INTEGER GP_DIST_INI
C     INTEGER GP_DIST_RST
C     INTEGER GP_DIST_REQHBASE
C     LOGICAL GP_DIST_HVALIDE
C     INTEGER GP_DIST_REQNBLCS
C     INTEGER GP_DIST_REQNCEL
C     INTEGER GP_DIST_REQNNOD
C     INTEGER GP_DIST_REQNELE
C     SUBROUTINE GP_DIST_REQKNG
C   Private:
C     SUBROUTINE GP_DIST_REQSELF
C     INTEGER GP_DIST_RAZ
C     INTEGER GP_DIST_BLCXEQ
C
C************************************************************************

!  Le module GP_DIST_M correspond à la distribution des noeuds et éléments
!  sur les tâches.

      MODULE GP_DIST_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_DIST_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: GP_DIST_SELF_T
         INTEGER NBLCS                         ! Nombre de blocs demandé
         INTEGER NBLEF                         ! Nombre de blocs effectif
         INTEGER, ALLOCATABLE :: KBLBLC(:,:)   ! Table des blocs
         INTEGER, ALLOCATABLE :: KBLELE(:)     ! Table des éléments
      END TYPE GP_DIST_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_DIST_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_DIST_SELF_T), POINTER :: GP_DIST_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_DIST_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_DIST_REQSELF => SELF
      RETURN
      END FUNCTION GP_DIST_REQSELF

      END MODULE GP_DIST_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_DIST_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_000
CDEC$ ENDIF

      USE GP_DIST_M

      IMPLICIT NONE

      INCLUDE 'gpdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_DIST_HBASE, 'GPU - Distribution')

      GP_DIST_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_DIST_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_999
CDEC$ ENDIF

      USE GP_DIST_M

      IMPLICIT NONE

      INCLUDE 'gpdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_DIST_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_DIST_HBASE, GP_DIST_DTR)

      GP_DIST_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_DIST_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_CTR
CDEC$ ENDIF

      USE GP_DIST_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gpdist.fc'

      INTEGER IERR, IRET
      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_DIST_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_DIST_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_DIST_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_DIST_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_DIST_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_DTR
CDEC$ ENDIF

      USE GP_DIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_DIST_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_DIST_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_DIST_HBASE)

      GP_DIST_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_DIST_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_RAZ(HOBJ)

      USE GP_DIST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdist.fc'

      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_DIST_REQSELF(HOBJ)
      SELF%NBLCS = 0
      SELF%NBLEF = 0

      GP_DIST_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_DIST_INI initialise l'objet à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NNL         Nombre de noeuds locaux
C     NCELV       Nombre de connectivités par élément de volume
C     NELV        Nombre d'éléments de volume
C     KNGV        Table des connectivités de volume
C     NBLCS       Nombre de blocs demandé
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_INI(HOBJ, NNL, NCELV, NELV, KNGV,  NBLCS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_INI
CDEC$ ENDIF

      USE GP_DIST_M

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NNL          ! Nombre de noeuds
      INTEGER, INTENT(IN) :: NCELV        ! Nombre de connectivités
      INTEGER, INTENT(IN) :: NELV         ! Nombre d'éléments
      INTEGER, INTENT(IN) :: KNGV(NCELV, NELV) ! Connectivités de volume
      INTEGER, INTENT(IN) :: NBLCS        ! Nombre de blocs

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdist.fc'

      INTEGER IERR
      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(NELV .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_DIST_RST(HOBJ)

C---     Distribue les éléments
      IERR = GP_DIST_BLCXEQ(HOBJ,
     &                      NNL,
     &                      NCELV,
     &                      NELV,
     &                      KNGV,
     &                      NBLCS)

      GP_DIST_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_RST
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdist.fc'

      INTEGER IERR
      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => GP_DIST_REQSELF(HOBJ)

C---     Désalloue la mémoire
D     CALL LOG_TODO('DE-ALLOUE la mémoire')

C---     Reset les paramètres
      IERR = GP_DIST_RAZ(HOBJ)

      GP_DIST_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_DIST_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_REQHBASE
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER GP_DIST_REQHBASE
C------------------------------------------------------------------------

      GP_DIST_REQHBASE = GP_DIST_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_DIST_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_HVALIDE
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_DIST_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: GP_DIST_BLCXEQ
C
C Description:
C     La fonction privée GP_DIST_BLCXEQ calcule la distribution. Elle
C     remplis les tables SELF%KBLBLC et SELF%KBLELE.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     NNL         le nombre de noeuds locaux
C     NCEL        le nombre de connectivités par élément
C     NELL        le nombre d'éléments locaux
C     KNG         les connectivités
C     NBLCS       Nombre de blocs à générer
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_BLCXEQ(HOBJ,
     &                        NNL,
     &                        NCEL,
     &                        NELL,
     &                        KNG,
     &                        NBLCS)

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: NCEL
      INTEGER, INTENT(IN) :: NELL
      INTEGER, INTENT(IN) :: KNG(NCEL, NELL)
      INTEGER, INTENT(IN) :: NBLCS

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'gpdist.fc'

      INTEGER IERR, IRET
      INTEGER NELMAX
      INTEGER NBLEF
      INTEGER NITR
      TYPE (GP_DIST_SELF_T), POINTER :: SELF

      INTEGER, PARAMETER :: NITR_MAX = 3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => GP_DIST_REQSELF(HOBJ)

C---     Alloue de l'espace pour les tables de travail
      ALLOCATE(SELF%KBLBLC(4, NBLCS), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Itère à cause de l'heuristique sur le dimensionnement
      NELMAX = MAX(1024, 6*NBLCS)
      NELMAX = MAX(NELMAX, NINT(NELL*2.5))
      NITR = 0
100   CONTINUE
         NITR = NITR + 1
         ALLOCATE(SELF%KBLELE(NELMAX), STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900

C---     Fait l'appel à la fonction de calcul des blocs
         IF (ERR_GOOD()) THEN
            IERR = SP_ELEM_BLCS(NNL,
     &                          NCEL,
     &                          NELL,
     &                          KNG,
     &                          NBLCS,
     &                          NELMAX,
     &                          SELF%KBLBLC,
     &                          SELF%KBLELE,
     &                          NBLEF)
         ENDIF

         IF (NITR .GE. NITR_MAX) GOTO 199
         IF (ERR_GOOD()) GOTO 199
         IF (.NOT. ERR_ESTMSG('ERR_ARRAY_UNDERSIZED')) GOTO 199
         CALL ERR_RESET()

         DEALLOCATE(SELF%KBLELE)
         NELMAX = SELF%KBLBLC(4,NBLEF)
      GOTO 100
199   CONTINUE

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF%NBLCS = NBLCS
         SELF%NBLEF = NBLEF
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      IF (ERR_BAD()) THEN
         IF (ALLOCATED(SELF%KBLELE)) DEALLOCATE(SELF%KBLELE)
         IF (ALLOCATED(SELF%KBLBLC)) DEALLOCATE(SELF%KBLBLC)
      ENDIF
      GP_DIST_BLCXEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de blocs.
C
C Description:
C     La fonction GP_DIST_REQNBLCS retourne le nombre de blocs d'éléments.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_REQNBLCS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_REQNBLCS
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdist.fi'

      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DIST_REQSELF(HOBJ)
      GP_DIST_REQNBLCS = INT(SELF%NBLCS)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de noeuds du bloc.
C
C Description:
C     La fonction GP_DIST_REQNNOD retourne le nombre de noeuds d'un bloc.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     IBLC        Indice du bloc
C
C Sortie:
C
C Notes:
C     KBLBLC contient les indices des noeuds debut/fin,
C     éléments début/fin de chaque bloc. Les indices des noeuds sont les
C     indices naturels, alors que les indices des éléments sont par rapport
C     à la table KBLELE.
C************************************************************************
      FUNCTION GP_DIST_REQNNOD(HOBJ, IBLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_REQNNOD
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IBLC

      INCLUDE 'gpdist.fi'

      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(IBLC .GT. 0)
C------------------------------------------------------------------------

      SELF => GP_DIST_REQSELF(HOBJ)
D     CALL ERR_ASR(IBLC .LE. SELF%NBLCS)
      GP_DIST_REQNNOD = SELF%KBLBLC(2,IBLC)-SELF%KBLBLC(1,IBLC)+1
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre d'éléments d'un bloc.
C
C Description:
C     La fonction GP_DIST_REQNELE retourne le nombre d'éléments d'un bloc.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     IBLC        Indice du bloc
C
C Sortie:
C
C Notes:
C     KBLBLC contient les indices des noeuds debut/fin,
C     éléments début/fin de chaque bloc. Les indices des noeuds sont les
C     indices naturels, alors que les indices des éléments sont par rapport
C     à la table KBLELE.
C************************************************************************
      FUNCTION GP_DIST_REQNELE(HOBJ, IBLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_REQNELE
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IBLC

      INCLUDE 'gpdist.fi'

      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(IBLC .GT. 0)
C------------------------------------------------------------------------

      SELF => GP_DIST_REQSELF(HOBJ)
D     CALL ERR_ASR(IBLC .LE. SELF%NBLCS)
      GP_DIST_REQNELE = SELF%KBLBLC(4,IBLC)-SELF%KBLBLC(3,IBLC)+1
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les éléments d'un bloc.
C
C Description:
C     La fonction GP_DIST_REQIELE retourne le numéro d'élément d'indice IE
C     du bloc IBLC. Si le bloc contient moins d'éléments que IE, la fonction
C     retourne 0.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     IBLC        Indice du bloc
C     IE          Indice d'élément dans le bloc
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DIST_REQIELE(HOBJ, IBLC, IE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DIST_REQKNG
CDEC$ ENDIF

      USE GP_DIST_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: IBLC
      INTEGER, INTENT(IN) :: IE

      INCLUDE 'gpdist.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IEDEB, IEFIN, NEBLC, IEL
      TYPE (GP_DIST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(IBLC .GT. 0)
C------------------------------------------------------------------------

      SELF => GP_DIST_REQSELF(HOBJ)
D     CALL ERR_ASR(IBLC .LE. SELF%NBLCS)
      IEDEB = SELF%KBLBLC(3,IBLC)
      IEFIN = SELF%KBLBLC(4,IBLC)
      NEBLC = IEFIN - IEDEB + 1
      IEL = 0
      IF (IE .GE. 1 .AND. IE. LE. NEBLC) THEN
         IEL = IEDEB + IE -1
         IEL = SELF%KBLELE(IEL)
      ENDIF

      GP_DIST_REQIELE = IEL
      RETURN
      END
