C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_CLC1_000
C     INTEGER GP_CLC1_999
C     INTEGER GP_CLC1_CTR
C     INTEGER GP_CLC1_DTR
C     INTEGER GP_CLC1_INI
C     INTEGER GP_CLC1_RST
C     INTEGER GP_CLC1_REQHBASE
C     LOGICAL GP_CLC1_HVALIDE
C     INTEGER GP_CLC1_XEQ
C   Private:
C     SUBROUTINE GP_CLC1_REQSELF
C     INTEGER GP_CLC1_RAZ
C
C************************************************************************

!Chaque tâche GPU est responsable d'assembler des noeuds (lignes) consécutives de la matrice.
!Elle a toute l'information pour y arriver. La charge de travaille est répartie en équilibrant
!le nombre d'éléments par tâche GPU.
!
!Chaque tâche OMP gère un certains nombre de blocs de tâches GPU, qui globalement représente
!des noeuds (lignes) consécutives de la matrice. Chaque tâche OMP pagine le travail, en fonction
!de la taille de la mémoire globale du device GPU.
!
!L'information est transférée de table nodales vers des tables élémentaires. Il y a répétition
!de l'information des noeuds communs. De plus, pour avoir des accès mémoire coalescents,
!les informations des tâches se suivent:
!   VDLG_P(1:NDLN, IN, ) = VDLG(1:NDLN, KNE(IN))
!
!
!!chaque OMP
!      alloue mémoire pour la pages VLDG_P, VPRN_P, KLOC_P, VRES_P
!      pour chaque page
!         gather l'info to page
!         copy page to device
!         call GPU
!         copy device to page
!         scatter



! Le module GP_CLC1_M correspond au travail fait par un OMP.
! Il a à disposition NTASK sur un device.
! Il pagine si la mémoire globale du device ne permet pas de traiter
! tous les éléments d'un coup
      MODULE GP_CLC1_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: GP_CLC1_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: GP_CLC1_SELF_T
         INTEGER HDEV      ! Handle sur le device
         INTEGER HDST      ! Handle sur la distribution
         INTEGER HPG1      ! Handle sur la page 1
         INTEGER HPG2      ! Handle sur la page 2
         INTEGER ICPU      ! Indice du CPU (i.e. OMP)
         INTEGER ITSK      ! Indice de la 1ère tâche (i.e bloc)
         INTEGER NTSK      ! Nombre de tâches de la page
         INTEGER NELE      ! Nombre d'éléments de la page
      END TYPE GP_CLC1_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée GP_CLC1_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (GP_CLC1_SELF_T), POINTER :: GP_CLC1_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      GP_CLC1_REQSELF => SELF
      RETURN
      END FUNCTION GP_CLC1_REQSELF

      END MODULE GP_CLC1_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_CLC1_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_000
CDEC$ ENDIF

      USE GP_CLC1_M

      IMPLICIT NONE

      INCLUDE 'gpclc1.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_CLC1_HBASE, 'GPU - Calculator for 1 OMP')

      GP_CLC1_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_CLC1_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_999
CDEC$ ENDIF

      USE GP_CLC1_M

      IMPLICIT NONE

      INCLUDE 'gpclc1.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_CLC1_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_CLC1_HBASE, GP_CLC1_DTR)

      GP_CLC1_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_CLC1_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_CTR
CDEC$ ENDIF

      USE GP_CLC1_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclc1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gpclc1.fc'

      INTEGER IERR, IRET
      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_CLC1_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_CLC1_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_CLC1_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_CLC1_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_CLC1_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_DTR
CDEC$ ENDIF

      USE GP_CLC1_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclc1.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLC1_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_CLC1_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_CLC1_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_CLC1_HBASE)

      GP_CLC1_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_CLC1_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_RAZ(HOBJ)

      USE GP_CLC1_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclc1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpclc1.fc'

      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLC1_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_CLC1_REQSELF(HOBJ)
      SELF%HDEV = 0
      SELF%HDST = 0
      SELF%HPG1 = 0
      SELF%HPG2 = 0
      SELF%ICPU = 0
      SELF%ITSK = 0
      SELF%NTSK = 0
      SELF%NELE = 0

      GP_CLC1_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_CLC1_INI initialise le calculateur pour un CPU sur
C     un device.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HDST        Handle sur la distribution
C     HDEV        Handle sur le device
C     ICPU        Indice du CPU sur le device
C     NELE        Nombre d'éléments du CPU
C     NNEL        Nombre de noeuds par élément
C     NDLN        Nombre de degré de liberté par noeud
C     NPRN        Nombre de propriétés nodales par noeud
C
C Sortie:
C
C Notes:
C     On considère que toute la mémoire du device est à disposition, donc
C     qu'il n'y a pas 2 GP_DEVC sur 1 device physique.
C************************************************************************
      FUNCTION GP_CLC1_INI(HOBJ,
     &                     HDST,
     &                     ITSK,
     &                     HDEV,
     &                     ICPU,
     &                     GDTA,
     &                     EDTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_INI
CDEC$ ENDIF

      USE GP_CLC1_M
      USE LM_GDTA_M
      USE LM_EDTA_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HDST      ! Handle sur la distribution
      INTEGER ITSK      ! Indice du la 1ère tâche
      INTEGER HDEV      ! Handle sur le device
      INTEGER ICPU      ! Indice du CPU sur le device
      TYPE (LM_GDTA_T), INTENT(IN) :: GDTA
      TYPE (LM_EDTA_T), INTENT(IN) :: EDTA

      INCLUDE 'gpclc1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'gpdist.fi'
      INCLUDE 'gppage.fi'

      INTEGER IERR, IRET
      INTEGER IT
      INTEGER HP1, HP2
      INTEGER NTSK, NMEM, NELE
      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLC1_HVALIDE(HOBJ))
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HDEV))
D     CALL ERR_PRE(GP_DIST_HVALIDE(HDST))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_CLC1_RST(HOBJ)

C---     Dimensions du device
      NTSK = GP_DEVC_REQNTSK(HDEV) / GP_DEVC_REQNCPU(HDEV)
      NMEM = GP_DEVC_REQGBL_MMSZ(HDEV) / GP_DEVC_REQGMEM(HDEV) ! c.f. note

C---     Cherche le nombre d'éléments max des tâches
      NELE = -1
      DO IT=ITSK,ITSK+NTSK-1
         NELE = MAX(NELE, GP_DIST_REQNELE(HDST, IT))
      ENDDO
      NELE = MIN(NELE, NMEM)

C---     Construis les pages
      HP1 = 0
      HP2 = 0
      IF (ERR_GOOD()) IERR = GP_PAGE_CTR(HP1)
      IF (ERR_GOOD()) IERR = GP_PAGE_INI(HP1,HDEV,NTSK,NELE,GDTA,EDTA)
      IF (ERR_GOOD()) IERR = GP_PAGE_CTR(HP2)
      IF (ERR_GOOD()) IERR = GP_PAGE_INI(HP2,HDEV,NTSK,NELE,GDTA,EDTA)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => GP_CLC1_REQSELF(HOBJ)
         SELF%HDEV = HDEV
         SELF%HDST = HDST
         SELF%HPG1 = HP1
         SELF%HPG2 = HP2
         SELF%ICPU = ICPU
         SELF%ITSK = ITSK
         SELF%NTSK = NTSK
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_CLC1_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_RST
CDEC$ ENDIF

      USE GP_CLC1_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclc1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gppage.fi'
      INCLUDE 'gpclc1.fc'

      INTEGER IERR
      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_CLC1_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => GP_CLC1_REQSELF(HOBJ)

C---     Désalloue la mémoire
      IF (GP_PAGE_HVALIDE(SELF%HPG1)) IERR = GP_PAGE_DTR(SELF%HPG1)
      IF (GP_PAGE_HVALIDE(SELF%HPG2)) IERR = GP_PAGE_DTR(SELF%HPG2)

C---     Reset les paramètres
      IERR = GP_CLC1_RAZ(HOBJ)

      GP_CLC1_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_CLC1_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_REQHBASE
CDEC$ ENDIF

      USE GP_CLC1_M
      IMPLICIT NONE

      INTEGER GP_CLC1_REQHBASE
C------------------------------------------------------------------------

      GP_CLC1_REQHBASE = GP_CLC1_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_CLC1_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CLC1_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_HVALIDE
CDEC$ ENDIF

      USE GP_CLC1_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpclc1.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_CLC1_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_CLC1_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: GP_CLC1_XEQ
C
C Description:
C     Transfert les données en page.
C     Les données des tables nodales sont copiés
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Si le device a plusieurs OMP, je ne sais pas laquelle l'objet
C     CLC1 représente
C     On est ici dans une tâche OMP
C************************************************************************
      FUNCTION GP_CLC1_XEQ(HOBJ, FNC, XARGS, VARGS)
      ! il faut passer les tables ou l'objet
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CLC1_XEQ
CDEC$ ENDIF

      USE GP_CLC1_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   EXTERNAL   :: FNC
      INTEGER*8, INTENT(IN) :: XARGS(*)
      REAL*8,    INTENT(IN) :: VARGS(*)

      INCLUDE 'gpclc1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gppage.fi'

      INTEGER IERR
      INTEGER IEMAX, ITMAX
      INTEGER IEDEB, IEFIN, IEMIN
      INTEGER HDST, HDEV, HPG1, HPG2, HTMP
      LOGICAL DONE
      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
C     CALL ERR_PRE(GP_HVALIDE(HOBJ))
C     CALL ERR_PRE(IEMIN .GT. 0)
C     CALL ERR_PRE(ITMIN .GT. 0)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => GP_CLC1_REQSELF(HOBJ)
      HDST = SELF%HDST
      HDEV = SELF%HDEV
      HPG1 = SELF%HPG1
      HPG2 = SELF%HPG2

      IF (ERR_GOOD()) THEN
!!!         HPG2 = SELF%HPG2
!!!         NELE = SELF%NELE
!!!         NTSK = SELF%NTSK
!!!!         NNEL = SELF%NNEL
!!!!         NDLN = SELF%NDLN
!!!!         NPRN = SELF%NPRN
      ENDIF

C---     Gather memory to page
      IEMIN = 1         ! Élément min à assembler, par rapport au début du bloc
      IF (ERR_GOOD()) IERR = GP_PAGE_GATHER(HPG1, HDST, IEMIN)

      DONE = .FALSE.
      DO WHILE (.NOT. DONE)
C---        Copy page to device
C           fait la connection des données au device
         IF (ERR_GOOD()) IERR= GP_PAGE_CP2DEV(HPG1)

C---        Compute
         IF (ERR_GOOD()) IERR = FNC(HPG1, XARGS, VARGS)

C---        Pendant le calcul, le CPU charge l'autre page
         IF (ERR_GOOD()) IERR = GP_PAGE_GATHER(HPG2, HDST, IEMIN)

C---        Copy device to page - Blocking, wait until computation is done
         IF (ERR_GOOD()) IERR = GP_PAGE_CP2PGE(HPG1)

C---        Swap 1 et 2
         HTMP = HPG1
         HPG1 = HPG2
         HPG2 = HTMP

C---        Scatter page to memory
         IF (ERR_GOOD()) IERR = GP_PAGE_SCATTER(HPG2)

         DONE = .TRUE.
      END DO

      GP_CLC1_XEQ = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: GP_CLC1_XEQ
!!!C
!!!C Description:
!!!C     Transfert les données en page.
!!!C     Les données des tables nodales sont copiés
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C     Si le device a plusieurs OMP, je ne sais pas laquelle l'objet
!!!C     CLC1 représente
!!!C     On est ici dans une tâche OMP
!!!C************************************************************************
!!!      FUNCTION GP_CLC1_XEQ_OMP(HOBJ, FNC, XARGS, VARGS)
!!!
!!!      USE GP_CLC1_M
!!!      USE GP_PAGE_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER GP_CLC1_XEQ_OMP
!!!      INTEGER,   INTENT(IN) :: HOBJ
!!!      INTEGER,   EXTERNAL   :: FNC
!!!!$omp  declare target (FNC)
!!!      INTEGER*8, INTENT(IN) :: XARGS(:)
!!!      REAL*8,    INTENT(IN) :: VARGS(:)
!!!
!!!      INCLUDE 'gpclc1.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'log.fi'
!!!      INCLUDE 'gppage.fi'
!!!      INCLUDE 'soallc.fi'
!!!
!!!      INTEGER IERR
!!!      INTEGER IEMAX, ITMAX
!!!      INTEGER IEDEB, IEFIN, IEMIN
!!!      INTEGER HDST, HDEV, HPG1, HPG2, HTMP
!!!      LOGICAL DONE
!!!      TYPE (GP_CLC1_SELF_T), POINTER :: SELF
!!!      TYPE (GP_PAGE_DATA_T), POINTER :: PDTA
!!!      INTEGER, POINTER :: KLOC(:,:,:,:)
!!!      REAL*8,  POINTER :: VPRN(:,:,:,:)
!!!      REAL*8,  POINTER :: VDLG(:,:,:,:)
!!!
!!!      INTEGER LLOC_PGE, LPRN_PGE, LDLG_PGE
!!!C-----------------------------------------------------------------------
!!!C     CALL ERR_PRE(GP_HVALIDE(HOBJ))
!!!C     CALL ERR_PRE(IEMIN .GT. 0)
!!!C     CALL ERR_PRE(ITMIN .GT. 0)
!!!C-----------------------------------------------------------------------
!!!
!!!C---     Récupère les attributs
!!!      SELF => GP_CLC1_REQSELF(HOBJ)
!!!      HDST = SELF%HDST
!!!      HDEV = SELF%HDEV
!!!      HPG1 = SELF%HPG1
!!!      HPG2 = SELF%HPG2
!!!
!!!!$omp  parallel sections
!!!
!!!!$omp  target data
!!!!$omp*   map(to: KLOC)
!!!!$omp*   map(to: VPRN)
!!!!$omp*   map(tofrom: VDLG)
!!!
!!!C---     Gather memory to page
!!!      IEMIN = 1         ! Élément min à assembler, par rapport au début du bloc
!!!      IF (ERR_GOOD()) IERR = GP_PAGE_GATHER(HPG1, HDST, IEMIN)
!!!
!!!      DONE = .FALSE.
!!!      DO WHILE (.NOT. DONE)
!!!
!!!C---------------------------------------------------------------------------
!!!C     Task on device
!!!C---------------------------------------------------------------------------
!!!!$omp  task
!!!      PDTA => GP_PAGE_REQPDTA(HPG1)
!!!      KLOC => PDTA%KLOC
!!!      VPRN => PDTA%VPRN
!!!      VDLG => PDTA%VDLG
!!!
!!!C---        Copy to device
!!!!$omp  target update device(HDEV)
!!!!$omp*   to( KLOC )
!!!!$omp*   to( VPRN )
!!!!$omp*   to( VDLG )
!!!
!!!C---        Compute on device
!!!!$omp  target device(HDEV)
!!!         IERR = FNC(XARGS, VARGS)
!!!!$omp  end target
!!!
!!!C---        Update from device
!!!!$omp  target update device(HDEV)
!!!!$omp*   from( VDLG )
!!!
!!!!$omp  end task
!!!
!!!C---------------------------------------------------------------------------
!!!C     Task on CPU
!!!C---------------------------------------------------------------------------
!!!!$omp  task
!!!C---        Scatter page to memory
!!!         IF (.FALSE.)  IERR = GP_PAGE_SCATTER(HPG2) ! seulement à partir de 2
!!!
!!!C---        Pendant le calcul, le CPU charge l'autre page
!!!         IERR = GP_PAGE_GATHER(HPG2, HDST, IEMIN)
!!!
!!!!$omp end task
!!!
!!!C---        Swap 1 et 2
!!!!$omp taskwait
!!!         HTMP = HPG1
!!!         HPG1 = HPG2
!!!         HPG2 = HTMP
!!!      END DO
!!!
!!!!$omp end target data
!!!
!!!!$omp end parallel sections
!!!
!!!      GP_CLC1_XEQ_OMP = ERR_TYP()
!!!      RETURN
!!!      END
