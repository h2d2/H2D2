C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_CUDA_REQNPLT
C     INTEGER GP_CUDA_REQNDEV
C     INTEGER GP_CUDA_REQDEV
C     INTEGER GP_CUDA_PRN
C   Private:
C     INTEGER GP_CUDA_ERRMSG
C     INTEGER GP_CUDA_NCORES_PER_SM
C     INTEGER GP_CUDA_REQPID
C     INTEGER GP_CUDA_REQDID
C     INTEGER GP_CUDA_START
C     INTEGER GP_CUDA_ALLRE8
C     INTEGER GP_CUDA_ALLINT
C     INTEGER GP_CUDA_V2DEV
C     INTEGER GP_CUDA_K2DEV
C     INTEGER GP_CUDA_V2CPU
C     INTEGER GP_CUDA_PRN1PTF
C     INTEGER GP_CUDA_PRN1DEV
C     INTEGER GP_CUDA_PRN32
C     INTEGER GP_CUDA_PRN64
C     INTEGER GP_CUDA_PRNSZ
C     INTEGER GP_CUDA_PRNNS
C     INTEGER GP_CUDA_PRNCH
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_ERRMSG traduis le message d'erreur CUDA
C     en H2D2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_ERRMSG(IRET)

      USE CUDA
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IRET
!      INTEGER (KIND(cudaSuccess)), INTENT(IN) :: IRET

      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER :: I
      INTEGER, PARAMETER :: MAX_LEN = 256
      CHARACTER*(MAX_LEN) :: MSG
      CHARACTER, POINTER :: M_P(:)
C------------------------------------------------------------------------------

      IF (IRET .NE. cudaSuccess) THEN
         CALL C_F_POINTER( cudaGetErrorString(IRET), M_P, [MAX_LEN] )

         I = 1
         DO WHILE ( M_P(I) .NE. C_NULL_CHAR .AND. I .LT. MAX_LEN )
            MSG(I:I) = M_P(I)
            I = I + 1
         ENDDO
         IF (I .GT. 0) THEN
            CALL ERR_ASG(ERR_ERR, MSG(1:I))
         ENDIF
      ENDIF

      GP_CUDA_ERRMSG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_NCORES_PER_SM traduis le message d'erreur OpenCL
C     en H2D2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_NCORES_PER_SM (VMAJ, VMIN)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: VMAJ, VMIN

      INCLUDE 'gpcuda.fc'

      INTEGER N
C------------------------------------------------------------------------------

      N = -1
      SELECT CASE ( ISHFT(VMAJ,4) + VMIN )
         CASE (Z'10' : Z'13')    !  Tesla Generation
            N =   8
         CASE (Z'20')            !  Fermi Generation (SM 2.0) GF100 class
            N =  32
         CASE (Z'21')            !  Fermi Generation (SM 2.1) GF10x class
            N =  48
         CASE (Z'30' : Z'37')    !  Kepler Generation
            N = 192
         CASE (Z'50' : Z'52')    !  Maxwell Generation
            N = 128
         END SELECT

      GP_CUDA_NCORES_PER_SM = N
      END FUNCTION


C************************************************************************
C Sommaire: Retourne le PID de la plate-forme
C
C Description:
C     La fonction privée GP_CUDA_REQPID retourne le PID (opaque pointer)
C     de plate-forme d'indice IP.
C
C Entrée:
C     IP       Indice de la plate-forme CUDA
C
C Sortie:
C     PIP      Opaque pointer sur la plate-forme
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_REQPID(PID, IP)

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_intptr_t), INTENT(OUT) :: PID
      INTEGER, INTENT(IN) :: IP

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'
C------------------------------------------------------------------------------
      CALL ERR_PRE(IP .GT. 0)
C------------------------------------------------------------------------------

      IF (IP .GT. GP_CUDA_REQNPLT()) GOTO 9900
      PID = IP - 1

      GOTO 9999
C------------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CU_INVALID_PTFM_INDEX'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_REQPID = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le DID du device
C
C Description:
C     La fonction privée GP_CUDA_REQDID retourne le DID (opaque pointer)
C     du device d'indice ID, de la plate-forme d'indice IP.
C
C Entrée:
C     ID       Indice du device CUDA
C     IP       Indice de la plate-forme CUDA
C
C Sortie:
C     PIP      Opaque pointer sur le device
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_REQDID(DID, ID, IP)

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_intptr_t), INTENT(OUT) :: DID
      INTEGER, INTENT(IN) :: ID
      INTEGER, INTENT(IN) :: IP

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'
C------------------------------------------------------------------------------
      CALL ERR_PRE(ID .GT. 0)
C------------------------------------------------------------------------------

      IF (ID .GT. GP_CUDA_REQNDEV(IP)) GOTO 9900
      DID = ID - 1

      GOTO 9999
C------------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CU_INVALID_DEVICE_INDEX'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_REQDID = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Nombre de plates-formes CUDA.
C
C Description:
C     La fonction GP_CUDA_REQNPLT retourne le nombre de plates-formes CUDA.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_REQNPLT()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CUDA_REQNPLT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
C------------------------------------------------------------------------------

      GP_CUDA_REQNPLT = 1
      RETURN
      END

C************************************************************************
C Sommaire: Nombre de devices.
C
C Description:
C     La fonction GP_CUDA_REQNDEV retourne le nombre de devices CUDA de
C     la plate-forme d'indice IP.
C
C Entrée:
C     IP       Indice de la plate-forme CUDA
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_REQNDEV(IP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CUDA_REQNDEV
CDEC$ ENDIF

      USE CUDA
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: IP

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IERR, IRET
      INTEGER(c_int) :: NIDS
C------------------------------------------------------------------------------
D     CALL ERR_PRE(IP .EQ. 1)
C------------------------------------------------------------------------------

      NIDS = 0
      IRET = cudaGetDeviceCount(NIDS)
      IF (IRET .NE. cudaSuccess) GOTO 9900
      IF (NIDS .LT. 1) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------------
9900  IERR = GP_CUDA_ERRMSG(IRET)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_CU_INVALID_DEVICE_COUNT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_REQNDEV = NIDS
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_CUDA_REQDEV retourne les attributs d'un device.
C
C Entrée:
C     IP       CUDA handle on the platform.
C     ID       CUDA handle on device to query.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_REQDEV(DEVC, IP, ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CUDA_REQDEV
CDEC$ ENDIF

      USE CUDA
      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE(GP_DEVICE_INFO_T), INTENT(OUT) :: DEVC
      INTEGER, INTENT(IN) :: IP
      INTEGER, INTENT(IN) :: ID

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IERR, IRET
      INTEGER CPSM

      INTEGER(c_intptr_t) :: PID
      INTEGER(c_intptr_t) :: DID
      integer(c_int) :: device
      type (cudaDeviceProp) :: prop
C------------------------------------------------------------------------------

C---     Get device
      IF (ERR_GOOD()) IERR = GP_CUDA_REQPID(PID, IP)
      IF (ERR_GOOD()) IERR = GP_CUDA_REQDID(DID, ID, IP)

C---     Initialize structure
      IF (ERR_GOOD()) THEN
         DEVC%PLATFORM_ID       = PID
         DEVC%DEVICE_ID         = DID
         DEVC%MIN_WITM_PER_WGRP = -1
         DEVC%MAX_WITM_PER_WGRP = -1
         DEVC%MIN_WGRP_PER_CU   =  1
         DEVC%MAX_WGRP_PER_CU   = -1
         DEVC%MIN_CU_PER_DEV    =  1
         DEVC%MAX_CU_PER_DEV    = -1
         DEVC%MAX_WITM_PER_DEV  = -1
         DEVC%MEM_SIZE          = -1
      ENDIF

C---     Get info from device
      IF (ERR_GOOD()) THEN
         device = DID
         IRET = cudaGetDeviceProperties(prop, device)
         IF (IRET .NE. cudaSuccess) GOTO 9900
      ENDIF

C---     Get info from device
      IF (ERR_GOOD()) THEN
         cpsm = GP_CUDA_NCORES_PER_SM(prop%major, prop%minor)
         DEVC%MIN_WITM_PER_WGRP = prop%warpSize
         DEVC%MAX_WITM_PER_WGRP = prop%maxThreadsPerBlock
         DEVC%MIN_WGRP_PER_CU   =  1
         DEVC%MAX_WGRP_PER_CU   = prop%maxThreadsPerBlock/prop%warpSize
         DEVC%MIN_CU_PER_DEV    =  1
         DEVC%MAX_CU_PER_DEV    = prop%multiProcessorCount
         DEVC%MAX_WITM_PER_DEV  = prop%multiProcessorCount*cpsm
         DEVC%MEM_SIZE          = prop%totalGlobalMem
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------------
9900  IERR = GP_CUDA_ERRMSG(IRET)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_REQDEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_CUDA_START
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_START(PID, DID, CID, QID)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8, INTENT(IN)  :: PID
      INTEGER*8, INTENT(IN)  :: DID
      INTEGER*8, INTENT(OUT) :: CID
      INTEGER*8, INTENT(OUT) :: QID

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER   IERR, IRET
      integer  num_devices
      integer(c_intptr_t), target :: ctx_props(3)
      integer(c_intptr_t), target :: device_id
      integer(c_intptr_t), target :: context
      integer(c_int64_t) :: cmd_queue_props
      integer(c_intptr_t), target :: cmd_queue
      integer(c_int), PARAMETER :: IFLAGS = 0
C------------------------------------------------------------------------------

C---     Create a context
      !num_devices = 1
      !ctx_props(1) = CL_CONTEXT_PLATFORM
      !ctx_props(2) = INT(PID, c_intptr_t)
      !ctx_props(3) = 0
      !device_id = INT(DID, c_intptr_t)
      !IF (ERR_GOOD()) THEN
      !   IRET = cuInit(IFLAGS)
      !   IERR = GP_CUDA_ERRMSG(IRET)
      !ENDIF

C---     Create a command queue
      IF (ERR_GOOD()) THEN
         cmd_queue_props = 0
         cmd_queue = clCreateCommandQueue(context,
     &                                    device_id,
     &                                    cmd_queue_props,
     &                                    IRET)
         IERR = GP_CUDA_ERRMSG(IRET)
      ENDIF

C---     Return values
      IF (ERR_GOOD()) THEN
         CID = context
         QID = cmd_queue
      ENDIF

      GP_CUDA_START = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_CL_ALLRE8
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_ALLRE8(CTX, BID, ILEN)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8, INTENT(IN)  ::  CTX
      INTEGER*8, INTENT(OUT) ::  BID
      INTEGER,   INTENT(IN)  ::  ILEN

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IRET
      integer(c_size_t)   :: isize
      integer(c_intptr_t) :: context
C------------------------------------------------------------------------------

      context = INT(CTX, c_intptr_t)
      isize = ILEN * c_double
      BID = clCreateBuffer(context,
     &                     CL_MEM_READ_WRITE,
     &                     isize,
     &                     C_NULL_PTR,
     &                     IRET)

      GP_CUDA_ALLRE8 = GP_CUDA_ERRMSG(IRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_CUDA_ALLINT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_ALLINT(CTX, BID, ILEN)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8 CTX
      INTEGER*8 BID
      INTEGER ILEN

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IRET
      integer(c_size_t)   :: isize
      integer(c_intptr_t) :: context
C------------------------------------------------------------------------------

      context = INT(CTX, c_intptr_t)
      isize = ILEN * c_int
      BID = clCreateBuffer(context,
     &                     CL_MEM_READ_WRITE,
     &                     isize,
     &                     C_NULL_PTR,
     &                     IRET)

      GP_CUDA_ALLINT = GP_CUDA_ERRMSG(IRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_V2DEV copie une table REAL*8 vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_V2DEV(CQ, N, V, BID)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8, INTENT(IN) :: CQ
      INTEGER,   INTENT(IN) :: N
      REAL*8,    INTENT(IN) :: V(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IRET
      integer(c_size_t)   :: ioff
      integer(c_size_t)   :: isize
      INTEGER(c_intptr_t) :: CmdQ
      integer(c_intptr_t), target :: BuffId
C------------------------------------------------------------------------

C---     Copy CPU data to device buffer
      CmdQ   = INT(CQ,  c_intptr_t)
      BuffId = INT(BID, c_intptr_t)
      ioff  = 0
      isize = N * c_float
      IRET = clEnqueueWriteBuffer(CmdQ,
     &                            BuffId,
     &                            CL_TRUE,
     &                            ioff,
     &                            isize,
     &                            C_LOC(V),
     &                            0,
     &                            C_NULL_PTR,
     &                            C_NULL_PTR)

      GP_CUDA_V2DEV = GP_CUDA_ERRMSG(IRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_K2DEV copie une table INTEGER vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_K2DEV(CQ, N, K, BID)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8, INTENT(IN) :: CQ
      INTEGER,   INTENT(IN) :: N
      INTEGER,   INTENT(IN) :: K(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IRET
      integer(c_size_t)   :: ioff
      integer(c_size_t)   :: isize
      INTEGER(c_intptr_t) :: CmdQ
      integer(c_intptr_t), target :: BuffId
C------------------------------------------------------------------------

C---     Copy CPU data to device buffer
      CmdQ   = INT(CQ,  c_intptr_t)
      BuffId = INT(BID, c_intptr_t)
      ioff  = 0
      isize = N * c_int
      IRET = clEnqueueWriteBuffer(CmdQ,
     &                            BuffId,
     &                            CL_TRUE,
     &                            ioff,
     &                            isize,
     &                            C_LOC(K),
     &                            0,
     &                            C_NULL_PTR,
     &                            C_NULL_PTR)

      GP_CUDA_K2DEV = GP_CUDA_ERRMSG(IRET)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_V2CPU copie une table REAL*8 depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_V2CPU(CQ, N, V, BID)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER*8, INTENT(IN) :: CQ
      INTEGER,   INTENT(IN) :: N
      REAL*8,    INTENT(IN) :: V(*)
      INTEGER*8, INTENT(IN) :: BID

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IRET
      integer(c_size_t)   :: ioff
      integer(c_size_t)   :: isize
      INTEGER(c_intptr_t) :: CmdQ
      integer(c_intptr_t), target :: BuffId
C------------------------------------------------------------------------

C---     Copy CPU data to device buffer
      CmdQ   = INT(CQ,  c_intptr_t)
      BuffId = INT(BID, c_intptr_t)
      ioff  = 0
      isize = N * c_float
      IRET = clEnqueueReadBuffer (CmdQ,
     &                            BuffId,
     &                            CL_TRUE,
     &                            ioff,
     &                            isize,
     &                            C_LOC(V),
     &                            0,
     &                            C_NULL_PTR,
     &                            C_NULL_PTR)

      GP_CUDA_V2CPU = GP_CUDA_ERRMSG(IRET)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime CUDA
C
C Description:
C     La fonction GP_CUDA_PRN imprime les plates-formes CUDA et leurs
C     devices.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRN()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_CUDA_PRN
CDEC$ ENDIF

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INCLUDE 'gpcuda.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IERR
      INTEGER IP
      INTEGER(c_int) :: NIDS, NTMP
      INTEGER(c_intptr_t), ALLOCATABLE, TARGET :: PIDS(:)
C------------------------------------------------------------------------------

      WRITE(LOG_BUF, *) 'CUDA configuration'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Get platforms number
      IERR = clGetPlatformIDs(0, C_NULL_PTR, NIDS)
      IF (IERR .NE. CL_SUCCESS .OR. NIDS .LT. 1) GOTO 9901
      WRITE(LOG_BUF,'(A,I6)') 'MSG_NBR_PLATFORMS#<20>#: ', NIDS
      CALL LOG_ECRIS(LOG_BUF)

C---     Get platforms IDs
      ALLOCATE( PIDS(NIDS) )
      IERR = clGetPlatformIDs(NIDS, C_LOC(PIDS), NTMP)
      IF (IERR .NE. CL_SUCCESS) GOTO 9903
      CALL ERR_ASR(NIDS .EQ. NTMP)

C---     Imprime chaque plate-forme
      DO IP=1,NIDS
         WRITE(LOG_BUF,'(A,I6)') 'MSG_PLATFORM#<20>#: ', IP
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         IERR = GP_CUDA_PRN1PTF(PIDS(IP))
         CALL LOG_DECIND()
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_CU_INVALID_PTFM_COUNT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') ''
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(PIDS)) DEALLOCATE(PIDS)
      CALL LOG_DECIND()
      GP_CUDA_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRN1PTF imprime les données d'une
C     plate-forme.
C
C Entrée:
C     PID      CUDA handle on platform to query.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRN1PTF(PID)

      USE CUDA
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_intptr_t), INTENT(IN) :: PID

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IERR
      INTEGER I

      !INTEGER(c_size_t)  :: LRET
      !INTEGER(c_int32_t) :: INFO32, V32
      !INTEGER(c_int64_t) :: INFO64
      INTEGER(c_int) :: NIDS

      !INTEGER, PARAMETER :: BUFSIZ = 256
      !CHARACTER*(BUFSIZ), TARGET :: BUF
C------------------------------------------------------------------------------

!!!C---     Name
!!!      INFO32= CL_PLATFORM_NAME
!!!      IERR = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
!!!      IF (IERR .NE. CL_SUCCESS .OR. LRET .GE. BUFSIZ) GOTO 9900
!!!      WRITE(LOG_BUF,'(2A)') 'MSG_NAME#<20>#: ', BUF(1:LRET-1)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!C---     Vendor
!!!      INFO32= CL_PLATFORM_VENDOR
!!!      IERR = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(buf), lret)
!!!      IF (IERR .NE. CL_SUCCESS .OR. lret .GE. BUFSIZ) GOTO 9900
!!!      WRITE(LOG_BUF,'(2A)') 'MSG_VENDOR#<20>#: ', BUF(1:LRET-1)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!C---     Version
!!!      INFO32= CL_PLATFORM_VERSION
!!!      IERR = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
!!!      IF (IERR .NE. CL_SUCCESS .OR. lret .GE. BUFSIZ) GOTO 9900
!!!      WRITE(LOG_BUF,'(2A)') 'MSG_VERSION#<20>#: ', BUF(1:LRET-1)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!C---     Profile
!!!      INFO32= CL_PLATFORM_PROFILE
!!!      IERR = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
!!!      IF (IERR .NE. CL_SUCCESS .OR. lret .GE. BUFSIZ) GOTO 9900
!!!      WRITE(LOG_BUF,'(2A)') 'MSG_PROFILE#<20>#: ', BUF(1:LRET-1)
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!
!!!C---     Extensions
!!!      INFO32= CL_PLATFORM_EXTENSIONS
!!!      IERR = clGetPlatformInfo(PID, INFO32, BUFSIZ, C_LOC(BUF), LRET)
!!!      IF (IERR .NE. CL_SUCCESS .OR. lret .GE. BUFSIZ) GOTO 9900
!!!      WRITE(LOG_BUF,'(2A)') 'MSG_EXTENSIONS#<20>#: ', BUF(1:LRET-1)
!!!      CALL LOG_ECRIS(LOG_BUF)

C---     Device count
      IERR = cudaGetDeviceCount(NIDS)
      IF (IERR .NE. cudaSuccess .OR. NIDS < 1) GOTO 9901
      WRITE(LOG_BUF,'(A,I6)') 'MSG_NBR_DEVICES#<20>#: ', NIDS
      CALL LOG_ECRIS(LOG_BUF)

C---     Loop over devices
      DO I=0,NIDS-1
         WRITE(LOG_BUF,'(A,I6)') 'MSG_DEVICE#<20>#: ', I+1
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()

         IERR = GP_CUDA_PRN1DEV(I)

         CALL LOG_DECIND()
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'Error querying platform: ', IERR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,*) 'No devices found: ', IERR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_PRN1PTF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRN1DEV imprime les données d'un device.
C
C Entrée:
C     DID      CUDA handle on device to print-out.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRN1DEV(ID)

      USE CUDA
      IMPLICIT NONE

      INTEGER(c_int), INTENT(IN) :: ID

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER, PARAMETER :: UN_KB = 1024
      INTEGER, PARAMETER :: UN_MB = 1024*UN_KB
      INTEGER, PARAMETER :: UN_GB = 1024*UN_MB
      INTEGER, PARAMETER :: BUFSIZ = 256

      INTEGER IERR, IE, I

      INTEGER(c_size_t)  :: VSZ
      INTEGER(c_int64_t) :: V64
      INTEGER(c_int32_t) :: V32
      CHARACTER*(BUFSIZ) :: BUF

      type (cudaDeviceProp) :: prop
C------------------------------------------------------------------------------
      INTEGER DI_32, DI_64, DI_SZ, DI_NS, DI_CH
      CHARACTER*(64) :: VCH, N
      DI_32(V32, N) = GP_CUDA_PRN32(V32, N(1:SP_STRN_LEN(N)))
      DI_64(V64, N) = GP_CUDA_PRN64(V64, N(1:SP_STRN_LEN(N)))
      DI_SZ(VSZ, N) = GP_CUDA_PRNSZ(VSZ, N(1:SP_STRN_LEN(N)))
      DI_NS(V32, N) = GP_CUDA_PRNNS(V32, N(1:SP_STRN_LEN(N)))
      DI_CH(VCH, N) = GP_CUDA_PRNCH(VCH, N(1:SP_STRN_LEN(N)))
C------------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.gpu.config.cuda'

C---     Get info from device
      IERR = cudaGetDeviceProperties(prop, ID)
      IF (IERR .NE. cudaSuccess) GOTO 9900

C---     Info
      BUF = " "
      I = 1
      DO WHILE (PROP%NAME(I) .NE. C_NULL_CHAR)
         BUF(I:I) = PROP%NAME(I)
         I = I + 1
      END DO
      WRITE(LOG_BUF,'(2A)') 'MSG_NAME#<30>#: ', BUF(1:SP_STRN_LEN(BUF))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      V64 = prop%totalGlobalMem
      WRITE(LOG_BUF,'(A,F12.3,A)') 'GLOBAL_MEM_SIZE#<30>#: ',
     &                              DBLE(V64)/UN_GB, ' (GB)'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      V64 = prop%multiProcessorCount * prop%warpSize
      WRITE(LOG_BUF,'(A,I6)') 'MAX_COMPUTE_UNITS#<30>#: ', V64
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      V64 = prop%multiProcessorCount
      WRITE(LOG_BUF,'(A,I6)') 'MAX_WORK_GROUP_SIZE#<30>#: ', V64
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)


C---     Complete device info
      CALL LOG_DBG(LOG_ZNE, 'Complete device info:')
!!      char   name#<256>#, 'ASCII string identifying device')
      IE=DI_SZ(prop%totalGlobalMem,
     &         'Global memory available on device')
      IE=DI_SZ(prop%sharedMemPerBlock,
     &         'Shared memory available per block')
      IE=DI_32(prop%regsPerBlock,
     &         '32-bit registers available per block')
      IE=DI_32(prop%warpSize,
     &         'Warp size in threads')
      IE=DI_SZ(prop%memPitch,
     &         'Maximum pitch in bytes allowed by memory copies')
      IE=DI_32(prop%maxThreadsPerBlock,
     &         'Maximum number of threads per block')
      IE=DI_NS(prop%maxThreadsDim(1),
     &         'Maximum size of each dimension of a block')
      IE=DI_NS(prop%maxGridSize(1),
     &         'Maximum size of each dimension of a grid')
      IE=DI_32(prop%clockRate,
     &         'Clock frequency in kilohertz')
      IE=DI_SZ(prop%totalConstMem,
     &         'Constant memory available on device in bytes')
      IE=DI_32(prop%major,
     &         'Major compute capability')
      IE=DI_32(prop%minor,
     &         'Minor compute capability')
      IE=DI_SZ(prop%textureAlignment,
     &         'Alignment requirement for textures')
!      IE=DI_SZ(prop%texturePitchAlignment,
!     &         'Pitch alignment requirement for texture references bound to pitched memory')
!      IE=DI_32(prop%deviceOverlap,
!     &         'Device can concurrently copy memory and execute a kernel. Deprecated. Use instead asyncEngineCount.')
      IE=DI_32(prop%multiProcessorCount,
     &         'Number of multiprocessors on device')
      IE=DI_32(prop%kernelExecTimeoutEnabled,
     &         'Specified whether there is a run time limit on kernels')
      IE=DI_32(prop%integrated,
     &         'Device is integrated as opposed to discrete')
!      IE=DI_32(prop%canMapHostMemory,
!     &         'Device can map host memory with cudaHostAlloc"cudaHostGetDevicePointer"')
      IE=DI_32(prop%computeMode,
     &         'Compute mode (See ::cudaComputeMode)')
      IE=DI_32(prop%maxTexture1D,
     &         'Maximum 1D texture size')
      IE=DI_32(prop%maxTexture1DMipmap,
     &         'Maximum 1D mipmapped texture size')
      IE=DI_32(prop%maxTexture1DLinear,
     &         'Maximum size for 1D textures bound to linear memory')
      IE=DI_NS(prop%maxTexture2D(1),
     &         'Maximum 2D texture dimensions')
      IE=DI_NS(prop%maxTexture2DMipmap(1),
     &         'Maximum 2D mipmapped texture dimensions')
!      IE=DI_NS(prop%maxTexture2DLinear,
!     &         'Maximum dimensions (width, height, pitch) for 2D textures bound to pitched memory')
!      IE=DI_NS(prop%maxTexture2DGather,
!     &          'Maximum 2D texture dimensions if texture gather operations have to be performed')
      IE=DI_NS(prop%maxTexture3D(1),
     &         'Maximum 3D texture dimensions')
      IE=DI_NS(prop%maxTexture3DAlt(1),
     &         'Maximum alternate 3D texture dimensions')
      IE=DI_32(prop%maxTextureCubemap,
     &         'Maximum Cubemap texture dimensions')
      IE=DI_NS(prop%maxTexture1DLayered(1),
     &         'Maximum 1D layered texture dimensions')
      IE=DI_NS(prop%maxTexture2DLayered(1),
     &         'Maximum 2D layered texture dimensions')
      IE=DI_NS(prop%maxTextureCubemapLayered(1),
     &         'Maximum Cubemap layered texture dimensions')
      IE=DI_32(prop%maxSurface1D,
     &         'Maximum 1D surface size')
      IE=DI_NS(prop%maxSurface2D(1),
     &         'Maximum 2D surface dimensions')
      IE=DI_NS(prop%maxSurface3D(1),
     &         'Maximum 3D surface dimensions')
      IE=DI_NS(prop%maxSurface1DLayered(1),
     &         'Maximum 1D layered surface dimensions')
      IE=DI_NS(prop%maxSurface2DLayered(1),
     &         'Maximum 2D layered surface dimensions')
      IE=DI_32(prop%maxSurfaceCubemap,
     &         'Maximum Cubemap surface dimensions')
      IE=DI_NS(prop%maxSurfaceCubemapLayered(1),
     &         'Maximum Cubemap layered surface dimensions')
      IE=DI_SZ(prop%surfaceAlignment,
     &         'Alignment requirements for surfaces')
!      IE=DI_32(prop%concurrentKernels,
!     &         'Device can possibly execute multiple kernels concurrently')
      IE=DI_32(prop%ECCEnabled,
     &         'Device has ECC support enabled')
      IE=DI_32(prop%pciBusID,
     &         'PCI bus ID of the device')
      IE=DI_32(prop%pciDeviceID,
     &         'PCI device ID of the device')
      IE=DI_32(prop%pciDomainID,
     &         'PCI domain ID of the device')
!      IE=DI_32(prop%tccDriver,
!     &         '1 if device is a Tesla device using TCC driver, 0 otherwise')
      IE=DI_32(prop%asyncEngineCount,
     &         'Number of asynchronous engines')
      IE=DI_32(prop%unifiedAddressing,
     &         'Device shares a unified address space with the host')
      IE=DI_32(prop%memoryClockRate,
     &         'Peak memory clock frequency in kilohertz')
      IE=DI_32(prop%memoryBusWidth,
     &         'Global memory bus width in bits')
      IE=DI_32(prop%l2CacheSize,
     &         'Size of L2 cache in bytes')
      IE=DI_32(prop%maxThreadsPerMultiProcessor,
     &         'Maximum resident threads per multiprocessor')
      IE=DI_32(prop%streamPrioritiesSupported,
     &         'Device supports stream priorities')
      IE=DI_32(prop%globalL1CacheSupported,
     &         'Device supports caching globals in L1')
      IE=DI_32(prop%localL1CacheSupported,
     &         'Device supports caching locals in L1')
      IE=DI_SZ(prop%sharedMemPerMultiprocessor,
     &         'Shared memory available per multiprocessor in bytes')
      IE=DI_32(prop%regsPerMultiprocessor,
     &         '32-bit registers available per multiprocessor')
!      IE=DI_32(prop%managedMemory,
!     &         'Device supports allocating managed memory on this system')
      IE=DI_32(prop%isMultiGpuBoard,
     &         'Device is on a multi-GPU board')
!      IE=DI_32(prop%multiGpuBoardGroupID,
!     &         'Unique identifier for a group of devices on the same multi-GPU board')

      GOTO 9999
C------------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'Error querying device: ', IERR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GP_CUDA_PRN1DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRN32 imprime les données d'un device.
C
C Entrée:
C     DID      CUDA handle on device to print-out.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRN32(VAL, TXT)

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_int), INTENT(IN) :: VAL
      CHARACTER*(*),  INTENT(IN) :: TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'
C------------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.config.cuda'

      WRITE(LOG_BUF,'(2A,I18)') TXT, '#<50>#: ', VAL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      GP_CUDA_PRN32 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRN64 imprime les données I64.
C
C Entrée:
C     DID      CUDA handle on device to print-out.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRN64(VAL, TXT)

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_int64_t), INTENT(IN) :: VAL
      CHARACTER*(*),      INTENT(IN) :: TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'
C------------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.config.cuda'

      IF (VAL .LT. 2**12) THEN
         WRITE(LOG_BUF,'(2A,B16.16)') TXT, '#<50>#: 0b', VAL
      ELSE
         WRITE(LOG_BUF,'(2A,Z16.16)') TXT, '#<50>#: 0x', VAL
      ENDIF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      GP_CUDA_PRN64 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRNSZ imprime les données d'un device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRNSZ(VAL, TXT)

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_size_t), INTENT(IN) :: VAL
      CHARACTER*(*),     INTENT(IN) :: TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'

      INTEGER IERR

      INTEGER, PARAMETER :: UN_KB = 1024
      INTEGER, PARAMETER :: UN_MB = 1024*1024
      INTEGER, PARAMETER :: UN_GB = 1024*1024*1024
C------------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.config.cuda'

      IF (VAL .GT. UN_MB) THEN
         WRITE(LOG_BUF,'(2A,F18.3,A)') TXT, '#<50>#: ',
     &                                    DBLE(VAL)/UN_GB, ' (GB)'
      ELSEIF (VAL .GT. UN_KB) THEN
         WRITE(LOG_BUF,'(2A,F18.3,A)') TXT, '#<50>#: ',
     &                                    DBLE(VAL)/UN_KB, ' (KB)'
      ELSE
         WRITE(LOG_BUF,'(2A,I18)') TXT, '#<50>#: ', VAL
      ENDIF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      GP_CUDA_PRNSZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRN1DINS imprime les données d'un device.
C
C Entrée:
C     DID      CUDA handle on device to print-out.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRNNS(VAL, TXT)

      USE clfortran
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER(c_int), INTENT(IN) :: VAL(*)
      CHARACTER*(*),  INTENT(IN) :: TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpcuda.fc'

C------------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.config.cuda'

      WRITE(LOG_BUF,'(2A,I18)') TXT, '#<50>#: ', VAL(1)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'( A,I18)')      '#<50>#: ', VAL(2)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'( A,I18)')      '#<50>#: ', VAL(3)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      GP_CUDA_PRNNS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GP_CUDA_PRNCH imprime les données d'un device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_CUDA_PRNCH(VAL, TXT)

      USE ISO_C_BINDING
      IMPLICIT NONE

      CHARACTER*(*), INTENT(IN) :: VAL
      CHARACTER*(*), INTENT(IN) :: TXT

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpcuda.fc'

C------------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.gpu.config.cuda'

      WRITE(LOG_BUF,'(3A)') TXT,'#<50>#: ', VAL(1:SP_STRN_LEN(VAL))
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      GP_CUDA_PRNCH = ERR_TYP()
      RETURN
      END
