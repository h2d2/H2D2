C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GP_DEVC_000
C     INTEGER GP_DEVC_999
C     INTEGER GP_DEVC_CTR
C     INTEGER GP_DEVC_DTR
C     INTEGER GP_DEVC_INI
C     INTEGER GP_DEVC_RST
C     INTEGER GP_DEVC_REQHBASE
C     LOGICAL GP_DEVC_HVALIDE
C     INTEGER*8 GP_DEVC_REQMIN_CU
C     INTEGER*8 GP_DEVC_REQMAX_CU
C     INTEGER*8 GP_DEVC_REQMIN_WGSZ
C     INTEGER*8 GP_DEVC_REQMAX_WGSZ
C     INTEGER*8 GP_DEVC_REQGBL_MMSZ
C     INTEGER GP_DEVC_ASGNCPU
C     INTEGER*8 GP_DEVC_REQNCPU
C     INTEGER GP_DEVC_ASGNTSK
C     INTEGER GP_DEVC_ASGITSK
C     INTEGER*8 GP_DEVC_REQNTSK
C     INTEGER GP_DEVC_ASGNWGP
C     INTEGER*8 GP_DEVC_REQNWGP
C     INTEGER*8 GP_DEVC_REQGWI
C     INTEGER GP_DEVC_ASGGMEM
C     INTEGER*8 GP_DEVC_REQGMEM
C     INTEGER GP_DEVC_START
C     INTEGER GP_DEVC_ALLOC
C     INTEGER GP_DEVC_ALLINT
C     INTEGER GP_DEVC_ALLRE8
C     INTEGER GP_DEVC_B2CPU
C     INTEGER GP_DEVC_K2CPU
C     INTEGER GP_DEVC_V2CPU
C     INTEGER GP_DEVC_B2DEV
C     INTEGER GP_DEVC_K2DEV
C     INTEGER GP_DEVC_V2DEV
C     INTEGER GP_DEVC_SETARG_BUF
C     INTEGER GP_DEVC_LOADK
C     INTEGER GP_DEVC_XEQK
C     INTEGER GP_DEVC_PRINT
C     INTEGER GP_DEVC_PRNALL
C   Private:
C     INTEGER GP_DEVC_RAZ
C     INTEGER GP_DEVC_CP2CPU
C     INTEGER GP_DEVC_CP2DEV
C
C************************************************************************

! La classe GP_DEVC représente un device physique (carte GPU) avec ses capacités.
! Un device à plusieurs groupes de calcul (work_group).
!
! Interface F77 associé au module GP_DEVC_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction GP_DEVC_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_000
CDEC$ ENDIF

      USE GP_DEVC_M

      IMPLICIT NONE

      INCLUDE 'gpdevc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(GP_DEVC_HBASE, 'GPU - Device')

      GP_DEVC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction GP_DEVC_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_999
CDEC$ ENDIF

      USE GP_DEVC_M

      IMPLICIT NONE

      INCLUDE 'gpdevc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL GP_DEVC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(GP_DEVC_HBASE, GP_DEVC_DTR)

      GP_DEVC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction GP_DEVC_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_CTR(HOBJ, ITYP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_CTR
CDEC$ ENDIF

      USE GP_DEVC_M
      !!!USE GP_DVOCL_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ITYP

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR, IRET
      TYPE (GP_DEVC_SELF_T),  POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   GP_DEVC_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = GP_DEVC_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. GP_DEVC_HVALIDE(HOBJ))

C---     Dispatch suivant le type
      !!!IF     (ITYP .EQ. GP_DEVC_TYP_OMP) THEN
      !!!   IERR = GP_DVOCL_CTR(SELF%DEVP)
      !!!ELSEIF (ITYP .EQ. GP_DEVC_TYP_OPENCL) THEN
      !!!   IERR = GP_DVOCL_CTR(SELF%DEVP)
      !!!ELSEIF (ITYP .EQ. GP_DEVC_TYP_CUDA) THEN
      !!!   IERR = GP_DVOCL_CTR(SELF%DEVP)
      !!!ELSE
      !!!   CALL ERR_ASR(.FALSE.)
      !!!ENDIF
      CALL ERR_ASR(.FALSE.)

C---     Assigne le type
      IF (ERR_GOOD()) SELF%ITYP = ITYP

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GP_DEVC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction GP_DEVC_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_DTR
CDEC$ ENDIF

      USE GP_DEVC_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = GP_DEVC_RST(HOBJ)

C---     Dé-alloue
      SELF => GP_DEVC_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, GP_DEVC_HBASE)

      GP_DEVC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée GP_DEVC_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_RAZ(HOBJ)

      USE GP_DEVC_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
      TYPE(GP_DEVICE_INFO_T) :: INFO0
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%INFO      = INFO0
      SELF%NCPU_ALL  = 0
      SELF%NTSK_ALL  = 0
      SELF%NWGP_ALL  = 0
      SELF%GMEM_ALL  = 0
      SELF%ITSK_ALL  = 0

      GP_DEVC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise.
C
C Description:
C     La fonction GP_DEVC_INI initialise l'objet à l'aide des paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_INI(HOBJ, IP, ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_INI
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IP        ! Indice de la plate-forme
      INTEGER ID        ! Indice du device

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GP_DEVC_RST(HOBJ)

      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_INI_VIRT(SELF%INFO, IP, ID)

      GP_DEVC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_RST
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les paramètres
      IERR = GP_DEVC_RAZ(HOBJ)

      GP_DEVC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GP_DEVC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQHBASE
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER GP_DEVC_REQHBASE
C------------------------------------------------------------------------

      GP_DEVC_REQHBASE = GP_DEVC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GP_DEVC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_HVALIDE
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      GP_DEVC_HVALIDE = OB_OBJN_HVALIDE(HOBJ, GP_DEVC_HBASE)
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: .
!!!C
!!!C Description:
!!!C     La fonction GP_DEVC_REQHWPID retourne le hardware platform id.
!!!C
!!!C Entrée:
!!!C     HOBJ     Handle sur l'objet courant
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION GP_DEVC_REQHWPID(HOBJ)
!!!C$pragma aux GP_DEVC_REQHWPID export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: GP_DEVC_REQHWPID
!!!
!!!      USE GP_DEVC_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!
!!!      INCLUDE 'gpdevc.fi'
!!!      INCLUDE 'err.fi'
!!!
!!!      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      SELF => GP_DEVC_REQSELF(HOBJ)
!!!      GP_DEVC_REQHWPID = SELF%PLATFORM_ID
!!!      RETURN
!!!      END

!!!C************************************************************************
!!!C Sommaire: .
!!!C
!!!C Description:
!!!C     La fonction GP_DEVC_REQHWDID retourne le hardware device id.
!!!C
!!!C Entrée:
!!!C     HOBJ     Handle sur l'objet courant
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION GP_DEVC_REQHWDID(HOBJ)
!!!C$pragma aux GP_DEVC_REQHWDID export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: GP_DEVC_REQHWDID
!!!
!!!      USE GP_DEVC_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!
!!!      INCLUDE 'gpdevc.fi'
!!!      INCLUDE 'err.fi'
!!!
!!!      TYPE (GP_DEVC_SELF_T), POINTER :: SELF
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      SELF => GP_DEVC_REQSELF(HOBJ)
!!!      GP_DEVC_REQHWDID = SELF%DEVICE_ID
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire: Retourne le type du device.
C
C Description:
C     La fonction GP_DEVC_REQNGPU retourne le nombre de tâches GPU du device
C     qui sont alloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQITYP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQITYP
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQITYP = SELF%ITYP
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre min de Compute-Units.
C
C Description:
C     La fonction GP_DEVC_REQMIN_CU le nombre min de Compute-Units.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQMIN_CU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQMIN_CU
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQMIN_CU = SELF%INFO%MIN_CU_PER_DEV
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre max de Compute-Units.
C
C Description:
C     La fonction GP_DEVC_REQMAX_CU le nombre max de Compute-Units.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQMAX_CU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQMAX_CU
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQMAX_CU = SELF%INFO%MAX_CU_PER_DEV
      RETURN
      END

C************************************************************************
C Sommaire: .
C
C Description:
C     La fonction GP_DEVC_REQMIN_WGSZ
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQMIN_WGSZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQMIN_WGSZ
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQMIN_WGSZ = SELF%INFO%MIN_WITM_PER_WGRP
      RETURN
      END

C************************************************************************
C Sommaire: WorgGoup size.
C
C Description:
C     La fonction GP_DEVC_REQMAX_WGSZ
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQMAX_WGSZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQMAX_WGSZ
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQMAX_WGSZ = SELF%INFO%MAX_WITM_PER_WGRP
      RETURN
      END

C************************************************************************
C Sommaire: Global memory size
C
C Description:
C     La fonction GP_DEVC_REQGBL_MMSZ
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQGBL_MMSZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQGBL_MMSZ
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQGBL_MMSZ = SELF%INFO%MEM_SIZE
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le nombre de CPU.
C
C Description:
C     La fonction GP_DEVC_ASGNCPU assigne au device le nombre de CPU
C     (tâches OMP) alloués.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NCPU     Nombre de CPU alloués
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ASGNCPU(HOBJ, NCPU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ASGNCPU
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCPU

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%NCPU_ALL = NCPU

      GP_DEVC_ASGNCPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de CPU.
C
C Description:
C     La fonction GP_DEVC_REQNCPU retourne le nombre de CPU (tâches OMP)
C     allouées au device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQNCPU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQNCPU
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQNCPU = SELF%NCPU_ALL
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le nombre de TSK.
C
C Description:
C     La fonction GP_DEVC_ASGNTSK assigne au device le nombre de TSK
C     alloués.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NTSK     Nombre de TSK alloués
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ASGNTSK(HOBJ, NTSK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ASGNTSK
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NTSK

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%NTSK_ALL = NTSK

      GP_DEVC_ASGNTSK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le nombre de TSK.
C
C Description:
C     La fonction GP_DEVC_ASGITSK assigne au device l'indice de la première
C     tâche.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     TSKI     Nombre de TSK alloués
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ASGITSK(HOBJ, TSKI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ASGITSK
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER TSKI

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%ITSK_ALL = TSKI

      GP_DEVC_ASGITSK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de tâches GPU.
C
C Description:
C     La fonction GP_DEVC_REQNGPU retourne le nombre de tâches GPU du device
C     qui sont alloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQNTSK(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQNTSK
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQNTSK = SELF%NTSK_ALL
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le nombre de groupe de TSK.
C
C Description:
C     La fonction GP_DEVC_ASGNTSK assigne au device le nombre de groupes
C     de TSK alloués.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NWGP     Nombre de groupes de TSK alloués
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ASGNWGP(HOBJ, NWGP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ASGNWGP
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NWGP

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%NWGP_ALL = NWGP

      GP_DEVC_ASGNWGP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de groupes de tâches GPU.
C
C Description:
C     La fonction GP_DEVC_REQNWGP retourne le nombre de groupes de
C     tâches GPU du device qui sont alloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQNWGP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQNWGP
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQNWGP = SELF%NWGP_ALL
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre total de Work-Items.
C
C Description:
C     La fonction GP_DEVC_REQGWI retourne le nombre total de Work-Items.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     SELF%MAX_CU_PER_DEV * SELF%GP_DEVC_REQMAX_WGSZ
C     ne représente pas le nombre max de work-Item
C************************************************************************
      FUNCTION GP_DEVC_REQGWI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQGWI
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      INTEGER N1, N2
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      N1 = SELF%INFO%MAX_CU_PER_DEV  *
     &     SELF%INFO%MAX_WGRP_PER_CU *
     &     SELF%INFO%MAX_WITM_PER_WGRP
      N2 = SELF%INFO%MAX_WITM_PER_DEV

      GP_DEVC_REQGWI = MIN(N1, N2)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne la mémoire totale nécessaire pour un élémnet.
C
C Description:
C     La fonction GP_DEVC_ASGGMEM assigne la mémoire nécessaire pour
C     calculer un élément.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     GMEM     Mémoire globale allouée
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ASGGMEM(HOBJ, GMEM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ASGGMEM
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 GMEM

      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fi'

      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      SELF%GMEM_ALL = GMEM

      GP_DEVC_ASGGMEM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la mémoire totale utilisée
C
C Description:
C     La fonction GP_DEVC_REQGMEM retourne la mémoire totale
C     utilisée du device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_REQGMEM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_REQGMEM
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gpdevc.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      GP_DEVC_REQGMEM = SELF%GMEM_ALL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_START
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_START(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_START
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_START_VIRT()

      GP_DEVC_START = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GP_DEVC_ALLOC
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ALLOC(HOBJ, ILEN, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ALLOC
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: ILEN
      INTEGER(8),INTENT(OUT):: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER :: IERR
      INTEGER(8) :: ISIZ
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      ISIZ = ILEN
      IERR = SELF%DEVP%GP_DEVC_ALLOC_VIRT(ISIZ, BID)

      GP_DEVC_ALLOC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode GP_DEVC_ALLRE8 est une méthode utilitaire d'allocation
C     d'une table INTEGER.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ALLINT(HOBJ, ILEN, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ALLINT
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: ILEN
      INTEGER(8),INTENT(OUT):: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      INTEGER :: IERR
      INTEGER :: IDUM
      INTEGER :: ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = ILEN * SIZEOF(IDUM)
      IERR = GP_DEVC_ALLOC(HOBJ, ISIZ, BID)

      GP_DEVC_ALLINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode GP_DEVC_ALLRE8 est une méthode utilitaire d'allocation
C     d'une table REAL*8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_ALLRE8(HOBJ, ILEN, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_ALLRE8
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN)  :: HOBJ
      INTEGER,   INTENT(IN)  :: ILEN
      INTEGER(8),INTENT(OUT) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER :: IERR
      REAL*8  :: VDUM
      INTEGER :: ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = ILEN * SIZEOF(VDUM)
      IERR = GP_DEVC_ALLOC(HOBJ, ISIZ, BID)

      GP_DEVC_ALLRE8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction GP_DEVC_B2CPU copie la table BYTE depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_B2CPU(HOBJ, N, B, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_B2CPU
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN)    :: HOBJ
      INTEGER,   INTENT(IN)    :: N
      BYTE,      INTENT(INOUT) :: B(:)
      INTEGER(8),INTENT(IN)    :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(B(1))
      IERR = GP_DEVC_CP2CPU(HOBJ,
     &                      ISIZ,
     &                      B,
     &                      BID)

      GP_DEVC_B2CPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction GP_DEVC_K2CPU copie la table INTEGER depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_K2CPU(HOBJ, N, K, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_K2CPU
CDEC$ ENDIF

      USE GP_DEVC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN)    :: HOBJ
      INTEGER,   INTENT(IN)    :: N
      INTEGER,   INTENT(INOUT) :: K(:)
      INTEGER(8),INTENT(IN)    :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
      BYTE, POINTER :: K_B(:)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(K(1))
      K_B => SO_ALLC_CST2B(K)
      IERR = GP_DEVC_CP2CPU(HOBJ,
     &                      ISIZ,
     &                      K_B,
     &                      BID)

      GP_DEVC_K2CPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction GP_DEVC_V2CPU copie la table REAL*8 depuis le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_V2CPU(HOBJ, N, V, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_V2CPU
CDEC$ ENDIF

      USE GP_DEVC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN)    :: HOBJ
      INTEGER,   INTENT(IN)    :: N
      REAL*8,    INTENT(INOUT) :: V(:)
      INTEGER*8, INTENT(IN)    :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
      BYTE, POINTER :: V_B(:)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(V(1))
      V_B => SO_ALLC_CST2B(V)
      IERR = GP_DEVC_CP2CPU(HOBJ,
     &                      ISIZ,
     &                      V_B,
     &                      BID)

      GP_DEVC_V2CPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction GP_DEVC_B2DEV copie la table BYTE vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_B2DEV(HOBJ, N, B, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_B2DEV
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      BYTE,      INTENT(IN) :: B(:)
      INTEGER(8),INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(B(1))
      IERR = GP_DEVC_CP2DEV(HOBJ,
     &                      ISIZ,
     &                      B,
     &                      BID)

      GP_DEVC_B2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La fonction GP_DEVC_K2DEV copie la table INTEGER vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_K2DEV(HOBJ, N, K, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_K2DEV
CDEC$ ENDIF

      USE GP_DEVC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER,   INTENT(IN) :: HOBJ
      INTEGER,   INTENT(IN) :: N
      INTEGER,   INTENT(IN) :: K(:)
      INTEGER(8),INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(K(1))
      IERR = GP_DEVC_CP2DEV(HOBJ,
     &                      ISIZ,
     &                      SO_ALLC_CST2B(K),
     &                      BID)

      GP_DEVC_K2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie une table.
C
C Description:
C     La méthode GP_DEVC_V2DEV copie la table REAL*8 vers le device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_V2DEV(HOBJ, N, V, BID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_V2DEV
CDEC$ ENDIF

      USE GP_DEVC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER,  INTENT(IN) :: HOBJ
      INTEGER,  INTENT(IN) :: N
      REAL*8,   INTENT(IN) :: V(:)
      INTEGER*8,INTENT(IN) :: BID

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpdevc.fc'

      INTEGER IERR
      INTEGER ISIZ
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISIZ = N * SIZEOF(V(1))
      IERR = GP_DEVC_CP2DEV(HOBJ,
     &                      ISIZ,
     &                      SO_ALLC_CST2B(V),
     &                      BID)

      GP_DEVC_V2DEV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Associe une table avec un argument.
C
C Description:
C     La fonction GP_DEVC_SET_ARG_BUF
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_SETARG_BUF(HOBJ, I, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_SETARG_BUF
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER,    INTENT(IN) :: HOBJ
      INTEGER,    INTENT(IN) :: I
      INTEGER(8), INTENT(IN) :: V

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_ARGBUF_VIRT(I, V)

      GP_DEVC_SETARG_BUF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode GP_DEVC_LOADK charge un kernel.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAM     Nom du fichier
C     KNAM     Nom du kernel
C
C Sortie:
C     INFO(1) = LOCAL_MEM_SIZE
C     INFO(2) = PRIVATE_MEM_SIZE
C     INFO(3) = PREFERRED_WORK_GROUP_SIZE_MULTIPLE
C     INFO(4) = WORK_GROUP_SIZE
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_LOADK(HOBJ, FNAM, KNAM, INFO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_LOADK
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      CHARACTER*(*), INTENT(IN) :: FNAM
      CHARACTER*(*), INTENT(IN) :: KNAM
      INTEGER,       INTENT(OUT):: INFO(4)

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      INTEGER :: IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_LOADK_VIRT(FNAM, KNAM, INFO)

      GP_DEVC_LOADK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode GP_DEVC_XEQK exécute un kernel.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_XEQK(HOBJ, NTSK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_XEQK
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NTSK

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'

      INTEGER :: IERR
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
D     CALL ERR_PRE(NTSK .GT. 0)
C------------------------------------------------------------------------

      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_XEQK_VIRT(NTSK)

      GP_DEVC_XEQK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Print la configuration des devices.
C
C Description:
C     La méthode GP_DEVC_PRINT imprime la configuration du device.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_PRINT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_PRINT
CDEC$ ENDIF

      USE GP_DEVC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER :: IERR
      INTEGER :: IUNIT
      CLASS (GP_DEVC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(GP_DEVC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IUNIT = LOG_REQMP()
      SELF => GP_DEVC_REQSELF(HOBJ)
      IERR = SELF%DEVP%GP_DEVC_PRN_VIRT(IUNIT)

      GP_DEVC_PRINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Print la configuration de tous les devices.
C
C Description:
C     La fonction statique GP_DEVC_PRNALL
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GP_DEVC_PRNALL(ITYP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GP_DEVC_PRNALL
CDEC$ ENDIF

      !!!USE OCL_SYSTEM_M, ONLY : PRINT_ALL
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: ITYP      ! Type de plate-forme

      INCLUDE 'gpdevc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER :: IERR
      INTEGER :: IUNIT
C------------------------------------------------------------------------

!!!      IUNIT = LOG_REQMP()
!!!C---     Dispatch suivant le type
!!!      IF     (ITYP .EQ. GP_DEVC_TYP_OMP) THEN
!!!      !!   IERR = GP_DEVC_OMP_PRNALL()
!!!      ELSEIF (ITYP .EQ. GP_DEVC_TYP_OPENCL) THEN
!!!!!         IERR = GP_DEVC_CL_PRNALL()
!!!         IERR = PRINT_ALL(IUNIT)
!!!      ELSEIF (ITYP .EQ. GP_DEVC_TYP_CUDA) THEN
!!!      !!   IERR = GP_DEVC_CU_PRNALL()
!!!      ELSE
!!!         CALL ERR_ASR(.FALSE.)
!!!      ENDIF
      CALL ERR_ASR(.FALSE.)

      GP_DEVC_PRNALL = ERR_TYP()
      RETURN
      END
