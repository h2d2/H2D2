C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_GPU_REQMDL
C     INTEGER IC_GPU_OPBDOT
C     CHARACTER*(32) IC_GPU_REQNOM
C     INTEGER IC_GPU_REQHDL
C   Private:
C     INTEGER IC_GPU_XEQGPU
C     INTEGER IC_GPU_XEQCL
C     INTEGER IC_GPU_XEQCU
C     INTEGER IC_GPU_XEQOMP
C     SUBROUTINE IC_GPU_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GPU_REQMDL(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GPU_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GPU_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les paramètres
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
         HOBJ = IC_GPU_REQHDL()
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  <b>gpu</b> returns the handle on the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GPU_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - IC_GPU_REQHDL()
      IF     (IOB .EQ. IC_GPU_MDL_GPU) THEN
C        <include>IC_GPU_XEQGPU@gpu_ic.for</include>
         IERR = IC_GPU_XEQGPU(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_GPU_MDL_CL) THEN
         IERR = IC_GPU_XEQCL(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_GPU_MDL_CU) THEN
         IERR = IC_GPU_XEQCU(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_GPU_MDL_OMP) THEN
         IERR = IC_GPU_XEQOMP(HOBJ, IMTH, IPRM)
      ELSE
         GOTO 9900
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HOBJ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_GPU_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_XEQGPU(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER      IERR
      INTEGER      IVAL
      CHARACTER*64 MDUL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GPU_AID()

C---     GET
      ELSEIF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, MDUL)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>The opencl module regroups functionalities related to opencl library.</comment>
         IF (MDUL .EQ. 'opencl') THEN
            WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ+IC_GPU_MDL_CL
C           <include>IC_GPU_XEQCL@gpu_ic.for</include>

C        <comment>The cuda module regroups functionalities related to the cuda library.</comment>
         ELSEIF (MDUL .EQ. 'cuda') THEN
            WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ+IC_GPU_MDL_CU
C           <include>IC_GPU_XEQCU@gpu_ic.for</include>

C        <comment>The omp module regroups functionalities related to the OMP library.</comment>
         ELSEIF (MDUL .EQ. 'omp') THEN
            WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ+IC_GPU_MDL_OMP
C           <include>IC_GPU_XEQOMP@gpu_ic.for</include>
         ELSE
            GOTO 9902
         ENDIF

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_MODULE_INVALIDE', ': ',
     &                       MDUL(1:SP_STRN_LEN(MDUL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_XEQGPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Module GPU-OpenCL
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_XEQCL(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER IERR, IRET
      INTEGER HCNF, HDVC, HKRN
      INTEGER IP, ID
      INTEGER NBPE, NEPT, NTSK
      CHARACTER*64  PROP
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_GPU_REQHDL() .EQ. IC_GPU_MDL_CL)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GPU_AID()

!!C---     GET
!!      ELSEIF (IMTH .EQ. '##property_get##') THEN
!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!         IF (IERR .NE. 0) GOTO 9901
!!
!!C        <comment>Hostname</comment>
!!         IF (PROP .EQ. 'hostname') THEN
!!            IRET = C_OS_HOSTNAME(SVAL)
!!            IF (IRET .NE. 0) GOTO 9905
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!C        <comment>Platform name</comment>
!!         ELSEIF (PROP .EQ. 'platform') THEN
!!            IRET = C_OS_PLATFORM(SVAL)
!!            IF (IRET .NE. 0) GOTO 9905
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!         ELSE
!!            GOTO 9903
!!         ENDIF

C---     Méthodes
C     <comment>print platform/devices</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

         IRET = GP_DEVC_PRNALL(GP_DEVC_TYP_OPENCL)
         IF (IRET .NE. 0) GOTO 9905
         WRITE(IPRM, '(2A,I3)') 'I', ',', 0

C     <comment>Generate a GPU device</comment>
      ELSEIF (IMTH .EQ. 'gen_device') THEN
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9901
         IF (SP_STRN_NTOK(IPRM, ',') .NE. 2) GOTO 9901
C        <comment>Platform ID</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, IP)
C        <comment>Device ID</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, ID)
         IF (IRET .NE. 0) GOTO 9901

         HDVC = 0
         IF (ERR_GOOD()) IERR=GP_DEVC_CTR(HDVC, GP_DEVC_TYP_OPENCL)
         IF (ERR_GOOD()) IERR=GP_DEVC_INI(HDVC, IP, ID)
         WRITE(IPRM, '(2A,I12)') 'H', ',', HDVC

C     <comment>Generate a compute configuration</comment>
      ELSEIF (IMTH .EQ. 'gen_config') THEN
C        <include>IC_GPU_XEQGENCONF@gpu_ic.for</include>
         IERR = IC_GPU_XEQGENCONF(HOBJ, IPRM)

C     <comment>Distribute the work load on a compute configuration</comment>
      ELSEIF (IMTH .EQ. 'distribute') THEN
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9901
         IF (SP_STRN_NTOK(IPRM, ',') .NE. 6) GOTO 9902
         HCNF = 0
C        <comment>Handle on the configuration</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HCNF)
C        <comment>Number of bytes requested per task</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, NBPE)
C        <comment>Number of elements per task (-1 if no limits)</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, NEPT)
C        <comment>Number of task (-1 if no limits)</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, NTSK)
C        <comment>Handle on kernel list</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 5, HKRN)
         IF (IRET .NE. 0) GOTO 9901
         
         IERR = GP_CONF_DISDEV(HCNF, NBPE, NEPT, NTSK, HKRN)
         
         WRITE(IPRM, '(2A,I12)') 'H', ',', HCNF

      ELSE
         GOTO 9904
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_APPEL_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_XEQCL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_XEQCU(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER IERR, IRET
      INTEGER HCNF, HDVC
      INTEGER IP, ID
      CHARACTER*64  PROP
      CHARACTER*256 SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_GPU_REQHDL() .EQ. IC_GPU_MDL_CU)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GPU_AID()

!!C---     GET
!!      ELSEIF (IMTH .EQ. '##property_get##') THEN
!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!         IF (IERR .NE. 0) GOTO 9901
!!
!!C        <comment>Hostname</comment>
!!         IF (PROP .EQ. 'hostname') THEN
!!            IRET = C_OS_HOSTNAME(SVAL)
!!            IF (IRET .NE. 0) GOTO 9904
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!C        <comment>Platform name</comment>
!!         ELSEIF (PROP .EQ. 'platform') THEN
!!            IRET = C_OS_PLATFORM(SVAL)
!!            IF (IRET .NE. 0) GOTO 9904
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!         ELSE
!!            GOTO 9902
!!         ENDIF

C---     Méthodes
C     <comment>print platform/devices</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

         IRET = GP_DEVC_PRNALL(GP_DEVC_TYP_CUDA)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,I3)') 'I', ',', 0

      ELSEIF (IMTH .EQ. 'gen_config') THEN
C        <include>IC_GPU_XEQGENCONF@gpu_ic.for</include>
         IERR = IC_GPU_XEQGENCONF(HOBJ, IPRM)

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_APPEL_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_XEQCU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Module GPU-OpenMP
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_XEQOMP(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'gpdevc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER IERR, IRET
      INTEGER HCNF, HDVC
      INTEGER IP, ID
      CHARACTER*64  PROP
      CHARACTER*256 SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_GPU_REQHDL() .EQ. IC_GPU_MDL_OMP)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GPU_AID()

!!C---     GET
!!      ELSEIF (IMTH .EQ. '##property_get##') THEN
!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!         IF (IERR .NE. 0) GOTO 9901
!!
!!C        <comment>Hostname</comment>
!!         IF (PROP .EQ. 'hostname') THEN
!!            IRET = C_OS_HOSTNAME(SVAL)
!!            IF (IRET .NE. 0) GOTO 9904
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!C        <comment>Platform name</comment>
!!         ELSEIF (PROP .EQ. 'platform') THEN
!!            IRET = C_OS_PLATFORM(SVAL)
!!            IF (IRET .NE. 0) GOTO 9904
!!            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
!!         ELSE
!!            GOTO 9902
!!         ENDIF

C---     Méthodes
C     <comment>print platform/devices</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

         IRET = GP_DEVC_PRNALL(GP_DEVC_TYP_OMP)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,I3)') 'I', ',', 0

      ELSEIF (IMTH .EQ. 'gen_config') THEN
C        <include>IC_GPU_XEQGENCONF@gpu_ic.for</include>
         IERR = IC_GPU_XEQGENCONF(HOBJ, IPRM)

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_APPEL_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_XEQOMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GPU_XEQGENCONF(HOBJ, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'gpu_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpconf.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gpu_ic.fc'

      INTEGER IERR, IRET
      INTEGER NTOK
      INTEGER HCNF, HDVC, HLST
      INTEGER IT
      CHARACTER*32 SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GPU_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_GPU_REQHDL() .EQ. IC_GPU_MDL_CL)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Contrôles
      IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900
         
C---     Construis la liste des devices
      HLST = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLST)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HLST)

C---     Monte la liste des devices
      NTOK = SP_STRN_NTOK(IPRM, ',')
      DO IT=1,NTOK
C        <comment>List of devices handle, comma separated</comment>
         IF (ERR_GOOD()) IRET = SP_STRN_TKI(IPRM, ',', IT, HDVC)
         IF (IRET .NE. 0) GOTO 9901
         IF (ERR_GOOD()) WRITE(SVAL, '(I12)') HDVC
         IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HLST, SVAL(1:12))
      ENDDO

C---     Crée la configuration
      IF (ERR_GOOD()) IERR = GP_CONF_CTR(HCNF)
      IF (ERR_GOOD()) IERR = GP_CONF_INI(HCNF, HLST)

C---     Nettoie
      IF (DS_LIST_HVALIDE(HLST)) THEN
         CALL ERR_PUSH()
         IERR = DS_LIST_DTR(HLST)
         HLST = 0
         CALL ERR_POP()
      ENDIF
      IF (ERR_BAD() .AND. GP_CONF_HVALIDE(HCNF)) THEN
         CALL ERR_PUSH()
         IERR = GP_CONF_DTR(HCNF)
         HCNF = 0
         CALL ERR_POP()
      ENDIF
      
C---     Valeur de retour
      WRITE(IPRM, '(2A,I12)') 'H', ',', HCNF
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GPU_AID()

9999  CONTINUE
      IC_GPU_XEQGENCONF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GPU_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GPU_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gpu_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>gpu</b> regroups functionalities related to the gpu.
C</comment>
      IC_GPU_REQNOM = 'gpu'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le handle doit être unique au niveau global
C************************************************************************
      FUNCTION IC_GPU_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GPU_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gpu_ic.fi'
C-------------------------------------------------------------------------

      IC_GPU_REQHDL = 1999007000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GPU_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('gpu_ic.hlp')

      RETURN
      END
