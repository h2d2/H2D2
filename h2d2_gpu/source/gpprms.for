C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Description:
C
C Functions:
C************************************************************************

      MODULE GP_PRMS_M

      USE ISO_C_BINDING

      TYPE, BIND(C) :: GP_PRMS_RES_T
         INTEGER(C_INT) :: NNEL
         INTEGER(C_INT) :: NDJV
         INTEGER(C_INT) :: NDLN
         INTEGER(C_INT) :: NPRG
         INTEGER(C_INT) :: NPRN
         INTEGER(C_INT) :: NPRC
         INTEGER(C_INT) :: NPRE
         INTEGER(C_INT) :: NELE
         INTEGER(C_INT) :: NTSK
      END TYPE GP_PRMS_RES_T

      END MODULE GP_PRMS_M

