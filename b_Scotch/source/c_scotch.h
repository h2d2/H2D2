//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#ifndef C_PSCTCH_H_DEJA_INCLU
#define C_PSCTCH_H_DEJA_INCLU

#ifndef MODULE_PTSCOTCH
#  define MODULE_PTSCOTCH 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_PSCTCH_GETVER C_PSCTCH_GETVER
#  define C_PSCTCH_ERRMSG C_PSCTCH_ERRMSG
#  define C_PSCTCH_PART   C_PSCTCH_PART
#  define C_PSCTCH_RENUM  C_PSCTCH_RENUM
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_PSCTCH_GETVER c_psctch_getver_
#  define C_PSCTCH_ERRMSG c_psctch_errmsg_
#  define C_PSCTCH_PART   c_psctch_part_
#  define C_PSCTCH_RENUM  c_psctch_renum_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_PSCTCH_GETVER c_psctch_getver__
#  define C_PSCTCH_ERRMSG c_psctch_errmsg__
#  define C_PSCTCH_PART   c_psctch_part__
#  define C_PSCTCH_RENUM  c_psctch_renum__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_PTSCOTCH)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_GETVER(F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_ERRMSG(F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_PART  (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_RENUM (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_PSCTCH_H_DEJA_INCLU
