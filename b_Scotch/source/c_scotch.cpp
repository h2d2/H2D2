//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#include "c_scotch.h"

#include "mpi.h"

#include <stdio.h>      // Demandé par ptscotch

#ifdef __cplusplus
extern "C"
{
#endif

#include "ptscotch.h"

#ifdef __cplusplus
}
#endif

#include <algorithm>   // Pour sort, min
#include <iostream>
#include <limits>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#ifdef MODE_DEBUG
#  include <assert.h>
#endif

// ---  Type des indexes pour unifier avec ParMetis
typedef SCOTCH_Num idx_t;

// ---  Code d'erreur
static std::string strErr = "";

// ---  MPI Type pour les INTEGER du Fortran
#if (MPI_VERSION == 2) && (MPI_SUBVERSION < 2)
static const MPI_Datatype MP_TYPE_FINT = (sizeof(fint_t) == sizeof(short))      ? MPI_SHORT :
                                         (sizeof(fint_t) == sizeof(int))        ? MPI_INT :
                                         (sizeof(fint_t) == sizeof(long))       ? MPI_LONG :
                                         (sizeof(fint_t) == sizeof(long long))  ? MPI_LONG_LONG :
                                         MPI_DATATYPE_NULL;
#else
static const MPI_Datatype MP_TYPE_FINT = (sizeof(fint_t) == 4) ? MPI_INT32_T :
                                         (sizeof(fint_t) == 8) ? MPI_INT64_T :
                                         MPI_DATATYPE_NULL;
#endif

// ---  Taille max des blocs MPI
static const int MP_BLOCK_SIZE = std::numeric_limits<int>::max();

#undef MODE_DEBUG_PRINTOUT
//#define MODE_DEBUG_PRINTOUT

namespace h2d2
{
namespace scotch
{

idx_t* min_element_not_neg(idx_t* _First, idx_t* _Last)
{
   idx_t* _Found = _First;
   while (*_Found < 0 && _First != _Last)
      if (*++_First >= 0) _Found = _First;
   if (_First != _Last)
      for (; ++_First != _Last; )
         if (*_First >= 0 && *_First < *_Found)
            _Found = _First;
   return (_Found);
}
idx_t* max_element_not_neg(idx_t* _First, idx_t* _Last)
{
   idx_t* _Found = _First;
   while (*_Found < 0 && _First != _Last)
      if (*++_First >= 0) _Found = _First;
   if (_First != _Last)
      for (; ++_First != _Last; )
         if (*_First >= 0 && *_First > *_Found)
            _Found = _First;
   return (_Found);
}

//************************************************************************
// Sommaire:   Construit le graphe nodale d'un maillage.
//
// Description:
//    La fonction privée <code>Mesh2Nodal(...)<code> monte le graphe
//    nodal de la partie du maillage entre nodMinLcl et nodMaxLcl. Tous
//    les liens vers des noeuds de connectivité positive sont pris en
//    compte. Il est ainsi possible de désactiver des portions de maillages
//    en mettant les connectivités en négatif.
//    <p>
//    La fonction retourne des blocs de mémoire alloués par des appels
//    à new[]. Il est de la responsabilité de la fonction appelante de
//    désallouer la mémoire par des appels à delete[].
//
// Entrée:
//    comm           Communicateur MPI, utilisé en mode debug
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nodMinLcl      Numéro global min du bloc de noeuds du process
//    nodMaxLcl      Numéro global max du bloc de noeuds du process
//
// Sortie:
//    xadj           Tables du graphe en format compressé
//    adjncy
//
// Notes:
//    On monte d'abord la table des connectivités inverse, puis la table
//    des liens. Ce sont des tables cumulées, on fait donc deux passes, la
//    première pour dimensionner, la deuxième pour charger.
//    <p>
//    Les tables sont dimensionnées à +1 pour faire un contrôle de
//    débordement par le haut.
//************************************************************************
void Mesh2Nodal(
#ifdef MODE_DEBUG_PRINTOUT
                MPI_Comm comm,
#else
                MPI_Comm /*comm*/,
#endif
                idx_t   nnel,
                idx_t   nelt,
                idx_t*  kng,
                idx_t   nodMinLcl,
                idx_t   nodMaxLcl,
                idx_t** xadj,
                idx_t** adjncy)
{
#ifdef MODE_DEBUG
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nodMinLcl >= 0);
   assert(nodMaxLcl > nodMinLcl);
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG_PRINTOUT
   int iproc;
   (void) MPI_Comm_rank(comm, &iproc);
#endif

   const idx_t nbrNodLcl = nodMaxLcl - nodMinLcl + 1;
   const idx_t nodMinGlb = *min_element_not_neg(kng, kng + nelt*nnel);
   const idx_t nodMaxGlb = *max_element_not_neg(kng, kng + nelt*nnel);
#ifdef MODE_DEBUG
   assert(nodMinGlb >= 0);
   assert(nodMaxGlb >  nodMinGlb);
#endif

   // ---  Table cumulée des connectivités inverses
   idx_t* idxElem = new idx_t[nbrNodLcl+2];
   for (idx_t in = 0; in <= nbrNodLcl; ++in) idxElem[in] = 0;
#ifdef MODE_DEBUG
   idxElem[nbrNodLcl+1] = 0xCCCCCCCC;
#endif

   // ---  Compte les éléments de chaque noeud local
   for (idx_t ie = 0; ie < nelt; ++ie)
   {
      const idx_t* k = &kng[ie*nnel];
      for (idx_t in = 0; in < nnel; ++in)
      {
         const idx_t noL = k[in] - nodMinLcl;
         if (noL >= 0 && noL < nbrNodLcl) idxElem[noL]++;
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Nombre d'elements par noeud\n", iproc);
   for (idx_t in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%d: %d\n", iproc, in+nodMinLcl+1, idxElem[in]);
   }
#endif
#ifdef MODE_DEBUG
   for (idx_t in = 0; in < nbrNodLcl; ++in) assert(idxElem[in] > 0);
   assert(static_cast<unsigned>(idxElem[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Cumule la table
   for (idx_t in = 1; in < nbrNodLcl; ++in) idxElem[in]+= idxElem[in-1];
   for (idx_t in = nbrNodLcl; in > 0; --in) idxElem[in] = idxElem[in-1];
   idxElem[0] = 0;
#ifdef MODE_DEBUG
   assert(static_cast<unsigned>(idxElem[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Table des connectivités inverses
   const idx_t nbrConnecInv = idxElem[nbrNodLcl];
   idx_t* connecInv = new idx_t[nbrConnecInv+1];
#ifdef MODE_DEBUG
   for (idx_t i = 0; i < nbrConnecInv; ++i) connecInv[i] = -1;
   connecInv[nbrConnecInv] = 0xCCCCCCCC;
#endif

   // ---  Table auxiliaire: Indice des éléments pour chaque noeud
   idx_t* aux = new idx_t[nodMaxGlb+2];
   for (idx_t in = 0; in < nbrNodLcl; ++in) aux[in] = idxElem[in];
#ifdef MODE_DEBUG
   aux[nbrNodLcl] = 0xCCCCCCCC;
#endif

   // ---  Monte la table des connectivités inverses
   for (idx_t ie = 0; ie < nelt; ++ie)
   {
      const idx_t* k = &kng[ie*nnel];
      for (idx_t in = 0; in < nnel; ++in)
      {
         const idx_t noL = k[in] - nodMinLcl;
         if (noL >= 0 && noL < nbrNodLcl) connecInv[aux[noL]++] = ie;
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Connectivites inverses\n", iproc);
   for (idx_t in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%2d: ", iproc, in+nodMinLcl+1);
      idx_t i1 = idxElem[in];
      idx_t i2 = idxElem[in+1];
      for (idx_t j = i1; j < i2; ++j)
      {
         printf(" %d", connecInv[j]+1);
      }
      printf("\n");
   }
#endif
#ifdef MODE_DEBUG
   for (idx_t i = 0; i < nbrNodLcl; ++i) assert(aux[i] == idxElem[i+1]);
   assert(static_cast<unsigned>(aux[nbrNodLcl]) == 0xCCCCCCCC);
   for (idx_t i = 0; i < nbrConnecInv;  ++i) assert(connecInv[i] != -1);
   assert(static_cast<unsigned>(connecInv[nbrConnecInv]) == 0xCCCCCCCC);
#endif


   // ---  Table cumulée du nombre de liens par noeuds
   idx_t* idxLiens = new idx_t[nbrNodLcl+2];
   for (idx_t in = 0; in <= nbrNodLcl; ++in) idxLiens[in] = 0;
#ifdef MODE_DEBUG
   idxLiens[nbrNodLcl+1] = 0xCCCCCCCC;
#endif

   // ---  Table auxiliaire: Évite les dédoublements
   for (idx_t in = 0; in <= nodMaxGlb; ++in) aux[in] = -1;
#ifdef MODE_DEBUG
   aux[nodMaxGlb+1] = 0xCCCCCCCC;
#endif

   // ---  Compte le nombre de liens
   for (idx_t in = 0; in < nbrNodLcl; ++in)
   {
      idx_t noG = in + nodMinLcl;
      aux[noG] = in;                // Marque avec le noeud actif
      for (idx_t ix = idxElem[in]; ix < idxElem[in+1]; ++ix)
      {
         const idx_t ie = connecInv[ix];
         const idx_t* k = &kng[ie*nnel];
         for (idx_t i = 0; i < nnel; ++i)
         {
            noG = k[i];
            if (noG >= 0 && aux[noG] != in)
            {
               aux[noG] = in;       // Marque avec le noeud actif
               idxLiens[in]++;      // Incrémente
            }
         }
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Nombre de liens par noeud\n", iproc);
   for (idx_t in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%d: %d\n", iproc, in+nodMinLcl+1, idxLiens[in]);
   }
#endif
#ifdef MODE_DEBUG
   for (idx_t in = 0; in < nbrNodLcl; ++in) assert(idxLiens[in] > 0);
   assert(static_cast<unsigned>(idxLiens[nbrNodLcl+1]) == 0xCCCCCCCC);
   assert(static_cast<unsigned>(aux[nodMaxGlb+1]) == 0xCCCCCCCC);
#endif

   // ---  Cumule la table des liens
   for (idx_t in = 1; in < nbrNodLcl; ++in) idxLiens[in]+= idxLiens[in-1];
   for (idx_t in = nbrNodLcl; in > 0; --in) idxLiens[in] = idxLiens[in-1];
   idxLiens[0] = 0;
#ifdef MODE_DEBUG
   assert(static_cast<unsigned>(idxLiens[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Table des liens
   const idx_t nbrLiens = idxLiens[nbrNodLcl];
   idx_t* liens = new idx_t[nbrLiens+1];
#ifdef MODE_DEBUG
   for (idx_t i = 0; i < nbrLiens; ++i) liens[i] = -1;
   liens[nbrLiens] = 0xCCCCCCCC;
#endif

   // ---  Monte la table des liens
   for (idx_t in = 0; in <= nodMaxGlb; ++in) aux[in] = -1;
#ifdef MODE_DEBUG
   aux[nodMaxGlb+1] = 0xCCCCCCCC;
#endif
   for (idx_t in = 0, il = 0; in < nbrNodLcl; ++in)
   {
      idx_t noG = in + nodMinLcl;
      aux[noG] = in;
      for (idx_t ix = idxElem[in]; ix < idxElem[in+1]; ++ix)
      {
         const idx_t ie = connecInv[ix];
         const idx_t* k = &kng[ie*nnel];
         for (idx_t i = 0; i < nnel; ++i)
         {
            noG = k[i];
            if (noG >= 0 && aux[noG] != in)
            {
               aux[noG] = in;
               liens[il++] = noG;
            }
         }
      }
//      std::sort(&liens[i1], &liens[i2]);   // Faut-il trier?
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Liens des noeuds\n", iproc);
   for (idx_t in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%2d: ", iproc, in+nodMinLcl+1);
      idx_t i1 = idxLiens[in];
      idx_t i2 = idxLiens[in+1];
      for (idx_t j = i1; j < i2; ++j)
      {
         printf(" %d", liens[j]+1);
      }
      printf("\n");
   }
#endif
#ifdef MODE_DEBUG
   for (idx_t i = 0; i < nbrLiens; ++i) assert(liens[i] != -1);
   assert(static_cast<unsigned>(liens[nbrLiens]) == 0xCCCCCCCC);
   assert(static_cast<unsigned>(aux[nodMaxGlb+1]) == 0xCCCCCCCC);
#endif

   // ---  Récupère la mémoire allouée
   delete[] aux;

   // ---  Tables de sortie
   *xadj   = idxLiens;
   *adjncy = liens;

#ifdef MODE_DEBUG
#endif   // MODE_DEBUG
  return;
}

//************************************************************************
// Sommaire: Synchronise les données
//
// Description:
//    La fonction privée <code>Mesh2Part_Allgather(...)</code>
//
// Entrée:
//    comm           Communicateur MPI
//    sbuf           Send buffer: Données locales
//    displ          Table cumulée des dimensions des sbuf
//
// Sortie:
//    rbuf           Receive buffer: Données globales
//
// Notes:
//************************************************************************
int Mesh2Part_Allgather(MPI_Comm comm,
                        idx_t *sbuf,
                        idx_t *rbuf,
                        idx_t *displ)
{
   int ierr = MPI_SUCCESS;

   int isize = 0;
   int irank = 0;
   ierr = MPI_Comm_size(comm, &isize);
   ierr = MPI_Comm_rank(comm, &irank);

   // ---  Copie la partie propre au process
   if (ierr == MPI_SUCCESS)
   {
      idx_t *sP = sbuf;
      idx_t *rP = rbuf + displ[irank];
      idx_t *eP = rbuf + displ[irank+1];
      while (rP != eP) *rP++ = *sP++;
   }

   // ---  Transmet aux autres process
   for (int ip = 0; ip < isize && ierr == MPI_SUCCESS; ++ip)
   {
      fint_t isizs = displ[ip+1] - displ[ip];
      fint_t ioffs = 0;
      fint_t ioffr = displ[ip];

      while (ioffs < isizs && ierr == MPI_SUCCESS)
      {
         int ibsize = static_cast<int>( std::min(isizs-ioffs, static_cast<fint_t>(MP_BLOCK_SIZE)) );
         ierr = MPI_Bcast(&rbuf[ioffr], ibsize, MP_TYPE_FINT, ip, comm);
         ioffs += ibsize;
         ioffr += ibsize;
      }
   }

   if (ierr != MPI_SUCCESS)
   {
      int strErrLen;
      char strErrBuf[MPI_MAX_ERROR_STRING];
      MPI_Error_string(ierr, strErrBuf, &strErrLen);
      strErr.assign(strErrBuf);
      ierr = 1;
   }

   return ierr;
}

//************************************************************************
// Sommaire: Partitionne le maillage
//
// Description:
//    La fonction privée <code>Mesh2Part_PTScotch(...)</code> partitionne le
//    maillage. Pour les noeuds à-priori associés au process courant, elle
//    retourne dans la table <code>part</code> le numéro de sous-domaine
//    déterminé par le partitionnement.
//    <p>
//    Elle fait appel à PTScotch pour le partitionnement propre.
//
// Entrée:
//    comm           Communicateur MPI
//    npart          Nombre de sous-domaines
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nnt            Nombre de noeuds total
//    nproc          Nombre de process == nombre de sous-domaines
//    iproc          Numéro du process
//    distNoeuds     Table [nproc] de distribution à-priori des noeuds
//
// Sortie:
//    part(*)        Numéro de sous-domaine de chaque noeuds
//
// Notes:
//************************************************************************
int Mesh2Part_PTScotch(MPI_Comm comm,
                       idx_t    npart,
                       idx_t    nnel,
                       idx_t    nelt,
                       idx_t*   kng,
                       idx_t    nnt,
                       idx_t    nproc,
                       idx_t    iproc,
                       idx_t*   distNoeuds,
                       idx_t**  part)
{
#ifdef MODE_DEBUG
   assert(npart > 0);
   assert(nnel > 0);
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nnt > 0);
   assert(nproc > 0);
   assert(iproc >= 0);
   assert(iproc < nproc);
   assert(distNoeuds != NULL);
   assert(part  != NULL);
   assert(*part == NULL);
#endif   // MODE_DEBUG

   int ierr = 0;

   // ---  Paramètres du bloc local de noeuds
   const idx_t nodMinLcl = distNoeuds[iproc];
   const idx_t nodMaxLcl = distNoeuds[iproc+1]-1;
   const idx_t nbrNodLcl = distNoeuds[iproc+1] - distNoeuds[iproc];

   // ---  Monte les tables de liens = graphe
   idx_t* xadj   = NULL;
   idx_t* adjncy = NULL;
   Mesh2Nodal(comm,
              nnel,
              nelt,
              kng,
              nodMinLcl,
              nodMaxLcl,
              &xadj,
              &adjncy);

   // ---  Table de partitionnement
   idx_t* partTrv = new idx_t[nnt];
//   for (idx_t i = 0; i < nbrNodLcl; ++i) partTrv[i] = iproc;

   // ---  Partitionne par PT-Scotch
   SCOTCH_Dgraph graph;
   SCOTCH_Strat  strategy;

   // ---  Initialise les structures Scotch
   if (ierr == 0)
   {
      if (ierr == 0) ierr = SCOTCH_stratInit (&strategy);
      if (ierr == 0) ierr = SCOTCH_dgraphInit(&graph, comm);

      // ---  Construis le graphe
      if (ierr == 0) ierr = SCOTCH_dgraphBuild(&graph,
                                               0,               // baseval
                                               nbrNodLcl,       // vertlocnbr
                                               nbrNodLcl,       // vertlocmax
                                               xadj,            // vertloctab
                                               NULL,            // vendloctab: NULL = xadj+1
                                               NULL,            // veloloctab: vertex load
                                               NULL,            // vlblocltab: vertex label array
                                               xadj[nbrNodLcl], // edgelocnbr,   // 2 * arcs?  nbrLiens = xadj[nbrNodLcl];
                                               xadj[nbrNodLcl], // edgelocnbr
                                               adjncy,          // edgeloctab
                                               NULL,            // edgegsttab: local and ghost indices
                                               NULL);           // edloloctab: edge load
      if (ierr == 0) ierr = SCOTCH_dgraphCheck(&graph);

      // ---  Stratégie
   //   if (ierr == 0) ierr = SCOTCH_stratDgraphOrder (&strategy, "blabla"); // Default

      // ---  Partitionne par PT_Scotch
      if (ierr == 0) ierr = SCOTCH_dgraphPart(&graph, npart, &strategy, partTrv);
      if (ierr != 0) strErr = "ERR_PTSCOTCH";
   }

   // ---  Libère les structures Scotch
   (void) SCOTCH_stratExit (&strategy);
   (void) SCOTCH_dgraphExit(&graph);

   // ---  Reclaim memory
   delete[] xadj;
   delete[] adjncy;

   // ---  Tables de sortie
   if (ierr == 0)
      *part = partTrv;
   else
      delete[] partTrv;

   return ierr;
}

//************************************************************************
// Sommaire: Partitionne le maillage
//
// Description:
//    La fonction privée <code>Mesh2Part_Scotch(...)</code> partitionne le
//    maillage. Elle fait appel à Scotch pour le partitionnement propre et
//    chaque process partitionne globalement le maillage.
//    <p>
//    Pour chaque noeud associé au process, elle retourne dans la table
//    <code>part</code> le numéro de sous-domaine déterminé par le
//    partitionnement.
//
// Entrée:
//    comm           Communicateur MPI
//    npart          Nombre de sous-domaines
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nnt            Nombre de noeuds total
//    nproc          Nombre de process
//    iproc          Numéro du process
//    distNoeuds     Table [nproc] de distribution à-priori des noeuds
//
// Sortie:
//    part(*)        Numéro de sous-domaine de chaque noeud du process
//
// Notes:
//************************************************************************
int Mesh2Part_Scotch(MPI_Comm comm,
                     idx_t    npart,
                     idx_t    nnel,
                     idx_t    nelt,
                     idx_t*   kng,
                     idx_t    nnt,
                     idx_t    nproc,
                     idx_t    iproc,
                     idx_t*   distNoeuds,
                     idx_t**  part)
{
#ifdef MODE_DEBUG
   assert(npart > 0);
   assert(nnel > 0);
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nnt > 0);
   assert(nproc > 0);
   assert(iproc >= 0);
   assert(iproc < nproc);
   assert(distNoeuds != NULL);
   assert(part  != NULL);
   assert(*part == NULL);
#endif   // MODE_DEBUG

   int ierr = 0;

   // ---  Monte les tables de liens = graphe
   idx_t* xadj   = NULL;
   idx_t* adjncy = NULL;
   Mesh2Nodal(comm,
              nnel,
              nelt,
              kng,
              0,           // Noeud min local: on prend tout
              nnt-1,       // Noeud max local
              &xadj,
              &adjncy);

   // ---  Table de partitionnement globale
   idx_t* partTrv = new idx_t[nnt];

   // ---  Partitionne par Scotch
   SCOTCH_Graph graph;
   SCOTCH_Strat strategy;

   // ---  Initialise les structures Scotch
   if (ierr == 0)
   {
      if (ierr == 0) ierr = SCOTCH_stratInit(&strategy);
      if (ierr == 0) ierr = SCOTCH_graphInit(&graph);

      // ---  Construis le graphe
      if (ierr == 0) ierr = SCOTCH_graphBuild(&graph,
                                              0,         // baseval
                                              nnt,       // vertnbr
                                              xadj,      // verttab
                                              NULL,      // vendtab: NULL = xadj+1
                                              NULL,      // velotab: vertex load
                                              NULL,      // vlbltab: vertex label array
                                              xadj[nnt], // edgenbr,   // 2 * arcs?  nbrLiens = xadj[nbrNodLcl];
                                              adjncy,    // edgetab
                                              NULL);     // edlotab: edge load
      if (ierr == 0) ierr = SCOTCH_graphCheck(&graph);

      // ---  Stratégie
   //   if (ierr == 0) ierr = SCOTCH_stratDgraphOrder (&strategy, "blabla"); // Default

      // ---  Partitionne par Scotch
      if (ierr == 0) ierr = SCOTCH_graphPart(&graph, npart, &strategy, partTrv);
      if (ierr != 0) strErr = "ERR_SCOTCH";
   }

   // ---  Libère les structures Scotch
   (void) SCOTCH_stratExit(&strategy);
   (void) SCOTCH_graphExit(&graph);

   // ---  Debug printout
#ifdef MODE_DEBUG_PRINTOUT
   if (ierr == 0 && part != NULL)
   {
      MPI_Barrier(comm);
      const idx_t imax = static_cast<idx_t>( sqrt( static_cast<double>(nnt) ) );
      for (idx_t ip = 0; ip < nproc; ++ip)
      {
         if (ip == iproc)
         {
            printf("[%d]Mesh2Part_Scotch: res. brut [nnt=%d]", iproc, nnt);
            for (idx_t i = 0; i < nnt; ++i)
            {
               if (i % imax == 0) printf("\n[%d]", iproc);
               printf("%1d", partTrv[i]);
            }
            printf("\n[%d]Mesh2Part_Scotch: res. brut\n", iproc);
         }
         MPI_Barrier(comm);
      }
      if (iproc == nproc-1) printf("\n");
   }
#endif

   // ---  Réduis la table globale à la distribution locale
   if (ierr == 0)
   {
      // ---  Paramètres du bloc local de noeuds
      const idx_t nodMinLcl = distNoeuds[iproc];
      const idx_t nbrNodLcl = distNoeuds[iproc+1] - distNoeuds[iproc];
      // ---  Réduis la table
      for (idx_t i = 0; i < nbrNodLcl; ++i)  partTrv[i] = partTrv[i+nodMinLcl];
      for (idx_t i = nbrNodLcl; i <nnt; ++i) partTrv[i] = -1;
   }

   // ---  Reclaim memory
   delete[] xadj;
   delete[] adjncy;

   // ---  Table de sortie
   if (ierr == 0)
      *part = partTrv;
   else
      delete[] partTrv;

   return ierr;
}

//************************************************************************
// Sommaire: Partitionne le maillage
//
// Description:
//    La fonction privée <code>Mesh2Part(...)</code> partitionne le maillage.
//    Elle retourne dans la table kdis le numéro de sous-domaine associé par
//    le partitionnement à chaque noeud du maillage.
//    <p>
//    Elle fait appel à PT_Scotch pour le partitionnement propre, et échange
//    ensuite l'info entre les process pour retourner la table globale.
//
// Entrée:
//    comm           Communicateur MPI
//    npart          Nombre de sous-domaines
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nnt            Nombre de noeuds total
//
// Sortie:
//    kdis(nnt)      Table des numéros de sous-domaine des noeuds
//
// Notes:
//************************************************************************
int Mesh2Part(MPI_Comm comm,
              idx_t    npart,
              idx_t    nnel,
              idx_t    nelt,
              idx_t*   kng,
              idx_t    nnt,
              idx_t*   kdis)
{
#ifdef MODE_DEBUG
   assert(npart> 0);
   assert(nnel > 0);
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nnt > 0);
   assert(kdis != NULL);
#endif   // MODE_DEBUG

   int ierr = 0;

   // ---  Nombre de process
   int iproc;
   int nproc;
   ierr = MPI_Comm_rank(comm, &iproc);
   if (ierr == MPI_SUCCESS)
   {
      ierr = MPI_Comm_size(comm, &nproc);
   }
   if (ierr != MPI_SUCCESS)
   {
      int strErrLen;
      char strErrBuf[256];
      MPI_Error_string(ierr, strErrBuf, &strErrLen);
      strErr.assign(strErrBuf);
      ierr = 1;
   }

   // ---  Nombre de noeuds global
   const idx_t nbrConnecGlb = nnel*nelt;
   const idx_t nodMinGlb = *min_element_not_neg(kng, kng+nbrConnecGlb);
   const idx_t nodMaxGlb = *max_element_not_neg(kng, kng+nbrConnecGlb);
#ifdef MODE_DEBUG
   assert(nodMinGlb >= 0);
   assert(nodMaxGlb <= nnt);
#endif

   // ---  Distribution des noeuds sur les process
   idx_t* nmbrNoeuds = new idx_t[nproc];
   idx_t* distNoeuds = new idx_t[nproc+1];    // Table cumulée
   distNoeuds[0] = 0;
   for (idx_t ip = 0, nnr = nodMaxGlb+1; ip < nproc; ++ip)
   {
      const idx_t nnp = nnr / (nproc-ip);
      nmbrNoeuds[ip]   = nnp;
      distNoeuds[ip+1] = distNoeuds[ip] + nnp;
      nnr -= nnp;
   }

   // ---  Paramètres du bloc local de noeuds
#ifdef MODE_DEBUG_PRINTOUT
   const idx_t nodMinLcl = distNoeuds[iproc];
#endif
   const idx_t nbrNodLcl = nmbrNoeuds[iproc];

   // ---  Partitionne
   idx_t* part = NULL;
   if (npart == 1)
   {
      part = new idx_t[nnt];
      for (idx_t i = 0; i < nnt; ++i) part[i] = iproc;
   }
   else
   {
      if (nproc == 1 || nnt < 5000)
         ierr = Mesh2Part_Scotch  (comm, npart, nnel, nelt, kng, nnt, nproc, iproc, distNoeuds, &part);
      else
         ierr = Mesh2Part_PTScotch(comm, npart, nnel, nelt, kng, nnt, nproc, iproc, distNoeuds, &part);
   }

   // ---  Debug printout
#ifdef MODE_DEBUG_PRINTOUT
   if (ierr == 0 && part != NULL)
   {
      MPI_Barrier(comm);
      const idx_t imax = static_cast<idx_t>( sqrt( static_cast<double>(nnt) ) );
      const idx_t nodMaxLcl = distNoeuds[iproc+1]-1;
      for (int ip = 0; ip < nproc; ++ip)
      {
         if (ip == iproc)
         {
            printf("[%d]Mesh2Part: proc. des noeuds [%d,%d]", iproc, nodMinLcl, nodMaxLcl);
            for (idx_t i = nodMinLcl, ii = 0; i <= nodMaxLcl; ++i, ++ii)
            {
               if (ii % imax == 0) printf("\n[%d]", iproc);
               printf("%1d", part[ii]);
            }
            printf("\n[%d]Mesh2Part: end proc. des noeuds\n", iproc);
         }
         MPI_Barrier(comm);
      }
      if (iproc == nproc-1) printf("\n");
   }
#endif

   // ---  Réduis avec tous les autres process
   if (ierr == 0)
   {
      // ---  Initialise la liste complète des noeuds
      for (idx_t in = 0; in < nnt; ++in) kdis[in] = -1;
      // ---  Réduis avec tous les autres process
      ierr = Mesh2Part_Allgather(comm, part, kdis, distNoeuds);
   }

   // --- Vérifie que chaque domaine à un noeud
   if (ierr == 0)
   {
      idx_t* tabVerif = new idx_t[npart];
      for (idx_t i = 0; i < npart; ++i) tabVerif[i] = 1;
      for (idx_t i = 0; i < nnt;   ++i) tabVerif[kdis[i]] = 0; // --- on flag les domaines ayant des noeuds
      for (idx_t i = 0; i < npart && ierr == 0; ++i)
      {
         if (tabVerif[i] != 0)
         {
            ierr = 1;
            strErr = "ERR_DOMAINE_SANS_NOEUDS";
         }
      }
      delete[] tabVerif;
   }

   // ---  Reclaim memory
   if (part != NULL) delete[] part;
   delete[] nmbrNoeuds;
   delete[] distNoeuds;

   return ierr;
}

//************************************************************************
// Sommaire: Renumérote le maillage
//
// Description:
//    La fonction privée <code>Mesh2Renum(...)</code> renumérote le maillage.
//    Pour le process courant, elle retourne dans la table kren les nouveaux
//    numéros de noeuds.
//    <p>
//    Elle fait appel à Scotch.
//
// Entrée:
//    comm           Communicateur MPI
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nnl            Nombre de noeuds locaux
//
// Sortie:
//    kren           Table de renumérotation
//
// Notes:
//************************************************************************
int Mesh2Renum(MPI_Comm comm,
               idx_t    nnel,
               idx_t    nelt,
               idx_t*   kng,
               idx_t    nnl,
               idx_t*   kren)
{
#ifdef MODE_DEBUG
   assert(nnel > 0);
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nnl > 0);
   assert(kren != NULL);
#endif   // MODE_DEBUG

   int ierr = 0;

   const idx_t nodMinLcl = *min_element_not_neg(kng, kng + nelt*nnel);
   const idx_t nodMaxLcl = *max_element_not_neg(kng, kng + nelt*nnel);
   const idx_t nbrNodLcl = nodMaxLcl - nodMinLcl + 1;
#ifdef MODE_DEBUG
   assert(nnl == nbrNodLcl);
#endif   // MODE_DEBUG

   // ---  Monte les tables de liens = graphe
   idx_t* xadj   = NULL;
   idx_t* adjncy = NULL;
   Mesh2Nodal(comm,
              nnel,
              nelt,
              kng,
              nodMinLcl,   // Noeud min local
              nodMaxLcl,   // Noeud max local
              &xadj,
              &adjncy);

   // ---  Table de permutation
   idx_t* iperm = new idx_t[nbrNodLcl];
   idx_t* iren  = new idx_t[nbrNodLcl]; // Pour passer de idx_t à idx_t

   // ---  Renumérote par Scotch
   SCOTCH_Graph    graph;
   SCOTCH_Strat    strategy;
   SCOTCH_Ordering ordering;

   // ---  Initialise les structures Scotch
   if (ierr == 0)
   {
      if (ierr == 0) ierr = SCOTCH_stratInit(&strategy);
      if (ierr == 0) ierr = SCOTCH_graphInit(&graph);

      // ---  Construis le graphe
      if (ierr == 0) ierr = SCOTCH_graphBuild(&graph,
                                               0,               // baseval
                                               nbrNodLcl,       // vertnbr
                                               xadj,            // verttab
                                               NULL,            // vendtab: NULL = xadj+1
                                               NULL,            // velotab: vertex load
                                               NULL,            // vlbltab: vertex label array
                                               xadj[nbrNodLcl], // edgenbr,   // 2 * arcs?  nbrLiens = xadj[nbrNodLcl];
                                               adjncy,          // edgetab
                                               NULL);           // edlotab: edge load
      if (ierr == 0) ierr = SCOTCH_graphCheck(&graph);

      // ---  Stratégie
   //   if (ierr == 0) ierr = SCOTCH_stratDgraphOrder (&strategy, "blabla"); // Default
      if (ierr == 0) ierr = SCOTCH_stratGraphOrder(&strategy, "g");

      // ---  Renumérote par Scotch
      if (ierr == 0) ierr = SCOTCH_graphOrderInit(&graph,
                                               &ordering,
                                               iren,
                                               iperm, /* MeTiS and Scotch have opposite definitions for (inverse) permutations */
                                               NULL,
                                               NULL,
                                               NULL);
      if (ierr == 0) SCOTCH_graphOrderCompute (&graph, &ordering, &strategy);
      if (ierr == 0) SCOTCH_graphOrderExit    (&graph, &ordering);

      if (ierr != 0) strErr = "ERR_SCOTCH";
   }

   // ---  Transfert
   for (idx_t i = 0; i < nbrNodLcl; ++i) kren[i] = iren[i];

   // ---  Libère les structures Scotch
   (void) SCOTCH_stratExit(&strategy);
   (void) SCOTCH_graphExit(&graph);

   // ---  Reclaim memory
   delete[] xadj;
   delete[] adjncy;
   delete[] iperm;
   delete[] iren;    // Pour passer de idx_t à idx_t

   // ---  Debug printout
#ifdef MODE_DEBUG_PRINTOUT
   {
      MPI_Barrier(comm);

      int iproc;
      int nproc;
      ierr = MPI_Comm_rank(comm, &iproc);
      if (ierr == MPI_SUCCESS)
      {
         ierr = MPI_Comm_size(comm, &nproc);
      }
      if (ierr != MPI_SUCCESS)
      {
         int strErrLen;
         char strErrBuf[MPI_MAX_ERROR_STRING];
         MPI_Error_string(ierr, strErrBuf, &strErrLen);
         strErr.assign(strErrBuf);
         ierr = 1;
      }
      for (int ip = 0; ip < nproc; ++ip)
      {
         if (ip == iproc)
         {
            printf("[%d]Mesh2Renum: renum des noeuds locaux [%d]\n", iproc, nbrNodLcl);
            for (idx_t i = 0; i < nbrNodLcl; ++i)
            {
               printf("[%d] %d --> %d\n", iproc, i, kren[i]);
            }
            printf("\n[%d]Mesh2Part: end renum des noeuds locaux\n", iproc);
         }
         MPI_Barrier(comm);
      }
      if (iproc == nproc-1) printf("\n");
   }
#endif

   return ierr;
}

}  // namespace scotch
}  // namespace h2d2


//************************************************************************
// Sommaire:   Retourne la version de Scotch
//
// Description:
//    La fonction <code>C_PSCTCH_GETVER(...)</code> retourne la version
//    de Scotch
//
// Entrée:
//
// Sortie:
//    str      La version
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_GETVER(F2C_CONF_STRING  str
                                                              F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_GETVER(F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   if (l < 10) return -1;

   int v0, v1, v2;
   SCOTCH_version(&v0, &v1, &v2);

   memset(sP, ' ', l);
   sprintf(sP, "%02d.%02d.%02d", v0, v1, v2);
   sP[strlen(sP)] = ' ';

   return 0;
}

//************************************************************************
// Sommaire:   Assigne un message d'erreur
//
// Description:
//    La fonction <code>C_PSCTCH_ERRMSG(...)</code> assigne un message
//    d'erreur à <code>msgErr</code> à partir du code d'erreur <code>ierr</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_ERRMSG(F2C_CONF_STRING  msgErr
                                                              F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_ERRMSG(F2C_CONF_STRING  msgErr)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* msgErrP = msgErr;
   int   l  = len;
#else
   char* msgErrP = msgErr->strP;
   int   l = msgErr->len;
#endif

   memset(msgErrP, ' ', l);
   if (l >= static_cast<int> (strErr.length()))
   {
      memcpy(msgErrP, strErr.c_str(), strErr.length());
   }

   strErr = "";

   return 0;
}

//************************************************************************
// Sommaire:   Partitionne un maillage éléments finis.
//
// Description:
//    La fonction <code>C_PSCTCH_PART(...)</code> partitionne le maillage
//    décrit par <code>nnt, nnel, nelt, kng</code>. Elle retourne dans la
//    table <code>kdis</code> les numéros des sous-domaine associé à chacun
//    des noeuds du maillage.
//
// Entrée:
//    fint_t* comm      Communicateur MPI
//    fint_t* npart     Nombre de sous-domaines
//    fint_t* nnt       Nombre de noeuds total
//    fint_t* nnel      Nombre de noeuds par élément
//    fint_t* nelt      Nombre d'éléments total
//    fint_t* kng       Table des connectivités kng[nelt][nnel]
//
// Sortie:
//    fint_t* kdis      Table(nnt) des numéros de sous-domaine des noeuds
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_PART(fint_t* comm,
                                                            fint_t* npart,
                                                            fint_t* nnt,
                                                            fint_t* kdis,
                                                            fint_t* nnel,
                                                            fint_t* nelt,
                                                            fint_t* kng)
{
#ifdef MODE_DEBUG
   assert(*npart> 0);
   assert(*nnel > 0);
   assert(*nelt > 0);
   assert(kng != NULL);
   assert(*nnt > 0);
   int mpi_init = 0;
   MPI_Initialized(&mpi_init);
   assert(mpi_init > 0);
#endif   // MODE_DEBUG

   int ierr = 0;

   setvbuf(stdout, NULL, _IONBF, 0);
   setvbuf(stderr, NULL, _IONBF, 0);

   // ---  Passe en numérotation C
   const fint_t nbrConnecGlb = (*nnel)*(*nelt);
   for (fint_t i = 0; i < nbrConnecGlb; ++i) --kng[i];

   // ---  Partitionne
   MPI_Comm comm_c = MPI_Comm_f2c(*comm);
   ierr = h2d2::scotch::Mesh2Part(comm_c, *npart, *nnel, *nelt, kng, *nnt, kdis);

   // ---  Repasse en numérotation FORTRAN
   for (fint_t i = 0; i < nbrConnecGlb; ++i) ++kng[i];
   for (fint_t i = 0; i < *nnt; ++i) ++kdis[i];

   return ierr;
}

//************************************************************************
// Sommaire:   Renumérote un maillage éléments finis.
//
// Description:
//    La fonction <code>C_PSCTCH_RENUM(...)</code> renumérote le maillage
//    décrit par <code>nnt, nnel, nelt, kng</code>.
//
// Entrée:
//    fint_t* comm      Communicateur MPI
//    fint_t* nnl       Nombre de noeuds locaux
//    fint_t* nnel      Nombre de noeuds par élément
//    fint_t* nelt      Nombre d'éléments total
//    fint_t* kng       Table globale des connectivités kng[nelt][nnel]
//
// Sortie:
//    fint_t* kren      Table des nouveaux numéros de noeuds
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PSCTCH_RENUM(fint_t* comm,
                                                             fint_t* nnl,
                                                             fint_t* kren,
                                                             fint_t* nnel,
                                                             fint_t* nelt,
                                                             fint_t* kng)
{
#ifdef MODE_DEBUG
   assert(*nnel > 0);
   assert(*nelt > 0);
   assert(kng != NULL);
   assert(*nnl > 0);
   int mpi_init = 0;
   MPI_Initialized(&mpi_init);
   assert(mpi_init > 0);
#endif   // MODE_DEBUG

   int ierr = 0;

   setvbuf(stdout, NULL, _IONBF, 0);
   setvbuf(stderr, NULL, _IONBF, 0);

   // ---  Passe en numérotation C
   const fint_t nbrConnecGlb = (*nnel)*(*nelt);
   for (fint_t i = 0; i < nbrConnecGlb; ++i) --kng[i];

   // ---  Renumérote
   MPI_Comm comm_c = MPI_Comm_f2c(*comm);
   ierr = h2d2::scotch::Mesh2Renum(comm_c, *nnel, *nelt, kng, *nnl, kren);

   // ---  Repasse en numérotation FORTRAN
   for (fint_t i = 0; i < nbrConnecGlb; ++i) ++kng[i];
   for (fint_t i = 0; i < *nnl; ++i) ++kren[i];

   return ierr;
}
