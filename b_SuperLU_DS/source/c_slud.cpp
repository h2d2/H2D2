//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************
/*
 * -- SuperLU routine (version 2.0) --
 * Univ. of California Berkeley, Xerox Palo Alto Research Center,
 * and Lawrence Berkeley National Lab.
 * November 15, 1997
 *
 */
//*****************************************************************************
// Fichier: $Id$
// Classe:
//*****************************************************************************
#include "c_slud.h"

#include "superlu_ddefs.h"
#include "util_dist.h"
#include "mpi.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#endif

struct SuperLUSystem
{
   SuperMatrix       A;
   ScalePermstruct_t P;
   LUstruct_t        LU;

//   int *etree;       // elimination tree
//   int *perm_r;      // row permutations from partial pivoting
//   int *perm_c;      // column permutation vector

   superlu_options_t opts;
   gridinfo_t        grid;
   double drop_tol;
   int panel_size;
   int relax;
   int lwork;

   int  n;
   bool ccs;

   SuperLUSystem()
   : /*etree     (NULL)
   , perm_r    (NULL)
   , perm_c    (NULL)
   , */drop_tol  (0.0)
   , panel_size(-1)
   , relax     (-1)
   , lwork     (0)
   , n         (0)
   , ccs       (false)
   {
      A.Store  = NULL;
//      P.Store  = NULL;
//      LU.Store = NULL;
   }
};

// ---  Table des matrices
typedef std::map<fint_t, SuperLUSystem*> TTMatrices;
TTMatrices c_slud_matrices;
      long c_slud_ninit = 0;
      long c_slud_nhndl = 0;
const long C_SLUD_HBASE = 99013000;

// ---  Fonction utilitaires
template <bool b>
int* c_slud_copyTbl(fint_t , fint_t*);

template <>
int* c_slud_copyTbl<true> (fint_t, fint_t* k)
{
   return reinterpret_cast<int*>(k);
}
template <>
int* c_slud_copyTbl<false> (fint_t n, fint_t* k)
{
   int* rP = new int[n];
   for (int i = 0; i < n; ++i) rP[i] = static_cast<int>(k[i]);
   return rP;
}

template <bool b>
void c_slud_deleteTbl(int*);

template <>
void c_slud_deleteTbl<true> (int*)
{
   return;
}
template <>
void c_slud_deleteTbl<false> (int* rP)
{
   delete[] rP;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction privée <code>c_slud_hvalide</code> retour true
//    si le handle est valide.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
bool c_slud_hvalide(fint_t* hndl)
{
   return (c_slud_matrices.find(*hndl) != s_slud_matrices.end());
};

//////************************************************************************
////// Sommaire:
//////
////// Description:
//////    La fonction privée <code>c_slud_initmat</code> initialise la matrice
//////    à partir des arguments.
//////
////// Entrée:
//////
////// Sortie:
//////
////// Notes:
//////************************************************************************
////void c_slud_initmat (SuperLUSystem *sP,
////                     fint_t *n,
////                     fint_t *nnz,
////                     double *values,
////                     fint_t *rowind,
////                     fint_t *colptr)
////{
////#ifdef MODE_DEBUG
////   assert(sP != NULL);
////   assert(sP->n == *n);
////#endif
////
////   // ---  SamePattern reuses only perm_c  (Ref: ~SuperLU_3.0/EXAMPLE/dlinsolx2.c)
////   if (sP->opts.Fact == SamePattern)
////   {
////      ScalePermstructFree(&(sP->P));
////      Destroy_LU (*n, &(sP->grid), &(sP->LU));
////      Destroy_CompRowLoc_Matrix_dist(&sP->A);
////   }
////
////   // ---  If int size are not equals, copy
////   const bool sameIntSize = (sizeof(fint_t) == sizeof(int));
////   int* rP = c_slud_copyTbl<sameIntSize>(*n,   rowind);
////   int* cP = c_slud_copyTbl<sameIntSize>(*nnz, colptr);
////
////   // ---  Create SuperLU Matrix
////   if (sP->opts.Fact == DOFACT || sP->opts.Fact == SamePattern)
////   {
////      //dCreate_CompRowLoc_Matrix_dist(&(sP->A), *n, *n, *nnz, values, rP, cP, SLU_NC, SLU_D, SLU_GE);
////   }
////
////   // ---  Reclaim memory
////   c_slud_deleteTbl<sameIntSize>(rP);
////   c_slud_deleteTbl<sameIntSize>(cP);
////
////   // --- If Factorize
////   if (sP->opts.Fact == DOFACT)
////   {
////      ScalePermstructInit(*n, *n, &(sP->P ));
////      LUstructInit       (*n, *n, &(sP->LU));
////   }
////
////   return;
////}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_SLUD_INITIALIZE</code> dimensionne la matrice.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_INITIALIZE()
{
#ifdef MODE_DEBUG
#endif

   if (c_slud_ninit == 0)
   {
      c_slud_matrices.clear();
   }
   ++c_slud_ninit;

   return 0;
}

//************************************************************************
// Sommaire: Finalise le système
//
// Description:
//    La fonction C_SLUD_FINALIZE() fait la finalisation de bas niveau
//    nécessaire au fonctionnement du module. Elle est le pendant de
//    C_SLUD_INITIALIZE.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_FINALIZE()
{
   int ierr;

   ierr = 0;

   if (c_slud_ninit == 1)
   {
      for (TTMatrices::iterator mI = c_slud_matrices.begin();
           mI != c_slud_matrices.end();
           ++mI)
      {
         ierr = C_SLUD_MAT_DESTROY(const_cast<fint_t*>(&(*mI).first));
      }
      c_slud_matrices.clear();
   }
   if (c_slud_ninit > 0) --c_slud_ninit;

   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_SLUD_INIT</code> crée la matrice.
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_CREATE(hndl_t* hndl,
                                                                fint_t* n,
                                                                fint_t* ccs,
                                                                MPI_Comm i_comm)
{
#ifdef MODE_DEBUG
   assert(*n > 0);
#endif

   // ---  Crée la matrice
   ++c_slud_nhndl;
   hndl = c_slud_nhndl + C_SLUS_HBASE;
   c_slud_matrices[hndl] = new (SuperLUSystem);

   // --- Assigne les options
   SuperLUSystem* sP = c_slud_matrices[hndl];
/* Default options (Ref: SuperLU_DIST-2.0/SRC/util.c)
    options->Fact = DOFACT;
    options->Equil = YES;
    options->ColPerm = MMD_AT_PLUS_A;
    options->RowPerm = LargeDiag;
    options->ReplaceTinyPivot = YES;
    options->Trans = NOTRANS;
    options->IterRefine = DOUBLE;
    options->SolveInitialized = NO;
    options->RefineInitialized = NO;
    options->PrintStat = YES;
*/
   set_default_options_dist(&sP->opts);
   sP->opts.Equil = NO;             //
   sP->opts.Trans = (*ccs == 0) ? NOTRANS : TRANS;

   sP->drop_tol   = 0.0;            // drop tolerance
   sP->lwork      = 0;              // allocate dynamically
   //sP->etree      = intMalloc(*n);
   //sP->perm_r     = intMalloc(*n);
   //sP->perm_c     = intMalloc(*n);
   sP->n          = *n;

   // ---  Communication grid
   int i_err, i_size;
   i_err = MPI_Comm_size(i_comm, &i_size);
   int n_proc = static_cast<int>(sqrt(i_size));
   superlu_gridinit(i_comm, n_proc, n_proc, &(sP->grid));

#ifdef MODE_DEBUG
   assert(c_slud_matrices[hndl] != NULL);
#endif
   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_DESTROY(fint_t* hndl)
{
#ifdef MODE_DEBUG
   assert(c_slud_hvalide(hndl))
#endif

   SuperLUSystem* sP = c_slud_matrices[hndl];
   assert(sP != NUL);

   //SUPERLU_FREE (sP->perm_r);
   //SUPERLU_FREE (sP->perm_c);
   //SUPERLU_FREE (sP->etree);

   if (sP->P.Store)  ScalePermstructFree(&(sP->P));
   if (sP->LU.Store) Destroy_LU (sP->n, &(sP->grid), &(sP->LU));
   if (sP->A.Store)  Destroy_CompCol_Matrix_dist(&(sP->A));

   delete sP;
   c_slud_matrices[hndl] = NUL;
   c_slud_matrices.erase(hndl);

   *hndl = 0;
   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_MAT_ANALYZE (hndl_t *hndl,
                                                           fint_t *n,
                                                           fint_t *nnz,
                                                           double *values,
                                                           fint_t *rowind,
                                                           fint_t *colptr)
{
#ifdef MODE_DEBUG
   assert(c_slud_hvalide(hndl));
#endif

   SuperLUSystem* sP = c_slud_matrices[hndl];
   assert(sP != NUL);

   // ---  Adjust to 0-based indexing
//   int *r = rowind;
//   const int *rmax = r + nnz;
//   while (r < rmax) (--*r)++;
   for (int i = 0; i < *nnz; ++i) --rowind[i];
   for (int i = 0; i <=  *n; ++i) --colptr[i];

   // ---  Initialise la matrice
   ////// ---  SamePattern reuses only perm_c  (Ref: ~SuperLU_3.0/EXAMPLE/dlinsolx2.c)
   ////if (sP->opts.Fact == SamePattern)
   ////{
   ////   ScalePermstructFree(&(sP->P));
   ////   Destroy_LU (*n, &(sP->grid), &(sP->LU));
   ////   Destroy_CompRowLoc_Matrix_dist(&sP->A);
   ////}

   // ---  If int size are not equals, copy
   const bool sameIntSize = (sizeof(fint_t) == sizeof(int));
   int* rP = c_slud_copyTbl<sameIntSize>(*n,   rowind);
   int* cP = c_slud_copyTbl<sameIntSize>(*nnz, colptr);

   // ---  Create SuperLU Matrix
   if (sP->opts.Fact == DOFACT || sP->opts.Fact == SamePattern)
   {
      //dCreate_CompRowLoc_Matrix_dist(&(sP->A), *n, *n, *nnz, values, rP, cP, SLU_NC, SLU_D, SLU_GE);
   }

   // ---  Reclaim memory
   c_slud_deleteTbl<sameIntSize>(rP);
   c_slud_deleteTbl<sameIntSize>(cP);

   // --- If Factorize
   if (sP->opts.Fact == DOFACT)
   {
      ScalePermstructInit(*n, *n, &(sP->P ));
      LUstructInit       (*n, *n, &(sP->LU));
   }


   c_slud_initmat(sP, n, nnz, values, rowind, colptr);

   // ---  Restore to 1-based indexing
   for (int i = 0; i < *nnz; ++i) ++rowind[i];
   for (int i = 0; i <=  *n; ++i) ++colptr[i];

   // ---  Flag factorize done
   if (info == 0)
   {
      sP->opts.Fact = SamePattern_SameRowPerm;
   }

   return info;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_FACT (hndl_t *hndl,
                                                           fint_t *n,
                                                           fint_t *nnz,
                                                           double *values,
                                                           fint_t *rowind,
                                                           fint_t *colptr)
{
#ifdef MODE_DEBUG
   assert(c_slud_hvalide(hndl));
#endif

   SuperLUSystem* sP = c_slud_matrices[hndl];
   assert(sP != NUL);

   // ---  Adjust to 0-based indexing
//   int *r = rowind;
//   const int *rmax = r + nnz;
//   while (r < rmax) (--*r)++;
   for (int i = 0; i < *nnz; ++i) --rowind[i];
   for (int i = 0; i <=  *n; ++i) --colptr[i];

   // ---  Initialise la matrice
   c_slud_initmat(sP, n, nnz, values, rowind, colptr);

   // ---  Initialize stats
   SuperLUStat_t stat;
   PStatInit(&stat);

   // ---  Factorize
   int info = 0;
   double anorm = 0.0;     //??  anorm = pdlangs(norm, A, grid);
   pdgstrf(&(sP->opts),
           (sP->n),
           (sP->n),
            anorm,
           &(sP->LU),
           &(sP->grid),
           &stat,
           &info);

   // ---  Reset stats
   PStatFree(&stat);

   // ---  Restore to 1-based indexing
   for (int i = 0; i < *nnz; ++i) ++rowind[i];
   for (int i = 0; i <=  *n; ++i) ++colptr[i];

   // ---  Flag factorize done
   if (info == 0)
   {
      sP->opts.Fact = SamePattern_SameRowPerm;
   }

   return info;
}




#ifdef ZZZZZZZZZZZZZZZZZZ



//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUD_SOLVE (hndl_t *hndl,
                                                            fint_t *n,     /* Matrix order */
                                                            fint_t *nrhs,  /* Number of right-hand sides */
                                                            double *b)     /* Right-hand side */
{
#ifdef MODE_DEBUG
   assert(reinterpret_cast<SuperLUSystem*>(*hndl) != NULL);
   assert(reinterpret_cast<SuperLUSystem*>(*hndl)->n == *n);
   assert(reinterpret_cast<SuperLUSystem*>(*hndl)->opts.Fact != DOFACT);
   assert(c_slud_hvalide(hndl));
#endif

   SuperLUSystem* sP = c_slud_matrices[hndl];
   assert(sP != NUL);

   // --- Create Dense SuperLU Matrix for the RHS
   SuperMatrix B;
   dCreate_Dense_Matrix (&B, *n, *nrhs, b, *n, SLU_DN, SLU_D, SLU_GE);

   // ---  Initialize stats
   SuperLUStat_t stat;
   PStatInit(&stat);

   // ---  Solve
   int info = 0;
   //pdgstrs(sP->opts.Trans,
   //        &(sP->L),
   //        &(sP->U),
   //        sP->perm_c,
   //        sP->perm_r,
   //        &B,
   //        &stat,
   //        &info);

   // ---  Reset stats
   PStatFree(&stat);

   // ---  Reclaim memory
   Destroy_SuperMatrix_Store(&B);

   return info;
}

#if defined(_MSC_VER)
#  pragma warning( pop )
#endif
