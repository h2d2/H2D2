C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PRDS_CMD
C     CHARACTER*(32) IC_PRDS_REQCMD
C   Private:
C     SUBROUTINE IC_PRDS_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PRDS_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRDS_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icprdso.fi'
      INCLUDE 'mrprdso.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icprdso.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HRES
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PRDS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Valide
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Construis et initialise l'objet concret
      HRES = 0
      IF (ERR_GOOD()) IERR = MR_PRDS_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_PRDS_INI(HRES)

C---     Construis et initialise le parent-proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = MR_RESO_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = MR_RESO_INI(HOBJ, HRES)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_PRDS_PRN(HRES)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the matrix solver object</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HRES
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES', ': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRDS_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_PRDS_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction PRiNt
C
C Description: Fonction PRN qui imprime tous les paramètres de l'objet
C
C Entrée: HOBJ
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRDS_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrprdso.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icprdso.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_MKL_PARDISO'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                             NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PRDS_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PRDS_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRDS_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icprdso.fi'
      INCLUDE 'mrprdso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icprdso.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IDX, IVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>setparam</b> assign the.</comment>
      IF (IMTH .EQ. 'setparam') THEN
D        CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IDX)
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = MR_PRDS_ASGPRM(HOBJ, IDX, IVAL)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = MR_PRDS_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(MR_PRDS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_PRDS_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PRDS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRDS_AID()

9999  CONTINUE
      IC_PRDS_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRDS_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRDS_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icprdso.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>mkl_pardiso</b> constructs a matrix solver; it is used to solve large
C  sparse symmetric and unsymmetric linear systems of equations on shared
C  memory multiprocessors. It is thread safe, high performance, robust,
C  memory efficient and easy to use.
C</comment>

      IC_PRDS_REQCLS = 'mkl_pardiso'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRDS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRDS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icprdso.fi'
      INCLUDE 'mrprdso.fi'
C-------------------------------------------------------------------------

      IC_PRDS_REQHDL = MR_PRDS_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PRDS_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icprdso.hlp')

      RETURN
      END
