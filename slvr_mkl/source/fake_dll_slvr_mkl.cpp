//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_mkl
// Entry point: extern "C" void fake_dll_slvr_mkl()
//
// This file is generated automatically, any change will be lost.
// Generated 2018-06-12 15:59:28.289000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_PRDS
F_PROT(IC_PRDS_XEQCTR, ic_prds_xeqctr);
F_PROT(IC_PRDS_XEQMTH, ic_prds_xeqmth);
F_PROT(IC_PRDS_REQCLS, ic_prds_reqcls);
F_PROT(IC_PRDS_REQHDL, ic_prds_reqhdl);
 
// ---  class MR_PRDS
F_PROT(MR_PRDS_000, mr_prds_000);
F_PROT(MR_PRDS_999, mr_prds_999);
F_PROT(MR_PRDS_CTR, mr_prds_ctr);
F_PROT(MR_PRDS_DTR, mr_prds_dtr);
F_PROT(MR_PRDS_INI, mr_prds_ini);
F_PROT(MR_PRDS_RST, mr_prds_rst);
F_PROT(MR_PRDS_REQHBASE, mr_prds_reqhbase);
F_PROT(MR_PRDS_HVALIDE, mr_prds_hvalide);
F_PROT(MR_PRDS_ESTDIRECTE, mr_prds_estdirecte);
F_PROT(MR_PRDS_ASMMTX, mr_prds_asmmtx);
F_PROT(MR_PRDS_FCTMTX, mr_prds_fctmtx);
F_PROT(MR_PRDS_RESMTX, mr_prds_resmtx);
F_PROT(MR_PRDS_XEQ, mr_prds_xeq);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_mkl()
{
   static char libname[] = "slvr_mkl";
 
   // ---  class IC_PRDS
   F_RGST(IC_PRDS_XEQCTR, ic_prds_xeqctr, libname);
   F_RGST(IC_PRDS_XEQMTH, ic_prds_xeqmth, libname);
   F_RGST(IC_PRDS_REQCLS, ic_prds_reqcls, libname);
   F_RGST(IC_PRDS_REQHDL, ic_prds_reqhdl, libname);
 
   // ---  class MR_PRDS
   F_RGST(MR_PRDS_000, mr_prds_000, libname);
   F_RGST(MR_PRDS_999, mr_prds_999, libname);
   F_RGST(MR_PRDS_CTR, mr_prds_ctr, libname);
   F_RGST(MR_PRDS_DTR, mr_prds_dtr, libname);
   F_RGST(MR_PRDS_INI, mr_prds_ini, libname);
   F_RGST(MR_PRDS_RST, mr_prds_rst, libname);
   F_RGST(MR_PRDS_REQHBASE, mr_prds_reqhbase, libname);
   F_RGST(MR_PRDS_HVALIDE, mr_prds_hvalide, libname);
   F_RGST(MR_PRDS_ESTDIRECTE, mr_prds_estdirecte, libname);
   F_RGST(MR_PRDS_ASMMTX, mr_prds_asmmtx, libname);
   F_RGST(MR_PRDS_FCTMTX, mr_prds_fctmtx, libname);
   F_RGST(MR_PRDS_RESMTX, mr_prds_resmtx, libname);
   F_RGST(MR_PRDS_XEQ, mr_prds_xeq, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
