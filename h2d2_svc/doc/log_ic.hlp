class log
=========
 
   The class **log** represents the log system used to output information
   either to a file or to the console. The verbosity level can be modified to
   increase or decrease the amount of information displayed. In development:
   the log will define zone and levels of verbosity associated with a zone.
   Zones are hierarchic in a tree like manner: 
     * h2d2
     * h2d2.grid
     * h2d2.grid.coor
    
   Active log zones are: 
     * h2d2
       * h2d2.algo
         * h2d2.algo.cinc
           * h2d2.algo.cinc.cfl
           * h2d2.algo.cinc.limiter
           * h2d2.algo.cinc.pid
           * h2d2.algo.cinc.predictif
           * h2d2.algo.cinc.simple
         * h2d2.algo.conv
         * h2d2.algo.homotopy
           * h2d2.algo.homotopy.prgl
         * h2d2.algo.matrix
           * h2d2.algo.matrix.distributed
           * h2d2.algo.matrix.morse
         * h2d2.algo.node
           * h2d2.algo.node.nodelr
         * h2d2.algo.projection
           * h2d2.algo.projection.pod
           * h2d2.algo.projection.polynomial
         * h2d2.algo.sequence
         * h2d2.algo.solver
           * h2d2.algo.solver.esdirk
           * h2d2.algo.solver.euler
           * h2d2.algo.solver.gmres
           * h2d2.algo.solver.homotopy
           * h2d2.algo.solver.mrqd
           * h2d2.algo.solver.newton
           * h2d2.algo.solver.picard
           * h2d2.algo.solver.read
           * h2d2.algo.solver.rk4l
       * h2d2.data
         * h2d2.data.condition
         * h2d2.data.dof
         * h2d2.data.elem
         * h2d2.data.limite
         * h2d2.data.list
         * h2d2.data.map
         * h2d2.data.prgl
         * h2d2.data.vnod
       * h2d2.element
         * h2d2.element.prc
         * h2d2.element.sv2d
           * h2d2.element.sv2d.bc
             * h2d2.element.sv2d.bc.cl143
             * h2d2.element.sv2d.bc.cl144
             * h2d2.element.sv2d.bc.clc
           * h2d2.element.sv2d.clim
           * h2d2.element.sv2d.tide
       * h2d2.geom
         * h2d2.geom.t6l
       * h2d2.glob
         * h2d2.glob.arlx
         * h2d2.glob.bkrs
         * h2d2.glob.btrk2
         * h2d2.glob.btrk3
         * h2d2.glob.compose
         * h2d2.glob.ktdamper
         * h2d2.glob.step
       * h2d2.gpu
         * h2d2.gpu.config
           * h2d2.gpu.config.cuda
         * h2d2.gpu.device
           * h2d2.gpu.device.cl
       * h2d2.grid
         * h2d2.grid.elem
         * h2d2.grid.front
         * h2d2.grid.metis
         * h2d2.grid.part
         * h2d2.grid.remesh
         * h2d2.grid.scotch
       * h2d2.io
         * h2d2.io.numr
       * h2d2.mpi
       * h2d2.post
         * h2d2.post.dof
         * h2d2.post.error
         * h2d2.post.hessian
         * h2d2.post.residuum
         * h2d2.post.simulation
       * h2d2.renum
         * h2d2.renum.blocs
         * h2d2.renum.color
         * h2d2.renum.front
       * h2d2.reso
         * h2d2.reso.algo
           * h2d2.reso.algo.util
         * h2d2.reso.solve
           * h2d2.reso.solve.pardiso
       * h2d2.services
         * h2d2.services.modules
       * h2d2.spmef
         * h2d2.spmef.gmres
           * h2d2.spmef.gmres.solve
       * h2d2.umef
         * h2d2.umef.spelem
    
   constructor handle log()
      The constructor **log** constructs an object and returns a handle on
      this object.
         handle   HOBJ        Return value: Handle on the log system
    
   getter debug
      Log level: debug
    
   getter verbose
      Log level: verbose
    
   getter info
      Log level: info
    
   getter terse
      Log level: terse
    
   getter warning
      Log level: warning
    
   getter error
      Log level: error
    
   property level
      Get the global log level (deprecated, replaced by the zones. See
      add_zone, set_zone_level)
    
   static method add_zone(sval, ival)
      The method **add_zone** adds a zone to the log system
         string   sval        Name of the zone
         integer  ival        Log level for the zone
    
   static method del_zone(sval)
      The method **del_zone** deletes a log zone.
         string   sval        Name of the zone
    
   static method set_zone_level(sval, ival)
      The method **set_zone_level** sets the log level for the given zone.
         string   sval        Name of the zone
         integer  ival        Log level for the zone. Valid values are:
                           info[1,2,3] or debug[30]
    
   static method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   static method print()
      The method **print** prints information about the object.
    
   static method help()
      The method **help** displays the help content for the class.
    
