C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C     INTEGER IO_XML_000
C     INTEGER IO_XML_999
C     INTEGER IO_XML_CTR
C     INTEGER IO_XML_DTR
C     INTEGER IO_XML_INI
C     INTEGER IO_XML_RST
C     INTEGER IO_XML_REQHBASE
C     LOGICAL IO_XML_HVALIDE
C     INTEGER IO_XML_OPEN
C     INTEGER IO_XML_CLOSE
C     INTEGER IO_XML_RO_B
C     INTEGER IO_XML_RO_E
C     INTEGER IO_XML_RD_1
C     INTEGER IO_XML_RD_N
C     INTEGER IO_XML_RD_V
C     INTEGER IO_XML_RH_A
C     INTEGER IO_XML_RH_R
C     INTEGER IO_XML_RI_1
C     INTEGER IO_XML_RI_N
C     INTEGER IO_XML_RX_1
C     INTEGER IO_XML_RX_N
C     INTEGER IO_XML_RS
C     INTEGER IO_XML_RT
C     INTEGER IO_XML_WO_B
C     INTEGER IO_XML_WO_E
C     INTEGER IO_XML_WD_1
C     INTEGER IO_XML_WD_N
C     INTEGER IO_XML_WH_A
C     INTEGER IO_XML_WH_R
C     INTEGER IO_XML_WI_1
C     INTEGER IO_XML_WI_N
C     INTEGER IO_XML_WX_1
C     INTEGER IO_XML_WX_N
C     INTEGER IO_XML_WS
C     INTEGER IO_XML_WT
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_XML_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IO_XML_NOBJMAX,
     &                   IO_XML_HBASE,
     &                   'Writer XML')

      IO_XML_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_XML_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fc'

      INTEGER  IERR
      EXTERNAL IO_XML_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IO_XML_NOBJMAX,
     &                   IO_XML_HBASE,
     &                   IO_XML_DTR)

      IO_XML_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IO_XML_NOBJMAX,
     &                   IO_XML_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IO_XML_HVALIDE(HOBJ))
         IOB = HOBJ - IO_XML_HBASE

         IO_XML_UNIT(IOB) = 0
         IO_XML_ISTA(IOB) = IO_MODE_INDEFINI
      ENDIF

      IO_XML_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IO_XML_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IO_XML_NOBJMAX,
     &                   IO_XML_HBASE)

      IO_XML_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = IO_XML_RST(HOBJ)

      IO_XML_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER   IERR
      INTEGER   IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_XML_HBASE

C---     RESET LES ATTRIBUTS
      IO_XML_UNIT(IOB) = 0
      IO_XML_ISTA(IOB) = IO_MODE_INDEFINI

      IO_XML_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IO_XML_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_XML_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioxml.fi'
      INCLUDE 'ioxml.fc'
C------------------------------------------------------------------------

      IO_XML_REQHBASE = IO_XML_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IO_XML_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_XML_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioxml.fc'
C------------------------------------------------------------------------

      IO_XML_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IO_XML_NOBJMAX,
     &                                  IO_XML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FNAME       Nom du fichier de coordonnées
C     ISTAT       IO_LECTURE ou IO_ECRITURE, mais pas IO_AJOUT
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_OPEN(HOBJ, FNAME, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_OPEN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER        IERR
      INTEGER        M
      CHARACTER*(64) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNAME) .GT. 0)
C------------------------------------------------------------------------

      M = IO_UTIL_FREEUNIT()

      IF     (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         OPEN(UNIT  = M,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED', !  'UNFORMATTED',
     &        STATUS= 'OLD',
     &        ERR   = 9901)
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
         OPEN(UNIT  = M,
     &        FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &        FORM  = 'FORMATTED', !  'UNFORMATTED',
     &        STATUS= 'REPLACE',
     &        ERR   = 9901)
      ELSE
         GOTO 9900
      ENDIF

      IO_XML_UNIT(HOBJ-IO_XML_HBASE) = M
      IO_XML_ISTA(HOBJ-IO_XML_HBASE) = ISTAT

      IF     (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         READ (M, *) BUF
         READ (M, *) BUF
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
         WRITE(M, '(A)') '<?xml version="1.0" encoding="ISO-8859-1"?>'
         WRITE(M, '(A)') '<h2d2_marshal>'
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_MODE_ACCES_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER', ': ',
     &                      FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_CLOSE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_CLOSE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER IS, MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)
      IF (MW .NE. 0) THEN
         IS = IO_XML_ISTA(HOBJ-IO_XML_HBASE)
         IF (IO_UTIL_MODACTIF(IS, IO_MODE_ECRITURE))
     &      WRITE(MW, '(A)') '</h2d2_marshal>'
         CLOSE(MW)
      ENDIF

      IO_XML_UNIT(HOBJ-IO_XML_HBASE) = 0
      IO_XML_ISTA(HOBJ-IO_XML_HBASE) = IO_MODE_INDEFINI

      IO_XML_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un objet - Début
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RO_B(HOBJ, LNEW, HITM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RO_B
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HITM

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER   IERR
      INTEGER   IC, ID
      INTEGER   HNDL
      INTEGER   MR
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

C---     Lis la ligne au complet
      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)
      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      IF (SP_STRN_LEN(BUF) .LE. 0)  GOTO 9901

C---     Contrôle la structure de la ligne
      IF (BUF(1:8) .NE. '<object ') GOTO 9901
      IC = SP_STRN_LFT(BUF, '>')
      ID = SP_STRN_TOK(BUF, ' id=', 2)
      IF (IC .LE. 0) GOTO 9901
      IF (ID .LE. 0) GOTO 9901

C---     Extrais les infos
      BUF = BUF(ID+1:SP_STRN_LEN(BUF)-1)
      READ (BUF(1:SP_STRN_LEN(BUF)), *, ERR=9901) HITM

C---     Unpickle l'objet via le handle
      HNDL = 0
      IERR = OB_OBJC_UPK(HNDL, LNEW, HOBJ)
      IF (ERR_GOOD() .AND. HNDL .NE. HITM) GOTO 9902

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_XML'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_READING_OBJECT')
      CALL ERR_AJT('MSG_INVALID_ENTRY:')
      CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      GOTO 9999
9902  CALL ERR_ASG(ERR_ERR, 'ERR_OBJET_CORROMPU')
      GOTO 9999

9999  CONTINUE
      IO_XML_RO_B = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un objet - Fin
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RO_E(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RO_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

C---     Lis la ligne au complet
      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)
      READ(MR, '(A)', ERR=9900) BUF
D     CALL ERR_ASR(BUF(1:SP_STRN_LEN(BUF)) .EQ. '</object>')

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_XML'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RO_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un handle comme attribut
C
C Description:
C     La méthode IO_XML_RH_A lis le handle HITM comme un attribut. Il
C     fait une lecture "profonde".
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RH_A(HOBJ, HITM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RH_A
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HITM

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER HMDL, HFNC
      LOGICAL HVALIDE, LNEW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - IO_XML_HBASE
      ISTAT = IO_XML_ISTA(IOB)

C---     Shallow copy
C        ============
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_SHLLWCPY)) THEN
         IERR = IO_XML_RH_R(HOBJ, HITM)

C---     Deep copy
C        =========
      ELSE

C---        Switch le mode IO_MODE_1DEEPCPY
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_1DEEPCPY))
     &      IO_XML_ISTA(IOB) = IO_XML_ISTA(IOB) + IO_MODE_SHLLWCPY

C---        Début d'objet
         IF (ERR_GOOD()) IERR = IO_XML_RO_B(HOBJ, LNEW, HITM)

C---        Un-pickle
         HFNC = 0
         IF (ERR_GOOD()) IERR = SO_UTIL_REQHMDLHBASE(HITM, HMDL)
         IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'UPK', HFNC)
         IF (ERR_GOOD()) IERR = SO_FUNC_CALL3 (HFNC,
     &                                         SO_ALLC_CST2B(HITM),
     &                                         SO_ALLC_CST2B(LNEW),
     &                                         SO_ALLC_CST2B(HOBJ))
         IF (HFNC .NE. 0)IERR = SO_FUNC_DTR   (HFNC)

C---        Fin d'objet
         IF (ERR_GOOD()) IERR = IO_XML_RO_E(HOBJ)

C---        Contrôle le handle
         HFNC = 0
         IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'HVALIDE', HFNC)
         IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                         SO_ALLC_CST2B(HITM))
         IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
         IF (HFNC .NE. 0)IERR = SO_FUNC_DTR(HFNC)
         IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_HANDLE_INVALIDE', ': ', HITM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RH_A = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un handle comme référence
C
C Description:
C     La méthode IO_XML_RH_R lis le handle HITM comme une référence. Il
C     fait uniquement une lecture du handle comme un pointeur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RH_R(HOBJ, H)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RH_R
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER H

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:8)     .NE. '<handle>')  GOTO 9901
      IF (BUF(LB-8:LB) .NE. '</handle>') GOTO 9901
      READ(BUF(9:LB-9), *, ERR=9902) H

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_HANDLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE',': ',BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_HANDLE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RH_R = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(64) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:8) .NE. '<double>') GOTO 9901
      IF (BUF(LB-8:LB) .NE. '</double>') GOTO 9901

      READ(BUF(9:LB-9), *, ERR=9902) V

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_DOUBLE', ': ', BUF(9:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX

      INTEGER NVAL
      PARAMETER (NVAL = 2)
      CHARACTER*(32) KVAR(NVAL)
      CHARACTER*(32) KVAL(NVAL)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IERR = IO_XML_RT(HOBJ, 'container', NVAL, KVAR, KVAL)
D     CALL ERR_ASR(KVAR(1) .EQ. 'type' .AND. KVAL(1) .EQ. 'double')
D     CALL ERR_ASR(KVAR(2) .EQ. 'size')
      READ(KVAL(2), *) N

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_RD_1(HOBJ, VX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_RD_1(HOBJ, VX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IERR = IO_XML_RT(HOBJ, '/container', 0, KVAR, KVAL)

      IO_XML_RD_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Alloue et lis un table.
C
C Description:
C     La méthode IO_XML_RD_V est une méthode utilitaire qui retourne
C     une nouvelle table de dimension NX et appelle IO_XML_RD_V pour
C     lire son contenu.
C     <p>
C     Il est de la responsabilité de la méthode appelante de gérer
C     la mémoire allouée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RD_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RD_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      LOGICAL LEXIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      NX = -1
      N  = -1
      L  = 0
      LEXIST = .FALSE.

      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_RL_1(HOBJ, LEXIST)
      IF (LEXIST) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NX, L)
         IF (ERR_GOOD()) IERR = IO_XML_RD_N(HOBJ,
     &                                      N,
     &                                      VA(SO_ALLC_REQVIND(VA,L)),
     &                                      IX)
      ENDIF

      IO_XML_RD_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(256) BUF

      INTEGER TL
      PARAMETER (TL = 9)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(    1:TL) .NE. '<integer>')  GOTO 9901
      IF (BUF(LB-TL:LB) .NE. '</integer>') GOTO 9901

      READ(BUF(TL+1:LB-(TL+1)), *, ERR=9902) I

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_INTEGER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_INTEGER', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX

      INTEGER NVAL
      PARAMETER (NVAL = 2)
      CHARACTER*(32) KVAR(NVAL)
      CHARACTER*(32) KVAL(NVAL)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IERR = IO_XML_RT(HOBJ, 'container', NVAL, KVAR, KVAL)
D     CALL ERR_ASR(KVAR(1) .EQ. 'type' .AND. KVAL(1) .eq. 'integer')
D     CALL ERR_ASR(KVAR(2) .EQ. 'size')
      READ(KVAL(2), *) N

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_RI_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_RI_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IERR = IO_XML_RT(HOBJ, '/container', 0, KVAR, KVAL)

      IO_XML_RI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Alloue et lis un table.
C
C Description:
C     La méthode IO_XML_RI_V est une méthode utilitaire qui retourne
C     une nouvelle table de dimension NX et appelle IO_XML_RI_V pour
C     lire son contenu.
C     <p>
C     Il est de la responsabilité de la méthode appelante de gérer
C     la mémoire allouée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RI_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RI_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      LOGICAL LEXIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      NX = -1
      N  = -1
      L  = 0
      LEXIST = .FALSE.

      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_RL_1(HOBJ, LEXIST)
      IF (LEXIST) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NX, L)
         IF (ERR_GOOD()) IERR = IO_XML_RI_N(HOBJ,
     &                                      N,
     &                                      KA(SO_ALLC_REQKIND(KA,L)),
     &                                      IX)
      ENDIF

      IO_XML_RI_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RX_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RX_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(256) BUF

      INTEGER TL
      PARAMETER (TL = 10)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(    1:TL) .NE. '<integer8>')  GOTO 9901
      IF (BUF(LB-TL:LB) .NE. '</integer8>') GOTO 9901

      READ(BUF(TL+1:LB-(TL+1)), *, ERR=9902) I

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_INTEGER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_INTEGER', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RX_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RX_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RX_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   N
      INTEGER*8 KX(*)
      INTEGER   IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX

      INTEGER NVAL
      PARAMETER (NVAL = 2)
      CHARACTER*(32) KVAR(NVAL)
      CHARACTER*(32) KVAL(NVAL)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IERR = IO_XML_RT(HOBJ, 'container', NVAL, KVAR, KVAL)
D     CALL ERR_ASR(KVAR(1) .EQ. 'type' .AND. KVAL(1) .EQ. 'integer8')
D     CALL ERR_ASR(KVAR(2) .EQ. 'size')
      READ(KVAL(2), *) N

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_RX_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_RX_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IERR = IO_XML_RT(HOBJ, '/container', 0, KVAR, KVAL)

      IO_XML_RX_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Alloue et lis un table.
C
C Description:
C     La méthode IO_XML_RX_V est une méthode utilitaire qui retourne
C     une nouvelle table de dimension NX et appelle IO_XML_RX_V pour
C     lire son contenu.
C     <p>
C     Il est de la responsabilité de la méthode appelante de gérer
C     la mémoire allouée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RX_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RX_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      LOGICAL LEXIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      NX = -1
      N  = -1
      L  = 0
      LEXIST = .FALSE.

      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_RL_1(HOBJ, LEXIST)
      IF (LEXIST) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN8(NX, L)
         IF (ERR_GOOD()) IERR = IO_XML_RX_N(HOBJ,
     &                                      N,
     &                                      XA(SO_ALLC_REQXIND(XA,L)),
     &                                      IX)
      ENDIF

      IO_XML_RX_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RL_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RL_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB
      CHARACTER*(64) BUF

      INTEGER TL
      PARAMETER (TL = 9)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(    1:TL) .NE. '<logical>')  GOTO 9901
      IF (BUF(LB-TL:LB) .NE. '</logical>') GOTO 9901

      IF     (BUF(TL+1:LB-(TL+1)) .EQ. 'True') THEN
         I = .TRUE.
      ELSEIF (BUF(TL+1:LB-(TL+1)) .EQ. 'False') THEN
         I = .FALSE.
      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_LOGICAL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_LOGICAL', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RL_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RL_N(HOBJ, N, LX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RL_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      LOGICAL LX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX

      INTEGER NVAL
      PARAMETER (NVAL = 2)
      CHARACTER*(32) KVAR(NVAL)
      CHARACTER*(32) KVAL(NVAL)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      IERR = IO_XML_RT(HOBJ, 'container', NVAL, KVAR, KVAL)
D     CALL ERR_ASR(KVAR(1) .EQ. 'type' .AND. KVAL(1) .EQ. 'logical')
D     CALL ERR_ASR(KVAR(2) .EQ. 'size')
      READ(KVAL(2), *) N

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_RL_1(HOBJ, LX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_RL_1(HOBJ, LX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      IERR = IO_XML_RT(HOBJ, '/container', 0, KVAR, KVAL)

      IO_XML_RL_N = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: Alloue et lis un table.!
!!!C
!!!C Description:
!!!C     La méthode IO_XML_RL_V est une méthode utilitaire qui retourne
!!!C     une nouvelle table de dimension NX et appelle IO_XML_RL_N pour
!!!C     lire son contenu.
!!!C     <p>
!!!C     Il est de la responsabilité de la méthode appelante de gérer
!!!C     la mémoire allouée.
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C
!!!C************************************************************************
!!!      FUNCTION IO_XML_RL_V(HOBJ, NX, N, L, IX)
!!!C$pragma aux IO_XML_RL_V export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: IO_XML_RL_V
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!      INTEGER NX
!!!      INTEGER N
!!!      INTEGER L
!!!      INTEGER IX
!!!
!!!      INCLUDE 'ioxml.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'soallc.fi'
!!!      INCLUDE 'ioxml.fc'
!!!
!!!      INTEGER IERR
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      L = 0
!!!      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NX, L)
!!!      IF (ERR_GOOD()) IERR = IO_XML_RL_N(HOBJ,
!!!     &                                   N,
!!!     &                                   LA(SO_ALLC_REQLIND(LA,L)),
!!!     &                                   IX)
!!!
!!!      IO_XML_RL_V = ERR_TYP()
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MR
      INTEGER LB, LS
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)

      IF (BUF(1:8) .NE. '<string>') GOTO 9901
      IF (BUF(LB-8:LB) .NE. '</string>') GOTO 9901

      BUF = BUF(9:LB-9)
      LB = INDEX(BUF,':')
      READ(BUF(1:LB-1), *) LS
      IF (LS .GT. LEN(S)) GOTO 9902
      S = BUF(LB+1:LB+LS)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_STRING'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_TAG_XML_INVALIDE', ': ', BUF(1:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_STRING', ': ', BUF(9:LB)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_RS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un tag
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_RT(HOBJ, TNAM, NVAL, KVAR, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_RT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) TNAM
      INTEGER       NVAL
      CHARACTER*(*) KVAR(*)
      CHARACTER*(*) KVAL(*)

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER   IERR, IRET
      INTEGER   I, IDUM
      INTEGER   L
      INTEGER   MR
      CHARACTER*(256) BUF
      CHARACTER*(64)  TK1, TK2
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_LECTURE))
C------------------------------------------------------------------------

C---     Lis la ligne au complet
      MR = IO_XML_UNIT(HOBJ-IO_XML_HBASE)
      READ(MR, '(A)', ERR=9900) BUF
      CALL SP_STRN_TRM(BUF)
      IF (SP_STRN_LEN(BUF) .LE. 0)  GOTO 9901

C---     Contrôle la structure de la ligne
      IF (BUF(1:1) .NE. '<') GOTO 9901
      BUF = BUF(2:)
      CALL SP_STRN_TRM(BUF)
      L = SP_STRN_LEN(BUF)
!!!      IF (BUF(L-1:L) .NE. '/>') GOTO 9901
!!!      BUF = BUF(:L-2)
      IF (BUF(L:L) .NE. '>') GOTO 9901
      BUF = BUF(:L-1)
      CALL SP_STRN_TRM(BUF)

C---     Contrôle le tag
      L = SP_STRN_LEN(TNAM)
      IF (BUF(1:L) .NE. TNAM(1:L)) GOTO 9901
      BUF = BUF(L+1:)
      IF (BUF(1:1) .NE. ' ') GOTO 9901
      CALL SP_STRN_TRM(BUF)

C---     Contrôle le nombre de valeurs
      IF (SP_STRN_NTOK(BUF, ' ') .NE. NVAL) GOTO 9901

C---     Copie
      IF (ERR_GOOD()) THEN
         IRET = 0
         DO I=1,NVAL
            IF (IRET .EQ. 0) IRET = SP_STRN_TKS(BUF, ' ', i, TK1)
            IF (IRET .EQ. 0) IRET = SP_STRN_TKS(TK1, '=', 2, TK2)
            IF (IRET .EQ. 0) IDUM = SP_STRN_LFT(TK1, '=')
            IF (IRET .EQ. 0) IDUM = SP_STRN_RHT(TK2, '"')
            IF (IRET .EQ. 0) IDUM = SP_STRN_LFT(TK2, '"')
            KVAR(I) = TK1(1:MIN(SP_STRN_LEN(TK1),LEN(KVAR(I))))
            KVAL(I) = TK2(1:MIN(SP_STRN_LEN(TK2),LEN(KVAL(I))))
         ENDDO
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_XML'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_READING_TAG')
      CALL ERR_AJT('MSG_INVALID_ENTRY:')
      CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      GOTO 9999
9902  CALL ERR_ASG(ERR_ERR, 'ERR_READING_OBJECT')
      CALL ERR_AJT('MSG_VARIABLE_OVERFLOW')
      GOTO 9999

9999  CONTINUE
      IO_XML_RT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris un objet - début.
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WO_B(HOBJ, HITM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WO_B
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HITM

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      IF (HITM .EQ. 0) THEN
         WRITE(MW, '(A)', ERR=9900) '<object id="0">'
      ELSE
         WRITE(MW, '(A,I0,A)', ERR=9900) '<object id="', HITM, '">'
         IERR = OB_OBJC_PKL(HITM, HOBJ)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_OBJET'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WO_B = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris un objet - fin
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WO_E(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WO_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)
      WRITE(MW, '(A)', ERR=9900) '</object>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_FERMETURE_TAG'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WO_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris un handle comme attribut
C
C Description:
C     La méthode IO_XML_WH_A écris le handle HITM comme un attribut. Il
C     fait une écriture "profonde". Le handle peut être nul.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HITM     Handle à écrire
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WH_A(HOBJ, HITM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WH_A
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HITM

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTAT
      INTEGER HMDL, HFNC
      LOGICAL HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IO_XML_HBASE
      ISTAT = IO_XML_ISTA(IOB)

C---     Shallow copy
C        ============
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_SHLLWCPY)) THEN
         IERR = IO_XML_WH_R(HOBJ, HITM)

C---     Handle null
C        ===========
      ELSE IF (HITM .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = IO_XML_WO_B(HOBJ, HITM)
         IF (ERR_GOOD()) IERR = IO_XML_WO_E(HOBJ)

C---     Deep copy
C        =========
      ELSE

C---        Switch le mode IO_MODE_1DEEPCPY
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_1DEEPCPY))
     &      IO_XML_ISTA(IOB) = IO_XML_ISTA(IOB) + IO_MODE_SHLLWCPY

C---        Demande le module avec le handle
         IERR = SO_UTIL_REQHMDLHBASE(HITM, HMDL)

C---        Contrôle le handle
         HFNC = 0
         IF (ERR_GOOD()) IERR=SO_MDUL_REQFNC(HMDL, 'HVALIDE', HFNC)
         IF (ERR_GOOD()) IERR=SO_FUNC_CALL1(HFNC,
     &                                      SO_ALLC_CST2B(HITM))
         IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
         IF (SO_FUNC_HVALIDE(HFNC)) IERR = SO_FUNC_DTR(HFNC)
         IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---        Charge la fonction
         HFNC = 0
         IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'PKL', HFNC)

C---        Pickle
         IF (ERR_GOOD()) IERR=IO_XML_WO_B(HOBJ, HITM)
         IF (ERR_GOOD()) IERR=SO_FUNC_CALL2(HFNC,
     &                                      SO_ALLC_CST2B(HITM),
     &                                      SO_ALLC_CST2B(HOBJ))
         IF (ERR_GOOD()) IERR=IO_XML_WO_E(HOBJ)

C---        Détruis la fonction
         IF (SO_FUNC_HVALIDE(HFNC)) IERR = SO_FUNC_DTR(HFNC)

      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_HANDLE_INVALIDE', ': ', HITM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WH_A = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris un handle comme référence
C
C Description:
C     La méthode IO_XML_WH_R écris le handle HITM comme une référence. Il
C     fait uniquement une écriture du handle comme un pointeur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WH_R(HOBJ, H)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WH_R
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER H

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LB
      CHARACTER*(16) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(BUF, '(I9)') H
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)
      WRITE(MW, '(3A)', ERR=9900) '<handle>', BUF(1:LB), '</handle>'

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_ECRITURE_HANDLE', ': ', H
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WH_R = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LB
      CHARACTER*(32) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(BUF, '(1PE25.17E3)') V
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)
      WRITE(MW, '(3A)', ERR=9900) '<double>', BUF(1:LB), '</double>'

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE25.17E3)') 'ERR_ECRITURE_DOUBLE', ': ', V
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(MW, '(3A,I0,A)', ERR=9900) '<container ',
     &                                 'type="double" ',
     &                                 'size=" ', N, '">'

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_WD_1(HOBJ, VX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_WD_1(HOBJ, VX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      WRITE(MW, '(A)', ERR=9900) '</container>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_CONTAINER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WD_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris une table d'allocation.
C
C Description:
C     La méthode IO_XML_WD_V est une méthode utilitaire qui cris
C     une table de dimension NX et appelle IO_XML_RD_V pour
C     écrire son contenu.
C     <p>
C
C Entrée:
C     HOBJ
C     NX    Dimension de la table
C     N     Nombre d'item écrire
C     L     Handle d'allocation
C     IX    Incrément d'accès à la table
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WD_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WD_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_WL_1(HOBJ, SO_ALLC_HEXIST(L))
      IF (SO_ALLC_HEXIST(L)) THEN
         IF (ERR_GOOD()) IERR = IO_XML_WD_N(HOBJ,
     &                                      N,
     &                                      VA(SO_ALLC_REQVIND(VA,L)),
     &                                      IX)
      ENDIF

      IO_XML_WD_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LB
      CHARACTER*(16) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(BUF, '(I16)') I
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)
      WRITE(MW, '(3A)', ERR=9900) '<integer>', BUF(1:LB), '</integer>'

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_ECRITURE_INTEGER', ': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(MW, '(3A,I0,A)', ERR=9900) '<container ',
     &                                 'type="integer" ',
     &                                 'size=" ', N, '">'

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_WI_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_WI_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      WRITE(MW, '(A)', ERR=9900) '</container>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_CONTAINER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris une table d'allocation.
C
C Description:
C     La méthode IO_XML_WI_V est une méthode utilitaire qui écris
C     une table de dimension NX et appelle IO_XML_WI_V pour
C     écrire son contenu.
C     <p>
C
C Entrée:
C     HOBJ
C     NX    Dimension de la table
C     N     Nombre d'item écrire
C     L     Handle d'allocation
C     IX    Incrément d'accès à la table
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WI_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WI_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_WL_1(HOBJ, SO_ALLC_HEXIST(L))
      IF (SO_ALLC_HEXIST(L)) THEN
         IF (ERR_GOOD()) IERR = IO_XML_WI_N(HOBJ,
     &                                      N,
     &                                      KA(SO_ALLC_REQKIND(KA,L)),
     &                                      IX)
      ENDIF

      IO_XML_WI_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WX_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WX_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LB
      CHARACTER*(16) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(BUF, '(I16)') I
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)
      WRITE(MW, '(3A)', ERR=9900) '<integer8>', BUF(1:LB), '</integer8>'

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9)') 'ERR_ECRITURE_INTEGER', ': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WX_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WX_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WX_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   N
      INTEGER*8 KX(*)
      INTEGER   IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(MW, '(3A,I0,A)', ERR=9900) '<container ',
     &                                 'type="integer8" ',
     &                                 'size=" ', N, '">'

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_WX_1(HOBJ, KX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_WX_1(HOBJ, KX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      WRITE(MW, '(A)', ERR=9900) '</container>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_CONTAINER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WX_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris une table d'allocation.
C
C Description:
C     La méthode IO_XML_WX_V est une méthode utilitaire qui cris
C     une table de dimension NX et appelle IO_XML_RD_V pour
C     écrire son contenu.
C     <p>
C
C Entrée:
C     HOBJ
C     NX    Dimension de la table
C     N     Nombre d'item écrire
C     L     Handle d'allocation
C     IX    Incrément d'accès à la table
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WX_V(HOBJ, NX, N, L, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WX_V
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NX
      INTEGER N
      INTEGER L
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HOBJ, NX)
      IF (ERR_GOOD()) IERR = IO_XML_WL_1(HOBJ, SO_ALLC_HEXIST(L))
      IF (SO_ALLC_HEXIST(L)) THEN
         IF (ERR_GOOD()) IERR = IO_XML_WX_N(HOBJ,
     &                                      N,
     &                                      XA(SO_ALLC_REQXIND(XA,L)),
     &                                      IX)
      ENDIF

      IO_XML_WX_V = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WL_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WL_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL I

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      IF (I) THEN
         WRITE(MW, '(A)', ERR=9900) '<logical>True</logical>'
      ELSE
         WRITE(MW, '(A)', ERR=9900) '<logical>False</logical>'
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,L6)') 'ERR_ECRITURE_LOGICAL', ': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WL_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WL_N(HOBJ, N, LX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WL_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      LOGICAL LX(*)
      INTEGER IX

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER I, JX
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(MW, '(3A,I0,A)', ERR=9900) '<container ',
     &                                 'type="logical" ',
     &                                 'size=" ', N, '">'

      IF (IX .EQ. 1) THEN
         DO I=1,N
            IERR = IO_XML_WL_1(HOBJ, LX(I))
         ENDDO
      ELSE
         JX = 1
         DO I=1,N
            IERR = IO_XML_WL_1(HOBJ, LX(JX))
            JX = JX + IX
         ENDDO
      ENDIF

      WRITE(MW, '(A)', ERR=9900) '</container>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_CONTAINER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WL_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
      INTEGER LB, LS
      CHARACTER*(8) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      LS = SP_STRN_LEN(S)
      WRITE(BUF, '(I8)', ERR=9900) LS
      CALL SP_STRN_TRM(BUF)
      LB = SP_STRN_LEN(BUF)
      WRITE(MW, '(5A)', ERR=9900)
     &   '<string>',
     &   BUF(1:LB), ':', S(1:LS),
     &  '</string>'

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_ECRITURE_STRING', ': ', S(1:LS)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris un tag.
C
C Description: Write a tag avec des attributs
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_XML_WT(HOBJ, TNAM, TTXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_XML_WT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) TNAM
      CHARACTER*(*) TTXT

      INCLUDE 'ioxml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ioxml.fc'

      INTEGER IERR
      INTEGER MW
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_XML_HVALIDE(HOBJ))
D     CALL ERR_PRE(IO_XML_UNIT(HOBJ-IO_XML_HBASE) .NE. 0)
D     CALL ERR_PRE(IO_UTIL_MODACTIF(IO_XML_ISTA(HOBJ-IO_XML_HBASE),
D    &                              IO_MODE_ECRITURE))
D     CALL ERR_PRE(SP_STRN_LEN(TNAM) .GT. 0)
C------------------------------------------------------------------------

      MW = IO_XML_UNIT(HOBJ-IO_XML_HBASE)

      WRITE(MW, '(5A)', ERR=9900) '<', TNAM, ' ', TTXT, '/>'

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_ECRITURE_TAG', ': ',
     &                      TNAM(1:SP_STRN_LEN(TNAM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_XML_WT = ERR_TYP()
      RETURN
      END


