C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER OB_OBJN_000
C     INTEGER OB_OBJN_999
C     INTEGER OB_OBJN_CTR
C     INTEGER OB_OBJN_CPY
C     INTEGER OB_OBJN_REQCNT
C     INTEGER OB_OBJN_REQDTA
C     INTEGER OB_OBJN_DTR
C     LOGICAL OB_OBJN_HVALIDE
C     LOGICAL OB_OBJN_TVALIDE
C     LOGICAL OB_OBJN_HEXIST
C     INTEGER OB_OBJC_000
C     INTEGER OB_OBJC_999
C     INTEGER OB_OBJC_CTR
C     INTEGER OB_OBJC_DTR
C     INTEGER OB_OBJC_PKL
C     INTEGER OB_OBJC_PKLALL
C     INTEGER OB_OBJC_UPK
C     INTEGER OB_OBJC_UPKALL
C     INTEGER OB_OBJC_ERRH
C     LOGICAL OB_OBJC_ESTHBASE
C     LOGICAL OB_OBJC_HVALIDE
C     LOGICAL OB_OBJC_TVALIDE
C     LOGICAL OB_OBJC_HLIBRE
C     INTEGER OB_OBJC_ASGCLS
C     INTEGER OB_OBJC_ASGLNK
C     INTEGER OB_OBJC_EFFLNK
C     INTEGER OB_OBJC_REQCLS
C     INTEGER OB_OBJC_REQNOMTYPE
C     INTEGER OB_OBJC_REQNOMCMPL
C     INTEGER OB_OBJC_REQNOBJ
C     INTEGER OB_OBJC_REQIOBJ
C   Private:
C     INTEGER OB_OBJC_REQNOMOBJ
C     INTEGER OB_OBJC_NEWHBASE
C     INTEGER OB_OBJC_NEWHPRXY
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction <code>OB_OBJC_OOO(...)</code> initialise les tables
C     internes de l'objet. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C     <p>
C     La fonction <code>OB_OBJC_OOO(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_000.
C
C Entrée:
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C     CHARACTER*(*)           Nom du type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_000(NOBJMAX, HBASE, NOM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       NOBJMAX
      INTEGER       HBASE
      CHARACTER*(*) NOM

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NOBJMAX .GE. OB_OBJC_IOBJMIN)
D     CALL ERR_PRE(NOBJMAX .LE. OB_OBJC_IOBJMAX)
C------------------------------------------------------------------------

      IF     (HBASE .EQ. OB_OBJC_HBSO_TAG) THEN
         HBASE = OB_OBJC_NEWHBASE()
      ELSEIF (HBASE .EQ. OB_OBJC_HBSP_TAG) THEN
         HBASE = OB_OBJC_NEWHPRXY()
      ELSE
         WRITE(ERR_BUF,'(2A,I12)')'ERR_BASE_HANDLE_INVALIDE',': ',HBASE
         CALL ERR_ASG(ERR_FTL, ERR_BUF)
      ENDIF

      IF (ERR_GOOD()) IERR = OB_OBJC_ASGCLS(HBASE, NOBJMAX, NOM)

      OB_OBJC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction <code>OB_OBJN_000(...)</code> initialise les tables
C     internes de l'objet. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C     <p>
C     La fonction <code>OB_OBJN_OOO(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_000.
C
C Entrée:
C     INTEGER HBASE           Base des handles
C     CHARACTER*(*)           Nom du type
C
C Sortie:
C
C Notes:
C     Code identique à OB_OBJC_000 à l'exception de NOBJMAX
C************************************************************************
      FUNCTION OB_OBJN_000(HBASE, NOM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HBASE
      CHARACTER*(*) NOM

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF     (HBASE .EQ. OB_OBJC_HBSO_TAG) THEN
         HBASE = OB_OBJC_NEWHBASE()
      ELSEIF (HBASE .EQ. OB_OBJC_HBSP_TAG) THEN
         HBASE = OB_OBJC_NEWHPRXY()
      ELSE
         WRITE(ERR_BUF,'(2A,I12)')'ERR_BASE_HANDLE_INVALIDE',': ',HBASE
         CALL ERR_ASG(ERR_FTL, ERR_BUF)
      ENDIF

      IF (ERR_GOOD()) IERR=OB_OBJC_ASGCLS(HBASE, OB_OBJC_IOBJMAX, NOM)

      OB_OBJN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction <code>OB_OBJC_999(...)</code> reset les tables
C     internes de l'objet. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets.
C     <p>
C     La fonction <code>OB_OBJC_999(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_999.
C
C Entrée:
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C     EXTERNAL FUNDTR         Fonction destructeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_999(NOBJMAX, HBASE, FUNDTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  NOBJMAX
      INTEGER  HBASE
      INTEGER  FUNDTR
      EXTERNAL FUNDTR

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER I
      INTEGER IERR
      INTEGER HOBJ
C------------------------------------------------------------------------
D     CALL ERR_PRE(NOBJMAX .GE. OB_OBJC_IOBJMIN)
D     CALL ERR_PRE(NOBJMAX .LE. OB_OBJC_IOBJMAX)
C------------------------------------------------------------------------

      IF (OB_OBJC_XOBJ .EQ. 0) GOTO 9999

      DO I=1,NOBJMAX
         HOBJ = HBASE + I
         IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .EQ. 0) THEN
            IERR = FUNDTR(HOBJ)
         ENDIF
      ENDDO

9999  CONTINUE
      OB_OBJC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction <code>OB_OBJN_999(...)</code> reset les tables
C     internes de l'objet. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets.
C     <p>
C     La fonction <code>OB_OBJN_999(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_999.
C
C Entrée:
C     INTEGER HBASE           Base des handles
C     EXTERNAL FUNDTR         Fonction destructeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_999(HBASE, FUNDTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_999
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HBASE
      INTEGER  FUNDTR
      EXTERNAL FUNDTR

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER I
      INTEGER IERR
      INTEGER HOBJ
C------------------------------------------------------------------------

      IF (OB_OBJC_XOBJ .EQ. 0) GOTO 9999

      DO I=OB_OBJC_IOBJMIN,OB_OBJC_IOBJMAX
         HOBJ = HBASE + I
         IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .EQ. 0) THEN
D           CALL ERR_ASR(OB_OBJN_REQCNT(HOBJ) .GT. 0)
            IF (OB_OBJN_REQCNT(HOBJ) .EQ. 1) THEN
               IERR = FUNDTR(HOBJ)
            ENDIF
         ENDIF
      ENDDO

9999  CONTINUE
      OB_OBJN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de l'objet
C
C Description:
C     La fonction <code>OB_OBJC_CTR(...)</code> est le constructeur
C     de l'objet. Elle recherche dans les tables internes un indice
C     de libre pour le nouvel objet.
C     <p>
C     La fonction <code>OB_OBJC_CTR(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_CTR.
C
C Entrée:
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C
C Sortie:
C     INTEGER IOBJ            Indice de l'objet
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_CTR(HOBJ, NOBJMAX, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NOBJMAX
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER I
      INTEGER IERR, IRET
C------------------------------------------------------------------------
D     CALL ERR_PRE(NOBJMAX .GE. OB_OBJC_IOBJMIN)
D     CALL ERR_PRE(NOBJMAX .LE. OB_OBJC_IOBJMAX)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HBASE))
C------------------------------------------------------------------------

C---     Au besoin, crée la map
      IF (OB_OBJC_XOBJ .EQ. 0) THEN
         OB_OBJC_XOBJ = C_MIV_CTR()
      ENDIF

C---     Cherche les entrées de HBASE
      DO I=1,NOBJMAX
         IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HBASE+I) .NE. 0) GOTO 200
      ENDDO
      GOTO 9900
200   CONTINUE

C---     Handle
      HOBJ = HBASE + I

C---     Register
      IRET = C_MIV_ASGVAL(OB_OBJC_XOBJ, HOBJ, OB_OBJC_NULL, 0)
      IF (IRET .NE. 0) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_CONSTRUCTION_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'ERR_NBR_OBJET_MAX:', NOBJMAX
      CALL ERR_AJT(ERR_BUF)
      CALL ERR_PUSH()
      IERR = OB_OBJC_REQNOMTYPE(ERR_BUF, HBASE)
      CALL ERR_POP()
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_INSERTION_MAP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      OB_OBJC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de l'objet
C
C Description:
C     La fonction <code>OB_OBJN_CTR(...)</code> est le constructeur
C     de l'objet. Elle recherche dans les tables internes un indice
C     de libre pour le nouvel objet.
C     <p>
C     La fonction <code>OB_OBJN_CTR(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_CTR.
C
C Entrée:
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C     C_PRT   CPTR            Pointeur aux données de l'objet
C
C Sortie:
C     INTEGER HOBJ            Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_CTR(HOBJ, HBASE, CPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_CTR
CDEC$ ENDIF

      !!!USE OB_HNDL_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(OUT):: HOBJ
      !!!TYPE(OB_HNDL_T), INTENT(OUT):: HOBJ
      INTEGER, INTENT(IN) :: HBASE
      TYPE(C_PTR), INTENT(IN) :: CPTR

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER I
      INTEGER IERR, IRET
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HBASE))
C------------------------------------------------------------------------

C---     Au besoin, crée le map
      IF (OB_OBJC_XOBJ .EQ. 0) THEN
         OB_OBJC_XOBJ = C_MIV_CTR()
      ENDIF

C---     Cherche les entrées de HBASE
      DO I=OB_OBJC_IOBJMIN,OB_OBJC_IOBJMAX
         IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HBASE+I) .NE. 0) GOTO 200
      ENDDO
      GOTO 9900
200   CONTINUE

C---     Handle
      HOBJ = HBASE + I
      !!!HOBJ = OB_HNDL_T(HBASE + I)

C---     Register
      IRET = C_MIV_ASGVAL(OB_OBJC_XOBJ, HOBJ, CPTR, 1)
      IF (IRET .NE. 0) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_CONSTRUCTION_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'ERR_NBR_OBJET_MAX:', OB_OBJC_IOBJMAX
      CALL ERR_AJT(ERR_BUF)
      CALL ERR_PUSH()
      IERR = OB_OBJC_REQNOMTYPE(ERR_BUF, HBASE)
      CALL ERR_POP()
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_INSERTION_MAP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      OB_OBJN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Copie: incrémente le compte de référence de l'objet
C
C Description:
C     La fonction <code>OB_OBJN_CPY(...)</code> est copie (shallow copy)
C     l'objet. Il incrémente le compte de référence sur l'objet.
C
C Entrée:
C     INTEGER HOBJ            Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_CPY(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_CPY
CDEC$ ENDIF

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR, IRET, ICTR
      TYPE(C_PTR) :: CPTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XOBJ .NE. 0)
D     CALL ERR_PRE(HOBJ .NE. 0)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HOBJ))
C------------------------------------------------------------------------

C---     Les données et le compte de référence
      IRET = C_MIV_REQVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)
      IF (IRET .NE. 0) GOTO 9900

C---     Incrémente le compte de référence
      ICTR = ICTR + 1
      IRET = C_MIV_ASGVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)
      IF (IRET .NE. 0) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_HANDLE_SANS_DONNEES')
      GOTO 9999
9901  CALL ERR_ASG(ERR_ERR, 'ERR_INSERTION_MAP')
      GOTO 9999

9999  CONTINUE
      OB_OBJN_CPY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le compte de référence de l'objet
C
C Description:
C     La fonction <code>OB_OBJN_REQCNT(...)</code> retourne
C     le compte de référence sur l'objet.
C
C Entrée:
C     INTEGER HOBJ            Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_REQCNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_REQCNT
CDEC$ ENDIF

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(OUT):: HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IRET, ICTR
      TYPE(C_PTR) :: CPTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XOBJ .NE. 0)
D     CALL ERR_PRE(HOBJ .NE. 0)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HOBJ))
C------------------------------------------------------------------------

      IRET = C_MIV_REQVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)

      OB_OBJN_REQCNT = ICTR
      RETURN
      END

C************************************************************************
C Sommaire: Requête des données
C
C Description:
C     La fonction <code>OB_OBJN_REQDTA(...)</code> retourne les données
C     associées au handle.
C
C Entrée:
C     INTEGER HOBJ            Handle sur l'objet
C
C Sortie:
C     C_PTR CPTR              Les données
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_REQDTA(HOBJ, CPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_REQDTA
CDEC$ ENDIF

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(IN):: HOBJ
      TYPE(C_PTR), INTENT(OUT) :: CPTR

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IRET
      INTEGER ICTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XOBJ .NE. 0)
D     CALL ERR_PRE(HOBJ .NE. 0)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HOBJ))
C------------------------------------------------------------------------

      IRET = C_MIV_REQVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_HANDLE_SANS_DONNEES')
      GOTO 9999

9999  CONTINUE
      OB_OBJN_REQDTA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de l'objet
C
C Description:
C     La fonction <code>OB_OBJC_DTR(...)</code> est le destructeur
C     de l'objet. Elle libère dans les tables internes l'indice
C     de l'objet.
C     <p>
C     La fonction <code>OB_OBJC_DTR(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_DTR.
C
C Entrée:
C     INTEGER HOBJ            Handle sur l'objet
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_DTR(HOBJ, NOBJMAX, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  NOBJMAX
      INTEGER  HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XOBJ .NE. 0)
D     CALL ERR_PRE(HOBJ .NE. 0)
D     CALL ERR_PRE(NOBJMAX .GE. OB_OBJC_IOBJMIN)
D     CALL ERR_PRE(NOBJMAX .LE. OB_OBJC_IOBJMAX)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HBASE))
C------------------------------------------------------------------------

      IOB = HOBJ - HBASE
D     CALL ERR_ASR(IOB .GE. 1)
D     CALL ERR_ASR(IOB .LE. NOBJMAX)

C---     Supprime l'entrée de HOBJ
      IF (C_MIV_EFFCLF(OB_OBJC_XOBJ, HOBJ) .NE. 0) GOTO 9900

C---     Reset HOBJ
      HOBJ = 0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_DESTRUCTION_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'ERR_OBJET_INEXISTANT:', HOBJ
      CALL ERR_AJT(ERR_BUF)
      CALL ERR_PUSH()
      IERR = OB_OBJC_REQNOMTYPE(ERR_BUF, HBASE)
      IF (ERR_GOOD()) CALL ERR_AJT(ERR_BUF)
      CALL ERR_POP()
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_INSERTION_MAP'

9999  CONTINUE
      OB_OBJC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de l'objet
C
C Description:
C     La fonction <code>OB_OBJN_DTR(...)</code> est le destructeur
C     de l'objet. Elle libère dans les tables internes l'indice
C     de l'objet.
C     <p>
C     La fonction <code>OB_OBJN_DTR(...)</code> tiens le rôle de parent
C     et regroupe les fonctionnalités communes aux autres fonctions
C     xx_xxx_DTR.
C
C Entrée:
C     INTEGER HOBJ            Handle sur l'objet
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_DTR(HOBJ, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_DTR
CDEC$ ENDIF

      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR, IRET, ICTR
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XOBJ .NE. 0)
D     CALL ERR_PRE(HOBJ .NE. 0)
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HBASE))
C------------------------------------------------------------------------

C---     Les données et le compte de référence
      IRET = C_MIV_REQVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)
      IF (IRET .NE. 0) GOTO 9900

C---     Supprime l'entrée de HOBJ
      ICTR = ICTR - 1
      IF (ICTR .LE. OB_OBJC_MINC) THEN
         IRET = C_MIV_EFFCLF(OB_OBJC_XOBJ, HOBJ)
      ELSE
         IRET = C_MIV_ASGVAL(OB_OBJC_XOBJ, HOBJ, CPTR, ICTR)
      ENDIF
      IF (IRET .NE. 0) GOTO 9900

C---     Reset HOBJ
      HOBJ = 0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_DESTRUCTION_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'ERR_OBJET_INEXISTANT:', HOBJ
      CALL ERR_AJT(ERR_BUF)
      CALL ERR_PUSH()
      IERR = OB_OBJC_REQNOMTYPE(ERR_BUF, HBASE)
      IF (ERR_GOOD()) CALL ERR_AJT(ERR_BUF)
      CALL ERR_POP()
      GOTO 9999

9999  CONTINUE
      OB_OBJN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les classes sont enregistrées au démarrage. Il n'y a pas besoin
C     de l'info de classe dans PKL, seulement du handle.
C************************************************************************
      FUNCTION OB_OBJC_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NOBJMAX
      INTEGER HBASE
      INTEGER HXML

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      CHARACTER*(256) TAG
      CHARACTER*(64) NT
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les données de classe
      IF (ERR_GOOD()) IERR = OB_OBJC_REQCLS(HBASE, NOBJMAX, NT, HOBJ)

C---     Register
      IF (ERR_GOOD())
     &   WRITE(TAG, '(3(A,I0,A),(A,A,A))', ERR=9901)
     &      'hobj="',   HOBJ,    '" ',
     &      'objmax="', NOBJMAX, '" ',
     &      'hbase="',  HBASE,   '" ',
     &      'name="',   NT(1:SP_STRN_LEN(NT)), '"'
      IF (ERR_GOOD()) IERR = IO_XML_WT(HXML,
     &                                 'class',
     &                                 TAG(1:SP_STRN_LEN(TAG)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_PKL_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF,'(A,I6)') 'MSG_HANDLE:', HOBJ
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'MSG_HBASE:', HBASE
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I6)') 'MSG_NOBJMAX:', NOBJMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      OB_OBJC_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_PKLALL(FBSE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_PKLALL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FBSE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR, IRET
      INTEGER IB
      INTEGER ISTAT
      INTEGER NOBJ
      INTEGER HOBJ, HXML
      INTEGER LB, LS
      CHARACTER*(256) FNAME
      LOGICAL DOCLOSE
C------------------------------------------------------------------------

      LB = SP_STRN_LEN(FBSE)
      IF (ERR_GOOD()) IERR = IO_XML_CTR(HXML)
      IF (ERR_GOOD()) IERR = IO_XML_INI(HXML)
      ISTAT = IO_MODE_ECRITURE + IO_MODE_1DEEPCPY

C---     Boucle sur tous les objets enregistrés
      NOBJ = C_MIV_REQDIM(OB_OBJC_XOBJ)
      DO IB=1,NOBJ
         IF (.NOT. ERR_GOOD()) GOTO 199

C---        Monte le nom de fichier
         IRET = C_MIV_REQCLF(OB_OBJC_XOBJ, IB, HOBJ)
         IF (IRET .NE. 0) GOTO 9900
         WRITE(FNAME, *, ERR=9900) FBSE(1:LB), '/', HOBJ, '.xml'
         CALL SP_STRN_DLC(FNAME, ' ')

C---        Ouvre le fichier
         LS = SP_STRN_LEN(FNAME)
         IF (ERR_GOOD()) IERR = IO_XML_OPEN(HXML, FNAME(1:LS), ISTAT)
         DOCLOSE = ERR_GOOD()

C---        Pickle
         IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, HOBJ)

C---        Ferme le fichier
         CALL ERR_PUSH()
         IF (DOCLOSE) IERR = IO_XML_CLOSE(HXML)
         CALL ERR_POP()

C---        En cas d'erreur, détruis le fichier
         IF (ERR_BAD()) THEN
            IRET = C_OS_DELFIC(FNAME(1:LS))
         ENDIF

C---        Gère les erreurs
         IF (ERR_ESTMSG('ERR_MODULE_INVALIDE')) THEN
            CALL ERR_RESET()
         ELSEIF (ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) THEN
            CALL ERR_RESET()
         ENDIF

      ENDDO
199   CONTINUE

      IF (IO_XML_HVALIDE(HXML)) IERR = IO_XML_DTR(HXML)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      OB_OBJC_PKLALL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HXML     Handle sur le fichier XML de pickle
C
C Sortie:
C     LNEW     .TRUE. si l'objet n'existe pas
C     HOBJ     Handle sur l'objet restauré
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR, IRET
      INTEGER IDUM
      INTEGER IOB
      INTEGER NOBJMAX
      INTEGER HBASE
      CHARACTER*(256) BUF

      INTEGER NVAL
      PARAMETER (NVAL = 4)
      CHARACTER*(32) KVAR(NVAL)
      CHARACTER*(32) KVAL(NVAL)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Au besoin, crée la liste
      IF (OB_OBJC_XOBJ .EQ. 0) THEN
         OB_OBJC_XOBJ = C_MIV_CTR()
      ENDIF

C---     Récupère les données de classe
      IF (ERR_GOOD()) IERR = IO_XML_RT(HXML, 'class', NVAL, KVAR, KVAL)
D     CALL ERR_ASR(ERR_BAD() .OR. KVAR(1) .EQ. 'hobj')
D     CALL ERR_ASR(ERR_BAD() .OR. KVAR(2) .EQ. 'objmax')
D     CALL ERR_ASR(ERR_BAD() .OR. KVAR(3) .EQ. 'hbase')
D     CALL ERR_ASR(ERR_BAD() .OR. KVAR(4) .EQ. 'name')
      IF (ERR_GOOD()) READ(KVAL(1), *, ERR=9900) HOBJ
      IF (ERR_GOOD()) READ(KVAL(2), *, ERR=9900) NOBJMAX
      IF (ERR_GOOD()) READ(KVAL(3), *, ERR=9900) HBASE

C---     Contrôles
      IOB = HOBJ - HBASE
      IF (IOB .LT. 1) GOTO 9901
      IF (IOB .GT. NOBJMAX) GOTO 9901

C---     Contrôle que HOBJ est libre
      LNEW = OB_OBJC_HLIBRE(HOBJ, NOBJMAX, HBASE)
!!      IF (.NOT. HL) GOTO 9902
      IF (.NOT. LNEW) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_OBJET_EXISTANT'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,'(A,I12)') 'MSG_HANDLE:', HOBJ
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Register
      IF (LNEW) THEN
         IRET = C_MIV_ASGVAL(OB_OBJC_XOBJ, HOBJ, OB_OBJC_NULL, 0)
         IF (IRET .NE. 0) GOTO 9902
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_UPK_OBJET'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      CALL SP_STRN_CLP(BUF, 80)
      WRITE(ERR_BUF,'(2A)') 'MSG_BUFFER:', BUF(1:SP_STRN_LEN(BUF))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_HANDLE:', HOBJ
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_HBASE:', HBASE
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NOBJMAX:', NOBJMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_OBJET_EXISTANT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_HANDLE:', HOBJ
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      OB_OBJC_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On utilise les fonctions de lecture C pour éviter un conflit
C     avec une numéro d'unité pas encore restauré.
C************************************************************************
      FUNCTION OB_OBJC_UPKALL(FBSE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_UPKALL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FBSE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER OB_OBJC_UPKONE

      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER IERR, IRET
      INTEGER IDUM
      INTEGER ISTAT
      INTEGER LS
      INTEGER NTOK
      INTEGER HOBJ, HXML
      CHARACTER*(32)  SVAL
      CHARACTER*(256) FTMP, FNAM
      LOGICAL DOCLOSE
      INTEGER, PARAMETER :: IRCR = 0   ! Non récursif
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Liste les *.xml dans un fichier
      FTMP = ' '
      IF (IRET .EQ. 0) IRET = C_OS_FICTMP(FTMP)
      IF (IRET .EQ. 0) IRET = C_OS_DIR(FBSE(1:SP_STRN_LEN(FBSE)),
     &                                 '*.xml',
     &                                 FTMP(1:SP_STRN_LEN(FTMP)),
     &                                 IRCR)
      IF (IRET .NE. 0) GOTO 9900

C---     Ouvre le fichier de fichiers
      XFIC = 0
      IF (ERR_GOOD()) THEN
         XFIC = C_FA_OUVRE(FTMP(1:SP_STRN_LEN(FTMP)), C_FA_LECTURE)
         IF (XFIC .EQ. 0) GOTO 9901
      ENDIF

C---     Boucle sur les fichiers
      HXML = 0
      IF (ERR_GOOD()) IERR = IO_XML_CTR(HXML)
      IF (ERR_GOOD()) IERR = IO_XML_INI(HXML)
      ISTAT = IO_MODE_LECTURE + IO_MODE_1DEEPCPY
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IRET = C_FA_LISLN(XFIC, FNAM)
         IF (IRET .EQ. -1) GOTO 199
         IF (IRET .NE.  0) GOTO 9901

C---        Extrait le handle
         CALL SP_STRN_SBC(FNAM, '\\', '/')
         NTOK = SP_STRN_NTOK(FNAM, '/')
         IRET = SP_STRN_TKS (FNAM, '/', NTOK, SVAL)
         IDUM = SP_STRN_TKI (SVAL, '.', 1, HOBJ)
         IF (IRET .NE. 0) GOTO 9901

C---        Ouvre le fichier
         LS = SP_STRN_LEN(FNAM)
         IF (ERR_GOOD()) IERR = IO_XML_OPEN(HXML, FNAM(1:LS), ISTAT)
         DOCLOSE = ERR_GOOD()

C---        Un-pickle
         IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, HOBJ)

C---        Ferme le fichier
         CALL ERR_PUSH()
         IF (DOCLOSE) IERR = IO_XML_CLOSE(HXML)
         CALL ERR_POP()

C---        Gère les erreurs
         IF (ERR_ESTMSG('ERR_OBJET_EXISTANT')) THEN
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF

         GOTO 100
199   CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_CREE_FICTMP'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_FICTMP'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (IO_XML_HVALIDE(HXML)) IERR = IO_XML_DTR(HXML)
      IF (XFIC .NE. 0) IRET = C_FA_FERME(XFIC)
      IF (SP_STRN_LEN(FTMP) .GT. 0)
     &   IRET = C_OS_DELFIC(FTMP(1:SP_STRN_LEN(FTMP)))

      OB_OBJC_UPKALL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Message d'erreur pour un handle invalide.
C
C Description:
C     La fonction <code>OB_OBJC_ERRH(...)</code> monte dans ERR_BUF
C     le message d'erreur pour un handle invalide.
C
C Entrée:
C     HOBJ            Handle à tester
C     MSG             Type attendu
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_ERRH(HOBJ, MSG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_ERRH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) MSG

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER         IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      CALL ERR_POP()

      IF (IERR .EQ. ERR_OK) THEN
         WRITE (ERR_BUF,'(3A)')
     &         'MSG_ATTEND', ': ', MSG(1:SP_STRN_LEN(MSG))
         CALL ERR_AJT(ERR_BUF)
         WRITE (ERR_BUF,'(3A)')
     &         'MSG_OBTIENT', ': ', NOM(1:SP_STRN_LEN(NOM))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      OB_OBJC_ERRH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est une base valide
C
C Description:
C     La fonction <code>OB_OBJC_ESTHBASE(...)</code> retourne .TRUE. si
C     le handle passé en argument est une base de handle valide.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJC_ESTHBASE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_ESTHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IC, ID, IT
      LOGICAL HBASE
C------------------------------------------------------------------------

      HBASE = .FALSE.
      IC = HOBJ / OB_OBJC_OBJRNG

C---     Les objets
      IF (.NOT. HBASE) THEN
         ID = (IC - OB_OBJC_HBSO)
         IT = (IC - ID)
         IF (ID .GT. 0) THEN
            IF (ID .LE. OB_OBJC_HNXT) THEN
               IF (IT .EQ. OB_OBJC_HBSO) THEN
                  HBASE = .TRUE.
               ENDIF
            ENDIF
         ENDIF
      ENDIF

C---     Les proxy
      IF (.NOT. HBASE) THEN
         ID = (IC - OB_OBJC_HBSP)
         IT = (IC - ID)
         IF (ID .GT. 0) THEN
            IF (ID .LE. OB_OBJC_HNXT) THEN
               IF (IT .EQ. OB_OBJC_HBSP) THEN
                  HBASE = .TRUE.
               ENDIF
            ENDIF
         ENDIF
      ENDIF

      OB_OBJC_ESTHBASE = HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est valide
C
C Description:
C     La fonction <code>OB_OBJC_HVALIDE(...)</code> retourne .TRUE. si
C     le handle passé en argument est valide dans le contexte donné par
C     HBASE, NOBJMAX.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJC_HVALIDE(HOBJ, NOBJMAX, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NOBJMAX
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL HVALIDE
C------------------------------------------------------------------------

      HVALIDE = .FALSE.
      IF (HOBJ .GT. HBASE) THEN
         IF (HOBJ .LE. HBASE+NOBJMAX) THEN
            IF (OB_OBJC_XOBJ .NE. 0) THEN
               IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .EQ. 0) THEN
                  IF (OB_OBJC_ESTHBASE(HOBJ)) THEN
                     HVALIDE = .TRUE.
                  ENDIF
               ENDIF
            ENDIF
         ENDIF
      ENDIF

      OB_OBJC_HVALIDE = HVALIDE
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est valide
C
C Description:
C     La fonction <code>OB_OBJN_HVALIDE(...)</code> retourne .TRUE. si
C     le handle passé en argument est valide dans le contexte donné par
C     HBASE.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJN_HVALIDE(HOBJ, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL HVALIDE
C------------------------------------------------------------------------

      HVALIDE = .FALSE.
      IF (HOBJ .GE. HBASE+OB_OBJC_IOBJMIN) THEN
         IF (HOBJ .LE. HBASE+OB_OBJC_IOBJMAX) THEN
            IF (OB_OBJC_XOBJ .NE. 0) THEN
               IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .EQ. 0) THEN
                  IF (OB_OBJC_ESTHBASE(HOBJ)) THEN
                     HVALIDE = .TRUE.
                  ENDIF
               ENDIF
            ENDIF
         ENDIF
      ENDIF

      OB_OBJN_HVALIDE = HVALIDE
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est valide
C
C Description:
C     La fonction <code>OB_OBJC_TVALIDE(...)</code> retourne .TRUE. si
C     le handle passé en argument est valide dans le contexte donné par
C     HBASE, NOBJMAX.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJC_TVALIDE(HOBJ, NOBJMAX, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_TVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NOBJMAX
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL TVALIDE
C------------------------------------------------------------------------

      TVALIDE = .FALSE.
      IF (HOBJ .GT. HBASE .AND. HOBJ .LE. HBASE+NOBJMAX) THEN
         TVALIDE = .TRUE.
      ENDIF

      OB_OBJC_TVALIDE = TVALIDE
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est valide
C
C Description:
C     La fonction <code>OB_OBJN_TVALIDE(...)</code> retourne .TRUE. si
C     le handle passé en argument est valide dans le contexte donné par
C     HBASE.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJN_TVALIDE(HOBJ, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_TVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL TVALIDE
C------------------------------------------------------------------------

      TVALIDE = .FALSE.
      IF (HOBJ .GE. HBASE+OB_OBJC_IOBJMIN .AND.
     &    HOBJ .LE. HBASE+OB_OBJC_IOBJMAX) THEN
         TVALIDE = .TRUE.
      ENDIF

      OB_OBJN_TVALIDE = TVALIDE
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle existe dans le système.
C
C Description:
C     La fonction <code>OB_OBJN_HEXIST(...)</code> retourne .TRUE. si
C     le handle passé en argument existe dans le système.
C     Ce test est moins exhaustif que OB_OBJN_HVALIDE.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJN_HEXIST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJN_HEXIST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL HEXIST
C------------------------------------------------------------------------

      HEXIST = .FALSE.
      IF (OB_OBJC_XOBJ .NE. 0) THEN
         IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .EQ. 0) THEN
            IF (OB_OBJC_ESTHBASE(HOBJ)) THEN
               HEXIST = .TRUE.
            ENDIF
         ENDIF
      ENDIF

      OB_OBJN_HEXIST = HEXIST
      RETURN
      END

C************************************************************************
C Sommaire: Test si un handle est libre
C
C Description:
C     La fonction <code>OB_OBJC_HLIBRE(...)</code> retourne .TRUE. si
C     le handle passé en argument est libre dans le contexte donné par
C     HBASE, NOBJMAX.
C
C Entrée:
C     INTEGER HOBJ            Handle à tester
C     INTEGER NOBJMAX         Nombre d'objets max
C     INTEGER HBASE           Base des handles
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION OB_OBJC_HLIBRE(HOBJ, NOBJMAX, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_HLIBRE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NOBJMAX
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'obobjc.fc'

      LOGICAL HFREE
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_ESTHBASE(HOBJ))
C------------------------------------------------------------------------

      HFREE = .FALSE.
      IF (HOBJ .GT. HBASE) THEN
         IF (HOBJ .LE. HBASE+NOBJMAX) THEN
            IF (C_MIV_ESTCLF(OB_OBJC_XOBJ, HOBJ) .NE. 0) THEN
               HFREE = .TRUE.
            ENDIF
         ENDIF
      ENDIF

      OB_OBJC_HLIBRE = HFREE
      RETURN
      END

C************************************************************************
C Sommaire: Enregistre une classe.
C
C Description:
C     La fonction <code>OB_OBJC_ASGCLS(...)</code> enregistre une classe
C     avec sa base de handle qui agit comme identifiant de classe (HBASE)
C     et ses information de classe, nombre d'objet max et nom.
C
C Entrée:
C     INTEGER       HBASE     Base de handle
C     INTEGER       NOBJMAX   Nombre d'objets max
C     CHARACTER*(*) NOM       Nom de la classe
C
C Sortie:
C
C Notes:
C     C_MAP travaille avec un INTEGER*8. Il faut être attentif à toute
C     utilisation de OB_OBJC_XMAP.
C     La valeur est une chaîne qui concatène NOBJMAX (1:12) et le nom (13:)
C************************************************************************
      FUNCTION OB_OBJC_ASGCLS(HBASE, NOBJMAX, NOM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_ASGCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HBASE
      INTEGER       NOBJMAX
      CHARACTER*(*) NOM

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER        IERR
      CHARACTER*(32) KEY, VAR
C------------------------------------------------------------------------

C---     Au besoin, crée le map
      IF (OB_OBJC_XMAP .EQ. 0) THEN
         OB_OBJC_XMAP = C_MAP_CTR()
      ENDIF

C---     HBASE comme string
      WRITE(KEY, '(I12)') HBASE
      CALL SP_STRN_TRM(KEY)

C---     NOBJMAX comme string
      WRITE(VAR, '(I12)') NOBJMAX
      CALL SP_STRN_TRM(VAR)

C---     Insère dans le map
      IERR = C_MAP_ASGVAL(OB_OBJC_XMAP,
     &                    KEY(1:SP_STRN_LEN(KEY)),
     &                    VAR(1:SP_STRN_LEN(KEY)) //
     &                    ';' //
     &                    NOM(1:SP_STRN_LEN(NOM)))
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_ASSIGNATION_MAP')
      ENDIF

      OB_OBJC_ASGCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un lien entre deux handles.
C
C Description:
C     La fonction <code>OB_OBJC_ASGLNK(...)</code> ajoute un lien entre un
C     handle de proxy et le handle qu'il gère.
C
C Entrée:
C     INTEGER HOBJ      Handle du proxy
C     INTEGER HLNK      Handle géré
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_ASGLNK(HOBJ, HLNK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_ASGLNK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLNK

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      CHARACTER*(12) VAR, VAL
C------------------------------------------------------------------------

C---     Au besoin, crée le map
      IF (OB_OBJC_XLNK .EQ. 0) THEN
         OB_OBJC_XLNK = C_MAP_CTR()
      ENDIF

C---     Les var et val comme string
      WRITE(VAR, '(I12)') HOBJ
      CALL SP_STRN_TRM(VAR)
      WRITE(VAL, '(I12)') HLNK
      CALL SP_STRN_TRM(VAL)

C---     Insère dans le map
      IERR = C_MAP_ASGVAL(OB_OBJC_XLNK,
     &                    VAR(1:SP_STRN_LEN(VAR)),
     &                    VAL(1:SP_STRN_LEN(VAL)))
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_ASSIGNATION_MAP')
      ENDIF

      OB_OBJC_ASGLNK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retire un lien entre deux handles.
C
C Description:
C     La fonction <code>OB_OBJC_EFFLNK(...)</code> efface le lien associé
C     au handle de proxy passé en argument.
C
C Entrée:
C     INTEGER HOBJ      Base de handle
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_EFFLNK(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_EFFLNK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLNK

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      CHARACTER*(12) VAR, VAL
C------------------------------------------------------------------------
D     CALL ERR_ASR(OB_OBJC_XLNK .NE. 0)
C------------------------------------------------------------------------

C---     Les var et val comme string
      WRITE(VAR, '(I12)') HOBJ
      CALL SP_STRN_TRM(VAR)

C---     Insère dans le map
      IERR = C_MAP_EFFCLF(OB_OBJC_XLNK,
     &                    VAR(1:SP_STRN_LEN(VAR)))
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_EFFACE_MAP')
      ENDIF

      OB_OBJC_EFFLNK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les informations de classe associées à un handle.
C
C Description:
C     La fonction <code>OB_OBJC_REQCLS(...)</code> retourne les informations
C     de classe, HBASE, NOBJMAX, et le nom.
C
C Entrée:
C     INTEGER       HOBJ      Handle
C
C Sortie:
C     INTEGER       HBASE     Base de handle
C     INTEGER       NOBJMAX   Nombre d'objets max
C     CHARACTER*(*) NOMOBJ    Nom de la classe
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_REQCLS(HBASE, NOBJMAX, NOM, HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HBASE
      INTEGER       NOBJMAX
      CHARACTER*(*) NOM
      INTEGER       HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER         IERR
      CHARACTER*(128) BUF
      CHARACTER*(16)  VAR
C------------------------------------------------------------------------
D     CALL ERR_PRE(OB_OBJC_XMAP .NE. 0)
D     CALL ERR_PRE(HOBJ         .NE. 0)
C------------------------------------------------------------------------

C---     Extrait HBASE de HOBJ
      HBASE = (HOBJ/OB_OBJC_OBJRNG)*OB_OBJC_OBJRNG

C---     Recherche l'info de classe
      WRITE(VAR, '(I12)') HBASE
      CALL SP_STRN_TRM(VAR)
      IERR = C_MAP_REQVAL(OB_OBJC_XMAP,
     &                    VAR(1:SP_STRN_LEN(VAR)),
     &                    BUF)
      IF (IERR .NE. 0) THEN
         WRITE(ERR_BUF, '(4A)') 'ERR_CLASSE_INCONNUE', ': ',
     &                          'HBASE = ', VAR(1:SP_STRN_LEN(VAR))
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF
      IF (ERR_GOOD()) IERR = SP_STRN_TKI(BUF, ';', 1, NOBJMAX)
      IF (ERR_GOOD()) IERR = SP_STRN_TKS(BUF, ';', 2, NOM)

      OB_OBJC_REQCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom de classe associé à un handle.
C
C Description:
C     La fonction <code>OB_OBJC_REQNOMTYPE(...)</code>
C
C Entrée:
C     CHARACTER*(*) NOMOBJ    Nom de la classe
C     INTEGER       HOBJ      Handle
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_REQNOMTYPE(NOM, HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_REQNOMTYPE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOM
      INTEGER       HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      INTEGER HBASE
      INTEGER NOBJMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(HOBJ .NE. 0)
C------------------------------------------------------------------------

      IERR = OB_OBJC_REQCLS(HBASE, NOBJMAX, NOM, HOBJ)

      OB_OBJC_REQNOMTYPE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom complet associé à un handle.
C
C Description:
C     La fonction <code>OB_OBJC_REQNOMCMPL(...)</code> retourne le nom
C     complet associé à un handle. Celui-ci est formé du nom de type suivi
C     de l'indice.
C
C Entrée:
C     CHARACTER*(*) NOMOBJ    Nom de la classe
C     INTEGER       HOBJ      Handle
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_REQNOMOBJ(NOMOBJ, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) NOMOBJ
      INTEGER       HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      INTEGER IOB
      CHARACTER*(8) NIND
C------------------------------------------------------------------------

C---     Nom du type
      IF (HOBJ .EQ. 0) THEN
         NOMOBJ = '---'
      ELSE
         IERR = OB_OBJC_REQNOMTYPE(NOMOBJ, HOBJ)
      ENDIF

C---     Indice de l'objet
      NIND = ' '
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - (HOBJ/OB_OBJC_OBJRNG)*OB_OBJC_OBJRNG
         IF (IOB .GT. 0) THEN
            WRITE(NIND, '(A,I3.3,A)') '#', IOB, '# '
         ENDIF
      ENDIF

C---     Nom complet
      IF (ERR_GOOD() .AND. SP_STRN_LEN(NIND) .GT. 0) THEN
         NOMOBJ = NIND(1:6) // NOMOBJ(1:SP_STRN_LEN(NOMOBJ))
      ENDIF

      OB_OBJC_REQNOMOBJ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom complet associé à un handle.
C
C Description:
C     La fonction <code>OB_OBJC_REQNOMCMPL(...)</code> retourne le nom
C     complet associé à un handle. Celui-ci est formé du nom de type suivi
C     de l'indice.
C
C Entrée:
C     CHARACTER*(*) NOMOBJ    Nom de la classe
C     INTEGER       HOBJ      Handle
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_REQNOMCMPL(NOMOBJ, HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_REQNOMCMPL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOMOBJ
      INTEGER       HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'obobjc.fc'

      INTEGER IERR
      INTEGER HACTU, LACTU
      CHARACTER*(64) BUF
      CHARACTER*(12) VAR, VAL
C------------------------------------------------------------------------

C---     Nom de l'objet
      HACTU = HOBJ
      IERR = OB_OBJC_REQNOMOBJ(NOMOBJ, HACTU)

C---     Boucle sur les liens
      IF (OB_OBJC_XLNK .EQ. 0) GOTO 9999
100   CONTINUE
         WRITE(VAR, '(I12)') HACTU
         CALL SP_STRN_TRM(VAR)

         IERR = C_MAP_REQVAL(OB_OBJC_XLNK,
     &                       VAR(1:SP_STRN_LEN(VAR)),
     &                       VAL)
         IF (IERR .NE. 0) GOTO 9999

         READ(VAL, *) HACTU
         IERR = OB_OBJC_REQNOMOBJ(BUF, HACTU)

         LACTU = SP_STRN_LEN(NOMOBJ) + SP_STRN_LEN(BUF) + 5
         IF (LEN(NOMOBJ) .GE. LACTU) THEN
            NOMOBJ = NOMOBJ(1:SP_STRN_LEN(NOMOBJ)) //
     &               ' --> ' //
     &               BUF(1:SP_STRN_LEN(BUF))
         ENDIF

      GOTO 100

9999  CONTINUE
      OB_OBJC_REQNOMCMPL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_REQNOBJ(HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_REQNOBJ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER   IRET
      INTEGER   IB
      INTEGER   NOBJ, HOBJ
      INTEGER*8 XITER
C------------------------------------------------------------------------

      NOBJ = 0
      IF (OB_OBJC_XOBJ .NE. 0) THEN
         XITER = 0
         HOBJ  = HBASE
         IRET  = 0
         DO WHILE (IRET .EQ. 0)
            IRET = C_MIV_ITER(OB_OBJC_XOBJ, XITER, HOBJ)
            IF (OB_OBJN_TVALIDE(HOBJ, HBASE)) THEN
               NOBJ = NOBJ + 1
            ELSEIF (IRET .EQ. 0) THEN
               HOBJ = -1
               IRET = C_MIV_ITER(OB_OBJC_XOBJ, XITER, HOBJ)
               GOTO 199
            ENDIF
         ENDDO
      ENDIF
199   CONTINUE

      OB_OBJC_REQNOBJ = NOBJ
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction <code>OB_OBJC_REQIOBJ(...)</code> retourne le IOBème 
C     handle avec la base de handle HBASE.
C
C Entrée:
C     IOB      Indice demandé
C     HBASE    Base de handle
C
C Sortie:
C
C Notes:
C     Il faut aller en fin d'itération pour notifier la fin et éviter
C     un memory leak dans C_MIV_ITER
C************************************************************************
      FUNCTION OB_OBJC_REQIOBJ(IOB, HBASE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: OB_OBJC_REQIOBJ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOB
      INTEGER HBASE

      INCLUDE 'obobjc.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fc'

      INTEGER   IRET
      INTEGER   NOBJ, HOBJ, HTMP
      INTEGER*8 XITER
C------------------------------------------------------------------------

      HOBJ = 0

      NOBJ  = 0
      XITER = 0
      IRET  = 0
      HTMP = HBASE
      DO WHILE (IRET .EQ. 0)
         IRET = C_MIV_ITER(OB_OBJC_XOBJ, XITER, HTMP)
         IF (OB_OBJN_TVALIDE(HTMP, HBASE)) THEN
            NOBJ = NOBJ + 1
            IF (NOBJ .EQ. IOB) THEN
               HOBJ = HBASE + IOB
               HTMP = -1
               IRET = C_MIV_ITER(OB_OBJC_XOBJ, XITER, HTMP)
               GOTO 199
            ENDIF
         ENDIF
      ENDDO
199   CONTINUE

      OB_OBJC_REQIOBJ = HOBJ
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une base de handle.
C
C Description:
C     La fonction privée <code>OB_OBJC_NEWHBASE(...)</code> retourne
C     une nouvelle base de handle.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_NEWHBASE()

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'obobjc.fc'
C------------------------------------------------------------------------

      OB_OBJC_HNXT = OB_OBJC_HNXT + 1

      OB_OBJC_NEWHBASE = (OB_OBJC_HBSO + OB_OBJC_HNXT) * OB_OBJC_OBJRNG
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une base de handle.
C
C Description:
C     La fonction privée <code>OB_OBJC_NEWHBASE(...)</code> retourne
C     une nouvelle base de handle pour un proxy.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION OB_OBJC_NEWHPRXY()

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'obobjc.fc'
C------------------------------------------------------------------------

      OB_OBJC_HNXT = OB_OBJC_HNXT + 1

      OB_OBJC_NEWHPRXY = (OB_OBJC_HBSP + OB_OBJC_HNXT) * OB_OBJC_OBJRNG
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data OB_OBJC initialise les COMMON propres au module
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA OB_OBJC

      IMPLICIT NONE

      INCLUDE 'obobjc.fc'

      DATA OB_OBJC_XOBJ /0/
      DATA OB_OBJC_XMAP /0/
      DATA OB_OBJC_XLNK /0/
      DATA OB_OBJC_HNXT /0/
      DATA OB_OBJC_MINC /0/

      END
