C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_ICMD_000
C     INTEGER IC_ICMD_999
C     INTEGER IC_ICMD_PKL
C     INTEGER IC_ICMD_UPK
C     INTEGER IC_ICMD_CTR
C     INTEGER IC_ICMD_DTR
C     INTEGER IC_ICMD_INI
C     INTEGER IC_ICMD_RST
C     INTEGER IC_ICMD_REQHBASE
C     LOGICAL IC_ICMD_HVALIDE
C     INTEGER IC_ICMD_EVAL
C     INTEGER IC_ICMD_XEQ
C     INTEGER IC_ICMD_ADDFNC
C     INTEGER IC_ICMD_ADDCLS
C     INTEGER IC_ICMD_ADDMDL
C     INTEGER IC_ICMD_REQSMB
C   Private:
C     INTEGER IC_ICMD_ADDKW
C     LOGICAL IC_ICMD_ESTKW
C     LOGICAL IC_ICMD_ESTCW
C     LOGICAL IC_ICMD_ESTIF
C     LOGICAL IC_ICMD_ESTELIF
C     LOGICAL IC_ICMD_ESTWHILE
C     INTEGER IC_ICMD_ERRVAL
C     LOGICAL IC_ICMD_DOPARS
C     LOGICAL IC_ICMD_ESTFNC
C     LOGICAL IC_ICMD_ESTFNI
C     INTEGER IC_ICMD_ASGSMB
C     INTEGER IC_ICMD_ASGSMBSMB
C     INTEGER IC_ICMD_ASGSMBBRK
C     INTEGER IC_ICMD_ASGSMBDOT
C     INTEGER IC_ICMD_EXT2INT
C     INTEGER IC_ICMD_INT2EXT
C     INTEGER IC_ICMD_REQHFNCFNC
C     INTEGER IC_ICMD_REQHFNCFNI
C     INTEGER IC_ICMD_REQHFNCCLS
C     INTEGER IC_ICMD_REQHNDLCLS
C     INTEGER IC_ICMD_REQSMBSMB
C     INTEGER IC_ICMD_REQSMBBRK
C     INTEGER IC_ICMD_REQSMBDOT
C     INTEGER IC_ICMD_UNWNDSTK
C     INTEGER IC_ICMD_PRSFIC
C     INTEGER IC_ICMD_PRSPIL
C     INTEGER IC_ICMD_PRS999
C     INTEGER IC_ICMD_XEQFIC
C     INTEGER IC_ICMD_XEQFNC
C     INTEGER IC_ICMD_XEQFNCFNC
C     INTEGER IC_ICMD_XEQFNCFNI
C     INTEGER IC_ICMD_XEQFNCFNI_END
C     INTEGER IC_ICMD_XEQFNCKWD
C     INTEGER IC_ICMD_XEQFNCDOT
C     INTEGER IC_ICMD_XEQFNC_CB
C     INTEGER IC_ICMD_PRXPIL
C     INTEGER IC_ICMD_XEQPIL
C     INTEGER IC_ICMD_XEQOPB
C     INTEGER IC_ICMD_XEQOPBARI
C     INTEGER IC_ICMD_XEQOPBASG
C     INTEGER IC_ICMD_XEQOPBBRK
C     INTEGER IC_ICMD_XEQOPBCMP
C     INTEGER IC_ICMD_XEQOPBDOT
C     INTEGER IC_ICMD_XEQOPBLGL
C     INTEGER IC_ICMD_XEQOPBPAR
C     INTEGER IC_ICMD_XEQOPBVRG
C     INTEGER IC_ICMD_XEQOPU
C     INTEGER IC_ICMD_XEQOPUKW
C     INTEGER IC_ICMD_XEQOPULST
C     INTEGER IC_ICMD_XEQOPUARI
C     INTEGER IC_ICMD_XEQKWSTOP
C     INTEGER IC_ICMD_XEQKWGOTO
C     INTEGER IC_ICMD_XEQKWHELP
C     INTEGER IC_ICMD_XEQKWPRINT
C     INTEGER IC_ICMD_XEQKWINT
C     INTEGER IC_ICMD_XEQKWFLOAT
C     INTEGER IC_ICMD_XEQKWSTR
C     INTEGER IC_ICMD_XEQKWEXIST
C     INTEGER IC_ICMD_XEQKWDEL
C     INTEGER IC_ICMD_XEQKWIF
C     INTEGER IC_ICMD_XEQKWELIF
C     INTEGER IC_ICMD_XEQKWELSE
C     INTEGER IC_ICMD_XEQKWENDIF
C     INTEGER IC_ICMD_XEQKWFNC
C     INTEGER IC_ICMD_XEQKWENDFNC
C     INTEGER IC_ICMD_XEQKWEXECF
C     INTEGER IC_ICMD_XEQKWFOR
C     INTEGER IC_ICMD_XEQKWIMPORT
C     INTEGER IC_ICMD_XEQKWWHILE
C     INTEGER IC_ICMD_XEQKWENDWHILE
C     INTEGER IC_ICMD_XTRFNC
C     INTEGER IC_ICMD_XTRLBL
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data IC_ICMD initialise les COMMON propres au module
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA IC_ICMD

      IMPLICIT NONE

      INCLUDE 'icicmd.fc'

      DATA IC_ICMD_KW / ' ' /
      DATA IC_ICMD_CW / ' ' /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>IC_ICMD_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icicmd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_ICMD_NOBJMAX,
     &                   IC_ICMD_HBASE,
     &                   'Command Interpreter')

      IC_ICMD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icicmd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      EXTERNAL IC_ICMD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_ICMD_NOBJMAX,
     &                   IC_ICMD_HBASE,
     &                   IC_ICMD_DTR)

      IC_ICMD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, IC_ICMD_STS   (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_CLSDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_FNCDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_FNIDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_LBLDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_GLBDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_SMBDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_FILSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_SCTSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_PRSSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_ICMD_XEQSTK(IOB))

      IC_ICMD_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - IC_ICMD_HBASE

C---     Initialise l'objet
!!      IF (LNEW) IERR = IC_ICMD_RAZ(HOBJ)
      CALL LOG_TODO('IC_ICMD_RAZ')
      IERR = IC_ICMD_INI(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, IC_ICMD_STS   (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_CLSDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_FNCDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_FNIDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_LBLDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_GLBDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_SMBDIC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_FILSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_SCTSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_PRSSTK(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_ICMD_XEQSTK(IOB))

      IC_ICMD_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

C---     Contrôles
      IF (.NOT. OB_OBJC_ESTHBASE(IC_ICMD_HBASE)) GOTO 9900

C---     Construit l'objet
      IERR = OB_OBJC_CTR(HOBJ,
     &                   IC_ICMD_NOBJMAX,
     &                   IC_ICMD_HBASE)

C---     Initialise
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IC_ICMD_HVALIDE(HOBJ))
         IOB = HOBJ - IC_ICMD_HBASE

         IC_ICMD_STS   (IOB) = 0
         IC_ICMD_CLSDIC(IOB) = 0
         IC_ICMD_FNCDIC(IOB) = 0
         IC_ICMD_FNIDIC(IOB) = 0
         IC_ICMD_LBLDIC(IOB) = 0
         IC_ICMD_GLBDIC(IOB) = 0
         IC_ICMD_SMBDIC(IOB) = 0
         IC_ICMD_FILSTK(IOB) = 0
         IC_ICMD_SCTSTK(IOB) = 0
         IC_ICMD_PRSSTK(IOB) = 0
         IC_ICMD_XEQSTK(IOB) = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CONFIGURATION_SYSTEME_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_ICMD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_ICMD_NOBJMAX,
     &                   IC_ICMD_HBASE)

      IC_ICMD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HCLS, HFNC, HFNI, HLBL, HGLB, HSMB
      INTEGER HFST, HSCT, HZST, HXST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RESET LES DONNEES
      IERR = IC_ICMD_RST(HOBJ)

C---     CONSTRUIS LE DICTIONNAIRE DES CLASSES
      HCLS = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HCLS)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HCLS)

C---     CONSTRUIS LE DICTIONNAIRE DES FONCTIONS
      HFNC = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HFNC)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HFNC)

C---     CONSTRUIS LE DICTIONNAIRE DES FONCTIONS
      HFNI = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HFNI)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HFNI)

C---     CONSTRUIS LE DICTIONNAIRE DES LABELS
      HLBL = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HLBL)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HLBL)

C---     CONSTRUIS LE DICTIONNAIRE DES SYMBOLES GLOBAUX
      HGLB = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HGLB)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HGLB)

C---     CONSTRUIS LE DICTIONNAIRE DES SYMBOLES
      HSMB = 0
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HSMB)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HSMB)

C---     CONSTRUIS LA PILE DES FICHIERS
      HFST = 0
      IF (ERR_GOOD()) IERR = IC_PFIC_CTR(HFST)
      IF (ERR_GOOD()) IERR = IC_PFIC_INI(HFST)

C---     CONSTRUIS LA PILE DES STRUCTURES DE CONTROLE
      HSCT = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HSCT)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HSCT)

C---     CONSTRUIS LA PILE DE PARSING
      HZST = 0
      IF (ERR_GOOD()) IERR = IC_PILE_CTR(HZST)
      IF (ERR_GOOD()) IERR = IC_PILE_INI(HZST)

C---     CONSTRUIS LA PILE D'EXECUTION
      HXST = 0
      IF (ERR_GOOD()) IERR = IC_PILE_CTR(HXST)
      IF (ERR_GOOD()) IERR = IC_PILE_INI(HXST)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - IC_ICMD_HBASE
         IC_ICMD_CLSDIC(IOB) = HCLS
         IC_ICMD_FNCDIC(IOB) = HFNC
         IC_ICMD_FNIDIC(IOB) = HFNI
         IC_ICMD_LBLDIC(IOB) = HLBL
         IC_ICMD_GLBDIC(IOB) = HGLB
         IC_ICMD_SMBDIC(IOB) = HSMB

         IC_ICMD_FILSTK(IOB) = HFST
         IC_ICMD_SCTSTK(IOB) = HSCT
         IC_ICMD_PRSSTK(IOB) = HZST
         IC_ICMD_XEQSTK(IOB) = HXST
      ENDIF

C---     ENREGISTRE LES MOTS CLEFS
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDKW(HOBJ)

      IC_ICMD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HCLS, HFNC, HFNI, HLBL, HGLB, HSMB
      INTEGER HFST, HSCT, HZST, HXST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_ICMD_HBASE
      HCLS = IC_ICMD_CLSDIC(IOB)
      HFNC = IC_ICMD_FNCDIC(IOB)
      HFNI = IC_ICMD_FNIDIC(IOB)
      HLBL = IC_ICMD_LBLDIC(IOB)
      HGLB = IC_ICMD_GLBDIC(IOB)
      HSMB = IC_ICMD_SMBDIC(IOB)
      HFST = IC_ICMD_FILSTK(IOB)
      HSCT = IC_ICMD_SCTSTK(IOB)
      HZST = IC_ICMD_PRSSTK(IOB)
      HXST = IC_ICMD_XEQSTK(IOB)

C---     Détruis les map
      IF (DS_MAP_HVALIDE(HSMB)) IERR = DS_MAP_DTR(HSMB)     ! Symboles
      IF (DS_MAP_HVALIDE(HGLB)) IERR = DS_MAP_DTR(HGLB)     ! Symboles globaux
      IF (DS_MAP_HVALIDE(HLBL)) IERR = DS_MAP_DTR(HLBL)     ! Labels
      IF (DS_MAP_HVALIDE(HFNC)) IERR = DS_MAP_DTR(HFNC)     ! Fonctions des modules
      IF (DS_MAP_HVALIDE(HFNI)) IERR = DS_MAP_DTR(HFNI)     ! Fonctions de l'interface
      IF (DS_MAP_HVALIDE(HCLS)) IERR = DS_MAP_DTR(HCLS)     ! Classes

C---     Détruis les piles
      IF (IC_PILE_HVALIDE(HXST)) IERR = IC_PILE_DTR(HXST)   ! Pile d'exécution
      IF (IC_PILE_HVALIDE(HZST)) IERR = IC_PILE_DTR(HZST)   ! Pile de parsing
      IF (DS_LIST_HVALIDE(HSCT)) IERR = DS_LIST_DTR(HSCT)   ! Pile des structures de contrôle
      IF (IC_PFIC_HVALIDE(HFST)) IERR = IC_PFIC_DTR(HFST)   ! Pile des fichiers

C---     RESET LES PARAMETRES
      IC_ICMD_STS   (IOB) = 0
      IC_ICMD_CLSDIC(IOB) = 0
      IC_ICMD_FNCDIC(IOB) = 0
      IC_ICMD_FNIDIC(IOB) = 0
      IC_ICMD_LBLDIC(IOB) = 0
      IC_ICMD_GLBDIC(IOB) = 0
      IC_ICMD_SMBDIC(IOB) = 0

      IC_ICMD_FILSTK(IOB) = 0
      IC_ICMD_SCTSTK(IOB) = 0
      IC_ICMD_PRSSTK(IOB) = 0
      IC_ICMD_XEQSTK(IOB) = 0

      IC_ICMD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_ICMD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icicmd.fi'
      INCLUDE 'icicmd.fc'
C------------------------------------------------------------------------

      IC_ICMD_REQHBASE = IC_ICMD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_ICMD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icicmd.fc'
C------------------------------------------------------------------------

      IC_ICMD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_ICMD_NOBJMAX,
     &                                  IC_ICMD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_ICMD_EVAL
C
C Description:
C     La fonction IC_ICMD_EVAL évalue une expression.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     RSLT     Le résultat de l'évaluation de l'expression
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_EVAL (HOBJ, EXPR, RSLT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_EVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) EXPR
      CHARACTER*(*) RSLT

      INCLUDE 'icicmd.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER         IERR, IRET
      INTEGER         IOB
      INTEGER         ISTS
      INTEGER         MW, LTMP
      CHARACTER*(256) NOMTMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(EXPR) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Crée un fic temp pour l'expression
      NOMTMP = ' '
      IRET = C_OS_FICTMP(NOMTMP)
      IF (IRET .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_CREE_FICTMP')

C---     Ouvre le fichier
      MW = 0
      IF (ERR_GOOD()) THEN
         MW = IO_UTIL_FREEUNIT()
         OPEN(UNIT  = MW,
     &        FILE  = NOMTMP,
     &        FORM  = 'FORMATTED',
     &        STATUS= 'UNKNOWN',
     &        ERR   = 108)
         GOTO 109
108      CONTINUE
         CALL ERR_ASG(ERR_ERR, 'ERR_OUVERTURE_FICHIER_TEMPORAIRE')
109      CONTINUE
      ENDIF

C---     Écris l'expression
      IF (ERR_GOOD()) THEN
         WRITE(MW, '(A)', ERR=208) EXPR(1:SP_STRN_LEN(EXPR))
         GOTO 209
208      CONTINUE
         CALL ERR_ASG(ERR_ERR, 'ERR_ECRITURE_FICHIER_TEMPORAIRE')
209      CONTINUE
      ENDIF

C---     Ferme le fichier
      IF (MW .NE. 0) CLOSE(MW)

C---     Execute le fichier temp
      LTMP = SP_STRN_LEN(NOMTMP)
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - IC_ICMD_HBASE
         ISTS = IC_ICMD_STS(IOB)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SST)
         IERR = IC_ICMD_XEQ(HOBJ, .TRUE., NOMTMP(1:LTMP), RSLT)
         IC_ICMD_STS(IOB) = ISTS
      ENDIF

C---     Efface le fichier temp
      IF (LTMP .GT. 0) IRET = C_OS_DELFIC(NOMTMP(1:LTMP))

      IC_ICMD_EVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_ICMD_XEQ
C
C Description:
C     La fonction IC_ICMD_XEQ exécute l'interface de commande. Celui-ci
C     va interpreter les commandes tant qu'il n'est pas en erreur ou qu'il
C     ne reçoit une commande de fin. Si le nom du fichier de commande est
C     'STDINP' les commandes sont lues de la console, sinon elle sont lues
C     du fichier.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     DOEXE    .TRUE. pour l'exécution, .FALSE. pour parsing
C     FNAME    File name
C
C Sortie:
C     RTRN     Result, if any from the execution
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQ (HOBJ, DOEXE, FNAME, RTRN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      LOGICAL  DOEXE
      CHARACTER*(*) FNAME
      CHARACTER*(*) RTRN

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER HFST, HXST
      INTEGER MW
      INTEGER LFNAME
      LOGICAL AFICHIER, INTERACTIF, ECRISPROMPT, ESTRESTART
      CHARACTER*(1024) RSLT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNAME) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)

C---     INITIALISE
      MW = 6
      RSLT = ' '
      RTRN = ' '

C---     DETERMINE SI ON DOIT OUVRIR LE FICHIER
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      LFNAME = SP_STRN_LEN(FNAME)
      AFICHIER = (LFNAME .NE. 6 .OR. FNAME(1:LFNAME) .NE. 'STDINP')
      ECRISPROMPT  = (I_RANK .EQ. I_MASTER .AND. .NOT. AFICHIER)
      INTERACTIF   = (.NOT. AFICHIER)

C---     DETERMINE SI ON est en restart
!      début:  xstk vide, stat=0
!      eval:   xstk non vide, stat=xeq and evl
!      restrt: xstk non vide, stat=xeq and not evl
      HXST = IC_ICMD_XEQSTK(IOB)
      ESTRESTART = (IC_PILE_HVALIDE(HXST) .AND.
     &              .NOT. IC_PILE_ESTVIDE(HXST))
      ESTRESTART = (ESTRESTART .AND.
     &              BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ) .AND.
     &              BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_MST) .AND.
     &              .NOT. BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SST))

C------------------------
C---     PARSE
C------------------------
      IF (AFICHIER .AND. .NOT. ESTRESTART) THEN

C---        Ouvre le fichier
         IERR = IC_PFIC_PUSH(HFST, FNAME, -1, 0)

C---        Parse le fichier
         IF (ERR_GOOD()) THEN
            IC_ICMD_STS(IOB) = 0
            IERR = IC_ICMD_PRSFIC(HOBJ)
         ENDIF
         IF (ERR_BAD()) CALL ERR_AJT('MSG_ERROR_IN_PARSING')
      ENDIF

C------------------------
C---     EXECUTE
C------------------------
      IF (ERR_GOOD() .AND. DOEXE .AND. .NOT. ESTRESTART) THEN

C---        Re-ouvre le fichier
         IERR = IC_PFIC_PUSH(HFST, FNAME, -1, 0)

C---        Execute le fichier
         IF (ERR_GOOD()) THEN
            IC_ICMD_STS(IOB) = 0
            IERR = IC_ICMD_XEQFIC(HOBJ,RSLT,MW,ECRISPROMPT,INTERACTIF)
         ENDIF
      ENDIF

C------------------------
C---     RESTART
C------------------------
      IF (ERR_GOOD() .AND. DOEXE .AND. ESTRESTART) THEN

C---        Execute le fichier
         IERR = IC_ICMD_XEQFIC(HOBJ,RSLT,MW,ECRISPROMPT,INTERACTIF)
      ENDIF

C------------------------
C---     CLEAN_UP
C------------------------
      IF (ERR_BAD()) THEN

C---        Execute le fichier
         IF (ERR_ESTMSG('ERR_CMD_STOP')) THEN
            CALL ERR_RESET()
         ELSEIF (ERR_BAD()) THEN
            IERR = IC_ICMD_UNWNDSTK(HOBJ)
         ENDIF
         IC_ICMD_STS(IOB) = 0

C---        Ferme tous les fichiers
         IF (ERR_BAD()) THEN
            CALL ERR_PUSH()
            IERR = IC_PFIC_END(HFST)
            CALL ERR_POP()
         ENDIF
      ENDIF

      RTRN = RSLT(1:MIN(SP_STRN_LEN(RSLT), LEN(RTRN)))
      IC_ICMD_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une fonction ou un constructeur
C
C Description:
C     La fonction IC_ICMD_ADDFNC ajoute une fonction ou un constructeur
C     de classe, identifié par son nom FNC et par la fonction à appeler
C     HFNC pour le traitement.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NOM      Nom de la fonction
C     HFNC     Handle de la fonction à appeler
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ADDFNC (HOBJ, NOM, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_ADDFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOM
      INTEGER       HFNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDIC
      CHARACTER*(16) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(NOM) .GT. 0)
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFNC))
C-----------------------------------------------------------------------

C---     Contrôles
      IF (.NOT. SP_STRN_VAR(NOM(1:SP_STRN_LEN(NOM)))) GOTO 9900

C---     Récupère l'indice
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_FNCDIC(IOB)

C---     Recherche si la fonction existe déjà
      IF (DS_MAP_ESTCLF(HDIC, NOM(1:SP_STRN_LEN(NOM)))) GOTO 9901

C---     Ajoute la fonction
      WRITE(VAL, '(I12)') HFNC
      IERR = DS_MAP_ASGVAL(HDIC,
     &                     NOM(1:SP_STRN_LEN(NOM)),
     &                     VAL(1:SP_STRN_LEN(VAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FONCTION_INVALIDE',': ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_REDEFINITION_FONCTION',': ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ADDFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une classe
C
C Description:
C     La fonction IC_ICMD_ADDCLS ajoute une classe aux classes connues de l'objet.
C     La classe identifiée par:
C        - son handle de base HBASE,
C        - le handle HCTR sur le constructeur
C        - le handle HMTH sur l'opérateur pour traiter les méthodes
C        - le handle HDOT sur l'opérateur pour les fonctions statiques
C     HDOT peut être nul.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NOM      Nom de la classe
C     HBASE    Handle de base de la classe
C     HCTR     Handle sur le constructeur
C     HMTH     Handle sur la fonction à appeler pour
C                 les méthodes de la classe.
C     HDOT     Handle sur la fonction à appeler pour
C                 les méthodes statiques de la classe.
C
C Sortie:
C
C Notes:
C   - Les classes virtuelles ont des noms qui ne sont pas valides comme
C     nom de fonction. Dans ce cas, on n'enregistre que le handle.
C   - Si HDOT est nul, on enregistre HMTH à sa place. Ceci couvre les cas
C     d'utilisation pour l'aide, car les classes ont toutes un constructeur
C     d'enregistré qui traite le cas particulier de l'aide. Par contre elles
C     ne sont pas toutes capables de traiter "help" comme un méthode statique.
C************************************************************************
      FUNCTION IC_ICMD_ADDCLS (HOBJ, NOM, HBASE, HCTR, HMTH, HDOT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_ADDCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOM
      INTEGER       HBASE
      INTEGER       HCTR
      INTEGER       HMTH
      INTEGER       HDOT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HDIC, HTMP
      CHARACTER*(64) KEY, VAL
      CHARACTER*(64) CLS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(HBASE .GT. 0)
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HCTR))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HMTH))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HDOT) .OR. HDOT .EQ. 0)
C-----------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_CLSDIC(IOB)

C---     Recherche si le handle existe déjà
      WRITE(KEY, '(2A,I12)') 'm', ',', HBASE
      IERR = IC_ICMD_EXT2INT(KEY)
      IF (DS_MAP_ESTCLF(HDIC, KEY(1:SP_STRN_LEN(KEY)))) GOTO 9900

C---     Ajoute la classe
      HTMP = HDOT
      IF (HTMP .EQ. 0) HTMP = HMTH
      IF (ERR_GOOD()) WRITE(VAL, '(2I12)') HMTH, HTMP
      IF (ERR_GOOD()) IERR = DS_MAP_ASGVAL(HDIC,
     &                                     KEY(1:SP_STRN_LEN(KEY)),
     &                                     VAL(1:SP_STRN_LEN(VAL)))

C---     Ajoute le nom comme variable globale
      IF (SP_STRN_VAR(NOM)) THEN
         VAL = KEY
         KEY = NOM
         IF (ERR_GOOD()) IERR = IC_PILE_VALENC(KEY,IC_PILE_TYP_VARIABLE)
         IF (ERR_GOOD()) IERR = IC_ICMD_ASGSMB(HOBJ, KEY, VAL)
      ENDIF

C---     Ajoute le nom comme une fonction
      IF (SP_STRN_VAR(NOM)) THEN
         IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC (HOBJ, NOM, HCTR)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  IF (OB_OBJC_REQNOMTYPE(CLS, HBASE) .EQ. ERR_OK) THEN
         WRITE(ERR_BUF, '(3A,I12)') 'ERR_REDEFINITION_CLASSE',': ',
     &                              CLS(1:SP_STRN_LEN(CLS))
      ELSE
         WRITE(ERR_BUF, '(2A,I12)') 'ERR_REDEFINITION_CLASSE',': ',
     &                              HBASE
      ENDIF
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ADDCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un module
C
C Description:
C     La fonction IC_ICMD_ADDMDL ajoute le module identifié par
C     son nom, son handle de base HBASE et le handle sur l'opérateur
C     binaire '.' (dot) d'accès aux composantes de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NOM      Nom du module
C     HBASE    Handle de base du module
C     HOPB     Handle sur la fonction à appeler pour l'accès aux
C              composantes du module.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ADDMDL (HOBJ, NOM, HBASE, HDOT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_ADDMDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOM
      INTEGER       HBASE
      INTEGER       HDOT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HDIC
      INTEGER, PARAMETER :: HMTH = 0
      CHARACTER*(64) KEY, VAL
      CHARACTER*(64) MDL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(HBASE .GT. 0)
C-----------------------------------------------------------------------

C---     Contrôles
      IF (.NOT. SP_STRN_VAR(NOM(1:SP_STRN_LEN(NOM)))) GOTO 9900

C---     Récupère l'indice
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_CLSDIC(IOB)

C---     Recherche si le module existe déjà
      WRITE(KEY, '(2A,I12)') 'm', ',', HBASE
      IERR = IC_ICMD_EXT2INT(KEY)
      IF (DS_MAP_ESTCLF(HDIC, KEY(1:SP_STRN_LEN(KEY)))) GOTO 9900

C---     Ajoute comme une classe
      WRITE(VAL, '(2I12)') HMTH, HDOT
      IERR = DS_MAP_ASGVAL(HDIC,
     &                     KEY(1:SP_STRN_LEN(KEY)),
     &                     VAL(1:SP_STRN_LEN(VAL)))

C---     Ajoute le module comme variable globales
      VAL = KEY
      KEY = NOM
      IF (ERR_GOOD()) IERR = IC_PILE_VALENC(KEY,IC_PILE_TYP_VARIABLE)
      IF (ERR_GOOD()) IERR = IC_ICMD_ASGSMB(HOBJ, KEY, VAL)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_MODULE_INVALIDE',': ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  IF (OB_OBJC_REQNOMTYPE(MDL, HBASE) .EQ. ERR_OK) THEN
         WRITE(ERR_BUF, '(3A,I12)') 'ERR_REDEFINITION_MODULE',': ',
     &                              MDL(1:SP_STRN_LEN(MDL))
      ELSE
         WRITE(ERR_BUF, '(2A,I12)') 'ERR_REDEFINITION_MODULE',': ',
     &                              HBASE
      ENDIF
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ADDMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute les mots clefs.
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ADDKW(...)</code> ajoute
C     les mots clefs comme fonctions à l'objet passé en argument.
C
C Entrée:
C     HOBJ     L'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ADDKW(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpars.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  HFNC
      EXTERNAL IC_ICMD_XEQKWDEL
      EXTERNAL IC_ICMD_XEQKWEXIST
      EXTERNAL IC_ICMD_XEQKWGOTO
      EXTERNAL IC_ICMD_XEQKWHELP
      EXTERNAL IC_ICMD_XEQKWPRINT
      EXTERNAL IC_ICMD_XEQKWINT
      EXTERNAL IC_ICMD_XEQKWFLOAT
      EXTERNAL IC_ICMD_XEQKWSTR
      EXTERNAL IC_ICMD_XEQKWSTOP
      EXTERNAL IC_ICMD_XEQKWIMPORT
      EXTERNAL IC_ICMD_XEQKWIF
      EXTERNAL IC_ICMD_XEQKWELIF
      EXTERNAL IC_ICMD_XEQKWELSE
      EXTERNAL IC_ICMD_XEQKWENDIF
      EXTERNAL IC_ICMD_XEQKWFNC
      EXTERNAL IC_ICMD_XEQKWENDFNC
      EXTERNAL IC_ICMD_XEQKWEXECF
      EXTERNAL IC_ICMD_XEQKWEVAL
      EXTERNAL IC_ICMD_XEQKWFOR
      EXTERNAL IC_ICMD_XEQKWWHILE
      EXTERNAL IC_ICMD_XEQKWENDWHILE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Initialise la variable statique ici
C        à cause de la concaténation des chaînes
      IF (SP_STRN_LEN(IC_ICMD_KW) .EQ. 0) THEN
         IC_ICMD_KW = '@del@exist@help@print' //   !  21
     &                '@int@float@str' //          ! +14
     &                '@stop@quit@exit' //         ! +15
     &                '@goto' //                   ! + 5
     &                '@'                          ! + 1 = 56
      ENDIF
      IF (SP_STRN_LEN(IC_ICMD_CW) .EQ. 0) THEN
         IC_ICMD_CW = '@execfile@import@eval' //   !  21
     &                '@if@elif@else@endif' //     ! +19
     &                '@function@endfunction' //   ! +21
!!     &                '@for@endfor' //             ! +11
     &                '@while@endwhile' //         ! +15
     &                '@'                          ! + 1 = 77
      ENDIF

C---     Les mots clefs
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWDEL)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'del', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWEXIST)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'exist', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWGOTO)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'goto', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWHELP)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'help', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWPRINT)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'print', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWINT)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'int', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWFLOAT)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'float', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWSTR)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'str', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWSTOP)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'stop', HFNC)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'quit', HFNC)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'exit', HFNC)

C---     Les structures de contrôle
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWEXECF)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'execfile', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWIMPORT)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'import', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWEVAL)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'eval', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWIF)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'if', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWELIF)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'elif', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWELSE)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'else', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWENDIF)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'endif', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWFNC)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'function', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWENDFNC)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'endfunction', HFNC)

!!      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
!!      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWFOR)
!!      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'for', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWWHILE)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'while', HFNC)

      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFNC, IC_ICMD_XEQKWENDWHILE)
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HOBJ, 'endwhile', HFNC)

C---     Assigne le tout au parser
      IF (ERR_GOOD()) IERR = IC_PARS_ASGKW(IC_ICMD_KW // IC_ICMD_CW)

      IC_ICMD_ADDKW = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour un mot clef.
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ESTKW(...)</code> retourne
C     .TRUE. si le mot passé en argument est un mot clef.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C     Écriture ampoulée à cause de g77. Ce que l'on test c'est:
C     RES = ((INDEX(IC_ICMD_KW, '@' // KW(1:LKW) // '@') .GT. 0) .OR.
C    &       (INDEX(IC_ICMD_CW, '@' // KW(1:LKW) // '@') .GT. 0))
C************************************************************************
      FUNCTION IC_ICMD_ESTKW(KW)

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IKW, LKW
      LOGICAL RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(KW) .GT. 0)
C-----------------------------------------------------------------------

      RES = .FALSE.

      LKW = SP_STRN_LEN(KW)
      IF (.NOT. RES) THEN
         IKW = INDEX(IC_ICMD_KW, '@' // KW(1:LKW) // '@')
         IF (IKW .GT. 0) RES = .TRUE.
      ENDIF
      IF (.NOT. RES) THEN
         IKW = INDEX(IC_ICMD_CW, '@' // KW(1:LKW) // '@')
         IF (IKW .GT. 0) RES = .TRUE.
      ENDIF

      IC_ICMD_ESTKW = RES
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour un mot de contrôle
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ESTCW(...)</code> retourne
C     .TRUE. si le mot passé en argument est un mot de contrôle.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C     Écriture ampoulée à cause de g77. Ce que l'on test c'est:
C     RES = (INDEX(IC_ICMD_CW, '@' // KW(1:LKW) // '@') .GT. 0)
C************************************************************************
      FUNCTION IC_ICMD_ESTCW(KW)

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IKW, LKW
      LOGICAL RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(KW) .GT. 0)
C-----------------------------------------------------------------------

      RES = .FALSE.

      LKW = SP_STRN_LEN(KW)
      IF (.NOT. RES) THEN
         IKW = INDEX(IC_ICMD_CW, KW(1:LKW))
         IF (IKW .GT. 0) THEN
            IF (IC_ICMD_CW(IKW-  1:IKW-  1) .EQ. '@' .AND.
     &          IC_ICMD_CW(IKW+LKW:IKW+LKW) .EQ. '@') RES = .TRUE.
         ENDIF
      ENDIF

      IC_ICMD_ESTCW = RES
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour le mot de contrôle 'if'
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ESTIF(...)</code> retourne
C     .TRUE. si le mot passé en argument est le mot de contrôle 'if'.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ESTIF(KW)

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(KW) .GT. 0)
C-----------------------------------------------------------------------

      IC_ICMD_ESTIF = (KW(1:SP_STRN_LEN(KW)) .EQ. 'if')
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour le mot de contrôle 'elif'
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ESTELIF(...)</code> retourne
C     .TRUE. si le mot passé en argument est le mot de contrôle 'elif'.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ESTELIF(KW)

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(KW) .GT. 0)
C-----------------------------------------------------------------------

      IC_ICMD_ESTELIF = (KW(1:SP_STRN_LEN(KW)) .EQ. 'elif')
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour le mot de contrôle 'while'
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ESTWHILE(...)</code> retourne
C     .TRUE. si le mot passé en argument est le mot de contrôle 'while'.
C
C Entrée:
C     KW      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ESTWHILE(KW)

      IMPLICIT NONE

      CHARACTER*(*) KW

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(KW) .GT. 0)
C-----------------------------------------------------------------------

      IC_ICMD_ESTWHILE = (KW(1:SP_STRN_LEN(KW)) .EQ. 'while')
      RETURN
      END

C************************************************************************
C Sommaire: Formate une valeur pour la rendre lisible
C
C Description:
C     La fonction privée statique <code>IC_ICMD_ERRVAL(...)</code> monte
C     le message d'erreur associé à la valeur VAL.
C
C Entrée:
C     VAL   La valeur
C
C Sortie:
C     MSG   Le message
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ERRVAL(MSG, VAL)

      IMPLICIT NONE

      CHARACTER*(*) MSG
      CHARACTER*(*) VAL

      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IT, I1, I2, IDUM
      INTEGER HNDL
      CHARACTER*(256) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GT. 0)
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IF (IC_PILE_ESTVALOK(VAL)) THEN
         IT = IC_PILE_VALTYP(VAL)
         I1 = IC_PILE_VALDEB(VAL)
         I2 = IC_PILE_VALFIN(VAL)

         IF (IC_PILE_ESTVALHDL(IT)) THEN
            BUF = VAL(I1:I2)
            IDUM = SP_STRN_LFT(BUF, '.')
            IDUM = SP_STRN_LFT(BUF, '[')
            READ(BUF(1:SP_STRN_LEN(BUF)), *, ERR=9900) HNDL
            BUF = ' '
            CALL ERR_PUSH()
            IERR = OB_OBJC_REQNOMCMPL(BUF, HNDL)
            CALL ERR_POP()
            IF (IERR .EQ. ERR_OK) THEN
               WRITE(MSG, '(6A)', ERR=9900)
     &            'MSG_TYPE: ', IC_PILE_TYP2TXT(IT),
     &            'MSG_VALEUR: ', VAL(I1:I2),
     &            ' --> ', BUF(1:SP_STRN_LEN(BUF))
            ELSE
               WRITE(MSG, '(4A)', ERR=9900)
     &            'MSG_TYPE: ', IC_PILE_TYP2TXT(IT),
     &            'MSG_VALEUR: ', VAL(I1:I2)
            ENDIF
         ELSE
            WRITE(MSG, '(4A)', ERR=9900)
     &         'MSG_TYPE: ', IC_PILE_TYP2TXT(IT),
     &         'MSG_VALEUR: ', VAL(I1:I2)
         ENDIF
      ELSE
         WRITE(MSG, '(2A)', ERR=9900)
     &      'MSG_VALEUR: ', VAL(1:SP_STRN_LEN(VAL))
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CONTINUE
      MSG = 'Decoding error (buffer overflow?)'
      GOTO 9999

9999  CONTINUE
      CALL ERR_POP()
      IC_ICMD_ERRVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour le mode PARSING.
C
C Description:
C     La fonction privée <code>IC_ICMD_DOPARS(...)</code> retourne
C     .TRUE. si l'interpréteur est en mode PARSING
C
C Entrée:
C     STR      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_DOPARS(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB  = HOBJ - IC_ICMD_HBASE
      IC_ICMD_DOPARS = BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_PRS)
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour une fonction enregistrée.
C
C Description:
C     La fonction privée <code>IC_ICMD_ESTFNC(...)</code> retourne
C     .TRUE. si le mot passé en argument est une fonction enregistrée.
C
C Entrée:
C     STR      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ESTFNC(HOBJ, FN)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FN

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER HDIC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FN) .GT. 0)
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_FNCDIC(IOB)

      IC_ICMD_ESTFNC = DS_MAP_ESTCLF(HDIC, FN(1:SP_STRN_LEN(FN)))
      RETURN
      END

C************************************************************************
C Sommaire: Teste pour un mot fonction de l'IC.
C
C Description:
C     La fonction privée <code>IC_ICMD_ESTFNI(...)</code> retourne
C     .TRUE. si le mot passé en argument est une fonction de l'IC,
C     déclarée dans un fichier de commande.
C
C Entrée:
C     STR      Nom à tester
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ESTFNI(HOBJ, FN)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FN

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER HDIC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FN) .GT. 0)
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_FNIDIC(IOB)

      IC_ICMD_ESTFNI = DS_MAP_ESTCLF(HDIC, FN(1:SP_STRN_LEN(FN)))
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un symbole
C
C Description:
C     La fonction privée <code>IC_ICMD_ASGSMB(...)</code> assigne au symbole
C     <code>SMB</code> la valeur <code>VAL</code>.
C     Cette fonction fait le dispatch nécessaire pour les opérateurs '[]' et '.'.
C
C Entrée:
C     HOBJ     L'objet courant
C     SMB      Le symbole à assigner (format interne)
C     VAL      La valeur (format interne)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ASGSMB (HOBJ, SMB, VAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER I1, I2
      INTEGER ITPSMB, ITPVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Début-fin du symbole
      I1 = IC_PILE_VALDEB(SMB)
      I2 = IC_PILE_VALFIN(SMB)

C---     Contrôle
      IF (SP_STRN_LEN(SMB) .LE. 0) GOTO 9900
D     CALL ERR_ASR(.NOT. IC_ICMD_ESTKW (SMB(I1:I2)))
D     CALL ERR_ASR(.NOT. IC_ICMD_ESTCW (SMB(I1:I2)))
      IF (IC_ICMD_ESTFNC(HOBJ, SMB(I1:I2))) GOTO 9901
!     IF (IC_ICMD_ESTFNI(HOBJ, SMB(I1:I2))) GOTO 9901    ! Assignables
      IF (.NOT. IC_PILE_ESTVAL(IC_PILE_VALTYP(VAL))) GOTO 9902

C---     Dispatch suivant le type
      ITPSMB = IC_PILE_VALTYP(SMB)
C---     Handle
      IF (IC_PILE_ESTVALHDL(ITPSMB)) THEN
         IF (INDEX(SMB(I1:I2), '[') .GT. 0) THEN      ! Opérateur []
            IERR = IC_ICMD_ASGSMBBRK(HOBJ, SMB(I1:I2), VAL)
         ELSEIF (INDEX(SMB(I1:I2), '.') .GT. 0) THEN  ! Opérateur .
            IERR = IC_ICMD_ASGSMBDOT(HOBJ, SMB(I1:I2), VAL)
         ELSE
            GOTO 9901
         ENDIF
C---     Variable
      ELSEIF (IC_PILE_ESTVAR(ITPSMB)) THEN
         ITPVAL = IC_PILE_VALTYP(VAL)
         IF (IC_PILE_ESTVALMDL(ITPVAL)) THEN
            IERR = IC_ICMD_ASGSMBGLB(HOBJ, SMB(I1:I2), VAL)
         ELSE
            IERR = IC_ICMD_ASGSMBSMB(HOBJ, SMB(I1:I2), VAL)
         ENDIF
C---     Parsing
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
         ! pass
      ELSE
         GOTO 9901
      END IF


      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_SYMBOLE_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VARIABLE_ATTENDUE', ': ',
     &                       SMB(1:SP_STRN_LEN(SMB))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'ERR_VALEUR_ATTENDUE',': ',
     &                       VAL(1:SP_STRN_LEN(VAL))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ASGSMB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne une valeur et un type à un symbole
C
C Description:
C     La fonction privée <code>IC_ICMD_ASGSMBGLB(...)</code> assigne au symbole
C     de nom <code>SMB</code> la valeur <code>VAL</code>.
C
C Entrée:
C     HOBJ     L'objet courant
C     SMB      Le nom du symbole à assigner
C     VAL      La valeur (format interne)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ASGSMBGLB (HOBJ, SMB, VAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ITP
      INTEGER HGLB
      CHARACTER*(256) TMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
D     CALL ERR_PRE(IC_PILE_ESTVAL(IC_PILE_VALTYP(VAL)))
C-----------------------------------------------------------------------

C---     Contrôle
      IF (.NOT. SP_STRN_VAR(SMB)) GOTO 9900

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HGLB = IC_ICMD_GLBDIC(IOB)

C---     Contrôle les assignations à constantes
      IERR = DS_MAP_REQVAL(HGLB,
     &                     SMB(1:SP_STRN_LEN(SMB)),
     &                     TMP)
      IF (ERR_GOOD()) THEN
         ITP = IC_PILE_VALTYP(TMP)
         IF (IC_PILE_ESTVALCST(ITP)) GOTO 9901
      ELSE
         CALL ERR_RESET()
      ENDIF

C---     Assigne le symbole
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IERR = DS_MAP_ASGVAL(HGLB,
     &                        '#-#' // SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL(1:SP_STRN_LEN(VAL)))
      ELSE
         IERR = DS_MAP_ASGVAL(HGLB,
     &                        SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL(1:SP_STRN_LEN(VAL)))
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_SYMBOLE_INVALIDE',': ',
     &                       SMB(1:SP_STRN_LEN(SMB))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_ASSIGNATION_A_CONSTANTE',': ',
     &                       SMB(1:SP_STRN_LEN(SMB))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ASGSMBGLB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne une valeur et un type à un symbole
C
C Description:
C     La fonction privée <code>IC_ICMD_ASGSMBSMB(...)</code> assigne au symbole
C     de nom <code>SMB</code> la valeur <code>VAL</code>.
C
C Entrée:
C     HOBJ     L'objet courant
C     SMB      Le nom du symbole à assigner
C     VAL      La valeur (format interne)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ASGSMBSMB (HOBJ, SMB, VAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ITP
      INTEGER HSMB
      CHARACTER*(256) TMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
D     CALL ERR_PRE(IC_PILE_ESTVAL(IC_PILE_VALTYP(VAL)))
C-----------------------------------------------------------------------

C---     Contrôle
      IF (.NOT. SP_STRN_VAR(SMB)) GOTO 9900

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HSMB = IC_ICMD_SMBDIC(IOB)

C---     Contrôle les assignations à constantes
      IERR = DS_MAP_REQVAL(HSMB,
     &                     SMB(1:SP_STRN_LEN(SMB)),
     &                     TMP)
      IF (ERR_GOOD()) THEN
         ITP = IC_PILE_VALTYP(TMP)
         IF (IC_PILE_ESTVALCST(ITP)) GOTO 9901
      ELSE
         CALL ERR_RESET()
      ENDIF

C---     Assigne le symbole
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IERR = DS_MAP_ASGVAL(HSMB,
     &                        '#-#' // SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL(1:SP_STRN_LEN(VAL)))
      ELSE
         IERR = DS_MAP_ASGVAL(HSMB,
     &                        SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL(1:SP_STRN_LEN(VAL)))
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_SYMBOLE_INVALIDE',': ',
     &                       SMB(1:SP_STRN_LEN(SMB))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_ASSIGNATION_A_CONSTANTE',': ',
     &                       SMB(1:SP_STRN_LEN(SMB))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_ASGSMBSMB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un symbole avec indice.
C
C Description:
C     La fonction privée <code>IC_ICMD_ASGSMBBRK(...)</code> assigne au symbole
C     <code>SMB</code> la valeur <code>VAL</code> de type <code>TYP</code>.
C
C Entrée:
C     HOBJ     L'objet courant
C     SMB      Le nom du symbole à assigner
C     VAL      La valeur (format interne)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ASGSMBBRK (HOBJ, SMB, VAL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  IDUM
      INTEGER  HCLS, HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB
      CHARACTER*(64) TMP

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
D     CALL ERR_PRE(IC_PILE_ESTVAL(IC_PILE_VALTYP(VAL)))
C-----------------------------------------------------------------------

C---     Saute les initialisations
      IF (IC_PILE_VALTYP(VAL) .EQ. IC_PILE_TYP_0VALEUR) GOTO 9999

C---     Handle de la classe
      TMP = SMB(1:SP_STRN_LEN(SMB))
      IDUM = SP_STRN_LFT(TMP, '[')
      IERR = IC_ICMD_REQHNDLCLS(HOBJ, TMP, HCLS)

C---     Mode PARSING, termine
      IF (IC_ICMD_DOPARS(HOBJ)) GOTO 9999

C---     Handle de la fonction à appeler
      IF (ERR_GOOD()) IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)

C---     Appel la fonction
      IF (ERR_GOOD() .AND. .NOT. IC_ICMD_DOPARS(HOBJ)) THEN
         IND_CMN = IND_CMN + 1
D        CALL ERR_ASR(IND_CMN .LE. SIZ_CMN)
         HDL_CMN(IND_CMN) = HCLS
         FNC_CMN(IND_CMN) = '##opb_[]_set##'          ! Méthode spéciale
         PRM_CMN(IND_CMN) = VAL(1:SP_STRN_LEN(VAL))   ! Indice en param
         IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))     ! et valeur
         TMP  = SMB(1:SP_STRN_LEN(SMB))
         IDUM = SP_STRN_RHT(TMP, '[')
         IDUM = SP_STRN_LFT(TMP, ']')
         IDUM = SP_STRN_LEN(PRM_CMN(IND_CMN))
         PRM_CMN(IND_CMN) = TMP(1:SP_STRN_LEN(TMP)) //
     &                      ',' //
     &                      PRM_CMN(IND_CMN)(1:IDUM)
         IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
         IND_CMN = IND_CMN - 1
D        CALL ERR_ASR(IND_CMN .GE. 0)
      ENDIF

9999  CONTINUE
      IC_ICMD_ASGSMBBRK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un symbole avec accesseur '.'
C
C Description:
C     La fonction privée <code>IC_ICMD_ASGSMBDOT(...)</code> assigne au symbole
C     <code>SMB</code> qui comprend un accesseur, la valeur <code>VAL</code> de
C     type <code>TYP</code>.
C
C Entrée:
C     HOBJ     L'objet courant
C     SMB      Le nom du symbole à assigner
C     VAL      La valeur (format interne)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_ASGSMBDOT (HOBJ, SMB, VAL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  IDUM, ITP, I1, I2
      INTEGER  HCLS, HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB
      CHARACTER*(64) SMB_TMP

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
D     CALL ERR_PRE(IC_PILE_ESTVAL(IC_PILE_VALTYP(VAL)))
C-----------------------------------------------------------------------

C---     Saute les initialisations
      IF (IC_PILE_VALTYP(VAL) .EQ. IC_PILE_TYP_0VALEUR) GOTO 9999

C---     Handle de la classe
      SMB_TMP = SMB(1:SP_STRN_LEN(SMB))
      IDUM = SP_STRN_LFT(SMB_TMP, '.')
      IERR = IC_ICMD_REQHNDLCLS(HOBJ, SMB_TMP, HCLS)

C---     Mode PARSING, termine
      IF (IC_ICMD_DOPARS(HOBJ)) GOTO 9999

C---     BOUCLE POUR RÉSOUDRE LES PROXY ET INDIRECTIONS
C        ==============================================
100   CONTINUE

C---        Handle de la fonction à appeler
         IF (ERR_GOOD()) IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)

C---        Appel la fonction
         IND_CMN = IND_CMN + 1
D        CALL ERR_ASR(IND_CMN .LE. SIZ_CMN)
         IF (ERR_GOOD()) THEN
            HDL_CMN(IND_CMN) = HCLS
            FNC_CMN(IND_CMN) = '##property_set##'        ! Méthode spéciale
            PRM_CMN(IND_CMN) = VAL(1:SP_STRN_LEN(VAL))   ! Combine attribut
            IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))     ! et valeur
            IDUM = SP_STRN_LEN(PRM_CMN(IND_CMN))
            PRM_CMN(IND_CMN) = SMB(1:SP_STRN_LEN(SMB))
     &                         // ',' //
     &                         PRM_CMN(IND_CMN)(1:IDUM)
            IDUM = SP_STRN_RHT(PRM_CMN(IND_CMN), '.')

            IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
         ENDIF

C---        Decode le retour
         HCLS = 0
         IF (ERR_GOOD()) THEN
            IERR = IC_ICMD_EXT2INT(PRM_CMN(IND_CMN))
            ITP  = IC_PILE_VALTYP (PRM_CMN(IND_CMN))
            IF (ITP .EQ. IC_PILE_TYP_XVALEUR) THEN
               I1 = IC_PILE_VALDEB(PRM_CMN(IND_CMN))
               I2 = IC_PILE_VALFIN(PRM_CMN(IND_CMN))
               READ (PRM_CMN(IND_CMN)(I1:I2), *) HCLS
            ENDIF
         ENDIF
         IND_CMN = IND_CMN - 1
D        CALL ERR_ASR(IND_CMN .GE. 0)

C---     Boucle tant que le type de retour est IC_PILE_TYP_XVALEUR
      IF (ERR_GOOD() .AND. HCLS .NE. 0) GOTO 100

9999  CONTINUE
      IC_ICMD_ASGSMBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_ICMD_EXT2INT transforme le valeur de son format
C     externe avec les commandes au format interne de la pile.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_EXT2INT(VAL)

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IRET
      INTEGER ITP
C-----------------------------------------------------------------------

      ITP = IC_PILE_TYP_NOOP
      IF (SP_STRN_LEN(VAL) .GT. 1) THEN
         IF (VAL(2:2) .EQ. ',') THEN
            IF (VAL(1:1) .EQ. 'S') ITP = IC_PILE_TYP_SVALEUR
            IF (VAL(1:1) .EQ. 'L') ITP = IC_PILE_TYP_LVALEUR
            IF (VAL(1:1) .EQ. 'I') ITP = IC_PILE_TYP_IVALEUR
            IF (VAL(1:1) .EQ. 'R') ITP = IC_PILE_TYP_RVALEUR
            IF (VAL(1:1) .EQ. 'H') ITP = IC_PILE_TYP_HVALEUR
            IF (VAL(1:1) .EQ. 'X') ITP = IC_PILE_TYP_XVALEUR
            IF (VAL(1:1) .EQ. 'M') ITP = IC_PILE_TYP_MVALEUR

            IF (VAL(1:1) .EQ. 's') ITP = IC_PILE_TYP_SCONST
            IF (VAL(1:1) .EQ. 'l') ITP = IC_PILE_TYP_LCONST
            IF (VAL(1:1) .EQ. 'i') ITP = IC_PILE_TYP_ICONST
            IF (VAL(1:1) .EQ. 'r') ITP = IC_PILE_TYP_RCONST
            IF (VAL(1:1) .EQ. 'h') ITP = IC_PILE_TYP_HCONST
            IF (VAL(1:1) .EQ. 'x') ITP = IC_PILE_TYP_XCONST
            IF (VAL(1:1) .EQ. 'm') ITP = IC_PILE_TYP_MCONST
         ENDIF
      ENDIF

      IF (ITP .EQ. IC_PILE_TYP_NOOP) THEN
         VAL = ' '
      ELSE
         IRET = SP_STRN_RHT(VAL, ',')
         CALL SP_STRN_TRM(VAL)
      ENDIF
      IERR = IC_PILE_VALENC(VAL, ITP)

      IC_ICMD_EXT2INT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_ICMD_INT2EXT transforme le valeur de son format
C     interne de la pile au format avec les commandes.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les fonctions externes ne prennent pas encore de type,
C     mais seulement un chaîne de tokens séparés par des ,
C************************************************************************
      FUNCTION IC_ICMD_INT2EXT(VAL)

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IRET
      INTEGER I, ITP
      INTEGER LVAL, LARG
      CHARACTER*(256) ARG
      CHARACTER*(1024) TMP
C-----------------------------------------------------------------------

      IF (SP_STRN_LEN(VAL) .LE. 0) GOTO 9999

      TMP = VAL(1:SP_STRN_LEN(VAL))
      VAL = ' '

      I = 0
      DO WHILE (ERR_GOOD())
         I = I + 1
         IRET = SP_STRN_TKS(TMP, ',{', I, ARG)
         IF (IRET .EQ. -1) EXIT
         IF (IRET .NE.  0) GOTO 9900
         IF (ARG(1:1) .NE. '{') ARG = '{' // ARG(1:SP_STRN_LEN(ARG))

         IERR = IC_PILE_VALDEC(ARG, ITP)
         LVAL = SP_STRN_LEN(VAL)
         LARG = SP_STRN_LEN(ARG)
         IF (I .EQ. 1) THEN
            IF (LARG .GT. 0) VAL = ARG(1:LARG)
         ELSE
            IF (LVAL .GT. 0) THEN
               VAL = VAL(1:LVAL) // ','
            ELSE
               VAL = ','
            ENDIF
            LVAL = LVAL + 1
            IF (LARG .GT. 0) VAL = VAL(1:LVAL) // ARG(1:LARG)
         ENDIF
      ENDDO

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      TMP(1:SP_STRN_LEN(TMP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_INT2EXT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle de la fonction à partir du nom.
C
C Description:
C     La méthode privée IC_ICMD_REQHFNCFNC retourne le handle de la fonction
C     dont le nom est passé en argument.
C
C Entrée:
C     INTEGER       HOBJ      L'objet courant
C     CHARACTER*(*) SFNC      Le nom de la fonction
C
C Sortie:
C     INTEGER       HFNC      Le handle de la fonction
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQHFNCFNC(HOBJ, SFNC, HFNC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SFNC
      INTEGER       HFNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDIC
      CHARACTER*(256) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SFNC) .GT. 0)
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_FNCDIC(IOB)

C---     RECHERCHE LA FONCTION
      IERR = DS_MAP_REQVAL(HDIC, SFNC(1:SP_STRN_LEN(SFNC)), VAL)
      IF (ERR_BAD()) GOTO 9900

C---     DECODE LE HANDLE
      IF (ERR_GOOD()) READ(VAL, *) HFNC

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_FNCT_INCONNUE', ': ',
     &                        SFNC(1:SP_STRN_LEN(SFNC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQHFNCFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle de la fonction à partir du nom.
C
C Description:
C     La méthode privée IC_ICMD_REQHFNCFNI retourne le handle de la fonction
C     de l'IC dont le nom est passé en argument.
C
C Entrée:
C     INTEGER       HOBJ      L'objet courant
C     CHARACTER*(*) SFNC      Le nom de la fonction
C
C Sortie:
C     INTEGER       HFNC      Le handle de la fonction
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQHFNCFNI(HOBJ, SFNC, HFNC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SFNC
      INTEGER       HFNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HDIC
      CHARACTER*(256) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SFNC) .GT. 0)
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_FNIDIC(IOB)

C---     RECHERCHE LA FONCTION
      IERR = DS_MAP_REQVAL(HDIC, SFNC(1:SP_STRN_LEN(SFNC)), VAL)
      IF (ERR_BAD()) GOTO 9900

C---     DECODE LE HANDLE
      IF (ERR_GOOD()) READ(VAL, *) HFNC

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_FNCT_INCONNUE', ': ',
     &                        SFNC(1:SP_STRN_LEN(SFNC))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQHFNCFNI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée <code>IC_ICMD_REQHFNCCLS</code> retourne le handle
C     de la fonction enregistrée pour traiter l'objet HCLS
C
C Entrée:
C      INTEGER HOBJ  Handle de l'objet courant
C      INTEGER HCLS  Handle de la classe à traiter
C
C Sortie:
C      INTEGER HFNC  Handle sur la fonction
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCLS
      INTEGER HFNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HDIC, HBASE
      INTEGER  HMTH, HDOT
      CHARACTER*(64) KEY, VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_ICMD_HBASE
      HDIC = IC_ICMD_CLSDIC(IOB)

C---     CANONISE LE HANDLE
      HBASE = HCLS/1000 * 1000

C---     RECHERCHE LA FONCTION
      WRITE(KEY, '(2A,I12)') 'm', ',', HBASE
      IERR = IC_ICMD_EXT2INT(KEY)
      IERR = DS_MAP_REQVAL(HDIC, KEY(1:SP_STRN_LEN(KEY)), VAL)
      IF (ERR_BAD()) GOTO 9900

C---     DECODE LE HANDLE
      IF (ERR_GOOD()) THEN
         READ(VAL, *) HMTH, HDOT
         HFNC = HMTH
         IF (HMTH .EQ. 0) HFNC = HDOT
         IF (HCLS .EQ. HBASE) HFNC = HDOT
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ',HCLS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQHFNCCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle d'un objet.
C
C Description:
C     La fonction privée <code>IC_ICMD_REQHNDLCLS</code> retourne le handle
C     de l'objet OBJ.
C
C Entrée:
C      INTEGER       HOBJ  Handle de l'objet courant
C      CHARACTER*(*) OBJ   Nom de la variable de type handle
C
C Sortie:
C      INTEGER       HNDL  Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQHNDLCLS(HOBJ, OBJ, HNDL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OBJ
      INTEGER       HNDL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  ITP
      CHARACTER*(32) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(OBJ) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECHERCHE LE SYMBOLE
      IF (ERR_GOOD()) IERR = IC_PILE_VALENC(OBJ, IC_PILE_TYP_VARIABLE)
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, OBJ, VAL)

C---     TROUVÉ
      IF (ERR_GOOD()) THEN
         IERR = IC_PILE_VALDEC(VAL, ITP)
         IF (IC_PILE_ESTVALHDL(ITP)) THEN
            READ(VAL, *, ERR=9900, END=9900) HNDL
         ELSEIF (IC_ICMD_DOPARS(HOBJ) .OR.
     &       ITP .EQ. IC_PILE_TYP_ZVALEUR .OR.
     &       ITP .EQ. IC_PILE_TYP_ZCONST) THEN
            READ(VAL, *, ERR=9900, END=9900) HNDL
         ELSE
            GOTO 9900
         ENDIF

C---     PAS TROUVÉ, ESSAYE DE LIRE DIRECTEMENT LA VALEUR
      ELSE
         IF (ERR_ESTMSG('ERR_SYMBOLE_INCONNU')) THEN
            CALL ERR_RESET()
            IERR = IC_PILE_VALDEC(OBJ, ITP)
            READ(OBJ, *, ERR=9901, END=9901) HNDL
         ENDIF
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_HANDLE_INVALIDE', ': ',
     &                        OBJ(1:SP_STRN_LEN(OBJ))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TYPE', ': ', IC_PILE_TYP2TXT(ITP)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TYPE_ATTENDU', ': ',
     &                       IC_PILE_TYP2TXT(IC_PILE_TYP_HVALEUR)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VALEUR', ': ', VAL(1:SP_STRN_LEN(VAL))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_HANDLE_INVALIDE', ': ',
     &                        OBJ(1:SP_STRN_LEN(OBJ))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TYPE', ': ', IC_PILE_TYP2TXT(ITP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQHNDLCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la valeur d'un symbole.
C
C Description:
C     La méthode IC_ICMD_REQSMB retourne la valeur du symbole passé en
C     argument.
C
C Entrée:
C     INTEGER       HOBJ      L'objet courant
C     CHARACTER*(*) SMB       Le symbole demandé
C
C Sortie:
C     CHARACTER*(*) VAL       La valeur du symbole
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQSMB (HOBJ, SMB, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ICMD_REQSMB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITYP
      INTEGER I1, I2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Contrôle
      IF (SP_STRN_LEN(SMB) .LE. 0) GOTO 9900

C---     Début-fin du symbole
      I1 = IC_PILE_VALDEB(SMB)
      I2 = IC_PILE_VALFIN(SMB)

C---     Dispatch suivant le type
      ITYP = IC_PILE_VALTYP(SMB)
C---     Handle
      IF (IC_PILE_ESTVALHDL(ITYP)) THEN
         IF (INDEX(SMB(I1:I2), '[') .GT. 0) THEN      ! Opérateur []
            IERR = IC_ICMD_REQSMBBRK(HOBJ, SMB(I1:I2), VAL)
         ELSEIF (INDEX(SMB(I1:I2), '.') .GT. 0) THEN  ! Opérateur .
            IERR = IC_ICMD_REQSMBDOT(HOBJ, SMB(I1:I2), VAL)
         ELSE
            VAL = SMB
         ENDIF
C---     Variable
      ELSEIF (IC_PILE_ESTVAR(ITYP)) THEN
         IERR = IC_ICMD_REQSMBSMB(HOBJ, SMB(I1:I2), VAL)
         IF (ERR_ESTMSG('ERR_SYMBOLE_INCONNU')) THEN
            CALL ERR_RESET()
            IERR = IC_ICMD_REQSMBGLB(HOBJ, SMB(I1:I2), VAL)
         ENDIF
C---     Valeur
      ELSEIF (IC_PILE_ESTVAL(ITYP)) THEN
         VAL = SMB
      ELSE
         GOTO 9901
      END IF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SYMBOLE_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_TYPE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_ATTENDU: (var, val)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQSMB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQSMBGLB (HOBJ, SMB, VAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ISTS
      INTEGER HGLB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
C-----------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - IC_ICMD_HBASE
      HGLB = IC_ICMD_GLBDIC(IOB)
      ISTS = IC_ICMD_STS   (IOB)

C---     Recherche la chaîne du symbole
      IERR = DS_MAP_REQVAL(HGLB,
     &                     SMB(1:SP_STRN_LEN(SMB)),
     &                     VAL)
      IF (ERR_BAD() .AND. BTEST(ISTS, IC_ICMD_STS_PRS)) THEN
         CALL ERR_RESET()
         IERR = DS_MAP_REQVAL(HGLB,
     &                        '#-#' // SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL)
      ENDIF
      
C---     Traduis le message pour plus de clarté
      IF (ERR_ESTMSG('ERR_CLEF_INVALIDE')) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_SYMBOLE_INCONNU', ' : ',
     &                       SMB(1:MIN(50,SP_STRN_LEN(SMB)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQSMBGLB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQSMBSMB (HOBJ, SMB, VAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ISTS
      INTEGER HSMB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
C-----------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - IC_ICMD_HBASE
      HSMB = IC_ICMD_SMBDIC(IOB)
      ISTS = IC_ICMD_STS   (IOB)

C---     Recherche la chaîne du symbole
      IERR = DS_MAP_REQVAL(HSMB,
     &                     SMB(1:SP_STRN_LEN(SMB)),
     &                     VAL)
      IF (ERR_BAD() .AND. BTEST(ISTS, IC_ICMD_STS_PRS)) THEN
         CALL ERR_RESET()
         IERR = DS_MAP_REQVAL(HSMB,
     &                        '#-#' // SMB(1:SP_STRN_LEN(SMB)),
     &                        VAL)
      ENDIF
      
C---     Traduis le message pour plus de clarté
      IF (ERR_ESTMSG('ERR_CLEF_INVALIDE')) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_SYMBOLE_INCONNU', ' : ',
     &                       SMB(1:MIN(50,SP_STRN_LEN(SMB)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_REQSMBSMB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQSMBBRK (HOBJ, SMB, VAL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR, IDUM
      INTEGER  HCLS, HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB
      CHARACTER*(64) SMB_TMP

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
C-----------------------------------------------------------------------

C---     HANDLE DE LA CLASSE
      SMB_TMP = SMB(1:SP_STRN_LEN(SMB))
      IDUM = SP_STRN_LFT(SMB_TMP, '[')
      IERR = IC_ICMD_REQHNDLCLS(HOBJ, SMB_TMP, HCLS)

C---     Mode PARSING, termine
      IF (ERR_GOOD() .AND. IC_ICMD_DOPARS(HOBJ)) THEN
         VAL = IC_PILE_VALTAG(IC_PILE_TYP_ZVALEUR)
         IERR = IC_PILE_VALENC(VAL, IC_PILE_TYP_ZVALEUR)
         GOTO 9999
      ENDIF

C---     HANDLE DE LA FONCTION A APPELER
      IF (ERR_GOOD()) IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)

C---     APPEL LA FONCTION
      IND_CMN = IND_CMN + 1
D     CALL ERR_ASR(IND_CMN .LT. SIZ_CMN)
      IF (ERR_GOOD()) THEN
         HDL_CMN(IND_CMN) = HCLS
         FNC_CMN(IND_CMN) = '##opb_[]_get##'          ! Méthode spéciale
         PRM_CMN(IND_CMN) = SMB(1:SP_STRN_LEN(SMB))   ! Indice en param
         IDUM = SP_STRN_RHT(PRM_CMN(IND_CMN), '[')
         IDUM = SP_STRN_LFT(PRM_CMN(IND_CMN), ']')
         IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
      ENDIF

C---     DECODE LE RETOUR
      IF (ERR_GOOD()) THEN
         VAL = PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
         IERR = IC_ICMD_EXT2INT(VAL)
      ENDIF
      IND_CMN = IND_CMN - 1
D     CALL ERR_ASR(IND_CMN .GE. 0)

9999  CONTINUE
      IC_ICMD_REQSMBBRK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_REQSMBDOT (HOBJ, SMB, VAL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) SMB
      CHARACTER*(*) VAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR, IDUM
      INTEGER  HCLS, HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB
      CHARACTER*(64) SMB_TMP

      INTEGER, PARAMETER :: SIZ_CMN = 3
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(SMB) .GT. 0)
C-----------------------------------------------------------------------

C---     HANDLE DE LA CLASSE
      SMB_TMP = SMB(1:SP_STRN_LEN(SMB))
      IDUM = SP_STRN_LFT(SMB_TMP, '.')
      IERR = IC_ICMD_REQHNDLCLS(HOBJ, SMB_TMP, HCLS)

C---     Mode PARSING, termine
      IF (ERR_GOOD() .AND. IC_ICMD_DOPARS(HOBJ)) THEN
         VAL = IC_PILE_VALTAG(IC_PILE_TYP_ZVALEUR)
         IERR = IC_PILE_VALENC(VAL, IC_PILE_TYP_ZVALEUR)
         GOTO 9999
      ENDIF

C---     BOUCLE POUR RÉSOUDRE LES PROXY ET INDIRECTIONS
C        ==============================================
100   CONTINUE

C---        HANDLE DE LA FONCTION A APPELER
         IF (ERR_GOOD()) IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)

C---        APPEL LA FONCTION
         IND_CMN = IND_CMN + 1
D        CALL ERR_ASR(IND_CMN .LT. SIZ_CMN)
         IF (ERR_GOOD()) THEN
            HDL_CMN(IND_CMN) = HCLS
            FNC_CMN(IND_CMN) = '##property_get##'        ! Méthode spéciale
            PRM_CMN(IND_CMN) = SMB(1:SP_STRN_LEN(SMB))   ! Attribut en param
            IDUM = SP_STRN_RHT(PRM_CMN(IND_CMN), '.')
            IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
         ENDIF

C---        DECODE LE RETOUR
         IF (ERR_GOOD()) THEN
            VAL = PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
            IERR = IC_ICMD_EXT2INT(VAL)
         ENDIF
         IND_CMN = IND_CMN - 1
D        CALL ERR_ASR(IND_CMN .GE. 0)

C---     BOUCLE TANT QUE LE TYPE DE RETOUR EST IC_PILE_TYP_XVALEUR
      IF (ERR_GOOD()) THEN
         IF (IC_PILE_VALTYP(VAL) .EQ. IC_PILE_TYP_XVALEUR) THEN
            READ (VAL, *) HCLS
            GOTO 100
         ENDIF
      ENDIF

9999  CONTINUE
      IC_ICMD_REQSMBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Unwind the stack
C
C Description:
C     La fonction IC_ICMD_UNWNDSTK dépile la pile d'exécution
C     (unwind the stack) et en fait l'impression.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_UNWNDSTK(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFST, HFIN, HFNI
      INTEGER LSRC
      CHARACTER*(256) NFNC, FSRC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2'

      CALL ERR_PUSH()
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_PILE_APPEL:')
      CALL LOG_INCIND()

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)    ! Pile des fichiers

100   CONTINUE
         IERR = IC_PFIC_TOP(HFST, HFIN, HFNI)
         IF (ERR_BAD()) GOTO 199

         LSRC = IC_FINP_REQLSRC(HFIN)

         IF (IC_FUNC_HVALIDE(HFNI)) THEN
            NFNC = IC_FUNC_REQNFNC(HFNI)
            FSRC = IC_FUNC_REQFSRC(HFNI)
         ELSE
            NFNC = 'main'
            IERR = IC_FINP_REQNOM (HFIN, FSRC)
         ENDIF

C---        Complète le message d'erreur
         CALL SP_STRN_CLP(FSRC(1:SP_STRN_LEN(FSRC)), 80)
         WRITE(LOG_BUF, '(A,4A,A,I4,A,4A)') '#<3>#',
     &      'MSG_FICHIER', ' ''', FSRC(1:SP_STRN_LEN(FSRC)), ''', ',
     &      'MSG_LIGNE',  LSRC, ', ',
     &      'MSG_DANS', ' ''', NFNC(1:SP_STRN_LEN(NFNC)), ''''
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---        Pop la fonction
         IERR = IC_PFIC_POP(HFST)
      GOTO 100
199   CONTINUE

      CALL LOG_DECIND()
      CALL ERR_POP()
      IC_ICMD_UNWNDSTK =  ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_ICMD_PRSFIC
C
C Description:
C     La fonction privée <code>IC_ICMD_PRSFIC</code> parse le fichier
C     de commande qui est sur le dessus de la pile des fichier. Celui-ci
C     va interpreter sans les exécuter les commandes afin de détecter
C     les erreurs.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_PRSFIC(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpars.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER   ILLIN
      PARAMETER (ILLIN = 1024)

      INTEGER IERR
      INTEGER IOB
      INTEGER HXST, HFST, HSCT, HFIN, HFNC, HFNI, HLST
      INTEGER LCMD, LINI, LCUR, LSRC
      INTEGER NFST, NSCT
      CHARACTER*(ILLIN) CMDSTAT
      CHARACTER*(256)   BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HXST = IC_ICMD_PRSSTK(IOB)    ! La pile de parse
      HFST = IC_ICMD_FILSTK(IOB)    ! La pile des fichiers
      HSCT = IC_ICMD_SCTSTK(IOB)    ! La pile des structures

C---     Initialise
      HFIN = 0          ! Fichier input
      HFNC = 0          ! Fonction en parsing
      HLST = 0          ! Stack des labels
      CMDSTAT = ' '
      IERR = IC_PILE_CLR(HXST)

C---     État initial
      NFST = IC_PFIC_SIZE(HFST)     ! Dim de la pile de fichier
      NSCT = DS_LIST_REQDIM(HSCT)   ! Dim de la pile des structures

C---     Met en mode PARSING
      IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
      IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_PRS)

C---     Boucle de traitement
200   CONTINUE
         IERR = IC_PFIC_TOP(HFST, HFIN, HFNI)
         IF (ERR_GOOD()) IERR = IC_FINP_LISCMD(HFIN,
     &                                         LINI, LCUR,
     &                                         CMDSTAT)
         LCMD = SP_STRN_LEN(CMDSTAT)
         IF (ERR_GOOD()) IERR = IC_ICMD_XTRFNC(HOBJ,
     &                                         HFNC,
     &                                         LINI, LCUR,
     &                                         CMDSTAT(1:LCMD))
!         IF (ERR_GOOD()) IERR = IC_ICMD_XTRLBL(HOBJ,
!     &                                         HLST,
!     &                                         CMDSTAT(1:LCMD))
         IF (ERR_GOOD()) IERR = IC_PARS_CMDPIL(CMDSTAT(1:LCMD),
     &                                         HXST)
         IF (ERR_GOOD()) IERR = IC_ICMD_PRXPIL(HOBJ, HXST)
         IF (ERR_GOOD()) IERR = IC_ICMD_PRSPIL(HOBJ, HXST)
         IF (ERR_GOOD()) IERR = IC_PILE_CLR(HXST)

         IF (ERR_ESTMSG('ERR_CMD_STOP')) THEN
            CALL ERR_RESET()
         ELSEIF (ERR_ESTMSG('ERR_FIN_FICHIER_CMD')) THEN
            IF (HFNC .EQ. 0) THEN
                CALL ERR_RESET()
                IERR = IC_PFIC_POP(HFST)
            ELSE
D               CALL ERR_ASR(IC_FUNC_HVALIDE(HFNC))
                CALL ERR_ASG(ERR_ERR, 'ERR_FONCTION_SANS_FIN')
            ENDIF
         ENDIF
      IF (ERR_GOOD() .AND. IC_PFIC_SIZE(HFST) .GE. NFST) GOTO 200

C---     Contrôle que la pile de parse est vide
      IF (ERR_GOOD() .AND. DS_LIST_REQDIM(HSCT) .NE. NSCT) THEN
         WRITE(ERR_BUF, '(4A)') 'ERR_STRUCTURES_NON_FERMEES'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

C---     Vide les symboles zombie de parsing
      IF (ERR_GOOD()) THEN
         IERR = IC_ICMD_PRS999(HOBJ)
      ENDIF

C---     Complete le message d'erreur
      IF (ERR_GOOD()) GOTO 9999
      IF (.NOT. IC_FINP_HVALIDE(HFIN)) GOTO 9999

      LCMD = SP_STRN_LEN(CMDSTAT)
      CALL SP_STRN_CLP(CMDSTAT(1:LCMD), 128)
      LCMD = SP_STRN_LEN(CMDSTAT)
      WRITE(ERR_BUF, '(4A)')
     &   '#<3>#MSG_COMMANDE', ': ''', CMDSTAT(1:LCMD), ''''
      CALL ERR_AJT(ERR_BUF)

      IF (IC_FUNC_HVALIDE(HFNI)) THEN
         BUF  = IC_FUNC_REQFSRC(HFNI)
      ELSE
         IERR = IC_FINP_REQNOM (HFIN, BUF)
      ENDIF
      CALL SP_STRN_CLP(BUF(1:SP_STRN_LEN(BUF)), 128)
      WRITE(ERR_BUF, '(3A)')
     &   '#<3>#MSG_FICHIER', ': ', BUF(1:SP_STRN_LEN(BUF))
      CALL ERR_AJT(ERR_BUF)

      LSRC = IC_FINP_REQLSRC(HFIN)
      WRITE(ERR_BUF, '(2A,I4)')
     &   '#<3>#MSG_LIGNE', ': ', LSRC
      CALL ERR_AJT(ERR_BUF)

      IF (IC_FUNC_HVALIDE(HFNC)) THEN
         BUF = IC_FUNC_REQNFNC(HFNC)
         CALL SP_STRN_CLP(BUF(1:SP_STRN_LEN(BUF)), 128)
         WRITE(ERR_BUF, '(4A)')
     &      '#<3>#MSG_FONCTION', ': ', BUF(1:SP_STRN_LEN(BUF))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

9999  CONTINUE
      IC_ICMD_PRSFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Parse la pile
C
C Description:
C     La fonction privée IC_ICMD_PRSPIL traverse la pile en contrôlant les
C     commandes.
C
C Entrée:
C     INTEGER       HOBJ                  Handle sur l'objet courant
C     INTEGER       HSCT                  Handle sur Parse STack
C
C Sortie:
C     CHARACTER     RSLT                  Le résultat
C
C Notes:
C     Il faut sauter la structure de la fonction car les variables
C     des fonctions sont locales.
C************************************************************************
      FUNCTION IC_ICMD_PRSPIL(HOBJ, HSCT)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSCT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER OPRTYP
      CHARACTER*(1024) OPR, RHS, LHS
      LOGICAL ENDLOOP
      LOGICAL LSKIP
C-----------------------------------------------------------------------
      LOGICAL PRSFNC
      INTEGER H
      PRSFNC(H) = BTEST(IC_ICMD_STS(H-IC_ICMD_HBASE),IC_ICMD_STS_PRSFNC)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_HVALIDE(HSCT))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IOB = HOBJ - IC_ICMD_HBASE

C---     Ne saute pas les mots de contrôle
      LSKIP = .FALSE.
      IF (PRSFNC(HOBJ)) THEN
         LSKIP = .TRUE.
         IF (IC_PILE_FRSIZ(HSCT) .GE. 3) THEN
            IERR = IC_PILE_FRBSE(HSCT, OPR)
            OPRTYP = IC_PILE_VALTYP(OPR)
            IF (IC_PILE_ESTKW(OPRTYP)) THEN            ! mot clef
               IERR = IC_PILE_VALDEC(OPR, OPRTYP)
               IF (IC_ICMD_ESTCW(OPR)) THEN            ! mot de contrôle
                  IF (IC_ICMD_ESTIF   (OPR) .OR.
     &                IC_ICMD_ESTELIF (OPR) .OR.
     &                IC_ICMD_ESTWHILE(OPR)) THEN
                     IERR=IC_PILE_FRCLR(HSCT)
                     IERR=IC_PILE_PUSHE(HSCT, OPR, OPRTYP)
                     IERR=IC_PILE_PUSHE(HSCT, 'F', IC_PILE_TYP_LVALEUR)
                     IERR=IC_PILE_PUSHE(HSCT, ' ', IC_PILE_TYP_OPPAR)
                  ELSE
                     IERR=IC_PILE_FRCLR(HSCT)
                     IERR=IC_PILE_PUSHE(HSCT, OPR, OPRTYP)
                     IERR=IC_PILE_PUSHE(HSCT, ' ', IC_PILE_TYP_SVALEUR)
                     IERR=IC_PILE_PUSHE(HSCT, ' ', IC_PILE_TYP_OPPAR)
                  ENDIF
                  LSKIP = .FALSE.
               ENDIF
            ENDIF
         ENDIF
      ENDIF
      IF (LSKIP) THEN
         IERR = IC_PILE_FRCLR(HSCT)
         GOTO 9999
      ENDIF

C---     Parse la pile
      ENDLOOP = .FALSE.
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (ENDLOOP)   GOTO 199

C---        Prochaine instruction
         IF (ERR_GOOD())
     &      IERR = IC_PILE_FROPDEB(HSCT, OPR, RHS, LHS)

C---        Switch sur le type d'opération
         OPRTYP = IC_PILE_VALTYP(OPR)
         IF     (IC_PILE_ESTNOOP(OPRTYP)) THEN
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTVAL(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_ICMD_INT2EXT(RHS)
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTVAR(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_ICMD_INT2EXT(RHS)
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTOPU(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_XEQOPU(HOBJ, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_PILE_FROPFIN(HSCT, OPR)

         ELSEIF (IC_PILE_ESTOPB(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_XEQOPB(HOBJ, LHS, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_PILE_FROPFIN(HSCT, OPR)

         ELSE
            GOTO 9900
         ENDIF

C---        Skip rest of statement
         IF (BTEST(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTA)) THEN
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTA)
            IERR = IC_PILE_FRCLR(HSCT)
            ENDLOOP = .TRUE.
         ENDIF

      GOTO 100
199   CONTINUE

C---     Contrôle que la pile est vide
      IF (ERR_GOOD()) THEN
         IF (IC_PILE_FRSIZ(HSCT) .NE. 0) GOTO 9901
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_OPERATION_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OPR', ': ', OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OP_ATTENDU', ': ',
     &                       '(noop, val, var, opu, opb)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OP_COURANT', ': ',
     &                       IC_PILE_TYP2TXT(OPRTYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_PILE_NON_VIDE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_PRSPIL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Nettoie en fin de parsing.
C
C Description:
C     La fonction privée IC_ICMD_PRS999 nettoie en fin de parsing.
C
C Entrée:
C     INTEGER       HOBJ                  Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_PRS999(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSCT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSMB
      INTEGER I
      CHARACTER*(256) VAR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE
      HSMB = IC_ICMD_SMBDIC(IOB)

      I = 1
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (I .GT. DS_MAP_REQDIM(HSMB)) GOTO 199

         IERR = DS_MAP_REQCLF(HSMB, I, VAR)
         IF (SP_STRN_LEN(VAR) .GT. 3 .AND. VAR(1:3) .EQ. '#-#') THEN
            IERR = DS_MAP_EFFCLF(HSMB, VAR(1:SP_STRN_LEN(VAR)))
         ELSE
            I = I + 1
         ENDIF

      GOTO 100
199   CONTINUE

      IC_ICMD_PRS999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_ICMD_XEQFIC
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFIC</code> exécute le fichier de
C     commande qui est sur le dessus de la pile des fichiers. Celui-ci va
C     interpreter les commandes tant qu'il n'est pas en erreur ou qu'il ne
C     reçoit une commande de fin. En mode interactif, le prompt est écrit
C     sur l'unité MW.
C
C Entrée:
C     INTEGER  HOBJ        Handle sur l'objet
C     INTEGER  MW          Unité d'écriture
C     LOGICAL  ECRISPROMPT .TRUE. si on doit écrire un prompt.
C     LOGICAL  INTERACTIF  .TRUE. si le mode est interactif.
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQFIC(HOBJ, RSLT, MW, ECRISPROMPT, INTERACTIF)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RSLT
      INTEGER       MW
      LOGICAL       ECRISPROMPT
      LOGICAL       INTERACTIF

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpars.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER   ILLIN
      PARAMETER (ILLIN = 1024)

      INTEGER IERR
      INTEGER IOB
      INTEGER HXST, HFST, HFIN, HFNI
      INTEGER LCMD, LINI, LCUR
      INTEGER NFST
      CHARACTER*(ILLIN) CMDSTAT
      LOGICAL DOITLIRE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(.NOT. (ECRISPROMPT .AND. .NOT. INTERACTIF))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HXST = IC_ICMD_XEQSTK(IOB)    ! La pile d'exécution
      HFST = IC_ICMD_FILSTK(IOB)    ! La pile des fichiers

C---     Initialise
      CMDSTAT = ' '
      IERR = IC_PILE_FRPUSH(HXST)

C---     État initial
      NFST = IC_PFIC_SIZE(HFST)     ! Dim de la pile de fichier

C---     Met en mode XEQ
      IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
      IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_PRS)

C---     Boucle de traitement
      DOITLIRE = .TRUE.
200   CONTINUE
         IF (DOITLIRE) THEN
            IF (ECRISPROMPT .AND. IC_PFIC_SIZE(HFST) .EQ. 1)
     &         WRITE(MW, '(A,$)') '>> '
            IERR = IC_PFIC_TOP(HFST, HFIN, HFNI)
            IF (ERR_GOOD()) IERR = IC_FINP_LISCMD(HFIN,
     &                                            LINI, LCUR,
     &                                            CMDSTAT)
            LCMD = SP_STRN_LEN(CMDSTAT)
            IF (ERR_GOOD()) IERR = IC_PARS_CMDPIL(CMDSTAT(1:LCMD),
     &                                            HXST)
!!!            IF (ERR_GOOD()) IERR = IC_PILE_DUMP  (HXST)
            IF (ERR_GOOD()) IERR = IC_ICMD_PRXPIL(HOBJ, HXST)
         ENDIF
         DOITLIRE = .TRUE.

         IF (ERR_GOOD()) IERR = IC_ICMD_XEQPIL(HOBJ, HXST, RSLT)

         IF (INTERACTIF) THEN
            IF (ERR_BAD() .AND. .NOT. ERR_ESTMSG('ERR_CMD_STOP')) THEN
               CALL LOG_MSG_ERR()
               CALL ERR_RESET()
               IERR = IC_PILE_FRCLR(HXST)  ! or only IC_PILE_FRCLR??
            ENDIF
         ENDIF
         IF (ERR_ESTMSG('ERR_FIN_FICHIER_CMD')) THEN
            CALL ERR_RESET()
            IERR = IC_PFIC_POP(HFST)
            IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQFNC)) THEN
               IERR = IC_ICMD_XEQFNCFNI_END(HOBJ)
               DOITLIRE = .FALSE.
            ENDIF
         ENDIF
      IF (ERR_GOOD() .AND. IC_PFIC_SIZE(HFST) .GE. NFST) GOTO 200

C---     Nettoie le frame
      IF (ERR_GOOD()) THEN
         IERR = IC_PILE_FRPOP(HXST)
      ELSE
         CALL ERR_PUSH()
         IERR = IC_PILE_FRCLR(HXST)
         CALL ERR_POP()
      ENDIF

      IC_ICMD_XEQFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de fonction.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNC</code> effectue un appel
C     de mot clef, de fonction ou un appel de méthode.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     PVAL     Valeur des paramètres de la fonction
C     FNC      Nom de la fonction
C
C Sortie:
C     RVAL     Valeur du retour
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQFNC (HOBJ, RVAL, PVAL, FNC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RVAL
      CHARACTER*(*) PVAL
      CHARACTER*(*) FNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IRET
      INTEGER IT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNC) .GT. 0)
C-----------------------------------------------------------------------

      IF (INDEX(FNC, '.') .GT. 0) THEN
         IERR = IC_ICMD_XEQFNCDOT(HOBJ, RVAL, PVAL, FNC)
      ELSEIF (IC_ICMD_ESTKW(FNC)) THEN
         IERR = IC_ICMD_XEQFNCKWD(HOBJ, RVAL, PVAL, FNC)
      ELSEIF (IC_ICMD_ESTFNC(HOBJ, FNC)) THEN
         IERR = IC_ICMD_XEQFNCFNC(HOBJ, RVAL, PVAL, FNC)
      ELSE
         IERR = IC_ICMD_XEQFNCFNI(HOBJ, RVAL, PVAL, FNC)
      ENDIF

C---     COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(3A)') '#<3>#MSG_FONCTION', ': ',
     &                           FNC(1:SP_STRN_LEN(FNC))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      IC_ICMD_XEQFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de fonction.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNCFNC</code> effectue un appel
C     de fonction.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     PVAL     Valeur des paramètres de la fonction
C     FNC      Nom de la fonction
C
C Sortie:
C     RVAL     Valeur du retour
C
C Notes:
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C************************************************************************
      FUNCTION IC_ICMD_XEQFNCFNC (HOBJ, RVAL, PVAL, FNC)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RVAL
      CHARACTER*(*) PVAL
      CHARACTER*(*) FNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNC) .GT. 0)
C-----------------------------------------------------------------------

C---     HANDLE DE LA FONCTION A APPELER
      IERR = IC_ICMD_REQHFNCFNC(HOBJ, FNC, HFNC)

C---     En mode PARSING, sort
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (ERR_GOOD()) THEN
            RVAL = IC_PILE_VALTAG(IC_PILE_TYP_ZVALEUR)
            IERR = IC_PILE_VALENC(RVAL, IC_PILE_TYP_ZVALEUR)
         ENDIF
         GOTO 9999
      ENDIF

C---     APPEL LA FONCTION
      IND_CMN = IND_CMN + 1
D     CALL ERR_ASR(IND_CMN .LE. SIZ_CMN)
      IF (ERR_GOOD()) THEN
         HDL_CMN(IND_CMN) = 0
         FNC_CMN(IND_CMN) = FNC(1:SP_STRN_LEN(FNC))
         PRM_CMN(IND_CMN) = PVAL(1:SP_STRN_LEN(PVAL))
         IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))
         IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
      ENDIF

C---     DECODE LE RETOUR
      IF (ERR_GOOD()) THEN
         RVAL = PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
         IERR = IC_ICMD_EXT2INT(RVAL)
      ENDIF

      IND_CMN = IND_CMN - 1
D     CALL ERR_ASR(IND_CMN .GE. 0)

9999  CONTINUE
      IC_ICMD_XEQFNCFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de fonction de l'IC.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNCFNI</code> effectue un appel
C     de fonction de l'IC.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     PVAL     Valeur des paramètres de la fonction
C     FNC      Nom de la fonction
C
C Sortie:
C     RVAL     Valeur du retour
C
C Notes:
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C************************************************************************
      FUNCTION IC_ICMD_XEQFNCFNI (HOBJ, RVAL, PVAL, FNC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RVAL
      CHARACTER*(*) PVAL
      CHARACTER*(*) FNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HSMF, HFST, HSCT, HXST, HFNC
      CHARACTER*(256)  FFNC
      CHARACTER*(1024) PRM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNC) .GT. 0)
C-----------------------------------------------------------------------

C---     Handle sur la fonction à appeler
      IERR = IC_ICMD_REQHFNCFNI(HOBJ, FNC, HFNC)

C---     Récupère les attributs
      IOB = HOBJ - IC_ICMD_HBASE
      IF (ERR_GOOD()) THEN
         HFST = IC_ICMD_FILSTK(IOB)
         HSCT = IC_ICMD_SCTSTK(IOB)
         HXST = IC_ICMD_XEQSTK(IOB)
      ENDIF

C---     En mode PARSING, contrôle les paramètres et sort
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (ERR_GOOD()) THEN
            PRM = PVAL(1:SP_STRN_LEN(PVAL))
            IF (PRM(1:SP_STRN_LEN(PRM)) .EQ. '{10:}') PRM = ' '
            IERR = IC_FUNC_CHKARGS(HFNC, HOBJ, PRM(1:SP_STRN_LEN(PRM)))
         ENDIF
         IF (ERR_GOOD()) THEN
            RVAL = IC_PILE_VALTAG(IC_PILE_TYP_ZVALEUR)
            IERR = IC_PILE_VALENC(RVAL, IC_PILE_TYP_ZVALEUR)
         ENDIF
         GOTO 9999
      ENDIF

C---     Push les paramètres de la fonction
      IF (ERR_GOOD()) THEN
         PRM = PVAL(1:SP_STRN_LEN(PVAL))
         IF (PRM(1:SP_STRN_LEN(PRM)) .EQ. '{10:}') PRM = ' '
         IERR=IC_FUNC_PUSH(HFNC, HOBJ, PRM(1:SP_STRN_LEN(PRM)), HSMF)
      ENDIF

C---     Push le stack des fonctions
      IF (ERR_GOOD()) IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_SMBDIC(IOB))
      IF (ERR_GOOD()) IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_STS   (IOB))
      IF (ERR_GOOD()) IERR = DS_LIST_PUSHI(HSCT, HFNC)
      IF (ERR_GOOD()) IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_SCT_FNC)    ! Token de contrôle

C---     Le nouveau dico des symboles et l'état
      IF (ERR_GOOD()) THEN
         IC_ICMD_SMBDIC(IOB)=HSMF
         IC_ICMD_STS   (IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQFNC)
         IC_ICMD_STS   (IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTA)
      ENDIF

C---     Ouvre le fichier
      IF (ERR_GOOD()) THEN
         IERR = IC_PILE_FRPUSH (HXST)
         FFNC = IC_FUNC_REQFFNC(HFNC)
         IERR = IC_PFIC_PUSH(HFST, FFNC(1:SP_STRN_LEN(FFNC)), -1, HFNC)
      ENDIF

9999  CONTINUE
      IC_ICMD_XEQFNCFNI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de fonction de l'IC.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNCFNI</code> effectue un appel
C     de fonction de l'IC.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQFNCFNI_END (HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  ITOK
      INTEGER  HSMB, HSCT, HXST, HFNC
      CHARACTER*(256) RVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_ICMD_HBASE
      IF (ERR_GOOD()) THEN
         HSMB = IC_ICMD_SMBDIC(IOB)
         HSCT = IC_ICMD_SCTSTK(IOB)
         HXST = IC_ICMD_XEQSTK(IOB)
      ENDIF

C---     Contrôles
      IF (ERR_GOOD()) THEN
         IF (DS_LIST_REQDIM(HSCT) .LT. 4) GOTO 9900
         IERR = DS_LIST_TOPI(HSCT, ITOK)
         IERR = DS_LIST_POP (HSCT)
         IF (ITOK .NE. IC_ICMD_SCT_FNC) GOTO 9901
      ENDIF

C---     Pop le handle de la fonction du stack
      IF (ERR_GOOD()) THEN
         IERR = DS_LIST_TOPI(HSCT, HFNC)
D        CALL ERR_ASR(IC_FUNC_HVALIDE(HFNC))
         IERR = DS_LIST_POP (HSCT)
      ENDIF

C---     Pop la fonction
      IF (ERR_GOOD()) IERR = IC_FUNC_POP(HFNC, HSMB, RVAL)

C---     Pop le reste du stack
      IF (ERR_GOOD()) IERR = DS_LIST_TOPI(HSCT, IC_ICMD_STS   (IOB))
      IF (ERR_GOOD()) IERR = DS_LIST_POP (HSCT)
      IF (ERR_GOOD()) IERR = DS_LIST_TOPI(HSCT, IC_ICMD_SMBDIC(IOB))
      IF (ERR_GOOD()) IERR = DS_LIST_POP (HSCT)

C---     Pop la pile d'exécution
      IF (ERR_GOOD()) IERR = IC_PILE_FRPOP(HXST)

C---     Assigne la valeur de retour
      IF (ERR_GOOD()) THEN
         IERR = IC_PILE_FROPFIN(HXST, RVAL(1:SP_STRN_LEN(RVAL)))
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,I3))') 'MSG_DIM_INVALIDE',
     &                              ':', DS_LIST_REQDIM(HSCT), ' / ', 4
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_JETON_FONCTION_ATTENDU'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQFNCFNI_END = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de mot-clef.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNCKWD</code> effectue un appel
C     de mot clef.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     PVAL     Valeur des paramètres de la fonction
C     FNC      Nom de la fonction
C
C Sortie:
C     RVAL     Valeur du retour
C
C Notes:
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C************************************************************************
      FUNCTION IC_ICMD_XEQFNCKWD (HOBJ, RVAL, PVAL, FNC)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RVAL
      CHARACTER*(*) PVAL
      CHARACTER*(*) FNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNC) .GT. 0)
C-----------------------------------------------------------------------

      IND_CMN = IND_CMN + 1
      IF (IND_CMN .GT. SIZ_CMN) GOTO 9900

C---     HANDLE DU MOT CLEF, ENREGISTRÉ COMME UNE FONCTION
C---     MAIS APPELÉ COMME UNE MÉTHODE
      IERR = IC_ICMD_REQHFNCFNC(HOBJ, FNC, HFNC)

C---     APPEL DU MOT CLEF
      IF (ERR_GOOD()) THEN
         HDL_CMN(IND_CMN) = HOBJ
         FNC_CMN(IND_CMN) = FNC (1:SP_STRN_LEN(FNC))
         PRM_CMN(IND_CMN) = PVAL(1:SP_STRN_LEN(PVAL))
         IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
      ENDIF

C---     DECODE LE RETOUR
      IF (ERR_GOOD()) THEN
         RVAL = PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
         IERR = IC_ICMD_EXT2INT(RVAL)
      ENDIF

      IND_CMN = IND_CMN - 1
D     CALL ERR_ASR(IND_CMN .GE. 0)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PILE_RECURSION_PLEINE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQFNCKWD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute un appel de méthode.
C
C Description:
C     La fonction privée <code>IC_ICMD_XEQFNCDOT</code> effectue un appel
C     de méthode.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     PTYP     Type des paramètres de la fonction
C     PVAL     Valeur des paramètres de la fonction
C     FNC      Nom de la fonction
C
C Sortie:
C     RTYP     Type du retour
C     RVAL     Valeur du retour
C
C Notes:
C     1) On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C     2) Si la classe est enregistrée, on fait un appel de méthode. Sinon,
C     on appelle la méthode comme un fonction en passant le handle de l'objet
C     comme premier paramètre.
C     3) Pour résoudre les indirections de proxy, on boucle.
C************************************************************************
      FUNCTION IC_ICMD_XEQFNCDOT (HOBJ, RVAL, PVAL, FNC)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) RVAL
      CHARACTER*(*) PVAL
      CHARACTER*(*) FNC

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER  IERR, IDUM, I1, I2
      INTEGER  HCLS, HFNC
      LOGICAL  EST_MTH, EST_FNC, EST_KWD
      INTEGER  IC_ICMD_XEQFNC_CB
      EXTERNAL IC_ICMD_XEQFNC_CB
      CHARACTER*(64)   BUF

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FNC) .GT. 0)
D     CALL ERR_PRE(INDEX(FNC, '.') .GT. 0)
C-----------------------------------------------------------------------

C---     En mode PARSING, sort
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         RVAL = IC_PILE_VALTAG(IC_PILE_TYP_ZVALEUR)
         IERR = IC_PILE_VALENC(RVAL, IC_PILE_TYP_ZVALEUR)
         GOTO 9999
      ENDIF

      IND_CMN = IND_CMN + 1
D     CALL ERR_ASR(IND_CMN .LE. SIZ_CMN)

C---     BOUCLE POUR RÉSOUDRE LES PROXY ET INDIRECTIONS
C        ==============================================
      HCLS = 0
100   CONTINUE

C---        Handle de la classe
         FNC_CMN(IND_CMN) = FNC(1:SP_STRN_LEN(FNC))
         IDUM = SP_STRN_LFT(FNC_CMN(IND_CMN), '.')
         IF (HCLS .EQ. 0) THEN
            IERR = IC_ICMD_REQHNDLCLS(HOBJ, FNC_CMN(IND_CMN), HCLS)
         ENDIF

C---        Handle de la fonction à appeler
         EST_MTH = .FALSE.
         EST_FNC = .FALSE.
         EST_KWD = .FALSE.
         IF (ERR_GOOD()) THEN
            IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)
            IF (ERR_GOOD()) THEN ! Classe connue
               EST_MTH = .TRUE.
            ELSE                 ! Classe inconnue ==> fonction
               CALL ERR_RESET()
               BUF = FNC(1:SP_STRN_LEN(FNC))
               IDUM = SP_STRN_RHT(BUF, '.')
               IERR = IC_ICMD_REQHFNCFNC(HOBJ, BUF, HFNC)
               IF (ERR_GOOD()) THEN
                  IF (IC_ICMD_ESTKW(BUF(1:SP_STRN_LEN(BUF)))) THEN
                     EST_KWD = .TRUE.
                  ELSE
                     EST_FNC = .TRUE.
                  ENDIF
               ENDIF
            ENDIF
         ENDIF

C---        Appel la méthode, la fonction ou le mot-clef
         IF (ERR_GOOD()) THEN
            IF (EST_MTH) THEN       ! Méthode
               HDL_CMN(IND_CMN) = HCLS
               FNC_CMN(IND_CMN) = FNC(1:SP_STRN_LEN(FNC))
               PRM_CMN(IND_CMN) = PVAL(1:SP_STRN_LEN(PVAL))
               IDUM = SP_STRN_RHT(FNC_CMN(IND_CMN), '.')
               IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))
               IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
            ELSEIF (EST_KWD) THEN   ! Mot-clef
               WRITE(BUF, '(2A,I12)') 'H', ',', HCLS
               IERR = IC_ICMD_EXT2INT(BUF)
               HDL_CMN(IND_CMN) = HOBJ
               FNC_CMN(IND_CMN) = FNC(1:SP_STRN_LEN(FNC))
               IDUM = SP_STRN_RHT(FNC_CMN(IND_CMN), '.')
               IF (SP_STRN_LEN(PVAL) .LE. 0) THEN
                  PRM_CMN(IND_CMN) = BUF(1:SP_STRN_LEN(BUF))
               ELSE
                  PRM_CMN(IND_CMN) = BUF(1:SP_STRN_LEN(BUF))
     &                            // ','
     &                            // PVAL(1:SP_STRN_LEN(PVAL))
               ENDIF
               !!!IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))
               IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
            ELSEIF (EST_FNC) THEN
               WRITE(BUF, '(2A,I12)') 'H', ',', HCLS
               IERR = IC_ICMD_EXT2INT(BUF)
               HDL_CMN(IND_CMN) = 0
               FNC_CMN(IND_CMN) = ' '
               IF (SP_STRN_LEN(PVAL) .LE. 0) THEN
                  PRM_CMN(IND_CMN) = BUF(1:SP_STRN_LEN(BUF))
               ELSE
                  PRM_CMN(IND_CMN) = BUF(1:SP_STRN_LEN(BUF))
     &                            // ','
     &                            // PVAL(1:SP_STRN_LEN(PVAL))
               ENDIF
               IERR = IC_ICMD_INT2EXT(PRM_CMN(IND_CMN))
               IERR = SO_FUNC_CBFNC0(HFNC, IC_ICMD_XEQFNC_CB)
            ENDIF
         ENDIF

C---        Décode le retour
         IF (ERR_GOOD()) THEN
            RVAL = PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
            IERR = IC_ICMD_EXT2INT(RVAL)
         ENDIF

C---     Boucle tant que le type de retour est IC_PILE_TYP_XVALEUR
      IF (ERR_GOOD())THEN
         IF (IC_PILE_VALTYP(RVAL) .EQ. IC_PILE_TYP_XVALEUR) THEN
            I1 = IC_PILE_VALDEB(RVAL)
            I2 = IC_PILE_VALFIN(RVAL)
            IF (I1 .GT. I2) GOTO 9900
            READ (RVAL(I1:I2), *, END=9900, ERR=9900) HCLS
            GOTO 100
         ENDIF
      ENDIF

      IND_CMN = IND_CMN - 1
D     CALL ERR_ASR(IND_CMN .GE. 0)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_RETOUR_FNC_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VALEUR', ': ',
     &             PRM_CMN(IND_CMN)(1:SP_STRN_LEN(PRM_CMN(IND_CMN)))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQFNCDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Call back de l'appel de fonction.
C
C Description:
C     La fonction privée IC_ICMD_XEQFNC_CB est le call-back de
C     IC_ICMD_XEQFNC et permet de résoudre l'appel à la fonction.
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C
C Entrée:
C     FNC   Fonction à exécuter
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQFNC_CB(FNC)

      IMPLICIT NONE

      INTEGER  IC_ICMD_XEQFNC_CB
      INTEGER  FNC
      EXTERNAL FNC

      INCLUDE 'err.fi'

      INTEGER IERR

      INTEGER          SIZ_CMN
      PARAMETER        (SIZ_CMN = 3)
      INTEGER          IND_CMN
      INTEGER          HDL_CMN
      CHARACTER*(64)   FNC_CMN
      CHARACTER*(1024) PRM_CMN
      COMMON /IC_ICMD_XEQFNC_CMN/ IND_CMN,
     &                            HDL_CMN(SIZ_CMN),
     &                            FNC_CMN(SIZ_CMN),
     &                            PRM_CMN(SIZ_CMN)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IND_CMN .GE. 0)
D     CALL ERR_PRE(IND_CMN .LE. SIZ_CMN)
C-----------------------------------------------------------------------

      IF (HDL_CMN(IND_CMN) .NE. 0) THEN
         IERR = FNC(HDL_CMN(IND_CMN),FNC_CMN(IND_CMN),PRM_CMN(IND_CMN))
      ELSE
         IERR = FNC(PRM_CMN(IND_CMN))
      ENDIF

      IC_ICMD_XEQFNC_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Pré-traitement à l'exécution de la pile
C
C Description:
C     La fonction privée IC_ICMD_PRXPIL pré-traite la pile pour résoudre les
C     mots-clefs qui sont identifiées comme des variables.
C
C Entrée:
C     INTEGER HOBJ               Handle sur l'objet courant
C     INTEGER HXST               La pile d'exécution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_PRXPIL(HOBJ, HXST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXST

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITYP
      CHARACTER*(256) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_HVALIDE(HXST))
C-----------------------------------------------------------------------

C---     Pour les mots clefs tout seuls,
C        ajoute un param vide et un appel de fonction
      IF (IC_PILE_FRSIZ(HXST) .EQ. 1) THEN
         IERR = IC_PILE_FRTOP (HXST, VAL)
         ITYP = IC_PILE_VALTYP(VAL)
         IF (IC_PILE_ESTKW(ITYP)) THEN
            IERR = IC_PILE_PUSHE(HXST, ' ', IC_PILE_TYP_SVALEUR)
            IERR = IC_PILE_PUSHE(HXST, ' ', IC_PILE_TYP_OPPAR)
         ENDIF
      ENDIF

      IC_ICMD_PRXPIL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute la pile
C
C Description:
C     La fonction privée IC_ICMD_XEQPIL traverse la pile en exécutant les
C     commandes.
C
C Entrée:
C     INTEGER       HOBJ                  Handle sur l'objet courant
C     INTEGER       HXST                  Handle sur eXecution STack
C
C Sortie:
C     CHARACTER      RSLT                 Le résultat
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQPIL(HOBJ, HXST, RSLT)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXST
      CHARACTER*(*) RSLT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER OPRTYP
      CHARACTER*(1024) OPR, RHS, LHS
      LOGICAL ENDLOOP
      LOGICAL LSKIP
C-----------------------------------------------------------------------
      LOGICAL DOXEQ, SKPSTR
      INTEGER H
      DOXEQ (H) = BTEST(IC_ICMD_STS(H-IC_ICMD_HBASE),IC_ICMD_STS_XEQ)
      SKPSTR(H) = BTEST(IC_ICMD_STS(H-IC_ICMD_HBASE),IC_ICMD_STS_SKPSTR)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_HVALIDE(HXST))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Ne saute pas les mots de contrôle
      LSKIP = .NOT. DOXEQ(HOBJ)
      IF (LSKIP) THEN      ! en mode skip
         IF (IC_PILE_FRSIZ(HXST) .GE. 3) THEN
            IERR = IC_PILE_FRBSE(HXST, OPR)
            OPRTYP = IC_PILE_VALTYP(OPR)
            IF (IC_PILE_ESTKW(OPRTYP)) THEN            ! mot clef
               IERR = IC_PILE_VALDEC(OPR, OPRTYP)
               IF (IC_ICMD_ESTCW(OPR)) THEN            ! mot de contrôle
                  IF (IC_ICMD_ESTIF   (OPR) .OR.
     &                IC_ICMD_ESTELIF (OPR) .OR.
     &                IC_ICMD_ESTWHILE(OPR)) THEN
                     IERR=IC_PILE_FRCLR(HXST)
                     IERR=IC_PILE_PUSHE(HXST, OPR, OPRTYP)
                     IERR=IC_PILE_PUSHE(HXST, 'F', IC_PILE_TYP_LVALEUR)
                     IERR=IC_PILE_PUSHE(HXST, ' ', IC_PILE_TYP_OPPAR)
                  ELSEIF (SKPSTR(HOBJ)) THEN
                     IERR=IC_PILE_FRCLR(HXST)
                     IERR=IC_PILE_PUSHE(HXST, OPR, OPRTYP)
                     IERR=IC_PILE_PUSHE(HXST, ' ', IC_PILE_TYP_SVALEUR)
                     IERR=IC_PILE_PUSHE(HXST, ' ', IC_PILE_TYP_OPPAR)
                  ENDIF
                  LSKIP = .FALSE.
               ENDIF
            ENDIF
         ENDIF
      ENDIF
      IF (LSKIP) THEN
         IERR = IC_PILE_FRCLR(HXST)
         GOTO 9999
      ENDIF

C---     Exécute la pile
      ENDLOOP = .FALSE.
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (ENDLOOP)   GOTO 199

C---        Prochaine instruction
         IF (ERR_GOOD())
     &      IERR = IC_PILE_FROPDEB(HXST, OPR, RHS, LHS)

C---        Switch sur le type d'opération
         OPRTYP = IC_PILE_VALTYP(OPR)
         IF     (IC_PILE_ESTNOOP(OPRTYP)) THEN
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTLBL(OPRTYP)) THEN
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTVAL(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, OPR, RSLT)
            IF (ERR_GOOD()) IERR = IC_ICMD_INT2EXT(RSLT)
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTVAR(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, OPR, RSLT)
            IF (ERR_GOOD()) IERR = IC_ICMD_INT2EXT(RSLT)
            ENDLOOP = .TRUE.

         ELSEIF (IC_PILE_ESTOPU(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_XEQOPU(HOBJ, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_PILE_FROPFIN(HXST, OPR)

         ELSEIF (IC_PILE_ESTOPB(OPRTYP)) THEN
            IF (ERR_GOOD()) IERR = IC_ICMD_XEQOPB(HOBJ, LHS, OPR, RHS)
            IF (ERR_GOOD()) IERR = IC_PILE_FROPFIN(HXST, OPR)

         ELSE
            GOTO 9900
         ENDIF

C---        Skip rest of statement
         IF (BTEST(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTA)) THEN
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTA)
            IERR = IC_PILE_FRCLR(HXST)
            ENDLOOP = .TRUE.
         ENDIF

      GOTO 100
199   CONTINUE

D     CALL ERR_ASR(ERR_BAD() .OR. IC_PILE_FRSIZ(HXST) .EQ. 0)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_OPERATION_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OPR', ': ', OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OP_ATTENDU', ': ',
     &                       '(noop, val, var, opu, opb)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OP_COURANT', ': ',
     &                       IC_PILE_TYP2TXT(OPRTYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQPIL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute une opération binaire.
C
C Description:
C     La fonction privée IC_ICMD_XEQOPB exécute une opération binaire.
C     Elle dispatch suivant l'opération à des fonctions plus spécialisées.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPB(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IRET
      INTEGER IT, LR
      INTEGER OPRTYP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     DISPATCH
      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF    (OPRTYP .EQ. IC_PILE_TYP_OPBRK) THEN
         IERR = IC_ICMD_XEQOPBBRK(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPDOT) THEN
         IERR = IC_ICMD_XEQOPBDOT(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPPAR) THEN
         IERR = IC_ICMD_XEQOPBPAR(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPASG) THEN
         IERR = IC_ICMD_XEQOPBASG(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPVRG) THEN
         IERR = IC_ICMD_XEQOPBVRG(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPADD .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPSUB .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPMUL .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPDIV .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPPOW) THEN
         IERR = IC_ICMD_XEQOPBARI(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLEQ .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPLNE .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPLGT .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPLGE .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPLLT .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPLLE) THEN
         IERR = IC_ICMD_XEQOPBCMP(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPAND .OR.
     &        OPRTYP .EQ. IC_PILE_TYP_OPOR) THEN
         IERR = IC_ICMD_XEQOPBLGL(HOBJ, LHSVAL, OPRVAL, RHSVAL)
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_OPB_INVALIDE')
      ENDIF

C---     COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') '#<3>#MSG_OPERATEUR_BINAIRE'
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF, '(4A)') '#<3>#MSG_LHS', ': ''',
     &         LHSVAL(1:MIN(128,SP_STRN_LEN(LHSVAL))), ''''
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF, '(4A)') '#<3>#MSG_RHS', ': ''',
     &         RHSVAL(1:MIN(128,SP_STRN_LEN(RHSVAL))), ''''
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF, '(2A)') '#<3>#MSG_RHS', ': '
         LR = SP_STRN_LEN(RHSVAL)
         DO IT=1, SP_STRN_NTOK(RHSVAL(1:LR), ',')
            IRET = SP_STRN_TKS(RHSVAL(1:LR), ',', IT, OPRVAL)
            IERR = IC_ICMD_ERRVAL(ERR_BUF, OPRVAL)
            CALL ERR_AJT('#<6>#' // ERR_BUF)
         ENDDO
         OPRVAL = ' '
      ENDIF

      IC_ICMD_XEQOPB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBARI exécute une opération binaire
C     de type arithmétique.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBARI(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER OPRTYP
      INTEGER LLN, RLN
      CHARACTER*(1024) LHS
      CHARACTER*(1024) RHS
      CHARACTER*(1024) RES
      INTEGER LTP, RTP
      INTEGER LVI, RVI
      REAL*8  LVR, RVR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(LHS, LTP)
      IF (ERR_BAD()) GOTO 9999

      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF (OPRTYP .EQ. IC_PILE_TYP_OPADD) THEN
         IF (RTP .EQ. IC_PILE_TYP_RVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_RVALEUR) RTP = LTP
         IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
         IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
         IF (RTP .NE. LTP) GOTO 9902
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            LLN = SP_STRN_LEN(LHS)
            RLN = SP_STRN_LEN(RHS)
            IF (LLN .LE. 0) THEN
                IF (RLN .LE. 0) THEN
                    RES = ' '
                ELSE
                    RES = RHS(1:RLN)
                ENDIF
            ELSE
                IF (RLN .LE. 0) THEN
                    RES = LHS(1:LLN)
                ELSE
                    RES = LHS(1:LLN) // RHS(1:RLN)
                ENDIF
            ENDIF
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVI
            LVI = LVI + RVI
            WRITE(RES, '(I12)') LVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVR
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVR
            LVR = LVR + RVR
            WRITE(RES, '(1PE25.17E3)') LVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF

      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPSUB) THEN
         IF (RTP .EQ. IC_PILE_TYP_RVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_RVALEUR) RTP = LTP
         IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
         IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
         IF (RTP .NE. LTP) GOTO 9902
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVI
            LVI = LVI - RVI
            WRITE(RES, '(I12)') LVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVR
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVR
            LVR = LVR - RVR
            WRITE(RES, '(1PE25.17E3)') LVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF

      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPMUL) THEN
         IF (RTP .EQ. IC_PILE_TYP_RVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_RVALEUR) RTP = LTP
         IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
         IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
         IF (RTP .NE. LTP) GOTO 9902
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVI
            LVI = LVI * RVI
            WRITE(RES, '(I12)') LVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVR
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVR
            LVR = LVR * RVR
            WRITE(RES, '(1PE25.17E3)') LVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF

      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPDIV) THEN
         IF (RTP .EQ. IC_PILE_TYP_RVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &       LTP .EQ. IC_PILE_TYP_RVALEUR) RTP = LTP
         IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
         IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
         IF (RTP .NE. LTP) GOTO 9902
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVI
            LVI = LVI / RVI
            WRITE(RES, '(I12)') LVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVR
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9905,END=9905) RVR
            LVR = LVR / RVR
            WRITE(RES, '(1PE25.17E3)') LVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF

      ELSE
         GOTO 9902
      ENDIF

      IF (SP_STRN_LEN(RES) .GT. LEN(OPRVAL)) GOTO 9906
      IF (ERR_GOOD()) IERR = IC_PILE_VALENC(RES, RTP)
      IF (ERR_GOOD()) OPRVAL = RES(1:SP_STRN_LEN(RES))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(5A)') 'ERR_OPER_INVALIDE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL)),
     &                       OPRVAL(1:SP_STRN_LEN(OPRVAL)),
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  CALL SP_STRN_CLP(LHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_LHS_INVALIDE', ': ',
     &                       LHS(1:SP_STRN_LEN(LHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  CALL SP_STRN_CLP(RHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_RHS_INVALIDE', ': ',
     &                       RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  CALL SP_STRN_CLP(RES, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ': ',
     &                       RES(1:SP_STRN_LEN(RES))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBARI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBASG exécute une opération binaire
C     de type assignation.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBASG(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
      IF (ERR_GOOD()) IERR = IC_ICMD_ASGSMB(HOBJ, LHSVAL, RHS)
      IF (ERR_BAD()) GOTO 9999

      IF (SP_STRN_LEN(LHSVAL) .GT. LEN(OPRVAL)) GOTO 9901
      OPRVAL = LHSVAL(1:SP_STRN_LEN(LHSVAL))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VARIABLE_ATTENDUE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  CALL SP_STRN_CLP(RHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ': ',
     &                       RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBASG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'opération binaire '[]'
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBBRK exécute l'opération binaire
C     '[]' d'accès à un indice.
C     <p>
C     On ne fait que concaténer les chaînes et déclarer le résultat comme
C     étant de type variable. L'exploitation du résultat est différée. Elle
C     se fera dans les fonctions d'accès aux variables pour couvrir les
C     cas d'assignation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBBRK(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER LHSTYP, RHSTYP
      CHARACTER*(1024) LHS, RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_VALTYP(OPRVAL) .EQ. IC_PILE_TYP_OPBRK)
D     CALL ERR_PRE(SP_STRN_LEN(LHSVAL) .GT. 0)
C-----------------------------------------------------------------------

C---     Résous le RHS
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RES)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RES, RHSTYP)
      IF (.NOT. IC_PILE_ESTVAL(RHSTYP)) GOTO 9900

C---     Résous le LHS
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(LHS, LHSTYP)

C---     Monte l'expression
      IF (ERR_GOOD()) THEN
         LHS = LHS(1:SP_STRN_LEN(LHS)) //
     &         '[' // RES(1:SP_STRN_LEN(RES)) // ']'
         IF (SP_STRN_LEN(LHS) .GT. LEN(OPRVAL)) GOTO 9901
      ENDIF

C---     Assigne le type
      IF (ERR_GOOD()) THEN
         IF (IC_ICMD_DOPARS(HOBJ)) THEN
            IERR = IC_PILE_VALENC(LHS, IC_PILE_TYP_ZVALEUR)
         ELSE
            IERR = IC_PILE_VALENC(LHS, LHSTYP)
         ENDIF
         RES = LHS(1:SP_STRN_LEN(LHS))
      ENDIF

      IF (ERR_GOOD()) OPRVAL = RES(1:SP_STRN_LEN(RES))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VALEUR_ATTENDUE', ': ',
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  CALL SP_STRN_CLP(RES, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ': ',
     &                       RES(1:SP_STRN_LEN(RES))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBBRK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBCMP exécute une opération binaire
C     de type comparaison.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBCMP(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER OPRTYP
      CHARACTER*(1024) LHS
      CHARACTER*(1024) RHS
      INTEGER LTP, RTP
      INTEGER LVI, RVI
      REAL*8  LVR, RVR
      LOGICAL LVL, RVL, RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Résous les variables
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(LHS, LTP)
      IF (ERR_BAD()) GOTO 9999

C---     Promotion de Integer en Real*8 au  besoin
      IF (RTP .EQ. IC_PILE_TYP_RVALEUR .AND.
     &    LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
      IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &    LTP .EQ. IC_PILE_TYP_RVALEUR) RTP = LTP
C---     Promotion de Integer en Handle au  besoin
      IF (RTP .EQ. IC_PILE_TYP_HVALEUR .AND.
     &    LTP .EQ. IC_PILE_TYP_IVALEUR) LTP = RTP
      IF (RTP .EQ. IC_PILE_TYP_IVALEUR .AND.
     &    LTP .EQ. IC_PILE_TYP_HVALEUR) RTP = LTP
C---     Promotion des zvaleurs
      IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
      IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
      IF (RTP .NE. LTP) GOTO 9902

C---     Décode les valeurs
      IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
C        RHS = RHS
      ELSEIF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
         READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVL   !!!!
      ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
         READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVI
      ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
         READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVR
      ELSEIF (RTP .EQ. IC_PILE_TYP_HVALEUR) THEN
         READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVI
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
C        PASS
      ELSE
         GOTO 9900
      ENDIF

      IF (LTP .EQ. IC_PILE_TYP_SVALEUR) THEN
C        LHS = LHS
      ELSEIF (LTP .EQ. IC_PILE_TYP_LVALEUR) THEN
         READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVL !!!!
      ELSEIF (LTP .EQ. IC_PILE_TYP_IVALEUR) THEN
         READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
      ELSEIF (LTP .EQ. IC_PILE_TYP_RVALEUR) THEN
         READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVR
      ELSEIF (LTP .EQ. IC_PILE_TYP_HVALEUR) THEN
         READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVI
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
C        PASS
      ELSE
         GOTO 9900
      ENDIF

C---     ==
      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF (OPRTYP .EQ. IC_PILE_TYP_OPLEQ) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .EQ. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
            RES = (LVI .EQ. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .EQ. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .EQ. RVR)
         ELSEIF (RTP .EQ. IC_PILE_TYP_HVALEUR) THEN
            RES = (LVI .EQ. RVI)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

C---     !=
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLNE) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .NE. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
            RES = (LVI .NE. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .NE. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .NE. RVR)
         ELSEIF (RTP .EQ. IC_PILE_TYP_HVALEUR) THEN
            RES = (LVI .NE. RVI)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

C---     >
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLGT) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .GT. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .GT. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .GT. RVR)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

C---     >=
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLGE) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .GE. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .GE. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .GE. RVR)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

C---     <
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLLT) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .LT. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .LT. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .LT. RVR)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

C---     <=
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLLE) THEN
         IF (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            RES = LHS(1:SP_STRN_LEN(LHS)) .LE. RHS(1:SP_STRN_LEN(RHS))
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            RES = (LVI .LE. RVI)
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            RES = (LVR .LE. RVR)
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = .TRUE.
         ELSE
            GOTO 9902
         ENDIF

      ELSE
         GOTO 9902
      ENDIF


      WRITE(OPRVAL,'(L2)',ERR=9906) RES
      IERR = IC_PILE_VALENC(OPRVAL, IC_PILE_TYP_LVALEUR)
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(5A)') 'ERR_OPER_INVALIDE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL)),
     &                       OPRVAL(1:SP_STRN_LEN(OPRVAL)),
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  CALL SP_STRN_CLP(RHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_RHS_INVALIDE', ': ',
     &                       RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  CALL SP_STRN_CLP(LHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_LHS_INVALIDE', ': ',
     &                       LHS(1:SP_STRN_LEN(LHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBCMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'opération binaire '.'
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBDOT exécute l'opération binaire
C     '.' d'accès à une composante.
C     <p>
C     On ne fait que concaténer les chaînes et déclarer le résultat comme
C     étant de type variable. L'exploitation du résultat est différée. Elle
C     se fera dans les fonctions d'accès aux variables pour couvrir les
C     cas d'assignation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBDOT(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER LHSTYP, RHSTYP
      CHARACTER*(1024) LHS, RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_VALTYP(OPRVAL) .EQ. IC_PILE_TYP_OPDOT)
D     CALL ERR_PRE(SP_STRN_LEN(RHSVAL) .GT. 0)
D     CALL ERR_PRE(SP_STRN_LEN(LHSVAL) .GT. 0)
C-----------------------------------------------------------------------

C---     Décode et contrôle le RHS
      RES = RHSVAL(1:SP_STRN_LEN(RHSVAL))
      IERR = IC_PILE_VALDEC(RES, RHSTYP)
      IF (.NOT.      ! Mots clefs utilisées comme méthodes
     &    ((RHSTYP .EQ. IC_PILE_TYP_VARIABLE) .OR.
     &     (RHSTYP .EQ. IC_PILE_TYP_MOTCLEF .AND. RES .EQ. 'help') .OR.
     &     (RHSTYP .EQ. IC_PILE_TYP_MOTCLEF .AND. RES .EQ. 'print')))
     &   GOTO 9900

C---     Résous le LHS
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(LHS, LHSTYP)

C---     Monte l'expression
      IF (ERR_GOOD()) THEN
         LHS = LHS(1:SP_STRN_LEN(LHS)) //
     &         '.' //
     &         RES(1:SP_STRN_LEN(RES))
         IF (SP_STRN_LEN(LHS) .GT. LEN(OPRVAL)) GOTO 9901
      ENDIF

C---     Assigne le type
      IF (ERR_GOOD()) THEN
         IF (IC_ICMD_DOPARS(HOBJ)) THEN
            IERR = IC_PILE_VALENC(LHS, IC_PILE_TYP_ZVALEUR)
         ELSE
            IERR = IC_PILE_VALENC(LHS, LHSTYP)
         ENDIF
         RES = LHS(1:SP_STRN_LEN(LHS))
      ENDIF

      IF (ERR_GOOD()) OPRVAL = RES(1:SP_STRN_LEN(RES))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_RHS_VARIABLE_ATTENDUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_NOM : ', RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TYPE : ', IC_PILE_TYP2TXT(RHSTYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  CALL SP_STRN_CLP(RES, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ': ',
     &                       RES(1:SP_STRN_LEN(RES))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBLGL exécute une opération binaire
C     de type logique.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBLGL(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER OPRTYP
      CHARACTER*(1024) LHS
      CHARACTER*(1024) RHS
      INTEGER LTP, RTP
      LOGICAL LVL, RVL, RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Résous les variables
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(LHS, LTP)
      IF (ERR_BAD()) GOTO 9999

C---     Promotion des zvaleurs
      IF (RTP .EQ. IC_PILE_TYP_ZVALEUR) LTP = RTP
      IF (LTP .EQ. IC_PILE_TYP_ZVALEUR) RTP = LTP
      IF (RTP .NE. LTP) GOTO 9902

C---     Décode les valeurs
      IF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
         READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVL
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
         RVL = .TRUE.
      ELSE
         GOTO 9902
      ENDIF

      IF (LTP .EQ. IC_PILE_TYP_LVALEUR) THEN
         READ (LHS(1:SP_STRN_LEN(LHS)),*,ERR=9904,END=9904) LVL
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
         LVL = .TRUE.
      ELSE
         GOTO 9902
      ENDIF

C---     OP
      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF (OPRTYP .EQ. IC_PILE_TYP_OPAND) THEN
         RES = (LVL .AND. RVL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPOR) THEN
         RES = (LVL .OR. RVL)
      ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
         RES = .TRUE.
      ELSE
         GOTO 9902
      ENDIF

      WRITE(OPRVAL,'(L2)',ERR=9906) RES
      IERR = IC_PILE_VALENC(OPRVAL, IC_PILE_TYP_LVALEUR)
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(5A)') 'ERR_OPER_INVALIDE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL)),
     &                       OPRVAL(1:SP_STRN_LEN(OPRVAL)),
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  CALL SP_STRN_CLP(RHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_RHS_INVALIDE', ': ',
     &                       RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  CALL SP_STRN_CLP(LHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_LHS_INVALIDE', ': ',
     &                       LHS(1:SP_STRN_LEN(LHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBLGL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'opération binaire '()'
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBPAR exécute l'opération binaire
C     '()' d'appel de fonction.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBPAR(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER I1, I2
      INTEGER LHSTYP
      LOGICAL DECODESMB
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_VALTYP(OPRVAL) .EQ. IC_PILE_TYP_OPPAR)
D     CALL ERR_PRE(SP_STRN_LEN(LHSVAL) .GT. 0)
C-----------------------------------------------------------------------

      LHSTYP = IC_PILE_VALTYP(LHSVAL)
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (.NOT. IC_PILE_ESTVAR   (LHSTYP) .AND.
     &       .NOT. IC_PILE_ESTVAL   (LHSTYP) .AND.
     &       .NOT. IC_PILE_ESTKW    (LHSTYP))  GOTO 9900
      ELSE
         IF (.NOT. IC_PILE_ESTVAR   (LHSTYP) .AND.
     &       .NOT. IC_PILE_ESTVALHDL(LHSTYP) .AND.
     &       .NOT. IC_PILE_ESTKW    (LHSTYP))  GOTO 9900
      ENDIF

      I1 = IC_PILE_VALDEB(LHSVAL)
      I2 = IC_PILE_VALFIN(LHSVAL)

C---          !!!!!!! GROSSE VERRUE !!!!!!!!!!!
C        On traite ici les mots-clef pour lesquels
C        on ne doit pas traduire les variables
      DECODESMB = .TRUE.
      IF (IC_PILE_ESTKW(LHSTYP)) THEN
         IF (LHSVAL(I1:I2) .EQ. 'exist') DECODESMB = .FALSE.
         IF (LHSVAL(I1:I2) .EQ. 'goto')  DECODESMB = .FALSE.
         IF (LHSVAL(I1:I2) .EQ. 'help')  DECODESMB = .FALSE.
      ENDIF

      IF (DECODESMB) THEN
         IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
         IF (ERR_BAD()) GOTO 9999
      ELSE
         RHS = RHSVAL
      ENDIF

      IERR = IC_ICMD_XEQFNC(HOBJ,
     &                      OPRVAL,
     &                      RHS(1:SP_STRN_LEN(RHS)),
     &                      LHSVAL(I1:I2))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VARIABLE_ATTENDUE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBPAR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_ICMD_XEQOPBARI exécute une opération binaire
C     de type arithmétique.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LHSVAL      Valeur du membre de gauche
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPBVRG(HOBJ, LHSVAL, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) LHSVAL
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER LTOT
      INTEGER OPRTYP
      CHARACTER*(1024) LHS
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF (OPRTYP .EQ. IC_PILE_TYP_OPVRG) THEN

         IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, LHSVAL, LHS)
         IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)

         IF (ERR_GOOD()) THEN
            LTOT = SP_STRN_LEN(LHS)+SP_STRN_LEN(RHS)+1
            IF (LEN(OPRVAL) .LT. LTOT) GOTO 9901

            OPRVAL = LHS(1:SP_STRN_LEN(LHS)) // ',' //
     &               RHS(1:SP_STRN_LEN(RHS))
         ENDIF
      ELSE
         GOTO 9900
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(5A)') 'ERR_OPER_INVALIDE', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL)),
     &                       OPRVAL(1:SP_STRN_LEN(OPRVAL)),
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  CALL SP_STRN_CLP(LHSVAL, 40)
      CALL SP_STRN_CLP(RHSVAL, 40)
      WRITE(ERR_BUF, '(5A)') 'ERR_DEBORDEMENT_ZONE_TAMPON', ': ',
     &                       LHSVAL(1:SP_STRN_LEN(LHSVAL)),
     &                       ',',
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQOPBVRG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute une opération unaire.
C
C Description:
C     La fonction privée IC_ICMD_XEQOPU exécute une opération unaire.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     RHSVAL      Valeur du membre de droite
C     RHSTYP      Type du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C     OPRTYP      Type du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPU(HOBJ, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER OPRTYP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     DISPATCH
      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF     (OPRTYP .EQ. IC_PILE_TYP_OPKW) THEN
         IERR = IC_ICMD_XEQOPUKW (HOBJ, OPRVAL, RHSVAL)
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPLST) THEN
         IERR = IC_ICMD_XEQOPULST(HOBJ, OPRVAL, RHSVAL)
      ELSE
         IERR = IC_ICMD_XEQOPUARI(HOBJ, OPRVAL, RHSVAL)
      ENDIF

      IC_ICMD_XEQOPU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute une opération unaire.
C
C Description:
C     La fonction privée IC_ICMD_XEQOPUKW exécute une opération unaire.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPUKW(HOBJ, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER RTP
      CHARACTER*(1024) RHS
      CHARACTER*(1024) RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_VALTYP(OPRVAL) .EQ. IC_PILE_TYP_OPKW)
C-----------------------------------------------------------------------

      RHS = RHSVAL(1:SP_STRN_LEN(RHSVAL))

      IERR = IC_PILE_VALDEC(RHS, RTP)
D     CALL ERR_ASR(RTP .EQ. IC_PILE_TYP_MOTCLEF)

      IERR = IC_ICMD_XEQFNC(HOBJ, RES, ' ', RHS(1:SP_STRN_LEN(RHS)))

      IC_ICMD_XEQOPUKW = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute une opération unaire de construction de liste.
C
C Description:
C     La fonction privée IC_ICMD_XEQOPULST exécute une opération unaire
C     de construction de liste.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPULST(HOBJ, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      CHARACTER*(1024) RES
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_PILE_VALTYP(OPRVAL) .EQ. IC_PILE_TYP_OPLST)
C-----------------------------------------------------------------------

C---     Sous-traite à la classe list
      IERR = IC_ICMD_XEQFNC(HOBJ, RES, RHSVAL, 'list')

      IC_ICMD_XEQOPULST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute une opération unaire arithmétique ou logique.
C
C Description:
C     La fonction privée IC_ICMD_XEQOPUARI exécute une opération unaire
C     arithmétique ou logique.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     RHSVAL      Valeur du membre de droite
C
C Sortie:
C     OPRVAL      Valeur du résultat de l'opération
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQOPUARI(HOBJ, OPRVAL, RHSVAL)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPRVAL
      CHARACTER*(*) RHSVAL

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER OPRTYP
      CHARACTER*(1024) RHS
      CHARACTER*(1024) RES
      REAL*8  RVR
      INTEGER RTP
      INTEGER RVI
      LOGICAL RVL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Décode le terme de droite
      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, RHSVAL, RHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)
      IF (ERR_BAD()) GOTO 9999

C---     Exécute l'opération unaire
      OPRTYP = IC_PILE_VALTYP(OPRVAL)
      IF     (OPRTYP .EQ. IC_PILE_TYP_OPPOS) THEN
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVI
            WRITE(RES, '(I12)') RVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVR
            WRITE(RES, '(1PE25.17E3)') RVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPNEG) THEN
         IF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVI
            RVI = - RVI
            WRITE(RES, '(I12)') RVI
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVR
            RVR = - RVR
            WRITE(RES, '(1PE25.17E3)') RVR
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF
      ELSEIF (OPRTYP .EQ. IC_PILE_TYP_OPNOT) THEN
         IF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
            READ (RHS(1:SP_STRN_LEN(RHS)),*,ERR=9903,END=9903) RVL
            RVL = .NOT. RVL
            WRITE(RES, '(L2)') RVL
         ELSEIF (IC_ICMD_DOPARS(HOBJ)) THEN
            RES = IC_PILE_VALTAG(RTP)
         ELSE
            GOTO 9902
         ENDIF
      ELSE
         GOTO 9902
      ENDIF

      IF (SP_STRN_LEN(RES) .GT. LEN(OPRVAL)) GOTO 9906
      IF (ERR_GOOD()) IERR = IC_PILE_VALENC(RES, RTP)
      IF (ERR_GOOD()) OPRVAL = RES(1:SP_STRN_LEN(RES))
D     CALL LOG_DEBUG('op_res: ' // OPRVAL(1:SP_STRN_LEN(OPRVAL)))

      GOTO 9999
C-----------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(4A)') 'ERR_OPER_INVALIDE', ': ',
     &                       OPRVAL(1:SP_STRN_LEN(OPRVAL)),
     &                       RHSVAL(1:SP_STRN_LEN(RHSVAL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  CALL SP_STRN_CLP(RHS, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_VALEUR_INVALIDE', ': ',
     &                       RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  CALL SP_STRN_CLP(RES, 80)
      WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ': ',
     &                       RES(1:SP_STRN_LEN(RES))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE    ! COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') '#<3>#MSG_OPERATEUR_UNAIRE'
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF, '(3A)') '#<3>#MSG_RHS', ': ',
     &         RHSVAL(1:MIN(128,SP_STRN_LEN(RHSVAL)))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      IC_ICMD_XEQOPUARI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'stop'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWSTOP exécute le mot clef
C     <code>stop</code>. Si ITYP est un handle, on appelle la méthode .help
C     sur ce handle. Sinon, on affiche toutes les commandes disponibles.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWSTOP(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_CMD_STOP')

      IC_ICMD_XEQKWSTOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'goto'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWGOTO exécute le mot clef
C     <code>goto</code>.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IFNC     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWGOTO(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITYP
      CHARACTER*(256) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Contrôle qu'on a bien un label
      ITYP = IC_PILE_VALTYP(IPRM)
      IF (ITYP .NE. IC_PILE_TYP_VARIABLE) GOTO 9900
      IERR = IC_ICMD_REQSMB(HOBJ, IPRM(1:SP_STRN_LEN(IPRM)), VAL)
      IF (ERR_GOOD()) THEN
         GOTO 9900
      ELSE
         CALL ERR_RESET()
      ENDIF

! récupérer le label
!  le label est-il une string ou une variable
!  même pblm que help
! trouver le label dans la pile
! remplacer le fichier top

      CALL LOG_TODO('IC_ICMD_XEQKWGOTO: EXPERIMENTAL !!!')
      CALL LOG_ECRIS(IPRM(1:SP_STRN_LEN(IPRM)))
      WRITE(LOG_BUF, *) 'IC_PILE_VALTYP(IPRM) = ', IC_PILE_VALTYP(IPRM)
      CALL LOG_ECRIS(LOG_BUF)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TYPE', ': ', IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_ATTENDU: (lbl)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWGOTO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'help'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWHELP exécute le mot clef
C     <code>help</code>. Si ITYP est un handle, on appel la méthode .help
C     sur ce handle. Sinon, on affiche toutes les commandes disponibles.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWHELP(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB, IC
      INTEGER HDIC, HGLB, HCLS, HMTH, HDOT
      INTEGER IT, I1, I2, IDUM
      INTEGER NTOK
      CHARACTER*(1024) TMP, TMP1
      CHARACTER*( 256) OVAL
      CHARACTER*(  16) FHLP
      LOGICAL FOOTER
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (IC_ICMD_DOPARS(HOBJ)) GOTO 9999

C---     Tente de décoder les variables
      IERR = IC_ICMD_REQSMB (HOBJ, IPRM, TMP)
      IF (.NOT. ERR_GOOD()) THEN
         TMP = IPRM(1:SP_STRN_LEN(IPRM))
         CALL ERR_RESET()
      ENDIF

C---     Type et indice
      IT = IC_PILE_VALTYP(TMP)
      I1 = IC_PILE_VALDEB(TMP)
      I2 = IC_PILE_VALFIN(TMP)

C---     Encode la commande comme string
      FHLP = 'help'
      IERR = IC_PILE_VALENC(FHLP, IC_PILE_TYP_SVALEUR)

C---     Les modules, les classes
      FOOTER = .FALSE.
      IF (IC_PILE_ESTVALHDL(IT)) THEN
         IERR = IC_ICMD_XEQFNC(HOBJ,
     &                         OVAL,
     &                         ' ',
     &                         TMP(I1:I2) // '.help')
C---     Les variables, traitées comme des fonctions potentielles
      ELSEIF (IC_PILE_ESTVAR(IT)) THEN
         IERR = IC_ICMD_XEQFNC(HOBJ,
     &                         OVAL,
     &                         FHLP(1:SP_STRN_LEN(FHLP)),
     &                         TMP(I1:I2))
C---     Les chaînes, traitées comme des fonctions potentielles
      ELSEIF (IT .EQ. IC_PILE_TYP_SVALEUR .AND. I2 .GE. I1) THEN
         IERR = IC_ICMD_XEQFNC(HOBJ,
     &                         OVAL,
     &                         FHLP(1:SP_STRN_LEN(FHLP)),
     &                         TMP(I1:I2))
C---     Les valeurs non nulles
      ELSEIF (IC_PILE_ESTVAL(IT) .AND. IT .NE. IC_PILE_TYP_0VALEUR) THEN
         IERR = IC_ICMD_ERRVAL(LOG_BUF, IPRM)
         CALL LOG_ECRIS(LOG_BUF)
         FOOTER = .TRUE.
      ELSE
         CALL LOG_ECRIS('List of available commands:')
         CALL LOG_INCIND()
         IOB  = HOBJ - IC_ICMD_HBASE
         HDIC = IC_ICMD_FNCDIC(IOB)

         CALL LOG_ECRIS('Key words:')
         CALL LOG_INCIND()
         HDIC = IC_ICMD_FNCDIC(IOB)
         DO IC=1,DS_MAP_REQDIM(HDIC)
            IERR = DS_MAP_REQCLF(HDIC, IC, TMP)
            IF (IC_ICMD_ESTKW(TMP(1:SP_STRN_LEN(TMP))))
     &         CALL LOG_ECRIS( TMP(1:SP_STRN_LEN(TMP)) )
         ENDDO
         CALL LOG_DECIND()

         CALL LOG_ECRIS('Modules:')
         CALL LOG_INCIND()
         HGLB = IC_ICMD_GLBDIC(IOB)
         HCLS = IC_ICMD_CLSDIC(IOB)
         DO IC=1,DS_MAP_REQDIM(HGLB)
            IERR = DS_MAP_REQCLF(HGLB, IC, TMP)
            IERR = DS_MAP_REQVAL(HGLB,
     &                           TMP(1:SP_STRN_LEN(TMP)),
     &                           OVAL)
            IF (IC_PILE_VALTYP(OVAL) .EQ. IC_PILE_TYP_MCONST) THEN
               TMP1 = OVAL(1:SP_STRN_LEN(OVAL))
               IF (DS_MAP_ESTCLF(HCLS, TMP1(1:SP_STRN_LEN(TMP1)))) THEN
                  IERR = DS_MAP_REQVAL(HCLS,
     &                                 TMP1(1:SP_STRN_LEN(TMP1)),
     &                                 OVAL)
                  READ(OVAL, *) HMTH, HDOT
                  IF (HMTH .EQ. 0)
     &               CALL LOG_ECRIS( TMP(1:SP_STRN_LEN(TMP)) )
               ENDIF
            ENDIF
         ENDDO
         CALL LOG_DECIND()

         CALL LOG_ECRIS('Classes:')
         CALL LOG_INCIND()
         HGLB = IC_ICMD_GLBDIC(IOB)
         HCLS = IC_ICMD_CLSDIC(IOB)
         DO IC=1,DS_MAP_REQDIM(HGLB)
            IERR = DS_MAP_REQCLF(HGLB, IC, TMP)
            IERR = DS_MAP_REQVAL(HGLB,
     &                           TMP(1:SP_STRN_LEN(TMP)),
     &                           OVAL)
            IF (IC_PILE_VALTYP(OVAL) .EQ. IC_PILE_TYP_MCONST) THEN
               TMP1 = OVAL(1:SP_STRN_LEN(OVAL))
               IF (DS_MAP_ESTCLF(HCLS, TMP1(1:SP_STRN_LEN(TMP1)))) THEN
                  IERR = DS_MAP_REQVAL(HCLS,
     &                                 TMP1(1:SP_STRN_LEN(TMP1)),
     &                                 OVAL)
                  READ(OVAL, *) HMTH, HDOT
                  IF (HMTH .NE. 0)
     &               CALL LOG_ECRIS( TMP(1:SP_STRN_LEN(TMP)) )
               ENDIF
            ENDIF
         ENDDO
         CALL LOG_DECIND()

         CALL LOG_ECRIS('Commands:')
         CALL LOG_INCIND()
         HDIC = IC_ICMD_FNCDIC(IOB)
         HGLB = IC_ICMD_GLBDIC(IOB)
         DO IC=1,DS_MAP_REQDIM(HDIC)
            IERR = DS_MAP_REQCLF(HDIC, IC, TMP)
            IF (.NOT. IC_ICMD_ESTKW(TMP(1:SP_STRN_LEN(TMP))) .AND.         ! not a keyword
     &          .NOT. DS_MAP_ESTCLF(HGLB, TMP(1:SP_STRN_LEN(TMP))) .AND.   ! not a class
     &          TMP(1:21) .NE. '#__dummy_placeholder__#__') THEN
                  CALL LOG_ECRIS( TMP(1:SP_STRN_LEN(TMP)) )
            ENDIF
         ENDDO
         CALL LOG_DECIND()

         FOOTER = .TRUE.
      END IF
      IPRM = ' '

      IF (FOOTER) THEN
         CALL LOG_DECIND()
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS('Type help(command) or help(''command'') ' //
     &                  'to get help on a command')
      ENDIF

9999  CONTINUE
      IC_ICMD_XEQKWHELP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'print'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWPRINT exécute le mot clef
C     <code>print</code>. Si ITYP est un handle, on appel la méthode .print
C     sur ce handle. Sinon, on affiche la valeur.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWPRINT(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITP
      INTEGER HCLS, HFNC
      LOGICAL XEQMTH
      CHARACTER*(256) OVAL
      CHARACTER*(1024)TMP
      
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (IC_ICMD_DOPARS(HOBJ)) GOTO 9999

C---     Détermine s'il s'agit d'une appel de méthode valide
      XEQMTH = .FALSE.
      IF (IC_PILE_ESTVALOK(IPRM)) THEN
         IERR = SP_STRN_TKS(IPRM, ',{', 1, TMP)
         IF (IC_PILE_VALTYP(TMP) .EQ. IC_PILE_TYP_HVALEUR) THEN
            IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(TMP, ITP)
            IF (ERR_GOOD()) IERR = IC_ICMD_REQHNDLCLS(HOBJ, TMP, HCLS)
            IF (ERR_GOOD()) IERR = IC_ICMD_REQHFNCCLS(HOBJ, HCLS, HFNC)
            IF (ERR_GOOD()) THEN
               XEQMTH = .TRUE.
            ELSE
               CALL ERR_RESET()
            ENDIF
         ENDIF
      ENDIF

C---     Traitement
      IF (XEQMTH) THEN
         TMP = IPRM(IC_PILE_VALDEB(IPRM):IC_PILE_VALFIN(IPRM))  ! g77 - var tmp
         IERR = IC_ICMD_XEQFNC(HOBJ,
     &                         OVAL,
     &                         ' ',
     &                         TMP(1:SP_STRN_LEN(TMP)) // '.print')
      ELSE
         IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, IPRM, TMP)
         IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(TMP, ITP)
         IF (ERR_GOOD()) CALL LOG_ECRIS(TMP(1:SP_STRN_LEN(TMP)))
      END IF
      IPRM = ' '

9999  CONTINUE
      IC_ICMD_XEQKWPRINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'int'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWINT exécute le mot clef
C     <code>int</code>. On retourne une chaîne qui représente la valeur
C     du paramètre.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWINT(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      REAL*8  RVAL
      INTEGER IERR, IRET
      INTEGER LRH, RTP
      INTEGER IVAL
      LOGICAL LVAL
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IVAL = 0

      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, IPRM, RHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)

      IF (ERR_GOOD()) THEN
         LRH = SP_STRN_LEN(RHS)

         IF     (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) IVAL
         ELSEIF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) LVAL
            IF (LVAL) IVAL = 1
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) IVAL
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) RVAL
            IVAL = INT(RVAL)
         ELSEIF (RTP .EQ. IC_PILE_TYP_ZVALEUR) THEN
            RHS = IC_PILE_VALTAG(RTP)
            LRH = SP_STRN_LEN(RHS)
            READ (RHS(1:LRH),*,ERR=9900,END=9900) IVAL
         ELSE
            GOTO 9901
         ENDIF

      ENDIF

      IF (ERR_GOOD()) WRITE(IPRM,'(A, I12)') 'I,', IVAL

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE_6056'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE    ! COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') '#<3>#MSG_KW_INT'
         CALL ERR_AJT(ERR_BUF)
         CALL SP_STRN_CLP(RHS, 40)
         WRITE(ERR_BUF, '(3A)') '#<3>#MSG_RHS', ': ',
     &         RHS(1:SP_STRN_LEN(RHS))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      IC_ICMD_XEQKWINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'float'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWFLOAT exécute le mot clef
C     <code>float</code>. On retourne une chaîne qui représente la valeur
C     du paramètre.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWFLOAT(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      REAL*8  RVAL
      INTEGER IERR, IRET
      INTEGER LRH, RTP
      LOGICAL LVAL
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      RVAL = 0.0D0

      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, IPRM, RHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)

      IF (ERR_GOOD()) THEN
         LRH = SP_STRN_LEN(RHS)

         IF     (RTP .EQ. IC_PILE_TYP_SVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) RVAL
         ELSEIF (RTP .EQ. IC_PILE_TYP_LVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) LVAL
            IF (LVAL) RVAL = 1.0D0
         ELSEIF (RTP .EQ. IC_PILE_TYP_IVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) RVAL
         ELSEIF (RTP .EQ. IC_PILE_TYP_RVALEUR) THEN
            READ (RHS(1:LRH),*,ERR=9900,END=9900) RVAL
         ELSEIF (RTP .EQ. IC_PILE_TYP_ZVALEUR) THEN
            RHS = IC_PILE_VALTAG(RTP)
            LRH = SP_STRN_LEN(RHS)
            READ (RHS(1:LRH),*,ERR=9900,END=9900) RVAL
         ELSE
            GOTO 9901
         ENDIF

      ENDIF

      IF (ERR_GOOD()) WRITE(IPRM,'(A, 1PE25.17E3)') 'R,', RVAL

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE_6143'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE    ! COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') '#<3>#MSG_KW_FLOAT'
         CALL ERR_AJT(ERR_BUF)
         CALL SP_STRN_CLP(RHS, 40)
         WRITE(ERR_BUF, '(3A)') '#<3>#MSG_RHS', ': ',
     &         RHS(1:SP_STRN_LEN(RHS))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      IC_ICMD_XEQKWFLOAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'str'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWSTR exécute le mot clef
C     <code>str</code>. On retourne une chaîne qui représente la valeur
C     du paramètre.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWSTR(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER RTP
      CHARACTER*(1024) RHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = IC_ICMD_REQSMB(HOBJ, IPRM, RHS)
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(RHS, RTP)
      IF (ERR_GOOD()) WRITE(IPRM,'(2A)') 'S,', RHS(1:SP_STRN_LEN(RHS))

      IC_ICMD_XEQKWSTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'exist'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWEXIST teste pour l'existence d'une
C     variable.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWEXIST(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITP
      CHARACTER*(256) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = IC_ICMD_REQSMB(HOBJ, IPRM(1:SP_STRN_LEN(IPRM)), VAL)
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(A, L2)') 'L,', .TRUE.
      ELSE
         WRITE(IPRM, '(A, L2)') 'L,', .FALSE.
         CALL ERR_RESET()
      ENDIF

      IC_ICMD_XEQKWEXIST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'del'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWDEL exécute le mot clef
C     <code>del</code>. Si ITYP est un handle, on appelle la méthode .del
C     sur ce handle, sinon on génère une erreur.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWDEL(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      CHARACTER*(256) OVAL
      CHARACTER*(1024)TMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IF (IC_ICMD_DOPARS(HOBJ)) GOTO 9999

      IF (IC_PILE_VALTYP(IPRM) .EQ. IC_PILE_TYP_HVALEUR) THEN
         TMP = IPRM(IC_PILE_VALDEB(IPRM):IC_PILE_VALFIN(IPRM))  ! g77 - var tmp
         IERR = IC_ICMD_XEQFNC(HOBJ,
     &                         OVAL,
     &                         ' ',
     &                         TMP(1:SP_STRN_LEN(TMP)) // '.del')
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_PARAMETRE_INVALIDE')
         CALL ERR_AJT('MSG_HANDLE_ATTENDU')
      END IF

9999  CONTINUE
      IC_ICMD_XEQKWDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'if'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWIF exécute le mot clef
C     <code>if</code>. En mode XEQ, si ITYP n'est pas un LOGICAL
C     on génère une erreur. Sinon, on pousse l'état actuel sur la
C     pile des IF et on met l'état actuel à la valeur de IPRM.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C     L'assertion sur la taille de la pile des if ne veut plus dire
C     grand chose car l'état XEQ est contrôlé par les if ET par les
C     fonctions de l'IC. Pour un if dans une fonction, la fonction met
C     l'état à SKIP sans toucher la pile des if.
C************************************************************************
      FUNCTION IC_ICMD_XEQKWIF(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB, ITP
      INTEGER HSCT
      LOGICAL LVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Pousse l'état courant sur la pile des structures de contrôle
      HSCT = IC_ICMD_SCTSTK(IOB)
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_STS(IOB))
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_SCT_IF)      ! Token de contrôle

C---     En mode PARSING, contrôle le type
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900

C---     En mode XEQ, assigne le nouveau mode
      ELSEIF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)) THEN
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900
         IERR = IC_PILE_VALDEC(IPRM, ITP)
         READ(IPRM, *, END=9900, ERR=9900) LVAL
         IF (LVAL) THEN
            IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         ELSE
            IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         ENDIF
         IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)

C---     Sinon, skip et SKIP_STRUCTURE pour sauter tout le bloc du if
      ELSE
D        CALL ERR_ASR(DS_LIST_REQDIM(HSCT) .GE. 1)
         IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)
      ENDIF

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE_6377'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWIF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'elif'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWELIF exécute le mot clef
C     <code>elif</code>. En mode XEQ, si ITYP n'est pas un logical
C     on génère une erreur. Sinon, on pousse l'état actuel sur la pile
C     des IF et on met l'état actuel à la valeur de IPRM.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWELIF(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITP
      LOGICAL LVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Partie du else
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
C        PASS
      ELSEIF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)) THEN
         IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)) THEN
D           CALL ERR_ASR(.FALSE.)
         ELSE
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTR)
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
         ENDIF
      ELSE
         IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)) THEN
C           PASS
         ELSE
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
         ENDIF
      ENDIF

C---     Partie du if
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900
      ELSEIF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)) THEN
C        PASS
      ELSE
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900
         IERR = IC_PILE_VALDEC(IPRM, ITP)
         READ(IPRM, *, END=9900, ERR=9900) LVAL
         IF (LVAL) THEN
            IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         ELSE
            IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         ENDIF
      ENDIF

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_EXPRESSION_LOGIQUE_ATTENDUE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWELIF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'else'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWELSE exécute le mot clef
C     <code>else</code>. On change l'état actuel.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWELSE(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

      IERR = IC_PILE_VALDEC(IPRM, ITP)
      IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

C---     Partie du else
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
C        PASS
      ELSEIF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)) THEN
         IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)) THEN
D           CALL ERR_ASR(.FALSE.)
         ELSE
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTR)
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
         ENDIF
      ELSE
         IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)) THEN
C           PASS
         ELSE
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
         ENDIF
      ENDIF

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWELSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'endif'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWENDIF exécute le mot clef
C     <code>endif</code>. On pop l'état de la pile des IF.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWENDIF(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITP
      INTEGER ITOK
      INTEGER HSCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Contrôles
      IERR = IC_PILE_VALDEC(IPRM, ITP)
      IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

C---     Pop la pile des if
      HSCT = IC_ICMD_SCTSTK(IOB)
      IF (DS_LIST_REQDIM(HSCT) .LT. 2) GOTO 9901
      IERR = DS_LIST_TOPI(HSCT, ITOK)              ! Le token de contrôle
      IERR = DS_LIST_POP (HSCT)
      IF (ITOK .NE. IC_ICMD_SCT_IF) GOTO 9902
      IERR = DS_LIST_TOPI(HSCT, IC_ICMD_STS(IOB))  ! Le status d'exécution
      IERR = DS_LIST_POP (HSCT)

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,I3))') 'MSG_DIM_INVALIDE',
     &                              ':', DS_LIST_REQDIM(HSCT), ' / ', 2
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_JETON_IF_ATTENDU'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWENDIF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'function'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWFNC exécute le mot clef
C     <code>function</code>. Elle n'est utilisée qu'en mode
C     parsing.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWFNC(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Pousse l'état courant sur la pile des structures de contrôle
      HSCT = IC_ICMD_SCTSTK(IOB)
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_STS(IOB))
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_SCT_FNC)      ! Token de contrôle

C---     En mode PARSING
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTA)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_PRSFNC)

C---     En mode XEQ
      ELSE
         IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTA)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)
      ENDIF

      IPRM = ' '

      IC_ICMD_XEQKWFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'endfunction'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWENDFNC exécute le mot clef
C     <code>endfunction</code>. Elle n'est utilisée qu'en mode
C     parsing.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWENDFNC(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITP, ITOK
      INTEGER HSCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

C---     Contrôles
      IERR = IC_PILE_VALDEC(IPRM, ITP)
      IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

C---     Pop la pile des if
      HSCT = IC_ICMD_SCTSTK(IOB)
      IF (DS_LIST_REQDIM(HSCT) .LT. 2) GOTO 9901
      IERR = DS_LIST_TOPI(HSCT, ITOK)              ! Le token de contrôle
      IERR = DS_LIST_POP (HSCT)
      IF (ITOK .NE. IC_ICMD_SCT_FNC) GOTO 9902
      IERR = DS_LIST_TOPI(HSCT, IC_ICMD_STS(IOB))  ! Le status d'exécution
      IERR = DS_LIST_POP (HSCT)

C---     Clear l'état de parse de fonction
      IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_PRSFNC)

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,I3))') 'MSG_DIM_INVALIDE',
     &                              ':', DS_LIST_REQDIM(HSCT), ' / ', 2
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_JETON_IF_ATTENDU'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWENDFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'eval'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWEVAL exécute le mot clef
C     <code>eval</code>.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IFNC     Nom de la fonction (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWEVAL(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER ITP, L
      CHARACTER*(1024) RSLT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Contrôles
      IERR = IC_PILE_VALDEC(IPRM, ITP)
      IF (ITP .NE. IC_PILE_TYP_SVALEUR) GOTO 9900
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9901

C---     Évalue l'expression
      IERR = IC_ICMD_EVAL(HOBJ, IPRM, RSLT)
      
C---     Encode le résultat
      IF (ERR_GOOD()) THEN
         L = SP_STRN_LEN(RSLT)
         IF (LEN(IPRM) .LT. L+2) GOTO 9902

         IF     (SP_STRN_INT(RSLT(1:L))) THEN
            WRITE(IPRM,'(2A)') 'I,', RSLT(1:L)
         ELSEIF (SP_STRN_DBL(RSLT(1:L))) THEN
            WRITE(IPRM,'(2A)') 'R,', RSLT(1:L)
         ELSE
            WRITE(IPRM,'(2A)') 'S,', RSLT(1:L)
         ENDIF
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_STRING_ATTENDUE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRE_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_VARIABLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWEVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'execfile'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWEXECF exécute le mot clef
C     <code>execfile</code>.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C     Il n'y a pas de différence entre import et execfile tant qu'il n'y a
C     pas déclarations.
C************************************************************************
      FUNCTION IC_ICMD_XEQKWEXECF(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IC_ICMD_XEQKWEXECF = IC_ICMD_XEQKWIMPORT(HOBJ, IFNC, IPRM)
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'for'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWFOR exécute le mot clef
C     <code>for</code>.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWFOR(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_ICMD_HBASE

      IPRM = ' '

      CALL LOG_TODO('kw for not implemented')
      CALL ERR_ASR(.FALSE.)

      IC_ICMD_XEQKWFOR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'import'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWIMPORT exécute le mot clef
C     <code>import</code>. Si ITYP n'est pas une string on génère une erreur.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWIMPORT(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITYP
      INTEGER HFST, HFNI
      LOGICAL DOSKIP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Contrôles
      IF (.NOT. IC_PILE_ESTVALOK(IPRM)) GOTO 9900
      IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_SVALEUR) GOTO 9900
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9901

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)

C---     Décode - extrait le nom du fichier
      IF (ERR_GOOD()) IERR = IC_PILE_VALDEC(IPRM, ITYP)

C---     Détecte les inclusions multiples
      DOSKIP = .FALSE.
      IF (ERR_GOOD()) THEN
         DOSKIP = IC_ICMD_DOPARS(HOBJ) .AND.
     &            IC_ICMD_ESTFNI(HOBJ,
     &               "##___" // IPRM(1:SP_STRN_LEN(IPRM)) // "__##")
      ENDIF

C---     Ouvre le fichier
      IF (ERR_GOOD() .AND. .NOT. DOSKIP) THEN
         IERR = IC_PFIC_PUSH(HFST,
     &                       IPRM(1:SP_STRN_LEN(IPRM)),
     &                       -1,
     &                       0)
      ENDIF

C---     Ajoute le fichier au dico des fonctions utilisateur
      IF (ERR_GOOD() .AND. .NOT. DOSKIP) THEN
         HFNI = IC_ICMD_FNIDIC(IOB)
         IERR = DS_MAP_ASGVAL(HFNI,
     &                   "##___" // IPRM(1:SP_STRN_LEN(IPRM)) // "__##",
     &                   "__multiple_import_protection__")
      ENDIF

      IPRM = ' '
      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_STRING_ATTENDUE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWIMPORT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'while'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWWHILE exécute le mot clef
C     <code>while</code>. En mode XEQ, si ITYP n'est pas un LOGICAL
C     on génère une erreur. Sinon, on pousse l'état actuel sur la
C     pile des structures de contrôle et on met l'état actuel à la
C     valeur de IPRM.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWWHILE(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IDUM, ITP
      INTEGER HSCT, HFST, HFIN
      INTEGER LC
      LOGICAL LVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)

C---     Le fichier et la position
      IERR = IC_PFIC_TOP(HFST, HFIN, IDUM)
      LC   = IC_FINP_REQLCMD(HFIN)

C---     Pousse l'état courant sur la pile des structures de contrôle
      HSCT = IC_ICMD_SCTSTK(IOB)
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_STS(IOB))
      IERR = DS_LIST_PUSHI(HSCT, LC)                     ! La ligne courante
      IERR = DS_LIST_PUSHI(HSCT, IC_ICMD_SCT_WHILE)      ! Token de contrôle

C---     En mode PARSING, contrôle le type
      IF (IC_ICMD_DOPARS(HOBJ)) THEN
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900

C---     En mode XEQ, assigne le nouveau mode
      ELSEIF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)) THEN
         IF (IC_PILE_VALTYP(IPRM) .NE. IC_PILE_TYP_LVALEUR) GOTO 9900
         IERR = IC_PILE_VALDEC(IPRM, ITP)
         READ(IPRM, *, END=9900, ERR=9900) LVAL
         IF (LVAL) THEN
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTR)
         ELSE
            IC_ICMD_STS(IOB)=IBCLR(IC_ICMD_STS(IOB),IC_ICMD_STS_XEQ)
            IC_ICMD_STS(IOB)=IBSET(IC_ICMD_STS(IOB),IC_ICMD_STS_SKPSTR)
         ENDIF

C---     Sinon, skip et SKIP_STRUCTURE pour sauter tout le bloc du if
      ELSE
D        CALL ERR_ASR(DS_LIST_REQDIM(HSCT) .GE. 1)
         IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_XEQ)
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_SKPSTR)
      ENDIF

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE_6940'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWWHILE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Exécute le mot-clef 'endwhile'
C
C Description:
C     La fonction privée IC_ICMD_XEQKWENDWHILE exécute le mot clef
C     <code>endwhile</code>. On pop l'état de la pile des structures.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IMTH     Nom de la méthode (ici vide)
C     IPRM     Paramètre
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XEQKWENDWHILE(HOBJ, IFNC, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) IFNC
      CHARACTER*(*) IPRM

      INCLUDE 'icicmd.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IDUM, ITP, ISTS, ITOK
      INTEGER HFST, HSCT, HFIN
      INTEGER LC
      CHARACTER*(256) NOMF
C-----------------------------------------------------------------------
      INTEGER I
      LOGICAL DOLOOP
      DOLOOP(I) = BTEST(ISTS, IC_ICMD_STS_XEQ) .AND.
     &            .NOT. BTEST(ISTS, IC_ICMD_STS_SKPSTR)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)
      HSCT = IC_ICMD_SCTSTK(IOB)

C---     Contrôles
      IERR = IC_PILE_VALDEC(IPRM, ITP)
      IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

C---     Etat courant
      ISTS = IC_ICMD_STS(IOB)

C---     Pop la pile des structures de contrôle
      IF (DS_LIST_REQDIM(HSCT) .LT. 3) GOTO 9901
      IF (ERR_GOOD()) IERR = DS_LIST_TOPI(HSCT, ITOK)              ! Le token de contrôle
      IF (ERR_GOOD()) IERR = DS_LIST_POP (HSCT)
      IF (ERR_GOOD()) IERR = DS_LIST_TOPI(HSCT, LC)                ! Le ligne
      IF (ERR_GOOD()) IERR = DS_LIST_POP (HSCT)
      IF (ERR_GOOD() .AND. ITOK .NE. IC_ICMD_SCT_WHILE) GOTO 9902
      IF (ERR_GOOD()) IERR = DS_LIST_TOPI(HSCT, IC_ICMD_STS(IOB))  ! Le status d'exécution
      IF (ERR_GOOD()) IERR = DS_LIST_POP (HSCT)

C---     Le fichier et la position
      IF (DOLOOP(ISTS)) THEN
         IF (ERR_GOOD()) IERR = IC_PFIC_TOP(HFST, HFIN, IDUM)
         IF (ERR_GOOD()) IERR = IC_FINP_REQNOM(HFIN, NOMF)
         IF (ERR_GOOD()) IERR = IC_FINP_INI   (HFIN, NOMF, LC-1)
!D     ELSE
!D        CALL ERR_ASR (BTEST(ISTS, IC_ICMD_STS_SKPSTR))
      ENDIF

      IPRM = ' '

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,I3))') 'MSG_DIM_INVALIDE',
     &                              ':', DS_LIST_REQDIM(HSCT), ' / ', 3
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_PILE_STRC_CTRL_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_JETON_WHILE_ATTENDU'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XEQKWENDWHILE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Extrais une fonction
C
C Description:
C     La fonction privée IC_ICMD_XTRFNC extrait les fonctions.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HFNC     Handle sur la fonction en cours de parsing
C     ILD      Ligne de début dans le fichier source
C     ILF      Ligne de fin dans le fichier source
C     CMDSTAT  Command statement
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XTRFNC(HOBJ, HFNC, ILD, ILF, CMDSTAT)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HFNC
      INTEGER ILD, ILF
      CHARACTER*(*) CMDSTAT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icfunc.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IDUM, IPOS
      INTEGER HFST, HFNI, HFIN
      CHARACTER*(256) NOM, ARGS
      CHARACTER*(256) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(HFNC .EQ. 0 .OR. IC_FUNC_HVALIDE(HFNC))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)
      HFNI = IC_ICMD_FNIDIC(IOB)

C---     Parsing de fonction
      IF (BTEST(IC_ICMD_STS(IOB), IC_ICMD_STS_PRSFNC)) GOTO 2000

C---     Nouvelle fonction
C        =================
      IF (SP_STRN_LEN(CMDSTAT) .LT. 12) GOTO 9999
      IF (CMDSTAT(1:9) .NE. 'function ') GOTO 9999
      IF (INDEX(CMDSTAT, '(') .LE. 0) GOTO 9999
      IF (INDEX(CMDSTAT, ')') .LE. 0) GOTO 9999
      IF (INDEX(CMDSTAT, ')') .LE. INDEX(CMDSTAT, '(')) GOTO 9999

C---     Enlève 'function '
      IF (SP_STRN_LEN(CMDSTAT) .GT. LEN(BUF)) GOTO 9900
      BUF = CMDSTAT(10:SP_STRN_LEN(CMDSTAT))
      CALL SP_STRN_TRM(BUF)

C---     Sépare le nom
      NOM = BUF(1:SP_STRN_LEN(BUF))
      IPOS = SP_STRN_LFT(NOM, '(')
      CALL SP_STRN_TRM(NOM)

C---     Sépare la liste des arguments
      ARGS = BUF(1:SP_STRN_LEN(BUF))
      IPOS = SP_STRN_RHT(ARGS, '(')
      IPOS = SP_STRN_LFT(ARGS, ')')
      CALL SP_STRN_TRM(ARGS)

C---     Contrôles
      IF (IC_ICMD_ESTFNC(HOBJ, NOM)) GOTO 9903
      IF (IC_ICMD_ESTFNI(HOBJ, NOM)) GOTO 9904
      IF (IC_ICMD_ESTKW(NOM)) GOTO 9905
      IF (IC_ICMD_ESTCW(NOM)) GOTO 9905
      IF (HFNC .NE. 0) GOTO 9906

C---     Le fichier
      IF (ERR_GOOD()) THEN
         IERR = IC_PFIC_TOP(HFST, HFIN, IDUM)
         IERR = IC_FINP_REQNOM (HFIN, BUF)
      ENDIF

C---     Crée un objet fonction
      IF (ERR_GOOD()) THEN
         IERR = IC_FUNC_CTR(HFNC)
         IERR = IC_FUNC_INI(HFNC, NOM, BUF, ARGS)
         IERR = IC_FUNC_INIWRITE(HFNC)
      ENDIF

C---     Ajoute la fonction au dico
      IF (ERR_GOOD()) THEN
         WRITE(BUF, *) HFNC
         IERR = DS_MAP_ASGVAL(HFNI,
     &                        NOM(1:SP_STRN_LEN(NOM)),
     &                        BUF(1:SP_STRN_LEN(BUF)))
      ENDIF

C---     Met en mode parsing de fonction
      IF (ERR_GOOD()) THEN
         IC_ICMD_STS(IOB) = IBSET(IC_ICMD_STS(IOB), IC_ICMD_STS_PRSFNC)
      ENDIF
      GOTO 9999

C---     En mode parsing de fonction
C        ===========================
2000  CONTINUE
      IF (.NOT. IC_FUNC_HVALIDE(HFNC)) GOTO 9907
      IF (SP_STRN_LEN(CMDSTAT) .LE. 0) GOTO 9999
      IF (CMDSTAT .EQ. 'endfunction') THEN
         IC_ICMD_STS(IOB) = IBCLR(IC_ICMD_STS(IOB), IC_ICMD_STS_PRSFNC)
         IERR = IC_FUNC_ENDWRITE(HFNC)
         HFNC = 0
      ELSE
         IERR = IC_FUNC_ADDSTAT(HFNC, ILD, ILF, CMDSTAT)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_ZONE_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_DECLARATION_TROP_LONGUE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(3A)') 'ERR_REDEFINITION_FNC_MODULE',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF,'(3A)') 'ERR_REDEFINITION_FNC_UTILISATEUR',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      CALL ERR_PUSH()
      IERR = IC_ICMD_REQHFNCFNI(HOBJ, NOM(1:SP_STRN_LEN(NOM)), HFNC)
      CALL ERR_POP()
      IF (IERR .EQ. ERR_OK) THEN    ! ERR_GOOD() pas utilisable ici
         NOM = IC_FUNC_REQFSRC(HFNC)
         WRITE(ERR_BUF,'(3A)') 'MSG_DEFINIE_ICI',': ',
     &                        NOM(1:SP_STRN_LEN(NOM))
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      GOTO 9999
9905  WRITE(ERR_BUF,'(3A)') 'ERR_REDEFINITION_MOT_RESERVE',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF,'(3A)') 'ERR_PARSING_FONCTION',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9907  WRITE(ERR_BUF,'(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',
     &                      HFNC
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XTRFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Extrais un label
C
C Description:
C     La fonction privée IC_ICMD_XTRLBL extrait les labels.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HLST     Handle sur la pile des labels
C     CMDSTAT  Command statement
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ICMD_XTRLBL(HOBJ, HLST, CMDSTAT)

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HLST
      CHARACTER*(*) CMDSTAT

      INCLUDE 'icicmd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'icfinp.fi'
!      INCLUDE 'icfunc.fi'
      INCLUDE 'icpfic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icicmd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I, IDUM, IPOS, LC
      INTEGER HFST, HLBL, HFIN
      CHARACTER*(256) BUF
      CHARACTER*(128) NOM

      INTEGER LBUF, LPRM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HOBJ))
D     CALL ERR_PRE(HLST .EQ. 0 .OR. DS_LIST_HVALIDE(HLST))
C-----------------------------------------------------------------------

C---     Contrôle et trim la ligne de commande
      IF (SP_STRN_LEN(CMDSTAT) .LT. 1) GOTO 9999
      BUF = CMDSTAT(1:SP_STRN_LEN(CMDSTAT))
      CALL SP_STRN_TRM(BUF)
      LBUF = SP_STRN_LEN(BUF)
      IF (LBUF .LT. 2) GOTO 9999
      IF (BUF(LBUF:LBUF) .NE. ':') GOTO 9999

C---     Contrôles
      BUF = BUF(1:LBUF-1)
      CALL SP_STRN_TRM(BUF)
      LBUF = SP_STRN_LEN(BUF)
      CALL SP_STRN_DLC(BUF, ' ')
      IF (LBUF .NE. SP_STRN_LEN(BUF)) GOTO 9905
      IF (IC_ICMD_ESTFNC(HOBJ, BUF(1:LBUF))) GOTO 9903
      IF (IC_ICMD_ESTFNI(HOBJ, BUF(1:LBUF))) GOTO 9903
      IF (IC_ICMD_ESTKW(BUF(1:LBUF))) GOTO 9904
      IF (IC_ICMD_ESTCW(BUF(1:LBUF))) GOTO 9904

C---     Récupère les attributs
      IOB  = HOBJ - IC_ICMD_HBASE
      HFST = IC_ICMD_FILSTK(IOB)
      HLBL = IC_ICMD_LBLDIC(IOB)

C---     Le fichier et la position
      IF (ERR_GOOD()) THEN
         IERR = IC_PFIC_TOP(HFST, HFIN, IDUM)
         LC   = IC_FINP_REQLCUR(HFIN)
!!         IERR = IC_FINP_REQNOM (HFIN, PRM)
      ENDIF

C---     Prépare le nom du label
      IF (ERR_GOOD()) WRITE(NOM,'(2A,I6)') BUF(1:LBUF), '@', LC

C---     Crée un objet label
      IF (ERR_GOOD()) THEN
         CALL LOG_TODO('IC_ICMD_XTRLBL: Cree label ' //
     &                  NOM(1:SP_STRN_LEN(NOM)))
!         IERR = IC_LABL_CTR(HFNC)
!         IERR = IC_LABL_INI(HFNC, NOM, PRM, LC, CMDSTAT)
      ENDIF

C---     Ajoute la fonction au dico
      IF (ERR_GOOD()) THEN
         CALL LOG_TODO('IC_ICMD_XTRLBL: Ajoute au dico')
!         WRITE(PRM, *) HFNC
!         IERR = DS_MAP_ASGVAL(HLBL,
!     &                        NOM(1:SP_STRN_LEN(NOM)),
!     &                        PRM(1:SP_STRN_LEN(PRM)))
      ENDIF

!         IERR = IC_FUNC_ADDSTAT(HFNC, CMDSTAT)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_INVALIDE_7308'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A, I6)') 'ERR_LECTURE_PARAMETRE',': ', I
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(3A)') 'ERR_REDEFINITION_FONCTION',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF,'(3A)') 'ERR_REDEFINITION_MOT_RESERVE',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF,'(3A)') 'ERR_PARSING_FONCTION',': ',
     &                      NOM(1:SP_STRN_LEN(NOM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF,'(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',
     &                      HLST
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_ICMD_XTRLBL = ERR_TYP()
      RETURN
      END
