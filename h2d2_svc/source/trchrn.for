C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER TR_CHRN_AJTZONE
C     INTEGER TR_CHRN_EFFZONE
C     SUBROUTINE TR_CHRN_START
C     SUBROUTINE TR_CHRN_STOP
C     INTEGER TR_CHRN_REQTEMPS
C     INTEGER TR_CHRN_LOGZONE
C   Private:
C     SUBROUTINE TR_CHRN_START_A
C     SUBROUTINE TR_CHRN_STOP_A
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise le COMMON
C
C Description:
C     Le block data LOG initialise le COMMON propre au module de timer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA TMR

      IMPLICIT NONE

      INCLUDE 'trchrn.fc'

      DATA TR_CHRN_ZACTU /' '/
      DATA TR_CHRN_LACTU /0/
      DATA TR_CHRN_XMAP  /0/
      DATA TR_CHRN_XSTK  /0/

      END

C************************************************************************
C Sommaire: Ajoute une zone au système de chrono.
C
C Description:
C     La fonctions <code>TR_CHRN_AJTZONE(...)</code> ajoute la zone
C     <code>ZONE</code> aux zones pour lesquelles le chrono est actif.
C     Le temps destiné à cette zone sera donc comptabilisé.
C     <p>
C     Les zones sont uniques. De multiples appels avec la même zone ne
C     génèrent qu'une seule entrée.
C     <p>
C     Les zones sont organisées de manière hiérarchiques. Par exemple,
C     'a' est le parent de 'a.b', lui même parent de 'a.b.c1' et 'a.b.c2'.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CHRN_AJTZONE(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_AJTZONE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER LZONE
      CHARACTER*(8) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

      IF (TR_CHRN_XMAP .EQ. 0) THEN
         TR_CHRN_XMAP = C_MAP_CTR()
         IF (TR_CHRN_XMAP .EQ. 0) GOTO 9900
         TR_CHRN_XSTK = C_MAP_CTR()
         IF (TR_CHRN_XSTK .EQ. 0) GOTO 9900
      ENDIF

      LZONE = SP_STRN_LEN(ZONE)
      IF (C_MAP_ESTCLF(TR_CHRN_XMAP, ZONE(1:LZONE)) .NE. 0) THEN
         BUF = '0.0D0'
         IERR = C_MAP_ASGVAL(TR_CHRN_XMAP, ZONE(1:LZONE), BUF(1:5))
         IF (IERR .NE. 0) GOTO 9901
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CONSTRUCTION_MAP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_ZONE_INVALIDE', ' : ',
     &                       ZONE(1:MIN(50,SP_STRN_LEN(ZONE)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      TR_CHRN_AJTZONE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Supprime une zone de chrono
C
C Description:
C     La fonctions <code>TR_CHRN_EFFZONE()</code> supprime la zone ZONE du
C     système de chrono. Une zone est supprimée si elle est connue. Si
C     elle n'est pas connue, on retourne sans erreur pour permettre
C     l'ajout et l'enlèvement multiple et symétrique.
C
C Entrée:
C     CHARACTER*(*) ZONE      Nom hiérarchique de la zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CHRN_EFFZONE(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_EFFZONE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER LZONE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

      LZONE = SP_STRN_LEN(ZONE)
      IERR = C_MAP_EFFCLF(TR_CHRN_XMAP, ZONE(1:LZONE))

      TR_CHRN_EFFZONE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Démarre le chrono sur une zone
C
C Description:
C     La fonction <code>TR_CHRN_START(...)</code> démarre le chrono pour la
C     zone donnée. Elle stoppe en premier le chrono pour la zone active
C     avant de le démarrer pour la nouvelle zone.
C
C Entrée:
C     CHARACTER*(*) ZONE         La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE TR_CHRN_START(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_START
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER LZONE
      CHARACTER*(64) BUF
!$    INTEGER OMP_GET_LEVEL
!$    INTEGER OMP_GET_THREAD_NUM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(TR_CHRN_XSTK .NE. 0)
D     CALL ERR_PRE(TR_CHRN_LACTU .GE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(TR_CHRN_ZACTU) .EQ. TR_CHRN_LACTU)
D     CALL ERR_PRE(C_MAP_REQDIM(TR_CHRN_XSTK) .EQ. TR_CHRN_LACTU)
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

C---     Log uniquement master thread
!$    IF (OMP_GET_LEVEL() .NE. 0) GOTO 9999
!$    IF (OMP_GET_THREAD_NUM() .NE. 0) GOTO 9999
      
C---     Enregistre la zone automatiquement
      LZONE = SP_STRN_LEN(ZONE)
      IERR = TR_CHRN_AJTZONE(ZONE)
      IF (ERR_BAD()) GOTO 9999

C---     Stoppe la zone actuelle
      IF (TR_CHRN_LACTU .GT. 0) THEN
         IERR = C_MAP_REQVAL(TR_CHRN_XSTK,
     &                       TR_CHRN_ZACTU(1:TR_CHRN_LACTU),
     &                       BUF)
D        CALL ERR_ASR(IERR .EQ. 0)
         CALL TR_CHRN_STOP_A(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF

C---     Push la nouvelle zone sur le stack
      TR_CHRN_ZACTU = TR_CHRN_ZACTU(1:TR_CHRN_LACTU) // '@'
      TR_CHRN_LACTU = TR_CHRN_LACTU + 1
      IERR = C_MAP_ASGVAL(TR_CHRN_XSTK,
     &                    TR_CHRN_ZACTU(1:TR_CHRN_LACTU),
     &                    ZONE(1:LZONE))
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Start le chrono sur la nouvelle zone
      CALL TR_CHRN_START_A(ZONE(1:LZONE))

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: Stoppe le chrono pour un zone, et réactive la zone précédente.
C
C Description:
C     La fonction <code>TR_CHRN_STOP(...)</code> stoppe le chrono pour la zone
C     passée en argument. Elle cumule le temps écoulé dans cette zone, puis
C     réactive la zone précédente.
C
C Entrée:
C     CHARACTER*(*) ZONE   La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE TR_CHRN_STOP(ZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_STOP
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER LZONE
      CHARACTER*(64) BUF
!$    INTEGER OMP_GET_LEVEL
!$    INTEGER OMP_GET_THREAD_NUM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(TR_CHRN_XSTK .NE. 0)
D     CALL ERR_PRE(TR_CHRN_LACTU .GT. 0)
D     CALL ERR_PRE(SP_STRN_LEN(TR_CHRN_ZACTU) .EQ. TR_CHRN_LACTU)
D     CALL ERR_PRE(C_MAP_REQDIM(TR_CHRN_XSTK) .EQ. TR_CHRN_LACTU)
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

C---     Log uniquement master thread
!$    IF (OMP_GET_LEVEL() .NE. 0) GOTO 9999
!$    IF (OMP_GET_THREAD_NUM() .NE. 0) GOTO 9999

C---     Si la zone n'est pas enregistrée, sort
      LZONE = SP_STRN_LEN(ZONE)
      IERR = C_MAP_ESTCLF(TR_CHRN_XMAP, ZONE(1:LZONE))
      IF (IERR .NE. 0) GOTO 9999

C---     Contrôle
      IERR = C_MAP_REQVAL(TR_CHRN_XMAP, ZONE(1:LZONE), BUF)
D     CALL ERR_ASR(IERR .EQ. 0)
      IERR = C_MAP_REQVAL(TR_CHRN_XSTK,
     &                    TR_CHRN_ZACTU(1:TR_CHRN_LACTU),
     &                    BUF)
D     CALL ERR_ASR(IERR .EQ. 0)
D     CALL ERR_ASR(BUF(1:SP_STRN_LEN(BUF)) .EQ. ZONE(1:LZONE))

C---     Stoppe la zone actuelle
      CALL TR_CHRN_STOP_A(ZONE(1:LZONE))

C---     Pop le stack
      IERR = C_MAP_EFFCLF(TR_CHRN_XSTK, TR_CHRN_ZACTU(1:TR_CHRN_LACTU))
      TR_CHRN_LACTU = TR_CHRN_LACTU - 1
      IF (TR_CHRN_LACTU .GT. 0) THEN
         TR_CHRN_ZACTU = TR_CHRN_ZACTU(1:TR_CHRN_LACTU)
      ELSE
         TR_CHRN_ZACTU = ' '
      ENDIF

C---     Start la nouvelle zone active
      IF (TR_CHRN_LACTU .GT. 0) THEN
         IERR = C_MAP_REQVAL(TR_CHRN_XSTK,
     &                       TR_CHRN_ZACTU(1:TR_CHRN_LACTU),
     &                       BUF)
D        CALL ERR_ASR(IERR .EQ. 0)
         CALL TR_CHRN_START_A(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps d'une zone
C
C Description:
C     La fonction TR_CHRN_REQTEMPS retourne le temps chrono cumulé pour la zone.
C
C Entrée:
C     CHARACTER*(*) ZONE   La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CHRN_REQTEMPS(ZONEG, TZONE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_REQTEMPS
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONEG
      REAL*8        TZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER I
      INTEGER LZONEG
      INTEGER LZON
      CHARACTER*(64) ZON, VAL
      REAL*8  DT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ZONEG) .GT. 0)
C-----------------------------------------------------------------------

      LZONEG = SP_STRN_LEN(ZONEG)
      TZONE  = 0.0D0

      DO I=1, C_MAP_REQDIM(TR_CHRN_XMAP)
         IERR = C_MAP_REQCLF(TR_CHRN_XMAP, I, ZON)
         IF (IERR .NE. 0) GOTO 9900

         LZON = SP_STRN_LEN(ZON)
         IF (LZON .GE. LZONEG) THEN
            IF (ZON(1:LZONEG) .EQ. ZONEG(1:LZONEG)) THEN
               IERR = C_MAP_REQVAL(TR_CHRN_XMAP, ZON(1:LZON), VAL)
               IF (IERR .NE. 0) GOTO 9901
               READ(VAL, *) DT
               TZONE = TZONE + DT
            ENDIF
         ENDIF

      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_MAP_CORROMPU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_ZONE_INVALIDE', ' : ',
     &                       ZONEG(1:MIN(50,SP_STRN_LEN(ZONEG)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      TR_CHRN_REQTEMPS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps d'une zone
C
C Description:
C     La fonction TR_CHRN_LOGZONE retourne le temps chrono
C     cumulé pour la zone.
C
C Entrée:
C     CHARACTER*(*) ZONE   La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CHRN_LOGZONE(ZONEG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CHRN_LOGZONE
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ZONEG

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      INTEGER I
      INTEGER LZONEG
      INTEGER LZON
      REAL*8  TZONEG
      REAL*8  TZON
      CHARACTER*(64) ZON, VAL

!$    INTEGER OMP_GET_MAX_THREADS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ZONEG) .GT. 0)
C-----------------------------------------------------------------------

      LZONEG = SP_STRN_LEN(ZONEG)
      IERR = TR_CHRN_REQTEMPS(ZONEG(1:LZONEG), TZONEG)
      IF (TZONEG .GT. 0.0D0) TZONEG = 100.0D0 / TZONEG

      CALL LOG_ECRIS('MSG_TIMER')
      CALL LOG_INCIND()
!$    IF (OMP_GET_MAX_THREADS() .GT. 1) THEN
!$       CALL LOG_ECRIS('MSG_TYPE_TEMPS: Wall time')
!$    ELSE
         CALL LOG_ECRIS('MSG_TYPE_TEMPS: Wall time')
!!!         CALL LOG_ECRIS('MSG_TYPE_TEMPS: CPU time')
!$    ENDIF

      DO I = C_MAP_REQDIM(TR_CHRN_XMAP), 1, -1
         IERR = C_MAP_REQCLF(TR_CHRN_XMAP, I, ZON)
         IF (IERR .NE. 0) GOTO 9900

         LZON = SP_STRN_LEN(ZON)
         IF (LZON .GE. LZONEG) THEN
            IF (ZON(1:LZONEG) .EQ. ZONEG(1:LZONEG)) THEN
               IERR = TR_CHRN_REQTEMPS(ZON(1:LZON), TZON)
               IERR = C_DH_TMRSTR (TZON, VAL)
               IF (TZONEG .GT. 0.0D0) THEN
                  WRITE(LOG_BUF,'(A,2X,F6.2,A,2X,A)')
     &               VAL(1:SP_STRN_LEN(VAL)),TZON*TZONEG,'%',ZON(1:LZON)
               ELSE
                  WRITE(LOG_BUF,'(A,3X,A)')
     &               VAL(1:SP_STRN_LEN(VAL)),ZON(1:LZON)
               ENDIF
               CALL LOG_ECRIS(LOG_BUF)
            ENDIF
         ENDIF

      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_MAP_CORROMPU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      TR_CHRN_LOGZONE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Démarre le chrono.
C
C Description:
C     La fonction privée <code>TR_CHRN_START_A(...)</code> s'occupe des
C     écritures dans le map des zones au démarrage du chrono sur une zone.
C
C Entrée:
C     CHARACTER*(*) ZONE      La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE TR_CHRN_START_A(ZONE)

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      CHARACTER*(64) BUF
      REAL*8   TTOT, T
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

      IERR = C_MAP_REQVAL(TR_CHRN_XMAP, ZONE, BUF)
D     CALL ERR_ASR(IERR .EQ. 0)
      READ(BUF, *) TTOT

      IERR = C_DH_TMRSTART(T)
      WRITE(BUF, '(2(1PE26.17E3))') TTOT, T
      IERR = C_MAP_ASGVAL(TR_CHRN_XMAP, ZONE, BUF)
D     CALL ERR_ASR(IERR .EQ. 0)

      RETURN
      END

C************************************************************************
C Sommaire: Écris le message
C
C Description:
C     La fonction privée <code>TR_CHRN_STOP_A(...)</code> s'occupe des
C     écritures dans le map des zones à l'arrêt du chrono sur une zone.
C
C Entrée:
C     CHARACTER*(*) ZONE   La zone
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE TR_CHRN_STOP_A(ZONE)

      IMPLICIT NONE

      CHARACTER*(*) ZONE

      INCLUDE 'trchrn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fc'

      INTEGER IERR
      CHARACTER*(64) BUF
      REAL*8   TTOTAL, TSTART, DELT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CHRN_XMAP .NE. 0)
D     CALL ERR_PRE(SP_STRN_LEN(ZONE) .GT. 0)
C-----------------------------------------------------------------------

      IERR = C_MAP_REQVAL(TR_CHRN_XMAP, ZONE, BUF)
D     CALL ERR_ASR(IERR .EQ. 0)
      READ(BUF, *) TTOTAL, TSTART

      IERR = C_DH_TMRDELT(DELT, TSTART)
      WRITE(BUF, '(1PE25.17E3)') (TTOTAL+DELT)
      IERR = C_MAP_ASGVAL(TR_CHRN_XMAP, ZONE, BUF)
D     CALL ERR_ASR(IERR .EQ. 0)

      RETURN
      END
