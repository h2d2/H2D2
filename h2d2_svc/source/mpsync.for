C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: MP_SYNC
C     INTEGER MP_SYNC_000
C     INTEGER MP_SYNC_999
C     INTEGER MP_SYNC_CTR
C     INTEGER MP_SYNC_DTR
C     INTEGER MP_SYNC_INILCL
C     INTEGER MP_SYNC_INIMPI
C     INTEGER MP_SYNC_RST
C     LOGICAL MP_SYNC_HVALIDE
C     INTEGER MP_SYNC_RECV
C     INTEGER MP_SYNC_P_RECV
C     INTEGER MP_SYNC_SEND
C     INTEGER MP_SYNC_WAIT
C     INTEGER MP_SYNC_P_WAIT
C     INTEGER MP_SYNC_P_DIM
C
C************************************************************************

      MODULE MP_SYNC_M

      USE SO_ALLC_M
      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: MP_SYNC_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: MP_SYNC_SELF_T
         INTEGER :: COMM
         INTEGER :: SIZE
         INTEGER :: NSND
         INTEGER :: NRCV
         INTEGER :: SBUF
         INTEGER :: NVAL
         INTEGER :: TBASE
         INTEGER :: TNVAL
         INTEGER :: TSEND(3,8)
         INTEGER :: TRECV(3,8)
         INTEGER :: RQST (8)
         TYPE(SO_ALLC_VPTR2_T) :: BUFP
      END TYPE MP_SYNC_SELF_T

      CONTAINS
      
C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée MP_SYNC_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (MP_SYNC_SELF_T), POINTER :: MP_SYNC_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      MP_SYNC_REQSELF => SELF
      RETURN
      END FUNCTION MP_SYNC_REQSELF

      END MODULE MP_SYNC_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction MP_SYNC_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_000
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INCLUDE 'mpsync.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(MP_SYNC_HBASE,
     &                   'MPI Synchronization')

      MP_SYNC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction MP_SYNC_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_999
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INCLUDE 'mpsync.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL MP_SYNC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(MP_SYNC_HBASE,
     &                   MP_SYNC_DTR)

      MP_SYNC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction MP_SYNC_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_CTR
CDEC$ ENDIF

      USE MP_SYNC_M
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR, IRET
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   MP_SYNC_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = MP_SYNC_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. MP_SYNC_HVALIDE(HOBJ))
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      MP_SYNC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction MP_SYNC_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_DTR
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = MP_SYNC_RST(HOBJ)

C---     Dé-alloue
      SELF => MP_SYNC_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, MP_SYNC_HBASE)

      MP_SYNC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise vide.
C
C Description:
C     La fonction MP_SYNC_INILCL initialise l'objet comme fonction d'une DLL
C     à l'aide des paramètres.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_INILCL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_INILCL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MP_SYNC_RST(HOBJ)

      MP_SYNC_INILCL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction MP_SYNC_INIDLL initialise l'objet comme fonction d'une DLL
C     à l'aide des paramètres.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     T_BASE         Type MPI de base des données
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     NVAL           Dimension 1 de la table VVAL
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_INIMPI(HOBJ,
     &                        T_BASE,
     &                        NPROC,
     &                        KPRECR,
     &                        NECR,
     &                        KECRD,
     &                        KECR,
     &                        NLIS,
     &                        KLISD,
     &                        KLIS,
     &                        NVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_INIMPI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER T_BASE
      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD (NPROC+1)
      INTEGER KECR  (NECR)
      INTEGER NLIS
      INTEGER KLISD (NPROC+1)
      INTEGER KLIS  (NLIS)
      INTEGER NVAL

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I_COMM, I_SIZE, I_ERROR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MP_SYNC_RST(HOBJ)

C---     TOUT SEUL ? ==> FIN
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      IF (ERR_BAD()) GOTO 9999
      IF (I_SIZE .LE. 1) GOTO 9999

      IERR = MP_SYNC_P_DIM(HOBJ,
     &                     I_COMM,
     &                     I_SIZE,
     &                     NPROC,
     &                     KPRECR,
     &                     NECR,
     &                     KECRD,
     &                     KECR,
     &                     NLIS,
     &                     KLISD,
     &                     KLIS,
     &                     NVAL,
     &                     T_BASE)

9999  CONTINUE
      MP_SYNC_INIMPI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_RAZ(HOBJ)

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fc'

      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MP_SYNC_REQSELF(HOBJ)

C---     Reset les attributs
      SELF%COMM  = 0
      SELF%SIZE  = 0
      SELF%SIZE  = 0
      SELF%NSND  = 0
      SELF%SBUF  = 0
      SELF%BUFP  = SO_ALLC_VPTR2()
      SELF%NVAL  = 0
      SELF%TBASE = 0
      SELF%TNVAL = 0
      SELF%TSEND = 0
      SELF%TRECV = 0
      SELF%RQST  = 0

      MP_SYNC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     La mémoire est récupérée automatiquement dans RAZ
C     par réassignation de BUFP
C************************************************************************
      FUNCTION MP_SYNC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les attributs
      IERR = MP_SYNC_RAZ(HOBJ)

      MP_SYNC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MP_SYNC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_HVALIDE
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mpsync.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      MP_SYNC_HVALIDE = OB_OBJN_HVALIDE(HOBJ, MP_SYNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation_R.
C
C Description:
C     La fonction MP_SYNC_RECV lance les commandes de reception
C     non bloquantes.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C
C Notes:
C     La table BUF est utilisée indifférenciée INT,  REAL.
C     La commande MPI_IRECV fait l'équivalent d'un MPI_PACK
C************************************************************************
      FUNCTION MP_SYNC_RECV (HOBJ,
     &                       NVAL,
     &                       NNL,
     &                       KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_RECV
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL (*)     ! (NVAL, NNL)

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TOUT SEUL ? ==> FIN
      SELF => MP_SYNC_REQSELF(HOBJ)
      IF (SELF%SIZE .LE. 1) GOTO 9999

C---     Receive
      IERR = MP_SYNC_P_RECV(HOBJ,
     &                      SELF%SBUF,
     &                      SELF%BUFP%VPTR)

9999  CONTINUE
      MP_SYNC_RECV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation_R.
C
C Description:
C     La fonction privée MP_SYNC_P_RECV lance les commandes de reception
C     non bloquantes. Elle permet de résoudre la table BUF.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C
C Notes:
C     La table BUF est utilisée indifférenciée INT,  REAL.
C     La commande MPI_IRECV fait l'équivalent d'un MPI_PACK
C************************************************************************
      FUNCTION MP_SYNC_P_RECV(HOBJ, LBUF1, BUF)

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER LBUF1
      REAL*8  BUF   (LBUF1, *)

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'mpsync.fc'

      INTEGER, PARAMETER :: I_TAGR = 1234

      INTEGER IP
      INTEGER I_ERR
      INTEGER I_COMM, I_RQST
      INTEGER I_TGT, I_NNE
      INTEGER N_RECV, T_NVAL
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => MP_SYNC_REQSELF(HOBJ)
      I_COMM = SELF%COMM
      N_RECV = SELF%NRCV
      T_NVAL = SELF%TNVAL

C---     Reception non bloquante des données
      DO IP=1, N_RECV
         I_RQST = 0
         IF (ERR_GOOD()) THEN
            I_TGT = SELF%TRECV(2, IP)
            I_NNE = SELF%TRECV(3, IP)
            CALL MPI_IRECV(BUF(1,IP),     ! BUF(KLISD(IP)),
     &                     I_NNE,
     &                     T_NVAL,
     &                     I_TGT,
     &                     I_TAGR,
     &                     I_COMM,
     &                     I_RQST,
     &                     I_ERR)
         ENDIF
         SELF%RQST(IP) = I_RQST
      ENDDO

      MP_SYNC_P_RECV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation.
C
C Description:
C     La fonction MP_SYNC_SEND va synchroniser avec les process
C     voisins les données de type T_BASE de la table générique KVAL.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     KVAL           Table de valeurs
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C     Utilise les appels non bloquants:
C     1) enregistre les RECV le plus tot possible. Elles travaillent avec
C        un buffer
C     2) crée les types
C     3) envoie les données
C     4) attends la fin de transmission
C     5) unpack les données
C
C remarques: i_comm peut être spécialisé pour le groupe
C************************************************************************
      FUNCTION MP_SYNC_SEND(HOBJ,
     &                      NVAL,
     &                      NNL,
     &                      KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_SEND
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL (*)     ! (NVAL, NNL)

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mpsync.fc'

      INTEGER, PARAMETER :: I_TAGS = 4321

      INTEGER IP
      INTEGER I_ERR
      INTEGER I_COMM
      INTEGER I_TYP, I_TGT
      INTEGER N_SEND
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     TOUT SEUL ? ==> FIN
      SELF => MP_SYNC_REQSELF(HOBJ)
      IF (SELF%SIZE .LE. 1) GOTO 9999

C---     Envoie les données
      I_COMM = SELF%COMM
      N_SEND = SELF%NSND
      DO IP=1,N_SEND
         IF (ERR_GOOD()) THEN
!            CALL MPI_ISEND(KVAL, 1, K_TSND(IP), KPRECR(IP)-1,
!     &                     I_TAGS, I_COMM, K_RQST(IP), I_ERROR)
            I_TYP = SELF%TSEND(1, IP)
            I_TGT = SELF%TSEND(2, IP)
            CALL MPI_SEND(KVAL, 1, I_TYP, I_TGT, I_TAGS, I_COMM, I_ERR)
         ENDIF
      ENDDO

9999  CONTINUE
      MP_SYNC_SEND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation.
C
C Description:
C     La fonction MP_UTIL_SYNC va synchroniser avec les process
C     voisins les données de type T_BASE de la table générique KVAL.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     KVAL           Table de valeurs
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C     Utilise les appels non bloquants:
C     1) enregistre les RECV le plus tot possible. Elles travaillent avec
C        un buffer
C     2) crée les types
C     3) envoie les données
C     4) attends la fin de transmission
C     5) unpack les données
C
C remarques: i_comm peut être spécialisé pour le groupe
C************************************************************************
      FUNCTION MP_SYNC_WAIT(HOBJ,
     &                      NVAL,
     &                      NNL,
     &                      KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_SYNC_WAIT
CDEC$ ENDIF

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL (*)     ! (NVAL, NNL)

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mpsync.fc'

      INTEGER, PARAMETER :: I_SIZMAX = 64

      INTEGER IERR
      integer in, ip

      INTEGER I_ERR
      INTEGER N_RQST
      INTEGER K_RQST(I_SIZMAX), K_STTS(MPI_STATUS_SIZE, I_SIZMAX)
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TOUT SEUL ? ==> FIN
      SELF => MP_SYNC_REQSELF(HOBJ)
      IF (SELF%SIZE .LE. 1) GOTO 9999

C---     Attend la fin de la transmission
      N_RQST = SELF%NRCV
      CALL MPI_WAITALL(N_RQST, K_RQST, K_STTS, I_ERR)
      if (err_bad()) then
         do ip=1,n_rqst
            write(6,*) ip, (k_stts(in,ip),in=1,MPI_STATUS_SIZE)
         enddo
      endif

C---     Transfert du buffer vers KVAL
      IF (ERR_GOOD()) THEN
         IERR = MP_SYNC_P_WAIT(HOBJ,
     &                         SELF%SBUF,
     &                         SELF%BUFP%VPTR,
     &                         KVAL)
      ENDIF
      
9999  CONTINUE
      MP_SYNC_WAIT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation_R.
C
C Description:
C     La fonction MP_UTIL_SYNC_R lance les commandes de reception
C     non bloquantes. Elle permet de résoudre la table BUF.
C
C Entrée:
C     NVAL
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     KVAL           Table de valeurs
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C************************************************************************
      FUNCTION MP_SYNC_P_WAIT(HOBJ, LBUF1, BUF, KVAL)

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER LBUF1
      REAL*8  BUF   (LBUF1, *)
      INTEGER KVAL  (*)

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IP

      INTEGER I_COMM, I_ERR, I_POS, I_TSIZ, I_TYP
      INTEGER N_RECV
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => MP_SYNC_REQSELF(HOBJ)
      I_COMM = SELF%COMM
      N_RECV = SELF%NRCV

      DO IP=1,N_RECV
         I_POS = 0
         I_TYP = SELF%TRECV(3, IP)
         IF (ERR_GOOD()) THEN
            CALL MPI_PACK_SIZE(1,               ! count
     &                         I_TYP,           ! datatype
     &                         I_COMM,          !
     &                         I_TSIZ,          ! size in byte
     &                         I_ERR)
         ENDIF
         IF (ERR_GOOD()) THEN
            CALL MPI_UNPACK   (BUF(1,IP),       ! buffer start
     &                         I_TSIZ,          ! size of buf in bytes
     &                         I_POS,           ! 0
     &                         KVAL,            ! output buffer
     &                         1,               ! nbr item to unpack
     &                         I_TYP,           ! datatype
     &                         I_COMM,
     &                         I_ERR)

         ENDIF
      ENDDO

      MP_SYNC_P_WAIT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SYNChronisation.
C
C Description:
C     La fonction privée MP_SYNC_DIM dimensionne l'objet. On crée
C     les tables et les types nécessaires à la synchro.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     NVAL           Dimension un de la table des valeurs
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C     i_comm peut être spécialisé pour le groupe
C************************************************************************
      FUNCTION MP_SYNC_P_DIM(HOBJ,
     &                       I_COMM,
     &                       I_SIZE,
     &                       NPROC,
     &                       KPRECR,
     &                       NECR,
     &                       KECRD,
     &                       KECR,
     &                       NLIS,
     &                       KLISD,
     &                       KLIS,
     &                       NVAL,
     &                       T_BASE)

      USE MP_SYNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I_COMM
      INTEGER I_SIZE
      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER KECR (NECR)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)
      INTEGER KLIS (NLIS)
      INTEGER NVAL
      INTEGER T_BASE

      INCLUDE 'mpsync.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'soallc.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'mpsync.fc'

      INTEGER IERR
      INTEGER IN, IP
      INTEGER SBUF
      INTEGER N_IDX
      INTEGER NSND, NRCV, NMAX
      INTEGER NNE

      INTEGER I_ERR
      INTEGER T_NVAL, T_GLOB
      TYPE (SO_ALLC_KPTR1_T) :: IDX_P
      TYPE (SO_ALLC_VPTR2_T) :: BUF_P
      TYPE (MP_SYNC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MP_SYNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPROC  .EQ. I_SIZE)
D     CALL ERR_PRE(I_SIZE .GT. 1)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.mpi'

C---     Récupère l'indice
      SELF => MP_SYNC_REQSELF(HOBJ)

C---     Détermine la taille max à lire/écrire
      NMAX = -1
      NSND =  0
      NRCV =  0
      DO IP=1,NPROC
         NNE = KECRD(IP+1)-KECRD(IP)
         IF (NNE .GT. 0) NSND = NSND + 1
         NMAX = MAX(NMAX, NNE)
      ENDDO
      DO IP=1,NPROC
         NNE = KLISD(IP+1)-KLISD(IP)
         IF (NNE .GT. 0) NRCV = NRCV + 1
         NMAX = MAX(NMAX, NNE)
      ENDDO
      IF (NMAX .LE. 0) GOTO 9900

C---     Crée le sous-type contenant NVAL valeurs contiguës
      T_NVAL = 0
      IF (ERR_GOOD()) CALL MPI_TYPE_CONTIGUOUS(NVAL,T_BASE,T_NVAL,I_ERR)
      IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT(T_NVAL, I_ERR)

C---     Alloue le buffer de reception
      BUF_P = SO_ALLC_VPTR2()
      IF (ERR_GOOD()) CALL MPI_PACK_SIZE(NMAX,T_NVAL,I_COMM,SBUF,I_ERR)
      IF (ERR_GOOD()) THEN
         SBUF = INT((SBUF+7) / 8)    ! En REAL*8
         BUF_P = SO_ALLC_VPTR2(SBUF, NRCV)
      ENDIF

C---     Stoke la première partie
      IF (ERR_GOOD()) THEN
         SELF%COMM = I_COMM
         SELF%SIZE = I_SIZE
         SELF%NRCV = NRCV
         SELF%NSND = NSND
         SELF%SBUF = SBUF
         SELF%BUFP = BUF_P
         SELF%NVAL = NVAL
         SELF%TBASE= T_BASE
         SELF%TNVAL= T_NVAL
      ENDIF

C---     Alloue la table du nbr d'elem par bloc
      IDX_P = SO_ALLC_KPTR1()
      IF (ERR_GOOD()) THEN
         IDX_P = SO_ALLC_KPTR1(NMAX)
         IDX_P%KPTR(1:N_IDX) = 1
      ENDIF

C---     Passe des indices aux offsets (inconditionnel)
      KLIS(1:NLIS) = KLIS(1:NLIS) - 1

C---     Crée les types pour l'envoi
      DO IP=1,(NPROC-1)
         T_GLOB = 0
         NNE = KECRD(IP+1)-KECRD(IP)
         IF (ERR_GOOD() .AND. NNE .GT. 0) THEN
            CALL MPI_TYPE_INDEXED(NNE,
     &                            IDX_P%KPTR,
     &                            KECR(KECRD(IP)),
     &                            T_NVAL,
     &                            T_GLOB,
     &                            I_ERR)
         ENDIF
         IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT(T_GLOB, I_ERR)
         IF (ERR_GOOD()) THEN
            SELF%TSEND(2,IP) = T_GLOB
            SELF%TSEND(1,IP) = NNE
            SELF%TSEND(3,IP) = IP-1
         ENDIF
      ENDDO

C---     Crée les types pour la réception
      DO IP=1,(NPROC-1)
         T_GLOB = 0
         IF (ERR_GOOD()) THEN
            NNE = KLISD(IP+1)-KLISD(IP)
            CALL MPI_TYPE_INDEXED(NNE,
     &                            IDX_P%KPTR,
     &                            KLIS(KLISD(IP)),
     &                            T_NVAL,
     &                            T_GLOB,
     &                            I_ERR)
         ENDIF
         IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT(T_GLOB, I_ERR)
         IF (ERR_GOOD()) THEN
            SELF%TRECV(1,IP) = T_GLOB
            SELF%TRECV(2,IP) = NNE
            SELF%TRECV(3,IP) = IP-1
         ENDIF
      ENDDO

C---     Repasse des offsets aux indices (inconditionnel)
      KLIS(1:NLIS) = KLIS(1:NLIS) + 1

C---     Désalloue la table du nbr d'elem par bloc
      !!delete IDX_P

      GOTO 9999
C-------------------------------------------------------------------------
9900  CONTINUE
      WRITE(ERR_BUF, '()') 'ERR_NMAX_ECR_LEC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MP_SYNC_P_DIM = ERR_TYP()
      RETURN
      END
