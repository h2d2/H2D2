C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE SO_ALLC_M

      USE ISO_C_BINDING, ONLY : C_LOC, C_PTR, C_NULL_PTR, C_F_POINTER
      IMPLICIT NONE

      !---------------------------------------------------------------------
      !  Fonction de cast en BYTE, POINTER
      !---------------------------------------------------------------------
      INTERFACE SO_ALLC_CST2B
          MODULE PROCEDURE SO_ALLC_CST2B_1C
          MODULE PROCEDURE SO_ALLC_CST2B_1F
          MODULE PROCEDURE SO_ALLC_CST2B_1H
          MODULE PROCEDURE SO_ALLC_CST2B_1B
          MODULE PROCEDURE SO_ALLC_CST2B_1L
          MODULE PROCEDURE SO_ALLC_CST2B_1I2
          MODULE PROCEDURE SO_ALLC_CST2B_1I4
          MODULE PROCEDURE SO_ALLC_CST2B_1I8
          MODULE PROCEDURE SO_ALLC_CST2B_1R8
          MODULE PROCEDURE SO_ALLC_CST2B_1Z8
          MODULE PROCEDURE SO_ALLC_CST2B_B
          MODULE PROCEDURE SO_ALLC_CST2B_L
          MODULE PROCEDURE SO_ALLC_CST2B_I2
          MODULE PROCEDURE SO_ALLC_CST2B_I4
          MODULE PROCEDURE SO_ALLC_CST2B_I8
          MODULE PROCEDURE SO_ALLC_CST2B_R8
          MODULE PROCEDURE SO_ALLC_CST2B_Z8
          MODULE PROCEDURE SO_ALLC_CST2B_S
      END INTERFACE

      !---------------------------------------------------------------------
      !  Classe qui encapsule un handle et le pointeur aux données
      !---------------------------------------------------------------------
      TYPE, PUBLIC :: SO_ALLC_PTR_T
         INTEGER,     PRIVATE :: HNDL  = 0
         TYPE(C_PTR), PRIVATE :: CPTR  = C_NULL_PTR
         LOGICAL,     PRIVATE :: DOOWN = .FALSE.
      CONTAINS
         PROCEDURE, PRIVATE :: CST2B0
         PROCEDURE, PRIVATE :: CST2K0, CST2K1, CST2K2
         PROCEDURE, PRIVATE :: CST2X0, CST2X1, CST2X2
         PROCEDURE, PRIVATE :: CST2V0, CST2V1, CST2V2
         
         FINAL :: SO_ALLC_PTR_DTR
         PROCEDURE, PUBLIC  :: REQHNDL
         GENERIC,   PUBLIC  :: CST2B => CST2B0
         GENERIC,   PUBLIC  :: CST2K => CST2K0, CST2K1, CST2K2
         GENERIC,   PUBLIC  :: CST2X => CST2X0, CST2X1, CST2X2
         GENERIC,   PUBLIC  :: CST2V => CST2V0, CST2V1, CST2V2
      END TYPE SO_ALLC_PTR_T

      TYPE, PUBLIC, EXTENDS(SO_ALLC_PTR_T) :: SO_ALLC_KPTR1_T
         INTEGER, POINTER, PUBLIC :: KPTR(:)
      CONTAINS
         FINAL :: SO_ALLC_KPTR1_DTR
      END TYPE SO_ALLC_KPTR1_T
      
      TYPE, PUBLIC, EXTENDS(SO_ALLC_PTR_T) :: SO_ALLC_KPTR2_T
         INTEGER, POINTER, PUBLIC :: KPTR(:,:)
      CONTAINS
         FINAL :: SO_ALLC_KPTR2_DTR
      END TYPE SO_ALLC_KPTR2_T
      
      TYPE, PUBLIC, EXTENDS(SO_ALLC_PTR_T) :: SO_ALLC_VPTR1_T
         REAL*8, POINTER, PUBLIC :: VPTR(:)
      CONTAINS
         FINAL :: SO_ALLC_VPTR1_DTR
      END TYPE SO_ALLC_VPTR1_T
      
      TYPE, PUBLIC, EXTENDS(SO_ALLC_PTR_T) :: SO_ALLC_VPTR2_T
         REAL*8, POINTER, PUBLIC :: VPTR(:,:)
      CONTAINS
         FINAL :: SO_ALLC_VPTR2_DTR
      END TYPE SO_ALLC_VPTR2_T
      
      INTERFACE SO_ALLC_PTR
          MODULE PROCEDURE SO_ALLC_PTR_CTR
          MODULE PROCEDURE SO_ALLC_PTR_CTR_HNDL
          MODULE PROCEDURE SO_ALLC_PTR_CTR_CPY
      END INTERFACE
      INTERFACE ASSIGNMENT (=)
          MODULE PROCEDURE SO_ALLC_PTR_ASG
      END INTERFACE

      INTERFACE SO_ALLC_KPTR1
          MODULE PROCEDURE SO_ALLC_KPTR1_CTR
          MODULE PROCEDURE SO_ALLC_KPTR1_CTR_LEN
          MODULE PROCEDURE SO_ALLC_KPTR1_CTR_CPY
      END INTERFACE
      INTERFACE ASSIGNMENT (=)
          MODULE PROCEDURE SO_ALLC_KPTR1_ASG
      END INTERFACE

      INTERFACE SO_ALLC_KPTR2
          MODULE PROCEDURE SO_ALLC_KPTR2_CTR
          MODULE PROCEDURE SO_ALLC_KPTR2_CTR_LEN
          MODULE PROCEDURE SO_ALLC_KPTR2_CTR_CPY
      END INTERFACE
      INTERFACE ASSIGNMENT (=)
          MODULE PROCEDURE SO_ALLC_KPTR2_ASG
      END INTERFACE

      INTERFACE SO_ALLC_VPTR1
          MODULE PROCEDURE SO_ALLC_VPTR1_CTR
          MODULE PROCEDURE SO_ALLC_VPTR1_CTR_LEN
          MODULE PROCEDURE SO_ALLC_VPTR1_CTR_CPY
      END INTERFACE
      INTERFACE ASSIGNMENT (=)
          MODULE PROCEDURE SO_ALLC_VPTR1_ASG
      END INTERFACE

      INTERFACE SO_ALLC_VPTR2
          MODULE PROCEDURE SO_ALLC_VPTR2_CTR
          MODULE PROCEDURE SO_ALLC_VPTR2_CTR_LEN
          MODULE PROCEDURE SO_ALLC_VPTR2_CTR_CPY
      END INTERFACE
      INTERFACE ASSIGNMENT (=)
          MODULE PROCEDURE SO_ALLC_VPTR2_ASG
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_PTR_CTR(...) est le constructeur par défaut
C     de la classe SO_ALLC_PTR_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_PTR_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_PTR_CTR
CDEC$ ENDIF
      
      TYPE(SO_ALLC_PTR_T) :: SELF
C-----------------------------------------------------------------------
      
      SELF%HNDL = 0
      SELF%CPTR = C_NULL_PTR
      SELF%DOOWN = .FALSE.
      
      RETURN
      END FUNCTION SO_ALLC_PTR_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_PTR_CTR_HNDL(...) construit un objet
C     SO_ALLC_PTR_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_PTR_CTR_HNDL(HNDL) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_PTR_CTR_HNDL
CDEC$ ENDIF

      TYPE(SO_ALLC_PTR_T) :: SELF
      INTEGER, INTENT(IN) :: HNDL

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
      
      INTEGER IERR
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(HNDL))
C-----------------------------------------------------------------------

      IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)

      SELF%HNDL  = HNDL
      SELF%CPTR  = CPTR
      SELF%DOOWN = .FALSE.
      
      RETURN
      END FUNCTION SO_ALLC_PTR_CTR_HNDL

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_PTR_CTR_CPY(...) est le constructeur copy
C     de la classe SO_ALLC_PTR_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_PTR_CTR_CPY(OTHER) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_PTR_CTR_CPY
CDEC$ ENDIF

      TYPE(SO_ALLC_PTR_T) :: SELF
      TYPE(SO_ALLC_PTR_T), INTENT(IN) :: OTHER
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      
      RETURN
      END FUNCTION SO_ALLC_PTR_CTR_CPY

C************************************************************************
C Sommaire: Opérateur = 
C
C Description:
C     La constructeur SO_ALLC_PTR_ASG(...) est l'opérateur d'assignation 
C     de la classe SO_ALLC_PTR_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_PTR_ASG(SELF, OTHER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_PTR_ASG
CDEC$ ENDIF

      TYPE(SO_ALLC_PTR_T), INTENT(INOUT) :: SELF
      TYPE(SO_ALLC_PTR_T), INTENT(IN)    :: OTHER
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      
      RETURN
      END SUBROUTINE SO_ALLC_PTR_ASG

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_PTR_DTR(...) est le destructeur
C     de la classe SO_ALLC_PTR_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_PTR_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_PTR_DTR
CDEC$ ENDIF
      TYPE(SO_ALLC_PTR_T) :: SELF
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'
      
      INTEGER IERR
      INTEGER ITYP
C-----------------------------------------------------------------------

      IF (SELF%DOOWN) THEN
         IERR = C_MM_DECREF(SO_ALLC_XMEM, SELF%HNDL)
      ENDIF

      SELF%HNDL = 0
      SELF%CPTR = C_NULL_PTR
      SELF%DOOWN = .FALSE.

      RETURN
      END SUBROUTINE SO_ALLC_PTR_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode REQHNDL(...) retourne le handle de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION REQHNDL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: REQHNDL
CDEC$ ENDIF
      INTEGER :: REQHNDL
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------
      REQHNDL = SELF%HNDL
      RETURN
      END FUNCTION REQHNDL

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2B(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2B0(SELF) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2B0
CDEC$ ENDIF
      BYTE, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/1/))
      RETURN
      END FUNCTION CST2B0

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2K(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2K0(SELF) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2K0
CDEC$ ENDIF
      INTEGER, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/1/))
      RETURN
      END FUNCTION CST2K0

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2K1(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2K1(SELF, I1) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2K1
CDEC$ ENDIF
      INTEGER, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1/))
      RETURN
      END FUNCTION CST2K1
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2K2(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2K2(SELF, I1, I2) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2K2
CDEC$ ENDIF
      INTEGER, POINTER :: PTR(:,:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
      INTEGER, INTENT(IN) :: I2
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1, I2/))
      RETURN
      END FUNCTION CST2K2
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2X0(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2X0(SELF) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2X0
CDEC$ ENDIF
      INTEGER*8, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/1/))
      RETURN
      END FUNCTION CST2X0
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2X1(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2X1(SELF, I1) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2X1
CDEC$ ENDIF
      INTEGER*8, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1/))
      RETURN
      END FUNCTION CST2X1
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2X2(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2X2(SELF, I1, I2) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2X2
CDEC$ ENDIF
      INTEGER*8, POINTER :: PTR(:,:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
      INTEGER, INTENT(IN) :: I2
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1, I2/))
      RETURN
      END FUNCTION CST2X2
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2V0(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2V0(SELF) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2V0
CDEC$ ENDIF
      REAL*8, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/1/))
      RETURN
      END FUNCTION CST2V0

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2V1(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2V1(SELF, I1) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2V1
CDEC$ ENDIF
      REAL*8, POINTER :: PTR(:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1/))
      RETURN
      END FUNCTION CST2V1

      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode CST2V2(...) retourne le pointeur à la table associée à
C     l'objet SELF.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CST2V2(SELF, I1, I2) RESULT(PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CST2V2
CDEC$ ENDIF
      REAL*8, POINTER :: PTR(:,:)
      CLASS(SO_ALLC_PTR_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: I1
      INTEGER, INTENT(IN) :: I2
C-----------------------------------------------------------------------
      CALL C_F_POINTER(SELF%CPTR, PTR, (/I1,I2/))
      RETURN
      END FUNCTION CST2V2

      
C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR1_CTR(...) est le constructeur par défaut
C     de la classe SO_ALLC_KPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR1_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR1_CTR
CDEC$ ENDIF
      
      TYPE(SO_ALLC_KPTR1_T) :: SELF
C-----------------------------------------------------------------------
      
      SELF%HNDL  = 0
      SELF%CPTR  = C_NULL_PTR
      SELF%DOOWN = .FALSE.
      SELF%KPTR  => NULL()
      
      RETURN
      END FUNCTION SO_ALLC_KPTR1_CTR

C************************************************************************
C Sommaire: Alloue une table INTEGER*4
C
C Description:
C     La constructeur SO_ALLC_KPTR1_CTR_LEN(...) construit un objet
C     SO_ALLC_KPTR1_T comme table INTEGER*4 de dimension ILEN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR1_CTR_LEN(IDIM) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR1_CTR_LEN
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR1_T) :: SELF
      INTEGER, INTENT(IN)  :: IDIM

      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
         
      INTEGER IERR
      INTEGER HNDL
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------

      HNDL = 0
      IERR = SO_ALLC_ALLIN4(IDIM, HNDL)
      IF (ERR_GOOD()) THEN
         IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
         CALL C_F_POINTER(CPTR, SELF%KPTR, (/IDIM/))
         SELF%HNDL  = HNDL
         SELF%CPTR  = CPTR
         SELF%DOOWN = .TRUE.
      ELSE
         SELF = SO_ALLC_KPTR1()
      ENDIF

       END FUNCTION SO_ALLC_KPTR1_CTR_LEN

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR1_CTR_CPY(...) est le constructeur copy
C     de la classe SO_ALLC_KPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR1_CTR_CPY(OTHER) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR1_CTR_CPY
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR1_T) :: SELF
      TYPE(SO_ALLC_KPTR1_T), INTENT(IN) :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%KPTR  => OTHER%KPTR
      IF (ASSOCIATED(SELF%KPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)

      RETURN
      END FUNCTION SO_ALLC_KPTR1_CTR_CPY
      
C************************************************************************
C Sommaire: Opérateur = 
C
C Description:
C     La constructeur SO_ALLC_KPTR1_ASG(...) est l'opérateur d'assignation 
C     de la classe SO_ALLC_KPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_KPTR1_ASG(SELF, OTHER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR1_ASG
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR1_T), INTENT(INOUT) :: SELF
      TYPE(SO_ALLC_KPTR1_T), INTENT(IN)    :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%KPTR  => OTHER%KPTR
      IF (ASSOCIATED(SELF%KPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)
      
      RETURN
      END SUBROUTINE SO_ALLC_KPTR1_ASG

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR1_DTR(...) est le destructeur
C     de la classe SO_ALLC_KPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_KPTR1_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR1_DTR
CDEC$ ENDIF
      TYPE(SO_ALLC_KPTR1_T) :: SELF
C-----------------------------------------------------------------------
      SELF%KPTR => NULL()
      RETURN
      END SUBROUTINE SO_ALLC_KPTR1_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR2_CTR(...) est le constructeur par défaut
C     de la classe SO_ALLC_KPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR2_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR2_CTR
CDEC$ ENDIF
      
      TYPE(SO_ALLC_KPTR2_T) :: SELF
C-----------------------------------------------------------------------
      
      SELF%HNDL  = 0
      SELF%CPTR  = C_NULL_PTR
      SELF%DOOWN = .FALSE.
      SELF%KPTR  => NULL()
      
      RETURN
      END FUNCTION SO_ALLC_KPTR2_CTR

C************************************************************************
C Sommaire: Alloue une table INTEGER*4
C
C Description:
C     La constructeur SO_ALLC_KPTR2_CTR_LEN(...) construit un objet
C     SO_ALLC_KPTR2_T comme table INTEGER*4 de dimension ILEN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR2_CTR_LEN(IDIM1, IDIM2) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR2_CTR_LEN
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR2_T) :: SELF
      INTEGER, INTENT(IN)  :: IDIM1, IDIM2

      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
         
      INTEGER IERR
      INTEGER HNDL
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------

      HNDL = 0
      IERR = SO_ALLC_ALLIN4(IDIM1*IDIM2, HNDL)
      IF (ERR_GOOD()) THEN
         IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
         CALL C_F_POINTER(CPTR, SELF%KPTR, (/IDIM1, IDIM2/))
         SELF%HNDL  = HNDL
         SELF%CPTR  = CPTR
         SELF%DOOWN = .TRUE.
      ELSE
         SELF = SO_ALLC_KPTR2()
      ENDIF

      END FUNCTION SO_ALLC_KPTR2_CTR_LEN

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR2_CTR_CPY(...) est le constructeur copy
C     de la classe SO_ALLC_KPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_KPTR2_CTR_CPY(OTHER) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR2_CTR_CPY
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR2_T) :: SELF
      TYPE(SO_ALLC_KPTR2_T), INTENT(IN) :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%KPTR  => OTHER%KPTR
      IF (ASSOCIATED(SELF%KPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)

      RETURN
      END FUNCTION SO_ALLC_KPTR2_CTR_CPY
      
C************************************************************************
C Sommaire: Opérateur = 
C
C Description:
C     La constructeur SO_ALLC_KPTR2_ASG(...) est l'opérateur d'assignation 
C     de la classe SO_ALLC_KPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_KPTR2_ASG(SELF, OTHER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR2_ASG
CDEC$ ENDIF

      TYPE(SO_ALLC_KPTR2_T), INTENT(INOUT) :: SELF
      TYPE(SO_ALLC_KPTR2_T), INTENT(IN)    :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%KPTR  => OTHER%KPTR
      IF (ASSOCIATED(SELF%KPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)
      
      RETURN
      END SUBROUTINE SO_ALLC_KPTR2_ASG

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_KPTR2_DTR(...) est le destructeur
C     de la classe SO_ALLC_KPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_KPTR2_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_KPTR2_DTR
CDEC$ ENDIF
      TYPE(SO_ALLC_KPTR2_T) :: SELF
C-----------------------------------------------------------------------
      SELF%KPTR => NULL()
      RETURN
      END SUBROUTINE SO_ALLC_KPTR2_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_VPTR1_CTR(...) est le constructeur par défaut
C     de la classe SO_ALLC_VPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR1_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR1_CTR
CDEC$ ENDIF
      
      TYPE(SO_ALLC_VPTR1_T) :: SELF
C-----------------------------------------------------------------------
      
      SELF%HNDL  = 0
      SELF%CPTR  = C_NULL_PTR
      SELF%DOOWN = .FALSE.
      SELF%VPTR  => NULL()
      
      RETURN
      END FUNCTION SO_ALLC_VPTR1_CTR

C************************************************************************
C Sommaire: Alloue une table REAL*8
C
C Description:
C     La constructeur SO_ALLC_VPTR1_CTR_LEN(...) construit un objet
C     SO_ALLC_VPTR1_T comme table REAL*8 de dimension ILEN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR1_CTR_LEN(IDIM1) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR1_CTR_LEN
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR1_T) :: SELF
      INTEGER, INTENT(IN)  :: IDIM1

      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
         
      INTEGER IERR
      INTEGER HNDL
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------

      HNDL = 0
      IERR = SO_ALLC_ALLRE8(IDIM1, HNDL)
      IF (ERR_GOOD()) THEN
         IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
         CALL C_F_POINTER(CPTR, SELF%VPTR, (/IDIM1/))
         SELF%HNDL  = HNDL
         SELF%CPTR  = CPTR
         SELF%DOOWN = .TRUE.
      ELSE
         SELF = SO_ALLC_VPTR1()
      ENDIF

      RETURN
      END FUNCTION SO_ALLC_VPTR1_CTR_LEN

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_PTR_KCTR_CPY(...) est le constructeur copy
C     de la classe SO_ALLC_VPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR1_CTR_CPY(OTHER) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR1_CTR_CPY
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR1_T) :: SELF
      TYPE(SO_ALLC_VPTR1_T), INTENT(IN) :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%VPTR  => OTHER%VPTR
      IF (ASSOCIATED(SELF%VPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)
      
      RETURN
      END FUNCTION SO_ALLC_VPTR1_CTR_CPY
      
C************************************************************************
C Sommaire: Opérateur = 
C
C Description:
C     La constructeur SO_ALLC_VPTR1_ASG(...) est l'opérateur d'assignation 
C     de la classe SO_ALLC_VPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_VPTR1_ASG(SELF, OTHER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR1_ASG
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR1_T), INTENT(INOUT) :: SELF
      TYPE(SO_ALLC_VPTR1_T), INTENT(IN)    :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%VPTR  => OTHER%VPTR
      IF (ASSOCIATED(SELF%VPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)
      
      RETURN
      END SUBROUTINE SO_ALLC_VPTR1_ASG

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_VPTR1_DTR(...) est le destructeur
C     de la classe SO_ALLC_VPTR1_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_VPTR1_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR1_DTR
CDEC$ ENDIF
      TYPE(SO_ALLC_VPTR1_T) :: SELF
C-----------------------------------------------------------------------
      SELF%VPTR => NULL()
      RETURN
      END SUBROUTINE SO_ALLC_VPTR1_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_VPTR2_CTR(...) est le constructeur par défaut
C     de la classe SO_ALLC_VPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR2_CTR() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR2_CTR
CDEC$ ENDIF
      
      TYPE(SO_ALLC_VPTR2_T) :: SELF
C-----------------------------------------------------------------------
      
      SELF%HNDL  = 0
      SELF%CPTR  = C_NULL_PTR
      SELF%DOOWN = .FALSE.
      SELF%VPTR  => NULL()
      
      RETURN
      END FUNCTION SO_ALLC_VPTR2_CTR

C************************************************************************
C Sommaire: Alloue une table INTEGER*4
C
C Description:
C     La constructeur SO_ALLC_VPTR2_CTR_LEN(...) construit un objet
C     SO_ALLC_VPTR2_T comme table INTEGER*4 de dimension ILEN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR2_CTR_LEN(IDIM1, IDIM2) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR2_CTR_LEN
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR2_T) :: SELF
      INTEGER, INTENT(IN)  :: IDIM1, IDIM2

      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
         
      INTEGER IERR
      INTEGER HNDL
      TYPE(C_PTR) :: CPTR
C-----------------------------------------------------------------------

      HNDL = 0
      IERR = SO_ALLC_ALLRE8(IDIM1*IDIM2, HNDL)
      IF (ERR_GOOD()) THEN
         IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
         CALL C_F_POINTER(CPTR, SELF%VPTR, (/IDIM1, IDIM2/))
         SELF%HNDL  = HNDL
         SELF%CPTR  = CPTR
         SELF%DOOWN = .TRUE.
      ELSE
         SELF = SO_ALLC_VPTR2()
      ENDIF

      END FUNCTION SO_ALLC_VPTR2_CTR_LEN

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_VPTR2_CTR_CPY(...) est le constructeur copy
C     de la classe SO_ALLC_VPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_VPTR2_CTR_CPY(OTHER) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR2_CTR_CPY
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR2_T) :: SELF
      TYPE(SO_ALLC_VPTR2_T), INTENT(IN) :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%VPTR  => OTHER%VPTR
      IF (ASSOCIATED(SELF%VPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)

      RETURN
      END FUNCTION SO_ALLC_VPTR2_CTR_CPY
      
C************************************************************************
C Sommaire: Opérateur = 
C
C Description:
C     La constructeur SO_ALLC_VPTR2_ASG(...) est l'opérateur d'assignation 
C     de la classe SO_ALLC_VPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_VPTR2_ASG(SELF, OTHER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR2_ASG
CDEC$ ENDIF

      TYPE(SO_ALLC_VPTR2_T), INTENT(INOUT) :: SELF
      TYPE(SO_ALLC_VPTR2_T), INTENT(IN)    :: OTHER
      
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
      
      SELF%HNDL  = OTHER%HNDL
      SELF%CPTR  = OTHER%CPTR
      SELF%DOOWN = OTHER%DOOWN
      SELF%VPTR  => OTHER%VPTR
      IF (ASSOCIATED(SELF%VPTR)) 
     &   IERR = C_MM_INCREF(SO_ALLC_XMEM, SELF%HNDL)
      
      RETURN
      END SUBROUTINE SO_ALLC_VPTR2_ASG

C************************************************************************
C Sommaire:
C
C Description:
C     La constructeur SO_ALLC_VPTR2_DTR(...) est le destructeur
C     de la classe SO_ALLC_VPTR2_T.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SO_ALLC_VPTR2_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_VPTR2_DTR
CDEC$ ENDIF
      TYPE(SO_ALLC_VPTR2_T) :: SELF
C-----------------------------------------------------------------------
      SELF%VPTR => NULL()
      RETURN
      END SUBROUTINE SO_ALLC_VPTR2_DTR

      



C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1C(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1C(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1C
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1C(:)
      TYPE(C_PTR), INTENT(IN) ::  D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------

      CALL C_F_POINTER(D, RES, (/1/))
      SO_ALLC_CST2B_1C => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1C

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1F(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1F(F, FDUMMY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1F
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1F(:)
      INTEGER :: F
      INTEGER(KIND=1), INTENT(IN) :: FDUMMY
      EXTERNAL F

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
      INTEGER HNDL
      TYPE(C_PTR) :: CPTR
      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------

      HNDL = C_MM_ADDPTR(SO_ALLC_XMEM, F)
      IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
      CALL C_F_POINTER(CPTR, RES, (/1/))
      SO_ALLC_CST2B_1F => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1F

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1H(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1H(H, HDUMMY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1H
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1H(:)
      INTEGER :: H
      INTEGER(KIND=2), INTENT(IN) :: HDUMMY

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR
      TYPE(C_PTR) :: CPTR
      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(H))
C-----------------------------------------------------------------------

      IERR = C_MM_REQDTA(SO_ALLC_XMEM, H, CPTR)
      CALL C_F_POINTER(CPTR, RES, (/1/))
      SO_ALLC_CST2B_1H => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1H

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1B(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1B(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1B
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1B(:)
      BYTE, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1B => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1B

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1L(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1L(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1L
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1L(:)
      LOGICAL, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1L => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1L

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1I2(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1I2(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1I2
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1I2(:)
      INTEGER*2, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1I2 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1I2

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1I4(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1I4(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1I4
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1I4(:)
      INTEGER*4, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1I4 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1I4

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1I8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1I8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1I8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1I8(:)
      INTEGER*8, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1I8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1I8

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1R8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1R8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1R8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1R8(:)
      REAL*8, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1R8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1R8

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_1Z8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_1Z8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_1Z8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_1Z8(:)
      COMPLEX*16, TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C-----------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_1Z8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_1Z8



C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_B(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_B(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_B
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_B(:)
      BYTE, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_B => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_B

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_L(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_L(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_L
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_L(:)
      LOGICAL, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_L => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_L

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_I2(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_I2(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_I2
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_I2(:)
      INTEGER*2, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_I2 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_I2

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_I4(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_I4(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_I4
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_I4(:)
      INTEGER*4, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_I4 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_I4

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_I8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_I8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_I8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_I8(:)
      INTEGER*8, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_I8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_I8

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_R8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_R8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_R8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_R8(:)
      REAL*8, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_R8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_R8

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_Z8(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_Z8(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_Z8
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_Z8(:)
      COMPLEX*16, TARGET, INTENT(IN) :: D(:)

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_Z8 => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_Z8

C************************************************************************
C Sommaire: Cast en pointeur à Byte.
C
C Description:
C     La fonction publique SO_ALLC_CST2B_S(...) retourne le pointeur
C     Fortran BYTE correspondant aux données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CST2B_S(D)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CST2B_S
CDEC$ ENDIF

      BYTE, POINTER :: SO_ALLC_CST2B_S(:)
      CHARACTER*(*), TARGET, INTENT(IN) :: D

      BYTE, POINTER :: RES(:)
C------------------------------------------------------------------------

      CALL C_F_POINTER(C_LOC(D), RES, (/1/))
      SO_ALLC_CST2B_S => RES
      RETURN
      END FUNCTION SO_ALLC_CST2B_S




C************************************************************************
C Sommaire: Pointeur aux données.
C
C Description:
C     La fonction publique SO_ALLC_REQKPTR(...) retourne le pointeur
C     Fortran INTEGER correspondant au données du handle HNDL.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
!!!      FUNCTION SO_ALLC_REQKPTR(HNDL, SHP)
      FUNCTION SO_ALLC_REQKPTR(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQKPTR
CDEC$ ENDIF

      INTEGER, POINTER :: SO_ALLC_REQKPTR(:)
      INTEGER, INTENT(IN) :: HNDL

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
      
      INTEGER IERR
      TYPE(C_PTR) :: CPTR
      INTEGER, POINTER :: RES(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(HNDL))
C-----------------------------------------------------------------------

      IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
      CALL C_F_POINTER(CPTR, RES, (/1/))
      SO_ALLC_REQKPTR => RES
      RETURN
      END FUNCTION SO_ALLC_REQKPTR

C************************************************************************
C Sommaire: Pointeur aux données.
C
C Description:
C     La fonction publique SO_ALLC_REQXPTR(...) retourne le pointeur
C     Fortran INTEGER*8 correspondant au données du handle HNDL.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
!!!      FUNCTION SO_ALLC_REQXPTR(HNDL, SHP)
      FUNCTION SO_ALLC_REQXPTR(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQXPTR
CDEC$ ENDIF

      INTEGER*8, POINTER :: SO_ALLC_REQXPTR(:)
      INTEGER, INTENT(IN) :: HNDL

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
      
      INTEGER IERR
      TYPE(C_PTR) :: CPTR
      INTEGER*8, POINTER :: RES(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(HNDL))
C-----------------------------------------------------------------------

      IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
      CALL C_F_POINTER(CPTR, RES, (/1/))
      SO_ALLC_REQXPTR => RES
      RETURN
      END FUNCTION SO_ALLC_REQXPTR

C************************************************************************
C Sommaire: Pointeur aux données.
C
C Description:
C     La fonction publique SO_ALLC_REQVPTR(...) retourne le pointeur
C     Fortran REAL*8 correspondant au données du handle HNDL.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
!!!      FUNCTION SO_ALLC_REQVPTR(HNDL)
      FUNCTION SO_ALLC_REQVPTR(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQVPTR
CDEC$ ENDIF

      REAL*8, POINTER :: SO_ALLC_REQVPTR(:)
      INTEGER, INTENT(IN) :: HNDL

      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
      
      INTEGER IERR
      TYPE(C_PTR) :: CPTR
      REAL*8, POINTER :: RES(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(HNDL))
C-----------------------------------------------------------------------

      IERR = C_MM_REQDTA(SO_ALLC_XMEM, HNDL, CPTR)
      CALL C_F_POINTER(CPTR, RES, (/1/))
      SO_ALLC_REQVPTR => RES
      RETURN
      END FUNCTION SO_ALLC_REQVPTR

      END MODULE SO_ALLC_M

