C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:   Message-Passing interface
C Objet:
C
C Functions:
C   Public:
C     LOGICAL MP_UTIL_ESTMPROC
C     LOGICAL MP_UTIL_ESTMAITRE
C     SUBROUTINE MP_UTIL_ERRHNDLR
C     INTEGER MP_UTIL_ASGERR
C     INTEGER MP_UTIL_SYNCERR
C     INTEGER MP_UTIL_REQCOMM
C     INTEGER MP_UTIL_DSYNC
C     INTEGER MP_UTIL_ISYNC
C     INTEGER MP_UTIL_GETOP_SIGNEDMAX
C     INTEGER MP_UTIL_GETOP_SIGNEDMIN
C     SUBROUTINE MP_UTIL_SIGNEDMAX
C     SUBROUTINE MP_UTIL_SIGNEDMIN
C     INTEGER MP_TYPE_IN4
C     INTEGER MP_TYPE_IN8
C     INTEGER MP_TYPE_INT
C     INTEGER MP_TYPE_RE8
C   Private:
C     INTEGER MP_UTIL_SYNC
C     INTEGER MP_UTIL_SYNC_R
C     INTEGER MP_UTIL_SYNC_T
C     INTEGER MP_UTIL_SEND
C     INTEGER MP_UTIL_SENDX
C     INTEGER MP_UTIL_RECV
C     INTEGER MP_UTIL_RECVX
C
C************************************************************************

      MODULE MP_UTIL_M

      IMPLICIT NONE
      PUBLIC  :: MP_UTIL_SYNC
      PRIVATE :: MP_UTIL_SYNC_R
      PRIVATE :: MP_UTIL_SYNC_T

      CONTAINS
      
C************************************************************************
C Sommaire: SYNChronisation.
C
C Description:
C     La fonction privée MP_UTIL_SYNC va synchroniser avec les process
C     voisins les données de type T_BASE de la table générique BVAL.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     BVAL           Table de valeurs castée en table à BYTE
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C     Utilise les appels non bloquants:
C     1) enregistre les RECV le plus tot possible. Elles travaillent avec
C        un buffer
C     2) crée les types
C     3) envoie les données
C     4) attends la fin de transmission
C     5) unpack les données
C
C remarques: i_comm peut être spécialisé pour le groupe
C************************************************************************
      INTEGER
     &FUNCTION MP_UTIL_SYNC (NPROC,
     &                       KPRECR,
     &                       NECR,
     &                       KECRD,
     &                       KECR,
     &                       NLIS,
     &                       KLISD,
     &                       KLIS,
     &                       NVAL,
     &                       NNL,
     &                       BVAL,
     &                       T_BASE)

      IMPLICIT NONE

      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER KECR (NECR)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)
      INTEGER KLIS (NLIS)
      INTEGER NVAL
      INTEGER NNL
      BYTE    BVAL(:)
      INTEGER T_BASE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'soallc.fi'

      INTEGER I_SIZMAX
      PARAMETER (I_SIZMAX = 64)

      INTEGER IERR
      INTEGER IN, IP
      INTEGER L_IDX, N_IDX
      INTEGER L_BUF, N_BUF, BSIZE
      INTEGER NMAX
      INTEGER NNE

      INTEGER I_ERROR
      INTEGER I_COMM, I_SIZE, I_TAGR, I_TAGS
      INTEGER T_NVAL, T_GLOB
      INTEGER N_RQST
      INTEGER K_TSND(I_SIZMAX), K_TRCV(I_SIZMAX)
      INTEGER K_RQST(I_SIZMAX), K_STTS(MPI_STATUS_SIZE, I_SIZMAX)
      PARAMETER (I_TAGR = 1234)
      PARAMETER (I_TAGS = 4321)

      SAVE L_IDX, N_IDX
      SAVE L_BUF, N_BUF
      DATA L_IDX /0/, N_IDX /0/
      DATA L_BUF /0/, N_BUF /0/
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.mpi'

C---     TOUT SEUL ? ==> FIN
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      IF (ERR_BAD()) GOTO 9999
      IF (I_SIZE .LE. 1) GOTO 9999

C---     Crée le sous-type contenant NVAL valeurs contiguës
      T_NVAL = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_TYPE_CONTIGUOUS(NVAL, T_BASE, T_NVAL, I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_TYPE_COMMIT(T_NVAL, I_ERROR)
      ENDIF

C---     Détermine la taille max à lire/écrire
      NMAX = -1
      DO IP=1,NPROC
         NMAX = MAX(NMAX, KECRD(IP+1)-KECRD(IP))
         NMAX = MAX(NMAX, KLISD(IP+1)-KLISD(IP))
      ENDDO
      IF (NMAX .LE. 0) GOTO 9900

C---     Alloue le buffer de reception
      CALL MPI_PACK_SIZE(NMAX, T_NVAL, I_COMM, BSIZE, I_ERROR)
      BSIZE = INT((BSIZE+7) / 8)
      IF (BSIZE .GT. N_BUF) THEN
         N_BUF = BSIZE
         IERR = SO_ALLC_ALLRE8(N_BUF*(NPROC-1), L_BUF)
         WRITE(LOG_BUF, *) 'Increase bufsize: ', BSIZE, '*', NPROC-1
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (ERR_GOOD())
     &   CALL DINIT(N_BUF*(NPROC-1),
     &              0.0D0,
     &              VA(SO_ALLC_REQVIND(VA, L_BUF)),
     &              1)

!C---     Alloue le buffer de reception
!      IF (NLIS*NVAL .GT. N_BUF) THEN
!         N_BUF = NLIS*NVAL
!         CALL MPI_PACK_SIZE(N_BUF, T_NVAL, I_COMM, BSIZE, I_ERROR)
!         IERR = SO_ALLC_ALLIN1(BSIZE, L_BUF)
!      ENDIF
!      IF (ERR_GOOD())
!     &   CALL DINIT(N_BUF, 0.0D0, VA(SO_ALLC_REQVIND(VA, L_BUF)), 1)

C---     Reception non bloquante dans le buffer
      N_RQST = 0
      IF (ERR_GOOD()) THEN
         IERR = MP_UTIL_SYNC_R(N_BUF,
     &                         VA(SO_ALLC_REQVIND(VA, L_BUF)),
     &                         NPROC,
     &                         KPRECR,
     &                         KLISD,
     &                         T_NVAL,
     &                         I_TAGR,
     &                         I_COMM,
     &                         K_RQST)
         N_RQST = NPROC-1
      ENDIF

C---     Alloue la table du nbr d'elem par bloc
      IF (NMAX .GT. N_IDX) THEN
         N_IDX = NMAX
         IERR = SO_ALLC_ALLINT(N_IDX, L_IDX)
         IF (ERR_GOOD())
     &      CALL IINIT(N_IDX, 1, KA(SO_ALLC_REQKIND(KA, L_IDX)), 1)   ! (1)
      ENDIF

C---     Passe des indices aux offsets (inconditionnel)
      DO IN=1,NECR
         KECR(IN) = KECR(IN) - 1
      ENDDO
      DO IN=1,NLIS
         KLIS(IN) = KLIS(IN) - 1
      ENDDO

C---     Crée les types
      DO IP=1,(NPROC-1)
         K_TRCV(IP) = 0
         K_TSND(IP) = 0

C---        Pour la reception
         T_GLOB = 0
         IF (ERR_GOOD()) THEN
            NNE = KLISD(IP+1)-KLISD(IP)
            CALL MPI_TYPE_INDEXED(NNE,
     &                            KA(SO_ALLC_REQKIND(KA, L_IDX)),
     &                            KLIS(KLISD(IP)),
     &                            T_NVAL,
     &                            T_GLOB,
     &                            I_ERROR)
         ENDIF
         IF (ERR_GOOD()) THEN
            CALL MPI_TYPE_COMMIT(T_GLOB, I_ERROR)
         ENDIF
         IF (ERR_GOOD()) K_TRCV(IP) = T_GLOB

C---        Pour l'envoi
         T_GLOB = 0
         IF (ERR_GOOD()) THEN
            NNE = KECRD(IP+1)-KECRD(IP)
            CALL MPI_TYPE_INDEXED(NNE,
     &                            KA(SO_ALLC_REQKIND(KA, L_IDX)),
     &                            KECR(KECRD(IP)),
     &                            T_NVAL,
     &                            T_GLOB,
     &                            I_ERROR)
         ENDIF
         IF (ERR_GOOD()) THEN
            CALL MPI_TYPE_COMMIT(T_GLOB, I_ERROR)
         ENDIF
         IF (ERR_GOOD()) K_TSND(IP) = T_GLOB

      ENDDO

C---     Envoie les données
      DO IP=1,(NPROC-1)
         IF (ERR_GOOD()) THEN
!            N_RQST = N_RQST + 1    ! Pblm de rw sur kval
!            CALL MPI_ISEND(BVAL, 1, K_TSND(IP), KPRECR(IP)-1,
!     &                     I_TAGS, I_COMM, K_RQST(N_RQST), I_ERROR)
            CALL MPI_SEND(BVAL, 1, K_TSND(IP), KPRECR(IP)-1,
     &                    I_TAGR, I_COMM, I_ERROR)
            WRITE(LOG_BUF, *) 'SEND(to, nne): ',
     &                         KPRECR(IP), KECRD(IP+1)-KECRD(IP)
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDDO

C---     Attend la fin de la transmission
      IF (ERR_GOOD()) THEN
         CALL MPI_WAITALL(N_RQST, K_RQST, K_STTS, I_ERROR)
         if (err_bad()) then
            do ip=1,n_rqst
               write(6,*) ip, (k_stts(in,ip),in=1,MPI_STATUS_SIZE)
            enddo
         endif
      ENDIF

C---     Transfert du buffer vers kval
      IF (ERR_GOOD())
     &   IERR = MP_UTIL_SYNC_T(N_BUF,
     &                         VA(SO_ALLC_REQVIND(VA, L_BUF)),
     &                         NPROC,
     &                         KPRECR,
     &                         KLISD,
     &                         K_TRCV,
     &                         BVAL,
     &                         I_COMM)

C---     Libère les types
      DO IP=1,(NPROC-1)
         IF (K_TSND(IP) .NE. 0) THEN
            CALL MPI_TYPE_FREE(K_TSND(IP), I_ERROR)
         ENDIF

         IF (K_TRCV(IP) .NE. 0) THEN
            CALL MPI_TYPE_FREE(K_TRCV(IP), I_ERROR)
         ENDIF
      ENDDO
      IF (T_NVAL .NE. 0) THEN
         CALL MPI_TYPE_FREE(T_NVAL, I_ERROR)
      ENDIF

C---     Repasse des offsets aux indices (inconditionnel)
      DO IN=1,NECR
         KECR(IN) = KECR(IN) + 1
      ENDDO
      DO IN=1,NLIS
         KLIS(IN) = KLIS(IN) + 1
      ENDDO

!C---     Désalloue la table temporaire
!      IF (L_IDX .NE. 0) IERR = SO_ALLC_ALLINT(0, L_IDX)

      GOTO 9999
C-------------------------------------------------------------------------
9900  CONTINUE
      WRITE(ERR_BUF, '()') 'ERR_NMAX_ECR_LEC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MP_UTIL_SYNC = ERR_TYP()
      RETURN
      END FUNCTION MP_UTIL_SYNC

C************************************************************************
C Sommaire: SYNChronisation_R.
C
C Description:
C     La fonction privée MP_UTIL_SYNC_R lance les commandes de reception
C     non bloquantes. Elle permet de résoudre la table BUF.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C
C Notes:
C     La table BUF est utilisée indifférenciée INT,  REAL.
C     La commande MPI_IRECV fait l'équivalent d'un MPI_PACK
C************************************************************************
      INTEGER
     &FUNCTION MP_UTIL_SYNC_R(LBUF1,
     &                        BUF,
     &                        NPROC,
     &                        KPRECR,
     &                        KLISD,
     &                        T_NVAL,
     &                        I_TAGR,
     &                        I_COMM,
     &                        K_RQST)

      IMPLICIT NONE

      INTEGER LBUF1
      REAL*8  BUF   (LBUF1, *)
      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER KLISD (NPROC+1)
      INTEGER T_NVAL
      INTEGER I_TAGR
      INTEGER I_COMM
      INTEGER K_RQST(NPROC-1)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'

      INTEGER IERR
      INTEGER IP
      INTEGER NNE

      INTEGER I_ERROR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.mpi'

C---     Reception non bloquante des données
      DO IP=1,(NPROC-1)
         K_RQST(IP) = 0
         IF (ERR_GOOD()) THEN
            NNE = KLISD(IP+1)-KLISD(IP)
            CALL MPI_IRECV(BUF(1,IP),     ! BUF(KLISD(IP)),
     &                     NNE,
     &                     T_NVAL,
     &                     KPRECR(NPROC-IP)-1,
     &                     I_TAGR,
     &                     I_COMM,
     &                     K_RQST(IP),
     &                     I_ERROR)
            WRITE(LOG_BUF, *) 'IRECV(from, nne, rqst): ',
     &                         KPRECR(NPROC-IP), NNE, K_RQST(IP)
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDDO

      MP_UTIL_SYNC_R = ERR_TYP()
      RETURN
      END FUNCTION MP_UTIL_SYNC_R

C************************************************************************
C Sommaire: SYNChronisation_R.
C
C Description:
C     La fonction privée MP_UTIL_SYNC_R lance les commandes de reception
C     non bloquantes. Elle permet de résoudre la table BUF.
C
C Entrée:
C     NVAL
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     BVAL           Table de valeurs comme table de BYTE
C     T_BASE         Type de donnée à transmettre, p.ex. MPI_INTEGER
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION MP_UTIL_SYNC_T(LBUF1,
     &                        BUF,
     &                        NPROC,
     &                        KPRECR,
     &                        KLISD,
     &                        K_TYP,
     &                        BVAL,
     &                        I_COMM)

      IMPLICIT NONE

      INTEGER LBUF1
      REAL*8  BUF   (LBUF1, *)
      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER KLISD (NPROC+1)
      INTEGER K_TYP (NPROC-1)
      BYTE    BVAL  (*)
      INTEGER I_COMM

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER IERR
      INTEGER IP

      INTEGER I_ERROR, I_POS, I_TSIZ, I_TYP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      DO IP=1,(NPROC-1)
         I_POS = 0
         I_TYP = K_TYP(IP)
         IF (ERR_GOOD()) THEN
            CALL MPI_PACK_SIZE(1,               ! count
     &                         I_TYP,           ! datatype
     &                         I_COMM,          !
     &                         I_TSIZ,          ! size in byte
     &                         I_ERROR)
         ENDIF
         IF (ERR_GOOD()) THEN
            CALL MPI_UNPACK   (BUF(1,IP),       ! buffer start
     &                         I_TSIZ,          ! size of buf in bytes
     &                         I_POS,           ! 0
     &                         BVAL,            ! output buffer
     &                         1,               ! nbr item to unpack
     &                         I_TYP,           ! datatype
     &                         I_COMM,
     &                         I_ERROR)

         ENDIF
      ENDDO

      MP_UTIL_SYNC_T = ERR_TYP()
      RETURN
      END FUNCTION MP_UTIL_SYNC_T
      
      END MODULE MP_UTIL_M


C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à CI_CINC_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA MP_UTIL_DATA

      IMPLICIT NONE

      INCLUDE 'mputil.fc'

      DATA MP_UTIL_ERRCTX  / ' ' /

      END

C************************************************************************
C Sommaire: Retourne le type INTEGER*4.
C
C Description:
C     La fonction MP_TYPE_IN4 retourne le type MPI correspondant
C     au type FORTRAN INTEGER*4.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_TYPE_IN4()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_TYPE_IN4
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'
C----------------------------------------------------------------------------

      MP_TYPE_IN4 = MPI_INTEGER4
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le type INTEGER*4.
C
C Description:
C     La fonction MP_TYPE_IN8 retourne le type MPI correspondant
C     au type FORTRAN INTEGER*8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_TYPE_IN8()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_TYPE_IN8
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'
C----------------------------------------------------------------------------

      MP_TYPE_IN8 = MPI_INTEGER8
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le type INTEGER.
C
C Description:
C     La fonction MP_TYPE_INT retourne le type MPI correspondant
C     au type FORTRAN INTEGER.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     MPICH2 n'a pas d'opération de réduction pour les types spécialisés
C     INTEGER4 et INTEGER8
C************************************************************************
      FUNCTION MP_TYPE_INT()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_TYPE_INT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'mpif.h'

      INTEGER I_SIZE, I_ERROR

      INTEGER I_TYPE
      SAVE I_TYPE
      DATA I_TYPE /0/
C----------------------------------------------------------------------------

      IF (I_TYPE .EQ. 0) THEN
         CALL MPI_TYPE_SIZE(MPI_INTEGER, I_SIZE, I_ERROR)
         IF (C_MM_REQINTSIZ() .EQ. I_SIZE) THEN    ! cf. note
            I_TYPE = MPI_INTEGER
         ELSEIF (C_MM_REQINTSIZ() .EQ. 4) THEN
            I_TYPE = MPI_INTEGER4
D           CALL MPI_TYPE_SIZE(MPI_INTEGER, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 4)
D           CALL MPI_TYPE_SIZE(MPI_INTEGER4, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 4)
D           CALL MPI_TYPE_SIZE(MPI_INTEGER8, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 8)
D           CALL MPI_TYPE_SIZE(MPI_2INTEGER, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 8)
         ELSEIF (C_MM_REQINTSIZ() .EQ. 8) THEN
            I_TYPE = MPI_INTEGER8
D           CALL MPI_TYPE_SIZE(MPI_INTEGER, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 8)
D           CALL MPI_TYPE_SIZE(MPI_INTEGER4, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 4)
D           CALL MPI_TYPE_SIZE(MPI_INTEGER8, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 8)
D           CALL MPI_TYPE_SIZE(MPI_2INTEGER, I_SIZE, I_ERROR)
D           CALL ERR_ASR(I_SIZE .EQ. 16)
D        ELSE
D           CALL ERR_ASR(C_MM_REQINTSIZ() .EQ. 4)
         ENDIF
      ENDIF
      MP_TYPE_INT = I_TYPE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le type REAL*8.
C
C Description:
C     La fonction MP_TYPE_RE8 retourne le type MPI correspondant
C     au type FORTRAN REAL*8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_TYPE_RE8()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_TYPE_RE8
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'
C----------------------------------------------------------------------------

      MP_TYPE_RE8 = MPI_REAL8
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si on a plusieurs process.
C
C Description:
C     La fonction MP_UTIL_ESTMPROC retourne .TRUE. si plusieurs process
C     participent à la résolution.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_ESTMPROC()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ESTMPROC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'

      INTEGER I_SIZE, I_ERROR
C----------------------------------------------------------------------------

      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)

      MP_UTIL_ESTMPROC = (I_SIZE .GT. 1)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si on a plusieurs process.
C
C Description:
C     La fonction MP_UTIL_ESTMAITRE retourne .TRUE. si le process
C     est le process maître (process de rang 0).
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_ESTMAITRE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ESTMAITRE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER I_RANK, I_ERROR
C----------------------------------------------------------------------------

      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

      MP_UTIL_ESTMAITRE = (I_RANK .EQ. I_MASTER)
      RETURN
      END

C************************************************************************
C Sommaire: Traduis l'erreur MPI en erreur standard.
C
C Description:
C     La fonction MP_UTIL_ASGERR transforme le message d'erreur retourné par
C     MPI en une erreur standard.
C
C Entrée:
C     INTEGER       I_ERROR            Code d'erreur de MPI
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE MP_UTIL_ERRHNDLR(I_CNTX, I_ERROR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ERRHNDLR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I_CNTX
      INTEGER I_ERROR

      INCLUDE 'mputil.fi'

      INTEGER IERR
C----------------------------------------------------------------------------

      IERR = MP_UTIL_ASGERR(I_ERROR)

      RETURN
      END

C************************************************************************
C Sommaire: Traduis l'erreur MPI en erreur standard.
C
C Description:
C     La fonction MP_UTIL_ASGERR transforme le message d'erreur retourné par
C     MPI en une erreur standard.
C
C Entrée:
C     INTEGER       I_ERROR            Code d'erreur de MPI
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_ASGERR(I_ERROR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ASGERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I_ERROR

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mputil.fc'

      INTEGER IERR
      INTEGER I_CLASS
      INTEGER L_ERRSTR
      CHARACTER*(MPI_MAX_ERROR_STRING) I_ERRSTR
C----------------------------------------------------------------------------

      IF (I_ERROR .NE. MPI_SUCCESS) THEN
         CALL MPI_ERROR_CLASS (I_ERROR, I_CLASS, IERR)
         CALL MPI_ERROR_STRING(I_CLASS, I_ERRSTR, L_ERRSTR, IERR)
         L_ERRSTR = MIN(L_ERRSTR, LEN(ERR_BUF))
         CALL ERR_ASG(ERR_FTL, I_ERRSTR(1:L_ERRSTR))

         CALL MPI_ERROR_STRING(I_ERROR, I_ERRSTR, L_ERRSTR, IERR)
         L_ERRSTR = MIN(L_ERRSTR, LEN(ERR_BUF))
         ERR_BUF = I_ERRSTR(1:L_ERRSTR)
         CALL SP_STRN_TRM(ERR_BUF)
         CALL ERR_AJT(ERR_BUF)
         
         IF (SP_STRN_LEN(MP_UTIL_ERRCTX) .GT. 0) THEN
            CALL ERR_AJT('MSG_CONTEXTE:')
            CALL ERR_AJT(MP_UTIL_ERRCTX)
         ENDIF
      ENDIF

      MP_UTIL_ASGERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un contexte au message d'erreur.
C
C Description:
C     La fonction MP_UTIL_ASGERRCTX assigne un contexte aux erreurs.
C     Le message sera ajouté aux message d'erreur MPI.
C
C Entrée:
C     CHARACTER*(*) ERR_CTX
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_ASGERRCTX(ERR_CTX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ASGERRCTX
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ERR_CTX

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mputil.fc'

      INTEGER LERR, LMAX
C----------------------------------------------------------------------------
      
      LERR = SP_STRN_LEN(ERR_CTX)
      IF (LERR .EQ. 0) THEN
         MP_UTIL_ERRCTX = ' '
      ELSE
         LMAX = LEN(MP_UTIL_ERRCTX)
         MP_UTIL_ERRCTX = ERR_CTX(1:MIN(LERR, LMAX))
      ENDIF
      
      MP_UTIL_ASGERRCTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Synchronise le message d'erreur
C
C Description:
C     La subroutine MP_UTIL_SYNCERR synchronise le message d'erreur entre
C     tous les process. C'est le process avec le type d'erreur le plus
C     élevé qui "gagne".
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_SYNCERR()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_SYNCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mpif.h'

      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER I
      INTEGER ITYP, ITPG, NMSG
      INTEGER I_RANK, I_SIZE, I_SEND, I_ERROR, I_COMM
      CHARACTER*256 MSG
      LOGICAL ISSENDER
C----------------------------------------------------------------------------

      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      IF (I_SIZE .LE. 1) GOTO 9999
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Réduis le type et détermine le sender
      ITYP = ERR_TYP()*1000 + I_SIZE - I_RANK
      ITPG = -1
      CALL MPI_ALLREDUCE(ITYP, ITPG, 1, MP_TYPE_INT(), MPI_MAX,
     &                   I_COMM, I_ERROR)
      ITYP = ITPG / 1000
      I_SEND = I_SIZE - (ITPG - ITYP*1000)

C---     Envoie le message
      ISSENDER = (I_RANK .EQ. I_SEND)
      IF (ITYP .EQ. ERR_OK) THEN
         CALL ERR_RESET()
      ELSE
         NMSG = ERR_NBR()
         CALL MPI_BCAST(NMSG, 1,MP_TYPE_INT(),I_SEND,I_COMM,I_ERROR)
D        CALL ERR_ASR(NMSG .GT. 0)

C---        Assigne le premier message
         IF (ISSENDER) MSG = ERR_MSG(1)
         CALL MPI_BCAST(MSG, 256, MPI_CHARACTER,
     &                  I_SEND, I_COMM, I_ERROR)
         IF (.NOT. ISSENDER) CALL ERR_ASG(ITYP, MSG(1:SP_STRN_LEN(MSG)))

C---        Ajoute les messages suivants
         DO I=2,NMSG
            IF (ISSENDER) MSG = ERR_MSG(I)
            CALL MPI_BCAST(MSG, 256, MPI_CHARACTER,
     &                     I_SEND, I_COMM, I_ERROR)
            IF (.NOT. ISSENDER) CALL ERR_AJT(MSG(1:SP_STRN_LEN(MSG)))
         ENDDO
      ENDIF

9999  CONTINUE      
      MP_UTIL_SYNCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le communicateur principal (COMM_WORLD).
C
C Description:
C     La fonction MP_UTIL_REQCOMM retourne le communicateur principal,
C     l'équivalent pour le programme de MPI_COMM_WORLD.
C     <p>
C     Sur certaine machines, il n'est pas possible d'utiliser MPI_COMM_WORLD.
C     Ce point d'entrée factorise l'accès au communicateur principal.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_REQCOMM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_REQCOMM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mputil.fi'
      INCLUDE 'mpif.h'

      INTEGER I_COMM, I_ERROR
      SAVE I_COMM
      DATA I_COMM /MPI_COMM_WORLD/
C----------------------------------------------------------------------------

      IF (I_COMM .EQ. MPI_COMM_WORLD) THEN
         CALL MPI_COMM_DUP(MPI_COMM_WORLD, I_COMM, I_ERROR)
      ENDIF
      MP_UTIL_REQCOMM = I_COMM
      RETURN
      END

C************************************************************************
C Sommaire: Double (real*8) SYNChronisation.
C
C Description:
C     La subroutine MP_UTIL_DSYNC va synchroniser avec les process
C     voisins les données REAL*8 de la table VVAL.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     NVAL           Dimension 1 de la table VVAL
C     NNL            Dimension 2 de la table VVAL
C     VVAL           Table de valeurs VVAL(NVAL, NNL)
C
C Notes:
C     Pour satisfaire le compilateur Intel qui contrôle les interfaces
C************************************************************************
      FUNCTION MP_UTIL_DSYNC (NPROC,
     &                        KPRECR,
     &                        NECR,
     &                        KECRD,
     &                        KECR,
     &                        NLIS,
     &                        KLISD,
     &                        KLIS,
     &                        NVAL,
     &                        NNL,
     &                        VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_DSYNC
CDEC$ ENDIF

      USE MP_UTIL_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER KECR (NECR)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)
      INTEGER KLIS (NLIS)
      INTEGER NVAL
      INTEGER NNL
      REAL*8  VVAL(NVAL, NNL)

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = MP_UTIL_SYNC(NPROC,
     &                    KPRECR,
     &                    NECR, KECRD, KECR,
     &                    NLIS, KLISD, KLIS,
     &                    NVAL, NNL, SO_ALLC_CST2B(VVAL(:,1)),
     &                    MP_TYPE_RE8())

      MP_UTIL_DSYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Integer SYNChronisation.
C
C Description:
C     La subroutine MP_UTIL_ISYNC va synchroniser avec les process
C     voisins les données INTEGER de la table KVAL.
C
C Entrée:
C     NPROC          Nombre de process
C     KPRECR         Table des process à écrire
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table cumulative du nombre de noeuds à écrire par process
C     KECR           Table des noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table cumulative du nombre de noeuds à lire par process
C     KLIS           Table des noeuds à lire
C     NVAL           Dimension 1 de la table KVAL
C     NNL            Dimension 2 de la table KVAL
C     KVAL           Table de valeurs KVAL(NVAL, NNL)
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_ISYNC (NPROC,
     &                        KPRECR,
     &                        NECR,
     &                        KECRD,
     &                        KECR,
     &                        NLIS,
     &                        KLISD,
     &                        KLIS,
     &                        NVAL,
     &                        NNL,
     &                        KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_ISYNC
CDEC$ ENDIF

      USE SO_ALLC_M
      USE MP_UTIL_M
      IMPLICIT NONE

      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER KECR (NECR)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)
      INTEGER KLIS (NLIS)
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL(NVAL, NNL)

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = MP_UTIL_SYNC(NPROC,
     &                    KPRECR,
     &                    NECR, KECRD, KECR,
     &                    NLIS, KLISD, KLIS,
     &                    NVAL, NNL, SO_ALLC_CST2B(KVAL(:,1)),
     &                    MP_TYPE_INT())

      MP_UTIL_ISYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MP_UTIL_SEND
C
C Description:
C     La fonction MP_UTIL_SEND envoie au process IPROC la table
C     pleine KVAL. Elle fonctionne aussi bien pour des tables entières
C     que des tables réelles.
C
C Entrée:
C     IPROC          Process à écrire
C     NECR           Nombre de noeuds à écrire
C     KVAL           Table de valeurs
C     I_TYPE         Type de la table
C
C Notes:
C
C************************************************************************
      FUNCTION MP_UTIL_SEND (IPROC,
     &                       NECR,
     &                       KVAL,
     &                       I_TYPE)

      IMPLICIT NONE

      INTEGER IPROC
      INTEGER NECR
      INTEGER KVAL (*)
      INTEGER I_TYPE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER IERR
      INTEGER I_ERROR, I_TAG
      PARAMETER (I_TAG = 1234)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NECR .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL MPI_SEND(KVAL, NECR, I_TYPE, IPROC-1,
     &              I_TAG, MP_UTIL_REQCOMM(), I_ERROR)

      MP_UTIL_SEND = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire: MP_UTIL_SENDX
C
C Description:
C     La fonction MP_UTIL_SENDX envoie au process IPROC la table
C     KVAL indexée suivant KECR. Elle fonctionne aussi bien pour des tables
C     entières que des tables réelles.
C
C Entrée:
C     IPROC          Process à écrire
C     NECR           Nombre de noeuds à écrire
C     KECR           Table des noeuds à écrire
C     K_UN           Table auxiliaire de 1
C     KVAL           Table de valeurs
C     I_TYPE         Type de la table
C
C Notes:
C
C************************************************************************
      FUNCTION MP_UTIL_SENDX (IPROC,
     &                        NECR,
     &                        KECR,
     &                        K_UN,
     &                        KVAL,
     &                        I_TYPE)

      IMPLICIT NONE

      INTEGER IPROC
      INTEGER NECR
      INTEGER KECR (NECR)
      INTEGER K_UN (NECR)
      INTEGER KVAL (*)
      INTEGER I_TYPE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER IERR
      INTEGER I_ERROR, I_TAG, I_NEW_TYPE
      PARAMETER (I_TAG = 1234)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NECR .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     ENVOIE LES NOEUDS
      CALL MPI_TYPE_INDEXED(NECR, K_UN, KECR,
     &                      I_TYPE, I_NEW_TYPE, I_ERROR)
      CALL MPI_TYPE_COMMIT(I_NEW_TYPE, I_ERROR)

      CALL MPI_SEND(KVAL, 1, I_NEW_TYPE, IPROC-1,
     &              I_TAG, MP_UTIL_REQCOMM(), I_ERROR)

      CALL MPI_TYPE_FREE(I_NEW_TYPE, I_ERROR)

      MP_UTIL_SENDX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MP_UTIL_RECV
C
C Description:
C     La fonction MP_UTIL_RECV reçois du processus IPROC la table
C     pleine KVAL. Elle fonctionne aussi bien pour des tables entières
C     que des tables réelles.
C
C Entrée:
C     IPROC          Process à écrire
C     NLIS           Nombre de noeuds à lire
C     KVAL           Table de valeurs
C     I_TYPE         Type de la table
C
C Notes:
C
C************************************************************************
      FUNCTION MP_UTIL_RECV (IPROC,
     &                       NLIS,
     &                       KVAL,
     &                       I_TYPE)

      IMPLICIT NONE

      INTEGER IPROC
      INTEGER NLIS
      INTEGER KVAL (*)
      INTEGER I_TYPE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER I_STATUS(MPI_STATUS_SIZE)

      INTEGER IERR
      INTEGER I_ERROR, I_TAG
      PARAMETER (I_TAG = 1234)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NLIS .GT. 0)
C-----------------------------------------------------------------------

      CALL MPI_RECV(KVAL, NLIS, I_TYPE, IPROC-1,
     &              I_TAG, MP_UTIL_REQCOMM(), I_STATUS, I_ERROR)

      MP_UTIL_RECV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MP_UTIL_RECVX
C
C Description:
C     La fonction MP_UTIL_RECVX reçois du processus IPROC la table KVAL
C     indexée suivant KLIS. Elle fonctionne aussi bien pour des tables entières
C     que des tables réelles.
C
C Entrée:
C     IPROC          Process à écrire
C     NLIS           Nombre de noeuds à lire
C     KLIS           Table des noeuds à lire
C     K_UN           Table de 1
C     KVAL           Table de valeurs
C     I_TYPE         Type de la table
C
C Notes:
C
C************************************************************************
      FUNCTION MP_UTIL_RECVX(IPROC,
     &                       NLIS,
     &                       KLIS,
     &                       K_UN,
     &                       KVAL,
     &                       I_TYPE)

      IMPLICIT NONE

      INTEGER IPROC
      INTEGER NLIS
      INTEGER KLIS (NLIS)
      INTEGER K_UN (NLIS)
      INTEGER KVAL (*)
      INTEGER I_TYPE

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'

      INTEGER I_STATUS(MPI_STATUS_SIZE)

      INTEGER IERR
      INTEGER I_ERROR, I_TAG, I_NEW_TYPE
      PARAMETER (I_TAG = 1234)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NLIS .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---        RECOIS LES DONNÉES
      CALL MPI_TYPE_INDEXED(NLIS, K_UN, KLIS,
     &                      I_TYPE, I_NEW_TYPE, I_ERROR)
      CALL MPI_TYPE_COMMIT(I_NEW_TYPE, I_ERROR)

      CALL MPI_RECV(KVAL, 1, I_NEW_TYPE, IPROC-1,
     &              I_TAG, MP_UTIL_REQCOMM(), I_STATUS, I_ERROR)

      CALL MPI_TYPE_FREE(I_NEW_TYPE, I_ERROR)

      MP_UTIL_RECVX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  MP_UTIL_GETOP_SIGNEDMAX
C
C Description:
C     La fonction MP_UTIL_GETOP_SIGNEDMAX retourne le handle MPI sur
C     l'opération de réduction SIGNEDMAX.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_GETOP_SIGNEDMAX()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_GETOP_SIGNEDMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
      INTEGER I_ERROR, I_OPSIGNEDMAX
      EXTERNAL MP_UTIL_SIGNEDMAX

      DATA I_OPSIGNEDMAX /MPI_OP_NULL/
      SAVE I_OPSIGNEDMAX
C-----------------------------------------------------------------------

      IF (ERR_GOOD() .AND. I_OPSIGNEDMAX .EQ. MPI_OP_NULL) THEN
         CALL MPI_OP_CREATE(MP_UTIL_SIGNEDMAX, .TRUE.,
     &                      I_OPSIGNEDMAX, I_ERROR)
      ENDIF

      MP_UTIL_GETOP_SIGNEDMAX = I_OPSIGNEDMAX
      RETURN
      END

C************************************************************************
C Sommaire:  MP_UTIL_GETOP_SIGNEDMIN
C
C Description:
C     La fonction MP_UTIL_GETOP_SIGNEDMIN retourne le handle MPI sur
C     l'opération de réduction SIGNEDMIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MP_UTIL_GETOP_SIGNEDMIN()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_GETOP_SIGNEDMIN
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
      INTEGER I_ERROR, I_OPSIGNEDMIN
      EXTERNAL MP_UTIL_SIGNEDMIN

      DATA I_OPSIGNEDMIN /MPI_OP_NULL/
      SAVE I_OPSIGNEDMIN
C-----------------------------------------------------------------------

      IF (ERR_GOOD() .AND. I_OPSIGNEDMIN .EQ. MPI_OP_NULL) THEN
         CALL MPI_OP_CREATE(MP_UTIL_SIGNEDMIN, .TRUE.,
     &                      I_OPSIGNEDMIN, I_ERROR)
      ENDIF

      MP_UTIL_GETOP_SIGNEDMIN = I_OPSIGNEDMIN
      RETURN
      END

C************************************************************************
C Sommaire:    Op MPI de max signé.
C
C Description:
C     La fonction MP_UTIL_SIGNEDMAX est une opération MPI de réduction.
C     Elle calcule le max en conservant le signe.
C
C Entrée:
C      ILEN             Longueur des tables
C      ITYPE            Type des données
C      INVEC(ILEN)      Table 1
C      INOUTVEC(ILEN)   Table 2 et résultat
C
C Sortie:
C      INOUTVEC(ILEN)   Table résultat
C
C Notes:
C     gcc-4.1 n'aime pas les lignes d'INCLUDE comme ligne D (debug)
C************************************************************************
      SUBROUTINE MP_UTIL_SIGNEDMAX (INVEC, INOUTVEC, ILEN, ITYPE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_SIGNEDMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN, ITYPE
      REAL*8  INVEC   (ILEN)
      REAL*8  INOUTVEC(ILEN)

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'

      INTEGER I
C------------------------------------------------------------------------
D     CALL ERR_ASR(ITYPE .EQ. MP_TYPE_RE8())
C------------------------------------------------------------------------

      DO I=1,ILEN
         IF (ABS(INVEC(I)) .GT. ABS(INOUTVEC(I))) THEN
            INOUTVEC(I) = INVEC(I)
         ENDIF
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:    Op MPI de min signé.
C
C Description:
C     La fonction MP_UTIL_SIGNEDMIN est une opération MPI de réduction.
C     Elle calcule le min en conservant le signe.
C
C Entrée:
C      ILEN             Longueur des tables
C      ITYPE            Type des données
C      INVEC(ILEN)      Table 1
C      INOUTVEC(ILEN)   Table 2 et résultat
C
C Sortie:
C      INOUTVEC(ILEN)   Table résultat
C
C Notes:
C     gcc-4.1 n'aime pas les lignes d'INCLUDE comme ligne D (debug)
C************************************************************************
      SUBROUTINE MP_UTIL_SIGNEDMIN (INVEC, INOUTVEC, ILEN, ITYPE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MP_UTIL_SIGNEDMIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN, ITYPE
      REAL*8  INVEC   (ILEN)
      REAL*8  INOUTVEC(ILEN)

      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'

      INTEGER I
C------------------------------------------------------------------------
D     CALL ERR_ASR(ITYPE .EQ. MP_TYPE_RE8())
C------------------------------------------------------------------------

      DO I=1,ILEN
         IF (ABS(INVEC(I)) .LT. ABS(INOUTVEC(I))) THEN
            INOUTVEC(I) = INVEC(I)
         ENDIF
      ENDDO

      RETURN
      END
