C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:   Interface de Commandes
C Objet:    FichierINPut
C
C Functions:
C   Public:
C     INTEGER IC_FINP_000
C     INTEGER IC_FINP_999
C     INTEGER IC_FINP_PKL
C     INTEGER IC_FINP_UPK
C     INTEGER IC_FINP_CTR
C     INTEGER IC_FINP_DTR
C     INTEGER IC_FINP_INI
C     INTEGER IC_FINP_RST
C     INTEGER IC_FINP_REQHBASE
C     LOGICAL IC_FINP_HVALIDE
C     INTEGER IC_FINP_LISCMD
C     INTEGER IC_FINP_REQCRC
C     INTEGER IC_FINP_REQLCMD
C     INTEGER IC_FINP_REQLCUR
C     INTEGER IC_FINP_REQNOM
C   Private:
C     INTEGER IC_FINP_RAZ
C     INTEGER IC_FINP_OPEN
C     INTEGER IC_FINP_LISCMND
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfinp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_FINP_NOBJMAX,
     &                   IC_FINP_HBASE,
     &                   'Command Interface Input File')

      IC_FINP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfinp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fc'

      INTEGER  IERR
      EXTERNAL IC_FINP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_FINP_NOBJMAX,
     &                   IC_FINP_HBASE,
     &                   IC_FINP_DTR)

      IC_FINP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LB
      INTEGER MR
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_FINP_HBASE
      MR = IC_FINP_MR(IOB)

C---     Récupère le nom de fichier
      BUF = ' '
      IF (MR .GT. 0) INQUIRE(UNIT = MR, ERR= 9900, NAME=BUF)
      LB = MAX(1, SP_STRN_LEN(BUF))

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WS  (HXML, BUF(1:LB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, IC_FINP_CRC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, IC_FINP_MR (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, IC_FINP_LC (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, IC_FINP_LD (IOB))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FINP_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ICRC, MR
      CHARACTER*(256) BUF
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_FINP_HBASE

C---     Initialise l'objet
      IF (LNEW) IERR = IC_FINP_RAZ(HOBJ)
      IERR = IC_FINP_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RS  (HXML, BUF)
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, IC_FINP_CRC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, IC_FINP_MR (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, IC_FINP_LC (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, IC_FINP_LD (IOB))

C---     Ouvre le fichier
      ICRC = 0
      MR = IC_FINP_MR(IOB)
      IF (ERR_GOOD() .AND. (MR .NE. 0 .AND. MR .NE. 5)) THEN
         IC_FINP_MR(IOB) = IO_UTIL_FREEUNIT()
         IERR = IC_FINP_OPEN(IC_FINP_MR(IOB),
     &                       BUF(1:SP_STRN_LEN(BUF)),
     &                       IC_FINP_LC(IOB),
     &                       ICRC)
      ENDIF
D     CALL ERR_ASR(ERR_BAD() .OR. ICRC .EQ. IC_FINP_CRC(IOB))

C---     Ouvre le fichier
      IC_FINP_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   IC_FINP_NOBJMAX,
     &                                   IC_FINP_HBASE)
      IF (ERR_GOOD()) IERR = IC_FINP_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. IC_FINP_HVALIDE(HOBJ))

      IC_FINP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_FINP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_FINP_NOBJMAX,
     &                   IC_FINP_HBASE)

      IC_FINP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: eRaze
C
C Description:
C     La fonction IC_FINP_RAZ fait une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de désallocation, etc ....
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_FINP_HBASE

      IC_FINP_CRC(IOB) = 0
      IC_FINP_MR (IOB) = 0
      IC_FINP_LC (IOB) = 0
      IC_FINP_LD (IOB) = 0

      IC_FINP_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il n'est pas clair si il faut synchroniser l'unité et la ligne
C************************************************************************
      FUNCTION IC_FINP_INI(HOBJ, FNAME, ILC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ILC

      INCLUDE 'icfinp.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I, ICRC
      INTEGER I_RANK, I_ERROR
      INTEGER LFNAME
      INTEGER MR
      LOGICAL AFICHIER, OUVREFICHIER
      CHARACTER*(256) BUFF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RESET LES DONNEES
      IERR = IC_FINP_RST(HOBJ)

C---     Initialise
      MR = 5                  ! STDINP
      AFICHIER     = .FALSE.
      OUVREFICHIER = .FALSE.

C---     Contrôle les param
      IF (SP_STRN_LEN(FNAME) .LE. 0) GOTO 9900

C---     Détermine si on doit ouvrir le fichier
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      LFNAME = SP_STRN_LEN(FNAME)
      AFICHIER = (LFNAME .NE. 6 .OR. FNAME(1:LFNAME) .NE. 'STDINP')
      OUVREFICHIER = (I_RANK .EQ. I_MASTER .AND. AFICHIER)

C---     Ouvre le fichier
      ICRC = 0
      IF (OUVREFICHIER) THEN
         MR = IO_UTIL_FREEUNIT()
         IERR = IC_FINP_OPEN(MR, FNAME(1:LFNAME), ILC, ICRC)
      ENDIF

C---     Sync error
      IERR = MP_UTIL_SYNCERR()

!!!C---     Broadcast l'unité
!!!      CALL MPI_BCAST(MR, 1, MP_TYPE_INT(),
!!!     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
!!!D     CALL ERR_ASR(ERR_GOOD())
!!!
!!!C---     Broadcast la ligne
!!!      CALL MPI_BCAST(ILC___, 1, MP_TYPE_INT(),
!!!     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
!!!D     CALL ERR_ASR(ERR_GOOD())

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - IC_FINP_HBASE
         IC_FINP_CRC(IOB) = ICRC
         IC_FINP_MR (IOB) = MR
         IC_FINP_LC (IOB) = MAX(0,ILC)
         IC_FINP_LD (IOB) = IC_FINP_LC(IOB)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE    ! COMPLETE LE MESSAGE D'ERREUR
      IF (ERR_BAD() .AND. OUVREFICHIER) THEN
         WRITE(ERR_BUF, '(3A)') '#<3>#MSG_FICHIER', ': ',
     &         FNAME(1:MIN(128,SP_STRN_LEN(FNAME)))
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      IC_FINP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_ERROR, I_RANK
      INTEGER MR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_FINP_HBASE
      MR = IC_FINP_MR(IOB)

C---     Détermine si on doit fermer le fichier
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le nom du fichier
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (MR .NE. 0 .AND. MR .NE. 5) THEN
            CLOSE(UNIT= MR, ERR= 101)
            GOTO 109
101         CONTINUE    ! ERR
            IERR = -1
109         CONTINUE
         ENDIF
      ENDIF

C---     Broadcast le code d'erreur
      CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
      IF (I_ERROR .EQ. MPI_SUCCESS .AND. IERR .EQ. -1) GOTO 9900
D     CALL ERR_ASR(I_ERROR .NE. MPI_SUCCESS .OR. IERR .EQ. 0)

C---     Efface les attributs
      IERR = IC_FINP_RAZ(HOBJ)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FERME_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FINP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_FINP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfinp.fi'
      INCLUDE 'icfinp.fc'
C------------------------------------------------------------------------

      IC_FINP_REQHBASE = IC_FINP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_FINP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icfinp.fc'
C------------------------------------------------------------------------

      IC_FINP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_FINP_NOBJMAX,
     &                                  IC_FINP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Lis une commande
C
C Description:
C     La fonction IC_FINP_LISCMD lis une commande complète de
C     l'unité fichier MR dans un contexte MPI. La commande peut
C     être sur plusieurs lignes. La commande est lue par le
C     process MASTER puis envoyée aux autres process du groupe.
C
C Entrée:
C     INTEGER       HOBJ      Handle sur l'objet courant
C
C Sortie:
C     INTEGER       LD        Ligne de début
C     INTEGER       LF        Ligne de fin
C     CHARACTER*(*) STRING    Chaîne contenant la commande
C
C Notes:
C     La ligne de fichier source peut être différente de la ligne
C     de début. Ell
C************************************************************************
      FUNCTION IC_FINP_LISCMD(HOBJ, LD, LF, STRING)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_LISCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER LD, LF
      CHARACTER*(*) STRING

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_ERROR, I_RANK
      INTEGER LS
      INTEGER MR
      INTEGER LSTR
C----------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - IC_FINP_HBASE
      MR = IC_FINP_MR(IOB)
      LF = IC_FINP_LC(IOB)    ! Ligne courante
      LD = IC_FINP_LD(IOB)    ! Ligne de début
      LS = IC_FINP_LS(IOB)    ! Ligne du fichier source

C---     Lis la ligne
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      LSTR = LEN(STRING)
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = IC_FINP_LISCMND(MR, LF, LD, LS, LSTR, STRING)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast l'info
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(LF, 1, MP_TYPE_INT(),
     &                  I_MASTER, MP_UTIL_REQCOMM(),
     &                  I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(LD, 1, MP_TYPE_INT(),
     &                  I_MASTER, MP_UTIL_REQCOMM(),
     &                  I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(STRING, LSTR, MPI_CHARACTER,
     &                  I_MASTER, MP_UTIL_REQCOMM(),
     &                  I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_BARRIER(MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) CALL LOG_ECRIS(STRING)

C---     Assigne les attributs même en cas d'erreur
      IC_FINP_LC(IOB) = LF
      IC_FINP_LD(IOB) = LD
      IC_FINP_LS(IOB) = LS

      IC_FINP_LISCMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le CRC32 du nom du fichier.
C
C Description:
C     La fonction IC_FINP_REQCRC retourne le CRC32 du nom de fichier.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQCRC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQCRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER ICRC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Seul I_MASTER garde l'info
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le CRC
      ICRC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IOB = HOBJ - IC_FINP_HBASE
         ICRC = IC_FINP_CRC(IOB)
      ENDIF

C---     Broadcast l'info
      CALL ERR_PUSH()
      CALL MPI_BCAST(ICRC, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
D     CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()

      IC_FINP_REQCRC = ICRC
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne la ligne de la commande.
C
C Description:
C     La fonction IC_FINP_REQLCMD retourne la ligne de la dernière commande
C     lue, soit sa ligne de début.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQLCMD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQLCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER LD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Seul I_MASTER garde l'info
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le numéro de ligne
      LD = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IOB = HOBJ - IC_FINP_HBASE
         LD = IC_FINP_LD(IOB)
      ENDIF

C---     Broadcast l'info
      CALL ERR_PUSH()
      CALL MPI_BCAST(LD, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
D     CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()

      IC_FINP_REQLCMD = LD
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne la ligne courante du fichier.
C
C Description:
C     La fonction IC_FINP_REQLCUR retourne la ligne courante du fichier.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQLCUR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQLCUR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER LC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Seul I_MASTER garde l'info
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le numéro de ligne
      LC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IOB = HOBJ - IC_FINP_HBASE
         LC = IC_FINP_LC(IOB)
      ENDIF

C---     Broadcast
      CALL ERR_PUSH()
      CALL MPI_BCAST(LC, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
D     CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()

      IC_FINP_REQLCUR = LC
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne la ligne de la commande dans le fichier source.
C
C Description:
C     La fonction IC_FINP_REQLSRC retourne la ligne la ligne de début de
C     la dernière command lue, dans le fichier source.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQLSRC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQLSRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER LS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Seul I_MASTER garde l'info
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le numéro de ligne
      LS = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IOB = HOBJ - IC_FINP_HBASE
         LS = IC_FINP_LS(IOB)
      ENDIF

C---     Broadcast l'info
      CALL ERR_PUSH()
      ! Impossible de contrôler le retour, car RESLSRC
      ! est utilisé pour dépiler en cas d'erreur,
      ! dont des erreurs MPI
      CALL MPI_BCAST(LS, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
      CALL ERR_POP()

      IC_FINP_REQLSRC = LS
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le nom du fichier.
C
C Description:
C     La fonction IC_FINP_REQNOM retourne le nom du fichier.
C
C Entrée:
C     HOBJ           Handle sur l'objet courant
C
C Sortie:
C     FNAME          Nom du fichier
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_REQNOM(HOBJ, FNAME)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FINP_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME

      INCLUDE 'icfinp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'icfinp.fc'

      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_ERROR
      INTEGER MR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FINP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Initialise
      FNAME = ' '

C---     Seul I_MASTER garde l'info
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Le nom du fichier
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IOB = HOBJ - IC_FINP_HBASE
         MR = IC_FINP_MR(IOB)
         IF (MR .NE. 5) THEN
            INQUIRE(UNIT = MR, ERR= 101, NAME=FNAME)
            GOTO 109
101         CONTINUE    ! ERR
            IERR = -1
109         CONTINUE
         ELSE
            FNAME = 'STDINP'
         ENDIF
      ENDIF

C---     Broadcast le code d'erreur
      CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(),
     &               I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
      IF (I_ERROR .EQ. MPI_SUCCESS .AND. IERR .EQ. -1) GOTO 9900
D     CALL ERR_ASR(I_ERROR .NE. MPI_SUCCESS .OR. IERR .EQ. 0)

C---     Broadcast le nom
      CALL MPI_BCAST(FNAME, LEN(FNAME), MPI_CHARACTER,
     &               I_MASTER, MP_UTIL_REQCOMM(),
     &               I_ERROR)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_INFO_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FINP_REQNOM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ouvre le fichier
C
C Description:
C     La fonction privée IC_FINP_OPEN ouvre le fichier et le positionne
C     à la ligne de lecture.
C
C Entrée:
C     MR          Numéro d'unité
C     FNAME       Nom du fichier
C     ILC         Numéro de ligne initial
C
C Sortie:
C     ICRC        CRC du nom de fichier
C
C Notes:
C************************************************************************
      FUNCTION IC_FINP_OPEN(MR, FNAME, ILC, ICRC)

      IMPLICIT NONE

      INTEGER       MR
      CHARACTER*(*) FNAME
      INTEGER       ILC
      INTEGER       ICRC

      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER IERR
      INTEGER I
      LOGICAL ESTOUVERT
      CHARACTER*(256) BUF
C-----------------------------------------------------------------------

      IERR = 0
      ICRC = 0

C---     Ouvre le fichier
      ESTOUVERT = .FALSE.
      OPEN(UNIT  = MR,
     &     FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &     FORM  = 'FORMATTED',
     &     STATUS= 'OLD',
     &     ERR   = 9900)
      ESTOUVERT = .TRUE.
      INQUIRE(UNIT = MR,
     &        ERR  = 9900,
     &        NAME = BUF)

      IERR = C_ST_CRC32(BUF, ICRC)

      DO I=1,MAX(0,ILC)
         READ (MR, '(A)', ERR=9900) BUF
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE    ! COMPLETE LE MESSAGE D'ERREUR
      WRITE(ERR_BUF, '(3A)') '#<3>#MSG_FICHIER', ': ',
     &         FNAME(1:MIN(128,SP_STRN_LEN(FNAME)))
      CALL ERR_AJT(ERR_BUF)
      IF (ESTOUVERT) CLOSE(MR)
      GOTO 9999

9999  CONTINUE
      IC_FINP_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis une commande
C
C Description:
C     La fonction privée IC_FINP_LISCMND lis une commande complète de
C     l'unité fichier MR. La commande peut être sur plusieurs lignes.
C
C Entrée:
C     INTEGER       MR                 Unité de lecture
C     INTEGER       NLACT              Ligne actuelle
C     INTEGER       LSTRMAX            Longueur max de la chaîne
C
C Sortie:
C     INTEGER       NLACT              Ligne actuelle
C     INTEGER       NLDEB              Ligne de début de statement
C     INTEGER       NLSRC              Ligne de début dans source
C     CHARACTER*(*) STRING             Chaîne contenant la commande
C
C Notes:
C     Avec des pipes, MR=5 (stdin) n'est pas bloquant et donc tente de
C     lire plus loin que EOF. La fonction EOF(MR) n'est pas utilisable
C     sur l'unité 5.
C************************************************************************
      FUNCTION IC_FINP_LISCMND(MR,
     &                         NLACT,
     &                         NLDEB,
     &                         NLSRC,
     &                         LSTRMAX,
     &                         STRING)

      IMPLICIT NONE

      INTEGER       MR
      INTEGER       NLACT
      INTEGER       NLDEB
      INTEGER       NLSRC
      INTEGER       LSTRMAX
      CHARACTER*(*) STRING

      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfinp.fc'

      INTEGER   ILLIN
      PARAMETER (ILLIN = 1024)

      CHARACTER*(ILLIN) STRTMP
      INTEGER           LSTRING, LSTRTMP
      INTEGER           I, IPOS, NPAR
C----------------------------------------------------------------------------

      NLDEB = -1
      NLSRC = -1

100   CONTINUE

C---     LIS UNE LIGNE
      IF (MR .EQ. 5) THEN
         READ (MR,'(A)',END=101,ERR=9901) STRING
         GOTO 102
101      CONTINUE
            CALL SLEEP(1)          ! cf. note
102      CONTINUE
      ELSE
         READ (MR,'(A)',END=9900,ERR=9901) STRING
      ENDIF

C---     TRIM
      NLACT = NLACT + 1
      CALL SP_STRN_TRM(STRING)
      LSTRING = SP_STRN_LEN(STRING)
      IF (LSTRING .EQ. 0) GOTO 100

C---     LIGNE DE PREPROC
      IF (STRING(1:8) .EQ. '#!Line: ') READ(STRING(9:), *) NLSRC

C---     LIGNE DE COMMENTAIRES
      IF (STRING(1:1) .EQ. '#') GOTO 100     ! COMMENTS TO SKIP

C---     SUPPRIME LES COMMENTAIRES DE LIGNE
      CALL SP_STRN_TCM(STRING)
      CALL SP_STRN_TRM(STRING)
      LSTRING = SP_STRN_LEN(STRING)
      IF (LSTRING .EQ. 0) GOTO 100

C---     DEBUT DU STATEMENT
      NLDEB = NLACT
      IF (NLSRC .LT. 0) NLSRC = NLDEB

C---     ( ?
      IPOS = INDEX(STRING( 1:LSTRING),'(')
      IF (IPOS .LT. 1 .OR. IPOS .GT. LSTRING) GOTO 900

C---    RECHERCHE )
200   CONTINUE
         NPAR = 0
         DO I=IPOS,LSTRING
            IF (STRING(I:I) .EQ. '(') NPAR = NPAR + 1
            IF (STRING(I:I) .EQ. ')') NPAR = NPAR - 1
         ENDDO
         IF (NPAR .EQ. 0) GOTO 210
         READ (MR,'(A)',END=9902,ERR=9901) STRTMP
         NLACT = NLACT + 1
         CALL SP_STRN_TCM(STRTMP)
         CALL SP_STRN_TRM(STRTMP)
         LSTRTMP = SP_STRN_LEN(STRTMP)
         IF ((LSTRING+LSTRTMP) .GT. LSTRMAX) GOTO 9904
         STRING = STRING(1:LSTRING) // STRTMP(1:LSTRTMP)
         CALL SP_STRN_TRM(STRING)
         LSTRING = SP_STRN_LEN(STRING)
      GOTO 200
210   CONTINUE


900   CONTINUE
      GOTO 9999
C----------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FIN_FICHIER_CMD'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IF (SP_STRN_LEN(STRING) .GT. 80) STRING = STRING(1:75) // ' ...'
      CALL ERR_AJT(STRING(1:SP_STRN_LEN(STRING)))
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_FIN_FICHIER_INATTENDUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IF (SP_STRN_LEN(STRING) .GT. 80) STRING = STRING(1:75) // ' ...'
      CALL ERR_AJT(STRING(1:SP_STRN_LEN(STRING)))
      GOTO 9999
9904  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_BUFFER_LECTURE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FINP_LISCMND = ERR_TYP()
      RETURN
      END
