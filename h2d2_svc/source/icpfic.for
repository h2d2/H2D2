C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PFIC_000
C     INTEGER IC_PFIC_999
C     INTEGER IC_PFIC_PKL
C     INTEGER IC_PFIC_UPK
C     INTEGER IC_PFIC_CTR
C     INTEGER IC_PFIC_DTR
C     INTEGER IC_PFIC_INI
C     INTEGER IC_PFIC_RST
C     INTEGER IC_PFIC_REQHBASE
C     LOGICAL IC_PFIC_HVALIDE
C     INTEGER IC_PFIC_END
C     INTEGER IC_PFIC_POP
C     INTEGER IC_PFIC_PUSH
C     INTEGER IC_PFIC_TOP
C     INTEGER IC_PFIC_SIZE
C   Private:
C     LOGICAL IC_PFIC_ESTREC
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>IC_PFIC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpfic.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_PFIC_NOBJMAX,
     &                   IC_PFIC_HBASE,
     &                   'Command File Stack')

      IC_PFIC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpfic.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      EXTERNAL IC_PFIC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_PFIC_NOBJMAX,
     &                   IC_PFIC_HBASE,
     &                   IC_PFIC_DTR)

      IC_PFIC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IV
      INTEGER IPRM
      INTEGER HSTK, HFIC
      INTEGER NVAL
      CHARACTER*(32) SVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_PFIC_HBASE
      HSTK = IC_PFIC_HSTK(IOB)

C---     Dimension de la pile
      NVAL = DS_LIST_REQDIM(HSTK)

C---     Le contenu de la pile
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, NVAL)
      DO IV=1,NVAL
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HSTK, IV, SVAL)
         IF (ERR_GOOD()) READ(SVAL, *, ERR=9900) HFIC, IPRM
         IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, HFIC)
      ENDDO

C---     La pile
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_PFIC_HSTK(IOB))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PFIC_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icpfic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IV
      INTEGER HFIC
      INTEGER NVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_PFIC_HBASE

C---     Le contenu de la pile
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, NVAL)
      DO IV=1,NVAL
         IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, HFIC)
      ENDDO

C---     La pile
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_PFIC_HSTK(IOB))

      IC_PFIC_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IC_PFIC_NOBJMAX,
     &                   IC_PFIC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IC_PFIC_HVALIDE(HOBJ))
         IOB = HOBJ - IC_PFIC_HBASE

         IC_PFIC_HSTK(IOB) = 0
      ENDIF

      IC_PFIC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icpfic.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_PFIC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_PFIC_NOBJMAX,
     &                   IC_PFIC_HBASE)

      IC_PFIC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSTK
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RESET LES DONNEES
      IERR = IC_PFIC_RST(HOBJ)

C---     CONSTRUIS LA PILE
      HSTK = 0
      IERR = DS_LIST_CTR(HSTK)
      IERR = DS_LIST_INI(HSTK)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - IC_PFIC_HBASE
         IC_PFIC_HSTK(IOB) = HSTK
      ENDIF

      IC_PFIC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSTK
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PFIC_HBASE
      HSTK = IC_PFIC_HSTK(IOB)

C---     DÉTRUIS LA PILE
      IF (DS_LIST_HVALIDE(HSTK)) THEN
         IERR = DS_LIST_DTR(HSTK)
      ENDIF

C---     RESET LES PARAMETRES
      IC_PFIC_HSTK(IOB) = 0

      IC_PFIC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_PFIC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpfic.fi'
      INCLUDE 'icpfic.fc'
C------------------------------------------------------------------------

      IC_PFIC_REQHBASE = IC_PFIC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_PFIC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icpfic.fc'
C------------------------------------------------------------------------

      IC_PFIC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_PFIC_NOBJMAX,
     &                                  IC_PFIC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Contrôle la récurrence
C
C Description:
C     La fonction privée IC_PFIC_ESTREC retourne .TRUE. si l'appel est
C     récursif.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_ESTREC(HOBJ, CRC)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER CRC

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB, IND, CRC_0
      INTEGER HSTK
      CHARACTER*(256) BUF
      LOGICAL ESTRECURSIF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_PFIC_HBASE

C---     Récupère les attributs
      HSTK = IC_PFIC_HSTK(IOB)

C---     Boucle sur les entrées de la pile
      ESTRECURSIF = .FALSE.
      DO IND=1,DS_LIST_REQDIM(HSTK)
         IERR = DS_LIST_REQVAL(HSTK, IND, BUF)
D        CALL ERR_ASR(ERR_GOOD())
         READ(BUF, *) CRC_0
         IF (CRC_0 .EQ. CRC) THEN
            ESTRECURSIF = .TRUE.
            GOTO 199
         ENDIF
      ENDDO
199   CONTINUE

      IC_PFIC_ESTREC = ESTRECURSIF
      RETURN
      END

C************************************************************************
C Sommaire:    Ferme tous les fichiers.
C
C Description:
C     La fonction privée IC_PFIC_END ferme tous les fichiers. Elle appelle
C     tour à tour IC_PFIC_POP tant que la pile des fichiers n'est pas vide.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_END(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_END
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Pousse la pile des erreurs
      CALL ERR_PUSH()

C---     Ferme tous les fichiers
100   IF (ERR_BAD() .OR. IC_PFIC_SIZE(HOBJ) .LE. 0) GOTO 199
         IERR = IC_PFIC_POP(HOBJ)
      GOTO 100
199   CONTINUE

C---     Pop la pile
      CALL ERR_POP()

      IC_PFIC_END = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Dépile.
C
C Description:
C     La fonction IC_PFIC_POP dépile le fichier sur le dessus de la pile.
C     Le fichier est détruis.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_POP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_POP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IPRM
      INTEGER HFIC, HSTK
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_PFIC_HBASE
      HSTK = IC_PFIC_HSTK(IOB)

C---     Top de la pile
      IF (ERR_GOOD()) IERR = IC_PFIC_TOP(HOBJ, HFIC, IPRM)

C---     Pop la pile
      IF (ERR_GOOD()) IERR = DS_LIST_POP(HSTK)

C---     Détruis le fichier
      IF (ERR_GOOD()) IERR = IC_FINP_DTR(HFIC)

      IC_PFIC_POP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Ouvre un fichier.
C
C Description:
C     La fonction IC_PFIC_PUSH ouvre le fichier de nom <code>FNAME</code>
C     et l'avance à la ligne ILC. Le fichier est poussée sur la pile des
C     fichiers. Il est important que l'appel à PUSH soit conclu par un
C     appel à POP.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAME    Nom du fichier à ouvrir
C     ILC      Ligne
C     IPRM     Paramètre libre pour l'appelant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_PUSH(HOBJ, FNAME, ILC, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_PUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ILC
      INTEGER       IPRM

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfinp.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ICRC
      INTEGER HSTK, HFIC
      CHARACTER*(64) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - IC_PFIC_HBASE
      HSTK = IC_PFIC_HSTK(IOB)

C---     Crée le fichier d'entrée
      HFIC = 0
      IF (ERR_GOOD()) IERR = IC_FINP_CTR(HFIC)
      IF (ERR_GOOD()) IERR = IC_FINP_INI(HFIC, FNAME, ILC)

C---     Contrôle la récursivité
      IF (ERR_GOOD()) THEN
         ICRC = IC_FINP_REQCRC(HFIC)
         IF (IC_PFIC_ESTREC(HOBJ, ICRC)) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_APPEL_RECURSIF')
         ENDIF
      ENDIF

C---     Pousse la pile
      IF (ERR_GOOD()) THEN
         WRITE(BUF, *) HFIC, IPRM
         IERR = DS_LIST_PUSH(HSTK, BUF(1:SP_STRN_LEN(BUF)))
      ENDIF

C---     Nettoie en cas d'erreur
      IF (ERR_BAD()) THEN
         IF (IC_FINP_HVALIDE(HFIC)) IERR = IC_FINP_DTR(HFIC)
      ENDIF

      IC_PFIC_PUSH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Fichier sur le dessus de la pile.
C
C Description:
C     La fonction privée IC_PFIC_TOP retourne le fichier sur le dessus de
C     la pile.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C     HFIC     Handle sur le fichier
C     IPRM     Paramètre utilisateur
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_TOP(HOBJ, HFIC, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_TOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HFIC
      INTEGER IPRM

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSTK
      CHARACTER*(64) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_PFIC_HBASE

C---     Récupère les attributs
      HSTK = IC_PFIC_HSTK(IOB)

C---     Top de la pile
      IERR = DS_LIST_TOP(HSTK, BUF)

C---     Décode les valeurs
      IF (ERR_GOOD()) READ(BUF, *, ERR=9900) HFIC, IPRM

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PFIC_TOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne la taille de la pile.
C
C Description:
C     La fonction privée IC_PFIC_SIZE retourne la taille de la pile
C     des fichiers.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PFIC_SIZE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PFIC_SIZE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpfic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpfic.fc'

      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PFIC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IOB = HOBJ - IC_PFIC_HBASE
      IC_PFIC_SIZE = DS_LIST_REQDIM(IC_PFIC_HSTK(IOB))
      RETURN
      END
