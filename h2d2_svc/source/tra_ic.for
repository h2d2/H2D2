C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_TRA_XEQCTR
C     INTEGER IC_TRA_XEQMTH
C     INTEGER IC_TRA_OPBDOT
C     CHARACTER*(32) IC_TRA_REQCLS
C     INTEGER IC_TRA_REQHDL
C   Private:
C     SUBROUTINE IC_TRA_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entre:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TRA_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TRA_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'tra_ic.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      
      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_TRA_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_TRA_REQHDL()
C        <comment>Return value: Handle on the tracer system</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>tracer</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TRA_AID()

9999  CONTINUE
      IC_TRA_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TRA_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TRA_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'tra_ic.fi'
C------------------------------------------------------------------------

      IC_TRA_XEQMTH = IC_TRA_OPBDOT(HOBJ, IMTH, IPRM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TRA_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TRA_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'tra_ic.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER         IERR
      INTEGER         IVAL
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_TRA_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>set_file</b> sets the output file name for the tracer.</comment>
      IF (IMTH .EQ. 'set_file') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Output file name</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = TRA_ASGFIC(SVAL(1:SP_STRN_LEN(SVAL)))
         IPRM = ' '

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IPRM = ' '

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = TRA_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test TRA_STS(HOBJ) -->')
         IPRM = ' '

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_TRA_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TRA_AID()

9999  CONTINUE
      IC_TRA_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TRA_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TRA_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tra_ic.fi'
      INCLUDE 'tra.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>tracer</b> will monitor the execution of
C  H2D2, logging to file iterations, progression and execution time.
C</comment>
      IC_TRA_REQCLS = 'tracer'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entre:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TRA_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TRA_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tra_ic.fi'
C-------------------------------------------------------------------------

      IC_TRA_REQHDL = 1999002000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_TRA_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('tra_ic.hlp')

      RETURN
      END
