C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_FUNC_000
C     INTEGER IC_FUNC_999
C     INTEGER IC_FUNC_PKL
C     INTEGER IC_FUNC_UPK
C     INTEGER IC_FUNC_CTR
C     INTEGER IC_FUNC_DTR
C     INTEGER IC_FUNC_INI
C     INTEGER IC_FUNC_RST
C     INTEGER IC_FUNC_REQHBASE
C     LOGICAL IC_FUNC_HVALIDE
C     INTEGER IC_FUNC_INIWRITE
C     INTEGER IC_FUNC_ADDSTAT
C     INTEGER IC_FUNC_ENDWRITE
C     INTEGER IC_FUNC_CHKARGS
C     INTEGER IC_FUNC_POP
C     INTEGER IC_FUNC_PUSH
C     INTEGER IC_FUNC_REQILIN
C     CHARACTER*(256) IC_FUNC_REQNFNC
C     CHARACTER*(256) IC_FUNC_REQFSRC
C     CHARACTER*(256) IC_FUNC_REQFFNC
C   Private:
C     INTEGER IC_FUNC_RAZ
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>IC_FUNC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_FUNC_NOBJMAX,
     &                   IC_FUNC_HBASE,
     &                   'Command Interpreter Function')

      IC_FUNC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
      EXTERNAL IC_FUNC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_FUNC_NOBJMAX,
     &                   IC_FUNC_HBASE,
     &                   IC_FUNC_DTR)

      IC_FUNC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_FUNC_HBASE

      IF (ERR_GOOD()) IERR = IO_XML_WS  (HXML, IC_FUNC_NFNC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WS  (HXML, IC_FUNC_FFNC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WS  (HXML, IC_FUNC_FSRC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, IC_FUNC_HSTK(IOB))

      IC_FUNC_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - IC_FUNC_HBASE

      IERR = IC_FUNC_RAZ(HOBJ)

      IF (ERR_GOOD()) IERR = IO_XML_RS  (HXML, IC_FUNC_NFNC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RS  (HXML, IC_FUNC_FFNC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RS  (HXML, IC_FUNC_FSRC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, IC_FUNC_HSTK(IOB))

      IC_FUNC_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   IC_FUNC_NOBJMAX,
     &                                   IC_FUNC_HBASE)
      IF (ERR_GOOD()) IERR = IC_FUNC_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. IC_FUNC_HVALIDE(HOBJ))

      IC_FUNC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icfunc.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_FUNC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_FUNC_NOBJMAX,
     &                   IC_FUNC_HBASE)

      IC_FUNC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IOB = HOBJ - IC_FUNC_HBASE

      IC_FUNC_NFNC(IOB) = ' '
      IC_FUNC_FFNC(IOB) = ' '
      IC_FUNC_FSRC(IOB) = ' '
      IC_FUNC_MW  (IOB) = 0
      IC_FUNC_HSTK(IOB) = 0

      IC_FUNC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NOM      Nom de la fonction
C     FSRC     Nom du fichier source
C     PRMS     Paramètres
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_INI(HOBJ, NOM, FSRC, PRMS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOM
      CHARACTER*(*) FSRC
      CHARACTER*(*) PRMS

      INCLUDE 'icfunc.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER I, ITK, LNOM, LVAR
      INTEGER HSTK
      CHARACTER*(256) FFNC, VAR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les données
      IERR = IC_FUNC_RST(HOBJ)

C---     Crée un fichier temporaire
      FFNC = ' '
      IF (ERR_GOOD()) THEN
         IRET = C_OS_FICTMP(FFNC)
         IF (IRET .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_CREE_FICTMP')
      ENDIF

C---     Crée la pile des paramètres
      IF (ERR_GOOD()) THEN
         IERR = DS_LIST_CTR(HSTK)
         IERR = DS_LIST_INI(HSTK)
      ENDIF

C---     Parse les param
      LNOM = SP_STRN_LEN(NOM)
      IF (ERR_GOOD()) THEN
         I = 0
         ITK = -1
200      CONTINUE
            I = I + 1
            IRET = SP_STRN_TKS(PRMS, ',', I, VAR)
            IF (IRET .EQ. -1) GOTO 299
            IF (IRET .NE.  0) GOTO 9900

            LVAR = SP_STRN_LEN(VAR)
            IF (.NOT. SP_STRN_VAR(VAR(1:LVAR))) GOTO 9901
            IF (VAR(1:LVAR) .EQ. NOM(1:LNOM))  GOTO 9902
            IF (INDEX(PRMS, VAR(1:LVAR)) .LE. ITK) GOTO 9902

            ITK = INDEX(PRMS, VAR(1:LVAR))
            IERR = DS_LIST_AJTVAL(HSTK, VAR(1:LVAR))
         GOTO 200
299      CONTINUE
      ENDIF

C---     Ajoute le nom de la fonction
      IF (ERR_GOOD()) THEN
         IERR = DS_LIST_AJTVAL(HSTK, NOM(1:LNOM))
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - IC_FUNC_HBASE
         IC_FUNC_NFNC(IOB) = NOM (1:SP_STRN_LEN(NOM))
         IC_FUNC_FFNC(IOB) = FFNC(1:SP_STRN_LEN(FFNC))
         IC_FUNC_FSRC(IOB) = FSRC(1:SP_STRN_LEN(FSRC))
         IC_FUNC_HSTK(IOB) = HSTK
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A, I6)') 'ERR_LECTURE_PARAMETRE',': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRE_INVALIDE',': ',
     &                      VAR(1:SP_STRN_LEN(VAR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRE_REPETE',': ',
     &                      VAR(1:SP_STRN_LEN(VAR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      CHARACTER*(256) FFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE

C---     Ferme le fichier temporaire
      IF (IC_FUNC_MW(IOB) .NE. 0) THEN
         CLOSE(IC_FUNC_MW(IOB))
      ENDIF

C---     Détruis le fichier temporaire
      FFNC = IC_FUNC_FFNC(IOB)
      IF (SP_STRN_LEN(FFNC) .GT. 0)
     &   IRET = C_OS_DELFIC(FFNC(1:SP_STRN_LEN(FFNC)))

C---     Détruis la pile des arguments
      IF (DS_LIST_HVALIDE(IC_FUNC_HSTK(IOB))) THEN
         IERR = DS_LIST_DTR(IC_FUNC_HSTK(IOB))
      ENDIF

C---     Reset les paramètres
      IERR = IC_FUNC_RAZ(HOBJ)

      IC_FUNC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_FUNC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icfunc.fi'
      INCLUDE 'icfunc.fc'
C------------------------------------------------------------------------

      IC_FUNC_REQHBASE = IC_FUNC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_FUNC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icfunc.fc'
C------------------------------------------------------------------------

      IC_FUNC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_FUNC_NOBJMAX,
     &                                  IC_FUNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:IC_FUNC_INIWRITE
C
C Description:
C     La  méthode IC_FUNC_INIWRITE initialise l'écriture des statements.
C     On ouvre le fichier temporaire.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_INIWRITE (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_INIWRITE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IOB, MW
      CHARACTER*(256) FFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      FFNC = IC_FUNC_FFNC(IOB)
      MW   = IC_FUNC_MW  (IOB)
D     CALL ERR_ASR(SP_STRN_LEN(FFNC) .GT. 0)
D     CALL ERR_ASR(MW .EQ. 0)

C---     Ouvre le fichier temporaire
      MW = IO_UTIL_FREEUNIT()
      OPEN(UNIT  = MW,
     &     FILE  = FFNC(1:SP_STRN_LEN(FFNC)),
     &     FORM  = 'FORMATTED',
     &     STATUS= 'UNKNOWN',
     &     ACCESS= 'APPEND',    ! Non standard
C     &     POSITION='APPEND',  ! Pas implanté par g77
     &     ERR   = 9900)

C---     Assigne les attributs
      IC_FUNC_MW  (IOB) = MW

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_INIWRITE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:IC_FUNC_ENDWRITE
C
C Description:
C     La  méthode IC_FUNC_ENDWRITE ferme le fichier temporaire.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     ILD      Ligne de début dans le fichier source
C     ILF      Ligne de fin dans le fichier source
C     STAT     Statement
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_ENDWRITE (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_ENDWRITE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IOB, MW
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      MW   = IC_FUNC_MW  (IOB)
D     CALL ERR_ASR(MW .NE. 0)

C---     Ferme inconditionnellement
      CLOSE(MW)

      IC_FUNC_ENDWRITE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:IC_FUNC_ADDSTAT
C
C Description:
C     La  méthode IC_FUNC_ADDSTAT ajoute un statement à la fonction.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     ILD      Ligne de début dans le fichier source
C     ILF      Ligne de fin dans le fichier source
C     STAT     Statement
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_ADDSTAT (HOBJ, ILD, ILF, STAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_ADDSTAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ILD, ILF
      CHARACTER*(*) STAT

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR, IRET
      INTEGER IOB, MW
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      MW   = IC_FUNC_MW  (IOB)
D     CALL ERR_ASR(MW .NE. 0)

C---     Ajoute la ligne
      WRITE(MW, '(A,I6)', ERR=9901) '#!Line: ', (ILD-1)
      WRITE(MW, '(A)',    ERR=9901) STAT(1:SP_STRN_LEN(STAT))

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_ADDSTAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_FUNC_CHKARGS
C
C Description:
C     La  méthode IC_FUNC_CHKARGS contrôle qu'un appel est valide.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HCTX     Handle sur le contexte d'exécution
C     ARGS     Arguments de la fonction
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_CHKARGS (HOBJ, HCTX, ARGS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_CHKARGS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HCTX
      CHARACTER*(*) ARGS

      INCLUDE 'icfunc.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NPRM
      INTEGER HSTK
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCTX))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      HSTK = IC_FUNC_HSTK(IOB)
D     CALL ERR_ASR(DS_LIST_HVALIDE(HSTK))

C---     Parse les param
      NPRM = DS_LIST_REQDIM(HSTK) - 1  ! - la fonction
      IF (NPRM .NE. SP_STRN_NTOK(ARGS, ',{')) GOTO 9901

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)') 'ERR_NBR_PARAM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_CHKARGS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_FUNC_POP
C
C Description:
C     La  méthode IC_FUNC_POP nettoie l'objet après l'appel de fonction.
C     Elle récupère la valeur de retour et détruis le dictionnaire des
C     symboles de l'exécution.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSMB     Handle sur le dictionnaire des symboles de la fonction
C
C Sortie:
C     VAL      Valeur de retour de la fonction
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_POP(HOBJ, HSMB, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_POP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HSMB
      CHARACTER*(*) VAL

      INCLUDE 'icfunc.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HSTK
      CHARACTER*(256) NFNC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_MAP_HVALIDE (HSMB))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      NFNC = IC_FUNC_NFNC (IOB)
!!      HSTK = IC_FUNC_HSTK(IOB)
!!D     CALL ERR_ASR(DS_MAP_HVALIDE(HSMB))
!!D     CALL ERR_ASR(DS_LIST_HVALIDE(HSTK))

C---     Récupère la valeur de retour
      IF (DS_MAP_ESTCLF(HSMB, NFNC(1:SP_STRN_LEN(NFNC)))) THEN
         IERR = DS_MAP_REQVAL(HSMB,
     &                        NFNC(1:SP_STRN_LEN(NFNC)),
     &                        VAL)
      ELSE
         VAL = ' '
         IERR = IC_PILE_VALENC(VAL, IC_PILE_TYP_0VALEUR)
      ENDIF

C---     Détruis le dictionnaire des symboles
      IERR = DS_MAP_DTR(HSMB)
      HSMB = 0

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A)') 'ERR_LECTURE_PARAMETRE',': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_POP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:IC_FUNC_PUSH
C
C Description:
C     La  méthode IC_FUNC_PUSH construis un dictionnaire de symboles qui
C     contient les arguments et qui peut donc servir à l'exécution de la
C     fonction. Elle retourne le dictionnaire de symboles.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HCTX     Handle sur le contexte d'exécution
C     ARGS     Arguments de la fonction
C
C Sortie:
C     HSMF     Handle sur le dictionnaire des symboles de la fonction
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_PUSH (HOBJ, HCTX, ARGS, HSMF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_PUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HCTX
      CHARACTER*(*) ARGS
      INTEGER       HSMF

      INCLUDE 'icfunc.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icfunc.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER I
      INTEGER NPRM
      INTEGER HSTK
      CHARACTER*(256) VAR, VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCTX))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - IC_FUNC_HBASE
      HSTK = IC_FUNC_HSTK(IOB)
D     CALL ERR_ASR(DS_LIST_HVALIDE(HSTK))

C---     Crée le dictionnaire des symboles
      IF (ERR_GOOD()) IERR = DS_MAP_CTR(HSMF)
      IF (ERR_GOOD()) IERR = DS_MAP_INI(HSMF)

C---     Parse les param
      IF (ERR_GOOD()) THEN
         NPRM = DS_LIST_REQDIM(HSTK) - 1  ! - la fonction
         IF (NPRM .NE. SP_STRN_NTOK(ARGS, ',{')) GOTO 9901
         DO I=1,NPRM
            IRET = SP_STRN_TKS(ARGS, ',{', I, VAR)
            IF (IRET .EQ. -1) GOTO 9901
            IF (IRET .NE.  0) GOTO 9900
            IF (VAR(1:1) .NE. '{') VAR = '{' // VAR(1:SP_STRN_LEN(VAR))

            IERR = IC_ICMD_REQSMB(HCTX, VAR, VAL)
            IERR = DS_LIST_REQVAL(HSTK, I, VAR)
            IERR = DS_MAP_ASGVAL (HSMF,
     &                            VAR(1:SP_STRN_LEN(VAR)),
     &                            VAL(1:SP_STRN_LEN(VAL)))
         ENDDO
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A, I6)') 'ERR_LECTURE_PARAMETRE',': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NBR_PARAM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_FUNC_PUSH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_FUNC_REQNFNC
C
C Description:
C     La fonction IC_FUNC_REQNFNC retourne le nom de la fonction.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_REQNFNC (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_REQNFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IC_FUNC_REQNFNC = IC_FUNC_NFNC(HOBJ - IC_FUNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_FUNC_REQFSRC
C
C Description:
C     La fonction IC_FUNC_REQFSRC retourne le nom du fichier dans lequel
C     la fonction est définie.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_REQFSRC (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_REQFSRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IC_FUNC_REQFSRC = IC_FUNC_FSRC(HOBJ - IC_FUNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_FUNC_REQFFNC
C
C Description:
C     La fonction IC_FUNC_REQFFNC retourne le nom du fichier temporaire
C     à parti duquel la fonction peut être exécutée. La fonction débute
C     en première ligne du fichier
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_FUNC_REQFFNC (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_FUNC_REQFFNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'icfunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icfunc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_FUNC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IC_FUNC_REQFFNC = IC_FUNC_FFNC(HOBJ - IC_FUNC_HBASE)
      RETURN
      END

