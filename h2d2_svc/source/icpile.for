C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PILE_000
C     INTEGER IC_PILE_999
C     INTEGER IC_PILE_PKL
C     INTEGER IC_PILE_UPK
C     INTEGER IC_PILE_CTR
C     INTEGER IC_PILE_DTR
C     INTEGER IC_PILE_INI
C     INTEGER IC_PILE_RST
C     INTEGER IC_PILE_REQHBASE
C     LOGICAL IC_PILE_HVALIDE
C     INTEGER IC_PILE_CMPCT
C     INTEGER IC_PILE_DUMP
C     LOGICAL IC_PILE_ESTKW
C     LOGICAL IC_PILE_ESTLBL
C     LOGICAL IC_PILE_ESTNOOP
C     LOGICAL IC_PILE_ESTOP
C     LOGICAL IC_PILE_ESTOPB
C     LOGICAL IC_PILE_ESTOPU
C     LOGICAL IC_PILE_ESTVAL
C     LOGICAL IC_PILE_ESTVALCST
C     LOGICAL IC_PILE_ESTVALHDL
C     LOGICAL IC_PILE_ESTVALVAL
C     LOGICAL IC_PILE_ESTVAR
C     LOGICAL IC_PILE_ESTVIDE
C     LOGICAL IC_PILE_ESTVIDEFR
C     INTEGER IC_PILE_FRBSE
C     INTEGER IC_PILE_FRCLR
C     INTEGER IC_PILE_FRPOP
C     INTEGER IC_PILE_FRPUSH
C     INTEGER IC_PILE_FRTOP
C     INTEGER IC_PILE_FRSIZ
C     INTEGER IC_PILE_FROPDEB
C     INTEGER IC_PILE_FROPFIN
C     INTEGER IC_PILE_CLR
C     INTEGER IC_PILE_POP
C     INTEGER IC_PILE_PUSH
C     INTEGER IC_PILE_PUSHE
C     INTEGER IC_PILE_SIZ
C     INTEGER IC_PILE_TOP
C     LOGICAL IC_PILE_ESTVALOK
C     SUBROUTINE IC_PILE_TYP2TXT
C     INTEGER IC_PILE_VALDEC
C     INTEGER IC_PILE_VALENC
C     INTEGER IC_PILE_VALTYP
C     INTEGER IC_PILE_VALDEB
C     INTEGER IC_PILE_VALFIN
C     CHARACTER*(16) IC_PILE_VALTAG
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IC_PILE_NOBJMAX,
     &                   IC_PILE_HBASE,
     &                   'Command Interface Stack')

      IC_PILE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER  IERR
      EXTERNAL IC_PILE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IC_PILE_NOBJMAX,
     &                   IC_PILE_HBASE,
     &                   IC_PILE_DTR)

      IC_PILE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST
      INTEGER   IERR
      INTEGER   IRET
      INTEGER   IOB
      INTEGER   IV, ISTB, NVAL
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------
      LOGICAL EG
      EG() = (ERR_GOOD() .AND. IRET .EQ. 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)

C---     Dimension de la pile
      NVAL = C_LST_REQDIM(XLST)

C---     La pile
      IERR = IO_XML_WI_1(HXML, NVAL)
      DO IV=1,NVAL
         IF (EG()) IRET = C_LST_REQVAL(XLST, IV, SVAL)
         IF (EG()) IERR = IO_XML_WS(HXML, SVAL(1:SP_STRN_LEN(SVAL)))
      ENDDO

C---     Stack base
      IF (EG()) IERR = IO_XML_WI_1(HXML, ISTB)

C---     Gère les erreurs
      IF (IRET .NE. 0) THEN
         WRITE(ERR_BUF, *) 'ERR_@&#%?'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

      IC_PILE_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST
      INTEGER   IERR
      INTEGER   IRET
      INTEGER   IOB
      INTEGER   IV, ISTB, NVAL
      CHARACTER*(256) SVAL
C------------------------------------------------------------------------
      LOGICAL EG
      EG() = (ERR_GOOD() .AND. IRET .EQ. 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Initialise
      IERR = IC_PILE_INI(HOBJ)

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)

C---     La pile
      IF (EG()) IERR = IO_XML_RI_1(HXML, NVAL)
      DO IV=1,NVAL
         IF (EG()) IERR = IO_XML_RS(HXML, SVAL)
         IF (EG()) IRET = C_LST_AJTVAL(XLST, SVAL(1:SP_STRN_LEN(SVAL)))
      ENDDO
D     CALL ERR_ASR(.NOT. EG() .OR. C_LST_REQDIM(XLST) .EQ. NVAL)

C---     Stack base
      IF (EG()) IERR = IO_XML_RI_1(HXML, ISTB)

C---     Gère les erreurs
      IF (IRET .NE. 0) THEN
         WRITE(ERR_BUF, *) 'ERR_@&#%?'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

      IC_PILE_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IC_PILE_NOBJMAX,
     &                   IC_PILE_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IC_PILE_HVALIDE(HOBJ))
         IOB = HOBJ - IC_PILE_HBASE

         IC_PILE_XLST(IOB) = 0
         IC_PILE_ISTB(IOB) = 0
      ENDIF

      IC_PILE_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IC_PILE_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IC_PILE_NOBJMAX,
     &                   IC_PILE_HBASE)

      IC_PILE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IERR
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = IC_PILE_RST(HOBJ)

C---     ALLOUE L'ESPACE
      XLST = C_LST_CTR()

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - IC_PILE_HBASE
         IC_PILE_XLST(IOB) = XLST
         IC_PILE_ISTB(IOB) = 1
      ENDIF

      IC_PILE_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST       ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IC_PILE_HBASE

C---     DESALLOUE LA MEMOIRE
      XLST = IC_PILE_XLST(IOB)
      IF (XLST .NE. 0) IRET = C_LST_DTR(XLST)

C---     RESET LES PARAMETRES
      IC_PILE_XLST(IOB) = 0
      IC_PILE_ISTB(IOB) = 0

      IC_PILE_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IC_PILE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icpile.fi'
      INCLUDE 'icpile.fc'
C------------------------------------------------------------------------

      IC_PILE_REQHBASE = IC_PILE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IC_PILE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'icpile.fc'
C------------------------------------------------------------------------

      IC_PILE_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IC_PILE_NOBJMAX,
     &                                  IC_PILE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_CMPCT
C
C Description:
C     La fonction IC_PILE_CMPCT compacte la pile et retire les entrées
C     inutiles (NOOP).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_CMPCT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_CMPCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB
      INTEGER   IP, ITP
      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Efface les entrées IC_PILE_TYP_NOOP
      DO IP=C_LST_REQDIM(XLST), 1, -1
         IRET = C_LST_REQVAL(XLST, IP, BUF)
         READ(BUF, *) ITP
         IF (ITP .EQ. IC_PILE_TYP_NOOP)
     &      IRET = C_LST_EFFVAL(XLST, IP)
      ENDDO

      IC_PILE_CMPCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_DUMP
C
C Description:
C     La fonction IC_PILE_DUMP écris dans le log le contenu de la
C     pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_DUMP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_DUMP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IOB
      INTEGER   IND, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

      CALL LOG_ECRIS('---------')
      DO IND=1, C_LST_REQDIM(XLST)
         IRET = C_LST_REQVAL(XLST, IND, LOG_BUF)
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO

      IC_PILE_DUMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTKW
C
C Description:
C     La fonction statique IC_PILE_ESTKW retourne .TRUE. si IOP
C     est une mot clef (KeyWord).
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTKW(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTKW
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTKW = (IOP .EQ. IC_PILE_TYP_MOTCLEF)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTLBL
C
C Description:
C     La fonction statique IC_PILE_ESTLBL retourne .TRUE. si IOP
C     est un label.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTLBL(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTLBL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTLBL = (IOP .EQ. IC_PILE_TYP_LABEL)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTNOOP
C
C Description:
C     La fonction statique IC_PILE_ESTNOOP retourne .TRUE. si IOP
C     est opération vide.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTNOOP(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTNOOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTNOOP = (IOP .EQ. IC_PILE_TYP_NOOP)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTOP
C
C Description:
C     La fonction statique IC_PILE_ESTOP retourne .TRUE. si IOP
C     est une opération.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTOP(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      INTEGER ICTR, I
      ICTR(I) = I - (I/100)*100
C-----------------------------------------------------------------------
      IC_PILE_ESTOP = ((IOP .GT. IC_PILE_TYP_SENTINEL) .AND.
     &                 ICTR(IOP) .LT. ICTR(IC_PILE_TYP_OP_LAST))
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTOPB
C
C Description:
C     La fonction statique IC_PILE_ESTOPB retourne .TRUE. si IOP
C     est une opération binaire.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTOPB(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTOPB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTOPB = (IC_PILE_ESTOP(IOP) .AND.
     &                  ((IOP/100)  - (IOP/1000)*10) .EQ. 2)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTOPU
C
C Description:
C     La fonction statique IC_PILE_ESTOPU retourne .TRUE. si IOP
C     est une opération unaire.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTOPU(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTOPU
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTOPU = (IC_PILE_ESTOP(IOP) .AND.
     &                  ((IOP/100)  - (IOP/1000)*10) .EQ. 1)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVAL
C
C Description:
C     La fonction statique IC_PILE_ESTVAL retourne .TRUE. si IOP
C     est une valeur, constante ou non constante.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVAL(I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVAL =
     &   (I .EQ. IC_PILE_TYP_0VALEUR)
     &   .OR.
     &   (I .GE. IC_PILE_TYP_SVALEUR .AND. I .LE. IC_PILE_TYP_ZVALEUR)
     &   .OR.
     &   (I .GE. IC_PILE_TYP_SCONST  .AND. I .LE. IC_PILE_TYP_ZCONST)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVALCST
C
C Description:
C     La fonction statique IC_PILE_ESTVALCST retourne .TRUE. si IOP
C     est une valeur constante.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVALCST(I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVALCST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVALCST =
     &   (I .GE. IC_PILE_TYP_SCONST  .AND. I .LE. IC_PILE_TYP_ZCONST)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVALHDL
C
C Description:
C     La fonction statique IC_PILE_ESTVALHDL retourne .TRUE. si IOP
C     est une valeur de type handle.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVALHDL(I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVALHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVALHDL =
     &   (I .GE. IC_PILE_TYP_HVALEUR .AND. I .LE. IC_PILE_TYP_XVALEUR)
     &   .OR.
     &   (I .GE. IC_PILE_TYP_HCONST  .AND. I .LE. IC_PILE_TYP_XCONST)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVALMDL
C
C Description:
C     La fonction statique IC_PILE_ESTVALMDL retourne .TRUE. si IOP
C     est une valeur de type handle de module.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVALMDL(I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVALMDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVALMDL = 
     &   (I .EQ. IC_PILE_TYP_MVALEUR .OR. I .GE. IC_PILE_TYP_MCONST)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVALVAL
C
C Description:
C     La fonction statique IC_PILE_ESTVALVAL retourne .TRUE. si IOP
C     est une valeur modifiable, non constante.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVALVAL(I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVALVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVALVAL =
     &   (I .EQ. IC_PILE_TYP_0VALEUR)
     &   .OR.
     &   (I .GE. IC_PILE_TYP_SVALEUR .AND. I .LE. IC_PILE_TYP_ZVALEUR)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVAR
C
C Description:
C     La fonction statique IC_PILE_ESTVAR retourne .TRUE. si IOP
C     est une variable.
C
C Entrée:
C     IOP      Type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVAR(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVAR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'icpile.fi'
C-----------------------------------------------------------------------
      IC_PILE_ESTVAR = (IOP .EQ. IC_PILE_TYP_VARIABLE)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVIDE
C
C Description:
C     La fonction IC_PILE_ESTVIDE retourne .TRUE. si la pile est vide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

      IC_PILE_ESTVIDE = (C_LST_REQDIM(XLST) .LE. 0)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVIDEFR
C
C Description:
C     La fonction IC_PILE_ESTVIDEFR retourne .TRUE. si la pile est vide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVIDEFR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVIDEFR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IOB
      INTEGER   ISTB, ISTT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      ISTT = C_LST_REQDIM(XLST)  ! Stack top

      IC_PILE_ESTVIDEFR = (ISTT .LT. ISTB)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRBSE
C
C Description:
C     La fonction IC_PILE_FRBSE retourne l'entrée (type et valeur) à la base
C     du cadre courant (FRame BaSE).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     VAL         La valeur encodée
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRBSE(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRBSE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB, ISTB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     La valeur
      IRET = C_LST_REQVAL(XLST, ISTB, VAL)

      IC_PILE_FRBSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRCLR
C
C Description:
C     La fonction IC_PILE_FRCLR efface le cadre courant (FRame CLeaR).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRCLR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRCLR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   IP, ISTB, ISTT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      ISTT = C_LST_REQDIM(XLST)  ! Stack top

C---     Efface les entrées
      DO IP=ISTT,ISTB,-1
         IRET = C_LST_EFFVAL(XLST, IP)
      ENDDO

      IC_PILE_FRCLR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRPOP
C
C Description:
C     La fonction IC_PILE_FRPOP dépile le cadre (pop le frame) et redonne
C     le contexte du cadre précédant.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRPOP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRPOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB, IND
      INTEGER   ITP, ISTB
      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      IND  = C_LST_REQDIM(XLST)
      IRET = C_LST_REQVAL(XLST, IND, BUF)
      READ(BUF, *) ITP, ISTB
D     CALL ERR_ASR(ITP .EQ. IC_PILE_TYP_FRAMEBASE)

C---     Efface la dernière entrée
      IRET = C_LST_EFFVAL(XLST, IND)

C---     Remplace le stack base courant
      IC_PILE_ISTB(IOB) = ISTB

      IC_PILE_FRPOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRPUSH
C
C Description:
C     La fonction IC_PILE_FRPUSH empile le cadre (push le frame) et donne
C     un nouveau contexte d'exécution.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRPUSH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRPUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB
      INTEGER   ISTB
      CHARACTER*(32) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Push le frame courant
      WRITE(BUF,'(I12,A,I12)') IC_PILE_TYP_FRAMEBASE, ', ', ISTB
      IRET = C_LST_AJTVAL(XLST, BUF(1:SP_STRN_LEN(BUF)))

C---     La nouvelle base
      ISTB = C_LST_REQDIM(XLST) + 1

C---     Remplace le stack base courant
      IC_PILE_ISTB(IOB) = ISTB

      IC_PILE_FRPUSH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRTOP
C
C Description:
C     La fonction IC_PILE_FRTOP retourne la valeur sur le dessus du cadre
C     courant (FRame TOP).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     VAL         La valeur encodée
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRTOP(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRTOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB, IND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      IND  = C_LST_REQDIM(XLST)
      IRET = C_LST_REQVAL(XLST, IND, VAL)

      IC_PILE_FRTOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FRSIZ
C
C Description:
C     La fonction IC_PILE_FRSIZ retourne la taille du cadre
C     courant (FRame SIZe).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FRSIZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FRSIZ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IOB
      INTEGER   ISTB, ISTT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      ISTT = C_LST_REQDIM(XLST)  ! Stack top

      IC_PILE_FRSIZ = ISTT - ISTB + 1
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FROPDEB
C
C Description:
C     La fonction IC_PILE_FROPDEB retourne la prochaine opération et les
C     arguments de cette opération.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     OPR         L'opération
C     RHS         Le membre de droite
C     LHS         Le membre de gauche
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_FROPDEB(HOBJ, OPR, RHS, LHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FROPDEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPR, RHS, LHS

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   IP, IP1
      INTEGER   ISTB, ISTP, ISTT
      INTEGER   ITYP
      CHARACTER*(16) BUF
      LOGICAL   ESTOPB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      ISTT = C_LST_REQDIM(XLST)  ! Stack top
D     CALL ERR_ASR(ISTB .LE. ISTT)

C---     Contrôles
      IF (ISTB .GT. ISTT) GOTO 9900

C---     Pile exécutée
C        =============
      IF (ISTB .EQ. ISTT) THEN
         IRET = C_LST_REQVAL(XLST, ISTT, OPR)
         ITYP = IC_PILE_VALTYP(OPR)
         IF (.NOT. IC_PILE_ESTNOOP(ITYP) .AND.
     &       .NOT. IC_PILE_ESTKW  (ITYP) .AND.
     &       .NOT. IC_PILE_ESTLBL (ITYP) .AND.
     &       .NOT. IC_PILE_ESTVAR (ITYP) .AND.
     &       .NOT. IC_PILE_ESTVAL (ITYP)) GOTO 9901
         IRET = C_LST_EFFVAL(XLST, ISTT)

C---     Prochain opérateur
C        ==================
      ELSE
C---        Cherche le prochain opérateur
         DO IP=ISTB, ISTT
            IRET = C_LST_REQVAL(XLST, IP, OPR)
            ITYP = IC_PILE_VALTYP(OPR)
            IF (ITYP .GT. IC_PILE_TYP_SENTINEL) GOTO 110
         ENDDO
110      IF (IP .GT. ISTT)   GOTO 9902
         IF (IP .LT. ISTB+1) GOTO 9902
         IF (.NOT. IC_PILE_ESTOPU(ITYP) .AND.
     &       .NOT. IC_PILE_ESTOPB(ITYP)) GOTO 9903
         ESTOPB = IC_PILE_ESTOPB(ITYP)

C---        Premier paramètre
         IP1 = IP - 1
         IRET = C_LST_REQVAL(XLST, IP1, RHS)
         ITYP = IC_PILE_VALTYP(RHS)
         IF (.NOT. IC_PILE_ESTKW (ITYP) .AND.
     &       .NOT. IC_PILE_ESTLBL(ITYP) .AND.
     &       .NOT. IC_PILE_ESTVAR(ITYP) .AND.
     &       .NOT. IC_PILE_ESTVAL(ITYP)) GOTO 9904
         IRET = C_LST_EFFVAL(XLST, IP1)
         IP = IP - 1

C---        Deuxième paramètre
         IF (ESTOPB) THEN
            IP1 = IP - 1
            IRET = C_LST_REQVAL(XLST, IP1, LHS)
            ITYP = IC_PILE_VALTYP(LHS)
            IF (.NOT. IC_PILE_ESTKW (ITYP) .AND.
     &          .NOT. IC_PILE_ESTVAR(ITYP) .AND.
     &          .NOT. IC_PILE_ESTVAL(ITYP)) GOTO 9905
            IRET = C_LST_EFFVAL(XLST, IP1)
            IP = IP - 1
         ENDIF

C---        Remplace l'op par une sentinelle
         BUF = ' '
         IERR = IC_PILE_VALENC(BUF, IC_PILE_TYP_SENTINEL)
         IRET = C_LST_ASGVAL(XLST, IP, BUF(1:SP_STRN_LEN(BUF)))
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PILE_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OPR', ': ', OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_ATTENDU: (noop, kw, lbl, var, val)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_TOP_OF_STACK_REACHED'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OPR', ': ', OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I3)') 'MSG_STACK_POINTER', ': ', IP
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I3)') 'MSG_STACK_BOTTOM', ': ', ISTB
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I3)') 'MSG_STACK_TOP', ': ', ISTT
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_OPR', ': ', OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_ATTENDU: (opu, opb)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_RHS', ': ', RHS(1:SP_STRN_LEN(RHS))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_ATTENDU: (kw, lbl, var, val)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_COURANT: ',
     &                        IC_PILE_TYP2TXT(IC_PILE_VALTYP(OPR))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(2A)') 'ERR_PILE_CORROMPUE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_LHS', ': ', LHS(1:SP_STRN_LEN(LHS))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_ATTENDU: (kw, var, val)'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_TYPE_COURANT: ', IC_PILE_TYP2TXT(ITYP)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_OP_COURANT: ',
     &                        IC_PILE_TYP2TXT(IC_PILE_VALTYP(OPR))
      GOTO 9999


9999  CONTINUE
      IC_PILE_FROPDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_FROPFIN
C
C Description:
C     La fonction IC_PILE_FROPFIN marque la fin de l'opération courante.
C     Elle place les résultats de l'opération dans la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     OPRTYP      Type du résultat de l'opération
C     OPRVAL      Valeur du résultat de l'opération
C
C Sortie:
C
C Notes:
C     Si (base > top) c'est qu'il y a eu un FRPUSH. La valeur de la
C     sentinelle sera remplacée lors du FRPOP.
C************************************************************************
      FUNCTION IC_PILE_FROPFIN(HOBJ, OPR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_FROPFIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) OPR

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB
      INTEGER   IP, IOP
      INTEGER   ISTB, ISTT
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
      ISTB = IC_PILE_ISTB(IOB)   ! Stack base
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère les données
      ISTT = C_LST_REQDIM(XLST)  ! Stack top

C---     base > top ==> appel de fonction
      IF (ISTB .GT. ISTT) GOTO 9999

C---     Cherche la sentinelle
      DO IP=ISTB, ISTT
         IRET = C_LST_REQVAL(XLST, IP, BUF)
         IF (IC_PILE_VALTYP(BUF) .EQ. IC_PILE_TYP_SENTINEL) GOTO 110
      ENDDO
110   IF (IP .GT. ISTT) GOTO 9900

C---     Remplace la sentinelle
      IRET = C_LST_ASGVAL(XLST, IP, OPR(1:SP_STRN_LEN(OPR)))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PILE_CORROMPUE', ': ',
     &                        OPR(1:SP_STRN_LEN(OPR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PILE_FROPFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_CLR
C
C Description:
C     La fonction IC_PILE_CLR vide toute la pile (CLeaR).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_CLR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_CLR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   IP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Efface les entrées
      DO IP=C_LST_REQDIM(XLST), 1, -1
         IERR = C_LST_EFFVAL(XLST, IP)
      ENDDO

      IC_PILE_CLR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_POP
C
C Description:
C     La fonction IC_PILE_POP retire la valeur sur le dessus de la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_POP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_POP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB, IND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Efface la dernière entrée
      IND  = C_LST_REQDIM(XLST)
      IRET = C_LST_EFFVAL(XLST, IND)

      IC_PILE_POP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_PUSH
C
C Description:
C     La fonction IC_PILE_PUSH ajoute la valeur sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur encodée
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_PUSH(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_PUSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IRET
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GE. 4)
D     CALL ERR_PRE(IC_PILE_ESTVALOK(VAL))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      IRET = C_LST_AJTVAL(XLST, VAL(1:SP_STRN_LEN(VAL)))

      IC_PILE_PUSH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_PUSHE
C
C Description:
C     La fonction IC_PILE_PUSHE (push étendu) ajoute la valeur et son type
C     non encodés sur le dessus la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAL         La valeur non encodée
C     ITP         Type de la valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_PUSHE(HOBJ, VAL, ITP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_PUSHE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL
      INTEGER       ITP

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER   IERR
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      BUF = VAL
      IERR = IC_PILE_VALENC(BUF, ITP)
      IERR = IC_PILE_PUSH  (HOBJ, BUF)

      IC_PILE_PUSHE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_SIZ
C
C Description:
C     La fonction IC_PILE_SIZ retourne la dimension de la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_SIZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_SIZ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

      IC_PILE_SIZ = C_LST_REQDIM (XLST)
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_TOP
C
C Description:
C     La fonction IC_PILE_TOP retourne la valeur sur le dessus de la pile.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     VAL         La valeur encodée
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_TOP(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_TOP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icpile.fc'

      INTEGER*8 XLST    ! handle eXterne
      INTEGER   IERR
      INTEGER   IOB, IND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IC_PILE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IC_PILE_HBASE
      XLST = IC_PILE_XLST(IOB)
D     CALL ERR_ASR(XLST .NE. 0)

C---     Récupère la donnée
      IND  = C_LST_REQDIM(XLST)
      IERR = C_LST_REQVAL(XLST, IND, VAL)

      IC_PILE_TOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_ESTVALOK
C
C Description:
C     La fonction statique IC_PILE_ESTVALOK retourne .TRUE. si la valeur
C     est valide dans sa syntaxe et correspond bien au format de la pile.
C
C Entrée:
C     VAL         La valeur encodée
C
C Sortie:
C     VAL         La valeur sans code
C     ITP         Type de la valeur
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_ESTVALOK(VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_ESTVALOK
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER L, D, T
      LOGICAL V
      LOGICAL ESTOK
C-----------------------------------------------------------------------

      ESTOK = .FALSE.

      L = SP_STRN_LEN(VAL)
      IF (L .LT. 4) GOTO 9999
      IF (VAL(1:1) .NE. '{') GOTO 9999
      IF (VAL(L:L) .NE. '}') GOTO 9999

      D = INDEX(VAL, ':')
      IF (D .LT. 3) GOTO 9999
      IF (D .GT. L) GOTO 9999
      IF (.NOT. SP_STRN_INT(VAL(2:D-1))) GOTO 9999

      READ(VAL(2:D-1), *) T
      IF (T .EQ. IC_PILE_TYP_NOOP)     GOTO 199
      IF (T .EQ. IC_PILE_TYP_LABEL)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_VARIABLE) GOTO 199
      IF (T .EQ. IC_PILE_TYP_MOTCLEF)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_0VALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_SVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_LVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_IVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_RVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_HVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_XVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_MVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_ZVALEUR)  GOTO 199
      IF (T .EQ. IC_PILE_TYP_SCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_LCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_ICONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_RCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_HCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_XCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_MCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_ZCONST)   GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPASG)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPVRG)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPADD)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPSUB)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPPOS)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPNEG)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPMUL)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPDIV)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPPOW)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPBRK)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLST)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPDOT)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPPAR)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPNOT)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPAND)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPOR)     GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLEQ)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLNE)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLGT)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLLT)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLGE)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPLLE)    GOTO 199
      IF (T .EQ. IC_PILE_TYP_OPKW)     GOTO 199
      IF (T .EQ. IC_PILE_TYP_FRAMEBASE)GOTO 199
      IF (T .EQ. IC_PILE_TYP_SENTINEL) GOTO 199
      GOTO 9999
199   CONTINUE

      ESTOK = .TRUE.

9999  CONTINUE
      IC_PILE_ESTVALOK = ESTOK
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_TYP2TXT
C
C Description:
C     La fonction statique IC_PILE_TYP2TXT retourne le type ITP en texte.
C
C Entrée:
C     ITP         Type à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_TYP2TXT(ITP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_TYP2TXT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ITP

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icpile.fc'

      CHARACTER*(8) TXT
C-----------------------------------------------------------------------

      TXT = 'invalid'
      IF (ITP .EQ. IC_PILE_TYP_NOOP)     TXT = 'noop'
      IF (ITP .EQ. IC_PILE_TYP_LABEL)    TXT = 'lbl'
      IF (ITP .EQ. IC_PILE_TYP_VARIABLE) TXT = 'var'
      IF (ITP .EQ. IC_PILE_TYP_MOTCLEF)  TXT = 'kw'
      IF (ITP .EQ. IC_PILE_TYP_0VALEUR)  TXT = '0val'
      IF (ITP .EQ. IC_PILE_TYP_SVALEUR)  TXT = 'sval'
      IF (ITP .EQ. IC_PILE_TYP_LVALEUR)  TXT = 'lval'
      IF (ITP .EQ. IC_PILE_TYP_IVALEUR)  TXT = 'ival'
      IF (ITP .EQ. IC_PILE_TYP_RVALEUR)  TXT = 'rval'
      IF (ITP .EQ. IC_PILE_TYP_HVALEUR)  TXT = 'hval'
      IF (ITP .EQ. IC_PILE_TYP_XVALEUR)  TXT = 'xval'
      IF (ITP .EQ. IC_PILE_TYP_MVALEUR)  TXT = 'mval'
      IF (ITP .EQ. IC_PILE_TYP_ZVALEUR)  TXT = 'zval'
      IF (ITP .EQ. IC_PILE_TYP_SCONST)   TXT = 'scst'
      IF (ITP .EQ. IC_PILE_TYP_LCONST)   TXT = 'lcst'
      IF (ITP .EQ. IC_PILE_TYP_ICONST)   TXT = 'icst'
      IF (ITP .EQ. IC_PILE_TYP_RCONST)   TXT = 'rcst'
      IF (ITP .EQ. IC_PILE_TYP_HCONST)   TXT = 'hcst'
      IF (ITP .EQ. IC_PILE_TYP_XCONST)   TXT = 'xcst'
      IF (ITP .EQ. IC_PILE_TYP_MCONST)   TXT = 'mcst'
      IF (ITP .EQ. IC_PILE_TYP_ZCONST)   TXT = 'zcst'
      IF (ITP .EQ. IC_PILE_TYP_OPASG)    TXT = 'opasg'
      IF (ITP .EQ. IC_PILE_TYP_OPVRG)    TXT = 'opvrg'
      IF (ITP .EQ. IC_PILE_TYP_OPADD)    TXT = 'opadd'
      IF (ITP .EQ. IC_PILE_TYP_OPSUB)    TXT = 'opsub'
      IF (ITP .EQ. IC_PILE_TYP_OPPOS)    TXT = 'oppos'
      IF (ITP .EQ. IC_PILE_TYP_OPNEG)    TXT = 'opneg'
      IF (ITP .EQ. IC_PILE_TYP_OPMUL)    TXT = 'opmul'
      IF (ITP .EQ. IC_PILE_TYP_OPDIV)    TXT = 'opdiv'
      IF (ITP .EQ. IC_PILE_TYP_OPPOW)    TXT = 'oppow'
      IF (ITP .EQ. IC_PILE_TYP_OPBRK)    TXT = 'opbrk'
      IF (ITP .EQ. IC_PILE_TYP_OPLST)    TXT = 'list'
      IF (ITP .EQ. IC_PILE_TYP_OPDOT)    TXT = 'opdot'
      IF (ITP .EQ. IC_PILE_TYP_OPPAR)    TXT = 'oppar'
      IF (ITP .EQ. IC_PILE_TYP_OPNOT)    TXT = 'opnot'
      IF (ITP .EQ. IC_PILE_TYP_OPAND)    TXT = 'opand'
      IF (ITP .EQ. IC_PILE_TYP_OPOR)     TXT = 'opor'
      IF (ITP .EQ. IC_PILE_TYP_OPLEQ)    TXT = 'opleq'
      IF (ITP .EQ. IC_PILE_TYP_OPLNE)    TXT = 'oplne'
      IF (ITP .EQ. IC_PILE_TYP_OPLGT)    TXT = 'oplgt'
      IF (ITP .EQ. IC_PILE_TYP_OPLLT)    TXT = 'opllt'
      IF (ITP .EQ. IC_PILE_TYP_OPLGE)    TXT = 'oplge'
      IF (ITP .EQ. IC_PILE_TYP_OPLLE)    TXT = 'oplle'
      IF (ITP .EQ. IC_PILE_TYP_OPKW)     TXT = 'opkw'
      IF (ITP .EQ. IC_PILE_TYP_FRAMEBASE)TXT = 'frbase'
      IF (ITP .EQ. IC_PILE_TYP_SENTINEL) TXT = 'sentry'

      IC_PILE_TYP2TXT = TXT
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALDEC
C
C Description:
C     La fonction statique IC_PILE_VALDEC decode le type et la valeur
C     de la valeur de la pile encodée au format de la pile.
C
C Entrée:
C     VAL         La valeur encodée
C
C Sortie:
C     VAL         La valeur sans code
C     ITP         Type de la valeur
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALDEC(VAL, ITP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALDEC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL
      INTEGER       ITP

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IDEB, IFIN
      LOGICAL ISSTR
C-----------------------------------------------------------------------

      ITP = IC_PILE_VALTYP(VAL)
      ISSTR = (ITP .EQ. IC_PILE_TYP_SVALEUR .OR.
     &         ITP .EQ. IC_PILE_TYP_SCONST)

      IDEB = IC_PILE_VALDEB(VAL)
      IFIN = IC_PILE_VALFIN(VAL)
      IF (ISSTR) THEN
         VAL = VAL(IDEB+1:IFIN-1)
      ELSE
         VAL = VAL(IDEB:IFIN)
      ENDIF

      IC_PILE_VALDEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALENC
C
C Description:
C     La fonction statique IC_PILE_VALENC encode le type dans la valeur.
C
C Entrée:
C     VAL         La valeur
C     ITP         Type de la valeur
C
C Sortie:
C     VAL         La valeur
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALENC(VAL, ITP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALENC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL
      INTEGER       ITP

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      LOGICAL ISSTR
      CHARACTER*(1024) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(.NOT. IC_PILE_ESTVALOK(VAL))
C-----------------------------------------------------------------------

      IF (SP_STRN_LEN(VAL)+8 .GT. LEN(VAL)) GOTO 9900

      ISSTR = (ITP .EQ. IC_PILE_TYP_SVALEUR .OR.
     &         ITP .EQ. IC_PILE_TYP_SCONST)

C---     Encode
      WRITE(BUF,'(I6)') ITP
      CALL SP_STRN_TRM(BUF)
      CALL SP_STRN_TRM(VAL)
      IF (ISSTR) VAL = '''' // VAL(1:SP_STRN_LEN(VAL)) // ''''
      BUF =  '{' // BUF(1:SP_STRN_LEN(BUF)) // ':'
     &           // VAL(1:SP_STRN_LEN(VAL)) // '}'
      VAL = BUF(1:SP_STRN_LEN(BUF))

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_BUFFER_OVERFLOW'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PILE_VALENC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALTYP
C
C Description:
C     La fonction statique IC_PILE_VALTYP retourne le type de la valeur.
C
C Entrée:
C     VAL         La valeur encodée
C
C Sortie:
C     ITP         Type de la valeur
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALTYP(VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALTYP
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER I1, I2
      INTEGER ITP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GE. 4)
D     CALL ERR_PRE(IC_PILE_ESTVALOK(VAL))
C-----------------------------------------------------------------------

      I1 = 2
      I2 = INDEX(VAL, ':') - 1
      READ(VAL(I1:I2), *) ITP

      IC_PILE_VALTYP = ITP
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALDEB
C
C Description:
C     La fonction statique IC_PILE_VALDEB retourne l'indice du début de la
C     valeur.
C
C Entrée:
C     VAL         La valeur encodée
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALDEB(VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALDEB
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GE. 4)
D     CALL ERR_PRE(IC_PILE_ESTVALOK(VAL))
C-----------------------------------------------------------------------

      IC_PILE_VALDEB = INDEX(VAL, ':') + 1
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALFIN
C
C Description:
C     La fonction statique IC_PILE_VALFIN retourne l'indice de fin de la
C     valeur.
C
C Entrée:
C     VAL         La valeur encodée
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALFIN(VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALFIN
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) VAL

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER ISTR, IEND, NBRC
      LOGICAL ISQT
      CHARACTER C
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(VAL) .GE. 4)
D     CALL ERR_PRE(IC_PILE_ESTVALOK(VAL))
C-----------------------------------------------------------------------

      ISQT = .FALSE.
      NBRC = 0

      IEND = SP_STRN_LEN(VAL)
      DO ISTR=1,IEND
         C = VAL(ISTR:ISTR)
         IF (C .EQ. '''') THEN
            ISQT = .NOT. ISQT
         ELSEIF (.NOT. ISQT) THEN
            IF (C .EQ. '{') THEN
               NBRC = NBRC + 1
            ELSEIF (C .EQ. '}') THEN
               NBRC = NBRC - 1
               IF (NBRC .EQ. 0) GOTO 19
            ENDIF
         ENDIF
      ENDDO
      GOTO 9900   ! Pas trouvé

19    CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_VAL_INVALIDE', ': ', VAL(1:IEND)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PILE_VALFIN = ISTR-1
      RETURN
      END

C************************************************************************
C Sommaire: IC_PILE_VALTAG
C
C Description:
C     La fonction statique IC_PILE_VALTAG retourne une valeur
C     "non initialisée" pour le type passé en paramètre.
C
C Entrée:
C     ITP         Type de la valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PILE_VALTAG(ITP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PILE_VALTAG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ITP

      INCLUDE 'icpile.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*(16) VAL
C-----------------------------------------------------------------------

      IF (ITP .EQ. IC_PILE_TYP_0VALEUR) THEN
         VAL = '0'
      ELSEIF (ITP .EQ. IC_PILE_TYP_SVALEUR) THEN
         VAL = 'uninitialized'
      ELSEIF (ITP .EQ. IC_PILE_TYP_LVALEUR) THEN
         VAL = '123'
      ELSEIF (ITP .EQ. IC_PILE_TYP_IVALEUR) THEN
         VAL = '-123456'
      ELSEIF (ITP .EQ. IC_PILE_TYP_RVALEUR) THEN
         VAL = '-1.23456e99'
      ELSEIF (ITP .EQ. IC_PILE_TYP_HVALEUR) THEN
         VAL = '-123456'
      ELSEIF (ITP .EQ. IC_PILE_TYP_XVALEUR) THEN
         VAL = '-123456'
      ELSEIF (ITP .EQ. IC_PILE_TYP_MVALEUR) THEN
         VAL = '-123456'
      ELSEIF (ITP .EQ. IC_PILE_TYP_ZVALEUR) THEN
         VAL = '-123456'
      ELSE
         VAL = ' '
D        CALL ERR_ASR(ITP .GE. IC_PILE_TYP_0VALEUR .AND.
D    &                ITP .LE. IC_PILE_TYP_ZVALEUR)
      ENDIF

      IC_PILE_VALTAG = VAL
      RETURN
      END
