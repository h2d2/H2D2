C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SO_ALLC_INI
C     INTEGER SO_ALLC_RST
C     INTEGER SO_ALLC_TEST
C     INTEGER SO_ALLC_CLCHSH
C     INTEGER SO_ALLC_CHKCRC
C     INTEGER SO_ALLC_ALLCHR
C     INTEGER SO_ALLC_ALLINT
C     INTEGER SO_ALLC_ALLIN2
C     INTEGER SO_ALLC_ALLIN4
C     INTEGER SO_ALLC_ALLIN8
C     INTEGER SO_ALLC_ALLRE4
C     INTEGER SO_ALLC_ALLRE8
C     INTEGER SO_ALLC_LOG
C     INTEGER SO_ALLC_MEMALL
C     INTEGER SO_ALLC_MEMMAX
C     LOGICAL SO_ALLC_HEXIST
C     INTEGER SO_ALLC_REQLEN
C     INTEGER*8 SO_ALLC_REQKIND
C     INTEGER*8 SO_ALLC_REQXIND
C     INTEGER*8 SO_ALLC_REQVIND
C     INTEGER*8 SO_ALLC_CSTC2B
C     INTEGER*8 SO_ALLC_CSTH2B
C     INTEGER*8 SO_ALLC_CSTF2B
C     INTEGER*8 SO_ALLC_CSTL2B
C     INTEGER*8 SO_ALLC_CSTK2B
C     INTEGER*8 SO_ALLC_CSTS2B
C     INTEGER*8 SO_ALLC_CSTV2B
C     INTEGER*8 SO_ALLC_CSTZ2B
C     INTEGER*8 SO_ALLC_CSTV2K
C     INTEGER SO_ALLC_REQKHND
C     INTEGER SO_ALLC_REQVHND
C     INTEGER SO_ALLC_ASGIN4
C     INTEGER SO_ALLC_REQIN4
C     INTEGER SO_ALLC_ASGIN8
C     INTEGER*8 SO_ALLC_REQIN8
C     INTEGER SO_ALLC_ASGRE8
C     REAL*8 SO_ALLC_REQRE8
C   Private:
C     INTEGER SO_ALLC_ALLOC
C     INTEGER SO_ALLC_LBYTES
C     INTEGER SO_ALLC_LREEL
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise l'allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_INI() initialise l'allocation
C     de mémoire pour la base KA. Elle crée les tables internes
C     nécessaires à l'allocation.
C     Cette fonction doit obligatoirement être appelée avant toute
C     autre fonction d'allocation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_INI()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 KIN8(2)
      INTEGER   KINT(2)
      INTEGER   IERR
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SO_ALLC_RST()

C---     Contrôle la taille des integer
      IERR = C_MM_CHKINTSIZ(KINT(1),KINT(2))
      IF (IERR .NE. 0) GOTO 9900
      IERR = C_MM_CHKHDLSIZ(KIN8(1),KIN8(2))
      IF (IERR .NE. 0) GOTO 9900

C---     Construit l'allocation de mémoire
      SO_ALLC_XMEM = C_MM_CTR()
      IF (SO_ALLC_XMEM .EQ. 0) GOTO 9901

C---     NTBL et NTBLMAX
      SO_ALLC_NTBL   = 0
      SO_ALLC_LMAX   = 0
      SO_ALLC_IKA    = 0
      SO_ALLC_IKAMAX = 0

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_INTEGER_C_FTN_INCONSISTANT'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9990
9901  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9990

9990  CONTINUE
      IERR = SO_ALLC_RST()

9999  CONTINUE
      SO_ALLC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_RST() libère toute les tables
C     internes associées à l'allocation dynamique de mémoire. Elle est
C     la dernière fonction à appeler.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_RST()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Libère l'allocation de mémoire
      IF (SO_ALLC_XMEM .NE. 0) IRET = C_MM_DTR(SO_ALLC_XMEM)

C---     Reset les valeurs
      SO_ALLC_XMEM   = 0
      SO_ALLC_NTBL   = 0
      SO_ALLC_LMAX   = 0
      SO_ALLC_IKA    = 0
      SO_ALLC_IKAMAX = 0

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_ALLC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Test l'allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_TEST() teste la structure
C     interne d'allocation dynamique de mémoire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_TEST()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_TEST
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IRET
C-----------------------------------------------------------------------

      IRET = C_MM_TEST(SO_ALLC_XMEM)
      IF (IRET .EQ. 0)
     &   CALL ERR_ASG(ERR_ERR, 'ERR_TEST_ALLOCATION_MEMOIRE')

      SO_ALLC_TEST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcul le CRC32 d'une table
C
C Description:
C    La fonction publique SO_ALLC_CLCHSH(...) retourne le hash du bloc
C    d'allocation de mémoire de handle HNDL.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CLCHSH(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CLCHSH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IRET
      INTEGER ICRC
C-----------------------------------------------------------------------

      IRET = C_MM_CLCCRC32(SO_ALLC_XMEM, HNDL, ICRC)
      IF (IRET .EQ. 0)
     &   CALL ERR_ASG(ERR_ERR, 'ERR_CALCUL_HASH')

      SO_ALLC_CLCHSH = ICRC
      RETURN
      END

C************************************************************************
C Sommaire: Test l'allocation dynamique de mémoire
C
C Description:
C    La fonction publique SO_ALLC_CHKCRC(...) calcule le CRC32 de chacun
C    des blocs d'allocation de mémoire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_CHKCRC()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_CHKCRC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IRET
C-----------------------------------------------------------------------

      IRET = C_MM_CHKCRC32(SO_ALLC_XMEM, SO_ALLC_RE8)
      IF (IRET .EQ. 0)
     &   CALL ERR_ASG(ERR_ERR, 'ERR_TEST_ALLOCATION_MEMOIRE')

      SO_ALLC_CHKCRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLCHR(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des CHARACTER. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLCHR(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLCHR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLCHR = SO_ALLC_ALLOC(SO_ALLC_CHR, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLINT(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des INTEGER. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLINT(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLINT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLINT = SO_ALLC_ALLOC(SO_ALLC_INT, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLIN2(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des INTEGER*2. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLIN2(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLIN2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLIN2 = SO_ALLC_ALLOC(SO_ALLC_IN2, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLIN4(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des INTEGER*4. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLIN4(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLIN4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLIN4 = SO_ALLC_ALLOC(SO_ALLC_IN4, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLIN8(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des INTEGER*8. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLIN8(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLIN8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLIN8 = SO_ALLC_ALLOC(SO_ALLC_IN8, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLRE4(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des REAL*4. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLRE4(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLRE4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLRE4 = SO_ALLC_ALLOC(SO_ALLC_RE4, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction publique SO_ALLC_ALLRE8(...) est la version spécialisée
C     de SO_ALLC_ALLOC(...) pour des REAL*8. Elle ne fait que transmettre
C     la requête d'allocation.
C
C Entrée:
C     ILEN           Longueur de la table
C
C Sortie:
C     IPTR           Pointeur à la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLRE8(ILEN, IPTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ALLRE8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILEN
      INTEGER IPTR

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
C------------------------------------------------------------------------

      SO_ALLC_ALLRE8 = SO_ALLC_ALLOC(SO_ALLC_RE8, ILEN, IPTR)
      RETURN
      END

C************************************************************************
C Sommaire: Allocation dynamique de mémoire
C
C Description:
C     La fonction privée SO_ALLC_ALLOC(...) alloue de la mémoire dynamique.
C     Elle dimensionne une table de type ITYP à la longueur ILEN. Si la table
C     n'existe pas, elle est crée. Si elle existe, sa taille est ajustée
C     et si la longueur demandée est ILEN = 0, la table est détruite.
C
C Entrée:
C     ITYP           Type de la table
C                          INTEGER*1 : SO_ALLC_CHR
C                          INTEGER*2 : SO_ALLC_IN2
C                          INTEGER*4 : SO_ALLC_IN4
C                          INTEGER*8 : SO_ALLC_IN8
C                          REAL*4    : SO_ALLC_RE4
C                          REAL*8    : SO_ALLC_RE8
C     ILEN           Longueur de la table
C
C Sortie:
C     HNDL           Handle sur la table
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ALLOC (ITYP, ILEN, HNDL)

      IMPLICIT NONE

      INTEGER ITYP
      INTEGER ILEN
      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER IRET
      INTEGER INC
      INTEGER LNEW, LOLD
      LOGICAL TBLXST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_XMEM .NE. 0)
D     CALL ERR_PRE(HNDL .EQ. 0 .OR. C_MM_EXIST(SO_ALLC_XMEM,HNDL).NE.0)
C------------------------------------------------------------------------

      IF (ILEN .LT. 0) GOTO 9900

C---     CHERCHE SI LA TABLE EXISTE
      TBLXST = (C_MM_EXIST(SO_ALLC_XMEM, HNDL) .NE. 0)

C---     NOUVELLE LONGUEUR EN BYTES
      LNEW = SO_ALLC_LBYTES(ITYP, ILEN)
      IF (LNEW .LT. 0) GOTO 9901

C---     ANCIENNE LONGUEUR EN BYTES
      LOLD = 0
      IF (TBLXST) LOLD = C_MM_REQLEN(SO_ALLC_XMEM, HNDL)

C---     SI LA TABLE EXISTE DEJA ET SI SA DIMENSION RESTE INCHANGÉE: SORTIR
      IF (TBLXST .AND. LNEW .EQ. LOLD) GOTO 999

C---     ALLOUE LA MEMOIRE
      INC  = 0
      IRET = -1
      IF (LNEW .LE. 0) THEN
         IF (TBLXST) THEN
            IRET = C_MM_FREE(SO_ALLC_XMEM, ITYP, HNDL)
         ELSE
            GOTO 9902
         ENDIF
      ELSE
         IF (TBLXST) THEN
            HNDL = C_MM_REALL(SO_ALLC_XMEM, ITYP, HNDL, LNEW)
         ELSE
            HNDL = C_MM_ALLOC(SO_ALLC_XMEM, ITYP, LNEW)
            INC = 1
         ENDIF
      ENDIF
      IF (HNDL .EQ. 0) GOTO 9903
      IF (IRET .EQ. 0) GOTO 9903

C---     AJUSTE LES INFO
      SO_ALLC_NTBL   = SO_ALLC_NTBL + INC
      SO_ALLC_LMAX   = MAX(SO_ALLC_LMAX, LNEW)
      SO_ALLC_IKA    = SO_ALLC_IKA + (LNEW-LOLD)
      SO_ALLC_IKAMAX = MAX(SO_ALLC_IKA, SO_ALLC_IKAMAX)

999   CONTINUE
      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_LONGUEUR_DE_TABLE_INVALIDE', ': ',
     &                           ILEN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_DIM_TABLE_BYTES', ': ',
     &                           LNEW
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_DEALLOC_TABLE_INEXISTANTE', ': ',
     &                           HNDL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_ALLC_ALLOC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Log les statistiques de l'allocation.
C
C Description:
C     La fonction SO_ALLC_LOG écris dans le système de log les statistiques
C     de l'allocation de mémoire.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_LOG()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_LOG
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------

      CALL LOG_ECRIS('MSG_MEMORY_ALLOCATION')
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(2A,F12.3)') 'MSG_ALLOC_MAX#<25>#','(MB): ',
     &                           DBLE(SO_ALLC_IKAMAX) / (1024*1024)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(2A,F12.3)') 'MSG_LEN_MAX#<25>#', '(MB): ',
     &                           DBLE(SO_ALLC_LMAX) / (1024*1024)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(2A,I12)') 'MSG_NBR_ALLOC#<25>#', '    : ',
     &                           SO_ALLC_NTBL
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()
      SO_ALLC_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Quantité actuelle de mémoire allouée
C
C Description:
C     La fonction publique SO_ALLC_MEMALL(...) retourne la quantité
C     de mémoire allouée actuellement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_MEMALL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_MEMALL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      SO_ALLC_MEMALL = SO_ALLC_IKA
      RETURN
      END

C************************************************************************
C Sommaire: Quantité max de mémoire allouée
C
C Description:
C     La fonction publique SO_ALLC_MEMMAX(...) retourne la quantité max
C     de mémoire allouée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_MEMMAX()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_MEMMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      SO_ALLC_MEMMAX = SO_ALLC_IKAMAX
      RETURN
      END

C************************************************************************
C Sommaire: Test pour l'existence.
C
C Description:
C     La fonction publique SO_ALLC_HEXIST(...) retourne .TRUE. si le
C     handle HNDL existe dans le système.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_HEXIST(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_HEXIST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      SO_ALLC_HEXIST = (C_MM_EXIST(SO_ALLC_XMEM, HNDL) .NE. 0)
      RETURN
      END

C************************************************************************
C Sommaire: Longueur d'une table.
C
C Description:
C     La fonction publique SO_ALLC_REQLEN(...) retourne la longueur de
C     la table de handle HNDL.
C
C Entrée:
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQLEN(HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQLEN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER ITYP, LTBL
      LOGICAL TBLXST
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      LTBL = -1
      TBLXST = (C_MM_EXIST(SO_ALLC_XMEM, HNDL) .NE. 0)
      IF (TBLXST) THEN
         ITYP = C_MM_REQTYP(SO_ALLC_XMEM, HNDL)
         LTBL = C_MM_REQLEN(SO_ALLC_XMEM, HNDL)
         LTBL = SO_ALLC_LREEL(ITYP, LTBL)
      ENDIF

      SO_ALLC_REQLEN = LTBL
      RETURN
      END

C************************************************************************
C Sommaire: Indice d'un pointeur dans une table.
C
C Description:
C     La fonction publique SO_ALLC_REQKIND(...) retourne l'indice
C     du handle HNDL par rapport à la table INTEGER*4 K.
C
C Entrée:
C     KA             Table
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQKIND(K, HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQKIND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER K(*)
      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IND
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IND = C_MM_REQINDIN4(SO_ALLC_XMEM, HNDL, K)
      SO_ALLC_REQKIND = IND
      RETURN
      END

C************************************************************************
C Sommaire: Indice d'un pointeur dans une table.
C
C Description:
C     La fonction publique SO_ALLC_REQXIND(...) retourne l'indice
C     du handle HNDL par rapport à la table INTEGER*8 X.
C
C Entrée:
C     X              Table
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQXIND(X, HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQXIND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER*8 X(*)
      INTEGER   HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IND
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IND = C_MM_REQINDIN8(SO_ALLC_XMEM, HNDL, X)
      SO_ALLC_REQXIND = IND
      RETURN
      END

C************************************************************************
C Sommaire: Indice d'un pointeur dans une table.
C
C Description:
C     La fonction publique SO_ALLC_REQIND(...) retourne l'indice
C     du handle HNDL par rapport à la table REAL*8 V.
C
C Entrée:
C     V              Table
C     HNDL           Handle sur un bloc alloué
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQVIND(V, HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQVIND
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  V(*)
      INTEGER HNDL

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IND
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IND = C_MM_REQINDRE8(SO_ALLC_XMEM, HNDL, V)
      SO_ALLC_REQVIND = IND
      RETURN
      END

C************************************************************************
C Sommaire: Retourne un handle pour une adresse INTEGER.
C
C Description:
C     La fonction publique SO_ALLC_REQKHND(...) retourne le handle de
C     l'adresse de la variable INTEGER K. Si l'adresse n'est pas connue
C     du système elle est ajoutée.
C     Le handle peut ensuite être utilisé comme s'il avait été retourné
C     par un appel à SO_ALLC_ALLnnn.
C
C Entrée:
C     K              Variable
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQKHND(K)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQKHND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER K(*)

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER HNDL
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      HNDL = C_MM_ADDPTR(SO_ALLC_XMEM, K)

      SO_ALLC_REQKHND = HNDL
      RETURN
      END

C************************************************************************
C Sommaire: Retourne un handle pour une adresse REAL*8.
C
C Description:
C     La fonction publique SO_ALLC_REQVHND(...) retourne le handle de
C     l'adresse de la variable REAL*8 V. Si l'adresse n'est pas connue
C     du système elle est ajoutée.
C     Le handle peut ensuite être utilisé comme s'il avait été retourné
C     par un appel à SO_ALLC_ALLnnn.
C
C Entrée:
C     V              Variable
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQVHND(V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQVHND
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 V(*)

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER HNDL
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      HNDL = C_MM_ADDPTR(SO_ALLC_XMEM, V)

      SO_ALLC_REQVHND = HNDL
      RETURN
      END

C************************************************************************
C Sommaire: Assigne une valeur INTEGER*4 dans une table
C
C Description:
C     La fonction publique SO_ALLC_ASGIN4(...) assigne la valeur INTEGER*4
C     IV dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C     IV             Valeur
C
C Sortie:
C
C Notes:
C     On utilise ICOPY car les compilateurs en mode release optimisent
C     et voient l'assignation KA(IKA+IND-1)=IV comme assignation d'une
C     variable temporaire qu'il optimisent.
C************************************************************************
      FUNCTION SO_ALLC_ASGIN4(HNDL, IND, IV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ASGIN4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL
      INTEGER IND
      INTEGER IV

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IKA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IKA = SO_ALLC_REQKIND(KA, HNDL)
      CALL ICOPY(1, IV, 1, KA(IKA+IND-1), 1)
      SO_ALLC_ASGIN4 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une valeur INTEGER*4 dans une table
C
C Description:
C     La fonction publique SO_ALLC_REQIN4(...) retourne la valeur INTEGER*4
C     dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQIN4(HNDL, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQIN4
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL
      INTEGER IND

      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IKA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IKA = SO_ALLC_REQKIND(KA, HNDL)
      SO_ALLC_REQIN4 = KA(IKA+IND-1)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne une valeur INTEGER*8 dans une table
C
C Description:
C     La fonction publique SO_ALLC_ASGIN8(...) assigne la valeur INTEGER*8
C     XV dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C     IV             Valeur
C
C Sortie:
C
C Notes:
C     On utilise ICOPY car les compilateurs en mode release optimisent
C     et voient l'assignation KA(IKA+IND-1)=IV comme assignation d'une
C     variable temporaire qu'il optimisent.
C************************************************************************
      FUNCTION SO_ALLC_ASGIN8(HNDL, IND, XV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ASGIN8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER   HNDL
      INTEGER   IND
      INTEGER*8 XV

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IKA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IKA = SO_ALLC_REQXIND(XA, HNDL)
      XA(IKA+IND-1) = XV
      SO_ALLC_ASGIN8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une valeur INTEGER*8 dans une table
C
C Description:
C     La fonction publique SO_ALLC_REQIN8(...) retourne la valeur INTEGER*8
C     dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQIN8(HNDL, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQIN8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL
      INTEGER IND

      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IKA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IKA = SO_ALLC_REQXIND(XA, HNDL)
      SO_ALLC_REQIN8 = XA(IKA+IND-1)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne une valeur REAL*8 dans une table
C
C Description:
C     La fonction publique SO_ALLC_ASGRE8(...) assigne la valeur REAL*8
C     V dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C     V              Valeur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_ASGRE8(HNDL, IND, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_ASGRE8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL
      INTEGER IND
      REAL*8  V

      INCLUDE 'soallc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IVA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IVA = SO_ALLC_REQVIND(VA, HNDL)
      CALL DCOPY(1, V, 1, VA(IVA+IND-1), 1)
      SO_ALLC_ASGRE8 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une valeur REAL*8 dans une table
C
C Description:
C     La fonction publique SO_ALLC_REQRE8(...) retourne la valeur REAL*8
C     dans la table de handle HNDL à l'indice IND.
C
C Entrée:
C     HNDL           Handle sur la table
C     IND            Indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_REQRE8(HNDL, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_ALLC_REQRE8
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNDL
      INTEGER IND

      INCLUDE 'soallc.fi'
      INCLUDE 'soallc.fc'

      INTEGER*8 IVA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IVA = SO_ALLC_REQVIND(VA, HNDL)
      SO_ALLC_REQRE8 = VA(IVA+IND-1)
      RETURN
      END

C************************************************************************
C Sommaire: Longueur en bytes.
C
C Description:
C     La fonction privée SO_ALLC_LBYTES(...) retourne la longueur en
C     bytes suivant le type.
C
C Entrée:
C     ITYP     le type de données
C     ILEN     la longueur en unités du type
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_LBYTES(ITYP, ILEN)

      IMPLICIT NONE

      INTEGER ITYP
      INTEGER ILEN

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER LBYTES
      INTEGER SO_ALLC_CHRBYT
      INTEGER SO_ALLC_IN2BYT
      INTEGER SO_ALLC_IN4BYT
      INTEGER SO_ALLC_IN8BYT
      INTEGER SO_ALLC_RE4BYT
      INTEGER SO_ALLC_RE8BYT

      PARAMETER (SO_ALLC_CHRBYT = 1)
      PARAMETER (SO_ALLC_IN2BYT = 2)
      PARAMETER (SO_ALLC_IN4BYT = 4)
      PARAMETER (SO_ALLC_IN8BYT = 8)
      PARAMETER (SO_ALLC_RE4BYT = 4)
      PARAMETER (SO_ALLC_RE8BYT = 8)
C-----------------------------------------------------------------------

      IF     (ITYP .EQ. SO_ALLC_CHR) THEN
        LBYTES = ILEN*SO_ALLC_CHRBYT
      ELSEIF (ITYP .EQ. SO_ALLC_IN2) THEN
        LBYTES = ILEN*SO_ALLC_IN2BYT
      ELSEIF (ITYP .EQ. SO_ALLC_INT) THEN
        LBYTES = ILEN*C_MM_REQINTSIZ()
      ELSEIF (ITYP .EQ. SO_ALLC_IN4) THEN
        LBYTES = ILEN*SO_ALLC_IN4BYT
      ELSEIF (ITYP .EQ. SO_ALLC_IN8) THEN
        LBYTES = ILEN*SO_ALLC_IN8BYT
      ELSEIF (ITYP .EQ. SO_ALLC_RE4) THEN
        LBYTES = ILEN*SO_ALLC_RE4BYT
      ELSEIF (ITYP .EQ. SO_ALLC_RE8) THEN
        LBYTES = ILEN*SO_ALLC_RE8BYT
      ELSE
        LBYTES = -1
      ENDIF

      SO_ALLC_LBYTES = LBYTES
      RETURN
      END

C************************************************************************
C Sommaire: Longueur en mots réels.
C
C Description:
C     La fonction privée SO_ALLC_LREEL(...) retourne la longueur en
C     mots suivant le type.
C
C Entrée:
C     ITYP     le type de données
C     ILEN     la longueur en bytes
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_ALLC_LREEL(ITYP, ILEN)

      IMPLICIT NONE

      INTEGER ITYP
      INTEGER ILEN

      INCLUDE 'soallc.fi'
      INCLUDE 'c_mm.fi'
      INCLUDE 'soallc.fc'

      INTEGER LREEL, LTYP
      INTEGER SO_ALLC_CHRBYT
      INTEGER SO_ALLC_IN2BYT
      INTEGER SO_ALLC_IN4BYT
      INTEGER SO_ALLC_IN8BYT
      INTEGER SO_ALLC_RE4BYT
      INTEGER SO_ALLC_RE8BYT

      PARAMETER (SO_ALLC_CHRBYT = 1)
      PARAMETER (SO_ALLC_IN2BYT = 2)
      PARAMETER (SO_ALLC_IN4BYT = 4)
      PARAMETER (SO_ALLC_IN8BYT = 8)
      PARAMETER (SO_ALLC_RE4BYT = 4)
      PARAMETER (SO_ALLC_RE8BYT = 8)
C-----------------------------------------------------------------------

      IF     (ITYP .EQ. SO_ALLC_CHR) THEN
        LTYP = SO_ALLC_CHRBYT
      ELSEIF (ITYP .EQ. SO_ALLC_IN2) THEN
        LTYP = SO_ALLC_IN2BYT
      ELSEIF (ITYP .EQ. SO_ALLC_INT) THEN
        LTYP = C_MM_REQINTSIZ()
      ELSEIF (ITYP .EQ. SO_ALLC_IN4) THEN
        LTYP = SO_ALLC_IN4BYT
      ELSEIF (ITYP .EQ. SO_ALLC_IN8) THEN
        LTYP = SO_ALLC_IN8BYT
      ELSEIF (ITYP .EQ. SO_ALLC_RE4) THEN
        LTYP = SO_ALLC_RE4BYT
      ELSEIF (ITYP .EQ. SO_ALLC_RE8) THEN
        LTYP = SO_ALLC_RE8BYT
      ELSE
        LTYP = -1
      ENDIF
      CALL ERR_PST(MOD(ILEN,LTYP) .EQ. 0)
      LREEL = ILEN / LTYP

      SO_ALLC_LREEL = LREEL
      RETURN
      END
