C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_TR_CDWN_XEQCTR
C     INTEGER IC_TR_CDWN_XEQMTH
C     CHARACTER*(32) IC_TR_CDWN_REQCLS
C     INTEGER IC_TR_CDWN_REQHDL
C   Private:
C     SUBROUTINE IC_TR_CDWN_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TR_CDWN_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CDWN_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'trcdwn_ic.fi'
      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      
C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_TR_CDWN_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les paramètres
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Construis l'objet
      IF (ERR_GOOD()) IERR = TR_CDWN_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = TR_CDWN_INI(HOBJ)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the count down timer</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>timer</b> constructs an object and returns a handle on this object.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TR_CDWN_AID()

9999  CONTINUE
      IC_TR_CDWN_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TR_CDWN_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CDWN_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'trcdwn_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trcdwn.fi'
      
      REAL*8  RVAL
      INTEGER IERR, IRET
      LOGICAL LVAL
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0
      
C     <comment>The method <b>is_elapsed</b> returns true (<> 0) if the time is elapsed.</comment>
      IF (IMTH .EQ. 'is_elapsed') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         LVAL = TR_CDWN_ESTFINI(HOBJ)

C     <comment>The method <b>set</b> sets the time to count down.</comment>
      ELSEIF (IMTH .EQ. 'set') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Time laps to count down, in seconds.</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', 1, RVAL)
         IF (IRET .NE. 0) GOTO 9901
         IF (RVAL .LT. 0.0D0) GOTO 9910
         IERR = TR_CDWN_SET(HOBJ, RVAL)

C     <comment>The method <b>start</b> starts the count down.</comment>
      ELSEIF (IMTH .EQ. 'start') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900
         IERR = TR_CDWN_START(HOBJ)

C     <comment>The method <b>pause</b> pauses the count down. It can be restarted with <b>start</b>.</comment>
      ELSEIF (IMTH .EQ. 'pause') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = TR_CDWN_STOP(HOBJ)

C     <comment>The method <b>reset</b> reset the count to 0.</comment>
      ELSEIF (IMTH .EQ. 'reset') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = TR_CDWN_RESET(HOBJ)

C     <comment>The method <b>get_remaining_time</b> returns the remaining time.</comment>
      ELSEIF (IMTH .EQ. 'get_remaining_time') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         RVAL = TR_CDWN_REQRESTE(HOBJ)
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = TR_CDWN_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(TR_CDWN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = TR_CDWN_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_TR_CDWN_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_DESTRUCTION_OBJET'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9910  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_NEGATIF',': ', RVAL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_IC_EXPR_AID()

9999  CONTINUE
      IC_TR_CDWN_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TR_CDWN_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CDWN_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'trcdwn_ic.fi'
      INCLUDE 'trcdwn.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>count_down_timer</b> represents a count down timer. Initialized
C  with a time to run, it can be started/stopped and will return true when the
C  time is elapsed.
C</comment>
      IC_TR_CDWN_REQCLS = 'count_down_timer'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TR_CDWN_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TR_CDWN_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'trcdwn_ic.fi'
      INCLUDE 'trcdwn.fi'
C-------------------------------------------------------------------------

      IC_TR_CDWN_REQHDL = TR_CDWN_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_TR_CDWN_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('trcdwn_ic.hlp')

      RETURN
      END
