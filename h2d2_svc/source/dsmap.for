C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DS_MAP_000
C     INTEGER DS_MAP_999
C     INTEGER DS_MAP_PKL
C     INTEGER DS_MAP_UPK
C     INTEGER DS_MAP_CTR
C     INTEGER DS_MAP_DTR
C     INTEGER DS_MAP_INI
C     INTEGER DS_MAP_RST
C     INTEGER DS_MAP_REQHBASE
C     LOGICAL DS_MAP_HVALIDE
C     INTEGER DS_MAP_PRN
C     INTEGER DS_MAP_ASGVAL
C     INTEGER DS_MAP_EFFCLF
C     LOGICAL DS_MAP_ESTCLF
C     INTEGER DS_MAP_REQDIM
C     INTEGER DS_MAP_REQCLF
C     INTEGER DS_MAP_REQVAL
C   Private:
C     SUBROUTINE DS_MAP_REQSELF
C     INTEGER DS_MAP_RAZ
C
C************************************************************************

      MODULE DS_MAP_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: DS_MAP_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: DS_MAP_SELF_T
         INTEGER*8 XMAP       ! handle eXterne
      END TYPE DS_MAP_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée DS_MAP_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_REQSELF(HOBJ) !, SELF)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (DS_MAP_SELF_T), POINTER :: DS_MAP_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (DS_MAP_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      DS_MAP_REQSELF => SELF ! ERR_TYP()
      RETURN
      END FUNCTION DS_MAP_REQSELF

      END MODULE DS_MAP_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_000
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(DS_MAP_HBASE,
     &                   'Dictionary data structure')

      DS_MAP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_999
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INCLUDE 'dsmap.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL DS_MAP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(DS_MAP_HBASE,
     &                   DS_MAP_DTR)

      DS_MAP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IK
      CHARACTER*(256) SKEY, SVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Les clefs/valeurs
      IERR = IO_XML_WI_1(HXML, DS_MAP_REQDIM(HOBJ))
      DO IK=1,DS_MAP_REQDIM(HOBJ)
         IERR = DS_MAP_REQCLF(HOBJ, IK, SKEY)
         IERR = DS_MAP_REQVAL(HOBJ, SKEY(1:SP_STRN_LEN(SKEY)), SVAL)

         IERR = IO_XML_WS(HXML, SKEY(1:SP_STRN_LEN(SKEY)))
         IERR = IO_XML_WS(HXML, SVAL(1:SP_STRN_LEN(SVAL)))
         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

      DS_MAP_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DS_MAP_UPK restaure l'objet à partir d'une sauvegarde sous
C     format XML. Le handle passé en argument n'est pas construit, et c'est
C     le travail de la fonction de le charger.
C
C Entrée:
C     HOBJ     Handle sur l'objet non construit
C     LNEW     .TRUE. si l'objet nouveau
C     HXML     Handle sur le reader XML
C
C Sortie:
C     HOBJ     Handle sur l'objet construit
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dsmap.fc'

      INTEGER IERR
      INTEGER IK, N
      CHARACTER*(256) SKEY, SVAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Initialise l'objet
      IF (LNEW) IERR = DS_MAP_RAZ(HOBJ)
      IERR = DS_MAP_INI(HOBJ)

C---     Les clef/valeurs
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, N)
      DO IK=1,N
         IF (ERR_GOOD()) IERR = IO_XML_RS(HXML, SKEY)
         IF (ERR_GOOD()) IERR = IO_XML_RS(HXML, SVAL)

         IF (ERR_GOOD()) IERR = DS_MAP_ASGVAL(HOBJ,
     &                                        SKEY(1:SP_STRN_LEN(SKEY)),
     &                                        SVAL(1:SP_STRN_LEN(SVAL)))
      ENDDO
D     CALL ERR_ASR(ERR_BAD() .OR. DS_MAP_REQDIM(HOBJ) .EQ. N)

      DS_MAP_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_CTR
CDEC$ ENDIF

      USE DS_MAP_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fc'

      INTEGER IERR, IRET
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   DS_MAP_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = DS_MAP_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DS_MAP_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      DS_MAP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_DTR
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = DS_MAP_RST(HOBJ)

C---     Dé-alloue
      SELF => DS_MAP_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, DS_MAP_HBASE)

      DS_MAP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée DS_MAP_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_RAZ(HOBJ)

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dsmap.fc'

      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      SELF%XMAP = 0

      DS_MAP_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_INI
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER*8 XMAP       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = DS_MAP_RST(HOBJ)

C---     Construis le map
      XMAP = 0
      IF (ERR_GOOD()) XMAP = C_MAP_CTR()

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => DS_MAP_REQSELF(HOBJ)
         SELF%XMAP = XMAP
      ENDIF

      DS_MAP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_RST
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dsmap.fc'

      INTEGER IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)

C---     Détruis le map
      IF (SELF%XMAP .NE. 0) IERR = C_MAP_DTR(SELF%XMAP)

C---     Reset les attributs
      IF (ERR_GOOD()) IERR = DS_MAP_RAZ(HOBJ)

      DS_MAP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DS_MAP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_REQHBASE
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INCLUDE 'dsmap.fi'
C------------------------------------------------------------------------

      DS_MAP_REQHBASE = DS_MAP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DS_MAP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_HVALIDE
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      DS_MAP_HVALIDE = OB_OBJN_HVALIDE(HOBJ,
     &                                 DS_MAP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DS_MAP_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C     Prototype
C************************************************************************
      FUNCTION DS_MAP_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IK, NVAL
      CHARACTER*256 BUF, VAR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tête
      LOG_ZNE = 'h2d2.data.map'
      WRITE (LOG_BUF, '(A)') 'MSG_MAP'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IF (ERR_GOOD()) IERR = OB_OBJC_REQNOMCMPL(BUF, HOBJ)
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        BUF(1:SP_STRN_LEN(BUF))
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Le nombre de valeurs
      NVAL = 0
      IF (ERR_GOOD()) THEN
         NVAL = DS_MAP_REQDIM(HOBJ)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NOMBRE_VALEURS#<35>#= ', NVAL
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Les valeurs
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF,'(A)') 'MSG_VALEURS:'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF
      CALL LOG_INCIND()
      DO IK=1,NVAL
         IF (ERR_GOOD()) IERR = DS_MAP_REQCLF(HOBJ, IK, VAR)
         IF (ERR_GOOD()) IERR = DS_MAP_REQVAL(HOBJ,VAR, BUF)
         IF (ERR_GOOD()) THEN
            CALL SP_STRN_CLP(VAR, 30)
            CALL SP_STRN_CLP(BUF, 50)
            WRITE (LOG_BUF,'(3A)') VAR(1:SP_STRN_LEN(VAR)),
     &                             '#<30># : ', BUF(1:SP_STRN_LEN(BUF))
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDDO
      CALL LOG_DECIND()

C---     Reset l'indentation
      CALL LOG_DECIND()

      DS_MAP_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_MAP_ASGVAL
C
C Description:
C     La fonction DS_MAP_ASFVAL associe une valeur à une clef
C     du dictionnaire maintenu par l'objet. Si la clef n'existe pas
C     dans le dictionnaire, elle est ajoutée.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAR         Nom de la clef
C     VAL         La valeur de la clef
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_ASGVAL(HOBJ, VAR, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_ASGVAL
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAR
      CHARACTER*(*) VAL

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 XMAP       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP
D     CALL ERR_ASR(XMAP .NE. 0)

C---     Assigne les données
      IERR = C_MAP_ASGVAL(XMAP, VAR, VAL)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_MAP_ASGVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_MAP_EFFCLF
C
C Description:
C     La fonction DS_MAP_EFFCLF efface une clef du dictionnaire
C     maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAR         Nom de la clef
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_EFFCLF(HOBJ, VAR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_EFFCLF
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAR

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 XMAP       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP
D     CALL ERR_ASR(XMAP .NE. 0)

C---     Efface la clef
      IERR = C_MAP_EFFCLF(XMAP, VAR)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_MAP_EFFCLF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Test une clef
C
C Description:
C     La fonction DS_MAP_ESTCLF teste si la clef existe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAR         Nom de la clef
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_ESTCLF(HOBJ, VAR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_ESTCLF
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAR

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XMAP       ! handle eXterne
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP
D     CALL ERR_ASR(XMAP .NE. 0)

      DS_MAP_ESTCLF = (C_MAP_ESTCLF(XMAP, VAR) .EQ. 0)
      RETURN
      END

C************************************************************************
C Sommaire: DS_MAP_REQDIM
C
C Description:
C     La fonction DS_MAP_REQDIM retourne la dimension du dictionnaire
C     maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_REQDIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_REQDIM
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'

      INTEGER*8 XMAP       ! handle eXterne
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP

C---     Retourne la dimension
      IF (XMAP .NE. 0) THEN
         DS_MAP_REQDIM = C_MAP_REQDIM(XMAP)
      ELSE
         DS_MAP_REQDIM = 0
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: DS_MAP_REQCLF
C
C Description:
C     La fonction DS_MAP_REQCLF retourne une clef à l'aide d'un indice
C     dans le dictionnaire maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IND         Indice de la clef
C
C Sortie:
C     VAR         Nom de la clef
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_REQCLF(HOBJ, IND, VAR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_REQCLF
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       IND
      CHARACTER*(*) VAR

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 XMAP       ! handle eXterne
      INTEGER   IERR
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP
D     CALL ERR_ASR(XMAP .NE. 0)

C---     Demande la valeur associée à la clef
      IERR = C_MAP_REQCLF(XMAP, IND, VAR)
      IF (IERR .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_MAP_REQCLF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DS_MAP_REQVAL
C
C Description:
C     La fonction DS_MAP_REQVAL retourne la valeur associe à une clef
C     du dictionnaire maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VAR         Nom de la clef
C
C Sortie:
C     VAL         La valeur de la clef
C
C Notes:
C************************************************************************
      FUNCTION DS_MAP_REQVAL(HOBJ, VAR, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DS_MAP_REQVAL
CDEC$ ENDIF

      USE DS_MAP_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) VAR
      CHARACTER*(*) VAL

      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'spstrn.fi'

      INTEGER*8 XMAP       ! handle eXterne
      INTEGER   IRET
      TYPE (DS_MAP_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_MAP_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => DS_MAP_REQSELF(HOBJ)
      XMAP = SELF%XMAP
D     CALL ERR_ASR(XMAP .NE. 0)

C---     Demande la valeur associée à la clef
      IRET = C_MAP_REQVAL(XMAP, VAR, VAL)
      IF (IRET .EQ. -1) GOTO 9900
      IF (IRET .EQ. -2) GOTO 9901
      IF (IRET .NE.  0) GOTO 9902

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_DEBORDEMENT_VARIABLE', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_RECHERCHE_MAP', ' : ',
     &                       VAR(1:MIN(50,SP_STRN_LEN(VAR)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DS_MAP_REQVAL = ERR_TYP()
      RETURN
      END
