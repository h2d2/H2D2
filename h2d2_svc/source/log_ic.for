C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_LOG_XEQCTR
C     INTEGER IC_LOG_XEQMTH
C     INTEGER IC_LOG_OPBDOT
C     CHARACTER*(32) IC_LOG_REQCLS
C     INTEGER IC_LOG_REQHDL
C   Private:
C     SUBROUTINE IC_LOG_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Version 1: Le log agit comme singleton, un objet est construit
C     avec log() et les méthodes sont appelées sur lui.
C     Version 2: Avec l'arrivée des méthodes statique, le log fonctionne
C     également comme classe statique.
C
C************************************************************************
      FUNCTION IC_LOG_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LOG_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'log_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      
      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_LOG_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LE HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_LOG_REQHDL()
C        <comment>Return value: Handle on the log system</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>log</b> constructs an object and returns a handle on this object.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_LOG_AID()

9999  CONTINUE
      IC_LOG_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_LOG_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LOG_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'log_ic.fi'
C------------------------------------------------------------------------

      IC_LOG_XEQMTH = IC_LOG_OPBDOT(HOBJ, IMTH, IPRM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_LOG_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LOG_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'log_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER      IERR
      INTEGER      IVAL
      CHARACTER*64 PROP, SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_LOG_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Log level: debug</comment>
         IF (PROP .EQ. 'debug') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_DEBUG
C        <comment>Log level: verbose</comment>
         ELSEIF (PROP .EQ. 'verbose') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_VERBOSE
C        <comment>Log level: info</comment>
         ELSEIF (PROP .EQ. 'info') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_INFO
C        <comment>Log level: terse</comment>
         ELSEIF (PROP .EQ. 'terse') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_TERSE
C        <comment>Log level: warning</comment>
         ELSEIF (PROP .EQ. 'warning') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_WARNING
C        <comment>Log level: error</comment>
         ELSEIF (PROP .EQ. 'error') THEN
            WRITE(IPRM, '(2A,I12)') 'I', ',', LOG_LVL_ERROR
C        <comment>
C        Get the global log level
C        (deprecated, replaced by the zones. See add_zone, set_zone_level)
C        </comment>
         ELSEIF (PROP .EQ. 'level') THEN
            IVAL = LOG_REQNIV()
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>
C        Set the global log level to one of [error, warning, terse, info, verbose, debug]
C        (deprecated, replaced by the zones. See add_zone, set_zone_level)
C        </comment>
         IF (PROP .EQ. 'level') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            IERR = LOG_ASGNIV(IVAL)
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>add_zone</b> adds a zone to the log system</comment>
      ELSEIF (IMTH .EQ. 'add_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
C        <comment>Log level for the zone</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = LOG_AJTZONE(SVAL, IVAL)

C     <comment>The method <b>del_zone</b> deletes a log zone.</comment>
      ELSEIF (IMTH .EQ. 'del_zone') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = LOG_EFFZONE(SVAL)

C     <comment>
C     The method <b>set_zone_level</b> sets the log level
C     for the given zone.
C     </comment>
      ELSEIF (IMTH .EQ. 'set_zone_level') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Name of the zone</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
C        <comment>
C        Log level for the zone. Valid values are: info[1,2,3] or debug[30]
C        </comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = LOG_ASGNIVZ(SVAL, IVAL)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LOG_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test LOG_STS(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_LOG_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_LOG_AID()

9999  CONTINUE
      IC_LOG_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LOG_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LOG_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log_ic.fi'
      INCLUDE 'log.fi'
C-------------------------------------------------------------------------

C<comment>
C The class <b>log</b> represents the log system used to output information
C either to a file or to the console. The verbosity level can be modified
C to increase or decrease the amount of information displayed. In
C development: the log will define zone and levels of verbosity associated
C with a zone. Zones are hierarchic in a tree like manner:
C <ul>
C    <li>h2d2</li>
C    <li>h2d2.grid</li>
C    <li>h2d2.grid.coor</li>
C </ul>
C Active log zones are:
C <ul>
C    <li>h2d2</li>
C    <ul>
C       <li>h2d2.algo</li>
C       <ul>
C          <li>h2d2.algo.cinc</li>
C          <ul>
C             <li>h2d2.algo.cinc.cfl</li>
C             <li>h2d2.algo.cinc.limiter</li>
C             <li>h2d2.algo.cinc.pid</li>
C             <li>h2d2.algo.cinc.predictif</li>
C             <li>h2d2.algo.cinc.simple</li>
C          </ul>
C          <li>h2d2.algo.conv</li>
C          <li>h2d2.algo.homotopy</li>
C          <ul>
C             <li>h2d2.algo.homotopy.prgl</li>
C          </ul>
C          <li>h2d2.algo.matrix</li>
C          <ul>
C             <li>h2d2.algo.matrix.distributed</li>
C             <li>h2d2.algo.matrix.morse</li>
C          </ul>
C          <li>h2d2.algo.node</li>
C          <ul>
C             <li>h2d2.algo.node.nodelr</li>
C          </ul>
C          <li>h2d2.algo.projection</li>
C          <ul>
C             <li>h2d2.algo.projection.pod</li>
C             <li>h2d2.algo.projection.polynomial</li>
C          </ul>
C          <li>h2d2.algo.sequence</li>
C          <li>h2d2.algo.solver</li>
C          <ul>
C             <li>h2d2.algo.solver.esdirk</li>
C             <li>h2d2.algo.solver.euler</li>
C             <li>h2d2.algo.solver.gmres</li>
C             <li>h2d2.algo.solver.homotopy</li>
C             <li>h2d2.algo.solver.mrqd</li>
C             <li>h2d2.algo.solver.newton</li>
C             <li>h2d2.algo.solver.picard</li>
C             <li>h2d2.algo.solver.read</li>
C             <li>h2d2.algo.solver.rk4l</li>
C          </ul>
C       </ul>
C       <li>h2d2.data</li>
C       <ul>
C          <li>h2d2.data.condition</li>
C          <li>h2d2.data.dof</li>
C          <li>h2d2.data.elem</li>
C          <li>h2d2.data.limite</li>
C          <li>h2d2.data.list</li>
C          <li>h2d2.data.map</li>
C          <li>h2d2.data.prgl</li>
C          <li>h2d2.data.vnod</li>
C       </ul>
C       <li>h2d2.element</li>
C       <ul>
C          <li>h2d2.element.prc</li>
C          <li>h2d2.element.sv2d</li>
C          <ul>
C             <li>h2d2.element.sv2d.bc</li>
C             <ul>
C                <li>h2d2.element.sv2d.bc.cl143</li>
C                <li>h2d2.element.sv2d.bc.cl144</li>
C                <li>h2d2.element.sv2d.bc.clc</li>
C             </ul>
C             <li>h2d2.element.sv2d.clim</li>
C             <li>h2d2.element.sv2d.tide</li>
C          </ul>
C       </ul>
C       <li>h2d2.geom</li>
C       <ul>
C          <li>h2d2.geom.t6l</li>
C       </ul>
C       <li>h2d2.glob</li>
C       <ul>
C          <li>h2d2.glob.arlx</li>
C          <li>h2d2.glob.bkrs</li>
C          <li>h2d2.glob.btrk2</li>
C          <li>h2d2.glob.btrk3</li>
C          <li>h2d2.glob.compose</li>
C          <li>h2d2.glob.ktdamper</li>
C          <li>h2d2.glob.step</li>
C       </ul>
C       <li>h2d2.gpu</li>
C       <ul>
C          <li>h2d2.gpu.config</li>
C          <ul>
C             <li>h2d2.gpu.config.cuda</li>
C          </ul>
C          <li>h2d2.gpu.device</li>
C          <ul>
C             <li>h2d2.gpu.device.cl</li>
C          </ul>
C       </ul>
C       <li>h2d2.grid</li>
C       <ul>
C          <li>h2d2.grid.elem</li>
C          <li>h2d2.grid.front</li>
C          <li>h2d2.grid.metis</li>
C          <li>h2d2.grid.part</li>
C          <li>h2d2.grid.remesh</li>
C          <li>h2d2.grid.scotch</li>
C       </ul>
C       <li>h2d2.io</li>
C       <ul>
C          <li>h2d2.io.numr</li>
C       </ul>
C       <li>h2d2.mpi</li>
C       <li>h2d2.post</li>
C       <ul>
C          <li>h2d2.post.dof</li>
C          <li>h2d2.post.error</li>
C          <li>h2d2.post.hessian</li>
C          <li>h2d2.post.residuum</li>
C          <li>h2d2.post.simulation</li>
C       </ul>
C       <li>h2d2.renum</li>
C       <ul>
C          <li>h2d2.renum.blocs</li>
C          <li>h2d2.renum.color</li>
C          <li>h2d2.renum.front</li>
C       </ul>
C       <li>h2d2.reso</li>
C       <ul>
C          <li>h2d2.reso.algo</li>
C          <ul>
C             <li>h2d2.reso.algo.util</li>
C          </ul>
C          <li>h2d2.reso.solve</li>
C          <ul>
C             <li>h2d2.reso.solve.pardiso</li>
C          </ul>
C       </ul>
C       <li>h2d2.services</li>
C       <ul>
C          <li>h2d2.services.modules</li>
C       </ul>
C       <li>h2d2.spmef</li>
C       <ul>
C          <li>h2d2.spmef.gmres</li>
C          <ul>
C             <li>h2d2.spmef.gmres.solve</li>
C          </ul>
C       </ul>
C       <li>h2d2.umef</li>
C       <ul>
C          <li>h2d2.umef.spelem</li>
C       </ul>
C    </ul>
C </ul>
C</comment>
      IC_LOG_REQCLS = 'log'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_LOG_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_LOG_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log_ic.fi'
C-------------------------------------------------------------------------

      IC_LOG_REQHDL = 1999001000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_LOG_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('log_ic.hlp')

      RETURN
      END
