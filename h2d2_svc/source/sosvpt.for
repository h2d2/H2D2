C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER SO_SVPT_000
C     INTEGER SO_SVPT_999
C     INTEGER SO_SVPT_CTR
C     INTEGER SO_SVPT_DTR
C     INTEGER SO_SVPT_INI
C     INTEGER SO_SVPT_RST
C     INTEGER SO_SVPT_REQHBASE
C     LOGICAL SO_SVPT_HVALIDE
C     INTEGER SO_SVPT_GET
C     INTEGER SO_SVPT_SAVE
C     INTEGER SO_SVPT_RBCK
C   Private:
C     INTEGER SO_SVPT_CCH_000
C     INTEGER SO_SVPT_CCH_999
C     SUBROUTINE SO_SVPT_CCH_GET
C     INTEGER SO_SVPT_CCH_ADD
C     SUBROUTINE SO_SVPT_CCH_XST
C     SUBROUTINE SO_SVPT_REQSELF
C     INTEGER SO_SVPT_RAZ
C
C************************************************************************

      MODULE SO_SVPT_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: SO_SVPT_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: SO_SVPT_SELF_T
         INTEGER :: STTUS
         INTEGER :: NDATA
         INTEGER :: LDATA
         INTEGER :: LSVPT
      END TYPE SO_SVPT_SELF_T

C---     Cache
      INTEGER, PARAMETER :: SO_SVPT_CACHE_SIZE = 8
      TYPE :: SO_SVPT_CACHE_T
         TYPE (SO_SVPT_SELF_T), POINTER :: SELF => NULL()
      END TYPE SO_SVPT_CACHE_T
      TYPE(SO_SVPT_CACHE_T), SAVE, DIMENSION(:), ALLOCATABLE :: CACHE

      INTEGER, PARAMETER :: SO_SVPT_CCHSTT_RAZ = 0
      INTEGER, PARAMETER :: SO_SVPT_CCHSTT_CTR = 1
      INTEGER, PARAMETER :: SO_SVPT_CCHSTT_INI = 2
      INTEGER, PARAMETER :: SO_SVPT_CCHSTT_RST = 3
      INTEGER, PARAMETER :: SO_SVPT_CCHSTT_DTR = 4
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_SVPT_CCH_000(...) alloue la cache.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CCH_000()

      IMPLICIT NONE

      INTEGER SO_SVPT_CCH_000

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IRET
      INTEGER IC
C-----------------------------------------------------------------------

      ALLOCATE ( CACHE(SO_SVPT_CACHE_SIZE), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SO_SVPT_CCH_000 = ERR_TYP()
      RETURN
      END FUNCTION SO_SVPT_CCH_000

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_SVPT_CCH_999(...) vide la cache avant de 
C     la désallouer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CCH_999()

      IMPLICIT NONE

      INTEGER SO_SVPT_CCH_999

      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER IC
C-----------------------------------------------------------------------

      IF (.NOT. ALLOCATED(CACHE)) GOTO 9999

      DO IC=1, SO_SVPT_CACHE_SIZE
         IF (ASSOCIATED(CACHE(IC)%SELF)) THEN
            IERR = SO_ALLC_ALLRE8(0, CACHE(IC)%SELF%LSVPT)
            DEALLOCATE(CACHE(IC)%SELF)
         ENDIF
      ENDDO
      DEALLOCATE(CACHE)

9999  CONTINUE
      SO_SVPT_CCH_999 = 0
      RETURN
      END FUNCTION SO_SVPT_CCH_999

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_SVPT_CCH_GET() retourne un pointeur à
C     SELF qui a été associé et qui est libre. Elle retourne NULL()
C     s'il n'y a pas de pointeur disponible (cache miss).
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CCH_GET()

      IMPLICIT NONE

      TYPE (SO_SVPT_SELF_T), POINTER :: SO_SVPT_CCH_GET

      INCLUDE 'err.fi'

      INTEGER IC
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => NULL()
      DO IC=1,SO_SVPT_CACHE_SIZE
         IF (ASSOCIATED(CACHE(IC)%SELF)) THEN
            IF (CACHE(IC)%SELF%STTUS .EQ. SO_SVPT_CCHSTT_DTR) THEN
               SELF => CACHE(IC)%SELF
               EXIT
            ENDIF
         ENDIF
      ENDDO

      SO_SVPT_CCH_GET => SELF
      RETURN
      END FUNCTION SO_SVPT_CCH_GET

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_SVPT_CCH_ADD(...) ajoute l'entrée dans la
C     cache en éliminant, si besoin est, l'entrée la plus vieille.
C     Le save-point existe toujours et la mémoire n'est pas désallouée.
C     Le save-point est uniquement retiré de la cache.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CCH_ADD(SELF)

      IMPLICIT NONE

      INTEGER SO_SVPT_CCH_ADD
      TYPE (SO_SVPT_SELF_T), POINTER, INTENT(IN) :: SELF

      INCLUDE 'err.fi'

      INTEGER IC
C-----------------------------------------------------------------------

      DO IC=SO_SVPT_CACHE_SIZE,2,-1
         CACHE(IC)%SELF => CACHE(IC-1)%SELF
      ENDDO
      CACHE(1)%SELF => SELF

      SO_SVPT_CCH_ADD = 0
      RETURN
      END FUNCTION SO_SVPT_CCH_ADD

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée SO_SVPT_CCH_XST() retourne .TRUE. si
C     SELF est dans la cache.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CCH_XST(SELF)

      IMPLICIT NONE

      LOGICAL :: SO_SVPT_CCH_XST
      TYPE (SO_SVPT_SELF_T), POINTER, INTENT(IN) :: SELF

      INCLUDE 'err.fi'

      INTEGER IC
C-----------------------------------------------------------------------

      SO_SVPT_CCH_XST = .FALSE.
      DO IC=1,SO_SVPT_CACHE_SIZE
         IF (ASSOCIATED(SELF, CACHE(IC)%SELF)) THEN
            SO_SVPT_CCH_XST = .TRUE.
            EXIT
         ENDIF
      ENDDO

      RETURN
      END FUNCTION SO_SVPT_CCH_XST

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée SO_SVPT_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (SO_SVPT_SELF_T), POINTER :: SO_SVPT_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)


      SO_SVPT_REQSELF => SELF
      RETURN
      END FUNCTION SO_SVPT_REQSELF

      END MODULE SO_SVPT_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_000
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INCLUDE 'sosvpt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(SO_SVPT_HBASE, 'Save Points')
      IERR = SO_SVPT_CCH_000()

      SO_SVPT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_999
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INCLUDE 'sosvpt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL SO_SVPT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(SO_SVPT_HBASE, SO_SVPT_DTR)
      IERR = SO_SVPT_CCH_999()

      SO_SVPT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_CTR
CDEC$ ENDIF

      USE SO_SVPT_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sosvpt.fc'

      INTEGER IERR, IRET
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
      LOGICAL INCCH
C------------------------------------------------------------------------

C---     Alloue la structure
      INCCH = .TRUE.
      SELF => SO_SVPT_CCH_GET()
      IF (.NOT. ASSOCIATED(SELF)) THEN
         INCCH = .FALSE.
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         IERR = SO_SVPT_CCH_ADD(SELF)
      ENDIF

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   SO_SVPT_HBASE,
     &                                   C_LOC(SELF))

C---     Si on n'est pas dans la cache: initialise
      IF (.NOT. INCCH) THEN
         IF (ERR_GOOD()) IERR = SO_SVPT_RAZ(HOBJ)
D        CALL ERR_ASR(ERR_BAD() .OR. SO_SVPT_HVALIDE(HOBJ))
      ENDIF

C---     Change le status
      IF (ERR_GOOD()) SELF%STTUS = SO_SVPT_CCHSTT_CTR
         
       GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      SO_SVPT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée SO_SVPT_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_RAZ(HOBJ)

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sosvpt.fc'

      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)

C---     Remise à zéro
      SELF%STTUS = SO_SVPT_CCHSTT_RAZ  ! Status
      SELF%NDATA = 0
      SELF%LDATA = 0
      SELF%LSVPT = 0

      SO_SVPT_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_DTR
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = SO_SVPT_RST(HOBJ)

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)
         
C---     Change le status
      SELF%STTUS = SO_SVPT_CCHSTT_DTR

C---     Si on n'est pas dans la cache: détruis
      IF (.NOT. SO_SVPT_CCH_XST(SELF)) THEN
C---        Dé-alloue
D        CALL ERR_ASR(ASSOCIATED(SELF))
         DEALLOCATE(SELF)
      ENDIF

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, SO_SVPT_HBASE)

      SO_SVPT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SO_SVPT_INI
C
C Description:
C     La fonction SO_SVPT_INI va créer un point de restauration (Save Point)
C     pour la table réelle de pointeur LDTA. Il y a copie des données.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C     LDTA     Pointeur aux données
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_INI(HOBJ, LDTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_INI
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER LDTA

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER LSPT
      INTEGER NDTA
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_ALLC_HEXIST(LDTA))
!!!D     CALL ERR_PRE(SO_ALLC_REQTYP(LDTA) .EQ. )
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)

C---     Récupère les paramètres
      NDTA  = SO_ALLC_REQLEN(LDTA)

C---     Alloue la mémoire
      LSPT = SELF%LSVPT
D     CALL ERR_ASR(LSPT .EQ. 0 .OR. SO_ALLC_HEXIST(LSPT))
      IERR = SO_ALLC_ALLRE8(NDTA, LSPT)

C---     Stocke les données
      IF (ERR_GOOD()) THEN
         SELF%NDATA = NDTA
         SELF%LDATA = LDTA    ! Bloc
         SELF%LSVPT = LSPT    ! Bloc
      ENDIF

C---     Copie les données vers le SavePoint
      CALL DCOPY(SELF%NDATA,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LDATA)), 1,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LSVPT)), 1)

C---     Change le status
      IF (ERR_GOOD()) THEN
         SELF%STTUS = SO_SVPT_CCHSTT_INI
      ENDIF

      SO_SVPT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SO_SVPT_RST
C
C Description:
C     La fonction SO_SVPT_RST va relâcher (release) le point de
C     restauration (Save Point).
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IDSP     Identifiant du point de restauration
C
C Sortie:
C
C Notes:
C     Les blocs d'allocation ne sont pas libérés. Ils sont mis en négatif
C     comme flag et redeviennent disponibles. On a ainsi un effet de cache
C     sur la mémoire.
C************************************************************************
      FUNCTION SO_SVPT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_RST
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fc'

      INTEGER IERR
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%STTUS .EQ. SO_SVPT_CCHSTT_INI)
D     CALL ERR_ASR(SO_ALLC_HEXIST(SELF%LSVPT))

C---     Change le status
      SELF%STTUS = SO_SVPT_CCHSTT_RST
      
C---     Si on n'est pas dans la cache: reset
      IF (.NOT. SO_SVPT_CCH_XST(SELF)) THEN
         IERR = SO_ALLC_ALLRE8(0, SELF%LSVPT)
         IERR = SO_SVPT_RAZ(HOBJ)
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_SVPT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SO_SVPT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_REQHBASE
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INCLUDE 'sosvpt.fi'
C------------------------------------------------------------------------

      SO_SVPT_REQHBASE = SO_SVPT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SO_SVPT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_HVALIDE
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      SO_SVPT_HVALIDE = OB_OBJN_HVALIDE(HOBJ, SO_SVPT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: SO_SVPT_SPGET
C
C Description:
C     La fonction SO_SVPT_SPGET retourne le pointeur aux données du point
C     de restauration (Save Point).
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C     LSPT     Pointeur aux données
C
C Notes:
C     Conceptuellement la méthode est const et les données ne sont pas
C     destinées à être modifiées. Elles sont READ-ONLY.
C************************************************************************
      FUNCTION SO_SVPT_GET(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_GET
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fc'

      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%STTUS .EQ. SO_SVPT_CCHSTT_INI)
D     CALL ERR_ASR(SO_ALLC_HEXIST(SELF%LSVPT))

      SO_SVPT_GET = SELF%LSVPT
      RETURN
      END

C************************************************************************
C Sommaire: SO_SVPT_SPSET
C
C Description:
C     La fonction SO_SVPT_SAVE va assigner les valeurs des données au point
C     de restauration (Save Point).
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_SAVE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_SAVE
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%STTUS .EQ. SO_SVPT_CCHSTT_INI)
D     CALL ERR_ASR(SO_ALLC_HEXIST(SELF%LSVPT))

C---     Copie les données vers le SavePoint
      CALL DCOPY(SELF%NDATA,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LDATA)), 1,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LSVPT)), 1)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_SVPT_SAVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SO_SVPT_RBCK
C
C Description:
C     La fonction SO_SVPT_RBCK va restaurer les valeurs à partir du
C     point de restauration (Save Point).
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_SVPT_RBCK(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_SVPT_RBCK
CDEC$ ENDIF

      USE SO_SVPT_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sosvpt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      TYPE (SO_SVPT_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_SVPT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => SO_SVPT_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%STTUS .EQ. SO_SVPT_CCHSTT_INI)
D     CALL ERR_ASR(SO_ALLC_HEXIST(SELF%LSVPT))

C---     Reset les données
      CALL DCOPY(SELF%NDATA,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LSVPT)), 1,
     &           VA(SO_ALLC_REQVIND(VA,SELF%LDATA)), 1)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_SVPT_RBCK = ERR_TYP()
      RETURN
      END
