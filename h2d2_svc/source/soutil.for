C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SO_UTIL_MDL000
C     INTEGER SO_UTIL_MDL999
C     INTEGER SO_UTIL_MDLVER
C     INTEGER SO_UTIL_MDLCTR
C     INTEGER SO_UTIL_REQHMDLHBASE
C     INTEGER SO_UTIL_REQHMDLNOM
C     INTEGER SO_UTIL_REQHMDLCOD
C     INTEGER SO_UTIL_MDLITR
C   Private:
C     INTEGER SO_UTIL_MDLHBASE_CB
C     INTEGER SO_UTIL_MDLVER_CB
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les modules
C
C Description:
C     La fonction SO_UTIL_MDL000 initialise les tables de la classe pour
C     chacun des modules chargés. Elle doit obligatoirement être appelée
C     avant toute utilisation des modules.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDL000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_MDL000
CDEC$ ENDIF

      USE SO_MDUL_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_SELF
      INTEGER  IOB
      INTEGER  IERR
      INTEGER  HBSM
      INTEGER  HMDL, HFNC
      INTEGER  HBSE
      INTEGER  NMDL
      INTEGER  SO_UTIL_MDLHBASE_CB
      EXTERNAL SO_UTIL_MDLHBASE_CB
      BYTE, POINTER :: B(:)
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

C---    Récupère le handle de base
      HBSE = SO_MDUL_REQHBASE()
      IF (HBSE .LE. 0) GOTO 9900
      B => SO_ALLC_CST2B(HBSE)

C---     Boucle sur les modules
      HBSM = SO_MDUL_REQHBASE()
      NMDL = OB_OBJC_REQNOBJ(HBSM)
      DO IOB=1,NMDL
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSM)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, '000', HFNC)
         IF (ERR_GOOD()) THEN
            IERR = SO_FUNC_CALL(HFNC)
            IF (ERR_BAD()) GOTO 9901
            IERR = SO_FUNC_DTR(HFNC)
         ELSEIF (ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) THEN
            CALL ERR_RESET()
         ENDIF

         IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQHBASE', HFNC)
         IF (ERR_GOOD()) THEN
            IERR = SO_FUNC_CBFNC1(HFNC,
     &                            SO_UTIL_MDLHBASE_CB,
     &                            B)
            IF (ERR_BAD()) GOTO 9901
            IERR = SO_FUNC_DTR(HFNC)
            SO_MDUL_SELF => SO_MDUL_REQSELF(HMDL)
            SO_MDUL_SELF%HBSE = HBSE
         ELSEIF (ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) THEN
            CALL ERR_RESET()
         ENDIF

100      CONTINUE
         IF (ERR_BAD()) GOTO 9901
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_BASE_MODULE_NON_INITIALISEE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_INITIALISATION_MODULE',': ',
     &                        SO_MDUL_REQNOM(HMDL)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_UTIL_MDL000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée SO_UTIL_MDLHBASE_CB est le call-back pour
C     récupérer le HBASE.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDLHBASE_CB(F, HBSE)

      IMPLICIT NONE

      INTEGER  SO_UTIL_MDLHBASE_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  HBSE

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HBSE = F()

      SO_UTIL_MDLHBASE_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Finalise les modules
C
C Description:
C     La fonction SO_UTIL_MDL999 finalise les tables de la classe pour
C     chacun des modules chargés.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDL999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_MDL999
CDEC$ ENDIF

      USE SO_FUNC_M
      IMPLICIT NONE

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'

      INTEGER IOB
      INTEGER IERR, IDUM
      INTEGER HMDL, HFNC
      INTEGER HBSE
      INTEGER NOBJ
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

C---    Récupère le handle de base
      HBSE = SO_MDUL_REQHBASE()
      IF (HBSE .LE. 0) GOTO 9999

C---     Boucle sur les modules
      NOBJ = OB_OBJC_REQNOBJ(HBSE)
      DO IOB=1,NOBJ
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSE)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         IERR = SO_MDUL_REQFNC(HMDL, '999', HFNC)

         IF (ERR_GOOD()) THEN
            IERR = SO_FUNC_CALL(HFNC)
            IDUM = SO_FUNC_DTR(HFNC)
            IF (IERR .NE. 0) GOTO 9900
         ELSEIF (ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) THEN
               CALL ERR_RESET()
         ENDIF

100      CONTINUE
         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_DESINSTALLATION_MODULE',': ',
     &                        SO_MDUL_REQNOM(HMDL)
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_UTIL_MDL999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime la version des modules
C
C Description:
C     La fonction SO_UTIL_MDLVER imprime pour chaque module son type, son nom
C     et sa version. Les modules sont triés par type.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDLVER()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_MDLVER
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'somdul.fi'

      INTEGER  ITYP, IDUM
      INTEGER  IERR
      INTEGER  SO_UTIL_MDLVER_CB
      EXTERNAL SO_UTIL_MDLVER_CB
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      WRITE(LOG_BUF, '(3A)') 'MSG_CONFIGURATION_SYSTEME'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()
      CALL LOG_INCIND()

C---     Itère sur les types de module
      DO ITYP=SO_MDUL_TYP_INDEFINI,SO_MDUL_TYP_TAG_LAST
         IDUM = 0
         IERR = SO_UTIL_MDLITR(ITYP, SO_UTIL_MDLVER_CB, IDUM)
      ENDDO

      CALL LOG_DECIND()
      CALL LOG_DECIND()
      SO_UTIL_MDLVER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction call-back de SO_UTIL_MDLVER
C
C Description:
C     La fonction SO_UTIL_MDLVER_CB
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDLVER_CB(IMDL, HMDL)

      IMPLICIT NONE

      INTEGER  SO_UTIL_MDLVER_CB
      INTEGER  IMDL
      INTEGER  HMDL

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*64 NOM
      CHARACTER*64 TYP
      CHARACTER*64 VER
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HMDL))
C------------------------------------------------------------------------

      IF (IMDL .EQ. 0) THEN
         CALL LOG_DECIND()
         TYP = SO_MDUL_REQNOMTYP(HMDL)
         WRITE(LOG_BUF, '(A)') TYP(1:SP_STRN_LEN(TYP))
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
      ENDIF

      NOM = SO_MDUL_REQNOM(HMDL)
      VER = SO_MDUL_REQVER(HMDL)
      IF (SP_STRN_LEN(VER) .GT. 0) THEN
         WRITE(LOG_BUF, '(3A)') NOM(1:SP_STRN_LEN(NOM)),
     &                          '  Version ', VER(1:SP_STRN_LEN(VER))
      ELSE
         WRITE(LOG_BUF, '(1A)') NOM(1:SP_STRN_LEN(NOM))
      ENDIF
      CALL LOG_ECRIS(LOG_BUF)

      IMDL = IMDL + 1
      SO_UTIL_MDLVER_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis les modules
C
C Description:
C     La fonction SO_UTIL_MDLCTR construit tous les modules à partir
C     d'un fichier de configuration. Elle crée pour chaque section
C     valide un module.
C     <p>
C     Un nom de fichier de configuration débutant par un "@" désigne
C     un fichier qui contient, un par ligne, uniquement d'autres noms
C     de fichiers.
C
C Entrée:
C     CHARACTER*(*) REPBASE      Repertoire de base
C     CHARACTER*(*) FICCONF      Fichier de configuration
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SO_UTIL_MDLCTR(REPBASE, FICCONF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_MDLCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) REPBASE
      CHARACTER*(*) FICCONF

      INCLUDE 'soutil.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER IDUM, IPEQ
      INTEGER HMDL
      INTEGER LLEN
      INTEGER MR
      INTEGER MDLCOD
      INTEGER NLIGNE
      INTEGER NLUS
      INTEGER ITYP
      CHARACTER*(256)  NOMFIC
      CHARACTER*(256)  LIGNE
      CHARACTER*(256)  LHS
      CHARACTER*(256)  MDLTYP
      CHARACTER*(256)  MDLNAM
      CHARACTER*(256)  DLLNAM
      CHARACTER*(256)  ROONAM

      INTEGER STK_MAXSTK
      PARAMETER (STK_MAXSTK = 16)
      INTEGER        STK_INDSTK
      INTEGER        STK_MR    (STK_MAXSTK)
      INTEGER        STK_NLIGNE(STK_MAXSTK)
      CHARACTER*(256)STK_FNAME (STK_MAXSTK)

      CHARACTER*(2048) GARDE
      LOGICAL          EST_RSP
      LOGICAL          ATEOF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FICCONF) .GT. 0)
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.services.modules'

C---     Initialise la pile
      STK_INDSTK = 0

C---     Initialise
      EST_RSP = .FALSE.
      NOMFIC = FICCONF(1:SP_STRN_LEN(FICCONF))
      IF (NOMFIC(1:1) .EQ. '@') THEN
         EST_RSP = .TRUE.
         NOMFIC = FICCONF(2:SP_STRN_LEN(FICCONF))
      ENDIF
      GARDE = NOMFIC(1:SP_STRN_LEN(NOMFIC))
      MR = IO_UTIL_FREEUNIT()
      OPEN(UNIT  = MR,
     &     FILE  = NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &     STATUS= 'OLD',
     &     ERR   = 9900)
      NLIGNE = 0

C---     Lis les fichiers
100   CONTINUE
         READ(MR,'(A)', ERR = 9901, END = 199) LIGNE
         NLIGNE = NLIGNE + 1
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 3) GOTO 100
         IF (LIGNE(1:1) .EQ. '#') GOTO 100
         IF (LIGNE(1:1) .EQ. '!') GOTO 100

C---        Fichier INCLUDE
         LHS = LIGNE(1:LLEN)
         CALL SP_STRN_UCS(LHS)
         IF ((LHS(1:8) .EQ. 'INCLUDE ') .OR.
     &       (EST_RSP .AND. STK_INDSTK .EQ. 0)) THEN
C---           Extrait le nom relatif du fichier
            IF (LHS(1:8) .EQ. 'INCLUDE ') THEN
               LIGNE = LIGNE(9:LLEN)
               CALL SP_STRN_TRM(LIGNE)
               LIGNE = LIGNE(2:SP_STRN_LEN(LIGNE)-1)
            ENDIF
            IF (REPBASE(1:SP_STRN_LEN(REPBASE)) .EQ.
     &          LIGNE(1:SP_STRN_LEN(REPBASE))) THEN
               IRET = C_OS_BASENAME(LIGNE)
D              CALL ERR_ASR(IRET .EQ. 0)
            ENDIF
C---           Bloque les inclusions multiples
            LLEN = SP_STRN_LEN(LIGNE)
            IF (INDEX(GARDE, '<@>' // LIGNE(1:LLEN)) .LE. 0) THEN
C---              Push le stack
               IF (STK_INDSTK .EQ. STK_MAXSTK) GOTO 9905
               STK_INDSTK = STK_INDSTK + 1
               STK_MR    (STK_INDSTK) = MR
               STK_NLIGNE(STK_INDSTK) = NLIGNE
               STK_FNAME (STK_INDSTK) = NOMFIC
C---              Ouvre le nouveau fichier
               NOMFIC = LIGNE(1:SP_STRN_LEN(LIGNE))
               GARDE = GARDE(1:SP_STRN_LEN(GARDE)) //
     &                 '<@>' //
     &                 NOMFIC(1:SP_STRN_LEN(NOMFIC))
               MR = IO_UTIL_FREEUNIT()
               LIGNE = REPBASE(1:SP_STRN_LEN(REPBASE))      ! g77: Var tmp
               OPEN(UNIT  = MR,
     &              FILE  = LIGNE(1:SP_STRN_LEN(LIGNE))     ! g77: erreur
     &                    // '/'                            ! avec REPBASE
     &                    // NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &              STATUS= 'OLD',
     &              ERR   = 9900)
               NLIGNE = 0
               GOTO 100
            ENDIF
         ENDIF

C---        Bloc MODULE
         CALL SP_STRN_UCS(LIGNE)
         IF (LIGNE(   1:   1) .NE. '[') GOTO 100
         IF (LIGNE(LLEN:LLEN) .NE. ']') GOTO 100
         LIGNE = LIGNE(2:LLEN-1)
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 0) GOTO 9902
         IF (LIGNE(1:LLEN) .NE. 'COMPONENT') GOTO 100

C---        Variables d'un MODULE
         NLUS = 0
         MDLCOD = -1
         ATEOF = .FALSE.
200      CONTINUE
            READ(MR, '(A)', ERR = 9901, END = 298) LIGNE
            NLIGNE = NLIGNE + 1
            CALL SP_STRN_TRM(LIGNE)
            LLEN = SP_STRN_LEN(LIGNE)
            IF (LLEN .LE. 0) GOTO 299

            LHS = LIGNE(1:LLEN)
            IPEQ = SP_STRN_LFT(LHS, '=')
            IF (IPEQ   .LE.    0) GOTO 299
            IF (IPEQ+1 .GE. LLEN) GOTO 9903
            CALL SP_STRN_UCS(LHS)
            CALL SP_STRN_DLC(LHS, ' ')
            IF (LHS(1:SP_STRN_LEN(LHS)) .EQ. 'COMPONENTTYPE') THEN
               MDLTYP = LIGNE(IPEQ+1:LLEN)
               CALL SP_STRN_TRM(MDLTYP)
               CALL SP_STRN_UCS(MDLTYP)
               CALL SP_STRN_DLC(MDLTYP, ' ')
               LLEN = SP_STRN_LEN(MDLTYP)
               ITYP = SO_MDUL_TYP_INDEFINI
               IF (MDLTYP(1:LLEN) .EQ. 'COMMANDMODULE') THEN
                  ITYP = SO_MDUL_TYP_CMDMDL
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'COMMANDOBJECT') THEN
                  ITYP = SO_MDUL_TYP_CMDOBJ
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'COMMANDFUNCTION') THEN
                  ITYP = SO_MDUL_TYP_CMDFNC
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'OBJECT') THEN
                  ITYP = SO_MDUL_TYP_OBJECT
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'GEOMETRY') THEN
                  ITYP = SO_MDUL_TYP_GEOMETRIE
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'ELEMENT') THEN
                  ITYP = SO_MDUL_TYP_ELEMENT
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'BOUNDARYCONDITION') THEN
                  ITYP = SO_MDUL_TYP_ELEMENT_BC
               ELSEIF (MDLTYP(1:LLEN) .EQ. 'NUMERICALINTEGRATION')
     &                  THEN
                  ITYP = SO_MDUL_TYP_NUMINTEGRATION
               ENDIF
               NLUS = NLUS + 1
            ENDIF
            IF (LHS(1:SP_STRN_LEN(LHS)) .EQ. 'COMPONENTNAME') THEN
               MDLNAM = LIGNE(IPEQ+1:LLEN)
               CALL SP_STRN_TRM(MDLNAM)
               CALL SP_STRN_UCS(MDLNAM)
               CALL SP_STRN_DMC(MDLNAM, ' ')
               NLUS = NLUS + 10
            ENDIF
            IF (LHS(1:SP_STRN_LEN(LHS)) .EQ. 'COMPONENTID') THEN
               LIGNE = LIGNE(IPEQ+1:LLEN)
               CALL SP_STRN_TRM(LIGNE)
               LLEN  = SP_STRN_LEN(LIGNE)
               IF (SP_STRN_INT(LIGNE(1:LLEN))) THEN
                  READ(LIGNE(1:LLEN), *, ERR=9903, END=9903) MDLCOD
               ELSE
                  IRET = C_ST_CRC32(LIGNE(1:LLEN), MDLCOD)
                  IF (IRET .NE. 0) GOTO 9903
               ENDIF
            ENDIF
            IF (LHS(1:SP_STRN_LEN(LHS)) .EQ. 'DLLNAME') THEN
               DLLNAM = LIGNE(IPEQ+1:LLEN)
               CALL SP_STRN_TRM(DLLNAM)
               NLUS = NLUS + 100
            ENDIF
            IF (LHS(1:SP_STRN_LEN(LHS)) .EQ. 'FUNCTIONROOT') THEN
               ROONAM = LIGNE(IPEQ+1:LLEN)
               CALL SP_STRN_TRM(ROONAM)
               NLUS = NLUS + 1000
            ENDIF

         GOTO 200
298      CONTINUE
         ATEOF = .TRUE.
299      CONTINUE
         IF (NLUS .NE. 1111) GOTO 9903
         IF (ITYP .EQ. SO_MDUL_TYP_INDEFINI) GOTO 9904

         IF (ERR_GOOD())  THEN
            WRITE(LOG_BUF,'(5A)') 'MSG_ENREGISTRE_MODULE#<25>#: ',
     &                             MDLNAM(1:SP_STRN_LEN(MDLNAM)), ' (',
     &                             DLLNAM(1:SP_STRN_LEN(DLLNAM)), ')'
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDIF
         HMDL = 0
         IF (ERR_GOOD()) IERR = SO_MDUL_CTR(HMDL)
         IF (ERR_GOOD()) IERR = SO_MDUL_INI(HMDL,
     &                                      ITYP,
     &                                      MDLCOD,
     &                                      MDLNAM,
     &                                      DLLNAM,
     &                                      ROONAM)
         IF (ERR_BAD() .OR. ATEOF) GOTO 199

      GOTO 100
199   CONTINUE

C---     Fin de fichier, pop le stack
      IF (ERR_GOOD() .AND. STK_INDSTK .GT. 0) THEN
         CLOSE(MR)
         MR     = STK_MR    (STK_INDSTK)
         NLIGNE = STK_NLIGNE(STK_INDSTK)
         NOMFIC = STK_FNAME (STK_INDSTK)
         STK_INDSTK = STK_INDSTK - 1
         GOTO 100
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_SECTION_VIDE',': ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A,I4,2A)') 'ERR_SECTION_INVALIDE',': ',
     &                       '(', NLIGNE,')',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(3A,I4,2A)') 'ERR_TYPE_INVALIDE',': ',
     &                       '(', NLIGNE,') ',
     &                        NOMFIC(1:SP_STRN_LEN(NOMFIC))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_PILE_INCLUDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CLOSE(MR)
      IF (STK_INDSTK .GT. 0) THEN
         MR = STK_MR(STK_INDSTK)
         STK_INDSTK = STK_INDSTK - 1
         GOTO 9999
      ENDIF

D     CALL ERR_PST(ERR_BAD() .OR. STK_INDSTK .EQ. 0)
      SO_UTIL_MDLCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle au module
C
C Description:
C     La fonction SO_UTIL_REQHMDLHBASE retourne le handle au module
C     dont le handle de classe est passé en argument.
C
C Entrée:
C     HCLS        Handle de base de classe
C
C Sortie:
C     HMDL        Handle sur le module de la classe
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_REQHMDLHBASE(HCLS, HMDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_REQHMDLHBASE
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER HCLS
      INTEGER HMDL

      INCLUDE 'soutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_SELF
      INTEGER IERR
      INTEGER IOB
      INTEGER HBSE, HBCL
      INTEGER NOBJ
      LOGICAL TROUVE
      CHARACTER*(64) NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(HCLS .GT. 0)
C------------------------------------------------------------------------

C---     Demande le handle de base (HBCL) de HCLS
      IERR = OB_OBJC_REQCLS(HBCL, NOBJ, NOM, HCLS)

C---     Recherche le module
      TROUVE = .FALSE.
      HBSE = SO_MDUL_REQHBASE()
      NOBJ = OB_OBJC_REQNOBJ(HBSE)
      DO IOB=1,NOBJ
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSE)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         SO_MDUL_SELF => SO_MDUL_REQSELF(HMDL)
         IF (SO_MDUL_SELF%HBSE .EQ. HBCL) THEN
            TROUVE = .TRUE.
            GOTO 199
         ENDIF

      ENDDO
199   CONTINUE
      IF (.NOT. TROUVE) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_MODULE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12)') 'MSG_MODULE_INCONNU_POUR_HANDLE',': ',
     &                           HCLS
      CALL ERR_AJT( ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_UTIL_REQHMDLHBASE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle au module
C
C Description:
C     La fonction SO_UTIL_REQHMDLNOM retourne le handle au module
C     dont les type et nom sont passés en argument.
C
C Entrée:
C     ITYP        Type du module
C     FRML        Nom du module
C
C Sortie:
C     HMDL        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_REQHMDLNOM(ITYP, FRML, HMDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_REQHMDLNOM
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER       ITYP
      CHARACTER*(*) FRML
      INTEGER       HMDL

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'spstrn.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_SELF
      INTEGER       IOB
      INTEGER       HOBJ
      INTEGER HBSE
      INTEGER NOBJ
      INTEGER       LCIBLE
      CHARACTER*256 NOMCIBLE
      CHARACTER*256 NOMTEST
      LOGICAL       TROUVE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ITYP .NE. SO_MDUL_TYP_INDEFINI)
D     CALL ERR_PRE(SP_STRN_LEN(FRML) .GT. 0)
C------------------------------------------------------------------------

C---     Canonise le nom du module
      NOMCIBLE = FRML(1:SP_STRN_LEN(FRML))
      CALL SP_STRN_TRM(NOMCIBLE)
      CALL SP_STRN_UCS(NOMCIBLE)
      CALL SP_STRN_DMC(NOMCIBLE, ' ')
      LCIBLE = SP_STRN_LEN(NOMCIBLE)
      IF (LCIBLE .LE. 0) GOTO 9900

C---     Recherche le module
      TROUVE = .FALSE.
      HBSE = SO_MDUL_REQHBASE()
      NOBJ = OB_OBJC_REQNOBJ(HBSE)
      DO IOB=1,NOBJ
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSE)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         SO_MDUL_SELF => SO_MDUL_REQSELF(HMDL)
         IF (ITYP .NE. SO_MDUL_SELF%ITYP ) GOTO 100
         NOMTEST = SO_MDUL_SELF%FRML
         IF ((SP_STRN_LEN(NOMTEST) .EQ. LCIBLE) .AND.
     &       (NOMTEST(1:LCIBLE) .EQ. NOMCIBLE(1:LCIBLE))) THEN
            TROUVE = .TRUE.
            GOTO 199
         ENDIF

100      CONTINUE
      ENDDO
199   CONTINUE
      IF (.NOT. TROUVE) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_MODULE_INVALIDE',': ',
     &                        FRML(1:SP_STRN_LEN(FRML))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_MODULE_INCONNU',': ',
     &                        FRML(1:SP_STRN_LEN(FRML))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_UTIL_REQHMDLNOM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle au module
C
C Description:
C     La fonction SO_UTIL_REQHMDLCOD retourne le handle au module
C     dont les type et code sont passés en argument.
C
C Entrée:
C     ITYP        Type du module
C     ICOD        Code du module
C
C Sortie:
C     HMDL        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_REQHMDLCOD(ITYP, ICOD, HMDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_REQHMDLCOD
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER ITYP
      INTEGER ICOD
      INTEGER HMDL

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_SELF
      INTEGER IOB
      INTEGER HBSE
      INTEGER NOBJ
      LOGICAL TROUVE
C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(ITYP .NE. SO_MDUL_TYP_INDEFINI)
D     CALL ERR_PRE(ICOD .GT. 0)
C------------------------------------------------------------------------

C---     Recherche le module
      TROUVE = .FALSE.
      HBSE = SO_MDUL_REQHBASE()
      NOBJ = OB_OBJC_REQNOBJ(HBSE)
      DO IOB=1,NOBJ
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSE)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         SO_MDUL_SELF => SO_MDUL_REQSELF(HMDL)
         IF (ITYP .NE. SO_MDUL_SELF%ITYP) GOTO 100
         IF (SO_MDUL_SELF%CODE .EQ. ICOD) THEN
            TROUVE = .TRUE.
            GOTO 199
         ENDIF

100      CONTINUE
      ENDDO
199   CONTINUE
      IF (.NOT. TROUVE) GOTO 9901

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_CODE_MODULE_INCONNU',': ',ICOD
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SO_UTIL_REQHMDLCOD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Itère sur les modules
C
C Description:
C     La fonction SO_UTIL_MDLITR itère sur tous les modules et pour
C     chaque module du type demandé, appelle la fonction call-back.
C
C Entrée:
C     ITYP        Type de module demandé
C     F_CB        Fonction call-back
C     HPRM        Paramètre de la fonction call-back
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SO_UTIL_MDLITR(ITYP, F_CB, HPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_UTIL_MDLITR
CDEC$ ENDIF

      USE SO_MDUL_M
      IMPLICIT NONE

      INTEGER  ITYP
      INTEGER  F_CB
      EXTERNAL F_CB
      INTEGER  HPRM

      INCLUDE 'soutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'

      TYPE (SO_MDUL_SELF_T), POINTER :: SO_MDUL_SELF
      INTEGER IERR
      INTEGER IOB
      INTEGER HMDL
      INTEGER HBSE
      INTEGER NOBJ
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Boucle sur les modules
!$omp master
      HBSE = SO_MDUL_REQHBASE()
      NOBJ = OB_OBJC_REQNOBJ(HBSE)
      DO IOB=1,NOBJ
         HMDL = OB_OBJC_REQIOBJ(IOB, HBSE)
D        CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

         SO_MDUL_SELF => SO_MDUL_REQSELF(HMDL)
         IF (ITYP .NE. SO_MDUL_SELF%ITYP) GOTO 100

         IERR = F_CB(HPRM, HMDL)
         IF (ERR_BAD()) GOTO 199

100      CONTINUE
      ENDDO
199   CONTINUE
!$omp end master

      SO_UTIL_MDLITR = ERR_TYP()
      RETURN
      END
