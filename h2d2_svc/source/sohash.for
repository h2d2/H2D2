C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Interface:
C   H2D2 Module: SO
C      H2D2 Class: SO_HASH
C         FTN (Sub)Module: SO_HASH_M
C            Public:
C               SUBROUTINE SO_HASH_CTR
C               INTEGER SO_HASH_DTR
C            Private:
C               LOGICAL SO_HASH_OPEQ
C               LOGICAL SO_HASH_OPNE
C            
C            FTN Type: SO_HASH_T
C               Public:
C                  INTEGER SO_HASH_RST
C               Private:
C                  INTEGER SO_HASH_RAZ
C                  INTEGER SO_HASH_INIB
C                  INTEGER SO_HASH_INIC
C                  INTEGER SO_HASH_INIH
C                  INTEGER SO_HASH_INIP
C                  LOGICAL SO_HASH_ESTEQ
C                  LOGICAL SO_HASH_ESTEQB
C                  LOGICAL SO_HASH_ESTEQC
C                  LOGICAL SO_HASH_ESTEQH
C
C************************************************************************

      MODULE SO_HASH_M

      IMPLICIT NONE
      PUBLIC

C---     Type de donnée associé à la classe
      TYPE :: SO_HASH_T
         INTEGER, DIMENSION(:) :: MD5(4)
      CONTAINS
         ! ---  Méthode publiques
         GENERIC,   PUBLIC :: INI  => SO_HASH_INIB,
     &                                SO_HASH_INIC,
     &                                SO_HASH_INIH,
     &                                SO_HASH_INIP
         PROCEDURE, PUBLIC :: RST  => SO_HASH_RST
         GENERIC,   PUBLIC :: ESTEQ=> SO_HASH_ESTEQ,
     &                                SO_HASH_ESTEQB,
     &                                SO_HASH_ESTEQC,
     &                                SO_HASH_ESTEQH

         ! ---  Méthode privées
         PROCEDURE, PRIVATE :: RAZ => SO_HASH_RAZ
         PROCEDURE, PRIVATE :: SO_HASH_INIB
         PROCEDURE, PRIVATE :: SO_HASH_INIC
         PROCEDURE, PRIVATE :: SO_HASH_INIH
         PROCEDURE, PRIVATE :: SO_HASH_INIP
         PROCEDURE, PRIVATE :: SO_HASH_ESTEQ
         PROCEDURE, PRIVATE :: SO_HASH_ESTEQB
         PROCEDURE, PRIVATE :: SO_HASH_ESTEQC
         PROCEDURE, PRIVATE :: SO_HASH_ESTEQH
      END TYPE SO_HASH_T

      ! ---  Constructor - Destructor
      PUBLIC :: SO_HASH_CTR_SELF
      PUBLIC :: DEL
      INTERFACE DEL
         MODULE PROCEDURE SO_HASH_DTR
      END INTERFACE

      INTERFACE OPERATOR (.EQ.)
         MODULE PROCEDURE SO_HASH_OPEQ
      END INTERFACE

      INTERFACE OPERATOR (.NE.)
         MODULE PROCEDURE SO_HASH_OPNE
      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction SO_HASH_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION SO_HASH_CTR_SELF() RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_CTR_SELF
CDEC$ ENDIF

      TYPE(SO_HASH_T) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

C---     Initialise
      IERR = SELF%RAZ()

      RETURN
      END FUNCTION SO_HASH_CTR_SELF

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction SO_HASH_DTR agit comme destructeur de la classe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_DTR
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF
C-----------------------------------------------------------------------

      SO_HASH_DTR = SELF%RST()
      RETURN
      END FUNCTION SO_HASH_DTR

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée SO_HASH_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_RAZ(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_RAZ
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SELF%MD5(:) = 0

      SO_HASH_RAZ = ERR_TYP()
      RETURN
      END FUNCTION SO_HASH_RAZ

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction SO_HASH_INI initialise l'objet à partir du buffer BUF.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LBUF        Longueur du buffer
C     BUF         Buffer
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_INIB(SELF, LBUF, BUF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_INIB
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF
      INTEGER LBUF
      BYTE    BUF(*)

      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Calcule le MD5
      IERR = C_ST_MD5(LBUF,
     &                BUF,
     &                SELF%MD5)

      SO_HASH_INIB = ERR_TYP()
      RETURN
      END FUNCTION SO_HASH_INIB

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction SO_HASH_INI initialise l'objet à partir
C     de la chaîne STR.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     STR         Chaîne de caractères
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_INIC(SELF, STR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_INIC
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF
      CHARACTER*(*), INTENT(IN) :: STR

      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER LBUF
C-----------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Calcule le MD5
      LBUF = LEN(STR)
      IERR = C_ST_MD5(LBUF,
     &                SO_ALLC_CST2B(STR),
     &                SELF%MD5)

      SO_HASH_INIC = ERR_TYP()
      RETURN
      END FUNCTION SO_HASH_INIC

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction SO_HASH_INI initialise l'objet à partir des données
C     de pointeur HNDL.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNDL        Pointeur sur les données
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_INIH(SELF, HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_INIH
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF
      INTEGER HNDL

      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(HNDL))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Calcule le MD5
      IERR = C_ST_MD5(SO_ALLC_REQLEN(HNDL),
     &                SO_ALLC_CST2B(HNDL,0_2),
     &                SELF%MD5)

      SO_HASH_INIH = ERR_TYP()
      RETURN
      END FUNCTION SO_HASH_INIH

C************************************************************************
C Sommaire: Initialise une fonction de DLL.
C
C Description:
C     La fonction SO_HASH_INI initialise l'objet à partir des données
C     de pointeur HNDL.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNDL        Pointeur sur les données
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_INIP(SELF, PTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_INIP
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_PTR_T

      CLASS(SO_HASH_T),     INTENT(INOUT) :: SELF
      CLASS(SO_ALLC_PTR_T), INTENT(IN)    :: PTR

      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PTR%ISVALID())
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Calcule le MD5
      IERR = C_ST_MD5(PTR%REQLENB(),
     &                PTR%CST2B(),
     &                SELF%MD5)

      SO_HASH_INIP = ERR_TYP()
      RETURN
      END FUNCTION SO_HASH_INIP

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SO_HASH_RST(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_RST
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(INOUT) :: SELF
C-----------------------------------------------------------------------

      SO_HASH_RST = SELF%RAZ()
      RETURN
      END FUNCTION SO_HASH_RST

C************************************************************************
C Sommaire:
C
C Description:
C     Surcharge de l'opérateur ==
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_OPEQ(LHS, RHS)

      CLASS(SO_HASH_T), INTENT(IN) ::  LHS
      CLASS(SO_HASH_T), INTENT(IN) ::  RHS
C---------------------------------------------------------------------
      SO_HASH_OPEQ = .FALSE.
      IF (LHS%MD5(1) .NE. RHS%MD5(1)) RETURN
      IF (LHS%MD5(2) .NE. RHS%MD5(2)) RETURN
      IF (LHS%MD5(3) .NE. RHS%MD5(3)) RETURN
      IF (LHS%MD5(4) .NE. RHS%MD5(4)) RETURN
      SO_HASH_OPEQ = .TRUE.
      RETURN
      END FUNCTION SO_HASH_OPEQ

C************************************************************************
C Sommaire:
C
C Description:
C     Surcharge de l'opérateur !=
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_OPNE(LHS, RHS)

      CLASS(SO_HASH_T), INTENT(IN) ::  LHS
      CLASS(SO_HASH_T), INTENT(IN) ::  RHS
C---------------------------------------------------------------------
      SO_HASH_OPNE = .NOT. (LHS .EQ. RHS)
      RETURN
      END FUNCTION SO_HASH_OPNE

C************************************************************************
C Sommaire: Test pour l'égalité
C
C Description:
C     La fonction SO_HASH_ESTEQB teste pour l'égalité entre l'objet et
C     le bugger BUF.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LBUF        Longueur du buffer
C     BUF         Buffer
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_ESTEQ(SELF, OTHR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_ESTEQ
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(IN) :: SELF
      CLASS(SO_HASH_T), INTENT(IN) :: OTHR
C-----------------------------------------------------------------------

      SO_HASH_ESTEQ = (SELF .EQ. OTHR)
      RETURN
      END  FUNCTION SO_HASH_ESTEQ

C************************************************************************
C Sommaire: Test pour l'égalité
C
C Description:
C     La fonction SO_HASH_ESTEQB teste pour l'égalité entre l'objet et
C     le bugger BUF.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     LBUF        Longueur du buffer
C     BUF         Buffer
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_ESTEQB(SELF, LBUF, BUF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_ESTEQB
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: LBUF
      BYTE,    INTENT(IN) :: BUF(:)

      INCLUDE 'err.fi'

      INTEGER IERR
      LOGICAL ESTEQ
      TYPE(SO_HASH_T) :: OTHR
C-----------------------------------------------------------------------

      ESTEQ = .FALSE.
      OTHR = SO_HASH_CTR_SELF ()
      IF (ERR_GOOD()) IERR = OTHR%INI(LBUF, BUF)
      IF (ERR_GOOD()) ESTEQ = (SELF .EQ. OTHR)

      SO_HASH_ESTEQB = ESTEQ
      RETURN
      END FUNCTION SO_HASH_ESTEQB

C************************************************************************
C Sommaire: Test pour l'égalité
C
C Description:
C     La fonction SO_HASH_ESTEQC teste pour l'égalité entre l'objet et
C     la chaîne STR.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     STR         Chaîne de caractères
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_ESTEQC(SELF, STR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_ESTEQC
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(IN) :: SELF
      CHARACTER*(*) STR

      INCLUDE 'err.fi'

      INTEGER IERR
      LOGICAL ESTEQ
      TYPE(SO_HASH_T) :: OTHR
C-----------------------------------------------------------------------

      ESTEQ = .FALSE.
      OTHR = SO_HASH_CTR_SELF ()
      IF (ERR_GOOD()) IERR = OTHR%INI(STR)
      IF (ERR_GOOD()) ESTEQ = (SELF .EQ. OTHR)

      SO_HASH_ESTEQC = ESTEQ
      RETURN
      END FUNCTION SO_HASH_ESTEQC

C************************************************************************
C Sommaire: Test pour l'égalité
C
C Description:
C     La fonction SO_HASH_ESTEQC teste pour l'égalité entre l'objet et
C     le pointeur HNDL.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNDL        Pointeur sur les données
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION SO_HASH_ESTEQH(SELF, HNDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SO_HASH_ESTEQH
CDEC$ ENDIF

      CLASS(SO_HASH_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: HNDL

      INCLUDE 'err.fi'

      INTEGER IERR
      LOGICAL ESTEQ
      TYPE(SO_HASH_T) :: OTHR
C-----------------------------------------------------------------------

      ESTEQ = .FALSE.
      OTHR = SO_HASH_CTR_SELF ()
      IF (ERR_GOOD()) IERR = OTHR%INI(HNDL)
      IF (ERR_GOOD()) ESTEQ = (SELF .EQ. OTHR)

      SO_HASH_ESTEQH = ESTEQ
      RETURN
      END FUNCTION SO_HASH_ESTEQH

      END MODULE SO_HASH_M

