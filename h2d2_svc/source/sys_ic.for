C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_SYS_REQMDL
C     INTEGER IC_SYS_OPBDOT
C     CHARACTER*(32) IC_SYS_REQNOM
C     INTEGER IC_SYS_REQHDL
C   Private:
C     INTEGER IC_SYS_XEQSYS
C     INTEGER IC_SYS_XEQDTE
C     INTEGER IC_SYS_XEQMAT
C     INTEGER IC_SYS_XEQMPI
C     INTEGER IC_SYS_XEQOMP
C     SUBROUTINE IC_SYS_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SYS_REQMDL(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SYS_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SYS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_SYS_REQHDL()
         WRITE(IPRM, '(2A,I12)') 'm', ',', HOBJ
      ENDIF

C  <comment>
C  <b>sys</b> returns the handle on the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SYS_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - IC_SYS_REQHDL()
      IF     (IOB .EQ. IC_SYS_MDL_SYS) THEN
C        <include>IC_SYS_XEQSYS@sys_ic.for</include>
         IERR = IC_SYS_XEQSYS(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_SYS_MDL_DTE) THEN
         IERR = IC_SYS_XEQDTE(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_SYS_MDL_MAT) THEN
         IERR = IC_SYS_XEQMAT(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_SYS_MDL_MPI) THEN
         IERR = IC_SYS_XEQMPI(HOBJ, IMTH, IPRM)
      ELSEIF (IOB .EQ. IC_SYS_MDL_OMP) THEN
         IERR = IC_SYS_XEQOMP(HOBJ, IMTH, IPRM)
      ELSE
         GOTO 9900
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HOBJ
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_SYS_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_XEQSYS(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

      INTEGER      IERR
      INTEGER      IVAL
      CHARACTER*64 MDUL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

C---     GET
      ELSEIF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, MDUL)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>The date module regroups functionalities related to date.</comment>
         IF (MDUL .EQ. 'date') THEN
            WRITE(IPRM, '(2A,I12)') 'm', ',', HOBJ+IC_SYS_MDL_DTE
C           <include>IC_SYS_XEQDTE@sys_ic.for</include>

C        <comment>The math module regroups mathematical functions and values.</comment>
         ELSEIF (MDUL .EQ. 'math') THEN
            WRITE(IPRM, '(2A,I12)') 'm', ',', HOBJ+IC_SYS_MDL_MAT
C           <include>IC_SYS_XEQMAT@sys_ic.for</include>

C        <comment>The mpi module regroups functionalities related to MPI.</comment>
         ELSEIF (MDUL .EQ. 'mpi') THEN
            WRITE(IPRM, '(2A,I12)') 'm', ',', HOBJ+IC_SYS_MDL_MPI
C           <include>IC_SYS_XEQMPI@sys_ic.for</include>

C        <comment>The omp module regroups functionalities related to OMP.</comment>
         ELSEIF (MDUL .EQ. 'omp') THEN
            WRITE(IPRM, '(2A,I12)') 'm', ',', HOBJ+IC_SYS_MDL_OMP
C           <include>IC_SYS_XEQOMP@sys_ic.for</include>
         ELSE
            GOTO 9902
         ENDIF

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_MODULE_INVALIDE', ': ',
     &                       MDUL(1:SP_STRN_LEN(MDUL))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_XEQSYS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_XEQDTE(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

      INTEGER      IERR, IRET
      INTEGER      IYE, IMO, IDY, IHR, IMN, ISC, IMS
      REAL*8       RVAL
      CHARACTER*64 PROP, SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_SYS_REQHDL() .EQ. IC_SYS_MDL_DTE)
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

C     <comment>
C     The function <b>utcdatetime</b> returns a double representing
C     the date-time since the epoch in UTC time. Date-time is expressed
C     as an integer sequence for year, month, day, hour, minute,
C     second and microsecond.
C     </comment>
      ELSEIF (IMTH .EQ. 'utcdatetime') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Year</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, IYE)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Month</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, IMO)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Day</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, IDY)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Hour</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, IHR)
         IF (IRET .NE. 0) IHR = 0
C        <comment>Minute</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 5, IMN)
         IF (IRET .NE. 0) IMN = 0
C        <comment>Second</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 6, ISC)
         IF (IRET .NE. 0) ISC = 0
C        <comment>Microsecond</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 7, IMS)
         IF (IRET .NE. 0) IMS = 0

C---        Convertis en double
         IRET = C_DH_C2DUTC(RVAL, IYE, IMO, IDY, IHR, IMN, ISC, IMS)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>
C     The function <b>datetime</b> returns a double representing
C     the date-time since the epoch in local time. Date-time is expressed
C     as an integer sequence for year, month, day, hour, minute,
C     second and microsecond.
C     </comment>
      ELSEIF (IMTH .EQ. 'datetime') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Year</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, IYE)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Month</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, IMO)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Day</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, IDY)
         IF (IRET .NE. 0) GOTO 9901
C        <comment>Hour</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, IHR)
         IF (IRET .NE. 0) IHR = 0
C        <comment>Minute</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 5, IMN)
         IF (IRET .NE. 0) IMN = 0
C        <comment>Second</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 6, ISC)
         IF (IRET .NE. 0) ISC = 0
C        <comment>Microsecond</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 7, IMS)
         IF (IRET .NE. 0) IMS = 0

C---        Convertis en double
         IRET = C_DH_C2DLCL(RVAL, IYE, IMO, IDY, IHR, IMN, ISC, IMS)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>The function <b>utcnow</b> return the current UTC time.</comment>
      ELSEIF (IMTH .EQ. 'utcnow') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

         IRET = C_DH_TIMUTC(RVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>The function <b>now</b> return the current local time.</comment>
      ELSEIF (IMTH .EQ. 'now') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

         IRET = C_DH_TIMLCL(RVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>
C     The function <b>utcisoformat</b> format the date-time argument
C     according to the ISO format YYYY-MM-DD HH:MM:SS.ssssss.
C     </comment>
      ELSEIF (IMTH .EQ. 'utcisoformat') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Date-time as double</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, RVAL)
         IF (IERR .NE. 0) GOTO 9901

         IRET = C_DH_ECRUTC(RVAL, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))

C     <comment>
C     The function <b>isoformat</b> format the date-time argument
C     according to the ISO format YYYY-MM-DD HH:MM:SS.ssssss.
C     </comment>
      ELSEIF (IMTH .EQ. 'isoformat') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Date-time as double</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, RVAL)
         IF (IERR .NE. 0) GOTO 9901

         IRET = C_DH_ECRLCL(RVAL, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))

C     <comment>
C     The function <b>utcstrptime</b> read an iso-formatted string as
C     a UTC time.
C     </comment>
      ELSEIF (IMTH .EQ. 'utcstrptime') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Date-time as string</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901

         IRET = C_DH_LISUTC(RVAL, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>
C     The function <b>strptime</b> read an iso-formatted string as
C     a local time.
C     </comment>
      ELSEIF (IMTH .EQ. 'strptime') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Date-time as string</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SVAL)
         IF (IERR .NE. 0) GOTO 9901

         IRET = C_DH_LISLCL(RVAL, SVAL)
         IF (IRET .NE. 0) GOTO 9904
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_DATE_INVALIDE', ': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_XEQDTE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_XEQMAT(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

      REAL*8       ALFA, X, X1, X2, Y, DX, DY, PI
      INTEGER      IERR
      INTEGER      IVAL
      CHARACTER*64 PROP, SVAL
      PARAMETER (PI = 3.1415926535897932384626433D0)
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_SYS_REQHDL() .EQ. IC_SYS_MDL_MAT)
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

C---     Constantes
C==================
      ELSEIF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>pi</comment>
         IF (PROP .EQ. 'pi') THEN
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', PI
         ELSE
            GOTO 9902
         ENDIF

C---     Trigo
C=============
!!!!!!!  acos asin atan atan2 cos cosh sin sinh tan tanh
C     <comment>The function <b>acos</b> returns the arc cosine in radian of the argument.</comment>
      ELSEIF (IMTH .EQ. 'acos') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', ACOS(X)

C     <comment>The function <b>asin</b> return the arc sine in radian of the argument.</comment>
      ELSEIF (IMTH .EQ. 'asin') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', ASIN(X)

C     <comment>The function <b>atan</b> return the arc tangent in radian of the argument.</comment>
      ELSEIF (IMTH .EQ. 'atan') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', ATAN(X)

C     <comment>The function <b>atan2</b> return the arc tangent in radian of dy/dx.</comment>
      ELSEIF (IMTH .EQ. 'atan2') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>dy</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, DY)
C        <comment>dx</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, DX)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', ATAN2(DY, DX)

C     <comment>The function <b>cos</b> return the cosine of the angle in radian.</comment>
      ELSEIF (IMTH .EQ. 'cos') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Angle in radian</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, ALFA)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', COS(ALFA)

C     <comment>The function <b>cosh</b> return the hyperbolic cosine of the argument.</comment>
      ELSEIF (IMTH .EQ. 'cosh') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', COSH(X)

C     <comment>The function <b>sin</b> return the sine of the angle in radian.</comment>
      ELSEIF (IMTH .EQ. 'sin') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Angle in radian</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, ALFA)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', SIN(ALFA)

C     <comment>The function <b>sinh</b> return the hyperbolic sine of the argument.</comment>
      ELSEIF (IMTH .EQ. 'sinh') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', SINH(X)

C     <comment>The function <b>tan</b> return the tangent of the angle in radian.</comment>
      ELSEIF (IMTH .EQ. 'tan') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Angle in radian</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, ALFA)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', TAN(ALFA)

C     <comment>The function <b>tanh</b> return the hyperbolic tangent of the argument.</comment>
      ELSEIF (IMTH .EQ. 'tanh') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', TANH(X)

C---     Math functions
C=============
!!! abs exp log max min mod modulo nint sqrt random sign
C     <comment>The function <b>abs</b> return the absolute value of the argument.</comment>
      ELSEIF (IMTH .EQ. 'abs') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', ABS(X)

C     <comment>The function <b>exp</b> return e raised to the power of the argument.</comment>
      ELSEIF (IMTH .EQ. 'exp') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', EXP(X)

C     <comment>The function <b>log</b> return the natural logarithm of the argument.</comment>
      ELSEIF (IMTH .EQ. 'log') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', LOG(X)

C     <comment>The function <b>max</b> return the largest argument.</comment>
      ELSEIF (IMTH .EQ. 'max') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument 1</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X1)
C        <comment>Argument 2</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, X2)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', MAX(X1, X2)

C     <comment>The function <b>min</b> return the smallest argument.</comment>
      ELSEIF (IMTH .EQ. 'min') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument 1</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X1)
C        <comment>Argument 2</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, X2)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', MIN(X1, X2)

C     <comment>The function <b>pow</b> return x to the power of y (x**y).</comment>
      ELSEIF (IMTH .EQ. 'pow') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, Y)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', (X**Y)

C     <comment>The function <b>sqrt</b> return the square root of the argument.</comment>
      ELSEIF (IMTH .EQ. 'sqrt') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .NE. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', SQRT(X)

C     <comment>The function <b>sign</b> return the sign of the argument.</comment>
      ELSEIF (IMTH .EQ. 'sign') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Argument</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, X)
         IF (IERR .NE. 0) GOTO 9901
         WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', SIGN(X, 1.0D0)

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_XEQMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_XEQMPI(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

      INTEGER      IERR, IRET
      INTEGER      I_ERROR, I_COMM
      INTEGER      IVAL
      CHARACTER*64 PROP, SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_SYS_REQHDL() .EQ. IC_SYS_MDL_MPI)
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

C---     GET
      ELSEIF (IMTH .EQ. '##property_get##') THEN
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>MPI size.</comment>
         IF (PROP .EQ. 'size') THEN
            I_COMM = MP_UTIL_REQCOMM()
            CALL MPI_COMM_SIZE(I_COMM, IVAL, I_ERROR)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>MPI rank.</comment>
         ELSEIF (PROP .EQ. 'rank') THEN
            I_COMM = MP_UTIL_REQCOMM()
            CALL MPI_COMM_RANK(I_COMM, IVAL, I_ERROR)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>MPI host.</comment>
         ELSEIF (PROP .EQ. 'host') THEN
            IRET = C_OS_HOSTNAME(SVAL)
            WRITE(IPRM, '(3A)') 'S', ',', SVAL(1:SP_STRN_LEN(SVAL))
         ELSE
            GOTO 9902
         ENDIF

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_XEQMPI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SYS_XEQOMP(HOBJ, IMTH, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'sys_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sys_ic.fc'

!$    INTEGER OMP_GET_MAX_THREADS
!$    INTEGER OMP_GET_NUM_THREADS

      INTEGER      IERR
      INTEGER      IVAL
      INTEGER      NUM_THREADS
      CHARACTER*64 PROP, SVAL
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_SYS_REQHDL())
D     CALL ERR_ASR(HOBJ-IC_SYS_REQHDL() .EQ. IC_SYS_MDL_OMP)
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>help</b> displays the help content for the module.</comment>
      IF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_SYS_AID()

C     <comment>
C     Return the maximum number of threads used for parallel regions.
C     The maximum number of threads can be set either with the environment
C     variable OMP_NUM_THREADS, or with a call to set_num_thread()
C     </comment>
      ELSEIF (IMTH .EQ. 'get_max_threads') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         NUM_THREADS = -1
!$       NUM_THREADS = OMP_GET_MAX_THREADS()
         WRITE(IPRM, '(2A,I12)') 'I', ',', NUM_THREADS

C     <comment>
C     Returns the number of threads in the current team.
C     </comment>
      ELSEIF (IMTH .EQ. 'get_num_threads') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         NUM_THREADS = -1
!$       NUM_THREADS = OMP_GET_NUM_THREADS()
         WRITE(IPRM, '(2A,I12)') 'I', ',', NUM_THREADS
         
!!!C     <comment>Return the maximum number of threads of the program.</comment>
!!!      ELSEIF (IMTH .EQ. 'get_thread_limit') THEN
!!!         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
!!!         NUM_THREADS = -1
!!!!$       NUM_THREADS = OMP_GET_THREAD_LIMIT()
!!!         WRITE(IPRM, '(2A,I12)') 'I', ',', NUM_THREADS
         
C     <comment>
C     Specifies the number of threads used by default in subsequent
C     parallel sections.
C     </comment>
      ELSEIF (IMTH .EQ. 'set_num_threads') THEN
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900
         IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
!$       CALL OMP_SET_NUM_THREADS(IVAL)
         
      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SYS_AID()

9999  CONTINUE
      IC_SYS_XEQOMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SYS_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SYS_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sys_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>sys</b> regroups system functionalities, not directly
C  related to the finite element method.
C</comment>
      IC_SYS_REQNOM = 'sys'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SYS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SYS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sys_ic.fi'
C-------------------------------------------------------------------------

      IC_SYS_REQHDL = 1999003000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SYS_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sys_ic.hlp')

      RETURN
      END
