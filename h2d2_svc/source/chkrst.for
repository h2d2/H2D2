C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PKL_DOCHCKP démarre la tâche d'attente avec le pas de
C     temps DELT. À tous les DELT, la tâche va incrémenter le compteur.
C     <p>
C
C Entrée:
C     DELT        Écart de temps entre 2 points de sauvegarde
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PKL_INICHCKP(DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PKL_INICHCKP
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 DELT

      INCLUDE 'chkrst.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'c_sp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'chkrst.fc'

      INTEGER IERR, IRET
      EXTERNAL PKL_TSK_MAIN
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Contrôles
      IF (DELT .LE. 0.0D0) GOTO 9900

C---     Reset
      IERR = PKL_RSTCHCKP()

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         PKL_DELT = DELT
         IRET = C_DH_TMRSTART(PKL_TLAST)
         IF (IRET .NE. 0) GOTO 9901
      ENDIF

C---     Démarre la tâche
      IF (ERR_GOOD()) IRET = C_SP_START(PKL_TASK, PKL_TSK_MAIN)
      IF (IRET .NE. 0) GOTO 9901

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_TEMPS_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_INIT_CHECKPOINT_RESTART'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PKL_INICHCKP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PKL_RSTCHCKP stoppe la tâche d'attente et réinitialise
C     les attributs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PKL_RSTCHCKP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PKL_RSTCHCKP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'chkrst.fi'
      INCLUDE 'c_sp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'chkrst.fc'

      INTEGER IRET
C-----------------------------------------------------------------------

      IRET = 0

C---     Stoppe la tâche
      IF (PKL_TASK .NE. 0) IRET = C_SP_KILL(PKL_TASK)
      IF (IRET .NE. 0) GOTO 9900

C---     Reset les attribus
      PKL_DELT  = -1
      PKL_TLAST = 0

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RESET_CHECKPOINT_RESTART'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PKL_RSTCHCKP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fait un checkpoint.
C
C Description:
C     La fonction PKL_DOCHCKP provoque un checkpoint si le compteur a été
C     incrémenté.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PKL_DOCHCKP()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PKL_DOCHCKP
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'chkrst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'chkrst.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (PKL_ITRG .LT. PKL_NTRG) THEN
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,*) 'PKL_DOCHCKP: init checkpoint', PKL_NTRG
         CALL LOG_ECRIS(LOG_BUF)
         IERR = PKL_PKLALL()
         PKL_ITRG = PKL_NTRG
      ENDIF

      PKL_DOCHCKP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fait un restart.
C
C Description:
C     La fonction PKL_DORSRT.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PKL_DORSTRT(NOMREP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PKL_DORSTRT
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOMREP

      INCLUDE 'chkrst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'chkrst.fc'

      INTEGER IERR
C-----------------------------------------------------------------------

      WRITE(LOG_BUF,*) 'PKL_DORSTRT: init restart from ',
     &                 NOMREP(1:SP_STRN_LEN(NOMREP))
      CALL LOG_ECRIS(LOG_BUF)
      IERR = OB_OBJC_UPKALL(NOMREP(1:SP_STRN_LEN(NOMREP)))

      PKL_DORSTRT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PKL_TSK_MAIN est la fonction de la tâche d'attente.
C     La fonction boucle sans fin, et tous les PKL_DELT incrémente
C     le compteur PKL_NTRG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PKL_TSK_MAIN()

      IMPLICIT NONE

      INCLUDE 'chkrst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'chkrst.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PKL_DELT .GT. 0.0D0)
C-----------------------------------------------------------------------

100   CONTINUE
         CALL SLEEP( NINT(PKL_DELT) )
         PKL_NTRG = PKL_NTRG + 1
!!!         WRITE(LOG_BUF,*) 'PKL_TSK_MAIN: wakeup', PKL_NTRG
!!!         CALL LOG_ECRIS(LOG_BUF)
      GOTO 100

      PKL_TSK_MAIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée PKL_PKLALL . Elle crée le répertoire et
C     appelle OB_OBJC_PKLALL pour le marshalling des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  le rep doit être unique, sur un drive partagé ou sur un drive local.
C  doit avoir le même nom unique pour tous les process
C  Il faut le synchroniser
C  export H2D2_CHKRST sinon TEMP/h2d2_chkrst_###id###
C   id unique basé sur date? pid (non)?
C************************************************************************
      FUNCTION PKL_PKLALL()

      IMPLICIT NONE

      INCLUDE 'chkrst.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'chkrst.fc'

      INTEGER IERR, IRET
      REAL*8  DNOW
      CHARACTER*(256) REPEXE
      CHARACTER*(256) NOMREP
      CHARACTER*(256) NOMTRV
C------------------------------------------------------------------------

      IRET = 0

C---     Récupère le répertoire de l'exe
      IF (IRET .EQ. 0) IRET = C_OS_REPEXE(REPEXE)

C---     La date forme le nom du nouveau répertoire
      IF (IRET .EQ. 0) IRET = C_DH_TIMLCL(DNOW)
      IF (IRET .EQ. 0) IRET = C_DH_ECRLCL(DNOW, NOMTRV)
      IF (IRET .EQ. 0) CALL SP_STRN_DLC(NOMTRV, '\\')
      IF (IRET .EQ. 0) CALL SP_STRN_DLC(NOMTRV, '/')
      IF (IRET .EQ. 0) CALL SP_STRN_DLC(NOMTRV, '-')
      IF (IRET .EQ. 0) CALL SP_STRN_DLC(NOMTRV, ':')
      IF (IRET .EQ. 0) CALL SP_STRN_DLC(NOMTRV, ' ')

C---     Crée le répertoire
      IF (IRET .EQ. 0)
     &   NOMREP = REPEXE(1:SP_STRN_LEN(REPEXE)) //
     &            '/../restart/' //
     &            NOMTRV(1:SP_STRN_LEN(NOMTRV))
      IF (IRET .EQ. 0) IRET = C_OS_NORMPATH(NOMREP)
      IF (IRET .EQ. 0) IRET = C_OS_MKDIR(NOMREP)

C---     Appel OB_OBJC pour le marshalling
      IF (IRET .EQ. 0) THEN
         IERR = OB_OBJC_PKLALL(NOMREP)
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_MAKE_DIR')
         CALL ERR_AJT(NOMREP(1:SP_STRN_LEN(NOMREP)))
      ENDIF

C--      Synchronise les erreurs
      IERR = MP_UTIL_SYNCERR()

      PKL_PKLALL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise le COMMON
C
C Description:
C     Le block data PKL initialise le COMMON propre au module de chkrst.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA PKL

      IMPLICIT NONE

      INCLUDE 'chkrst.fc'

      DATA PKL_DELT     /3600.0D0/
      DATA PKL_TLAST    /0.0D0/
      DATA PKL_TASK     /0/

      END
