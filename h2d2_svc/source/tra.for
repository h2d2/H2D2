C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Assigne le nom de l'algorithme.
C
C Description:
C     La fonction <code>TRA_ASGALGO(...)</code> assigne le nom de
C     l'algorithme. Seuls les 4 premiers caractère seront retenus.
C
C Entrée:
C     ALGO        Le nom de l'algo
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGALGO(ALGO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGALGO
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ALGO

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'

      INTEGER L
C-----------------------------------------------------------------------

      L = LEN(ALGO)
      IF (L .GE. 1) THEN
         TRA_ALGO = ALGO(1: MIN(4,L))
      ELSE
         TRA_ALGO = '----'
      ENDIF

      TRA_ASGALGO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne la valeur du critère d'arrêt.
C
C Description:
C     La fonction <code>TRA_ASGCRIA(...)</code> assigne la valeur du
C     critère d'arrêt.
C
C Entrée:
C     CRIA           L'indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGCRIA(CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGCRIA
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8 CRIA

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_CRIA = CRIA
      TRA_ASGCRIA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le fichier de trace.
C
C Description:
C     La fonction TRA_ASGFIC assigne un nouveau fichier au système de trace.
C     Le nom spécial 'STDOUT' reconnecte à l'unité 6.
C
C Entrée:
C     CHARACTER*(*) FTRA     Nom du fichier de trace
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGFIC(FTRA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGFIC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FTRA

      INCLUDE 'tra.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tra.fc'

      INTEGER     I_RANK, I_ERROR
      INTEGER     I_MASTER
      LOGICAL     I_INIT
      PARAMETER (I_MASTER = 0)

      INTEGER IRET
      INTEGER MP
      INTEGER LFTRA
C-----------------------------------------------------------------------

      LFTRA = SP_STRN_LEN(FTRA)
      I_RANK = I_MASTER

C---     PARAM MPI
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (I_INIT) CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(),I_RANK,I_ERROR)

C---     DETERMINE SI ON DOIT OUVRIR ET QUEL EST LE NOM
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (LFTRA .EQ. 6 .AND. FTRA(1:LFTRA) .EQ. 'STDOUT') THEN
            MP = 6
         ELSE
            MP = IO_UTIL_FREEUNIT()
         ENDIF
      ELSE
         MP = -1
      ENDIF

C---     FERME L'ANCIEN FICHIER
      IF (TRA_MP .NE. 6 .AND. TRA_MP .GE. 0) THEN
         CLOSE(TRA_MP)
         TRA_MP = -1
      ENDIF

C---     OUVRE LE NOUVEAU FICHIER
      IF (MP .NE. 6 .AND. MP .GE. 0) THEN
         OPEN(UNIT  = MP,
     &        FILE  = FTRA(1:SP_STRN_LEN(FTRA)),
     &        FORM  = 'FORMATTED',
     &        STATUS= 'UNKNOWN',
     &        ERR   = 9900)
      ENDIF

C---     ASSIGNE L'UNITÉ
      TRA_MP = MP

C---     DEMARRE LE CHRONO
      IRET = C_DH_TMRSTART(TRA_TMR)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(FTRA(1:SP_STRN_LEN(FTRA)))
      GOTO 9999

9999  CONTINUE
      TRA_ASGFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne l'indice du noeud de la stratégie de résolution.
C
C Description:
C     La fonction <code>TRA_ASGINOD(...)</code> assigne l'indice du
C     noeud de la stratégie de résolution.
C
C Entrée:
C     INOD           L'indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGINOD(INOD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGINOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER INOD

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_INOD = INOD
      TRA_ASGINOD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne l'indice de la séquence
C
C Description:
C     La fonction <code>TRA_ASGISEQ(...)</code> assigne l'indice de la
C     séquence.
C
C Entrée:
C     ISEQ           L'indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGISEQ(ISEQ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGISEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ISEQ

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_ISEQ = ISEQ
      TRA_ASGISEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne l'itération.
C
C Description:
C     La fonction <code>TRA_ASGITER(...)</code> assigne le numéro de
C     l'itération.
C
C Entrée:
C     ITER        L'indice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGITER(ITER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGITER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ITER

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_ITER = ITER
      TRA_ASGITER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le temps de simulation.
C
C Description:
C     La fonction <code>TRA_ASGTSIM(...)</code> assigne le temps de la
C     simulation.
C
C Entrée:
C     TSIM     Le temps
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGTSIM(TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGTSIM
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  TSIM

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_TSIM = TSIM
      TRA_ASGTSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le temps de l'algorithme.
C
C Description:
C     La fonction <code>TRA_ASGTALG(...)</code> assigne le temps de
C     l'algorithme.
C
C Entrée:
C     TSIM     Le temps
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ASGTALG(TALG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ASGTALG
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  TALG

      INCLUDE 'tra.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tra.fc'
C-----------------------------------------------------------------------

      TRA_TALG = TALG
      TRA_ASGTALG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris le message
C
C Description:
C     La fonction TRA_ECRIS écris le message passé en argument dans
C     le fichier de trace.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TRA_ECRIS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TRA_ECRIS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tra.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tra.fc'

      REAL*8  DELT
      INTEGER IRET
      CHARACTER*(64) BUF
C-----------------------------------------------------------------------

      IF (TRA_MP .LE. 0) GOTO 9999

C---     Le temps écoulé
      IRET = C_DH_TMRDELT(DELT, TRA_TMR)
      IRET = C_DH_TMRSTR (DELT, BUF)

      WRITE(TRA_MP, '(A,I12,I12,I12,I12,A12,I12,1X,1PE14.6E3)')
     &      BUF(1:SP_STRN_LEN(BUF)),
     &      NINT(TRA_TSIM),
     &      NINT(TRA_TALG),
     &      TRA_ISEQ,
     &      TRA_INOD,
     &      TRA_ALGO,
     &      TRA_ITER,
     &      TRA_CRIA

9999  CONTINUE
      TRA_ECRIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise le COMMON
C
C Description:
C     Le block data TRA initialise le COMMON propre au module de trace.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA TRA

      IMPLICIT NONE

      INCLUDE 'tra.fi'
      INCLUDE 'tra.fc'

      DATA TRA_MP   /-1/
      DATA TRA_CRIA /0.0D0/
      DATA TRA_TSIM /0.0D0/
      DATA TRA_TALG /0.0D0/
      DATA TRA_ISEQ /0/
      DATA TRA_INOD /0/
      DATA TRA_ITER /0/
      DATA TRA_ALGO /'----'/

      END

