C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DS_RNGE_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_RNGE_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dsrnge_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER I, IP, I1, I2, I3
      CHARACTER*(16) BUF
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DS_RNGE_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IP = 0
      I3 = 1
C     <comment>range(e): Interval [1,e] with step 1</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, I1)
      IF (IERR .EQ. 0) IP = IP + 1
C     <comment>range(b,e): Interval [b,e] with step 1</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, I2)
      IF (IERR .EQ. 0) IP = IP + 1
C     <comment>range(b,e,s): Interval [b,e] with step s</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, I3)
      IF (IERR .EQ. 0) IP = IP + 1

C---     VALIDE
      IF (IP .EQ. 0) GOTO 9901
      IF (I2 .LT. 1) GOTO 9901
      IF (I3 .EQ. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET
      IERR = DS_LIST_CTR(HOBJ)
      IERR = DS_LIST_INI(HOBJ)

C---     Remplis
      IF (IP .EQ. 1) THEN
         I2 = I1
         I1 = 1
      ENDIF
      DO I=I1,I2,I3
         WRITE(BUF, '(I12)') I
         IERR = DS_LIST_AJTVAL(HOBJ, BUF(1:12))
      ENDDO

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the range</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C    The constructor <b>range</b> constructs an object, with the given arguments,
C    and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DS_RNGE_AID()

9999  CONTINUE
      IC_DS_RNGE_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DS_RNGE_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_RNGE_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dsrnge_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dslsti.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HLST
      INTEGER IVAL
      CHARACTER*64 SVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     DISPATCH LES MÉTHODES, PROPRIÉTÉS ET OPERATEURS
C     <comment>The method <b>iter</b> returns an iterator on the list.</comment>
      IF (IMTH .EQ. 'iter') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

         IERR = DS_LSTI_CTR(HLST)
         IERR = DS_LSTI_INI(HLST, HOBJ)
         IF (IERR .NE. 0) GOTO 9902
         WRITE(IPRM, '(I12)') 'H', ',', HLST

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DS_LIST_DTR(HOBJ)
         IF (IERR .NE. 0) GOTO 9902

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(DS_LIST_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LOG_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test DS_LIST_MAP___(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DS_RNGE_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_DESTRUCTION_OBJET'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DS_RNGE_AID()

9999  CONTINUE
      IC_DS_RNGE_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DS_RNGE_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_RNGE_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dsrnge_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C    The class <b>range</b> generates a list containing an arithmetic progression,
C    which can be used for iteration. (Status: prototype)
C</comment>
      IC_DS_RNGE_REQCLS = 'range'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Base de handle particulière, car n'a pas de classe associée
C************************************************************************
      FUNCTION IC_DS_RNGE_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DS_RNGE_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dsrnge_ic.fi'
C-------------------------------------------------------------------------

      IC_DS_RNGE_REQHDL = 1999006000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DS_RNGE_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dsrnge_ic.hlp')

      RETURN
      END
