C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Description:
C     La classe TR_CDWN est un minuteur (Count Down timer).
C     TR_CDWN_START le démarre avec  un temps à écouler.
C     TR_CDWN_ESTFINI retourne .TRUE. lorsque ce temps est écoulé.
C
C Functions:
C   Public:
C     INTEGER TR_CDWN_000
C     INTEGER TR_CDWN_999
C     INTEGER TR_CDWN_CTR
C     INTEGER TR_CDWN_DTR
C     INTEGER TR_CDWN_INI
C     INTEGER TR_CDWN_RST
C     LOGICAL TR_CDWN_HVALIDE
C     INTEGER TR_CDWN_START
C     INTEGER TR_CDWN_STOP
C     LOGICAL TR_CDWN_ESTFINI
C     REAL*8 TR_CDWN_REQRESTE
C   Private:
C     SUBROUTINE TR_CDWN_REQSELF
C
C************************************************************************

      MODULE TR_CDWN_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: TR_CDWN_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: TR_CDWN_T
         REAL*8 :: TDEL
         REAL*8 :: TINI
      END TYPE TR_CDWN_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée TR_CDWN_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (TR_CDWN_T), POINTER :: TR_CDWN_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (TR_CDWN_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      TR_CDWN_REQSELF => SELF
      RETURN
      END FUNCTION TR_CDWN_REQSELF

      END MODULE TR_CDWN_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_000
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INCLUDE 'trcdwn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(TR_CDWN_HBASE, 'Count down timer')

      TR_CDWN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_999
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL TR_CDWN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(TR_CDWN_HBASE, TR_CDWN_DTR)

      TR_CDWN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_CTR
CDEC$ ENDIF

      USE TR_CDWN_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER, INTENT(OUT):: HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR, IRET
      TYPE (TR_CDWN_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   TR_CDWN_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
D     CALL ERR_ASR(ERR_BAD() .OR. TR_CDWN_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      TR_CDWN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_DTR
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (TR_CDWN_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = TR_CDWN_RST(HOBJ)

C---     Désalloue la mémoire
      SELF => TR_CDWN_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dés-enregistre
      IERR = OB_OBJN_DTR(HOBJ, TR_CDWN_HBASE)

      TR_CDWN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode virtuelle TR_CDWN_INI(...)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_INI
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     SELF doit exister
      SELF => TR_CDWN_REQSELF(HOBJ)

C---     Initialise les attributs
      IF (ERR_GOOD()) THEN
         SELF%TDEL = 0.0D0
         SELF%TINI = 0.0D0
      ENDIF

      TR_CDWN_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode virtuelle TR_CDWN_RST(...)
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_RST
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Les attributs
      SELF => TR_CDWN_REQSELF(HOBJ)

C---     Reset le temps
      SELF%TDEL = 0.0D0
      SELF%TINI = 0.0D0

      TR_CDWN_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction TR_CDWN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_REQHBASE
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INCLUDE 'trcdwn.fi'
C------------------------------------------------------------------------

      TR_CDWN_REQHBASE = TR_CDWN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction TR_CDWN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_HVALIDE
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      TR_CDWN_HVALIDE = OB_OBJN_HVALIDE(HOBJ, TR_CDWN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_TR_CDWN_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_PRN(HOBJ)

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      CHARACTER*(256) NOM
      TYPE (TR_CDWN_T), POINTER :: SELF
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     En-tête
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_MINUTEUR'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres de l'objet
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      SELF => TR_CDWN_REQSELF(HOBJ)
      WRITE (LOG_BUF,'(2A,F20.6)')
     &      'MSG_TEMPS_RESTANT#<35>#', '= ', TR_CDWN_REQRESTE(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      TR_CDWN_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_START(...) démarre le timer avec le temps TDEL
C     à écouler.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_SET(HOBJ, TDEL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_SET
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER,INTENT(IN) :: HOBJ
      REAL*8, INTENT(IN) :: TDEL

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'

      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      SELF%TDEL = TDEL
      SELF%TINI = 0.0D0

      TR_CDWN_SET = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_START(...) démarre le timer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_START(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_START
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER,INTENT(IN) :: HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'

      REAL*8  TNOW
      INTEGER IRET
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      IF (SELF%TDEL .GT. 0.0D0 .AND. SELF%TINI .LE. 0.0D0) THEN
         IRET = C_DH_TMRSTART(TNOW)
         SELF%TINI = TNOW
      ENDIF

      TR_CDWN_START = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_STOP(...) stoppe le timer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_STOP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_STOP
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      IF (SELF%TINI .GT. 0.0D0) THEN
         SELF%TDEL = TR_CDWN_REQRESTE(HOBJ)
      ENDIF
      SELF%TINI = 0.0D0

      TR_CDWN_STOP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_RESET(...) reset le timer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_RESET(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_RESET
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      SELF%TDEL = 0.0D0
      SELF%TINI = 0.0D0

      TR_CDWN_RESET = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_ESTFINI(...) retourne .TRUE. si le temps est
C     écoulé.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_ESTFINI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_ESTFINI
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'

      REAL*8 TDEL
      INTEGER IRET
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      IRET = C_DH_TMRDELT(TDEL, SELF%TINI)

      TR_CDWN_ESTFINI = TDEL .GE. SELF%TDEL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La  méthode TR_CDWN_REQRESTE(...) retourne le temps qu'il reste à
C     écouler.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TR_CDWN_REQRESTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TR_CDWN_AJTMTH
CDEC$ ENDIF

      USE TR_CDWN_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'trcdwn.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'

      REAL*8  TDEL
      INTEGER IRET
      TYPE (TR_CDWN_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TR_CDWN_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => TR_CDWN_REQSELF(HOBJ)
      IRET = C_DH_TMRDELT(TDEL, SELF%TINI)

      TR_CDWN_REQRESTE = - MIN(TDEL-SELF%TDEL, 0.0D0)
      RETURN
      END
