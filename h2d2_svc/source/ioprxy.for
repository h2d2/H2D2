C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à IO_PRXY_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA IO_PRXY_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'ioprxy.fc'

      DATA IO_PRXY_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(IO_PRXY_NOBJMAX,
     &                   IO_PRXY_HBASE,
     &                   'Proxy: Input-Output')

      IO_PRXY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER  IERR
      EXTERNAL IO_PRXY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(IO_PRXY_NOBJMAX,
     &                   IO_PRXY_HBASE,
     &                   IO_PRXY_DTR)

      IO_PRXY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_PRXY_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   IO_PRXY_NOBJMAX,
     &                   IO_PRXY_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(IO_PRXY_HVALIDE(HOBJ))
         IOB = HOBJ - IO_PRXY_HBASE

         IO_PRXY_HMNG(IOB) = 0
         IO_PRXY_HMDL(IOB) = 0
      ENDIF

      IO_PRXY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_PRXY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = IO_PRXY_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   IO_PRXY_NOBJMAX,
     &                   IO_PRXY_HBASE)

      IO_PRXY_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_PRXY_INI(HOBJ, HMNG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_INI
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMNG

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFNC
      INTEGER HMDL
      LOGICAL HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
D     CALL ERR_PRE(HMNG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = IO_PRXY_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - IO_PRXY_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HMNG, HMDL)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HVALIDE = .FALSE.
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HMNG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HMNG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IO_PRXY_HMNG(IOB) = HMNG
         IO_PRXY_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HMNG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IO_PRXY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IO_PRXY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RST
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFNC
      INTEGER HMDL
      INTEGER HMNG
      LOGICAL HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - IO_PRXY_HBASE
      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HVALIDE = .FALSE.
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HMNG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC, SO_ALLC_CST2B(HMNG))
      IF (HFNC .NE. 0)IERR = SO_FUNC_DTR(HFNC)

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      IO_PRXY_HMNG(IOB) = 0
      IO_PRXY_HMDL(IOB) = 0

      IO_PRXY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction IO_PRXY_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ioprxy.fi'
      INCLUDE 'ioprxy.fc'
C------------------------------------------------------------------------

      IO_PRXY_REQHBASE = IO_PRXY_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction IO_PRXY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ioprxy.fc'
C------------------------------------------------------------------------

      IO_PRXY_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  IO_PRXY_NOBJMAX,
     &                                  IO_PRXY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_OPEN
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_OPEN(HOBJ, FNAME, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_OPEN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('IO_PRXY_OPEN: : FTN string in call-back')

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'OPEN', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_OPEN: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL3(HFNC,
!!     &                                     HMNG,
!!     &                                     FNAME,
!!     &                                     ISTAT)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_CLOSE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_CLOSE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_CLOSE
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'CLOSE', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HMNG))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_ENDL
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_ENDL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_ENDL
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'ENDL', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HMNG))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_ENDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_WS
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_WS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_WS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('IO_PRXY_RS: FTN string in call-back')
      CALL ERR_ASR(.FALSE.)

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'WS', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_WS: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC, HMNG, S)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_WS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_WI_1
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_WI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_WI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
      INTEGER*8 K2B
      INTEGER J
      K2B(J) = SO_ALLC_CSTK2B(BA, J)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'WI_1', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     BA(K2B(HMNG)),
     &                                     BA(K2B(I)))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_WI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_WI_N
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_WI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_WI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
      INTEGER*8 K2B
      INTEGER J
      K2B(J) = SO_ALLC_CSTK2B(BA, J)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'WI_N', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC,
     &                                     BA(K2B(HMNG)),
     &                                     BA(K2B(N)),
     &                                     BA(K2B(KX(1))),
     &                                     BA(K2B(IX)))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_WI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_WD_1
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_WD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_WD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'WD_1', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_WD_1: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC, HMNG, V)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_WD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_WD_N
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_WD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_WD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'WD_N', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_WD_N: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC, HMNG, N, VX, IX)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_WD_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_RS
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_RS(HOBJ, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) S

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('IO_PRXY_RS: FTN string in call-back')
      CALL ERR_ASR(.FALSE.)

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'RS', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_RS: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC, HMNG, S)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_RS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_RI_1
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_RI_1(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RI_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
      INTEGER*8 K2B
      INTEGER J
      K2B(J) = SO_ALLC_CSTK2B(BA, J)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'RI_1', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     BA(K2B(HMNG)),
     &                                     BA(K2B(I)))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_RI_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_RI_N
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_RI_N(HOBJ, N, KX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RI_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
      INTEGER*8 K2B
      INTEGER J
      K2B(J) = SO_ALLC_CSTK2B(BA, J)
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'RI_N', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC,
     &                                     BA(K2B(HMNG)),
     &                                     BA(K2B(N)),
     &                                     BA(K2B(KX(1))),
     &                                     BA(K2B(IX)))

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_RI_N = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_RD_1
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_RD_1(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RD_1
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'RD_1', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_RD_1: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC, HMNG, V)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_RD_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IO_PRXY_RD_N
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IO_PRXY_RD_N(HOBJ, N, VX, IX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IO_PRXY_RD_N
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  VX(*)
      INTEGER IX

      INCLUDE 'ioprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'ioprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMNG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IO_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - IO_PRXY_HBASE

      HMNG = IO_PRXY_HMNG(IOB)
      HMDL = IO_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'RD_N', HFNC)

C---     FAIT L'APPEL
      CALL LOG_TODO('IO_PRXY_RD_N: a revoir')
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC, HMNG, N, VX, IX)

C---     LIBERE LA FONCTION
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      IO_PRXY_RD_N = ERR_TYP()
      RETURN
      END
