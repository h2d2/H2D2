C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE DBG_DMP_DTBL
C     SUBROUTINE DBG_DMP_DTBL2
C     SUBROUTINE DBG_DMP_ITBL
C     SUBROUTINE DBG_DMP_ITBL2
C     INTEGER DBG_OUVRE
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Ouvre un fichier de dump.
C
C Description:
C     La fonction privée DBG_OUVRE_FIC(MP) .
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DBG_OUVRE(MP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DBG_OUVRE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER MP

      INCLUDE 'dbg.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'

!$    INTEGER OMP_GET_THREAD_NUM
      INTEGER I_RANK
      INTEGER I_ERROR

      REAL*8  TACTU
      INTEGER IRET, INUM
      CHARACTER*(8)   BUF1, BUF2, BUF4
      CHARACTER*(32)  BUF3
      CHARACTER*(256) NAM

      INTEGER CTR
      DATA    CTR /0/
      SAVE    CTR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Monte le nom de fichier
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      WRITE(BUF1, '(I3.3)') I_RANK

      INUM = 0
!$    INUM = OMP_GET_THREAD_NUM()
      WRITE(BUF2, '(I3.3)') INUM

      IRET = C_DH_TIMLCL(TACTU)
      IRET = C_DH_ECRLCL(TACTU, BUF3)
      CALL SP_STRN_DLC(BUF3, '/')
      CALL SP_STRN_DLC(BUF3, ':')

      CTR = CTR + 1
      WRITE(BUF4, '(I3.3)') CTR

      NAM = 'h2d2_dbg_' //
     &      buf1(1:sp_strn_len(buf1)) //
     &      '_' //
     &      buf2(1:sp_strn_len(buf2)) //
     &      '_' //
     &      buf3(1:sp_strn_len(buf3)) //
     &      '_' //
     &      buf4(1:sp_strn_len(buf4)) //
     &      '.log'

C---     Ouvre le fichier
!$omp critical(OMP_CS_DBG)
      IRET = 1
      MP = IO_UTIL_FREEUNIT()
      OPEN(UNIT  = MP,
     &     FILE  = NAM,
     &     FORM  = 'FORMATTED',
     &     STATUS= 'UNKNOWN',
     &     ERR   = 100)
      IRET = 0
100   CONTINUE
!$omp end critical(OMP_CS_DBG)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_OUVERTURE_FICHIER', ' : ',
     &                       NAM(1:SP_STRN_LEN(NAM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9998

9998  IF (MP .NE. 0) CLOSE(MP)
      MP  = 0
      GOTO 9999

9999  CONTINUE
      DBG_OUVRE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dump d'une table REAL*8
C
C Description:
C     La fonction DBG_DMP_DTBL(...) écris la table REAL*8 dans un fichier.
C
C Entrée:
C     INTEGER N      Dimension de la table
C     REAL*8  V      Valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE DBG_DMP_DTBL(N1, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DBG_DMP_DTBL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N1
      REAL*8  V(N1)

      INCLUDE 'err.fi'

      INTEGER DBG_OUVRE
      INTEGER IERR
      INTEGER I, MP
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.dump')

C---     Écris
      MP = 0
      IERR = DBG_OUVRE(MP)
      IF (ERR_GOOD()) WRITE(MP,'(1PE25.17E3)') (V(I),I=1,N1)
      IF (MP .NE. 0) CLOSE(MP)

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.io.dump')

      RETURN
      END

C************************************************************************
C Sommaire: Dump d'une table REAL*8
C
C Description:
C     La fonction DBG_DMP_DTBL(...) écris la table REAL*8 dans un fichier.
C
C Entrée:
C     INTEGER N      Dimension de la table
C     REAL*8  V      Valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE DBG_DMP_DTBL2(N1, N2, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DBG_DMP_DTBL2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N1
      INTEGER N2
      REAL*8  V(N1, N2)

      INCLUDE 'err.fi'

      INTEGER        DBG_OUVRE
      INTEGER        IERR
      INTEGER        I, J, MP
      CHARACTER*(24) FMT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.dump')

C---     Écris
      MP = 0
      IERR = DBG_OUVRE(MP)
      IF (ERR_GOOD()) THEN
         WRITE(FMT, '(A,I6,A)') '(', N1, '(1PE26.17E3))'
         WRITE(MP, FMT) ((V(I,J),I=1,N1),J=1,N2)
      ENDIF
      IF (MP .NE. 0) CLOSE(MP)

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.io.dump')

      RETURN
      END

C************************************************************************
C Sommaire: Dump d'une table INTEGER
C
C Description:
C     La fonction DBG_DMP_ITBL(...) écris la table INTEGER dans un fichier.
C
C Entrée:
C     INTEGER N1        Dimensions de la table
C     INTEGER K(N1)     Valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE DBG_DMP_ITBL(N1, K)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DBG_DMP_ITBL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N1
      INTEGER K(N1)

      INCLUDE 'err.fi'

      INTEGER DBG_OUVRE
      INTEGER IERR
      INTEGER I, MP
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.dump')

C---     Écris
      MP = 0
      IERR = DBG_OUVRE(MP)
      WRITE(MP,'(I12)') (K(I),I=1,N1)
      IF (MP .NE. 0) CLOSE(MP)

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.io.dump')

      RETURN
      END

C************************************************************************
C Sommaire: Dump d'une table INTEGER à deux dimensions.
C
C Description:
C     La fonction DBG_DMP_ITBL2(...) écris la table INTEGER à deux
C     dimensions dans un fichier.
C
C Entrée:
C     INTEGER N1        Dimensions de la table
C     INTEGER N2
C     INTEGER K(N1,N2)  Valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE DBG_DMP_ITBL2(N1, N2, K)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DBG_DMP_ITBL2
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N1
      INTEGER N2
      INTEGER K(N1, N2)

      INCLUDE 'err.fi'

      INTEGER        DBG_OUVRE
      INTEGER        IERR
      INTEGER        I, J, MP
      CHARACTER*(16) FMT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.dump')

C---     Écris
      MP = 0
      IERR = DBG_OUVRE(MP)
      IF (ERR_GOOD()) THEN
         WRITE(FMT, '(A,I6,A)') '(', N1, '(I12))'
         WRITE(MP, FMT) ((K(I,J),I=1,N1),J=1,N2)
      ENDIF
      IF (MP .NE. 0) CLOSE(MP)

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.io.dump')

      RETURN
      END
