C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C
C************************************************************************

!!!   Prototype de classe de handle pour remplacer les int
!!!   Devrait permettre un refcount et une destruction sur compte nul
!!!   Le problème vient de classes qui construisent d'autres objets. Lors du grand
!!!   nettoyage finale par les 999, l'ordre de création n'est pas respecté et les
!!!   sous-objets sont possiblement détruis sous les pieds des propriétaires

!Use case:
!      La classe qui veut gérer des données
!      A l'allocation:
!         ALLOCATE (SELF)
!         IERR = OB_OBJN_CTR(HOBJ, SO_FUNC_HBASE, C_LOC(SELF))  ! Enregistre dans le grand registre de contrôle
!         HNDL = OB_HNDL_T( C_LOC(SELF), HOBJ )                 ! Construit un handle
!      A la désallocation:
!         ICNT = HNDL%REQCNT()
!         IF (ICNT .eq. 1) THEN
!           IERR = OB_OBJN_DTR(HNDL%HOBJ, SO_FUNC_HBASE)
!           IERR = OB_OBJN_DTR(HNDL)
!           deallocate(self)
!        ENDIF

! OB_OBJ est utilisé comme contrôle que les données sont valides sur la base d'un handle

      MODULE OB_HNDL_M
         USE ISO_C_BINDING

         TYPE OB_HNDL_T
            TYPE(C_PTR)      :: CPTR = C_NULL_PTR
            INTEGER, POINTER :: CNTR
            INTEGER          :: HNDL
         CONTAINS
            FINAL     :: OB_HNDL_DTR           ! Destructeur
            PROCEDURE :: OB_HNDL_ASG           ! méthode de la classe OB_HNDL_T
            GENERIC   :: ASSIGNMENT(=) => OB_HNDL_ASG
            
            ! public
            PROCEDURE :: OB_HNDL_REQCNT
            PROCEDURE :: OB_HNDL_REQDTA
            ! private
            PROCEDURE :: OB_HNDL_INCCNT
            PROCEDURE :: OB_HNDL_DECCNT
         END TYPE OB_HNDL_T

         PUBLIC  :: OB_HNDL_REQCNT
         PUBLIC  :: OB_HNDL_REQDTA
         
         PRIVATE :: OB_HNDL_INCCNT
         PRIVATE :: OB_HNDL_DECCNT
         
         INTERFACE OB_HNDL_T                       ! Constructeurs
            MODULE PROCEDURE OB_HNDL_CTR
            MODULE PROCEDURE OB_HNDL_CTR_CPY
            MODULE PROCEDURE OB_HNDL_CTR_HNDL
         END INTERFACE

         INTERFACE INT
            MODULE PROCEDURE OB_HNDL_TO_INT
         END INTERFACE

      CONTAINS

         !************************************************************************
         ! Sommaire: Constructeur par défaut
         !
         ! Description:
         !     La constructeur par défaut OB_HNDL_REQCNT initialise l'objet comme
         !     pointeur vide.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         FUNCTION OB_HNDL_CTR() RESULT(SELF)
 
         TYPE(OB_HNDL_T) :: SELF
         !---------------------------------------------------------------------
         
         SELF%CPTR = C_NULL_PTR
         SELF%CNTR => NULL()
         SELF%HNDL = 0
         
         RETURN
         END FUNCTION OB_HNDL_CTR

         !************************************************************************
         ! Sommaire: Constructeur copie
         !
         ! Description:
         !     La constructeur OB_HNDL_CTR_CPY est le constructeur copie de la classe.
         !     Il prend copie des attributs et incrémente le compte de référence.
         !
         ! Entrée:
         !     OTHER    Le pointeur à copier
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         FUNCTION OB_HNDL_CTR_CPY(OTHER) RESULT(SELF)
         
         TYPE(OB_HNDL_T), INTENT(IN)  :: OTHER
         
         TYPE(OB_HNDL_T) :: SELF
         INTEGER :: ICNT
         !---------------------------------------------------------------------

         SELF%CPTR =  OTHER%CPTR
         SELF%CNTR => OTHER%CNTR
         SELF%HNDL =  OTHER%HNDL
         
         ICNT = SELF%OB_HNDL_INCCNT()
         
         RETURN
         END FUNCTION OB_HNDL_CTR_CPY

         !************************************************************************
         ! Sommaire: Constructeur avec paramètres.
         !
         ! Description:
         !     La constructeur OB_HNDL_CTR_HNDL est le constructeur avec paramètres.
         !
         ! Entrée:
         !     CPRT     Le pointeur C à la structure de données
         !     HNDL     Le handle correspondant
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         FUNCTION OB_HNDL_CTR_HNDL(CPTR, HNDL) RESULT(SELF)

         TYPE(C_PTR), INTENT(IN) :: CPTR
         INTEGER,     INTENT(IN) :: HNDL
           
         TYPE(OB_HNDL_T) :: SELF
         INTEGER :: IRET, ICNT
         !---------------------------------------------------------------------
           
         SELF%CPTR =  CPTR
         SELF%CNTR => NULL()
         SELF%HNDL =  HNDL
           
         ALLOCATE(SELF%CNTR, STAT=IRET)
         !IF (IRET .NE. 0) GOTO 9900
         ICNT = SELF%OB_HNDL_INCCNT()
         
         RETURN
         END FUNCTION OB_HNDL_CTR_HNDL

         !************************************************************************
         ! Sommaire: Destructeur
         !
         ! Description:
         !     La méthode OB_HNDL_DTR est le destructeur de la classe. Il décrémente
         !     le compte de référence et désalloue les structure internes si celui-ci
         !     tombe à zéro.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         SUBROUTINE OB_HNDL_DTR(SELF)

         TYPE(OB_HNDL_T), INTENT(INOUT) :: SELF
         
         INTEGER ICNT
         !---------------------------------------------------------------------

         ! ---  Décrémente le compte de ref
         ICNT = SELF%OB_HNDL_DECCNT()
         
         ! ---  Désalloue le compteur si 0 ref
         IF (ICNT .LE. 0) THEN
            CALL ERR_ASR(ASSOCIATED(SELF%CNTR))
            DEALLOCATE(SELF%CNTR)
         ENDIF
         
         ! ---  Reset les attributs
         SELF%CPTR =  C_NULL_PTR
         SELF%CNTR => NULL()
         SELF%HNDL =  0

         RETURN
         END SUBROUTINE OB_HNDL_DTR

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !     La méthode OB_HNDL_ASG est l'opérateur d'assignation. Il prend copie
         !     de OTHER et incrément le compte de référence.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         SUBROUTINE OB_HNDL_ASG(SELF, OTHER)

         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         CLASS(OB_HNDL_T), INTENT(IN)    :: OTHER
         
         INTEGER :: IERR, ICNT
         !---------------------------------------------------------------------
         
         IF (C_ASSOCIATED(SELF%CPTR)) THEN
            CALL OB_HNDL_DTR(SELF)
         ENDIF
         
         SELF%CPTR = OTHER%CPTR
         SELF%HNDL = OTHER%HNDL
         ICNT = SELF%OB_HNDL_INCCNT()
         
         RETURN
         END SUBROUTINE OB_HNDL_ASG

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !     La méthode OB_HNDL_TO_INT est l'implantation de la fonction INT.
         !     Elle retourne le handle des données.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         INTEGER FUNCTION OB_HNDL_TO_INT(SELF)

         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         !---------------------------------------------------------------------

         OB_HNDL_TO_INT = SELF%HNDL
         RETURN
         END FUNCTION OB_HNDL_TO_INT

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !    La méthode privée OB_HNOB_HNDL_INCCNT incrémente le compte de référence.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         INTEGER FUNCTION OB_HNDL_INCCNT(SELF)
         
         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         !---------------------------------------------------------------------
         CALL ERR_ASR(C_ASSOCIATED(SELF%CPTR))
         CALL ERR_ASR(SELF%CNTR .GE. 0)
         CALL ERR_ASR(SELF%HNDL .NE. 0)
         !---------------------------------------------------------------------

         SELF%CNTR = SELF%CNTR + 1
         OB_HNDL_INCCNT = SELF%CNTR
         RETURN
         END FUNCTION OB_HNDL_INCCNT

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !    La méthode privée OB_HNDL_DECCNT décrémente le compte de référence
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         INTEGER FUNCTION OB_HNDL_DECCNT(SELF)

         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         !---------------------------------------------------------------------
         CALL ERR_ASR(C_ASSOCIATED(SELF%CPTR))
         CALL ERR_ASR(SELF%CNTR .GE. 1)
         CALL ERR_ASR(SELF%HNDL .NE. 0)
         !---------------------------------------------------------------------

         SELF%CNTR = SELF%CNTR - 1
         OB_HNDL_DECCNT = SELF%CNTR
         RETURN
         END FUNCTION OB_HNDL_DECCNT

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !    La méthode OB_HNDL_REQCNT retourne le compte de référence
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         INTEGER FUNCTION OB_HNDL_REQCNT(SELF)

         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         !---------------------------------------------------------------------
         CALL ERR_ASR(C_ASSOCIATED(SELF%CPTR))
         CALL ERR_ASR(SELF%CNTR .GE. 0)
         CALL ERR_ASR(SELF%HNDL .NE. 0)
         !---------------------------------------------------------------------

         OB_HNDL_REQCNT = SELF%CNTR
         RETURN
         END FUNCTION OB_HNDL_REQCNT

         !************************************************************************
         ! Sommaire:
         !
         ! Description:
         !    La méthode OB_HNDL_REQDTA retourne le pointeur aux données.
         !
         ! Entrée:
         !
         ! Sortie:
         !
         ! Notes:
         !************************************************************************
         TYPE(C_PTR) FUNCTION OB_HNDL_REQDTA(SELF)

         CLASS(OB_HNDL_T), INTENT(INOUT) :: SELF
         !---------------------------------------------------------------------
         CALL ERR_ASR(C_ASSOCIATED(SELF%CPTR))
         CALL ERR_ASR(SELF%CNTR .GE. 0)
         CALL ERR_ASR(SELF%HNDL .NE. 0)
         !---------------------------------------------------------------------

         OB_HNDL_REQDTA = SELF%CPTR
         RETURN
         END FUNCTION OB_HNDL_REQDTA
         
      END MODULE OB_HNDL_M
