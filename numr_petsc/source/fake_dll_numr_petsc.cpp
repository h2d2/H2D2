//************************************************************************
// H2D2 - External declaration of public symbols
// Module: numr_petsc
// Entry point: extern "C" void fake_dll_numr_petsc()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:04.285000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_NM_PTSC
F_PROT(IC_NM_PTSC_XEQCTR, ic_nm_ptsc_xeqctr);
F_PROT(IC_NM_PTSC_XEQMTH, ic_nm_ptsc_xeqmth);
F_PROT(IC_NM_PTSC_REQCLS, ic_nm_ptsc_reqcls);
F_PROT(IC_NM_PTSC_REQHDL, ic_nm_ptsc_reqhdl);
 
// ---  class NM_PTSC
F_PROT(NM_PTSC_000, nm_ptsc_000);
F_PROT(NM_PTSC_999, nm_ptsc_999);
F_PROT(NM_PTSC_CTR, nm_ptsc_ctr);
F_PROT(NM_PTSC_DTR, nm_ptsc_dtr);
F_PROT(NM_PTSC_INI, nm_ptsc_ini);
F_PROT(NM_PTSC_RST, nm_ptsc_rst);
F_PROT(NM_PTSC_REQHBASE, nm_ptsc_reqhbase);
F_PROT(NM_PTSC_HVALIDE, nm_ptsc_hvalide);
F_PROT(NM_PTSC_PART, nm_ptsc_part);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_numr_petsc()
{
   static char libname[] = "numr_petsc";
 
   // ---  class IC_NM_PTSC
   F_RGST(IC_NM_PTSC_XEQCTR, ic_nm_ptsc_xeqctr, libname);
   F_RGST(IC_NM_PTSC_XEQMTH, ic_nm_ptsc_xeqmth, libname);
   F_RGST(IC_NM_PTSC_REQCLS, ic_nm_ptsc_reqcls, libname);
   F_RGST(IC_NM_PTSC_REQHDL, ic_nm_ptsc_reqhdl, libname);
 
   // ---  class NM_PTSC
   F_RGST(NM_PTSC_000, nm_ptsc_000, libname);
   F_RGST(NM_PTSC_999, nm_ptsc_999, libname);
   F_RGST(NM_PTSC_CTR, nm_ptsc_ctr, libname);
   F_RGST(NM_PTSC_DTR, nm_ptsc_dtr, libname);
   F_RGST(NM_PTSC_INI, nm_ptsc_ini, libname);
   F_RGST(NM_PTSC_RST, nm_ptsc_rst, libname);
   F_RGST(NM_PTSC_REQHBASE, nm_ptsc_reqhbase, libname);
   F_RGST(NM_PTSC_HVALIDE, nm_ptsc_hvalide, libname);
   F_RGST(NM_PTSC_PART, nm_ptsc_part, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
