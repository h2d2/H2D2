C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   Algorithme de partitionnement pt-ScoTCH
C Type:    Concret
C Note:
C     Structure de la table de redistribution locale: KDISTR(NPROC+2, NNL)
C        NNO_PR1 NNO_PR2 ...  NNO_PRn NNO_Glb IPROC   ! .LE. 0 si absent
C
C Functions:
C   Public:
C     INTEGER NM_PTSC_000
C     INTEGER NM_PTSC_999
C     INTEGER NM_PTSC_CTR
C     INTEGER NM_PTSC_DTR
C     INTEGER NM_PTSC_INI
C     INTEGER NM_PTSC_RST
C     INTEGER NM_PTSC_REQHBASE
C     LOGICAL NM_PTSC_HVALIDE
C     INTEGER NM_PTSC_PART
C     INTEGER NM_PTSC_RENUM
C     INTEGER NM_PTSC_GENNUM
C     INTEGER NM_PTSC_SAUVE
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PTSC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_PTSC_NOBJMAX,
     &                   NM_PTSC_HBASE,
     &                   'Renumérotation Scotch')

      NM_PTSC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PTSC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER  IERR
      EXTERNAL NM_PTSC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_PTSC_NOBJMAX,
     &                   NM_PTSC_HBASE,
     &                   NM_PTSC_DTR)

      NM_PTSC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PTSC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_PTSC_NOBJMAX,
     &                   NM_PTSC_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_PTSC_HVALIDE(HOBJ))
         IOB = HOBJ - NM_PTSC_HBASE

         NM_PTSC_LDSTR(IOB) = 0
         NM_PTSC_HFPRT(IOB) = 0
         NM_PTSC_HFRNM(IOB) = 0
      ENDIF

      NM_PTSC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PTSC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_PTSC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_PTSC_NOBJMAX,
     &                   NM_PTSC_HBASE)

      NM_PTSC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PTSC_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_PTSC_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - NM_PTSC_HBASE

         NM_PTSC_LDSTR(IOB) = 0
         NM_PTSC_HFPRT(IOB) = 0
         NM_PTSC_HFRNM(IOB) = 0
      ENDIF

      NM_PTSC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PTSC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LDSTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_PTSC_HBASE
      LDSTR = NM_PTSC_LDSTR(IOB)
      !HFPRT = NM_PTSC_HFPRT(IOB)
      !HFRNM = NM_PTSC_HFRNM(IOB)

C---     DETRUIS LES ATTRIBUTS
      IF (LDSTR .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSTR)
      !IF (SO_FUNC_HVALIDE(HFRNM)) IERR = SO_FUNC_DTR(HFRNM)
      !IF (SO_FUNC_HVALIDE(HFPRT)) IERR = SO_FUNC_DTR(HFPRT)

C---     RESET
      NM_PTSC_LDSTR(IOB) = 0
      NM_PTSC_HFPRT(IOB) = 0
      NM_PTSC_HFRNM(IOB) = 0

      NM_PTSC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_PTSC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PTSC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmptsc.fi'
      INCLUDE 'nmptsc.fc'
C------------------------------------------------------------------------

      NM_PTSC_REQHBASE = NM_PTSC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_PTSC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PTSC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmptsc.fc'
C------------------------------------------------------------------------

      NM_PTSC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_PTSC_NOBJMAX,
     &                                  NM_PTSC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Partitionne un maillage.
C
C Description:
C     La fonction NM_PTSC_PART partitionne le maillage donné par HELEM en NPROC
C     sous-domaines. Si NPROC .LE. 0, alors on partitionne pour le nombre
C     de process dans le groupe MPI (cluster).
C     <p>
C     La partition n'est pas utilisable immédiatement car il n'y a pas
C     redistribution des données entre les process. La redistribution a lieu au
C     chargement des données. La partition doit donc être sauvée puis chargée.
C     Le maillage devra également être relu.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur le maillage à partitionner
C     NPART       Nombre de sous-domaines demandés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PTSC_PART (HOBJ, HGRID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PTSC_PART
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRID

      INCLUDE 'nmptsc.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmptsc.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER I_RANK, I_SIZE, I_ERROR, I_PROC, I_COMM
      INTEGER LCOR, LELE, LDSTR
      INTEGER NDIM, NNT, NNEL, NELT
      
      REAL*8, PARAMETER :: TNEW = 0.0D0
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_GRID_HVALIDE(HGRID))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      I_PROC = I_RANK + 1

C---     Récupère les attributs
      IOB = HOBJ - NM_PTSC_HBASE
      !!!LDSTR = NM_PTSC_LDSTR(IOB)

C---     Charge le maillage
      IF (ERR_GOOD()) IERR = DT_GRID_CHARGE(HGRID, TNEW)

C---     Récupère les attributs du maillage
      LCOR = 0
      LELE = 0
      IF (ERR_GOOD()) THEN
         NDIM = DT_GRID_REQNDIM(HGRID)
         NNT  = DT_GRID_REQNNT (HGRID)
         NNEL = DT_GRID_REQNNEL(HGRID)
         NELT = DT_GRID_REQNELT(HGRID)
         LELE = DT_ELEM_REQLELE(DT_GRID_REQHELEV(HGRID))
         LCOR = DT_VNOD_REQLVNO(DT_GRID_REQHCOOR(HGRID))
      ENDIF

C---     Dimensionne la table de distribution
      LDSTR = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT((I_SIZE+1)*NNT, LDSTR)
      
C---     Partitionne - distribue les noeuds
      IF (ERR_GOOD()) THEN
         IRET = C_PTSC_INITIALIZE('')
         IRET = C_PTSC_PART(I_COMM,
     &                      I_SIZE,
     &                      KA(SO_ALLC_REQKIND(KA,LDSTR)),
     &                      NDIM,
     &                      NNT,
     &                      VA(SO_ALLC_REQVIND(VA,LCOR)),
     &                      NNEL,
     &                      NELT,
     &                      KA(SO_ALLC_REQKIND(KA,LELE)))

         IF (IRET .NE. 0) THEN
            IRET = C_PTSC_ERRMSG(IRET, ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
         IERR = MP_UTIL_SYNCERR()
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         !!!NM_PTSC_LDSTR (IOB) = LDSTR
         !!!NM_PTSC_NNT   (IOB) = NNT
         !!!NM_PTSC_NPRTG (IOB) = NPRTG
         !!!NM_PTSC_NPRTL (IOB) = NPRTL
         !!!NM_PTSC_IPRTL (IOB) = IDEB
      ENDIF

      NM_PTSC_PART = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire: Renumérote un maillage.
!!!C
!!!C Description:
!!!C     La fonction NM_PTSC_RENUM renumérote le maillage HELEM afin d'en
!!!C     réduire la largeur de bande.
!!!C
!!!C Entrée:
!!!C     HOBJ        Handle sur l'objet
!!!C     HELEM       Handle sur le maillage à partitionner
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION NM_PTSC_RENUM (HOBJ, HELEM)
!!!C$pragma aux NM_PTSC_RENUM export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: NM_PTSC_RENUM
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!      INTEGER HELEM
!!!
!!!      INCLUDE 'nmptsc.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'nmnbse.fi'
!!!      INCLUDE 'nmptsc.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER IOB
!!!      INTEGER HPRNT
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
!!!C-----------------------------------------------------------------------
!!!
!!!      IERR = ERR_OK
!!!
!!!C---     RECUPERE LES ATTRIBUTS
!!!      IOB = HOBJ - NM_PTSC_HBASE
!!!      HPRNT = NM_PTSC_HPRNT(IOB)
!!!
!!!C---     APPEL LE PARENT
!!!      IERR  = NM_PTSC_RENUM(HPRNT, HELEM)
!!!
!!!      NM_PTSC_RENUM = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire: Construis une renumérotation.
!!!C
!!!C Description:
!!!C     La fonction NM_PTSC_GENNUM retourne un renumérotation globale
!!!C     nouvellement créée à partir des données de l'objet. Il est de
!!!C     la responsabilité de l'utilisateur de disposer de HNUM.
!!!C
!!!C Entrée:
!!!C     HOBJ        Handle sur l'objet
!!!C
!!!C Sortie:
!!!C     HNUM        Handle sur la renumérotation
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION NM_PTSC_GENNUM (HOBJ, HNUM)
!!!C$pragma aux NM_PTSC_GENNUM export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: NM_PTSC_GENNUM
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!      INTEGER HNUM
!!!
!!!      INCLUDE 'nmptsc.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'nmnbse.fi'
!!!      INCLUDE 'nmptsc.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER IOB
!!!      INTEGER HPRNT
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
!!!C-----------------------------------------------------------------------
!!!
!!!      IERR = ERR_OK
!!!
!!!C---     RECUPERE LES ATTRIBUTS
!!!      IOB = HOBJ - NM_PTSC_HBASE
!!!      HPRNT = NM_PTSC_HPRNT(IOB)
!!!
!!!C---     APPEL LE PARENT
!!!      IERR  = NM_PTSC_GENNUM(HPRNT, HNUM)
!!!
!!!      NM_PTSC_GENNUM = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire: Écris la table de redistribution.
!!!C
!!!C Description:
!!!C     La fonction NM_PTSC_SAUVE écris la table de redistribution dans le
!!!C     fichier NOMFIC.
!!!C
!!!C Entrée:
!!!C     HOBJ        Handle sur l'objet
!!!C     NOMFIC      Nom du fichier
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION NM_PTSC_SAUVE (HOBJ, NOMFIC)
!!!C$pragma aux NM_PTSC_SAUVE export
!!!CDEC$ ATTRIBUTES DLLEXPORT :: NM_PTSC_SAUVE
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER       HOBJ
!!!      CHARACTER*(*) NOMFIC
!!!
!!!      INCLUDE 'nmptsc.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'nmnbse.fi'
!!!      INCLUDE 'nmptsc.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER IOB
!!!      INTEGER HPRNT
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(NM_PTSC_HVALIDE(HOBJ))
!!!C-----------------------------------------------------------------------
!!!
!!!      IERR = ERR_OK
!!!
!!!C---     RECUPERE LES ATTRIBUTS
!!!      IOB = HOBJ - NM_PTSC_HBASE
!!!      HPRNT = NM_PTSC_HPRNT(IOB)
!!!
!!!C---     APPEL LE PARENT
!!!      IERR  = NM_PTSC_SAUVE(HPRNT, NOMFIC)
!!!
!!!      NM_PTSC_SAUVE = ERR_TYP()
!!!      RETURN
!!!      END
