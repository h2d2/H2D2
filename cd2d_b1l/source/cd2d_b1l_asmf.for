C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulérienne 2-D
C     Contaminant conservatif
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_B1L_ASMF
C
C Description:
C        ASSEMBLAGE :   "{F}"
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_B1L_ASMF_V(KLOCE,
     &                           VFE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLC,
     &                           VSOLR,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG,
     &                           VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      REAL*8  B1, B2, B3, SB
      REAL*8  CONST, DETJT3
      INTEGER IERR
      INTEGER IC, IE
      INTEGER IPB
      INTEGER NDLEV
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .EQ. 1)
C-----------------------------------------------------------------------

C---     INDICES DANS VPRNO
      IPB = LM_CMMN_NPRNOL + 7

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      NDLEV = LM_CMMN_NDLEV
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES DU T3
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)

C---        MÉTRIQUES DU T3
         DETJT3 = VDJV(5,IE)

C---        CONSTANTES DÉPENDANT DU VOLUME
         CONST = UN_24 * DETJT3

C---        INITIALISATION DES PROPRIÉTÉS NODALES
         B1 = CONST*VPRNO(IPB,NO1)
         B2 = CONST*VPRNO(IPB,NO2)
         B3 = CONST*VPRNO(IPB,NO3)
         SB = B1 + B2 + B3

C---        ASSEMBLAGE DU TERME PUITS ET SOURCE
         VFE(1, 1) = SB + B1
         VFE(1, 2) = SB + B2
         VFE(1, 3) = SB + B3

C---        TABLE KLOCE DE LOCALISATION DES DDLS
         KLOCE(1, 1)= KLOCN(1, NO1)
         KLOCE(1, 2)= KLOCN(1, NO2)
         KLOCE(1, 3)= KLOCN(1, NO3)

C---        ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

