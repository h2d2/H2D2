C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   renumérotation des éléments par une méthode FRoNTale
C Type:    Concret
C Note:
C     Structure de la table de redistribution locale: KDISTR(NPROC+2, NNL)
C        NNO_PR1 NNO_PR2 ...  NNO_PRn NNO_Glb IPROC   ! .LE. 0 si absent
C
C Functions:
C   Public:
C     INTEGER NM_FRNT_000
C     INTEGER NM_FRNT_999
C     INTEGER NM_FRNT_CTR
C     INTEGER NM_FRNT_DTR
C     INTEGER NM_FRNT_INI
C     INTEGER NM_FRNT_RST
C     INTEGER NM_FRNT_REQHBASE
C     LOGICAL NM_FRNT_HVALIDE
C     INTEGER NM_FRNT_RENUM
C     INTEGER NM_FRNT_GENNUM
C     INTEGER NM_FRNT_SAUVE
C   Private:
C     INTEGER NM_FRNT_RN_XEQ
C     INTEGER NM_FRNT_RN_NME
C     INTEGER NM_FRNT_RN_C2R
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_FRNT_NOBJMAX,
     &                   NM_FRNT_HBASE,
     &                   'Frontal element renumbering')

      NM_FRNT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER  IERR
      EXTERNAL NM_FRNT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_FRNT_NOBJMAX,
     &                   NM_FRNT_HBASE,
     &                   NM_FRNT_DTR)

      NM_FRNT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_FRNT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_FRNT_NOBJMAX,
     &                   NM_FRNT_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_FRNT_HVALIDE(HOBJ))
         IOB = HOBJ - NM_FRNT_HBASE

         NM_FRNT_LDSTR(IOB) = 0
         NM_FRNT_NELT (IOB) = 0
         NM_FRNT_NELL (IOB) = 0
         NM_FRNT_NPROC(IOB) = 0
         NM_FRNT_IPROC(IOB) = 0
      ENDIF

      NM_FRNT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_FRNT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_FRNT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_FRNT_NOBJMAX,
     &                   NM_FRNT_HBASE)

      NM_FRNT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_FRNT_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER I_FLAG, I_ERROR
      LOGICAL I_INIT
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE LES PARAMETRES
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (.NOT. I_INIT)     GOTO 9900

C---     RESET LES DONNEES
      IERR = NM_FRNT_RST(HOBJ)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MPI_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_FRNT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_FRNT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LDSTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_FRNT_HBASE
      LDSTR = NM_FRNT_LDSTR(IOB)

C---     DESALLOUE LA MÉMOIRE
      IF (LDSTR .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSTR)

C---     RESET
      NM_FRNT_LDSTR(IOB) = 0
      NM_FRNT_NELT (IOB) = 0
      NM_FRNT_NELL (IOB) = 0
      NM_FRNT_NPROC(IOB) = 0
      NM_FRNT_IPROC(IOB) = 0

      NM_FRNT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_FRNT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'nmfrnt.fc'
C------------------------------------------------------------------------

      NM_FRNT_REQHBASE = NM_FRNT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_FRNT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmfrnt.fc'
C------------------------------------------------------------------------

      NM_FRNT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_FRNT_NOBJMAX,
     &                                  NM_FRNT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote un maillage.
C
C Description:
C     La fonction NM_FRNT_RENUM renumérote les éléments de maillage HELEM,
C     de renumérotation des noeuds HNUMC, afin d'ordonner les éléments
C     d'après l'avancée du front des noeuds. En sortie, HELEM n'est pas
C     modifié.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur les connectivités du maillage
C     HNUMC       Handle sur la renumérotation des noeuds
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_RENUM (HOBJ, HELEM, HNUMC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_RENUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER HNUMC

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER I_ERROR, I_SIZE
      INTEGER HNMID, H_ELE, LELE
      INTEGER NNEL, NELT, NDUM, NNT
      INTEGER LTXT
      CHARACTER*(256) TXT
      REAL*8  TNEW
      LOGICAL I_INIT, MODIF

      INTEGER IIAMAX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.renum')

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.front'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_FRONTAL_RENUM')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Contrôle les paramètres
      IF (.NOT. DT_ELEM_HVALIDE(HELEM)) GOTO 9900
      IF (HNUMC .NE. 0 .AND. .NOT. NM_NUMR_CTRLH(HNUMC)) GOTO 9901
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (.NOT. I_INIT)     GOTO 9902
      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (I_SIZE .LE. 0) GOTO 9902
      IF (HNUMC .NE. 0) THEN
         IF (I_SIZE .NE. NM_NUMR_REQNPRT(HNUMC)) GOTO 9903
      ENDIF

C---     Au besoin crée la numérotation identité
      IF (HNUMC .EQ. 0) THEN
C---        Construis un nouveau maillage global
         H_ELE = 0
         IF (ERR_GOOD()) THEN
            IERR = DT_ELEM_CTR   (H_ELE)
            IERR = DT_ELEM_INIFIC(H_ELE,
     &                            DT_ELEM_REQNOMF(HELEM),
     &                            IO_MODE_LECTURE + IO_MODE_ASCII)
         ENDIF
C---        Récupère les dimensions du maillage
         NNEL = DT_ELEM_REQNNEL(H_ELE)
         NELT = DT_ELEM_REQNELT(H_ELE)
C---        Lis les connectivités avec une num. identité
         IF (ERR_GOOD()) THEN
            TNEW = 0.0D0
            NDUM = NELT*NNEL   ! Surdimensionné, devrait être NNT
            IERR = NM_IDEN_CTR   (HNUMC)
            IERR = NM_IDEN_INI   (HNUMC, NDUM)
            IERR = DT_ELEM_CHARGE(H_ELE, HNUMC, HNUMC, TNEW, MODIF)
         ENDIF
C---        Récupère la table des connectivités
         IF (ERR_GOOD()) LELE = DT_ELEM_REQLELE(H_ELE)
C---        Calcule le nombre de noeuds
         IF (ERR_GOOD()) THEN
            NNT = IIAMAX(NNEL*NELT, KA(SO_ALLC_REQKIND(KA,LELE)), 1)
            NNT = SO_ALLC_REQIN4(LELE, NNT)
         ENDIF
C---        Reset la num. identité avec le bon nombre de noeuds
         IF (ERR_GOOD()) IERR = NM_IDEN_RST(HNUMC)
         IF (ERR_GOOD()) IERR = NM_IDEN_INI(HNUMC, NNT)
C---        Détruit le maillage
         IF (DT_ELEM_HVALIDE(H_ELE)) IERR = DT_ELEM_DTR(H_ELE)
      ENDIF

C---     Renumérote
      IF (ERR_GOOD()) IERR = NM_FRNT_RN_XEQ(HOBJ, HELEM, HNUMC)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CONNEC_MAILLAGE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_RENUM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_MPI_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,I3,A,I3)') 'ERR_NPART_VS_MPI_INCOHERENT',
     &                              NM_NUMR_REQNPRT(HNUMC), '/', I_SIZE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      CALL TR_CHRN_STOP('h2d2.grid.renum')
      NM_FRNT_RENUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis une renumérotation.
C
C Description:
C     La fonction NM_FRNT_GENNUM retourne un renumérotation globale
C     nouvellement créée à partir des données de l'objet. Il est de
C     la responsabilité de l'utilisateur de disposer de HNUM.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HNUM        Handle sur la renumérotation crée
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_GENNUM(HOBJ, HNUM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_GENNUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I_SIZE, I_RANK, I_ERROR
      INTEGER IPROC, NPROC
      INTEGER NELT, NELL
      INTEGER LDSTR, L_ROW
      INTEGER LTXT
      CHARACTER*(256) TXT
      
      INTEGER NM_FRNT_RN_C2R
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.front'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_FRONTAL_GENNUM')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_FRNT_HBASE
      LDSTR = NM_FRNT_LDSTR(IOB)
      NELT  = NM_FRNT_NELT (IOB)
      NELL  = NM_FRNT_NELL (IOB)
      NPROC = NM_FRNT_NPROC(IOB)
      IPROC = NM_FRNT_IPROC(IOB)

C---     PARAMÈTRES MPI
      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     CONTROLES
      IF (NM_NUMR_CTRLH(HNUM)) GOTO 9900
      IF (NPROC .NE. I_SIZE)   GOTO 9901
      IF (IPROC .NE. I_RANK+1) GOTO 9902
      IF (NELL  .LE. 0) GOTO 9903

C---     ALLOUE LA MÉMOIRE
      L_ROW = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELL*(NPROC+2), L_ROW)

C---     REMPLIS LA TABLE EN FORMAT LIGNE
      IF (ERR_GOOD())
     &   IERR = NM_FRNT_RN_C2R(NELL,
     &                         NELT,
     &                         NPROC,
     &                         1,         ! NPROC local
     &                         IPROC,
     &                         KA(SO_ALLC_REQKIND(KA,LDSTR)),
     &                         KA(SO_ALLC_REQKIND(KA,L_ROW)))

C---     CREE ET INITIALISE LA RENUMEROTATION
      HNUM = 0
      IF (ERR_GOOD()) IERR = NM_LCGL_CTR(HNUM)
      IF (ERR_GOOD()) IERR = NM_LCGL_INI(HNUM,
     &                                   ' ',
     &                                   NELL,
     &                                   NELT,
     &                                   NPROC,
     &                                   L_ROW)

C---     LIS
      IF (ERR_GOOD())
     &   IERR = NM_LCGL_CHARGE(HNUM)

C---     RECUPERE LA MÉMOIRE
      IF (L_ROW .NE. 0) IERR = SO_ALLC_ALLINT(0, L_ROW)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_NULL_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A,I3,A,I3)') 'ERR_PART_CLUSTER_INCOHERENTS: ',
     &      NPROC, '/', I_SIZE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,I3)') 'ERR_: ', IPROC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_NBR_ELEM_LOCAUX_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      NM_FRNT_GENNUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris la table de redistribution.
C
C Description:
C     La fonction NM_FRNT_SAUVE écris la table de redistribution dans le
C     fichier NOMFIC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_FRNT_SAUVE (HOBJ, NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_FRNT_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmfrnt.fc'

      INTEGER IERR
      INTEGER IOB, IPROC
      INTEGER HFIC
      INTEGER LDSTR
      INTEGER NELT, NPROC
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_FRNT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.front'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_CMD_FRONTAL_SAUVE')
      CALL LOG_INCIND()

C---     Log self
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_FRNT_HBASE
      LDSTR = NM_FRNT_LDSTR(IOB)
      NELT  = NM_FRNT_NELT (IOB)
      NPROC = NM_FRNT_NPROC(IOB)
      IPROC = NM_FRNT_IPROC(IOB)

C---     CONTROLES
      IF (LDSTR .EQ. 0) GOTO 9900
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9901

C---     CREE ET INITIALISE L'OBJET FICHIER
      HFIC = 0
      IF (ERR_GOOD()) IERR = FD_NUMR_CTR(HFIC)
      IF (ERR_GOOD())
     &   IERR = FD_NUMR_INI(HFIC,
     &                      NOMFIC,
     &                      IO_MODE_ECRITURE + IO_MODE_ASCII)

C---     ECRIS
      IF (ERR_GOOD())
     &   IERR = FD_NUMR_ECRIS(HFIC,
     &                        NELT,
     &                        NPROC,
     &                        1,
     &                        IPROC,
     &                        KA(SO_ALLC_REQKIND(KA,LDSTR)))

C---     DETRUIS L'OBJET FICHIER
      IF (FD_NUMR_HVALIDE(HFIC)) IERR = FD_NUMR_DTR(HFIC)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OBJET_NON_CHARGE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      NM_FRNT_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute la renumérotation des éléments d'un maillage.
C
C Description:
C     La méthode privée NM_FRNT_RN_XEQ monte la table de renumérotation
C     des éléments HELEM.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HELEM       Handle sur les connectivités du maillage
C     HNUMC       Handle sur la renumérotation des noeuds
C
C Sortie:
C
C Notes:
C     on lit les connec avec des renum identité pour avoir tous les
C     éléments.
C************************************************************************
      FUNCTION NM_FRNT_RN_XEQ(HOBJ, HELEM, HNUMC)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER HNUMC

      INCLUDE 'nmfrnt.fi'
      INCLUDE 'c_front.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmfrnt.fc'

      REAL*8  TNEW
      INTEGER IERR
      INTEGER IOB
      INTEGER I_PROC, I_RANK, I_SIZE, I_ERROR, I_COMM
      INTEGER HNMID_C, HNMID_E, H_ELE
      INTEGER LELE, LDSTR, L_TRV
      INTEGER NNEL, NELT, NELL, NDUM
      LOGICAL MODIF

      INTEGER NM_FRNT_RN_NME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HELEM))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - NM_FRNT_HBASE
      LDSTR = NM_FRNT_LDSTR(IOB)

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      I_PROC = I_RANK + 1

C---     Construis un nouveau maillage global de travail
      H_ELE = 0
      IF (ERR_GOOD()) THEN
         IERR = DT_ELEM_CTR   (H_ELE)
         IERR = DT_ELEM_INIFIC(H_ELE,
     &                         DT_ELEM_REQNOMF(HELEM),
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)
      ENDIF

C---     Récupère les dimensions du maillage
      IF (ERR_GOOD()) THEN
         NNEL = DT_ELEM_REQNNEL(H_ELE)
         NELT = DT_ELEM_REQNELT(H_ELE)
      ENDIF

C---     Lis toutes les connec avec une num. identité
      HNMID_C = 0
      HNMID_E = 0
      IF (ERR_GOOD()) THEN
         TNEW = 0.0D0
         NDUM = NELT*NNEL   ! Surdimensionné, devrait être max(NNT,NELT)
         IERR = NM_IDEN_CTR   (HNMID_C)
         IERR = NM_IDEN_INI   (HNMID_C, NDUM)
         IERR = NM_IDEN_CTR   (HNMID_E)
         IERR = NM_IDEN_INI   (HNMID_E, NELT)
         IERR = DT_ELEM_CHARGE(H_ELE, HNMID_C, HNMID_E, TNEW, MODIF)
         IERR = NM_IDEN_DTR   (HNMID_E)
         IERR = NM_IDEN_DTR   (HNMID_C)
      ENDIF

C---     Récupère les données du maillage
      IF (ERR_GOOD()) THEN
         LELE = DT_ELEM_REQLELE(H_ELE)
      ENDIF

C---     Dimensionne la table de travail
      L_TRV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELT, L_TRV)

C---     Dimensionne la table de distribution
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(2*NELT, LDSTR)

C---     Renumérote les elements
      IF (ERR_GOOD()) THEN
         IERR = NM_FRNT_RN_NME(I_PROC,
     &                         NELL,
     &                         HNUMC,
     &                         NNEL,
     &                         NELT,
     &                         KA(SO_ALLC_REQKIND(KA, LELE)),
     &                         KA(SO_ALLC_REQKIND(KA, L_TRV)),
     &                         KA(SO_ALLC_REQKIND(KA, LDSTR)))
      ENDIF

C---     Récupère la mémoire
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)

C---     Détruis le maillage
      IF (DT_ELEM_HVALIDE(H_ELE)) THEN
         IERR = DT_ELEM_DTR(H_ELE)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         NM_FRNT_LDSTR (IOB) = LDSTR
         NM_FRNT_NELT  (IOB) = NELT
         NM_FRNT_NELL  (IOB) = NELL
         NM_FRNT_NPROC (IOB) = I_SIZE
         NM_FRNT_IPROC (IOB) = I_PROC
      ENDIF

      NM_FRNT_RN_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote les éléments.
C
C Description:
C     La fonction privée NM_FRNT_RN_NME renumérote les éléments de KNG.
C     Elle retourne dans KDIST la table de renumérotation complète pour le
C     process courant. La table KNG est détruite.
C     <p>
C     1) On renumérote les connectivités, flagant au passage les éléments
C     du process courant et comptant le nombre d'éléments à connectivités 0.
C     2) Puis on renumérote tous les éléments; les éléments qui ne sont pas
C     du process et qui ont donc des connectivités 0 se retrouvent en début
C     du tri.
C     3) On transfert la renumérotation en numéros d'éléments locaux.
C     4) On assigne le numéro de process.
C
C Entrée:
C     IPROC       Numéro de process
C     HNUMC       Handle sur la renumérotation des noeuds
C     NNEL        Nombre de noeuds par élément
C     NELT        Nombre d'éléments total
C     KNG         Table des connectivités élémentaires
C     KTRV        Table de travail
C
C Sortie:
C     NELL        Nombre d'éléments locaux
C     KDIST       Table de renumérotation chargée
C
C Notes:
C     Bug sun12.1: la boucle de transfert est détruite (sautée?) si kdist
C        n'est pas accédé après, d'où la boucle de log
C************************************************************************
      FUNCTION NM_FRNT_RN_NME(IPROC,
     &                        NELL,
     &                        HNUMC,
     &                        NNEL,
     &                        NELT,
     &                        KNG,
     &                        KTRV,
     &                        KDIST)

      IMPLICIT NONE

      INTEGER NM_FRNT_RN_NME
      INTEGER IPROC
      INTEGER NELL
      INTEGER HNUMC
      INTEGER NNEL
      INTEGER NELT
      INTEGER KNG  (NNEL, NELT)
      INTEGER KTRV (NELT)
      INTEGER KDIST(2, NELT)

      INCLUDE 'c_front.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER IERR, IRET
      INTEGER IE, IN, IL
      INTEGER ISKIP
      LOGICAL ESTLOCAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_PRE(NNEL .GT. 0)
D     CALL ERR_PRE(NELT .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.renum.front'

      CALL IINIT(NELT,   0, KTRV,  1)
      CALL IINIT(NELT*2, 0, KDIST, 1)

C---     Renumérote les connectivités
      ISKIP = 0
      DO IE=1,NELT
         ESTLOCAL = .TRUE.
         DO IN=1,NNEL
            KNG(IN,IE) = NM_NUMR_REQNINT(HNUMC, KNG(IN,IE))
            IF (KNG(IN,IE) .LE. 0) ESTLOCAL = .FALSE.
         ENDDO
         IF (.NOT. ESTLOCAL) THEN
            ISKIP = ISKIP + 1
         ENDIF
      ENDDO
      NELL = NELT - ISKIP
      WRITE(LOG_BUF, '(A,3I6)') 'IPROC, NELT, NELL: ', IPROC, NELT, NELL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Renumérote les elements
      IF (ERR_GOOD()) THEN
         IRET = C_FRONT_RENUM(KTRV,    ! Les elem à connec 0
     &                        NNEL,    ! vont se retrouver dans
     &                        NELT,    ! le bas
     &                        KNG)
      ENDIF

C---     Transfert et compacte (cf.note)
      DO IL=1,NELL
         IE = KTRV(IL+ISKIP)
         KDIST(1,IE) = IL
         KDIST(2,IE) = IPROC
      ENDDO
D     DO IE=1,ISKIP
D        CALL ERR_ASR( KDIST(1,KTRV(IE)) .EQ. 0 )
D     ENDDO

C---     Log en debug (cf.note)
      IF (LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_DEBUG)) THEN
         CALL LOG_INCIND()
         DO IE=1,NELT
            WRITE(LOG_BUF, '(A,2I6)') 'IE_G, IE_L: ', IE, KDIST(1,IE)
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDDO
         CALL LOG_DECIND()
      ENDIF

      NM_FRNT_RN_NME = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NM_FRNT_RN_C2R
C
C Description:
C     La fonction privée NM_FRNT_RN_C2R passe d'une table de renumérotation
C     par colonne en une table des lignes pour un process.
C
C Entrée:
C     NNL         Nombre de noeuds local
C     NNT         Nombre de noeuds total
C     NPRTG       Nombre de partitions global
C     NPRTL       Nombre de partitions local
C     IPRTL       Partition locale à traiter
C     KCOL        Table de renumérotation par colonne
C
C Sortie:
C     KROW        Table de renumérotation des lignes du process IPRTL
C
C Note:
C     C'est le même code que NM_NBSE_PA_C2R!!
C************************************************************************
      FUNCTION NM_FRNT_RN_C2R(NNL,
     &                        NNT,
     &                        NPRTG,
     &                        NPRTL,
     &                        IPRTL,
     &                        KCOL,
     &                        KROW)

      IMPLICIT NONE

      INTEGER NM_FRNT_RN_C2R
      INTEGER NNL
      INTEGER NNT
      INTEGER NPRTG
      INTEGER NPRTL
      INTEGER IPRTL
      INTEGER KCOL(NPRTL+1, NNT)
      INTEGER KROW(NPRTG+2, NNL)

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
!!      INCLUDE 'nmnbse.fc'

D     INTEGER I_MASTER
D     PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IDUM
      INTEGER I_PROC, I_RANK, I_SIZE, I_ERROR, I_COMM
      INTEGER IN, ID, IL, I
      INTEGER IGLB, IPRC
      INTEGER NPROC_MAX
      PARAMETER (NPROC_MAX = 256)
      INTEGER KTMP(NPROC_MAX)
      INTEGER KRECV(NPROC_MAX), KDSPL(NPROC_MAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GT. 0)
D     CALL ERR_PRE(NPRTG .GT. 0)
D     CALL ERR_PRE(NPRTL .GT. 0)
D     CALL ERR_PRE(IPRTL .GT. 0)
D     CALL ERR_PRE(NPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL+NPRTL-1 .LE. NPRTG)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     CONTROLE
      IF (NPRTG+2 .GT. NPROC_MAX) GOTO 9900

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      I_PROC = I_RANK + 1

C---     SI ON A UN SEUL PROCESS
      IF (I_SIZE .GT. 1) GOTO 1000
D        CALL ERR_ASR(NNL   .EQ. NNT)
D        CALL ERR_ASR(NPRTL .EQ. NPRTG)
         CALL ICOPY(NNT, KCOL(1,1), NPRTL+1, KROW(1,1), NPRTG+2)
         DO IN=1,NNT                            ! NNO global
            KROW(2,IN) = IN
         ENDDO
         CALL IINIT(NNT, 1, KROW(3,1), NPRTG+2) ! IPROC
      GOTO 1999

C---     ECRIS LA SEQUENCE
1000  CONTINUE

C---     INDICES
      IGLB = NPRTG + 1     ! NNO global
      IPRC = NPRTG + 2     ! No du process propriétaire

C---     BROADCAST NNT COMME CONTROLE QUE TOUS ONT LE MÊME
D     IF (ERR_GOOD()) THEN
D        IF (I_RANK .EQ. I_MASTER) IDUM = NNT
D        CALL MPI_BCAST(IDUM, 1, MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
D        CALL ERR_ASR(IDUM .EQ. NNT)
D     ENDIF

C---     STRUCTURES POUR LE GATHERV
      IF (ERR_GOOD()) CALL MPI_ALLGATHER(NPRTL, 1, MP_TYPE_INT(),
     &                                   KRECV, 1, MP_TYPE_INT(),
     &                                   I_COMM, I_ERROR)
      IF (ERR_GOOD()) CALL MPI_ALLGATHER(IPRTL-1, 1, MP_TYPE_INT(),
     &                                   KDSPL, 1, MP_TYPE_INT(),
     &                                   I_COMM, I_ERROR)

C---     BOUCLE SUR LES NOEUDS GLOBAUX
      IL = 0
      DO IN=1,NNT
         IF (ERR_BAD()) GOTO 399

C---        GATHER UNE LIGNE
         CALL MPI_ALLGATHERV(KCOL(1,IN), NPRTL, MP_TYPE_INT(),
     &                       KTMP, KRECV, KDSPL, MP_TYPE_INT(),
     &                       I_COMM, I_ERROR)

C---        UN NOEUD LOCAL
         IF (KTMP(I_PROC) .GT. 0) THEN
            IL = IL + 1
D           CALL ERR_ASR(IL .LE. NNL)
            CALL ICOPY(NPRTG, KTMP, 1, KROW(1,IL), 1)
            KROW(IGLB,IL) = IN
            KROW(IPRC,IL) = KCOL(NPRTL+1, IN)
         ENDIF

      ENDDO
399   CONTINUE

1999  CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_FRNT_RN_C2R = ERR_TYP()
      RETURN
      END
