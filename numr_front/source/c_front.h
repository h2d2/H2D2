//************************************************************************
// --- Copyright (c) INRS 2007-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Renumérotation d'éléments par méthode frontale.
//
// Description:
//    Le fichier c_front.h
//
// Notes:
//
//************************************************************************
#ifndef C_FRONT_H_DEJA_INCLU
#define C_FRONT_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif

#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_FRONT_RENUM  C_FRONT_RENUM
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_FRONT_RENUM  c_front_renum_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_FRONT_RENUM  c_front_renum__
#endif

F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FRONT_RENUM (fint_t*, fint_t*, fint_t*, fint_t*);

#ifdef __cplusplus
}
#endif

#endif   // C_FRONT_H_DEJA_INCLU
