//************************************************************************
// --- Copyright (c) INRS 2007-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//*****************************************************************************
// Fichier: $Id$
// Classe :
//*****************************************************************************
#include "c_front.h"

#include <algorithm>

//************************************************************************
// Sommaire:    Functor de comparaison pour 2 éléments
//
// Description:
//      La classe privée <code>Compare</code> est un functor qui implante
//      l’opérateur <code>operator< </code> de comparaison entre deux éléments.
//      Les éléments sont comparés sur la base de leur connectivités triées.
//
// Notes:
//************************************************************************
class CompareProxy;
class Compare
{
public:
   friend class CompareProxy;

   Compare(fint_t* _kng, fint_t* _kmn, fint_t _nnel, fint_t _nelt)
      : kne1(NULL), kne2(NULL), kng(_kng), kmn(_kmn), nnel(_nnel), nelt(_nelt)
   {
      kne1 = new fint_t[nnel];
      kne2 = new fint_t[nnel];
   }
   ~Compare()
   {
      delete[] kne1; kne1 = NULL;
      delete[] kne2; kne2 = NULL;
   }

private:
   Compare(const Compare&);

   fint_t* kne1;
   fint_t* kne2;
   fint_t* kng;
   fint_t* kmn;
   fint_t  nnel;
   fint_t  nelt;
};

class CompareProxy
{
public:
         CompareProxy(const Compare* _bse) : base(_bse)  { }
         CompareProxy(const CompareProxy& other) : base(other.base) { }
        ~CompareProxy() {}

   // --- Trie par insertion de kne vers kds
   void insertion_sort(fint_t* kne, fint_t* kns, fint_t nnel)
   {
      kns[0] = kne[0];
      for (fint_t i = 1; i < nnel; ++i)
      {
         fint_t key = kne[i];
         fint_t j = i-1;
         while (j >= 0 && kns[j] > key)
         {
            kns[j+1] = kns[j];
            --j;
         }
         kns[j+1] = key;
      }
   }

   // ---  Implantation de la comparaison <
   // ---  Compare deux éléments sur la base des connectivités triées
   bool operator() (fint_t ie1, fint_t ie2)
   {
      fint_t* kmn  = base->kmn;
      fint_t* kng  = base->kng;

      if (kmn[ie1] != kmn[ie2])
         return (kmn[ie1] < kmn[ie2]);

      fint_t* kne1 = base->kne1;
      fint_t* kne2 = base->kne2;
      fint_t  nnel = base->nnel;

      insertion_sort(kng + ie1*nnel, kne1, nnel);
      insertion_sort(kng + ie2*nnel, kne2, nnel);

      for (fint_t i = 1; i < nnel; ++i)
         if (kne1[i] != kne2[i])
            return (kne1[i] < kne2[i]);

      return false;
   }

private:
   const Compare* base;
};

//************************************************************************
// Sommaire:
//
// Description:
//      La fonction publique C_FRONT_RENUM renumérote les éléments
//      suivant la numérotation des noeuds.
//
// Entrée:
//      fint_t* nnel;       Nombre de noeuds par élément
//      fint_t* nelt;       Nombre d'éléments
//      fint_t* kng;        Table des connectivités
//
// Sortie:
//      fint_t* kdis;       Table des redistribution des éléments
//
// Notes:
//************************************************************************
fint_t F2C_CONF_CNV_APPEL C_FRONT_RENUM(fint_t* kdis,
                                        fint_t* nnel,
                                        fint_t* nelt,
                                        fint_t* kng)
{
   // --- Table auxiliaire du noeuds min de chaque élément
   //     pour accélérer les comparaisons
   fint_t* kmin = new fint_t[*nelt];
   for (fint_t i = 0, *k = kng; i < (*nelt); ++i)
   {
      kmin[i] = *std::min_element(k, k+*nnel);
      k += *nnel;
   }

   // ---  Initialise kdis en base 0
   for (fint_t i = 0; i < (*nelt); ++i) kdis[i] = i;

   // ---  Trie les éléments par noeud min
   Compare      sngl(kng, kmin, *nnel, *nelt);
   CompareProxy comp(&sngl);
   std::sort(kdis, kdis+(*nelt), comp);

   // ---  Passe en base 1
   for (fint_t i = 0; i < (*nelt); ++i) ++kdis[i];

   // ---  Nettoie
   delete [] kmin;

   return 0;
}

/*
int main()
{
   fint_t nelt = 10;
   fint_t nnel =  6;
   fint_t* kng  = new fint_t[nelt*nnel];
   fint_t* kdis = new fint_t[nelt];

   fint_t* k = kng;
   for (fint_t i = 0; i < nelt; ++i)
   {
      for (fint_t j = 0; j < nnel; ++j)
      {
         *k++ = (i+1)*nelt - j;
      }
   }
   C_FRONT_RENUM(kdis, &nnel, &nelt, kng);

   return 0;
}
*/
