//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Fake DLL/SO
// Sommaire: Fonctions C de "Fake DLL/SO"
//************************************************************************
#include "c_fkso.h"

extern "C"
{
    void fake_dlls();
}

//************************************************************************
// Sommaire:   Initialise le système de pseudo DLL.
//
// Description:
//    La fonction C_FKSO_INI(...) initialise le système de pseudo DLL
//    pour une compilation sans DLL/SO, sous forme d'un executable
//    monolithique.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FKSO_INI()
{

#ifdef FAKE_DLL
   fake_dlls();
#endif

   return 0;
}

