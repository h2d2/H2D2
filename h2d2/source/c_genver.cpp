//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Fake DLL/SO
// Sommaire: Fonctions C pour générer le numéro de version
//************************************************************************
#include "c_genver.h"

#include <algorithm>
#include <sstream>
#include <string.h>

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction C_FKSO_INI(...) initialise le système de pseudo DLL
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_GEN_VERSION (fint_t* add_date,
                                                             F2C_CONF_STRING  vmajP,
                                                             F2C_CONF_STRING  vminP,
                                                             F2C_CONF_STRING  vrevP,
                                                             F2C_CONF_STRING  vtotP
                                                             F2C_CONF_SUP_INT lmaj
                                                             F2C_CONF_SUP_INT lmin
                                                             F2C_CONF_SUP_INT lrev
                                                             F2C_CONF_SUP_INT ltot)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_GEN_VERSION (fint_t* add_date,
                                                             F2C_CONF_STRING  vmaj,
                                                             F2C_CONF_STRING  vmin,
                                                             F2C_CONF_STRING  vrev,
                                                             F2C_CONF_STRING  vtot)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
#else
   char* vmajP  = vmaj->strP;
   int   lmaj   = vmaj->len;
   char* vminP  = vmin->strP;
   int   lmin   = vmin->len;
   char* vrevP  = vrev->strP;
   int   lrev   = vrev->len;
   char* vtotP  = vtot->strP;
   int   ltot   = vtot->len;
#endif

   std::ostringstream os;
   if (lmaj > 0) os << std::string(vmajP, lmaj);
   if (lmin > 0) os << "." << std::string(vminP, lmin);
   if (lrev > 0) os << "." << std::string(vrevP, lrev);
   if (*add_date != 0) os << " (" << __DATE__ << ")";
   os << std::ends;

   const std::string s = os.str();
   memset(vtotP, ' ', ltot);
   memcpy(vtotP, s.c_str(), std::min(static_cast<int>(s.size()-1),static_cast<int>(ltot)));

   return 0;
}

