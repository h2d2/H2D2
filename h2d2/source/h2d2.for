C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:
C Objet:   Programme principal
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Main, point d'entrée.
C
C Description:
C     La fonction H2D2_MAIN est le point d'entrée. C'est ici que commence
C     l'exécution de H2D2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      PROGRAM H2D2_MAIN

      IMPLICIT NONE

      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'chkrst.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trchrn.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR, IRET
      INTEGER HCMD
      LOGICAL TIMEROK
      LOGICAL ENERREUR
      LOGICAL ESTMAITRE
      CHARACTER*(256) NOMCMD, RETCMD
C------------------------------------------------------------------------

      CALL ERR_INI()
      ENERREUR = .FALSE.

C---     INITIALISE MPI
      ESTMAITRE = .TRUE.
      IF (ERR_GOOD()) IERR = H2D2_MPI_000()
      IF (ERR_GOOD()) ESTMAITRE = MP_UTIL_ESTMAITRE()

C---     FICHIER DES COMMANDES PASSÉES SUR LA LIGNE DE COMMANDE
      NOMCMD = ' '
      IF (ERR_GOOD()) THEN
         IRET = C_OS_FICTMP(NOMCMD)
         IF (IRET .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_CREE_FICTMP')
      ENDIF

C---     PARAMETRES DE LA LIGNE DE COMMANDE
      IF (ERR_GOOD()) IERR = H2D2_REQPRM(NOMCMD(1:SP_STRN_LEN(NOMCMD)))

C---     DEBUG MPI
      IF (ERR_GOOD()) IERR = H2D2_MPI_DBG()

C---     CONFIGURE LE LOG
      IF (ERR_GOOD()) IERR = LOG_ASGNIV(H2D2_NIVLOG)
      IF (ERR_GOOD()) IERR = LOG_ASGFIC(H2D2_FOUT)
      IF (ERR_GOOD()) IERR = LOG_AJTZONE('h2d2', H2D2_NIVLOG)

C---     CONFIGURE LE TIMER
      TIMEROK = .FALSE.
      IF (ERR_GOOD()) THEN
         IERR = TR_CHRN_AJTZONE('h2d2')
         IF (ERR_GOOD()) TIMEROK = .TRUE.
      ENDIF

C---     ECRIS L'ENTETE
      IF (ERR_GOOD()) IERR = H2D2_ENTETE()

C---     INITIALISE LE SYSTEME DE CLASSES
      IF (ERR_GOOD()) IERR = H2D2_000()

C---     CONFIGURE LE SYSTEME (LANGUE, MODULES, ...)
      IF (ERR_GOOD()) IERR = H2D2_CONFIG()

C---     INITIALISE L'INTERPRETEUR DE COMMANDES
      HCMD = 0
      IF (ERR_GOOD()) IERR = IC_ICMD_CTR(HCMD)
      IF (ERR_GOOD()) IERR = IC_ICMD_INI(HCMD)
      IF (ERR_GOOD()) IERR = IC_EXPR_ASGCTX(HCMD)

C---     ENREGISTER LES COMMANDES
      IF (ERR_GOOD()) IERR = H2D2_INICMD(HCMD)

C---     RESTART
      IF (ERR_GOOD() .AND. H2D2_DORST) THEN
         IERR = PKL_DORSTRT(H2D2_FRST(1:SP_STRN_LEN(H2D2_FRST)))
      ENDIF

C---     TRAITE LES COMMANDES
      IF (ERR_GOOD()) THEN
         CALL TR_CHRN_START('h2d2')
         IF (ERR_GOOD() .AND. .NOT. H2D2_DORST)
     &      IERR = IC_ICMD_XEQ(HCMD, H2D2_DOEXE,
     &                     NOMCMD(1:SP_STRN_LEN(NOMCMD)), RETCMD)
         IF (ERR_GOOD())
     &      IERR = IC_ICMD_XEQ(HCMD, H2D2_DOEXE,
     &                     H2D2_FINP(1:SP_STRN_LEN(H2D2_FINP)), RETCMD)
         CALL TR_CHRN_STOP('h2d2')
      ENDIF

C---     ECRIS LE MESSAGE D'ERREUR
      IF (ERR_BAD() .AND. .NOT. ERR_ESTMSG('ERR_CMD_STOP')) THEN
         CALL LOG_MSG_ERR()
         ENERREUR = .TRUE.
      ENDIF
      CALL ERR_RESET()

C---     ECRIS LE PIED DE PAGE
      IF (ERR_GOOD()) IERR = H2D2_PIED(TIMEROK)

C---     DETRUIS L'INTERPRETEUR ET TERMINE LE SYSTÈME DE CLASSES
      IF (IC_ICMD_HVALIDE(HCMD)) IERR = IC_ICMD_DTR(HCMD)
      IF (ERR_GOOD()) IERR = H2D2_999()

C---     EFFACE LE FICHIER DES COMMANDES
      IF (SP_STRN_LEN(NOMCMD) .GT. 0)
     &   IRET = C_OS_DELFIC(NOMCMD(1:SP_STRN_LEN(NOMCMD)))

C---     GÈRE LES ERREURS
      IF (ERR_BAD()) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_MSG_ERR()
      ELSE
         CALL LOG_ECRIS(' ')
         IF (ENERREUR) THEN
            CALL LOG_ECRIS('MSG_FIN_ERREUR')
         ELSE
            CALL LOG_ECRIS('MSG_FIN_SUCCES')
         ENDIF
      ENDIF

C---     TERMINE MPI
      IERR = H2D2_MPI_999()

C---     Exit code
      IF (ESTMAITRE) THEN
         IF (ENERREUR) THEN
            STOP 1
         ELSE
            STOP 0
         ENDIF

      ENDIF
      END

C************************************************************************
C Sommaire: Initialisation de base des structures du système
C
C Description:
C     La fonction privée H2D2_000() initialise le système. Il s'agit
C     ici de l'initialisation de très bas niveau pour préparer
C     les structures.
C     <p>
C     On n'initialise ici que les structures internes. Les modules externes
C     sont initialisés après l'enregistrement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_000()

      IMPLICIT NONE

      INCLUDE 'c_fp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR, IRET
C------------------------------------------------------------------------

C---     INITIALISE LE FPU
      IF (ERR_GOOD()) IRET = C_FP_INIT(H2D2_XPREC)

C---     PUIS L'ALLOCATION DE MEMOIRE
      IF (ERR_GOOD()) IERR = SO_ALLC_INI()

C---     PUIS LES MODULES EXTERNES
      IF (ERR_GOOD()) IERR = SO_MDUL_000()
      IF (ERR_GOOD()) IERR = SO_FUNC_000()
      IF (ERR_GOOD()) IERR = SO_VTBL_000()

      H2D2_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset de base des structures du système
C
C Description:
C     La fonction privée H2D2_999() reset le système. Il s'agit ici du
C     reset de très bas niveau pour libérer les structures.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_999()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR
C------------------------------------------------------------------------

C---     D'ABORD LES MODULES EXTERNES
      IF (ERR_GOOD()) IERR = SO_UTIL_MDL999()

C---     PUIS LES MODULES INTERNES
      IF (ERR_GOOD()) IERR = SO_MDUL_999()

C---     À LA FIN, L'ALLOCATION DE MÉMOIRE
      IERR = SO_ALLC_RST()

      H2D2_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Configure le système
C
C Description:
C     La fonction privée H2D2_CONFIG configure le système. Les opérations
C     suivantes sont effectuées:
C        - Chargement des fichiers de langue
C        - Enregistrement des modules
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_CONFIG()

      IMPLICIT NONE

      INCLUDE 'c_os.fi'
      INCLUDE 'c_fkso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'trd.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR, IRET
      CHARACTER*(256) REPEXE
      CHARACTER*(256) NOMREP
      CHARACTER*(256) NOMTMP
      CHARACTER*(8)   LCLE
      INTEGER, PARAMETER :: IRCR = 0   ! Non récursif
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     PSEUDO-DLL
      IF (IRET .EQ. 0) IRET = C_FKSO_INI()

C---     RÉCUPÈRE LE REPERTOIRE
      IF (IRET .EQ. 0) IRET = C_OS_REPEXE(REPEXE)
      IF (IRET .NE. 0) THEN
         WRITE(ERR_BUF, '(A)') 'ERR_REQ_DIR_EXE'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

C---     NOM DE FICHIER TEMPORAIRE
      NOMTMP = ' '
      IF (ERR_GOOD()) THEN
         IRET = C_OS_FICTMP(NOMTMP)
         IF (IRET .NE. 0) CALL ERR_ASG(ERR_ERR, 'ERR_CREE_FICTMP')
      ENDIF

C---     AJOUTE LES FICHIERS D'ERREUR
      IF (ERR_GOOD()) THEN
         IRET = C_OS_REQVARENV('H2D2_LOCALE', LCLE)
         IF (IRET .NE. 0) LCLE = 'fr'
      ENDIF
      IF (ERR_GOOD()) THEN
         NOMREP = REPEXE(1:SP_STRN_LEN(REPEXE)) //
     &            '/../locale/' //
     &            LCLE(1:SP_STRN_LEN(LCLE))
         IRET = C_OS_NORMPATH(NOMREP)
         IRET = C_OS_DIR(NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                   '*.err.stbl',
     &                   NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &                   IRCR)
         IF (IRET .NE. 0) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_LIST_DIR')
            WRITE(ERR_BUF, '(3A)') NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                             '/', '*.err.stbl'
            CALL ERR_AJT(ERR_BUF)
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = TRD_ASGFIC('@' // NOMTMP(1:SP_STRN_LEN(NOMTMP)))
      ENDIF
      IF (ERR_BAD() .AND. (SP_STRN_LEN(NOMTMP) .GT. 0)) THEN
         CALL LOG_MSG_ERR()
         CALL ERR_RESET()
      ENDIF

C---     AJOUTE LES FICHIERS DE MESSAGES
      IF (ERR_GOOD()) THEN
         IRET = C_OS_REQVARENV('H2D2_LOCALE', LCLE)
         IF (IRET .NE. 0) LCLE = 'fr'
      ENDIF
      IF (ERR_GOOD()) THEN
         NOMREP = REPEXE(1:SP_STRN_LEN(REPEXE)) //
     &            '/../locale/' //
     &            LCLE(1:SP_STRN_LEN(LCLE))
         IRET = C_OS_NORMPATH(NOMREP)
         IRET = C_OS_DIR(NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                   '*.msg.stbl',
     &                   NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &                   IRCR)
         IF (IRET .NE. 0) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_LIST_DIR')
            WRITE(ERR_BUF, '(3A)') NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                             '/', '*.msg.stbl'
            CALL ERR_AJT(ERR_BUF)
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = TRD_ASGFIC('@' // NOMTMP(1:SP_STRN_LEN(NOMTMP)))
      ENDIF
      IF (ERR_BAD() .AND. (SP_STRN_LEN(NOMTMP) .GT. 0)) THEN
         CALL LOG_MSG_ERR()
         CALL ERR_RESET()
      ENDIF

C---     AJOUTE LES MODULES
      IF (ERR_GOOD()) THEN
         NOMREP = REPEXE(1:SP_STRN_LEN(REPEXE)) // '/../etc'
         IRET = C_OS_NORMPATH(NOMREP)
         IRET = C_OS_DIR(NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                   '*.h2d2-cfg',
     &                   NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &                   IRCR)
         IF (IRET .NE. 0) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_LIST_DIR')
            WRITE(ERR_BUF, '(3A)') NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                             '/', '*.h2d2-cfg'
            CALL ERR_AJT(ERR_BUF)
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = SO_UTIL_MDLCTR(NOMREP(1:SP_STRN_LEN(NOMREP)),
     &                         '@' // NOMTMP(1:SP_STRN_LEN(NOMTMP)))
      ENDIF
      IF (ERR_GOOD()) IERR = SO_UTIL_MDL000()

C---     DÉTRUIS LE FICHIER TEMPORAIRE MEME EN CAS D'ERREUR
      IF (SP_STRN_LEN(NOMTMP) .GT. 0)
     &   IRET = C_OS_DELFIC(NOMTMP(1:SP_STRN_LEN(NOMTMP)))

C---     AFFICHE LES VERSIONS SI DEMANDÉ
      IF (ERR_GOOD() .AND. H2D2_PRNVER) THEN
         IERR = SO_UTIL_MDLVER()
         CALL LOG_ECRIS('--------------------' //
     &                  '--------------------' //
     &                  '--------------------')
         CALL LOG_ECRIS(' ')
      ENDIF

      H2D2_CONFIG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris l'entête
C
C Description:
C     La fonction privée H2D2_ENTETE écris l'entête dans le fichier de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_ENTETE()

      IMPLICIT NONE

      INCLUDE 'c_dh.fi'
      INCLUDE 'c_genver.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'h2d2.fc'

      REAL*8  TACTU
      INTEGER IERR, IRET
      INTEGER I, IP
      INTEGER I_SIZE, I_RANK, I_ERROR
      INTEGER NUM_THREADS
      CHARACTER*(128) TXTBUF, TRVBUF

!$    INTEGER OMP_GET_MAX_THREADS
C------------------------------------------------------------------------

      TXTBUF = ' '
      IERR = C_GEN_VERSION(H2D2_VERDTE,
     &                     H2D2_VERMAJ(1:SP_STRN_LEN(H2D2_VERMAJ)),
     &                     H2D2_VERMIN(1:SP_STRN_LEN(H2D2_VERMIN)),
     &                     H2D2_VERREV(1:SP_STRN_LEN(H2D2_VERREV)),
     &                     TXTBUF)

      WRITE (LOG_BUF, '(8A,I8)')
     &     H2D2_NOMPRGM(1:SP_STRN_LEN(H2D2_NOMPRGM)),'   ',
     &     'Version ', TXTBUF(1:SP_STRN_LEN(TXTBUF))
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_ECRIS('Copyright (C) 2003-2018 INRS')
      CALL LOG_ECRIS('----------------------------')
      CALL LOG_ECRIS(' ')

      WRITE (LOG_BUF, '(A)') 'Configuration:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IRET = C_OS_PLATFORM(TXTBUF)
      I = 0
100   CONTINUE
         I = I + 1
         IRET = SP_STRN_TKS(TXTBUF, ';', I, TRVBUF)
         IF (IRET .EQ. 0) THEN
            IP = INDEX(TRVBUF, ':')
            IF (IP .LT. 1 .OR. IP .GT. SP_STRN_LEN(TRVBUF)) THEN
               LOG_BUF = TRVBUF(1:SP_STRN_LEN(TRVBUF))
            ELSE
               LOG_BUF = TRVBUF(1:IP-1) //
     &                  '#<25>#' //
     &                  TRVBUF(IP:SP_STRN_LEN(TRVBUF))
            ENDIF
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      IF (IRET .EQ. 0) GOTO 100

      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF, '(A,I6)') 'MPI size#<25>#:', I_SIZE
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (ERR_GOOD() .AND. LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
         IF (ERR_GOOD()) THEN
            WRITE (LOG_BUF, '(A,I6)') 'MPI rank#<25>#:', I_RANK
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         IF (ERR_GOOD()) THEN
            IRET = C_OS_HOSTNAME(TXTBUF)
            WRITE (LOG_BUF, '(3A)') 'MPI host#<25>#:', ' ',
     &                              TXTBUF(1:SP_STRN_LEN(TXTBUF))
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDIF

      IF (ERR_GOOD()) THEN
         NUM_THREADS = -1
!$       NUM_THREADS = OMP_GET_MAX_THREADS()
         IF (NUM_THREADS .EQ. -1) THEN
!$          CALL ERR_ASR(.FALSE.)
            WRITE (LOG_BUF, '(A)')
     &         'Number of OMP threads#<25>#: OMP deactivated'
         ELSE
            WRITE (LOG_BUF, '(A,I6)')
     &         'Number of OMP threads#<25>#:', NUM_THREADS
         ENDIF
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      CALL LOG_DECIND()
      CALL LOG_ECRIS(' ')

      TACTU = -1.0D0
      IF (ERR_GOOD()) THEN
         IRET = C_DH_TIMLCL(TACTU)
         IRET = C_DH_ECRLCL(TACTU, TXTBUF)
         WRITE (LOG_BUF, '(2A)')
     &      'Start#<12>#: ', TXTBUF(1:SP_STRN_LEN(TXTBUF))
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_ECRIS(' ')
      ENDIF

C---     Conserve le temps de départ
      H2D2_TSTART = TACTU

      H2D2_ENTETE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les commandes
C
C Description:
C     La fonction privée H2D2_INICMD enregistre auprès de l'interpréteur de
C     commande toutes les commandes du système. On itère sur les modules
C     pour enregistrer tour à tour les commandes et les objets.
C
C Entrée:
C     HCMD     Handle de l'interpréteur de commande
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_INICMD(HCMD)

      IMPLICIT NONE

      INTEGER HCMD

      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'h2d2.fc'

      INTEGER  IERR

      EXTERNAL H2D2_INICMD_CBMDL
      EXTERNAL H2D2_INICMD_CBCLS
      EXTERNAL H2D2_INICMD_CBFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCMD))
C------------------------------------------------------------------------

C---     Boucle sur les modules pour les enregistrer
      IF (ERR_GOOD()) THEN
         IERR=SO_UTIL_MDLITR(SO_MDUL_TYP_CMDMDL,H2D2_INICMD_CBMDL,HCMD)
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR=SO_UTIL_MDLITR(SO_MDUL_TYP_CMDOBJ,H2D2_INICMD_CBCLS,HCMD)
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR=SO_UTIL_MDLITR(SO_MDUL_TYP_CMDFNC,H2D2_INICMD_CBFNC,HCMD)
      ENDIF

      H2D2_INICMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une commande à l'interface de commande
C
C Description:
C     La fonction privée H2D2_INICMD_CBFNC est la fonction call-back appelée
C     par un module pour enregistrer ses commandes de l'IC.
C
C Entrée:
C     HCMD     Handle sur l'interpréteur de commande
C     HMDL     Handle sur le module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_INICMD_CBFNC(HCMD, HMDL)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HCMD
      INTEGER HMDL

      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR
      INTEGER HFNOM
      INTEGER HFCMD
      EXTERNAL H2D2_INICMD_CBNOM

      INTEGER        MDL_HDL
      CHARACTER*(32) MDL_NOM
      COMMON /H2D2_ICMD_CMN/ MDL_HDL, MDL_NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCMD))
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HMDL))
C------------------------------------------------------------------------

C---     Charge les deux fonctions
      HFNOM = 0
      HFCMD = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQCMD', HFNOM)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'CMD',    HFCMD)

C---     CB pour résoudre le nom
      MDL_NOM = ' '
      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFNOM, H2D2_INICMD_CBNOM)
!!      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFHDL, H2D2_CBHDL)

C---     Enregistre la fonction
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDFNC(HCMD, MDL_NOM, HFCMD)

C---     Décharge la fonction REQNOM()
      IF (SO_FUNC_HVALIDE(HFNOM)) IERR = SO_FUNC_DTR(HFNOM)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_INITIALISATION_MODULE',': ',
     &                        SO_MDUL_REQNOM(HMDL)
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      H2D2_INICMD_CBFNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une classe à l'interface de commande
C
C Description:
C     La fonction privée H2D2_INICMD_CBCLS est la fonction call-back appelée
C     par un module pour enregistrer ses classes de l'IC.
C
C Entrée:
C     HCMD     Handle sur l'interpréteur de commande
C     HMDL     Handle sur le module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_INICMD_CBCLS(HCMD, HMDL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HCMD
      INTEGER HMDL

      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR
      INTEGER HFNOM, HFHDL, HFCTR, HFMTH, HFDOT
      EXTERNAL H2D2_INICMD_CBHDL
      EXTERNAL H2D2_INICMD_CBNOM

      INTEGER        MDL_HDL
      CHARACTER*(64) MDL_NOM
      COMMON /H2D2_ICMD_CMN/ MDL_HDL, MDL_NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCMD))
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HMDL))
C------------------------------------------------------------------------

C---     Charge les fonctions
      HFNOM = 0
      HFHDL = 0
      HFCTR = 0
      HFMTH = 0
      HFDOT = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQCLS', HFNOM)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQHDL', HFHDL)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'XEQCTR', HFCTR)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'XEQMTH', HFMTH)
      IF (ERR_GOOD()) THEN    ! Fonction optionnelle
         IERR = SO_MDUL_REQFNC(HMDL, 'OPBDOT', HFDOT)
         IF (ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) CALL ERR_RESET()
      ENDIF

C---     CB pour résoudre le nom et le handle
      MDL_NOM = ' '
      MDL_HDL = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFNOM, H2D2_INICMD_CBNOM)
      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFHDL, H2D2_INICMD_CBHDL)

C---     Enregistre la classe
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDCLS(HCMD,
     &                                      MDL_NOM,
     &                                      MDL_HDL,
     &                                      HFCTR,
     &                                      HFMTH,
     &                                      HFDOT)

C---     Décharge les fonctions REQCLS() et REQHDL()
      IF (SO_FUNC_HVALIDE(HFNOM)) IERR = SO_FUNC_DTR(HFNOM)
      IF (SO_FUNC_HVALIDE(HFHDL)) IERR = SO_FUNC_DTR(HFHDL)

C---     Complète l'erreur au besoin
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(3A)') 'ERR_INITIALISATION_MODULE',': ',
     &                           SO_MDUL_REQNOM(HMDL)
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      H2D2_INICMD_CBCLS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un module à l'interface de commande
C
C Description:
C     La fonction privée H2D2_INICMD_CBMDL est la fonction call-back appelée
C     par un module pour enregistrer ses modules de l'IC.
C
C Entrée:
C     HCMD     Handle sur l'interpréteur de commande
C     HMDL     Handle sur le module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_INICMD_CBMDL(HCMD, HMDL)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HCMD
      INTEGER HMDL

      INCLUDE 'err.fi'
      INCLUDE 'icicmd.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR
      INTEGER HFNOM
      INTEGER HFHDL
      INTEGER HFOPB
      EXTERNAL H2D2_INICMD_CBHDL
      EXTERNAL H2D2_INICMD_CBNOM

      INTEGER        MDL_HDL
      CHARACTER*(32) MDL_NOM
      COMMON /H2D2_ICMD_CMN/ MDL_HDL, MDL_NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(IC_ICMD_HVALIDE(HCMD))
D     CALL ERR_PRE(SO_MDUL_HVALIDE(HMDL))
C------------------------------------------------------------------------

C---     Charge les fonctions
      HFNOM = 0
      HFHDL = 0
      HFOPB = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNOM', HFNOM)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQHDL', HFHDL)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'OPBDOT', HFOPB)

C---     CB pour résoudre le nom et le handle
      MDL_NOM = ' '
      MDL_HDL = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFNOM, H2D2_INICMD_CBNOM)
      IF (ERR_GOOD()) IERR = SO_FUNC_CBFNC0(HFHDL, H2D2_INICMD_CBHDL)

C---     Enregistre le module
      IF (ERR_GOOD()) IERR = IC_ICMD_ADDMDL(HCMD,
     &                                      MDL_NOM,
     &                                      MDL_HDL,
     &                                      HFOPB)

C---     Décharge les fonctions REQCLS() et REQHDL()
      IF (SO_FUNC_HVALIDE(HFNOM)) IERR = SO_FUNC_DTR(HFNOM)
      IF (SO_FUNC_HVALIDE(HFHDL)) IERR = SO_FUNC_DTR(HFHDL)

C---     Complète l'erreur au besoin
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(3A)') 'ERR_INITIALISATION_MODULE',': ',
     &                           SO_MDUL_REQNOM(HMDL)
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      H2D2_INICMD_CBMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CB pour résoudre le nom de la commande, classe ...
C
C Description:
C     La fonction privée H2D2_INICMD_CBNOM est la fonction call-back appelée
C     pour résoudre le nom de la commande à ajouter.
C
C Entrée:
C     FNOM     Fonction retournant le nom de la commande
C
C Sortie:
C
C Notes:
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C************************************************************************
      FUNCTION H2D2_INICMD_CBNOM(FNOM)

      IMPLICIT NONE

      CHARACTER*(32) FNOM
      EXTERNAL       FNOM

      INCLUDE 'err.fi'
      INCLUDE 'h2d2.fc'

      INTEGER        MDL_HDL
      CHARACTER*(32) MDL_NOM
      COMMON /H2D2_ICMD_CMN/ MDL_HDL, MDL_NOM
C------------------------------------------------------------------------

      MDL_NOM = FNOM()

      H2D2_INICMD_CBNOM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: CB pour résoudre le nom de la commande, classe ...
C
C Description:
C     La fonction privée H2D2_INICMD_CBHDL est la fonction call-back appelée
C     pour résoudre le nom de la commande à ajouter.
C
C Entrée:
C     FNOM     Fonction retournant le nom de la commande
C
C Sortie:
C
C Notes:
C     On passe les arguments au call-back par common, car ce sont
C     des chaînes et les info sur la dimension peut ajouter des
C     arguments cachés.
C************************************************************************
      FUNCTION H2D2_INICMD_CBHDL(FHDL)

      IMPLICIT NONE

      INTEGER  FHDL
      EXTERNAL FHDL

      INCLUDE 'err.fi'
      INCLUDE 'h2d2.fc'

      INTEGER        MDL_HDL
      CHARACTER*(32) MDL_NOM
      COMMON /H2D2_ICMD_CMN/ MDL_HDL, MDL_NOM
C------------------------------------------------------------------------

      MDL_HDL = FHDL()

      H2D2_INICMD_CBHDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris l'entête
C
C Description:
C     La fonction privée H2D2_PIED écris le pied dans le fichier de log.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_PIED(DOTIMER)

      IMPLICIT NONE

      LOGICAL DOTIMER

      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'trchrn.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'h2d2.fc'

      REAL*8  TACTU
      INTEGER IRET, IERR
      CHARACTER*64 TXTBUF
C------------------------------------------------------------------------

      IF (DOTIMER) THEN
         CALL LOG_ECRIS(' ')
         IERR = TR_CHRN_LOGZONE('h2d2')
      ENDIF

      CALL LOG_ECRIS(' ')
      IERR = SO_ALLC_LOG()

      CALL LOG_ECRIS(' ')
      IF (H2D2_TSTART .GT. 0.0D0) THEN
         IRET = C_DH_ECRLCL(H2D2_TSTART, TXTBUF)
         WRITE (LOG_BUF, '(2A)')
     &     'MSG_START#<12>#: ', TXTBUF(1:SP_STRN_LEN(TXTBUF))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      IRET = C_DH_TIMLCL(TACTU)
      IRET = C_DH_ECRLCL(TACTU, TXTBUF)
      WRITE (LOG_BUF, '(2A)')
     &     'MSG_END#<12>#: ', TXTBUF(1:SP_STRN_LEN(TXTBUF))
      CALL LOG_ECRIS(LOG_BUF)

      IF (H2D2_TSTART .GT. 0.0D0) THEN
         TACTU= TACTU - H2D2_TSTART
         IRET = C_DH_TMRSTR(TACTU, TXTBUF)
         WRITE (LOG_BUF, '(2A)')
     &     'MSG_ELPS#<12>#: ', TXTBUF(1:SP_STRN_LEN(TXTBUF))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      H2D2_PIED = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Paramètre de la ligne de commande
C
C Description:
C     La fonction privée H2D2_REQPRM() parse la ligne de commande pour
C     en extraire les valeurs et les commandes.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_REQPRM(NOMCMD)

      IMPLICIT NONE

      CHARACTER*(*)  NOMCMD

      INCLUDE 'f_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'h2d2.fc'

      INTEGER I_ERROR

      INTEGER IC
      INTEGER IERR, IRET
      INTEGER IDUM
      INTEGER INIV
      INTEGER NARG, LARG
      INTEGER MR
      LOGICAL ESTMAITRE
      CHARACTER*(256) ARG
      CHARACTER*(256) CMD
      CHARACTER*(256) VAL
C------------------------------------------------------------------------

C---     SEUL I_MASTER LIS LES ARGUMENTS
      ESTMAITRE = MP_UTIL_ESTMAITRE()
!!      IF (.NOT. ESTMAITRE) GOTO 9999

C---     OUVRE LE FICHIER DES COMMANDES
      MR = 0
      IF (ESTMAITRE) THEN
         MR = IO_UTIL_FREEUNIT()
         OPEN(UNIT  = MR,
     &        FILE  = NOMCMD,
     &        FORM  = 'FORMATTED',
     &        STATUS= 'UNKNOWN',
     &        ERR   = 9900)
      ENDIF

C---     NARGC
      NARG = F_OS_NARGLC()

C---     BOUCLE SUR LES ARGS
      IC = 0
200   CONTINUE
         IC = IC + 1
         IF (IC .GT. NARG) GOTO 299

         CALL F_OS_IARGLC(IC, ARG)

C---        EXTRAIT LA COMMANDE
         CMD = ARG
         IDUM = SP_STRN_LFT(CMD, '=')
         CALL SP_STRN_LCS(CMD)
         CALL SP_STRN_TRM(CMD)
C         IF (SP_STRN_LEN(CMD) .LE. 0) GOTO 9901

C---        EXTRAIT LA VALEUR
         VAL = ARG
         IDUM = SP_STRN_RHT(VAL, '=')
         CALL SP_STRN_TRM(VAL)

C---        SWITCH
         IF (CMD .EQ. '-h') THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_CMD_STOP')
            GOTO 9998
         ELSEIF (CMD .EQ. '-fi') THEN
            IF (SP_STRN_LEN(VAL) .LE. 0) GOTO 9901
            IF (SP_STRN_LEN(VAL) .GT. LEN(H2D2_FINP)) GOTO 9901
            H2D2_FINP = VAL(1:SP_STRN_LEN(VAL))
         ELSEIF (CMD .EQ. '-fo') THEN
            IF (SP_STRN_LEN(VAL) .LE. 0) GOTO 9901
            IF (SP_STRN_LEN(VAL) .GT. LEN(H2D2_FOUT)) GOTO 9901
            H2D2_FOUT = VAL(1:SP_STRN_LEN(VAL))
         ELSEIF (CMD .EQ. '-fc') THEN
            IF (SP_STRN_LEN(VAL) .LE. 0) GOTO 9901
            IF (SP_STRN_LEN(VAL) .GT. LEN(H2D2_FRST)) GOTO 9901
            H2D2_FRST = VAL(1:SP_STRN_LEN(VAL))
            H2D2_DORST = .TRUE.
         ELSEIF (CMD .EQ. '-mpdebug') THEN
            IF (SP_STRN_LEN(VAL) .GT. 0) GOTO 9901
            H2D2_MPIDBG = 1
         ELSEIF (CMD .EQ. '-syntax') THEN
            IF (SP_STRN_LEN(VAL) .GT. 0) GOTO 9901
            H2D2_DOEXE = .FALSE.
         ELSEIF (CMD .EQ. '-xprec') THEN
            IF (SP_STRN_LEN(VAL) .GT. 0) GOTO 9901
            H2D2_XPREC = 1
         ELSEIF (CMD .EQ. '-d') THEN
            IF (SP_STRN_LEN(VAL) .LE. 0) GOTO 9901
            IERR = SP_STRN_TKI(VAL, ',', 1, INIV)
            IF (ERR_BAD()) GOTO 9901
            H2D2_NIVLOG = INIV
         ELSEIF (CMD .EQ. '-v') THEN
            IF (SP_STRN_LEN(VAL) .GT. 0) GOTO 9901
            H2D2_PRNVER = .TRUE.
         ELSEIF (ARG .EQ. '-p4pg') THEN   ! Param ajoutés par mpirun
D           CALL ERR_ASR(IC .LT. NARG)
            IC = IC + 1
         ELSEIF (ARG .EQ. '-p4wd') THEN   ! sous Linux
D           CALL ERR_ASR(IC .LT. NARG)
            IC = IC + 1
         ELSE
            LARG = SP_STRN_LEN(ARG)
            IF     (ARG(1:1).EQ.'''' .AND. ARG(LARG:LARG).EQ.'''') THEN
               ARG = ARG(2:LARG-1)
            ELSEIF (ARG(1:1).EQ.'"'  .AND. ARG(LARG:LARG).EQ.'"')  THEN
               ARG = ARG(2:LARG-1)
            ENDIF
            IF (ESTMAITRE) THEN
               WRITE (MR, '(A)', ERR=9902) ARG(1:SP_STRN_LEN(ARG))
            ENDIF
         ENDIF

      GOTO 200
299   CONTINUE

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER_TEMPORAIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9998
9901  WRITE(ERR_BUF,'(2A,A,I2,2A)') 'ERR_LIGNE_CMD_ARG_INVALIDE', ' : ',
     &                       'arg(', ic, ') = ',ARG(1:SP_STRN_LEN(ARG))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9998
9902  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_FICHIER_TEMPORAIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9998

9998  CONTINUE
      CALL LOG_ECRIS('Usage: h2d2 [options] commands')
      CALL LOG_ECRIS('Options:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('-h            Write this message')
      CALL LOG_ECRIS('-v            Write versions')
      CALL LOG_ECRIS('-d=level      Verbosity level')
      CALL LOG_ECRIS('-mpdebug      Multi-Process debug mode')
      CALL LOG_ECRIS('-syntax       No execution, only syntax control')
      CALL LOG_ECRIS('-xprec        Extended precision')
      CALL LOG_ECRIS('-fi=filename  Input file name')
      CALL LOG_ECRIS('-fo=filename  Output file name')
      CALL LOG_ECRIS('-fc=path      Restart data folder')
      CALL LOG_DECIND()
      CALL LOG_ECRIS('Commands:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('White space separated commands will be submitted')
      CALL LOG_ECRIS('one per line, first things first, to the command')
      CALL LOG_ECRIS('interpreter.')
      CALL LOG_ECRIS('String shall be enclosed with single and double')
      CALL LOG_ECRIS('quotes, i.e. ''"string"'' or "''string"''.')
      CALL LOG_DECIND()

9999  CONTINUE
      IF (MR .NE. 0) CLOSE(MR)
      CALL MPI_BARRIER(MP_UTIL_REQCOMM(), I_ERROR)
      IERR = MP_UTIL_SYNCERR()

      H2D2_REQPRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Initialise MPI
C
C Description:
C     La fonction privée H2D2_MPI_000() initialise MPI.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_MPI_000()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'h2d2.fc'

      INTEGER IERR
      INTEGER I_COMM, I_HDLR, I_ERROR
      EXTERNAL MP_UTIL_ERRHNDLR
C------------------------------------------------------------------------

      I_ERROR = MPI_SUCCESS
      CALL MPI_INIT(I_ERROR)
      IF (I_ERROR .NE. MPI_SUCCESS) GOTO 9901

      I_COMM = MP_UTIL_REQCOMM()

C---     Generic MPI Error handler
      I_HDLR = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_CREATE_ERRHANDLER(MP_UTIL_ERRHNDLR,
     &                                   I_HDLR,
     &                                   I_ERROR)
         IERR = MP_UTIL_ASGERR(I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_SET_ERRHANDLER   (I_COMM,
     &                                   I_HDLR,
     &                                   I_ERROR)
         IERR = MP_UTIL_ASGERR(I_ERROR)
      ENDIF

C---     Default File Error handler
      I_HDLR = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_CREATE_ERRHANDLER(MP_UTIL_ERRHNDLR,
     &                                   I_HDLR,
     &                                   I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_SET_ERRHANDLER   (MPI_FILE_NULL,
     &                                   I_HDLR,
     &                                   I_ERROR)
C           Bug OpenMPI (sera réglé dans 1.5)
         IF (I_ERROR .EQ. MPI_ERR_ARG) CALL ERR_RESET()
      ENDIF

      IERR = MP_UTIL_SYNCERR()

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)') 'ERR_INITIALISATION_MPI'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      H2D2_MPI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Termine la connection à MPI
C
C Description:
C     La fonction privée H2D2_MPI_999() termine la connection à MPI
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_MPI_999()

      IMPLICIT NONE

      INTEGER H2D2_MPI_999

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
      INTEGER I_HDLR, I_COMM, I_ERR
      LOGICAL I_INIT
C------------------------------------------------------------------------

      CALL MPI_INITIALIZED(I_INIT, I_ERR)
      IF (ERR_BAD() .OR. .NOT. I_INIT) GOTO 9999

      I_COMM = MP_UTIL_REQCOMM()

C---     Reset le gestionnaire d'erreur des fichiers
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_GET_ERRHANDLER(MPI_FILE_NULL,
     &                                I_HDLR,
     &                                I_ERR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_SET_ERRHANDLER(MPI_FILE_NULL,
     &                                MPI_ERRORS_ARE_FATAL,
     &                                I_ERR)
      ENDIF
!!      OpenMPI sous MARMOT saute sur l'appel à  MPI_ERRHANDLER_FREE
!!      IF (ERR_GOOD()) THEN
!!         CALL MPI_ERRHANDLER_FREE    (I_HDLR,I_ERR)
!!      ENDIF

C---     Reset le gestionnaire d'erreur générique
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_GET_ERRHANDLER(I_COMM,
     &                                I_HDLR,
     &                                I_ERR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_SET_ERRHANDLER(I_COMM,
     &                                MPI_ERRORS_ARE_FATAL,
     &                                I_ERR)
      ENDIF
!!      OpenMPI sous MARMOT saute sur l'appel à  MPI_ERRHANDLER_FREE
!!      IF (ERR_GOOD()) THEN
!!         CALL MPI_ERRHANDLER_FREE    (I_HDLR,I_ERR)
!!      ENDIF

C---     Synchronise avant finalize
      IERR = MP_UTIL_SYNCERR()

      CALL MPI_FINALIZE(I_ERR)

9999  CONTINUE
      H2D2_MPI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Permet de debugger sous MPI.
C
C Description:
C     La fonction privée H2D2_MPI_DBG() bloque tant que la variable
C     I_CONTINUE n'est pas mise à .TRUE. Ceci permet de lancer un calcul
C     parallèle à partir d'une console, puis d'amener les process
C     dans un debugger et de les libérer.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION H2D2_MPI_DBG()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'h2d2.fc'

      INTEGER I_ERROR
      LOGICAL I_CONTINUE
C------------------------------------------------------------------------

C---     EN MODE DEBUG, ATTEND
      IF (H2D2_MPIDBG .NE. 0 .AND. MP_UTIL_ESTMAITRE()) THEN
         I_CONTINUE = .FALSE.
100      CONTINUE
            CALL SLEEP(2)
         IF (.NOT. I_CONTINUE) GOTO 100
      ENDIF
      CALL MPI_BARRIER(MP_UTIL_REQCOMM(), I_ERROR)

      H2D2_MPI_DBG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data H2D2 initialise les COMMON propres au main de H2D2
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Dans h2d2.fc, les déclarations de fonctions ne sont pas dans le common.
C     Or, ce fichier étant inclus par BLOC DATA H2D2 et le travail d'un
C     BLOCK DATA étant justement d'initialiser les variables du COMMON,
C     Intel 9.0 donne le commentaire suivant :
C     Info: Symbol in BLOCK DATA program unit is not in a COMMON block.   [H2D2_999]
C************************************************************************
      BLOCK DATA H2D2

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'h2d2.fc'

      DATA H2D2_TSTART  /-1.0D0/
      DATA H2D2_NIVLOG  /LOG_LVL_INFO/
      DATA H2D2_MPIDBG  /0/
      DATA H2D2_XPREC   /0/
      DATA H2D2_PRNVER  /.FALSE./
      DATA H2D2_DOEXE   /.TRUE./
      DATA H2D2_DORST   /.FALSE./
      DATA H2D2_FINP    /'STDINP'/
      DATA H2D2_FRST    /'Error: unspecified file name'/
      DATA H2D2_FOUT    /'STDOUT'/
      DATA H2D2_NOMPRGM /'h2d2'/
      DATA H2D2_VERMAJ  /'19'/         !!! Modifier également h2d2.rc
      DATA H2D2_VERMIN  /'04'/
      DATA H2D2_VERREV  /'p1'/
      DATA H2D2_VERDTE  /.TRUE./

      END
