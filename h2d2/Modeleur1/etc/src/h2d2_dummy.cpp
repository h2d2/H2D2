//************************************************************************
// Fichier:  h2d2_dummy.cpp
//
// Description:
//
//************************************************************************
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

//********* Constantes **************
const unsigned int PRECISION = 17;
const unsigned int BUF_SIZE = 1024;
const char ESPACE = ' ';
const char FINL = '\n';

//*********** prototypes ************

int filtre(int, const string&, const string&);


int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 4)
	{
      err = -1;
	}
   if (!err)
   {
      string isName(arguments[1]);
      string osName(arguments[2]);
      int dim;
      istringstream is(arguments[3]);
      is >> dim;

	   err = filtre(dim, isName, osName);
   }
   return err;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Résultat:
//************************************************************************
int filtre(int dim, const string& isName, const string& osName)
{
   int err = 0;

   unsigned int nbrNoeuds;
   double temps = 0.0;

   ifstream is(isName.c_str());
	ofstream os(osName.c_str());

   is >> nbrNoeuds;
   os << nbrNoeuds << ESPACE << dim << ESPACE << temps << FINL;

   for (unsigned int i=0; i < nbrNoeuds; ++i)
   {
      for (unsigned int j=0; j < dim; ++j) os << "0.0 ";
      os << FINL;
   }

   return err;
}
