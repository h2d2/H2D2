//****************************************************************************
// Fichier:     $Id$
// Description: Extrait les résidus de la vitesse en X et Y ainsi
//              que les résidus sur le niveau de surface à partir
//              du fichier de résultat d'H2D2.
//
//****************************************************************************
#include <string>
#include <fstream>

using namespace std;

/********* Constantes **************/
const char ESPACE = ' ';
const char FINL = '\n';
const unsigned int BUF_SIZE = 256;

/*********** prototypes ************/

int filtre(const string&, const string&, const string&, const string&);


int main(int nbArg, char* arguments[])
{
   int err = 0;

   if (nbArg < 5)
   {
      err = -1;
   }

   if (!err)
   {
      string isName(arguments[1]);
      string VXResiOsName(arguments[2]);
      string VYResiOsName(arguments[3]);
      string PResiOsName(arguments[4]);

	   err = filtre(isName, VXResiOsName, VYResiOsName, PResiOsName);
   }
   return err;
}

//***********************************************
// Sommaire:
//
// Description:
//
// Entrée:  fileNameIs: nom du fichier d'entrée
//          fileNameOs: nom du fichier de sortie
//
// Sortie:
//
// Résultat:
//************************************************
int filtre(const string& isName, const string& VXResiOsName,
           const string& VYResiOsName, const string& PResiOsName)
{
   int err = 0;

	ifstream is(isName.c_str());
	ofstream VXOs(VXResiOsName.c_str());
	ofstream VYOs(VYResiOsName.c_str());
	ofstream POs (PResiOsName.c_str());

   VXOs.precision(17);
   VXOs.flags(ios::scientific);
   VYOs.precision(17);
   VYOs.flags(ios::scientific);
   POs.precision(17);
   POs.flags(ios::scientific);

   unsigned int nbrNoeuds;
   unsigned int nbrDim;
   double temps;

   // --- nombre de noeuds et d'élements
   is >> nbrNoeuds;
   char buffer[BUF_SIZE];
   is.get(buffer, BUF_SIZE);
   if (is)
   {
      VXOs << nbrNoeuds << FINL;
      VYOs << nbrNoeuds << FINL;
    	POs  << nbrNoeuds << FINL;
   }
   if (is)
   {
      for (unsigned int i = 0; i < nbrNoeuds; i++)
      {
         double valVX;
         double valVY;
         double valP;
         is >> valVX >> valVY >> valP;
         if (is)
         {
            VXOs << valVX << FINL;
            VYOs << valVY << FINL;
            POs  << valP  << FINL;
         }
         if (!is)
         {
            break;
         }
      }
   }
   if (!is)
   {
      err = -1;
   }
   return err;
}

