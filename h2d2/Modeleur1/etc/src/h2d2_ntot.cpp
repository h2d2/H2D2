//*****************************************************************************
// $Id$
// $Date$
//*****************************************************************************
//****************************************************************************
// Fichier: genprno.cpp
//
// Description: Génère un fichier de frottement pour H2D2
//
//****************************************************************************
#include <string>
#include <fstream>
#include <math.h>

using namespace std;

//********** constantes ************
const char ESPACE = ' ';
const char FINL  = '\n';

//*********** prototypes ************
int filtre(const string&, const string&, const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

   if (nbArg < 5)
   {
      err = -1;
   }

   if (!err)
   {
      string subName(arguments[1]);
      string macName(arguments[2]);
      string froName(arguments[3]);
      string prnName(arguments[4]);

      err = filtre(subName, macName, froName, prnName);
   }
   return err;
}

//************************************************
// Sommaire:    Génère les propriétées nodales
//
// Description: Génère les propriétées nodales
//              Format du fichier
//              ---------------------------
//              NB_Total_noeuds  NB_Propropriétés_nodales_lues Temps
//              topographie0 manning0 epaisseurGlace0 option0 option0
//              topographie1 manning1 epaisseurGlace1 option1 option1
//              ...          ...      ...             ...     ...
//              manningN epaisseurGlaceN optionN optionN
//
// Entree:      xxxNameIs:  Les différents fichiers de
//                          propriétés nodales
//              prnNameOs:  nom du fichier Propriétés nodales
//
// Sortie: -----
//
// Résultat:
//*************************************************
int filtre(const string& subNameIs,
           const string& macNameIs,
           const string& froNameIs,
           const string& prnNameOs)
{
   if (subNameIs == "NOT_DEF" &&
       macNameIs == "NOT_DEF" &&
       froNameIs == "NOT_DEF") return -1;

   ifstream subIs(subNameIs.c_str());
   ifstream macIs(macNameIs.c_str());
   ifstream froIs(froNameIs.c_str());
   ofstream prnOs(prnNameOs.c_str());

   prnOs.precision(17);
   prnOs.flags(ios::scientific);

   unsigned int nbrVno = 0;
   if (subNameIs != "NOT_DEF") subIs >> nbrVno, subIs.ignore(1024,'\n');
   if (macNameIs != "NOT_DEF") macIs >> nbrVno, macIs.ignore(1024,'\n');
   if (froNameIs != "NOT_DEF") froIs >> nbrVno, froIs.ignore(1024,'\n');

   prnOs << nbrVno << ESPACE << 1 << ESPACE << 0.0 << FINL;

   for (unsigned int i = 0; i < nbrVno; ++i)
   {
      double vSub = 0;
      double vMac = 0;
      double vFro = 0;

      if (subNameIs != "NOT_DEF") subIs >> vSub;
      if (macNameIs != "NOT_DEF") macIs >> vMac;
      if (froNameIs != "NOT_DEF") froIs >> vFro;

      double vMan = vSub*fabs(vSub) + vMac*fabs(vMac) + vFro*fabs(vFro);
      vMan = sqrt( std::max(0.0, vMan) );

      prnOs << vMan << FINL;
   }

   return 0;
}
