//*****************************************************************************
// Fichier:     extrvno.cpp
// Description: Voici la description des colonnes de valeurs dans le fichier
//              de post-traitement issu du simulateur HYDROSIM:
//              1. Module de la vitesse
//              2. Vitesse en X
//              3. Vitesse en Y
//              4. Hauteur d'eau (profondeur)
//              5. Niveau de surface
//              6. Topographie du fond
//              7. Nombre de froude
//              8. Vitesse de cisaillement (module)
//              9. Viscosité physique
//              10.Viscosité numérique
//              11.Bilan de masse local (%)
//              12.Bilan de masse global (%)
//
//              Extrait les valeurs nodales suivantes à partir du fichier de
//              résultat d'HYDROSIM:
//              VNO Scalaire du module de la vitesse (c1)
//              VNO Vectoriel de la vitesse (c2 et c3)
//              VNO Scalaire de la profondeur  (c4)
//              VNO Scalaire du niveau de surface (c5)
//              VNO Scalaire du nombre de froude (c7)
//              VNO Scalaire de vitesse de cisaillement (c8)
//              VNO Scalaire de la viscosité physique (c9)
//              VNO Scalaire de la viscosité numérique (c10)
//              VNO Scalaire du bilan de masse local (c11)
//              VNO Scalaire du bilan de masse global (c12)
//
//*****************************************************************************
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

//********* Constantes **************
const char ESPACE = ' ';
const char FINL = '\n';
const unsigned int BUF_SIZE = 256;

//*********** prototypes ************

int filtre(const string&, const string&, const string&, const string&, const string&, const string&,
           const string&, const string&, const string&, const string&, const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 13)
	{
      err = -1;
	}


   if (!err)
   {
      string isSimName(arguments[1]);
      string isQName(arguments[2]);
      string ModuleVitesseOsName(arguments[3]);
      string VitesseOsName(arguments[4]);
      string ProfondeurOsName(arguments[5]);
      string NiveauOsName(arguments[6]);
      string FroudeOsName(arguments[7]);
      string VitesseCisaillementOsName(arguments[8]);
      string ViscositePhysiqueOsName(arguments[9]);
      string ViscositeNumeriqueOsName(arguments[10]);
      string BilanMasseLocalOsName(arguments[11]);
      string BilanMasseGlobalOsName(arguments[12]);

   	err = filtre(isSimName, isQName,
                           ModuleVitesseOsName, VitesseOsName,
                           ProfondeurOsName, NiveauOsName,
                           FroudeOsName, VitesseCisaillementOsName,
                           ViscositePhysiqueOsName, ViscositeNumeriqueOsName,
                           BilanMasseLocalOsName, BilanMasseGlobalOsName);
   }
   return err;
}

//*****************************************************************************
// Sommaire:     Construit des fichiers de valeurs aux noeuds à partir d'un
//               fichier de résultat.
//
// Description:  La fonction filtre(...) permet d'extraire des données à partir
//               d'un fichier source isName.  On produit 10 fichiers:
//               VNO Scalaire du module de la vitesse (c1)
//               VNO Vectoriel de la vitesse (c2 et c3)
//               VNO Scalaire de la profondeur  (c4)
//               VNO Scalaire du niveau de surface (c5)
//               VNO Scalaire du nombre de froude (c7)
//               VNO Scalaire de vitesse de cisaillement (c8)
//               VNO Scalaire de la viscosité physique (c9)
//               VNO Scalaire de la viscosité numérique (c10)
//               VNO Scalaire du bilan de masse local (c11)
//               VNO Scalaire du bilan de masse global (c12)
//
// Entrée:       const string& isName  : le nom du fichier source
//               const string& ModuleVitesseOsName:
//               const string& VitesseOsName:
//               const string& ProfondeurOsName:
//               const string& NiveauOsName:
//               const string& FroudeOsName:
//               const string& VitesseCisaillementOsName:
//               const string& ViscositePhysiqueOsName:
//               const string& ViscositeNumeriqueOsName:
//               const string& BilanMasseLocalOsName:
//               const string& BilanMasseGlobalOsName:
//
// Sortie:
//
// Résultat:
//*****************************************************************************
int filtre(const string& isSimName,
           const string& isQName,
           const string& ModuleVitesseOsName,
           const string& VitesseOsName,
           const string& ProfondeurOsName,
           const string& NiveauOsName,
           const string& FroudeOsName,
           const string& VitesseCisaillementOsName,
           const string& ViscositePhysiqueOsName,
           const string& ViscositeNumeriqueOsName,
           const string& BilanMasseLocalOsName,
           const string& BilanMasseGlobalOsName)
{
   int err = 0;

	ifstream isSim                 (isSimName.c_str());
	ifstream isQ                   (isQName.c_str());
   ofstream ModuleVitesseOs       (ModuleVitesseOsName.c_str());
   ofstream VitesseOs             (VitesseOsName.c_str());
   ofstream ProfondeurOs          (ProfondeurOsName.c_str());
   ofstream NiveauOs              (NiveauOsName.c_str());
   ofstream FroudeOs              (FroudeOsName.c_str());
   ofstream VitesseCisaillementOs (VitesseCisaillementOsName.c_str());
   ofstream ViscositePhysiqueOs   (ViscositePhysiqueOsName.c_str());
   ofstream ViscositeNumeriqueOs  (ViscositeNumeriqueOsName.c_str());
   ofstream BilanMasseLocalOs     (BilanMasseLocalOsName.c_str());
   ofstream BilanMasseGlobalOs    (BilanMasseGlobalOsName.c_str());

   ModuleVitesseOs.precision(17);
   ModuleVitesseOs.flags(ios::scientific);
   VitesseOs.precision(17);
   VitesseOs.flags(ios::scientific);
   ProfondeurOs.precision(17);
   ProfondeurOs.flags(ios::scientific);
   NiveauOs.precision(17);
   NiveauOs.flags(ios::scientific);
   FroudeOs.precision(17);
   FroudeOs.flags(ios::scientific);
   VitesseCisaillementOs.precision(17);
   VitesseCisaillementOs.flags(ios::scientific);
   ViscositePhysiqueOs.precision(17);
   ViscositePhysiqueOs.flags(ios::scientific);
   ViscositeNumeriqueOs.precision(17);
   ViscositeNumeriqueOs.flags(ios::scientific);
   BilanMasseLocalOs.precision(17);
   BilanMasseLocalOs.flags(ios::scientific);
   BilanMasseGlobalOs.precision(17);
   BilanMasseGlobalOs.flags(ios::scientific);

   unsigned int nbrNoeuds;
   unsigned int nbrDim;
   double temps;

   // --- nombre de noeuds
   isSim >> nbrNoeuds >> nbrDim >> temps;
   char buffer[BUF_SIZE];
   isSim.getline(buffer, BUF_SIZE);
   isQ.getline(buffer, BUF_SIZE);
   if (isSim && isQ)
   {
      ModuleVitesseOs << nbrNoeuds << FINL;
      VitesseOs << nbrNoeuds << ESPACE << 2 << FINL;
      ProfondeurOs << nbrNoeuds << FINL;
      NiveauOs << nbrNoeuds << FINL;
      FroudeOs << nbrNoeuds << FINL;
      VitesseCisaillementOs << nbrNoeuds << FINL;
      ViscositePhysiqueOs << nbrNoeuds << FINL;
      ViscositeNumeriqueOs << nbrNoeuds << FINL;
      BilanMasseLocalOs << nbrNoeuds << FINL;
      BilanMasseGlobalOs << nbrNoeuds << FINL;
   }
   if (isSim)
   {
      for (unsigned int i = 0; i < nbrNoeuds; i++)
      {
         double valVX;
         double valVY;
         double valN;
         double valMV;
         double valP;
         double valQ;
         double valFR;
         double valVC;
         double valVP;
         double valVN;
         double valBML;
         double valBMG;

         isSim >> valVX >> valVY >> valN >> valMV >> valP >> valQ
               >> valFR >> valVC >> valVP >> valVN;
         isQ   >> valBML >> valBMG;
         isSim.getline(buffer, BUF_SIZE);
         isQ.getline(buffer, BUF_SIZE);

         if (isSim && isQ)
         {
            ModuleVitesseOs << valMV << FINL;
            VitesseOs << valVX << ESPACE << valVY << FINL;
            ProfondeurOs << valP << FINL;
            NiveauOs << valN << FINL;
            FroudeOs << valFR << FINL;
            VitesseCisaillementOs << valVC << FINL;
            ViscositePhysiqueOs << valVP << FINL;
            ViscositeNumeriqueOs << valVN << FINL;
            BilanMasseLocalOs << valBML << FINL;
            BilanMasseGlobalOs << valBMG << FINL;
         }
         if (!isSim || !isQ)
         {
            break;
         }
      }
   }
   if (!isSim || !isQ)
   {
      err = -1;
      cout << "erreur!" << endl;
   }
   return err;
}

