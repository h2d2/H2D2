//************************************************************************
// Fichier:  h2d2_fimaiexp.cpp
//
// Sommaire: Filtre pour exporter un maillage dans le format
//           externe H2D2.
//
// Description:
//    Filtre d'exportation pour un maillage.
//    On recoit un fichier d'entré du format intermédiaire
//    d'un maillage du modeleur et on produit deux
//    fichier: un fichier de coordonnées et un fichier
//    d'éléments.
//
// Note: Bon pour le maillage avec éléments T6L seulement
//************************************************************************
#include <fstream>
#include <string>

using namespace std;

//************** constantes
const unsigned int NBR_NOEUDS_T6L = 6;
const unsigned int BUF_SIZE = 256;
const unsigned int PRECISION = 17;
const char ESPACE = ' ';
const char FINL = '\n';

//*********** prototypes ************
int filtre(const string&, const string&, const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 5)
	{
      err = -1;
	}

   if (!err)
   {
      string isName(arguments[1]);
      string corOsName(arguments[2]);
      string eleOsName(arguments[3]);
      string repereOsName(arguments[4]);

      err = filtre(isName, corOsName, eleOsName, repereOsName);
   }
   return err;
}

//************************************************************************
// Sommaire:    Filtre un maillage du format intermédiaire
//              au format externe pour HYDROSIM.
//
// Description: Transforme le maillage du format
//              intermédiaire au format externe pour HYDROSIM.
//              i.e. Élimine le min et le max, le système de
//              transformée de coordonnées et crée un fichier
//              pour les coordonnées et un pour les éléments.
//
// Entree:      const string& fileNameIs   : Maillage d'entrée
//              const string& corNameOs    : fichier de coordonnées
//              const string& eleNameOs    : fichier d'éléments
//              const string& repereNameOs : fichier du repère
//
// Sortie: -----
//
// Notes:
//************************************************************************
int filtre(const string& fileNameIs, const string& corNameOs,
           const string& eleNameOs, const string& repereNameOs)
{
   int err = 0;

	ifstream is(fileNameIs.c_str());
	ofstream corOs(corNameOs.c_str());
	ofstream eleOs(eleNameOs.c_str());
	ofstream repereOs(repereNameOs.c_str());

   // --- ajuste la précision du fichier de coordonnées
   corOs.precision(PRECISION);
   corOs.flags(ios::scientific);

   string finLigne;
   unsigned int nbrNoeud;
   unsigned int nbrElements;
   unsigned int dimension;
   double temps = 0.0;

   char buffer[BUF_SIZE];
   is.getline(buffer, BUF_SIZE-1);  // --- On lit la fin de la ligne.
   if (is)
   {
      string ligneRepere(buffer);
      repereOs << buffer << FINL;

      if (ligneRepere == "transformation=cartesienne")
      {
         // --- transformation=cartesienne (2 lignes)
         is.getline(buffer, BUF_SIZE-1);
         repereOs << buffer << FINL;
      }
      else
      {
         // --- transformation=geo ou transformation=longlat (3 lignes)
         is.getline(buffer, BUF_SIZE-1);
         repereOs << buffer << FINL;
         is.getline(buffer, BUF_SIZE-1);
         repereOs << buffer << FINL;
      }
   }
   if (is)
   {
      is >> nbrNoeud >> nbrElements >> dimension;
   }
   if (is)
   {
      // --- On peut commencer la lecture des noeuds.
      corOs << nbrNoeud << ESPACE << dimension << ESPACE << temps << FINL;
   }
   if (is && corOs)
   {
      for (unsigned int i = 0; i < nbrNoeud; i++)
      {
         double coordX;
         double coordY;
         unsigned int noNoeud;
         unsigned int noVno;
         is >> noNoeud >> noVno;  // --- On n'écrit pas la numérotation
         is >> coordX >> coordY;
         corOs << coordX << ESPACE << coordY << FINL;

         if (!is || !corOs)
         {
            err = -1;
            break;
         }
      }
   }

   if (is && corOs && !err)
   {
      for (unsigned int i=0; i<7; i++)
      {
         unsigned int typeElem = 0;
         unsigned int nbrElem = 0;
         is >> typeElem >> nbrElem;
         switch (typeElem)
         {
            case 0:
            {
               break;
            }
            case 1:
            {
               break;
            }
            case 2:
            {
               break;
            }
            case 3:
            {
               break;
            }
            case 4:
            {
               if (nbrElem > 0)
               {
                  eleOs << nbrElem << ESPACE << NBR_NOEUDS_T6L << " 203" << ESPACE << temps << FINL;;
                  for (unsigned int k = 0; k < nbrElem; k++)
                  {
                     unsigned int noeud0, noeud1, noeud2, noeud3, noeud4, noeud5;
                     unsigned int noElem;
                     is >> noElem
                        >> noeud0
                        >> noeud1
                        >> noeud2
                        >> noeud3
                        >> noeud4
                        >> noeud5;

                     eleOs << noeud0 << ESPACE
                           << noeud1 << ESPACE
                           << noeud2 << ESPACE
                           << noeud3 << ESPACE
                           << noeud4 << ESPACE
                           << noeud5 << FINL;
                     if (!is || !eleOs)
                     {
                        err = -1;
                        break;
                     }
                  }
               }
               break;
            }
            case 5:
            {
               break;
            }
            case 6:
            {
               break;
            }
         }
         if (!is || !eleOs)
         {
            err = -1;
            break;
         }
      }
   }
   return err;
}
