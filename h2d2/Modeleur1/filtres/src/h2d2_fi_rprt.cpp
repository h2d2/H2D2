//************************************************************************
// Fichier:  $Id$
//
// Sommaire: Filtre pour importer un partitionnement de maillage
//
// Description:
//
//************************************************************************
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//************** constantes
const unsigned int BUF_SIZE = 256;
const unsigned int PRECISION = 17;
const char ESPACE = ' ';
const char FINL = '\n';

//*********** prototypes ************
int filtre(const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 3)
	{
      err = -1;
	}

   if (!err)
   {
      string isName(arguments[1]);
      string osName(arguments[2]);

      err = filtre(isName, osName);
   }
   return err;
}

//************************************************************************
// Sommaire:    Lis un partitionnement.
//
// Description:
//      Filtre un partitionnement sous la forme d'un VNO qui
//      contient le numéro de sous-domaine comme valeur.
//
// Entree:
//      const string& fileNameIs : format d'entrée
//      const string& fileNameOs : format de sortie
//
// Sortie: -----
//
// Notes:
//************************************************************************
int filtre(const string& fileNameIs, const string& fileNameOs)
{
   int err = 0;

	ifstream is(fileNameIs.c_str());
	ofstream os(fileNameOs.c_str());

   // --- ajuste la précision du fichier de sortie
   os.precision(PRECISION);
   os.flags(ios::scientific);

   string finLigne;
   int nbrNoeud;
   int nbrProc;

   is >> nbrNoeud >> nbrProc;
   std:cout << nbrNoeud << "  " << nbrProc << std::endl;
   is.ignore(1000,'\n');
   if (is)
   {
      os << nbrNoeud << FINL;
   }
   is.ignore(1000,'\n');
   if (is && os)
   {
      int idum;
      int iproc;
      for (int i = 0; i < nbrNoeud; ++i)
      {
         for (int j = 0; j < nbrProc; ++j) is >> idum;
         is >> iproc;
         is.ignore(1000,'\n');

         os << static_cast<double>(iproc) << FINL;
         if (!is || !os)
         {
            err = -1;
            break;
         }
      }
   }
   return err;
}
