//************************************************************************
// Fichier: h2d2_ficndexp.cpp
//
// Sommaire: Filtre pour exporter des conditions aux limites.
//
// Description:
//    Filtre d'exportation pour des conditions limites.
//
// Note:
//
//************************************************************************
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <math.h>

#include <map>
#include <vector>

using namespace std;

//************** constantes
const unsigned int ICODE_SIZE = 11;
const unsigned int BUF_SIZE   = 256;
const unsigned int PRECISION  = 17;
const char ESPACE = ' ';
const char FINL   = '\n';

enum cl_t { CL_NONE, CL_H, CL_SOLC, CL_QN, CL_QT };
typedef std::pair<unsigned int, double> pair_t;
typedef std::vector<pair_t> vec_t;
typedef std::multimap<cl_t, vec_t*> map_t;

//*********** prototypes ************
int filtre(const string&, const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 4)
	{
      err = -1;
	}

   if (!err)
   {
      string isName(arguments[1]);
      string cndOsName(arguments[2]);
      string bndOsName(arguments[3]);

      err = filtre(isName, cndOsName, bndOsName);
   }
   return err;
}

//************************************************************************
// Sommaire:    filtre les conditions limites pour exportation
//
// Description: Filtre les conditions limites du
//              format intermédiaire au format
//              externe standard.
//
// Entrée:      fileNameIs: nom du fichier d'entrée
//              cndNameOs:  nom du fichier de sortie conditions limites
//              bndNameOs:  nom du fichier de sortie solicitations
//
// Sortie:
//
// Résultat:
//************************************************************************
int filtre(const string& fileNameIs, const string& cndNameOs, const string& bndNameOs)
{
   int err = 0;

	ifstream is(fileNameIs.c_str());
	ofstream cndOs(cndNameOs.c_str());
	ofstream bndOs(bndNameOs.c_str());

   //cndOs.width(24);
   //cndOs.precision(PRECISION);
   cndOs.flags(ios::scientific);

   // --- Nombre de noeuds où sont spécifiés des conditions limites
   unsigned int nbrNoeuds;
   is >> nbrNoeuds;
   is.get();

   // --- Lis le fichier dans un map de {type de CL; vecteurs de noeuds}
   map_t m;
   vec_t* vP = new vec_t();
   cl_t   tp = CL_NONE;
   double vl = -1.0e-123;

   for (unsigned int i = 1; i <= nbrNoeuds; ++i)
   {
      unsigned int noNoeud = 0;
      cl_t         tpNoeud = CL_NONE;
      double       vlNoeud;

      string separateur;
      string hCh, solcCh, qnCh, qtCh, val;
      is >> noNoeud >> hCh >> separateur >> solcCh >> separateur >> qnCh >> separateur >> qtCh;

      ++noNoeud;
      if      (hCh    != "NIL") { tpNoeud = CL_H;     val = hCh; }
      else if (solcCh != "NIL") { tpNoeud = CL_SOLC;  val = solcCh; }
      else if (qnCh   != "NIL") { tpNoeud = CL_QN;    val = qnCh; }
      else if (qtCh   != "NIL") { tpNoeud = CL_QT;    val = qtCh; }

      istringstream isbuf(val);
      isbuf >> vlNoeud;

      double dv = fabs(vlNoeud-vl);
      if (fabs(vlNoeud) > 1.0e-80) dv = dv / fabs(vlNoeud);
      bool isSameVal = (dv < 1.0e-10);

      if (tpNoeud != tp || ! isSameVal)
      {
         if (vP->size() > 0)
         {
            m.insert(map_t::value_type(tp, vP));
            vP = new vec_t();
         }
         tp = tpNoeud;
         vl = vlNoeud;
      }

      vP->push_back( pair_t(noNoeud, vlNoeud) );
   }
   if (tp != CL_NONE && vP->size() > 0)
   {
      m.insert(map_t::value_type(tp, vP));
   }

   // --- Nombre de limites
   cndOs << m.size() << " 0.0" << endl;
   bndOs << m.size() << endl;

   // --- Pour chaque limite
   int clCtr = 0;
   for (map_t::const_iterator mI = m.begin(); mI != m.end(); ++mI)
   {
      cl_t   tp = (*mI).first;
      vec_t* vP = (*mI).second;

      ostringstream osbuf;
      double v;
      for (vec_t::const_iterator vI = vP->begin(); vI != vP->end(); ++vI)
      {
         osbuf << " " << (*vI).first;
         v = (*vI).second;
      }

      int code = -1;
      switch (tp)
      {
         case CL_H:     code =  2; break;
         case CL_SOLC:  code = 12; break;
      }

      ++clCtr;
      bndOs << "$__" << setw(9) << setfill('0') << clCtr << "__$ " << setw(9) << setfill(' ') << osbuf.str() << endl;
      cndOs << "$__" << setw(9) << setfill('0') << clCtr << "__$ " << setw(9) << setfill(' ') << code << " " << setw(24) << setprecision(PRECISION) << v << endl;

      if (!is || !bndOs || !cndOs)
      {
         err = -1;
         break;
      }

   }

   return err;
}
