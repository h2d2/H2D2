//************************************************************************
// Fichier:  $Id$
//
// Sommaire: Filtre pour importer les conditions initiales dans le format H2D2
//
// Description:
//
//************************************************************************
#include <fstream>
#include <string>
using namespace std;

//************** constantes
const unsigned int BUF_SIZE = 256;
const unsigned int PRECISION = 17;
const char ESPACE = ' ';
const char FINL = '\n';

//*********** prototypes ************
int filtre(const string&, const string&);

int main(int nbArg, char* arguments[])
{
   int err = 0;

	if (nbArg < 3)
	{
      err = -1;
	}

   if (!err)
   {
      string isName(arguments[1]);
      string osName(arguments[2]);

      err = filtre(isName, osName);
   }
   return err;
}

//************************************************************************
// Sommaire:    Filtre un fichier de résultat ascii H2D2.
//
// Description: Transforme le résultat du format externe d'H2D2 vers
//              le format résultat ascii H2D2 intermédiaire
//
// Entree:      const string& fileNameIs : format d'entrée
//              const string& fileNameOs : format de sortie
//
// Sortie: -----
//
// Notes:
//************************************************************************
int filtre(const string& fileNameIs, const string& fileNameOs)
{
   int err = 0;

	ifstream is(fileNameIs.c_str());
	ofstream os(fileNameOs.c_str());

   // --- ajuste la précision du fichier de sortie
   os.precision(PRECISION);
   os.flags(ios::scientific);

   string finLigne;
   unsigned int nbrNoeud;
   unsigned int dimension;
   double temps;

   is >> nbrNoeud >> dimension >> temps;
   if (is)
   {
      os << nbrNoeud << FINL;
   }
   if (is && os)
   {
      for (unsigned int i=0; i < nbrNoeud; ++i)
      {
         os << dimension << FINL;
         double valeur;
         for (unsigned int j=0; j < dimension; ++j)
         {
            is >> valeur;
            os << valeur << FINL;
            if (!is || !os)
            {
               err = -1;
               break;
            }
         }
         if (!is || !os)
         {
            err = -1;
            break;
         }
      }
   }
   return err;
}
