C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Notes:
C************************************************************************
C$NOREFERENCE

C---     DIMENSIONS
      INTEGER SED2D_NCLSMAX
      INTEGER SED2D_NDLNMAX

      PARAMETER (SED2D_NCLSMAX = 10)                  ! 10 SEDIMENT CLASSES
      PARAMETER (SED2D_NDLNMAX = SED2D_NCLSMAX+10)     ! +10 FOR SUBLAYER THICKNESSES

C---     GLOBAL PROPERTIES
      REAL*8  SED2D_GRAVITE      ! GRAVITY
      REAL*8  SED2D_RHO_EAU      ! DENSITY OF WATER
      REAL*8  SED2D_VISC_EAU     ! VISCOSITY OF WATER
      REAL*8  SED2D_RHOS         ! SEDIMENT DENSITY
      REAL*8  SED2D_LAMBDA       ! BED MATERIAL POROSITY
      REAL*8  SED2D_LA           ! ACTIVE LAYER THICKNESS
      REAL*8  SED2D_SUBL         ! MINIMUM TOTAL THICKNESS SUBLAYER
      REAL*8  SED2D_VISC_Z       ! NUMERICAL VISCOSITY FOR Z
      REAL*8  SED2D_VISC_Z_DZC   ! CRITICAL Z gradient
      REAL*8  SED2D_VISC_FAI     ! NUMERICAL VISCOSITY FOR FAI
      REAL*8  SED2D_VISC_TAI     ! NUMERICAL VISCOSITY FOR TAI
      REAL*8  SED2D_VISC_TSI     ! NUMERICAL VISCOSITY FOR TSI
      REAL*8  SED2D_VISC_DZ      ! NUMERICAL VISCOSITY FOR DZ
      REAL*8  SED2D_VISC_DFAI    ! NUMERICAL VISCOSITY FOR DFAI
      REAL*8  SED2D_VISC_DTAI    ! NUMERICAL VISCOSITY FOR DTAI
      REAL*8  SED2D_VISC_DTSI    ! NUMERICAL VISCOSITY FOR DTSI
      REAL*8  SED2D_DMPG_Z       ! DAMPING FOR Z
      REAL*8  SED2D_DMPG_FAI     ! DAMPING FOR FAI
      REAL*8  SED2D_DMPG_TAI     ! DAMPING FOR TAI
      REAL*8  SED2D_DMPG_TSI     ! DAMPING FOR TSI
      REAL*8  SED2D_ALPHA        ! EXCHANGE RATE PARAMETER
      REAL*8  SED2D_HELA         ! BED SHEAR DIRECTION CORRECTION DUE TO HELICAL FLOW
      REAL*8  SED2D_FS           ! BED SHEAR DIRECTION CORRECTION DUE TO BED SLOPE (1/FS)
      REAL*8  SED2D_FRCT_TOTAL   ! CHOICE OF EMPIRICAL FORMULAE FOR TOTAL SHEAR STRESS COMPUTATION
      REAL*8  SED2D_FRCT_EFF     ! CHOICE OF EMPIRICAL FORMULAE FOR EFFECTIVE SHEAR STRESS COMPUTATION
      REAL*8  SED2D_TAUSTR_CR    ! DIMENSIONLESS CRITICAL SHEAR STRESS
      REAL*8  SED2D_DMT          ! SEDIMENT SIZE IN UNITS OF METER
      REAL*8  SED2D_PHI          ! SEDIMENT SIZE IN PHI UNITS

      COMMON/SED2D_CDT1/SED2D_GRAVITE,
     &                  SED2D_RHO_EAU,
     &                  SED2D_VISC_EAU,
     &                  SED2D_RHOS,
     &                  SED2D_LAMBDA,
     &                  SED2D_LA,
     &                  SED2D_VISC_Z,
     &                  SED2D_VISC_Z_DZC,
     &                  SED2D_VISC_FAI,
     &                  SED2D_VISC_TAI,
     &                  SED2D_VISC_TSI,
     &                  SED2D_VISC_DZ,
     &                  SED2D_VISC_DFAI,
     &                  SED2D_VISC_DTAI,
     &                  SED2D_VISC_DTSI,
     &                  SED2D_DMPG_Z,
     &                  SED2D_DMPG_FAI,
     &                  SED2D_DMPG_TAI,
     &                  SED2D_DMPG_TSI,
     &                  SED2D_ALPHA,
     &                  SED2D_HELA,
     &                  SED2D_FS,
     &                  SED2D_FRCT_TOTAL,
     &                  SED2D_FRCT_EFF,
     &                  SED2D_TAUSTR_CR,
     &                  SED2D_DMT(SED2D_NCLSMAX),
     &                  SED2D_PHI(SED2D_NCLSMAX+1),
     &                  SED2D_SUBL

      INTEGER SED2D_BLEQ             ! BED LOAD EQUATION TYPE
      COMMON/SED2D_CDT2/SED2D_BLEQ

C---     TOTAL STRESS EQUATION ENUMERATION
      INTEGER SED2D_FRCT_TOTAL_UNDEFINED
      INTEGER SED2D_FRCT_TOTAL_MAN                 ! 1 - MANNING LAW
      INTEGER SED2D_FRCT_TOTAL_MODIF_MAN           ! 2 - MODIFIED MANNING LAW

      PARAMETER (SED2D_FRCT_TOTAL_UNDEFINED  = 0)
      PARAMETER (SED2D_FRCT_TOTAL_MAN        = 1)
      PARAMETER (SED2D_FRCT_TOTAL_MODIF_MAN  = 2)

C---     EFFECTIVE STRESS EQUATION ENUMERATION
      INTEGER SED2D_FRCT_EFF_UNDEFINED
      INTEGER SED2D_FRCT_EFF_TOT                   ! 1 - NO SHEAR PARTITION (EFFECTIVE STRESS = TOTAL STRESS)
      INTEGER SED2D_FRCT_EFF_ENGL                  ! 2 - USING ENGELUND AND HANSEN, 1967 FORMULA
      INTEGER SED2D_FRCT_EFF_WU                    ! 3 - USING WU AND WANG, 1999 FORMULA

      PARAMETER (SED2D_FRCT_EFF_UNDEFINED    = 0)
      PARAMETER (SED2D_FRCT_EFF_TOT          = 1)
      PARAMETER (SED2D_FRCT_EFF_ENGL         = 2)
      PARAMETER (SED2D_FRCT_EFF_WU           = 3)

C---     BEDLOAD EQUATION ENUMERATION
      INTEGER SED2D_BLEQ_UNDEFINED
      INTEGER SED2D_BLEQT_AW                       ! 1 - ACKERS AND WHITE (1973) BED LOAD EQUATION
      INTEGER SED2D_BLEQT_WU                       ! 2 - WU, W., WANG, S.S.Y. AND JIA, Y. (2000) BED LOAD EQUATION
      INTEGER SED2D_BLEQT_TEST                     ! 3 - BED LOAD EQUATION FOR MODEL TESTING
      INTEGER SED2D_BLEQT_TEST2                    ! 4 - BED LOAD EQUATION FOR TESTING SED2D-SV2D CONNECTION
      INTEGER SED2D_BLEQT_TEST3                    ! 5 - BED LOAD EQUATION FOR TESTING SED2D-SV2D QB FROM LOCATION EROSION
      INTEGER SED2D_BLEQT_TEST4                    ! 6 - BED LOAD EQUATION FOR TESTING SED2D-SV2D QB FROM LOCATION SEDIMENTATION

      PARAMETER (SED2D_BLEQ_UNDEFINED        = 0)
      PARAMETER (SED2D_BLEQT_AW              = 1)
      PARAMETER (SED2D_BLEQT_WU              = 2)
      PARAMETER (SED2D_BLEQT_TEST            = 3)
      PARAMETER (SED2D_BLEQT_TEST2           = 4)
      PARAMETER (SED2D_BLEQT_TEST3           = 5)
      PARAMETER (SED2D_BLEQT_TEST4           = 6)

C---     FUNCTIONS
      INTEGER SED2D_BSE_PRN1PRGL
      REAL*8  SED2D_SIZEPARM
      REAL*8  SED2D_MSS_SIZEPARM
      REAL*8  SED2D_FEXI
      REAL*8  SED2D_EXI

C$REFERENCE
