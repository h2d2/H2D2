C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_FRC_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_ASMF(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sed2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER   IERR

      INTEGER SED2D_NNELT3
      INTEGER SED2D_NDLEMAX
      PARAMETER (SED2D_NNELT3  =  3)
      PARAMETER (SED2D_NDLEMAX = SED2D_NNELT3*SED2D_NDLNMAX)

      INTEGER KLOCE(SED2D_NDLEMAX)
      REAL*8  VFE  (SED2D_NDLEMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. SED2D_NDLNMAX)
C-----------------------------------------------------------------

      IERR = SP_ELEM_ASMFE(LM_CMMN_NDLL, KLOCN, VSOLC, VFG)

      IF (ERR_GOOD()) THEN
         CALL SED2D_FRC_ASMF_V(KLOCE,
     &                         VFE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
      ENDIF

      IF (ERR_GOOD()) THEN
         CALL SED2D_FRC_ASMF_S(KLOCE,
     &                         VFE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_FRC_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_ASMF_V(KLOCE,
     &                           VFE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLC,
     &                           VSOLR,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG,
     &                           VFG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  VKX,VKY,VEX,VEY,VSX,VSY,DETJ
      REAL*8  FQBI1(SED2D_NCLSMAX)
      REAL*8  FQBI2(SED2D_NCLSMAX)
      REAL*8  FQBI3(SED2D_NCLSMAX)
      REAL*8  FI(SED2D_NCLSMAX)
      REAL*8  FAI1(SED2D_NCLSMAX)
      REAL*8  FAI2(SED2D_NCLSMAX)
      REAL*8  FAI3(SED2D_NCLSMAX)
      REAL*8  FI1,FI2,FI3
      REAL*8  DELTA,THETA1,THETA2,THETA3
      REAL*8  QB1,QB2,QB3
      REAL*8  QBX1,QBX2,QBX3
      REAL*8  QBY1,QBY2,QBY3
      REAL*8  ZP1,ZP2,ZP3
      REAL*8  DISPZ, DZDX, DZDY
      REAL*8  DISPFAI, DFAIDX, DFAIDY
      REAL*8  F,FQ
      REAL*8  FX, FY
      INTEGER IDZ, IDZP
      INTEGER IDTHETA
      INTEGER IDFLI1,IDFLI2,IDL1,IDL2
      INTEGER IDQB
      INTEGER IDFQBI,IDQ
      INTEGER IERR
      INTEGER IC,ID,IE
      INTEGER NO1,NO2,NO3
      INCLUDE 'log.fi'

C----------------------------- Lapidus - Begin
      REAL*8  GRD1, GRD2, GRD3
      REAL*8  DELZ, VLAPDZ
C----------------------------- Lapidus - End
C-----------------------------------------------------------------------

C---     INDICES
      IDZ         = LM_CMMN_NDLN
      IDZP        = 5
      IDFLI1      = 6
      IDFLI2      = IDFLI1 + SED2D_NCLASS
      IDQB        = 7 + SED2D_NCLASS * SED2D_NLAYER
      IDFQBI      = IDQB
      IDTHETA     = 8 + SED2D_NCLASS * (SED2D_NLAYER + 1)

C---     INITIALIZE VFE
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)

C---     INITIALIZE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO IC=1,EG_CMMN_NELCOL
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(3,IE)
         NO3  = KNGV(5,IE)

C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
         VKX    = VDJV(1,IE)
         VEX    = VDJV(2,IE)
         VKY    = VDJV(3,IE)
         VEY    = VDJV(4,IE)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)
         DETJ   = VDJV(5,IE)

C---        NODAL VALUES
         ZP1   = VPRNO(IDZP,NO1)    ! Z previous
         QB1   = VPRNO(IDQB,NO1)    ! Total transport rate
         THETA1= VPRNO(IDTHETA,NO1) ! Shear stress direction
         ZP2   = VPRNO(IDZP,NO2)
         QB2   = VPRNO(IDQB,NO2)
         THETA2= VPRNO(IDTHETA,NO2)
         ZP3   = VPRNO(IDZP,NO3)
         QB3   = VPRNO(IDQB,NO3)
         THETA3= VPRNO(IDTHETA,NO3)

C---        CORRECT SHEAR DIRECTION FOR HELICAL FLOW
         DELTA = VPREV(1,IE)
         THETA1= THETA1 - DELTA
         THETA2= THETA2 - DELTA
         THETA3= THETA3 - DELTA

C---        TRANSPORT RATE
C---        X-COMPONENT
         QBX1   = QB1 * COS(THETA1)
         QBX2   = QB2 * COS(THETA2)
         QBX3   = QB3 * COS(THETA3)

C---        Y-COMPONENT
         QBY1   = QB1 * SIN(THETA1)
         QBY2   = QB2 * SIN(THETA2)
         QBY3   = QB3 * SIN(THETA3)

C----------------------------- Lapidus dZ - Begin
         DELZ = (VSX*QBX1 + VKX*QBX2 + VEX*QBX3    ! delZ = div(QB)
     &        +  VSY*QBY1 + VKY*QBY2 + VEY*QBY3)
         DELZ = ABS(DELZ)
         GRD1 = SQRT(VSX*VSX + VSY*VSY)
         GRD2 = SQRT(VKX*VKX + VKY*VKY)
         GRD3 = SQRT(VEX*VEX + VEY*VEY)
         VLAPDZ = (GRD1 + GRD2 + GRD3) * DELZ / DETJ
C----------------------------- Lapidus dZ - End

C---        CALCULATE LOAD VECTOR FOR BED ELEVATION
         F    = -UN_6*(VSX*QBX1 + VKX*QBX2 + VEX*QBX3 +
     &                 VSY*QBY1 + VKY*QBY2 + VEY*QBY3)
D      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'F: ',F
D      CALL LOG_ECRIS(LOG_BUF)
         FX   = UN_6*(QBX1 + QBX2 + QBX3)
         FY   = UN_6*(QBY1 + QBY2 + QBY3)

C---        NUMERICAL VISCOSITY FOR Z
         DISPZ = SED2D_VISC_DZ*VLAPDZ*UN_2/DETJ

C---        DERIVATIVES DZDX, DZDY
         DZDX  = DISPZ * (VSX*ZP1 + VKX*ZP2 + VEX*ZP3)
         DZDY  = DISPZ * (VSY*ZP1 + VKY*ZP2 + VEY*ZP3)

C---        ASSEMBLE ELEMENT VECTOR FOR BED ELEVATION
!!         VFE(IDZ, 1) = F + VSX*DZDX + VSY*DZDY
!!         VFE(IDZ, 2) = F + VKX*DZDX + VKY*DZDY
!!         VFE(IDZ, 3) = F + VEX*DZDX + VEY*DZDY
         VFE(IDZ, 1) = VSX*FX + VSY*FY + VSX*DZDX + VSY*DZDY
         VFE(IDZ, 2) = VKX*FX + VKY*FY + VKX*DZDX + VKY*DZDY
         VFE(IDZ, 3) = VEX*FX + VEY*FY + VEX*DZDX + VEY*DZDY

C---        EXCHANGE RATE FRACTIONS ARE NEEDED TO
C---        COMPUTE LOAD VECTOR FOR SIZE FRACTIONS
         DO ID = 1,SED2D_NCLASS

C---           TRANSPORT RATE FIRST LAYER SIZE FRACTIONS
            IDQ      = IDFQBI+ID
            IDL1     = IDFLI1+ID
            FQBI1(ID)= VPRNO(IDQ,NO1)     ! transport rate fractions
            FAI1(ID) = VPRNO(IDL1,NO1)    ! first layer size fractions
            FQBI2(ID)= VPRNO(IDQ,NO2)
            FAI2(ID) = VPRNO(IDL1,NO2)
            FQBI3(ID)= VPRNO(IDQ,NO3)
            FAI3(ID) = VPRNO(IDL1,NO3)

C---           CALCULATE EXCHANGE RATE FRACTIONS
            IDL2    = IDFLI2+ID
            FI1     = SED2D_FEXI(FAI1(ID),
     &                           VPRNO(IDL2,NO1),
     &                           FQBI1(ID),
     &                           F)
            FI2     = SED2D_FEXI(FAI2(ID),
     &                           VPRNO(IDL2,NO2),
     &                           FQBI2(ID),
     &                           F)
            FI3     = SED2D_FEXI(FAI3(ID),
     &                           VPRNO(IDL2,NO3),
     &                           FQBI3(ID),
     &                           F)
            FI(ID)  = UN_3*(FI1 +FI2 + FI3)
         ENDDO

C---        ENSURE SUM OF EXCHANGE RATE FRACTIONS TO BE 100%
         CALL SED2D_NORMALIZE(SED2D_NCLASS, FI)

C---        NUMERICAL VISCOSITY FOR FAI
         DISPFAI = SED2D_VISC_DFAI*UN_2/DETJ

C---        CALCULATE LOAD VECTOR FOR SIZE FRACTIONS
         DO ID = 1,SED2D_NCLASS
            FQ = -UN_6*((VSX*QBX1 + VSY*QBY1)*FQBI1(ID) +
     &                  (VKX*QBX2 + VKY*QBY2)*FQBI2(ID) +
     &                  (VEX*QBX3 + VEY*QBY3)*FQBI3(ID))- FI(ID)*F

C---           DERIVATIVES DFAI1DX, DFAI1DY
            DFAIDX = DISPFAI * (VSX*FAI1(ID) + VKX*FAI2(ID) +
     &                          VEX*FAI3(ID))
            DFAIDY = DISPFAI * (VSY*FAI1(ID) + VKY*FAI2(ID) +
     &                          VEY*FAI3(ID))


C---           ASSEMBLE ELEMENT VECTOR FOR SIZE FRACTIONS
            VFE(ID, 1) = FQ + VSX*DFAIDX + VSY*DFAIDY
            VFE(ID, 2) = FQ + VKX*DFAIDX + VKY*DFAIDY
            VFE(ID, 3) = FQ + VEX*DFAIDX + VEY*DFAIDY
         ENDDO

C---        ELEMENT LOCALIZATION TABLE
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
            KLOCE(ID, 2)= KLOCN(ID, NO2)
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLE GLOBAL VECTOR
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

      ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_FRC_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_ASMF_S(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      INTEGER IERR
      INTEGER IES, IEV
      INTEGER IDZ, IDQB, IDTH
      INTEGER NP1, NP2
      REAL*8  VNX, VNY, DJL2, REP
      REAL*8  DELTA
      REAL*8  QB1, QBX1, QBY1, THETA1
      REAL*8  QB2, QBX2, QBY2, THETA2
      REAL*8  QN1, QN2
C-----------------------------------------------------------------------
      LOGICAL ELE_ESTTYPE1
      INTEGER IE
      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     INDICES
      IDZ  = LM_CMMN_NDLN
      IDQB = 7 + SED2D_NCLASS * SED2D_NLAYER
      IDTH = 8 + SED2D_NCLASS * (SED2D_NLAYER + 1)

C---     INITIALISE VFE
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)

C---     INITIALISE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C---     BOUCLE SUR LES ELEMENTS DE SURFACE
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE1(IES)) GOTO 199

         NP1 = KNGS(1,IES)
         NP2 = KNGS(3,IES)
         IEV = KNGS(4,IES)

C---        CONSTANTE DÉPENDANT DU VOLUME
         VNX =  VDJS(2,IES)     ! VNX =  VTY
         VNY = -VDJS(1,IES)     ! VNY = -VTX
         DJL2=  VDJS(3,IES)
         REP = UN_3*DJL2

C---        NODAL VALUES
         QB1   = VPRNO(IDQB,NP1)    ! Total transport rate
         THETA1= VPRNO(IDTH,NP1)    ! Shear stress direction
         QB2   = VPRNO(IDQB,NP2)
         THETA2= VPRNO(IDTH,NP2)

C---        CORRECT SHEAR DIRECTION FOR HELICAL FLOW
         DELTA = VPREV(1,IEV)
         THETA1= THETA1 - DELTA
         THETA2= THETA2 - DELTA

C---        TRANSPORT RATE
C---        X-COMPONENT
         QBX1 = QB1 * COS(THETA1)
         QBX2 = QB2 * COS(THETA2)

C---        Y-COMPONENT
         QBY1 = QB1 * SIN(THETA1)
         QBY2 = QB2 * SIN(THETA2)

C---        FLUX NORMAL
         QN1 = QBX1*VNX + QBY1*VNY
         QN2 = QBX2*VNX + QBY2*VNY

C---
         VFE(IDZ,1) = - (QN1+QN1 + QN2)*REP
         VFE(IDZ,2) = - (QN1 + QN2+QN2)*REP

C---      TABLE KLOCE DE LOCALISATION DES DDLS
         KLOCE(IDZ, 1)= KLOCN(IDZ, NP1)
         KLOCE(IDZ, 2)= KLOCN(IDZ, NP2)

C---       ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLES, KLOCE, VFE, VFG)

199      CONTINUE
      ENDDO

      RETURN
      END
