C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_FRC_PRCPRGL
C
C Description:
C     Pre-computing global properties.
C     Pre-computation doesn't depend on the degrees of freedom (VDLG).
C
C Entrée:
C     REAL*8    VPRGL                 ! table for global properties
C
C Sortie:
C     REAL*8  SED2D_GRAVITE           ! gravity [m^2/s]
C     REAL*8  SED2D_RHO_EAU           ! water density [kg/m^3]
C     REAL*8  SED2D_VISC_EAU          ! water viscosity [m^2/s]
C     REAL*8  SED2D_PHI               ! sediment size (phi units)
C     REAL*8  SED2D_DMT               ! sediment size (m)
C     REAL*8  SED2D_RHOS              ! sediment density (kg/m^3)
C     REAL*8  SED2D_LAMBDA            ! bed material porosity
C     REAL*8  SED2D_LA                ! active layer thickness (m)
C     REAL*8  SED2D_VISC_Z            ! numerical viscosity for z
C     REAL*8  SED2D_VISC_FAI          ! numerical viscosity for fai
C     REAL*8  SED2D_VISC_DZC          ! critical z gradient
C     REAL*8  SED2D_DMPG_Z            ! damping for z
C     REAL*8  SED2D_DMPG_FAI          ! damping for fai
C     REAL*8  SED2D_ALPHA             ! exchange rate parameter
C     REAL*8  SED2D_HELA              ! bed shear direction correction due to helical flow
C     REAL*8  SED2D_FS                ! bed shear direction correction due to bed slope effects (1/FS)
C     REAL*8  SED2D_FRCT_TOTAL        ! choice of empirical formulae for total shear stress computation
C     REAL*8  SED2D_FRCT_EFF          ! choice of empirical formulae for effective shear stress computation
C     REAL*8  SED2D_TAUSTR_CR         ! dimensionless critical shear stress
C     INTEGER SED2D_NCLASS            ! number of sediment classes
C     INTEGER SED2D_BLEQ              ! equation for bed load transport rate
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_FRC_PRCPRGL(VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_FRC_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'eacnst.fi'

      INTEGER I,ID
      REAL*8  DM
C-----------------------------------------------------------------------

      SED2D_NCLASS = LM_CMMN_NDLN - 1   ! -1 for bed elevation

      CALL SED2D_BSE_PRCPRGL(VPRGL,IERR)

      SED2D_VISC_Z      = VPRGL( 7)     ! NUMERICAL VISCOSITY FOR BED ELEVATION (Z)
      SED2D_VISC_FAI    = VPRGL( 9)     ! NUMERICAL VISCOSITY FOR SIZE FRACTIONS(FAI)
      SED2D_VISC_DZ     = VPRGL(10)     ! NUMERICAL VISCOSITY FOR INCREMENT OF BED ELEVATION (Z)
      SED2D_VISC_DFAI   = VPRGL(11)     ! NUMERICAL VISCOSITY FOR INCREMENT OF SIZE FRACTIONS(FAI)
      SED2D_DMPG_Z      = VPRGL(12)     ! DAMPING FOR BED ELEVATION (Z)
      SED2D_DMPG_FAI    = VPRGL(13)     ! DAMPING FOR SIZE FRACTIONS (FAI)

      IERR = ERR_TYP()
      RETURN
      END
