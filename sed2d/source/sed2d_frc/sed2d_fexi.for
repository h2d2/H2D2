C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_FEXI
C
C Description:
C     computes interfacial grain size fractions
C
C Entrée:
C      REAL*8  FL1          ! size fractions of 1st layer
C      REAL*8  FL2          ! size fractions of 2nd layer
C      REAL*8  FQB          ! size fractions of transported sediment
C      REAL*8  F            ! change in bed elevation (degrading or aggrading)
C
C Sortie:
C     REAL*8 FEX            ! exchange size fractions
C
C Notes:
C************************************************************************
      FUNCTION SED2D_FEXI(FL1,
     &                    FL2,
     &                    FQB,
     &                    F)

      IMPLICIT NONE

      REAL*8  FL1,FL2
      REAL*8  FQB
      REAL*8  F

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_fexi.fc'

      REAL*8  FEX
C-----------------------------------------------------------------------

C---    IF THE BED IS DEGRADING
      IF (F .LE. ZERO)THEN
         FEX  = FL2

C---    IF THE BED IS AGRADING
      ELSE
          FEX  = FQB*(UN - SED2D_ALPHA) + FL1*SED2D_ALPHA
      ENDIF
D     CALL ERR_PST(FEX .GE. ZERO .AND. FEX .LE. UN)

      SED2D_FEXI = FEX
      RETURN
      END
