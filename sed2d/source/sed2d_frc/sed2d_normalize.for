C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_NORMALIZE
C
C Description:
C     Normalizes size fractions
C
C Entrée:
C
C Sortie:
C     REAL*8 SIZE
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_NORMALIZE(NCLASS, FI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_NORMALIZE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCLASS
      REAL*8  FI(NCLASS)

      INCLUDE 'sed2d.fi'

      REAL*8  CUMPC
      INTEGER ID
C-----------------------------------------------------------------------

C---        ENSURE SUM OF EXCHANGE RATE FRACTIONS TO BE 100%
         CUMPC = 0.0D0
         DO ID = 1,NCLASS
            CUMPC = CUMPC + FI(ID)
         ENDDO
         DO ID = 1,NCLASS
            FI(ID) =  FI(ID) / CUMPC
         ENDDO

      RETURN
      END
   