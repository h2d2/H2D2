C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: SED2D_BSE_PRCDLIB
C
C Description:
C     La fonction SED2D_BSE_PRCDLIB monte la table de localisation
C     des DDL des noeuds.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_BSE_PRCDLIB(KNGV,
     &                            KLOCN,
     &                            KDIMP,
     &                            VDIMP,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

      INTEGER ID, IL, IN, IK
C-----------------------------------------------------------------------
      LOGICAL EST_RETIRE
      LOGICAL EST_DIRICHLET
      LOGICAL EST_PHANTOME
      LOGICAL EST_PARALLEL
      EST_DIRICHLET(ID) = BTEST(ID, EA_TPCL_DIRICHLET)
      EST_PHANTOME (ID) = BTEST(ID, EA_TPCL_PHANTOME)
      EST_PARALLEL (ID) = BTEST(ID, EA_TPCL_PARALLEL)
      EST_RETIRE(ID) = EST_DIRICHLET(ID) .OR.
     &                 EST_PHANTOME (ID)
C-----------------------------------------------------------------------

C---     Initialise KLOCN
      CALL IINIT(LM_CMMN_NDLN*EG_CMMN_NNL, 0, KLOCN, 1)

C---     Assigne les numéros d'équation
      IL = 0
      DO IN=1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IK = KDIMP(ID,IN)
            IF (.NOT. EST_RETIRE(IK)) THEN
               IL = IL + 1
               IF (.NOT. EST_PARALLEL(IK)) THEN
                  KLOCN(ID,IN) = IL
               ELSE
                  KLOCN(ID,IN) = -IL
               ENDIF
            ENDIF
         ENDDO
      ENDDO
      LM_CMMN_NEQL = IL

C---     Impose les C.L. de Dirichlet dans VDLG
      DO IN=1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IK = KDIMP(ID,IN)
            IF (EST_DIRICHLET(IK)) THEN
               VDLG(ID, IN) = VDIMP(ID, IN)
            ENDIF
         ENDDO
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
