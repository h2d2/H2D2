C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_Z_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SED2D_PST_Z_NOBJMAX,
     &                   SED2D_PST_Z_HBASE,
     &                   'SED2D - Post-treatment Z')

      SED2D_PST_Z_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_Z_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER  IERR
      EXTERNAL SED2D_PST_Z_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SED2D_PST_Z_NOBJMAX,
     &                   SED2D_PST_Z_HBASE,
     &                   SED2D_PST_Z_DTR)

      SED2D_PST_Z_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SED2D_PST_Z_NOBJMAX,
     &                   SED2D_PST_Z_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SED2D_PST_Z_HVALIDE(HOBJ))
         IOB = HOBJ - SED2D_PST_Z_HBASE

         SED2D_PST_Z_HZ  (IOB) = 0
         SED2D_PST_Z_HSIM(IOB) = 0
         SED2D_PST_Z_LPST(IOB) = 0
      ENDIF

      SED2D_PST_Z_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SED2D_PST_Z_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SED2D_PST_Z_NOBJMAX,
     &                   SED2D_PST_Z_HBASE)

      SED2D_PST_Z_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_INI(HOBJ, HZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HZ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROL
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HZ))

C---     RESET LES DONNEES
      IERR = SED2D_PST_Z_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SED2D_PST_Z_HBASE
         SED2D_PST_Z_HZ(IOB) = HZ
      ENDIF

      SED2D_PST_Z_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - SED2D_PST_Z_HBASE

      SED2D_PST_Z_HZ  (IOB) = 0
      SED2D_PST_Z_HSIM(IOB) = 0
      SED2D_PST_Z_LPST(IOB) = 0

      SED2D_PST_Z_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SED2D_PST_Z_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_Z_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'sed2d_pst_z.fc'
C------------------------------------------------------------------------

      SED2D_PST_Z_REQHBASE = SED2D_PST_Z_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SED2D_PST_Z_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_Z_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sed2d_pst_z.fc'
C------------------------------------------------------------------------

      SED2D_PST_Z_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SED2D_PST_Z_NOBJMAX,
     &                                       SED2D_PST_Z_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'lmelnw.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER HZ
      INTEGER HSIMD, HDLIB, HNUMR
      INTEGER NNT, NNL
      INTEGER NNL_D
      INTEGER LVNO_D
      INTEGER L_PST
      REAL*8  TSIM

      INTEGER NPOST
      PARAMETER (NPOST = 1)
      EXTERNAL SED2D_PST_Z_CLC
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB   = HOBJ - SED2D_PST_Z_HBASE
      HZ    = SED2D_PST_Z_HZ  (IOB)
      HSIMD = SED2D_PST_Z_HSIM(IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HZ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIMD))

C---     Récupère les données de simulation
      HDLIB = HS_SIMD_REQHDLIB(HSIMD)
      HNUMR = HS_SIMD_REQHNUMC(HSIMD)

C---     Récupère les données des ddl
      NNT   = HS_DLIB_REQNNT  (HDLIB)
      NNL   = HS_DLIB_REQNNL  (HDLIB)
      TSIM  = HS_DLIB_REQTEMPS(HDLIB)

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HZ)
      LVNO_D = DT_VNOD_REQLVNO(HZ)

C---     Contrôles
      IF (NNL_D  .NE. NNL)  GOTO 9900

C---     ALLOUE L'ESPACE POUR LA SOLUTION
      L_PST = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NPOST*NNL, L_PST)

C---     CALCUL - LM_ELNW_PSTSUP VA APPELER SED2D_PST_Z_CLC
      IF (ERR_GOOD()) THEN
         SED2D_PST_Z_LPST(IOB) = L_PST
         IERR = LM_ELNW_PSTSUP(HSIMD, SED2D_PST_Z_CLC, HOBJ)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HZ,
     &                         HNUMR,
     &                         VA(SO_ALLC_REQVIND(VA, L_PST)),
     &                         TSIM)
      ENDIF

C---     Récupère l'espace
      IF (L_PST .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_PST)
      SED2D_PST_Z_LPST(IOB) = 0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               NNL, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_PST_Z_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIMD, HVNOD, HNUMR
      INTEGER NNL_D, NDLN_D
      INTEGER LVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB   = HOBJ - SED2D_PST_Z_HBASE
      HVNOD = SED2D_PST_Z_HZ  (IOB)
      HSIMD = SED2D_PST_Z_HSIM(IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))
D     CALL ERR_ASR(HS_SIMD_HVALIDE(HSIMD))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNOD)
      NDLN_D = DT_VNOD_REQNVNO(HVNOD)
      LVNO_D = DT_VNOD_REQLVNO(HVNOD)

C---     Récupère la numérotation
      HNUMR  = HS_SIMD_REQHNUMC(HSIMD)

C---     Contrôles
      IF (NNL_D  .LE. 0) GOTO 9900
      IF (NDLN_D .LE. 0) GOTO 9901

C---     LOG
      IF (ERR_GOOD()) THEN
         IERR = SED2D_PST_Z_LOG(HNUMR,
     &                          NDLN_D,
     &                          NNL_D,
     &                          VA(SO_ALLC_REQVIND(VA,LVNO_D)))
      ENDIF

C---     Ecris les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_SAUVE(HVNOD, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NDLN_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_PST_Z_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
      INTEGER HDLIB
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des z et le temps
      HDLIB = HS_SIMD_REQHDLIB(HSIM)
      TSIM  = HS_DLIB_REQTEMPS(HDLIB)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = SED2D_PST_Z_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = SED2D_PST_Z_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = SED2D_PST_Z_FIN(HOBJ)

      SED2D_PST_Z_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_DLL_ASGHSIM
C
C Description:
C     La fonction SED2D_PST_Z_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - SED2D_PST_Z_HBASE
         SED2D_PST_Z_HSIM(IOB) = HSIM
      ENDIF

      SED2D_PST_Z_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_Z_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_Z_REQHVNO = 0
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_Z_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_Z_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_z.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_Z_REQNOMF = 'invalid file name'
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_Z_CLC
C
C Description:
C     La fonction privée SED2D_PST_Z_CLC est la fonction de call-back
C     appelée par LM_ELNW_PSTSUP pour exécuter le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_Z_CLC(HOBJ,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sed2d_pst_z.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L_PST
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_Z_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - SED2D_PST_Z_HBASE
      L_PST = SED2D_PST_Z_LPST(IOB)

C---     CALCUL DE POST-TRAITEMENT
      CALL SED2D_PST_Z_CLC2 (VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VA(SO_ALLC_REQVIND(VA, L_PST)))

      SED2D_PST_Z_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_Z_CLC2
C
C Description:
C     La fonction privée SED2D_PST_Z_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_PST_Z_CLC2 (VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLC,
     &                             VSOLR,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             VDLG,
     &                             VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'
      INCLUDE 'log.fi'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VPOST (1, EG_CMMN_NNL)

      INTEGER IDZ
D     INTEGER IX
C-----------------------------------------------------------------------

      IDZ = LM_CMMN_NDLN
!P      IDZ = 5
      CALL DCOPY(EG_CMMN_NNL, VDLG(IDZ,1), LM_CMMN_NDLN, VPOST, 1)
!P      CALL DCOPY(EG_CMMN_NNL, VPRNO(IDZ,1), LM_CMMN_NDLN, VPOST, 1)

C---     Log
D     LOG_ZNE = 'h2d2.element.sed2d'
D     DO IX=1,EG_CMMN_NNL
D        WRITE(LOG_BUF,'(A,I6,A,1PE14.6E3)')
D    &   'IX: ', IX,' DCOPY Z: ',VDLG(IDZ,IX) !! VPOST(1,IX)
D        CALL LOG_DBG(LOG_ZNE, LOG_BUF)
D     ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_Z_LOG
C
C Description:
C     La fonction privée SED2D_PST_Z_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_Z_LOG(HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'eacmmn.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_z.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPST .EQ. 1)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SED2D_POST_Z:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)') 'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'Z')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SED2D_PST_Z_LOG = ERR_TYP()
      RETURN
      END
