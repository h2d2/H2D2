C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_BSE_PRCPRGL
C
C Description:
C     Pre-computing global properties.
C     Pre-computation doesn't depend on the degrees of freedom (VDLG).
C
C Entrée:
C     REAL*8    VPRGL                 ! table for global properties
C
C Sortie:
C     REAL*8  SED2D_GRAVITE           ! gravity [m^2/s]
C     REAL*8  SED2D_RHO_EAU           ! water density [kg/m^3]
C     REAL*8  SED2D_VISC_EAU          ! water viscosity [m^2/s]
C     REAL*8  SED2D_PHI               ! sediment size (phi units)
C     REAL*8  SED2D_DMT               ! sediment size (m)
C     REAL*8  SED2D_RHOS              ! sediment density (kg/m^3)
C     REAL*8  SED2D_LAMBDA            ! bed material porosity
C     REAL*8  SED2D_LA                ! active layer thickness (m)
C     REAL*8  SED2D_VISC_Z            ! numerical viscosity for z
C     REAL*8  SED2D_VISC_FAI          ! numerical viscosity for fai
C     REAL*8  SED2D_VISC_DZC          ! critical z gradient
C     REAL*8  SED2D_DMPG_Z            ! damping for z
C     REAL*8  SED2D_DMPG_FAI          ! damping for fai
C     REAL*8  SED2D_ALPHA             ! exchange rate parameter
C     REAL*8  SED2D_HELA              ! bed shear direction correction due to helical flow
C     REAL*8  SED2D_FS                ! bed shear direction correction due to bed slope effects (1/FS)
C     REAL*8  SED2D_FRCT_TOTAL        ! choice of empirical formulae for total shear stress computation
C     REAL*8  SED2D_FRCT_EFF          ! choice of empirical formulae for effective shear stress computation
C     REAL*8  SED2D_TAUSTR_CR         ! dimensionless critical shear stress
C     INTEGER SED2D_NCLASS            ! number of sediment classes
C     INTEGER SED2D_BLEQ              ! equation for bed load transport rate
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BSE_PRCPRGL(VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'eacnst.fi'

      INTEGER I,ID
      REAL*8  DM
C-----------------------------------------------------------------------

      SED2D_GRAVITE     = VPRGL( 1)     ! GRAVITY
      SED2D_RHO_EAU     = VPRGL( 2)     ! DENSITY OF WATER
      SED2D_VISC_EAU    = VPRGL( 3)     ! VISCOSITY OF WATER
      SED2D_RHOS        = VPRGL( 4)     ! SEDIMENT DENSITY
      SED2D_LAMBDA      = VPRGL( 5)     ! BED MATERIAL POROSITY
      SED2D_LA          = VPRGL( 6)     ! ACTIVE LAYER THICKNESS
      SED2D_VISC_Z_DZC  = VPRGL( 8)     ! Critical Z gradient
      SED2D_ALPHA       = VPRGL(14)     ! EXCHANGE RATE PARAMETER
      SED2D_HELA        = VPRGL(15)     ! CORRECTION FOR BED SHEAR DIRECTION DUE TO HELICAL FLOW EFFECTS
      SED2D_FS          = VPRGL(16)     ! CORRECTION FOR BED SHEAR DIRECTION DUE TO BED SLOPE EFFECTS

C---     EMPIRICAL FORMULA TO COMPUTE TOTAL STRESS
      SED2D_FRCT_TOTAL = SED2D_FRCT_TOTAL_UNDEFINED
      ID = NINT(VPRGL(17))
      IF     (ID .EQ. 1) THEN
         SED2D_FRCT_TOTAL = SED2D_FRCT_TOTAL_MAN          ! 1 - MANNING LAW
      ELSEIF (ID .EQ. 2) THEN
         SED2D_FRCT_TOTAL = SED2D_FRCT_TOTAL_MODIF_MAN    ! 2 - MODIFIED MANNING LAW
D     ELSE
D        CALL ERR_ASR(ID .GE. SED2D_FRCT_TOTAL_MAN .AND.
D    &                ID .LE. SED2D_FRCT_TOTAL_MODIF_MAN)
      ENDIF

C---     EMPIRICAL FORMULA TO COMPUTE EFFECTIVE STRESS
      SED2D_FRCT_EFF  = SED2D_FRCT_EFF_UNDEFINED
      ID = NINT(VPRGL(18))
      IF     (ID .EQ. 1) THEN
         SED2D_FRCT_EFF = SED2D_FRCT_EFF_TOT          ! 1 - NO SHEAR PARTITION (EFFECTIVE STRESS = TOTAL STRESS)
      ELSEIF (ID .EQ. 2) THEN
         SED2D_FRCT_EFF = SED2D_FRCT_EFF_ENGL         ! 2 - USING ENGELUND AND HANSEN, 1967 FORMULA
      ELSEIF (ID .EQ. 3) THEN
         SED2D_FRCT_EFF = SED2D_FRCT_EFF_WU           ! 3 - USING WU AND WANG, 1999 FORMULA
D     ELSE
D        CALL ERR_ASR(ID .GE. SED2D_FRCT_EFF_TOT .AND.
D    &                ID .LE. SED2D_FRCT_EFF_WU)
      ENDIF

C---     DIMENSIONLESS CRITICAL SHEAR STRESS
      SED2D_TAUSTR_CR   = VPRGL(19)

C---     BEDLOAD TRANSPORT RATE EQUATION
      SED2D_BLEQ = SED2D_BLEQ_UNDEFINED
      ID = NINT(VPRGL(20))
      IF     (ID .EQ. 1) THEN
         SED2D_BLEQ = SED2D_BLEQT_AW    ! 1 - ACKERS AND WHITE, 1973
      ELSEIF (ID .EQ. 2) THEN
         SED2D_BLEQ = SED2D_BLEQT_WU    ! 2 - WU, W., WANG, S.S.Y. AND JIA, Y., 2000
      ELSEIF (ID .EQ. 3) THEN
         SED2D_BLEQ = SED2D_BLEQT_TEST  ! 3 - TESTING SED2D MODEL
      ELSEIF (ID .EQ. 4) THEN
         SED2D_BLEQ = SED2D_BLEQT_TEST2 ! 4 - TESTING SED2D-SV2D CONNECTION
      ELSEIF (ID .EQ. 5) THEN
         SED2D_BLEQ = SED2D_BLEQT_TEST3 ! 5 - TESTING SED2D-SV2D BED ELEVATION
      ELSEIF (ID .EQ. 6) THEN
         SED2D_BLEQ = SED2D_BLEQT_TEST4 ! 6 - TESTING SED2D-SV2D BED ELEVATION
D     ELSE
D        CALL ERR_ASR(ID .GE. SED2D_BLEQT_AW .AND.
D    &                ID .LE. SED2D_BLEQT_TEST4)
      ENDIF

C---     READ GRAIN SIZE DIAMETER FOR EACH SEDIMENT CLASS IN PHI UNITS
      DO I = 1,SED2D_NCLASS
         ID = I + LM_CMMN_NPRGL-SED2D_NCLASS !P 20 !!SL
         SED2D_PHI(I+1) = VPRGL(ID)
      ENDDO

C---     COMPUTE THE LOWER END OF THE FIRST SEDIMENT CLASS SIZE IN PHI UNITS
      IF (SED2D_NCLASS .EQ. 1)THEN
         SED2D_PHI(1) = SED2D_PHI(2)
      ELSE
         SED2D_PHI(1) = DEUX*SED2D_PHI(2)- SED2D_PHI(3)
      ENDIF

C---     CHANGE PHI UNITS TO METER AT THE MEDIAN OF EACH SEDIMENT CLASS

      DO I = 1,SED2D_NCLASS
         DM = UN_2*(SED2D_PHI(I)+SED2D_PHI(I+1))
         SED2D_DMT(I) = DEUX**DM/1000.0D0
      ENDDO

C---     INITIALIZE THE TRANSPORT RATE EQUATION
      DO ID = 1,SED2D_NCLASS
         IF     (SED2D_BLEQ .EQ. SED2D_BLEQT_AW) THEN
            CALL SED2D_BLEQ_AW_PARM(ID)
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_WU) THEN
            CALL SED2D_BLEQ_WU_PARM(ID)
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST) THEN
            CALL SED2D_BLEQ_TEST_PARM(ID)
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST2) THEN
            CALL SED2D_BLEQ_TEST2_PARM(ID)
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST3) THEN
            CALL SED2D_BLEQ_TEST3_PARM(ID)
         ELSEIF (SED2D_BLEQ .EQ. SED2D_BLEQT_TEST4) THEN
            CALL SED2D_BLEQ_TEST4_PARM(ID)
         ENDIF
      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
