C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_BSE_PRCSOLC
C
C Description:
C     Pré-traitement au calcul des sollicitations concentrées.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BSE_PRCSOLC (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VSOLC,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRCSOLC
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VSOLC(LM_CMMN_NSOLC, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END

