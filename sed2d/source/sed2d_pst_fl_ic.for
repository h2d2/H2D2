C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SED2D_PST_FL_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SED2D_PST_FL_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sed2d_pst_fl_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sed2d_pst_fl.fi'

      INTEGER IERR
      INTEGER I
      INTEGER HOBJ
      INTEGER HPST
      INTEGER NFL
      INTEGER NFLMAX
      PARAMETER (NFLMAX = 8)
      INTEGER KFL(NFLMAX)
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_TODO('sed2d_pst_fl is deprecated. To be removed.')
      CALL ERR_ASG(ERR_ERR, 'ERR_DEPRECATED_FUNCTION')
      GOTO 9999

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SED2D_PST_FL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      NFL = 0
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         I = 0
100      CONTINUE
            IF (I .GE. NFLMAX) GOTO 9901
            I = I + 1
C           <comment>List of handles on fraction layers, comma separated.</comment>
            IERR = SP_STRN_TKI(IPRM, ',', I, KFL(I))
            IF (IERR .EQ. -1) GOTO 199
            IF (IERR .NE.  0) GOTO 9902
         GOTO 100
199      CONTINUE
         NFL = I - 1
      ENDIF

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR = SED2D_PST_FL_CTR(HPST)
      IF (ERR_GOOD()) IERR = SED2D_PST_FL_INI(HPST, NFL, KFL)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>sed2d_pst_fl</b> constructs an object, with the given argument,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SED2D_PST_FL_AID()

9999  CONTINUE
      IC_SED2D_PST_FL_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SED2D_PST_FL_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SED2D_PST_FL_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_fl_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>sed2d_pst_fl</b> represents the post-treatment on the
C  bed layer fractions. It is used to update bed layer data.
C  sed2d_pst_fl is deprecated. To be removed.
C</comment>
      IC_SED2D_PST_FL_REQCMD = 'sed2d_pst_fl'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SED2D_PST_FL_AID()

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_fl_ic.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sed2d_pst_fl_ic.hlp')

      RETURN
      END
