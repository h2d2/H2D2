C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C     FUNCTION SED2D_BSE_PRN1PRGL(IP,TXT,V)
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Imprime une propriété globale
C
C Description:
C     La fonction auxiliaire SED2D_BSE_PRN1PRGL imprime une seule
C     propriété globale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_BSE_PRN1PRGL(IP, TXT, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRN1PRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       IP
      CHARACTER*(*) TXT
      REAL*8        V

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sed2d.fi'
C-----------------------------------------------------------------------

      IF (TXT(1:3) .EQ. '---') THEN
         WRITE (LOG_BUF, '(I3,1X,A)') IP, TXT, '#<38>#'
      ELSE
         WRITE (LOG_BUF, '(I3,1X,2A,1PE14.6E3)') IP, TXT, '#<50>#=', V
      ENDIF
      CALL LOG_ECRIS(LOG_BUF)

      SED2D_BSE_PRN1PRGL = ERR_TYP()
      RETURN
      END
