C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C     FUNCTION SED2D_BLEQ_WU
C     SUBROUTINE  SED2D_BLEQ_WU_PARM
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Computes bedload transport rate
C
C Description:
C     The subroutine SED2D_BLEQ_WU computes bedload transport rate based on
C     WU, W., WANG, S.S.Y. AND JIA, Y. (2000) bed load equation.
C
C Entrée:
C
C
C
C
C
C Sortie:
C     REAL*8  SED2D_BLEQ_WU          Bed load transport rate [m^2/s]
C
C Notes:
C  Reference:  WU ET AL. (2000B)
C************************************************************************
      FUNCTION SED2D_BLEQ_WU(H,
     &                       UBAR,
     &                       USTR,
     &                       N,
     &                       D50,
     &                       ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_WU
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  H
      REAL*8  UBAR
      REAL*8  USTR
      REAL*8  N
      REAL*8  D50
      INTEGER ID

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_wu.fi'
      INCLUDE 'sed2d_bleq_wu.fc'

      REAL*8  RD_UN
      REAL*8  TB, T
      REAL*8  NP, QBGR, QB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_BLEQ .EQ. SED2D_BLEQT_WU)
D     CALL ERR_PRE(SED2D_DMT(ID) .GT. ZERO)
D     CALL ERR_PRE(H .GT. ZERO)
C-----------------------------------------------------------------------

C---     RELATIVE DENSITY
      RD_UN = SED2D_RHOS / SED2D_RHO_EAU - UN
D     CALL ERR_ASR(RD_UN .GT. ZERO)

C---     BED SHEAR STRESS
      TB    = SED2D_RHO_EAU*USTR*USTR

C---     NON-DIMENSIONAL EXCESS SHEAR STRESS
      T     = MAX(ZERO, TB / SED2D_WU_TC(ID) - UN)

C---     NON-DIMENSIONAL TRANSPORT RATE
      QBGR  = 0.0053D0 * T**2.2D0
      QB    = QBGR * SQRT(RD_UN * SED2D_GRAVITE * SED2D_DMT(ID)**3.0D0)
D     CALL ERR_ASR(QB .GE. ZERO)

      SED2D_BLEQ_WU = QB

      RETURN
      END


C************************************************************************
C Sommaire : SED2D_BLEQ_WU_PARM
C
C Description:
C
C
C
C
C Entrée:
C
C
C
C Sortie:
C
C
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BLEQ_WU_PARM (ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_AW_PARM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ID

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_wu.fi'
      INCLUDE 'sed2d_bleq_wu.fc'

      REAL*8  TC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_DMT(ID) .GT. ZERO)
C-----------------------------------------------------------------------

C---     CRITICAL SHEAR STRESS
      TC = SED2D_TAUSTR_CR * SED2D_GRAVITE *
     &     (SED2D_RHOS - SED2D_RHO_EAU)* SED2D_DMT(ID)
D     CALL ERR_ASR(TC .GE. ZERO)

      SED2D_WU_TC(ID) = TC

      RETURN
      END
