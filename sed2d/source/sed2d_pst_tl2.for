C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SED2D_PST_TL2_000
C     INTEGER SED2D_PST_TL2_999
C     INTEGER SED2D_PST_TL2_CTR
C     INTEGER SED2D_PST_TL2_DTR
C     INTEGER SED2D_PST_TL2_INI
C     INTEGER SED2D_PST_TL2_RST
C     INTEGER SED2D_PST_TL2_REQHBASE
C     LOGICAL SED2D_PST_TL2_HVALIDE
C     INTEGER SED2D_PST_TL2_ACC
C     INTEGER SED2D_PST_TL2_FIN
C     INTEGER SED2D_PST_TL2_XEQ
C     INTEGER SED2D_PST_TL2_REQHVNO
C     CHARACTER*256 SED2D_PST_TL2_REQNOMF
C   Private:
C     INTEGER SED2D_PST_TL2_CLC
C     SUBROUTINE SED2D_PST_TL2_CLC2
C     INTEGER SED2D_PST_TL2_LOG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_TL2_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(SED2D_PST_TL2_NOBJMAX,
     &                   SED2D_PST_TL2_HBASE,
     &                   'SED2D - Post-treatment TL2')

      SED2D_PST_TL2_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_TL2_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER  IERR
      EXTERNAL SED2D_PST_TL2_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(SED2D_PST_TL2_NOBJMAX,
     &                   SED2D_PST_TL2_HBASE,
     &                   SED2D_PST_TL2_DTR)

      SED2D_PST_TL2_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   SED2D_PST_TL2_NOBJMAX,
     &                   SED2D_PST_TL2_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(SED2D_PST_TL2_HVALIDE(HOBJ))
         IOB = HOBJ - SED2D_PST_TL2_HBASE

         SED2D_PST_TL2_HTL2  (IOB) = 0
         SED2D_PST_TL2_LPST(IOB) = 0
      ENDIF

      SED2D_PST_TL2_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = SED2D_PST_TL2_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   SED2D_PST_TL2_NOBJMAX,
     &                   SED2D_PST_TL2_HBASE)

      SED2D_PST_TL2_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_INI(HOBJ, HTL2)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HTL2

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROL
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HTL2))

C---     RESET LES DONNEES
      IERR = SED2D_PST_TL2_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - SED2D_PST_TL2_HBASE
         SED2D_PST_TL2_HTL2(IOB) = HTL2
      ENDIF

      SED2D_PST_TL2_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SED2D_PST_TL2_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction SED2D_PST_TL2_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_TL2_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'sed2d_pst_tl2.fc'
C------------------------------------------------------------------------

      SED2D_PST_TL2_REQHBASE = SED2D_PST_TL2_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction SED2D_PST_TL2_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_TL2_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sed2d_pst_tl2.fc'
C------------------------------------------------------------------------

      SED2D_PST_TL2_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                       SED2D_PST_TL2_NOBJMAX,
     &                                       SED2D_PST_TL2_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_ACC(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'eacmmn.fc'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'lmelnw.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER HTL2
      INTEGER HDLIB, HNUMR
      INTEGER NNT, NNL
      INTEGER NNL_D
      INTEGER LVNO_D
      INTEGER L_PST
      REAL*8  TSIM

      INTEGER NPOST
      PARAMETER (NPOST = 1)
      EXTERNAL SED2D_PST_TL2_CLC
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB   = HOBJ - SED2D_PST_TL2_HBASE
      HTL2  = SED2D_PST_TL2_HTL2(IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HTL2))

C---     Récupère les données de simulation
      HDLIB = HS_SIMD_REQHDLIB(HSIM)
      HNUMR = HS_SIMD_REQHNUMC(HSIM)

C---     Récupère les données des ddl
      NNT   = HS_DLIB_REQNNT  (HDLIB)
      NNL   = HS_DLIB_REQNNL  (HDLIB)
      TSIM  = HS_DLIB_REQTEMPS(HDLIB)

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HTL2)
      LVNO_D = DT_VNOD_REQLVNO(HTL2)

C---     Contrôles
      IF (NNL_D  .NE. NNL)  GOTO 9900

C---     ALLOUE L'ESPACE POUR LA SOLUTION
      L_PST = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NPOST*NNL, L_PST)

C---     CALCUL - LM_ELNW_PSTSUP VA APPELER SED2D_PST_TL2_CLC
      IF (ERR_GOOD()) THEN
         SED2D_PST_TL2_LPST(IOB) = L_PST
         IERR = LM_ELNW_PSTSUP(HSIM, SED2D_PST_TL2_CLC, HOBJ)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HTL2,
     &                         HNUMR,
     &                         VA(SO_ALLC_REQVIND(VA, L_PST)),
     &                         TSIM)
      ENDIF

C---     Récupère l'espace
      IF (L_PST .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_PST)
      SED2D_PST_TL2_LPST(IOB) = 0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               NNL, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_PST_TL2_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_FIN(HOBJ, HSIMD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIMD, HVNOD, HNUMR
      INTEGER NNL_D, NDLN_D
      INTEGER LVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIMD))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB   = HOBJ - SED2D_PST_TL2_HBASE
      HVNOD = SED2D_PST_TL2_HTL2(IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNOD)
      NDLN_D = DT_VNOD_REQNVNO(HVNOD)
      LVNO_D = DT_VNOD_REQLVNO(HVNOD)

C---     Récupère la numérotation
      HNUMR  = HS_SIMD_REQHNUMC(HSIMD)

C---     Contrôles
      IF (NNL_D  .LE. 0) GOTO 9900
      IF (NDLN_D .LE. 0) GOTO 9901

C---     LOG
      IF (ERR_GOOD()) THEN
         IERR = SED2D_PST_TL2_LOG(HNUMR,
     &                          NDLN_D,
     &                          NNL_D,
     &                          VA(SO_ALLC_REQVIND(VA,LVNO_D)))
      ENDIF

C---     Ecris les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_SAUVE(HVNOD, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NDLN_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_PST_TL2_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
      INTEGER HDLIB
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des tl2 et le temps
      HDLIB = HS_SIMD_REQHDLIB(HSIM)
      TSIM  = HS_DLIB_REQTEMPS(HDLIB)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     ACCUMULE
      IF (ERR_GOOD()) IERR = SED2D_PST_TL2_ACC(HOBJ, HSIM)

C---     FINALISE
      IF (ERR_GOOD()) IERR = SED2D_PST_TL2_FIN(HOBJ, HSIM)

      SED2D_PST_TL2_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_TL2_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_TL2_REQHVNO = 0
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_TL2_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_PST_TL2_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sed2d_pst_tl2.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      SED2D_PST_TL2_REQNOMF = 'invalid file name'
      RETURN
      END

C************************************************************************
C Sommaire: SED2D_PST_TL2_CLC
C
C Description:
C     La fonction privée SED2D_PST_TL2_CLC est la fonction de call-back
C     appelée par LM_ELNW_PSTSUP pour exécuter le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_PST_TL2_CLC(HOBJ,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER HOBJ
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sed2d_pst_tl2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L_PST
C------------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_PST_TL2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - SED2D_PST_TL2_HBASE
      L_PST = SED2D_PST_TL2_LPST(IOB)

C---     CALCUL DE POST-TRAITEMENT
      CALL SED2D_PST_TL2_CLC2 (VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VA(SO_ALLC_REQVIND(VA, L_PST)))

      SED2D_PST_TL2_CLC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_TL2_CLC2
C
C Description:
C     La fonction privée SED2D_PST_TL2_CLC2 fait le calcul de post-traitement.
C     C'est ici que le calcul est réellement effectué.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_PST_TL2_CLC2 (VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             VSOLC,
     &                             VSOLR,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             VDLG,
     &                             VPOST)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VPOST (1, EG_CMMN_NNL)

      INTEGER IDTL2,IN
C-----------------------------------------------------------------------
      IDTL2     = 6

C---     INITIALISATION DE VPOST
      CALL DINIT(EG_CMMN_NNL, ZERO, VPOST, 1)

C---     CHARGEMENT DE VPOST
      DO IN=1,EG_CMMN_NNL

C---        SUBLAYER THICKNESS
            VPOST(1,IN) = VPRNO(IDTL2,IN)
      END DO

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_PST_TL2_LOG
C
C Description:
C     La fonction privée SED2D_PST_TL2_LOG écris dans le log les résultats
C     du post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_PST_TL2_LOG(HNUMR, NPST, NNL, VPOST)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NPST
      INTEGER NNL
      REAL*8  VPOST (NPST, NNL)

      INCLUDE 'eacmmn.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d_pst_tl2.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPST .EQ. 1)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_SED2D_POST_TL2:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE(LOG_BUF, '(A,I6)') 'MSG_NPOST#<15>#:', NPST
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF, '(A)') 'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()
      IERR = PS_PSTD_LOGVAL(HNUMR,  1, NPST, NNL, VPOST, 'TL2')
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      SED2D_PST_TL2_LOG = ERR_TYP()
      RETURN
      END
