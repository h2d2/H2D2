C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction SED2D_BSE_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomDll@nomFonction".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION SED2D_BSE_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER SED2D_BSE_REQNFN
      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'

      INTEGER        I
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
C-----------------------------------------------------------------------

      IF (IFNC .EQ. EA_FUNC_ASMF) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMK) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKT) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMKU) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMM) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_ASMMU) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCCLIM) THEN
                                    NF = 'SED2D_BSE_CLCCLIM@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCDLIB) THEN
                                    NF = 'SED2D_BSE_CLCDLIB@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRES) THEN
                                    NF = 'SED2D_BSE_CLCPRES@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPREV) THEN
                                    NF = 'SED2D_BSE_CLCPREV@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_CLCPRNO) THEN
                                    NF = 'SED2D_BSE_CLCPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCCLIM) THEN
                                    NF = 'SED2D_BSE_PRCCLIM@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCDLIB) THEN
                                    NF = 'SED2D_BSE_PRCDLIB@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRES) THEN
                                    NF = 'SED2D_BSE_PRCPRES@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPREV) THEN
                                    NF = 'SED2D_BSE_PRCPREV@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRGL) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCPRNO) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLC) THEN
                                    NF = 'SED2D_BSE_PRCSOLC@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRCSOLR) THEN
                                    NF = 'SED2D_BSE_PRCSOLR@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNCLIM) THEN
                                    NF = 'SED2D_BSE_PRNCLIM@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRGL) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PRNPRNO) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSCPRNO) THEN
                                    NF = 'SED2D_BSE_PSCPRNO@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLCLIM) THEN
                                    NF = 'SED2D_BSE_PSLCLIM@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPREV) THEN
                                    NF = 'SED2D_BSE_PSLPREV@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRGL) THEN
                                    NF = 'SED2D_BSE_PSLPRGL@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLPRNO) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLC) THEN
                                    NF = 'SED2D_BSE_PSLSOLC@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_PSLSOLR) THEN
                                    NF = 'SED2D_BSE_PSLSOLR@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_REQNFN) THEN
                                    NF = 'SED2D_BSE_REQNFN@sed2d'
      ELSEIF (IFNC .EQ. EA_FUNC_REQPRM) THEN
                                    NF = 'SED2D_BSE_DUMMY@sed2d'
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ENDIF

      IF (ERR_GOOD()) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

      SED2D_BSE_REQNFN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction vide, tenant lieu d'une autre.
C
C Description: Dummy function for reqnfn
C              If function doesn't exist it stop the simulation
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SED2D_BSE_DUMMY()

      IMPLICIT NONE

      INTEGER  SED2D_BSE_DUMMY

      INCLUDE 'err.fi'

C------------------------------------------------------------------------

D     CALL ERR_ASR(.FALSE.)
      CALL ERR_ASG(ERR_FTL, 'ERR_APPEL_FNCT_INVALIDE')

      SED2D_BSE_DUMMY = ERR_TYP()
      RETURN
      END
