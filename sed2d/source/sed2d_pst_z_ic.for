C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SED2D_PST_Z_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SED2D_PST_Z_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'sed2d_pst_z_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sed2d_pst_z.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HPST
      INTEGER HZ
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL LOG_TODO('sed2d_pst_z is deprecated. To be removed.')
C      CALL ERR_ASG(ERR_ERR, 'ERR_DEPRECATED_FUNCTION')
C      GOTO 9999

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SED2D_PST_Z_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on z</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HZ)
      IF (IERR .NE. 0) GOTO 9901

C---     CONTROL
      IF (.NOT. DT_VNOD_HVALIDE(HZ)) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR = SED2D_PST_Z_CTR(HPST)
      IF (ERR_GOOD()) IERR = SED2D_PST_Z_INI(HPST, HZ)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>sed2d_pst_z</b> constructs an object, with the given argument,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SED2D_PST_Z_AID()

9999  CONTINUE
      IC_SED2D_PST_Z_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SED2D_PST_Z_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SED2D_PST_Z_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_z_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>sed2d_pst_z</b> represents the post-treatment on the
C  bed elevation (z). It is used to link sv2d with sed2d.
C</comment>
      IC_SED2D_PST_Z_REQCMD = 'sed2d_pst_z'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SED2D_PST_Z_AID()

      IMPLICIT NONE

      INCLUDE 'sed2d_pst_z_ic.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = LOG_ECRISFIC('sed2d_pst_z_ic.hlp')

      RETURN
      END
