C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM
C
C Description:
C     La sous-routine SED2D_BSE_PRCCLIM fait tout le traitement des
C     conditions limites qui ne dépend pas de VDLG. On réinitialise
C     les codes de KDIMP, contrôle la cohérence, traite les profondeurs
C     négatives, assigne les conditions sur les éléments de contour.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les conditions limites imposées doivent être de même type pour tous
C     les degrés de libertés considérés (pour toutes les variables simulées)
C************************************************************************
      SUBROUTINE SED2D_BSE_PRCCLIM(KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             KDIMP,
     &                             VDIMP,
     &                             KEIMP,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRCCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER SED2D_BSE_PRCCLIM_CN
      INTEGER SED2D_BSE_PRCCLIM_NS
      INTEGER SED2D_BSE_PRCCLIM_EC
      INTEGER NEQ, NCLT, NCLTI, ICOD
      INTEGER IL
      INTEGER IC, IE, IN, ID
C-----------------------------------------------------------------------

C---- INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:'
C      WRITE(MP,1001)'CODE C.L.=1 CONDITION DE DIRICHLET;'
C1000  FORMAT(/)
C1001  FORMAT(15X,A)

C---     INITIALISE
      NEQ  = LM_CMMN_NDLL
      NCLT = 0

C---     RESET LES C.L.
      CALL IINIT(LM_CMMN_NDLL,    0, KDIMP, 1)
      CALL IINIT(EG_CMMN_NELS,    0, KEIMP, 1)
      CALL DINIT(LM_CMMN_NDLL, ZERO, VDIMP, 1)

C---     FLAG LES DDL SUR LES NOEUDS MILIEUX
      DO IC=1,EG_CMMN_NELCOL
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         IN = KNGV(2,IE)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_PHANTOME)
         ENDDO
         IN = KNGV(4,IE)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_PHANTOME)
         ENDDO
         IN = KNGV(6,IE)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_PHANTOME)
         ENDDO
      ENDDO
      ENDDO

C---     BOUCLE SUR LES LIMITES POUR ASSIGNER LES CODES ET VALEURS DE CONDITION
      DO IL=1, EG_CMMN_NCLLIM
         IERR = SED2D_BSE_PRCCLIM_CN(IL,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              KDIMP,
     &                              VDIMP)
      ENDDO

C---     CONTROLE LES C.L. POUR CHAQUE TYPE DE DDL
      DO ID=1,LM_CMMN_NDLN
         NCLTI = 0
         DO IN=1,EG_CMMN_NNL
            IF (KDIMP(ID,IN) .NE. 0) NCLTI = NCLTI + 1
         ENDDO
         IF (NCLTI .EQ. 0) THEN
            WRITE(LOG_BUF,'(2A,I12)') 'MSG_DDL_NON_IMPOSE',': ',ID
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         NCLT = NCLT + NCLTI
      ENDDO

C---     CONTRÔLE LA COHERENCE DES C.L.
      IF (LM_CMMN_NDLN .GT. 1) THEN
         IERR = 0
         DO IN=1,EG_CMMN_NNL
            NCLTI = 0
            ICOD = KDIMP(1,IN)
            DO ID=2,LM_CMMN_NDLN
               IF (KDIMP(ID,IN) .NE. ICOD) NCLTI = NCLTI+1
            ENDDO
            IF (NCLTI .NE. 0) THEN
               WRITE(LOG_BUF,'(2A, I12)')
     &               'MSG_CLIM_MAL_IMPOSEES_SUR_NOEUD', ': ', IN
               CALL LOG_ECRIS(LOG_BUF)
               WRITE(ERR_BUF,'(A)') 'ERR_CLIM_MAL_IMPOSEES'
               CALL ERR_ASG(ERR_ERR, ERR_BUF)
            ENDIF
         ENDDO
         IF (ERR_BAD()) GOTO 9999
      ENDIF

C---     TRAITE LES PROFONDEURS NULLES OU NÉGATIVES
      IERR = SED2D_BSE_PRCCLIM_NS(KNGV,
     &                            VPRNO,
     &                            KDIMP,
     &                            VDIMP)

C---     ASSIGNE LES C.L. SUR LES ÉLÉMENTS DE CONTOUR
      IERR = SED2D_BSE_PRCCLIM_EC(KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRES,
     &                            KDIMP,
     &                            KEIMP)

C---     CONTROLE NEQ
      DO IN = 1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IF (BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)) NEQ = NEQ-1
         ENDDO
      ENDDO
      IF (NEQ. LE. 0) THEN
         WRITE(ERR_BUF,'(2A,I12)') 'ERR_CLIM_NBR_EQUATIONS',': ',NEQ
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         GOTO 9999
      ENDIF

9999  CONTINUE
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM_CN
C
C Description:
C     La fonction SED2D_BSE_PRCCLIM_CN fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_BSE_PRCCLIM_CN(IL,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             KDIMP,
     &                             VDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BSE_PRCCLIM_CN
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SED2D_BSE_PRCCLIM_CN
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

      INTEGER SED2D_BSE_PRCCLIM_001
      INTEGER SED2D_BSE_PRCCLIM_003
      INTEGER IC
      INTEGER IT
C-----------------------------------------------------------------------

C      IERR = SED2D_BSE_PRCCLIM_001(IL,
C     &                                KCLCND, VCLCNV,
C     &                                KCLLIM, KCLNOD, KCLELE,
C     &                                KDIMP,  VDIMP)
C
C      SED2D_BSE_PRCCLIM_CN = ERR_TYP()
C      RETURN
C      END


      IC = KCLLIM(2, IL)
      IT = KCLCND(2, IC)
      IF (IT .EQ. 1) THEN
         IERR = SED2D_BSE_PRCCLIM_001(IL,
     &                                KCLCND, VCLCNV,
     &                                KCLLIM, KCLNOD, KCLELE,
     &                                KDIMP,  VDIMP)
      ELSEIF (IT .EQ. 3) THEN
         IERR = SED2D_BSE_PRCCLIM_003(IL,
     &                               KCLCND, VCLCNV,
     &                               KCLLIM, KCLNOD, KCLELE,
     &                               KDIMP,  VDIMP)
      ENDIF

      SED2D_BSE_PRCCLIM_CN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM_001
C
C Description:
C     La sous-routine privée SED2D_BSE_PRCCLIM_001 impose les
C     conditions de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C
C Sortie:
C     KDIMP          Codes des ddl imposés
C     VDIMP          Valeurs des ddl imposés
C
C Notes:
C************************************************************************
      FUNCTION SED2D_BSE_PRCCLIM_001(IL,
     &                               KCLCND,
     &                               VCLCNV,
     &                               KCLLIM,
     &                               KCLNOD,
     &                               KCLELE,
     &                               KDIMP,
     &                               VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SED2D_BSE_PRCCLIM_001
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, ID, IN, INDEB, INFIN, IV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      IF (KCLLIM(4,IL)-KCLLIM(3,IL)+1 .LT. 1) GOTO 9900
      IF (KCLCND(4,IC)-KCLCND(3,IC)+1 .NE. LM_CMMN_NDLN) GOTO 9902

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IV = KCLCND(3,IC)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
            VDIMP(ID,IN) = VCLCNV(IV)
            IV = IV + 1
         ENDDO
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_BSE_PRCCLIM_001 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM_003
C
C Description:
C     La sous-routine privée SED2D_BSE_PRCCLIM_003 impose les
C     conditions de Ouvert sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Eléments des limites
C
C Sortie:
C     KDIMP          Codes des ddl imposés
C     VDIMP          Valeurs des ddl imposés
C
C Notes: OPEN BOUNDARY CONDITION in development!!!
C************************************************************************
      FUNCTION SED2D_BSE_PRCCLIM_003(IL,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              KDIMP,
     &                              VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SED2D_BSE_PRCCLIM_003
      INTEGER IL
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, ID, IN, INDEB, INFIN, IV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      IF (KCLLIM(4,IL)-KCLLIM(3,IL)+1 .LT. 1) GOTO 9900
      IF (KCLCND(4,IC)-KCLCND(3,IC)+1 .NE. 0) GOTO 9902

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IV = KCLCND(3,IC)
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_OUVERT)
            IV = IV + 1
         ENDDO
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NBR_NOD_LIM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VAL_CND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SED2D_BSE_PRCCLIM_003 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM_NS
C
C Description:
C     The function SED2D_BSE_PRCCLIM_NS imposes previous z and
C     size fraction values for dry cells.
C
C Entrée:
C     KNGV        Connectivités de volume
C     VPRNO       Propriétés nodales
C     KDIMP       Codes des DDL imposés
C     VDIMP       Valeur des DDL imposés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SED2D_BSE_PRCCLIM_NS(KNGV,
     &                             VPRNO,
     &                             KDIMP,
     &                             VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SED2D_BSE_PRCCLIM_NS
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      SED2D_BSE_PRCCLIM_NS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_BSE_PRCCLIM_EC
C
C Description:
C     La fonction privée SED2D_BSE_PRCCLIM_EC assigne les codes
C     des éléments de contour en fonction des C.L. imposées aux
C     noeuds.
C
C Entrée:
C
C Sortie:   KNG(), VDJ() et VPREG()
C
C Notes:    VDJ (1)TXN  (2)TYN  (3)Le  (4)QN1  (5)QN2
C           *** CONCENTRATION NULLE SI NOEUD DÉCOUVERT
C
C     Les conditions limites imposées doivent être de même type pour tous
C     les degrés de libertés considérés (pour toutes les variables simulées)
C
C************************************************************************
      FUNCTION SED2D_BSE_PRCCLIM_EC(KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRES,
     &                              KDIMP,
     &                              KEIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER SED2D_BSE_PRCCLIM_EC
      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJV, EG_CMMN_NELS)
      REAL*8  VPRES(LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP(EG_CMMN_NELS)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      CHARACTER*(48) TYPCL
      LOGICAL ESTCAUCHY
      LOGICAL ESTOUVERT

      INTEGER IEP, NP1, NP2
C-----------------------------------------------------------------------
      INTEGER ID, IN
      LOGICAL DDL_ESTCAUCHY
      LOGICAL DDL_ESTOUVERT
      DDL_ESTCAUCHY(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_CAUCHY)
      DDL_ESTOUVERT(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     LES LIMITES DE CAUCHY ET OUVERT
      DO IEP=1,EG_CMMN_NELS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)

C---        LES LIMITES DE CAUCHY
         ESTCAUCHY  = (DDL_ESTCAUCHY(1,NP1) .AND. DDL_ESTCAUCHY(1,NP2))
         IF (ESTCAUCHY) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_CAUCHY)
         ENDIF

C---        LES LIMITES "OUVERT"
         ESTOUVERT  = (DDL_ESTOUVERT(1,NP1) .AND. DDL_ESTOUVERT(1,NP2))
         IF (ESTOUVERT) THEN
            KEIMP(IEP) = IBSET(KEIMP(IEP), EA_TPCL_OUVERT)
         ENDIF

      ENDDO

C---   IMPRESSION DES CONNECTIVITÉS DE LA PEAU DES FLUX DE
C      CAUCHY(ENTRE), OUVERT(NUL+SORT), DIRICHLET+NEUMAN(DÉFAUT)
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,'(A)') 'MSG_TYPE_CLIM_ELEMENTS_NOEUDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         DO IEP=1,EG_CMMN_NELS

            IF (BTEST(KEIMP(IEP), EA_TPCL_CAUCHY)) THEN
               TYPCL = 'MSG_CL_CAUCHY#<40>#'
            ELSEIF (BTEST(KEIMP(IEP), EA_TPCL_OUVERT)) THEN
               TYPCL = 'MSG_CL_OUVERT#<40>#'
            ELSE
               TYPCL = 'MSG_CL_DEFAUT#<40>#'
            ENDIF
            WRITE(LOG_BUF,'(A,2X,I9,3X,2(1X,I9))')
     &            TYPCL(1:SP_STRN_LEN(TYPCL)),
     &            IEP, KNGS(1,IEP), KNGS(2,IEP)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
         CALL LOG_DECIND()
      ENDIF

C---  IMPRESSION DE L'INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'TRAITEMENT DES ELEMENTS DE CONTOUR:'
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DE CAUCHY       =',
C     &               NELC
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. OUVERT          =',
C     &               NELS
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DIRICHLET-NEUMAN=',
C     &               EG_CMMN_NELS-NELC-NELS
C      WRITE(MP,1000)

C---------------------------------------------------------------
C1000  FORMAT(/)
C1001  FORMAT(15X,A)
C1002  FORMAT(15X,A,I12)
C---------------------------------------------------------------

      SED2D_BSE_PRCCLIM_EC = ERR_TYP()
      RETURN
      END
