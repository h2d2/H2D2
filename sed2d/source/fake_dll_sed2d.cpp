//************************************************************************
// H2D2 - External declaration of public symbols
// Module: sed2d
// Entry point: extern "C" void fake_dll_sed2d()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:11.766725
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SED2D
F_PROT(IC_SED2D_PST_DDL_CMD, ic_sed2d_pst_ddl_cmd);
F_PROT(IC_SED2D_PST_DDL_REQCMD, ic_sed2d_pst_ddl_reqcmd);
F_PROT(IC_SED2D_PST_FL_CMD, ic_sed2d_pst_fl_cmd);
F_PROT(IC_SED2D_PST_FL_REQCMD, ic_sed2d_pst_fl_reqcmd);
F_PROT(IC_SED2D_PST_SIM_XEQCTR, ic_sed2d_pst_sim_xeqctr);
F_PROT(IC_SED2D_PST_SIM_XEQMTH, ic_sed2d_pst_sim_xeqmth);
F_PROT(IC_SED2D_PST_SIM_REQCLS, ic_sed2d_pst_sim_reqcls);
F_PROT(IC_SED2D_PST_SIM_REQHDL, ic_sed2d_pst_sim_reqhdl);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_sed2d()
{
   static char libname[] = "sed2d";
 
   // ---  class IC_SED2D
   F_RGST(IC_SED2D_PST_DDL_CMD, ic_sed2d_pst_ddl_cmd, libname);
   F_RGST(IC_SED2D_PST_DDL_REQCMD, ic_sed2d_pst_ddl_reqcmd, libname);
   F_RGST(IC_SED2D_PST_FL_CMD, ic_sed2d_pst_fl_cmd, libname);
   F_RGST(IC_SED2D_PST_FL_REQCMD, ic_sed2d_pst_fl_reqcmd, libname);
   F_RGST(IC_SED2D_PST_SIM_XEQCTR, ic_sed2d_pst_sim_xeqctr, libname);
   F_RGST(IC_SED2D_PST_SIM_XEQMTH, ic_sed2d_pst_sim_xeqmth, libname);
   F_RGST(IC_SED2D_PST_SIM_REQCLS, ic_sed2d_pst_sim_reqcls, libname);
   F_RGST(IC_SED2D_PST_SIM_REQHDL, ic_sed2d_pst_sim_reqhdl, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
