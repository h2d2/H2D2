C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C     FUNCTION    SED2D_BLEQ_AW
C     SUBROUTINE  SED2D_BLEQ_AW_PARM
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Computes bedload transport rate
C
C Description:
C     The subroutine SED2D_BLEQ_AW computes bedload transport rate based on
C     Ackers and White (1973) empirical formula with a revised form of the
C     coefficients published in 1990 (HR Wallingford, 1990).
C
C Entrée:
C
C
C
C Sortie:
C     REAL*8  SED2D_BLEQ_AW          Bed load transport rate [m^2/s]
C
C Notes:
C  Reference:
C************************************************************************
      FUNCTION SED2D_BLEQ_AW(H,
     &                       UBAR,
     &                       USTR,
     &                       N,
     &                       D50,
     &                       ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_AW
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  H
      REAL*8  UBAR
      REAL*8  USTR
      REAL*8  N
      REAL*8  D50
      INTEGER ID

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_aw.fi'
      INCLUDE 'sed2d_bleq_aw.fc'
      INCLUDE 'log.fi'

      REAL*8  RD_UN
      REAL*8  FGR1, FGR2, FGR
      REAL*8  GGR
      REAL*8  QB
      REAL*8  XI
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_BLEQ .EQ. SED2D_BLEQT_AW)
D     CALL ERR_PRE(SED2D_DMT(ID) .GT. ZERO)
D     CALL ERR_PRE(H .GT. ZERO)
C-----------------------------------------------------------------------

C---     RELATIVE DENSITY
      RD_UN  = SED2D_RHOS / SED2D_RHO_EAU - UN
D     CALL ERR_ASR(RD_UN .GT. ZERO)

C---     PARTICLE MOBILITY PARAMETER (FGR)
      IF(USTR .GT. ZERO)THEN
         FGR1   = USTR**SED2D_AW_N(ID) /
     &            SQRT(SED2D_GRAVITE * RD_UN * SED2D_DMT(ID))
         FGR2   = UBAR /
     &            (SQRT(32.0D0)*LOG10(ALPHA*H/SED2D_DMT(ID)))
         FGR    = FGR1 * FGR2**(UN-SED2D_AW_N(ID))
      ELSE
         FGR = ZERO
      ENDIF

C---     SEDIMENT TRANSPORT PARAMETER (GGR)
      IF (FGR .GT. SED2D_AW_A(ID))THEN
         GGR = SED2D_AW_C(ID)*
     &         ((FGR/SED2D_AW_A(ID)-UN)**SED2D_AW_M(ID))

C---     TRANSPORT RATE
         XI = GGR * SED2D_DMT(ID) * SED2D_RHOS / SED2D_RHO_EAU / H *
     &        ((UBAR/USTR)**SED2D_AW_N(ID))
         QB = XI * H * UBAR * RD_UN
      ELSE
         QB  = ZERO
      ENDIF

D     CALL ERR_PST(QB .GE. ZERO)

      SED2D_BLEQ_AW = QB

      RETURN
      END

C************************************************************************
C Sommaire : SED2D_BLEQ_AW_PARM
C
C Description:
C     Computes parameters (N, M, A, C) of the Ackers and White (1973)
C     empirical formula with a revised form of the
C     coefficients published in 1990 (HR Wallingford, 1990).
C     The coefficients are based on best-fit curves of laboratory data
C
C
C Entrée:
C
C
C
C Sortie:
C     REAL*8  SED2D_AW_N(NC)     N
C     REAL*8  SED2D_AW_M(NC)     M
C     REAL*8  SED2D_AW_A(NC)     A
C     REAL*8  SED2D_AW_C(NC)     C
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BLEQ_AW_PARM (ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_AW_PARM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_aw.fc'
      INCLUDE 'log.fi'

      INTEGER ID

      REAL*8  RD_UN
      REAL*8  DGR
      REAL*8  DGRLOG,CLOG
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_DMT(ID) .GT. ZERO)
C-----------------------------------------------------------------------

C---     RELATIVE DENSITY
      RD_UN = SED2D_RHOS / SED2D_RHO_EAU - UN
D     CALL ERR_ASR(RD_UN .GT. ZERO)

C---     DIMENSIONLESS GRAIN DIAMETER (DGR)
      DGR = SED2D_DMT(ID) *
     &      (SED2D_GRAVITE*RD_UN / (SED2D_VISC_EAU**2))**(UN_3)

C---     LIMIT DGR
      DGR = MIN(COARSE, DGR)  !This limits the coefficients N, M, A and C
                              !to take the same values for the coarser
                              !sediment (when DGR is greater than 60)

C---     COMPUTE N
       SED2D_AW_N(ID) = N1 - N2*LOG10(DGR)

C---     COMPUTE M
       SED2D_AW_M(ID) = M1/DGR + M2

C---     COMPUTE A
       SED2D_AW_A(ID) = A1/SQRT(DGR) + A2

C---     COMPUTE C
       DGRLOG         = LOG10(DGR)
       CLOG           = -C1 + DGRLOG*(C2 - C3*DGRLOG)
       SED2D_AW_C(ID) = 10.0D0**CLOG
!D      CALL ERR_ASR(SED2D_AW_C(ID) .GT. ZERO)

      RETURN
      END
