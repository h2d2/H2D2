C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_MSS_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMF(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_ASMF
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'sed2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER   IERR

      INTEGER SED2D_NNELT3
      INTEGER SED2D_NDLEMAX
      PARAMETER (SED2D_NNELT3  =  3)
      PARAMETER (SED2D_NDLEMAX = SED2D_NNELT3*SED2D_NDLNMAX)

      INTEGER KLOCE(SED2D_NDLEMAX)
      REAL*8  VFE  (SED2D_NDLEMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. SED2D_NDLNMAX)
C-----------------------------------------------------------------

      IERR = SP_ELEM_ASMFE(LM_CMMN_NDLL, KLOCN, VSOLC, VFG)

      IF (ERR_GOOD()) THEN
         CALL SED2D_MSS_ASMF_V(KLOCE,
     &                         VFE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLC,
     &                         VSOLR,
     &                         KCLCND,
     &                         VCLCNV,
     &                         KCLLIM,
     &                         KCLNOD,
     &                         KCLELE,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         VFG)
      ENDIF

!P      IF (ERR_GOOD()) THEN
!P         CALL SED2D_MSS_ASMF_S(KLOCE,
!P     &                         VFE,
!P     &                         VCORG,
!P     &                         KLOCN,
!P     &                         KNGV,
!P     &                         KNGS,
!P     &                         VDJV,
!P     &                         VDJS,
!P     &                         VPRGL,
!P     &                         VPRNO,
!P     &                         VPREV,
!P     &                         VPRES,
!P     &                         VSOLC,
!P     &                         VSOLR,
!P     &                         KCLCND,
!P     &                         VCLCNV,
!P     &                         KCLLIM,
!P     &                         KCLNOD,
!P     &                         KCLELE,
!P     &                         KDIMP,
!P     &                         VDIMP,
!P     &                         KEIMP,
!P     &                         VDLG,
!P     &                         VFG)
!P      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_MSS_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMF_V(KLOCE,
     &                           VFE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLC,
     &                           VSOLR,
     &                           KCLCND,
     &                           VCLCNV,
     &                           KCLLIM,
     &                           KCLNOD,
     &                           KCLELE,
     &                           KDIMP,
     &                           VDIMP,
     &                           KEIMP,
     &                           VDLG,
     &                           VFG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  VKX,VKY,VEX,VEY,VSX,VSY,DETJ
      REAL*8  QBI1(SED2D_NCLSMAX)
      REAL*8  QBI2(SED2D_NCLSMAX)
      REAL*8  QBI3(SED2D_NCLSMAX)
      REAL*8  TI(SED2D_NCLSMAX)
      REAL*8  TAI1(SED2D_NCLSMAX)
      REAL*8  TAI2(SED2D_NCLSMAX)
      REAL*8  TAI3(SED2D_NCLSMAX)
      REAL*8  TSI1(SED2D_NCLSMAX)
      REAL*8  TSI2(SED2D_NCLSMAX)
      REAL*8  TSI3(SED2D_NCLSMAX)
      REAL*8  TI1,TI2,TI3
      REAL*8  DELTA,THETA1,THETA2,THETA3
      REAL*8  TSUBL1,TSUBL2,TSUBL3
      REAL*8  QB1,QB2,QB3
      REAL*8  QBX1,QBX2,QBX3
      REAL*8  QBY1,QBY2,QBY3
      REAL*8  QBIX1(SED2D_NCLSMAX)
      REAL*8  QBIX2(SED2D_NCLSMAX)
      REAL*8  QBIX3(SED2D_NCLSMAX)
      REAL*8  QBIY1(SED2D_NCLSMAX)
      REAL*8  QBIY2(SED2D_NCLSMAX)
      REAL*8  QBIY3(SED2D_NCLSMAX)
      REAL*8  DISPTAI, DTAIDX, DTAIDY
      REAL*8  DISPTSI, DTSIDX, DTSIDY
      REAL*8  F,FQ
      INTEGER IDTHETA
      INTEGER IDTLI1,IDTLI2,IDL1,IDL2
      INTEGER IDDZ
      INTEGER IDQB
      INTEGER IDVQBI,IDQ
      INTEGER IERR
      INTEGER IC,ID,IE
      INTEGER NO1,NO2,NO3
      INCLUDE 'log.fi'

C-----------------------------------------------------------------------

C---     INDICES
      IDTLI1      = 5
      IDTLI2      = IDTLI1 + SED2D_NCLASS
      IDQB        = 6 + SED2D_NCLASS * SED2D_NLAYER
      IDVQBI      = IDQB
      IDTHETA     = 7 + SED2D_NCLASS * (SED2D_NLAYER + 1)
      IDDZ        = IDTHETA + 1

C---     INITIALIZE VFE
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)

C---     INITIALIZE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO IC=1,EG_CMMN_NELCOL
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(3,IE)
         NO3  = KNGV(5,IE)

C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
         VKX    = VDJV(1,IE)
         VEX    = VDJV(2,IE)
         VKY    = VDJV(3,IE)
         VEY    = VDJV(4,IE)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)
         DETJ   = VDJV(5,IE)

C---        NODAL VALUES
         QB1   = VPRNO(IDQB,NO1)     ! Total transport rate
         THETA1= VPRNO(IDTHETA,NO1)  ! Shear stress direction
         TSUBL1= 0.0D+0              ! Sublayer thickness
         QB2   = VPRNO(IDQB,NO2)
         THETA2= VPRNO(IDTHETA,NO2)
         TSUBL2= 0.0D+0
         QB3   = VPRNO(IDQB,NO3)
         THETA3= VPRNO(IDTHETA,NO3)
         TSUBL3= 0.0D+0
         DO ID = 1,SED2D_NCLASS
            TSUBL1= TSUBL1 + VPRNO(IDTLI2+ID,NO1)
            TSUBL2= TSUBL2 + VPRNO(IDTLI2+ID,NO2)
            TSUBL3= TSUBL3 + VPRNO(IDTLI2+ID,NO3)
         ENDDO

C---        CORRECT SHEAR DIRECTION FOR HELICAL FLOW
         DELTA = VPREV(1,IE)
         THETA1= THETA1 - DELTA
         THETA2= THETA2 - DELTA
         THETA3= THETA3 - DELTA

C---        TRANSPORT RATE
C---        X-COMPONENT
         QBX1   = QB1 * COS(THETA1)
         QBX2   = QB2 * COS(THETA2)
         QBX3   = QB3 * COS(THETA3)

C---        Y-COMPONENT
         QBY1   = QB1 * SIN(THETA1)
         QBY2   = QB2 * SIN(THETA2)
         QBY3   = QB3 * SIN(THETA3)


C---        CALCULATE LOAD VECTOR FOR BED ELEVATION
CP  F = div(QS)
CP  div (QS) should be stored for sublayer update
         F    = -UN_6*(VSX*QBX1 + VKX*QBX2 + VEX*QBX3 +
     &                 VSY*QBY1 + VKY*QBY2 + VEY*QBY3)
         VPRNO(IDDZ,NO1) = VPRNO(IDDZ,NO1) + F !P not sure if this is the way to track dz
         VPRNO(IDDZ,NO2) = VPRNO(IDDZ,NO2) + F
         VPRNO(IDDZ,NO3) = VPRNO(IDDZ,NO3) + F

C---        NUMERICAL VISCOSITY FOR Z
!P REPLACE WITH VISCOSITY FOR SUBLAYER???

C---        EXCHANGE RATE FRACTIONS ARE NEEDED TO
C---        COMPUTE LOAD VECTOR FOR SIZE FRACTIONS
         DO ID = 1,SED2D_NCLASS

C---           TRANSPORT RATE FIRST LAYER SIZE FRACTIONS
            IDQ      = IDVQBI+ID
            IDL1     = IDTLI1+ID
            IDL2    = IDTLI2+ID

            QBI1(ID) = VPRNO(IDQ,NO1)!!/QB1     ! transport rate class relative to the total transport
            TAI1(ID) = VPRNO(IDL1,NO1)    ! first layer size thickness
            TSI1(ID) = VPRNO(IDL2,NO1)    ! first sublayer size thickness

            QBI2(ID) = VPRNO(IDQ,NO2)!!/QB2
            TAI2(ID) = VPRNO(IDL1,NO2)
            TSI2(ID) = VPRNO(IDL2,NO2)

            QBI3(ID) = VPRNO(IDQ,NO3)!!/QB3
            TAI3(ID) = VPRNO(IDL1,NO3)
            TSI3(ID) = VPRNO(IDL2,NO3)

C---           CALCULATE EXCHANGE RATE SIZE CLASS

C---        X-COMPONENT
         QBIX1(ID)   = QBI1(ID) * COS(THETA1) !P direction
         QBIX2(ID)   = QBI2(ID) * COS(THETA2) !P direction
         QBIX3(ID)   = QBI3(ID) * COS(THETA3) !P direction

C---        Y-COMPONENT
         QBIY1(ID)   = QBI1(ID) * SIN(THETA1) !P direction
         QBIY2(ID)   = QBI2(ID) * SIN(THETA2) !P direction
         QBIY3(ID)   = QBI3(ID) * SIN(THETA3) !P direction

            TI1     = SED2D_EXI(TAI1(ID),
     &                           VPRNO(IDL2,NO1),
     &                           TSUBL1,
     &                           QBI1(ID),
     &                           F)
            TI2     = SED2D_EXI(TAI2(ID),
     &                           VPRNO(IDL2,NO2),
     &                           TSUBL2,
     &                           QBI2(ID),
     &                           F)
            TI3     = SED2D_EXI(TAI3(ID),
     &                           VPRNO(IDL2,NO3),
     &                           TSUBL3,
     &                           QBI3(ID),
     &                           F)
            TI(ID)  = UN_3*(TI1 +TI2 + TI3)
         ENDDO

C---        ENSURE SUM OF EXCHANGE RATE FRACTIONS TO BE 100%
C NOT NEEDED FOR MASS a check on thickness could be done here
!!         CALL SED2D_NORMALIZE(SED2D_NCLASS, FI)

C---        NUMERICAL VISCOSITY FOR DTAI
         DISPTAI = SED2D_VISC_DTAI*UN_2/DETJ
C---        NUMERICAL VISCOSITY FOR DTSI
         DISPTSI = SED2D_VISC_DTSI*UN_2/DETJ

C---        CALCULATE LOAD VECTOR FOR SIZE FRACTIONS
CP FQ = div(qbi)-(Ei-fqbi)*div(QB)
         DO ID = 1,SED2D_NCLASS
            FQ = -UN_6*((VSX*QBIX1(ID) + VSY*QBIY1(ID)) +
     &                  (VKX*QBIX2(ID) + VKY*QBIY2(ID)) +
     &                  (VEX*QBIX3(ID) + VEY*QBIY3(ID)))- TI(ID)*F

C---           DERIVATIVES DTAI1DX, DTAI1DY
            DTAIDX = DISPTAI * (VSX*TAI1(ID) + VKX*TAI2(ID) +
     &                          VEX*TAI3(ID))
            DTAIDY = DISPTAI * (VSY*TAI1(ID) + VKY*TAI2(ID) +
     &                          VEY*TAI3(ID))

C---           DERIVATIVES DTS1DX, DTS1DY
CP-- CAN USE THE EXCHANCE SIZE (CHECK WITH LAYER UPDATE!!)
            DTSIDX = DISPTSI * (VSX*TSI1(ID) + VKX*TSI2(ID) +
     &                          VEX*TSI3(ID))
            DTSIDY = DISPTSI * (VSY*TSI1(ID) + VKY*TSI2(ID) +
     &                          VEY*TSI3(ID))


C---           ASSEMBLE ELEMENT VECTOR FOR SIZE FRACTIONS
            VFE(ID, 1) = FQ + VSX*DTAIDX + VSY*DTAIDY
            VFE(ID, 2) = FQ + VKX*DTAIDX + VKY*DTAIDY
            VFE(ID, 3) = FQ + VEX*DTAIDX + VEY*DTAIDY
!P (SUBL) similar for sub-layer F=(Ei-fqbi)*div(QB)
            VFE(ID+SED2D_NCLASS, 1) = TI(ID)*F + VSX*DTSIDX + VSY*DTSIDY
            VFE(ID+SED2D_NCLASS, 2) = TI(ID)*F + VKX*DTSIDX + VKY*DTSIDY
            VFE(ID+SED2D_NCLASS, 3) = TI(ID)*F + VEX*DTSIDX + VEY*DTSIDY
         ENDDO

C---        ELEMENT LOCALIZATION TABLE
         DO ID=1,LM_CMMN_NDLN !P sub-layer included with change of NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
            KLOCE(ID, 2)= KLOCN(ID, NO2)
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLE GLOBAL VECTOR
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

      ENDDO
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_MSS_ASMF
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMF_S(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES,EG_CMMN_NELV)
      REAL*8  VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8  VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      REAL*8  VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VFG   (LM_CMMN_NEQL)

      INCLUDE 'spelem.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      INTEGER IERR
      INTEGER IES, IEV
      INTEGER IDZ, IDQB, IDTH
      INTEGER NP1, NP2
      REAL*8  VNX, VNY, DJL2, REP
      REAL*8  DELTA
      REAL*8  QB1, QBX1, QBY1, THETA1
      REAL*8  QB2, QBX2, QBY2, THETA2
      REAL*8  QN1, QN2
C-----------------------------------------------------------------------
      LOGICAL ELE_ESTTYPE1
      INTEGER IE
      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     INDICES
      IDQB = 6 + SED2D_NCLASS * SED2D_NLAYER
      IDTH = 7 + SED2D_NCLASS * (SED2D_NLAYER + 1)

C---     INITIALISE VFE
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VFE,1)

C---     INITIALISE KLOCE
      CALL IINIT(LM_CMMN_NDLEV,0,KLOCE,1)

C---     BOUCLE SUR LES ELEMENTS DE SURFACE
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE1(IES)) GOTO 199

         NP1 = KNGS(1,IES)
         NP2 = KNGS(3,IES)
         IEV = KNGS(4,IES)

C---        CONSTANTE DÉPENDANT DU VOLUME
         VNX =  VDJS(2,IES)     ! VNX =  VTY
         VNY = -VDJS(1,IES)     ! VNY = -VTX
         DJL2=  VDJS(3,IES)
         REP = UN_3*DJL2

C---        NODAL VALUES
         QB1   = VPRNO(IDQB,NP1)    ! Total transport rate
         THETA1= VPRNO(IDTH,NP1)    ! Shear stress direction
         QB2   = VPRNO(IDQB,NP2)
         THETA2= VPRNO(IDTH,NP2)

C---        CORRECT SHEAR DIRECTION FOR HELICAL FLOW
         DELTA = VPREV(1,IEV)
         THETA1= THETA1 - DELTA
         THETA2= THETA2 - DELTA

C---        TRANSPORT RATE
C---        X-COMPONENT
         QBX1 = QB1 * COS(THETA1)
         QBX2 = QB2 * COS(THETA2)

C---        Y-COMPONENT
         QBY1 = QB1 * SIN(THETA1)
         QBY2 = QB2 * SIN(THETA2)

C---        FLUX NORMAL
         QN1 = QBX1*VNX + QBY1*VNY
         QN2 = QBX2*VNX + QBY2*VNY

C---       ASSEMBLAGE DU VECTEUR GLOBAL
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLES, KLOCE, VFE, VFG)

199      CONTINUE
      ENDDO

      RETURN
      END
