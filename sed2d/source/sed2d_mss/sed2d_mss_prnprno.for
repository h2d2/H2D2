C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_MSS_PRNPRNO
C
C Description:
C     PRINTING NODAL PROPERTIES:
C     1) qx
C     2) qy
C     3) H
C     4) n
C     5) zpre                                  idzpre    = 5
C     5 + 1)                tli(1,1)                         !thickness of 1st size class in active layer
C     5 + i)                tli(i,1)-----------idtli1    = 5
C     5 + nc)               tli(nc,1)
C     5 + nc + 1)           tli(1,2)                         !thickness of 1st size class in sublayer
C     5 + nc + i)           tli(i,2)-----------idtli2    = 5 + nc
C     5 + nc + nc)          tli(nc,2)
C
C     5 + (j-1)*nc + i)     tli(i,j)-----------idtlij    = 5 + (j-1)*nc + i
C
C     5 + (nl-1)*nc + 1)    tli(1,nl)
C     5 + (nl-1)*nc + i)    tli(i,nl)----------idtlinl    = 5 + nl*nc
C     5 + nl*nc)            tli(nc,nl)
C     6 + nl*nc)            qb-----------------idqb        = 6 + nl*nc !total sediment transport rate (m2/s)
C     6 + nl*nc + 1)        vqbi(1)                                    !sediment transport rate of 1st size class
C     6 + nl*nc + i)        vqbi(i)------------idvqbi    = 7 + nl*nc
C     6 + (nl+1)*nc)        vqbi(nc)
C     7 + (nl+1)*nc)        theta--------------idtheta   = 7 + (nl+1)*nc
C     7 + (nl+2)*nc)        dz-----------------iddz      = 7 + (nl+2)*nc !BED CHANGE
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C
C************************************************************************
      SUBROUTINE SED2D_MSS_PRNPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PRNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     PRINTING NODAL PROPERTIES
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('NODAL PROPERTIES READ FROM SV2D:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('X-COMPONENT VELOCITY u;')
      CALL LOG_ECRIS('Y-COMPONENT VELOCITY v;')
      CALL LOG_ECRIS('WATER DEPTH H;')
      CALL LOG_ECRIS('MANNING ROUGHNESS n;')
      CALL LOG_DECIND()

      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('NODAL PROPERTIES CALCULATED:')
      CALL LOG_INCIND()
      CALL LOG_ECRIS('PREVIOUS BED ELEVATION, ZPRE;')
      CALL LOG_ECRIS('ACTIVE LAYER SIZE CLASS THICKNESS, TLI;')
      CALL LOG_ECRIS('TOTAL SEDIMENT TRANSPORT RATE, QB;')
      CALL LOG_ECRIS('SEDIMENT TRANSPORT RATE SIZE CLASS, VQBI;')
      CALL LOG_ECRIS('MEAN FLOW DIRECTION, THETA;')
      CALL LOG_ECRIS('BED LEVEL CHANGE, DZ;')
      CALL LOG_DECIND()

      RETURN
      END
