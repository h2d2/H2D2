C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de sediment continuity 2-D
C     Élément T3 - linéaire
C
C Notes:
C     Subroutines de base.
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>SED2D_MSS_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_MSS_REQPRM(TGELV,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_REQPRM
CDEC$ ENDIF

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES

      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'eacmmn.fc'
C-----------------------------------------------------------------------

C---     INITIALISE LES PARAMETRES DE L'ÉLÉMENT PARENT
      CALL SED2D_BSE_REQPRM(TGELV,
     &                     NPRGL,
     &                     NPRGLL,
     &                     NPRNO,
     &                     NPRNOL,
     &                     NPREV,
     &                     NPREVL,
     &                     NPRES,
     &                     NSOLC,
     &                     NSOLCL,
     &                     NSOLR,
     &                     NSOLRL,
     &                     NDLN,
     &                     NDLEV,
     &                     NDLES,
     &                     ASURFACE,
     &                     ESTLIN)


C---     INITIALISATION DES PARAMETRES VARIABLES SUJETS À CHANGEMENT CHEZ L'ÉLÉMENT HÉRITIER
      NPRGLL =  NPRGLL + 1            ! NB DE PROPRIÉTÉS GLOBALES LUES ! add min sub-layer
      NPRGL  =  NPRGLL + 1            ! NB DE PROPRIÉTÉS GLOBALES ! add min sub-layer
      NPRNOL =  5                     ! NB DE PROPRIÉTÉS NODALES LUES
      NPRNO  =  NPRNOL + 3            ! NB DE PROPRIÉTÉS NODALES + QB + THETA + DELZ
      NDLN   =  0                     ! zero as only the thickness of the size class is unknown, bed elevation will be calculate from the thickness changes.

      ASURFACE = .TRUE.
      ESTLIN   = .TRUE.

      RETURN
      END

