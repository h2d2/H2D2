C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: SED2D_MSS_ASMK
C
C Description:
C     La fonction SED2D_MSS_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMK (VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'sed2d.fi'

      INTEGER SED2D_NNELT3
      INTEGER SED2D_NDLEMAX
      PARAMETER (SED2D_NNELT3  =  3)
      PARAMETER (SED2D_NDLEMAX = SED2D_NNELT3*SED2D_NDLNMAX)

      INTEGER KLOCE(SED2D_NDLEMAX)
      REAL*8  VDLE (SED2D_NDLEMAX)
      REAL*8  VKE  (SED2D_NDLEMAX, SED2D_NDLEMAX)
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. SED2D_NDLNMAX)
C-----------------------------------------------------------------

      IF (ERR_GOOD())
     &   CALL SED2D_MSS_ASMK_V(KLOCE,
     &                        VDLE,
     &                        VKE,
     &                        VCORG,
     &                        KLOCN,
     &                        KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VPRGL,
     &                        VPRNO,
     &                        VPREV,
     &                        VPRES,
     &                        VSOLR,
     &                        VDLG,
     &                        HMTX,
     &                        F_ASM)

C      IF (ERR_GOOD())
C     &   CALL SED2D_MSS_ASMK_S(KLOCE,
C     &                        VDLE,
C     &                        VKE,
C     &                        VCORG,
C     &                        KLOCN,
C     &                        KNGV,
C     &                        KNGS,
C     &                        VDJV,
C     &                        VDJS,
C     &                        VPRGL,
C     &                        VPRNO,
C     &                        VPREV,
C     &                        VPRES,
C     &                        KCLCND,
C     &                        VCLCNV,
C     &                        KCLLIM,
C     &                        KCLNOD,
C     &                        KCLELE,
C     &                        KDIMP,
C     &                        VDIMP,
C     &                        KEIMP,
C     &                        VDLG,
C     &                        HMTX,
C     &                        F_ASM)

      RETURN
      END

C************************************************************************
C Sommaire: SED2D_MSS_ASMK_V
C
C Description:
C     La fonction SED2D_MSS_ASMK_V calcul le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMK_V(KLOCE,
     &                           VDLE,
     &                           VKE,
     &                           VCORG,
     &                           KLOCN,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VSOLR,
     &                           VDLG,
     &                           HMTX,
     &                           F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'sed2d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fc'

      INTEGER IERR
      INTEGER IC, IE, ID, ID1, ID2, ID3
      INTEGER NO1, NO2, NO3
      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  DISPTAI,DISPTSI
      REAL*8  VKE11,VKE21,VKE31, VKE12,VKE22,VKE32, VKE13,VKE23,VKE33
      REAL*8  AMASTAI, VMETA11, VMETA21
      REAL*8  AMASTSI, VMETS11, VMETS21

C-----------------------------------------------------------------------

C---     INITIALIZE
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(3,IE)
         NO3  = KNGV(5,IE)

C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         DETJ = VDJV(5,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)

C---        ELEMENT CONSTANTES
         AMASTAI = SED2D_DMPG_TAI*UN_24*DETJ            ! Damping TAI
         DISPTAI = (SED2D_VISC_TAI +
     &              SED2D_VISC_DTAI) * UN_2/DETJ        ! Dispersion TAI
         AMASTSI = SED2D_DMPG_TSI*UN_24*DETJ            ! Damping TSI
         DISPTSI = (SED2D_VISC_TSI +
     &              SED2D_VISC_DTSI) * UN_2/DETJ        ! Dispersion TSI

C---        TERMS COMMON TO ALL DEGREES OF FREEDOM
         VKE11 = VSX*VSX + VSY*VSY
         VKE21 = VKX*VSX + VKY*VSY
         VKE31 = VEX*VSX + VEY*VSY
         VKE12 = VSX*VKX + VSY*VKY
         VKE22 = VKX*VKX + VKY*VKY
         VKE32 = VEX*VKX + VEY*VKY
         VKE13 = VSX*VEX + VSY*VEY
         VKE23 = VKX*VEX + VKY*VEY
         VKE33 = VEX*VEX + VEY*VEY

C---           MASS MATRIX FOR DAMPING
         VMETA11 = AMASTAI+AMASTAI    ! VME_II = AMAS*DEUX FOR I  = J
         VMETA21 = AMASTAI            ! VME_IJ = AMAS*UN   FOR I /= J
         VMETS11 = AMASTSI+AMASTSI    ! VME_II = AMAS*DEUX FOR I  = J
         VMETS21 = AMASTSI            ! VME_IJ = AMAS*UN   FOR I /= J

C---        CALCULATE MATRIX DUE TO DIFFUSION TRANSPORT - TAI
         DO ID=1,SED2D_NCLASS
CP for ID=1:Nclass to have Dispersion for active layer and sublayer separate??
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN

C---           ASSEMBLE ELEMENT MATRIX
            VKE(ID1,ID1) = VMETA11 + DISPTAI*VKE11
            VKE(ID2,ID1) = VMETA21 + DISPTAI*VKE21
            VKE(ID3,ID1) = VMETA21 + DISPTAI*VKE31
            VKE(ID1,ID2) = VMETA21 + DISPTAI*VKE12
            VKE(ID2,ID2) = VMETA11 + DISPTAI*VKE22
            VKE(ID3,ID2) = VMETA21 + DISPTAI*VKE32
            VKE(ID1,ID3) = VMETA21 + DISPTAI*VKE13
            VKE(ID2,ID3) = VMETA21 + DISPTAI*VKE23
            VKE(ID3,ID3) = VMETA11 + DISPTAI*VKE33

            ID1 = ID+SED2D_NCLASS
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN
            VKE(ID1,ID1) = VMETS11 + DISPTSI*VKE11
            VKE(ID2,ID1) = VMETS21 + DISPTSI*VKE21
            VKE(ID3,ID1) = VMETS21 + DISPTSI*VKE31
            VKE(ID1,ID2) = VMETS21 + DISPTSI*VKE12
            VKE(ID2,ID2) = VMETS11 + DISPTSI*VKE22
            VKE(ID3,ID2) = VMETS21 + DISPTSI*VKE32
            VKE(ID1,ID3) = VMETS21 + DISPTSI*VKE13
            VKE(ID2,ID3) = VMETS21 + DISPTSI*VKE23
            VKE(ID3,ID3) = VMETS11 + DISPTSI*VKE33
         ENDDO

C---        ASSIGN PETIT WHEN DIAGONAL TERM IS ZERO
         DO ID=1,LM_CMMN_NDLEV
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = DETJ*PETIT
         ENDDO

C---        ELEMENT LOCALIZATION TABLE
         DO ID=1,LM_CMMN_NDLN !P sub-layer included with change of NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
            KLOCE(ID, 2)= KLOCN(ID, NO2)
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLE GLOBAL MATRIX
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_V',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: SED2D_MSS_ASMK_S
C
C Description:
C     La fonction SED2D_MSS_ASMK_S calcule le matrice de rigidité
C     élémentaire due aux éléments de surface. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
C      SUBROUTINE SED2D_MSS_ASMK_S(KLOCE,
C     &                           VDLE,
C     &                           VKE,
C     &                           VCORG,
C     &                           KLOCN,
C     &                           KNGV,
C     &                           KNGS,
C     &                           VDJV,
C     &                           VDJS,
C     &                           VPRGL,
C     &                           VPRNO,
C     &                           VPREV,
C     &                           VPRES,
C     &                           KCLCND,
C     &                           VCLCNV,
C     &                           KCLLIM,
C     &                           KCLNOD,
C     &                           KCLELE,
C     &                           KDIMP,
C     &                           VDIMP,
C     &                           KEIMP,
C     &                           VDLG,
C     &                           HMTX,
C     &                           F_ASM)

C      IMPLICIT REAL*8 (A-H, O-Z)
C
C      INCLUDE 'eacdcl.fi'
C      INCLUDE 'eacnst.fi'
C      INCLUDE 'eacmmn.fc'
C      INCLUDE 'egcmmn.fc'
C
C      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
C      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
C      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
C      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
C      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
C      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
C      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
C      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
C      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
C      REAL*8   VPRGL (LM_CMMN_NPRGL)
C      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
C      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
C      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
C      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
C      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
C      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
C      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
C      INTEGER  KCLELE(    EG_CMMN_NCLELE)
C      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
C      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
C      INTEGER  KEIMP (EG_CMMN_NELS)
C      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
C      INTEGER  HMTX
C      INTEGER  F_ASM
C      EXTERNAL F_ASM
C      INCLUDE 'err.fi'
C      INCLUDE 'log.fi'
C      INCLUDE 'sed2d_cnst.fi'
C
C      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJT3
C      REAL*8  VNX, VNY, DETJL2
C      REAL*8  DISC
C      REAL*8  DIF11,DIF12,DIF13,DIF21,DIF22,DIF23
C      REAL*8  VKT11,VKT21,VKT12,VKT22,VKT13,VKT23
C      REAL*8  AKXX1,AKXX2,AKXY1,AKXY2,AKYY1,AKYY2
C      INTEGER IERR
C      INTEGER IEC,ID,IE,ID1,ID2,ID3
C      INTEGER NP1,NP2,NP3,NO1,NO2,NO3
C-----------------------------------------------------------------------
C      LOGICAL ELE_ESTTYPE1
C      LOGICAL ELE_ESTTYPE2
C      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_ENTRANT) .AND.
C     &                   (BTEST(KEIMP(IE), EA_TPCL_CAUCHY) .OR.
C     &                    BTEST(KEIMP(IE), EA_TPCL_OUVERT))
C      ELE_ESTTYPE2(IE) = BTEST(KEIMP(IE), EA_TPCL_SORTANT) .AND.
C     &                   BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     PARAMETERS
C         AKXX1 = SED2D_VISC_ART_XX   !for boundary conditions???
C         AKXX2 = SED2D_VISC_ART_XX
C         AKXY1 = SED2D_VISC_ART_XY
C         AKXY2 = SED2D_VISC_ART_XY
C         AKYY1 = SED2D_VISC_ART_YY
C         AKYY2 = SED2D_VISC_ART_YY

C---     INITIALISE
C      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
C      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
C      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---- BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR OUVERTS "SORTANTS"
C     ==============================================================
C      DO IEC=1,EG_CMMN_NELS
C         IF (.NOT. ELE_ESTTYPE2(IEC)) GOTO 299        !and for "ENTRANTS"???

C         NP1    = KNGS(1,IEC)
C         NP2    = KNGS(2,IEC)
C         IE     = KNGS(3,IEC)
C         NO1    = KNGV(1,IE )
C         NO2    = KNGV(2,IE )
C         NO3    = KNGV(3,IE )

C---        RECHERCHE LE TROISIEME NOEUD DE L'ELEMENT DE VOLUME
C         IF(     NP1 .EQ. NO1 .AND. NP2 .EQ. NO2) THEN
C            NP3 = NO3
C         ELSEIF(NP1 .EQ. NO2 .AND. NP2 .EQ. NO3) THEN
C            NP3 = NO1
C         ELSEIF(NP1 .EQ. NO3 .AND. NP2 .EQ. NO1) THEN
C            NP3 = NO2
CD        ELSE
CD           CALL ERR_ASR(.FALSE.)
C         ENDIF
C
C---        MÉTRIQUES DU T3 PARENT (recalculées car permutés)
C         VKX    = VCORG(2,NP3) - VCORG(2,NP1)               ! Ksi,x
C         VEX    = VCORG(2,NP1) - VCORG(2,NP2)               ! Eta,x
C         VKY    = VCORG(1,NP1) - VCORG(1,NP3)               ! Ksi,y
C         VEY    = VCORG(1,NP2) - VCORG(1,NP1)               ! Eta,y
C         DETJT3 = VDJV(5,IE)
C         VSX    = -(VKX+VEX)
C         VSY    = -(VKY+VEY)

C---       MÉTRIQUES DE L'ÉLÉMENT - COMPOSANTES DE LA NORMALE EXTÉRIEURE
C         VNY    = -VDJS(1,IEC)
C         VNX    =  VDJS(2,IEC)
C         DETJL2 =  VDJS(3,IEC)

C---       CONSTANTE DÉPENDANT DU CONTOUR
C         DISC = -UN_2*DETJL2/DETJT3

C---      TERMES DE CONTOUR
C         DIF11 = DISC*( (AKXX1 * VSX + AKXY1 * VSY ) * VNX
C     &         +        (AKXY1 * VSX + AKYY1 * VSY ) * VNY )
C         DIF12 = DISC*( (AKXX1 * VKX + AKXY1 * VKY ) * VNX
C     &         +        (AKXY1 * VKX + AKYY1 * VKY ) * VNY )
C         DIF13 = DISC*( (AKXX1 * VEX + AKXY1 * VEY ) * VNX
C     &         +        (AKXY1 * VEX + AKYY1 * VEY ) * VNY )
C         DIF21 = DISC*( (AKXX2 * VSX + AKXY2 * VSY ) * VNX
C     &         +        (AKXY2 * VSX + AKYY2 * VSY ) * VNY )
C         DIF22 = DISC*( (AKXX2 * VKX + AKXY2 * VKY ) * VNX
C     &         +        (AKXY2 * VKX + AKYY2 * VKY ) * VNY )
C         DIF23 = DISC*( (AKXX2 * VEX + AKXY2 * VEY ) * VNX
C     &         +        (AKXY2 * VEX + AKYY2 * VEY ) * VNY )

C---       MATRICE TANGENTE ELEMENTAIRE DE FRONTIERE OUVERTE
C         VKT11 = DIF11
C         VKT21 = DIF21
C         VKT12 = DIF12
C         VKT22 = DIF22
C         VKT13 = DIF13
C         VKT23 = DIF23

C---       BOUCLE D'ASSEMBLAGE SUR LES DDL
C         DO ID=1,LM_CMMN_NDLN
C            ID1 = ID
C            ID2 = ID1+LM_CMMN_NDLN
C            ID3 = ID2+LM_CMMN_NDLN
C            VKE(ID1,ID1) = VKT11
C            VKE(ID2,ID1) = VKT21
C            VKE(ID1,ID2) = VKT12
C            VKE(ID2,ID2) = VKT22
C            VKE(ID1,ID3) = VKT13
C            VKE(ID2,ID3) = VKT23
C         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
C         DO ID=1,LM_CMMN_NDLN
C            KLOCE(ID, 1)= KLOCN(ID, NP1)
C            KLOCE(ID, 2)= KLOCN(ID, NP2)
C            KLOCE(ID, 3)= KLOCN(ID, NP3)
C         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
CD        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
CD           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_S2',': ',IEC
CD           CALL LOG_ECRIS(LOG_BUF)
CD        ENDIF
C         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
CD        IF (IERR .NE. ERR_OK) THEN
CD           WRITE(LOG_BUF,'(2A,I9)')
CD    &         'ERR_CALCUL_MATRICE_K_CAUCHY_SORTANT', ': ', IEC
CD           CALL LOG_ECRIS(LOG_BUF)
CD        ENDIF

C299      CONTINUE
C      ENDDO
C
C      RETURN
C      END
