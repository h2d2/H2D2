C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_MSS_PRCPREV
C
C Description:
C     Computes direction of shear over each element and corrects
C     for helical flow effects using the procedures of Abad et al. (2008)
C     Should compute the divergence of qb (dz).
C
C Entrée:
C
C Sortie:
C     REAL*8 VPREV(1,EG_CMMN_NELV)     !deviation of shear direction due
C                                      !to helical flow and bed slope effects
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_PRCPREV(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PRCPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV, EG_CMMN_NELV)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  VKX,VEX,VKY,VEY,VSX,VSY,DETJ
      REAL*8  TLI1(SED2D_NCLSMAX)
      REAL*8  U1,V1,H1,N1,ZPRE1,U2,V2,H2,N2,ZPRE2,U3,V3,H3,N3,ZPRE3
      REAL*8  U,V,H,N,UBAR2
      REAL*8  DUDX,DUDY,DVDX,DVDY,DZDX,DZDY
      REAL*8  RC1,RC2,R,DELTAH
      REAL*8  USTR,TAUSTR
      REAL*8  D50
      REAL*8  DELTAX,DELTAY,DELTA
      REAL*8  TROIS_DEUX
      REAL*8  UN_FS
      REAL*8  A
      REAL*8  RD_UN
      INTEGER IDTLI1,IDL1
      INTEGER IC,IE,ID
      INTEGER NO1, NO2, NO3

      INTEGER IDDZ,IDZP,IN
      INTEGER IDQB,IDTHETA
      REAL*8  ZP1,ZP2,ZP3
      REAL*8  QB1,QB2,QB3,QBX1,QBX2,QBX3,QBY1,QBY2,QBY3
      REAL*8  THETA1,THETA2,THETA3
      REAL*8  DELZ(EG_CMMN_NNL),DZ
C-----------------------------------------------------------------------
D     CALL LOG_TODO('sed2d_mss_prcprev')
D     CALL LOG_ECRIS('does not do anything (calls bse_prcprev)')
D     CALL LOG_ECRIS('needed for direction of transport?')
!D     CALL ERR_ASR(.FALSE.)
CP-- let the base calulate the direction of shear stress
      CALL SED2D_BSE_PRCPREV(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       VPREV,
     &                       IERR)

C---     PARAMETERS
!      IDZP        = 5
!P      IDTLI1      = 5
!      IDQB        = 6 + SED2D_NLAYER*SED2D_NCLASS
!      IDTHETA     = 7 + (SED2D_NLAYER+1)*SED2D_NCLASS
!P      IDDZ        = 7 + (SED2D_NLAYER+2)*SED2D_NCLASS
!       IDDZ       = IDTHETA +1
!      TROIS_DEUX  =  1.50D0
!      UN_FS       =  SED2D_FS
!      A           =  SED2D_HELA
!C-------  LOOP OVER NODES
!C         ================
!P      WRITE(LOG_BUF,'(A)') 'Set VPRNO to zero for dz: '
!P      CALL LOG_ECRIS(LOG_BUF)
!      DO IN = 1,EG_CMMN_NNL
!         DELZ(IN) = 0.0D+0
!P         VPRNO(IDDZ,IN) = 0.0D+0
!P         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'VPRNO (DZ): ',VPRNO(IDDZ,IN)
!P         CALL LOG_ECRIS(LOG_BUF)
!      ENDDO

!C-------  LOOP OVER THE ELEMENTS
!C         =======================
!      DO IC = 1,EG_CMMN_NELCOL
!      DO IE = EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
!C---       Divergence of Qb i.e. dz
!C---        ELEMENT CONNECTIVITY FOR T3
!         NO1  = KNGV(1,IE)
!         NO2  = KNGV(3,IE)
!         NO3  = KNGV(5,IE)

!C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
!         VKX    = VDJV(1,IE)
!         VEX    = VDJV(2,IE)
!         VKY    = VDJV(3,IE)
!         VEY    = VDJV(4,IE)
!         VSX    = -(VKX+VEX)
!         VSY    = -(VKY+VEY)
!!         DETJ   = VDJV(5,IE)

!C---        NODAL VALUES
!         ZP1   = VPRNO(IDZP,NO1)    ! Z previous
!         QB1   = VPRNO(IDQB,NO1)    ! Total transport rate
!         THETA1= VPRNO(IDTHETA,NO1) ! Shear stress direction
!         ZP2   = VPRNO(IDZP,NO2)
!         QB2   = VPRNO(IDQB,NO2)
!         THETA2= VPRNO(IDTHETA,NO2)
!         ZP3   = VPRNO(IDZP,NO3)
!         QB3   = VPRNO(IDQB,NO3)
!         THETA3= VPRNO(IDTHETA,NO3)
!C---        CORRECT SHEAR DIRECTION FOR HELICAL FLOW
!         DELTA = VPREV(1,IE)
!         THETA1= THETA1 - DELTA
!         THETA2= THETA2 - DELTA
!         THETA3= THETA3 - DELTA

!C---        TRANSPORT RATE
!C---        X-COMPONENT
!         QBX1   = QB1 !P* COS(THETA1)
!         QBX2   = QB2 !P* COS(THETA2)
!         QBX3   = QB3 !P* COS(THETA3)

!C---        Y-COMPONENT
!         QBY1   = 0.0D+0 !PQB1 * SIN(THETA1)
!         QBY2   = 0.0D+0 !PQB2 * SIN(THETA2)
!         QBY3   = 0.0D+0 !PQB3 * SIN(THETA3)

!         DZ = (VSX*QBX1 + VKX*QBX2 + VEX*QBX3)
!     &        +  (VSY*QBY1 + VKY*QBY2 + VEY*QBY3)
!         WRITE(LOG_BUF,'(A,I4,A,1PE14.6E3)') 'IE: ',IE,' DZ: ',DZ
!         CALL LOG_ECRIS(LOG_BUF)
!P         DELZ(NO1)=DELZ(NO1)+VSX*QBX1+VSY*QBY1
!P         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'DELZ1: ',DELZ(NO1)
!P         CALL LOG_ECRIS(LOG_BUF)
!P         DELZ(NO2)=DELZ(NO2)+VKX*QBX2+VKY*QBY2
!P         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'DELZ2: ',DELZ(NO2)
!P         CALL LOG_ECRIS(LOG_BUF)
!P         DELZ(NO3)=DELZ(NO3)+VEX*QBX3+VEY*QBY3
!P         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'DELZ3: ',DELZ(NO3)
!P         CALL LOG_ECRIS(LOG_BUF)
!PP         VPRNO(IDDZ,NO1)=VPRNO(IDDZ,NO1)+VSX*QBX1+VSY*QBY1
!PP         VPRNO(IDDZ,NO2)=VPRNO(IDDZ,NO2)+VKX*QBX2+VKY*QBY2
!PP         VPRNO(IDDZ,NO3)=VPRNO(IDDZ,NO3)+VEX*QBX3+VEY*QBY3
!          DELZ(NO1)=DELZ(NO1)+VSX*QBX1+VSY*QBY1
!          DELZ(NO2)=DELZ(NO2)+VKX*QBX2+VKY*QBY2
!          DELZ(NO3)=DELZ(NO3)+VEX*QBX3+VEY*QBY3
!      ENDDO
!      ENDDO

!P      WRITE(LOG_BUF,'(A)') 'VPRNO after calculation of divergence: '
!P      CALL LOG_ECRIS(LOG_BUF)
!      DO IN = 1,EG_CMMN_NNL
!         VPRNO(IDDZ,IN) = DELZ(IN)
!P         VPRNO(IDDZ,IN)=-1.2345D-04
!         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'DELZ: ',DELZ(IN)
!         CALL LOG_ECRIS(LOG_BUF)
!P         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'VPRNO (DZ): ',VPRNO(IDDZ,IN)
!P         CALL LOG_ECRIS(LOG_BUF)
!      ENDDO



















C---        ELEMENT CONNECTIVITY FOR T3
!         NO1 = KNGV(1,IE)
!         NO2 = KNGV(3,IE)
!         NO3 = KNGV(5,IE)

C---        TABLE FOR INVERSE OF JACOBIAN MATRIX FOR T3
!         VKX  = VDJV(1,IE)
!         VEX  = VDJV(2,IE)
!         VKY  = VDJV(3,IE)
!         VEY  = VDJV(4,IE)
!         VSX  = -(VKX+VEX)
!         VSY  = -(VKY+VEY)
!         DETJ = VDJV(5,IE)

C---        BED ELEVATION, FLOW DEPTH, VELOCITY AND ROUGHNESS
!         ZPRE1 = VPRNO(5,NO1)
!         H1    = MAX(PROFMIN, (VPRNO(3,NO1)- ZPRE1))
!         U1    = VPRNO(1,NO1) / H1
!         V1    = VPRNO(2,NO1) / H1
!         N1    = VPRNO(4,NO1)
!         ZPRE2 = VPRNO(5,NO2)
!         H2    = MAX(PROFMIN, (VPRNO(3,NO2)- ZPRE2))
!         U2    = VPRNO(1,NO2) / H2
!         V2    = VPRNO(2,NO2) / H2
!         N2    = VPRNO(4,NO2)
!         ZPRE3 = VPRNO(5,NO3)
!         H3    = MAX(PROFMIN, (VPRNO(3,NO3)- ZPRE3))
!         U3    = VPRNO(1,NO3) / H3
!         V3    = VPRNO(2,NO3) / H3
!         N3    = VPRNO(4,NO3)

C---        MEAN VALUES
!         U     = UN_3*(U1 + U2 + U3)
!         V     = UN_3*(V1 + V2 + V3)
!         H     = UN_3*(H1 + H2 + H3)
!         N     = UN_3*(N1 + N2 + N3)
!         IF(H .GT. PROFMIN)THEN
!            UBAR2 = U*U + V*V
!         ELSE
!            UBAR2 = ZERO
!         ENDIF

C---       DERIVATIVES DUDX, DUDY, DVDX, DVDY, DZDX, DZDY
!         DUDX  = (VSX*U1 + VKX*U2 + VEX*U3) / DETJ
!         DUDY  = (VSY*U1 + VKY*U2 + VEY*U3) / DETJ
!         DVDX  = (VSX*V1 + VKX*V2 + VEX*V3) / DETJ
!         DVDY  = (VSY*V1 + VKY*V2 + VEY*V3) / DETJ
!         DZDX  = (VSX*ZPRE1 + VKX*ZPRE2 + VEX*ZPRE3) / DETJ
!         DZDY  = (VSY*ZPRE1 + VKY*ZPRE2 + VEY*ZPRE3) / DETJ

C---       BED SHEAR DIRECTION CORRECTION FOR HELICAL FLOW
!         RC1      =  UBAR2**TROIS_DEUX
!         RC2      =  U*U*DVDX - V*V*DUDY + U*V*(DVDY-DUDX)
!         IF(RC2 .EQ. ZERO)THEN
!           DELTAH   =  ZERO
!         ELSE
!           R = RC1 / RC2
!           DELTAH = ATAN(A*H/R)    ! A*H is always positive, no need to use ATAN2
!         ENDIF

C---        BED SHEAR DIRECTION CORRECTION FOR TRANSVERSAL BED SLOPE
C---        COMPUTE D50 FOR ACTIVE LAYER SIZE FRACTIONS
!         DO ID = 1, SED2D_NCLASS
!            IDL1      = IDTLI1+ID
!            TLI1(ID)  = UN_3*(VPRNO(IDL1,NO1) +
!     &                        VPRNO(IDL1,NO2) +
!     &                        VPRNO(IDL1,NO3))
!         ENDDO
!         D50 = SED2D_MSS_SIZEPARM(SED2D_NCLASS,
!     &                            0.5D0,
!     &                            TLI1)

C---        COMPUTE TOTAL BED SHEAR VELOCITY USING MANNING LAW
!          IF(SED2D_FRCT_TOTAL .EQ. SED2D_FRCT_TOTAL_MAN)THEN
!             USTR = SQRT(SED2D_GRAVITE*UBAR2)*N/(H**UN_6)

C---        COMPUTE TOTAL BED SHEAR VELOCITY USING MODIFIED MANNING LAW !modified to produce smooth stress
                                                                        !distribution in complex bed setup
!          ELSEIF(SED2D_FRCT_TOTAL .EQ. SED2D_FRCT_TOTAL_MODIF_MAN)THEN
!             USTR = SQRT(SED2D_GRAVITE*UBAR2)*N*H**UN_6
!          ELSE
!            CALL ERR_ASR(SED2D_FRCT_TOTAL .EQ.
!     &                   SED2D_FRCT_TOTAL_UNDEFINED)
!          ENDIF

C---        COMPUTE TOTAL BED SHEAR STRESS
!         TAUSTR   =  SED2D_RHO_EAU*USTR*USTR

C---        CORRECT FOR TRANSVERSAL BED SLOPE
!         IF(TAUSTR .GT. ZERO)THEN
!!            DELTAY   = SIN(DELTAH) - UN_FS*DZDY/TAUSTR
!!            DELTAX   = COS(DELTAH) - UN_FS*DZDX/TAUSTR
!!            DELTA    = ATAN2(DELTAY,DELTAX)
!            DELTAY = SIN(DELTAH) - UN_FS/TAUSTR*ATAN(DZDY)
!            DELTAX = COS(DELTAH) - UN_FS/TAUSTR*ATAN(DZDX)
!            DELTA  = ATAN2(DELTAY,DELTAX)
!         ELSE
!            DELTA = ZERO
!         ENDIF

!         VPREV(1,IE) = DELTA

!      ENDDO
!      ENDDO

      IERR = ERR_TYP()
      RETURN
      END
