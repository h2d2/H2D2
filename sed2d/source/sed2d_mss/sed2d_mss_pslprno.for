C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : SED2D_MSS_PSLPRNO
C
C Description:
C     Traitement post-lecture des propriétés nodales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_MSS_PSLPRNO (VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PSLPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'sed2d_cnst.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'log.fi'

      INTEGER IDTLI,IN,ID
      REAL*8 THCKLA,THCKSUBL
      REAL*8 THCKLA2,THCKSUBL2
C-----------------------------------------------------------------------

      IDTLI = 5 !thichness SIZECLASS in layers
C---     Loop on the nodes
      DO IN = 1,EG_CMMN_NNL

C---     CONTROLE DE POSITIVITE DE SUBLAYER
!! the sum of the input files should be checked here
         IERR = 0
         DO ID = 1, SED2D_NCLASS*SED2D_NLAYER
         IF (VPRNO(IDTLI+ID,IN) .LT. ZERO) THEN
            CALL LOG_ECRIS('ERR_PROP_NODAL_NEGATIVE:')
            CALL LOG_INCIND()
            IERR = -1
            CALL LOG_ECRIS('Negative thickness')
            WRITE(LOG_BUF,'(2(A,I6),A,1PE15.8,A,I6)') 'IN: ',
     &      IN,' ID-LAYER: ',IDTLI+ID,' VPRNO: ',VPRNO(IDTLI+ID,IN),
     &      'NLAYER: ',SED2D_NLAYER
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         ENDDO

C---     Loop for size class check thickness active layer and sublayer
         THCKLA   = 0.0D0
         THCKSUBL = 0.0D0
         DO ID = 1, SED2D_NCLASS
            THCKLA   = THCKLA + VPRNO(IDTLI+ID,IN)
            THCKSUBL = THCKSUBL + VPRNO(IDTLI+ID+SED2D_NCLASS,IN)
         ENDDO

      IF (IERR .EQ. -1) THEN
         CALL LOG_ECRIS('Error pslprno')
         IERR = ERR_TYP()
         RETURN
      ENDIF
      ENDDO

!!C---     PROPRIÉTÉ EGALE A PETIT SI ZERO
!!      VPRGL(IP) = MAX(VPRGL(IP), PETIT)

C---     APPEL L'ELEMENT DE BASE
!P      CALL SED2D_BSE_PSLPRNO (VPRNO, IERR)


      IERR = ERR_TYP()
      RETURN
      END

