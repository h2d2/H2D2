C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  SED2D_MSS_PSCPRNO
C
C Description:
C     1) stores the computed z as nodal value
C     2) updates the bed layer thickness and size fractions
C
C Entrée: VCORG,KNG,VDJ,VPRGL,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C  La boucle d'imposition des noeuds milieu devait être dans un PSCDLIB
C************************************************************************
      SUBROUTINE SED2D_MSS_PSCPRNO(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             MODIF,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_PSCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      LOGICAL MODIF
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'
      INCLUDE 'sed2d_cnst.fi'

      REAL*8  TLI (SED2D_NLAYER, SED2D_NCLSMAX)
      REAL*8  TEXI(SED2D_NCLSMAX)
      REAL*8  TAI (SED2D_NCLSMAX)
      REAL*8  DELZ
      REAL*8  TLA,TL2 !P OBSOLETE?
      INTEGER IC, IE
      INTEGER NO1, NO2, NO3, NO4, NO5, NO6
      INTEGER ID,IL,IN,IDZ,IDTL2,IDTLI,IDVQBI,IDTL,IX

      INTEGER IDDZ
      REAL*8  DTAI(SED2D_NCLASS),DTL2I(SED2D_NCLASS),TL2OLD
      REAL*8  TLNEW(2,SED2D_NCLASS)
      INTEGER NORM
C-----------------------------------------------------------------------

C---     INDICES
      IDZ       = 5
      IDDZ      = 8 + (SED2D_NLAYER+1)*SED2D_NCLASS
      IDTLI     = 5
      IDTL2     = IDTLI + SED2D_NCLASS
      IDVQBI    = 7 + SED2D_NCLASS*SED2D_NLAYER

C---     Impose les noeuds milieux linéaire
!$omp  parallel
!$omp& default(shared)
!$omp& private(IE, IC, ID)
!$omp& private(NO1, NO2, NO3, NO4, NO5, NO6)
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)
         NO1 = KNGV(1,IE)
         NO2 = KNGV(2,IE)
         NO3 = KNGV(3,IE)
         NO4 = KNGV(4,IE)
         NO5 = KNGV(5,IE)
         NO6 = KNGV(6,IE)

         DO ID=1,LM_CMMN_NDLN !sub-layer included by NDLN
            VDLG(ID,NO2) = UN_2*(VDLG(ID,NO1) + VDLG(ID,NO3))
            VDLG(ID,NO4) = UN_2*(VDLG(ID,NO3) + VDLG(ID,NO5))
            VDLG(ID,NO6) = UN_2*(VDLG(ID,NO5) + VDLG(ID,NO1))
         ENDDO
         VPRNO(IDDZ,NO2) = UN_2*(VPRNO(IDDZ,NO1) + VPRNO(IDDZ,NO3))
         VPRNO(IDDZ,NO4) = UN_2*(VPRNO(IDDZ,NO3) + VPRNO(IDDZ,NO5))
         VPRNO(IDDZ,NO6) = UN_2*(VPRNO(IDDZ,NO5) + VPRNO(IDDZ,NO1))
      ENDDO
!$omp end do
      ENDDO
!$omp end parallel


C---     LOOP OVER THE NODES
      DO IN = 1, EG_CMMN_NNL
         DO ID = 1,SED2D_NCLASS
            TAI(ID) = VDLG(ID,IN)
         ENDDO

C---        UPDATE BED LAYER DATA

C---        NEW THICKNESS OF ACTIVE AND SUBLAYER
         TLA = 0.0D0
         TL2 = 0.0D0
         DO ID = 1, SED2D_NCLASS
            TLA = TLA + VDLG(ID,IN)
            TL2 = TL2 + VDLG(SED2D_NCLASS+ID,IN)
            TLNEW(1,ID) = VDLG(ID,IN)
            TLNEW(2,ID) = VDLG(SED2D_NCLASS+ID,IN)
         ENDDO

C---        COMPUTE CHANGE IN BED ELEVATION
         DELZ = 0.0D+0
         DO ID = 1,SED2D_NCLASS
            DTAI(ID) = VDLG(ID,IN)-VPRNO(IDTLI+ID,IN)
            DTL2I(ID)= VDLG(ID+SED2D_NCLASS,IN)-VPRNO(IDTL2+ID,IN)
            DELZ = DELZ + DTL2I(ID)!P + DTAI(ID) for variable active layer thickness
         ENDDO


C--- UPDATE BED LAYER SIZE FRACTIONS IF NEEDED TS<0 OR TS>1.5*LA !!SL
         IF (TL2 .LE. SED2D_SUBL .OR. TL2 .GE. 1.5D0*SED2D_LA) THEN
            TL2OLD=TL2-DELZ
C---        PREVIOUS THICKNESS OF BEDLAYER DATA
            IDTL  = IDTLI
            DO IL = 1, SED2D_NLAYER
               DO ID = 1, SED2D_NCLASS
                  IDTL       = IDTL + 1
                  TLI(IL,ID) = VPRNO(IDTL,IN)
               ENDDO
            ENDDO
            CALL SED2D_MSS_UPDATE(SED2D_NCLASS,
     &                            TLI,
     &                            TLNEW,
     &                            TL2,
     &                            TL2OLD)

C-- UPDATE VPRNO AND VDLG
            IDTL  = IDTLI
            DO IL = 1,SED2D_NLAYER
               DO ID = 1, SED2D_NCLASS
                  IDTL  = IDTL + 1
                  VPRNO(IDTL,IN)=TLI(IL,ID)
CP-- UPDATE THE ACTIVE LAYER IN VDLG
                  IF (IL .EQ. 1) THEN
                     VDLG(ID,IN)=TLI(IL,ID)
CP-- UPDATE THE SUBLAYER IN VDLG
                  ELSEIF (IL .EQ. 2) THEN
                     VDLG(ID+SED2D_NCLASS,IN)=TLI(IL,ID)
                  ENDIF
               ENDDO
            ENDDO
         ENDIF

C-- UPDATE VPRNO FOR NEXT TIME STEP
         DO ID = 1,SED2D_NCLASS*2 !P LM_CMMN_NDLN
            VPRNO(IDTLI+ID,IN) = VDLG(ID,IN)
         ENDDO

C---        UPDATE BED ELEVATION
         VPRNO(IDZ,IN)  = VPRNO(IDZ,IN) + DELZ
         VPRNO(IDDZ,IN) = DELZ


!         CALL ERR_ASR(TSPRE+DELZ .EQ. TL2) !stop when sublayer thickness is different
         !!PP remark this should be changed of the update of sublayer
D        IDTL  = 0 !P IDTLI
D        DO ID = 1,SED2D_NCLASS
D           CALL ERR_ASR(TAI(ID) .GT. ZERO) !THICKNESS is positive
D           CALL ERR_ASR(TAI(ID) .LE. SED2D_LA) !thickness cant be more then the total thickness
D           DO IL = 1,2 !P SED2D_NLAYER
D              IDTL=IDTL+1
D              CALL ERR_ASR(VDLG(IDTL,IN) .GT. ZERO)
D              CALL ERR_ASR(VDLG(IDTL,IN) .LE. 1.5*SED2D_LA)
D           ENDDO
D        ENDDO
      ENDDO

      MODIF = .TRUE.
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  SED2D_MSS_UPDATE
C
C Description:
C     1) updates the bed layer thickness and size fractions
C
C Entrée:
C
C Sortie:
C
C Notes: 1) TL contains the previous time step and is updated for the
C           new thicknesses, active layer needs to be update in both cases
C
C************************************************************************
      SUBROUTINE SED2D_MSS_UPDATE(NCLASS,
     &                            TL,
     &                            TLNEW,
     &                            T2,
     &                            T2OLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_UPDATE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sed2d_cnst.fi'

      INTEGER NCLASS
      REAL*8  TL (SED2D_NLAYER, NCLASS)
      REAL*8  T2

      INCLUDE 'err.fi'
      INCLUDE 'sed2d.fi'
      INCLUDE 'log.fi'

      REAL*8  TL2MAX
      INTEGER IERR
      INTEGER ID,IL
      REAL*8  TL2N,TL3,R,T2OLD,TLNEW(2,NCLASS)
      REAL*8  TATOT,TACHCK
C-----------------------------------------------------------------------

C---     PARAMETERS
      TL2MAX = 1.5D0*SED2D_LA
      TL2N   = 0.0D0
      TL3    = 0.0D0
      TATOT  = 0.0D0
      TACHCK = 0.0D0
      DO ID = 1,NCLASS
         TATOT = TATOT + TL(1,ID)
      ENDDO

C---     UPDATE SUB LAYERS THICKNESSES FOR LIMITS 1ST SUB LAYER
      IF (T2 .GE. TL2MAX) THEN
D        CALL LOG_ECRIS('Sub layer update T2>=1.5*LA')
C---       UPDATE  4TH, 5TH, ... LAYER MASS
         DO IL = SED2D_NLAYER,4,-1
            DO ID = 1,NCLASS
               TL(IL,ID) = TL(IL-1,ID)
            ENDDO
         ENDDO
C--- UPDATE ACTIVE LAYER; 2ND AND 3RD LAYER
         R=SED2D_LA/T2 !P ratio of 1st sublayer to be stored in 3rd layer
         DO ID = 1,NCLASS
            TL(3,ID) = TLNEW(2,ID)*R
            TL3 = TL3 + TL(3,ID)
            TL(2,ID) = TLNEW(2,ID)-TL(3,ID)
            TL2N = TL2N + TL(2,ID)
            TL(1,ID) = TLNEW(1,ID) !P FOR THE UPDATE OF VDLG IN MAIN ROUTINE
         ENDDO
C---       UPDATE SUB LAYER THICKNESS
!P        T2 = MIN(TL2MAX, T2 - SED2D_LA)
!CHECK ON 2ND LAYER AND 3ND LAYER
D        CALL ERR_ASR(ABS(TL2N + SED2D_LA-T2) .LE. 1.0D-15)
D        CALL ERR_ASR(ABS(TL3-SED2D_LA) .LE. 1.0D-15)

      ELSE IF(T2 .LE. SED2D_SUBL) THEN    ! when 2nd layer becomes too small
CP--  also update the active layer for the contribution of the 3rd layer towards the active layer
D        CALL LOG_ECRIS('Sub layer update T2<=0')
CP--  calculate thickness 3rd layer
         DO ID = 1,NCLASS
            TL3=TL3+TL(3,ID)
         ENDDO
CP--  correct active layer and sub layer
         DO ID = 1,NCLASS
            TL(1,ID) = TLNEW(1,ID) - T2*TL(3,ID)/TL3 +
     &      T2*TL(2,ID)/T2OLD
            TACHCK = TACHCK + TL(1,ID)
            TL(2,ID) = TL(3,ID) + T2*TL(3,ID)/TL3
            TL2N = TL2N + TL(2,ID)
         ENDDO
         CALL ERR_ASR(ABS(TATOT - TACHCK) .LE. 1.0D-15)

C---       UPDATE 3RD, 4TH, 5TH, ... LAYER MASS
         DO IL = 3, SED2D_NLAYER-1
            DO ID = 1,NCLASS
               TL(IL,ID) = TL(IL+1,ID)
            ENDDO
         ENDDO
!PC---        UPDATE SUB LAYER THICKNESS
!P        T2 = MAX(TL2MIN,T2 + SED2D_LA)
!CHECK ON 2ND LAYER
D        CALL ERR_ASR(ABS(TL2N - SED2D_LA-T2) .LE. 1.0D-15)
      ENDIF
!P      CALL ERR_ASR() CHECK ON TOTAL MASS

      IERR = ERR_TYP()
      RETURN
      END