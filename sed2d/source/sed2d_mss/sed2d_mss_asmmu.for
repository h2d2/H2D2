C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines: CNF2RES,
C               CNF2FC,
C               CNF2KU,
C               CNF2MU,
C               CNF2FV,
C               CNF2KT,
C               CNF2KTE,
C               CNF2MKT,
C               CNF2MKTE,
C               CNF2ML,
C               CNF2LT,
C               CNF2LTE,
C               CNF2DIM,
C               CNF2IND,
C               CNF2DIME,
C               CNF2INDE,
C               CNF2KDLE
C
C Description: ÉQUATION : ÉQUATION DE SEDIMENT CONTINUITY 2-D.
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[M].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMMU(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_MSS_ASMMU
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sed2d.fi'

      INTEGER SED2D_NNELT3
      INTEGER SED2D_NDLEMAX
      PARAMETER (SED2D_NNELT3  =  3)
      PARAMETER (SED2D_NDLEMAX = SED2D_NNELT3*SED2D_NDLNMAX)

      INTEGER   KLOCE(SED2D_NDLEMAX)
      REAL*8    VFE  (SED2D_NDLEMAX)
C-----------------------------------------------------------------------

      CALL SED2D_MSS_ASMMU_V(KLOCE,
     &                      VFE,
     &                      VCORG,
     &                      KLOCN,
     &                      KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VPRGL,
     &                      VPRNO,
     &                      VPREV,
     &                      VPRES,
     &                      VSOLC,
     &                      VSOLR,
     &                      KCLCND,
     &                      VCLCNV,
     &                      KCLLIM,
     &                      KCLNOD,
     &                      KCLELE,
     &                      KDIMP,
     &                      VDIMP,
     &                      KEIMP,
     &                      VDLG,
     &                      VFG)

      RETURN
      END

C************************************************************************
C Sommaire:  CNF2KU
C
C Description: ASSEMBLAGE DU RESIDU:   "[M].{U}"
C
C Entrée:
C
C Sortie: VFG
C
C Notes: MODIFICATION : VDLG=C
C
C************************************************************************
      SUBROUTINE SED2D_MSS_ASMMU_V(KLOCE,
     &                            VFE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLC,
     &                            VSOLR,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            VFG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VFE   (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VFG   (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d.fc'

      INTEGER IERR
      INTEGER IC,ID,IE,IDZ
      INTEGER NO1,NO2,NO3
      REAL*8  UN_LMD,UN_LMDLA
      REAL*8  AMAS,S11,S12
C-----------------------------------------------------------------------

C---     PARAMETERS
      UN_LMD    = UN - SED2D_LAMBDA

C-------  LOOP OVER THE ELEMENTS
C         =======================
      DO 10 IC = 1,EG_CMMN_NELCOL
      DO 20 IE = EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        ELEMENT CONNECTIVITY FOR T3
         NO1 = KNGV(1,IE)
         NO2 = KNGV(3,IE)
         NO3 = KNGV(5,IE)

C---        ELEMENT CONSTANTES FOR GRAIN SIZE FRACTIONS
         AMAS = UN_LMD*UN_24*VDJV(5,IE)
         S11  = AMAS*DEUX      !  S33 = S11 FOR I = J
         S12  = AMAS           !  SIJ = S12 FOR I /= J

C---        VECTOR FOR GRAIN SIZE FRACTIONS
         DO ID=1,SED2D_NCLASS*2 !P include sub-layer by setting ID=1,2*NCLASS
            VFE(ID,1) = (S11*VDLG(ID,NO1) +
     &                   S12*(VDLG(ID,NO2) + VDLG(ID,NO3)))
            VFE(ID,2) = (S11*VDLG(ID,NO2) +
     &                   S12*(VDLG(ID,NO1) + VDLG(ID,NO3)))
            VFE(ID,3) = (S11*VDLG(ID,NO3) +
     &                   S12*(VDLG(ID,NO1) + VDLG(ID,NO2)))
         ENDDO

C---        ELEMENT LOCALIZATION TABLE
         DO ID=1,LM_CMMN_NDLN !P sub-layer included with change of NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
            KLOCE(ID, 2)= KLOCN(ID, NO2)
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        ASSEMBLE GLOBAL VECTOR
         IERR = SP_ELEM_ASMFE(LM_CMMN_NDLEV, KLOCE, VFE, VFG)

20    CONTINUE
10    CONTINUE

      RETURN
      END
