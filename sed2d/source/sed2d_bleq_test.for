C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C     FUNCTION SED2D_BLEQ_TEST
C
C Description: ÉQUATION : 2-D SEDIMENT CONTINUITY EQUATION
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire: Computes bedload transport rate - used for testing purpose
C
C Description:
C     The subroutine SED2D_BLEQ_TEST computes bedload transport rate
C     It is used for model testing
C
C
C Entrée:
C
C
C Sortie:
C     REAL*8  QB
C
C Notes:
C  Reference:
C************************************************************************
      FUNCTION SED2D_BLEQ_TEST(H,
     &                         UBAR,
     &                         USTR,
     &                         N,
     &                         D50,
     &                         ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_TEST
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  H
      REAL*8  UBAR
      REAL*8  USTR
      REAL*8  N
      REAL*8  D50
      INTEGER ID


      INCLUDE 'sed2d.fi'
      INCLUDE 'sed2d_bleq_test.fi'

      REAL*8 QB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SED2D_BLEQ .EQ. SED2D_BLEQT_TEST)
D     CALL ERR_PRE(H .GE. 0.0D0)
C-----------------------------------------------------------------------

C---     TRANSPORT RATE FOR MODEL TESTING
      QB = 1/(10000*H)
D     CALL ERR_ASR(QB .GE. 0.0D0)

      SED2D_BLEQ_TEST = QB
      RETURN
      END

C************************************************************************
C Sommaire : SED2D_BLEQ_TEST_PARM
C
C Description:
C
C
C Entrée:
C
C
C Sortie:
C
C
C Notes:
C************************************************************************
      SUBROUTINE SED2D_BLEQ_TEST_PARM (ID)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SED2D_BLEQ_TEST_PARM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ID
C-----------------------------------------------------------------------

      RETURN
      END
      