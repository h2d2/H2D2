//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#include "c_bgl.h"

#include "mpi.h"     // Doit venir avant

#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/properties.hpp"
#include "boost/graph/sloan_ordering.hpp"

#include <math.h>
#include <iostream>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#ifdef MODE_DEBUG
#  include <assert.h>
#endif

// ---  Type des indexes pour unifier avec ParMetis
typedef int idx_t;

// --- code d'erreur
static std::string strErr = "";

#undef MODE_DEBUG_PRINTOUT
//#define MODE_DEBUG_PRINTOUT

namespace h2d2
{
namespace bgl
{
using namespace boost;

// --- Définition du type Graph avec les propriétés nécessaires à sloan_ordering
typedef adjacency_list<vecS,
                       vecS,
                       undirectedS,
                       property<vertex_color_t,
                                default_color_type,
                                property<vertex_degree_t,
                                         int,
                                         property<vertex_priority_t,
                                                  double > > >,
                       no_property,
                       listS > Graph;

typedef graph_traits<Graph>::vertex_descriptor Noeud;
typedef graph_traits<Graph>::vertices_size_type size_type;

class Arete
{
   public:
      Arete(fint_t nod1, fint_t nod2)
      {
         if (nod1 < nod2)
         {
            nodMin = nod1;
            nodMax = nod2;
         }
         else
         {
            nodMin = nod2;
            nodMax = nod1;
         }
      }

      bool operator<(const Arete& a) const
      {
         bool ret = false;
         if (this->nodMin < a.nodMin)
         {
            ret = true;
         }
         else if (this->nodMin > a.nodMin)
         {
            ret = false;
         }
         else
         {
            ret = this->nodMax < a.nodMax;
         }
         return ret;
      }

      inline fint_t reqNodMin() const
      {
         return nodMin;
      }
      inline fint_t reqNodMax() const
      {
         return nodMax;
      }

   private:
      fint_t nodMin;
      fint_t nodMax;
};

fint_t* min_element_not_neg(fint_t* _First, fint_t* _Last)
{
   fint_t* _Found = _First;
   while (*_Found < 0 && _First != _Last)
      if (*++_First >= 0) _Found = _First;
   if (_First != _Last)
      for (; ++_First != _Last; )
         if (*_First >= 0 && *_First < *_Found)
            _Found = _First;
   return (_Found);
}
fint_t* max_element_not_neg(fint_t* _First, fint_t* _Last)
{
   fint_t* _Found = _First;
   while (*_Found < 0 && _First != _Last)
      if (*++_First >= 0) _Found = _First;
   if (_First != _Last)
      for (; ++_First != _Last; )
         if (*_First >= 0 && *_First > *_Found)
            _Found = _First;
   return (_Found);
}


//************************************************************************
// Sommaire:   Construit le graphe nodale d'un maillage.
//
// Description:
//    La fonction privée <code>Mesh2Nodal(...)<code> monte le graphe
//    nodal de la partie du maillage entre nodMinLcl et nodMaxLcl. Tous
//    les liens vers des noeuds de connectivité positive sont pris en
//    compte. Il est ainsi possible de désactiver des portions de maillages
//    en mettant les connectivités en négatif.
//    <p>
//    La fonction retourne des blocs de mémoire alloués par des appels
//    à new[]. Il est de la responsabilité de la fonction appelante de
//    désalouer la mémoire par des appels à delete[].
//
// Entrée:
//    comm           Communicateur MPI, utilisé en mode debug
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nodMinLcl      Numéro global min du bloc de noeuds du process
//    nodMaxLcl      Numéro global max du bloc de noeuds du process
//
// Sortie:
//    xadj           Tables du graphe en format compressé
//    adjncy
//
// Notes:
//    On monte d'abord la table des connectivités inverse, puis la table
//    des liens. Ce sont des tables cumulées, on fait donc deux passes, la
//    première pour dimensioner, la deuxième pour charger.
//    <p>
//    Les tables sont dimensionnées à +1 pour faire un contrôle de
//    débordement par le haut.
//************************************************************************
void Mesh2Nodal(int     comm,
                int     nnel,
                int     nelt,
                fint_t* kng,
                int     nodMinLcl,
                int     nodMaxLcl,
                idx_t** xadj,
                idx_t** adjncy)
{
#ifdef MODE_DEBUG
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nodMinLcl >= 0);
   assert(nodMaxLcl > nodMinLcl);
#endif   // MODE_DEBUG

#ifdef MODE_DEBUG_PRINTOUT
   int iproc;
   (void) MPI_Comm_rank(comm, &iproc);
#endif

   const int nbrNodLcl = nodMaxLcl - nodMinLcl + 1;
   const int nodMinGlb = *min_element_not_neg(kng, kng + nelt*nnel);
   const int nodMaxGlb = *max_element_not_neg(kng, kng + nelt*nnel);
#ifdef MODE_DEBUG
   assert(nodMinGlb >= 0);
   assert(nodMaxGlb >  nodMinGlb);
#endif

   // ---  Table cumulée des connectivités inverses
   int* idxElem = new int[nbrNodLcl+2];
   for (int in = 0; in <= nbrNodLcl; ++in) idxElem[in] = 0;
#ifdef MODE_DEBUG
   idxElem[nbrNodLcl+1] = 0xCCCCCCCC;
#endif

   // ---  Compte les éléments de chaque noeud local
   for (int ie = 0; ie < nelt; ++ie)
   {
      const fint_t* k = &kng[ie*nnel];
      for (int in = 0; in < nnel; ++in)
      {
         const int noL = k[in] - nodMinLcl;
         if (noL >= 0 && noL < nbrNodLcl) idxElem[noL]++;
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Nombre d'elements par noeud\n", iproc);
   for (int in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%d: %d\n", iproc, in+nodMinLcl+1, idxElem[in]);
   }
#endif
#ifdef MODE_DEBUG
   for (int in = 0; in < nbrNodLcl; ++in) assert(idxElem[in] > 0);
   assert(static_cast<unsigned>(idxElem[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Cumule la table
   for (int in = 1; in < nbrNodLcl; ++in) idxElem[in]+= idxElem[in-1];
   for (int in = nbrNodLcl; in > 0; --in) idxElem[in] = idxElem[in-1];
   idxElem[0] = 0;
#ifdef MODE_DEBUG
   assert(static_cast<unsigned>(idxElem[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Table des connectivités inverses
   const int nbrConnecInv = idxElem[nbrNodLcl];
   int* connecInv = new int[nbrConnecInv+1];
#ifdef MODE_DEBUG
   for (int i = 0; i < nbrConnecInv; ++i) connecInv[i] = -1;
   connecInv[nbrConnecInv] = 0xCCCCCCCC;
#endif

   // ---  Table auxiliaire: Indice des éléments pour chaque noeud
   int* aux = new int[nodMaxGlb+2];
   for (int in = 0; in < nbrNodLcl; ++in) aux[in] = idxElem[in];
#ifdef MODE_DEBUG
   aux[nbrNodLcl] = 0xCCCCCCCC;
#endif

   // ---  Monte la table des connectivités inverses
   for (int ie = 0; ie < nelt; ++ie)
   {
      const fint_t* k = &kng[ie*nnel];
      for (int in = 0; in < nnel; ++in)
      {
         const int noL = k[in] - nodMinLcl;
         if (noL >= 0 && noL < nbrNodLcl) connecInv[aux[noL]++] = ie;
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Connectivites inverses\n", iproc);
   for (int in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%2d: ", iproc, in+nodMinLcl+1);
      int i1 = idxElem[in];
      int i2 = idxElem[in+1];
      for (int j = i1; j < i2; ++j)
      {
         printf(" %d", connecInv[j]+1);
      }
      printf("\n");
   }
#endif
#ifdef MODE_DEBUG
   for (int i = 0; i < nbrNodLcl; ++i) assert(aux[i] == idxElem[i+1]);
   assert(static_cast<unsigned>(aux[nbrNodLcl]) == 0xCCCCCCCC);
   for (int i = 0; i < nbrConnecInv;  ++i) assert(connecInv[i] != -1);
   assert(static_cast<unsigned>(connecInv[nbrConnecInv]) == 0xCCCCCCCC);
#endif


   // ---  Table cumulée du nombre de liens par noeuds
   idx_t* idxLiens = new idx_t[nbrNodLcl+2];
   for (int in = 0; in <= nbrNodLcl; ++in) idxLiens[in] = 0;
#ifdef MODE_DEBUG
   idxLiens[nbrNodLcl+1] = 0xCCCCCCCC;
#endif

   // ---  Table auxiliaire: Evite les dédoublements
   for (int in = 0; in <= nodMaxGlb; ++in) aux[in] = -1;
#ifdef MODE_DEBUG
   aux[nodMaxGlb+1] = 0xCCCCCCCC;
#endif

   // ---  Compte le nombre de liens
   for (int in = 0; in < nbrNodLcl; ++in)
   {
      const int noG = in + nodMinLcl;
      aux[noG] = in;                // Marque avec le noeud actif
      for (int ix = idxElem[in]; ix < idxElem[in+1]; ++ix)
      {
         const int ie = connecInv[ix];
         const fint_t* k = &kng[ie*nnel];
         for (int i = 0; i < nnel; ++i)
         {
            const int noG = k[i];
            if (noG >= 0 && aux[noG] != in)
            {
               aux[noG] = in;       // Marque avec le noeud actif
               idxLiens[in]++;      // Incrémente
            }
         }
      }
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Nombre de liens par noeud\n", iproc);
   for (int in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%d: %d\n", iproc, in+nodMinLcl+1, idxLiens[in]);
   }
#endif
#ifdef MODE_DEBUG
   for (int in = 0; in < nbrNodLcl; ++in) assert(idxLiens[in] > 0);
   assert(static_cast<unsigned>(idxLiens[nbrNodLcl+1]) == 0xCCCCCCCC);
   assert(static_cast<unsigned>(aux[nodMaxGlb+1]) == 0xCCCCCCCC);
#endif

   // ---  Cumule la table des liens
   for (int in = 1; in < nbrNodLcl; ++in) idxLiens[in]+= idxLiens[in-1];
   for (int in = nbrNodLcl; in > 0; --in) idxLiens[in] = idxLiens[in-1];
   idxLiens[0] = 0;
#ifdef MODE_DEBUG
   assert(static_cast<unsigned>(idxLiens[nbrNodLcl+1]) == 0xCCCCCCCC);
#endif

   // ---  Table des liens
   const int nbrLiens = idxLiens[nbrNodLcl];
   idx_t* liens = new idx_t[nbrLiens+1];
#ifdef MODE_DEBUG
   for (int i = 0; i < nbrLiens; ++i) liens[i] = -1;
   liens[nbrLiens] = 0xCCCCCCCC;
#endif

   // ---  Monte la table des liens
   for (int in = 0; in <= nodMaxGlb; ++in) aux[in] = -1;
#ifdef MODE_DEBUG
   aux[nodMaxGlb+1] = 0xCCCCCCCC;
#endif
   for (int in = 0, il = 0; in < nbrNodLcl; ++in)
   {
      const int noG = in + nodMinLcl;
      aux[noG] = in;
      for (int ix = idxElem[in]; ix < idxElem[in+1]; ++ix)
      {
         const int ie = connecInv[ix];
         const fint_t* k = &kng[ie*nnel];
         for (int i = 0; i < nnel; ++i)
         {
            const int noG = k[i];
            if (noG >= 0 && aux[noG] != in)
            {
               aux[noG] = in;
               liens[il++] = noG;
            }
         }
      }
//      std::sort(&liens[i1], &liens[i2]);   // Faut-il trier?
   }
#ifdef MODE_DEBUG_PRINTOUT
   printf("[%d]Liens des noeuds\n", iproc);
   for (int in = 0; in < nbrNodLcl; ++in)
   {
      printf("[%d]%2d: ", iproc, in+nodMinLcl+1);
      int i1 = idxLiens[in];
      int i2 = idxLiens[in+1];
      for (int j = i1; j < i2; ++j)
      {
         printf(" %d", liens[j]+1);
      }
      printf("\n");
   }
#endif
#ifdef MODE_DEBUG
   for (int i = 0; i < nbrLiens; ++i) assert(liens[i] != -1);
   assert(static_cast<unsigned>(liens[nbrLiens]) == 0xCCCCCCCC);
   assert(static_cast<unsigned>(aux[nodMaxGlb+1]) == 0xCCCCCCCC);
#endif

   // ---  Récupère la mémoire allouée
   delete[] aux;

   // ---  Tables de sortie
   *xadj   = idxLiens;
   *adjncy = liens;

#ifdef MODE_DEBUG
#endif   // MODE_DEBUG
   return;
}

//************************************************************************
// Sommaire: Renumérote le maillage
//
// Description:
//    La fonction privée <code>Mesh2Renum(...)</code> renumérote le maillage.
//    Pour le process courant, elle retourne dans la table kren les nouveaux
//    numéros de noeuds.
//    <p>
//    Elle fait appel à sloan_ordering.
//
// Entrée:
//    comm           Communicateur MPI
//    nnel           Nombre de noeuds par élément
//    nelt           Nombre d'éléments
//    kng            Table des connectivités kng(nelt,nnel)
//    nnl            Nombre de noeuds locaux
//
// Sortie:
//    kren           Table de renumérotation
//
// Notes:
//************************************************************************
int Mesh2Renum(int     comm,
               int     nnel,
               int     nelt,
               fint_t* kng,
               int     nnl,
               fint_t* kren)
{
#ifdef MODE_DEBUG
   assert(nnel > 0);
   assert(nelt > 0);
   assert(kng != NULL);
   assert(nnl > 0);
   assert(kren != NULL);
#endif   // MODE_DEBUG

   int ierr = 0;

   const int nodMinLcl = *min_element_not_neg(kng, kng + nelt*nnel);
   const int nodMaxLcl = *max_element_not_neg(kng, kng + nelt*nnel);
   const int nbrNodLcl = nodMaxLcl - nodMinLcl + 1;
#ifdef MODE_DEBUG
   assert(nnl == nbrNodLcl);
#endif   // MODE_DEBUG

   // ---  Monte les tables de liens
   idx_t* xadj   = NULL;
   idx_t* adjncy = NULL;
   Mesh2Nodal(comm,
              nnel,
              nelt,
              kng,
              nodMinLcl,
              nodMaxLcl,
              &xadj,
              &adjncy);

   // ---  Monte le graphe
   Graph g(nbrNodLcl);

   for (fint_t in = 0; in < nbrNodLcl; ++in)
   {
      for (fint_t i = xadj[in]; i < xadj[in+1]; ++i)
      {
         fint_t jn = adjncy[i];
         if (in < jn) // pour s'assurer de ne pas doubler les arêtes
         {
            size_type inSizeType = static_cast<size_type>(in);
            size_type jnSizeType = static_cast<size_type>(jn);
            Noeud iNod = vertex(inSizeType, g);
            Noeud jNod = vertex(jnSizeType, g);
            add_edge(iNod, jNod, g);
         }
      }
   }

   // --- Création d'une property_map du degré de chaque noeud
   graph_traits<Graph>::vertex_iterator ui, ui_end;

   property_map<Graph,vertex_degree_t>::type deg = get(vertex_degree, g);
   for (boost::tie(ui, ui_end) = vertices(g); ui != ui_end; ++ui)
      deg[*ui] = degree(*ui, g);

   // --- Vecteur de noeuds pour la renumérotation
   std::vector<Noeud> renumSloan(num_vertices(g));

   // --- Renumérotation
   sloan_ordering(g,
                  renumSloan.begin(),
                  get(vertex_color, g),
                  make_degree_map(g),
                  get(vertex_priority,g));

   // --- Index: map un noeud à son indice
   property_map<Graph, vertex_index_t>::type idx = get(vertex_index, g);

   for (size_type i = 0; i != renumSloan.size(); ++i)
   {
      kren[idx[renumSloan[i]]] = i; // cast de size_type en fint_t
   }

   // ---  Debug printout
#ifdef MODE_DEBUG_PRINTOUT
   {
      MPI_Barrier(comm);

      int iproc;
      int nproc;
      ierr = MPI_Comm_rank(comm, &iproc);
      if (ierr == MPI_SUCCESS)
      {
         ierr = MPI_Comm_size(comm, &nproc);
      }
      if (ierr != MPI_SUCCESS)
      {
         int strErrLen;
         char strErrBuf[MPI_MAX_ERROR_STRING];
         MPI_Error_string(ierr, strErrBuf, &strErrLen);
         strErr.assign(strErrBuf);
         ierr = 1;
      }
      for (int ip = 0; ip < nproc; ++ip)
      {
         if (ip == iproc)
         {
            printf("[%d]Mesh2Renum: renum des noeuds locaux [%d]\n", iproc, nbrNodLcl);
            for (int i = 0; i < nbrNodLcl; ++i)
            {
               printf("[%d] %d --> %d\n", iproc, i, kren[i]);
            }
            printf("\n[%d]Mesh2Part: end renum des noeuds locaux\n", iproc);
         }
         MPI_Barrier(comm);
      }
      if (iproc == nproc-1) printf("\n");
   }
#endif

   return ierr;
}

}  // namespace bgl
}  // namespace h2d2


//************************************************************************
// Sommaire:   Assigne un message d'erreur
//
// Description:
//    La fonction <code>C_BGL_ASGMSG(...)</code> assigne un message
//    d'erreur à <code>msgErr</code> à partir du code d'erreur <code>ierr</code>
//
// Entrée:
//
// Sortie:
//    F2C_CONF_STRING msgErr   Message d'erreur
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_BGL_ASGMSG(F2C_CONF_STRING  msgErr
                                                              F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_BGL_ASGMSG(F2C_CONF_STRING  msgErr)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* msgErrP = msgErr;
   int   l  = len;
#else
   char* msgErrP = msgErr->strP;
   int   l  = str->len;
#endif

   memset(msgErrP, ' ', l);
   if (l >= static_cast<int> (strErr.length()))
   {
      memcpy(msgErrP, strErr.c_str(), strErr.length());
   }

   strErr = "";

   return 0;
}

//************************************************************************
// Sommaire:   Renumérote un maillage éléments finis.
//
// Description:
//    La fonction <code>BGL_Renum(...)</code> renumérote le maillage
//    décrit par <code>nnt, nnel, nelt, kng</code>.
//
// Entrée:
//    fint_t* comm      Communicateur MPI
//    fint_t* nnl       Nombre de noeuds locaux
//    fint_t* nnel      Nombre de noeuds par élément
//    fint_t* nelt      Nombre d'éléments total
//    fint_t* kng       Table globale des connectivités kng[nelt][nnel]
//
// Sortie:
//    fint_t* kren      Table des nouveaux numéros de noeuds
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_BGL_RENUM(fint_t* comm,
                                                             fint_t* nnl,
                                                             fint_t* kren,
                                                             fint_t* nnel,
                                                             fint_t* nelt,
                                                             fint_t* kng)
{
#ifdef MODE_DEBUG
   assert(*nnel > 0);
   assert(*nelt > 0);
   assert(kng != NULL);
   assert(*nnl > 0);
   int mpi_init = 0;
   MPI_Initialized(&mpi_init);
   assert(mpi_init > 0);
#endif   // MODE_DEBUG

   int ierr = 0;

   setvbuf(stdout, NULL, _IONBF, 0);
   setvbuf(stderr, NULL, _IONBF, 0);

   // ---  Passe en numérotation C
   const int nbrConnecGlb = (*nnel)*(*nelt);
   for (int i = 0; i < nbrConnecGlb; ++i) --kng[i];

   // ---  Renumérote
   ierr = h2d2::bgl::Mesh2Renum(*comm, *nnel, *nelt, kng, *nnl, kren);

   // ---  Repasse en numérotation FORTRAN
   for (int i = 0; i < nbrConnecGlb; ++i) ++kng[i];
   for (int i = 0; i < *nnl; ++i) ++kren[i];

   return ierr;
}
