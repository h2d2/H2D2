//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire:
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_BGL_H_DEJA_INCLU
#define C_BGL_H_DEJA_INCLU

#ifndef MODULE_BGL
#  define MODULE_BGL 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_BGL_ASGMSG C_BGL_ASGMSG
#  define C_BGL_RENUM  C_BGL_RENUM
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_BGL_ASGMSG c_bgl_asgmsg_
#  define C_BGL_RENUM  c_bgl_renum_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_BGL_ASGMSG c_bgl_asgmsg__
#  define C_BGL_RENUM  c_bgl_renum__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_BGL)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_BGL_ASGMSG (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_BGL_RENUM  (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_BGL_H_DEJA_INCLU
