C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  PEAU
C Sommaire: Fonctions de filtrage de la peau d'un maillage
C************************************************************************

C************************************************************************
C Sommaire: SP_PEAU_FILTRE
C
C Description:
C     La fonction SP_PEAU_FILTRE extrait la peau du maillage
C     de volume passé en argument. Il est de la responsabilité de
C     la fonction appelante de dimensionner la table KNGS des
C     connectivités des éléments de surface.
C
C Entrée:
C     ITPEV       TYPE DES ELEMENTS DE VOLUME
C     NCELV       NOMBRE DE CONNECTIVITÉS PAR ELEMENT DE VOLUME
C     NNELV       NOMBRE DE NOEUDS PAR ELEMENT DE VOLUME
C     NELV        NOMBRE D'ELEMENTS DE VOLUME
C     KNGV        CONNECTIVITES GLOBALE DE VOLUME
C     ITPES       TYPE DES ELEMENTS DE SURFACE
C     NNELS       NOMBRE DE NOEUDS PAR ELEMENT DE SURFACE
C     NCELV       NOMBRE DE CONNECTIVITÉS PAR ELEMENT DE SURFACE
C     NELS        DIMENSION DE LA TABLE KNGS
C
C Sortie:
C     NELS        NOMBRE D'ELEMENTS DE SURFACE
C     KNGS        CONNECTIVITES GLOBALE DE SURFACE
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_FILTRE (ITPEV,
     &                         NNELV,
     &                         NCELV,
     &                         NELV,
     &                         KNGV,
     &                         ITPES,
     &                         NNELS,
     &                         NCELS,
     &                         NELS,
     &                         KNGS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_PEAU_FILTRE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ITPEV
      INTEGER NNELV
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER ITPES
      INTEGER NNELS
      INTEGER NCELS
      INTEGER NELS
      INTEGER KNGS (NCELS,*)

      INCLUDE 'sppeau.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER IE, IN
      INTEGER L_PNT
      INTEGER L_LIE
      INTEGER LENCR
      INTEGER NELS_NEW
      INTEGER NNT
      INTEGER SP_PEAU_XEQFLT
      INTEGER SP_PEAU_TRFELE
C-----------------------------------------------------------------------

C---     RECHERCHE LA MAX DE NO DE NOEUD
      NNT = 0
      DO IE=1, NELV
         DO IN=1, NNELV
            NNT = MAX(NNT, KNGV(IN, IE))
         ENDDO
      ENDDO

C---     ALLOUE L'ESPACE
      L_PNT = 0
      IERR = SO_ALLC_ALLINT(NNT  , L_PNT)
      L_LIE = 0
      LENCR = MAX(100, NINT(1.5*NELV))*4     ! 1.5 COEF. HEURISTIQUE,
      IERR = SO_ALLC_ALLINT(LENCR, L_LIE)    ! 4 POUR DIMENSIONNER KLIEN

C---     FILTRE
100   CONTINUE
         IERR = SP_PEAU_XEQFLT(ITPEV,
     &                         NNELV,
     &                         NCELV,
     &                         NELV,
     &                         KNGV,
     &                         NNT,
     &                         KA(SO_ALLC_REQKIND(KA,L_PNT)),
     &                         LENCR/4,
     &                         KA(SO_ALLC_REQKIND(KA,L_LIE)),
     &                         NELS_NEW)
         IF (.NOT. ERR_ESTMSG('ERR_LISTE_PLEINE')) GOTO 110
         CALL ERR_RESET()
         LENCR = NINT(1.5*LENCR)
         IERR = SO_ALLC_ALLINT(LENCR, L_LIE)
      GOTO 100
110   CONTINUE

C---     STOKE LES NOUVEAUX ELEMENTS
      IF (ERR_GOOD()) THEN
         IF (NELS_NEW .GT. NELS) GOTO 9900
         IERR = SP_PEAU_TRFELE(ITPEV,
     &                         NNELV,
     &                         NCELV,
     &                         NELV,
     &                         KNGV,
     &                         ITPES,
     &                         NNELS,
     &                         NCELS,
     &                         NELS_NEW,
     &                         KNGS,
     &                         NNT,
     &                         KA(SO_ALLC_REQKIND(KA,L_PNT)),
     &                         LENCR/4,
     &                         KA(SO_ALLC_REQKIND(KA,L_LIE)))
         IF (ERR_GOOD()) NELS = NELS_NEW
      ENDIF

C---      RECUPERE L'ESPACE
      IERR = SO_ALLC_ALLINT(0, L_LIE)
      IERR = SO_ALLC_ALLINT(0, L_PNT)

      GOTO 9999
C----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_TABLE_ELEM_SURFACE_MAL_DIMENSIONNEE')
      GOTO 9999

9999  CONTINUE
      SP_PEAU_FILTRE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Filtre la peau avec une liste chaînée.
C
C Description:
C     La fonction SP_PEAU_XEQFLT monte la liste chaînée qui contient
C     les éléments de peau.
C
C Entrée:
C       KNGV          GLOBAL CONECTIVITIES
C       KPNT         \ ARRAY FOR THE
C       KLIEN        / LINKED CHAIN
C       NLIEN        MAX NUMBER OF ELEMENTS IN CHAIN
C
C Sortie:
CC Notes: Coder pour P12L
C************************************************************************
      FUNCTION SP_PEAU_XEQFLT(ITPEV,
     &                        NNELV,
     &                        NCELV,
     &                        NELV,
     &                        KNGV,
     &                        NNT,
     &                        KPNT,
     &                        NLIEN,
     &                        KLIEN,
     &                        NELS)

      IMPLICIT NONE

      INTEGER SP_PEAU_XEQFLT
      INTEGER ITPEV
      INTEGER NNELV
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER NELS

      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'splstc.fi'

      INTEGER INEXT
      INTEGER IERR
      INTEGER SP_PEAU_FLTL2
      INTEGER SP_PEAU_FLTL3
      INTEGER SP_PEAU_FLTT3
      INTEGER SP_PEAU_FLTT6
C----------------------------------------------------------------------

C---     INITIALIZE THE LINKED CHAIN
      IERR = SP_LSTC_INIT(NNT, KPNT, 3, NLIEN, KLIEN, INEXT)

C---     ELEMENT L2
      IF     (ITPEV .EQ. EG_TPGEO_L2) THEN
         IERR=SP_PEAU_FLTL2(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT L3
      ELSEIF (ITPEV .EQ. EG_TPGEO_L3) THEN
         IERR=SP_PEAU_FLTT3(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT L3L
      ELSEIF (ITPEV .EQ. EG_TPGEO_L3L) THEN
         IERR=SP_PEAU_FLTT3(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT T3
      ELSEIF (ITPEV .EQ. EG_TPGEO_T3) THEN
         IERR=SP_PEAU_FLTT3(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT T6
      ELSEIF (ITPEV .EQ. EG_TPGEO_T6) THEN
         IERR=SP_PEAU_FLTT6(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT T6L
      ELSEIF (ITPEV .EQ. EG_TPGEO_T6L) THEN
         IERR=SP_PEAU_FLTT6(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)

C---     ELEMENT P12L
      ELSEIF (ITPEV .EQ. EG_TPGEO_P12L) THEN
         IERR=SP_PEAU_FLTT6(NCELV,NELV,KNGV,NNT,KPNT,NLIEN,KLIEN,INEXT)
         CALL LOG_TODO('SP_PEAU_XEQFLT  mettre code pour P12L')
      ENDIF

C---     GET NUMBER OF ELEMENTS OF SKIN
      NELS = SP_LSTC_REQDIM(NNT, KPNT, 3, NLIEN, KLIEN)

      SP_PEAU_XEQFLT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_FLTL2(NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NNT,
     &                       KPNT,
     &                       NLIEN,
     &                       KLIEN,
     &                       INEXT)

      IMPLICIT NONE

      INTEGER SP_PEAU_FLTL2
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      INFO(2) = 0
      DO IE=1,NELV
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(2, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(2, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF

         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999
      ENDDO

9999  CONTINUE
      SP_PEAU_FLTL2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_FLTL3(NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NNT,
     &                       KPNT,
     &                       NLIEN,
     &                       KLIEN,
     &                       INEXT)

      IMPLICIT NONE

      INTEGER SP_PEAU_FLTL3
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      INFO(2) = 0
      DO IE=1,NELV
         IF (KNGV(1,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF

         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999
      ENDDO

9999  CONTINUE
      SP_PEAU_FLTL3 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_FLTT3(NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NNT,
     &                       KPNT,
     &                       NLIEN,
     &                       KLIEN,
     &                       INEXT)

      IMPLICIT NONE

      INTEGER SP_PEAU_FLTT3
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      INFO(2) = 0
      DO IE=1,NELV

C---        SIDE 1 -- NODES 1-2
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(2, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(2, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 2-3
         IF (KNGV(2,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(2, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(2, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 3-1
         IF (KNGV(3,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      SP_PEAU_FLTT3 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_FLTT6(NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NNT,
     &                       KPNT,
     &                       NLIEN,
     &                       KLIEN,
     &                       INEXT)

      IMPLICIT NONE

      INTEGER SP_PEAU_FLTT6
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF, INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      INFO(2) = 0
      DO IE=1,NELV

C---        SIDE 1 -- NODES 1-3
         IF (KNGV(1,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 3-5
         IF (KNGV(3,IE) .LT. KNGV(5,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 5-1
         IF (KNGV(5,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      SP_PEAU_FLTT6 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_PEAU_TRFELE
C
C Description:
C     SAUVEGARDE LES CONNECTIVITÉS DES ÉLÉMENTS DE PEAU
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_PEAU_TRFELE (ITPEV,
     &                         NNELV,
     &                         NCELV,
     &                         NELV,
     &                         KNGV,
     &                         ITPES,
     &                         NNELS,
     &                         NCELS,
     &                         NELS,
     &                         KNGS,
     &                         NNT,
     &                         KPNT,
     &                         NLIEN,
     &                         KLIEN)

      IMPLICIT NONE

      INTEGER SP_PEAU_TRFELE
      INTEGER ITPEV
      INTEGER NNELV
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER ITPES
      INTEGER NNELS
      INTEGER NCELS
      INTEGER NELS
      INTEGER KNGS (NCELS,NELS)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4, *)

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'

      INTEGER IERR
      INTEGER IEL
      INTEGER IN
      INTEGER IACTU
      INTEGER INFO(3)
      INTEGER IMST, ICOT
      INTEGER NNO1, NNO2, NNO3
      INTEGER NOP1, NOP2, NOP3, NOP5, NOEMIL
C-----------------------------------------------------------------------

C---     STOKE LES ELEMENTS
      IEL = 0
      DO IN=1,NNT
         IACTU = 0

         DO WHILE (.TRUE.)
            IERR = SP_LSTC_REQCHN(NNT, KPNT, 3, NLIEN, KLIEN,
     &                            IN, IACTU, INFO)
            IF (IACTU .LE. 0) EXIT

            IMST = INFO(3)
            IF (IMST .GT. 0) THEN
               IMST = IMST
               NNO1 = IN
               NNO2 = INFO(1)
               NNO3 = INFO(2)
            ELSE
               IMST =-IMST
               NNO2 = IN
               NNO1 = INFO(1)
               NNO3 = INFO(2)
            ENDIF
            IEL = IEL + 1

C---           ELEMENT P1
            IF (ITPES .EQ. EG_TPGEO_P1) THEN
D              CALL ERR_ASR(NCELS .GE. 2)
               KNGS(1,IEL) = NNO1
               KNGS(2,IEL) = IMST

C---           ELEMENT L2
            ELSEIF (ITPES .EQ. EG_TPGEO_L2) THEN
D              CALL ERR_ASR(NCELS .GE. 4)
               KNGS(1,IEL) = NNO1
               KNGS(2,IEL) = NNO2
               KNGS(3,IEL) = IMST
               KNGS(4,IEL) = -1
C---              RECHERCHE DU CÔTÉ
               NOP1 = KNGV(1,IMST)
               NOP2 = KNGV(2,IMST)
               NOP3 = KNGV(3,IMST)
               IF     (NNO1.EQ.NOP1 .AND. NNO2.EQ.NOP2) THEN  ! COTE 1-3
                  ICOT = 1
               ELSEIF (NNO1.EQ.NOP2 .AND. NNO2.EQ.NOP3) THEN  ! COTE 3-5
                  ICOT = 2
               ELSEIF (NNO1.EQ.NOP3 .AND. NNO2.EQ.NOP1) THEN  ! COTE 5-1
                  ICOT = 3
               ELSE
                  GOTO 9900
               ENDIF
               KNGS(4,IEL) = ICOT

C---           ELEMENT L3L
            ELSEIF (ITPES .EQ. EG_TPGEO_L3L) THEN
D              CALL ERR_ASR(NCELS .GE. 5)
               KNGS(1,IEL) = NNO1
               KNGS(2,IEL) = -1
               KNGS(3,IEL) = NNO2
               KNGS(4,IEL) = IMST
               KNGS(5,IEL) = -1
C---              RECHERCHE DU NOEUD MILIEU
               NOP1 = KNGV(1,IMST)
               NOP3 = KNGV(3,IMST)
               NOP5 = KNGV(5,IMST)
               IF     (NNO1.EQ.NOP1 .AND. NNO2.EQ.NOP3) THEN  ! COTE 1-3
                  NOEMIL = KNGV(2,IMST)
                  ICOT = 1
               ELSEIF (NNO1.EQ.NOP3 .AND. NNO2.EQ.NOP5) THEN  ! COTE 3-5
                  NOEMIL = KNGV(4,IMST)
                  ICOT = 2
               ELSEIF (NNO1.EQ.NOP5 .AND. NNO2.EQ.NOP1) THEN  ! COTE 5-1
                  NOEMIL = KNGV(6,IMST)
                  ICOT = 3
               ELSE
                  GOTO 9900
               ENDIF
               KNGS(2,IEL) = NOEMIL
               KNGS(5,IEL) = ICOT

C---           ELEMENT L3
            ELSEIF (ITPES .EQ. EG_TPGEO_L3) THEN
D              CALL ERR_ASR(NCELS .GE. 5)
               KNGS(1,IEL) = NNO1
               KNGS(2,IEL) = -1
               KNGS(3,IEL) = NNO2
               KNGS(4,IEL) = IMST
               KNGS(5,IEL) = -1
C---              RECHERCHE DU NOEUD MILIEU ET DU CÔTÉ
               NOP1 = KNGV(1,IMST)
               NOP3 = KNGV(3,IMST)
               NOP5 = KNGV(5,IMST)
               IF     (NNO1.EQ.NOP1 .AND. NNO2.EQ.NOP3) THEN  ! COTE 1-3
                  NOEMIL = KNGV(2,IMST)
                  ICOT = 1
               ELSEIF (NNO1.EQ.NOP3 .AND. NNO2.EQ.NOP5) THEN  ! COTE 3-5
                  NOEMIL = KNGV(4,IMST)
                  ICOT = 2
               ELSEIF (NNO1.EQ.NOP5 .AND. NNO2.EQ.NOP1) THEN  ! COTE 5-1
                  NOEMIL = KNGV(6,IMST)
                  ICOT = 3
               ELSE
                  GOTO 9900
               ENDIF
               KNGS(2,IEL) = NOEMIL
               KNGS(5,IEL) = ICOT

C---           ELEMENT Q4
            ELSEIF (NNELV .EQ. 4) THEN
D              CALL ERR_ASR(NCELS .GE. 4)
               call log_todo('SP_PEAU_TRFELE Q4 non valide')
               KNGS(1,IEL) = NNO1
               KNGS(2,IEL) = NNO2
               KNGS(3,IEL) = NNO3
               KNGS(4,IEL) = IMST
            ENDIF

         ENDDO
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_CONNEC_INVALIDE_PILE_CORROMPUE')
      GOTO 9999

9999  CONTINUE
      SP_PEAU_TRFELE = ERR_TYP()
      RETURN
      END
