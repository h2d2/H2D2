C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C     FUNCTION SP_GMRS_ALLOC
C     FUNCTION SP_GMRS_DEALLOC
C     FUNCTION SP_GMRS_SOLV
C************************************************************************

C**************************************************************
C Sommaire: Allocation des tables de travail de GMRES
C
C Description:
C     La fonction privée SP_GMRS_ALLOC alloue la mémoire pour
C     toutes les tables de travail spécifiques et nécessaires
C     à l'algorithme de résolution.
C
C Entrée:
C     NEQ      Nombre d'équation
C     NITER    Nombre d'itérations, dim. de l'espace de Krylov
C
C Sortie:
C     KTRV(8)  Pointeurs aux tables de travail
C
C Notes:
C
C****************************************************************
      FUNCTION SP_GMRS_ALLOC(NEQ, NITER, KTRV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_GMRS_ALLOC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER NITER
      INTEGER KTRV(8)

      INCLUDE 'spgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER I
      INTEGER NMAX, KMAX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NEQ   .GT. 0)
D     CALL ERR_PRE(NITER .GT. 0)
C-----------------------------------------------------------------------

C---     Initialise
      DO I=1,8
         KTRV(I) = 0
      ENDDO

      KMAX = NITER + 1
      NMAX = NEQ + MOD(NEQ,2)    ! Aligne les colonnes sur 16 bytes

C---     Alloue l'espace
      KTRV(1) = KMAX
      KTRV(2) = NMAX
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(NMAX,     KTRV(3))
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(KMAX*NMAX,KTRV(4))
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(KMAX*KMAX,KTRV(5))
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(KMAX,     KTRV(6))
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(KMAX,     KTRV(7))
      IF (ERR_GOOD()) IERR=SO_ALLC_ALLRE8(KMAX,     KTRV(8))

      SP_GMRS_ALLOC = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: Désalloue la mémoire des tables de travail
C
C Description:
C     La fonction privée SP_GMRS_DEALLOC désalloue la mémoire de
C     toutes les tables de travail spécifiques et nécessaires
C     à l'algorithme de résolution.
C
C Entrée:
C     KTRV(8)  Pointeurs aux tables de travail
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_GMRS_DEALLOC(KTRV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_GMRS_DEALLOC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER KTRV(8)

      INCLUDE 'spgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER I
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'espace mémoire
      IF (SO_ALLC_HEXIST(KTRV(3))) IERR = SO_ALLC_ALLRE8(0, KTRV(3))
      IF (SO_ALLC_HEXIST(KTRV(4))) IERR = SO_ALLC_ALLRE8(0, KTRV(4))
      IF (SO_ALLC_HEXIST(KTRV(5))) IERR = SO_ALLC_ALLRE8(0, KTRV(5))
      IF (SO_ALLC_HEXIST(KTRV(6))) IERR = SO_ALLC_ALLRE8(0, KTRV(6))
      IF (SO_ALLC_HEXIST(KTRV(7))) IERR = SO_ALLC_ALLRE8(0, KTRV(7))
      IF (SO_ALLC_HEXIST(KTRV(8))) IERR = SO_ALLC_ALLRE8(0, KTRV(8))

C---     Reset
      DO I=1,8
         KTRV(I) = 0
      ENDDO

      SP_GMRS_DEALLOC = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: GMRES
C
C Description:
C     La fonction SO_GMRS_SOLV tente de résoudre un système par
C     la méthode de GMRES non-linéaire en utilisant un espace
C     de Krylov de dimension NITER. En sortie, elle fournit
C     l'incrément sur la solution initiale.
C     <p>
C     La matrice tangente est approximée par différences finies.
C     <p>
C     La fonction nécessite 2 call-back, un pour le calcul du
C     résidu pour un vecteur donné, et le second pour le
C     préconditionnement.
C
C Entrée:
C     NITER    Nombre d'itérations, dim. de l'espace de Krylov
C     NDLL     Nombre de degrés de libertés
C     NEQ      Nombre d'équations, dim. du problème
C     KLOCN    Table de localisation pour passer de NEQ à NDLL
C     VDLG     Solution initiale
C     VEPS     EPS = VEPS(1) + (VEPS(2) + VEPS(3)*ETA) * ||U||
C              VEPS(1) Facteur absolu: terme constant
C              VEPS(2) Facteur relatif: terme constante
C              VEPS(3) Facteur relatif: terme linéaire
C              VEPS(4) Espace de travail < 0 en première entrée
C              VEPS(5) Espace de travail < 0 en première entrée
C              VEPS(6) Espace de travail < 0 en première entrée
C     KTRV     Pointeurs sur les tables de travail
C     HPRE     Handle sur le préconditionnement
C     F_PRE    Call-back de préconditionnement
C     HRES     Handle sur l'algorithme d'assemblage
C     F_RES    Call-back d'assemblage de résidu
C
C Sortie:
C     VSOL     Incrément sur la solution
C
C Notes:
C****************************************************************
      FUNCTION SP_GMRS_SOLV(NREDM,
     &                      NITER,
     &                      NDLL,
     &                      NEQ,
     &                      KLOCN,
     &                      VDLG,
     &                      VSOL,
     &                      VEPS,
     &                      VSTAT,
     &                      KTRV,
     &                      HPRE,
     &                      F_PRE,
     &                      HRES,
     &                      F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_GMRS_SOLV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  NREDM
      INTEGER  NITER
      INTEGER  NDLL
      INTEGER  NEQ
      INTEGER  KLOCN(NDLL)
      REAL*8   VDLG (NDLL)
      REAL*8   VSOL (NDLL)
      REAL*8   VEPS (6)
      REAL*8   VSTAT(7)
      INTEGER  KTRV (8)
      INTEGER  HPRE
      INTEGER  F_PRE
      EXTERNAL F_PRE
      INTEGER  HRES
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'spgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spadft.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER IR
      INTEGER SP_GMRS_UN_REDM
      REAL*8  DNRMU0
      REAL*8  EPSGMR
      REAL*8  RSTAT(5)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.spmef.gmres.solve'

C---     Adaptative forcing term
      EPSGMR = SP_ADFT_EPS(VEPS,
     &                     NDLL,
     &                     NEQ,
     &                     VDLG,                             !VDLG
     &                     VA(SO_ALLC_REQVIND(VA, KTRV(3))), !VDELU
     &                     HPRE,
     &                     F_PRE,
     &                     HRES,
     &                     F_RES)

C---     Boucle sur les redémarrages
      DNRMU0 = 0.0D0
      DO IR=1,NREDM

         WRITE(LOG_BUF, '(A,I6)') 'RESTART: ', IR
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

         IERR = SP_GMRS_UN_REDM(NITER,
     &                          NDLL,
     &                          NEQ,
     &                          KLOCN,
     &                          VDLG,
     &                          VSOL,
     &                          KTRV(1),
     &                          KTRV(2),
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(3))),   !VRES
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(4))),   !V
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(5))),   !H
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(6))),   !G
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(7))),   !CJ
     &                          VA(SO_ALLC_REQVIND(VA,KTRV(8))),   !SJ
     &                          HPRE,
     &                          F_PRE,
     &                          HRES,
     &                          F_RES,
     &                          EPSGMR,
     &                          RSTAT)

C---        M.A.J DE LA SOLUTION
         CALL DAXPY(NDLL, 1.0D0, VSOL, 1, VDLG, 1)

C---        CUMULE LES STATS
         IF (IR .EQ. 1) DNRMU0 = RSTAT(3) ! Résidu initial
         VSTAT(4) = VSTAT(4) + RSTAT(2)   ! Somme J
         VSTAT(6) = RSTAT(4)              ! Résidu GMRES final

         CALL LOG_DECIND()

         IF (NINT(RSTAT(5)) .EQ. 1) GOTO 199
      ENDDO
199   CONTINUE

C---     STATISTIQUES
      VSTAT(1) = NREDM
      VSTAT(2) = IR
      VSTAT(3) = NREDM * NITER
      VSTAT(5) = DNRMU0
      VSTAT(7) = RSTAT(5)     ! 0: Echec, 1: Réussite

C---     ETA
      VEPS(6) = VSTAT(4)

      SP_GMRS_SOLV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: Effectue un redémarage de GMRes
C
C Description:
C     La fonction SP_GMRS_REDM effectue un redémarrage de GMRes
C     non-linéaire en utilisant un espace de Krylov de dimension
C     NITER. En sortie, elle fournit l'incrément sur la solution
C     initiale.
C     <p>
C     Cette fonction est à utiliser lorsque le contrôle des
C     redémarrages est externe à cause de la mise à jour de la
C     solution, des limiteurs, ...
C     <p>
C     La fonction nécessite 2 call-back, un pour le calcul du
C     résidu pour un vecteur donné, et le second pour le
C     préconditionnement.
C
C Entrée:
C     NITER    Nombre d'itérations, dim. de l'espace de Krylov
C     NDLL     Nombre de degrés de libertés
C     NEQ      Nombre d'équations, dim. du problème
C     KLOCN    Table de localisation pour passer de NEQ à NDLL
C     VDLG     Solution initiale
C     EPSGMR   Norme de convergence pour GMRES
C     KTRV     Pointeurs sur les tables de travail
C     HPRE     Handle sur le préconditionnement
C     F_PRE    Call-back de préconditionnement
C     HRES     Handle sur l'algorithme d'assemblage
C     F_RES    Call-back d'assemblage de résidu
C
C Sortie:
C     VSOL     Incrément sur la solution
C     VSTAT    Table des statistiques du calcul
C
C Notes:
C
C****************************************************************
      FUNCTION SP_GMRS_REDM(NITER,
     &                      NDLL,
     &                      NEQ,
     &                      KLOCN,
     &                      VDLG,
     &                      VSOL,
     &                      EPSGMR,
     &                      VSTAT,
     &                      KTRV,
     &                      HPRE,
     &                      F_PRE,
     &                      HRES,
     &                      F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_GMRS_REDM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  NITER
      INTEGER  NDLL
      INTEGER  NEQ
      INTEGER  KLOCN(NDLL)
      REAL*8   VDLG (NDLL)
      REAL*8   VSOL (NDLL)
      REAL*8   EPSGMR
      REAL*8   VSTAT(7)
      INTEGER  KTRV (8)
      INTEGER  HPRE
      INTEGER  F_PRE
      EXTERNAL F_PRE
      INTEGER  HRES
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'spgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER SP_GMRS_UN_REDM

      REAL*8  RSTAT(5)
C-----------------------------------------------------------------------

      IERR = SP_GMRS_UN_REDM(NITER,
     &                       NDLL,
     &                       NEQ,
     &                       KLOCN,
     &                       VDLG,
     &                       VSOL,
     &                       KTRV(1),
     &                       KTRV(2),
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(3))),   !VRES
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(4))),   !V
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(5))),   !H
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(6))),   !G
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(7))),   !CJ
     &                       VA(SO_ALLC_REQVIND(VA,KTRV(8))),   !SJ
     &                       HPRE,
     &                       F_PRE,
     &                       HRES,
     &                       F_RES,
     &                       EPSGMR,
     &                       RSTAT)

C---     STATISTIQUES
      VSTAT(1) = 1            ! 1 redémarrage
      VSTAT(2) = 1            ! 1 de fait
      VSTAT(3) = NITER
      VSTAT(4) = RSTAT(2)     ! Nombre d'itération
      VSTAT(5) = RSTAT(3)     ! Résidu initial
      VSTAT(6) = RSTAT(4)     ! Résidu GMRES final
      VSTAT(7) = RSTAT(5)     ! 0: Echec, 1: Réussite

      SP_GMRS_REDM = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: Un redémarrage de GMRES
C
C Description:
C     La fonction privée SP_GMRS_UN_REDM  effectue un redémarage
C     de GMRES
C
C Entrée:
C
C Sortie:
C     VSTAT(1) = NITER
C     VSTAT(2) = J
C     VSTAT(3) = ||U||     ! Norme de U en entrée
C     VSTAT(4) = ||R||     ! Norme du résidu GMRES en sortie
C     VSTAT(5) = Réussite  ! 0: Echec, 1: Réussite
C
C Notes:
C
C****************************************************************
      FUNCTION SP_GMRS_UN_REDM(NITER,
     &                         NDLL,
     &                         NEQ,
     &                         KLOCN,
     &                         VDLG,
     &                         VSOL,
     &                         KMAX,
     &                         NMAX,
     &                         VRES,
     &                         V,
     &                         H,
     &                         G,
     &                         CJ,
     &                         SJ,
     &                         HPRE,
     &                         F_PRE,
     &                         HRES,
     &                         F_RES,
     &                         EPSGMR,
     &                         VSTAT)

      IMPLICIT NONE

      INTEGER SP_GMRS_UN_REDM
      INTEGER NITER
      INTEGER NDLL
      INTEGER NEQ
      INTEGER KLOCN(NDLL)
      REAL*8  VDLG (NDLL)
      REAL*8  VSOL (NDLL)
      INTEGER KMAX
      INTEGER NMAX
      REAL*8  VRES (NMAX)
      REAL*8  V    (NMAX,KMAX)
      REAL*8  H    (KMAX,KMAX)
      REAL*8  G    (KMAX)
      REAL*8  CJ   (KMAX)
      REAL*8  SJ   (KMAX)
      INTEGER HPRE
      INTEGER F_PRE
      EXTERNAL F_PRE
      INTEGER HRES
      INTEGER F_RES
      EXTERNAL F_RES
      REAL*8  EPSGMR
      REAL*8  VSTAT(5)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER I, I1, ID
      INTEGER J, J1
      REAL*8  DU, DUINV
      REAL*8  HTMP, VANG, XNORM, XNORMGMR
      LOGICAL FINITER

      REAL*8 ZERO, UN
      REAL*8 EPSMAC
      PARAMETER (ZERO   = 0.0000000000000000000D+00)
      PARAMETER (UN     = 1.0000000000000000000D+00)
      PARAMETER (EPSMAC = 1.0000000000000000000D-15)

      REAL*8  DNRM2, DDOT     ! Fonctions BLAS
      INTEGER IDAMAX          ! Fonctions BLAS
C-----------------------------------------------------------------------
D     CALL ERR_ASR(KMAX .GT. NITER)
D     CALL ERR_ASR(NMAX .GE. NEQ)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.spmef.gmres.solve'

      J = 0
      FINITER = .FALSE.
      CALL LOG_INCIND()

C---     VSOL = VDLG
      CALL DCOPY(NDLL, VDLG, 1, VSOL, 1)

C---     Variation des ddl
      DU = 1.0D-7*(DNRM2(NDLL,VSOL,1)+1.0D-09)
      IF (DU .GT. 1.0D-7) DU = 1.0D-7
      DUINV = UN/DU

C---     Initialise à zero la table VRES du residu
      CALL DINIT(NEQ, ZERO, VRES, 1)
C---     Calcul du residu dans VRES
      IERR = F_RES(HRES, VRES, VSOL, .TRUE.)
C---     Premier vecteur de Krylov   V(.,1) = {F} - [K]{U}={R}
      CALL DCOPY(NEQ, VRES, 1, V(1,1), 1)
C---     Preconditionne le premier vecteur de Krylov
      IERR = F_PRE(HPRE, V(1,1))

C---     G(1) = |R|
      G(1) = DNRM2(NEQ, V(1,1), 1)
      XNORMGMR = G(1)

C---     Statistiques
      VSTAT(3) = G(1)     ! Norme du résidu en entrée

C---     Log
      WRITE(LOG_BUF,1000) 'ITER. : ',0, ' (GMRES)= ',XNORMGMR
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Normalise V(.,1) = V(.,1)/|V(.,1)|
      IF (G(1) .LT. EPSGMR) GOTO 900
      CALL DSCAL(NEQ,UN/G(1),V(1,1),1)
!!      write(6, *) 'v(1,1)'
!!      write(6, '(1pe25.17e3)') (v(id,1),id=1,neq)
!!      write(6, '(a,1pe25.17e3)') 'dnrm2: ', DNRM2(NEQ,V(1,1),1)

C---     Boucle sur les iterations
C        -------------------------
      DO J=1,NITER
         J1 = J + 1

C---        A*V(.,J)
         CALL DINIT(NDLL, ZERO, VSOL, 1)
         IERR = SP_ELEM_NEQ2NDLT(NEQ, NDLL, KLOCN, V(1,J), VSOL)
         CALL DSCAL(NDLL, DU, VSOL, 1)           ! VSOL *= DU
         CALL DAXPY(NDLL, UN, VDLG, 1, VSOL, 1)  ! VSOL += VDLG
         CALL DINIT(NEQ, ZERO, V(1,J1), 1)       ! V(.,J1) = 0
!!      write(6, *)'vsol'
!!      write(6, '(1pe25.17e3)') (vsol(id),id=1,ndll)
         IERR = F_RES(HRES, V(1,J1), VSOL, .TRUE.)
C---        Calcule [Kt]{DU} par un schema de Newton-inexact
         DO ID=1,NEQ
            V(ID,J1) = (VRES(ID) - V(ID,J1))*DUINV
         ENDDO
C---        Preconditionne [Kt]{DU}
         IERR = F_PRE(HPRE, V(1,J1))
!!      write(6, *)'v(1,j1)'
!!      write(6, '(1pe25.17e3)') (v(id,1),id=1,neq)

C---        Orthogonalise par rapport au V precedant
         DO I=1,J
            H(I,J) = DDOT(NEQ,V(1,I),1,V(1,J1),1)
!!      write(6, '(a,2i3,1pe25.17e3)') 'h(i,j): ',i,j,h(i,j)
            CALL DAXPY(NEQ,-H(I,J),V(1,I),1,V(1,J1),1)
         ENDDO

C---        Norme de V(.,J+1) et normalise V(.,J+1)
         H(J1,J) = DNRM2(NEQ,V(1,J1),1)
!!      write(6, '(a,2i3,1pe25.17e3)') 'h(j1,j): ',j1,j,h(j1,j)
         IF (H(J1,J) .GE. EPSMAC) THEN
            CALL DSCAL(NEQ,UN/H(J1,J),V(1,J1),1)
         ELSE
            FINITER = .TRUE.
         ENDIF

C---        Factorisation de H
         DO I=2,J
            I1 = I - 1
            HTMP = H(I1,J)
            H(I1,J) =   CJ(I1)*HTMP + SJ(I1)*H(I,J)
            H(I ,J) = - SJ(I1)*HTMP + CJ(I1)*H(I,J)
         ENDDO

C---        Elimination de H(J+1,J)
         VANG  = HYPOT(H(J1,J), H(J,J))
!!!         IF (VANG .LT. EPSMAC) GOTO 901
!!!         CJ(J) = H(J ,J)/VANG
!!!         SJ(J) = H(J1,J)/VANG
         IF (VANG .GE. EPSMAC) THEN
            CJ(J) = H(J ,J)/VANG
            SJ(J) = H(J1,J)/VANG
         ELSE IF (VANG .EQ. ZERO) THEN
            CJ(J) = UN                       ! On est possiblement
            SJ(J) = ZERO                     ! en erreur ici
            FINITER = .TRUE.
         ELSE
            SJ(J) = ATAN2(H(J1,J), H(J,J))   ! L'angle dans sJ pour
            CJ(J) = COS(SJ(J))               ! pour conserver VANG
            SJ(J) = SIN(SJ(J))
            FINITER = .TRUE.
         ENDIF
         H(J,J)= VANG
         G(J1) = -SJ(J)*G(J)
         G(J)  =  CJ(J)*G(J)

C---        Norme de convergence
         XNORMGMR = ABS(G(J1))
         IF (XNORMGMR .LT. EPSGMR) FINITER = .TRUE.
         WRITE(LOG_BUF,1000) 'ITER. : ',J, ' (GMRES)= ',XNORMGMR
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---        TEST DE SORTIE
         IF (FINITER) GOTO 900

      ENDDO       ! Boucle sur les itérations
      J = J - 1

900   CONTINUE
      CALL LOG_DECIND()

C---     Statistiques
      VSTAT(1) = NITER
      VSTAT(2) = J
      VSTAT(4) = XNORMGMR
      VSTAT(5) = 0
      IF (XNORMGMR .LT. EPSGMR) VSTAT(5) = 1

C---     Back-substitution to get y
      CALL SP_GMRS_BACKSB(KMAX, J, H, G)

C---     Calcule delta(u)
      CALL DCOPY(NEQ, V(1,1), 1, VRES, 1)
      CALL DSCAL(NEQ, G(1), VRES, 1)               ! VRES = V(.,1)*G(1)
      DO I=2,J
         CALL DAXPY(NEQ, G(I), V(1,I), 1, VRES, 1) ! VRES = VRES + G*V
      ENDDO

C---     Log les normes
      XNORM = VRES(IDAMAX(NEQ, VRES, 1))
      WRITE(LOG_BUF,1010) 'NB.ITER: ',J,
     &                    ' NORMES: (MAX)=',XNORM,
     &                    ' ; (GMRES)=',XNORMGMR
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     VSOL = VRES
      CALL DINIT(NDLL, ZERO, VSOL, 1)
      IERR = SP_ELEM_NEQ2NDLT(NEQ, NDLL, KLOCN, VRES, VSOL)

      GOTO 9999
C----------------------------------------------------------------------
1000  FORMAT(A,I4,A,1PE14.6E3)
1010  FORMAT(A,I4,A,1PE14.6E3,A,1PE14.6E3)
C----------------------------------------------------------------------

9999  CONTINUE
      SP_GMRS_UN_REDM = ERR_TYP()
      RETURN
      END

      SUBROUTINE SP_GMRS_BACKSB (LDIM,IDIM,A,B)
C========================================================================
C     BACK SUBSTITUTION
C========================================================================
      IMPLICIT NONE

      INTEGER LDIM
      INTEGER IDIM
      REAL*8  A(LDIM,1)
      REAL*8  B(1)

      INTEGER J, I
C------------------------------------------------------------------------

C-------  BACK-SUBSTITUTION
      DO J=IDIM,1,-1
         B(J) = B(J)/A(J,J)
         DO I=1,J-1
            B(I) = B(I) - A(I,J)*B(J)
         ENDDO
      ENDDO

      RETURN
      END
