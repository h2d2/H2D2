C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************
C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER SP_MORS_DIMILUN
C     INTEGER SP_MORS_CMLTBL
C     INTEGER SP_MORS_ASMINDCCS
C     INTEGER SP_MORS_ASMINDCRS
C     INTEGER SP_MORS_TRIIND
C     INTEGER SP_MORS_CCSCRS
C     INTEGER SP_MORS_XTRJD
C     INTEGER SP_MORS_ASMKE
C     INTEGER SP_MORS_ASGCLIM
C     INTEGER SP_MORS_FACT
C     INTEGER SP_MORS_SOLV
C     INTEGER SP_MORS_SCAL
C     INTEGER SP_MORS_ADDD
C     INTEGER SP_MORS_MULD
C     INTEGER SP_MORS_MULV
C     INTEGER SP_MORS_LUMP
C     INTEGER SP_MORS_DMIN
C     INTEGER SP_MORS_LISMAT
C     INTEGER SP_MORS_DMPMAT
C   Private:
C     INTEGER SP_MORS_ASMDIMCCS
C     INTEGER SP_MORS_ASMDIMCRS
C     INTEGER SP_MORS_LISMAT_AUX
C
C************************************************************************

C************************************************************************
C Sommaire : SP_MORS_ASMDIMCCS
C
C Description:
C     La fonction SP_MORS_ASMDIMCCS assemble par cumule la dimension de la
C     table IA des pointeurs pour un stockage morse de type compression
C     de colonne (CCS). Elle cumule uniquement les liens créés par les DDL
C     de deux noeuds dans le triangle strictement supérieur.
C     <p>
C     La fonction est un élément d'un dimensionnement ILU(n).
C
C Entrée:
C     INTEGER NDLN               Nombre de DLL par noeuds
C     INTEGER KLOCN1(NDLN)       DDL du noeud 1
C     INTEGER KLOCN2(NDLN)       DDL du noeud 2
C     INTEGER NIA                Dimension de IA
C
C Sortie:
C     INTEGER IA (NIA)           Table IA des pointeurs
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_ASMDIMCCS(NDLN, KLOCN1, KLOCN2, NIA, IA)

      IMPLICIT NONE

      INTEGER NDLN
      INTEGER KLOCN1(NDLN)
      INTEGER KLOCN2(NDLN)
      INTEGER NIA
      INTEGER IA(NIA)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      INTEGER J, JJ
      INTEGER IC, IL
C-----------------------------------------------------------------------

C---     BOUCLE SUR LES DEGRES DE LIBERTE
      DO I=1,NDLN
         IJ = KLOCN1(I)
         IF (IJ .EQ. 0) GOTO 199

C---        BOUCLE SUR LES DDL DE COUPLAGE INFERIEUR
         DO J=1,NDLN
            JJ = KLOCN2(J)
            IF (JJ .NE. 0) THEN
               IC = MAX(ABS(IJ), ABS(JJ))    ! Colonne
               IL = MIN(ABS(IJ), ABS(JJ))    ! Ligne
               IF (IL .EQ. IJ .OR. IL .EQ. JJ)
     &            IA(IC) = IA(IC)+1
            ENDIF
         ENDDO

199      CONTINUE
      ENDDO

      SP_MORS_ASMDIMCCS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_ASMDIMCRS
C
C Description:
C     La fonction SP_MORS_ASMDIMCRS assemble par cumule la dimension de la
C     table IA des pointeurs pour un stockage morse de type compression
C     de ligne (CRS). Elle cumule les liens créés par les DDL de deux noeuds
C     dans la matrice au complet.
C
C Entrée:
C     INTEGER NDLN               Nombre de DLL par noeuds
C     INTEGER KLOCN1(NDLN)       DDL du noeud 1
C     INTEGER KLOCN2(NDLN)       DDL du noeud 2
C     INTEGER NIA                Dimension de IA
C
C Sortie:
C     INTEGER IA (NIA)           Table IA des pointeurs
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_ASMDIMCRS(NDLN, KLOCN1, KLOCN2, NIA, IA)

      IMPLICIT NONE

      INTEGER NDLN
      INTEGER KLOCN1(NDLN)
      INTEGER KLOCN2(NDLN)
      INTEGER NIA
      INTEGER IA(NIA)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      INTEGER J, JJ
C-----------------------------------------------------------------------

C---     Boucle sur les couplages de DDL
      DO I=1,NDLN
         IJ = KLOCN1(I)
         IF (IJ .EQ. 0) CYCLE

         IF (IJ .GT. 0) THEN
            DO J=1,NDLN
               JJ = KLOCN2(J)
               IF (JJ .NE. 0) IA(IJ) = IA(IJ)+1
            ENDDO
         ENDIF
!dir$ IVDEP
         DO J=1,NDLN
            JJ = KLOCN2(J)
            IF (JJ .GT. 0) IA(JJ) = IA(JJ)+1
         ENDDO

      ENDDO

      SP_MORS_ASMDIMCRS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_DIMILUN
C
C Description:
C     La fonction <code>SP_MORS_DIMILUN(...)</code> réserve de l'espace
C     supplémentaire pour ILU(n) dans IA, la table qui contient à cette
C     étape la dimension de chaque ligne. De même KLD, contient à cette
C     étape la dimension de chaque hauteur de colonne.
C
C Entrée:
C     INTEGER ILU             Remplissage ILU demandé
C     INTEGER NIA             Dimension de IA
C     INTEGER IA   (NIA)      Table de la dimension de chaque ligne
C     INTEGER KLD  (NIA)      Table globales des hauteur de colonne
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_DIMILUN(ILU, NIA, IA, KLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_DIMILUN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ILU
      INTEGER NIA
      INTEGER IA (NIA)
      INTEGER KLD(NIA)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, ILUMIN, ILUMAX
C---------------------------------------------------------------

      LOG_ZNE = 'h2d2.umef.spelem'

C---     AUGMENTE A ILU
      ILUMIN = NIA+1
      ILUMAX = 0
      IF (ILU .LT. 0) THEN
         DO I=1,NIA
            ILUMIN = MIN(ILUMIN, KLD(I)-IA(I))
            ILUMAX = MAX(ILUMAX, KLD(I)-IA(I))
            IA(I) = KLD(I)
         ENDDO
      ELSEIF (ILU .EQ. 0) THEN
         DO I=1,NIA
            ILUMIN = MIN(ILUMIN, KLD(I)-IA(I))
            ILUMAX = MAX(ILUMAX, KLD(I)-IA(I))
         ENDDO
      ELSE
         DO I=1,NIA
            ILUMIN = MIN(ILUMIN, KLD(I)-IA(I))
            ILUMAX = MAX(ILUMAX, KLD(I)-IA(I))
            IA(I)= MIN(IA(I)+ILU, KLD(I))
         ENDDO
      ENDIF

      WRITE(LOG_BUF, '(A)') 'MSG_DIM_ILU:'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(A,I6)') 'MSG_ILU#<15>#=', ILU
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I6)') 'MSG_ILU_MIN#<15>#=', ILUMIN
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I6)') 'MSG_ILU_MAX#<15>#=', ILUMAX
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

      IF (ILUMIN .LT. 0) GOTO 9901

      GOTO 9999
C---------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)') 'ERR_LARGEUR_BANDE_NEGATIVE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_MORS_DIMILUN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_CMLTBL
C
C Description:
C     La fonction <code>SP_MORS_CMLTBL(...)</code> cumule les deux tables
C     IA et KLD.
C
C Entrée:
C     INTEGER NEQL            Nombre d'Equation Local
C     INTEGER IA   (*)        Table de la dimension de chaque ligne
C     INTEGER KLD  (*)        Table globales des hauteur de colonne
C
C Sortie:
C     INTEGER IA   (*)        Table cumulative
C     INTEGER KLD  (*)        Table cumulative
C
C Notes:
C************************************************************************
      FUNCTION SP_MORS_CMLTBL(NEQL, IA, KLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_CMLTBL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQL
      INTEGER IA (NEQL+1)
      INTEGER KLD(NEQL+1)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I
      INTEGER II, JJ, IP1, JP1
C---------------------------------------------------------------

C---     TRANSFORME KLD ET IA EN TABLES CUMULATIVES
      II = 1
      JJ = 1
      DO I=1,NEQL
         IP1 = KLD(I+1)
         JP1 = IA (I+1)
         KLD(I+1) = KLD(I) + II
         IA (I+1) = IA (I) + JJ
         II = IP1
         JJ = JP1
      ENDDO
      KLD(1) = 1
      IA (1) = 1

      SP_MORS_CMLTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_ASMINDCCS
C
C Description:
C     La fonction <code>SP_MORS_ASMINDCCS(...)</code> assemble dans JA les
C     indices de ligne pour un stockage morse du type compression de colonne (CCS).
C     Elle assemble uniquement les liens créés par les DDL de deux noeuds dans
C     le triangle strictement supérieur.
C     <p>
C     Les indices sont simplement assemblés et ne sont pas triés par ordre
C     croissant.
C
C Entrée:
C     INTEGER NDLE            Nombre de Degré de Liberté Élémentaire
C     INTEGER KLOCE(*)        Table de LOCalisation Élémentaire
C     INTEGER IA   (*)        Table cumulative de la dimension de chaque colonne
C
C Sortie:
C     INTEGER JA   (*)        Table des indices de ligne
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_ASMINDCCS(NDLE, KLOCE, IA, JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ASMINDCCS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(*)
      INTEGER IA   (*)
      INTEGER JA   (*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      INTEGER J, JJ
      INTEGER II
C---------------------------------------------------------------

C---     BOUCLE SUR LES DEGRES DE LIBERTE
      DO I=1,NDLE
         IJ = ABS(KLOCE(I))
         IF (IJ .EQ. 0) GOTO 109

C---        BOUCLE SUR LES DDL DE COUPLAGE INFERIEUR
         DO J=1,NDLE
            JJ = KLOCE(J)
            IF (JJ .LE. 0 .OR. JJ .GE. IJ) GOTO 119

C---           ENREGISTREMENT DE jj DANS ja SI CELA N'A PAS ETE EFFECTUE
            DO II=IA(IJ),IA(IJ+1)-1
               IF (JA(II) .NE. 0) THEN
                  IF (JJ .EQ. JA(II)) GOTO 119
               ELSE
                  JA(II)=JJ
                  GOTO 119
               ENDIF
            ENDDO

119         CONTINUE
         ENDDO

109      CONTINUE
      ENDDO

      SP_MORS_ASMINDCCS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_ASMINDCRS
C
C Description:
C     La fonction <code>SP_MORS_ASMINDCRS(...)</code> assemble dans JA les
C     indices de colonne pour un stockage morse du type compression de ligne (CRS).
C     Elle assemble les liens créés par les DDL de deux noeuds dans la
C     matrice au complet.
C     <p>
C     Les indices sont simplement assemblés et ne sont pas triés par ordre
C     croissant.
C
C Entrée:
C     INTEGER NDLE            Nombre de Degré de Liberté Élémentaire
C     INTEGER KLOCE(*)        Table de LOCalisation Élémentaire
C     INTEGER IA   (*)        Table cumulative de la dimension de chaque colonne
C
C Sortie:
C     INTEGER JA   (*)        Table des indices de ligne
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_ASMINDCRS(NDLE, KLOCE, IA, JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ASMINDCRS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(*)
      INTEGER IA   (*)
      INTEGER JA   (*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      INTEGER J, JJ
      INTEGER II, IC
C---------------------------------------------------------------

C---     BOUCLE SUR LES COUPLAGES
      DO I=1,NDLE
         IJ = KLOCE(I)
         IF (IJ .EQ. 0) GOTO 109

         DO J=1,NDLE
            JJ = KLOCE(J)
            IF (JJ .EQ. 0) GOTO 119

C---           Couplage ij-jj
            IF (IJ .GT. 0) THEN
               IC = ABS(JJ)               ! Indice de colonne
               DO II=IA(IJ),IA(IJ+1)-1
                  IF (JA(II) .NE. 0) THEN
                     IF (IC .EQ. JA(II)) GOTO 111
                  ELSE
                     JA(II) = IC
                     GOTO 111
                  ENDIF
               ENDDO
D              CALL ERR_ASR(II .LT. IA(IJ+1))
111            CONTINUE
            ENDIF

C---           Couplage jj-ij
            IF (JJ .GT. 0) THEN
               IC = ABS(IJ)               ! Indice de colonne
               DO II=IA(JJ),IA(JJ+1)-1
                  IF (JA(II) .NE. 0) THEN
                     IF (IC .EQ. JA(II)) GOTO 112
                  ELSE
                     JA(II) = IC
                     GOTO 112
                  ENDIF
               ENDDO
D              CALL ERR_ASR(II .LT. IA(JJ+1))
112            CONTINUE
            ENDIF

119         CONTINUE
         ENDDO

109      CONTINUE
      ENDDO

      SP_MORS_ASMINDCRS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_TRIIND
C
C Description:
C     La fonction <code>SP_MORS_TRIIND(...)</code> trie pour chaque colonne
C     les indices des lignes contenus dans JA.
C     <p>
C     Pour CRS, le résultat trié est retourné dans JA, les tables
C     JR, IAC, JAC ne sont pas utilisées.
C     Pour CCS, le résultat trié est retourné dans IAC et JAC, la table
C     JA est modifiée par la fonction.
C
C Entrée:
C     LOGICAL ESTCCS          .TRUE. si CCS, sinon est CRS
C     INTEGER ILU             Remplissage ILU demandé
C     INTEGER NEQL            Nombre d'Équations Local
C     INTEGER JR   (*)        Table de travail
C     INTEGER IA   (*)        Table cumulative de la dimension de chaque colonne
C
C Sortie:
C     INTEGER JA   (*)        Table des indices de ligne
C     INTEGER IAC  (*)        Table cumulative de la dimension de chaque colonne
C     INTEGER JAC  (*)        Table triée des indices de ligne
C
C Notes:
C************************************************************************
      FUNCTION SP_MORS_TRIIND(ESTCCS, ILU, NEQL, JR, IA, JA, IAC, JAC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_TRIIND
CDEC$ ENDIF

      IMPLICIT NONE

      LOGICAL ESTCCS
      INTEGER ILU
      INTEGER NEQL
      INTEGER JR(*)
      INTEGER IA(*)
      INTEGER JA(*)
      INTEGER IAC(*)
      INTEGER JAC(*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER IJ, II, JJ
      INTEGER JVAL
      INTEGER J, K, IFILL, JFILL
C---------------------------------------------------------------

C---     CRS ou CCS
      IF (ESTCCS) GOTO 200

C---     Trie pour CRS (uniquement ILU(0))
100   CONTINUE
      IF (ILU .NE. 0) GOTO 9900
      DO IJ=1,NEQL

C---        CONTROLE LA COLONNE
         IF (IA(IJ+1) .EQ. IA(IJ)) GOTO 110

C---        CLASSE LES INDICES PAR ORDRE CROISSANT
         DO II=IA(IJ),IA(IJ+1)-2
            DO JJ=II+1,IA(IJ+1)-1
               IF (JA(II) .GT. JA(JJ)) THEN
                  JVAL   = JA(II)
                  JA(II) = JA(JJ)
                  JA(JJ) = JVAL
               ENDIF
            ENDDO
         ENDDO
D        CALL ERR_ASR(JA(IA(IJ))     .GT. 0)
D        CALL ERR_ASR(JA(IA(IJ+1)-1) .GT. 0)

110      CONTINUE
      ENDDO
      GOTO 9999

C---     Trie pour CCS
200   CONTINUE
      IAC(1) = 1
      DO IJ=1,NEQL

C---        INITIALISE IAC POUR CHAQUE COLONNE
         IAC(IJ+1)=IAC(IJ)

C---        CONTROLE DE LA COLONNE
         IF (IA(IJ+1) .EQ. IA(IJ)) GOTO 210

C---        TRANSPOSE JA DANS JR
         K = 0
         DO II=IA(IJ),IA(IJ+1)-1
            K = K+1
            JR(K) = JA(II)
         ENDDO

C---        CLASSE LES INDICES PAR ORDRE CROISSANT
         DO II=1,K-1
            DO JJ=II,K
               IF (JR(II) .LT. JR(JJ))THEN
                  JVAL   = JR(II)
                  JR(II) = JR(JJ)
                  JR(JJ) = JVAL
               ENDIF
            ENDDO
         ENDDO

C---        AJOUT DES TERMES SUPPLEMENTAIRES A PARTIR DE LA DIAGONALE
         J = 1
         JFILL = IJ
         IFILL = 0
         DO II=IA(IJ+1)-1,IA(IJ),-1
            JVAL = JR(J)
            IF (IFILL .GE. ILU) THEN
               JA(II) = JVAL
               J = J+1
            ELSE
               JFILL=JFILL-1
               IF (JVAL .GE. JFILL) THEN
                  J = J+1
               ELSE
                  JVAL = JFILL
                  IFILL= IFILL+1
               ENDIF
               JA(II) = JVAL
            ENDIF
         ENDDO

C---        STOCKE LES TERMES DANS IAC ET JAC DANS L'ORDRE CROISSANT
210      CONTINUE
         DO II=IA(IJ),IA(IJ+1)-1
            JAC(IAC(IJ+1)) = JA(II)
            IAC(IJ+1) = IAC(IJ+1)+1
         ENDDO
D        CALL ERR_ASR(IAC(IJ).EQ.IAC(IJ+1) .OR. JAC(IAC(IJ))    .NE.0)
D        CALL ERR_ASR(IAC(IJ).EQ.IAC(IJ+1) .OR. JAC(IAC(IJ+1)-1).NE.0)

      ENDDO

      GOTO 9999
C---------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_ILUN_INVALIDE_EN_CCS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_MORS_TRIIND = ERR_TYP()
      END

C************************************************************************
C Sommaire : SP_MORS_CCSCRS
C
C Description:
C     La fonction <code>SP_MORS_CCSCRS(...)</code> transforme les tables
C     de stockage d'une matrice morse de type compression de colonne à
C     compression de ligne.
C
C Entrée:
C     INTEGER NDLT            Nombre de Degré de Liberté Total
C     INTEGER NKGP            Nombre de termes de la matrice
C     INTEGER JR   (*)        Table de travail
C     INTEGER IAC  (*)        Table CCS de la dimension de chaque ligne
C     INTEGER JAC  (*)        Table CCS des indices de ligne
C
C Sortie:
C     INTEGER IA   (*)        Table CSR de la dimension de chaque ligne
C     INTEGER JA   (*)        Table CSR des indices de ligne
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SP_MORS_CCSCRS(NDLT, NKGP, JR, IAC, JAC, IA, JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_CCSCRS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLT
      INTEGER NKGP
      INTEGER JR (*)
      INTEGER IAC(*)
      INTEGER JAC(*)
      INTEGER IA (*)
      INTEGER JA (*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER II, IJ, IL, JL
      INTEGER IPI
C---------------------------------------------------------------

C---     INITIALISE LES TABLES IA ET JA
      CALL IINIT(NDLT+1, 0, IA, 1)
      CALL IINIT(NKGP  , 0, JA, 1)

C---     BOUCLE SIMULTANEE SUR LES TRIANGLES INF. SUP. ET LA DIAGONALE
C        POUR DIMENSIONNER LA TABLE DES POINTEURS DE LIGNES IA
      DO IL=1,NDLT
         IA(IL) = IA(IL) + IAC(IL+1)-IAC(IL)+1
C---        SI LA COLONNE N'EST PAS VIDE
         IF (IAC(IL+1) .NE. IAC(IL)) THEN
            DO II=IAC(IL),IAC(IL+1)-1
               JL = JAC(II)
               IA(JL) = IA(JL)+1
            ENDDO
         ENDIF
      ENDDO

C---     FAIS DE IA UNE TABLE CUMULATIVE
      II = 1
      DO IJ=1,NDLT
         IPI = IA(IJ+1)
         IA(IJ+1) = IA(IJ)+II
         II = IPI
      ENDDO
      IA(1) = 1

C---     CONTROLE DE LA DIMENSION DE JA
      IF ((IA(NDLT+1)-IA(1)) .NE. NKGP) GOTO 9900
C
C--- BOUCLE SUR LE TRIANGLE INF. ET LA DIAGONALE POUR IDENTIFIER LES
C    TERMES NON NULS DU TRIANGLE INFERIEUR (REMARQUE : LES TERMES DU TRIANGLE
C    INFERIEUR SONT OBTENUS DIRECTEMENT DU STOCKAGE PAR COMPRESSION DE COLONNE
C    LA STRUCTURE SYMETRIQUE DE LA MATRICE PERMETTANT D'EFFECTUER CETTE OPERATION)
      DO IL=1,NDLT
         JR(IL)=IA(IL)

C---        SI LA COLONNE N'EST PAS VIDE
         IF (IAC(IL+1) .NE. IAC(IL)) THEN
            DO II=IAC(IL),IAC(IL+1)-1
               JA(JR(IL))=JAC(II)
               JR(IL)=JR(IL)+1
            ENDDO
         ENDIF

C---        TERME DIAGONAL
         JA(JR(IL))=IL
         JR(IL)=JR(IL)+1
      ENDDO

C--- OPERATION SUR LE TRIANGLE SUPERIEUR POUR IDENTIFIER LES TERMES NON NULS
C    ET COMPLETER LA TABLE JA.
      DO IL=1,NDLT
C---        SI LA COLONNE N'EST PAS VIDE
         IF (IAC(IL+1) .NE. IAC(IL)) THEN
            DO II=IAC(IL),IAC(IL+1)-1
               JL = JAC(II)
               JA(JR(JL)) = IL
               JR(JL) = JR(JL)+1
            ENDDO
         ENDIF
      ENDDO

      GOTO 9999
C---------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_CALCUL_JAP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_MORS_CCSCRS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_MORS_XTRJD
C
C Description:
C     La fonction <code>SP_MORS_XTRJD(...)</code> extrais les indices
C     des termes diagonaux.
C
C Entrée:
C     NEQ                     Nombre d'EQuations (local)
C     INTEGER IA   (*)        Table CRS de la dimension de chaque ligne
C     INTEGER JA   (*)        Table CRS des indices de ligne
C
C Sortie:
C     INTEGER JD   (*)        Table CRS des indices au terme diagonal
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MORS_XTRJD(NEQ, IA, JA, JD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_XTRJD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA (*)
      INTEGER JA (*)
      INTEGER JD (*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER IL, II, ID
      INTEGER ITDM        ! Terme diag manquant
C---------------------------------------------------------------

      ITDM = 0
      CALL IINIT(NEQ, -1, JD, 1)

      DO IL=1,NEQ
         DO II=IA(IL),IA(IL+1)-1
            IF (JA(II) .EQ. IL) THEN
               JD(IL) = II
               GOTO 199
            ENDIF
         ENDDO
199      CONTINUE
         IF (JD(IL) .EQ. -1) ITDM = IL
      ENDDO
      IF (ITDM .GT. 0) GOTO 9900

      GOTO 9999
C---------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_TERME_DIAGONAL_MANQUANT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)') 'MSG_EQUATION', ': ', IL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_MORS_XTRJD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_MORS_ASMKE
C
C Description:
C     La fonction <code>SP_MORS_ASMKE(...)</code> assemble la
C     matrice élémentaire dans une matrice globale stockée par
C     compression des lignes.
C
C Entrée:
C     NDLE   : NOMBRE DE DDL PAR ELEMENT
C     KLOCE  : TABLE ELEMENTAIRE DE LOCALISATION DES DDL
C     VKE    : MATRICE ELEMENTAIRE
C     NEQ    : NOMBRE D'EQUATIONS (local)
C     JR     : VECTEUR DE TRAVAIL
C     IA     : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA     : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG    : MATRICE GLOBALE
C     IOFFSET: OFFSET DE VKG(1) PAR RAPPORT À JA(1)
C
C Sortie:
C
C Notes:
C     L'instruction atomic de OMP est très chère, même si
C     OMP_NUM_THREADS=1. On a donc recours au coloriage des
C     éléments (mars 2009)
C************************************************************************
C************************************************************************
C     Insertion sort adapted from
C     http://jean-pierre.moreau.pagesperso-orange.fr/Fortran/sort1_f90.txt
C
C     Sort ascending according to K(1,:)
C************************************************************************
      SUBROUTINE SP_MORS_PIKSRT(N1, N2, K)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N1
      INTEGER, INTENT(IN) :: N2
      INTEGER, INTENT(INOUT) :: K(N1, N2)

      INTEGER I, J
      INTEGER A(N1)
C-----------------------------------------------------------------------
      DO J=2,N2
         A = K(:,J)
         I = J - 1
         DO WHILE (I > 0  .AND. K(1, I) > A(1))
            K(:,I+1) = K(:,I)
            I = I - 1
         END DO
         K(:,I+1) = A(:)
      END DO
      RETURN
      END

C************************************************************************
C     Adapted from Numerical Recipes
C
C     Given an array XX(1:N), and a value X, returns an index J,
C     J in [1,N+1], such that if X is inserted at XX(J), XX is kept sorted.
C     The case X = XX(J) is possible.
C************************************************************************
      FUNCTION SP_MORS_FIND_I(XX, X, JMIN, JMAX) RESULT(JU)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: XX(*)
      INTEGER, INTENT(IN) :: X
      INTEGER, INTENT(IN) :: JMIN
      INTEGER, INTENT(IN) :: JMAX
      
      INTEGER :: JL, JM, JU
C-----------------------------------------------------------------------
      IF (XX(JMIN) .EQ. X) THEN
         JU = JMIN
         RETURN
      ENDIF

      JL = JMIN                  ! Initialize lower
      JU = JMAX + 1              ! and upper limits.
      DO WHILE(JU-JL > 1)        ! Repeat until this condition is satisfied.
         JM = (JU+JL)/2          ! Compute a midpoint,
         IF (X > XX(JM)) THEN
            JL = JM              ! and replace either the lower limit
         ELSE
            JU = JM              ! or the upper limit, as appropriate.
         END IF
      END DO

      END FUNCTION SP_MORS_FIND_I
      
      FUNCTION SP_MORS_ASMKE(NDLE,
     &                       KLOCE,
     &                       VKE,
     &                       NEQ,
     &                       IA,
     &                       JA,
     &                       VKG,
     &                       IOFFSET)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      REAL*8  VKE  (NDLE, NDLE)
      INTEGER NEQ
      INTEGER IA   (NEQ+1)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      INTEGER IOFFSET

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, J, IL, JL, IE, JE, JK
D     INTEGER N
      INTEGER J1, JMIN, JMAX
      INTEGER KSRT(3,24)
      INTEGER KJK(24)
      INTEGER SP_MORS_FIND_I
C-----------------------------------------------------------------------

C---     Contrôle de la matrice
D     N = COUNT(VKE(1:NDLE,1:NDLE) .EQ. 0.0D0)
D     IF (N .EQ. NDLE*NDLE) GOTO 9900

C---     Trie KLOCE en valeurs absolue
      KSRT(1,1:NDLE) = ABS(KLOCE(:))
      KSRT(2,1:NDLE) = KLOCE(:)
      KSRT(3,1:NDLE) = (/ (I, I=1,NDLE) /)
      CALL SP_MORS_PIKSRT(3, NDLE, KSRT)
      
C---     Premier indice non nul
      J1 = 1
      DO WHILE (KSRT(1,J1) <= 0)
         J1 = J1 + 1
      ENDDO

C---     Boucle sur les lignes
      DO I=J1,NDLE
         IL = KSRT(2, I)
         IF (IL .LE. 0) CYCLE       ! IL peut être négatif
D        CALL ERR_ASR(IL .LE. NEQ)
         IE = KSRT(3,I)

C---        Indices dans VKG
         JMIN = IA(IL)              ! Indice min-max
         JMAX = IA(IL+1)-1          ! dans JA
         DO J=J1,NDLE
            JL = KSRT(1, J)
D           CALL ERR_ASR(JL .GE.   1)
D           CALL ERR_ASR(JL .LE. NEQ)

            JK = SP_MORS_FIND_I(JA, JL, JMIN, JMAX) - IOFFSET
            KJK(J) = JK
            JMIN = JK + 1
         ENDDO
         
C---        Assemblage
!$omp simd
         DO J=J1,NDLE
            JK = KJK(J)
            JE = KSRT(3, J)
            VKG(JK) = VKG(JK) + VKE(IE,JE)
         ENDDO

      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_MATRICE_ELEMENTAIRE_NULLE')
      GOTO 9999

9999  CONTINUE
      SP_MORS_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_MORS_ASMFE
C
C Description:
C     La fonction <code>SP_MORS_ASMFE(...)</code> assemble la
C     colonne élémentaire dans une matrice globale stockée par
C     compression des lignes.
C
C Entrée:
C     NDLE   : NOMBRE DE DDL PAR ELEMENT
C     JCOL   : INDICE DE LA COLONNE
C     KLOCE  : TABLE ELEMENTAIRE DE LOCALISATION DES DDL
C     VFE    : VECTEUR ELEMENTAIRE
C     NEQ    : NOMBRE D'EQUATIONS (LOCAL)
C     JR     : VECTEUR DE TRAVAIL
C     IA     : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA     : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG    : MATRICE GLOBALE
C     IOFFSET: OFFSET DE VKG(1) PAR RAPPORT À JA(1)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MORS_ASMFE(NDLE,
     &                       JCOL,
     &                       KLOCE,
     &                       VFE,
     &                       NEQ,
     &                       IA,
     &                       JA,
     &                       VKG,
     &                       IOFFSET)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ASMFE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER JCOL
      INTEGER KLOCE(NDLE)
      REAL*8  VFE  (NDLE)
      INTEGER NEQ
      INTEGER IA   (NEQ+1)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      INTEGER IOFFSET

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, J, IL, JL, IE, JE, JK
      INTEGER J1, JMIN, JMAX
      INTEGER KSRT(3,24)
      INTEGER SP_MORS_FIND_I
C-----------------------------------------------------------------------

C---     Trie KLOCE en valeurs absolue
      KSRT(1,:) = ABS(KLOCE(:))
      KSRT(2,:) = KLOCE(:)
      KSRT(3,:) = (/ (I, I=1,NDLE) /)
      CALL SP_MORS_PIKSRT(3, NDLE, KSRT)
      
C---     Premier indice non nul
      J1 = 1
      DO WHILE (KSRT(1,J1) <= 0)
         J1 = J1 + 1
      ENDDO

C---     Boucle sur les lignes
      DO I=J1,NDLE
         IL = KSRT(2, I)
         IF (IL .LE. 0) CYCLE       ! IL peut être négatif
D        CALL ERR_ASR(IL .LE. NEQ)
         IE = KSRT(3,I)

C---        Indices dans VKG
         JMIN = IA(IL)              ! Indice min-max
         JMAX = IA(IL+1)-1          ! dans JA
         JL = JCOL
D        CALL ERR_ASR(JL .GE.   1)
D        CALL ERR_ASR(JL .LE. NEQ)
         JK = SP_MORS_FIND_I(JA, JL, JMIN, JMAX) - IOFFSET
         
C---        Assemblage
         VKG(JK) = VKG(JK) + VFE(IE)
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_MATRICE_ELEMENTAIRE_NULLE')
      GOTO 9999

9999  CONTINUE
      SP_MORS_ASMFE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_MORS_ASGCLIM
C
C Description:
C     INTRODUCTION DES CONDITIONS AUX LIMITES PAR PENALISATION
C     DANS UNE MATRICE GLOBALE STOCKEE PAR COMPRESSION DES LIGNES.
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     KDIMP : TABLE DES CODES DES DDL IMPOSÉS
C     JD    : TABLE DES POINTEURS AU TERME DIAGONAL
C     VKG   : MATRICE GLOBALE
C
C Sortie:
C     VPEN  : VALEUR DE PENALISATION
C
C Notes:
C     Le code du bit Dirichlet est redéfini ici pour couper les
C     dépendances. Attention à la fragilité!!
C************************************************************************
      FUNCTION SP_MORS_ASGCLIM(NEQ, KDIMP, JD, VKG, VPEN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ASGCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER KDIMP(NEQ)
      INTEGER JD   (NEQ)
      REAL*8  VKG  (*)
      REAL*8  VPEN

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER IP, ID
      REAL*8  SOM
      REAL*8  VNDLT

      REAL*8  ZERO
      REAL*8  GRAND
      PARAMETER (ZERO  =0.000000000000000000000000000D+00)
      PARAMETER (GRAND =1.000000000000000000000000000D+16)

      INTEGER IBITCLDIR
      PARAMETER (IBITCLDIR = 1)  ! Défini également dans 'eacdcl.fi'
C-----------------------------------------------------------------------

C---     SOMME DE LA VALEUR ABSOLUE DES TERMES DIAGONAUX
      SOM = ZERO
      DO IP=1,NEQ
         SOM = SOM + ABS(VKG(JD(IP)))
      ENDDO
C      CALL MPREDUIS(MP$SUM,  SOM, 1)
      VNDLT = NEQ
C      CALL MPREDUIS(MP$SUM,VNDLT, 1)

C---     MISE A JOUR DU TERME DE PENALISATION
      VPEN = SOM*GRAND/VNDLT

C---     INTRODUCTION DES C.L. PAR PENALISATION DU TERME DIAGONAL
      DO IP=1,NEQ
         IF (BTEST(KDIMP(IP), IBITCLDIR)) THEN
            ID = JD(IP)
CCC!$__omp_atomic     ! cf Note SP_MORS_ASMKE
            VKG(ID) = VKG(ID) + VPEN
         ENDIF
      ENDDO

      SP_MORS_ASGCLIM = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_FACT
C
C Description:
C     La fonction SP_MORS_FACT factorise la matrice stockée par
C     compression de ligne en LU. Il n'y a pas de remplissage au
C     delà de la place déjà allouée.
C     <p>
C     L'appel est valide uniquement pour une matrice carrée.
C
C Entrée:
C     NEQ      : DIMENSION DE LA MATRICE
C     JR & JR2 : TABLES DE TRAVAIL
C     JD       : TABLE DES POINTEURS AU TERME DIAGONAL
C     IA       : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA       : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG      : MATRICE GLOBALE
C
C Sortie:
C
C Notes:
C****************************************************************
      FUNCTION SP_MORS_FACT(NEQ, JR, JR2, JD, IA, JA, VKG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_FACT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER JR (NEQ)
      INTEGER JR2(NEQ)
      INTEGER JD (NEQ)
      INTEGER IA (*)
      INTEGER JA (*)
      REAL*8  VKG(*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IPIV, JPIV
      INTEGER II, IL
      INTEGER JJ, JK, JK2
      REAL*8  PIVOT, AI

      REAL*8  ZERO
      REAL*8  UN
      REAL*8  PETIT
      PARAMETER (ZERO  =0.000000000000000000000000000D+00)
      PARAMETER (UN    =1.000000000000000000000000000D+00)
      PARAMETER (PETIT =1.000000000000000000000000000D-32)
C----------------------------------------------------------

      IERR = 0

C---     INITIALISE LES TABLES DE TRAVAIL
      CALL IINIT(NEQ, 0, JR,  1)
      CALL IINIT(NEQ, 0, JR2, 1)

C---     BOUCLE SUR LES PIVOTS
      DO IPIV=1,NEQ

C---        DECOMPRESSION DE LA LIGNE DU PIVOT
         DO II=IA(IPIV),IA(IPIV+1)-1
            JR(JA(II))=II
         ENDDO

C---        EXTRACTION DU PIVOT
         IF (ABS(VKG(JR(IPIV))) .LE. PETIT) GOTO 9900
         PIVOT = UN / VKG(JR(IPIV))
         VKG(JR(IPIV)) = PIVOT

C---        OPERATION SUR LES LIGNES
         DO JPIV =JD(IPIV)+1,IA(IPIV+1)-1
            IL = JA(JPIV)

C---           DECOMPRESSE LA LIGNE
            DO II=IA(IL),IA(IL+1)-1
               JR2(JA(II)) = II
            ENDDO

C---           OPERATION SUR LES COLONNES
            IF (JR2(IPIV) .GT. 0) THEN
               AI = VKG(JR2(IPIV))*PIVOT
               VKG(JR2(IPIV)) = AI

               DO JJ=JD(IPIV)+1,IA(IPIV+1)-1
                  JK = JA(JJ)
                  JK2= JR2(JK)
                  IF(JK2 .GT. 0) VKG(JK2)=VKG(JK2)-AI*VKG(JR(JK))
               ENDDO
            ENDIF

C---           REINITIALISE JR2
            DO II=IA(IL),IA(IL+1)-1
               JR2(JA(II)) = 0
            ENDDO

         ENDDO
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'FTL_PIVOT_NUL'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_EQUATION', ' : ', IPIV
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,1PE25.17E3)') 'MSG_VALEUR',' : ',VKG(JR(IPIV))
      CALL ERR_AJT(ERR_BUF)

9999  CONTINUE
      SP_MORS_FACT = ERR_TYP()
      END

C**************************************************************
C Sommaire: SP_MORS_SOLV
C
C Description:
C     La fonction SP_MORS_SOLV résous un système algébrique stocké
C     par compression morse des lignes. Le vecteur du second membre
C     contient en sortie la solution.
C     <p>
C     L'appel est valide uniquement pour une matrice carrée.
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     JD    : TABLE DES POINTEURS AU TERME DIAGONAL
C     IA    : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA    : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG   : MATRICE GLOBALE FACTORISEE EN LU
C     V     : SCALAIRE MULTIPLICATIF
C     VRES  : VECTEUR SECOND MEMBRE EN ENTREE ET SOLUTION EN SORTIE
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_SOLV(NEQ, JD, IA, JA, VKG, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_SOLV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER JD   (NEQ)
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      REAL*8  VRES (NEQ)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER I, K
C----------------------------------------------------------

C---     RESOLUTION PAR DESCENTE
      DO I=1,NEQ
         DO K=IA(I),JD(I)-1
            VRES(I) = VRES(I) - VKG(K)*VRES(JA(K))
         ENDDO
      ENDDO

C---     RESOLUTION PAR REMONTEE
      DO I=NEQ,1,-1
         DO K=JD(I)+1,IA(I+1)-1
            VRES(I) = VRES(I) - VKG(K)*VRES(JA(K))
         ENDDO
         VRES(I) = VRES(I)*VKG(JD(I))
      ENDDO

      SP_MORS_SOLV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_SCAL
C
C Description:
C     La fonction SP_MORS_SCAL multiplie la matrice par un scalaire.
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     IA    : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA    : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG   : MATRICE GLOBALE
C     V     : SCALAIRE MULTIPLICATIF
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_SCAL(NEQ, IA, JA, VKG, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_SCAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      REAL*8  V

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER NKG
C----------------------------------------------------------

      NKG = IA(NEQ+1) - 1
      CALL DSCAL(NKG, V, VKG, 1)

      SP_MORS_SCAL = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_ADDD
C
C Description:
C     La fonction SP_MORS_ADDD additionne à la matrice une
C     matrice diagonale de diagonale V.
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     JD    : TABLE DES POINTEURS AU TERME DIAGONAL
C     VKG   : MATRICE GLOBALE
C     V     : DIAGONALE DE LA MATRICE DIAGONALE
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_ADDD(NEQ, JD, VKG, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_ADDD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER JD (NEQ)
      REAL*8  VKG(*)
      REAL*8  V  (NEQ)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, ID
C----------------------------------------------------------

!$omp parallel do default(shared) private(I, ID)
      DO I=1,NEQ
         ID = JD(I)
         IF (ID .GT. 0) VKG(ID) = VKG(ID) + V(I)
      ENDDO
!$omp end parallel do

      SP_MORS_ADDD = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_MULD
C
C Description:
C     La fonction SP_MORS_MULD multiplie la matrice par une
C     matrice diagonale de diagonale V.
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     IA    : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA    : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG   : MATRICE GLOBALE
C     V     : DIAGONALE DE LA MATRICE DIAGONALE
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_MULD(NEQ, IA, JA, VKG, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_MULD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      REAL*8  V    (NEQ)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I
      INTEGER IA_I0, IA_I1
      INTEGER NX
C----------------------------------------------------------

      IA_I0 = IA(1)
      DO I=1,NEQ
         IA_I1 = IA(I+1)
         NX = IA_I1 - IA_I0

         CALL DSCALI(NX, V(I), JA(IA_I0), VKG)

         IA_I0 = IA_I1
      ENDDO

      SP_MORS_MULD = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_MULV
C
C Description:
C     La fonction SP_MORS_MULV multiplie la matrice par un
C     vecteur V et renvoie le résultat R (i.e. R = M.V).
C
C Entrée:
C     NEQ   : DIMENSION DE LA MATRICE
C     IA    : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA    : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG   : MATRICE GLOBALE
C     V     : Vecteur multiplicatif
C
C Sortie:
C     R     : Vecteur résultat
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_MULV(NEQ, IA, JA, VKG, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_MULV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKG  (*)
      REAL*8  V    (NEQ)
      REAL*8  R    (NEQ)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      REAL*8  DDOTI        ! BLAS function

      INTEGER I
      INTEGER IA_I0, IA_I1
      INTEGER NX
C----------------------------------------------------------

      IA_I0 = IA(1)
      DO I=1,NEQ
         IA_I1 = IA(I+1)
         NX = IA_I1 - IA_I0

         R(I) = DDOTI(NX, VKG(IA_I0), JA(IA_I0), V)

         IA_I0 = IA_I1
      ENDDO

      SP_MORS_MULV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_LUMP
C
C Description:
C     La fonction SP_MORS_LUMP somme par ligne dans VL les termes
C     de la matrice VKG stockée par compression de ligne en LU. La
C     matrice n'est par modifiée.
C
C Entrée:
C     NEQ      : Nombre d'équations
C     IA       : Table des pointeurs de debut de chaque ligne
C     JA       : Table des indices de colonnes de chaque terme
C     VKG      : Matrice globale
C
C Sortie:
C     VL       : Matrice lumpée
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_LUMP(NEQ, IA, JA, VKG, VL, IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_LUMP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA (NEQ+1)
      INTEGER JA (*)
      REAL*8  VKG(*)
      REAL*8  VL (NEQ)
      INTEGER IOP

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      REAL*8  V

      REAL*8  ZERO
      PARAMETER (ZERO = 0.000000000000000000000000000D+00)
C----------------------------------------------------------

      IF (IOP .EQ. SP_MORS_LUMP_SUM) THEN
         DO I=1, NEQ
            V = ZERO
            DO IJ=IA(I), IA(I+1)-1
               V = V + VKG(IJ)
            ENDDO
            VL(I) = V
         ENDDO
      ELSEIF (IOP .EQ. SP_MORS_LUMP_SUMABS) THEN
         DO I=1, NEQ
            V = ZERO
            DO IJ=IA(I), IA(I+1)-1
               V = V + ABS(VKG(IJ))
            ENDDO
            VL(I) = V
         ENDDO
      ELSEIF (IOP .EQ. SP_MORS_LUMP_MAX) THEN
         DO I=1, NEQ
            V = -1.0D-32
            DO IJ=IA(I), IA(I+1)-1
               V = MAX(V, VKG(IJ))
            ENDDO
            VL(I) = V
         ENDDO
      ELSEIF (IOP .EQ. SP_MORS_LUMP_MAXABS) THEN
         DO I=1, NEQ
            V = ZERO
            DO IJ=IA(I), IA(I+1)-1
               V = MAX(V, ABS(VKG(IJ)))
            ENDDO
            VL(I) = V
         ENDDO
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_SP_MORS_LUMP_OP_INVALIDE')
      ENDIF

      SP_MORS_LUMP = ERR_TYP()
      END

C**************************************************************
C Sommaire: SP_MORS_DMIN
C
C Description:
C     La fonction SP_MORS_DMIN assure que les termes sur la
C     diagonale aient au moins la valeur min V.
C
C Entrée:
C     NEQ   : Dimension de la matrice
C     JD    : Table des pointeurs au terme diagonal
C     VKG   : Matrice globale
C     V     : Valeur min
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION SP_MORS_DMIN(NEQ, JD, VKG, VMIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_DMIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER JD (NEQ)
      REAL*8  VKG(*)
      REAL*8  VMIN

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      REAL*8  V
      INTEGER I, ID
C----------------------------------------------------------

!$omp parallel do default(shared) private(V, I, ID)
      DO I=1,NEQ
         ID = JD(I)
         IF (ID .GT. 0) THEN
            V = VKG(ID)
            IF (ABS(V) .LT. VMIN) VKG(ID) = SIGN(VMIN, VKG(ID))
         ENDIF
      ENDDO
!$omp end parallel do

      SP_MORS_DMIN = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: SP_MORS_LISMAT
C
C Description:
C     La fonction SP_MORS_LISMAT lis la matrice morse stockée en format standard
C     Harwell-Boeing du fichier donné par FNAME. Les tables sont dimensionnées;
C     il est de la responsabilité de la fonction appelante de désallouer la
C     mémoire.
C
C Entrée:
C     FNAME    Nom du fichier à lire
C
C Sortie:
C     NEQ      Dimension de la matrice
C     NKG      Nombre de termes non nuls
C     LIA      Pointeur à la table INTEGER des pointeurs de début de chaque ligne
C     LJA      Pointeur à la table INTEGER des indices de colonnes de chaque terme
C     LKG      Pointeur à la table REAL*8  des valeurs de la matrice
C
C Notes:
C     Code repris de SUPERLU, hbcode1.f
C     Harwell-Boeing = CSC. Comme nous travaillons en CRS, la matrice
C     n'est par directement utilisable.
C     Il faudrait également lire le second membre.
C****************************************************************
      FUNCTION SP_MORS_LISMAT(FNAME, NEQ, NKG, LIA, LJA, LKG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_LISMAT
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) FNAME
      INTEGER       NEQ
      INTEGER       NKG
      INTEGER       LIA
      INTEGER       LJA
      INTEGER       LKG

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER   IERR
      INTEGER   MR
      LOGICAL   OUVERT

      CHARACTER TITLE*72 , KEY*8    , MXTYPE*3 ,
     &          PTRFMT*16, INDFMT*16, VALFMT*20, RHSFMT*20
      INTEGER   TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,
     &          NROW  , NCOL  , NNZERO, NELTVL

      INTEGER        SP_MORS_LISMAT_AUX
      PARAMETER (MR = 99)
C----------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FNAME) .GT. 0)
C----------------------------------------------------------

C---     OUVRE LE FICHIER
      OUVERT = .FALSE.
      OPEN(UNIT  = MR,
     &     FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &     FORM  = 'FORMATTED',
     &     STATUS= 'OLD',
     &     ERR   = 9900)
      OUVERT = .TRUE.

C---     LIS L'ENTÊTE
      READ (MR, 1000, ERR=9901, END=9902)
     &                TITLE , KEY   ,
     &                TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,
     &                MXTYPE, NROW  , NCOL  , NNZERO, NELTVL,
     &                PTRFMT, INDFMT, VALFMT, RHSFMT
1000  FORMAT (A72, A8 / 5I14 / A3, 11X, 4I14 / 2A16, 2A20)
      NEQ = NCOL
      NKG = NNZERO

C---     CONTROLE QUE LA FICHIER CONTIENNE BIEN LES VALEURS
      IF (VALCRD .LE. 0) GOTO 9903

C---     DIMENSIONNE LES TABLES
      LIA = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NCOL+1, LIA)
      LJA = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNZERO, LJA)
      LKG = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNZERO, LKG)

C---     LIS LA STRUCTURE
      IF (ERR_GOOD()) THEN
         IERR = SP_MORS_LISMAT_AUX(MR,
     &                             PTRFMT,
     &                             INDFMT,
     &                             VALFMT,
     &                             NCOL,
     &                             NNZERO,
     &                             KA(SO_ALLC_REQKIND(KA,LIA)),
     &                             KA(SO_ALLC_REQKIND(KA,LJA)),
     &                             VA(SO_ALLC_REQVIND(VA,LKG)))
         IF (.NOT. ERR_GOOD()) GOTO 9988
      ENDIF

      GOTO 9999
C----------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_OUVERTURE_FICHIER')
      GOTO 9988
9901  CALL ERR_ASG(ERR_ERR, 'ERR_LECTURE_FICHIER')
      GOTO 9988
9902  CALL ERR_ASG(ERR_ERR, 'ERR_FIN_FICHIER')
      GOTO 9988
9903  CALL ERR_ASG(ERR_ERR, 'ERR_FICHIER_SANS_VALEURS')
      GOTO 9988

9988  CONTINUE
      CALL ERR_AJT(FNAME(1:SP_STRN_LEN(FNAME)))
      GOTO 9999

9999  CONTINUE
      IF (OUVERT) CLOSE(MR)
      SP_MORS_LISMAT = ERR_TYP()
      END

C**************************************************************
C Sommaire: SP_MORS_LISMAT_AUX
C
C Description:
C     La fonction SP_MORS_LISMAT_AUX
C
C Entrée:
C     NEQ      : DIMENSION DE LA MATRICE
C     IA       : TABLE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     JA       : TABLE DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG      : MATRICE GLOBALE
C
C Sortie:
C
C Notes:
C     Code repris de SUPERLU, hbcode1.f
C****************************************************************
      FUNCTION SP_MORS_LISMAT_AUX(MR,
     &                            PTRFMT,
     &                            INDFMT,
     &                            VALFMT,
     &                            NEQ,
     &                            NKG,
     &                            IA,
     &                            JA,
     &                            VKG)

      IMPLICIT NONE

      INTEGER        SP_MORS_LISMAT_AUX

      INTEGER        MR
      CHARACTER*(*)  PTRFMT
      CHARACTER*(*)  INDFMT
      CHARACTER*(*)  VALFMT
      INTEGER        NEQ
      INTEGER        NKG
      INTEGER        IA(*)
      INTEGER        JA(*)
      REAL*8         VKG(*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'

      INTEGER   I
C----------------------------------------------------------
D     CALL ERR_PRE(MR .GT. 0)
D     CALL ERR_PRE(NEQ .GT. 0)
D     CALL ERR_PRE(NKG .GT. 0)
C----------------------------------------------------------

C---     LIS LA STRUCTURE
      READ (MR, PTRFMT, ERR=9901, END=9902) (IA(I), I = 1, NEQ+1)
      READ (MR, INDFMT, ERR=9901, END=9902) (JA(I), I = 1, NKG)

C---     LIS LES VALEURS
      READ (MR, VALFMT, ERR=9901, END=9902) (VKG(I), I = 1, NKG)

      GOTO 9999
C----------------------------------------------------------
9901  CALL ERR_ASG(ERR_ERR, 'ERR_LECTURE_FICHIER')
      GOTO 9999
9902  CALL ERR_ASG(ERR_ERR, 'ERR_FIN_FICHIER')
      GOTO 9999

9999  CONTINUE
      SP_MORS_LISMAT_AUX = ERR_TYP()
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction SP_MORS_DMPMAT imprime la matrice morse.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_MORS_DMPMAT(NEQ, IAP, JAP, VKG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_MORS_DMPMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IAP(*)
      INTEGER JAP(*)
      REAL*8  VKG(*)

      INCLUDE 'spmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, J, IJ
      REAL*8  V
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      DO I=1, NEQ
         WRITE(LOG_BUF,*) 'Row: ', I
         CALL LOG_ECRIS(LOG_BUF)
         DO IJ=IAP(I), IAP(I+1)-1
            J = JAP(IJ)
            V = VKG(IJ)
            WRITE(LOG_BUF,*) '(', J, ',', V, ')'
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
      ENDDO

      SP_MORS_DMPMAT = ERR_TYP()
      RETURN
      END
