C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     REAL*8 SP_ADFT_EPS
C   Private:
C
C************************************************************************

C**************************************************************
C Sommaire: ADAPTATIVE FORCING TERM
C
C Description:
C     La fonction SP_ADFT_EPS calcule le facteur <code>eta</code> qui
C     contrôle la norme de convergence de résolution non linéaire de
C     type Inexact-Newton-Raphson.
C
C Entrée:
C     VEPS     EPS = VEPS(1) + (VEPS(2) + VEPS(3)*ETA) * ||U||
C              VEPS(1) Facteur absolu: terme constant
C              VEPS(2) Facteur relatif: terme constant
C              VEPS(3) Facteur relatif: terme linéaire
C              VEPS(4) Espace de travail: ETA_K
C              VEPS(5) Espace de travail: FU_K
C              VEPS(6) Espace de travail: R_K (<0 en première entrée)
C     NDLT     Nombre de degrés de liberté
C     NEQ      Nombre d'équations, dim. du problème
C     VDLG     Solution initiale
C     VRES     Table de travail
C     HPRE     Handle sur le préconditionnement
C     F_PRE    Call-back de préconditionnement
C     HRES     Handle sur l'algorithme d'assemblage
C     F_RES    Call-back d'assemblage de résidu
C
C Sortie:
C
C Notes:
C****************************************************************
      FUNCTION SP_ADFT_EPS(VEPS,
     &                     NDLT,
     &                     NEQ,
     &                     VDLG,
     &                     VRES,
     &                     HPRE,
     &                     F_PRE,
     &                     HRES,
     &                     F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ADFT_EPS
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8   VEPS (6)
      INTEGER  NDLT
      INTEGER  NEQ
      REAL*8   VDLG (NDLT)
      REAL*8   VRES (NEQ)
      INTEGER  HPRE
      INTEGER  F_PRE
      EXTERNAL F_PRE
      INTEGER  HRES
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'spadft.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR

      REAL*8  DNRM2              ! Fonction BLAS
      REAL*8  ETA_K, ETA_KM1
      REAL*8  FU_K,  FU_KM1
      REAL*8  R_KM1
      REAL*8  EPS, TMP

      REAL*8  ETA_0
      REAL*8  ETA_MAX, ETA_MIN
      REAL*8  SECOR
      REAL*8  ZERO
      PARAMETER (ETA_0   = 0.01D+00)
      PARAMETER (ETA_MAX = 0.9D+00)
      PARAMETER (ETA_MIN = 1.0D-06)
      PARAMETER (SECOR   = 1.6180339887498948482D+00)  ! (1+SQRT(5))/2
      PARAMETER (ZERO    = 0.0D-00)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(HRES .NE. 0)
C-----------------------------------------------------------------------

C---     Calcul la norme du résidu
      CALL DINIT(NEQ, ZERO, VRES, 1)   ! VRES = 0
      IERR = F_RES(HRES, VRES, VDLG, .TRUE.)
      IF (HPRE. NE. 0) IERR = F_PRE(HPRE, VRES)
      FU_K = DNRM2(NEQ, VRES, 1)

C---     Calcul de eta (adaptive forcing term)
      ETA_KM1 = VEPS(4)
      FU_KM1  = VEPS(5)
      R_KM1   = VEPS(6)
      IF (R_KM1 .GT. 0.0D0) THEN
         ETA_K = ABS(FU_K - R_KM1) / FU_KM1
         TMP   = ETA_KM1**SECOR       ! ETA(K-1)**SECTION_OR
         IF (TMP .GT. 0.1D0) THEN
            ETA_K = MAX(ETA_K, TMP)
         ENDIF
         ETA_K = MIN(ETA_K, ETA_MAX)
         ETA_K = MAX(ETA_K, ETA_MIN)
      ELSE
         ETA_K = ETA_0
      ENDIF

C---     Log les infos
      EPS = VEPS(1) + (VEPS(2) + VEPS(3)*ETA_K)*FU_K
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_ADAPTATIVE_FORCING_TERM:'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         WRITE(LOG_BUF, '(2A)') 'EPS = EPS0 + (EPS11 + EPS12*ETA)*||U||'
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'EPS0#<6>#', '= ', VEPS(1)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'EPS11#<6>#', '= ', VEPS(2)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'EPS12#<6>#', '= ', VEPS(3)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'ETA#<6>#', '= ', ETA_K
         CALL LOG_DECIND()
      ELSEIF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'ETA#<6>#', '= ', ETA_K
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Conserve les valeurs
      VEPS(4) = ETA_K
      VEPS(5) = FU_K

      SP_ADFT_EPS = EPS
      RETURN
      END
