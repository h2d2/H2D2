C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C************************************************************************

C************************************************************************
C Sommaire : SP_LSTC_INIT
C
C Description:
C     INITIALISATION DE LA LISTE CHAINEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_LSTC_INIT(NPNT, KPNT, NINFO, NLIEN, KLIEN, INEXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LSTC_INIT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1,*)
      INTEGER INEXT

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER I
C-----------------------------------------------------------------------

      INEXT = 1                      ! POINTER TO NEXT FREE
      DO I=1,NLIEN-1
         KLIEN(1,I) = I + 1          ! POINTER TO NEXT IN THE CHAIN
      ENDDO
      KLIEN(1,NLIEN) = 0
      DO I=1,NPNT
         KPNT(I) = 0
      ENDDO

      SP_LSTC_INIT = ERR_TYP()
      END

C************************************************************************
C Sommaire: SP_LSTC_AJTCHN
C
C Description:
C     La fonctions SP_LSTC_AJTCHN ajoute une chainon à la liste
C     chaînée.
C
C     MANAGE THE LINKED CHAINED TO FIND SKIN ELEMENTS. THE ENTRIES OF
C     KPNT ARE POINTERS TO THE CHAIN OF ELEMENTS WHOSE NODE NUMBERS
C     BEGINS WITH NO1
C      NO1,NO2,NO3      NODE NUMBERS OF THE SIDE IN INCREASING ORDER
C      IELEM            MASTER ELEMENT NUMBER
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_LSTC_AJTCHN(NPNT,
     &                        KPNT,
     &                        NINFO,
     &                        NLIEN,
     &                        KLIEN,
     &                        INEXT,
     &                        ICLEF,
     &                        INFO,
     &                        NCMP,
     &                        NEWLNK)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LSTC_AJTCHN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1, *)
      INTEGER INEXT
      INTEGER ICLEF
      INTEGER INFO (*)
      INTEGER NCMP
      LOGICAL NEWLNK

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER I
      INTEGER IACTU
      INTEGER IPRED
      INTEGER ISUCC
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ICLEF .LE. NPNT)
D     CALL ERR_PRE(NCMP  .LE. NINFO)
C-----------------------------------------------------------------------

      NEWLNK = .FALSE.
      IACTU = KPNT(ICLEF)                  ! POINTER TO THE SUB CHAIN

C-------  THE NODE IS NOT YET IN THE CHAIN - ADD IT
      IF (IACTU .EQ. 0) THEN
         IF (INEXT .EQ. 0) GOTO 9900
         IACTU = INEXT                     ! GET NEXT FREE
         INEXT = KLIEN(1,IACTU)            ! UPDATE NEXT FREE
         KPNT(ICLEF) = IACTU               ! UPDATE POINTER OF NODE

         DO I=1,NINFO                      ! INFO
            KLIEN(I+1,IACTU) = INFO(I)
         ENDDO
         KLIEN(1,IACTU) = 0                ! END ELEMENT
         NEWLNK = .TRUE.
      ELSE

C-------  THE NODE IS IN THE CHAIN - DOES THE INFO ALLREADY EXIST ?
         IPRED = 0
100      CONTINUE
            IF (IACTU .EQ. 0) GOTO 200     ! LAST ELEMENT FOR THIS NODE
            DO I=1,NCMP
               IF (KLIEN(I+1,IACTU) .NE. INFO(I)) GOTO 110
            ENDDO
            GOTO 300
110         CONTINUE
            IPRED = IACTU
            IACTU = KLIEN(1,IACTU)
         GOTO 100

C---        SIDE DOES NOT EXIST : ADD IT
200      CONTINUE
         IACTU = IPRED
         ISUCC = INEXT                     ! GET NEXT FREE
         IF (ISUCC .EQ. 0) GOTO 9900
         INEXT = KLIEN(1,ISUCC)            ! UPDATE NEXT FREE
         KLIEN(1,IACTU) = ISUCC            ! LINK ACTUAL TO NEXT

         KLIEN(1,ISUCC) = 0                ! END ELEMENT
         DO I=1,NINFO                      ! INFO
            KLIEN(I+1,ISUCC) = INFO(I)
         ENDDO
         NEWLNK = .TRUE.
         GOTO 400

C---        SIDE EXIST : ERASE
300      CONTINUE
         ISUCC = KLIEN(1,IACTU)
         IF (IPRED .EQ. 0) THEN            ! FIRST ELEMENT OF SUB-CHAIN
            IF (ISUCC .EQ. 0) THEN
               KPNT(ICLEF) = 0
            ELSE
               KPNT(ICLEF) = ISUCC
            ENDIF
         ELSE
            KLIEN(1,IPRED) = ISUCC
         ENDIF
         KLIEN(1,IACTU) = INEXT
         INEXT = IACTU

400      CONTINUE
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_LISTE_PLEINE')
      GOTO 9999

9999  CONTINUE
      SP_LSTC_AJTCHN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_LSTC_REQCHN
C
C Description:
C     La fonctions SP_LSTC_REQCHN retourne le prochain chaînon de la
C     liste. Elle permet de parcourir de proche en proche toute la
C     liste.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'assignation d'erreur est "chère" pour qq chose de trivial
C     et courant comme une fin de chainon. La fin est également
C     marquée par (IACTU .LE. 0)
C************************************************************************
      FUNCTION SP_LSTC_REQCHN(NPNT,
     &                        KPNT,
     &                        NINFO,
     &                        NLIEN,
     &                        KLIEN,
     &                        ICLEF,
     &                        IACTU,
     &                        INFO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LSTC_REQCHN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1, *)
      INTEGER ICLEF
      INTEGER IACTU
      INTEGER INFO (*)

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER I
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ICLEF .GT. 0 .AND. ICLEF .LE. NPNT)
C-----------------------------------------------------------------------

      IF (IACTU .LE. 0) THEN
         IACTU = KPNT(ICLEF)
      ELSE
         IACTU = KLIEN(1,IACTU)
      ENDIF
      IF (IACTU .GT. 0) THEN
         INFO(1:NINFO) = KLIEN(1+1:NINFO+1,IACTU)
      ENDIF

      SP_LSTC_REQCHN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimension de la liste
C
C Description:
C     La fonction <code>SP_LSTC_REQDIM</code> retourne le nombre de
C     chaînons, donc la dimension de la liste.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SP_LSTC_REQDIM(NPNT,
     &                        KPNT,
     &                        NINFO,
     &                        NLIEN,
     &                        KLIEN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LSTC_REQDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1, *)

      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IP, IPNT
      INTEGER NITEM
C-----------------------------------------------------------------------

C---     GET NUMBER OF ELEMENTS OF SKIN
      NITEM = 0
      DO IP=1,NPNT
         IPNT = KPNT(IP)
         IF (IPNT .NE. 0) THEN
100         CONTINUE
               NITEM = NITEM + 1
               IPNT  = KLIEN(1,IPNT)
            IF (IPNT .NE. 0) GOTO 100
         ENDIF
      ENDDO

      SP_LSTC_REQDIM = NITEM
      RETURN
      END
