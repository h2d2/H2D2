C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C  PUBLIQUES:
C     SUBROUTINE SP_LDUD_FACT
C     SUBROUTINE SP_LDUD_SOLV
C  PRIVEES:
C     SUBROUTINE SP_LDUD_DIMSL     ! DIMENSION DES TABLES SKY-LINE
C     SUBROUTINE SP_LDUD_INISKL    ! INIT LES TABLES SKY-LINE
C     SUBROUTINE SP_LDUD_EXFACT    ! GERE LA FACTORISATION
C     SUBROUTINE SP_LDUD_FCTMAT    ! FACTORISE LA MATRICE
C     SUBROUTINE SP_LDUD_CR2SKL    ! TRANSFERT DES LIGNES DE CR A SL
C     SUBROUTINE SP_LDUD_ECRSKL    ! ECRIS DES LIGNES SL SUR FICHIER
C     SUBROUTINE SP_LDUD_EFFSKL    ! EFFACE DES LIGNES SL
C     SUBROUTINE SP_LDUD_EXSOLV    ! RESOUD
C     SUBROUTINE SP_LDUD_LISSLI    ! LIS LES DONNEES INF
C     SUBROUTINE SP_LDUD_LISSLDS   ! LIS LES DONNEES DIAG+SUP
C     SUBROUTINE SP_LDUD_DMPMAT    ! DUMP LA MATRICE (DEBUG)
C
C Sommaire:
C     Résolution LDU sur Disque
C
C************************************************************************

C************************************************************************
C Sommaire: SP_LDUD_FACT
C
C Description:
C     LA SUBROUTINE PUBLIQUE SP_LDUD_FACT FACTORISE PAR UNE METHODE
C     FRONTALE UNE MATRICE GLOBALE STOCKEE PAR COMPRESSION DE LIGNE.
C     EN ENTREE, LES TABLES nn_CR CONTIENNENT LA MATRICE COMPRESSEE.
C     EN SORTIE, L'INFO A ETE ECRASEE POUR CONTENIR LES DONNEES
C     FRONTALES.
C
C Entrée:
C     NDLT     : Nombre de Degré de Liberté Total
C     NKGP     : Nombre de termes de VKG
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG_CR   : MATRICE GLOBALE CRS
C
C Sortie:
C     VKG_CR   : ESPACE DE TRAVAIL
C     IA_SL    : TABLE SKY-LINE
C
C Notes:
C
C************************************************************************
      FUNCTION SP_LDUD_FACT(NDLT, NKGP, IA_CR, JA_CR, VKG_CR, IA_SL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LDUD_FACT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLT
      INTEGER NKGP
      INTEGER IA_CR (*)
      INTEGER JA_CR (*)
      REAL*8  VKG_CR(*)
      INTEGER IA_SL (*)

      INCLUDE 'spldud.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER NVKGD
      INTEGER NVKGS
      INTEGER LJA_SL
      INTEGER LGS_SL
      INTEGER LGD_SL
      INTEGER LGI_SL
      INTEGER LVT_SL
      INTEGER LKT_SL
C----------------------------------------------------------

C---     CONTROLES
      IF (NKGP .LT. NDLT) GOTO 9001

C---     CALCULE LES DIMENSIONS DE LA SOUS-MATRICE SKY-LINE
      CALL SP_LDUD_DIMSL(NVKGD, NVKGS, NDLT, IA_CR, JA_CR)

C---     DIMENSIONNE LES TABLES SKY-LINE
      LJA_SL = 0
      IERR = SO_ALLC_ALLINT(NDLT,LJA_SL)
      LGS_SL = 0
      IERR = SO_ALLC_ALLRE8(NVKGS,LGS_SL)
      LGD_SL = 0
      IERR = SO_ALLC_ALLRE8(NVKGD,LGD_SL)
      LGI_SL = 0
      IERR = SO_ALLC_ALLRE8(NVKGS,LGI_SL)
      LVT_SL = 0
      IERR = SO_ALLC_ALLRE8(2*NVKGD,LVT_SL)
      LKT_SL = 0
      IERR = SO_ALLC_ALLINT(  NVKGD,LKT_SL)

C---     INITIALISE LES TABLES D'INDICE SKY-LINE
      CALL SP_LDUD_INISKL(NDLT,
     &               IA_SL,
     &               KA(SO_ALLC_REQKIND(KA,LJA_SL)),
     &               IA_CR,
     &               JA_CR)

C---     EXECUTE LA FACTORISATION
      CALL SP_LDUD_EXFACT(NDLT,
     &               NVKGD,
     &               NVKGS,
     &               IA_SL,
     &               KA(SO_ALLC_REQKIND(KA,LJA_SL)),
     &               VA(SO_ALLC_REQVIND(VA,LGS_SL)),
     &               VA(SO_ALLC_REQVIND(VA,LGD_SL)),
     &               VA(SO_ALLC_REQVIND(VA,LGI_SL)),
     &               VA(SO_ALLC_REQVIND(VA,LVT_SL)),
     &               KA(SO_ALLC_REQKIND(KA,LKT_SL)),
     &               IA_CR,
     &               JA_CR,
     &               VKG_CR)

C---     RECUPERE LA MEMOIRE
      IERR = SO_ALLC_ALLINT(0, LKT_SL)
      IERR = SO_ALLC_ALLRE8(0, LVT_SL)
      IERR = SO_ALLC_ALLRE8(0, LGI_SL)
      IERR = SO_ALLC_ALLRE8(0, LGD_SL)
      IERR = SO_ALLC_ALLRE8(0, LGS_SL)
      IERR = SO_ALLC_ALLINT(0, LJA_SL)

      GOTO 9999
C--------------------------------------------------------------
9001  WRITE(ERR_BUF,'(A)') 'SP_LDUD_FACT --- NKGP < NDLT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_LDUD_FACT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_SOLV
C
C Description:
C     LA SUBROUTINE PUBLIQUE SP_LDUD_SOLV FACTORISE PAR UNE METHODE
C     FRONTALE UNE MATRICE GLOBALE STOCKEE PAR COMPRESSION DE LIGNE.
C
C Entrée:
C     NDLT     : Nombre de Degré de Liberté Total
C     NKGP     : Nombre de termes de VKG
C     IA_SL    : TABLE SKY-LINE DES POINTEURS DE DEBUT DE CHAQUE LIGNE
C     VKG_SL   : ESPACE DE TRAVAIL
C     VRES     : MEMBRE DE DROITE A RESOUDRE
C
C Sortie:
C     VRES     : MEMBRE DE DROITE - SOLUTION
C
C Notes:
C
C************************************************************************
      FUNCTION SP_LDUD_SOLV(NDLT,
     &                      NKGP,
     &                      IA_SL,
     &                      VKG_SL,
     &                      VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_LDUD_SOLV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLT
      INTEGER NKGP
      INTEGER IA_SL (*)
      REAL*8  VKG_SL(*)
      REAL*8  VRES  (*)

      INCLUDE 'spldud.fi'
      INCLUDE 'err.fi'
C----------------------------------------------------------

      CALL SP_LDUD_EXSOLV(NDLT,
     &                    IA_SL,
     &                    NKGP,
     &                    VKG_SL,
     &                    VRES)

      SP_LDUD_SOLV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_DIMSL
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_DIMSL DETERMINE LES DIMENSIONS
C     DE LA SOUS-MATRICE SKY-LINE.
C
C Entrée:
C     NLIN     : DIMENSION DU PROBLEME
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C
C Sortie:
C     NVKGD    : NOMBRE DE TERMES DIAG
C     NVKGS    : NOMBRE DE TERMES DANS CHAQUE TRIANGLE
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_DIMSL(NVKGD, NVKGS, NLIN, IA_CR, JA_CR)

      IMPLICIT NONE

      INTEGER NVKGD
      INTEGER NVKGS
      INTEGER NLIN
      INTEGER IA_CR(*)
      INTEGER JA_CR(*)

      INTEGER IL, I0
      INTEGER JMIN
      INTEGER LBMAX
      INTEGER NSUMLB
C----------------------------------------------------------

C---     DETERMINE LA LONGUEUR DIAG MAX
      LBMAX   = 0
!$omp parallel do default(shared) private(IL,JMIN) reduction(MAX:LBMAX)
      DO IL=1,NLIN
         JMIN  = JA_CR(IA_CR(IL))
         LBMAX = MAX(LBMAX,IL-JMIN)
      ENDDO
!$omp end parallel do
      NVKGD = LBMAX + 1

C---     DETERMINE LE NBR DE TERMES NON NUL SUR LA LONGUEUR DIAG MAX
      NSUMLB = 0
      DO IL=1,NVKGD
         JMIN = JA_CR(IA_CR(IL))
         NSUMLB = NSUMLB + (IL-JMIN)
      ENDDO
      NVKGS = NSUMLB
      DO IL=NVKGD+1,NLIN
         I0 = IL - NVKGD
         JMIN = JA_CR(IA_CR(I0))
         NSUMLB = NSUMLB - (I0-JMIN)
         JMIN = JA_CR(IA_CR(IL))
         NSUMLB = NSUMLB + (IL-JMIN)
         NVKGS = MAX(NVKGS,NSUMLB)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_INISKL
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_INISKL INITIALISE LES TABLES
C     D'INDICES SKY-LINE
C
C Entrée:
C     NLIN     : DIMENSION DU PROBLEME
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C
C Sortie:
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_SL    : TABLE SKY-LINE DES JMAX POUR CHAQUE LIGNE
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_INISKL(NLIN, IA_SL, JA_SL, IA_CR, JA_CR)

      IMPLICIT NONE

      INTEGER NLIN
      INTEGER IA_SL(*)
      INTEGER JA_SL(*)
      INTEGER IA_CR(*)
      INTEGER JA_CR(*)

      INTEGER IL
      INTEGER J, JMIN, JMAX
C----------------------------------------------------------

C---     CONSTRUCTION DE IA_SL
!$omp parallel do default(shared) private(IL,JMIN)
      DO IL=1,NLIN
         JMIN = JA_CR(IA_CR(IL))
         IA_SL(IL+1) = IL-JMIN
      ENDDO
!$omp end parallel do
      IA_SL(1) = 1
      DO IL=2,NLIN+1
         IA_SL(IL) = IA_SL(IL)+IA_SL(IL-1)
      ENDDO

C---     CONSTRUCTION DE JA_SL
!$omp parallel do default(shared) private(IL,J,JMIN,JMAX)
      DO IL=1,NLIN
         JMIN = IL - (IA_SL(IL+1)-IA_SL(IL))
         JMAX = IL         ! ON PREND LA DIAGONALE
         DO J=JMIN,JMAX
            JA_SL(J) = IL
         ENDDO
      ENDDO
!$omp end parallel do

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_EXFACT
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_EXFACT GERE LA FACTORISATION PAR
C     METHODE FRONTALE D'UNE MATRICE GLOBALE STOCKEE PAR COMPRESSION
C     DE LIGNE.
C
C Entrée:
C     NLIN     : DIMENSION DU PROBLEME A RESOUDRE
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_SL    : TABLE SKY-LINE DES JMAX POUR CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG_CR   : MATRICE GLOBALE CRS
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_EXFACT(NLIN,
     &                          NVKGD,
     &                          NVKGS,
     &                          IA_SL,
     &                          JA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL,
     &                          VTRV_SL,
     &                          KTRV_SL,
     &                          IA_CR,
     &                          JA_CR,
     &                          VKG_CR)

      IMPLICIT NONE

      INTEGER NLIN
      INTEGER NVKGD
      INTEGER NVKGS
      INTEGER IA_SL  (*)
      INTEGER JA_SL  (*)
      REAL*8  VKGS_SL(*)
      REAL*8  VKGD_SL(*)
      REAL*8  VKGI_SL(*)
      REAL*8  VTRV_SL(*)
      INTEGER KTRV_SL(*)
      INTEGER IA_CR  (*)
      INTEGER JA_CR  (*)
      REAL*8  VKG_CR (*)

      INCLUDE 'c_fb.fi'

      INTEGER   IERR
      INTEGER*8 XMS, XMD, XMI
C----------------------------------------------------------

C---     OUVRE LES FICHIERS DE LA MATRICE
      XMS = C_FB_OUVRE('vkgs.tmp', C_FB_ECRITURE)
      XMD = C_FB_OUVRE('vkgd.tmp', C_FB_ECRITURE)
      XMI = C_FB_OUVRE('vkgi.tmp', C_FB_ECRITURE)

C---     FACTORISE LA MATRICE
      CALL SP_LDUD_FCTMAT(XMS,
     &                    XMD,
     &                    XMI,
     &                    NLIN,
     &                    NVKGD,
     &                    NVKGS,
     &                    IA_SL,
     &                    JA_SL,
     &                    VKGS_SL,
     &                    VKGD_SL,
     &                    VKGI_SL,
     &                    VTRV_SL,
     &                    KTRV_SL,
     &                    IA_CR,
     &                    JA_CR,
     &                    VKG_CR)

C---     FERME LES FICHIERS DE LA MATRICE
      IERR = C_FB_FERME(XMS)
      IERR = C_FB_FERME(XMD)
      IERR = C_FB_FERME(XMI)

C---     INVERSE LE TRIANGLE SUP
      IERR = C_FB_INVERSE('vkgs.tmp',
     &                    'vkgs_i.tmp',
     &                    IA_SL(NLIN+1)-1,
     &                    IA_CR(NLIN+1)-1,
     &                    VKG_CR)

C---     INVERSE LA DIAGONALE
      IERR = C_FB_INVERSE('vkgd.tmp',
     &                    'vkgd_i.tmp',
     &                    NLIN,
     &                    IA_CR(NLIN+1)-1,
     &                    VKG_CR)

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_FCTMAT
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_FCTMAT EXECUTE LA FACTORISATION PAR
C     METHODE FRONTALE D'UNE MATRICE GLOBALE STOCKEE PAR COMPRESSION
C     DE LIGNE.
C
C Entrée:
C     XMS      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE SUPERIEURE
C     XMD      : HANDLE EXTERNE SUR LE FICHIER DE LA DIAGONALE
C     XMI      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE INFERIEUR
C     NLIN     : DIMENSION DU PROBLEME A RESOUDRE
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_SL    : TABLE SKY-LINE DES JMAX POUR CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG_CR   : MATRICE GLOBALE CRS
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_FCTMAT(XMS,
     &                          XMD,
     &                          XMI,
     &                          NLIN,
     &                          NVKGD,
     &                          NVKGS,
     &                          IA_SL,
     &                          JA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL,
     &                          VTRV_SL,
     &                          KTRV_SL,
     &                          IA_CR,
     &                          JA_CR,
     &                          VKG_CR)

      IMPLICIT NONE

      INTEGER*8 XMS
      INTEGER*8 XMD
      INTEGER*8 XMI
      INTEGER   NLIN
      INTEGER   NVKGD
      INTEGER   NVKGS
      INTEGER   IA_SL  (*)
      INTEGER   JA_SL  (*)
      REAL*8    VKGS_SL(*)
      REAL*8    VKGD_SL(*)
      REAL*8    VKGI_SL(*)
      REAL*8    VTRV_SL(NVKGD,*)
      INTEGER   KTRV_SL(*)
      INTEGER   IA_CR  (*)
      INTEGER   JA_CR  (*)
      REAL*8    VKG_CR (*)

      INCLUDE 'spskyl.fi'

      INTEGER IERR
      INTEGER IMIN_SL
      INTEGER IMAX_SL
      INTEGER IOFF_SL
      INTEGER KOFF_SL
      INTEGER IPIV
      INTEGER IPIVMAX
      INTEGER NK
      INTEGER NI
C----------------------------------------------------------

C---     INITIALISE LES VARIABLES
      IMIN_SL = 1       ! LIGNE MIN TRANSFEREE
      IMAX_SL = 0       ! LIGNE MAX TRANSFEREE
      IOFF_SL = 0       ! OFFSET DU DEBUT DE VKGD_SL
      KOFF_SL = 0       ! OFFSET DU DEBUT DE VKGn_SL

C---     BOUCLE SUR LES LIGNES
      DO IPIV=1,NLIN

         IPIVMAX = JA_SL(IPIV)

C---        SI BESOIN, COMPACTE LA SOUS-MATRICE SKY-LINE
         NK = IA_SL(IPIVMAX+1) - IA_SL(IMIN_SL)
         NI = IPIVMAX - IMIN_SL + 1
         IF (NI .GT. NVKGD .OR. NK .GT. NVKGS) THEN
            CALL SP_LDUD_ECRSKL(XMS,      ! ECRIS LES LIGNES FACTORISEES
     &                          XMD,
     &                          XMI,
     &                          IMIN_SL,
     &                          IPIV-1,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL)
            CALL SP_LDUD_EFFSKL(IMIN_SL,       !  ELIMINE LES LIGNES FACTORISEES
     &                          IPIV-1,
     &                          IMAX_SL,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL)
            IMIN_SL = IPIV
         ENDIF

C---        CHARGE LES DONNEES NECESSAIRES
         IF (IPIVMAX .GT. IMAX_SL) THEN
            CALL SP_LDUD_CR2SKL(IPIV,
     &                          IPIVMAX,
     &                          IMAX_SL,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL,
     &                          IA_CR,
     &                          JA_CR,
     &                          VKG_CR)
            IMAX_SL = MAX(IMAX_SL,IPIVMAX)
         ENDIF

C---        FACTORISE LA LIGNE DU PIVOT
         IERR = SP_SKYL_FCTLIN(IPIV,
     &                         IOFF_SL,
     &                         KOFF_SL,
     &                         IA_SL,
     &                         JA_SL,
     &                         VKGS_SL,
     &                         VKGD_SL,
     &                         VKGI_SL,
     &                         VTRV_SL(1,1),
     &                         VTRV_SL(1,2),
     &                         KTRV_SL)

      ENDDO

C---     ECRIS LES LIGNES FACTORISEES
      CALL SP_LDUD_ECRSKL(XMS,
     &                    XMD,
     &                    XMI,
     &                    IMIN_SL,
     &                    IMAX_SL,
     &                    IOFF_SL,
     &                    KOFF_SL,
     &                    IA_SL,
     &                    VKGS_SL,
     &                    VKGD_SL,
     &                    VKGI_SL)

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_CR2SKL
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_CR2SLK TRANSFERT LES LIGNES DE IP1 A IP2
C     STOCKEES PAR COMPRESSION DE LIGNES A DES LIGNES STOCKEE EN FORMAT
C     SKY-LINE.
C
C Entrée:
C     IP1      : INDICE DE LA PREMIERE LIGNE A TRANSFERER
C     IP2      : INDICE DU LA DERNIERE LA LIGNE A TRANSFERER
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     IOFF_SL  : OFFSET SKY-LINE DANS VKGD
C     KOFF_SL  : OFFSET SKY-LINE DANS VKGS-I
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C     IA_CR    : TABLE CRS DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_CR    : TABLE CRS DES INDICES DE COLONNES DE CHAQUE TERME
C     VKG_CR   : MATRICE GLOBALE CRS
C
C Sortie:
C     VKGS_SL  : SOUS MATRICE SKY-LINE MIS A JOUR
C     VKGD_SL  :
C     VKGI_SL  :
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_CR2SKL(IP1,
     &                          IP2,
     &                          IMAX_SL,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL,
     &                          IA_CR,
     &                          JA_CR,
     &                          VKG_CR)

      IMPLICIT NONE

      INTEGER IP1
      INTEGER IP2
      INTEGER IMAX_SL
      INTEGER IOFF_SL
      INTEGER KOFF_SL
      INTEGER IA_SL  (*)
      REAL*8  VKGS_SL(*)
      REAL*8  VKGD_SL(*)
      REAL*8  VKGI_SL(*)
      INTEGER IA_CR  (*)
      INTEGER JA_CR  (*)
      REAL*8  VKG_CR (*)

      INTEGER I, II, J, K
      INTEGER IAMIN,IAMAX
      INTEGER JMIN, JMAX
      REAL*8  V
C----------------------------------------------------------

C---     BOUCLE SUR LES LIGNES
      DO I=IP1,IP2

         IAMIN = IA_CR(I)
         IAMAX = IA_CR(I+1)-1

C---        BORNES EN J
         IF (I .LE. IMAX_SL) THEN
            JMIN = IMAX_SL + 1
         ELSE
            JMIN = JA_CR(IAMIN)
         ENDIF
         JMAX = MIN(JA_CR(IAMAX),IP2)

C---        TRANSFERT LES DONNEES
         IF (JMIN .GT. JMAX) GOTO 210
         DO 200 II=IAMIN,IAMAX
            J = JA_CR(II)
            IF (J .LT. JMIN) GOTO 200     ! DEJA DEDANS
            IF (J .GT. JMAX) GOTO 210     ! HORS CADRE
            V = VKG_CR(II)

            IF (J .LT. I) THEN
               K = IA_SL(I+1) - I + J - KOFF_SL
               VKGI_SL(K) = V
            ELSEIF (J .EQ. I) THEN
               VKGD_SL(I-IOFF_SL) = V
            ELSEIF (J .GT. I) THEN
               K = IA_SL(J+1) - J + I - KOFF_SL
               VKGS_SL(K) = V
            ENDIF
200      CONTINUE
210      CONTINUE

      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_ECRSKL
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_ECRSKL ECRIS LES LIGNES DE CIEL
C     (ET LES COLONNE) ASSOCIEES AUX PIVOTS IP1 A IP2. ON MAINTIENT
C     3 FICHIERS, POUR VKGS, VKGI ET VKGD
C
C Entrée:
C     XMS      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE SUPERIEURE
C     XMD      : HANDLE EXTERNE SUR LE FICHIER DE LA DIAGONALE
C     XMI      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE INFERIEUR
C     IP1      : INDICE DE LA PREMIERE LIGNE A ECRIRE
C     IP2      : INDICE DU LA DERNIERE LA LIGNE A ECRIRE
C     IOFF_SL  : OFFSET SKY-LINE DANS VKGD
C     KOFF_SL  : OFFSET SKY-LINE DANS VKGS-I
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_ECRSKL(XMS,
     &                          XMD,
     &                          XMI,
     &                          IP1,
     &                          IP2,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL)

      IMPLICIT NONE

      INTEGER*8 XMS
      INTEGER*8 XMD
      INTEGER*8 XMI
      INTEGER   IP1
      INTEGER   IP2
      INTEGER   IOFF_SL
      INTEGER   KOFF_SL
      INTEGER   IA_SL  (*)
      REAL*8    VKGS_SL(*)
      REAL*8    VKGD_SL(*)
      REAL*8    VKGI_SL(*)

      INCLUDE 'c_fb.fi'

      INTEGER IERR
      INTEGER I1
      INTEGER NI
      INTEGER K1, NK
C----------------------------------------------------------

      K1 = IA_SL(IP1)   - KOFF_SL
      NK = IA_SL(IP2+1) - IA_SL(IP1)
      IERR = C_FB_ECRIS(XMS, NK, VKGS_SL(K1))
      IERR = C_FB_ECRIS(XMI, NK, VKGI_SL(K1))

      I1 = IP1 - IOFF_SL
      NI = IP2 - IP1 + 1
      IERR = C_FB_ECRIS(XMD, NI, VKGD_SL(I1))

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_EFFSKL
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_EFFSKL EFFACE LES LIGNES DE CIEL
C     (ET LES COLONNES) ASSOCIEES AUX PIVOTS IP1 A IP2. ELLE MET
C     A JOUR LES OFFSETS DANS LES TABLES.
C
C Entrée:
C     IP1      : INDICE DE LA PREMIERE LIGNE A EFFACER
C     IP2      : INDICE DE LA DERNIERE LIGNE A EFFACER
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     IOFF_SL  : OFFSET SKY-LINE DANS VKGD
C     KOFF_SL  : OFFSET SKY-LINE DANS VKGS-I
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C
C Sortie:
C     IOFF_SL  : OFFSET SKY-LINE DANS VKGD
C     KOFF_SL  : OFFSET SKY-LINE DANS VKGS-I
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_EFFSKL(IP1,
     &                          IP2,
     &                          IMAX_SL,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL)

      IMPLICIT NONE

      INTEGER IP1
      INTEGER IP2
      INTEGER IMAX_SL
      INTEGER IOFF_SL
      INTEGER KOFF_SL
      INTEGER IA_SL  (*)
      REAL*8  VKGS_SL(*)
      REAL*8  VKGD_SL(*)
      REAL*8  VKGI_SL(*)

      INTEGER NI, NK
      INTEGER K1, K2
      INTEGER KMIN, KMAX

      REAL*8 ZERO
      PARAMETER (ZERO = 0.0000000000000000000D+00)
C----------------------------------------------------------

      KMIN = IA_SL(IP1)         - KOFF_SL
      KMAX = IA_SL(IMAX_SL+1)-1 - KOFF_SL
      NK   = IA_SL(IP2+1)-IA_SL(IP1)

C---     DEPLACER VKGS
      IF (NK .GT. 0) THEN
         K1 = KMIN
         DO K2=KMIN+NK,KMAX
            VKGS_SL(K1) = VKGS_SL(K2)
            K1 = K1 + 1
         ENDDO
         DO K2=K1,KMAX
            VKGS_SL(K2) = ZERO
         ENDDO
      ENDIF

C---     DEPLACER VKGI
      IF (NK .GT. 0) THEN
         K1 = KMIN
         DO K2=KMIN+NK,KMAX
            VKGI_SL(K1) = VKGI_SL(K2)
            K1 = K1 + 1
         ENDDO
         DO K2=K1,KMAX
            VKGI_SL(K2) = ZERO
         ENDDO
      ENDIF

C---     DEPLACER VKGD
      KMIN = IP1     - IOFF_SL
      KMAX = IMAX_SL - IOFF_SL
      NI = IP2 - IP1 + 1
      IF (NI .GT. 0) THEN
         K1 = KMIN
         DO K2=KMIN+NI,KMAX
            VKGD_SL(K1) = VKGD_SL(K2)
            K1 = K1 + 1
         ENDDO
         DO K2=K1,KMAX
            VKGD_SL(K2) = ZERO
         ENDDO
      ENDIF

C---     MET A JOUR LES OFFSETS
      KOFF_SL = KOFF_SL + NK
      IOFF_SL = IOFF_SL + NI

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_EXSOLV
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_EXSOLV RESOUD LE SYSTEME ALGEBRIQUE
C     [K]{U}={F} POUR LE SECOND MEMBRE VRES AVEC LA MATRICE FACTORISEE
C     STOCKEE EN LIGNE DE CIEL SUR FICHIERS.
C
C Entrée:
C     NLIN     : DIMENSION DU PROBLEME
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     NKG_SL   : DIMENSION DE LA TABLE VKG_SL
C     VKG_SL   : TABLE DE TRAVAIL POUR CONTENUE LES ÉLÉMENTS DE LA MATRICE
C     VRES     : VECTEUR SECOND MEMBRE EN ENTREE ET SOLUTION EN SORTIE
C
C Sortie:
C     VRES   : VECTEUR SECOND MEMBRE EN ENTREE ET SOLUTION EN SORTIE
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_EXSOLV(NLIN,
     &                          IA_SL,
     &                          NKG_SL,
     &                          VKG_SL,
     &                          VRES)

      IMPLICIT NONE

      INTEGER NLIN
      INTEGER IA_SL (*)
      INTEGER NKG_SL
      REAL*8  VKG_SL(*)
      REAL*8  VRES  (*)

      INCLUDE 'c_fb.fi'
      REAL*8    DDOT

      REAL*8    VI
      INTEGER*8 XMS, XMD, XMI
      INTEGER   IERR
      INTEGER   I, ID, J, K
      INTEGER   IMIN, JMIN
      INTEGER   IMIN_SL, IMAX_SL
      INTEGER   IA_SL_I0, IA_SL_I1
      INTEGER   IA_SL_J0, IA_SL_J1
      INTEGER   NX
C-----------------------------------------------------------------------

C---     OUVRE LES FICHIERS
      XMS = C_FB_OUVRE('vkgs_i.tmp', C_FB_LECTURE)
      XMD = C_FB_OUVRE('vkgd_i.tmp', C_FB_LECTURE)
      XMI = C_FB_OUVRE('vkgi.tmp'  , C_FB_LECTURE)

C---     RESOLUTION PAR DESCENTE
      IMIN_SL = 0
      IMAX_SL = 0
100   CONTINUE
         CALL SP_LDUD_LISSLI(XMI,
     &                       NLIN,
     &                       IMIN_SL,
     &                       IMAX_SL,
     &                       IA_SL,
     &                       NKG_SL,
     &                       VKG_SL)
         K = 1
         IA_SL_I0 = IA_SL(IMIN_SL)
         DO I=IMIN_SL,IMAX_SL
            IA_SL_I1 = IA_SL(I+1)

            NX   = IA_SL_I1 - IA_SL_I0
            JMIN = I - NX
            VRES(I) = VRES(I) - DDOT(NX,VKG_SL(K),1,VRES(JMIN),1)

            K = K + NX
            IA_SL_I0 = IA_SL_I1
         ENDDO
      IF (IMAX_SL .LT. NLIN) GOTO 100

C---     RESOLUTION PAR REMONTEE
      IMIN_SL = 0
      IMAX_SL = 0
200   CONTINUE
         CALL SP_LDUD_LISSLDS(XMS,
     &                        XMD,
     &                        NLIN,
     &                        IMIN_SL,
     &                        IMAX_SL,
     &                        IA_SL,
     &                        NKG_SL,
     &                        VKG_SL)
         ID = 1
         K  = (IMAX_SL-IMIN_SL+1) + 1     ! Prend en compte la diag
         IA_SL_J1 = IA_SL(IMAX_SL+1)
         DO J=IMAX_SL,IMIN_SL,-1
            IA_SL_J0 = IA_SL(J)

            VRES(J) = VRES(J)*VKG_SL(ID)
            VI = VRES(J)
            ID = ID + 1

            NX   = IA_SL_J1 - IA_SL_J0
            IMIN = J - NX
            CALL DAXPY(NX,-VI,VKG_SL(K),1,VRES(IMIN),-1)
            K = K + NX

            IA_SL_J1 = IA_SL_J0
         ENDDO
      IF (IMIN_SL .GT. 1) GOTO 200

C---     FERME LES FICHIERS
      IERR = C_FB_FERME(XMS)
      IERR = C_FB_FERME(XMD)
      IERR = C_FB_FERME(XMI)

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_LISSLI
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_LISSLI LIS LE TRIANGLE INFERIEUR DE LA
C     MATRICE SKY-LINE STOCKEE SUR FICHIERS. ON LIT AUTANT DE LIGNES
C     QUE PEUT CONTENIR LA TABLE DE TRAVAIL VKG_SL.
C
C Entrée:
C     XMI      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE INFERIEUR
C     NLIN     : DIMENSION DU PROBLEME
C     IMIN_SL  : LIGNE MIN DE LA SOUS-MATRICE SKY-LINE
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     NKG_SL   : DIMENSION DE LA TABLE VKG_SL
C
C Sortie:
C     IMIN_SL  : LIGNE MIN DE LA SOUS-MATRICE SKY-LINE
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     VKG_SL   : TABLE DE TRAVAIL CONTENANT LES ÉLÉMENTS DE LA MATRICE
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_LISSLI(XMI,
     &                          NLIN,
     &                          IMIN_SL,
     &                          IMAX_SL,
     &                          IA_SL,
     &                          NKG_SL,
     &                          VKG_SL)

      IMPLICIT NONE

      INTEGER*8 XMI
      INTEGER   NLIN
      INTEGER   IMIN_SL
      INTEGER   IMAX_SL
      INTEGER   IA_SL (*)
      INTEGER   NKG_SL
      REAL*8    VKG_SL(*)

      INCLUDE 'c_fb.fi'

      INTEGER IERR
      INTEGER IM
      INTEGER K1, K2
      INTEGER KOFF_SL
C----------------------------------------------------------

      IMIN_SL = IMAX_SL + 1

      K1 = IA_SL(IMIN_SL)
      DO IM=IMIN_SL+1,NLIN
         K2 = IA_SL(IM+1)-1
         IF ((K2-K1+1) .GT. NKG_SL) GOTO 110
      ENDDO
110   CONTINUE
      IMAX_SL = IM - 1

      KOFF_SL = IA_SL(IMIN_SL)-1
      K1 = IA_SL(IMIN_SL)     - KOFF_SL
      K2 = IA_SL(IMAX_SL+1)-1 - KOFF_SL

      IERR = C_FB_LIS(XMI, (K2-K1+1), VKG_SL(K1))

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_LISSLDS
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_LISSLDS LIS LE TRIANGLE SUPERIEUR ET LA
C     DIAGONALE DE LA MATRICE SKY-LINE STOCKEE SUR FICHIERS. ON LIT
C     AUTANT DE LIGNES QUE PEUT CONTENIR LA TABLE DE TRAVAIL VKG_SL.
C     LA LECTURE COMMENCE PAR LA FIN ET REMONTE DANS LA MATRICE.
C
C Entrée:
C     XMS      : HANDLE EXTERNE SUR LE FICHIER DU TRIANGLE SUPERIEURE
C     XMD      : HANDLE EXTERNE SUR LE FICHIER DE LA DIAGONALE
C     NLIN     : DIMENSION DU PROBLEME
C     IMIN_SL  : LIGNE MIN DE LA SOUS-MATRICE SKY-LINE
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     NKG_SL   : DIMENSION DE LA TABLE VKG_SL
C
C Sortie:
C     IMIN_SL  : LIGNE MIN DE LA SOUS-MATRICE SKY-LINE
C     IMAX_SL  : LIGNE MAX DE LA SOUS-MATRICE SKY-LINE
C     VKG_SL   : TABLE DE TRAVAIL CONTENANT LES ÉLÉMENTS DE LA MATRICE
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_LISSLDS(XMS,
     &                           XMD,
     &                           NLIN,
     &                           IMIN_SL,
     &                           IMAX_SL,
     &                           IA_SL,
     &                           NKG_SL,
     &                           VKG_SL)

      IMPLICIT NONE

      INTEGER*8 XMS
      INTEGER*8 XMD
      INTEGER   NLIN
      INTEGER   IMIN_SL
      INTEGER   IMAX_SL
      INTEGER   IA_SL (*)
      INTEGER   NKG_SL
      REAL*8    VKG_SL(*)

      INCLUDE 'c_fb.fi'

      INTEGER IERR
      INTEGER I1, I2, IM
      INTEGER K1, K2, KOFF_SL
C----------------------------------------------------------

      IF (IMIN_SL .LE. 0) IMIN_SL = NLIN+1
      IMAX_SL = IMIN_SL - 1

      K2 = IA_SL(IMAX_SL+1)-1
      DO IM=IMAX_SL,1,-1
         K1 = IA_SL(IM)
         IF ((K2-K1+1+IMAX_SL-IM+1) .GT. NKG_SL) GOTO 110
      ENDDO
110   CONTINUE
      IMIN_SL = IM + 1

      I1 = 1
      I2 = IMAX_SL - IMIN_SL + 1
      KOFF_SL = IA_SL(IMIN_SL)-1 - I2
      K1 = IA_SL(IMIN_SL)     - KOFF_SL
      K2 = IA_SL(IMAX_SL+1)-1 - KOFF_SL

      IERR = C_FB_LIS(XMD, (I2-I1+1), VKG_SL(I1))
      IERR = C_FB_LIS(XMS, (K2-K1+1), VKG_SL(K1))

      RETURN
      END

C************************************************************************
C Sommaire: SP_LDUD_DMPMAT
C
C Description:
C     LA SUBROUTINE PRIVEE SP_LDUD_DMPMAT DUMP LA MATRICE A DES FINS
C     DE DEBUGGAGE. ELLE EST LIMITEE A UNE MATRICE 20X20
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE SP_LDUD_DMPMAT(MP,
     &                          IPIV,
     &                          IMAX_SL,
     &                          IOFF_SL,
     &                          KOFF_SL,
     &                          IA_SL,
     &                          VKGS_SL,
     &                          VKGD_SL,
     &                          VKGI_SL)

      IMPLICIT NONE

      INTEGER MP
      INTEGER IPIV
      INTEGER IMAX_SL
      INTEGER IOFF_SL
      INTEGER KOFF_SL
      INTEGER IA_SL  (*)
      REAL*8  VKGS_SL(*)
      REAL*8  VKGD_SL(*)
      REAL*8  VKGI_SL(*)

      INTEGER IDIMMAX
      INTEGER I, II, J, JJ, K, KJ
      INTEGER JMIN, JMAX

      INTEGER NKGMAX
      PARAMETER (NKGMAX = 20)
      REAL*8  VKG(NKGMAX,NKGMAX)
C-----------------------------------------------------------------------

      IDIMMAX = MIN(IPIV+NKGMAX, IMAX_SL)
      DO I=IPIV,IDIMMAX
         II = I - IOFF_SL
         DO J=IPIV,IDIMMAX
            JJ = J - IOFF_SL
            VKG(II,JJ) = 0.0D0
            IF (J .LT. I) THEN
               KJ = I - (IA_SL(I+1)-IA_SL(I))
               IF (KJ .LE. J) THEN
                  K = IA_SL(I+1)-I+J - KOFF_SL
                  VKG(II,JJ) = VKGI_SL(K)
               ENDIF
            ELSEIF (J .EQ. I) THEN
               VKG(II,II) = VKGD_SL(II)
            ELSEIF (J .GT. I) THEN
               KJ = J - (IA_SL(J+1)-IA_SL(J))
               IF (KJ .LE. I) THEN
                  K = IA_SL(J+1)-J+I - KOFF_SL
                  VKG(II,JJ) = VKGS_SL(K)
               ENDIF
            ENDIF
         ENDDO
      ENDDO

      WRITE(MP,'(A)')
      WRITE(MP,'(2I9)') IPIV, IMAX_SL
      DO I=IPIV,IDIMMAX
         II = I - IOFF_SL
         JMIN = IPIV    - IOFF_SL
         JMAX = IDIMMAX - IOFF_SL
         WRITE(MP,'(100(1PE26.17E3))')(VKG(II,JJ),JJ=JMIN,JMAX)
      ENDDO
      WRITE(MP,'(A)')

      RETURN
      END

