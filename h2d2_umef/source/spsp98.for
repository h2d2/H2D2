C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Source:
C     PAUL SCHUREMAN (Senior Mathematician)
C     MANUAL OF HARMONIC ANALYSIS AND PREDICTION OF TIDES
C     DEPARTMENT OF COMMERCE
C     US COAST AND GEODETIC SURVEY
C     Special Publication No. 98
C     Revised (1940) Edition
C     (Reprinted 1958 with corrections)
C
C Functions:
C   Public:
C   Private:
C     REAL*8 SP98_DT
C     REAL*8 SP98_TBL1_T
C     REAL*8 SP98_TBL1_S
C     REAL*8 SP98_TBL1_H
C     REAL*8 SP98_TBL1_P
C     REAL*8 SP98_TBL1_P1
C     REAL*8 SP98_TBL1_N
C     REAL*8 SP98_I
C     REAL*8 SP98_Qu
C     REAL*8 SP98_XI
C     REAL*8 SP98_NU
C     REAL*8 SP98_NUP
C     REAL*8 SP98_NUP2
C     REAL*8 SP98_Q
C     REAL*8 SP98_P
C     REAL*8 SP98_R
C     SUBROUTINE SP98_VARGS
C     SUBROUTINE SP98_UARGS
C     SUBROUTINE SP98_VCOEF
C     SUBROUTINE SP98_UCOEF
C     SUBROUTINE SP98_FEQU
C     SUBROUTINE SP98_FVAL
C
C************************************************************************

      MODULE SP_SP98_private

      USE TRIGD_M
      IMPLICIT NONE
      PUBLIC

      REAL*8, PRIVATE, PARAMETER :: O = 23 + 27.0D0/60 +  8.26D0/3600   ! Small Omega
      REAL*8, PRIVATE, PARAMETER :: I =  5 +  8.0D0/60 + 43.3546D0/3600 ! Small i

!      REAL*8, PRIVATE, PARAMETER :: CO = COS(O * DEGRAD_DP)
!      REAL*8, PRIVATE, PARAMETER :: SO = SIN(O * DEGRAD_DP)
!      REAL*8, PRIVATE, PARAMETER :: CI = COS(I * DEGRAD_DP)
!      REAL*8, PRIVATE, PARAMETER :: SI = SIN(I * DEGRAD_DP)

      REAL*8, PRIVATE, PARAMETER :: CO = 0.917391762526178573616D0
      REAL*8, PRIVATE, PARAMETER :: SO = 0.397985369139509421453D0
      REAL*8, PRIVATE, PARAMETER :: CI = 0.995970351843271161748D0
      REAL*8, PRIVATE, PARAMETER :: SI = 0.089683099016429223633D0

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     Time since Sun, 31 Dec 1899 12:00:00 GMT
C
C Entrée:
C     T     Temps au format H2D2
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_DT(T)

      REAL*8 SP98_DT
      REAL*8, INTENT(IN) :: T

      REAL*8, PARAMETER :: EPOCH = -2209032000.0D0
C-----------------------------------------------------------------------

      SP98_DT = (T - EPOCH)
      END FUNCTION SP98_DT

C************************************************************************
C Sommaire: Number of Julian centuries from Greenwich 31-12-1899 12:00
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_T(TS)

      USE SP_JUL_M, ONLY: SP_JUL_S2C
      
      REAL*8 SP98_TBL1_T
      REAL*8, INTENT(IN) :: TS
C-----------------------------------------------------------------------

      SP98_TBL1_T = SP_JUL_S2C( SP98_DT(TS) )
      END FUNCTION SP98_TBL1_T

C************************************************************************
C Sommaire:    Mean longitude of the moon (small s, table 1)
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_S(TS)

      REAL*8 SP98_TBL1_S
      REAL*8, INTENT(IN) :: TS

      REAL*8 T, A

      REAL*8, PARAMETER :: C0 = 270 + 26.0D0/60 + 14.72D0/3600
      REAL*8, PARAMETER :: C1 = 1336*360 + 1108411.20D0/3600
      REAL*8, PARAMETER :: C2 = 9.09D0/3600
      REAL*8, PARAMETER :: C3 = 0.0068D0/3600
C-----------------------------------------------------------------------

      T = SP98_TBL1_T(TS)
      A = C0 + T*(C1 + T*(C2 * T*C3))
      SP98_TBL1_S = NORM(A)
      END FUNCTION SP98_TBL1_S

C************************************************************************
C Sommaire:    Mean longitude of the sun (small h, table 1)
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_H(TS)

      REAL*8 SP98_TBL1_H
      REAL*8, INTENT(IN) :: TS

      REAL*8 T, A

      REAL*8, PARAMETER :: C0 = 279 + 41.0D0/60 + 48.04D0/3600
      REAL*8, PARAMETER :: C1 = 129602768.13D0/3600
      REAL*8, PARAMETER :: C2 = 1.089D0/3600
      REAL*8, PARAMETER :: C3 = 0.0D0
C-----------------------------------------------------------------------

      T = SP98_TBL1_T(TS)
      A = C0 + T*(C1 + T*(C2 * T*C3))
      SP98_TBL1_H = NORM(A)
      END FUNCTION SP98_TBL1_H

C************************************************************************
C Sommaire:    Longitude of lunar perigee (small p, table 1)
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_P(TS)

      REAL*8 SP98_TBL1_P
      REAL*8, INTENT(IN) :: TS

      REAL*8 T, A

      REAL*8, PARAMETER :: C0 = 334 + 19.0D0/60 + 40.87D0/3600
      REAL*8, PARAMETER :: C1 = 11*360 + 392515.94D0/3600
      REAL*8, PARAMETER :: C2 = -37.24D0/3600
      REAL*8, PARAMETER :: C3 = -0.045D0/3600
C-----------------------------------------------------------------------

      T = SP98_TBL1_T(TS)
      A = C0 + T*(C1 + T*(C2 * T*C3))
      SP98_TBL1_P = NORM(A)
      END FUNCTION SP98_TBL1_P

C************************************************************************
C Sommaire:    Longitude of solar perigee (small p1, table 1)
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_P1(TS)

      REAL*8 SP98_TBL1_P1
      REAL*8, INTENT(IN) :: TS

      REAL*8 T, A

      REAL*8, PARAMETER :: C0 = 281 + 13.0D0/60 + 15.0D0/3600
      REAL*8, PARAMETER :: C1 = 6189.03D0/3600
      REAL*8, PARAMETER :: C2 = 1.630D0/3600
      REAL*8, PARAMETER :: C3 = 0.012D0/3600
C-----------------------------------------------------------------------

      T = SP98_TBL1_T(TS)
      A = C0 + T*(C1 + T*(C2 * T*C3))
      SP98_TBL1_P1 = NORM(A)
      END FUNCTION SP98_TBL1_P1

C************************************************************************
C Sommaire:    Longitude of Moon's node (capital N, table 1)
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_TBL1_N(TS)

      REAL*8 SP98_TBL1_N
      REAL*8, INTENT(IN) :: TS

      REAL*8 T, A

      REAL*8, PARAMETER :: C0 = 259 + 10.0D0/60 + 57.12D0/3600
      REAL*8, PARAMETER :: C1 = -(5*360 + 482912.63D0/3600)
      REAL*8, PARAMETER :: C2 = 7.58D0/3600
      REAL*8, PARAMETER :: C3 = 0.008D0/3600
C-----------------------------------------------------------------------

      T = SP98_TBL1_T(TS)
      A = C0 + T*(C1 + T*(C2 * T*C3))
      SP98_TBL1_N = NORM(A)
      END FUNCTION SP98_TBL1_N

C************************************************************************
C Sommaire:
C
C Description: Big I
C
C Entrée:
C
C Sortie:
C
C Notes: p.156
C************************************************************************
      FUNCTION SP98_I(N)

      REAL*8 SP98_I
      REAL*8, INTENT(IN) :: N
C-----------------------------------------------------------------------
      SP98_I = ACOSD(CO*CI - SO*SI*COSD(N))
      END FUNCTION SP98_I

C************************************************************************
C Sommaire: nu
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula take from tide_fac.f & congen
C************************************************************************
      FUNCTION SP98_NU(N)

      REAL*8 SP98_NU
      REAL*8, INTENT(IN) :: N

      REAL*8 BIG_I, A
C-----------------------------------------------------------------------

      BIG_I = SP98_I(N)
      A = ASIND( SI * SIND(N) / SIND(BIG_I) )
!!      SP98_NU = NORM(A)
      SP98_NU = A
      END FUNCTION SP98_NU

C************************************************************************
C Sommaire: ksi
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula take from tide_fac.f & congen
C************************************************************************
      FUNCTION SP98_XI(N)

      REAL*8 SP98_XI
      REAL*8, INTENT(IN) :: N

      REAL*8 BIG_I, SML_NU
      REAL*8 SIN_O, COS_O
      REAL*8 A
C-----------------------------------------------------------------------

      BIG_I  = SP98_I(N)
      SML_NU = SP98_NU(N)
      SIN_O = SO * SIND(N) / SIND( BIG_I)
      COS_O = COSD(N)*COSD(SML_NU) + SIND(N)*SIND(SML_NU)*CO

      A = N - ATAN2D(SIN_O, COS_O)
      SP98_XI = NORM(A)
      END FUNCTION SP98_XI

C************************************************************************
C Sommaire: nu'
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula 224 from SP 98.
C************************************************************************
      FUNCTION SP98_NUP(N)

      REAL*8 SP98_NUP
      REAL*8, INTENT(IN) :: N

      REAL*8 BIG_I, SML_NU
      REAL*8 T, A
C-----------------------------------------------------------------------

      BIG_I  = SP98_I (N)
      SML_NU = SP98_NU(N)

      T = SIND(2 * BIG_I)
      A = ATAN2D(T*SIND(SML_NU), T*COSD(SML_NU) + 0.3347D0)

      SP98_NUP = A
      END FUNCTION SP98_NUP

C************************************************************************
C Sommaire: nu"
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula 232 from SP 98.
C************************************************************************
      FUNCTION SP98_NUP2(N)

      REAL*8 SP98_NUP2
      REAL*8, INTENT(IN) :: N

      REAL*8 BIG_I, SML_NU
      REAL*8 T1, T2, A
C-----------------------------------------------------------------------

      BIG_I  = SP98_I (N)
      SML_NU = SP98_NU(N)

      T1 = SIND(BIG_I)**2
      T2 = 2 * SML_NU
      A = 0.5D0 * ATAN2D(T1*SIND(T2), T1*COSD(T2) + 0.0727D0)

      SP98_NUP2 = A
      END FUNCTION SP98_NUP2

C************************************************************************
C Sommaire: P
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula 191 from SP 98
C************************************************************************
      FUNCTION SP98_P(TS, N)

      REAL*8 SP98_P
      REAL*8, INTENT(IN) :: TS
      REAL*8, INTENT(IN) :: N
C-----------------------------------------------------------------------

      SP98_P = SP98_TBL1_P(TS) - SP98_XI(N)
      END FUNCTION SP98_P

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_Q(TS, N)

      REAL*8 SP98_Q
      REAL*8, INTENT(IN) :: TS
      REAL*8, INTENT(IN) :: N

      REAL*8 P
C-----------------------------------------------------------------------
      P = SP98_P(TS, N)
      SP98_Q = ATAN2D (0.483D0 * SIND(P), COSD(P))
      END FUNCTION SP98_Q

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula 204 from SP 98
C************************************************************************
      FUNCTION SP98_Qu(TS, N)

      REAL*8 SP98_Qu
      REAL*8, INTENT(IN) :: TS
      REAL*8, INTENT(IN) :: N
C-----------------------------------------------------------------------

      SP98_Qu = SP98_P(TS, N) - SP98_Q(TS, N)
      END FUNCTION SP98_Qu

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Formula 214 from SP 98
C************************************************************************
      FUNCTION SP98_R(TS, N)

      REAL*8 SP98_R
      REAL*8, INTENT(IN) :: TS
      REAL*8, INTENT(IN) :: N

      REAL*8 P, I, COTI2
C-----------------------------------------------------------------------
      REAL*8 COTD
      REAL*8 A
      COTD(A) = 1.0D0 / TAND(A)
C-----------------------------------------------------------------------
      P = SP98_P(TS, N)
      I = SP98_I(N)
      COTI2  = COTD (I/2)
      SP98_R = ATAN2D (SIND (2*P), COTI2*COTI2/6 - COSD(2*P))
      END FUNCTION SP98_R

C************************************************************************
C Sommaire:
C
C Description:
C // V portion of argument, evaluated at beginning of year.
C        T, s, h, p, p1, c
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_VARGS(TS)

      USE SP_JUL_M, ONLY: SP_JUL_S2D

      REAL*8 :: SP98_VARGS(6)
      REAL*8, INTENT(IN) :: TS

      REAL*8 T
C-----------------------------------------------------------------------

      SP98_VARGS(1) = NORM( SP_JUL_S2D( SP98_DT(TS) ) * 360.0D0 )
      SP98_VARGS(2) = SP98_TBL1_S (TS)
      SP98_VARGS(3) = SP98_TBL1_H (TS)
      SP98_VARGS(4) = SP98_TBL1_P (TS)
      SP98_VARGS(5) = SP98_TBL1_P1(TS)
      SP98_VARGS(6) = 1.0D0

      END FUNCTION SP98_VARGS

C************************************************************************
C Sommaire:
C
C Description:
C // u portion of argument, evaluated at mid-year.
C        XI, NU, NU', 2*NU'', Q, R, Qu
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_UARGS(TS)

      REAL*8 :: SP98_UARGS(7)
      REAL*8, INTENT(IN) :: TS

      REAL*8 :: N
C-----------------------------------------------------------------------

      N = SP98_TBL1_N(TS)
      SP98_UARGS(1) = SP98_XI  (N)
      SP98_UARGS(2) = SP98_NU  (N)
      SP98_UARGS(3) = SP98_NUP (N)
      SP98_UARGS(4) = SP98_NUP2(N)
      SP98_UARGS(5) = SP98_Q   (TS, N)
      SP98_UARGS(6) = SP98_R   (TS, N)
      SP98_UARGS(7) = SP98_Qu  (TS, N)

      END FUNCTION SP98_UARGS

C************************************************************************
C Sommaire:
C
C Description:
C     T, s, h, p, p1, c     p164-165
C
C Entrée:
C
C Sortie:
C
C Notes:
C Il faudrait tout lire d'un fichier ou des fichier t_tide.
C     Quel est le lien entre les nombres de Doodson et les arguments?
C     t_tide donne les nombre de Doodson
C     Le nombre de Doodson sont des facteurs multiplicatifs pour:
C         tau  s  h  p  N'  p1
C     Alors que la table 2 de Shureman est écrite pour
C          T   s  h  p  p1 et un déphasage
C     ON a les relations: 
C        tau = T - s + h
C        N' = -N
C************************************************************************
      FUNCTION SP98_VCOEF(C) RESULT(V)

      INTEGER :: V(6)
      CHARACTER*(*), INTENT(IN) :: C
C-----------------------------------------------------------------------

      SELECT CASE(C)
         CASE('K1'  ); V = (/ 1, 0, 1, 0, 0,-90 /);
         CASE('K2'  ); V = (/ 2, 0, 2, 0, 0,  0 /);
         CASE('L2'  ); V = (/ 2,-1, 2,-1, 0,180 /);
         CASE('M2'  ); V = (/ 2,-2, 2, 0, 0,  0 /);
         CASE('N2'  ); V = (/ 2,-3, 2, 1, 0,  0 /);
         CASE('O1'  ); V = (/ 1,-2, 1, 0, 0, 90 /);
         CASE('P1'  ); V = (/ 1, 0,-1, 0, 0, 90 /);
         CASE('Q1'  ); V = (/ 1,-3, 1, 1, 0, 90 /);
         CASE('S1'  ); V = (/ 1, 0, 0, 0, 0,  0 /);
         CASE('S2'  ); V = (/ 2, 0, 0, 0, 0,  0 /);
!         CASE('N2'); V = (/ 2,-3, 4,-1, 0,  0 /);
!         CASE('M1-DUTCH'); V = (/ 1,-1, 1, 1, 0,-90 /);
!         CASE('Λ2'); V = (/ 2,-1, 0, 1, 0,180 /);

!!!   For basic constituents:
!!!              name            T  s  h  p  p1 c
!!!         CASE('J1'  ); V = (/ 1, 1, 1,-1, 0,-90 /);  !!       0 -1  0  0  0  0      76
!!!         CASE('M1'  ); V = (/ 1,-1, 1, 0, 0,-90 /);  !!       1 -1  0  0  1  0     206
!!!         CASE('M3'  ); V = (/ 3,-3, 3, 0, 0,  0 /);  !!       3 -3  0  0  0  0     149
!!!         CASE('2N2' ); V = (/ 2,-4, 2, 2, 0,  0 /);  !!       2 -2  0  0  0  0      78
!!!         CASE('OO1' ); V = (/ 1, 2, 1, 0, 0,-90 /);  !!      -2 -1  0  0  0  0      77
!!!         CASE('2Q1' ); V = (/ 1,-4, 1, 2, 0, 90 /);  !!       2 -1  0  0  0  0      75
!!!         CASE('R2'  ); V = (/ 2, 0, 1, 0,-1,180 /);  !!       0  0  0  0  0  0       1
!!!         CASE('S1'  ); V = (/ 1, 0, 0, 0, 0,  0 /);  !!       0  0  0  0  0  0       1
!!!         CASE('T2'  ); V = (/ 2, 0,-1, 0, 1,  0 /);  !!       0  0  0  0  0  0       1
!!!         CASE('LDA2'); V = (/ 2,-1, 0, 1, 0,180 /);  !!       2 -2  0  0  0  0      78
!!!         CASE('MU2' ); V = (/ 2,-4, 4, 0, 0,  0 /);  !!       2 -2  0  0  0  0      78
!!!         CASE('NU2' ); V = (/ 2,-3, 4,-1, 0,  0 /);  !!       2 -2  0  0  0  0      78
!!!         CASE('RHO1'); V = (/ 1,-3, 3,-1, 0, 90 /);  !!       2 -1  0  0  0  0      75
!!!         CASE('MF'  ); V = (/ 0, 2, 0, 0, 0,  0 /);  !!      -2  0  0  0  0  0      74
!!!         CASE('MM'  ); V = (/ 0, 1, 0,-1, 0,  0 /);  !!       0  0  0  0  0  0      73
!!!         CASE('SA'  ); V = (/ 0, 0, 1, 0, 0,  0 /);  !!       0  0  0  0  0  0       1
!!!         CASE('SSA' ); V = (/ 0, 0, 2, 0, 0,  0 /);  !!       0  0  0  0  0  0       1
!!!         CASE('A7'  ); V = (/ 0, 3, 0,-1, 0,  0 /);  !!      -2  0  0  0  0  0      74
!!!         CASE('CHI1'); V = (/ 1,-1, 3,-1, 0,-90 /);  !!       0 -1  0  0  0  0      76
!!!         CASE('KJ2' ); V = (/ 2, 1, 2,-1, 0,  0 /);  !!       0 -2  0  0  0  0      79
!!!         CASE('KQ1' ); V = (/ 1, 3, 1,-1, 0,-90 /);  !!      -2 -1  0  0  0  0      77
!!!         CASE('MP1' ); V = (/ 1,-2, 3, 0, 0,-90 /);  !!       0 -1  0  0  0  0      76
!!!         CASE('MSM' ); V = (/ 0, 1,-2, 1, 0,  0 /);  !!       0  0  0  0  0  0      73
!!!         CASE('PHI1'); V = (/ 1, 0, 3, 0, 0,-90 /);  !!       0  0  0  0  0  0       1
!!!         CASE('PI1' ); V = (/ 1, 0,-2, 0, 1, 90 /);  !!       0  0  0  0  0  0       1
!!!         CASE('TK1' ); V = (/ 1, 0,-2, 0, 1, 90 /);  !!       0  0  1  0  0  0     227
!!!         CASE('PSI1'); V = (/ 1, 0, 2, 0,-1,-90 /);  !!       0  0  0  0  0  0       1
!!!         CASE('RP1' ); V = (/ 1, 0, 2, 0,-1, 90 /);  !!       0  0  0  0  0  0       1
!!!         CASE('S3'  ); V = (/ 3, 0, 0, 0, 0,  0 /);  !!       0  0  0  0  0  0       1
!!!         CASE('SIG1'); V = (/ 1,-4, 3, 0, 0, 90 /);  !!       2 -1  0  0  0  0      75
!!!         CASE('SO1' ); V = (/ 1, 2,-1, 0, 0,-90 /);  !!       0 -1  0  0  0  0      76
!!!         CASE('THE1'); V = (/ 1, 1,-1, 1, 0,-90 /);  !!       0 -1  0  0  0  0      76
!!!         CASE('M1C' ); V = (/ 1,-1, 1, 0, 0,  0 /);  !!       1 -1  0  0  0  0     144
         
         
         CASE default
            CALL ERR_ASR(.false.)
      END SELECT

      END FUNCTION SP98_VCOEF

C************************************************************************
C Sommaire:
C
C Description:
C     SP98 TABLE 2 P 164
C     Xi, NU, NU', NU'', Q, R, Qu     p164-165
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_UCOEF(C) RESULT(U)

      INTEGER :: U(7)
      CHARACTER*(*), INTENT(IN) :: C
C-----------------------------------------------------------------------

      SELECT CASE(C)
         CASE('K1'  ); U = (/ 0, 0,-1, 0, 0, 0, 0 /);
         CASE('K2'  ); U = (/ 0, 0, 0,-2, 0, 0, 0 /);
         CASE('L2'  ); U = (/ 2,-2, 0, 0, 0,-1, 0 /);
         CASE('M2'  ); U = (/ 2,-2, 0, 0, 0, 0, 0 /);
         CASE('N2'  ); U = (/ 2,-2, 0, 0, 0, 0, 0 /);
         CASE('O1'  ); U = (/ 2,-1, 0, 0, 0, 0, 0 /);
         CASE('P1'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /);
         CASE('Q1'  ); U = (/ 2,-1, 0, 0, 0, 0, 0 /);
         CASE('S1'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /);
         CASE('S2'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /);
!         CASE('N2'); U = (/ 2,-2, 0, 0, 0, 0, 0 /);
!         CASE('M1-DUTCH'); U = (/ 0,-1, 0, 0, 0, 0,-1 /);
!         CASE('Λ2'); U = (/ 2,-2, 0, 0, 0, 0, 0 /);

!!!   For basic constituents:
!!!              name           xi v vp dvpp Q R

!!!         CASE('J1'  ); U = (/ 0,-1, 0, 0, 0, 0, 0 /); !!      76
!!!         CASE('M1'  ); U = (/ 1,-1, 0, 0, 1, 0, 0 /); !!     206
!!!         CASE('M3'  ); U = (/ 3,-3, 0, 0, 0, 0, 0 /); !!     149
!!!         CASE('2N2' ); U = (/ 2,-2, 0, 0, 0, 0, 0 /); !!      78
!!!         CASE('OO1' ); U = (/-2,-1, 0, 0, 0, 0, 0 /); !!      77
!!!         CASE('2Q1' ); U = (/ 2,-1, 0, 0, 0, 0, 0 /); !!      75
!!!         CASE('R2'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('S1'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('T2'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('LDA2'); U = (/ 2,-2, 0, 0, 0, 0, 0 /); !!      78
!!!         CASE('MU2' ); U = (/ 2,-2, 0, 0, 0, 0, 0 /); !!      78
!!!         CASE('NU2' ); U = (/ 2,-2, 0, 0, 0, 0, 0 /); !!      78
!!!         CASE('RHO1'); U = (/ 2,-1, 0, 0, 0, 0, 0 /); !!      75
!!!         CASE('MF'  ); U = (/-2, 0, 0, 0, 0, 0, 0 /); !!      74
!!!         CASE('MM'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!      73
!!!         CASE('SA'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('SSA' ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('A7'  ); U = (/-2, 0, 0, 0, 0, 0, 0 /); !!      74
!!!         CASE('CHI1'); U = (/ 0,-1, 0, 0, 0, 0, 0 /); !!      76
!!!         CASE('KJ2' ); U = (/ 0,-2, 0, 0, 0, 0, 0 /); !!      79
!!!         CASE('KQ1' ); U = (/-2,-1, 0, 0, 0, 0, 0 /); !!      77
!!!         CASE('MP1' ); U = (/ 0,-1, 0, 0, 0, 0, 0 /); !!      76
!!!         CASE('MSM' ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!      73
!!!         CASE('PHI1'); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('PI1' ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('TK1' ); U = (/ 0, 0, 1, 0, 0, 0, 0 /); !!     227
!!!         CASE('PSI1'); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('RP1' ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('S3'  ); U = (/ 0, 0, 0, 0, 0, 0, 0 /); !!       1
!!!         CASE('SIG1'); U = (/ 2,-1, 0, 0, 0, 0, 0 /); !!      75
!!!         CASE('SO1' ); U = (/ 0,-1, 0, 0, 0, 0, 0 /); !!      76
!!!         CASE('THE1'); U = (/ 0,-1, 0, 0, 0, 0, 0 /); !!      76
!!!         CASE('M1C' ); U = (/ 1,-1, 0, 0, 0, 0, 0 /); !!     144
         
         
         CASE default
            CALL ERR_ASR(.false.)
      END SELECT

      END FUNCTION SP98_UCOEF

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_FEQU(C) RESULT(E)

      INTEGER E
      CHARACTER*(*), INTENT(IN) :: C
C-----------------------------------------------------------------------
!!!!!# SPSP98 TABLE 2 P 164
!!!!!             EQ.
      SELECT CASE(C)
         CASE('K1'  ); E = 227;
         CASE('K2'  ); E = 235;
         CASE('L2'  ); E = 215;
         CASE('M2'  ); E =  78;
         CASE('N2'  ); E =  78;
         CASE('O1'  ); E =  75;
         CASE('P1'  ); E =   1;
         CASE('Q1'  ); E =  75;
         CASE('S1'  ); E =   1;
         CASE('S2'  ); E =   1;
!!!!!'Ν2',       =  78
!!!!!'M1-DUTCH', = 206
!!!!!'Λ2',       =  78

!!!         CASE('J1'  ); E =  76;
!!!         CASE('M1'  ); E = 206;
!!!         CASE('M3'  ); E = 149;
!!!         CASE('2N2' ); E =  78;
!!!         CASE('OO1' ); E =  77;
!!!         CASE('2Q1' ); E =  75;
!!!         CASE('R2'  ); E =   1;
!!!         CASE('S1'  ); E =   1;
!!!         CASE('T2'  ); E =   1;
!!!         CASE('LDA2'); E =  78;
!!!         CASE('MU2' ); E =  78;
!!!         CASE('NU2' ); E =  78;
!!!         CASE('RHO1'); E =  75;
!!!         CASE('MF'  ); E =  74;
!!!         CASE('MM'  ); E =  73;
!!!         CASE('SA'  ); E =   1;
!!!         CASE('SSA' ); E =   1;
!!!         CASE('A7'  ); E =  74;
!!!         CASE('CHI1'); E =  76;
!!!         CASE('KJ2' ); E =  79;
!!!         CASE('KQ1' ); E =  77;
!!!         CASE('MP1' ); E =  76;
!!!         CASE('MSM' ); E =  73;
!!!         CASE('PHI1'); E =   1;
!!!         CASE('PI1' ); E =   1;
!!!         CASE('TK1' ); E = 227;
!!!         CASE('PSI1'); E =   1;
!!!         CASE('RP1' ); E =   1;
!!!         CASE('S3'  ); E =   1;
!!!         CASE('SIG1'); E =  75;
!!!         CASE('SO1' ); E =  76;
!!!         CASE('THE1'); E =  76;
!!!         CASE('M1C' ); E = 144;
         CASE default
            CALL ERR_ASR(.false.)
      END SELECT

      END FUNCTION SP98_FEQU

C************************************************************************
C Sommaire:
C
C Description:
!      // Node factor formulas.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP98_FVAL(ID, TS) RESULT(F)

      REAL*8 :: F
      INTEGER, INTENT(IN) :: ID
      REAL*8, INTENT(IN) :: TS

      REAL*8 :: N
      REAL*8 I, P, NU
C-----------------------------------------------------------------------
      REAL*8 F073, F074, F075, F076, F077, F078, F079
      REAL*8 F144, F149
      REAL*8 F206, F215, F227, F235
      F073(I) = (2.0D0/3.0D0 - SIND(I)**2) / 0.5021D0
      F074(I) = SIND(I)**2 / 0.1578D0
      F075(I) = SIND(I) * COSD(I/2)**2 / 0.38D0
      F076(I) = SIND(2*I) / 0.7214D0
      F077(I) = SIND(I) * SIND(I/2)**2 / 0.0164D0
      F078(I) = COSD(I/2)**4  / 0.9154D0
      F079(I) = SIND(I)**2 / 0.1565D0
      F144(I) = (1 - 10*SIND(I/2)**2 + 15*SIND(I/2)**4)
     &        * COSD(I/2)**2 / 0.5873D0
      F149(I) = COSD(I/2)**6 / 0.8758D0
      F206(I, P) = F075(I)
     &           * SQRT(2.310D0 + 1.435D0*COSD(2*P))
      F215(I, P) = F078(I)
     &           * SQRT(1 - 12*TAND(I/2)**2*COSD(2*P) + 36*TAND(I/2)**4)
      F227(I,NU) = SQRT(0.8965D0*SIND(2*I)**2 +
     &                  0.6001D0*SIND(2*I)*COSD(NU) + 0.1006D0);
      F235(I,NU) = SQRT(19.0444D0*SIND(I)**4 +
     &                   2.7702D0*SIND(I)**2*COSD(2*NU) + 0.0981D0);
C-----------------------------------------------------------------------

      N = SP98_TBL1_N(TS)
      I = SP98_I (N)
      NU= SP98_NU(N)
      P = SP98_P (TS, N)
      SELECT CASE(ID)
         CASE(  1); F = 1;
         CASE( 73); F = F073(I);
         CASE( 74); F = F074(I);
         CASE( 75); F = F075(I);
         CASE( 76); F = F076(I);
         CASE( 77); F = F077(I);
         CASE( 78); F = F078(I);
         CASE( 79); F = F079(I);
         CASE(144); F = F144(I);
         CASE(149); F = F149(I);
         CASE(206); F = F206(I, P);
         CASE(215); F = F215(I, P);
         CASE(227); F = F227(I, NU);
         CASE(235); F = F235(I, NU);
         CASE default
            CALL ERR_ASR(.false.)
      END SELECT
      END FUNCTION SP98_FVAL

      END MODULE SP_SP98_private


      MODULE SP_SP98_M
         USE SP_SP98_private, ONLY:
     &      SP_SP98_DT    => SP98_DT,
     &      SP_SP98_VARGS => SP98_VARGS,
     &      SP_SP98_UARGS => SP98_UARGS,
     &      SP_SP98_VCOEF => SP98_VCOEF,
     &      SP_SP98_UCOEF => SP98_UCOEF,
     &      SP_SP98_FEQU  => SP98_FEQU,
     &      SP_SP98_FVAL  => SP98_FVAL
      END MODULE SP_SP98_M
      