C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  SKY-Line
C Sommaire: Fonctions pour les matrices Sky-Line
C************************************************************************

C************************************************************************
C Sommaire : SP_SKYL_ASMHCL
C
C Description:
C     La fonction SP_SKYL_ASMHCL assemble dans KLD les hauteurs de
C     colonnes à partir des informations de la table de localisation
C     élémentaire KLOCE.
C     Dans cette phase on cumule les liens créés par les DDL dans le
C     triangle supérieur.
C
C Entrée:
C     INTEGER NDLE            Nombre de degré de liberté élémentaire
C     INTEGER KLOCE(NDLE)     Table de localisation élémentaire
C     INTEGER KLD  (*)        Table globales des hauteur de colonne
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_ASMHCL(NDLE, KLOCE, KLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_ASMHCL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      INTEGER KLD  (*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'

      INTEGER I, IJ
      INTEGER J, JJ
C-----------------------------------------------------------------

C---     BOUCLE SUR LES DEGRES DE LIBERTE
      DO I=1,NDLE
         IJ = ABS(KLOCE(I))
         IF (IJ .EQ. 0) GOTO 199

C---        BOUCLE SUR LES DDL DE COUPLAGE INFERIEUR
         DO J=1,NDLE
            JJ = KLOCE(J)
            IF (JJ .GT. 0) THEN
!$omp atomic
               KLD(IJ) = MAX(IJ-JJ, KLD(IJ))
            ENDIF
         ENDDO

199      CONTINUE
      ENDDO

      SP_SKYL_ASMHCL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_CLCIND
C
C Description:
C     La fonction SP_SKYL_CLCIND construit à partir de la table IA,
C     la table JA des indices max de colonne pour chaque ligne et
C     la table JD des pointeurs au terme diagonal.
C
C Entrée:
C     INTEGER N
C     INTEGER IA(*)
C
C Sortie:
C     INTEGER JA(*)
C     INTEGER JD(*)
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_CLCIND(N, IA, JA, JD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_CLCIND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER N
      INTEGER IA(*)
      INTEGER JA(*)
      INTEGER JD(*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'

      INTEGER I, J, J1, J2
C-----------------------------------------------------------------------

C---     CONSTRUCTION DE JA
      CALL IINIT(N,0,JA,1)
      DO I=1,N
         J1 = I-(IA(I+1)-IA(I))
         J2 = I-1
         DO J=J1,J2
            JA(J) = I
         ENDDO
      ENDDO

C---     CONSTRUCTION DE JD
      DO I=1,N
         JD(I)=I
      ENDDO

      SP_SKYL_CLCIND = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_ASMKE
C
C Description:
C     La fonction SP_SKYL_ASMKE assemble la matrice élémentaire VKE dans
C     la matrice sky-line. La matrice VKE est caractérisée par la table
C     de localisation élémentaire KLOCE. Elle est toujours pleine.
C     La matrice globale est composée des tables IA pour les hauteurs
C     cumulées des colonnes et des tables des composantes VKGS, VKGD et VKGI.
C
C Entrée:
C     INTEGER NDLE               NOMBRE DE D.L. ELEMENTAIRE
C     INTEGER KLOCE(NDLE)        TABLE DE LOCALISATION ELEMENTAIRE
C     REAL*8  VKE  (NDLE,NDLE)   MATRICE ELEMENTAIRE KE (PLEINE )
C     INTEGER IA   (*)           HAUTEURS CUMULEES DE COLONNES DE KG
C
C Sortie:
C     REAL*8  VKGS (*)           MATRICE GLOBALE : TABLE SUPERIEURE
C     REAL*8  VKGD (*)           MATRICE GLOBALE : TABLE DIAGONALE
C     REAL*8  VKGI (*)           MATRICE GLOBALE : TABLE INFERIEURE
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_ASMKE(NDLE,
     &                       KLOCE,
     &                       VKE,
     &                       IA,
     &                       VKGS,
     &                       VKGD,
     &                       VKGI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      REAL*8  VKE  (NDLE,NDLE)
      INTEGER IA   (*)
      REAL*8  VKGS (*)
      REAL*8  VKGD (*)
      REAL*8  VKGI (*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'
      include 'log.fi'

      INTEGER I, J, K
      INTEGER IL, JL
C-----------------------------------------------------------------------

C---     CONTROLE DE LA MATRICE
      K = 0
      DO J=1,NDLE
         DO I=1,NDLE
            IF (VKE(I,J) .EQ. 0) K = K+1
         ENDDO
      ENDDO
      IF (K .EQ. NDLE*NDLE) GOTO 9900

C---     BOUCLE SUR LES COLONNES
      DO J=1,NDLE
         JL = KLOCE(J)
         IF (JL .LE. 0) GOTO 19

         DO I=1,NDLE
            IL = ABS(KLOCE(I))
            IF (IL .LE. 0) GOTO 29

            IF (IL .EQ. JL) THEN             ! Diagonale
!$omp atomic
               VKGD(IL) = VKGD(IL) + VKE(I,J)
            ELSEIF (IL .LT. JL) THEN         ! Triangle supérieur
               K = IA(JL+1) - JL+IL
!$omp atomic
               VKGS(K) = VKGS(K) + VKE(I,J)
            ELSE                             ! Triangle inférieur
               K = IA(IL+1) - IL+JL
!$omp atomic
               VKGI(K) = VKGI(K) + VKE(I,J)
            ENDIF

29          CONTINUE
         ENDDO

19       CONTINUE
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_MATRICE_ELEMENTAIRE_NULLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_SKYL_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_FACT
C
C Description:
C     La fonction SP_SKYL_FACT factorise la matrice ligne de ciel
C     en LU. La matrice est composée des tables VKGS,VKGD,VKGI.
C
C Entrée:
C     INTEGER NEQ                DIMENSION DE LA MATRICE
C     INTEGER IA(*)              TABLE DES POINTEURS DE HAUTS DE COLONNE
C     INTEGER JA(*)              TABLE DES INDICE DE COLONNE MAX
C     REAL*8  VKGS (*)           MATRICE GLOBALE : TABLE SUPERIEURE
C     REAL*8  VKGD (*)           MATRICE GLOBALE : TABLE DIAGONALE
C     REAL*8  VKGI (*)           MATRICE GLOBALE : TABLE INFERIEURE
C
C Sortie:
C     VKGS,VKGD,VKGI             MATRICE TRIANGULARISEE
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_FACT(NEQ, IA, JA, VKGS, VKGD, VKGI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_FACT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA  (*)
      INTEGER JA  (*)
      REAL*8  VKGS(*)
      REAL*8  VKGD(*)
      REAL*8  VKGI(*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER IPIV
      INTEGER IOFF
      INTEGER KOFF
      INTEGER L_VT1
      INTEGER L_VT2
      INTEGER L_KTR
      INTEGER LBMAX
C----------------------------------------------------------

C---     CHERCHE LA LARGEUR DE BANDE MAX
      LBMAX = 0
!$omp parallel do default(shared) private(IPIV) reduction(MAX:LBMAX)
      DO IPIV=1,NEQ
         LBMAX = MAX(LBMAX, JA(IPIV)-IPIV+1)
      ENDDO
!$omp end parallel do
      LBMAX = MAX(1,LBMAX)    ! Pour les cas à 1 eq.

C---     ALLOUE L'ESPACE
      L_VT1=0
      L_VT2=0
      L_KTR=0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(LBMAX, L_VT1)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(LBMAX, L_VT2)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(LBMAX, L_KTR)

C---     FACTORISE LES LIGNES UNE A UNE
      IF (ERR_GOOD()) THEN
         IOFF = 0
         KOFF = 0
         DO IPIV=1,NEQ
            IERR = SP_SKYL_FCTLIN(IPIV,
     &                            IOFF,
     &                            KOFF,
     &                            IA,
     &                            JA,
     &                            VKGS,
     &                            VKGD,
     &                            VKGI,
     &                            VA(SO_ALLC_REQVIND(VA,L_VT1)),
     &                            VA(SO_ALLC_REQVIND(VA,L_VT2)),
     &                            KA(SO_ALLC_REQKIND(KA,L_KTR)))
            IF (.NOT. ERR_GOOD()) GOTO 199
         ENDDO
199      CONTINUE
      ENDIF

C---     DESALLOUE L'ESPACE
      IF (L_KTR .NE. 0) IERR = SO_ALLC_ALLINT(0, L_KTR)
      IF (L_VT2 .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_VT2)
      IF (L_VT1 .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_VT1)

      SP_SKYL_FACT = ERR_TYP()
      END

C************************************************************************
C Sommaire: SP_SKYL_FCTLIN
C
C Description:
C     La subroutine SP_SKYL_FCTLIN factorise une ligne d'un
C     système linéaire général en LU. La matrice est stockée
C     par ligne de ciel.
C
C Entrée:
C     IPIV     : INDICE DU PIVOT, DE LA LIGNE A FACTORISER
C     IOFF_SL  : OFFSET SKY-LINE DANS VKGD
C     KOFF_SL  : OFFSET SKY-LINE DANS VKGS-I
C     IA_SL    : TABLE SKY-LINE DES INDICES DE DEBUT DE CHAQUE LIGNE
C     JA_SL    : TABLE SKY-LINE DES JMAX POUR CHAQUE LIGNE
C     VKGS_SL  : SOUS MATRICE SKY-LINE: TRIANGLE SUPERIEUR
C     VKGD_SL  :                        DIAGONALE
C     VKGI_SL  :                        TRIANGLE INFERIEUR
C     VTR1_SL  : VECTEURS DE TRAVAIL DE LA DIMENSION DE
C     VTR2_SL  :     LA LARGEUR DE BANDE MAX
C     KTRV_SL  :
C
C Sortie:
C     VKGS_SL  : MATRICE TRIANGULARISEE POUR LA LIGNE IPIV
C     VKGD_SL  :
C     VKGI_SL  :
C
C Notes:
C
C************************************************************************
      FUNCTION SP_SKYL_FCTLIN(IPIV,
     &                        IOFF_SL,
     &                        KOFF_SL,
     &                        IA_SL,
     &                        JA_SL,
     &                        VKGS_SL,
     &                        VKGD_SL,
     &                        VKGI_SL,
     &                        VTR1_SL,
     &                        VTR2_SL,
     &                        KTRV_SL)

      IMPLICIT NONE

      INTEGER IPIV
      INTEGER IOFF_SL
      INTEGER KOFF_SL
      INTEGER IA_SL  (*)
      INTEGER JA_SL  (*)
      REAL*8  VKGS_SL(*)
      REAL*8  VKGD_SL(*)
      REAL*8  VKGI_SL(*)
      REAL*8  VTR1_SL(*)
      REAL*8  VTR2_SL(*)
      INTEGER KTRV_SL(*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'

      INTEGER I, IA, ID, IV
      INTEGER J, JV
      INTEGER K, KC, KD, KL
      INTEGER IMIN_PIV, IMAX_PIV
      INTEGER JMIN_PIV, JMAX_PIV
      INTEGER IMIN_COL
      INTEGER IA_SL_J0, IA_SL_J1
      INTEGER KOFF
      INTEGER NX, NACTIF
      REAL*8  AI, PIVOT

      REAL*8  ZERO
      REAL*8  UN
      REAL*8  PETIT
      PARAMETER (ZERO  =0.000000000000000000000000000D+00)
      PARAMETER (UN    =1.000000000000000000000000000D+00)
      PARAMETER (PETIT =1.000000000000000000000000000D-32)
C-----------------------------------------------------------------------

C---     PIVOT
      K = IPIV-IOFF_SL
      IF (ABS(VKGD_SL(K)) .LE. PETIT) GOTO 9900
      PIVOT = UN/VKGD_SL(K)
      VKGD_SL(K) = PIVOT

C---     BORNES D'ITERATION
      IMIN_PIV = IPIV+1
      IMAX_PIV = JA_SL(IPIV)
      JMIN_PIV = IMIN_PIV
      JMAX_PIV = IMAX_PIV
      KOFF     = KOFF_SL - IPIV

C---     FILTRE LES LIGNES ACTIVES
      NACTIF = 0
      IA_SL_J0 = IA_SL(JMIN_PIV)
      DO J=JMIN_PIV,JMAX_PIV
         IA_SL_J1 = IA_SL(J+1)
         IMIN_COL = J - (IA_SL_J1-IA_SL_J0)
         IF (IMIN_COL .LE. IPIV) THEN
            NACTIF = NACTIF + 1
            KTRV_SL(NACTIF) = J
         ENDIF
         IA_SL_J0 = IA_SL_J1
      ENDDO

! !$omp parallel sections
!!!!!$omp& default(shared)
!!!!!$omp& firstprivate(ipiv,jmin_piv,jmax_piv,koff,nactif,pivot)
!!!!!$omp& private(ia,i,iv,j,jv,kc,kl)
C---     TRANSFERT LA LIGNE DU PIVOT DANS LE VECTEUR DE TRAVAIL
! !$omp section
      CALL DINIT((JMAX_PIV-JMIN_PIV+1), ZERO, VTR1_SL, 1)
      DO IA=1,NACTIF
         J  = KTRV_SL(IA)
         KC = IA_SL(J+1)-J-KOFF           ! IA_SL(J+1)-J+IPIV - KOFF_SL
         JV = J-IPIV
         VTR1_SL(JV) = VKGS_SL(KC)
      ENDDO

C---     MODIFIE ET TRANSFERT LA COLONNE DU PIVOT DANS LE VECTEUR DE TRAVAIL
! !$omp section
      CALL DINIT((IMAX_PIV-IMIN_PIV+1), ZERO, VTR2_SL, 1)
      DO IA=1,NACTIF
         I = KTRV_SL(IA)
         KL = IA_SL(I+1)-I-KOFF           ! IA_SL(I+1)-I+IPIV - KOFF_SL
         VKGI_SL(KL) = VKGI_SL(KL)*PIVOT
         IV = I-IPIV
         VTR2_SL(IV) = VKGI_SL(KL)
      ENDDO
! !$omp end parallel sections

! !$omp parallel sections
!!!!!$omp&   default(shared)
!!!!!$omp&   firstprivate(ioff_sl,ipiv,koff,nactif)
!!!!!$omp&   private(ai,i,ia,id,j,kd,kl,nx)
C---     MODIFIE LES LIGNES SOUS LA DIAGONALE
! !$omp section
      DO IA=1,NACTIF
         I = KTRV_SL(IA)
         KL = IA_SL(I+1)-I-KOFF           ! IA_SL(I+1)-I+IPIV - KOFF_SL

         AI = VKGI_SL(KL)
         IF (AI .NE. ZERO) THEN
            KL = KL+1
            NX = I-(IPIV+1)
            CALL DAXPY(NX,-AI,VTR1_SL,1,VKGI_SL(KL),1)
         ENDIF
      ENDDO

C---     MODIFIE LA DIAGONALE
! !$omp section
      DO IA=1,NACTIF
         I = KTRV_SL(IA)
         KD = I-IOFF_SL
         ID = I-IPIV
         VKGD_SL(KD) = VKGD_SL(KD) - VTR1_SL(ID)*VTR2_SL(ID)
      ENDDO

C---     MODIFIE LES COLONNES AU-DESSUS DE LA DIAGONALE
      DO IA=1,NACTIF
         J = KTRV_SL(IA)
         KL = IA_SL(J+1)-J-KOFF           ! IA_SL(J+1)-J+IPIV - KOFF_SL

         AI = VKGS_SL(KL)
         IF (AI .NE. ZERO) THEN
            KL = KL + 1
            NX = J-(IPIV+1)
            CALL DAXPY(NX,-AI,VTR2_SL,1,VKGS_SL(KL),1)
         ENDIF
      ENDDO
! !$omp end parallel sections

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'FTL_PIVOT_NUL'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_EQUATION', ' : ', IPIV
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,1PE25.17E3)') 'MSG_VALEUR',' : ',VKGD_SL(K)
      CALL ERR_AJT(ERR_BUF)

9999  CONTINUE
      SP_SKYL_FCTLIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_SOLV
C
C Description:
C     La fonction SP_SKYL_SOLV résout un système algébrique stocké
C     par ligne de ciel. Le vecteur du second membre contient en
C     sortie la solution.
C
C Entrée:
C     IA    : TABLE DES POINTEURS DES HAUTEURS DE COLONNES
C     JA    : TABLE DE L'INDICE DE COLONNE MAX POUR CHAQUE LIGNE
C     VKGS  : TABLE DES ÉLÉMENTS DU TRIANGLE SUPÉRIEUR
C     VKGD  : TABLE DES ÉLÉMENTS DIAGONAUX
C     VKGI  : TABLE DES ÉLÉMENTS DU TRIANGLE INFÉRIEUR
C     VRES  : VECTEUR SECOND MEMBRE EN ENTREE ET SOLUTION EN SORTIE
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_SOLV(NEQ, IA, JA, VKGS, VKGD, VKGI, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_SOLV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKGS (*)
      REAL*8  VKGD (*)
      REAL*8  VKGI (*)
      REAL*8  VRES (*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'
      REAL*8  DDOT

      REAL*8  VI
      INTEGER I, J
      INTEGER IA_I0, IA_I1
      INTEGER IA_J0, IA_J1
      INTEGER IMIN, JMIN
      INTEGER NX
C-----------------------------------------------------------------------

C---     RESOLUTION PAR DESCENTE
      IA_I0 = IA(1)
      DO I=1,NEQ
         IA_I1 = IA(I+1)

         NX   = IA_I1 - IA_I0
         JMIN = I - NX
         VRES(I) = VRES(I) - DDOT(NX,VKGI(IA_I0),1,VRES(JMIN),1)

         IA_I0 = IA_I1
      ENDDO

C---     RESOLUTION PAR REMONTEE
      IA_J1 = IA(NEQ+1)
      DO J=NEQ,1,-1
         IA_J0 = IA(J)

         VRES(J) = VRES(J)*VKGD(J)
         VI = VRES(J)

         NX   = IA_J1 - IA_J0
         IMIN = J - NX
         CALL DAXPY(NX,-VI,VKGS(IA_J0),1,VRES(IMIN),1)

         IA_J1 = IA_J0
      ENDDO

      SP_SKYL_SOLV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_SCAL
C
C Description:
C     La fonction SP_MORS_SCAL multiplie la matrice par un scalaire.
C
C Entrée:
C     IA    : TABLE DES POINTEURS DES HAUTEURS DE COLONNES
C     JA    : TABLE DE L'INDICE DE COLONNE MAX POUR CHAQUE LIGNE
C     VKGS  : TABLE DES ÉLÉMENTS DU TRIANGLE SUPÉRIEUR
C     VKGD  : TABLE DES ÉLÉMENTS DIAGONAUX
C     VKGI  : TABLE DES ÉLÉMENTS DU TRIANGLE INFÉRIEUR
C     V     : SCALAIRE MULTIPLICATIF
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_SCAL(NEQ, IA, JA, VKGS, VKGD, VKGI, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_SCAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKGS (*)
      REAL*8  VKGD (*)
      REAL*8  VKGI (*)
      REAL*8  V

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'

      INTEGER NKGSP
C-----------------------------------------------------------------------

      NKGSP = IA(NEQ+1) - 1
      CALL DSCAL(NKGSP, V, VKGS,1)
      CALL DSCAL(NKGSP, V, VKGI,1)
      CALL DSCAL(NEQ,   V, VKGD,1)

      SP_SKYL_SCAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_MULD
C
C Description:
C     La fonction SP_SKYL_MULD scale vector
C
C Entrée:
C     IA    : TABLE DES POINTEURS DES HAUTEURS DE COLONNES
C     JA    : TABLE DE L'INDICE DE COLONNE MAX POUR CHAQUE LIGNE
C     VKGS  : TABLE DES ÉLÉMENTS DU TRIANGLE SUPÉRIEUR
C     VKGD  : TABLE DES ÉLÉMENTS DIAGONAUX
C     VKGI  : TABLE DES ÉLÉMENTS DU TRIANGLE INFÉRIEUR
C     VRES  : VECTEUR SECOND MEMBRE EN ENTREE ET SOLUTION EN SORTIE
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_MULD(NEQ, IA, JA, VKGS, VKGD, VKGI, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_MULD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKGS (*)
      REAL*8  VKGD (*)
      REAL*8  VKGI (*)
      REAL*8  V    (*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'

      REAL*8  VI
      INTEGER I, J
      INTEGER IA_I0, IA_I1
      INTEGER IA_J0, IA_J1
      INTEGER IMIN, JMIN
      INTEGER NX
C-----------------------------------------------------------------------

C---     Triangle inférieur
      IA_I0 = IA(1)
      DO I=1,NEQ
         IA_I1 = IA(I+1)

         NX   = IA_I1 - IA_I0
         CALL DSCAL(NX, V(I), VKGI(IA_I0), 1)

         IA_I0 = IA_I1
      ENDDO

C---     Diagonale
      CALL DAXTY(NEQ, 1.0D0, V, 1, VKGD, 1)

C---     Triangle supérieur
      IA_J1 = IA(NEQ+1)
      DO J=NEQ,1,-1
         IA_J0 = IA(J)

         NX   = IA_J1 - IA_J0
         IMIN = J - NX
         CALL DAXTY(NX, 1.0D0, V(IMIN), 1, VKGS(IA_J0), 1)

         IA_J1 = IA_J0
      ENDDO

      SP_SKYL_MULD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_MULV
C
C Description:
C     La fonction SP_SKYL_MULV fait l'opération de multiplication
C     matrice*vecteur R = M . V
C
C Entrée:
C     IA    : TABLE DES POINTEURS DES HAUTEURS DE COLONNES
C     JA    : TABLE DE L'INDICE DE COLONNE MAX POUR CHAQUE LIGNE
C     VKGS  : TABLE DES ÉLÉMENTS DU TRIANGLE SUPÉRIEUR
C     VKGD  : TABLE DES ÉLÉMENTS DIAGONAUX
C     VKGI  : TABLE DES ÉLÉMENTS DU TRIANGLE INFÉRIEUR
C     V     : VECTEUR MULTIPLICATIF
C
C Sortie:
C     R     : SOLUTION EN SORTIE
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_MULV(NEQ, IA, JA, VKGS, VKGD, VKGI, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_MULV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA   (*)
      INTEGER JA   (*)
      REAL*8  VKGS (*)
      REAL*8  VKGD (*)
      REAL*8  VKGI (*)
      REAL*8  V    (*)
      REAL*8  R    (*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'
      REAL*8  DDOT

      REAL*8  VI
      INTEGER I, J
      INTEGER IA_I0, IA_I1
      INTEGER IA_J0, IA_J1
      INTEGER IMIN, JMIN
      INTEGER NX
C-----------------------------------------------------------------------

C---     Diagonale
      CALL DCOPY(NEQ, V, 1, R, 1)
      CALL DAXTY(NEQ, 1.0D0, VKGD, 1, R, 1)

C---     Triangle inférieur
      IA_I0 = IA(1)
      DO I=1,NEQ
         IA_I1 = IA(I+1)

         NX   = IA_I1 - IA_I0
         JMIN = I - NX
         R(I) = R(I) + DDOT(NX, VKGI(IA_I0), 1, V(JMIN), 1)

         IA_I0 = IA_I1
      ENDDO

C---     Triangle supérieur
      IA_J1 = IA(NEQ+1)
      DO J=NEQ,1,-1
         IA_J0 = IA(J)

         NX   = IA_J1 - IA_J0
         IMIN = J - NX
         CALL DAXPY(NX, V(J), VKGS(IA_J0), 1, R(IMIN), 1)

         IA_J1 = IA_J0
      ENDDO

      SP_SKYL_MULV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_SKYL_DMPMAT
C
C Description:
C     La subroutine privée SP_SKYL_DMPMAT est un utilitaire
C     d'impression de matrice stockée en ligne-de-ciel. Son
C     utilisation est limitée à des matrices 100x100.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_SKYL_DMPMAT(NEQ, IA, VKGS, VKGD, VKGI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SKYL_DMPMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER IA  (*)
      REAL*8  VKGS(*)
      REAL*8  VKGD(*)
      REAL*8  VKGI(*)

      INCLUDE 'spskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I,J, K, KJ
      REAL*8  V
C-----------------------------------------------------------------------

      DO I=1,NEQ
         WRITE(LOG_BUF,*) 'Row: ', I
         CALL LOG_ECRIS(LOG_BUF)

         DO J=MAX(1,I-2),MIN(I+2,NEQ) ! 1,NEQ
            V = 0.0D0

            IF (J .LT. I) THEN
               KJ = I-(IA(I+1)-IA(I))
               IF (J .GE. KJ) THEN
                  K = IA(I+1)-I+J
                  V = VKGI(K)
               ENDIF
            ELSEIF (J .EQ. I) THEN
               V = VKGD(I)
            ELSEIF (J.GT.I) THEN
               KJ = J-(IA(J+1)-IA(J))
               IF (I .GE. KJ) THEN
                  K = IA(J+1)-J+I
                  V = VKGS(K)
               ENDIF
            ENDIF

            WRITE(LOG_BUF,*) '(', J, ',', V, ')'
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
      ENDDO

      SP_SKYL_DMPMAT = ERR_TYP()
      RETURN
      END
