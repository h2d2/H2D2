C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Notes: Subroutine élémentaires générique qui peuvent être utilisée par tous
C        les éléments
C
C Functions:
C   Public:
C     SUBROUTINE SP_SPLIT_SPLIT
C   Private:
C
C************************************************************************

C**************************************************************
C Sommaire: SP_SPLIT_SPLIT
C
C Description:
C     La fonction SP_SPLIT_SPLIT(...)
C
C Entrée:
C
C Sortie:
C
C Notes:
C****************************************************************
      FUNCTION SP_SPLIT_SPLIT(ITPEV,
     &                        NNELV,
     &                        NCELV,
     &                        NELV,
     &                        KNGV,
     &                        ITPEZ,
     &                        NNELZ,
     &                        NCELZ,
     &                        NELZ,
     &                        KNGZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_SPLIT_SPLIT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: ITPEV
      INTEGER, INTENT(IN) :: NNELV
      INTEGER, INTENT(IN) :: NCELV
      INTEGER, INTENT(IN) :: NELV
      INTEGER, INTENT(IN) :: KNGV (NCELV,NELV)
      INTEGER ITPEZ
      INTEGER NNELZ
      INTEGER NCELZ
      INTEGER NELZ
      INTEGER KNGZ (NCELZ,*)

      INCLUDE 'spsplit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'

      INTEGER IEV, IEZ
C-----------------------------------------------------------------------

C ---    Dispatch
      IF (ITPEZ .EQ. EG_TPGEO_INDEFINI) THEN
         GOTO 1000
      ELSE
         GOTO 2000
      ENDIF

C---     Dimensionnement
C        ===============
1000  CONTINUE
      ITPEZ = EG_TPGEO_INDEFINI; NNELZ = 0; NCELZ = 0; NELZ = 0
      IF (ITPEV .EQ. EG_TPGEO_T6) THEN
         ITPEZ = EG_TPGEO_T3; NNELZ = 3; NCELZ = 3; NELZ = NELV*4
      ELSEIF (ITPEV .EQ. EG_TPGEO_T6L) THEN
         ITPEZ = EG_TPGEO_T3; NNELZ = 3; NCELZ = 3; NELZ = NELV*4
      ENDIF
      GOTO 9999

C---     Splitting
C        =========
2000  CONTINUE
D     CALL ERR_ASR(ITPEV .EQ. EG_TPGEO_T6 .OR.
D    &             ITPEV .EQ. EG_TPGEO_T6L)
      IEZ = 0
      DO IEV=1,NELV
         IEZ = IEZ + 1
         KNGZ(1,IEZ) = KNGV(1,IEV)
         KNGZ(2,IEZ) = KNGV(2,IEV)
         KNGZ(3,IEZ) = KNGV(6,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = KNGV(2,IEV)
         KNGZ(2,IEZ) = KNGV(3,IEV)
         KNGZ(3,IEZ) = KNGV(4,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = KNGV(6,IEV)
         KNGZ(2,IEZ) = KNGV(4,IEV)
         KNGZ(3,IEZ) = KNGV(5,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = KNGV(4,IEV)
         KNGZ(2,IEZ) = KNGV(6,IEV)
         KNGZ(3,IEZ) = KNGV(2,IEV)
      ENDDO
D     CALL ERR_ASR(IEZ .EQ. NELZ)
      GOTO 9999

9999  CONTINUE
      SP_SPLIT_SPLIT = ERR_TYP()
      END

