C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Notes: Subroutine élémentaires générique qui peuvent être utilisée par tous
C        les éléments
C
C Functions:
C   Public:
C     INTEGER SP_ELEM_ASMCLIM
C     INTEGER SP_ELEM_ASMFE
C     INTEGER SP_ELEM_ASMKEL
C     INTEGER SP_ELEM_NEQ2NDLT
C     INTEGER SP_ELEM_NDLT2NEQ
C     INTEGER SP_ELEM_CLIM2VAL
C     INTEGER SP_ELEM_DIMILU0
C     INTEGER SP_ELEM_DIMILUN
C     INTEGER SP_ELEM_INDILU0
C     INTEGER SP_ELEM_INDILUN
C     INTEGER SP_ELEM_CTRCON
C     SUBROUTINE SP_ELEM_HESSELLPS
C     INTEGER SP_ELEM_COLO
C     INTEGER SP_ELEM_BLCS
C   Private:
C     INTEGER SP_ELEM_XEQDIMILU0
C     INTEGER SP_ELEM_XEQDIMILUN
C     SUBROUTINE SP_ELEM_EIGENV
C
C************************************************************************

C************************************************************************
C************************************************************************
C************************************************************************
      MODULE SP_BLCS_M

      IMPLICIT NONE

      PUBLIC  :: SP_BLCS_BLOAD
      PUBLIC  :: SP_BLCS_KINV
      PUBLIC  :: SP_BLCS_LOG
      PUBLIC  :: SP_BLCS_STATTS

      PRIVATE :: FIND_I
      PRIVATE :: INSERT_SORT_NO_DUP

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire SP_BLCS_BLOAD calcule la charge pour chaque bloc,
C     c'est à dire les éléments adhérents aux noeuds de chaque bloc.
C
C Entrée:
C     NCELV       Nombre de Connectivités par Élément de Volume
C     NELV        Nombre d'Élement de Volume
C     KNGV        Table de KonnectiVité Globale de Volume
C     NBLCS       Nombre de blocs
C     NELMAX      Nombre max d'éléments dim_2 de KBLELE
C
C Sortie:
C     KBLBLC      Indices (noeuds, éléments) des blocs
C     KBLELE      Table des éléments par bloc
C
C Notes:
C
C************************************************************************
      FUNCTION SP_BLCS_BLOAD(NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NBLCS,
     &                       NELMAX,
     &                       KBLBLC,
     &                       KBLELE,
     &                       KIDX,
     &                       KINV,
     &                       KTRV)

      IMPLICIT NONE

      INTEGER SP_BLCS_BLOAD
      INTEGER, INTENT(IN)    :: NCELV
      INTEGER, INTENT(IN)    :: NELV
      INTEGER, INTENT(IN)    :: KNGV   (NCELV, NELV)
      INTEGER, INTENT(IN)    :: NBLCS
      INTEGER, INTENT(IN)    :: NELMAX
      INTEGER, INTENT(INOUT) :: KBLBLC (4, NBLCS)
      INTEGER, INTENT(INOUT) :: KBLELE (*)
      INTEGER, INTENT(IN)    :: KIDX(:)
      INTEGER, INTENT(IN)    :: KINV(:)
      INTEGER, INTENT(INOUT) :: KTRV(:)

      INCLUDE 'err.fi'

      INTEGER LTRV
      INTEGER IB, IE, IN
      INTEGER IENXT
      INTEGER INDEB, INFIN
      INTEGER IEDEB, IEFIN
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Boucle sur les blocs
      IENXT = 1
      DO IB=1,NBLCS
         INDEB = KBLBLC(1,IB)
         INFIN = KBLBLC(2,IB)

C---        Éléments de la plage de noeuds
D        KTRV(:) = -1
         IEDEB = KIDX(INDEB)
         IEFIN = KIDX(INDEB+1)-1
         LTRV  = IEFIN - IEDEB + 1
         KTRV(1:LTRV) = KINV(IEDEB:IEFIN)
         DO IN=INDEB+1,INFIN
            IEDEB = KIDX(IN)
            IEFIN = KIDX(IN+1)-1
            DO IE=IEDEB,IEFIN
               CALL INSERT_SORT_NO_DUP(LTRV, KTRV, KINV(IE))
D              CALL ERR_ASR(LTRV .LT. SIZE(KTRV))
            ENDDO
         ENDDO
D        CALL ERR_ASR(KTRV(LTRV+1) .EQ. -1)

C---        Enregistre les éléments du bloc
         IEDEB = IENXT
         DO IE=1,LTRV
            IF (IENXT .LE. NELMAX) KBLELE(IENXT) = KTRV(IE)
            IENXT = IENXT + 1
         ENDDO

C---        Enregistre le bloc
         IEFIN = IENXT - 1
         KBLBLC(3,IB) = IEDEB
         KBLBLC(4,IB) = IEFIN
      ENDDO

      SP_BLCS_BLOAD = ERR_TYP()
      RETURN
      END FUNCTION SP_BLCS_BLOAD

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire SP_BLCS_LOG
C
C Entrée:
C     KBLBLC      Indices (noeuds, éléments) des blocs
C     KBLELE      Table des éléments par bloc
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SP_BLCS_LOG(NBLCS, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)

      IMPLICIT NONE

      INTEGER SP_BLCS_LOG
      INTEGER, INTENT(IN) :: NBLCS
      INTEGER, INTENT(IN) :: KBLBLC(4, NBLCS)
      INTEGER, INTENT(IN) :: IBMIN
      INTEGER, INTENT(IN) :: IBMAX
      REAL*8,  INTENT(IN) :: SMOY
      REAL*8,  INTENT(IN) :: SDEV

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IB
C-----------------------------------------------------------------------
      INTEGER LOAD, NNOD
      NNOD(IB) = KBLBLC(2, IB) - KBLBLC(1, IB) + 1
      LOAD(IB) = KBLBLC(4, IB) - KBLBLC(3, IB) + 1
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.renum.blocs'

      WRITE(LOG_BUF, '(6I6,1PE14.6E3,1PE14.6E3)')
     &   IBMIN, IBMAX,
     &   NNOD(IBMIN), NNOD(IBMAX),
     &   LOAD(IBMIN), LOAD(IBMAX),
     &   SMOY,
     &   SDEV
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      SP_BLCS_LOG = ERR_TYP()
      RETURN
      END FUNCTION SP_BLCS_LOG

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire SP_BLCS_BLOAD calcule la charge pour chaque bloc,
C     c'est à dire les éléments adhérents aux noeuds de chaque bloc.
C
C Entrée:
C     NCELV       Nombre de Connectivités par Élément de Volume
C     NELV        Nombre d'Élement de Volume
C     KNGV        Table de KonnectiVité Globale de Volume
C     NBLCS       Nombre de blocs
C     NELMAX      Nombre max d'éléments dim_2 de KBLELE
C
C Sortie:
C     KBLBLC      Indices (noeuds, éléments) des blocs
C     KBLELE      Table des éléments par bloc
C
C Notes:
C
C************************************************************************
      FUNCTION SP_BLCS_STATTS(NBLCS, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)

      IMPLICIT NONE

      INTEGER SP_BLCS_STATTS
      INTEGER, INTENT(IN)  :: NBLCS
      INTEGER, INTENT(IN)  :: KBLBLC(4, NBLCS)
      INTEGER, INTENT(OUT) :: IBMIN
      INTEGER, INTENT(OUT) :: IBMAX
      REAL*8,  INTENT(OUT) :: SMOY
      REAL*8,  INTENT(OUT) :: SDEV

      INCLUDE 'err.fi'

      INTEGER IB
      INTEGER SSUM, SUM2
      INTEGER LMIN, LMAX, L
C-----------------------------------------------------------------------
      INTEGER LOAD, NNOD
      LOAD(IB) = KBLBLC(4, IB) - KBLBLC(3, IB) + 1
C-----------------------------------------------------------------------

      IBMIN = 1
      IBMAX = 1
      LMIN = 2017
      LMAX = -1
      SSUM = 0
      SUM2 = 0
      DO IB=2,NBLCS
         L = LOAD(IB)
         IF (L .LT. LMIN) THEN
            IBMIN = IB
            LMIN  = L
         ELSEIF (L .GT. LMAX) THEN
            IBMAX = IB
            LMAX  = L
         ENDIF
         SSUM = SSUM + L
         SUM2 = SUM2 + L**2
      ENDDO
      SMOY = DBLE(SSUM) / DBLE(NBLCS)
      SDEV = SQRT(DBLE(SUM2) / DBLE(NBLCS) - SMOY**2)

      SP_BLCS_STATTS = ERR_TYP()
      RETURN
      END FUNCTION SP_BLCS_STATTS

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire SP_BLCS_KINV monte la table des connectivités
C     inverses.
C
C Entrée:
C     NCELV       Nombre de Connectivités par Élément de Volume
C     NELV        Nombre d'Élement de Volume
C     KNGV        Table de KonnectiVité Globale de Volume
C     NBLCS       Nombre de blocs
C     NELMAX      Nombre max d'éléments dim_2 de KBLELE
C
C Sortie:
C     KBLBLC      Indices (noeuds, éléments) des blocs
C     KBLELE      Table des éléments par bloc
C
C Notes:
C
C************************************************************************
      FUNCTION SP_BLCS_KINV (NNL,
     &                       NNEL,
     &                       NELL,
     &                       KNGV,
     &                       KIDX,
     &                       KINV)

      IMPLICIT NONE

      INTEGER SP_BLCS_KINV
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: NNEL
      INTEGER, INTENT(IN) :: NELL
      INTEGER, INTENT(IN) :: KNGV  (NNEL, NELL)
      INTEGER, ALLOCATABLE, INTENT(OUT) :: KIDX(:)
      INTEGER, ALLOCATABLE, INTENT(OUT) :: KINV(:)

      INCLUDE 'err.fi'

      INTEGER IE, IN, NO
      INTEGER IERR
      INTEGER NKINV
      INTEGER, ALLOCATABLE :: KAUX(:)
C-----------------------------------------------------------------------

C---     Dimensionne la table des débuts
      ALLOCATE(KIDX(NNL+2), STAT=IERR)
      KIDX(:) = 0
D     KIDX(NNL+1) = Z'CCCCCCCC'
D     KIDX(NNL+2) = Z'CCCCCCCC'

C---     Compte les éléments de chaque noeud local
      DO IE=1,NELL
         DO IN=1,NNEL
            NO = KNGV(IN,IE)
            KIDX(NO) = KIDX(NO) + 1
         ENDDO
      ENDDO
D     DO IN=1,NNL
D        CALL ERR_ASR(KIDX(IN) .GT. 0)
D     ENDDO
D     CALL ERR_ASR(KIDX(NNL+1) .EQ. Z'CCCCCCCC')

C---     Cumule la table
      DO IN=2,NNL
         KIDX(IN) = KIDX(IN) + KIDX(IN-1)
      ENDDO
      DO IN=NNL+1,2,-1
         KIDX(IN) = KIDX(IN-1) + 1
      ENDDO
      KIDX(1) = 1
D     CALL ERR_ASR(KIDX(NNL+2) .EQ. Z'CCCCCCCC')

C---     Table des connectivités inverses
      NKINV = KIDX(NNL+1) - 1
      ALLOCATE(KINV(NKINV+1), STAT=IERR)
D     KINV(:) = -1
D     KINV(NKINV+1) = Z'CCCCCCCC'

C---     Table auxiliaire: Indice des éléments pour chaque noeud
      ALLOCATE(KAUX(NNL+1), STAT=IERR)
      KAUX(1:NNL) = KIDX(1:NNL)
D     KAUX(NNL+1) = Z'CCCCCCCC'

C---     Monte la table des connectivités inverses
      DO IE=1,NELL
         DO IN=1,NNEL
            NO = KNGV(IN,IE)
            KINV(KAUX(NO)) = IE
            KAUX(NO) = KAUX(NO) + 1
         ENDDO
      ENDDO
D     DO IN=1,NNL
D        CALL ERR_ASR(KAUX(IN) .EQ. KIDX(IN+1))
D     ENDDO
D     CALL ERR_ASR(KAUX(NNL+1) .EQ. Z'CCCCCCCC')
D     DO IN=1,NKINV
D        CALL ERR_ASR(KINV(IN) .NE. -1)
D     ENDDO
D     CALL ERR_ASR(KINV(NKINV+1) .EQ. Z'CCCCCCCC')

C---     Récupère la mémoire
      IF (ALLOCATED(KAUX)) DEALLOCATE(KAUX)

      SP_BLCS_KINV = ERR_TYP()
      RETURN
      END FUNCTION SP_BLCS_KINV

C************************************************************************
C     Adapted from Numerical Recipes
C
C     Given an array XX(1:N), and a value X, returns an index J,
C     J in [1,N+1], such that if X is inserted at XX(J), XX is kept sorted.
C     The case X = XX(J) is possible.
C************************************************************************
      INTEGER FUNCTION FIND_I(XX, X)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: XX(:)
      INTEGER, INTENT(IN) :: X

      INTEGER :: JL,JM,JU
C-----------------------------------------------------------------------

      JL = 0                     ! Initialize lower
      JU = SIZE(XX) + 1          ! and upper limits.
      DO WHILE(JU-JL > 1)        ! Repeat until this condition is satisfied.
         JM = (JU+JL)/2          ! Compute a midpoint,
         IF (X > XX(JM)) THEN
            JL=JM                ! and replace either the lower limit
         ELSE
            JU=JM                ! or the upper limit, as appropriate.
         END IF
      END DO

      FIND_I = JU
      END FUNCTION FIND_I

   !!!def binary_search(l, value):
   !!!    low = 0
   !!!    high = len(l)-1
   !!!    while low <= high:
   !!!        mid = (low+high)//2
   !!!        if l[mid] > value: high = mid-1
   !!!        elif l[mid] < value: low = mid+1
   !!!        else: return mid
   !!!    return -1

C************************************************************************
C     From https://rosettacode.org
C************************************************************************
      SUBROUTINE INSERT_SORT_NO_DUP(N, A, K)
      IMPLICIT NONE
      INTEGER, INTENT(INOUT) :: N
      INTEGER, INTENT(INOUT) :: A(:)
      INTEGER, INTENT(IN)    :: K

      INTEGER I, J
C-----------------------------------------------------------------------

C---     Search index of elem greater than K , I in [1, N+1]
      I = FIND_I(A(1:N), K)
D     CALL ERR_ASR(I .GE. 1 .AND. I .LE. N+1)

C---     Check for border cases
      IF ((I .LE. N) .AND. (K .EQ. A(I))) RETURN

C---     Shift
      DO J=N,I,-1
         A(J+1) = A(J)
      ENDDO

C---     Insert
      A(I) = K
      N = N + 1

      RETURN
      END SUBROUTINE INSERT_SORT_NO_DUP

      END MODULE SP_BLCS_M
C************************************************************************
C************************************************************************
C************************************************************************


C************************************************************************
C Sommaire: SP_ELEM_ASMCLIM
C
C Description:
C     La fonction SP_ELEM_ASMCLIM assemble pour un élément l'influence de
C     l'élimination des conditions limites sur le membre de droite.
C
C Entrée:
C     NDLE   : NOMBRE DE DDL PAR ELEMENT
C     KLOCE  : TABLE ELEMENTAIRE DE LOCALISATION DES DDL
C     VKE    : MATRICE ELEMENTAIRE
C     VDLE   : DDL ELEMENTAIRES
C     VFG    : VECTEUR GLOBAL
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_ASMCLIM(NDLE,
     &                         KLOCE,
     &                         VKE,
     &                         VDLE,
     &                         VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_ASMCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      REAL*8  VKE  (NDLE, NDLE)
      REAL*8  VDLE (NDLE)
      REAL*8  VFG  (*)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I, J
      INTEGER IL, JL
      REAL*8  V
C-----------------------------------------------------------------------

C---     BOUCLE SUR LES COLONNES
      DO J=1,NDLE
         JL = KLOCE(J)
         IF (JL .NE. 0) GOTO 19

         V = VDLE(J)
         DO I=1,NDLE
            IL = KLOCE(I)
            IF (IL .GT. 0) THEN
CCC!$__omp_atomic     ! cf Note SP_MORS_ASMKE
               VFG(IL) = VFG(IL) + VKE(I,J)*V
            ENDIF
         ENDDO

19       CONTINUE
      ENDDO

      SP_ELEM_ASMCLIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_ASMFE
C
C Description:
C     La fonction <code>SP_ELEM_ASMFE(...)</code> assemble le
C     vecteur élémentaire dans le vecteur global.
C
C Entrée:
C     NDLE   : NOMBRE DE DDL PAR ELEMENT
C     KLOCE  : TABLE ELEMENTAIRE DE LOCALISATION DES DDL
C     VFE    : VECTEUR ELEMENTAIRE
C     VFG    : VECTEUR GLOBAL
C
C Sortie:
C     VFG    : VECTEUR GLOBAL DANS LEQUEL VFE EST ASSEMBLÉ
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_ASMFE(NDLE,
     &                       KLOCE,
     &                       VFE,
     &                       VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_ASMFE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      REAL*8  VFE  (NDLE)
      REAL*8  VFG  (*)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I, II
C-----------------------------------------------------------------------

C---     Boucle sur DDL de l'élément
CDIR$ IVDEP
      DO I=1,NDLE
         II = KLOCE(I)
         IF (II .GT. 0) THEN
            VFG(II) = VFG(II) + VFE(I)    ! cf Note SP_MORS_ASMKE
         ENDIF
      ENDDO

      SP_ELEM_ASMFE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_ASMKEL
C
C Description:
C     La fonction <code>SP_ELEM_ASMKEL(...)</code> assemble la
C     matrice élémentaire dans une matrice globale diagonale avec
C     sommation sur la diagonale (lumping).
C
C Entrée:
C     NDLE   : Nombre de DDL par element
C     KLOCE  : Table élémentaire de localisation des DDL
C     VKE    : Matrice élémentaire
C     VDLE   : DDL élémentaire
C     VKG    : Matrice diagonale
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_ASMKEL (NDLE,
     &                         KLOCE,
     &                         VKE,
     &                         VKG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_ASMKEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLE
      INTEGER KLOCE(NDLE)
      REAL*8  VKE  (NDLE, NDLE)
      REAL*8  VKG  (*)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I, J
      INTEGER IL, JL
C-----------------------------------------------------------------------

      DO J=1,NDLE
         JL = ABS(KLOCE(J))
         IF (JL .NE. 0) THEN

            DO I=1,NDLE
               IL = KLOCE(I)
               IF (IL .GT. 0) THEN
CCC!$__omp_atomic     ! cf Note SP_MORS_ASMKE
                  VKG(IL) = VKG(IL) + VKE(I,J)
               ENDIF
            ENDDO

         ENDIF
      ENDDO

      SP_ELEM_ASMKEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_NEQ2NDLT
C
C Description:
C     La fonction <code>SP_ELEM_NEQ2NDLT(...)</code> remplace dans une table
C     de stockage en NDLT les entrées correspondantes par celles d'une table
C     de stockage en NEQ (C.L. éliminées).
C
C Entrée:
C     INTEGER NEQ                   Nombre d'équations
C     INTEGER NDLT                  Nombre de DDL
C     INTEGER KLOCN(NDLT)           Table de localisation des DLL
C     REAL*8  VNEQ (NEQ)            Table à transférer
C
C Sortie:
C     REAL*8  VDLG (NDLT)           Table modifiée
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_NEQ2NDLT(NEQ, NDLT, KLOCN, VNEQ, VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_NEQ2NDLT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER NDLT
      INTEGER KLOCN(NDLT)
      REAL*8  VNEQ (NEQ)
      REAL*8  VDLG (NDLT)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I, ID
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     BOUCLE SUR LES DDL
      DO I=1,NDLT
         ID = KLOCN(I)
         IF (ID .GT. 0) VDLG(I) = VNEQ(ID)
      ENDDO

      SP_ELEM_NEQ2NDLT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_NDLT2NEQ
C
C Description:
C     La fonction <code>SP_ELEM_NDLT2NEQ(...)</code> transforme une table
C     de stockage en NDLT en une table de stockage en NEQ (C.L. éliminées).
C
C Entrée:
C     INTEGER NEQ                   Nombre d'équations
C     INTEGER NDLT                  Nombre de DDL
C     INTEGER KLOCN(NDLT)           Table de localisation des DLL
C     REAL*8  VDLG (NDLT)           Table à transférer
C
C Sortie:
C     REAL*8  VNEQ (NEQ)            Table modifiée
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_NDLT2NEQ(NEQ, NDLT, KLOCN, VDLG, VNEQ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_NDLT2NEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NEQ
      INTEGER NDLT
      INTEGER KLOCN(NDLT)
      REAL*8  VDLG (NDLT)
      REAL*8  VNEQ (NEQ)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I, ID
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---        BOUCLE SUR DDL
      DO I=1,NDLT
         ID = KLOCN(I)
         IF (ID .GT. 0) THEN
D           CALL ERR_ASR(ID .LE. I)
D           CALL ERR_ASR(ID .LE. NEQ)
            VNEQ(ID) = VDLG(I)
         ENDIF
      ENDDO

      SP_ELEM_NDLT2NEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_CLIM2VAL
C
C Description:
C     La fonction <code>SP_ELEM_CLIM2VAL(...)</code> assigne les degrés
C     imposés à la valeur VAL.
C
C Entrée:
C     INTEGER NDLT                  Nombre de DDL
C     INTEGER KLOCN(NDLT)           Table de localisation des DLL
C     REAL*8  VAL                   Valeur des DDL imposés
C     REAL*8  VDLG (NDLT)           Table à transférer
C
C Sortie:
C     REAL*8  VDLG (NDLT)           Table modifiée
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_CLIM2VAL(NDLT, KLOCN, VAL, VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_CLIM2VAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDLT
      INTEGER KLOCN(NDLT)
      REAL*8  VAL
      REAL*8  VDLG (NDLT)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER I
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---        BOUCLE SUR DDL
      DO I=1,NDLT
         IF (KLOCN(I) .LE. 0) VDLG(I) = VAL
      ENDDO

      SP_ELEM_CLIM2VAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_DIMILU
C
C Description:
C     La fonction SP_ELEM_DIMILU(...) détermine pour chaque colonne du
C     triangle strictement supérieur de la matrice le nombre de terme
C     pour un stockage morse de type ILU(0).
C
C Entrée:
C     KLOCE
C     KLOCN
C     KNG
C
C Sortie:
C     IA
C     KLD
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_DIMILU0(NNL,
     &                         NDLN,
     &                         NNEL,
     &                         NCEL,
     &                         NELT,
     &                         NEQL,
     &                         KLOCN,
     &                         KLOCE,
     &                         KNG,
     &                         IA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_DIMILU0
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NDLN
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER NEQL
      INTEGER KLOCN(NDLN, NNL)
      INTEGER KLOCE(NDLN, *)
      INTEGER KNG  (NCEL, NELT)
      INTEGER IA   (NEQL+1)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER L_PNT
      INTEGER L_LIE
      INTEGER NPNT
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER SP_ELEM_XEQDIMILU0
      PARAMETER (NINFO = 1)
C-----------------------------------------------------------------

C---     DIMENSIONS DE LA LISTE CHAINEE
      NPNT  = NNL
      NLIEN = (NNEL-1)*NNL       ! Heuristique

C---     ALLOUE L'ESPACE
      L_PNT=0
      IERR = SO_ALLC_ALLINT(NPNT, L_PNT)
      L_LIE=0
      IERR = SO_ALLC_ALLINT((NINFO+1)*NLIEN, L_LIE)

C---     DIMENSIONNE LES TABLES ILU
100   CONTINUE
         IERR = SP_ELEM_XEQDIMILU0(NNL,
     &                             NDLN,
     &                             NNEL,
     &                             NCEL,
     &                             NELT,
     &                             KLOCN,
     &                             KLOCE,
     &                             KNG,
     &                             NPNT,
     &                             KA(SO_ALLC_REQKIND(KA,L_PNT)),
     &                             NINFO,
     &                             NLIEN,
     &                             KA(SO_ALLC_REQKIND(KA,L_LIE)),
     &                             NEQL,
     &                             IA)
         IF (.NOT. ERR_ESTMSG('ERR_LISTE_PLEINE')) GOTO 110
         CALL ERR_RESET()
         NLIEN = NINT(1.5*NLIEN)
         IERR = SO_ALLC_ALLINT((NINFO+1)*NLIEN, L_LIE)
      GOTO 100
110   CONTINUE

C---     DESALLOUE L'ESPACE
      IERR = SO_ALLC_ALLINT(0, L_LIE)
      IERR = SO_ALLC_ALLINT(0, L_PNT)

      SP_ELEM_DIMILU0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_DIMILUN
C
C Description:
C     La fonction SP_ELEM_DIMILUN(...) détermine pour chaque colonne du
C     triangle strictement supérieur de la matrice le nombre de terme
C     pour un stockage morse de type ILU(n).
C
C Entrée:
C     KLOCE
C     KLOCN
C     KNG
C
C Sortie:
C     IA
C     KLD
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_DIMILUN(NNL,
     &                         NDLN,
     &                         NNEL,
     &                         NCEL,
     &                         NELT,
     &                         NEQL,
     &                         KLOCN,
     &                         KLOCE,
     &                         KNG,
     &                         IA,
     &                         KLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_DIMILUN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NDLN
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER NEQL
      INTEGER KLOCN(NDLN, NNL)
      INTEGER KLOCE(NDLN, *)
      INTEGER KNG  (NCEL, NELT)
      INTEGER IA   (NEQL+1)
      INTEGER KLD  (NEQL+1)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER L_PNT
      INTEGER L_LIE
      INTEGER NPNT
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER SP_ELEM_XEQDIMILUN
      PARAMETER (NINFO = 1)
C-----------------------------------------------------------------

C---     DIMENSIONS DE LA LISTE CHAINEE
      NPNT  = NNL
      NLIEN = (NNEL-1)*NNL       ! Heuristique

C---     ALLOUE L'ESPACE
      L_PNT=0
      IERR = SO_ALLC_ALLINT(NPNT, L_PNT)
      L_LIE=0
      IERR = SO_ALLC_ALLINT((NINFO+1)*NLIEN, L_LIE)

C---     DIMENSIONNE LES TABLES ILU
100   CONTINUE
         IERR = SP_ELEM_XEQDIMILUN(NNL,
     &                             NDLN,
     &                             NNEL,
     &                             NCEL,
     &                             NELT,
     &                             KLOCN,
     &                             KLOCE,
     &                             KNG,
     &                             NPNT,
     &                             KA(SO_ALLC_REQKIND(KA,L_PNT)),
     &                             NINFO,
     &                             NLIEN,
     &                             KA(SO_ALLC_REQKIND(KA,L_LIE)),
     &                             NEQL,
     &                             IA,
     &                             KLD)
         IF (.NOT. ERR_ESTMSG('ERR_LISTE_PLEINE')) GOTO 110
         CALL ERR_RESET()
         NLIEN = NINT(1.5*NLIEN)
         IERR = SO_ALLC_ALLINT((NINFO+1)*NLIEN, L_LIE)
      GOTO 100
110   CONTINUE

C---     DESALLOUE L'ESPACE
      IERR = SO_ALLC_ALLINT(0, L_LIE)
      IERR = SO_ALLC_ALLINT(0, L_PNT)

      SP_ELEM_DIMILUN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_XEQDIMILU0
C
C Description:
C     La fonction privée SP_ELEM_XEQDIMILU0 dimensionne la matrice
C     morse par compression de ligne de type CRS.
C     Dans cette phase on compte les liens créés par les DDL dans le
C     matrice en vue du dimensionnement.
C
C Entrée:
C     KLOCE,KNG,KPNT,KLIEN
C
C Sortie:
C     IA    Table non cumulée du nombre de en j
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_XEQDIMILU0(NNL,
     &                            NDLN,
     &                            NNEL,
     &                            NCEL,
     &                            NELT,
     &                            KLOCN,
     &                            KLOCE,
     &                            KNG,
     &                            NPNT,
     &                            KPNT,
     &                            NINFO,
     &                            NLIEN,
     &                            KLIEN,
     &                            NEQL,
     &                            IA)

      IMPLICIT NONE

      INTEGER SP_ELEM_XEQDIMILU0
      INTEGER NNL
      INTEGER NDLN
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER KLOCN(NDLN, NNL)
      INTEGER KLOCE(NDLN, *)
      INTEGER KNG  (NCEL, NELT)
      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1,*)
      INTEGER NEQL
      INTEGER IA   (NEQL+1)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'     ! pour test
      INCLUDE 'splstc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'spskyl.fi'

      INTEGER I, J, I1, I2, II, IN, ID, JD, IE
      INTEGER IERR
      INTEGER INEXT
      INTEGER NOMIN, NOMAX
      INTEGER INFO(1)
      INTEGER KNE(27)
      LOGICAL NVLIEN
C-----------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.umef.spelem'

C---     Initialise la liste chaînée: INEXT, KPNT et KLIEN
      IERR = SP_LSTC_INIT(NPNT, KPNT, NINFO, NLIEN, KLIEN, INEXT)

C---     Initialise la table IA du stockage morse
      CALL IINIT(NEQL+1, 0, IA, 1)

C---  CONSTRUCTION DE LA TABLE IA A PARTIR DU COUPLAGE ENTRE LES DIFFERENTS
C     DEGRES DE LIBERTES

C---     Couplage des DDL d'un même noeud
      DO IN=1,NNL
         DO I=1,NDLN
            ID = KLOCN(I,IN)
            IF (ID. LE. 0) GOTO 119

            II = 1      ! pour le couplage i-i
            DO J=1,NDLN
               IF (I .NE. J) THEN
                  JD = KLOCN(J,IN)
                  IF (JD. NE. 0) II = II + 1
               ENDIF
            ENDDO
            IA(ID) = II

119         CONTINUE
         ENDDO
      ENDDO

C---     Couplages entre DDL d'un element
      DO IE=1,NELT

C---        Extraction des connectivités
         DO I=1,NNEL
            KNE(I) = KNG(I,IE)
         ENDDO

C---        Construction de la table KLOCE
         DO I=1,NNEL
            IN = KNE(I)
            DO ID=1,NDLN
               KLOCE(ID, I) = KLOCN(ID, IN)
            ENDDO
         ENDDO

C---        Couples de noeuds susceptibles d'être dupliqués, autrement
C           dit pouvant apparaître dans un autre élément.
C           Pour éviter d'avoir à stocker deux fois cette information
C           nous avons recours à l'algorithme de liste chaînée qui nous
C           retourne l'information via NVLIEN. Si NVLIEN = .TRUE. le lien
C           est nouveau.
         DO I1=1,NNEL-1
            DO I2=I1+1,NNEL
               NOMIN = MIN(KNE(I1),KNE(I2))
               NOMAX = MAX(KNE(I1),KNE(I2))
               INFO(1) = NOMAX
               IERR = SP_LSTC_AJTCHN(NPNT,KPNT,NINFO,NLIEN,KLIEN,INEXT,
     &                               NOMIN, INFO, 1, NVLIEN)
               IF (ERR_BAD()) GOTO 9999
               IF (NVLIEN) THEN
                  IERR = SP_MORS_ASMDIMCRS(NDLN,
     &                                     KLOCN(1,NOMIN),
     &                                     KLOCN(1,NOMAX),
     &                                     NEQL,   ! En fait NEQL+1, le
     &                                     IA)    ! +1 n'est pas utilisé
               ENDIF

            ENDDO
         ENDDO

      ENDDO

C-----------------------------------------------------------------
9999  CONTINUE
      SP_ELEM_XEQDIMILU0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_XEQDIMILUN
C
C Description:
C     La fonction privée SP_ELEM_XEQDIMILUN dimensionne la matrice
C     morse par compression de colonne de type ILU(0).
C     Dans cette phase on compte les liens créés par les DDL dans le
C     triangle supérieur en vue du dimensionnement.
C
C Entrée:
C     KLOCE,KNG,KPNT,KLIEN
C
C Sortie:
C     IA    Table non cumulée du nombre de en j
C     KLD   Table des hauteurs de ligne de ciel
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_XEQDIMILUN(NNL,
     &                            NDLN,
     &                            NNEL,
     &                            NCEL,
     &                            NELT,
     &                            KLOCN,
     &                            KLOCE,
     &                            KNG,
     &                            NPNT,
     &                            KPNT,
     &                            NINFO,
     &                            NLIEN,
     &                            KLIEN,
     &                            NEQL,
     &                            IA,
     &                            KLD)

      IMPLICIT NONE

      INTEGER SP_ELEM_XEQDIMILUN
      INTEGER NNL
      INTEGER NDLN
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER KLOCN(NDLN, NNL)
      INTEGER KLOCE(NDLN, *)
      INTEGER KNG  (NCEL, NELT)
      INTEGER NPNT
      INTEGER KPNT (*)
      INTEGER NINFO
      INTEGER NLIEN
      INTEGER KLIEN(NINFO+1,*)
      INTEGER NEQL
      INTEGER IA   (NEQL+1)
      INTEGER KLD  (NEQL+1)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'     ! pour test
      INCLUDE 'splstc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'spskyl.fi'

      INTEGER I, I1, I2, II, IN, ID, JD, IE
      INTEGER IERR
      INTEGER INEXT
      INTEGER NOMIN, NOMAX
      INTEGER INFO(1)
      INTEGER KNE(27)
      LOGICAL NVLIEN
C-----------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.umef.spelem'

C---     Initialise la liste chaînée: INEXT, KPNT et KLIEN
      IERR = SP_LSTC_INIT(NPNT, KPNT, NINFO, NLIEN, KLIEN, INEXT)

C---     Initialise les tables KLD de la ligne de ciel et IA du stockage morse
      CALL IINIT(NEQL+1, 0, KLD,1)
      CALL IINIT(NEQL+1, 0, IA, 1)

C---  CONSTRUCTION DE LA TABLE IA A PARTIR DU COUPLAGE ENTRE LES DIFFERENTS
C     DEGRES DE LIBERTES

C---     Couplage des DDL d'un même noeud
      DO IN=1,NNL                ! Uniquement les DDL privés
         II = 0                  ! Pas de couplage d'un DDL avec lui-meme (1-1)
         DO ID=1,NDLN            ! Pas de double couplage (1-2) (2-1)
            JD = KLOCN(ID,IN)    ! ==> une suite (0,1,2...)
            IF (JD .GT. 0) THEN
               IA(JD) = II
               II = II + 1
            ENDIF
         ENDDO
      ENDDO

C---     Couplages entre DDL d'un element
      DO IE=1,NELT

C---        Extraction des connectivités
         DO I=1,NNEL
            KNE(I) = KNG(I,IE)
         ENDDO

C---        Construction de la table KLOCE
         DO I=1,NNEL
            IN = KNE(I)
            DO ID=1,NDLN
               KLOCE(ID, I) = KLOCN(ID, IN)
            ENDDO
         ENDDO

C---        Assemble la table KLD pour la ligne de ciel
         IERR = SP_SKYL_ASMHCL(NDLN*NNEL, KLOCE, KLD)

C---        Couples de noeuds susceptibles d'être dupliqués, autrement
C           dit pouvant apparaître dans un autre élément.
C           Pour éviter d'avoir à stocker deux fois cette information
C           nous avons recours à l'algorithme de liste chaînée qui nous
C           retourne l'information via NVLIEN. Si NVLIEN = .TRUE. le lien
C           est nouveau.
         DO I1=1,NNEL-1
            DO I2=I1+1,NNEL
               NOMIN = MIN(KNE(I1),KNE(I2))
               NOMAX = MAX(KNE(I1),KNE(I2))
               INFO(1) = NOMAX
               IERR = SP_LSTC_AJTCHN(NPNT,KPNT,NINFO,NLIEN,KLIEN,INEXT,
     &                               NOMIN, INFO, 1, NVLIEN)
               IF (ERR_BAD()) GOTO 9999
               IF (NVLIEN) THEN
                  IERR = SP_MORS_ASMDIMCCS(NDLN,
     &                                     KLOCN(1,NOMIN),
     &                                     KLOCN(1,NOMAX),
     &                                     NEQL,   ! En fait NEQL+1, le
     &                                     IA)    ! +1 n'est pas utilisé
               ENDIF

            ENDDO
         ENDDO

      ENDDO

C-----------------------------------------------------------------
9999  CONTINUE
      SP_ELEM_XEQDIMILUN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_INDILU0
C
C Description:
C     La fonction SP_ELEM_INDILU0 assemble les indices d'une matrice ILU(0)
C     pour un stockage morse de type CRS. La table JA doit être vide en
C     entrée.
C
C Entrée:
C     KLOCE
C     KLOCN
C     KNG
C     IA
C
C Sortie:
C     JA
C
C Notes:
C     Les préconditions sont relativement faibles car on n'a pas ici
C     les dimensions des tables IA et JA.
C************************************************************************
      FUNCTION SP_ELEM_INDILU0(KLOCE,
     &                         NDLN,
     &                         NNL,
     &                         KLOCN,
     &                         NNEL,
     &                         NCEL,
     &                         NELT,
     &                         KNG,
     &                         IA,
     &                         JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_INDILU0
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER KLOCE(*)
      INTEGER NDLN
      INTEGER NNL
      INTEGER KLOCN(NDLN,NNL)
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER KNG  (NCEL,NELT)
      INTEGER IA   (*)
      INTEGER JA   (*)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spmors.fi'

      INTEGER I, IN, ID
      INTEGER IE
      INTEGER IERR
      INTEGER JD
C-----------------------------------------------------------------
D     CALL ERR_PRE(IA(1) .GT. 0)
D     CALL ERR_PRE(JA(1) .EQ. 0)
C-----------------------------------------------------------------

      DO IE=1,NELT

C---        CONSTRUCTION DE LA TABLE KLOCE
         JD=0
         DO I=1,NNEL
            IN = KNG(I,IE)
            DO ID=1,NDLN
               JD = JD + 1
               KLOCE(JD) = KLOCN(ID,IN)
            ENDDO
         ENDDO

C---        CONSTRUCTION DE LA TABLE JA
         IERR = SP_MORS_ASMINDCRS(NDLN*NNEL, KLOCE, IA, JA)

      ENDDO

      SP_ELEM_INDILU0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: SP_ELEM_INDILUN
C
C Description:
C     La fonction SP_ELEM_INDILUN assemble les indices du triangle strictement
C     supérieur d'une matrice pour un stockage morse de type CCS.
C
C Entrée:
C     KLOCE
C     KLOCN
C     KNG
C     IA
C
C Sortie:
C     JA
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_INDILUN(KLOCE,
     &                         NDLN,
     &                         NNT,
     &                         KLOCN,
     &                         NNEL,
     &                         NCEL,
     &                         NELT,
     &                         KNG,
     &                         IA,
     &                         JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_INDILUN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER KLOCE(*)
      INTEGER NDLN
      INTEGER NNT
      INTEGER KLOCN(NDLN,NNT)
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER KNG  (NCEL,NELT)
      INTEGER IA   (*)
      INTEGER JA   (*)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spmors.fi'

      INTEGER I, IN, ID
      INTEGER IE
      INTEGER IERR
      INTEGER JD
C-----------------------------------------------------------------

      DO IE=1,NELT

C---       CONSTRUCTION DE LA TABLE KLOCE
         JD=0
         DO I=1,NNEL
            IN = KNG(I,IE)
            DO ID=1,NDLN
               JD = JD + 1
               KLOCE(JD) = KLOCN(ID,IN)
            ENDDO
         ENDDO

C---        CONSTRUCTION DE LA TABLE JA
         IERR = SP_MORS_ASMINDCCS(NDLN*NNEL, KLOCE, IA, JA)

      ENDDO

      SP_ELEM_INDILUN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : SP_ELEM_CTRCON
C
C Description:
C     La fonction SP_ELEM_CTRCON contrôle les connectivités. Elle recherche
C     les noeuds nuls ou les numéros dupliqués.
C
C Entrée:
C     NNEL     Nombre de Noeuds par ELement
C     NCEL     Nombre de Connectivités par ELement
C     NELT     Nombre d'ELements Total
C     KNG      Table de Konnectivités globale, KNG(NCEL, NELT)
C     LMAIN    .TRUE. s'il s'agit d'un maillage principal, pas d'un sous-maillage
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_CTRCON(NNEL, NCEL, NELT, KNG, LMAIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_CTRCON
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELT
      INTEGER KNG(NCEL, NELT)
      LOGICAL LMAIN

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      INTEGER IE
      INTEGER I, IN, INMIN, INMAX
      INTEGER J, JN
      INTEGER K, NERR
      LOGICAL, ALLOCATABLE :: IEXIST(:)
C-----------------------------------------------------------------
D     CALL ERR_ASR(NNEL .LE. NCEL)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Assigne une erreur par défaut qui sera effacée au besoin
      WRITE(ERR_BUF,'(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

C---     Détecte les sous-maillages
      IF (.NOT. LMAIN) GOTO 2000

C ---    Contrôle des numéros de noeuds
1000  CONTINUE
      INMIN = MINVAL(KNG)
      INMAX = MAXVAL(KNG)
      IF (INMIN .NE. 1) THEN
         WRITE(ERR_BUF,'(A,I12)') 'ERR_NOEUD_MIN_INVALIDE:', INMIN
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      IF (INMAX .LE. INMIN) THEN
         WRITE(ERR_BUF,'(A,I12)') 'ERR_NOEUD_MAX_INVALIDE:', INMAX
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      ALLOCATE(IEXIST(INMIN:INMAX), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      IEXIST(:) = .FALSE.

      DO IE=1,NELT
         IEXIST(KNG(:,IE)) = .TRUE.
      ENDDO

      NERR = 0
      DO IN=INMIN,INMAX
         IF (.NOT. IEXIST(IN)) THEN
            WRITE(ERR_BUF,'(A,I12)') 'ERR_NOEUD_INUTILISE:', IN
            IF     (NERR .EQ.  0) THEN
               CALL ERR_AJT(ERR_BUF)
            ELSEIF (NERR .LT. 10) THEN
               CALL ERR_AJT(ERR_BUF)
            ELSEIF (NERR .EQ. 11) THEN
               CALL ERR_AJT('...')
            ENDIF
            NERR = NERR + 1
         ENDIF
      ENDDO

      IF (NERR .GT. 0) THEN
         WRITE(ERR_BUF,'(A,I12)') 'ERR_NBR_NOEUDS_INVALIDES: ', NERR
         CALL ERR_AJT(ERR_BUF)
         IERR = ERR_ERR
      ENDIF

      IF (ALLOCATED(IEXIST)) DEALLOCATE(IEXIST)

C ---    Contrôle des éléments
2000  CONTINUE
      NERR = 0
      DO IE=1,NELT

         K = 0
         DO I=1,NNEL-1
            IN = KNG(I,IE)
            IF (IN .EQ. 0) K = K + 1      ! Noeud nul
            DO J=I+1,NNEL
               JN = KNG(J,IE)
               IF (IN .EQ. JN) K = K + 1  ! Noeud dédoublé
            ENDDO
         ENDDO

         IF (K .GT. 0) THEN
            WRITE(ERR_BUF,'(A,I12)') 'ERR_CONNECTIVITES_INVALIDES: ', IE
            IF     (NERR .LT. 10) THEN
               CALL ERR_AJT(ERR_BUF)
            ELSEIF (NERR .EQ. 11) THEN
               CALL ERR_AJT('...')
            ENDIF
            NERR = NERR + 1
         ENDIF

      ENDDO

      IF (NERR .GT. 0) THEN
         WRITE(ERR_BUF,'(A,I12)') 'ERR_NBR_CONNEC_INVALIDES: ', NERR
         CALL ERR_AJT(ERR_BUF)
         IERR = ERR_ERR
      ENDIF

C---     Reset l'erreur par défaut si tout est OK
      IF (IERR .EQ. ERR_OK) CALL ERR_RESET()

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_ELEM_CTRCON = ERR_TYP()
      END

C************************************************************************
C Sommaire : SP_ELEM_CTRSRF
C
C Description:
C     La fonction SP_ELEM_CTRSRF contrôle la surface du maillage. Elle recherche
C     les éléments de volume ayant plus d'une face sur la surface.
C
C Entrée:
C     NELV     Nombre d'ELements de Volume
C     NNELS    Nombre de Noeuds par ELement de Surface
C     NCELS    Nombre de Connectivités par ELement de Surface
C     NELS     Nombre d'ELements de Surface
C     KNGS     Table de Konnectivités globale de Surface
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_ELEM_CTRSRF(NELV, NNELS, NCELS, NELS, KNGS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_CTRSRF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: NELV
      INTEGER, INTENT(IN) :: NNELS
      INTEGER, INTENT(IN) :: NCELS
      INTEGER, INTENT(IN) :: NELS
      INTEGER, INTENT(IN) :: KNGS(NCELS, NELS)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      INTEGER IES, IEV
      INTEGER IKEV
      INTEGER NERR
      INTEGER, ALLOCATABLE :: NSRF(:)
C-----------------------------------------------------------------
D     CALL ERR_ASR(NNELS .LE. NCELS)
C-----------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle les éléments de surface
      IF (NELS .LT. 1) GOTO 9900

C---     Alloue la table de travail
      ALLOCATE(NSRF(1:NELV), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9901
      NSRF(:) = 0

C---     Assemble l'info
      IKEV = NNELS+1
      DO IES=1,NELS
         IEV = KNGS(IKEV, IES)
         NSRF(IEV) = NSRF(IEV) + 1
      ENDDO

C---     Les éléments à 3 côtés sur la surface
C        =====================================
      IF (ERR_GOOD()) THEN
      
C---        Assigne l'erreur qui sera effacée au besoin
         WRITE(ERR_BUF,'(A)') 'ERR_MAILLAGE_ELEM_3SRF'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)

C---        Les éléments à 3 côtés sur la surface
         NERR = 0
         DO IEV=1,NELV
            IF (NSRF(IEV) .LT. 3) CYCLE   ! Prend pour acquis des triangles

            NERR = NERR + 1
            IF (NERR .LT. 10) THEN
               WRITE(ERR_BUF,'(I12)') IEV
               CALL ERR_AJT(ERR_BUF)
            ELSEIF (NERR .EQ. 11) THEN
               CALL ERR_AJT('...')
            ENDIF
         ENDDO

         IF (NERR .GT. 0) THEN
            WRITE(ERR_BUF,'(A,I12)') 'ERR_NBR_ELEM_3SRF: ', NERR
            CALL ERR_AJT(ERR_BUF)
            CALL LOG_MSG_ERR()
         ELSE
            CALL ERR_RESET()
         ENDIF
      ENDIF

      
C---     Les éléments à 2 côtés sur la surface
C        =====================================
      IF (ERR_GOOD()) THEN
      
C---        Assigne l'erreur qui sera effacée au besoin
         WRITE(ERR_BUF,'(A)') 'ERR_MAILLAGE_ELEM_2SRF'
         CALL ERR_ASG(ERR_WRN, ERR_BUF)

C---        Les éléments à 2 côtés sur la surface
         NERR = 0
         DO IEV=1,NELV
            IF (NSRF(IEV) .NE. 2) CYCLE

            NERR = NERR + 1
            IF (NERR .LT. 10) THEN
               WRITE(ERR_BUF,'(I12)') IEV
               CALL ERR_AJT(ERR_BUF)
            ELSEIF (NERR .EQ. 11) THEN
               CALL ERR_AJT('...')
            ENDIF
         ENDDO

         IF (NERR .GT. 0) THEN
            WRITE(ERR_BUF,'(A,I12)') 'ERR_NBR_ELEM_2SRF: ', NERR
            CALL ERR_AJT(ERR_BUF)
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()     ! Nettoie, car c'est un Warning
         ELSE
            CALL ERR_RESET()
         ENDIF
      ENDIF
         
      IF (ALLOCATED(NSRF)) DEALLOCATE(NSRF)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A,I12)') 'ERR_NELS_INVALIDE:', NELS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_ELEM_CTRSRF = ERR_TYP()
      END

C**************************************************************
C Sommaire: SP_ELEM_EIGENV
C
C Description:
C     La fonction SP_ELEM_EIGENV(...) transforme le Hessien
C
C Entrée:
C
C Sortie:
C
C Notes:
C     http://www.math.harvard.edu/archive/21b_fall_04/exhibits/2dmatrices/index.html
C****************************************************************
      SUBROUTINE SP_ELEM_EIGENV(AA, EV)

      IMPLICIT NONE

      REAL*8  AA(2, 2)
      REAL*8  EV(2)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      REAL*8  A, B, C, D, TR, DT, TT
C-----------------------------------------------------------------------

         A = AA(1,1)
         C = AA(2,1)
         B = AA(1,2)
         D = AA(2,2)

         TR = A + D        ! TRACE
         DT = A*D - B*B    ! DETERMINANT
         TT = (0.25D0*TR*TR - DT)
         IF (TT .GE. 0.0D0) THEN
            EV(1) = 0.5D0*TR - SQRT(TT)
            EV(2) = 0.5D0*TR + SQRT(TT)
         ELSE
            EV(1) = 1.0D-16
            EV(2) = 1.0D-16
         ENDIF
         IF (C .NE. 0.0D0) THEN
            AA(1,1) = EV(1) - D
            AA(2,1) = C
            AA(1,2) = EV(2) - D
            AA(2,2) = C
         ELSEIF (B .NE. 0.0D0) THEN
            AA(1,1) = B
            AA(2,1) = EV(1) - A
            AA(1,2) = B
            AA(2,2) = EV(2) - A
         ELSE
            AA(1,1) = 1.0D0
            AA(2,1) = 0.0D0
            AA(1,2) = 1.0D0
            AA(2,2) = 0.0D0
         ENDIF

      RETURN
      END

C**************************************************************
C Sommaire: SP_ELEM_HESSELLPS
C
C Description:
C     La fonction SP_ELEM_HESSELLPS(...) transforme le Hessien
C     (matrice de 2. dérivée) en une ellipse d'erreur. Le Hessien
C     est rendu elliptique en prenant la valeur absolue de ses
C     valeurs propres.
C     En sortie, on retourne:
C        VHESS(1,.)     Le grand axe (rayon)
C        VHESS(2,.)     Le petit axe (rayon)
C        VHESS(3,.)     L'inclinaison (radian)
C
C Entrée:
C      NCOL             Nombre de colonnes de la table VHESS
C      NNT              Nombre de lignes de la table VHESS
C      VHESS(NCOL, NNT)
C
C Sortie:
C
C Notes:
C****************************************************************
      SUBROUTINE SP_ELEM_HESSELLPS(NCOL, NNT, VHESS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_HESSELLPS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCOL
      INTEGER NNT
      REAL*8  VHESS(NCOL, NNT)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      Real*8, PARAMETER :: AniRatio      = 1.0D+12
      Real*8, PARAMETER :: MinEigenvalue = 1.0D-12

      REAL*8  A(2, 2)
      REAL*8  E(2)
      INTEGER IN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NCOL .GE. 3)
C-----------------------------------------------------------------------

      DO IN = 1, NNT
         A(1,1) = VHESS(1,IN)   ! F,xx
         A(2,1) = VHESS(2,IN)   ! F,yx
         A(1,2) = VHESS(2,IN)   ! F,xy
         A(2,2) = VHESS(3,IN)   ! F,yy

         CALL SP_ELEM_EIGENV(A, E)

         E(1) = ABS(E(1))
         E(2) = ABS(E(2))
         IF (E(1) .GT. E(2)) THEN
            CALL DSWAP(2, A(1,1), 1, A(1,2), 1)
            CALL DSWAP(1, E(1), 1, E(2), 1)
         ENDIF
         IF (A(1,1) .LT. 0.0D0) THEN
            A(1,1) = - A(1,1)
            A(2,1) = - A(2,1)
         ENDIF
         E(1) = MAX(E(1), MinEigenvalue)
         E(2) = MIN(E(2), E(1)*AniRatio)
         E(2) = MAX(E(2), MinEigenvalue)

         VHESS(1,IN) = 1.0D0 / SQRT(E(1))
         VHESS(2,IN) = 1.0D0 / SQRT(E(2))
         VHESS(3,IN) = ATAN2(A(2,1), A(1,1))
      ENDDO

      RETURN
      END

C**************************************************************
C Sommaire: Tri des éléments par 'couleur'
C
C Description:
C     La fonction SP_ELEM_COLO(...) trie les éléments par couleur.
C     En fait elle retourne la table de renumérotation ainsi que
C     les éléments de chaque couleur dans cette numérotation.
C
C Entrée:
C     NNT         Le nombre de noeuds total
C     NNEL        Le nombre de noeuds par élément
C     NELT        Le nombre d'éléments total
C     NCOLMAX     Le nombre max de couleurs
C     KNG         Les connectivités: KNG(NNEL,NELT)
C     KTRV        Une table de travail: KTRV(NNT)
C
C Sortie:
C     NELCOL      Le nombre de couleurs
C     KELC        La table de renumérotation KELC(NELT)
C     KELCOL      Les couleurs
C
C Notes:
C****************************************************************
      FUNCTION SP_ELEM_COLO(NNT,
     &                      NNEL,
     &                      NELT,
     &                      NCOLMAX,
     &                      KNG,
     &                      KTRV,
     &                      NELCOL,
     &                      KELC,
     &                      KELCOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_COLO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NNT
      INTEGER NNEL
      INTEGER NELT
      INTEGER NCOLMAX
      INTEGER KNG   (NNEL, NELT)
      INTEGER KTRV  (NNT)
      INTEGER NELCOL
      INTEGER KELC  (NELT)
      INTEGER KELCOL(2, NCOLMAX)

      INCLUDE 'spelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ICOL
      INTEGER NELBG, NELNW
      INTEGER IC, IE, IN, NNO, I
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.renum.color'

C---     Imprime les valeurs
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_COLORING:'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NNT#<25>#= ', NNT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NNEL#<25>#= ', NNEL
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NELT#<25>#= ', NELT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NCOLMAX#<25>#= ', NCOLMAX
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Initialise
      CALL IINIT(NELT,      0, KELC,  1)
      CALL IINIT(2*NCOLMAX, 0, KELCOL,1)
      NELCOL= 0
      NELNW = 0

C---     Boucle sur les couleurs
      DO IC=1,NCOLMAX
         CALL IINIT(NNT, 0, KTRV, 1)
         NELBG = NELNW

C---        Boucle sur éléments non marqués
         DO IE=1,NELT
            IF (KELC(IE) .NE. 0) GOTO 199

C---           Compte le nombre d'apparitions des noeuds
            ICOL = 0
            DO IN=1,NNEL
D              CALL ERR_ASR(KNG(IN,IE) .GT. 0)
D              CALL ERR_ASR(KNG(IN,IE) .LE. NNT)
               ICOL = ICOL + KTRV(KNG(IN,IE))
            ENDDO
C---           Si <> 0 ==> prochain élément
            IF (ICOL .NE. 0) GOTO 199

C---           Flag les noeuds de l'élément
            DO IN=1,NNEL
               KTRV(KNG(IN,IE)) = 1
            ENDDO

C---           Ajuste la table de renumérotation
            NELNW = NELNW + 1
            KELC(IE) = NELNW

199         CONTINUE
         ENDDO

         WRITE(LOG_BUF,'(A,2I12)') 'NELNW#<25>#= ', IC, NELNW
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

         IF (NELNW .LE. NELBG) GOTO 299
         NELCOL = NELCOL + 1
         KELCOL(1,NELCOL) = NELBG + 1
         KELCOL(2,NELCOL) = NELNW
      ENDDO
299   CONTINUE

C---     Imprime les valeurs
      WRITE(LOG_BUF, '(A,I6)') 'MSG_NBR_COLOR#<25>#=', NELCOL
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DBG(LOG_ZNE, 'MSG_ELEM_OF_COLOR')
      CALL LOG_INCIND()
      DO IC=1,NELCOL
         WRITE(LOG_BUF,'(I3,A,2I12)') IC,':',KELCOL(1,IC),KELCOL(2,IC)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

C---     Contrôles
      IF (NELCOL .GE. NCOLMAX .AND. NELNW .LT. NELT) GOTO 9900
      IF (NELNW  .NE. NELT) GOTO 9901

      GOTO 9999
C-----------------------------------------------------------------------
9900  CONTINUE
      WRITE(ERR_BUF,'(A)') 'ERR_PALETTE_COULEUR_EPUISEE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  CONTINUE
      WRITE(ERR_BUF,'(A)') 'ERR_DIVERGENCE_COLORIAGE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I9,A,I9)') 'NELNW <> NELT', NELNW, ' / ', NELT
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL LOG_DECIND()
      SP_ELEM_COLO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Divise les noeuds en blocs consécutifs.
C
C Description:
C     La sous-routine SP_ELEM_BLCS divise les noeuds en blocs consécutifs.
C     Elle monte la liste de tous les éléments adhérents aux noeuds de
C     chaque bloc. Les noeuds et les éléments conservent leur ordre. Il y a
C     possiblement répétition d'éléments entre les blocs et un élément n'a pas
C     toujours tous ses noeuds dans un seul bloc.
C     En sortie la table KBLBLC contient les indices des noeuds debut/fin,
C     éléments début/fin de chaque bloc. Les indices des noeuds sont les
C     indices naturels, alors que les indices des éléments sont par rapport
C     à la table KBLELE.
C     Les blocs non utilisés sont marqués par des -1.
C     En sortie, la dimension necéssaire de KBLELE est données par
C     KBLBLC(4,NBLEF)
C
C Entrée:
C     NNL         Nombre de Noeuds Locaux
C     NCELV       Nombre de Connectivités par Élément de Volume
C     NELV        Nombre d'Élement de Volume
C     KNGV        Table de KonnectiVité Globale de Volume
C     KTRV        Table de Travail de dimension NNL
C     NBLCS       Nombre de blocs
C     NELMAX      Nombre max d'éléments dim_2 de KBLELE
C
C Sortie:
C     KBLBLC      Indices (noeuds, éléments) des blocs
C     KBLELE      Table des éléments par bloc
C     NBLEF       Nombre de bloc effectif
C
C Notes:
C
C************************************************************************
      FUNCTION SP_ELEM_BLCS (NNL,
     &                       NCELV,
     &                       NELV,
     &                       KNGV,
     &                       NBLCS,
     &                       NELMAX,
     &                       KBLBLC,
     &                       KBLELE,
     &                       NBLEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_ELEM_BLCS
CDEC$ ENDIF

      USE SP_BLCS_M
      IMPLICIT NONE

      INTEGER NNL
      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV   (NCELV, NELV)
      INTEGER NBLCS
      INTEGER NELMAX
      INTEGER KBLBLC (4, NBLCS)
      INTEGER KBLELE (NELMAX)
      INTEGER NBLAL

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spelem.fi'

      REAL*8  NNTGT, SMOY, SDEV
      INTEGER IB, IT
      INTEGER IERR
      INTEGER IBMIN, IBMAX
      INTEGER INDEB
      INTEGER IEFIN
      INTEGER NBLEF, NBLMX, NBLAC
      INTEGER NETOL, NEDIF
      INTEGER SSUM, SINF, SSUP, STGT, ITGT, L1, L2, DL, IDIF
      INTEGER, ALLOCATABLE :: KIDX(:)
      INTEGER, ALLOCATABLE :: KINV(:)
      INTEGER, ALLOCATABLE :: KTRV(:)

      INTEGER, PARAMETER :: NITER_MAX = 15
C-----------------------------------------------------------------------
      INTEGER LOAD, NNOD
      NNOD(IB) = KBLBLC(2, IB) - KBLBLC(1, IB) + 1
      LOAD(IB) = KBLBLC(4, IB) - KBLBLC(3, IB) + 1
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.renum.blocs'

C---     Initialise
      CALL IINIT(NELMAX, 0, KBLELE, 1)

C---     Construis les connectivités inverses
      IERR = SP_BLCS_KINV (NNL, NCELV, NELV, KNGV, KIDX, KINV)

C---     Alloue la table de travail
      ALLOCATE(KTRV(NELMAX), STAT=IERR)

C---     Entête
      NBLEF = MIN(NNL, NBLCS)
      CALL LOG_DBG(LOG_ZNE, 'SP_ELEM_BLCS: Load balancing')
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(6A6,2A14)') 'IMIN', 'IMAX',
     &                             'NMIN', 'NMAX',
     &                             'LMIN', 'LMAX',
     &                             'LMED', 'LSTD'
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     Répartition initiale
      NNTGT = DBLE(NNL) / DBLE(NBLEF)
      KBLBLC(1, 1) =  1
      KBLBLC(2, 1) =  MAX(KBLBLC(1, 1), NINT(NNTGT))
      DO IB=2,NBLEF
         KBLBLC(1, IB) = KBLBLC(2, IB-1) + 1
         KBLBLC(2, IB) = MAX(KBLBLC(1,IB), NINT(IB*NNTGT))
      ENDDO
      KBLBLC(2, NBLEF) = NNL

C---     Alloue la table de travail
      SSUM = NINT(NNTGT * 6 * 2)       ! Pirate SSUM: 6 ele/noeud
      ALLOCATE(KTRV(SSUM), STAT=IERR)  ! facteur 2 de securité

C---     Charge initiale
      IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                     NBLEF, -1, KBLBLC, KBLELE,
     &                     KIDX, KINV, KTRV)
      SSUM = 0
      DO IB=1,NBLEF
         SSUM = SSUM + LOAD(IB)
      ENDDO
C---     Statistiques
      IERR = SP_BLCS_STATTS(NBLEF, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)
C---     Log
      IERR = SP_BLCS_LOG   (NBLEF, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)

C---     Balance
      SINF = -2
      SSUP = -1
      NBLMX = -1
      DO IT=1, NITER_MAX
         SMOY = DBLE(SSUM)/DBLE(NBLEF)

C---        Charge en visant la moyenne
         INDEB = 1
         STGT = 0
         DO IB=1, NBLEF
            IDIF = STGT - NINT((IB-1)*SMOY)           ! Over-under shoot
            ITGT = NINT(IB*SMOY + IDIF*0.5D0) - STGT  ! Correct half overshoot

C---           Commence avec 1 noeud
            KBLBLC(1, IB) = INDEB
            KBLBLC(2, IB) = KBLBLC(1, IB)
            IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                           1, -1, KBLBLC(1,IB), KBLELE,
     &                           KIDX, KINV, KTRV)
C---           Augmente progressivement
            L2 = LOAD(IB)
            L1 = 0
            DL = ABS(L1 - ITGT) - ABS(L2 - ITGT)
            DO WHILE ((DL .GE. 0) .AND. (KBLBLC(2,IB) .LE. NNL))
               KBLBLC(2, IB) = KBLBLC(2, IB) + 1
               IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                              1, -1, KBLBLC(1,IB), KBLELE,
     &                              KIDX, KINV, KTRV)
               L1 = L2
               L2 = LOAD(IB)
               DL = ABS(L1 - ITGT) - ABS(L2 - ITGT)
            ENDDO
C---           On a fini 1 trop loin ==> retour en arrière
            IF (KBLBLC(2,IB) .LE. NNL) THEN
               IF (KBLBLC(2, IB) .GT. KBLBLC(1, IB)) THEN
                  INDEB = KBLBLC(2, IB)
                  KBLBLC(2, IB) = KBLBLC(2, IB) - 1
               ENDIF
               IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                              1, -1, KBLBLC(1,IB), KBLELE,
     &                              KIDX, KINV, KTRV)
               STGT = STGT + LOAD(IB)
            ENDIF

            IF (KBLBLC(2,IB) .GE. NNL) GOTO 109
         ENDDO
109      CONTINUE

C---     Statistiques
         NBLAC = MIN(IB,NBLEF)
         IERR = SP_BLCS_STATTS(NBLAC, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)
C---        Log
         IERR = SP_BLCS_LOG   (NBLAC, KBLBLC, IBMIN, IBMAX, SMOY, SDEV)

C---        Test de sortie
         IF ((IB .GT. NBLEF) .AND. (KBLBLC(2,NBLEF) .EQ. NNL)) GOTO 119
         IF ((IB .LE. NBLEF) .AND. (IB .LT. NBLMX)) GOTO 119
         IF (SINF .EQ. SSUP) GOTO 119

C---        Corrige la charge totale
         IF (IB .GT. NBLEF) THEN    ! Somme trop petit, pas assez de noeuds
D           CALL ERR_ASR(KBLBLC(2,NBLEF) .LT. NNL)
            IF (SSUP .GT. 0) THEN
               SINF = SSUM
               SSUM = NINT((SSUP + SINF) * 0.5D0)
            ELSE
               IB = 1
               KBLBLC(1,IB) = KBLBLC(2,NBLEF)-1
               KBLBLC(2,IB) = NNL
               IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                              1, -1, KBLBLC(1,IB), KBLELE,
     &                              KIDX, KINV, KTRV)
               SINF = SSUM
               SSUM = SSUM + LOAD(IB)
            ENDIF
         ELSE                       ! Somme trop grande, blocs non chargés
            NBLMX = MAX(NBLMX, IB)
            IF (SINF .GT. 0) THEN
               SSUP = SSUM
               SSUM = NINT((SSUP + SINF) * 0.5D0)
            ELSE
               SSUP = SSUM
               SSUM = SSUM - (NBLEF-IB)*SMOY*2
            ENDIF
         ENDIF

      ENDDO
119   CONTINUE
      CALL LOG_DECIND()

C---     Nouvelles connectivités
      IERR = SP_BLCS_BLOAD(NCELV, NELV, KNGV,
     &                     NBLEF, NELMAX, KBLBLC, KBLELE,
     &                     KIDX, KINV, KTRV)

C---     Complète pour les blocs non utilisés
      DO IB=NBLEF+1, NBLCS
         KBLBLC(:, IB) = -1
      ENDDO

C---     Log la répartition
      CALL LOG_DBG(LOG_ZNE, 'SP_ELEM_BLCS: Load per bloc')
      DO IB=1,NBLEF
         WRITE(LOG_BUF,*) IB,
     &                    KBLBLC(2,IB)-KBLBLC(1,IB)+1,
     &                    KBLBLC(4,IB)-KBLBLC(3,IB)+1
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

C---     Contrôle les dimensions
      write(6,*) 'Nelv   ', NELV
      write(6,*) 'Nelmax ', NELMAX
      write(6,*) 'Nelblcs', KBLBLC(4,NBLEF)
      IF (KBLBLC(4,NBLEF) .GT. NELMAX) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ARRAY_UNDERSIZED'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(KTRV)) DEALLOCATE(KTRV)
      IF (ALLOCATED(KINV)) DEALLOCATE(KINV)
      IF (ALLOCATED(KIDX)) DEALLOCATE(KIDX)
      SP_ELEM_BLCS = ERR_TYP()
      RETURN
      END


