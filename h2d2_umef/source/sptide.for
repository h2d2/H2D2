C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Sous-routine de calcul de marée
C
C Functions:
C   Public:
C     INTEGER SP_TIDE_CFGTTD
C     INTEGER SP_TIDE_READTTD
C     INTEGER SP_TIDE_READFIC
C     INTEGER SP_TIDE_ADDCMP
C     INTEGER SP_TIDE_ADDCMP2
C     LOGICAL SP_TIDE_CMPEXIST
C     REAL*8 SP_TIDE_TTDE_HD
C     REAL*8 SP_TIDE_NOAA_HD
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     Le block data SV2D_CL_DATA_000 initialise les tables de la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA SP_TIDE_DATA_000

      IMPLICIT NONE

      INCLUDE 'sptide.fc'

      DATA SP_TIDE_XCMP /0/
      DATA SP_TIDE_BASE /'K1','K2','L2','M2','N2','O1','P1','S2'/

      END

C************************************************************************
C Sommaire: Configure à partir du fichier de t_tide dans etc
C
C Description:
C     La fonction SP_TIDE_CFGTTD configure les composantes à partir
C     du fichier de configuration de t_tide du répertoire etc.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_CFGTTD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_CFGTTD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'sptide.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET

      CHARACTER*(256) REPEXE
      CHARACTER*(256) NOMPTH
C-----------------------------------------------------------------------

      IRET = 0
      IERR = ERR_OK

      IF (IRET .EQ. 0) IRET = C_OS_REPEXE(REPEXE)
      IF (IRET .EQ. 0) NOMPTH = REPEXE(1:SP_STRN_LEN(REPEXE)) //
     &                          '/../etc/' //
     &                          'tide3.dat'
      IF (IRET .EQ. 0) IRET = C_OS_NORMPATH(NOMPTH)

      IERR = SP_TIDE_READTTD(NOMPTH(1:SP_STRN_LEN(NOMPTH)))

      SP_TIDE_CFGTTD = ERR_TYP()
      END

C************************************************************************
C Sommaire: Ajoute un fichier de composantes t_tide.
C
C Description:
C     La fonction SP_TIDE_READTTD ajoute un fichier de composantes
C     au format de t_tide. Il lis les composantes et les coefficients
C     de facteurs nodaux.
C
C Entrée:
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_READTTD(NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_READTTD
CDEC$ ENDIF

      USE SP_SP98_M
      IMPLICIT NONE

      CHARACTER*(*) NOMFIC

      INCLUDE 'sptide.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sptide.fc'

      REAL*8 RVAL
      REAL*8 C(8)
      INTEGER IERR, IRET
      INTEGER IC, IT, IB
      INTEGER ICNT
      INTEGER LLEN
      INTEGER MR

      CHARACTER*(256) LIGNE
      CHARACTER*(16)  SVAL, SCMP
      CHARACTER*(16)  DVAL
      CHARACTER*(16)  HVAL
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Ouvre le fichier
      MR = IO_UTIL_FREEUNIT()
      OPEN(UNIT  = MR,
     &     FILE  = NOMFIC,
     &     STATUS= 'OLD',
     &     ERR   = 9900)

C---     Boucle sur le 1er bloc
100   CONTINUE
         READ(MR,'(A)', ERR = 9901, END = 9902) LIGNE
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 3) GOTO 199

         CALL SP_STRN_DMC(LIGNE(1:LLEN), ' ')
         LLEN = SP_STRN_LEN (LIGNE)
         IF (SP_STRN_NTOK(LIGNE(1:LLEN), ' ') .LT. 2) GOTO 9903

         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(LIGNE, ' ', 1, SCMP)
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(LIGNE, ' ', 2, RVAL)
         IF (IRET .NE. 0) GOTO 9904

         IERR = SP_TIDE_ADDCMP(SCMP(1:SP_STRN_LEN(SCMP)), RVAL)
      IF (ERR_GOOD()) GOTO 100
199   CONTINUE

C---     Saute le 2nd bloc
200   CONTINUE
         READ(MR,'(A)', ERR = 9901, END = 9902) LIGNE
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 3) GOTO 299
      IF (ERR_GOOD()) GOTO 200
299   CONTINUE

C---     Boucle sur le 3ème bloc
300   CONTINUE
         READ(MR,'(A)', ERR = 9901, END = 399) LIGNE
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 3) GOTO 399

         CALL SP_STRN_DMC(LIGNE(1:LLEN), ' ')
         LLEN = SP_STRN_LEN (LIGNE)
         IF (SP_STRN_NTOK(LIGNE(1:LLEN), ' ') .LT. 1) GOTO 9903

         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(LIGNE, ' ', 1, SCMP)
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(LIGNE, ' ', 2, ICNT)
         IF (IRET .NE. 0) GOTO 9904

         IT = 1
         C = 0.0D0
         DO IC=1,ICNT
            IT = IT + 2
            IF (IRET .EQ. 0) IRET = SP_STRN_TKR(LIGNE, ' ', IT+0, RVAL)
            IF (IRET .EQ. 0) IRET = SP_STRN_TKS(LIGNE, ' ', IT+1, SVAL)
            IF (IRET .NE. 0) GOTO 9904

            DO IB=1,SP_TIDE_NBASE
               IF (SVAL(1:2) .EQ. SP_TIDE_BASE(IB))
     &            C(IB) = C(IB) + RVAL
            ENDDO
         ENDDO

         IERR = SP_TIDE_ADDCMP2(SCMP(1:SP_STRN_LEN(SCMP)), C)
      IF (ERR_GOOD()) GOTO 300
399   CONTINUE

C---     Ajoute les composantes de base
      DO IB=1,SP_TIDE_NBASE
         C = 0.0D0
         C(IB) = 1.0D0
         IERR = SP_TIDE_ADDCMP2(SP_TIDE_BASE(IB), C)
      ENDDO

      GOTO 9988
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(NOMFIC(1:SP_STRN_LEN(NOMFIC)))
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(NOMFIC(1:SP_STRN_LEN(NOMFIC)))
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(LIGNE(1:SP_STRN_LEN(LIGNE)))
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_LIGNE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(LIGNE(1:SP_STRN_LEN(LIGNE)))
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_VALEURS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(LIGNE(1:SP_STRN_LEN(LIGNE)))
      GOTO 9988

9988  CONTINUE
      CLOSE(MR)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_READTTD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un fichier de composantes
C
C Description:
C     La fonction SP_TIDE_READFIC ajoute un fichier de composantes
C     harmoniques de marée. Pour chaque ligne valide il enregistre la
C     composante. L'extrait de fichier explique le format:
C
C     #---------------------------------------------
C     #  Name     Frequency     Reference time
C     #---------------------------------------------
C        Z0       0.0000000000
C        M2       0.0805114006
C
C Entrée:
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_READFIC(NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_READFIC
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOMFIC

      INCLUDE 'sptide.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sptide.fc'

      REAL*8 RVAL
      INTEGER IERR, IRET
      INTEGER LLEN
      INTEGER MR

      CHARACTER*(256) LIGNE
      CHARACTER*(16)  SVAL
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SP_STRN_LEN(NOMFIC) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Ouvre le fichier
      MR = IO_UTIL_FREEUNIT()
      OPEN(UNIT  = MR,
     &     FILE  = NOMFIC,
     &     STATUS= 'OLD',
     &     ERR   = 9900)

C---     Boucle sur les lignes
100   CONTINUE
         READ(MR,'(A)', ERR = 9901, END = 199) LIGNE
         CALL SP_STRN_TRM(LIGNE)
         LLEN = SP_STRN_LEN (LIGNE)
         IF (LLEN .LE. 3) GOTO 100
         IF (LIGNE(1:1) .EQ. '#') GOTO 100
         IF (LIGNE(1:1) .EQ. '!') GOTO 100

         CALL SP_STRN_DMC(LIGNE(1:LLEN), ' ')
         LLEN = SP_STRN_LEN (LIGNE)
         IF (SP_STRN_NTOK(LIGNE(1:LLEN), ' ') .LT. 4) GOTO 9902

         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(LIGNE, ' ', 1, SVAL)
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(LIGNE, ' ', 2, RVAL)
         IF (IRET .NE. 0) GOTO 9903

         IERR = SP_TIDE_ADDCMP(SVAL(1:SP_STRN_LEN(SVAL)), RVAL)
      IF (ERR_GOOD()) GOTO 100
199   CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(NOMFIC(1:SP_STRN_LEN(NOMFIC)))
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(NOMFIC(1:SP_STRN_LEN(NOMFIC)))
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_LIGNE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(LIGNE(1:SP_STRN_LEN(LIGNE)))
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_VALEURS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT(LIGNE(1:SP_STRN_LEN(LIGNE)))
      GOTO 9988

9988  CONTINUE
      CLOSE(MR)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_READFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une composante de marée.
C
C Description:
C     La fonction SP_TIDE_ADDCMP ajoute une composante harmonique de marée.
C     La composante est identifiée par son code et par sa fréquence.
C
C Entrée:
C     COD      Nom de la composante i.e. 'M2'
C     S        Fréquence en cycles/heures (inverse de la période en h)
C
C Sortie:
C
C Notes:
C     Les fréquences stockées sont pour des calculs en deg
C************************************************************************
      FUNCTION SP_TIDE_ADDCMP(COD, S)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_ADDCMP
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) COD
      REAL*8        S

      INCLUDE 'sptide.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sptide.fc'

      REAL*8  RRHS
      INTEGER ILHS
      INTEGER IRET
      CHARACTER*16 SLHS
      CHARACTER*64 SRHS
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.tide'

C---     Crée le map
      IF (SP_TIDE_XCMP .EQ. 0) THEN
         SP_TIDE_XCMP = C_MAP_CTR()
      ENDIF
D     CALL ERR_ASR(SP_TIDE_XCMP .NE. 0)

C---     Ajoute l'entrée dans le map
      WRITE(LOG_BUF, '(A,A6,A,1PE14.6E3)')
     &   'Registering tide component ', COD, ' with frequency f = ', S
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      IRET = C_ST_CRC32(COD, ILHS)
      IF (IRET .NE. 0) GOTO 9900
      WRITE(SLHS, '(I16)') ILHS
      RRHS = 0.1D0 * S        ! 0.1 = 360 deg / 3600 s
      WRITE(SRHS, '(A8, E26.17E3)') COD, RRHS
      IRET = C_MAP_ASGVAL(SP_TIDE_XCMP, SLHS, SRHS)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ', COD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_ADDCMP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute les coefs à une composante de marée.
C
C Description:
C     La fonction SP_TIDE_ADDCMP2 ajoute les coefficients multiplicateur
C     des composantes de base dans le calcul de (v, u, f).
C
C Entrée:
C     COD      Identifiant
C     C        Coefficients
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_ADDCMP2(COD, C)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_ADDCMP2
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) COD
      REAL*8        C(8)

      INCLUDE 'sptide.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'sptide.fc'

      REAL*8  P
      INTEGER ICRC, I
      INTEGER IRET
      CHARACTER*( 16) SLHS
      CHARACTER*(128) SRHS
      CHARACTER*(  8) DUM
C-----------------------------------------------------------------------
C     CALL ERR_PRE(SP_TIDE_XCMP .NE. 0)
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.element.sv2d.tide'

C---     CRC32 du nom
      IRET = C_ST_CRC32(COD, ICRC)
      IF (IRET .NE. 0) GOTO 9900

C---     Lis l'entrée dans le map
      WRITE(SLHS, '(I16)', ERR=9900) ICRC
      IRET = C_MAP_REQVAL(SP_TIDE_XCMP, SLHS, SRHS)
      IF (IRET .NE. 0) GOTO 9901
      READ(SRHS, *, ERR=9900) DUM, P

C---     Modifie l'entrée dans le map
      WRITE(SRHS, '(A8,E26.17E3,8(F5.1))') COD, P, C
      IRET = C_MAP_ASGVAL(SP_TIDE_XCMP, SLHS, SRHS)
      IF (IRET .NE. 0) GOTO 9902

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DECODING_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_UNKNOWN_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_CLEF_INVALIDE', ' : ', COD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_ADDCMP2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Teste la présence d'une composante
C
C Description:
C     La fonction <code>SP_TIDE_CMPEXIST</code> retourne TRUE si
C     la composante de CRC32 ICRC est enregistrée.
C
C Entrée:
C     ICRC  CRC32 du nom de la composante
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_CMPEXIST(ICRC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_CMPEXIST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER ICRC

      INCLUDE 'sptide.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sptide.fc'

      INTEGER IRET
      CHARACTER*(128) SRHS
      CHARACTER*( 16) SLHS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_TIDE_XCMP .NE. 0)
C-----------------------------------------------------------------------

      IRET = 1
      WRITE(SLHS, '(I16)', ERR=9999) ICRC
      IRET = C_MAP_REQVAL(SP_TIDE_XCMP, SLHS, SRHS)

9999  CONTINUE
      SP_TIDE_CMPEXIST = (IRET .EQ. 0)
      RETURN
      END

C************************************************************************
C Sommaire: Niveau de la marée, avec déphasage en degrés
C
C Description:
C     La fonction <code>SP_TIDE_TTDE_HD</code> retourne le niveau d'eau H
C     au temps T pour la composante de CRC32 ICRC avec une amplitude
C     de A et un déphasage (en degrés) de D.
C
C Entrée:
C     ICRC  CRC32 du nom de la composante
C     A     Amplitude en m
C     D     Déphasage en deg
C     T     Temps en s au format H2D2 (Unix epoch)
C     T0    Temps de référence au format H2D2 (Unix epoch)
C     DONODCOR .TRUE. s'il faut appliquer les corrections nodales
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_TTDE_HD(ICRC, A, D, T, T0, DONODCOR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_TTDE_HD
CDEC$ ENDIF

      USE SP_SP98_M
      USE TRIGD_M
      IMPLICIT NONE

      INTEGER ICRC
      REAL*8  A, D, T, T0
      LOGICAL DONODCOR

      INCLUDE 'sptide.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sptide.fc'

      REAL*8  G, P, H
      REAL*8  F, E, Df, DE
      REAL*8  C(8)
      INTEGER IRET
      CHARACTER*(128) SRHS
      CHARACTER*( 16) SLHS
      CHARACTER*(  8) COD

      INTEGER IB
      REAL*8, dimension(6) :: V_fact, V_args
      REAL*8, dimension(7) :: U_fact, U_args
D     REAL*8 V, U
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_TIDE_XCMP .NE. 0)
C-----------------------------------------------------------------------

C---     Initialize
      H = 0.0D0
      E = 0.0D0
      F = 1.0D0

C---     Read component data
      WRITE(SLHS, '(I16)', ERR=9900) ICRC
      IRET = C_MAP_REQVAL(SP_TIDE_XCMP, SLHS, SRHS)
      IF (IRET .NE. 0) GOTO 9901
      READ(SRHS, *, ERR=9900) COD, P, C

C---     Compute nodal correction
      IF (.TRUE.) THEN
         V_fact = 0
         U_fact = 0
         DO IB=1,SP_TIDE_NBASE
            V_fact = V_fact + C(IB)*SP_SP98_VCOEF( SP_TIDE_BASE(IB) )
            U_fact = U_fact + C(IB)*SP_SP98_UCOEF( SP_TIDE_BASE(IB) )
         ENDDO

         U_args = 0.0D0
         V_args = 0.0D0
         IF (DONODCOR) V_args = SP_SP98_VARGS(T0)
         IF (DONODCOR) U_args = SP_SP98_UARGS(T0)

         E = DOT_PRODUCT(V_fact,V_args) + DOT_PRODUCT(U_fact,U_args)
D        V = NORM( DOT_PRODUCT(V_fact,V_args) )
D        U = NORM( DOT_PRODUCT(U_fact,U_args) )
D        E = NORM(E)
         IF (DONODCOR) F = SP_SP98_FVAL( SP_SP98_FEQU( TRIM(COD) ), T0)
      ENDIF

C---     Component tide level
      G = P*(T-T0) - D + E
      H = F*A*COSD(G)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DECODING_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_UNKNOWN_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_TTDE_HD = H
      RETURN
      END

C************************************************************************
C Sommaire: Niveau de la marée, avec déphasage en degrés
C
C Description:
C     La fonction <code>SP_TIDE_HD</code> retourne le niveau d'eau H
C     au temps T pour la composante de CRC32 ICRC avec une amplitude
C     de A et un déphasage (en degrés) de D.
C
C Entrée:
C     ICRC  CRC32 du nom de la composante
C     A     Amplitude en m
C     D     Déphasage en deg
C     T     Temps en s au format H2D2 (Unix epoch)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_TIDE_NOAA_HD(ICRC, A, D, T, T0, DONODCOR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: SP_TIDE_NOAA_HD
CDEC$ ENDIF

      USE TRIGD_M
      IMPLICIT NONE

      INTEGER ICRC
      REAL*8  A, D, T
      LOGICAL DONODCOR

      INCLUDE 'sptide.fi'
      INCLUDE 'c_ds.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'sptide.fc'

      REAL*8 DEGRAD

      REAL*8  G, P, H, T0
      REAL*8  F, E
      INTEGER IRET
      CHARACTER*(64) SRHS
      CHARACTER*(16) SLHS
      CHARACTER*( 8) COD

      INTEGER TIDE_NOAA_INI
      INTEGER TIDE_FACT_GET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_TIDE_XCMP .NE. 0)
C-----------------------------------------------------------------------

      H = 0.0D0

      WRITE(SLHS, '(I16)', ERR=9900) ICRC
      IRET = C_MAP_REQVAL(SP_TIDE_XCMP, SLHS, SRHS)
      IF (IRET .NE. 0) GOTO 9901
      READ(SRHS, *, ERR=9900) COD, P

!!      IRET = TIDE_NOAA_INI(T0, T, DONODCOR)
!!      IRET = TIDE_FACT_GET(IC, F, E)

      G = P*(T-T0) - D + E
      H = F*A*COSD(G)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DECODING_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_UNKNOWN_TIDE_COMPONENT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      SP_TIDE_NOAA_HD = H
      RETURN
      END
