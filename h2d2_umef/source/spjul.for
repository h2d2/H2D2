C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Notes:
C     https://fr.wikipedia.org/wiki/Jour_julien   
C     Le jour julien est la base d'un système de datation consistant
C     à compter le nombre de jours et fraction de jour écoulés depuis
C     une date conventionnelle fixée au 1er janvier -4712 à 12 heures.
C************************************************************************

      MODULE SP_JUL_M
      
      IMPLICIT NONE
      PUBLIC

      TYPE :: SP_JUL_T
         REAL*8 :: DJUL
      CONTAINS
         ! ---  Public methods
         PROCEDURE, PUBLIC :: DATE  => SP_JUL_JU2DT      ! Date & components
         PROCEDURE, PUBLIC :: CNTRY => SP_JUL_JU2C       ! Centuries 
         PROCEDURE, PUBLIC :: YEAR  => SP_JUL_JU2Y       ! Civil year
         PROCEDURE, PUBLIC :: MONTH => SP_JUL_JU2M       ! Civil month
         PROCEDURE, PUBLIC :: DAY   => SP_JUL_JU2D       ! Civil day
         PROCEDURE, PUBLIC :: HOURS => SP_JUL_JU2H       ! Hours of the day
         
         PROCEDURE, PUBLIC :: ISOZ  => SP_JUL_JU2ISOZ    ! As string ISO
         PROCEDURE, PUBLIC :: J2000 => SP_JUL_JU2000     ! As J2000
      END TYPE SP_JUL_T

      ! ---  Constructors
      INTERFACE SP_JUL
         MODULE PROCEDURE SP_JUL_CTRDEF    ! Default constructor & real
         MODULE PROCEDURE SP_JUL_CTRCPY    ! Copy constructor
         MODULE PROCEDURE SP_JUL_CTRDTE    ! Constructor with date components
      END INTERFACE SP_JUL

      ! ---  Operators
      INTERFACE ASSIGNMENT (=)
         MODULE PROCEDURE SP_JUL_OPASGDJU
         MODULE PROCEDURE SP_JUL_OPASGDBL
      END INTERFACE ASSIGNMENT (=)

      INTERFACE OPERATOR (+)
         MODULE PROCEDURE SP_JUL_OPADD
      END INTERFACE OPERATOR (+)
      
      INTERFACE OPERATOR (-)
         MODULE PROCEDURE SP_JUL_OPSUB
         MODULE PROCEDURE SP_JUL_OPSUB_R
      END INTERFACE OPERATOR (-)
      
      INTERFACE OPERATOR (.EQ.)
         MODULE PROCEDURE SP_JUL_OPEQ
      END INTERFACE OPERATOR (.EQ.)
      INTERFACE OPERATOR (.NE.)
         MODULE PROCEDURE SP_JUL_OPNE
      END INTERFACE OPERATOR (.NE.)
      INTERFACE OPERATOR (.LT.)
         MODULE PROCEDURE SP_JUL_OPLT
      END INTERFACE OPERATOR (.LT.)
      INTERFACE OPERATOR (.LE.)
         MODULE PROCEDURE SP_JUL_OPLE
      END INTERFACE OPERATOR (.LE.)
      INTERFACE OPERATOR (.GT.)
         MODULE PROCEDURE SP_JUL_OPGT
      END INTERFACE OPERATOR (.GT.)
      INTERFACE OPERATOR (.GE.)
         MODULE PROCEDURE SP_JUL_OPGE
      END INTERFACE OPERATOR (.GE.)
      
      TYPE(SP_JUL_T), PARAMETER :: SP_JUL_J2000 = SP_JUL_T(2451545.0D0)
      REAL*8, PARAMETER :: SP_JUL_CTRY    =   36525.0D0
      REAL*8, PARAMETER :: SP_JUL_YEAR    =     365.25D0
      REAL*8, PARAMETER :: SP_JUL_MONTH   =      30.6001D0
      INTEGER,PARAMETER :: SP_JUL_GREGCAL = 2299171   ! 1582-10-15: Switch to Gregorian cal

      CONTAINS

C************************************************************************
C Sommaire: Seconds to century
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION SP_JUL_S2C(T)

      REAL*8, INTENT(IN) :: T
C-----------------------------------------------------------------------

      SP_JUL_S2C = T / 3155760000.0D0  ! 24*3600*365.25*100
      RETURN
      END FUNCTION SP_JUL_S2C

C************************************************************************
C Sommaire: Seconds to days
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION SP_JUL_S2D(T)

      REAL*8, INTENT(IN) :: T
C-----------------------------------------------------------------------

      SP_JUL_S2D = T / 86400.0D0 ! 24*3600
      RETURN
      END FUNCTION SP_JUL_S2D

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_CTRDEF
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_CTRDEF(DJU) RESULT(SELF)
      
      REAL*8, INTENT(IN), OPTIONAL :: DJU
      
      TYPE(SP_JUL_T) :: SELF
C-----------------------------------------------------------------------
      IF (PRESENT(DJU)) THEN
         SELF%DJUL = DJU
      ELSE
         SELF%DJUL = 0.0D0
      ENDIF
      RETURN
      END FUNCTION SP_JUL_CTRDEF

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_CTRDEF
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_CTRCPY(OTHER) RESULT(SELF)
      
      TYPE(SP_JUL_T) :: SELF
      TYPE(SP_JUL_T), INTENT(IN) :: OTHER
C-----------------------------------------------------------------------
      
      SELF%DJUL = OTHER%DJUL
      RETURN
      END FUNCTION SP_JUL_CTRCPY

C************************************************************************
C Sommaire: Jour Julien correspondant à une date
C
C Description:
C     La fonction SP_JUL_DT2JU calcule le jour Julien d'une date du
C     calendrier Grégorien ou du calendrier Julien.
C     La passage au calendrier Grégorien est automatique après le
C     1582-10-15.
C
C Entrée:
C     IY       Year  (-4712...)
C     IM       Month (1-12)
C     ID       Day   (1-31)
C     FD       Hours as fraction of day
C
C Sortie:
C
C Notes:
C     http://adsbit.harvard.edu//full/1984QJRAS..25...53H/0000054.000.html
C************************************************************************
      FUNCTION SP_JUL_CTRDTE(IY, IM, ID, FD) RESULT(SELF)
      
      TYPE(SP_JUL_T) :: SELF
      INTEGER,INTENT(IN) :: IY, IM, ID
      REAL*8, INTENT(IN) :: FD
      
      REAL*8  :: DJU
      INTEGER :: IYp, IMp
      INTEGER :: JY, JM, JD, JG
C-----------------------------------------------------------------------
      
      IYp = IY - INT((12-IM)/10.0D0)
      IMp = MOD(IM-3+12, 12)

      JY = INT(SP_JUL_YEAR*(IYp + 4712))
      JM = INT(SP_JUL_MONTH*IMp + 0.5D0)      !! NINT
      JD = JY + JM + ID + 59
      IF (JD .GE. SP_JUL_GREGCAL) THEN
         JG = INT(INT(IYp/100.0D0 + 49.0D0) * 0.75D0) - 38
         JD = JD - JG
      ENDIF
      DJU = JD + FD - 0.5D0

      SELF%DJUL = DJU
      RETURN
      END FUNCTION SP_JUL_CTRDTE

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPASGDJU
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_JUL_OPASGDJU(SELF, OTHER)
      
      CLASS(SP_JUL_T), INTENT(INOUT) :: SELF
      CLASS(SP_JUL_T), INTENT(IN)    :: OTHER
C-----------------------------------------------------------------------
      
      SELF%DJUL = OTHER%DJUL
      RETURN
      END SUBROUTINE SP_JUL_OPASGDJU

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPASGDBL
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_JUL_OPASGDBL(SELF, DJUL)
      
      CLASS(SP_JUL_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: DJUL
C-----------------------------------------------------------------------
      
      SELF%DJUL = DJUL
      RETURN
      END SUBROUTINE SP_JUL_OPASGDBL

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPADD
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_OPADD(OPL, DEL) RESULT(RES)
      
      CLASS(SP_JUL_T), INTENT(IN) :: OPL
      REAL*8, INTENT(IN) :: DEL
      
      TYPE(SP_JUL_T) :: RES
C-----------------------------------------------------------------------
      
      RES%DJUL = OPL%DJUL + DEL
      RETURN
      END FUNCTION SP_JUL_OPADD

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPSUB
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_OPSUB(OPL, OPR) RESULT(RES)
      
      CLASS(SP_JUL_T), INTENT(IN) :: OPL
      CLASS(SP_JUL_T), INTENT(IN) :: OPR
      
      REAL*8 :: RES
C-----------------------------------------------------------------------
      
      RES = OPL%DJUL - OPR%DJUL
      RETURN
      END FUNCTION SP_JUL_OPSUB

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPSUB
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_OPSUB_R(OPL, DEL) RESULT(RES)
      
      CLASS(SP_JUL_T), INTENT(IN) :: OPL
      REAL*8, INTENT(IN) :: DEL
      
      TYPE(SP_JUL_T) :: RES
C-----------------------------------------------------------------------
      
      RES%DJUL = OPL%DJUL - DEL
      RETURN
      END FUNCTION SP_JUL_OPSUB_R

C************************************************************************
C Sommaire: 
C
C Description:
C     La fonction SP_JUL_OPLESS
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_OPEQ(OPL, OPR) RESULT(RES)
      
      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .EQ. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPEQ
C-----------------------------------------------------------------------
      FUNCTION SP_JUL_OPNE(OPL, OPR) RESULT(RES)
      
      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .NE. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPNE
C-----------------------------------------------------------------------
      FUNCTION SP_JUL_OPLT(OPL, OPR) RESULT(RES)

      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .LT. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPLT
C-----------------------------------------------------------------------
      FUNCTION SP_JUL_OPLE(OPL, OPR) RESULT(RES)

      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .LE. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPLE
C-----------------------------------------------------------------------
      FUNCTION SP_JUL_OPGT(OPL, OPR) RESULT(RES)

      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .GT. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPGT
C-----------------------------------------------------------------------
      FUNCTION SP_JUL_OPGE(OPL, OPR) RESULT(RES)

      CLASS(SP_JUL_T), INTENT(IN) :: OPL, OPR
      LOGICAL :: RES
      
      RES = (OPL%DJUL .GE. OPR%DJUL)
      RETURN
      END FUNCTION SP_JUL_OPGE

C************************************************************************
C Sommaire: Jour calendrier correspondant à un jour Julien
C
C Description:
C     La fonction SP_JUL_JU2DT calcule la date calendrier qui correspond
C     au jour Julien DJU. La date est soit en calendrier Grégorien soit
C     en calendrier Julien.
C     La passage au calendrier Grégorien est automatique après le
C     1582-10-15.
C
C Entrée:
C     DJU      Jour Julien
C
C Sortie:
C     IY       Year  (-4712...)
C     IM       Month (1-12)
C     ID       Day   (1-31)
C     FD       Hours as fraction of day
C
C Notes:
C************************************************************************
      INTEGER FUNCTION SP_JUL_JU2DT(SELF, IY, IM, ID, FD)
      
      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(OUT) :: IY, IM, ID
      REAL*8,  INTENT(OUT) :: FD
      
      REAL*8  :: DJU
      INTEGER :: JD, JG, Dd
C-----------------------------------------------------------------------
      
      DJU = SELF%DJUL
      FD = MOD(DJU - INT(DJU) + 0.5D0, 1.0D0)
      JD = INT(DJU+0.5D0)
      IF (DJU .GT. SP_JUL_GREGCAL) THEN
         JG = INT((DJU-4479.5D0) / 36524.25D0)
         JG = INT(JG*0.75D0 + 0.5D0) - 37
         JD = JD + JG
      ENDIF
      IY = INT(JD/SP_JUL_YEAR) - 4712
      Dd = INT( MOD(JD-59.25D0, SP_JUL_YEAR) )
      IM = MOD(INT((Dd+0.5D0)/SP_JUL_MONTH)+2, 12) + 1
      ID = INT(MOD(Dd+0.5D0, SP_JUL_MONTH)) + 1
      
      SP_JUL_JU2DT = 0
      RETURN
      END FUNCTION SP_JUL_JU2DT

C************************************************************************
C Sommaire: Jour calendrier correspondant à un jour Julien
C
C Description:
C     La fonction SP_JUL_JU2DT calcule la date calendrier qui correspond
C     au jour Julien DJU. La date est soit en calendrier Grégorien soit
C     en calendrier Julien.
C     La passage au calendrier Grégorien est automatique après le
C     1582-10-15.
C
C Entrée:
C     DJU      Jour Julien
C
C Sortie:
C
C Notes:
C     Imprime avec une résolution de microseconde
C************************************************************************
      CHARACTER*(24) FUNCTION SP_JUL_JU2ISOZ(SELF)
      
      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      
      REAL*8  :: FD
      INTEGER :: IY, IM, ID, IH, IB, IS, NT
      INTEGER :: IERR
      CHARACTER*(24) :: ISO
      
      INTEGER, PARAMETER :: SECRES_S = 1000
      INTEGER, PARAMETER :: SECRES_M = 60*SECRES_S
      INTEGER, PARAMETER :: SECRES_H = 60*SECRES_M
      INTEGER, PARAMETER :: SECRES_J = 24*SECRES_H
C-----------------------------------------------------------------------

      IERR = SELF%DATE(IY, IM, ID, FD)
      
      NT = NINT(FD*SECRES_J)
      IH = INT(NT/SECRES_H) ; NT = NT - IH*SECRES_H
      IB = INT(NT/SECRES_M) ; NT = NT - IB*SECRES_M
      IS = INT(NT/SECRES_S) ; NT = NT - IS*SECRES_S
      
      WRITE(ISO, 1111) IY, IM, ID, IH, IB, IS, NT
1111  FORMAT(I4.4,'-',I2.2,'-',I2.2,'T',
     &       I2.2,':',I2.2,':',I2.2,'.',I3.3,'Z')

      SP_JUL_JU2ISOZ = ISO
      RETURN
      END FUNCTION SP_JUL_JU2ISOZ

C************************************************************************
C Sommaire: Année
C
C Description:
C     La fonction SP_JUL_JU2C.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2C(SELF, SINCE) RESULT(C)

      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      CLASS(SP_JUL_T), INTENT(IN), OPTIONAL :: SINCE
      
      REAL*8  :: C
C-----------------------------------------------------------------------
      IF (PRESENT(SINCE)) THEN
         C  = (SELF%DJUL-SINCE%DJUL) / SP_JUL_CTRY
      ELSE
         C  = SELF%DJUL / SP_JUL_CTRY
      ENDIF
      RETURN
      END FUNCTION SP_JUL_JU2C

C************************************************************************
C Sommaire: Année
C
C Description:
C     La fonction SP_JUL_JU2H retourne l'année du jour Julien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2Y(SELF) RESULT(IY)

      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      
      REAL*8  :: FD
      INTEGER :: IY, IM, ID, IH, IB, IS, NT
      INTEGER :: IERR
C-----------------------------------------------------------------------

      IERR = SELF%DATE(IY, IM, ID, FD)
      RETURN
      END FUNCTION SP_JUL_JU2Y

C************************************************************************
C Sommaire: Année
C
C Description:
C     La fonction SP_JUL_JU2H retourne l'année du jour Julien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2M(SELF) RESULT(IM)

      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      
      REAL*8  :: FD
      INTEGER :: IY, IM, ID, IH, IB, IS, NT
      INTEGER :: IERR
C-----------------------------------------------------------------------

      IERR = SELF%DATE(IY, IM, ID, FD)
      RETURN
      END FUNCTION SP_JUL_JU2M

C************************************************************************
C Sommaire: Année
C
C Description:
C     La fonction SP_JUL_JU2H retourne l'année du jour Julien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2D(SELF) RESULT(ID)

      CLASS(SP_JUL_T), INTENT(IN) :: SELF
      
      REAL*8  :: FD
      INTEGER :: IY, IM, ID, IH, IB, IS, NT
      INTEGER :: IERR
C-----------------------------------------------------------------------

      IERR = SELF%DATE(IY, IM, ID, FD)
      RETURN
      END FUNCTION SP_JUL_JU2D

C************************************************************************
C Sommaire: Heures du jour
C
C Description:
C     La fonction SP_JUL_JU2H retourne les heures de jour (jour Julien),
C     en point flottant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2H(SELF) RESULT(FD)

      REAL*8 :: FD
      CLASS(SP_JUL_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      FD = MOD(SELF%DJUL - INT(SELF%DJUL) + 0.5D0, 1.0D0)
      FD = FD*24.0D0
      RETURN
      END FUNCTION SP_JUL_JU2H

C************************************************************************
C Sommaire: Heures du jour
C
C Description:
C     La fonction SP_JUL_JU2H retourne les heures de jour (jour Julien),
C     en point flottant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_JUL_JU2000(SELF) RESULT(DJ2000)

      TYPE(SP_JUL_T) :: DJ2000
      CLASS(SP_JUL_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      DJ2000%DJUL = SELF%DJUL - SP_JUL_J2000%DJUL
      RETURN
      END FUNCTION SP_JUL_JU2000

      END MODULE SP_JUL_M
      