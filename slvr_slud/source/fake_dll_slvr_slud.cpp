//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_slud
// Entry point: extern "C" void fake_dll_slvr_slud()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:05.520000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SLUD
F_PROT(IC_SLUD_CMD, ic_slud_cmd);
F_PROT(IC_SLUD_REQCMD, ic_slud_reqcmd);
 
// ---  class MR_SLUD
F_PROT(MR_SLUD_000, mr_slud_000);
F_PROT(MR_SLUD_999, mr_slud_999);
F_PROT(MR_SLUD_CTR, mr_slud_ctr);
F_PROT(MR_SLUD_DTR, mr_slud_dtr);
F_PROT(MR_SLUD_INI, mr_slud_ini);
F_PROT(MR_SLUD_RST, mr_slud_rst);
F_PROT(MR_SLUD_REQHBASE, mr_slud_reqhbase);
F_PROT(MR_SLUD_HVALIDE, mr_slud_hvalide);
F_PROT(MR_SLUD_ESTDIRECTE, mr_slud_estdirecte);
F_PROT(MR_SLUD_ASMMTX, mr_slud_asmmtx);
F_PROT(MR_SLUD_FCTMTX, mr_slud_fctmtx);
F_PROT(MR_SLUD_RESMTX, mr_slud_resmtx);
F_PROT(MR_SLUD_XEQ, mr_slud_xeq);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_slud()
{
   static char libname[] = "slvr_slud";
 
   // ---  class IC_SLUD
   F_RGST(IC_SLUD_CMD, ic_slud_cmd, libname);
   F_RGST(IC_SLUD_REQCMD, ic_slud_reqcmd, libname);
 
   // ---  class MR_SLUD
   F_RGST(MR_SLUD_000, mr_slud_000, libname);
   F_RGST(MR_SLUD_999, mr_slud_999, libname);
   F_RGST(MR_SLUD_CTR, mr_slud_ctr, libname);
   F_RGST(MR_SLUD_DTR, mr_slud_dtr, libname);
   F_RGST(MR_SLUD_INI, mr_slud_ini, libname);
   F_RGST(MR_SLUD_RST, mr_slud_rst, libname);
   F_RGST(MR_SLUD_REQHBASE, mr_slud_reqhbase, libname);
   F_RGST(MR_SLUD_HVALIDE, mr_slud_hvalide, libname);
   F_RGST(MR_SLUD_ESTDIRECTE, mr_slud_estdirecte, libname);
   F_RGST(MR_SLUD_ASMMTX, mr_slud_asmmtx, libname);
   F_RGST(MR_SLUD_FCTMTX, mr_slud_fctmtx, libname);
   F_RGST(MR_SLUD_RESMTX, mr_slud_resmtx, libname);
   F_RGST(MR_SLUD_XEQ, mr_slud_xeq, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
