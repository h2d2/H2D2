class remesh
============
 
   The class **remesh** is a remeshing algorithm based on an ellipse error
   field. (Status: Experimental).
    
   constructor handle remesh(hgrid, hlimt)
      The constructor **remesh** constructs an object and returns a handle on
      this object.
         handle   HOBJ        Return value: Handle on the algorithm
         handle   hgrid       Handle on the input grid
         handle   hlimt       Handle on the boundaries (default 0)
    
   method asg_error_field(herr, vscl, vmin, vmax)
      The method **asg_error** assign the error field to be used for mesh
      adaptation. It replaces the existing error fields. The error ellipses
      are possibly scaled and limited.
         handle   herr        Handle on the input error field
         double   vscl        Specific limit: ellipse scaling factor (default
                           1.0)
         double   vmin        Specific limit: ellipse min axis size (default
                           1.0e-6)
         double   vmax        Specific limit: ellipse max axis size (default
                           1.0e+6)
    
   method add_error_field(herr, vscl, vmin, vmax)
      The method **add_error_field** adds an error field to the fields to be
      used for mesh adaptation. Th resulting error field is the intersection
      of the existing fields and of the added one. The error ellipses are
      possibly scaled and limited.
         handle   herr        Handle on the input error field
         double   vscl        Specific limit: ellipse scaling factor (default
                           1.0)
         double   vmin        Specific limit: ellipse min axis size (default
                           1.0e-6)
         double   vmax        Specific limit: ellipse max axis size (default
                           1.0e+6)
    
   method adapt(hscn, ntgt, vtgt, kmin, kmax)
      The method **adapt** will adapt the mesh according to the scenario and
      the target parameters. A scenario is a list containing either records or
      other lists. A record is composed of an action, a repeat count
      (optional, default to 1) and provision for two float values. Supported
      actions are: 
        * 'smooth_metrics'
        * 'swap_edges'
        * 'coarsen_by_vertex'
        * 'refine_by_edges'
        * 'refine_by_element'
        * 'smooth_by_vertex'
        * 'smooth_by_element'
        * 'smooth_barycentric'
        * 'smooth_stars'
        * 'cure_bridges'
        * 'cure_skin'
       
      As of version 19.04, validated actions are: 
        * 'smooth_metrics'
        * 'swap_edges'
        * 'coarsen_by_vertex'
        * 'refine_by_edges'
        * 'smooth_barycentric'
        * 'smooth_stars'
        * 'cure_bridges'
        * 'cure_skin'
         handle   hscn        Handle on the list, scenario of actions
         integer  ntgt        Global target number of nodes. This is the
                           number of vertexes used for the refinement. It does
                           not
                              correspond to the final mesh node count as it
                           does not consider mid-side nodes.
                              The T6L element is treated like a T3 element.
         double   vtgt        Global target grid size, in the riemannian
                           metric.
                              On a unit circle, for an equilateral triangle,
                           the grid size if sqrt(3) ~ 1.732.
         integer  kmin        Minimal coarsening level. 0 is the original
                           level (default -2).
         integer  kmax        IF (IERR .NE. 0) KMIN = -2
                              Maxima refinement level. 0 is the original level
                           (default +2).
    
   method handle gen_grid()
      The method **gen_grid** generates the grid in the current adaptation
      state.
    
   method handle gen_limit()
      The method **gen_limit** generates the limit in the current adaptation
      state.
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
