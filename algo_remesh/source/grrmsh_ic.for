C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_GR_RMSH_XEQCTR
C     INTEGER IC_GR_RMSH_XEQMTH
C     CHARACTER*(32) IC_GR_RMSH_REQCLS
C     INTEGER IC_GR_RMSH_REQHDL
C   Private:
C     SUBROUTINE IC_GR_RMSH_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GR_RMSH_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GR_RMSH_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'grrmsh_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'grrmsh_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HGRID, HLIMT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GR_RMSH_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the input grid</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRID)
      IF (IERR .NE. 0) GOTO 9900
C     <comment>Handle on the boundaries (default 0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HLIMT)
      IF (IERR .NE. 0) HLIMT = 0

C---     Construis et initialise l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = GR_RMSH_CTR    (HOBJ)
      IF (ERR_GOOD()) IERR = GR_RMSH_INI    (HOBJ)
      IF (ERR_GOOD()) IERR = GR_RMSH_ASGGRID(HOBJ, HGRID, HLIMT)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>remesh</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GR_RMSH_AID()

9999  CONTINUE
      IC_GR_RMSH_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GR_RMSH_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GR_RMSH_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'grrmsh_ic.fi'
      INCLUDE 'grrmsh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      REAL*8       VSCL, VMIN, VMAX, VTGT
      INTEGER      IERR
      INTEGER      KMIN, KMAX, NTGT
      INTEGER      HERR, HSCN, HGRD, HLMT
      CHARACTER*64 SVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     DISPATCH LES MÉTHODES, PROPRIÉTÉS ET OPERATEURS
C     <comment>
C     The method <b>asg_error</b> assign the error field to be used
C     for mesh adaptation. It replaces the existing error fields. The
C     error ellipses are possibly scaled and limited.
C     </comment>
      IF (IMTH .EQ. 'asg_error_field') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Handle on the input error field</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HERR)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Specific limit: ellipse scaling factor (default 1.0)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, VSCL)
         IF (IERR .NE. 0) VTGT = 1.0D0
C        <comment>Specific limit: ellipse min axis size (default 1.0e-6)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, VMIN)
         IF (IERR .NE. 0) VMIN = 1.0D-6
C        <comment>Specific limit: ellipse max axis size (default 1.0e+6)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, VMAX)
         IF (IERR .NE. 0) VMAX = 1.0D+6

         IERR = GR_RMSH_ASGMTRX(HOBJ, HERR, VSCL, VMIN, VMAX)

C     <comment>
C     The method <b>add_error_field</b> adds an error field to the
C     fields to be used for mesh adaptation. Th resulting error field
C     is the intersection of the existing fields and of the added one.
C     The error ellipses are possibly scaled and limited.
C     </comment>
      ELSEIF (IMTH .EQ. 'add_error_field') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Handle on the input error field</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HERR)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Specific limit: ellipse scaling factor (default 1.0)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, VSCL)
         IF (IERR .NE. 0) VTGT = 1.0D0
C        <comment>Specific limit: ellipse min axis size (default 1.0e-6)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, VMIN)
         IF (IERR .NE. 0) VMIN = 1.0D-6
C        <comment>Specific limit: ellipse max axis size (default 1.0e+6)</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, VMAX)
         IF (IERR .NE. 0) VMAX = 1.0D+6

         IERR = GR_RMSH_AJTMTRX(HOBJ, HERR, VSCL, VMIN, VMAX)

C     <comment>
C     The method <b>adapt</b> will adapt the mesh according to the scenario and the target parameters.
C     A scenario is a list containing either records or other lists. A record is composed of an action,
C     a repeat count (optional, default to 1) and provision for two float values. Supported actions are:
C     <ul>
C       <li>'smooth_metrics'</li>
C       <li>'swap_edges'</li>
C       <li>'coarsen_by_vertex'</li>
C       <li>'refine_by_edges'</li>
C       <li>'refine_by_element'</li>
C       <li>'smooth_by_vertex'</li>
C       <li>'smooth_by_element'</li>
C       <li>'smooth_barycentric'</li>
C       <li>'smooth_stars'</li>
C       <li>'cure_bridges'</li>
C       <li>'cure_skin'</li>
C     </ul>
C     As of version 19.04, validated actions are:
C     <ul>
C       <li>'smooth_metrics'</li>
C       <li>'swap_edges'</li>
C       <li>'coarsen_by_vertex'</li>
C       <li>'refine_by_edges'</li>
C       <li>'smooth_barycentric'</li>
C       <li>'smooth_stars'</li>
C       <li>'cure_bridges'</li>
C       <li>'cure_skin'</li>
C     </ul>
C     </comment>
      ELSEIF (IMTH .EQ. 'adapt') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (SP_STRN_LEN(IPRM) .GT. LEN(SVAL)) GOTO 9901

C        <comment>Handle on the list, scenario of actions</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSCN)
C        <comment>
C        Global target number of nodes. This is the number of vertexes used for the refinement. It does not
C        correspond to the final mesh node count as it does not consider mid-side nodes.
C        The T6L element is treated like a T3 element.
C        </comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, NTGT)
C        <comment>
C        Global target grid size, in the riemannian metric.
C        On a unit circle, for an equilateral triangle, the grid size if sqrt(3) ~ 1.732.
C        </comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, VTGT)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>
C        Minimal coarsening level. 0 is the original level (default -2).
C        </comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, KMIN)
C        <comment>
         IF (IERR .NE. 0) KMIN = -2
C        Maxima refinement level. 0 is the original level (default +2).
C        </comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, KMAX)
         IF (IERR .NE. 0) KMAX = +2

         IERR = GR_RMSH_ADAPT(HOBJ, HSCN, NTGT, VTGT, KMIN, KMAX)

C     <comment>The method <b>gen_grid</b> generates the grid in the current adaptation state.</comment>
      ELSEIF (IMTH .EQ. 'gen_grid') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         HGRD = 0
         IERR = GR_RMSH_REQGRID(HOBJ, HGRD)
         IF (ERR_GOOD())  WRITE(IPRM, '(2A,I12)') 'H', ',', HGRD

C     <comment>The method <b>gen_limit</b> generates the limit in the current adaptation state.</comment>
      ELSEIF (IMTH .EQ. 'gen_limit') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         HGRD = 0
         IERR = GR_RMSH_REQLIMT(HOBJ, HLMT)
         IF (ERR_GOOD())  WRITE(IPRM, '(2A,I12)') 'H', ',', HLMT

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GR_RMSH_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LOG_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test GR_RMSH_PRINT___(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GR_RMSH_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GR_RMSH_AID()

9999  CONTINUE
      IC_GR_RMSH_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GR_RMSH_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GR_RMSH_REQCLS
CDEC$ ENDIF

      INCLUDE 'grrmsh_ic.fi'
C------------------------------------------------------------------------

C  <comment>
C  The class <b>remesh</b> is a remeshing algorithm based on an
C  ellipse error field. (Status: Experimental).
C  </comment>
      IC_GR_RMSH_REQCLS = 'remesh'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GR_RMSH_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GR_RMSH_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'grrmsh_ic.fi'
      INCLUDE 'grrmsh.fi'
C-------------------------------------------------------------------------

      IC_GR_RMSH_REQHDL = GR_RMSH_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_COORD_AID écris dans le log l'aide relative
C     à la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GR_RMSH_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('grrmsh_ic.hlp')

      RETURN
      END
