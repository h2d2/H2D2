C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  GRid
C Objet:   ReMeSH
C Type:    Concret
C Functions:
C   Public:
C     INTEGER GR_RMSH_000
C     INTEGER GR_RMSH_999
C     INTEGER GR_RMSH_CTR
C     INTEGER GR_RMSH_DTR
C     INTEGER GR_RMSH_INI
C     INTEGER GR_RMSH_RST
C     INTEGER GR_RMSH_REQHBASE
C     LOGICAL GR_RMSH_HVALIDE
C     INTEGER GR_RMSH_XEQ
C     INTEGER GR_RMSH_ASGGRID
C     INTEGER GR_RMSH_ASGMTRX
C     INTEGER GR_RMSH_AJTMTRX
C     INTEGER GR_RMSH_ADAPT
C     INTEGER GR_RMSH_REQGRID
C     INTEGER GR_RMSH_REQLIMT
C   Private:
C     INTEGER GR_RMSH_ASGGRID_CB
C     INTEGER GR_RMSH_ASGLIMT
C     SUBROUTINE GR_RMSH_CBINFO
C     INTEGER GR_RMSH_REQGRID_CB
C     INTEGER GR_RMSH_TRDSCN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'grrmsh.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GR_RMSH_NOBJMAX,
     &                   GR_RMSH_HBASE,
     &                   'Mesh adaptation algorithm')

      GR_RMSH_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'grrmsh.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER  IERR
      EXTERNAL GR_RMSH_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GR_RMSH_NOBJMAX,
     &                   GR_RMSH_HBASE,
     &                   GR_RMSH_DTR)

      GR_RMSH_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GR_RMSH_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'grrmsh.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GR_RMSH_NOBJMAX,
     &                   GR_RMSH_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GR_RMSH_HVALIDE(HOBJ))
         IOB = HOBJ - GR_RMSH_HBASE

         GR_RMSH_XALG(IOB) = 0
         GR_RMSH_HLMG(IOB) = 0
!         GR_RMSH_HGRI(IOB) = 0
         GR_RMSH_HERR(IOB) = 0
      ENDIF

      GR_RMSH_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GR_RMSH_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'grrmsh.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GR_RMSH_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GR_RMSH_NOBJMAX,
     &                   GR_RMSH_HBASE)

      GR_RMSH_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GR_RMSH_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER*8 XALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = GR_RMSH_RST(HOBJ)

C---     Construis l'algorithme
      IRET = 0
      IF (ERR_GOOD()) THEN
         IRET = C_RMESH_CTR(XALG)
         IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         GR_RMSH_XALG(IOB) = XALG
      ENDIF

      GR_RMSH_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GR_RMSH_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   HLMG
      INTEGER*8 XALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - GR_RMSH_HBASE
      XALG = GR_RMSH_XALG(IOB)
      HLMG = GR_RMSH_HLMG(IOB)

C---     Détruis l'algorithme
      IRET = 0
      IF (ERR_GOOD() .AND. IRET .EQ. 0) THEN
         IRET = C_RMESH_DTR(XALG)
      ENDIF

C---     Détruis l'élément
      IF (ERR_GOOD() .AND. LM_HGEO_HVALIDE(HLMG)) THEN
         IERR = LM_HGEO_DTR(HLMG)
      ENDIF

C---     Reset les attributs
      IF (ERR_GOOD()) THEN
         GR_RMSH_XALG(IOB) = 0
         GR_RMSH_HLMG(IOB) = 0
!         GR_RMSH_HGRI(IOB) = 0
         GR_RMSH_HERR(IOB) = 0
      ENDIF

      GR_RMSH_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GR_RMSH_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'grrmsh.fi'
      INCLUDE 'grrmsh.fc'
C------------------------------------------------------------------------

      GR_RMSH_REQHBASE = GR_RMSH_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GR_RMSH_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'grrmsh.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'grrmsh.fc'
C------------------------------------------------------------------------

      GR_RMSH_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GR_RMSH_NOBJMAX,
     &                                  GR_RMSH_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Adapte un maillage.
C
C Description:
C     La fonction GR_RMSH_XEQ adapte le maillage HGRID d'après
C     le champ d'ellipses HELLP et le scénario d'adaptation HSCNR.
C     Elle produit un nouveau maillage HGRO.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HGRID       Handle on the source (input) grid
C     HLIMT       Handle on the source (input) boundaries
C     HELLP       Handle on the ellipse VNO
C     HSCNR       Handle on the adaptation scenario
C
C Sortie:
C     HGRO        Handle on the new (output) grid
C     HLMO        Handle on the new (output) limit
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_XEQ(HOBJ,
     &                     HGRO,
     &                     HLMO,
     &                     HGRID,
     &                     HLIMT,
     &                     HELLP,
     &                     HSCNR,
     &                     NTGT,
     &                     ETGT,
     &                     KMIN,
     &                     KMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRO
      INTEGER HLMO
      INTEGER HGRID
      INTEGER HLIMT
      INTEGER HELLP
      INTEGER HSCNR
      INTEGER NTGT
      REAL*8  ETGT
      INTEGER KMIN
      INTEGER KMAX

      INCLUDE 'grrmsh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

      CALL TR_CHRN_START('h2d2.grid.remesh')

      IF (ERR_GOOD()) IERR = GR_RMSH_ASGGRID(HOBJ, HGRID, HLIMT)
      IF (ERR_GOOD()) IERR = GR_RMSH_ADAPT  (HOBJ, HSCNR,
     &                                       NTGT, ETGT, KMIN, KMAX)
      IF (ERR_GOOD()) IERR = GR_RMSH_REQGRID(HOBJ, HGRO)
      IF (ERR_GOOD()) IERR = GR_RMSH_REQLIMT(HOBJ, HLMO)

      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le maillage à adapter.
C
C Description:
C     La fonction GR_RMSH_ASGGRID assigne le maillage initial qui peut
C     ensuite être adapté.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HGRID       Handle on the source (input) grid
C     HLIMT       Handle on the source (input) boundaries
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_ASGGRID(HOBJ, HGRID, HLIMT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_ASGGRID
CDEC$ ENDIF

      USE LG_LGCY_M, ONLY: LG_LGCY_T, LG_LGCY_CTR
      USE LM_HGEO_M, ONLY: LM_HGEO_CTR
      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRID
      INTEGER HLIMT

      INCLUDE 'grrmsh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'grrmsh.fc'

      REAL*8    TSIM
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   ITPEV
      INTEGER   HCONF
      INTEGER   HLMG, HMTH
      CLASS(LG_LGCY_T), POINTER :: OGEOM
      EXTERNAL  GR_RMSH_ASGGRID_CB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Contrôle les paramètres
      IF (.NOT. DT_GRID_HVALIDE(HGRID)) GOTO 9900
      IF (.NOT. (HLIMT .EQ. 0 .OR. DT_LIMT_HVALIDE(HLIMT))) GOTO 9900

C---     Récupère les attributs
      IOB = HOBJ - GR_RMSH_HBASE
      HLMG = GR_RMSH_HLMG(IOB)
D     CALL ERR_ASR(HLMG .EQ. 0 .OR. LM_HGEO_HVALIDE(HLMG))

C---     Construis l'élément géométrique
      IF (ERR_GOOD() .AND. HLMG .NE. 0) THEN
         IERR  = LM_HGEO_DTR(HLMG)
         HLMG = 0
      ENDIF
      IF (ERR_GOOD()) THEN
         ITPEV = DT_ELEM_REQITPE(DT_GRID_REQHELEV(HGRID))
         HCONF = 0
         OGEOM => LG_LGCY_CTR(ITPEV)
         IERR  = OGEOM%INI(HCONF, HGRID, HLIMT)
         IERR  = LM_HGEO_CTR(HLMG, OGEOM)
      ENDIF
      IF (ERR_GOOD()) THEN
         GR_RMSH_HLMG(IOB) = HLMG
      ENDIF

C---     Charge les données
      !! Pas moyen de savoir si le maillage est chargé
!!      TSIM = MAX(0.0D0, DT_GRID_REQTEMPS(HERR))
      TSIM = 0.0D0
      IF (ERR_GOOD()) IERR = LM_HGEO_PASDEB (HLMG, TSIM)
      IF (ERR_GOOD()) IERR = LM_HGEO_ASMESCL(HLMG)

C---     Construis le CB
      HMTH = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HMTH)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HMTH,
     &                                      HOBJ,
     &                                      GR_RMSH_ASGGRID_CB)

C---     Fait l'appel
      IERR = SO_FUNC_CALL(HMTH, SO_ALLC_CST2B(HLMG))

C---     Détruis le CB
      IF (SO_FUNC_HVALIDE(HMTH)) THEN
         IERR = SO_FUNC_DTR(HMTH)
         HMTH = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_ELEMENT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_TRIANGLES_REQUIS'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_ASGGRID = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le maillage à adapter.
C
C Description:
C     La fonction GR_RMSH_ASGGRID_CB assigne le maillage initial qui peut
C     ensuite être adapté.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HGEO        Handle on the geometry
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_ASGGRID_CB(HOBJ, HGEO)

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGEO

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER*8 XALG
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   ITPRV, ITPRS
      LOGICAL   ALIMITES
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - GR_RMSH_HBASE
      XALG = GR_RMSH_XALG(IOB)

C---     Récupère les données
      GDTA => LM_HGEO_REQGDTA(HGEO)
      ALIMITES = (GDTA%NCLLIM .GT. 0)

C---     Détermine le type d'élément du remaillage
      ITPRV = 0
      IF (ERR_GOOD()) THEN
         SELECT CASE(GDTA%ITPEV)
            CASE(EG_TPGEO_T3);  ITPRV = C_RMESH_ELTYP_T3
            CASE(EG_TPGEO_T6);  ITPRV = C_RMESH_ELTYP_T6
            CASE(EG_TPGEO_T6L); ITPRV = C_RMESH_ELTYP_T6L
            CASE DEFAULT
               GOTO 9900
         END SELECT
      ENDIF
      ITPRS = 0
      IF (ERR_GOOD() .AND. ALIMITES) THEN
         SELECT CASE(GDTA%ITPES)
            CASE(EG_TPGEO_L2);  ITPRS = C_RMESH_ELTYP_L2
            CASE(EG_TPGEO_L3);  ITPRS = C_RMESH_ELTYP_L3
            CASE(EG_TPGEO_L3L); ITPRS = C_RMESH_ELTYP_L3L
            CASE DEFAULT
               GOTO 9900
         END SELECT
      ENDIF

C---     Assigne le maillage
      IRET = 0
      IF (ERR_GOOD() .AND. IRET .EQ. 0) THEN
         IRET = C_RMESH_ASGGRI (XALG,
     &                          GDTA%NDIM,
     &                          GDTA%NNT,
     &                          GDTA%VCORG,
     &                          ITPRV,
     &                          GDTA%NNELV,
     &                          GDTA%NCELV,
     &                          GDTA%NELLV,
     &                          GDTA%KNGV,
     &                          ALIMITES)
      ENDIF

C---     Assigne les limites
      IF (ERR_GOOD() .AND. IRET .EQ. 0 .AND. ALIMITES) THEN
         IERR = GR_RMSH_ASGLIMT(XALG,
     &                          GDTA%NCLLIM,
     &                          GDTA%NCLELE,
     &                          GDTA%KCLLIM,
     &                          GDTA%KCLELE,
     &                          ITPRS,
     &                          GDTA%NNELS,
     &                          GDTA%NCELS,
     &                          GDTA%NELLS,
     &                          GDTA%KNGS)
      ENDIF

C---     Récupère les messages d'erreur
      IF (IRET .NE. 0) THEN
         IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         GR_RMSH_ITPV(IOB) = ITPRV
         GR_RMSH_ITPS(IOB) = ITPRS
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_ELEMENT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_TRIANGLES_REQUIS'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GR_RMSH_ASGGRID_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée GR_RMSH_ASGLIMT boucles sur les limite pour les
C     assigner tour à tour à l'algorithme XALG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_ASGLIMT(XALG,
     &                         NCLLIM,
     &                         NCLELE,
     &                         KCLLIM,
     &                         KCLELE,
     &                         ITPRS,
     &                         NNELS,
     &                         NCELS,
     &                         NELLS,
     &                         KNGS)

      IMPLICIT NONE

      INTEGER*8 XALG
      INTEGER   NCLLIM
      INTEGER   NCLELE
      INTEGER   KCLLIM( 7, NCLLIM)
      INTEGER   KCLELE(    NCLELE)
      INTEGER   ITPRS
      INTEGER   NNELS
      INTEGER   NCELS
      INTEGER   NELLS
      INTEGER   KNGS  (NCELS, NELLS)

      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER IRET
      INTEGER IL
      INTEGER IEDEB, IEFIN
      INTEGER NELEM
C-----------------------------------------------------------------------

      IRET = 0

C---     Boucle sur les limites
      DO IL=1, NCLLIM
         IEDEB = KCLLIM(5,IL)
         IEFIN = KCLLIM(6,IL)
         NELEM = IEFIN-IEDEB+1

         IF (NELEM .GT. 0)
     &      IRET = C_RMESH_ASGLMT(XALG,
     &                            ITPRS,
     &                            NNELS,
     &                            NCELS,
     &                            NELLS,
     &                            KNGS,
     &                            KCLLIM(1,IL),    ! CRC-32 comme info
     &                            NELEM,
     &                            KCLELE(IEDEB))
         IF (IRET .NE. 0) GOTO 9901
      ENDDO


      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_TYPE_ELEMENT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GR_RMSH_ASGLIMT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne le champ d'erreur.
C
C Description:
C     La fonction GR_RMSH_ASGMTRX assigne le champ de métriques d'erreur.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HERR        Handle on the error VNO
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_ASGMTRX(HOBJ, HERR, VSCL, VMIN, VMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_ASGMTRX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HERR
      REAL*8  VSCL
      REAL*8  VMIN
      REAL*8  VMAX

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER*8 XALG
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   HLMG, HNUM
      INTEGER   NERR, NNT, LERR
      REAL*8    TSIM
      LOGICAL   MODIF
      CHARACTER*(256) NOM

      REAL*8, PARAMETER :: V_MIN = 1.0D-09
      REAL*8, PARAMETER :: V_MAX = 1.0D+09
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.remesh'

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_REMESH_ASG_METRIX'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Écris les paramètres
      CALL LOG_INCIND()
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(NOM, HERR)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_CHAMP_ERREUR#<35>#', '= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_FMUL#<35>#', '= ',
     &                                  VSCL
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_AXE_MIN#<35>#', '= ',
     &                                  VMIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_AXE_MAX#<35>#', '= ',
     &                                  VMAX
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

C---     Contrôle les paramètres
      IF (.NOT. DT_VNOD_HVALIDE(HERR)) GOTO 9900
      IF (VSCL .LT. V_MIN) GOTO 9901
      IF (VSCL .GT. V_MAX) GOTO 9901
      IF (VMIN .LT. V_MIN) GOTO 9902
      IF (VMAX .GT. V_MAX) GOTO 9902
      IF (VMAX .LT. VMIN)  GOTO 9902

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         XALG = GR_RMSH_XALG(IOB)
         HLMG = GR_RMSH_HLMG(IOB)
         IF (.NOT. LM_HGEO_HVALIDE(HLMG)) GOTO 9903
      ENDIF

C---     Charge les données
      TSIM = MAX(0.0D0, DT_VNOD_REQTEMPS(HERR))
      HNUM = LM_HGEO_REQHNUMC(HLMG)
      IF (ERR_GOOD()) IERR = DT_VNOD_CHARGE(HERR, HNUM, TSIM, MODIF)

C---     Récupère les données
      IF (ERR_GOOD()) THEN
         NERR = DT_VNOD_REQNVNO (HERR)
         NNT  = DT_VNOD_REQNNT  (HERR)
         LERR = DT_VNOD_REQLVNO (HERR)
D        CALL ERR_ASR(SO_ALLC_HEXIST(LERR))
      ENDIF

C--      Contrôle la cohérence
      IF (ERR_GOOD()) THEN
         IF (NERR .NE. 3) GOTO 9904
      ENDIF

C---     Fait le traitement
      IF (ERR_GOOD()) THEN
         IRET = 0

C---        Assigne le champ d'erreur
         IF (IRET .EQ. 0)
     &      IRET = C_RMESH_ASGERR(XALG,
     &                            NERR,
     &                            NNT,
     &                            VA(SO_ALLC_REQVIND(VA,LERR)),
     &                            VSCL,
     &                            VMIN,
     &                            VMAX)

C---        Gère les erreurs
         IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CHAMP_ERREUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,1PE14.6E3))') 'MSG_VALEURS_ATTENDUES',
     &               ': ', V_MIN, ' <= VSCL <= ', V_MAX
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,3(2A,1PE14.6E3))') 'MSG_VALEURS_OBTENUES',
     &               ', ', 'VSCL = ', VSCL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,2(A,1PE14.6E3))') 'MSG_VALEURS_ATTENDUES',
     &               ': ', V_MIN,
     &               ' <= VMIN <= VMAX <= ', V_MAX
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,2(2A,1PE14.6E3))') 'MSG_VALEURS_OBTENUES',
     &               ': ', 'VMIN = ', VMIN,
     &               ', ', 'VMAX = ', VMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_ASSIGNER_MAILLAGE_EN_PREMIER'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,I6))') 'ERR_NVNO_CHAMP_ERREUR_INVALIDE',
     &                              ': ', NERR, ' / ', 3
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_ASGMTRX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un champ de métriques.
C
C Description:
C     La fonction GR_RMSH_AJTMTRX ajoute un champ d'erreur aux champs
C     déjà présents.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HERR        Handle on the error VNO
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_AJTMTRX(HOBJ, HERR, VSCL, VMIN, VMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_AJTMTRX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HERR
      REAL*8  VSCL
      REAL*8  VMIN
      REAL*8  VMAX

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER*8 XALG
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   HLMG, HNUM
      INTEGER   NERR, NNT, LERR
      REAL*8    TSIM
      LOGICAL   MODIF
      CHARACTER*(256) NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.remesh'

C---     Log l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_REMESH_AJT_METRIX'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Log les paramètres
      CALL LOG_INCIND()
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(NOM, HERR)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_CHAMP_ERREUR#<35>#', '= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_FMUL#<35>#', '= ',
     &                                  VSCL
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_AXE_MIN#<35>#', '= ',
     &                                  VMIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_ELLPS_AXE_MAX#<35>#', '= ',
     &                                  VMAX
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

C---     Contrôle les paramètres
      IF (.NOT. DT_VNOD_HVALIDE(HERR)) GOTO 9900
      IF (VSCL .LE. 0.0D0) GOTO 9901
      IF (VMIN .LE. 0.0D0) GOTO 9901
      IF (VMAX .LE. VMIN)  GOTO 9901
!!      IF (VSCL .LE. VMIN)  GOTO 9901
!!      IF (VSCL .GE. VMAX)  GOTO 9901

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         XALG = GR_RMSH_XALG(IOB)
         HLMG = GR_RMSH_HLMG(IOB)
         IF (.NOT. LM_HGEO_HVALIDE(HLMG)) GOTO 9902
      ENDIF

C---     Charge les données
      TSIM = MAX(0.0D0, DT_VNOD_REQTEMPS(HERR))
      HNUM = LM_HGEO_REQHNUMC(HLMG)
      IF (ERR_GOOD()) IERR = DT_VNOD_CHARGE(HERR, HNUM, TSIM, MODIF)

C---     Récupère les données
      IF (ERR_GOOD()) THEN
         NERR = DT_VNOD_REQNVNO (HERR)
         NNT  = DT_VNOD_REQNNT  (HERR)
         LERR = DT_VNOD_REQLVNO (HERR)
D        CALL ERR_ASR(SO_ALLC_HEXIST(LERR))
      ENDIF

C--      Contrôle la cohérence
      IF (ERR_GOOD()) THEN
         IF (NERR .NE. 3) GOTO 9903
      ENDIF

C---     Fait le traitement
      IF (ERR_GOOD()) THEN
         IRET = 0

C---        Assigne le champ d'erreur
         IF (IRET .EQ. 0)
     &      IRET = C_RMESH_AJTERR(XALG,
     &                            NERR,
     &                            NNT,
     &                            VA(SO_ALLC_REQVIND(VA,LERR)),
     &                            VSCL,
     &                            VMIN,
     &                            VMAX)

C---        Gère les erreurs
         IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CHAMP_ERREUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_VALEURS_ATTENDUES',
     &               ': ', '0.0 < VMIN < VSCL < VMAX'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,3(2A,1PE14.6E3))') 'MSG_VALEURS_OBTENUES',
     &               ': ', 'VMIN = ', VMIN,
     &               ', ', 'VSCL = ', VSCL,
     &               ', ', 'VMAX = ', VMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_ASSIGNER_MAILLAGE_EN_PREMIER'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,2(A,I6))') 'ERR_NVNO_CHAMP_ERREUR_INVALIDE',
     &                              ': ', NERR, ' / ', 3
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_AJTMTRX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Adapte un maillage.
C
C Description:
C     La fonction GR_RMSH_ADAPT
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSCN        Handle sur le scénario d'adaptation
C     NTGT        Nombre de noeuds cible 
C     ETGT        Erreur cible
C     KMIN       Raffinement min
C     KMAX       Raffinement max
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_ADAPT(HOBJ, HSCN, NTGT, ETGT, KMIN, KMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_ADAPT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSCN
      INTEGER NTGT
      REAL*8  ETGT
      INTEGER KMIN
      INTEGER KMAX

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER, PARAMETER :: NINFO = 10

      INTEGER*8 XALG
      REAL*8    VINFO(NINFO)
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   ITPV
      INTEGER   HLMG
      INTEGER   NNT_NEW, NELT_NEW
      INTEGER   NACT, LSCN
      EXTERNAL  GR_RMSH_CBINFO
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.remesh'

C---     Log l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_MESH_ADAPTATION'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Log les paramètres
      CALL LOG_INFO(LOG_ZNE, 'MSG_ADAPT_PARAMETRES')
      CALL LOG_INCIND()
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NNT_TGT#<35>#: ', NTGT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_E_TGT#<35>#: ', ETGT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_RAF_MIN#<35>#: ', KMIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_RAF_MAX#<35>#: ', KMAX
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

C---     Contrôle les paramètres
      IF (.NOT. DS_LIST_HVALIDE(HSCN)) GOTO 9900

C--      Traduis le scénario
      LSCN = 0
      IF (ERR_GOOD()) IERR = GR_RMSH_TRDSCN (HSCN, NACT, LSCN)

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         XALG = GR_RMSH_XALG(IOB)
         HLMG = GR_RMSH_HLMG(IOB)
         ITPV = GR_RMSH_ITPV(IOB)
         IF (.NOT. LM_HGEO_HVALIDE(HLMG)) GOTO 9901
      ENDIF

C---     Fait le traitement
      IF (ERR_GOOD()) THEN
         IRET = 0

C---        Adapte
         IF (IRET .EQ. 0)
     &      IRET = C_RMESH_ADAPT(XALG,
     &                           NINFO,
     &                           VINFO,
     &                           GR_RMSH_CBINFO,
     &                           NTGT,
     &                           ETGT,
     &                           KMIN,
     &                           KMAX,
     &                           4,
     &                           NACT,
     &                           VA(SO_ALLC_REQVIND(VA,LSCN)))

C---        Gère les erreurs
         IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(XALG, ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
      ENDIF

C---     Log les résultats
      IF (ERR_GOOD()) THEN
         NNT_NEW  = C_RMESH_REQNNT (XALG, ITPV)
         NELT_NEW = C_RMESH_REQNELT(XALG, ITPV)
         CALL LOG_INFO(LOG_ZNE, 'MSG_ADAPT_MAILLAGE')
         CALL LOG_INCIND()
         WRITE(LOG_BUF,'(A,I12)') 'MSG_NNT#<35>#: ', NNT_NEW
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         WRITE(LOG_BUF,'(A,I12)') 'MSG_NELT#<35>#: ', NELT_NEW
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_DECIND()
      ENDIF
      CALL LOG_DECIND()

C---     Récupère la mémoire
      IF (SO_ALLC_HEXIST(LSCN)) THEN
         CALL ERR_PUSH()
         IERR = SO_ALLC_ALLRE8(0, LSCN)
         CALL ERR_POP()
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SCENARIO_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_ASSIGNER_MAILLAGE_EN_PREMIER'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_ADAPT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Call-back d'information.
C
C Description:
C     La subroutine privée GR_RMSH_CBINFO est le call-back pour gérer
C     les informations d'adaptation générées par l'adaptation de
C     maillage.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE GR_RMSH_CBINFO(IOP, VINFO)

      IMPLICIT NONE

      INTEGER IOP
      REAL*8  VINFO(*)

      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER L
      CHARACTER*(64) BUF
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.remesh'

C---     Opération
      IF (IOP .EQ. C_RMESH_INDEFINI) THEN
         BUF = '---'
      ELSEIF (IOP .EQ. C_RMESH_ETAT_INITIAL) THEN
         BUF = 'Initial state'
      ELSEIF (IOP .EQ. C_RMESH_ETAT_FINAL) THEN
         BUF = 'Final state'
      ELSEIF (IOP .EQ. C_RMESH_LISSE_METRIQUES) THEN
         BUF = 'smooth_metrics'
!!!      ELSEIF (ICOD .EQ. C_RMESH_RESCALE_METRIQUES) THEN
!!!         BUF = 'rescale_metrics'
!!!      ELSEIF (ICOD .EQ. C_RMESH_MAILLE_PEAU) THEN
!!!         BUF = 'mesh_from_boundary'
      ELSEIF (IOP .EQ. C_RMESH_RETOURNE_ARETES) THEN
         BUF = 'swap_edges'
      ELSEIF (IOP .EQ. C_RMESH_SOIGNE_PEAU) THEN
         BUF = 'cure_skin'
      ELSEIF (IOP .EQ. C_RMESH_SOIGNE_PONT) THEN
         BUF = 'cure_bridges'
      ELSEIF (IOP .EQ. C_RMESH_DERAFFINE_PAR_SOMMETS) THEN
         BUF = 'coarsen_by_vertex'
      ELSEIF (IOP .EQ. C_RMESH_RAFFINE_PAR_ARETES) THEN
         BUF = 'refine_by_edges'
      ELSEIF (IOP .EQ. C_RMESH_RAFFINE_PAR_SURFACES) THEN
         BUF = 'refine_by_element'
      ELSEIF (IOP .EQ. C_RMESH_REGULARISE_PAR_ARETES) THEN
         BUF = 'smooth_by_vertex'
      ELSEIF (IOP .EQ. C_RMESH_REGULARISE_PAR_SURFACES) THEN
         BUF = 'smooth_by_element'
      ELSEIF (IOP .EQ. C_RMESH_REGULARISE_BARYCENTRIQUE) THEN
         BUF = 'smooth_barycentric'
      ELSEIF (IOP .EQ. C_RMESH_REGULARISE_ETOILES) THEN
         BUF = 'smooth_stars'
      ELSE
         WRITE(BUF,'(A,I6)') 'ERR_OPERATION_INVALIDE: ', IOP
      ENDIF
      L = SP_STRN_LEN(BUF)

C---     Log l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_ADAPT_OPERATION')

C---     Log les infos
      CALL LOG_INCIND()
      WRITE(LOG_BUF,'(2A)') 'MSG_OPERATION#<35>#: ',BUF(1:L)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NBR_SOMMETS#<35>#: ', NINT(VINFO(2))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,I12)') 'MSG_NBR_SURFACES#<35>#: ',NINT(VINFO(3))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_QUALITE#<35>#: ', VINFO(4)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_SOMMET_CMIN#<35>#: ', VINFO(5)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_SOMMET_CMED#<35>#: ', VINFO(6)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_SOMMET_CMAX#<35>#: ', VINFO(7)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_ARETE_CMIN#<35>#: ', VINFO(8)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_ARETE_CMED#<35>#: ', VINFO(9)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_ARETE_CMAX#<35>#: ', VINFO(10)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

      RETURN
      END

C************************************************************************
C Sommaire: Retourne le maillage adapté.
C
C Description:
C     La fonction GR_RMSH_REQGRID retourne le maillage adapté, Le maillage
C     est nouvellement créé et il est de la responsabilité de l'appelant
C     d'en disposer.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HGRO        Handle on the result (output) grid
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_REQGRID(HOBJ, HGRO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_REQGRID
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRO

      INCLUDE 'grrmsh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER   IERR
      INTEGER   IOB
      INTEGER   HLMG, HMTH
      EXTERNAL GR_RMSH_REQGRID_CB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Contrôle les paramètres
      IF (DT_GRID_HVALIDE(HGRO)) GOTO 9900

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         HLMG = GR_RMSH_HLMG(IOB)
         IF (.NOT. LM_HGEO_HVALIDE(HLMG)) GOTO 9901
      ENDIF

C---     Construis le CB
      HMTH = 0
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HMTH)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIMTH(HMTH,
     &                                      HOBJ,
     &                                      GR_RMSH_REQGRID_CB)

C---     Fait l'appel
      IERR = SO_FUNC_CBFNC(HMTH,
     &                     GR_RMSH_REQGRID_CB,
     &                     SO_ALLC_CST2B(HLMG),
     &                     SO_ALLC_CST2B(HGRO))

C---     Détruis le CB
      IF (SO_FUNC_HVALIDE(HMTH)) THEN
         IERR = SO_FUNC_DTR(HMTH)
         HMTH = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_MAILLAGE_INVALIDE'
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_ASSIGNER_MAILLAGE_EN_PREMIER'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_REQGRID = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le maillage adapté.
C
C Description:
C     La fonction GR_RMSH_REQGRID_CB retourne le maillage adapté, Le maillage
C     est nouvellement créé et il est de la responsabilité de l'appelant
C     d'en disposer.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HGRO        Handle on the result (output) grid
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_REQGRID_CB(HOBJ, HGEO, HGRO)

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGEO
      INTEGER HGRO

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER*8 XALG
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   ITPV
      INTEGER   HCOR, HELE, HNUM
      INTEGER   NNT, LCOR
      INTEGER   NELT, LELE
      REAL*8    TSIM
      REAL*8    VINI(2)
      LOGICAL   MODIF, DOINIT
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Récupère les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GR_RMSH_HBASE
         XALG = GR_RMSH_XALG(IOB)
         ITPV = GR_RMSH_ITPV(IOB)
      ENDIF

C---     Récupère les données
      GDTA => LM_HGEO_REQGDTA(HGEO)

C---     Récupère les dimensions du mailleur
      IF (ERR_GOOD()) THEN
         NNT  = C_RMESH_REQNNT (XALG, ITPV)
         NELT = C_RMESH_REQNELT(XALG, ITPV)
      ENDIF

C---     Alloue la mémoire pour les données
      LCOR = 0
      LELE = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNT * GDTA%NDIM,  LCOR)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELT* GDTA%NCELV, LELE)

C---     Charge les tables
      IF (ERR_GOOD()) THEN
         IRET=C_RMESH_REQGRO(XALG,
     &                       GDTA%NDIM,
     &                       NNT,
     &                       VA(SO_ALLC_REQVIND(VA,LCOR)),
     &                       ITPV,
     &                       GDTA%NCELV,
     &                       NELT,
     &                       KA(SO_ALLC_REQKIND(KA,LELE)))
        IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
        ENDIF
      ENDIF

C---     Construis une renumérotation identité
      HNUM = 0
      IF (ERR_GOOD()) IERR = NM_IDEN_CTR(HNUM)
      IF (ERR_GOOD()) IERR = NM_IDEN_INI(HNUM, NNT)

C---     Crée les coordonnées
      VINI(1) = 0.0D0
      VINI(2) = 0.0D0
      DOINIT = .TRUE.
      TSIM = 0.0D0
D     CALL ERR_ASR(GDTA%NDIM .LE. 2)
      IF (ERR_GOOD()) IERR=DT_VNOD_CTR   (HCOR)
      IF (ERR_GOOD()) IERR=DT_VNOD_INIDIM(HCOR,
     &                                    GDTA%NDIM,
     &                                    NNT,
     &                                    NNT,
     &                                    DOINIT,
     &                                    VINI)
      IF (ERR_GOOD()) IERR=DT_VNOD_ASGVALS(HCOR,
     &                                    HNUM,
     &                                    VA(SO_ALLC_REQVIND(VA,LCOR)),
     &                                    TSIM)

C---     Crée les connectivités
      IF (ERR_GOOD()) IERR=DT_ELEM_CTR   (HELE)
      IF (ERR_GOOD()) IERR=DT_ELEM_INIDIM(HELE,
     &                                    GDTA%ITPEV,
     &                                    GDTA%NCELV,
     &                                    GDTA%NNELV,
     &                                    NELT,
     &                                    NELT)
      IF (ERR_GOOD()) IERR=DT_ELEM_MAJVAL(HELE,
     &                                    GDTA%NCELV,
     &                                    NELT,
     &                                    KA(SO_ALLC_REQKIND(KA,LELE)),
     &                                    TSIM)

C---     Construis le maillage
      IF (ERR_GOOD()) IERR = DT_GRID_CTR(HGRO)
      IF (ERR_GOOD()) IERR = DT_GRID_INI(HGRO,
     &                                   HCOR, .TRUE., ! Do trf ownership HCOR
     &                                   HELE, .TRUE., ! Do trf ownership HELE
     &                                   0,    .TRUE., ! Do trf ownership HNUMC
     &                                   0,    .TRUE.) ! Do trf ownership HNUME

C---     Récupère la mémoire
      IF (LELE .NE. 0) IERR = SO_ALLC_ALLINT(0, LELE)
      IF (LCOR .NE. 0) IERR = SO_ALLC_ALLRE8(0, LCOR)

C---     Nettoie
      CALL ERR_PUSH()
      IF (NM_IDEN_HVALIDE(HNUM)) IERR = NM_IDEN_DTR(HNUM)
      IF (ERR_BAD()) THEN
         IF (DT_GRID_HVALIDE(HGRO)) IERR = DT_GRID_DTR(HGRO)
         IF (DT_ELEM_HVALIDE(HELE)) IERR = DT_ELEM_DTR(HELE)
         IF (DT_VNOD_HVALIDE(HCOR)) IERR = DT_VNOD_DTR(HCOR)
         HGRO = 0
      ENDIF
      CALL ERR_POP()

      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_REQGRID_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la limite adaptée.
C
C Description:
C     La fonction GR_RMSH_REQLIMT retourne la limite adaptée.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HLMTO       Handle on the result (output) limit
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_REQLIMT(HOBJ, HLMTO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GR_RMSH_REQLIMT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLMTO

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
!      INCLUDE 'log.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'grrmsh.fc'

      INTEGER*8 XALG
      INTEGER   IERR, IRET
      INTEGER   IOB
      INTEGER   HLIMT
      INTEGER   LLIM, LNOD
      INTEGER   NLIM, NNOD
      REAL*8    TSIM
      LOGICAL   MODIF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GR_RMSH_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.remesh')

C---     Contrôle les paramètres
      IF (DT_LIMT_HVALIDE(HLMTO)) GOTO 9900

C---     Récupère les attributs
      IOB = HOBJ - GR_RMSH_HBASE
      XALG = GR_RMSH_XALG(IOB)

C---     Récupère les dimensions du mailleur
      NLIM = 0
      NNOD = 0
      IF (ERR_GOOD()) THEN
         NLIM = C_RMESH_REQNCLLIM(XALG)
         NNOD = C_RMESH_REQNCLNOD(XALG)
      ENDIF

C---     Alloue la mémoire pour les données
      LLIM = 0
      LNOD = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NLIM*7, LLIM)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOD,   LNOD)

C---     Charge les tables
      IF (ERR_GOOD()) THEN
         IRET=C_RMESH_REQLMT(XALG,
     &                       NLIM,
     &                       KA(SO_ALLC_REQKIND(KA,LLIM)),
     &                       NNOD,
     &                       KA(SO_ALLC_REQKIND(KA,LNOD)))
        IF (IRET .NE. 0) THEN
            IRET = C_RMESH_ERRMSG(ERR_BUF)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
        ENDIF
      ENDIF

C---     Crée la limite
      TSIM = 0.0D0
      HLIMT = 0
      IF (ERR_GOOD()) IERR=DT_LIMT_CTR   (HLIMT)
      IF (ERR_GOOD()) IERR=DT_LIMT_INIDIM(HLIMT,
     &                                    NLIM,
     &                                    NNOD)
      IF (ERR_GOOD()) IERR=DT_LIMT_MAJVAL(HLIMT,
     &                                    NLIM,
     &                                    KA(SO_ALLC_REQKIND(KA,LLIM)),
     &                                    NNOD,
     &                                    KA(SO_ALLC_REQKIND(KA,LNOD)),
     &                                    TSIM)

C---     Récupère la mémoire
      IF (LLIM .NE. 0) IERR = SO_ALLC_ALLINT(0, LLIM)
      IF (LNOD .NE. 0) IERR = SO_ALLC_ALLINT(0, LNOD)

C---     Nettoie
      CALL ERR_PUSH()
      IF (ERR_BAD()) THEN
         IF (DT_LIMT_HVALIDE(HLIMT)) IERR = DT_LIMT_DTR(HLIMT)
      ENDIF
      CALL ERR_POP()

C---    Assigne la valeur de retour
      HLMTO = 0
      IF (ERR_GOOD()) HLMTO = HLIMT

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LIMITE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.remesh')
      GR_RMSH_REQLIMT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GR_RMSH_TRDSCN (HSCN, NSCN, LSCN)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSCN
      INTEGER NSCN
      INTEGER LSCN

      INCLUDE 'grrmsh.fi'
      INCLUDE 'c_remesh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'grrmsh.fc'

      REAL*8    VAL1, VAL2
      INTEGER   IERR, IRET
      INTEGER   IA, IND
      INTEGER   ICOD
      INTEGER   IACT, ICNT
      INTEGER   NACT, NCNT, NTOT
      INTEGER   HACT, HLST, NLST
      INTEGER   NRECMAX
      PARAMETER (NRECMAX = 5)
      INTEGER   KREC(5, NRECMAX), IREC
      CHARACTER*(64) BUF, A
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DS_LIST_HVALIDE(HSCN))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise la pile de récursion
      IREC = 0

C---     Compte
      HACT = HSCN
      IACT = 0
      NACT = DS_LIST_REQDIM(HACT)
      ICNT = 1
      NCNT = 1
      NTOT = 0
100   CONTINUE
         IACT = IACT + 1
         IF (IACT .GT. NACT) THEN      ! Test for end of list
            ICNT = ICNT + 1
            IF (ICNT .LE. NCNT) THEN   ! Next of count
               IACT = 0
               GOTO 190                ! Continue
            ELSEIF (IREC .GT. 0) THEN  ! Test stack
               IACT = KREC(1,IREC)     ! Pop
               NACT = KREC(2,IREC)
               ICNT = KREC(3,IREC)
               NCNT = KREC(4,IREC)
               HACT = KREC(5,IREC)
               IREC = IREC - 1
               GOTO 190                ! Continue
            ELSE
               GOTO 199                ! Exit loop
            ENDIF
         ENDIF

         IERR = DS_LIST_REQVAL(HACT, IACT, BUF)
         IF (ERR_BAD()) GOTO 199

         HLST = 0
         IRET = SP_STRN_TKI(BUF, ',', 1, HLST)
         IF (IRET .EQ. 0 .AND. DS_LIST_HVALIDE(HLST)) THEN
            IRET = SP_STRN_TKI(BUF, ',', 2, NLST)
            IF (IRET .NE. 0) NLST = 1
            IREC = IREC + 1         ! Push
            IF (IREC .GT. NRECMAX) GOTO 9900
            KREC(1,IREC) = IACT
            KREC(2,IREC) = NACT
            KREC(3,IREC) = ICNT
            KREC(4,IREC) = NCNT
            KREC(5,IREC) = HACT
            HACT = HLST
            IACT = 0
            NACT = DS_LIST_REQDIM(HACT)
            ICNT = 1
            NCNT = NLST
            GOTO 190
         ENDIF

         NTOT = NTOT + 1

190      CONTINUE
      IF (ERR_GOOD()) GOTO 100
199   CONTINUE
D     CALL ERR_ASR(ERR_BAD() .OR. IREC .EQ. 0)

C---     Alloue la table
      LSCN = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(4*NTOT, LSCN)

C---     Traduis les actions
      IND = 0
      HACT = HSCN
      IACT = 0
      NACT = DS_LIST_REQDIM(HACT)
      ICNT = 1
      NCNT = 1
200   CONTINUE
         IACT = IACT + 1
         IF (IACT .GT. NACT) THEN      ! Test for end of list
            ICNT = ICNT + 1
            IF (ICNT .LE. NCNT) THEN   ! Next of count
               IACT = 0
               GOTO 290                ! Continue
            ELSEIF (IREC .GT. 0) THEN  ! Test stack
               IACT = KREC(1,IREC)     ! Pop
               NACT = KREC(2,IREC)
               ICNT = KREC(3,IREC)
               NCNT = KREC(4,IREC)
               HACT = KREC(5,IREC)
               IREC = IREC - 1
               GOTO 290                ! Continue
            ELSE
               GOTO 299                ! Exit loop
            ENDIF
         ENDIF

         IERR = DS_LIST_REQVAL(HACT, IACT, BUF)
         IF (ERR_BAD()) GOTO 299

C---        Push les sous-listes
         HLST = 0
         IRET = SP_STRN_TKI(BUF, ',', 1, HLST)
         IF (IRET .EQ. 0 .AND. DS_LIST_HVALIDE(HLST)) THEN
            IRET = SP_STRN_TKI(BUF, ',', 2, NLST)
            IF (IRET .NE. 0) NLST = 1
            IREC = IREC + 1         ! Push
D           CALL ERR_ASR(IREC .LE. NRECMAX)
            KREC(1,IREC) = IACT
            KREC(2,IREC) = NACT
            KREC(3,IREC) = ICNT
            KREC(4,IREC) = NCNT
            KREC(5,IREC) = HACT
            HACT = HLST
            IACT = 0
            NACT = DS_LIST_REQDIM(HACT)
            ICNT = 1
            NCNT = NLST
            GOTO 290
         ENDIF

         IRET = 0
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(BUF, ',', 1, A)
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(BUF, ',', 2, NLST)
         IF (IRET .NE. 0) NLST = 1
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(BUF, ',', 3, VAL1)
         IF (IRET .NE. 0) VAL1 = 0.0D0
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(BUF, ',', 4, VAL2)
         IF (IRET .NE. 0) VAL2 = 0.0D0

         IA = SP_STRN_LEN(A)
         ICOD = C_RMESH_INDEFINI
         IF (A(1:IA) .EQ. 'smooth_metrics') THEN
            ICOD = C_RMESH_LISSE_METRIQUES
!!!         ELSEIF (A(1:IA) .EQ. 'rescale_metrics') THEN       ! pas implanté
!!!            ICOD = C_RMESH_RESCALE_METRIQUES
!!!         ELSEIF (A(1:IA) .EQ. 'mesh_from_boundary') THEN    ! ne s'applique pas
!!!            ICOD = C_RMESH_MAILLE_PEAU
         ELSEIF (A(1:IA) .EQ. 'swap_edges') THEN
            ICOD = C_RMESH_RETOURNE_ARETES
         ELSEIF (A(1:IA) .EQ. 'cure_skin') THEN
            ICOD = C_RMESH_SOIGNE_PEAU
         ELSEIF (A(1:IA) .EQ. 'cure_bridges') THEN
            ICOD = C_RMESH_SOIGNE_PONT
         ELSEIF (A(1:IA) .EQ. 'coarsen_by_vertex') THEN
            ICOD = C_RMESH_DERAFFINE_PAR_SOMMETS
         ELSEIF (A(1:IA) .EQ. 'refine_by_edges') THEN
            ICOD = C_RMESH_RAFFINE_PAR_ARETES
         ELSEIF (A(1:IA) .EQ. 'refine_by_element') THEN
            ICOD = C_RMESH_RAFFINE_PAR_SURFACES
         ELSEIF (A(1:IA) .EQ. 'smooth_by_vertex') THEN
            ICOD = C_RMESH_REGULARISE_PAR_ARETES
         ELSEIF (A(1:IA) .EQ. 'smooth_by_element') THEN
            ICOD = C_RMESH_REGULARISE_PAR_SURFACES
         ELSEIF (A(1:IA) .EQ. 'smooth_barycentric') THEN
            ICOD = C_RMESH_REGULARISE_BARYCENTRIQUE
         ELSEIF (A(1:IA) .EQ. 'smooth_stars') THEN
            ICOD = C_RMESH_REGULARISE_ETOILES
         ELSE
            WRITE(ERR_BUF,'(2A)') 'ERR_SCENARIO_INVALIDE: ', A(1:IA)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF

         IND = IND + 1
         IF (ERR_GOOD()) IERR = SO_ALLC_ASGRE8(LSCN, IND, DBLE(ICOD))
         IND = IND + 1
         IF (ERR_GOOD()) IERR = SO_ALLC_ASGRE8(LSCN, IND, DBLE(NLST))
         IND = IND + 1
         IF (ERR_GOOD()) IERR = SO_ALLC_ASGRE8(LSCN, IND, VAL1)
         IND = IND + 1
         IF (ERR_GOOD()) IERR = SO_ALLC_ASGRE8(LSCN, IND, VAL2)

290      CONTINUE
      IF (ERR_GOOD()) GOTO 200
299   CONTINUE
D     CALL ERR_ASR(ERR_BAD() .OR. IND .EQ. 4*NTOT)

C---     Nettoie cas d'erreur
      IF (ERR_GOOD()) THEN
         NSCN = NTOT
      ELSE
         CALL ERR_PUSH()
         IERR = SO_ALLC_ALLRE8(0, LSCN)
         CALL ERR_POP()
         LSCN = 0
         NSCN = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_ZONE_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GR_RMSH_TRDSCN = ERR_TYP()
      RETURN
      END
