C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GOT3_CLCGRAD
C     INTEGER LM_GOT3_CLCDDX
C     INTEGER LM_GOT3_CLCDDY
C     INTEGER LM_GOT3_CLCHESS
C     INTEGER LM_GOT3_ASMDDX
C     INTEGER LM_GOT3_ASMDDY
C     INTEGER LM_GOT3_ASMML
C     INTEGER LM_GOT3_KUEHSSV
C     INTEGER LM_GOT3_KUEHSSS
C     INTEGER LM_GOT3_KUEGRDV
C     INTEGER LM_GOT3_KUEDDX
C     INTEGER LM_GOT3_KUEDDY
C     INTEGER LM_GOT3_KUEML
C   Private:
C
C************************************************************************

C**************************************************************
C Sommaire: LM_GOT3_CLCGRAD
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_CLCGRAD(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VDX,
     &                         VDY,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_CLCGRAD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_CLCGRAD

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN
      REAL*8  COEF

      INTEGER LM_GOT3_KUEGRDV
      INTEGER LM_GOT3_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble le gradient et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEGRDV(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VDX,
     &                          VDY)
         IERR = LM_GOT3_KUEML  (KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,x et F,y
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VML(IN) = UN / VML(IN)
      ENDDO
!$omp  end do
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDX(IN) = VDX(IN) * VML(IN)
      ENDDO
!$omp  end do
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDY(IN) = VDY(IN) * VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel

      LM_GOT3_CLCGRAD = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_CLCDDX
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_CLCDDX(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VNO,
     &                        VDX,
     &                        VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_CLCDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_CLCDDX

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN

      INTEGER LM_GOT3_KUEDDX
      INTEGER LM_GOT3_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble F,x et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEDDX(KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VNO,
     &                         VDX)
         IERR = LM_GOT3_KUEML (KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,x
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDX(IN) = VDX(IN) / VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel

      LM_GOT3_CLCDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_CLCDDY
C
C Description:
C     Calcul du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_CLCDDY(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VNO,
     &                        VDY,
     &                        VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_CLCDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_CLCDDY

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE, IN

      INTEGER LM_GOT3_KUEDDY
      INTEGER LM_GOT3_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IERR)

C---     Assemble F,y et Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEDDY(KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VNO,
     &                         VDY)
         IERR = LM_GOT3_KUEML (KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Résous pour F,y
!$omp  do
      DO IN=1,EG_CMMN_NNL
         VDY(IN) = VDY(IN) / VML(IN)
      ENDDO
!$omp  end do

!$omp  end parallel

      LM_GOT3_CLCDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_CLCHESS
C
C Description:
C     Calcul du Hessien par le formule de Green.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C****************************************************************
      FUNCTION LM_GOT3_CLCHESS(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VHS,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_CLCHESS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_CLCHESS

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO   (EG_CMMN_NNL)
      REAL*8   VHS   (3, EG_CMMN_NNL)
      REAL*8   VML   (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  COEF
      INTEGER IERR
      INTEGER IC, IE, IN
      INTEGER IES, IEV

      INTEGER LM_GOT3_KUEHSSV
      INTEGER LM_GOT3_KUEHSSS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE, IN)
!$omp& private(IES, IEV)
!$omp& private(IERR)
!$omp& private(COEF)

C---     Assemble les termes de volume
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEHSSV(KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VNO,
     &                          VHS,
     &                          VML)

      ENDDO
!$omp  end do
      ENDDO

C---     Assemble les termes de contour
      DO IES=1,EG_CMMN_NELS

         IEV = KNGS(3,IES)
         IERR = LM_GOT3_KUEHSSS(KNGV(1,IEV),
     &                          KNGS(1,IES),
     &                          VDJV(1,IEV),
     &                          VDJS(1,IES),
     &                          VNO,
     &                          VHS,
     &                          VML)

      ENDDO

C---     Finalise
!$omp  do
      DO IN=1,EG_CMMN_NNL
         COEF = UN / VML(IN)
         VHS(1,IN) = VHS(1,IN) * COEF
         VHS(2,IN) = VHS(2,IN) * COEF
         VHS(3,IN) = VHS(3,IN) * COEF
      ENDDO
!$omp  end do

!$omp  end parallel

      LM_GOT3_CLCHESS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_ASMDDX
C
C Description:
C     Assemble la dérivée en x
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_ASMDDX(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VNO,
     &                        VDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_ASMDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_ASMDDX

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LM_GOT3_KUEDDX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble F,x
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEDDX(KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VNO,
     &                         VDX)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LM_GOT3_ASMDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_ASMDDY
C
C Description:
C     Assemble la dérivée en y
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_ASMDDY(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VNO,
     &                        VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_ASMDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_ASMDDY

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LM_GOT3_KUEDDY
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble F,y
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEDDY(KNGV(1,IE),
     &                         VDJV(1,IE),
     &                         VNO,
     &                         VDY)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LM_GOT3_ASMDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_ASMML
C
C Description:
C     Assemble la matrice masse lumpée
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_ASMML(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_ASMML
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_ASMML

      INTEGER  KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS(EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV(EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS(EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IC, IE

      INTEGER LM_GOT3_KUEML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

!$omp  parallel
!$omp& default(shared)
!$omp& private(IC, IE)
!$omp& private(IERR)

C---     Assemble Ml
      DO IC=1,EG_CMMN_NELCOL
!$omp  do
      DO IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

         IERR = LM_GOT3_KUEML  (KNGV(1,IE),
     &                          VDJV(1,IE),
     &                          VML)

      ENDDO
!$omp  end do
      ENDDO

!$omp  end parallel

      LM_GOT3_ASMML = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEHSSV
C
C Description:
C     Contribution élémentaire de volume du Hessien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C****************************************************************
      FUNCTION LM_GOT3_KUEHSSV(KNGE,
     &                         VDJE,
     &                         VNO,
     &                         VHS,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEHSSV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEHSSV

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VHS (3, EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  VKX, VEX, VSX
      REAL*8  VKY, VEY, VSY
      REAL*8  DJT3, COEF
      REAL*8  F1, F2, F3, FX, FY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)
      VSX = -(VKX+VEX)
      VKY = VDJE(3)
      VEY = VDJE(4)
      VSY = -(VKY+VEY)
      DJT3 = VDJE(5)

C---     Connectivités
      NO1 = KNGE(1)
      NO2 = KNGE(2)
      NO3 = KNGE(3)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)

C---     Assemble le Hessien
      COEF = UN_2 / DJT3
      FX = COEF * (VKX*(F2-F1) + VEX*(F3-F1))         ! F,x
      FY = COEF * (VKY*(F2-F1) + VEY*(F3-F1))         ! F,y
      VHS(1,NO1) = VHS(1,NO1) - VSX*FX                ! F,xx
      VHS(2,NO1) = VHS(2,NO1) - UN_2*(VSX*FY+VSY*FX)  ! F,xy
      VHS(3,NO1) = VHS(3,NO1) - VSY*FY                ! F,yy
      VHS(1,NO2) = VHS(1,NO2) - VKX*FX
      VHS(2,NO2) = VHS(2,NO2) - UN_2*(VKX*FY+VKY*FX)
      VHS(3,NO2) = VHS(3,NO2) - VKY*FY
      VHS(1,NO3) = VHS(1,NO3) - VEX*FX
      VHS(2,NO3) = VHS(2,NO3) - UN_2*(VEX*FY+VEY*FX)
      VHS(3,NO3) = VHS(3,NO3) - VEY*FY

C---     Matrice masse lumpée
      COEF = UN_6 * DJT3
      VML(NO1) = VML(NO1) + COEF
      VML(NO2) = VML(NO2) + COEF
      VML(NO3) = VML(NO3) + COEF

      LM_GOT3_KUEHSSV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEHSSS
C
C Description:
C     Contribution élémentaire de surface du Hessien.
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C
C****************************************************************
      FUNCTION LM_GOT3_KUEHSSS(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VNO,
     &                         VHS,
     &                         VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEHSSS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEHSSS

      INTEGER  KNGV(EG_CMMN_NCELV)
      INTEGER  KNGS(EG_CMMN_NCELS)
      REAL*8   VDJV(EG_CMMN_NDJV)
      REAL*8   VDJS(EG_CMMN_NDJS)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VHS (3, EG_CMMN_NNL)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  VKX, VEX, VNX
      REAL*8  VKY, VEY, VNY
      REAL*8  DJT3, DJL2, COEF
      REAL*8  F1, F2, F3, FX, FY, FXX, FXY, FYY
      INTEGER NO1, NO2, NO3
      INTEGER NP1, NP2
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Calcul des termes de contour
      NP1 = KNGS(1)
      NP2 = KNGS(2)
      NO1 = KNGV(1)
      NO2 = KNGV(2)
      NO3 = KNGV(3)

C---     Métriques de l'élément - composantes de la normale extérieure
      VNY  = -VDJS(1)
      VNX  =  VDJS(2)
      DJL2 =  VDJS(3)

C---        Métriques du T3 parent
      VKX  = VDJV(1)      ! Ksi,x
      VEX  = VDJV(2)      ! Eta,x
      VKY  = VDJV(3)      ! Ksi,y
      VEY  = VDJV(4)      ! Eta,y
      DJT3 = VDJV(5)

C---     Degrés de liberté
      COEF = DJL2 / DJT3
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)
      FX = COEF * (VKX*(F2-F1) + VEX*(F3-F1))   ! F,x
      FY = COEF * (VKY*(F2-F1) + VEY*(F3-F1))   ! F,y

C---     Assemble le Hessien
      FXX = VNX*FX                              ! F,x . nx
      FXY = UN_2*(VNX*FY + VNY*FX)              ! F,xy
      FYY = VNY*FY                              ! F,y . ny
      VHS(1,NP1) = VHS(1,NP1) + FXX
      VHS(2,NP1) = VHS(2,NP1) + FXY
      VHS(3,NP1) = VHS(3,NP1) + FYY
      VHS(1,NP2) = VHS(1,NP2) + FXX
      VHS(2,NP2) = VHS(2,NP2) + FXY
      VHS(3,NP2) = VHS(3,NP2) + FYY

C---     Matrice masse lumpée
      COEF = DJL2
      VML(NP1) = VML(NP1) + COEF
      VML(NP2) = VML(NP2) + COEF

      LM_GOT3_KUEHSSS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEGRDV
C
C Description:
C     Contribution élémentaire du gradient
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_KUEGRDV(KNGE,
     &                         VDJE,
     &                         VNO,
     &                         VDX,
     &                         VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEGRDV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEGRDV

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  VKX, VEX
      REAL*8  VKY, VEY
      REAL*8  F1, F2, F3, FX, FY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)
      VKY = VDJE(3)
      VEY = VDJE(4)

C---     Connectivités
      NO1 = KNGE(1)
      NO2 = KNGE(2)
      NO3 = KNGE(3)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)

C---     Assemble
      FX = UN_6*(VKX*(F2-F1) + VEX*(F3-F1))   ! F,x
      FY = UN_6*(VKY*(F2-F1) + VEY*(F3-F1))   ! F,y
      VDX(NO1) = VDX(NO1) + FX
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO3) = VDX(NO3) + FX
      VDY(NO1) = VDY(NO1) + FY
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO3) = VDY(NO3) + FY

      LM_GOT3_KUEGRDV = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEDDX
C
C Description:
C     Contribution élémentaire de la dérivée en x
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_KUEDDX(KNGE,
     &                        VDJE,
     &                        VNO,
     &                        VDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEDDX
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEDDX

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDX (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  VKX, VEX
      REAL*8  F1, F2, F3, FX
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Métriques
      VKX = VDJE(1)
      VEX = VDJE(2)

C---     Connectivités
      NO1 = KNGE(1)
      NO2 = KNGE(2)
      NO3 = KNGE(3)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)

C---     Assemble
      FX = UN_6*(VKX*(F2-F1) + VEX*(F3-F1))   ! F,x
      VDX(NO1) = VDX(NO1) + FX
      VDX(NO2) = VDX(NO2) + FX
      VDX(NO3) = VDX(NO3) + FX

      LM_GOT3_KUEDDX = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEDDY
C
C Description:
C     Contribution élémentaire de la dérivée en y
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_KUEDDY(KNGE,
     &                        VDJE,
     &                        VNO,
     &                        VDY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEDDY
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEDDY

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VNO (EG_CMMN_NNL)
      REAL*8   VDY (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  VKY, VEY
      REAL*8  F1, F2, F3, FY
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Métriques
      VKY = VDJE(3)
      VEY = VDJE(4)

C---     Connectivités
      NO1 = KNGE(1)
      NO2 = KNGE(2)
      NO3 = KNGE(3)

C---     Degrés de liberté
      F1 = VNO(NO1)
      F2 = VNO(NO2)
      F3 = VNO(NO3)

C---     Assemble
      FY = UN_6*(VKY*(F2-F1) + VEY*(F3-F1))   ! F,y
      VDY(NO1) = VDY(NO1) + FY
      VDY(NO2) = VDY(NO2) + FY
      VDY(NO3) = VDY(NO3) + FY

      LM_GOT3_KUEDDY = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LM_GOT3_KUEML
C
C Description:
C     Contribution élémentaire de matrice masse lumpée
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION LM_GOT3_KUEML(KNGE,
     &                       VDJE,
     &                       VML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_KUEML
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GOT3_KUEML

      INTEGER  KNGE(EG_CMMN_NCELV)
      REAL*8   VDJE(EG_CMMN_NDJV)
      REAL*8   VML (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  DJT3
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Métriques
      DJT3= UN_6 * VDJE(5)

C---     Connectivités
      NO1 = KNGE(1)
      NO2 = KNGE(2)
      NO3 = KNGE(3)

C---     Matrice masse lumpée
      VML(NO1) = VML(NO1) + DJT3
      VML(NO2) = VML(NO2) + DJT3
      VML(NO3) = VML(NO3) + DJT3

      LM_GOT3_KUEML = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LM_GOT3_LOCE
C
C Description:
C     La fonction LM_GOT3_LOCE localise l'élément sous les points
C     de la liste VCORP. Elle retourne dans KELEP les numéros d'élément
C     et dans VCORE les coordonnées sur l'élément de référence du point.
C     Si un point n'est pas trouvé son numéro d'élément est 0.
C
C Entrée:
C     NPNT        Nombre de points
C     VCORP       Coordonnées des points
C
C Sortie:
C     KELEP       Table des numéros d'élément
C     VCORE       Coordonnées sur l'élément de référence
C
C Notes:
C************************************************************************
      FUNCTION LM_GOT3_LOCE(TOL,
     &                      VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VCORP,
     &                      KELEP,
     &                      VCORE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT3_LOCE
CDEC$ ENDIF

      IMPLICIT NONE
      
      INTEGER LM_GOT3_LOCE

      REAL*8,  INTENT(IN) :: TOL
      REAL*8,  INTENT(IN) :: VCORG(:, :)
      INTEGER, INTENT(IN) :: KNGV (:, :)
      REAL*8,  INTENT(IN) :: VDJV (:, :)
      REAL*8,  INTENT(IN) :: VCORP(:, :)
      INTEGER, INTENT(OUT):: KELEP(:)
      REAL*8,  INTENT(OUT):: VCORE(:, :)

      INCLUDE 'err.fi'

      REAL*8, PARAMETER :: ZERO = 0.0D0
      REAL*8, PARAMETER :: UN   = 1.0D0

      REAL*8  VKX, VEX, VKY, VEY, DETJ
      REAL*8  X0X1, Y0Y1
      REAL*8  KSI,  ETA,  LAMBDA, DIST
      REAL*8  KOLD, EOLD, LOLD,   DOLD
      INTEGER IE, IP
      INTEGER NO1
      INTEGER NELV, NPNT
      INTEGER NRESTE
      LOGICAL DEDANS
C----------------------------------------------------------------------

      NELV = SIZE(KNGV,  2)
      NPNT = SIZE(VCORP, 2)
      KELEP(:) = 0
      NRESTE = NPNT

      DO IE=1,NELV
         NO1 = KNGV(1,IE)

C---        Les métriques
         VKX  = VDJV(1,IE)           ! Ksi,x
         VEX  = VDJV(2,IE)           ! Eta,x
         VKY  = VDJV(3,IE)           ! Ksi,y
         VEY  = VDJV(4,IE)           ! Eta,y
         DETJ = VDJV(5,IE)           ! Det J

C---        Les points
         DO IP=1,NPNT
            IF (KELEP(IP) .GT. 0) GOTO 199

            X0X1 = VCORP(1,IP) - VCORG(1,NO1)
            Y0Y1 = VCORP(2,IP) - VCORG(2,NO1)

C---           Coordonnées sur l'élément de référence
            KSI = (VKX * X0X1 + VKY * Y0Y1) / DETJ
            ETA = (VEY * Y0Y1 + X0X1 * VEX) / DETJ
            LAMBDA = UN - KSI - ETA

            DEDANS = (KSI .GE. ZERO) .AND.
     &               (ETA .GE. ZERO) .AND.
     &               (LAMBDA .GE. ZERO)
            IF (DEDANS) THEN
               KELEP(IP)   = IE
               VCORE(1,IP) = KSI
               VCORE(2,IP) = ETA
               NRESTE = NRESTE - 1
            ELSEIF (KELEP(IP) .EQ. 0) THEN
               KELEP(IP)   = -IE
               VCORE(1,IP) = KSI
               VCORE(2,IP) = ETA
            ELSEIF (KELEP(IP) .LT. 0) THEN
               KOLD = VCORE(1,IP)
               EOLD = VCORE(2,IP)
               LOLD = UN - KOLD - EOLD
               DOLD = ABS( MIN(KOLD, EOLD, LOLD) )
               DIST = ABS( MIN(KSI,  ETA,  LAMBDA) )
               IF (DIST .LT. DOLD) THEN
                  KELEP(IP)   = -IE
                  VCORE(1,IP) = KSI
                  VCORE(2,IP) = ETA
               ENDIF
            ENDIF

199         CONTINUE
         ENDDO

         IF (NRESTE .LE. 0) GOTO 200
      ENDDO
200   CONTINUE

C---     Apply tolerance
      DO IP=1,NPNT
         IF (KELEP(IP) .LT. 0) THEN
            KSI = VCORE(1,IP)
            ETA = VCORE(2,IP)
            LAMBDA = UN - KSI - ETA
            DIST = ABS( MIN(KSI, ETA, LAMBDA) )
            IF (DIST .LE. TOL) KELEP(IP) = -KELEP(IP)
         ENDIF
      ENDDO

      LM_GOT3_LOCE = ERR_TYP()
      RETURN
      END
      