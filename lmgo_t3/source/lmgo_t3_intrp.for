C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T3_INTRP
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T3_INTRP
C
C Description:
C     La fonction LMGO_T3_INTRP interpole les valeurs de VSRC dans VDST à
C     la position des NPNT points localisés par leur élément dans KELE et
C     leur coordonnées sur l'élément de référence VCORE.
C
C Entrée:
C     NCELV       ! Nombre de connectivités par élément
C     NELV        ! Nombre d'éléments de volume
C     KNGV        ! Table des connectivités de volume
C     NDIM        ! Nombre de dimensions
C     NPNT        ! Nombre de points à interpoler
C     KDDL        ! Table des DDL à interpoler
C     KELE        ! Table des éléments des points
C     VCORE       ! Coord de ref. des points
C     NDLNS       ! Dim 1 de la table d'entrée
C     NPNTS       ! Dim 2 de la table d'entrée
C     INDXS       ! Index de la table d'entrée à interpoler
C     VSRC        ! Table des données d'entrée à interpoler
C     NDLND       ! Dim 1 de la table de sortie
C     NPNTD       ! Dim 2 de la table de sortie
C     INDXD       ! Index de la table de sortie interpolée
C
C Sortie:
C     VDST        ! Table des valeurs interpolées
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_INTRP (NCELV,
     &                        NELV,
     &                        KNGV,
     &                        NDIM,
     &                        NPNT,
     &                        KELE,
     &                        VCORE,
     &                        NDLNS,
     &                        NPNTS,
     &                        INDXS,
     &                        VSRC,
     &                        NDLND,
     &                        NPNTD,
     &                        INDXD,
     &                        VDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_INTRP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NDIM
      INTEGER NPNT
      INTEGER KELE (NPNT)
      REAL*8  VCORE(NDIM, NPNT)
      INTEGER NDLNS
      INTEGER NPNTS
      INTEGER INDXS
      REAL*8  VSRC (NDLNS, NPNTS)
      INTEGER NDLND
      INTEGER NPNTD
      INTEGER INDXD
      REAL*8  VDST (NDLND, NPNTD)

      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      REAL*8  KSI, ETA
      REAL*8  V1, V2, V3
      INTEGER IP, IE
      INTEGER NO1, NO2, NO3
C----------------------------------------------------------------------

      DO IP=1,NPNT
         IE = KELE(IP)
         IF (IE .LE. 0) GOTO 199

         KSI = VCORE(1, IP)
         ETA = VCORE(2, IP)

         NO1 = KNGV(1, IE)
         NO2 = KNGV(2, IE)
         NO3 = KNGV(3, IE)

         V1 = VSRC(INDXS, NO1)
         V2 = VSRC(INDXS, NO2)
         V3 = VSRC(INDXS, NO3)

         VDST(INDXD, IP) = V1 + KSI*(V2-V1) + ETA*(V3-V1)

199      CONTINUE
      ENDDO

      LMGO_T3_INTRP = ERR_TYP()
      RETURN
      END
