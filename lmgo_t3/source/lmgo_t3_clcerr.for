C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T3_CLCERR
C     INTEGER LMGO_T3_ERRRCOV
C   Private:
C     INTEGER LM_GO_T3_ERRRCOV_DIMMAT
C     INTEGER LM_GO_T3_ERRRCOV_ASMDIM
C     INTEGER LM_GO_T3_ERRRCOV_ASMIND
C     INTEGER LMGO_T3_ERRHESS
C     INTEGER LMGO_T3_ERRDXDX
C     INTEGER LMGO_T3_ERRRCOV1
C     INTEGER LMGO_T3_ERRRCOV2
C
C************************************************************************

C**************************************************************
C Sommaire: LMGO_T3_CLCERR
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     ÉQUATION : NORME DE LA 2. DERIVÉE
C
C Entrée:
C     VCORG       Table des COoRdonnées Globales
C     KNGV        Table des coNectivités Globales de Volume
C     KNGS        Table des coNectivités Globales de Surface
C     VDJV        Table des métriques de l'élément de Volume
C     VDJS        Table des métriques de l'élément de Surface
C     VFRM        Table des valeurs nodales de la fonction
C     ITPCLC      Bit-set du type de calcul
C
C Sortie:
C     VHESS       Table du hessien ou de l'ellipse
C     VHNRM       Table de la norme du hessien
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T3_CLCERR (VCORG,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VFRM,
     &                         VHESS,
     &                         VHNRM,
     &                         ITPCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (3, EG_CMMN_NNL)
      REAL*8   VHNRM (EG_CMMN_NNL)
      INTEGER  ITPCLC

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IN

      INTEGER LMGO_T3_ERRHESS
      INTEGER LMGO_T3_ERRDXDX
      INTEGER LMGO_T3_ERRRCOV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Initialise
      CALL DINIT (3*EG_CMMN_NNL, ZERO, VHESS, 1)
      CALL DINIT (  EG_CMMN_NNL, ZERO, VHNRM, 1)

C---     Calcule le Hessien
      IF     (BTEST(ITPCLC, EG_TPERR_HESSGREEN)) THEN
         IERR = LMGO_T3_ERRHESS(KNGV,KNGS,VDJV,VDJS,VFRM,VHESS,VHNRM)
      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSDXDX)) THEN
         IERR = LMGO_T3_ERRDXDX(KNGV,KNGS,VDJV,VDJS,VFRM,VHESS,VHNRM)
      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSRECOV)) THEN
         IERR = LMGO_T3_ERRRCOV(VCORG,KNGV,VFRM,VHESS)
      ELSE
         CALL ERR_ASR(.FALSE.)
      ENDIF

C---     Transforme le Hessien
      IF     (BTEST(ITPCLC, EG_TPERR_NRMHESS)) THEN
         DO IN=1,EG_CMMN_NNL
            VHNRM(IN) = SQRT(VHESS(1,IN)*VHESS(1,IN) +
     &                       VHESS(2,IN)*VHESS(2,IN)*DEUX +
     &                       VHESS(3,IN)*VHESS(3,IN))
         ENDDO
      ELSEIF (BTEST(ITPCLC, EG_TPERR_ELLIPSE)) THEN
         CALL SP_ELEM_HESSELLPS(3, EG_CMMN_NNL, VHESS)
      ENDIF

      LMGO_T3_CLCERR = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T3_ERRHESS
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C  Les termes de contour:
C     {N} . d/dx(du/dx) = - {N,x}<N,x>{u} + {N}<N,x>{u}.nx
C     {N} . d/dx(du/dy) = - {N,x}<N,y>{u} + {N}<N,y>{u}.nx
C     {N} . d/dy(du/dy) = - {N,y}<N,x>{u} + {N}<N,x>{u}.ny
C     {N} . d/dy(du/dy) = - {N,y}<N,y>{u} + {N}<N,y>{u}.ny
C****************************************************************
      FUNCTION LMGO_T3_ERRHESS(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VFRM,
     &                         VHESS,
     &                         VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T3_ERRHESS

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (3, EG_CMMN_NNL)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR

      INTEGER LM_GOT3_CLCHESS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Calcule le Hessien
      IERR = LM_GOT3_CLCHESS(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VFRM,
     &                       VHESS,
     &                       VTRV)

      LMGO_T3_ERRHESS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T3_ERRDXDX
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage.
C     Calcul du hessien par double dérivation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La table est reçue inversée (nnl,3) plutôt que (3,nnl) pour
C     faciliter les calculs. A la fin elle est rebasculée.
C****************************************************************
      FUNCTION LMGO_T3_ERRDXDX(KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VFRM,
     &                         VHESS,
     &                         VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T3_ERRDXDX

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (EG_CMMN_NNL, 3)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IN

      INTEGER LM_GOT3_CLCDDX
      INTEGER LM_GOT3_CLCDDY
      INTEGER LM_GOT3_ASMDDX
      INTEGER LM_GOT3_ASMDDY
      INTEGER LM_GOT3_ASMML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 3)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 3)
C-----------------------------------------------------------------------

C---     Calcule f,y dans VTRV
D     CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
D     CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,1), 1)
      IERR = LM_GOT3_CLCDDY(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VFRM,
     &                      VTRV,
     &                      VHESS(1,1))

C---     Assemble f,yy dans VHESS(.,3)
D     CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,3), 1)
      IERR = LM_GOT3_ASMDDY(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VTRV,
     &                      VHESS(1,3))

C---     Assemble f,xy dans VHESS(.,2)
D     CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,2), 1)
      IERR = LM_GOT3_ASMDDX(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VTRV,
     &                      VHESS(1,2))

C---     Calcule f,x dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,1), 1)
      IERR = LM_GOT3_CLCDDX(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VFRM,
     &                      VTRV,
     &                      VHESS(1,1))

C---     Assemble f,xx dans VHESS(.,1)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,1), 1)
      IERR = LM_GOT3_ASMDDX(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VTRV,
     &                      VHESS(1,1))

C---     Assemble f,xy dans VHESS(.,2)
      IERR = LM_GOT3_ASMDDY(KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VTRV,
     &                      VHESS(1,2))

C---     Assemble Ml dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      IERR = LM_GOT3_ASMML (KNGV,
     &                      KNGS,
     &                      VDJV,
     &                      VDJS,
     &                      VTRV)

C---     Finalise
      DO IN=1,EG_CMMN_NNL
         VTRV(IN) = UN / VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,1) = VHESS(IN,1) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,2) = UN_2 * VHESS(IN,2) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,3) = VHESS(IN,3) * VTRV(IN)
      ENDDO

C---     Transpose la table VHESS
      CALL TRANS(VHESS,EG_CMMN_NNL, 3, 3*EG_CMMN_NNL,
     &           VTRV, EG_CMMN_NNL, IERR)
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_TRANSPOSE_TABLE')
      ENDIF

      LMGO_T3_ERRDXDX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: LMGO_T3_ERRRCOV
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage.
C     Calcul du hessien par "least-square polynomial fit' i.e. recovery.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La fonction est exportée car elle est utilisée par d'autres comme
C     fonction utilitaire.
C************************************************************************
      FUNCTION LMGO_T3_ERRRCOV(VCORG,
     &                         KNGV,
     &                         VFRM,
     &                         VHESS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T3_ERRRCOV
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T3_ERRRCOV

      REAL*8,  INTENT(IN) :: VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER, INTENT(IN) :: KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8,  INTENT(IN) :: VFRM  (EG_CMMN_NNL)
      REAL*8,  INTENT(OUT):: VHESS (3, EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER NEQL, NKGP
      INTEGER L_IA, L_JA

      INTEGER LMGO_T3_ERRRCOV1
      INTEGER LMGO_T3_ERRRCOV2
C----------------------------------------------------------------------

C---     Assemble les indices
      IF (ERR_GOOD())
     &   IERR = LMGO_T3_ERRRCOV1(KNGV,
     &                           NEQL,
     &                           NKGP,
     &                           L_IA,
     &                           L_JA)

C---     Assemble les indices
      IF (ERR_GOOD())
     &   IERR = LMGO_T3_ERRRCOV2(VCORG,
     &                           VFRM,
     &                           VHESS,
     &                           NEQL,
     &                           NKGP,
     &                           KA(SO_ALLC_REQKIND(KA,L_IA)),
     &                           KA(SO_ALLC_REQKIND(KA,L_JA)))

      IF (SO_ALLC_HEXIST(L_JA)) IERR = SO_ALLC_ALLINT(0, L_JA)
      IF (SO_ALLC_HEXIST(L_IA)) IERR = SO_ALLC_ALLINT(0, L_IA)

      LMGO_T3_ERRRCOV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_ERRRCOV1(KNGV,
     &                          NEQL,
     &                          NKGP,
     &                          L_IA,
     &                          L_JA)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T3_ERRRCOV1

      INTEGER, INTENT(IN)  :: KNGV(EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER, INTENT(OUT) :: NEQL
      INTEGER, INTENT(OUT) :: NKGP
      INTEGER, INTENT(OUT) :: L_IA
      INTEGER, INTENT(OUT) :: L_JA

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      INTEGER I
      INTEGER NDLN
      INTEGER, DIMENSION(:), ALLOCATABLE :: KLOCN

      INTEGER LM_GO_T3_ERRRCOV_DIMMAT
C----------------------------------------------------------------------

C---     Paramètres de la simulation
      NDLN = 1
      NEQL = EG_CMMN_NNL

C---     Alloue la table de travail
      ALLOCATE(KLOCN(NEQL), STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Assemble les indices
      IF (ERR_GOOD()) THEN

C---        Initialise KTRV comme KLOCN, comme (1, 2, 3, 4...)
         DO I=1,NEQL       ! KLOCN(1:NEQL) = (/ (I, I=1,NEQL) /)
            KLOCN(I) = I   ! provoque un stack overflow (Intel 17)
         ENDDO

C---        Cherche les voisins via la structure d'une matrice morse
         IERR = LM_GO_T3_ERRRCOV_DIMMAT(EG_CMMN_NNL,
     &                                  NDLN,
     &                                  EG_CMMN_NNELV,
     &                                  EG_CMMN_NCELV,
     &                                  EG_CMMN_NELV,
     &                                  NEQL,
     &                                  KLOCN,
     &                                  KNGV,
     &                                  NKGP,
     &                                  L_IA,
     &                                  L_JA)
      ENDIF

C---        Récupère l'espace
      IF (ALLOCATED(KLOCN)) DEALLOCATE(KLOCN)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      LMGO_T3_ERRRCOV1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T3_ERRRCOV2(VCORG,
     &                          VFRM,
     &                          VHESS,
     &                          NEQL,
     &                          NKGP,
     &                          IA,
     &                          JA)

      USE SP_PFIT_M
      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T3_ERRRCOV2

      REAL*8,  INTENT(IN) :: VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      REAL*8,  INTENT(IN) :: VFRM  (EG_CMMN_NNL)
      REAL*8,  INTENT(OUT):: VHESS (3, EG_CMMN_NNL)
      INTEGER, INTENT(IN) :: NEQL
      INTEGER, INTENT(IN) :: NKGP
      INTEGER, INTENT(IN) :: IA(NEQL+1)
      INTEGER, INTENT(IN) :: JA(NKGP)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER, PARAMETER :: NP_MAX = 128

      INTEGER IERR, IRET
      INTEGER I, J
      INTEGER IN1, IN1_1, IN1_2
      INTEGER IN2, IN2_1, IN2_2
      INTEGER NP
      INTEGER :: KTRV(NP_MAX)
      REAL*8  :: XY(2,NP_MAX)
      REAL*8  :: Z(NP_MAX)
      TYPE(SP_PFIT_T) :: PFIT
C----------------------------------------------------------------------

      IERR = ERR_OK

!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(KTRV, XY, Z, PFIT)
!$omp& private(I, J, IN1, IN2, IN1_1, IN1_2, IN2_1, IN2_2, NP)
!$omp& reduction(MIN: IRET)

      IRET = SP_PFIT_CTR(PFIT, SP_PFIT_ITYP_QUAD)

C---     Boucle sur les noeuds
      NP = NP_MAX
!$omp do
      DO IN1=1,EG_CMMN_NNL
         KTRV(1:NP) = 0

C---        Cherche les voisins
         IN1_1 = IA(IN1)
         IN1_2 = IA(IN1+1)-1
         NP  = IN1_2 - IN1_1 + 1
         KTRV(1:NP) = JA(IN1_1:IN1_2)      ! 1ère couche
         DO I=IN1_1,IN1_2
            IN2 = JA(I)
            IF (IN2 .EQ. IN1) CYCLE

            IN2_1 = IA(IN2)
            IN2_2 = IA(IN2+1)-1
            DO J=IN2_1,IN2_2
               IF (.NOT. ANY(KTRV(1:NP) .EQ. JA(J))) THEN
                  NP = MIN(NP+1, NP_MAX)
                  KTRV(NP) = JA(J)
               ENDIF
            ENDDO
         ENDDO

C---        Gather les coords
         IF (NP .EQ. NP_MAX) THEN
            WRITE(LOG_BUF,*) 'MSG_NGHBRS_BUF_FULL: Node = ', IN1
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         XY(:,:) = VCORG(:,KTRV(:))
         Z(:)    = VFRM(KTRV(:))

C---        Calcule le Hessien
         IF (IRET .EQ. 0) IRET = SP_PFIT_CALC(PFIT, NP, XY, Z)
         IF (IRET .EQ. 0) THEN
            IRET = SP_PFIT_D2FDXX(PFIT, 1, VCORG(1,IN1), VHESS(1,IN1))
            IRET = SP_PFIT_D2FDXY(PFIT, 1, VCORG(1,IN1), VHESS(2,IN1))
            IRET = SP_PFIT_D2FDYY(PFIT, 1, VCORG(1,IN1), VHESS(3,IN1))
         ENDIF
C---        Sort
!!         IF (IRET .NE. 0) EXIT    ! OMP: Illegal to branch out
      ENDDO
!$omp end do

      IF     (IRET .EQ. SP_PFIT_ERR_INVALID_TYPE) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PFIT_TYPE_INVALIDE')
      ELSEIF (IRET .EQ. SP_PFIT_ERR_INVALID_STRUCT) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PFIT_STRUCTURE_INVALIDE')
      ELSEIF (IRET .EQ. SP_PFIT_ERR_INVALID_NBR_POINTS) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PFIT_NBR_POINTS_INVALIDE')
      ELSEIF (IRET .EQ. SP_PFIT_ERR_ALLOCATION_ERROR) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PFIT_ALLOCATION')
      ENDIF

      IRET = SP_PFIT_DTR(PFIT)
      IERR = ERR_OMP_RDC()
!$omp end parallel

      LMGO_T3_ERRRCOV2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne et initialise la matrice.
C
C Description:
C     La méthode LM_GO_T3_ERRRCOV_DIMMAT dimensionne la matrice.
C     Elle fait les appels pour assembler les dimensions et les indices.
C     Elle initialise l'espace de la matrice.
C     La méthode doit être appelée avant tout calcul sur la matrice
C     afin de l'initialiser.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GO_T3_ERRRCOV_DIMMAT(NNL,
     &                                 NDLN,
     &                                 NNEL,
     &                                 NCEL,
     &                                 NELT,
     &                                 NEQL,
     &                                 KLOCN,
     &                                 KNGV,
     &                                 NKGP,
     &                                 L_IA,
     &                                 L_JA)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LM_GO_T3_ERRRCOV_DIMMAT

      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNEL
      INTEGER, INTENT(IN) :: NCEL
      INTEGER, INTENT(IN) :: NELT
      INTEGER, INTENT(IN) :: NEQL
      INTEGER, INTENT(IN) :: KLOCN(NDLN, NNL)
      INTEGER, INTENT(IN) :: KNGV (NCEL, NELT)
      INTEGER, INTENT(OUT):: NKGP
      INTEGER, INTENT(OUT):: L_IA
      INTEGER, INTENT(OUT):: L_JA

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER NKGSP

      INTEGER LM_GO_T3_ERRRCOV_ASMDIM
      INTEGER LM_GO_T3_ERRRCOV_ASMIND
C----------------------------------------------------------------------

      L_IA = 0
      L_JA = 0

C---     Allocation d'espace
      IERR = SO_ALLC_ALLINT(NEQL+1, L_IA)

C---     Assemble les dimensions
      IF (ERR_GOOD()) THEN
         IERR = LM_GO_T3_ERRRCOV_ASMDIM(EG_CMMN_NNL,
     &                                  NDLN,
     &                                  EG_CMMN_NNELV,
     &                                  EG_CMMN_NCELV,
     &                                  EG_CMMN_NELV,
     &                                  NEQL,
     &                                  KLOCN,
     &                                  KNGV,
     &                                  NKGP,
     &                                  NKGSP,
     &                                  KA(SO_ALLC_REQKIND(KA,L_IA)))
      ENDIF

C---     Assemble les indices
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLINT(NKGP, L_JA)
         IERR = LM_GO_T3_ERRRCOV_ASMIND(EG_CMMN_NNL,
     &                                  NDLN,
     &                                  EG_CMMN_NNELV,
     &                                  EG_CMMN_NCELV,
     &                                  EG_CMMN_NELV,
     &                                  NEQL,
     &                                  KLOCN,
     &                                  KNGV,
     &                                  NKGP,
     &                                  NKGSP,
     &                                  KA(SO_ALLC_REQKIND(KA,L_IA)),
     &                                  KA(SO_ALLC_REQKIND(KA,L_JA)))
      ENDIF

      LM_GO_T3_ERRRCOV_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble les dimensions de la matrice
C
C Description:
C     La fonction privée LM_GO_T3_ERRRCOV_ASMDIM assemble les dimensions de la
C     matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GO_T3_ERRRCOV_ASMDIM(NNL,
     &                                 NDLN,
     &                                 NNEL,
     &                                 NCEL,
     &                                 NELT,
     &                                 NEQL,
     &                                 KLOCN,
     &                                 KNG,
     &                                 NKGP,
     &                                 NKGSP,
     &                                 IA)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNEL
      INTEGER, INTENT(IN) :: NCEL
      INTEGER, INTENT(IN) :: NELT
      INTEGER, INTENT(IN) :: NEQL
      INTEGER, INTENT(IN) :: KLOCN(NDLN, NNL)
      INTEGER, INTENT(IN) :: KNG  (NCEL, NELT)
      INTEGER, INTENT(OUT):: NKGP
      INTEGER, INTENT(OUT):: NKGSP
      INTEGER, INTENT(OUT):: IA   (NEQL+1)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER L_KLD, L_LOCE

      INTEGER LM_GO_T3_ERRRCOV_ASMDIM
      INTEGER LM_GO_T3_ERRRCOV_ASMIND
C-----------------------------------------------------------------------

C---     Alloue l'espace de travail
      L_KLD  = 0
      L_LOCE = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNL+1, L_KLD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNEL,  L_LOCE)

C---     Assemble les dimensions
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_DIMILU0(NNL,
     &                          NDLN,
     &                          NNEL,
     &                          NCEL,
     &                          NELT,
     &                          NEQL,
     &                          KLOCN,
     &                          KA(SO_ALLC_REQKIND(KA,L_LOCE)),
     &                          KNG,
     &                          IA)
      ENDIF

C---     Cumule les tables
      IF (ERR_GOOD()) THEN
         IERR=SP_MORS_CMLTBL(NNL,
     &                       IA,
     &                       KA(SO_ALLC_REQKIND(KA,L_KLD)))
      ENDIF

C---     Calcule les dimensions globales
      IF (ERR_GOOD()) THEN
         NKGP  = IA(NEQL+1) - IA(1)
         NKGSP = (NKGP - NEQL) / 2
      ENDIF

C---     Récupère l'espace
      IF (L_LOCE .NE. 0) IERR = SO_ALLC_ALLINT(0, L_LOCE)
      IF (L_KLD  .NE. 0) IERR = SO_ALLC_ALLINT(0, L_KLD)

      LM_GO_T3_ERRRCOV_ASMDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée LM_GO_T3_ERRRCOV_ASMIND assemble les indices des pointeurs
C     pour le stockage par compression de ligne.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GO_T3_ERRRCOV_ASMIND(NNL,
     &                                 NDLN,
     &                                 NNEL,
     &                                 NCEL,
     &                                 NELT,
     &                                 NEQL,
     &                                 KLOCN,
     &                                 KNG,
     &                                 NKGP,
     &                                 NKGSP,
     &                                 IA,
     &                                 JA)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNEL
      INTEGER, INTENT(IN) :: NCEL
      INTEGER, INTENT(IN) :: NELT
      INTEGER, INTENT(IN) :: NEQL
      INTEGER, INTENT(IN) :: KLOCN(NDLN, NNL)
      INTEGER, INTENT(IN) :: KNG  (NCEL, NELT)
      INTEGER, INTENT(IN) :: NKGP
      INTEGER, INTENT(IN) :: NKGSP
      INTEGER, INTENT(IN) :: IA   (NEQL+1)
      INTEGER, INTENT(OUT):: JA   (NKGP)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER ILU
      INTEGER L_IAC
      INTEGER L_JAC
      INTEGER L_JR
      INTEGER L_LOCE
      LOGICAL ESTCCS

      INTEGER LM_GO_T3_ERRRCOV_ASMIND
C-----------------------------------------------------------------------

C---     États
      ILU = 0
      ESTCCS = (ILU .NE. 0) !! = .FALSE.

C---     Allocation d'espace
      L_IAC = 0
      L_JAC = 0
      L_JR  = 0
      L_LOCE= 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL+1, L_IAC)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NKGSP,  L_JAC)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL,   L_JR)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNEL,   L_LOCE)

C---     Assemble les indices
      IF (ERR_GOOD()) THEN
         IERR=SP_ELEM_INDILU0(KA(SO_ALLC_REQKIND(KA,L_LOCE)),
     &                        NDLN,
     &                        NNL,
     &                        KLOCN,
     &                        NNEL,
     &                        NCEL,
     &                        NELT,
     &                        KNG,
     &                        IA,
     &                        JA)
      ENDIF

C---     Trie les indices de lignes par ordre croissant et remplis
      IF (ERR_GOOD())
     &   IERR=SP_MORS_TRIIND(ESTCCS,
     &                       ILU,
     &                       NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,L_JR)),
     &                       IA,
     &                       JA,
     &                       KA(SO_ALLC_REQKIND(KA,L_IAC)),
     &                       KA(SO_ALLC_REQKIND(KA,L_JAC)))

C---     Rotation du type de stockage creux de colonne à ligne
      IF (ERR_GOOD() .AND. ESTCCS)
     &   IERR=SP_MORS_CCSCRS(NEQL,
     &                       NKGP,
     &                       KA(SO_ALLC_REQKIND(KA,L_JR)),
     &                       KA(SO_ALLC_REQKIND(KA,L_IAC)),
     &                       KA(SO_ALLC_REQKIND(KA,L_JAC)),
     &                       IA,
     &                       JA)

C---     Récupère l'espace
      IF (L_LOCE.NE. 0) IERR = SO_ALLC_ALLINT(0, L_LOCE)
      IF (L_JR  .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JR)
      IF (L_JAC .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JAC)
      IF (L_IAC .NE. 0) IERR = SO_ALLC_ALLINT(0, L_IAC)

      GOTO 9999
C----------------------------------------------------------------------

9999  CONTINUE
      LM_GO_T3_ERRRCOV_ASMIND = ERR_TYP()
      RETURN
      END
