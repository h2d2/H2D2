C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GOT3_CLCJELV
C   Private:
C
C************************************************************************

      SUBMODULE(LM_GOT3_M) LM_GOT3_CLCJELV_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LMGO_T3_CLCJELV
C
C Description:
C     La fonction LMGO_T3_CLCJELV calcule les métriques pour des
C     éléments de volume de type T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT3_CLCJELV(SELF)

      CLASS(LM_GOT3_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      REAL*8,  DIMENSION(:,:), POINTER :: VDJV
      REAL*8,  DIMENSION(:,:), POINTER :: VCORG
      INTEGER IE, IN
      INTEGER NO1, NO2, NO3
      INTEGER NCELV, NELLV

      REAL*8, PARAMETER :: ZERO = 0.0D0
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 6)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 6)
      NELLV = GDTA%NNELV
      NCELV = GDTA%NCELV
      KNGV  => GDTA%KNGV
      VCORG => GDTA%VCORG
      VDJV  => GDTA%VDJV

C---     Boucle sur les éléments
      DO IE=1,NELLV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)
         VDJV(1,IE) = VCORG(2,NO3) - VCORG(2,NO1)                   ! Ksi,x
         VDJV(2,IE) = VCORG(2,NO1) - VCORG(2,NO2)                   ! Eta,x
         VDJV(3,IE) = VCORG(1,NO1) - VCORG(1,NO3)                   ! Ksi,y
         VDJV(4,IE) = VCORG(1,NO2) - VCORG(1,NO1)                   ! Eta,y
         VDJV(5,IE) = VDJV(4,IE)*VDJV(1,IE) - VDJV(3,IE)*VDJV(2,IE) ! Det J

C---        Imprime les éléments à déterminant négatif
         IF (VDJV(5,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9,A,16I9)')
     &         'ERR_DETJ_NEGATIF:', IE, '--', (KNGV(IN,IE),IN=1,NCELV)
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      LM_GOT3_CLCJELV = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT3_CLCJELV

      END SUBMODULE LM_GOT3_CLCJELV_M
