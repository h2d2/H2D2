C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GOT3_INTRP
C   Private:
C
C************************************************************************

      SUBMODULE(LM_GOT3_M) LM_GOT3_INTRP_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LMGO_T3_INTRP
C
C Description:
C     La fonction LMGO_T3_INTRP interpole les valeurs de VSRC dans VDST à
C     la position des NPNT points localisés par leur élément dans KELE et
C     leur coordonnées sur l'élément de référence VCORE.
C
C Entrée:
C     HOBJ        ! Handle sur l'objet courant
C     NPNT        ! Nombre de points à interpoler
C     KDDL        ! Table des DDL à interpoler
C     KELE        ! Table des éléments des points
C     VCORE       ! Coord de ref. des points
C     NDLNS       ! Dim 1 de la table d'entrée
C     NPNTS       ! Dim 2 de la table d'entrée
C     INDXS       ! Index de la table d'entrée à interpoler
C     VSRC        ! Table des données d'entrée à interpoler
C     NDLND       ! Dim 1 de la table de sortie
C     NPNTD       ! Dim 2 de la table de sortie
C     INDXD       ! Index de la table de sortie interpolée
C
C Sortie:
C     VDST        ! Table des valeurs interpolées
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT3_INTRP (SELF,
     &                                       KELE,
     &                                       VCORE,
     &                                       INDXS,
     &                                       VSRC,
     &                                       INDXD,
     &                                       VDST)

      CLASS(LM_GOT3_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: KELE (:)
      REAL*8,  INTENT(IN) :: VCORE(:, :)
      INTEGER, INTENT(IN) :: INDXS
      REAL*8,  INTENT(IN) :: VSRC (:, :)
      INTEGER, INTENT(IN) :: INDXD
      REAL*8,  INTENT(OUT):: VDST (:, :)

      INCLUDE 'err.fi'

      REAL*8, PARAMETER :: UN   = 1.0D0
      REAL*8, PARAMETER :: UN_2 = 0.5D0

      TYPE (LM_GDTA_T), POINTER :: GDTA
      REAL*8  KSI, ETA, LMB
      REAL*8  V1, V2, V3
      INTEGER IP, IE
      INTEGER NO1, NO2, NO3
      INTEGER NPNT
C----------------------------------------------------------------------
D     CALL ERR_ASR(SIZE(KELE, 1) .EQ. SIZE(VCORE,2))
D     CALL ERR_ASR(SIZE(KELE, 1) .EQ. SIZE(VDST, 2))
D     CALL ERR_PRE(INDXS .GT. 0)
D     CALL ERR_PRE(INDXS .LE. SIZE(VSRC, 1))
D     CALL ERR_PRE(INDXD .GT. 0)
D     CALL ERR_PRE(INDXD .LE. SIZE(VDST, 1))
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 3)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 6)

C---     Boucle sur les points
      NPNT = SIZE(VCORE, 2)
      DO IP=1,NPNT
         IE = KELE(IP)
         IF (IE .LE. 0) GOTO 199

         KSI = VCORE(1, IP)
         ETA = VCORE(2, IP)

         NO1 = GDTA%KNGV(1, IE)
         NO2 = GDTA%KNGV(2, IE)
         NO3 = GDTA%KNGV(3, IE)

         V1 = VSRC(INDXS, NO1)
         V2 = VSRC(INDXS, NO2)
         V3 = VSRC(INDXS, NO3)

         VDST(INDXD, IP) = V1 + KSI*(V2-V1) + ETA*(V3-V1)

199      CONTINUE
      ENDDO

      LM_GOT3_INTRP = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT3_INTRP
      
      END SUBMODULE LM_GOT3_INTRP_M
