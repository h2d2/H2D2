C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_GOT3_CLCJELS
C   Private:
C
C************************************************************************

      SUBMODULE(LM_GOT3_M) LM_GOT3_CLCJELS_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LMGO_T3_CLCJELS
C
C Description:
C     La fonction LMGO_T3_CLCJELS calcule les métriques de pour les
C     éléments de surface de type L2, surface d'éléments T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT3_CLCJELS(SELF)

      CLASS(LM_GOT3_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      INTEGER, DIMENSION(:,:), POINTER :: KNGS
      REAL*8,  DIMENSION(:,:), POINTER :: VDJV
      REAL*8,  DIMENSION(:,:), POINTER :: VDJS
      REAL*8  TX, TY, TL
      REAL*8  TXN, TYN
      INTEGER IEP, IE
      INTEGER NP1, NP2, NO1

      REAL*8, PARAMETER :: UN_2 = 5.0000000000000000000D-01
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 3)
D     CALL ERR_PRE(GDTA%NCELS .GE. 4)
D     CALL ERR_PRE(GDTA%NDJV  .GE. 5)
D     CALL ERR_PRE(GDTA%NDJS  .GE. 3)
      KNGV  => GDTA%KNGV
      KNGS  => GDTA%KNGS
      VDJV  => GDTA%VDJV
      VDJS  => GDTA%VDJS

C---     Boucle sur les éléments
      DO IEP=1,GDTA%NELLS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)
         IE  = KNGS(3,IEP)
         NO1 = KNGV(1,IE)
         IF (NP1 .EQ. NO1) THEN            ! COTE 1-2
            TX =   VDJV(4,IE)
            TY = - VDJV(2,IE)
         ELSEIF (NP2 .EQ. NO1) THEN        ! COTE 3-1
            TX =   VDJV(3,IE)
            TY = - VDJV(1,IE)
         ELSE                              ! COTE 2-3
            TX = - (VDJV(3,IE) + VDJV(4,IE))
            TY =   (VDJV(1,IE) + VDJV(2,IE))
         ENDIF
         TL  = HYPOT(TX, TY)
         TXN = TX/TL
         TYN = TY/TL
         VDJS(1,IEP) = TXN
         VDJS(2,IEP) = TYN
         VDJS(3,IEP) = UN_2*TL
      ENDDO

      LM_GOT3_CLCJELS = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT3_CLCJELS

      END SUBMODULE LM_GOT3_CLCJELS_M
