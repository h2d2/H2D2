C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Copie les données des anciens common vers la DLL
C
C Functions:
C   Public:
C   Private:
C     INTEGER LM_LGCY_ASGCMN
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_ASGCMN_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction LM_LGCY_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_ASGCMN(SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL, HFNC
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Transfert EG_CMMN
      EG_CMMN_NDIM   = GDTA%NDIM
      EG_CMMN_NNL    = GDTA%NNL
C
      EG_CMMN_NNELV  = GDTA%NNELV
      EG_CMMN_NCELV  = GDTA%NCELV
      EG_CMMN_NELV   = GDTA%NELLV
      EG_CMMN_NDJV   = GDTA%NDJV
      EG_CMMN_NNELS  = GDTA%NNELS
      EG_CMMN_NCELS  = GDTA%NCELS
      EG_CMMN_NELS   = GDTA%NELLS
      EG_CMMN_NDJS   = GDTA%NDJS
C
      EG_CMMN_NELCOL = GDTA%NELCOL
      EG_CMMN_KELCOL(:,:) = GDTA%KELCOL(:,:)
C
      EG_CMMN_NCLLIM = GDTA%NCLLIM
      EG_CMMN_NCLNOD = GDTA%NCLNOD
      EG_CMMN_NCLELE = GDTA%NCLELE

C---     Transfert LM_CMMN_K
      LM_CMMN_NDLN   = EDTA%NDLN
      LM_CMMN_NNELV  = GDTA%NNELV
      LM_CMMN_NNELS  = GDTA%NNELS
      LM_CMMN_NDLEV  = EDTA%NDLEV
      LM_CMMN_NDLES  = EDTA%NDLES
      LM_CMMN_NDLL   = EDTA%NDLL
C
      LM_CMMN_NPRGL  = EDTA%NPRGL
      LM_CMMN_NPRGLL = EDTA%NPRGLL
C
      LM_CMMN_NPRNO  = EDTA%NPRNO
      LM_CMMN_NPRNOL = EDTA%NPRNOL
C
      LM_CMMN_NPREV    = EDTA%NPREV
      LM_CMMN_NPREVL   = EDTA%NPREVL
      LM_CMMN_NPREV_D1 = EDTA%NPREV_D1
      LM_CMMN_NPREV_D2 = EDTA%NPREV_D2
      LM_CMMN_NPRES    = EDTA%NPRES
      LM_CMMN_NPRES_D1 = EDTA%NPRES_D1
      LM_CMMN_NPRES_D2 = EDTA%NPRES_D2
C
      LM_CMMN_NSOLC  = EDTA%NSOLC
      LM_CMMN_NSOLCL = EDTA%NSOLCL
      LM_CMMN_NSOLR  = EDTA%NSOLR
      LM_CMMN_NSOLRL = EDTA%NSOLRL
C
      LM_CMMN_NCLCND = EDTA%NCLCND
      LM_CMMN_NCLCNV = EDTA%NCLCNV
C
      LM_CMMN_NEQL   = EDTA%NEQL
      LM_CMMN_NEQP   = EDTA%NEQP

C---     Transfert LM_CMMN_V
      LM_CMMN_TSIM   = EDTA%TSIM
!      LM_CMMN_DELT   =

C---     Charge la fonction
      HFNC = 0
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASGCMN', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(EG_CMMN_KA(:)),
     &                       SO_ALLC_CST2B(LM_CMMN_KA(:)),
     &                       SO_ALLC_CST2B(LM_CMMN_VA(:)))

      LM_LGCY_ASGCMN = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_ASGCMN

      END SUBMODULE LM_LGCY_ASGCMN_M
