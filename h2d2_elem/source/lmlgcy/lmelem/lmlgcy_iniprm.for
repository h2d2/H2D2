C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER LM_LGCY_INIPRMS
C     INTEGER LM_LGCY_INIPRMS_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_INIPRMS_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_LGCY_INIPRMS_CB(F, SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
      EXTERNAL F

      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmgeom.fi'

      INTEGER IERR
      INTEGER TGELV
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV, NPREV_D1, NPREV_D2
      INTEGER NPREVL
      INTEGER NPRES, NPRES_D1, NPRES_D2
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NPOST
      LOGICAL ASURFACE
      LOGICAL ESTLIN
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Demande les paramètres à l'élément
      CALL F (TGELV,
     &        NPRGL,
     &        NPRGLL,
     &        NPRNO,
     &        NPRNOL,
     &        NPREV,
     &        NPREVL,
     &        NPRES,
     &        NSOLC,
     &        NSOLCL,
     &        NSOLR,
     &        NSOLRL,
     &        NDLN,
     &        NDLEV,
     &        NDLES,
     &        ASURFACE,
     &        ESTLIN)

      IF (ERR_GOOD()) THEN
         EDTA => SELF%EDTA
         GDTA => SELF%GDTA
         IF (GDTA%ITPEV .NE. TGELV) GOTO 9900

C---        Décode NPRES
         IF (NPREV .LT. 1000) THEN
            NPREV_D1 = NPREV
            NPREV_D2 = 1
         ELSE
            NPREV_D1 = NPREV / 1000
            NPREV_D2 = NPREV - NPREV_D1*1000
            NPREV = NPREV_D1 * NPREV_D2
         ENDIF
C---        Décode NPRES
         IF (NPRES .LT. 1000) THEN
            NPRES_D1 = NPRES
            NPRES_D2 = 1
         ELSE
            NPRES_D1 = NPRES / 1000
            NPRES_D2 = NPRES - NPRES_D1*1000
            NPRES = NPRES_D1 * NPRES_D2
         ENDIF

         EDTA%NPRGLL   = NPRGLL
         EDTA%NPRGL    = NPRGL
         EDTA%NPRNOL   = NPRNOL
         EDTA%NPRNO    = NPRNO
         EDTA%NPREVL   = NPREVL
         EDTA%NPREV    = NPREV
         EDTA%NPREV_D1 = NPREV_D1
         EDTA%NPREV_D2 = NPREV_D2
         EDTA%NPRES    = NPRES
         EDTA%NPRES_D1 = NPRES_D1
         EDTA%NPRES_D2 = NPRES_D2
         EDTA%NSOLCL   = NSOLCL
         EDTA%NSOLC    = NSOLC
         EDTA%NSOLRL   = NSOLRL
         EDTA%NSOLR    = NSOLR
         EDTA%NDLN     = NDLN
         EDTA%NDLEV    = NDLEV
         EDTA%NDLES    = NDLES

      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LMGO_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_LGCY_INIPRMS_CB = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_INIPRMS_CB

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée LM_LGCY_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_INIPRMS(SELF)

      USE SO_FUNC_M, ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HSOF
      LOGICAL LDTR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

C---     Charge la fonction
      HSOF = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EA_FUNC_REQPRM, HSOF, LDTR)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HSOF,
     &                        LM_LGCY_INIPRMS_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)))

C---     Libère la fonction
      IF (HSOF .NE. 0 .AND. LDTR) IERR = SO_FUNC_DTR(HSOF)

      LM_LGCY_INIPRMS = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_INIPRMS

      END SUBMODULE LM_LGCY_INIPRMS_M
      