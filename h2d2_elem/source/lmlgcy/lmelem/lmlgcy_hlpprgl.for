C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_LGCY_HLPPRGL
C   Private:
C     INTEGER LM_LGCY_HLPPRGL_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_HLPPRGL_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Imprime les propriétés globales
C
C Description:
C     La fonction LM_LGCY_PRINTPRGL imprime les propriétés globales
C     de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_HLPPRGL(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_HLPPRGL
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      LOGICAL LDTR
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EA_FUNC_HLPPRGL, HFNC, LDTR)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL(HFNC)

C---     Libère la fonction
      IF (HFNC .NE. 0 .AND. LDTR) IERR = SO_FUNC_DTR(HFNC)

      LM_LGCY_HLPPRGL = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_HLPPRGL

      END SUBMODULE LM_LGCY_HLPPRGL_M
