C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_REQNFN_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Retourne la fonction d'après le code
C
C Description:
C     La fonction privée LM_LGCY_REQFNC retourne un handle HFUNC à
C     la fonction de code IFUNC. Si LDTR est .TRUE. il est
C     de la responsabilité de la fonction appelante de disposer du
C     handle HFUNC par un appel à SO_FUNC_DTR(...).
C
C Entrée:
C     SELF       Handle à la formulation
C     IFUNC       Code de la fonction demandée
C
C Sortie:
C     HFUNC       Handle à la fonction
C     LDTR        .TRUE. si HFUNC doit être détruit
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_REQFNC(SELF,
     &                                       IFUNC,
     &                                       HFUNC,
     &                                       LDTR)

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: HFUNC
      LOGICAL, INTENT(OUT) :: LDTR

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      LDTR = .TRUE.

C---     Cherche la fonction directement
      IF (ERR_GOOD()) IERR = LM_LGCY_REQFNC_REQFNC(SELF, IFUNC, HFUNC)
      IF (ERR_GOOD()) LDTR = .FALSE.

C---     Cherche la fonction via le nom
      IF (ERR_BAD() .AND. ERR_ESTMSG('ERR_CHARGE_FNCT_DLL')) THEN
         CALL ERR_RESET()
         IERR = LM_LGCY_REQFNC_REQNFN(SELF, IFUNC, HFUNC)
      ENDIF

      LM_LGCY_REQFNC = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_REQFNC

C************************************************************************
C Sommaire: Retourne la fonction d'après le code
C
C Description:
C     La fonction privée LM_LGCY_REQFNC retourne un handle HFUNC à
C     la fonction de code IFUNC. Il est de la responsabilité de
C     la fonction appelante de disposer du handle HFUNC par un appel
C     à SO_FUNC_DTR(...).
C
C Entrée:
C     SELF     Handle à la formulation
C     IFUNC    Code de la fonction demandée
C
C Sortie:
C     HFUNC    Handle à la fonction
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_LGCY_REQFNC_REQFNC(SELF,
     &                                       IFUNC,
     &                                       HFUNC)

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: HFUNC

      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL
      INTEGER HFNC
C-----------------------------------------------------------------------

C---     Les attributs
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction de requête de fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQFNC', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(IFUNC),
     &                       SO_ALLC_CST2B(HFUNC))

      LM_LGCY_REQFNC_REQFNC = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_REQFNC_REQFNC

C************************************************************************
C Sommaire: Retourne la fonction d'après le code
C
C Description:
C     La fonction privée LM_LGCY_REQFNC retourne un handle HFUNC à
C     la fonction de code IFUNC. Il est de la responsabilité de
C     la fonction appelante de disposer du handle HFUNC par un appel
C     à SO_FUNC_DTR(...).
C
C Entrée:
C     SELF     Handle à la formulation
C     IFUNC    Code de la fonction demandée
C
C Sortie:
C     HFUNC    Handle à la fonction
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_LGCY_REQFNC_REQNFN(SELF,
     &                                       IFUNC,
     &                                       HFUNC)

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: HFUNC

      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'spstrn.fi'

      INTEGER        IERR
      INTEGER        IPOS
      INTEGER        HMDL
      INTEGER        HFNC
      INTEGER        KFUNC(16)
      CHARACTER*(64) NFUNC
      CHARACTER*(64) NOMDLL
      CHARACTER*(64) NOMFNC
      EQUIVALENCE   (NFUNC, KFUNC)
C-----------------------------------------------------------------------

C---     Les attributs
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction de requête
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQNFN', HFNC)

C---     Fait l'appel
      NFUNC = ' '
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(KFUNC),
     &                       SO_ALLC_CST2B(IFUNC))

C---     Split le nom
      IF (ERR_GOOD()) THEN
         CALL SP_STRN_TRM(NFUNC)
         NOMFNC = NFUNC(1:SP_STRN_LEN(NFUNC))
         IPOS = SP_STRN_LFT(NOMFNC, '@')
         NOMDLL = NFUNC(1:SP_STRN_LEN(NFUNC))
         IPOS = SP_STRN_RHT(NOMDLL, '@')
      ENDIF

C---     Charge la vrai fonction
      IF (ERR_GOOD()) IERR=SO_FUNC_CTR   (HFUNC)
      IF (ERR_GOOD()) IERR=SO_FUNC_INIFSO(HFUNC,
     &                                    NOMDLL(1:SP_STRN_LEN(NOMDLL)),
     &                                    NOMFNC(1:SP_STRN_LEN(NOMFNC)))

      LM_LGCY_REQFNC_REQNFN = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_REQFNC_REQNFN

C************************************************************************
C Sommaire: Retourne la nom de la fonction d'après le code
C
C Description:
C     La fonction privée LM_LGCY_REQFNC retourne le nom de la fonction
C     de code IFUNC.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     IFUNC    Code de la fonction demandée
C
C Sortie:
C     KFUNC    Le nom de la fonction
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_REQNFN(SELF,
     &                                       IFUNC,
     &                                       KFUNC)

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: IFUNC
      INTEGER, INTENT(OUT) :: KFUNC(:)

      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL
      INTEGER HFNC
C-----------------------------------------------------------------------

C---     Charge la fonction de requête de nom
      HFNC = 0
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQNFN', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(KFUNC(:)),
     &                       SO_ALLC_CST2B(IFUNC))

      LM_LGCY_REQNFN = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_REQNFN

      END SUBMODULE LM_LGCY_REQNFN_M
