C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_LGCY_HLPCLIM
C   Private:
C     INTEGER LM_LGCY_HLPCLIM_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_LGCY_M) LM_LGCY_HLPCLIM_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Imprime les propriétés globales
C
C Description:
C     La fonction LM_LGCY_HLPCLIM imprime l'aide sur les conditions limites
C     de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_LGCY_HLPCLIM(SELF, HPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_HLPCLIM
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL

      CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER HPRGL

      INCLUDE 'err.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      LOGICAL LDTR
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EA_FUNC_HLPCLIM, HFNC, LDTR)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL(HFNC)

C---     Libère la fonction
      IF (HFNC .NE. 0 .AND. LDTR) IERR = SO_FUNC_DTR(HFNC)

      LM_LGCY_HLPCLIM = ERR_TYP()
      RETURN
      END  FUNCTION LM_LGCY_HLPCLIM

      END SUBMODULE LM_LGCY_HLPCLIM_M
