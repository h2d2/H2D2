C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  eLement Géométrique
C Objet:   Legacy
C Type:    Concret
C Interface:
C   H2D2 Module: LG
C      H2D2 Class: LG_LGCY
C         INTEGER LG_LGCY_000
C         INTEGER LG_LGCY_999
C         INTEGER LG_LGCY_CTR
C         INTEGER LG_LGCY_DTR
C         INTEGER LG_LGCY_INI
C         INTEGER LG_LGCY_RST
C         INTEGER LG_LGCY_REQHBASE
C         LOGICAL LG_LGCY_HVALIDE
C         INTEGER LG_LGCY_INIVTBL
C         FTN Module: LG_LGCY_M
C            Public:
C            Private:
C               TYPE (LG_LGCY_T), POINTER LG_LGCY_REQSELF
C               TYPE (LM_GDTA_T), POINTER LG_LGCY_REQGDTA
C            
C            FTN Type: LG_LGCY_T
C               Public:
C               Private:
C
C************************************************************************

      MODULE LG_LGCY_M

      USE LM_GEOM_M, ONLY: LM_GEOM_T
      IMPLICIT NONE

      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: LG_LGCY_HBASE = 0

C---     Attributs privés
      TYPE, EXTENDS(LM_GEOM_T) :: LG_LGCY_T
         INTEGER, PRIVATE :: HMDUL
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR     => LG_LGCY_DTR
         PROCEDURE, PUBLIC :: ASMESCL => LG_LGCY_ASMESCL
         !!!PROCEDURE, PUBLIC :: ASMPEAU => LG_LGCY_ASMPEAU
         PROCEDURE, PUBLIC :: CLCERR  => LG_LGCY_CLCERR
         PROCEDURE, PUBLIC :: CLCJELS => LG_LGCY_CLCJELS
         PROCEDURE, PUBLIC :: CLCJELV => LG_LGCY_CLCJELV
         PROCEDURE, PUBLIC :: CLCSPLT => LG_LGCY_CLCSPLT
         PROCEDURE, PUBLIC :: INTRP   => LG_LGCY_INTRP
         PROCEDURE, PUBLIC :: LCLELV  => LG_LGCY_LCLELV

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: ASGCMN => LG_LGCY_ASGCMN
         PROCEDURE, PRIVATE :: INIPRMS=> LG_LGCY_INIPRMS
         PROCEDURE, PRIVATE :: REQFNC => LG_LGCY_REQFNC
         PROCEDURE, PRIVATE :: REQNFN => LG_LGCY_REQNFN
      END TYPE LG_LGCY_T

      ! ---  Constructor-destructor
      PUBLIC :: LG_LGCY_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: LG_LGCY_DTR
      END INTERFACE DEL
      
      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LG_LGCY_ASGCMN(SELF)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
         END FUNCTION LG_LGCY_ASGCMN
         
         MODULE INTEGER FUNCTION LG_LGCY_INIPRMS(SELF)
            CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LG_LGCY_INIPRMS
         
         MODULE INTEGER FUNCTION LG_LGCY_REQFNC(SELF, IFUNC, HFUNC)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: IFUNC
            INTEGER, INTENT(OUT) :: HFUNC
         END FUNCTION LG_LGCY_REQFNC
         
         MODULE INTEGER FUNCTION LG_LGCY_REQNFN(SELF, IFUNC, KFUNC)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: IFUNC
            INTEGER, INTENT(OUT) :: KFUNC(:)
         END FUNCTION LG_LGCY_REQNFN

         MODULE INTEGER FUNCTION LG_LGCY_ASMESCL(SELF)
            CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LG_LGCY_ASMESCL

!!!         INTEGER FUNCTION ASMPEAU(SELF)
!!!            IMPORT :: LG_LGCY_T
!!!            CLASS(LG_LGCY_T), INTENT(IN) :: SELF
!!!         END FUNCTION ASMPEAU

         MODULE INTEGER FUNCTION LG_LGCY_CLCERR(SELF,
     &         VFRM, VHESS, VHNRM, ITPCLC)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: VFRM (:)
            REAL*8,  INTENT(OUT) :: VHESS(:,:)
            REAL*8,  INTENT(OUT) :: VHNRM(:)
            INTEGER, INTENT(IN)  :: ITPCLC
         END FUNCTION LG_LGCY_CLCERR

         MODULE INTEGER FUNCTION LG_LGCY_CLCJELS(SELF)
            CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LG_LGCY_CLCJELS

         MODULE INTEGER FUNCTION LG_LGCY_CLCJELV(SELF)
            CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LG_LGCY_CLCJELV

         MODULE INTEGER FUNCTION LG_LGCY_CLCSPLT(SELF, KNGZ)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(OUT) :: KNGZ(:, :)
         END FUNCTION LG_LGCY_CLCSPLT

         MODULE INTEGER FUNCTION LG_LGCY_INTRP(SELF, KELE, VCORE,
     &         INDXS, VSRC, INDXD, VDST)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: KELE (:)
            REAL*8,  INTENT(IN)  :: VCORE(:, :)
            INTEGER, INTENT(IN)  :: INDXS
            REAL*8,  INTENT(IN)  :: VSRC (:, :)
            INTEGER, INTENT(IN)  :: INDXD
            REAL*8,  INTENT(OUT) :: VDST (:, :)
         END FUNCTION LG_LGCY_INTRP

         MODULE INTEGER FUNCTION LG_LGCY_LCLELV(SELF,
     &         TOL, VCORP, KELEP, VCORE)
            CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
            REAL*8,  INTENT(IN)  :: TOL
            REAL*8,  INTENT(IN)  :: VCORP(:, :)
            INTEGER, INTENT(OUT) :: KELEP(:)
            REAL*8,  INTENT(OUT) :: VCORE(:, :)
         END FUNCTION LG_LGCY_LCLELV

      END INTERFACE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     l'élément est concret
C************************************************************************
      FUNCTION LG_LGCY_CTR(ITPGEO) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_CTR
CDEC$ ENDIF

      USE LM_GEOM_M, ONLY: LM_GEOM_CTR

      INTEGER, INTENT(IN) :: ITPGEO

      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'

      INTEGER IERR, IRET
      INTEGER OTPGEO
      CLASS(LG_LGCY_T), POINTER :: SELF
      CLASS(LM_GEOM_T), POINTER :: OPRNT
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
!!      IF (ERR_GOOD()) IERR = LG_LGCY_RAZ(SELF)

C---     Construis le parent
      IF (ERR_GOOD()) OPRNT => LM_GEOM_CTR(SELF)

C---     Connecte au module
      SELF%HMDUL = 0
      OTPGEO = 10000 + ITPGEO    ! Old type
      IF (ERR_GOOD()) IERR = SO_UTIL_REQHMDLCOD(SO_MDUL_TYP_GEOMETRIE,
     &                                          OTPGEO,
     &                                          SELF%HMDUL)
D     CALL ERR_ASR(ERR_BAD() .OR. SO_MDUL_HVALIDE(SELF%HMDUL))

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LG_LGCY_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_DTR
CDEC$ ENDIF

      USE LM_GEOM_M, ONLY: LM_GEOM_DTR
      
      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LG_LGCY_T), POINTER :: SELF_P
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset l'objet
      IF (ERR_GOOD()) IERR = SELF%RST()

C---     Détruis le parent
      IF (ERR_GOOD()) IERR = LM_GEOM_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      LG_LGCY_DTR = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_DTR

      END MODULE LG_LGCY_M
      