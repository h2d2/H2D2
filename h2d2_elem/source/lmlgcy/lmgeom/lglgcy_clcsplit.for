C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_CLCSPLT
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_CLCSPLT_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_CLCSPLT(SELF,
     &                                        KNGZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_CLCSPLT
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(OUT) :: KNGZ(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      INTEGER NCELZ, NELZ
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      CALL LOG_TODO('Qui fait les appels à CP2GDTA??')
      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_CLCSPLIT, HFNC)

C---     Fait l'appel
      GDTA => SELF%GDTA
      IF (ERR_GOOD()) THEN
         NCELZ = SIZE(KNGZ, 1)
         NELZ  = SIZE(KNGZ, 2)
         IERR = SO_FUNC_CALL(HFNC,
     &                       SO_ALLC_CST2B(GDTA%NCELV),
     &                       SO_ALLC_CST2B(GDTA%NELLV),
     &                       SO_ALLC_CST2B(GDTA%KNGV(:,1)),
     &                       SO_ALLC_CST2B(NCELZ),
     &                       SO_ALLC_CST2B(NELZ),
     &                       SO_ALLC_CST2B(KNGZ(:,1)))
      ENDIF

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_CLCSPLT = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_CLCSPLT
      
      END SUBMODULE LG_LGCY_CLCSPLT_M
