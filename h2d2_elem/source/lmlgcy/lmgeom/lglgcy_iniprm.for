C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER LG_LGCY_INIPRMS
C     INTEGER LG_LGCY_INIPRMS_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_INIPRMS_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LG_LGCY_INIPRMS_CB(F, SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      EXTERNAL F
      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NDIM
      INTEGER ITPEV, NNELV, NCELV, NDJV
      INTEGER ITPES, NNELS, NCELS, NDJS
      INTEGER ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Demande les paramètres à l'élément
      CALL F(NDIM,
     &       ITPEV, NNELV, NCELV, NDJV,
     &       ITPES, NNELS, NCELS, NDJS,
     &       ITPEZ, NNELZ, NCELZ, NDJZ, NEV2EZ)

C---     Assigne à l'objet
      IF (ERR_GOOD()) THEN
         GDTA => SELF%GDTA

         GDTA%NDIM  = NDIM
         GDTA%ITPEV = ITPEV
         GDTA%NNELV = NNELV
         GDTA%NCELV = NCELV
         GDTA%NDJV  = NDJV
         GDTA%ITPES = ITPES
         GDTA%NNELS = NNELS
         GDTA%NCELS = NCELS
         GDTA%NDJS  = NDJS
         GDTA%ITPEZ = ITPEZ
         GDTA%NNELZ = NNELZ
         GDTA%NCELZ = NCELZ
         GDTA%NDJZ  = NDJZ
         GDTA%NEV2EZ= NEV2EZ

      ENDIF

      LG_LGCY_INIPRMS_CB = ERR_TYP()
      RETURN
      END
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée LG_LGCY_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_INIPRMS(SELF)

      USE LM_GDTA_M,     ONLY: LM_GDTA_T
      USE SO_FUNC_M,     ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LG_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL, HSOF
C-----------------------------------------------------------------------

C---     Charge la fonction
      HSOF = 0
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQPRM', HSOF)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HSOF,
     &                        LG_LGCY_INIPRMS_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)))

C---     Libère la fonction
      IF (HSOF .NE. 0) IERR = SO_FUNC_DTR(HSOF)

      LG_LGCY_INIPRMS = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_INIPRMS

      END SUBMODULE LG_LGCY_INIPRMS_M
