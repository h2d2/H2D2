C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_LCLELV
C   Private:
C     INTEGER LG_LGCY_LCLELV_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_LCLELV_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LG_LGCY_LCLELV_CB est le call-back pour
C     la fonction LG_LGCY_LCLELV.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER 
     &FUNCTION LG_LGCY_LCLELV_CB(F, SELF, TOL, VCORP, KELEP, VCORE)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER  F
      EXTERNAL F
      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      REAL*8,  INTENT(IN)  :: TOL
      REAL*8,  INTENT(IN)  :: VCORP(:,:)
      INTEGER, INTENT(OUT) :: KELEP(:)
      REAL*8,  INTENT(OUT) :: VCORE(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
D     INTEGER NDIM
      INTEGER NPNT
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données d'approximation
      GDTA => SELF%GDTA
D     NDIM = SIZE(VCORP, 2)
      NPNT = SIZE(VCORP, 1)
D     CALL ERR_ASR(NDIM .EQ. GDTA%NDIM)
D     CALL ERR_ASR(NDIM .EQ. SIZE(VCORE, 1))
D     CALL ERR_ASR(NPNT .EQ. SIZE(VCORE, 2))
D     CALL ERR_ASR(NPNT .EQ. SIZE(KELEP, 1))

C---     Fait l'appel
      IERR = F(TOL,
     &         GDTA%NDIM,
     &         GDTA%NNL,
     &         GDTA%NCELV,
     &         GDTA%NELLV,
     &         GDTA%NDJV,
     &         GDTA%VCORG,
     &         GDTA%KNGV,
     &         GDTA%VDJV,
     &         NPNT,
     &         VCORP,
     &         KELEP,
     &         VCORE)

      LG_LGCY_LCLELV_CB = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_LCLELV_CB
      
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER 
     &FUNCTION LG_LGCY_LCLELV(SELF, 
     &                        TOL, 
     &                        VCORP, 
     &                        KELEP, 
     &                        VCORE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_LCLELV
CDEC$ ENDIF

      USE LM_GDTA_M,     ONLY: LM_GDTA_T
      USE SO_FUNC_M,     ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      REAL*8,  INTENT(IN)  :: TOL
      REAL*8,  INTENT(IN)  :: VCORP(:,:)
      INTEGER, INTENT(OUT) :: KELEP(:)
      REAL*8,  INTENT(OUT) :: VCORE(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_LCLELV, HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HFNC,
     &                        LG_LGCY_LCLELV_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)),
     &                        SO_ALLC_CST2B(TOL),
     &                        SO_ALLC_CST2B(VCORP(:,1)),
     &                        SO_ALLC_CST2B(KELEP(:)),
     &                        SO_ALLC_CST2B(VCORE(:,1)))

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_LCLELV = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_LCLELV

      END SUBMODULE LG_LGCY_LCLELV_M
