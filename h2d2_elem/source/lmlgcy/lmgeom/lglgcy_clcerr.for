C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_CLCERR
C   Private:
C     INTEGER LG_LGCY_CLCERR_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_CLCERR_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LG_LGCY_CLCERR_CB est le call-back pour
C     la fonction LG_LGCY_CLCERR.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER 
     &FUNCTION LG_LGCY_CLCERR_CB(F, SELF, VFRM, VHESS, VHNRM, ITPCLC)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER  F
      EXTERNAL F
      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(IN)  :: VFRM (:)
      REAL*8, INTENT(OUT) :: VHESS(:,:)
      REAL*8, INTENT(OUT) :: VHNRM(:)
      INTEGER,INTENT(IN)  :: ITPCLC

      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA
      IERR = F(GDTA%VCORG,
     &         GDTA%KNGV,
     &         GDTA%KNGS,
     &         GDTA%VDJV,
     &         GDTA%VDJS,
     &         VFRM,
     &         VHESS,
     &         VHNRM,
     &         ITPCLC)

      LG_LGCY_CLCERR_CB = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_CLCERR_CB

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_CLCERR(SELF,
     &                                       VFRM, 
     &                                       VHESS, 
     &                                       VHNRM, 
     &                                       ITPCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_CLCERR
CDEC$ ENDIF

      USE LM_GDTA_M,     ONLY: LM_GDTA_T
      USE SO_FUNC_M
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(IN)  :: VFRM (:)
      REAL*8, INTENT(OUT) :: VHESS(:,:)
      REAL*8, INTENT(OUT) :: VHNRM(:)
      INTEGER,INTENT(IN)  :: ITPCLC

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      CALL LOG_TODO('Qui fait les appels à CP2GDTA??')
      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_CLCERR, HFNC)

C---     Fait l'appel
      GDTA => SELF%GDTA
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC(HFNC,
     &                        LG_LGCY_CLCERR_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)),
     &                        SO_ALLC_CST2B(VFRM (:)),
     &                        SO_ALLC_CST2B(VHESS(:,1)),
     &                        SO_ALLC_CST2B(VHNRM(:)),
     &                        SO_ALLC_CST2B(ITPCLC))

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_CLCERR = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_CLCERR

      END SUBMODULE LG_LGCY_CLCERR_M
