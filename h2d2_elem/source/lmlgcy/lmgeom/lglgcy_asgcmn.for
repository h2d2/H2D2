C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Copie les données des anciens common vers la DLL
C
C Functions:
C   Public:
C   Private:
C     INTEGER LG_LGCY_ASGCMN
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_ASGCMN_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction LG_LGCY_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C************************************************************************
      MODULE INTEGER FUNCTION LG_LGCY_ASGCMN(SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE SO_FUNC_M
      USE SO_ALLC_M

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'egcmmn.fc'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HMDL, HFNC
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA

C---     Transfert EG_CMMN
      EG_CMMN_NDIM   = GDTA%NDIM
      EG_CMMN_NNL    = GDTA%NNL
C
      EG_CMMN_NNELV  = GDTA%NNELV
      EG_CMMN_NCELV  = GDTA%NCELV
      EG_CMMN_NELV   = GDTA%NELLV
      EG_CMMN_NDJV   = GDTA%NDJV
      EG_CMMN_NNELS  = GDTA%NNELS
      EG_CMMN_NCELS  = GDTA%NCELS
      EG_CMMN_NELS   = GDTA%NELLS
      EG_CMMN_NDJS   = GDTA%NDJS
C
      EG_CMMN_NELCOL = GDTA%NELCOL
      EG_CMMN_KELCOL(:,:) = GDTA%KELCOL(:,:)
C
      EG_CMMN_NCLLIM = GDTA%NCLLIM
      EG_CMMN_NCLNOD = GDTA%NCLNOD
      EG_CMMN_NCLELE = GDTA%NCLELE

C---     Charge la fonction
      HFNC = 0
      HMDL = SELF%HMDUL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASGCMN', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL1(HFNC,
     &                        SO_ALLC_CST2B(EG_CMMN_KA(:)))

      LG_LGCY_ASGCMN = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_ASGCMN

      END SUBMODULE LG_LGCY_ASGCMN_M
