C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LG_LGCY_INTRP
C   Private:
C     INTEGER LG_LGCY_INTRP_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LG_LGCY_M) LG_LGCY_INTRP_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée LG_LGCY_INTRP_CB est le call-back pour
C     la fonction LG_LGCY_INTRP.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LG_LGCY_INTRP_CB(F,
     &                                  SELF,
     &                                  KELE,
     &                                  VCORE,
     &                                  INDXS,
     &                                  VSRC,
     &                                  INDXD,
     &                                  VDST)

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      INTEGER  F
      EXTERNAL F
      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: KELE (:)
      REAL*8,  INTENT(IN)  :: VCORE(:, :)
      INTEGER, INTENT(IN)  :: INDXS
      REAL*8,  INTENT(IN)  :: VSRC (:, :)
      INTEGER, INTENT(IN)  :: INDXD
      REAL*8,  INTENT(OUT) :: VDST (:, :)

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NDIM,  NPNT
      INTEGER NDLNS, NPNTS
      INTEGER NDLND, NPNTD
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      GDTA => SELF%GDTA
      NDIM  = SIZE(VCORE, 1)
      NPNT  = SIZE(VCORE, 2)
      NDLNS = SIZE(VSRC, 1)
      NPNTS = SIZE(VSRC, 2)
      NDLND = SIZE(VDST, 1)
      NPNTD = SIZE(VDST, 2)

      IERR = F(GDTA%NCELV,
     &         GDTA%NELLV,
     &         GDTA%KNGV,
     &         NDIM,
     &         NPNT,
     &         KELE,
     &         VCORE,
     &         NDLNS,
     &         NPNTS,
     &         INDXS,
     &         VSRC,
     &         NDLND,
     &         NPNTD,
     &         INDXD,
     &         VDST)

      LG_LGCY_INTRP_CB = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_INTRP_CB
      
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER
     &FUNCTION LG_LGCY_INTRP(SELF,
     &                       KELE,
     &                       VCORE,
     &                       INDXS,
     &                       VSRC,
     &                       INDXD,
     &                       VDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LG_LGCY_INTRP
CDEC$ ENDIF

      USE LM_GDTA_M,     ONLY: LM_GDTA_T
      USE SO_FUNC_M,     ONLY: SO_FUNC_CBFNC
      USE SO_ALLC_M,     ONLY: SO_ALLC_CST2B
      USE ISO_C_BINDING, ONLY: C_LOC

      CLASS(LG_LGCY_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: KELE (:)
      REAL*8,  INTENT(IN)  :: VCORE(:, :)
      INTEGER, INTENT(IN)  :: INDXS
      REAL*8,  INTENT(IN)  :: VSRC (:, :)
      INTEGER, INTENT(IN)  :: INDXD
      REAL*8,  INTENT(OUT) :: VDST (:, :)

      INCLUDE 'err.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER HFNC
C-----------------------------------------------------------------------

      IERR = SELF%ASGCMN()

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SELF%REQFNC(EG_FUNC_INTRP, HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CBFNC(HFNC,
     &                        LG_LGCY_INTRP_CB,
     &                        SO_ALLC_CST2B(C_LOC(SELF)),
     &                        SO_ALLC_CST2B(KELE (:)),
     &                        SO_ALLC_CST2B(VCORE(:,1)),
     &                        SO_ALLC_CST2B(INDXS),
     &                        SO_ALLC_CST2B(VSRC (:,1)),
     &                        SO_ALLC_CST2B(INDXD),
     &                        SO_ALLC_CST2B(VDST (:,1)))
      ENDIF

C---     Libère la fonction
      IF (HFNC .NE. 0) IERR = SO_FUNC_DTR(HFNC)

      LG_LGCY_INTRP = ERR_TYP()
      RETURN
      END FUNCTION LG_LGCY_INTRP

      END SUBMODULE LG_LGCY_INTRP_M
