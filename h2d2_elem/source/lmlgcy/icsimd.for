C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_SIMD_XEQCTR
C     CHARACTER*(32) IC_SIMD_REQCLS
C     INTEGER IC_SIMD_REQHDL
C   Private:
C     SUBROUTINE IC_SIMD_AID
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à CI_CINC_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA IC_SIMD_DATA

      IMPLICIT NONE

      INCLUDE 'icsimd.fc'

      DATA IC_SIMD_HDIC  / 0 /

      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SIMD_AJTCTR(NOM, HFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SIMD_AJTCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) NOM
      INTEGER       HFNC

      INCLUDE 'icsimd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icsimd.fc'

      INTEGER IERR
      CHARACTER*(16) VAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(NOM) .GT. 0)
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFNC))
C-----------------------------------------------------------------------

C---     Au besoin, construis le map
      IF (.NOT. DS_MAP_HVALIDE(IC_SIMD_HDIC)) THEN
         IF (ERR_GOOD()) IERR = DS_MAP_CTR(IC_SIMD_HDIC)
         IF (ERR_GOOD()) IERR = DS_MAP_INI(IC_SIMD_HDIC)
      ENDIF
      
C---     Ajoute la nouvelle entrée
      IF (ERR_GOOD()) THEN
         WRITE(VAL, '(I12)') HFNC
         IERR = DS_MAP_ASGVAL(IC_SIMD_HDIC, NOM, VAL)
      ENDIF

      IC_SIMD_AJTCTR = ERR_TYP()
      RETURN
      END
      
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SIMD_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SIMD_XEQCTR
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B
      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icsimd.fi'
      INCLUDE 'dsmap.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icsimd.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HFRML
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREL
      INTEGER HFNC, LFRM
      INTEGER KI(9)
      CHARACTER*(256) FRML
      CHARACTER*(16) VAL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SIMD_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the problem formulation</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HFRML)
C     <comment>Handle on the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HGRID)
C     <comment>Handle on the degrees of freedom (unknowns)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HDLIB)
C     <comment>Handle on the boundary conditions</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HCLIM)
C     <comment>Handle on the concentrated solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HSOLC)
C     <comment>Handle on the distributed solicitations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, HSOLR)
C     <comment>Handle on the global properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 7, HPRGL)
C     <comment>Handle on the nodal properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 8, HPRNO)
C     <comment>Handle on the elemental properties</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 9, HPREL)
      IF (IERR .NE. 0) GOTO 9901

C---     Récupère les donnés de la formulation
      IF (.NOT. LM_FRML_HVALIDE(HFRML)) GOTO 9902
      FRML = LM_FRML_REQFRML (HFRML)

C---     La fonction de construction
      HFNC = 0
      LFRM = SP_STRN_LEN(FRML)
      IF (DS_MAP_HVALIDE(IC_SIMD_HDIC)) THEN
         IF (DS_MAP_ESTCLF(IC_SIMD_HDIC, FRML(1:LFRM))) THEN
            IERR = DS_MAP_REQVAL(IC_SIMD_HDIC, FRML(1:LFRM), VAL)
            READ(VAL, *) HFNC
         ENDIF
      ENDIF
      
C---     Construis et initialise l'objet
      KI = (/ HFRML,HGRID,HDLIB,HCLIM,HSOLC,HSOLR,HPRGL,HPRNO,HPREL /)
      IF (SO_FUNC_HVALIDE(HFNC)) THEN
         IERR = SO_FUNC_CALL(HFNC, 
     &                       SO_ALLC_CST2B(HOBJ),
     &                       SO_ALLC_CST2B(KI))
      ELSE
         IERR = IC_SIMD_XEQCTR_TRV(HOBJ, KI)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the simulation data object</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = LM_HELE_PRN(HOBJ)
      END IF

C  <comment>
C  The constructor <b>simd</b> constructs an object, with the given arguments, and
C  returns a handle on this object. It controls also the coherence.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HFRML
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SIMD_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SIMD_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SIMD_XEQCTR_TRV(HOBJ, KI)

      USE LM_LGCY_M
      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER KI(*)

      INCLUDE 'icsimd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fi'
      !INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icsimd.fc'

      INTEGER IERR
      INTEGER ITPG
      INTEGER HFRML
      INTEGER HCONF
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREL
      INTEGER L
      CHARACTER*(256) FRML
      CLASS(LM_LGCY_T), POINTER :: OELEM
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les handles
      HFRML = KI(1)
      HCONF = 0
      HGRID = KI(2)
      HDLIB = KI(3)
      HCLIM = KI(4)
      HSOLC = KI(5)
      HSOLR = KI(6)
      HPRGL = KI(7)
      HPRNO = KI(8)
      HPREL = KI(9)

C---     Récupère les donnés de la formulation
      FRML = LM_FRML_REQFRML (HFRML)
      ITPG = LM_FRML_REQTGELV(HFRML)

C---     Construis et initialise l'objet
      L = SP_STRN_LEN(FRML)
      IF (ERR_GOOD()) OELEM => LM_LGCY_CTR(FRML(1:L), ITPG)
      IF (ERR_GOOD()) IERR = OELEM%INI(HCONF,
     &                                 HGRID,
     &                                 HDLIB,
     &                                 HCLIM,
     &                                 HSOLC,
     &                                 HSOLR,
     &                                 HPRGL,
     &                                 HPRNO,
     &                                 HPREL)
      IF (ERR_GOOD()) IERR = LM_HELE_CTR(HOBJ, OELEM)
     
      IC_SIMD_XEQCTR_TRV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SIMD_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SIMD_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icsimd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem_ic.fi'
      
      INTEGER IERR
C------------------------------------------------------------------------

      IF (IMTH .EQ. 'help') THEN
         CALL IC_SIMD_AID()
      ELSE
C        <include>IC_LM_ELEM_XEQMTH@lmelem_ic.for</include>
         IERR = IC_LM_ELEM_XEQMTH(HOBJ, IMTH, IPRM)
      ENDIF

      IC_SIMD_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SIMD_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SIMD_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icsimd.fi'
C-------------------------------------------------------------------------

C<comment>
C *****<br>
C This command has been DEPRECATED. Please use the specialized elements instead.<br>
C *****
C <p>
C The class <b>simd</b> represents the simulation data. It assembles all the all
C data related to a simulation. By regrouping all the data, <b>simd</b> controls
C the coherence between the chosen formulation and the different data, in
C terms of dimensions.
C</comment>
      IC_SIMD_REQCLS = 'simd'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SIMD_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SIMD_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icsimd.fi'
      INCLUDE 'lmhele.fi'
C-------------------------------------------------------------------------

      IC_SIMD_REQHDL = LM_HELE_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SIMD_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icsimd.hlp')

      RETURN
      END

