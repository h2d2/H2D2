C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Notes:
C  Les éléments sont des classes avec CTR et DTR. 2 cas de figures:
C  1. Statique: ils enregistrent les fonctions dans 000 et les appels
C     se font avec HOBJ comme premier paramètre.
C  2. Dynamique: ils enregistrent les méthodes.
C
C Functions:
C   Public:
C     INTEGER LM_LGCY_000
C     INTEGER LM_LGCY_999
C     INTEGER LM_LGCY_CTR
C     INTEGER LM_LGCY_DTR
C     INTEGER LM_LGCY_INI
C     INTEGER LM_LGCY_RST
C     INTEGER LM_LGCY_REQHBASE
C     LOGICAL LM_LGCY_HVALIDE
C   Private:
C     SUBROUTINE LM_LGCY_REQSELF
C     SUBROUTINE LM_LGCY_REQGDTA
C     SUBROUTINE LM_LGCY_REQEDTA
C     INTEGER LM_LGCY_INIVTBL
C     INTEGER LM_LGCY_RAZ
C
C************************************************************************

      MODULE LM_LGCY_M

      USE LM_ELEM_M, ONLY: LM_ELEM_T
      IMPLICIT NONE

      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: LM_LGCY_HBASE = 0

C---     Attributs privés
      TYPE, EXTENDS(LM_ELEM_T) :: LM_LGCY_T
         INTEGER, PRIVATE :: HMDUL
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR     => LM_LGCY_DTR
         PROCEDURE, PUBLIC :: CLCF    => LM_LGCY_CLCF
         PROCEDURE, PUBLIC :: CLCK    => LM_LGCY_CLCK
         PROCEDURE, PUBLIC :: CLCKT   => LM_LGCY_CLCKT
         PROCEDURE, PUBLIC :: CLCKU   => LM_LGCY_CLCKU
         PROCEDURE, PUBLIC :: CLCM    => LM_LGCY_CLCM
         PROCEDURE, PUBLIC :: CLCMU   => LM_LGCY_CLCMU
         !PROCEDURE, PUBLIC :: CLCRES  => LM_LGCY_CLCRES
         PROCEDURE, PUBLIC :: CLCCLIM => LM_LGCY_CLCCLIM
         PROCEDURE, PUBLIC :: CLCDLIB => LM_LGCY_CLCDLIB
         PROCEDURE, PUBLIC :: CLCPRES => LM_LGCY_CLCPRES
         PROCEDURE, PUBLIC :: CLCPREV => LM_LGCY_CLCPREV
         PROCEDURE, PUBLIC :: CLCPRNO => LM_LGCY_CLCPRNO
         PROCEDURE, PUBLIC :: PRCCLIM => LM_LGCY_PRCCLIM
         PROCEDURE, PUBLIC :: PRCDLIB => LM_LGCY_PRCDLIB
         PROCEDURE, PUBLIC :: PRCPRES => LM_LGCY_PRCPRES
         PROCEDURE, PUBLIC :: PRCPREV => LM_LGCY_PRCPREV
         PROCEDURE, PUBLIC :: PRCPRGL => LM_LGCY_PRCPRGL
         PROCEDURE, PUBLIC :: PRCPRLL => LM_LGCY_PRCPRLL
         PROCEDURE, PUBLIC :: PRCPRNO => LM_LGCY_PRCPRNO
         PROCEDURE, PUBLIC :: PRCSOLC => LM_LGCY_PRCSOLC
         PROCEDURE, PUBLIC :: PRCSOLR => LM_LGCY_PRCSOLR
         PROCEDURE, PUBLIC :: PRNCLIM => LM_LGCY_PRNCLIM
         PROCEDURE, PUBLIC :: PRNPRGL => LM_LGCY_PRNPRGL
         PROCEDURE, PUBLIC :: PRNPRNO => LM_LGCY_PRNPRNO
         PROCEDURE, PUBLIC :: PSCPRNO => LM_LGCY_PSCPRNO
         PROCEDURE, PUBLIC :: PSLCLIM => LM_LGCY_PSLCLIM
         PROCEDURE, PUBLIC :: PSLDLIB => LM_LGCY_PSLDLIB
         PROCEDURE, PUBLIC :: PSLPREV => LM_LGCY_PSLPREV
         PROCEDURE, PUBLIC :: PSLPRGL => LM_LGCY_PSLPRGL
         PROCEDURE, PUBLIC :: PSLPRNO => LM_LGCY_PSLPRNO
         PROCEDURE, PUBLIC :: PSLSOLC => LM_LGCY_PSLSOLC
         PROCEDURE, PUBLIC :: PSLSOLR => LM_LGCY_PSLSOLR
         !PROCEDURE, PUBLIC :: PST

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: ASGCMN => LM_LGCY_ASGCMN
         PROCEDURE, PRIVATE :: INIPRMS=> LM_LGCY_INIPRMS
         PROCEDURE, PRIVATE :: REQFNC => LM_LGCY_REQFNC
         PROCEDURE, PRIVATE :: REQNFN => LM_LGCY_REQNFN
      END TYPE LM_LGCY_T

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LM_LGCY_ASGCMN(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_ASGCMN
         
         MODULE INTEGER FUNCTION LM_LGCY_INIPRMS(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_INIPRMS
         
         MODULE INTEGER FUNCTION LM_LGCY_REQFNC(SELF, IFUNC, HFUNC,LDTR)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: IFUNC
            INTEGER, INTENT(OUT) :: HFUNC
            LOGICAL, INTENT(OUT) :: LDTR
         END FUNCTION LM_LGCY_REQFNC
         
         MODULE INTEGER FUNCTION LM_LGCY_REQNFN(SELF, IFUNC, KFUNC)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: IFUNC
            INTEGER, INTENT(OUT) :: KFUNC(:)
         END FUNCTION LM_LGCY_REQNFN

         ! ---  Fonction ASM
         MODULE INTEGER FUNCTION LM_LGCY_CLCF(SELF, VRES)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION LM_LGCY_CLCF

         MODULE INTEGER FUNCTION LM_LGCY_CLCKU(SELF, VRES)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION LM_LGCY_CLCKU

         MODULE INTEGER FUNCTION LM_LGCY_CLCK(SELF, HMTRX, F_ASM)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_LGCY_CLCK

         MODULE INTEGER FUNCTION LM_LGCY_CLCKT(SELF, HMTRX, F_ASM)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_LGCY_CLCKT

         MODULE INTEGER FUNCTION LM_LGCY_CLCM(SELF, HMTRX, F_ASM)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_LGCY_CLCM

         MODULE INTEGER FUNCTION LM_LGCY_CLCMU(SELF, VRES)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION LM_LGCY_CLCMU

         ! ---  Fonction CLC
         MODULE INTEGER FUNCTION LM_LGCY_CLCCLIM(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_CLCCLIM

         MODULE INTEGER FUNCTION LM_LGCY_CLCDLIB(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_CLCDLIB

         MODULE INTEGER FUNCTION LM_LGCY_CLCPRES(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_CLCPRES

         MODULE INTEGER FUNCTION LM_LGCY_CLCPREV(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_CLCPREV

         MODULE INTEGER FUNCTION LM_LGCY_CLCPRNO(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_CLCPRNO

         ! ---  Fonction PRC
         MODULE INTEGER FUNCTION LM_LGCY_PRCCLIM(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCCLIM

         MODULE INTEGER FUNCTION LM_LGCY_PRCDLIB(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCDLIB

         MODULE INTEGER FUNCTION LM_LGCY_PRCPRES(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCPRES

         MODULE INTEGER FUNCTION LM_LGCY_PRCPREV(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCPREV

         MODULE INTEGER FUNCTION LM_LGCY_PRCPRGL(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCPRGL

         MODULE INTEGER FUNCTION LM_LGCY_PRCPRLL(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCPRLL

         MODULE INTEGER FUNCTION LM_LGCY_PRCPRNO(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCPRNO

         MODULE INTEGER FUNCTION LM_LGCY_PRCSOLC(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCSOLC

         MODULE INTEGER FUNCTION LM_LGCY_PRCSOLR(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PRCSOLR

         ! ---  Fonction PRN
         MODULE INTEGER FUNCTION LM_LGCY_PRNCLIM(SELF)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
         END FUNCTION LM_LGCY_PRNCLIM

         MODULE INTEGER FUNCTION LM_LGCY_PRNPRGL(SELF)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
         END FUNCTION LM_LGCY_PRNPRGL

         MODULE INTEGER FUNCTION LM_LGCY_PRNPRNO(SELF)
            CLASS(LM_LGCY_T), INTENT(IN), TARGET :: SELF
         END FUNCTION LM_LGCY_PRNPRNO

         ! ---  Fonction PSC
         MODULE INTEGER FUNCTION LM_LGCY_PSCPRNO(SELF, MODIF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
            LOGICAL, INTENT(OUT) :: MODIF
         END FUNCTION LM_LGCY_PSCPRNO

         ! ---  Fonction PSL
         MODULE INTEGER FUNCTION LM_LGCY_PSLCLIM(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLCLIM

         MODULE INTEGER FUNCTION LM_LGCY_PSLDLIB(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLDLIB

         MODULE INTEGER FUNCTION LM_LGCY_PSLPREV(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLPREV

         MODULE INTEGER FUNCTION LM_LGCY_PSLPRGL(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLPRGL

         MODULE INTEGER FUNCTION LM_LGCY_PSLPRNO(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLPRNO

         MODULE INTEGER FUNCTION LM_LGCY_PSLSOLC(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLSOLC

         MODULE INTEGER FUNCTION LM_LGCY_PSLSOLR(SELF)
            CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_LGCY_PSLSOLR

      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_LGCY_CTR(FORML, ITPGEO) RESULT(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_CTR
CDEC$ ENDIF

      USE LG_LGCY_M, ONLY: LG_LGCY_T, LG_LGCY_CTR
      USE LM_ELEM_M, ONLY: LM_ELEM_CTR

      CHARACTER*(*), INTENT(IN) :: FORML
      INTEGER,       INTENT(IN) :: ITPGEO

      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      CLASS(LG_LGCY_T), POINTER :: OGEOM
      CLASS(LM_ELEM_T), POINTER :: OPRNT
      TYPE (LM_LGCY_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(FORML) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Construis les parents
      IF (ERR_GOOD()) OGEOM => LG_LGCY_CTR(ITPGEO)
      IF (ERR_GOOD()) OPRNT => LM_ELEM_CTR(SELF, OGEOM)

C---     Connecte au module
      SELF%HMDUL = 0
      IF (ERR_GOOD()) IERR = SO_UTIL_REQHMDLNOM(SO_MDUL_TYP_ELEMENT,
     &                                          FORML,
     &                                          SELF%HMDUL)
D     CALL ERR_ASR(ERR_BAD() .OR. SO_MDUL_HVALIDE(SELF%HMDUL))

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION LM_LGCY_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_LGCY_DTR(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_LGCY_DTR
CDEC$ ENDIF

      CLASS(LM_LGCY_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
!!      INCLUDE 'f_lc.fi'
!!      INCLUDE 'obobjc.fi'
!!      INCLUDE 'obvtbl.fi'
!!      INCLUDE 'somdul.fi'
!!
!!      INTEGER  IERR
!!      INTEGER  HVTBL, HELEM, HGEOM
!!C------------------------------------------------------------------------
!!
!!      IERR = ERR_OK
!!      HELEM = 0
!!      HGEOM = 0
!!
!!C---     Récupère les attributs
!!      IF (OB_VTBL_HVALIDE(SELF%HVTBL)) THEN
!!         HELEM = LM_UTIL_REQxxx(HOBJ, IX_ELE)
!!         HGEOM = LM_UTIL_REQxxx(HOBJ, IX_GEO)
!!      ENDIF
!!
!!C---     Reset les données
!!      IERR = LM_LGCY_RST(HOBJ)
!!
!!C---     Détruis les parents
!!      IF (ERR_GOOD()) IERR = LM_ELEM_DTR(HELEM)
!!      IF (ERR_GOOD()) IERR = LM_LGCY_DTR(HGEOM)
!!
!!C---     Désalloue la structure
!!      IF (ASSOCIATED(SELF)) THEN
!!         DEALLOCATE(SELF)
!!      ENDIF
         
      LM_LGCY_DTR = ERR_TYP()
      RETURN
      END FUNCTION LM_LGCY_DTR

      END MODULE LM_LGCY_M
