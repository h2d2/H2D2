C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE LM_ELNW_XPLSIM
C     SUBROUTINE LM_ELIB_XPLSIM
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Pré-calcul des propriétées nodales
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_ELNW_XPLSIM(HSIMD, KI)
CDEC$ATTRIBUTES DLLEXPORT :: LM_ELNW_XPLSIM

      IMPLICIT NONE

      INTEGER HSIMD
      INTEGER KI(22)

      INCLUDE 'lmelnw.fi'
      INCLUDE 'eafunc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'lmelib.fi'
      INCLUDE 'lmfrml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HCB1
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      HCB1 = LM_FRML_REQHCB1(HS_SIMD_REQHFRML(HSIMD))
      IF (SO_FUNC_HVALIDE(HCB1)) THEN
         CALL LOG_TODO('LM_ELNW_XPLSIM')
         CALL ERR_ASR(.FALSE.)
!         IERR = SO_FUNC_CALL1(HCB1,
!     &                        BA(SO_ALLC_CSTK2B(BA, EA_FUNC_XPLSIM)))
      ELSE
         IERR = LM_ELIB_XPLSIM(HSIMD, KI)
      ENDIF

      LM_ELNW_XPLSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Explose un handle HSIMD et retourne les pointeurs aux tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION LM_ELIB_XPLSIM(HSIMD, KI)
CDEC$ATTRIBUTES DLLEXPORT :: LM_ELIB_XPLSIM

      IMPLICIT NONE

      INTEGER HSIMD
      INTEGER KI(22)

      INCLUDE 'lmelib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelib.fc'
      INCLUDE 'lmglib.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = LM_ELIB_ASGSIMD(HSIMD)

      KI( 1) = LM_GLIB_LCORG
      KI( 2) = LM_ELIB_LLOCN
      KI( 3) = LM_GLIB_LNGV
      KI( 4) = LM_GLIB_LNGS
      KI( 5) = LM_GLIB_LDJV
      KI( 6) = LM_GLIB_LDJS
      KI( 7) = LM_ELIB_LPRGL
      KI( 8) = LM_ELIB_LPRNO
      KI( 9) = LM_ELIB_LPREV
      KI(10) = LM_ELIB_LPRES
      KI(11) = LM_ELIB_LSOLC
      KI(12) = LM_ELIB_LSOLR
      KI(13) = LM_ELIB_LCLCND
      KI(14) = LM_ELIB_LCLCNV
      KI(15) = LM_GLIB_LCLLIM
      KI(16) = LM_GLIB_LCLNOD
      KI(17) = LM_GLIB_LCLELE
      KI(18) = LM_GLIB_LCLDST
      KI(19) = LM_ELIB_LDIMP
      KI(20) = LM_ELIB_LDIMV
      KI(21) = LM_ELIB_LEIMP
      KI(22) = LM_ELIB_LDLG

      LM_ELIB_XPLSIM = ERR_TYP()
      RETURN
      END

