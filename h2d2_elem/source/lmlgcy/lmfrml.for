C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_FRML_000
C     INTEGER LM_FRML_999
C     INTEGER LM_FRML_CTR
C     INTEGER LM_FRML_DTR
C     INTEGER LM_FRML_INIFRM
C     INTEGER LM_FRML_RST
C     INTEGER LM_FRML_REQHBASE
C     LOGICAL LM_FRML_HVALIDE
C     CHARACTER*256 LM_FRML_REQFRML
C     INTEGER LM_FRML_REQHMDL
C     INTEGER LM_FRML_REQNDLN
C     INTEGER LM_FRML_REQNDLEV
C     INTEGER LM_FRML_REQNDLES
C     INTEGER LM_FRML_REQNPRGL
C     INTEGER LM_FRML_REQNPRGLL
C     INTEGER LM_FRML_REQNPRNO
C     INTEGER LM_FRML_REQNPRNOL
C     INTEGER LM_FRML_REQNPREV
C     INTEGER LM_FRML_REQNPREVL
C     INTEGER LM_FRML_REQNPRES
C     INTEGER LM_FRML_REQNSOLR
C     INTEGER LM_FRML_REQNSOLRL
C     INTEGER LM_FRML_REQNSOLC
C     INTEGER LM_FRML_REQNSOLCL
C     INTEGER LM_FRML_REQTGELV
C     LOGICAL LM_FRML_ESTLIN
C   Private:
C     INTEGER LM_FRML_INI_CB
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'lmfrml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

      IERR = OB_OBJC_000(LM_FRML_NOBJMAX,
     &                   LM_FRML_HBASE,
     &                   'Finite Element Formulation')

      LM_FRML_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER  IERR
      EXTERNAL LM_FRML_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(LM_FRML_NOBJMAX,
     &                   LM_FRML_HBASE,
     &                   LM_FRML_DTR)

      LM_FRML_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   LM_FRML_NOBJMAX,
     &                   LM_FRML_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(LM_FRML_HVALIDE(HOBJ))
         IOB = HOBJ - LM_FRML_HBASE

         LM_FRML_FORML   (IOB) = ' '
         LM_FRML_HMDL    (IOB) = 0
         LM_FRML_TGELV   (IOB) = 0
         LM_FRML_TGELS   (IOB) = 0
         LM_FRML_TGELZ   (IOB) = 0
         LM_FRML_NNELV   (IOB) = 0
         LM_FRML_NNELS   (IOB) = 0
         LM_FRML_NDLN    (IOB) = 0
         LM_FRML_NDLEV   (IOB) = 0
         LM_FRML_NDLES   (IOB) = 0
         LM_FRML_NPRGL   (IOB) = 0
         LM_FRML_NPRGLL  (IOB) = 0
         LM_FRML_NPRNO   (IOB) = 0
         LM_FRML_NPRNOL  (IOB) = 0
         LM_FRML_NPREV   (IOB) = 0
         LM_FRML_NPREV_D1(IOB) = 0
         LM_FRML_NPREV_D2(IOB) = 0
         LM_FRML_NPREVL  (IOB) = 0
         LM_FRML_NPRES   (IOB) = 0
         LM_FRML_NPRES_D1(IOB) = 0
         LM_FRML_NPRES_D2(IOB) = 0
         LM_FRML_NSOLR   (IOB) = 0
         LM_FRML_NSOLRL  (IOB) = 0
         LM_FRML_NSOLC   (IOB) = 0
         LM_FRML_NSOLCL  (IOB) = 0
         LM_FRML_ELELIN  (IOB) = .FALSE.
      ENDIF

      LM_FRML_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = LM_FRML_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   LM_FRML_NOBJMAX,
     &                   LM_FRML_HBASE)

      LM_FRML_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On fait un appel directement à l'élément ici pour couper
C     les liens avec LM_ELIB
C************************************************************************
      FUNCTION LM_FRML_INIFRM(HOBJ, FORML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_INIFRM
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) FORML

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      INTEGER  LM_FRML_INI_CB
      EXTERNAL LM_FRML_INI_CB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C--      CONTROLES DES PARAMETRES
      IF (SP_STRN_LEN(FORML) .LE. 0) GOTO 9900

C---     RESET LES DONNEES
      IERR = LM_FRML_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - LM_FRML_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLNOM(SO_MDUL_TYP_ELEMENT, FORML, HMDL)
D     CALL ERR_ASR(ERR_BAD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) THEN
         IERR = SO_MDUL_CCHFNC(HMDL, 'REQPRM', HFNC)
      ENDIF

C---     DEMANDE LES PARAM A L'ELEMENT
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CBFNC1(HFNC,
     &                         LM_FRML_INI_CB,
     &                         SO_ALLC_CST2B(HOBJ))
      ENDIF

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         LM_FRML_FORML (IOB) = FORML
         LM_FRML_HMDL  (IOB) = HMDL
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FORMULATION_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_FRML_INIFRM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_INI_CB(FNC_REQPARAM, HOBJ)

      IMPLICIT NONE

      INTEGER  LM_FRML_INI_CB
      INTEGER  HOBJ
      EXTERNAL FNC_REQPARAM

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER TGELV
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV, NPREV_D1, NPREV_D2
      INTEGER NPREVL
      INTEGER NPRES, NPRES_D1, NPRES_D2
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NPOST
      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - LM_FRML_HBASE

C---     DEMANDE LES PARAM A L'ELEMENT
      CALL FNC_REQPARAM(TGELV,
     &                  NPRGL,
     &                  NPRGLL,
     &                  NPRNO,
     &                  NPRNOL,
     &                  NPREV,
     &                  NPREVL,
     &                  NPRES,
     &                  NSOLC,
     &                  NSOLCL,
     &                  NSOLR,
     &                  NSOLRL,
     &                  NDLN,
     &                  NDLEV,
     &                  NDLES,
     &                  ASURFACE,
     &                  ESTLIN)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN

C---        Décode NPRES
         IF (NPREV .LT. 1000) THEN
            NPREV_D1 = NPREV
            NPREV_D2 = 1
         ELSE
            NPREV_D1 = NPREV / 1000
            NPREV_D2 = NPREV - NPREV_D1*1000
            NPREV = NPREV_D1 * NPREV_D2
         ENDIF
C---        Décode NPRES
         IF (NPRES .LT. 1000) THEN
            NPRES_D1 = NPRES
            NPRES_D2 = 1
         ELSE
            NPRES_D1 = NPRES / 1000
            NPRES_D2 = NPRES - NPRES_D1*1000
            NPRES = NPRES_D1 * NPRES_D2
         ENDIF

         LM_FRML_TGELV   (IOB) = TGELV
         LM_FRML_NDLN    (IOB) = NDLN
         LM_FRML_NDLEV   (IOB) = NDLEV
         LM_FRML_NDLES   (IOB) = NDLES
         LM_FRML_NPRGL   (IOB) = NPRGL
         LM_FRML_NPRGLL  (IOB) = NPRGLL
         LM_FRML_NPRNO   (IOB) = NPRNO
         LM_FRML_NPRNOL  (IOB) = NPRNOL
         LM_FRML_NPREV   (IOB) = NPREV
         LM_FRML_NPREVL  (IOB) = NPREVL
         LM_FRML_NPREV_D1(IOB) = NPREV_D1
         LM_FRML_NPREV_D2(IOB) = NPREV_D2
         LM_FRML_NPRES   (IOB) = NPRES
         LM_FRML_NPRES_D1(IOB) = NPRES_D1
         LM_FRML_NPRES_D2(IOB) = NPRES_D2
         LM_FRML_NSOLR   (IOB) = NSOLR
         LM_FRML_NSOLRL  (IOB) = NSOLRL
         LM_FRML_NSOLC   (IOB) = NSOLC
         LM_FRML_NSOLCL  (IOB) = NSOLCL
         LM_FRML_ELELIN  (IOB) = ESTLIN
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------

9999  CONTINUE
      LM_FRML_INI_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfrml.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - LM_FRML_HBASE

C---     RESET LES PARAMETRES
      LM_FRML_FORML   (IOB) = ' '
      LM_FRML_HMDL    (IOB) = 0
      LM_FRML_TGELV   (IOB) = 0
      LM_FRML_TGELS   (IOB) = 0
      LM_FRML_TGELZ   (IOB) = 0
      LM_FRML_NNELV   (IOB) = 0
      LM_FRML_NNELS   (IOB) = 0
      LM_FRML_NDLN    (IOB) = 0
      LM_FRML_NDLEV   (IOB) = 0
      LM_FRML_NDLES   (IOB) = 0
      LM_FRML_NPRGL   (IOB) = 0
      LM_FRML_NPRGLL  (IOB) = 0
      LM_FRML_NPRNO   (IOB) = 0
      LM_FRML_NPRNOL  (IOB) = 0
      LM_FRML_NPREV   (IOB) = 0
      LM_FRML_NPREV_D1(IOB) = 0
      LM_FRML_NPREV_D2(IOB) = 0
      LM_FRML_NPREVL  (IOB) = 0
      LM_FRML_NPRES   (IOB) = 0
      LM_FRML_NPRES_D1(IOB) = 0
      LM_FRML_NPRES_D2(IOB) = 0
      LM_FRML_NSOLR   (IOB) = 0
      LM_FRML_NSOLRL  (IOB) = 0
      LM_FRML_NSOLC   (IOB) = 0
      LM_FRML_NSOLCL  (IOB) = 0
      LM_FRML_ELELIN  (IOB) = .FALSE.

      LM_FRML_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction LM_FRML_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C------------------------------------------------------------------------

      LM_FRML_REQHBASE = LM_FRML_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction LM_FRML_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'lmfrml.fc'
C------------------------------------------------------------------------

      LM_FRML_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  LM_FRML_NOBJMAX,
     &                                  LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQFRML(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQFRML
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQFRML = LM_FRML_FORML(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQHMDL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQHMDL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQHMDL = LM_FRML_HMDL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNDLN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNDLN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNDLN = LM_FRML_NDLN(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNDLEV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNDLEV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNDLEV = LM_FRML_NDLEV(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNDLES(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNDLES
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNDLES = LM_FRML_NDLES(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPRGL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPRGL = LM_FRML_NPRGL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPRGLL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPRGLL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPRGLL = LM_FRML_NPRGLL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPRNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPRNO = LM_FRML_NPRNO(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPRNOL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPRNOL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPRNOL = LM_FRML_NPRNOL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPREV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPREV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPREV = LM_FRML_NPREV(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPREVL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPREVL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPREVL = LM_FRML_NPREVL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNPRES(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNPRES
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNPRES = LM_FRML_NPRES(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNSOLR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNSOLR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNSOLR = LM_FRML_NSOLR(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNSOLRL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNSOLRL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNSOLRL = LM_FRML_NSOLRL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNSOLC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNSOLC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNSOLC = LM_FRML_NSOLC(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQNSOLCL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQNSOLCL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQNSOLCL = LM_FRML_NSOLCL(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Type de Géométrie des ELements de Volume
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_REQTGELV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_REQTGELV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_REQTGELV = LM_FRML_TGELV(HOBJ-LM_FRML_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: .TRUE. si l'élément est linéaire
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_FRML_ESTLIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_FRML_ESTLIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmfrml.fi'
      INCLUDE 'lmfrml.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_FRML_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LM_FRML_ESTLIN = LM_FRML_ELELIN(HOBJ-LM_FRML_HBASE)
      RETURN
      END
