C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Interface:
C   H2D2 Module: GR
C      H2D2 Class: GR_GRID
C         FTN (Sub)Module: GR_GRID_M
C            Public:
C            Private:
C               SUBROUTINE GR_GRID_CTR
C               INTEGER GR_GRID_DTR
C            
C            FTN Type: GR_GRID_T
C               Public:
C                  INTEGER GR_GRID_PKL
C                  INTEGER GR_GRID_UPK
C                  INTEGER GR_GRID_INI
C                  INTEGER GR_GRID_RST
C                  INTEGER GR_GRID_PASDEB
C                  INTEGER GR_GRID_REQHGRID
C                  INTEGER GR_GRID_REQHCOOR
C                  INTEGER GR_GRID_REQHCONF
C                  INTEGER GR_GRID_REQHDIST
C                  INTEGER GR_GRID_REQHNUMC
C                  INTEGER GR_GRID_REQHNUME
C                  INTEGER GR_GRID_REQNDIM
C                  INTEGER GR_GRID_REQNNL
C                  INTEGER GR_GRID_REQNNP
C                  INTEGER GR_GRID_REQNNT
C                  INTEGER GR_GRID_REQNNELS
C                  INTEGER GR_GRID_REQNNELV
C                  INTEGER GR_GRID_REQNCELS
C                  INTEGER GR_GRID_REQNCELV
C                  INTEGER GR_GRID_REQNELLS
C                  INTEGER GR_GRID_REQNELLV
C                  INTEGER GR_GRID_REQNELTS
C                  INTEGER GR_GRID_REQNELTV
C                  INTEGER GR_GRID_REQNDJV
C                  INTEGER GR_GRID_REQNDJS
C                  INTEGER GR_GRID_REQLDJV
C                  INTEGER GR_GRID_REQLDJS
C               Private:
C                  INTEGER GR_GRID_RAZ
C
C************************************************************************

      MODULE GR_GRID_M

      USE GR_ELEM_M, ONLY: GR_ELEM_T
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T
      IMPLICIT NONE

      PRIVATE

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, PUBLIC :: GR_GRID_T
         CLASS(GR_ELEM_T), POINTER :: OELEV
         CLASS(GR_ELEM_T), POINTER :: OELES
         TYPE(SO_ALLC_VPTR2_T) :: PDJV
         TYPE(SO_ALLC_VPTR2_T) :: PDJS
         INTEGER HGRID
         !INTEGER NDJV
         !INTEGER NDJS
      CONTAINS
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI       => GR_GRID_INI
         PROCEDURE, PUBLIC :: RST       => GR_GRID_RST
         PROCEDURE, PUBLIC :: PKL       => GR_GRID_PKL
         PROCEDURE, PUBLIC :: UPK       => GR_GRID_UPK
!!!         PROCEDURE, PUBLIC :: PRN       => GR_GRID_PRN
         PROCEDURE, PUBLIC :: PASDEB    => GR_GRID_PASDEB
!!!         PROCEDURE, PUBLIC :: PASFIN    => GR_GRID_PASFIN

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHGRID   => GR_GRID_REQHGRID
         PROCEDURE, PUBLIC :: REQHCOOR   => GR_GRID_REQHCOOR
!!!         PROCEDURE, PUBLIC :: REQOELEV   => GR_GRID_REQOELEV
!!!         PROCEDURE, PUBLIC :: REQOELES   => GR_GRID_REQOELES
         PROCEDURE, PUBLIC :: REQHCONF   => GR_GRID_REQHCONF
         PROCEDURE, PUBLIC :: REQHDIST   => GR_GRID_REQHDIST
         PROCEDURE, PUBLIC :: REQHNUMC   => GR_GRID_REQHNUMC
         PROCEDURE, PUBLIC :: REQHNUME   => GR_GRID_REQHNUME
!!!         PROCEDURE, PUBLIC :: REQTGELVER => GR_GRID_REQTGEL
!!!         PROCEDURE, PUBLIC :: REQTGELSER => GR_GRID_REQTGEL
         PROCEDURE, PUBLIC :: REQNDIM    => GR_GRID_REQNDIM
         PROCEDURE, PUBLIC :: REQNNT     => GR_GRID_REQNNT
         PROCEDURE, PUBLIC :: REQNNP     => GR_GRID_REQNNP
         PROCEDURE, PUBLIC :: REQNNL     => GR_GRID_REQNNL

         PROCEDURE, PUBLIC :: REQNNELV   => GR_GRID_REQNNELV
         PROCEDURE, PUBLIC :: REQNCELV   => GR_GRID_REQNCELV
         PROCEDURE, PUBLIC :: REQNELTV   => GR_GRID_REQNELTV
         PROCEDURE, PUBLIC :: REQNELLV   => GR_GRID_REQNELLV
         PROCEDURE, PUBLIC :: REQNDJV    => GR_GRID_REQNDJV
         PROCEDURE, PUBLIC :: REQLDJV    => GR_GRID_REQLDJV

         PROCEDURE, PUBLIC :: REQNNELS   => GR_GRID_REQNNELS
         PROCEDURE, PUBLIC :: REQNCELS   => GR_GRID_REQNCELS
         PROCEDURE, PUBLIC :: REQNELTS   => GR_GRID_REQNELTS
         PROCEDURE, PUBLIC :: REQNELLS   => GR_GRID_REQNELLS
         PROCEDURE, PUBLIC :: REQNDJS    => GR_GRID_REQNDJS
         PROCEDURE, PUBLIC :: REQLDJS    => GR_GRID_REQLDJS

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ       => GR_GRID_RAZ
      END TYPE GR_GRID_T

      PUBLIC :: GR_GRID_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: GR_GRID_DTR
      END INTERFACE DEL

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_PKL(SELF, HXML)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
      INTEGER HXML

      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER N, L
      INTEGER NELV, NELS, NDJV, NDJS
C------------------------------------------------------------------------

C---     Récupère les paramètres
      !!NELV = GR_GRID_REQNELL(SELF%HGRID(IOB))
      !!NELS = GR_ELEM_REQNELL(SELF%HELES(IOB))
      !!!NDJV = GR_ELEM_REQNDJ (SELF%HELEV(IOB))
      !!!NDJS = GR_ELEM_REQNDJ (SELF%HELES(IOB))

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, SELF%HGRID)
!!!      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, SELF%OELES)

!!!C---     La table des métriques de volume
!!!      IF (ERR_GOOD()) THEN
!!!         N = NELV * NDJV
!!!         L = GR_GRID_LDJV(IOB)
!!!         IERR = IO_XML_WD_V(HXML, N, N, L, 1)
!!!      ENDIF
!!!
!!!C---     La table des métriques de surface
!!!      IF (ERR_GOOD()) THEN
!!!         N = NELS * NDJS
!!!         L = GR_GRID_LDJS(IOB)
!!!         IERR = IO_XML_WD_V(HXML, N, N, L, 1)
!!!      ENDIF

      GR_GRID_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Pas d'assertion: On ne peut pas faire d'appels de fonctions
C     tant que tous les objets n'ont pas été reconstruits.
C************************************************************************
      INTEGER FUNCTION GR_GRID_UPK(SELF, LNEW, HXML)

      CLASS(GR_GRID_T), INTENT(INOUT) :: SELF
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, NX, N
C------------------------------------------------------------------------

C---     Efface l'objet
      IF (ERR_GOOD()) IERR = SELF%RAZ()

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, SELF%HGRID)
!!!      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, SELF%OELES)

!!!C---     La table des métriques de volume
!!!      L = 0
!!!      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
!!!      GR_GRID_LDJV(IOB) = L      ! cf note
!!!
!!!C---     La table des métriques de surface
!!!      L = 0
!!!      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
!!!      GR_GRID_LDJS(IOB) = L      ! cf note

      GR_GRID_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GR_GRID_CTR() RESULT(SELF)

      IMPLICIT NONE

      INCLUDE 'err.fi'

      TYPE(GR_GRID_T), POINTER :: SELF
      INTEGER IERR, IRET
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      IERR = SELF%RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION GR_GRID_DTR(SELF)

      CLASS(GR_GRID_T), INTENT(INOUT), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

      IF (.NOT. ASSOCIATED(SELF)) GOTO 9999

      IERR = SELF%RST()

      DEALLOCATE(SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900
      SELF => NULL()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_DEALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      GR_GRID_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION GR_GRID_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(GR_GRID_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

      SELF%OELEV => NULL()
      SELF%OELES => NULL()
      SELF%PDJV  = SO_ALLC_VPTR2()
      SELF%PDJS  = SO_ALLC_VPTR2()
      SELF%HGRID = 0

      GR_GRID_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction GR_GRID_INI initialise l'objet à l'aide des valeurs passées
C     en paramètre.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION GR_GRID_INI(SELF, GDTA, HGRID, HCONF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE GR_ELEM_M, ONLY: GR_ELEM_T, GR_ELEM_CTR
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(GR_GRID_T), INTENT(INOUT) :: SELF
      CLASS(LM_GDTA_T), INTENT(IN)    :: GDTA
      INTEGER HGRID
      INTEGER HCONF

      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'lmgeom.fi'

      INTEGER IERR
      CLASS(GR_ELEM_T), POINTER :: OELEV
      CLASS(GR_ELEM_T), POINTER :: OELES
      INTEGER H
C------------------------------------------------------------------------

C--      Contrôles des paramètres
      IF (.NOT. DT_GRID_HVALIDE(HGRID)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Construis le maillage de volume
      H = DT_GRID_REQHELEV(HGRID)
      IF (ERR_GOOD()) OELEV => GR_ELEM_CTR()
      IF (ERR_GOOD()) IERR = OELEV%INIVOL(GDTA, H, HCONF)

C---     Construis le maillage de peau
      IF (ERR_GOOD()) OELES => GR_ELEM_CTR()
      IF (ERR_GOOD()) IERR = OELES%INISRF(GDTA, OELEV)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OELEV => OELEV
         SELF%OELES => OELES
         SELF%PDJV  = SO_ALLC_VPTR2()
         SELF%PDJS  = SO_ALLC_VPTR2()
         SELF%HGRID = HGRID
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_GRID_INVALIDE',': ',HGRID
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GR_GRID_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est récupérée dans RAZ par réinitialisation des pointeurs
C************************************************************************
      INTEGER FUNCTION GR_GRID_RST(SELF)

      USE GR_ELEM_M, ONLY: DEL

      CLASS(GR_GRID_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Détruis les maillages
      IF (ASSOCIATED(SELF%OELES)) IERR = DEL(SELF%OELES)
      IF (ASSOCIATED(SELF%OELEV)) IERR = DEL(SELF%OELEV)

C---     Reset les données
      IERR = SELF%RAZ()

      GR_GRID_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION GR_GRID_PASDEB(SELF, GDTA, TEMPS, MODIF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(GR_GRID_T), INTENT(INOUT) :: SELF
      CLASS(LM_GDTA_T), INTENT(IN)    :: GDTA
      REAL*8,  INTENT(IN)  :: TEMPS
      LOGICAL, INTENT(OUT) :: MODIF

      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'lmgeom.fi'

      INTEGER IERR
      INTEGER HCOOR
      INTEGER HNUMC, HNUME
      INTEGER NDJV,  NDJS
      INTEGER NELV,  NELS
      LOGICAL MDFCOR, MDFELV, MDFELS
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid')

C---     Récupère les attributs
      HCOOR = SELF%REQHCOOR()
      HNUMC = SELF%REQHNUMC()
      HNUME = SELF%REQHNUME()
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUME))

C---     Charge les données
      MDFCOR = .FALSE.
      MDFELV = .FALSE.
      MDFELS = .FALSE.
      IF (ERR_GOOD())
     &   IERR = DT_VNOD_CHARGE(HCOOR, HNUMC, TEMPS, MDFCOR)
      IF (ERR_GOOD())
     &   IERR = SELF%OELEV%PASDEB(HNUMC, HNUME, TEMPS, MDFELV)
      IF (ERR_GOOD())
     &   IERR = SELF%OELES%PASDEB(HNUMC, HNUME, TEMPS, MDFELS)

C---     (Ré)alloue les tables des métriques
      MODIF = (MDFCOR .OR. MDFELV .OR. MDFELS)
      IF (ERR_GOOD()) THEN
C---        Modifs ==> (Ré)alloue les tables des métriques
         IF (MODIF) THEN
C---           Récupère les paramètres
            NDJV = GDTA%NDJV
            NDJS = GDTA%NDJS
            NELV = SELF%OELEV%REQNELL()
            NELS = SELF%OELES%REQNELL()
C---           Alloue l'espace pour les métriques
            IF (ERR_GOOD()) SELF%PDJV = SO_ALLC_VPTR2(NDJV, NELV)
            IF (ERR_GOOD()) SELF%PDJS = SO_ALLC_VPTR2(NDJS, NELS)

C---        Pas de modifs
         ELSE
D           CALL ERR_ASR(SELF%PDJV%ISVALID())
D           CALL ERR_ASR(SELF%PDJS%ISVALID())
         ENDIF
      ENDIF

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.grid')

      GR_GRID_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>GR_GRID_REQHGRID(...)</code> retourne le handle
C     au DT_GRID associé à l'objet courant.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHGRID(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQHGRID = SELF%HGRID
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHCOOR(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQHCOOR = DT_GRID_REQHCOOR(HGRID)
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      INTEGER FUNCTION GR_GRID_REQHELEV(SELF)
!!!
!!!      CLASS(GR_GRID_T), INTENT(IN) :: SELF
!!!C------------------------------------------------------------------------
!!!
!!!      GR_GRID_REQHELEV = SELF%OELEV
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      INTEGER FUNCTION GR_GRID_REQHELES(SELF)
!!!
!!!      CLASS(GR_GRID_T), INTENT(IN) :: SELF
!!!C------------------------------------------------------------------------
!!!
!!!      GR_GRID_REQHELES = SELF%OELES
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire: Retourne le handle sur la configuration de calcul.
C
C Description:
C     La fonction DT_GRID_REQHCONF retourne le handle sur
C     la configuration de calcul (GPU) associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHCONF(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQHCONF = SELF%OELEV%REQHCONF()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la distribution des noeuds.
C
C Description:
C     La fonction GR_GRID_REQHDIST retourne le handle sur
C     la distribution des noeuds (GPU) associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHDIST(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQHDIST = SELF%OELEV%REQHDIST()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la numérotation des noeuds.
C
C Description:
C     La fonction GR_GRID_REQHNUMC retourne le handle sur
C     la renumérotation des noeuds associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHNUMC(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SELF%HGRID .NE. 0)
C------------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQHNUMC = DT_GRID_REQHNUMC(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la renumérotation des éléments.
C
C Description:
C     La fonction GR_GRID_REQHNUME retourne le handle sur
C     la renumérotation des éléments associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQHNUME(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SELF%HGRID .NE. 0)
C------------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQHNUME = DT_GRID_REQHNUME(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNDIM
C
C Description:
C     La fonction GR_GRID_REQNDIM retourne le nombre de dimension.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNDIM(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQNDIM = DT_GRID_REQNDIM(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNNL
C
C Description:
C     La fonction GR_GRID_REQNNL retourne le nombre de noeuds local.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNNL(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQNNL = DT_GRID_REQNNL(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNNP
C
C Description:
C     La fonction GR_GRID_REQNNP retourne le nombre de noeuds privés.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNNP(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQNNP = DT_GRID_REQNNP(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNNT
C
C Description:
C     La fonction GR_GRID_REQNNT retourne le nombre de noeuds total (global).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNNT(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF

      INCLUDE 'dtgrid.fi'

      INTEGER HGRID
C-----------------------------------------------------------------------

      HGRID = SELF%HGRID
      GR_GRID_REQNNT = DT_GRID_REQNNT(HGRID)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNNELS
C
C Description:
C     La fonction GR_GRID_REQNNELS retourne le nombre de noeuds par
C     élément de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNNELS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNNELS = SELF%OELES%REQNNEL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNNELV
C
C Description:
C     La fonction GR_GRID_REQNNELV retourne le nombre de noeuds par
C     élément de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNNELV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNNELV = SELF%OELEV%REQNNEL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNCELS
C
C Description:
C     La fonction GR_GRID_REQNCELS retourne le nombre de connectivités par
C     élément de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNCELS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNCELS = SELF%OELES%REQNCEL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNCELV
C
C Description:
C     La fonction GR_GRID_REQNCELV retourne le nombre de connectivités par
C     élément de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNCELV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNCELV = SELF%OELEV%REQNCEL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNELLS
C
C Description:
C     La fonction GR_GRID_REQNELLS retourne le nombre local d'éléments
C     de surface.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNELLS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNELLS = SELF%OELES%REQNELL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNELLV
C
C Description:
C     La fonction GR_GRID_REQNELLV retourne le nombre local d'éléments
C     de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNELLV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNELLV = SELF%OELEV%REQNELL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNELTS
C
C Description:
C     La fonction GR_GRID_REQNELTS retourne le nombre total d'éléments
C     de surface.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNELTS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      GR_GRID_REQNELTS = SELF%OELES%REQNELT()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQNELTV
C
C Description:
C     La fonction GR_GRID_REQNELTV retourne le nombre total d'éléments
C     de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNELTV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQNELTV = SELF%OELEV%REQNELT()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNDJV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      GR_GRID_REQNDJV = SIZE(SELF%PDJV%VPTR, 1)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQNDJS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      GR_GRID_REQNDJS = SIZE(SELF%PDJS%VPTR, 1)
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQLDJV
C
C Description:
C     La méthode GR_GRID_REQLDJV retourne le pointeur au métriques de
C     volume. Le pointeur peut être nul.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQLDJV(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      GR_GRID_REQLDJV = SELF%PDJV%REQHNDL()
      RETURN
      END

C************************************************************************
C Sommaire: GR_GRID_REQLDJS
C
C Description:
C     La méthode GR_GRID_REQLDJS retourne le pointeur au métriques de
C     surface. Le pointeur peut être nul.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION GR_GRID_REQLDJS(SELF)

      CLASS(GR_GRID_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      GR_GRID_REQLDJS = SELF%PDJS%REQHNDL()
      RETURN
      END

      END MODULE GR_GRID_M

