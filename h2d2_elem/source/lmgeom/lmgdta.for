C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  eLeMent
C Objet:   Géométrie DaTA
C Type:    Structure de données
C Note: La structure est publique
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************
      MODULE LM_GDTA_M

      IMPLICIT NONE
      PUBLIC
      
      INTEGER, PARAMETER :: NCOLMAX = 32  ! comme GR_ELEM_NCOLMAX

C---     Structure des données géométriques
      TYPE, PUBLIC :: LM_GDTA_T
         INTEGER :: ITPEV
         INTEGER :: ITPES
         INTEGER :: ITPEZ

         INTEGER :: NDIM
         INTEGER :: NNP
         INTEGER :: NNL
         INTEGER :: NNT
         REAL*8, POINTER :: VCORG(:,:)    ! VCORG(NDIM,NNL)

         INTEGER :: NNELV
         INTEGER :: NCELV
         INTEGER :: NELLV
!         INTEGER :: NELTV    Activer plus tard, lorsqu'on utilisera partout NELLV
         INTEGER, POINTER :: KNGV(:,:)    ! KNGV(NCELV,NELLV)
         INTEGER :: NNELS
         INTEGER :: NCELS
         INTEGER :: NELLS
!         INTEGER NELTS    Activer plus tard, lorsqu'on utilisera partout NELLS
         INTEGER, POINTER :: KNGS(:,:)    ! KNGS(NCELS,NELLS)
         INTEGER :: NNELZ
         INTEGER :: NCELZ
         INTEGER :: NELLZ
         INTEGER :: NEV2EZ

         INTEGER :: NDJV
         REAL*8, POINTER :: VDJV(:,:)     ! VDJV(NDJV,NNELV)

         INTEGER :: NDJS
         REAL*8, POINTER :: VDJS(:,:)     ! VDJS(NDJS,NNELS)

         INTEGER :: NDJZ

         INTEGER :: NCLLIM
         INTEGER :: NCLNOD
         INTEGER :: NCLELE
         INTEGER, POINTER :: KCLLIM(:,:)  ! KCLLIM(7,NCLLIM)
         INTEGER, POINTER :: KCLNOD(:)    ! KCLNOD(NCLNOD)
         INTEGER, POINTER :: KCLELE(:)    ! KCLELE(NCLELE)
         REAL*8,  POINTER :: VCLDST(:)    ! VCLDST(NCLNOD)

         INTEGER :: NELCOL
         INTEGER :: KELCOL(2, NCOLMAX)
      CONTAINS
         PROCEDURE, PUBLIC :: RST => LM_GDTA_RST
      END TYPE LM_GDTA_T

      CONTAINS
      
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GDTA_RST(SELF)
      
      CLASS(LM_GDTA_T), INTENT(INOUT) :: SELF
C------------------------------------------------------------------------
         
      SELF%ITPEV =  0
      SELF%ITPES =  0
      SELF%ITPEZ =  0

      SELF%NDIM  = -1
      SELF%NNP   = -1
      SELF%NNL   = -1
      SELF%NNT   = -1
      SELF%VCORG => NULL()

      SELF%NNELV  = -1
      SELF%NCELV  = -1
      SELF%NELLV  = -1
!         SELF%NELTV    Activer plus tard, lorsqu'on utilisera partout NELLV
      SELF%KNGV   => NULL()
      SELF%NNELS  = -1
      SELF%NCELS  = -1
      SELF%NELLS  = -1
!         SELF%NELTS    Activer plus tard, lorsqu'on utilisera partout NELLS
      SELF%KNGS   => NULL()
      SELF%NNELZ  = -1
      SELF%NCELZ  = -1
      SELF%NELLZ  = -1
      SELF%NEV2EZ =  0

      SELF%NDJV = -1
      SELF%VDJV => NULL()

      SELF%NDJS = -1
      SELF%VDJS => NULL()

      SELF%NDJZ = -1

      SELF%NCLLIM = -1
      SELF%NCLNOD = -1
      SELF%NCLELE = -1
      SELF%KCLLIM => NULL()
      SELF%KCLNOD => NULL()
      SELF%KCLELE => NULL()
      SELF%VCLDST => NULL()

      SELF%NELCOL = -1
      SELF%KELCOL(:, 1) = [-1, -1]
         
      LM_GDTA_RST = 0
      RETURN
      END FUNCTION LM_GDTA_RST
      
      END MODULE LM_GDTA_M
