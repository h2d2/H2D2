C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C  Élément géométrique, interface HOBJ pour LM_ELEM_T.
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_HELE
C         SUBROUTINE LM_HELE_000
C         SUBROUTINE LM_HELE_999
C         SUBROUTINE LM_HELE_DTR
C         SUBROUTINE LM_HELE_INI
C         SUBROUTINE LM_HELE_RST
C         SUBROUTINE LM_HELE_REQHBASE
C         SUBROUTINE LM_HELE_HVALIDE
C         SUBROUTINE LM_HELE_REQPRM
C         SUBROUTINE LM_HELE_ESTINI
C         SUBROUTINE LM_HELE_ESTLIS
C         SUBROUTINE LM_HELE_ESTPRC
C         SUBROUTINE LM_HELE_ESTCLC
C         SUBROUTINE LM_HELE_ESTPSC
C         SUBROUTINE LM_HELE_PUSHLDLG
C         SUBROUTINE LM_HELE_POPLDLG
C         SUBROUTINE LM_HELE_REQHLMGO
C         SUBROUTINE LM_HELE_REQHASH
C         SUBROUTINE LM_HELE_REQHNUMC
C         SUBROUTINE LM_HELE_REQHNUME
C         SUBROUTINE LM_HELE_REQTEMPS
C         FTN (Sub)Module: LM_HELE_M
C            Public:
C               TYPE (LM_EDTA_T), POINTER LM_HELE_REQEDTA
C               INTEGER LM_HELE_CTR
C            Private:
C               SUBROUTINE LM_HELE_REQSELF
C               SUBROUTINE LM_HELE_REQOMNG
C               TYPE (LM_GDTA_T), POINTER LM_HELE_REQGDTA
C            
C            FTN Type: LM_HELE_T
C               Public:
C               Private:
C
C************************************************************************

      MODULE LM_HELE_M

      USE LM_ELEM_M, ONLY: LM_ELEM_T
      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: LM_HELE_HBASE = 0

C---     Type pour encapsuler le pointeur
      TYPE :: LM_HELE_T
         CLASS(LM_ELEM_T), POINTER :: SELF   ! Pointeur à l'objet VRAI
         INTEGER :: HGEO                     ! Handle sur la géométrie
      END TYPE

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée LM_HELE_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQSELF(HOBJ) RESULT(SELF)

      USE ISO_C_BINDING

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (LM_HELE_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      RETURN
      END FUNCTION LM_HELE_REQSELF

C************************************************************************
C Sommaire:    Retourne le pointeur à l'objet managé.
C
C Description:
C     La fonction protégée LM_HELE_REQOMNG(...) retourne le pointeur à
C     la structure de données managée de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQOMNG(HOBJ) RESULT(SELF)

      USE ISO_C_BINDING

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
      TYPE (LM_HELE_T), POINTER :: SELF_H
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF_H)
      SELF => SELF_H%SELF

      RETURN
      END FUNCTION LM_HELE_REQOMNG

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée LM_HELE_REQGDTA(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQGDTA(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQGDTA
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      TYPE (LM_GDTA_T), POINTER :: LM_HELE_REQGDTA
      INTEGER, INTENT(IN) :: HOBJ

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQGDTA => SELF%GDTA
      RETURN
      END FUNCTION LM_HELE_REQGDTA

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée LM_HELE_REQEDTA(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQEDTA(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQEDTA
CDEC$ ENDIF

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      IMPLICIT NONE

      TYPE (LM_EDTA_T), POINTER :: LM_HELE_REQEDTA
      INTEGER HOBJ

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQEDTA => SELF%EDTA
      RETURN
      END FUNCTION LM_HELE_REQEDTA

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     Le constructeur de la classe LM_HELE_CTR fait toutes les
C     connections liées à la structure de la hiérarchie des classes.
C
C Entrée:
C     HKID     Handle sur l'héritier
C     HGEO     Handle sur l'élément géométrique parent
C
C Sortie:
C     HOBJ     Handle sur l'objet créé
C
C Notes:
C     La classe est abstraite, HKID doit exister.
C************************************************************************
      INTEGER FUNCTION LM_HELE_CTR(HOBJ, SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_CTR
CDEC$ ENDIF

      USE LM_HGEO_M, ONLY: LM_HGEO_CTR
      USE ISO_C_BINDING, ONLY: C_LOC

      INTEGER, INTENT(OUT) :: HOBJ
      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      INTEGER HGEO
      TYPE(LM_HELE_T), POINTER :: SELF_H
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      ALLOCATE(SELF_H)

C---     Encapsule le pointeur polymorphique
      SELF_H%SELF => SELF

C---     Handle sur la géométrie
      IF (ERR_GOOD()) IERR = LM_HGEO_CTR(HGEO, SELF%OGEO)

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   LM_HELE_HBASE,
     &                                   C_LOC(SELF_H))
D     CALL ERR_ASR(ERR_BAD() .OR. LM_HELE_HVALIDE(HOBJ))

      LM_HELE_CTR = ERR_TYP()
      RETURN
      END FUNCTION LM_HELE_CTR

      END MODULE LM_HELE_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_000
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(LM_HELE_HBASE,
     &                   'Finite Element')

      LM_HELE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_999
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL LM_HELE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(LM_HELE_HBASE,
     &                   LM_HELE_DTR)

      LM_HELE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Memory leak de LM_HELE_T
C************************************************************************
      FUNCTION LM_HELE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_DTR
CDEC$ ENDIF

      USE LM_HELE_M
      USE LM_ELEM_M, ONLY: DEL
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      CLASS(LM_HELE_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      SELF => LM_HELE_REQSELF(HOBJ)
      IERR = LM_HGEO_DTR(SELF%HGEO)
      IERR = DEL(SELF%SELF)

C---     Efface du registre
      IERR = OB_OBJN_DTR(HOBJ, LM_HELE_HBASE)
      HOBJ = 0

C---     Désalloue le mémoire
      DEALLOCATE(SELF)

      LM_HELE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il y a un lien 1-1 entre HSIMD et l'élément. Il faut quand même
C     réassigner le LM_EDTA_T à chaque appel, car les données
C     peuvent ne pas être encore toutes chargées, ou alors elle changent
C     (en particulier VDLG).
C************************************************************************
      FUNCTION LM_HELE_INI(HOBJ,
     &                     HCONF,
     &                     HGRID,
     &                     HDLIB,
     &                     HCLIM,
     &                     HSOLC,
     &                     HSOLR,
     &                     HPRGL,
     &                     HPRNO,
     &                     HPREV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_INI
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCONF
      INTEGER HGRID
      INTEGER HDLIB
      INTEGER HCLIM
      INTEGER HSOLC
      INTEGER HSOLR
      INTEGER HPRGL
      INTEGER HPRNO
      INTEGER HPREV

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_INI = SELF%INI(HCONF, HGRID,
     &                       HDLIB, HCLIM, HSOLC,
     &                       HSOLR,HPRGL,HPRNO,HPREV)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_RST
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_RST = SELF%RST()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction LM_HELE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHBASE
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INCLUDE 'lmhele.fi'
C------------------------------------------------------------------------

      LM_HELE_REQHBASE = LM_HELE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction LM_HELE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_HVALIDE
CDEC$ ENDIF

      USE LM_HELE_M
      INTEGER HOBJ

      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      LM_HELE_HVALIDE = OB_OBJN_HVALIDE(HOBJ, LM_HELE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQPRM(HOBJ, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQPRM
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: IPRM

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQPRM = SELF%REQPRM(IPRM)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_ESTINI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_ESTINI
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_ESTINI = SELF%ESTINI()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_ESTLIS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_ESTLIS
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_ESTLIS = SELF%ESTLIS()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_ESTPRC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_ESTPRC
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_ESTPRC = SELF%ESTPRC()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_ESTCLC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_ESTCLC
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_ESTCLC = SELF%ESTCLC()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_ESTPSC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_ESTPSC
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_ESTPSC = SELF%ESTPSC()
      RETURN
      END

C************************************************************************
C Sommaire: LM_HELE_PUSHLDLG
C
C Description:
C     La fonction LM_HELE_PUSHLDLG permet d'assigner à l'objet une table
C     externe comme VDLG. L'ancienne table est poussée sur une pile et
C     peut être dépilée par l'appel symétrique à LM_HELE_POPLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_PUSHLDLG (HOBJ, LDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_PUSHLDLG
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER LDLG

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_ESTPRC(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_PUSHLDLG = SELF%PUSHLDLG(LDLG)
      RETURN
      END

C************************************************************************
C Sommaire: LM_HELE_POPLDLG
C
C Description:
C     La fonction LM_HELE_POPLDLG restaure à l'objet la table poussée
C     sur la pile par LM_HELE_PUSHLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_POPLDLG (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_POPLDLG
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_ESTPRC(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_POPLDLG = SELF%POPLDLG()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQHLMGO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHLMGO
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_HELE_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQSELF(HOBJ)
      LM_HELE_REQHLMGO = SELF%HGEO
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQHASH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHASH
CDEC$ ENDIF

      USE LM_HELE_M
      USE SO_HASH_M, ONLY: SO_HASH_T
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'
      INCLUDE 'sohash_hobj.fi'

      INTEGER IERR
      INTEGER HASH
      CLASS(LM_ELEM_T), POINTER :: SELF
      TYPE (SO_HASH_T), POINTER :: OHASH
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      OHASH = SELF%REQHASH()

      IERR = SO_HASH_CTR(HASH)
      LM_HELE_REQHASH = SO_HASH_INIO(HASH, OHASH)
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C     La méthode LM_HELE_REQHDLIB retourne le handle au DTVNO qui contient
!!!C     les DDL.
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION LM_HELE_REQHDLIB(HOBJ)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHDLIB
!!!CDEC$ ENDIF
!!!
!!!      USE LM_HELE_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!
!!!      INCLUDE 'lmhele.fi'
!!!      INCLUDE 'hsdlib.fi'
!!!
!!!      CLASS(LM_ELEM_T), POINTER :: SELF
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      SELF => LM_HELE_REQOMNG(HOBJ)
!!!      LM_HELE_REQHDLIB = HS_DLIB_REQHVNOD(SELF%HDLIB)
!!!      RETURN
!!!      END
!!!
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQHNUMC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHNUMC
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_ESTPRC(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQHNUMC = SELF%REQHNUMC()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQHNUME(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQHNUME
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_ESTPRC(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQHNUME = SELF%REQHNUME()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HELE_REQTEMPS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HELE_REQTEMPS
CDEC$ ENDIF

      USE LM_HELE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmhele.fi'

      CLASS(LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HELE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HELE_REQOMNG(HOBJ)
      LM_HELE_REQTEMPS = SELF%REQTEMPS()
      RETURN
      END

