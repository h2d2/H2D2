C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C  Élément géométrique, interface HOBJ pour LM_GEOM_T.
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_HGEO
C         INTEGER LM_HGEO_000
C         INTEGER LM_HGEO_999
C         INTEGER LM_HGEO_CTR
C         INTEGER LM_HGEO_DTR
C         INTEGER LM_HGEO_INI
C         INTEGER LM_HGEO_RST
C         INTEGER LM_HGEO_REQHBASE
C         LOGICAL LM_HGEO_HVALIDE
C         INTEGER LM_HGEO_REQHGRID
C         INTEGER LM_HGEO_REQHLIMT
C         INTEGER LM_HGEO_REQHNUMC
C         INTEGER LM_HGEO_REQHNUME
C         INTEGER LM_HGEO_REQHCONF
C         INTEGER LM_HGEO_REQHDIST
C         INTEGER LM_HGEO_REQPRM
C         FTN Module: LM_HGEO_M
C            Public:
C               SUBROUTINE LM_HGEO_REQGDTA
C            Private:
C               SUBROUTINE LM_HGEO_REQOMNG
C         FTN Module: LM_HGEO_CALLCB_M
C            Public:
C               INTEGER LM_HGEO_CALLCB
C            Private:
C
C************************************************************************

      MODULE LM_HGEO_M

      USE LM_GEOM_M, ONLY: LM_GEOM_T
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: LM_HGEO_HBASE = 0

C---     Type pour encapsuler le pointeur
      TYPE, PUBLIC :: LM_HGEO_T
         CLASS(LM_GEOM_T), POINTER :: SELF
      END TYPE

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée LM_HGEO_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQSELF(HOBJ) RESULT(SELF)

      USE ISO_C_BINDING

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (LM_HGEO_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF)

      RETURN
      END FUNCTION LM_HGEO_REQSELF

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction protégée LM_HGEO_REQOMNG(...) retourne le pointeur à
C     la structure de données managée par l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQOMNG(HOBJ) RESULT(SELF)

      USE ISO_C_BINDING

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
      TYPE (LM_HGEO_T), POINTER :: SELF_H
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(ERR_GOOD())
C------------------------------------------------------------------------

      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL C_F_POINTER(CELF, SELF_H)
      SELF => SELF_H%SELF

      RETURN
      END FUNCTION LM_HGEO_REQOMNG

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La méthode privée LM_HGEO_REQGDTA(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQGDTA(HOBJ) RESULT(GDTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQGDTA
CDEC$ ENDIF

      INTEGER, INTENT(IN) :: HOBJ

      CLASS(LM_GEOM_T), POINTER :: SELF
      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      GDTA => SELF%GDTA
      RETURN
      END FUNCTION LM_HGEO_REQGDTA

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La classe est abstraite, HKID doit exister.
C************************************************************************
      INTEGER FUNCTION LM_HGEO_CTR(HOBJ, SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_CTR
CDEC$ ENDIF

      USE ISO_C_BINDING

      INTEGER, INTENT(OUT) :: HOBJ
      CLASS(LM_GEOM_T), INTENT(IN), TARGET :: SELF

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      TYPE(LM_HGEO_T), POINTER :: SELF_H
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Encapsule le pointeur polymorphique
      ALLOCATE(SELF_H)
      SELF_H%SELF => SELF

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   LM_HGEO_HBASE,
     &                                   C_LOC(SELF_H))
D     CALL ERR_ASR(ERR_BAD() .OR. LM_HGEO_HVALIDE(HOBJ))

      LM_HGEO_CTR = ERR_TYP()
      RETURN
      END FUNCTION LM_HGEO_CTR

      END MODULE LM_HGEO_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_000
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(LM_HGEO_HBASE, 'Geometrical Element')

      LM_HGEO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_999
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      EXTERNAL LM_HGEO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(LM_HGEO_HBASE, LM_HGEO_DTR)

      LM_HGEO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_DTR
CDEC$ ENDIF

      USE LM_HGEO_M
      USE LM_GEOM_M, ONLY: DEL
      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER  IERR
      CLASS(LM_HGEO_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      SELF => LM_HGEO_REQSELF(HOBJ)
      IERR = DEL(SELF%SELF)
      
C---     Efface du registre
      IERR = OB_OBJN_DTR(HOBJ, LM_HGEO_HBASE)
      HOBJ = 0

C---     Désalloue le mémoire
      DEALLOCATE(SELF)

      LM_HGEO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C     HGRID       Handle sur la maillage
C     HLIMT       Handle sur les limites
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_INI(HOBJ, HCONF, HGRID, HLIMT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_INI
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER HCONF
      INTEGER HGRID
      INTEGER HLIMT

      INCLUDE 'lmhgeo.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_INI = SELF%INI(HCONF, HGRID, HLIMT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_RST
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_RST = SELF%RST()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction LM_HGEO_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHBASE
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INCLUDE 'lmhgeo.fi'
C------------------------------------------------------------------------

      LM_HGEO_REQHBASE = LM_HGEO_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction LM_HGEO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_HVALIDE
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      LM_HGEO_HVALIDE = OB_OBJN_HVALIDE(HOBJ, LM_HGEO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur le maillage.
C
C Description:
C     La méthode LM_HGEO_REQHGRID(...) retourne le handle sur le maillage.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHGRID(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHGRID
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHGRID = SELF%REQHGRID()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur le maillage.
C
C Description:
C     La méthode LM_HGEO_REQHLIMT(...) retourne le handle sur le maillage.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHLIMT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHLIMT
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHLIMT = SELF%REQHLIMT()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la numérotation des noeuds.
C
C Description:
C     La méthode LM_HGEO_REQHNUMC(...) retourne le handle sur la
C     numérotation des noeuds.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHNUMC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHNUMC
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHNUMC = SELF%REQHNUMC()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle la numérotation des éléments
C
C Description:
C     La méthode LM_HGEO_REQHNUME(...) retourne le handle sur la
C        numérotation des éléments.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHNUME(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHNUME
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHNUME = SELF%REQHNUME()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la configuration GPU.
C
C Description:
C     La méthode LM_HGEO_REQHCONF(...) retourne le handle sur la
C     configuration GPU.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHCONF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHCONF
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHCONF = SELF%REQHCONF()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne le handle sur la distribution GPU des noeuds.
C
C Description:
C     La méthode LM_HGEO_REQHDIST(...) retourne le handle sur la
C     distribution GPU des noeuds.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQHDIST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQHDIST
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'err.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQHDIST = SELF%REQHDIST()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_HGEO_REQPRM(HOBJ, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_REQPRM
CDEC$ ENDIF

      USE LM_HGEO_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: IPRM

      INCLUDE 'lmhgeo.fi'

      CLASS(LM_GEOM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_HGEO_REQOMNG(HOBJ)
      LM_HGEO_REQPRM = SELF%REQPRM(IPRM)
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C     Fait un module à cause des paramètres OPTIONAL
!!!C************************************************************************
!!!      MODULE LM_HGEO_CALLCB_M
!!!      IMPLICIT NONE
!!!      CONTAINS
!!!
!!!      FUNCTION LM_HGEO_CALLCB(HOBJ, HFNC, B1, B2, B3, B4, B5)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: LM_HGEO_CALLCB
!!!CDEC$ ENDIF
!!!
!!!      USE LM_HGEO_M
!!!      USE SO_FUNC_M
!!!      USE SO_ALLC_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER LM_HGEO_CALLCB
!!!      INTEGER, INTENT(IN) :: HOBJ
!!!      INTEGER, INTENT(IN) :: HFNC
!!!      BYTE, INTENT(IN), OPTIONAL :: B1(:)
!!!      BYTE, INTENT(IN), OPTIONAL :: B2(:)
!!!      BYTE, INTENT(IN), OPTIONAL :: B3(:)
!!!      BYTE, INTENT(IN), OPTIONAL :: B4(:)
!!!      BYTE, INTENT(IN), OPTIONAL :: B5(:)
!!!
!!!      INCLUDE 'lmhgeo.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'sofunc.fi'
!!!
!!!      INTEGER IERR
!!!      BYTE, POINTER:: HOBJ_B(:)
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(LM_HGEO_HVALIDE(HOBJ))
!!!D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFNC))
!!!C-----------------------------------------------------------------------
!!!
!!!      HOBJ_B => SO_ALLC_CST2B(HOBJ)
!!!      IF (PRESENT(B5)) THEN
!!!         IERR = SO_FUNC_CALL6(HFNC, HOBJ_B, B1, B2, B3, B4, B5)
!!!      ELSEIF (PRESENT(B4)) THEN
!!!         IERR = SO_FUNC_CALL5(HFNC, HOBJ_B, B1, B2, B3, B4)
!!!      ELSEIF (PRESENT(B3)) THEN
!!!         IERR = SO_FUNC_CALL4(HFNC, HOBJ_B, B1, B2, B3)
!!!      ELSEIF (PRESENT(B2)) THEN
!!!         IERR = SO_FUNC_CALL3(HFNC, HOBJ_B, B1, B2)
!!!      ELSE IF (PRESENT(B1)) THEN
!!!         IERR = SO_FUNC_CALL2(HFNC, HOBJ_B, B1)
!!!      ELSE
!!!         IERR = SO_FUNC_CALL1(HFNC, HOBJ_B)
!!!      ENDIF
!!!
!!!      LM_HGEO_CALLCB = ERR_TYP()
!!!      RETURN
!!!      END FUNCTION LM_HGEO_CALLCB
!!!
!!!      END MODULE LM_HGEO_CALLCB_M
