C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Functions:
C   Public:
C     INTEGER HS_PRNO_000
C     INTEGER HS_PRNO_999
C     INTEGER HS_PRNO_CTR
C     INTEGER HS_PRNO_DTR
C     INTEGER HS_PRNO_INI
C     INTEGER HS_PRNO_RST
C     INTEGER HS_PRNO_REQHBASE
C     LOGICAL HS_PRNO_HVALIDE
C     INTEGER HS_PRNO_PASDEB
C     INTEGER HS_PRNO_PASFIN
C     INTEGER HS_PRNO_REQHLMAP
C     INTEGER HS_PRNO_REQHGRID
C     INTEGER HS_PRNO_REQHPRNO
C     INTEGER HS_PRNO_REQNNL
C     INTEGER HS_PRNO_REQNPRNO
C     INTEGER HS_PRNO_REQNPRNOL
C     INTEGER HS_PRNO_REQLPRNO
C   Private:
C
C************************************************************************

      MODULE HS_PRNO_M
      
      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_PRNO_T
         TYPE(SO_ALLC_VPTR2_T) :: PPRNO
         INTEGER, PRIVATE :: HPRNO
         INTEGER, PRIVATE :: NPRNO
         INTEGER, PRIVATE :: NPRNOL
      CONTAINS
         ! ---  Méthodes virtuelles    
         PROCEDURE, PUBLIC :: DTR      => HS_PRNO_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_PRNO_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_PRNO_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_PRNO_PASFIN

         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_PRNO_INI
         PROCEDURE, PUBLIC :: RST      => HS_PRNO_RST

         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQHPRNO => HS_PRNO_REQHPRNO
         PROCEDURE, PUBLIC :: REQNPRNO => HS_PRNO_REQNPRNO
         PROCEDURE, PUBLIC :: REQNPRNOL=> HS_PRNO_REQNPRNOL
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_PRNO_RAZ
      END TYPE HS_PRNO_T

      ! ---  Constructor
      PUBLIC :: HS_PRNO_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_PRNO_CTR
C
C Description:
C     Le constructeur <code>HS_PRNO_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_PRNO_CTR() RESULT(SELF)

      TYPE(HS_PRNO_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_PRNO_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_PRNO_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_DTR(SELF)
      
      CLASS(HS_PRNO_T), INTENT(INOUT), TARGET :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_PRNO_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_PRNO_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_PRNO_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HPRNO       Handle sur les propriétés nodales (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_INI(SELF, EDTA, OGRID, HPRNO)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HPRNO

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NNT, NPRNO, NPRNOL
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (HPRNO .NE. 0 .AND. .NOT. DT_PRNO_HVALIDE(HPRNO)) GOTO 9900

C---     Reset les données
      IERR = HS_PRNO_RST(SELF)

C---     Recherche les dimensions
      NNT   = OGRID%REQNNT()
      NPRNO = EDTA%NPRNO
      NPRNOL= EDTA%NPRNOL
      IF (NPRNO .LT. NPRNOL) GOTO 9902

C---     Contrôles
      NLCL = NNT
      IF (HPRNO .NE. 0) NLCL = DT_PRNO_REQNNT (HPRNO)
      IF (NLCL .NE. NNT)  GOTO 9903
      NLCL = 0
      IF (HPRNO .NE. 0) NLCL = DT_PRNO_REQNPRN(HPRNO)
      IF (NLCL .NE. NPRNOL) GOTO 9904

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID  => OGRID
         SELF%HPRNO  = HPRNO
         SELF%NPRNO  = NPRNO
         SELF%NPRNOL = NPRNOL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HPRNO
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_PRNO_INVALIDE', ': ',
     &   NPRNO, ' > ', NPRNOL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NBR_NOEUD_INVALIDE', ': ',
     &   NLCL, ' /', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_PRNO_INVALIDE', ': ',
     &   NLCL, ' /', NPRNOL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_PRNO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      
      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS  = -1.0D0
      SELF%OGRID  => NULL()
      SELF%HPRNO  = 0
      SELF%NPRNO  = 0
      SELF%NPRNOL = 0
      SELF%PPRNO  = SO_ALLC_VPTR2()

      HS_PRNO_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_RST(SELF)

      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_PRNO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_PRNO_PASDEB
C
C Description:
C     La méthode HS_PRNO_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      
      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER IOB
      INTEGER HGRID, HPRNO, HNUMR
      INTEGER L_DST
      INTEGER NNL, NPRN
      INTEGER, PARAMETER :: NPRN_MAX = 32
      LOGICAL DOGTHR(NPRN_MAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HPRNO = SELF%HPRNO

C---     Récupère les données
      NNL   = SELF%REQNNL   ()
      NPRN  = SELF%REQNPRNO ()
D     CALL ERR_ASR(NPRN_MAX .GE. NPRN)

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PPRNO = SO_ALLC_VPTR2(NPRN, NNL)

C---     Lis les propriétés nodales
      IF (HPRNO .NE. 0) THEN
         HNUMR = SELF%OGRID%REQHNUMC()
C---        Lis
         IF (ERR_GOOD()) THEN
            IERR = DT_PRNO_CHARGE(HPRNO, HNUMR, TNEW)
         ENDIF
C---        Gather
         IF (ERR_GOOD()) THEN
            DOGTHR(1:NPRN) = .TRUE.
            IERR = DT_PRNO_GATHER(HPRNO,
     &                            HNUMR,
     &                            NPRN,
     &                            NNL,
     &                            SELF%PPRNO%VPTR,
     &                            DOGTHR,
     &                            TGET)
         ENDIF
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_PRNO_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_PRNO_PASMAJ
C
C Description:
C     La méthode HS_PRNO_PASMAJ fait la mise à jour des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_PASMAJ(SELF)

      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PRNO_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_PRNO_PASMAJ

C************************************************************************
C Sommaire: HS_PRNO_PASFIN
C
C Description:
C     La méthode HS_PRNO_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TFIN        Nouveau temps de la simulation
C     MODIF(*)    .TRUE. si une colonne a été modifiée
C
C Sortie:
C
C Notes:
C     PRNO n'est pas garanti cohérent pour TFIN. Les données modifiées en fin
C     de pas de temps sont à TFIN alors eu celles lues sont à TNEW.
C     On choisit de ne pas modifier le temps, et donc tout reste à TNEW.
C     Si des données modifiées sont relues pour T == TNEW, elles ne seront
C     pas modifiées.
C************************************************************************
      INTEGER FUNCTION HS_PRNO_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_PRNO_T), INTENT(INOUT) :: SELF
      REAL*8,  INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HPRNO, HNUMR
      INTEGER NNL, NPRN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PRESENT(MODIF))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HPRNO = SELF%HPRNO
      NPRN  = SELF%NPRNO
D     CALL ERR_ASR(SELF%PPRNO%ISVALID())

C---     Récupère les données
      NNL   = SELF%REQNNL  ()

C---     Distribue les propriétés nodales
      IF (HPRNO .NE. 0) THEN
         HNUMR = SELF%OGRID%REQHNUMC()
C---        Scatter
         IF (ERR_GOOD()) THEN
            IERR = DT_PRNO_SCATTER(HPRNO,
     &                             HNUMR,
     &                             NPRN,
     &                             NNL,
     &                             SELF%PPRNO%VPTR,
     &                             MODIF,
     &                             TFIN)
         ENDIF
      ENDIF

      HS_PRNO_PASFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Valeurs nodales associées.
C
C Description:
C     La méthode HS_PRNO_REQHPRNO retourne le handle sur les propriétés
C     nodales.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_REQHPRNO(SELF)

      CLASS(HS_PRNO_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRNO_REQHPRNO = SELF%HPRNO
      RETURN
      END FUNCTION HS_PRNO_REQHPRNO

C************************************************************************
C Sommaire: Retourne le nombre de propriétés.
C
C Description:
C     La fonction HS_PRNO_REQNPRNO retourne le nombre de propriétés de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_REQNPRNO(SELF)

      CLASS(HS_PRNO_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRNO_REQNPRNO = SELF%NPRNO
      RETURN
      END FUNCTION HS_PRNO_REQNPRNO

C************************************************************************
C Sommaire: Retourne le nombre de propriétés lues.
C
C Description:
C     La fonction HS_PRNO_REQNPRNO retourne le nombre de propriétés lues de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_REQNPRNOL(SELF)

      CLASS(HS_PRNO_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRNO_REQNPRNOL = SELF%NPRNOL
      RETURN
      END FUNCTION HS_PRNO_REQNPRNOL

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PRNO_REQLPRNO retourne le pointeur à la table des
C     propriétés maintenues par l'objet. La table est de dimensions
C     (NPRN, NNL). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRNO_REQLPRNO(SELF)

      CLASS(HS_PRNO_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRNO_REQLPRNO = SELF%PPRNO%REQHNDL()
      RETURN
      END FUNCTION HS_PRNO_REQLPRNO

      END MODULE HS_PRNO_M
