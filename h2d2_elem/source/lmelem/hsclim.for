C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE HS_CLIM_M

      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T,
     &                     SO_ALLC_KPTR2_T,
     &                     SO_ALLC_VPTR1_T,
     &                     SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC

      TYPE, EXTENDS(HS_BASE_T) :: HS_CLIM_T
         TYPE(SO_ALLC_KPTR2_T) :: PCLCND
         TYPE(SO_ALLC_VPTR1_T) :: PCLCNV
         TYPE(SO_ALLC_KPTR2_T) :: PDIMP
         TYPE(SO_ALLC_VPTR2_T) :: PDIMV
         TYPE(SO_ALLC_KPTR1_T) :: PEIMP
         INTEGER, PRIVATE :: HCLIM
         INTEGER, PRIVATE :: NDLN
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR       => HS_CLIM_DTR
         PROCEDURE, PUBLIC :: PASDEB    => HS_CLIM_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ    => HS_CLIM_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN    => HS_CLIM_PASFIN
                                        
         ! ---  Méthodes publiques      
         PROCEDURE, PUBLIC :: INI       => HS_CLIM_INI
         PROCEDURE, PUBLIC :: RST       => HS_CLIM_RST

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHCLIM  => HS_CLIM_REQHCLIM
         PROCEDURE, PUBLIC :: REQNCLCND => HS_CLIM_REQNCLCND
         PROCEDURE, PUBLIC :: REQLCLCND => HS_CLIM_REQLCLCND
         PROCEDURE, PUBLIC :: REQNCLCNV => HS_CLIM_REQNCLCNV
         PROCEDURE, PUBLIC :: REQLCLCNV => HS_CLIM_REQLCLCNV
         PROCEDURE, PUBLIC :: REQNCLLIM => HS_CLIM_REQNCLLIM
         PROCEDURE, PUBLIC :: REQNCLNOD => HS_CLIM_REQNCLNOD
         PROCEDURE, PUBLIC :: REQNCLELE => HS_CLIM_REQNCLELE
         PROCEDURE, PUBLIC :: REQLCLLIM => HS_CLIM_REQLCLLIM
         PROCEDURE, PUBLIC :: REQLCLNOD => HS_CLIM_REQLCLNOD
         PROCEDURE, PUBLIC :: REQLCLELE => HS_CLIM_REQLCLELE
         PROCEDURE, PUBLIC :: REQLCLDST => HS_CLIM_REQLCLDST
         PROCEDURE, PUBLIC :: REQLDIMP  => HS_CLIM_REQLDIMP
         PROCEDURE, PUBLIC :: REQLDIMV  => HS_CLIM_REQLDIMV
         PROCEDURE, PUBLIC :: REQLEIMP  => HS_CLIM_REQLEIMP

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ      => HS_CLIM_RAZ
      END TYPE HS_CLIM_T

      ! ---  Constructor
      PUBLIC :: HS_CLIM_CTR

      CONTAINS

C************************************************************************
C Sommaire:  HS_CLIM_CTR
C
C Description:
C     Le constructeur <code>HS_CLIM_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_CLIM_CTR() RESULT(SELF)

      TYPE(HS_CLIM_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure
      IERR = SELF%RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_CLIM_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur SV2D_XCL_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_DTR(SELF)

      CLASS(HS_CLIM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
      CLASS(HS_CLIM_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_CLIM_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_INI(SELF, EDTA, OGRID, HCLIM)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER, INTENT(IN) :: HCLIM

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER NDLN
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. DT_CLIM_HVALIDE(HCLIM)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NDLN = EDTA%NDLN
      IF (NDLN .LE. 0) GOTO 9902

C---     Assigne les valeurs
      IERR = SELF%RAZ()
      IF (ERR_GOOD()) THEN
         SELF%OGRID => OGRID
         SELF%TEMPS = -1.0D0
         SELF%HCLIM = HCLIM
         SELF%NDLN  = NDLN
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HCLIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NDLN_INVALIDE', ': ',
     &   NDLN, ' > ', 0
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_CLIM_INI = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_KPTR2,
     &                     SO_ALLC_VPTR2

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les paramètres
      SELF%OGRID => NULL()
      SELF%PDIMP = SO_ALLC_KPTR2()
      SELF%PDIMV = SO_ALLC_VPTR2()
      SELF%PEIMP = SO_ALLC_KPTR1()
      SELF%TEMPS = -1.0D0
      SELF%HCLIM = 0
      SELF%NDLN  = 0

      HS_CLIM_RAZ = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_RAZ

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      INTEGER FUNCTION HS_CLIM_RST(SELF)

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_CLIM_RST = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_RST

C************************************************************************
C Sommaire: HS_CLIM_PASDEB
C
C Description:
C     La méthode HS_CLIM_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_KPTR2,
     &                     SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HCLIM
      INTEGER HNUMR
      INTEGER NDLN, NELS, NNL
      INTEGER NCLCND, NCLCNV
      INTEGER LCLCND, LCLCNV
      CLASS(GR_GRID_T), POINTER :: OGRID
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les données de l'objet
      OGRID => SELF%OGRID
      HCLIM  = SELF%HCLIM
      NDLN   = SELF%NDLN

C---     Récupère les données du maillage
      NNL  = OGRID%REQNNL  ()
      NELS = OGRID%REQNELLS()
D     CALL ERR_ASR( NELS .GT. 0)

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PDIMP = SO_ALLC_KPTR2(NDLN, NNL)
      IF (ERR_GOOD()) SELF%PDIMV = SO_ALLC_VPTR2(NDLN, NNL)
      IF (ERR_GOOD()) SELF%PEIMP = SO_ALLC_KPTR1(NELS)

C---     Lis les données
      IF (ERR_GOOD()) THEN
         HNUMR = OGRID%REQHNUMC()
         IERR = DT_CLIM_CHARGE(HCLIM, HNUMR, TNEW)
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) THEN
         NCLCND = DT_CLIM_REQNCLCND(HCLIM)
         NCLCNV = DT_CLIM_REQNCLCNV(HCLIM)
         LCLCND = DT_CLIM_REQLCLCND(HCLIM)
         LCLCNV = DT_CLIM_REQLCLCNV(HCLIM)
         SELF%TEMPS = TNEW
         SELF%PCLCND = SO_ALLC_KPTR2(LCLCND, 4, NCLCND)
         SELF%PCLCNV = SO_ALLC_VPTR1(LCLCND, NCLCNV)
      ENDIF

      HS_CLIM_PASDEB = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_PASDEB

C************************************************************************
C Sommaire: HS_CLIM_PASMAJ
C
C Description:
C     La méthode HS_CLIM_PASMAJ fait la mise à jour des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_PASMAJ(SELF)

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_CLIM_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_PASMAJ

C************************************************************************
C Sommaire: HS_CLIM_PASFIN
C
C Description:
C     La méthode HS_CLIM_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_CLIM_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_CLIM_PASFIN = ERR_TYP()
      RETURN
      END FUNCTION HS_CLIM_PASFIN

C************************************************************************
C Sommaire: Conditions limites associées.
C
C Description:
C     La méthode HS_CLIM_REQHCLIM retourne le handle sur les conditions
C     limites.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQHCLIM(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_CLIM_REQHCLIM = SELF%HCLIM
      RETURN
      END FUNCTION HS_CLIM_REQHCLIM

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQNCLCND(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQNCLCND = DT_CLIM_REQNCLCND(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQNCLCND

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLCND(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLCND = DT_CLIM_REQLCLCND(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLCND

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQNCLCNV(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQNCLCNV = DT_CLIM_REQNCLCNV(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQNCLCNV

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLCNV(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C-----------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLCNV = DT_CLIM_REQLCLCNV(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLCNV

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQNCLLIM(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQNCLLIM = DT_CLIM_REQNCLLIM(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQNCLLIM

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQNCLNOD(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQNCLNOD = DT_CLIM_REQNCLNOD(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQNCLNOD

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQNCLELE(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQNCLELE = DT_CLIM_REQNCLELE(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQNCLELE

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLLIM(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLLIM = DT_CLIM_REQLCLLIM(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLLIM

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLNOD(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLNOD = DT_CLIM_REQLCLNOD(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLNOD

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLELE(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLELE = DT_CLIM_REQLCLELE(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLELE

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLCLDST(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF

      INCLUDE 'dtclim.fi'

      INTEGER HCLIM
C------------------------------------------------------------------------

      HCLIM = SELF%HCLIM
      HS_CLIM_REQLCLDST = DT_CLIM_REQLCLDST(HCLIM)
      RETURN
      END FUNCTION HS_CLIM_REQLCLDST

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_CLIM_REQLDIMP retourne le pointeur à la table des
C     noeuds imposés. La table est de dimensions (NDLN, NNL).
C     Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLDIMP(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_CLIM_REQLDIMP = SELF%PDIMP%REQHNDL()
      RETURN
      END FUNCTION HS_CLIM_REQLDIMP

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_CLIM_REQLDIMV retourne le pointeur à la table des
C     valeurs imposés. La table est de dimensions (NDLN, NNL).
C     Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLDIMV(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_CLIM_REQLDIMV = SELF%PDIMV%REQHNDL()
      RETURN
      END FUNCTION HS_CLIM_REQLDIMV

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_CLIM_REQLEIMP retourne le pointeur à la table des
C     type de CL des éléments imposés. La table est de dimensions (NELS).
C     Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_CLIM_REQLEIMP(SELF)

      CLASS(HS_CLIM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      HS_CLIM_REQLEIMP = SELF%PEIMP%REQHNDL()
      RETURN
      END FUNCTION HS_CLIM_REQLEIMP

      END MODULE HS_CLIM_M
