C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_ELEM
C         FTN (Sub)Module: LM_ELEM_PASDEB_M
C   Public:
C               MODULE INTEGER LM_ELEM_PASDEB
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_PASDEB_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: Lecture.
C
C Description:
C     La sous-routine LM_ELEM_PASDEB fait toute la lecture des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_PASDEB(SELF, TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PASDEB
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
      REAL*8, INTENT(IN) :: TSIM

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'

      INTEGER IERR
!$    INTEGER I_COMM, I_SIZE, I_ERROR
!$    INTEGER NB_OMP
!$    LOGICAL DO_OMP
!$    INTEGER omp_get_max_threads
!$    LOGICAL omp_get_nested
C-----------------------------------------------------------------------

      IERR = ERR_OK

!$    I_COMM = MP_UTIL_REQCOMM()
!$    CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
!$    DO_OMP = (omp_get_max_threads() .ge. 3)
!$    DO_OMP = DO_OMP .and. omp_get_nested()
!$    DO_OMP = DO_OMP .and. I_SIZE .LE. 1
!$    NB_OMP = omp_get_max_threads() / 2
!$    NB_OMP = min(NB_OMP, 3)
!$    NB_OMP = max(NB_OMP, 1)

!$omp parallel
!$omp& num_threads(NB_OMP) if(DO_OMP)
!$omp& default(shared)
!$omp& private(IERR)

!$omp sections

!$omp section
C---     Charge le maillage
      IF (ERR_GOOD()) IERR = SELF%OGEO%PASDEB(TSIM)

C---     Initialise les DDL
      IF (ERR_GOOD()) IERR = SELF%ODLIB%PASDEB(TSIM)

C---     Charge les données
!$omp section
      IF (ERR_GOOD()) IERR = SELF%OCLIM%PASDEB(TSIM)
C---     Assemble les éléments de surface des C.L.
!TODO!      IF (ERR_GOOD()) IERR = OLMAP%ASMESCL()

      IF (ERR_GOOD()) IERR = SELF%OSOLC%PASDEB(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OSOLR%PASDEB(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OPRGL%PASDEB(TSIM)
!$omp section
      IF (ERR_GOOD()) IERR = SELF%OPRNO%PASDEB(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OPREV%PASDEB(TSIM)
      IF (ERR_GOOD()) IERR = SELF%OPRES%PASDEB(TSIM)
!$omp end sections
      IERR = ERR_OMP_RDC()

!$omp sections

C---     Traitement post-lecture des données
!$omp section
      IF (ERR_GOOD()) IERR = SELF%PSLPRGL()
      IF (ERR_GOOD()) IERR = SELF%PSLPRNO()
!$omp section
      IF (ERR_GOOD()) IERR = SELF%PSLPREV()
      IF (ERR_GOOD()) IERR = SELF%PSLSOLC()
!$omp section
      IF (ERR_GOOD()) IERR = SELF%PSLSOLR()
      IF (ERR_GOOD()) IERR = SELF%PSLCLIM()

!$omp section
C---     Traitement post-lecture des DDL
      IF (ERR_GOOD()) IERR = SELF%PSLDLIB()

!$omp end sections
      IERR = ERR_OMP_RDC()
!$omp end parallel
      
C---     Temps de la simulatoon
      IF (ERR_GOOD()) THEN
         SELF%EDTA%TSIM = TSIM
      ENDIF

C---     Status
      IF (ERR_GOOD()) THEN
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_LIS)
      ENDIF

      LM_ELEM_PASDEB = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_PASDEB

      END SUBMODULE LM_ELEM_PASDEB_M

