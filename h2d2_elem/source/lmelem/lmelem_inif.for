C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER LM_ELEM_INIFIC
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_INIFIC(HOBJ, TSIM, FILNAM, ISTAT)

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER       HOBJ
      REAL*8        TSIM
      CHARACTER*(*) FILNAM
      INTEGER       ISTAT

      INCLUDE 'lmelem.fi'
      INCLUDE 'hsdlib.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (LM_ELEM_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Initialise
      SELF => LM_ELEM_REQSELF(HOBJ)
      IERR = HS_DLIB_INIFIC(SELF%HDLIB, TSIM, FILNAM, ISTAT)

C---     Met à jour l'état
      SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_INI)

C---     Lis les données
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HOBJ, TSIM)

      LM_ELEM_INIFIC = ERR_TYP()
      RETURN
      END
