C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMKU
C   Private:
C     INTEGER LM_ELEM_ASMKU_CB
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble {F}
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     VRES     F
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_ASMKU (HOBJ, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMKU
CDEC$ ENDIF

      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VRES(*)

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gpclcl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'lmelem.fc'

      INTEGER IERR
      INTEGER HCLC
      INTEGER*8 XARGS(2)
      REAL*8    VARGS(1)
      TYPE (LM_EDTA_T), POINTER :: EDTA

      EXTERNAL LM_ELEM_ASMKU_CB
      INTEGER LM_ELEM_ASMKU_CB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.residu')

C---     Transfert les valeurs
      EDTA => LM_ELEM_REQEDTA(HOBJ)

C---     Construis le calculateur
      HCLC = 0
      IERR = LM_ELEM_CFGGPU(HOBJ, HCLC)

C---     L'appel
      XARGS(1) = HOBJ
      XARGS(2) = SO_ALLC_REQVHND(VRES)
      IERR = GP_CLCL_XEQ(HCLC, LM_ELEM_ASMKU_CB, XARGS, VARGS)

C---     Stope le chrono
      CALL TR_CHRN_STOP('h2d2.reso.residu')

      LM_ELEM_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble {F}
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C     VRES     F
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_ASMKU_CB(XARGS, VARGS)

      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER   LM_ELEM_ASMKU_CB
      INTEGER*8 XARGS(*)
      REAL*8    VARGS(*)

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'lmelem.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HVTBL, HMETH
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      HOBJ = XARGS(1)

C---     La méthode
      HVTBL = 0
      HMETH = 0
      IF (ERR_GOOD()) IERR = OB_VTBL_REQVTBL(HOBJ, HVTBL)
      IF (ERR_GOOD()) IERR = SO_VTBL_REQMTH (HVTBL,
     &                                       LM_VTBL_FNC_ASMKU,
     &                                       HMETH)

C---     Fait l'appel de méthode
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL1 (HMETH,
     &                         SO_ALLC_CST2B(VA(XARGS(2)))))

      LM_ELEM_ASMKU_CB = ERR_TYP()
      RETURN
      END
