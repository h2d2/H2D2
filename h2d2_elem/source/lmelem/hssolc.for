C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER HS_SOLC_000
C     INTEGER HS_SOLC_999
C     INTEGER HS_SOLC_CTR
C     INTEGER HS_SOLC_DTR
C     INTEGER HS_SOLC_INI
C     INTEGER HS_SOLC_RST
C     INTEGER HS_SOLC_REQHBASE
C     LOGICAL HS_SOLC_HVALIDE
C     INTEGER HS_SOLC_PASDEB
C     INTEGER HS_SOLC_PASFIN
C     INTEGER HS_SOLC_REQHLMAP
C     INTEGER HS_SOLC_REQHGRID
C     INTEGER HS_SOLC_REQHSOLC
C     INTEGER HS_SOLC_REQNNL
C     INTEGER HS_SOLC_REQNSOLC
C     INTEGER HS_SOLC_REQNSOLCL
C     INTEGER HS_SOLC_REQLSOLC
C   Private:
C
C************************************************************************

      MODULE HS_SOLC_M
      
      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_SOLC_T
         TYPE(SO_ALLC_VPTR2_T) :: PSOLC
         INTEGER, PRIVATE :: HSOLC
         INTEGER, PRIVATE :: NSOLC
         INTEGER, PRIVATE :: NSOLCL
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => HS_SOLC_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_SOLC_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_SOLC_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_SOLC_PASFIN

         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_SOLC_INI
         PROCEDURE, PUBLIC :: RST      => HS_SOLC_RST
               
         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQHSOLC => HS_SOLC_REQHSOLC
         PROCEDURE, PUBLIC :: REQNSOLC => HS_SOLC_REQNSOLC
         PROCEDURE, PUBLIC :: REQNSOLCL=> HS_SOLC_REQNSOLCL
         PROCEDURE, PUBLIC :: REQLSOLC => HS_SOLC_REQLSOLC
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_SOLC_RAZ
      END TYPE HS_SOLC_T

      ! ---  Constructor
      PUBLIC :: HS_SOLC_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_SOLC_CTR
C
C Description:
C     Le constructeur <code>HS_SOLC_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_SOLC_CTR() RESULT(SELF)

      TYPE(HS_SOLC_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_SOLC_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_SOLC_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_DTR(SELF)
      
      CLASS(HS_SOLC_T), INTENT(INOUT), TARGET :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_SOLC_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_SOLC_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_SOLC_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HSOLC       Handle sur les sollicitations concentrées (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_INI(SELF, EDTA, OGRID, HSOLC)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HSOLC

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER NNT, NSOLC, NSOLCL
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (HSOLC .NE. 0 .AND. .NOT. DT_PRNO_HVALIDE(HSOLC)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NNT   = OGRID%REQNNT()
      NSOLC = EDTA%NSOLC
      NSOLCL= EDTA%NSOLCL
      IF (NSOLC .LT. NSOLCL) GOTO 9902

C---     Contrôles
      NLCL = NNT
      IF (HSOLC .NE. 0) NLCL = DT_PRNO_REQNNT  (HSOLC)
      IF (NLCL .NE. NNT)  GOTO 9903
      NLCL = NSOLCL
      IF (HSOLC .NE. 0) NLCL = DT_PRNO_REQNPRN(HSOLC)
      IF (NLCL .NE. NSOLCL) GOTO 9904

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID  => OGRID
         SELF%HSOLC  = HSOLC
         SELF%NSOLC  = NSOLC
         SELF%NSOLCL = NSOLCL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HSOLC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_SOLC_INVALIDE', ': ',
     &   NSOLC, ' > ', NSOLCL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NBR_NOEUD_INVALIDE', ': ',
     &   NLCL, ' /', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_SOLC_INVALIDE', ': ',
     &   NLCL, ' /', NSOLCL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_SOLC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS = -1.0D0
      SELF%OGRID => NULL()
      SELF%HSOLC = 0
      SELF%NSOLC = 0
      SELF%NSOLC = 0
      SELF%PSOLC = SO_ALLC_VPTR2()

      HS_SOLC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_RST(SELF)

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER :: IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_SOLC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_SOLC_PASDEB
C
C Description:
C     La méthode HS_SOLC_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER HSOLC, HNUMR
      INTEGER NNL, NSOLC
      INTEGER, PARAMETER :: NSOLC_MAX = 8
      LOGICAL DOGTHR(NSOLC_MAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HSOLC = SELF%HSOLC
      NSOLC = SELF%NSOLC

C---     Récupère les données
      NNL   = SELF%REQNNL  ()

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PSOLC = SO_ALLC_VPTR2(NSOLC, NNL)

C---     Lis les propriétés nodales
      IF (HSOLC .NE. 0) THEN
         HNUMR = SELF%OGRID%REQHNUMC()
C---        Lis
         IF (ERR_GOOD()) THEN
            IERR = DT_PRNO_CHARGE(HSOLC, HNUMR, TNEW)
         ENDIF
C---        Gather
         IF (ERR_GOOD()) THEN
            DOGTHR(1:NSOLC) = .TRUE.
            IERR = DT_PRNO_GATHER(HSOLC,
     &                            HNUMR,
     &                            NSOLC,
     &                            NNL,
     &                            SELF%PSOLC%VPTR,
     &                            DOGTHR,
     &                            TGET)
         ENDIF
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_SOLC_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_SOLC_PASMAJ
C
C Description:
C     La méthode HS_SOLC_PASMAJ fait la mise à jour des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_PASMAJ(SELF)

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_SOLC_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_SOLC_PASMAJ

C************************************************************************
C Sommaire: HS_SOLC_PASFIN
C
C Description:
C     La méthode HS_SOLC_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C   Pas de modification des SOLC possible
C************************************************************************
      INTEGER FUNCTION HS_SOLC_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_SOLC_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_SOLC_PASFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Sollicitations réparties associées.
C
C Description:
C     La méthode HS_SOLC_REQHSOLC retourne le handle sur les sollicitations
C     concentrées.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_REQHSOLC(SELF)

      CLASS(HS_SOLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLC_REQHSOLC = SELF%HSOLC
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sollicitations.
C
C Description:
C     La fonction HS_SOLC_REQNSOLC retourne le nombre de sollicitations de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_REQNSOLC(SELF)

      CLASS(HS_SOLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLC_REQNSOLC = SELF%NSOLC
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sollicitations lues.
C
C Description:
C     La fonction HS_SOLC_REQNSOLCL retourne le nombre de sollicitations lues de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_REQNSOLCL(SELF)

      CLASS(HS_SOLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLC_REQNSOLCL = SELF%NSOLCL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_SOLC_REQLSOLC retourne le pointeur à la table des
C     sollicitations maintenues par l'objet. La table est de dimensions
C     (NDLN, NNL). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLC_REQLSOLC(SELF)

      CLASS(HS_SOLC_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLC_REQLSOLC = SELF%PSOLC%REQHNDL()
      RETURN
      END

      END MODULE HS_SOLC_M
