C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMM
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMM_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Assemble la matrice [K]
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMM(SELF, HMTRX, F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMM
CDEC$ ENDIF

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: HMTRX
      INTEGER F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     La méthode
      IERR = SELF%CLCK(HMTRX, F_ASM)

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')

      LM_ELEM_ASMM = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASMM

      END SUBMODULE LM_ELEM_ASMM_M
