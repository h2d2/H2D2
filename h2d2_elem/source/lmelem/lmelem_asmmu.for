C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMMU
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMMU_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Assemble le produit [M].{U}
C
C Description:
C     La fonction LM_ELEM_ASMMU assemble le produit [M].{U} dans
C     le tableau VRES. Celui-ci est initialisé à zéro.
C
C Entrée:
C     SELF     L'objet
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMMU(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMMU
CDEC$ ENDIF

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'

      INTEGER IERR
      INTEGER NEQL
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.residu')

C---     Assemble
      NEQL = SELF%EDTA%NEQL
      VRES(1:NEQL) = ZERO
      IF (ERR_GOOD()) IERR = SELF%CLCMU(VRES(1:NEQL))

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.reso.residu')

      LM_ELEM_ASMMU = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASMMU

      END SUBMODULE LM_ELEM_ASMMU_M
