C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMDIM
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMDIM_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMDIM(SELF, ILU, IA, KLD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMDIM
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T, SO_ALLC_KPTR2

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: ILU
      INTEGER, INTENT(OUT) :: IA (:)
      INTEGER, INTENT(OUT) :: KLD(:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER HAPP, HSIMD
      TYPE(SO_ALLC_KPTR2_T) :: PLOCE
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Dimensionne KLOCE avec le nombre de noeuds géométrique
C        parce qu’on boucle sur tous les noeuds
      IF (ERR_GOOD()) PLOCE = SO_ALLC_KPTR2(EDTA%NDLN, GDTA%NNELV)

      IF (ERR_GOOD()) THEN
         IF (ILU .EQ. 0) THEN
            IERR=SP_ELEM_DIMILU0(GDTA%NNL,
     &                           EDTA%NDLN,
     &                           GDTA%NNELV,
     &                           GDTA%NCELV,
     &                           GDTA%NELLV,
     &                           EDTA%NEQL,
     &                           EDTA%KLOCN,
     &                           PLOCE%KPTR,
     &                           GDTA%KNGV,
     &                           IA)
         ELSE
            IERR=SP_ELEM_DIMILUN(GDTA%NNL,
     &                           EDTA%NDLN,
     &                           GDTA%NNELV,
     &                           GDTA%NCELV,
     &                           GDTA%NELLV,
     &                           EDTA%NEQL,
     &                           EDTA%KLOCN,
     &                           PLOCE%KPTR,
     &                           GDTA%KNGV,
     &                           IA,
     &                           KLD)
         ENDIF
      ENDIF
      PLOCE = SO_ALLC_KPTR2()

      LM_ELEM_ASMDIM = ERR_TYP()
      RETURN
      END

      END SUBMODULE LM_ELEM_ASMDIM_M
      