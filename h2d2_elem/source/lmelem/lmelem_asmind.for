C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMIND
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMIND_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMIND(SELF, ILU, IA, JA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMIND
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T, SO_ALLC_KPTR2

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN)  :: ILU
      INTEGER, INTENT(IN)  :: IA (:)
      INTEGER, INTENT(OUT) :: JA (:)

      INCLUDE 'err.fi'
      INCLUDE 'spelem.fi'

      INTEGER IERR
      INTEGER HAPP, HSIMD
      TYPE(SO_ALLC_KPTR2_T) :: PLOCE
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA
      EDTA => SELF%EDTA

C---     Dimensionne KLOCE avec le nombre de noeuds géométrique
C        parce qu'on boucle sur tous les noeuds
      IF (ERR_GOOD()) PLOCE = SO_ALLC_KPTR2(EDTA%NDLN, GDTA%NNELV)

      IF (ERR_GOOD()) THEN
         IF (ILU .EQ. 0) THEN
            IERR=SP_ELEM_INDILU0(PLOCE%KPTR,
     &                           EDTA%NDLN,
     &                           GDTA%NNL,
     &                           EDTA%KLOCN,
     &                           GDTA%NNELV,
     &                           GDTA%NCELV,
     &                           GDTA%NELLV,
     &                           GDTA%KNGV,
     &                           IA,
     &                           JA)
         ELSE
            IERR=SP_ELEM_INDILUN(PLOCE%KPTR,
     &                           EDTA%NDLN,
     &                           GDTA%NNL,
     &                           EDTA%KLOCN,
     &                           GDTA%NNELV,
     &                           GDTA%NCELV,
     &                           GDTA%NELLV,
     &                           GDTA%KNGV,
     &                           IA,
     &                           JA)
         ENDIF
      ENDIF

      PLOCE = SO_ALLC_KPTR2()

      LM_ELEM_ASMIND = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASMIND

      END SUBMODULE LM_ELEM_ASMIND_M
      