C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER HS_PRES_000
C     INTEGER HS_PRES_999
C     INTEGER HS_PRES_CTR
C     INTEGER HS_PRES_DTR
C     INTEGER HS_PRES_INI
C     INTEGER HS_PRES_RST
C     INTEGER HS_PRES_REQHBASE
C     LOGICAL HS_PRES_HVALIDE
C     INTEGER HS_PRES_PASDEB
C     INTEGER HS_PRES_PASFIN
C     INTEGER HS_PRES_REQHLMAP
C     INTEGER HS_PRES_REQHGRID
C     INTEGER HS_PRES_REQNELE
C     INTEGER HS_PRES_REQNPREL
C     INTEGER HS_PRES_REQNPRELL
C     INTEGER HS_PRES_REQLPREL
C   Private:
C
C************************************************************************

      MODULE HS_PRES_M
      
      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_PRES_T
         TYPE(SO_ALLC_VPTR3_T) :: PPRES
         INTEGER, PRIVATE :: NPRESL
         INTEGER, PRIVATE :: NPRES
         INTEGER, PRIVATE :: NPRES_D1
         INTEGER, PRIVATE :: NPRES_D2
      CONTAINS
         ! ---  Méthodes virtuelles    
         PROCEDURE, PUBLIC :: DTR      => HS_PRES_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_PRES_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_PRES_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_PRES_PASFIN
      
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_PRES_INI
         PROCEDURE, PUBLIC :: RST      => HS_PRES_RST

         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQNELE  => HS_PRES_REQNELE
         PROCEDURE, PUBLIC :: REQNPREL => HS_PRES_REQNPREL
         PROCEDURE, PUBLIC :: REQNPRELL=> HS_PRES_REQNPRELL
         PROCEDURE, PUBLIC :: REQLPREL => HS_PRES_REQLPREL
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_PRES_RAZ
      END TYPE HS_PRES_T

      ! ---  Constructor
      PUBLIC :: HS_PRES_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_PRES_CTR
C
C Description:
C     Le constructeur <code>HS_PRES_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_PRES_CTR() RESULT(SELF)

      TYPE(HS_PRES_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_PRES_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_PRES_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_DTR(SELF)
      
      CLASS(HS_PRES_T), INTENT(INOUT), TARGET :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_PRES_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_PRES_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HSOLR       Handle sur les sollicitations réparties (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_INI(SELF, EDTA, OGRID)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NELET, NPRESL, NPRES, NPRES_D1, NPRES_D2
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NELET  = OGRID%REQNELTV()
      NPRESL = 0
      NPRES  = EDTA%NPRES
      NPRES_D1= EDTA%NPRES_D1
      NPRES_D2= EDTA%NPRES_D2
      IF (NPRES .LT. NPRESL) GOTO 9902

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID  => OGRID
         SELF%NPRESL = NPRESL
         SELF%NPRES  = NPRES
         SELF%NPRES_D1= NPRES_D1
         SELF%NPRES_D2= NPRES_D2
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_PRES_INVALIDE', ': ',
     &   NPRES, ' > ', NPRESL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_PRES_INI = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3
      
      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS  = -1.0D0
      SELF%OGRID  => NULL()
      SELF%NPRESL = 0
      SELF%NPRES  = 0
      SELF%NPRES_D1 = 0
      SELF%NPRES_D2 = 0
      SELF%PPRES  = SO_ALLC_VPTR3()

      HS_PRES_RAZ = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_RAZ

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      INTEGER FUNCTION HS_PRES_RST(SELF)

      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_PRES_RST = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_RST

C************************************************************************
C Sommaire: HS_PRES_PASDEB
C
C Description:
C     La méthode HS_PRES_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3
      
      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER ISIZ
      INTEGER NPRES, NPRES_D1, NPRES_D2, NELLS
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      NPRES   = SELF%NPRES
      NPRES_D1= SELF%NPRES_D1
      NPRES_D2= SELF%NPRES_D2
      NELLS = SELF%REQNELE ()

C---     Alloue l'espace
      ISIZ = MAX(1, NELLS)  ! Min de 1 pour garantir l’existence
      SELF%PPRES = SO_ALLC_VPTR3(NPRES_D1, NPRES_D2, ISIZ)

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_PRES_PASDEB = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_PASDEB

C************************************************************************
C Sommaire: HS_PRES_PASMAJ
C
C Description:
C     La méthode HS_PRES_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_PASMAJ(SELF)

      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PRES_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_PASMAJ

C************************************************************************
C Sommaire: HS_PRES_PASFIN
C
C Description:
C     La méthode HS_PRES_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_PRES_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PRES_PASFIN = ERR_TYP()
      RETURN
      END FUNCTION HS_PRES_PASFIN

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_REQNELE(SELF)

      CLASS(HS_PRES_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRES_REQNELE = SELF%OGRID%REQNELLS()
      RETURN
      END FUNCTION HS_PRES_REQNELE

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PRES_REQNPREL retourne le nombre de propriétés de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_REQNPREL(SELF)

      CLASS(HS_PRES_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRES_REQNPREL = SELF%NPRES
      RETURN
      END FUNCTION HS_PRES_REQNPREL

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PRES_REQNPRELL retourne le nombre de propriétés lues de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_REQNPRELL(SELF)

      CLASS(HS_PRES_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRES_REQNPRELL = SELF%NPRESL
      RETURN
      END FUNCTION HS_PRES_REQNPRELL

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PRES_REQLPREL retourne le pointeur à la table des
C     propriétés maintenues par l'objet. La table est de dimensions
C     (NPRES, NELS) ou (NPRES_D1, NPRES_D2, NELS).
C     Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_REQLPREL(SELF)

      CLASS(HS_PRES_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRES_REQLPREL = SELF%PPRES%REQHNDL()
      RETURN
      END FUNCTION HS_PRES_REQLPREL

C************************************************************************
C Sommaire: Temps.
C
C Description:
C     La méthode HS_PRES_REQTEMPS retourne le temps des données.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRES_REQTEMPS(SELF)

      CLASS(HS_PRES_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRES_REQTEMPS = SELF%TEMPS
      RETURN
      END FUNCTION HS_PRES_REQTEMPS

      END MODULE HS_PRES_M
