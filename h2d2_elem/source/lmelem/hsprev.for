C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER HS_PREV_000
C     INTEGER HS_PREV_999
C     INTEGER HS_PREV_CTR
C     INTEGER HS_PREV_DTR
C     INTEGER HS_PREV_INI
C     INTEGER HS_PREV_RST
C     INTEGER HS_PREV_REQHBASE
C     LOGICAL HS_PREV_HVALIDE
C     INTEGER HS_PREV_PASDEB
C     INTEGER HS_PREV_PASFIN
C     INTEGER HS_PREV_REQHLMAP
C     INTEGER HS_PREV_REQHGRID
C     INTEGER HS_PREV_REQHPREV
C     INTEGER HS_PREV_REQNELE
C     INTEGER HS_PREV_REQNPREL
C     INTEGER HS_PREV_REQNPRELL
C     INTEGER HS_PREV_REQLPREL
C   Private:
C
C************************************************************************

      MODULE HS_PREV_M

      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_PREV_T
         TYPE(SO_ALLC_VPTR3_T) :: PPREV
         INTEGER, PRIVATE :: HPREV
         INTEGER, PRIVATE :: NPREVL
         INTEGER, PRIVATE :: NPREV
         INTEGER, PRIVATE :: NPREV_D1
         INTEGER, PRIVATE :: NPREV_D2
      CONTAINS
         ! ---  Méthodes virtuelles    
         PROCEDURE, PUBLIC :: DTR      => HS_PREV_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_PREV_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_PREV_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_PREV_PASFIN
      
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_PREV_INI
         PROCEDURE, PUBLIC :: RST      => HS_PREV_RST

         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQHPREV => HS_PREV_REQHPREV
         PROCEDURE, PUBLIC :: REQNELE  => HS_PREV_REQNELE
         PROCEDURE, PUBLIC :: REQNPREL => HS_PREV_REQNPREL
         PROCEDURE, PUBLIC :: REQNPRELL=> HS_PREV_REQNPRELL
         PROCEDURE, PUBLIC :: REQLPREL => HS_PREV_REQLPREL
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_PREV_RAZ
      END TYPE HS_PREV_T

      ! ---  Constructor
      PUBLIC :: HS_PREV_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_PREV_CTR
C
C Description:
C     Le constructeur <code>HS_PREV_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_PREV_CTR() RESULT(SELF)

      TYPE(HS_PREV_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_PREV_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_PREV_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_DTR(SELF)

      CLASS(HS_PREV_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_PREV_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_PREV_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HSOLR       Handle sur les sollicitations réparties (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_INI(SELF, EDTA, OGRID, HPREV)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HPREV

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NELET, NPREVL, NPREV, NPREV_D1, NPREV_D2
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (HPREV .NE. 0 .AND. .NOT. DT_PRNO_HVALIDE(HPREV)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NELET   = OGRID%REQNELTV()
      NPREVL  = EDTA%NPREVL
      NPREV   = EDTA%NPREV
      NPREV_D1= EDTA%NPREV_D1
      NPREV_D2= EDTA%NPREV_D2
      IF (NPREV .LT. NPREVL) GOTO 9902

C---     Contrôles
      NLCL = NELET
      IF (HPREV .NE. 0) NLCL = DT_PRNO_REQNNL (HPREV)
      IF (NLCL .NE. NELET)  GOTO 9903
      NLCL = 0
      IF (HPREV .NE. 0) NLCL = DT_PRNO_REQNPRN(HPREV)
      IF (NLCL .NE. NPREVL) GOTO 9904

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID   => OGRID
         SELF%HPREV   = HPREV
         SELF%NPREVL  = NPREVL
         SELF%NPREV   = NPREV
         SELF%NPREV_D1= NPREV_D1
         SELF%NPREV_D2= NPREV_D2
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HPREV
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_PREV_INVALIDE', ': ',
     &   NPREV, ' > ', NPREVL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NBR_ELEM_INVALIDE',': ',
     &   NLCL, ' /', NELET
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_PREV_INVALIDE', ': ',
     &   NLCL, ' /', NPREVL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_PREV_INI = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3
      
      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS  = -1.0D0
      SELF%OGRID  => NULL()
      SELF%HPREV  = 0
      SELF%NPREVL = 0
      SELF%NPREV  = 0
      SELF%NPREV_D1 = 0
      SELF%NPREV_D2 = 0
      SELF%PPREV  = SO_ALLC_VPTR3()

      HS_PREV_RAZ = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_RAZ

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      INTEGER FUNCTION HS_PREV_RST(SELF)

      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_PREV_RST = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_RST

C************************************************************************
C Sommaire: HS_PREV_PASDEB
C
C Description:
C     La méthode HS_PREV_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR3
      
      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER ISIZ
      INTEGER HPREV, HNUMR
      INTEGER NELLV, NPREV, NPREV_D1, NPREV_D2
      INTEGER, PARAMETER :: NPREL_MAX = 16
      LOGICAL DOGTHR(NPREL_MAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HPREV   = SELF%HPREV
      NPREV   = SELF%NPREV
      NPREV_D1= SELF%NPREV_D1
      NPREV_D2= SELF%NPREV_D2
      NELLV   = SELF%REQNELE()

C---     Alloue l'espace
      ISIZ = MAX(1, NELLV)  ! Min de 1 pour garantir l’existence
      SELF%PPREV = SO_ALLC_VPTR3(NPREV_D1, NPREV_D2, ISIZ)

C---     Lis les propriétés élémentaires
      IF (HPREV .NE. 0) THEN
         HNUMR = SELF%OGRID%REQHNUME()
C---        Lis
         IF (ERR_GOOD()) THEN
            IERR = DT_PRNO_CHARGE(HPREV, HNUMR, TNEW)
         ENDIF
C---        Gather
         IF (ERR_GOOD()) THEN
            DOGTHR(1:NPREV) = .TRUE.
            IERR = DT_PRNO_GATHER(HPREV,
     &                            HNUMR,
     &                            NPREV,
     &                            NELLV,
     &                            SELF%PPREV%VPTR,
     &                            DOGTHR,
     &                            TGET)
         ENDIF
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_PREV_PASDEB = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_PASDEB

C************************************************************************
C Sommaire: HS_PREV_PASMAJ
C
C Description:
C     La méthode HS_PREV_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_PASMAJ(SELF)

      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PREV_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_PASMAJ

C************************************************************************
C Sommaire: HS_PREV_PASFIN
C
C Description:
C     La méthode HS_PREV_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_PREV_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PREV_PASFIN = ERR_TYP()
      RETURN
      END FUNCTION HS_PREV_PASFIN

C************************************************************************
C Sommaire: Propriétés élémentaires de volume associées.
C
C Description:
C     La méthode HS_PREV_REQHPREV retourne le handle sur les propriétés
C     élémentaires de volume.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQHPREV(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQHPREV = SELF%HPREV
      RETURN
      END FUNCTION HS_PREV_REQHPREV

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQNELE(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQNELE = SELF%OGRID%REQNELLV()
      RETURN
      END FUNCTION HS_PREV_REQNELE

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PREV_REQNPREL retourne le nombre de propriétés de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQNPREL(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQNPREL = SELF%NPREV
      RETURN
      END FUNCTION HS_PREV_REQNPREL

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PREV_REQNPRELL retourne le nombre de propriétés lues de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQNPRELL(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQNPRELL = SELF%NPREVL
      RETURN
      END FUNCTION HS_PREV_REQNPRELL

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PREV_REQLPREL retourne le pointeur à la table des
C     propriétés maintenues par l'objet. La table est de dimensions
C     (NPREV, NELV) ou (NPREV_D1, NPREV_D2, NELV).
C     Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQLPREL(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQLPREL = SELF%PPREV%REQHNDL()
      RETURN
      END FUNCTION HS_PREV_REQLPREL

C************************************************************************
C Sommaire: Temps.
C
C Description:
C     La méthode HS_PREV_REQTEMPS retourne le temps des données.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PREV_REQTEMPS(SELF)

      CLASS(HS_PREV_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PREV_REQTEMPS = SELF%TEMPS
      RETURN
      END FUNCTION HS_PREV_REQTEMPS

      END MODULE HS_PREV_M
