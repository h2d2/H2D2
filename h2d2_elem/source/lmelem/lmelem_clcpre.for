C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_ELEM
C         FTN (Sub)Module: LM_ELEM_CLCPRE_M
C            Public:
C               MODULE INTEGER LM_ELEM_CLCPRE
C            Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_CLCPRE_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire: Pre-calcul.
C
C Description:
C     La fonction LM_ELEM_CLCPRE fait le pré-traitement avant
C     calcul sur toutes les données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_CLCPRE(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_CLCPRE
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Les prop. globales
      IF (ERR_GOOD()) IERR = SELF%PRCPRGL()

C---     Les prop. nodales
      IF (ERR_GOOD()) IERR = SELF%PRCPRNO()

C---     Les prop. élémentaires (volume et surface)
      IF (ERR_GOOD()) IERR = SELF%PRCPREV()
      IF (ERR_GOOD()) IERR = SELF%PRCPRES()

C---     Les sollicitations (réparties et concentrées)
      IF (ERR_GOOD()) IERR = SELF%PRCSOLC()
      IF (ERR_GOOD()) IERR = SELF%PRCSOLR()

C---     Les conditions limites
      IF (ERR_GOOD()) IERR = SELF%PRCCLIM()

C---     LE parallélisme
      IF (ERR_GOOD()) IERR = SELF%PRCPRLL()

C---     Les D.D.L.
      IF (ERR_GOOD()) IERR = SELF%PRCDLIB()

C---     Met à jour le nombre d’équations
      IF (ERR_GOOD()) IERR = SELF%ODLIB%MAJNEQ()
      IF (ERR_GOOD()) THEN
         SELF%EDTA%NEQL = SELF%ODLIB%REQNEQL()
         SELF%EDTA%NEQP = SELF%ODLIB%REQNEQP()
      ENDIF

C---     STATUS
      IF (ERR_GOOD()) THEN
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_PRC)
      ENDIF

      LM_ELEM_CLCPRE = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_CLCPRE

      END SUBMODULE LM_ELEM_CLCPRE_M

