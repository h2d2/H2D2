C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_PASMAJ
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_PASMAJ_M
      
      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Post-lecture.
C
C Description:
C     La sous-routine LM_ELEM_PASMAJ fait tout le traitement
C     post-lecture.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_PASMAJ(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PASMAJ
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     MAJ des données
!!!      IF (ERR_GOOD()) IERR = SELF%OCLIM%PASMAJ()
!!!      IF (ERR_GOOD()) IERR = SELF%OSOLC%PASMAJ()
!!!      IF (ERR_GOOD()) IERR = SELF%OSOLR%PASMAJ()
      IF (ERR_GOOD()) IERR = SELF%OPRGL%PASMAJ()
!!!      IF (ERR_GOOD()) IERR = SELF%OPRNO%PASMAJ()
!!!      IF (ERR_GOOD()) IERR = SELF%OPREV%PASMAJ()
!!!      IF (ERR_GOOD()) IERR = SELF%OPRES%PASMAJ()

C---     Traitement post-lecture des données lues
      IF (ERR_GOOD()) IERR = SELF%PSLPRGL()
!!!      IF (ERR_GOOD()) IERR = SELF%PSLPRNO()
!!!      IF (ERR_GOOD()) IERR = SELF%PSLPREV()
!!!      IF (ERR_GOOD()) IERR = SELF%PSLSOLC()
!!!      IF (ERR_GOOD()) IERR = SELF%PSLSOLR()
!!!      IF (ERR_GOOD()) IERR = SELF%PSLCLIM()

C---     STATUS
      IF (ERR_GOOD()) THEN
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_LIS)
      ENDIF

      LM_ELEM_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_PASMAJ

      END SUBMODULE LM_ELEM_PASMAJ_M
      