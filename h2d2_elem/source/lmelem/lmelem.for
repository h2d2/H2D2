C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_ELEM
C         FTN (Sub)Module: LM_ELEM_M
C            Public:
C            Private:
C               SUBROUTINE LM_ELEM_CTR
C               INTEGER LM_ELEM_DTR
C               INTEGER LM_ELEM_DEL
C            
C            FTN Type: LM_ELEM_T
C               Public:
C                  INTEGER LM_ELEM_INI
C                  INTEGER LM_ELEM_RST
C                  INTEGER LM_ELEM_REQPRM
C                  LOGICAL LM_ELEM_ESTINI
C                  LOGICAL LM_ELEM_ESTLIS
C                  LOGICAL LM_ELEM_ESTPRC
C                  LOGICAL LM_ELEM_ESTCLC
C                  LOGICAL LM_ELEM_ESTPSC
C                  INTEGER LM_ELEM_PUSHLDLG
C                  INTEGER LM_ELEM_POPLDLG
C                  INTEGER LM_ELEM_INIFIC
C                  INTEGER LM_ELEM_INIVAL
C                  INTEGER LM_ELEM_SAUVE
C                  SUBROUTINE LM_ELEM_REQHASH
C                  INTEGER LM_ELEM_REQHNUMC
C                  INTEGER LM_ELEM_REQHNUME
C                  REAL*8 LM_ELEM_REQTEMPS
C               Private:
C                  INTEGER LM_ELEM_INIHS
C                  INTEGER LM_ELEM_RAZ
C
C************************************************************************

      MODULE LM_ELEM_M

      USE LM_GEOM_M, ONLY: LM_GEOM_T
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE HS_BASE_M, ONLY: HS_BASE_T
      USE HS_CLIM_M, ONLY: HS_CLIM_T
      USE HS_DLIB_M, ONLY: HS_DLIB_T
      USE HS_PRES_M, ONLY: HS_PRES_T
      USE HS_PREV_M, ONLY: HS_PREV_T
      USE HS_PRGL_M, ONLY: HS_PRGL_T
      USE HS_PRNO_M, ONLY: HS_PRNO_T
      USE HS_SOLC_M, ONLY: HS_SOLC_T
      USE HS_SOLR_M, ONLY: HS_SOLR_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---  La classe
      !========================================================================
      TYPE, ABSTRACT :: LM_ELEM_T
         INTEGER STTUS     ! Status
         ! ---  Données gérées par la classe
         CLASS(HS_CLIM_T), POINTER :: OCLIM
         CLASS(HS_DLIB_T), POINTER :: ODLIB     ! Simulation DOF
         CLASS(HS_PRES_T), POINTER :: OPRES
         CLASS(HS_PREV_T), POINTER :: OPREV
         CLASS(HS_PRGL_T), POINTER :: OPRGL
         CLASS(HS_PRNO_T), POINTER :: OPRNO
         CLASS(HS_SOLC_T), POINTER :: OSOLC
         CLASS(HS_SOLR_T), POINTER :: OSOLR
         INTEGER HCALC     ! Handle on GPU Calculator
         TYPE (LM_EDTA_T) :: EDTA
         ! ---  Données externes
         TYPE (LM_GDTA_T), POINTER :: GDTA      ! Raccourci
         CLASS(LM_GEOM_T), POINTER :: OGEO
      CONTAINS
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI     => LM_ELEM_INI
         PROCEDURE, PUBLIC :: RST     => LM_ELEM_RST
         PROCEDURE, PUBLIC :: REQHASH => LM_ELEM_REQHASH
         PROCEDURE, PUBLIC :: REQPRM  => LM_ELEM_REQPRM

         PROCEDURE, PUBLIC :: PUSHLDLG=> LM_ELEM_PUSHLDLG
         PROCEDURE, PUBLIC :: POPLDLG => LM_ELEM_POPLDLG

         PROCEDURE, PUBLIC :: INIFIC  => LM_ELEM_INIFIC
         PROCEDURE, PUBLIC :: INIVAL  => LM_ELEM_INIVAL
         PROCEDURE, PUBLIC :: SAUVE   => LM_ELEM_SAUVE

         PROCEDURE, PUBLIC :: ASMDIM  => LM_ELEM_ASMDIM
         PROCEDURE, PUBLIC :: ASMIND  => LM_ELEM_ASMIND
         
         PROCEDURE, PUBLIC :: ASMF    => LM_ELEM_ASMF
         PROCEDURE, PUBLIC :: ASMFKU  => LM_ELEM_ASMFKU
         PROCEDURE, PUBLIC :: ASMK    => LM_ELEM_ASMK
         PROCEDURE, PUBLIC :: ASMKT   => LM_ELEM_ASMKT
         PROCEDURE, PUBLIC :: ASMKU   => LM_ELEM_ASMKU
         PROCEDURE, PUBLIC :: ASMM    => LM_ELEM_ASMM
         PROCEDURE, PUBLIC :: ASMMU   => LM_ELEM_ASMMU

         PROCEDURE, PUBLIC :: CLC     => LM_ELEM_CLC
         PROCEDURE, PUBLIC :: CLCPRE  => LM_ELEM_CLCPRE
         PROCEDURE, PUBLIC :: CLCPST  => LM_ELEM_CLCPST
         PROCEDURE, PUBLIC :: PRN     => LM_ELEM_PRN

         PROCEDURE, PUBLIC :: PASDEB  => LM_ELEM_PASDEB
         PROCEDURE, PUBLIC :: PASFIN  => LM_ELEM_PASFIN
         PROCEDURE, PUBLIC :: PASMAJ  => LM_ELEM_PASMAJ

         PROCEDURE, PUBLIC :: ESTINI  => LM_ELEM_ESTINI
         PROCEDURE, PUBLIC :: ESTLIS  => LM_ELEM_ESTLIS
         PROCEDURE, PUBLIC :: ESTPRC  => LM_ELEM_ESTPRC
         PROCEDURE, PUBLIC :: ESTCLC  => LM_ELEM_ESTCLC
         PROCEDURE, PUBLIC :: ESTPSC  => LM_ELEM_ESTPSC

         ! ---  Méthodes virtuelles
         PROCEDURE(DTR_GEN),     PUBLIC, DEFERRED :: DTR
         PROCEDURE(CLCF_GEN),    PUBLIC, DEFERRED :: CLCF
         PROCEDURE(CLCK_GEN),    PUBLIC, DEFERRED :: CLCK
         PROCEDURE(CLCKT_GEN),   PUBLIC, DEFERRED :: CLCKT
         PROCEDURE(CLCKU_GEN),   PUBLIC, DEFERRED :: CLCKU
         PROCEDURE(CLCM_GEN),    PUBLIC, DEFERRED :: CLCM
         PROCEDURE(CLCMU_GEN),   PUBLIC, DEFERRED :: CLCMU
         !PROCEDURE(CLCRES_GEN),  PUBLIC, DEFERRED :: CLCRES
         PROCEDURE(CLCCLIM_GEN), PUBLIC, DEFERRED :: CLCCLIM
         PROCEDURE(CLCDLIB_GEN), PUBLIC, DEFERRED :: CLCDLIB
         PROCEDURE(CLCPRES_GEN), PUBLIC, DEFERRED :: CLCPRES
         PROCEDURE(CLCPREV_GEN), PUBLIC, DEFERRED :: CLCPREV
         PROCEDURE(CLCPRNO_GEN), PUBLIC, DEFERRED :: CLCPRNO
         PROCEDURE(PRCCLIM_GEN), PUBLIC, DEFERRED :: PRCCLIM
         PROCEDURE(PRCDLIB_GEN), PUBLIC, DEFERRED :: PRCDLIB
         PROCEDURE(PRCPRES_GEN), PUBLIC, DEFERRED :: PRCPRES
         PROCEDURE(PRCPREV_GEN), PUBLIC, DEFERRED :: PRCPREV
         PROCEDURE(PRCPRGL_GEN), PUBLIC, DEFERRED :: PRCPRGL
         PROCEDURE(PRCPRLL_GEN), PUBLIC, DEFERRED :: PRCPRLL
         PROCEDURE(PRCPRNO_GEN), PUBLIC, DEFERRED :: PRCPRNO
         PROCEDURE(PRCSOLC_GEN), PUBLIC, DEFERRED :: PRCSOLC
         PROCEDURE(PRCSOLR_GEN), PUBLIC, DEFERRED :: PRCSOLR
         PROCEDURE(PRNCLIM_GEN), PUBLIC, DEFERRED :: PRNCLIM
         PROCEDURE(PRNPRGL_GEN), PUBLIC, DEFERRED :: PRNPRGL
         PROCEDURE(PRNPRNO_GEN), PUBLIC, DEFERRED :: PRNPRNO
         PROCEDURE(PSCPRNO_GEN), PUBLIC, DEFERRED :: PSCPRNO
         PROCEDURE(PSLCLIM_GEN), PUBLIC, DEFERRED :: PSLCLIM
         PROCEDURE(PSLDLIB_GEN), PUBLIC, DEFERRED :: PSLDLIB
         PROCEDURE(PSLPREV_GEN), PUBLIC, DEFERRED :: PSLPREV
         PROCEDURE(PSLPRGL_GEN), PUBLIC, DEFERRED :: PSLPRGL
         PROCEDURE(PSLPRNO_GEN), PUBLIC, DEFERRED :: PSLPRNO
         PROCEDURE(PSLSOLC_GEN), PUBLIC, DEFERRED :: PSLSOLC
         PROCEDURE(PSLSOLR_GEN), PUBLIC, DEFERRED :: PSLSOLR
         !PROCEDURE(PST_GEN),     PUBLIC, DEFERRED :: PST

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHNUMC => LM_ELEM_REQHNUMC
         PROCEDURE, PUBLIC :: REQHNUME => LM_ELEM_REQHNUME
         PROCEDURE, PUBLIC :: REQTEMPS => LM_ELEM_REQTEMPS
!!!         PROCEDURE, PUBLIC :: REQHCONF => LM_GEOM_REQHCONF  ! de OGEO
!!!         PROCEDURE, PUBLIC :: REQHDIST => LM_GEOM_REQHDIST
!!!         PROCEDURE, PUBLIC :: REQHGRID => LM_GEOM_REQHGRID
!!!         PROCEDURE, PUBLIC :: REQHLIMT => LM_GEOM_REQHLIMT

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => LM_ELEM_RAZ
         PROCEDURE, PRIVATE :: INIHS   => LM_ELEM_INIHS
         PROCEDURE, PRIVATE :: CP2EDTA => LM_ELEM_CP2EDTA
      END TYPE LM_ELEM_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: LM_ELEM_CTR
      PUBLIC :: LM_ELEM_DTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: LM_ELEM_DEL
      END INTERFACE DEL

      !========================================================================
      ! ---  Interface abstraite
      !========================================================================
      ABSTRACT INTERFACE
         INTEGER FUNCTION DTR_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION DTR_GEN

         ! ---  Fonction ASM
         INTEGER FUNCTION CLCF_GEN(SELF, VRES)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION CLCF_GEN

         INTEGER FUNCTION CLCKU_GEN(SELF, VRES)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION CLCKU_GEN

         INTEGER FUNCTION CLCK_GEN(SELF, HMTRX, F_ASM)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION CLCK_GEN

         INTEGER FUNCTION CLCKT_GEN(SELF, HMTRX, F_ASM)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION CLCKT_GEN

         INTEGER FUNCTION CLCM_GEN(SELF, HMTRX, F_ASM)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION CLCM_GEN

         INTEGER FUNCTION CLCMU_GEN(SELF, VRES)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(:)
         END FUNCTION CLCMU_GEN

         ! ---  Fonction CLC
         INTEGER FUNCTION CLCCLIM_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCCLIM_GEN

         INTEGER FUNCTION CLCDLIB_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCDLIB_GEN

         INTEGER FUNCTION CLCPRES_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCPRES_GEN

         INTEGER FUNCTION CLCPREV_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCPREV_GEN

         INTEGER FUNCTION CLCPRNO_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION CLCPRNO_GEN

         ! ---  Fonction PRC
         INTEGER FUNCTION PRCCLIM_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCCLIM_GEN

         INTEGER FUNCTION PRCDLIB_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCDLIB_GEN

         INTEGER FUNCTION PRCPRES_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCPRES_GEN

         INTEGER FUNCTION PRCPREV_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCPREV_GEN

         INTEGER FUNCTION PRCPRGL_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCPRGL_GEN

         INTEGER FUNCTION PRCPRLL_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCPRLL_GEN

         INTEGER FUNCTION PRCPRNO_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCPRNO_GEN

         INTEGER FUNCTION PRCSOLC_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCSOLC_GEN

         INTEGER FUNCTION PRCSOLR_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PRCSOLR_GEN

         ! ---  Fonction PRN
         INTEGER FUNCTION PRNCLIM_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
         END FUNCTION PRNCLIM_GEN

         INTEGER FUNCTION PRNPRGL_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
         END FUNCTION PRNPRGL_GEN

         INTEGER FUNCTION PRNPRNO_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
         END FUNCTION PRNPRNO_GEN

         ! ---  Fonction PSC
         INTEGER FUNCTION PSCPRNO_GEN(SELF, MODIF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
            LOGICAL, INTENT(OUT) :: MODIF
         END FUNCTION PSCPRNO_GEN

         ! ---  Fonction PSL
         INTEGER FUNCTION PSLCLIM_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLCLIM_GEN

         INTEGER FUNCTION PSLDLIB_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLDLIB_GEN

         INTEGER FUNCTION PSLPREV_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLPREV_GEN

         INTEGER FUNCTION PSLPRGL_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLPRGL_GEN

         INTEGER FUNCTION PSLPRNO_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLPRNO_GEN

         INTEGER FUNCTION PSLSOLC_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLSOLC_GEN

         INTEGER FUNCTION PSLSOLR_GEN(SELF)
            IMPORT :: LM_ELEM_T
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION PSLSOLR_GEN

      END INTERFACE

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LM_ELEM_CP2EDTA(SELF)
            CLASS (LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_ELEM_CP2EDTA

         MODULE INTEGER FUNCTION LM_ELEM_ASMDIM(SELF, ILU, IA, KLD)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: ILU
            INTEGER, INTENT(OUT) :: IA (:)
            INTEGER, INTENT(OUT) :: KLD(:)
         END FUNCTION LM_ELEM_ASMDIM
         
         MODULE INTEGER FUNCTION LM_ELEM_ASMIND(SELF, ILU, IA, JA)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN)  :: ILU
            INTEGER, INTENT(IN)  :: IA (:)
            INTEGER, INTENT(OUT) :: JA (:)
         END FUNCTION LM_ELEM_ASMIND
         
         ! ---  ASM
         MODULE INTEGER FUNCTION LM_ELEM_ASMF(SELF, VRES)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(*)
         END FUNCTION LM_ELEM_ASMF

         MODULE INTEGER FUNCTION LM_ELEM_ASMKU(SELF, VRES)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(*)
         END FUNCTION LM_ELEM_ASMKU

         MODULE INTEGER FUNCTION LM_ELEM_ASMFKU(SELF, VRES)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(*)
         END FUNCTION LM_ELEM_ASMFKU

         MODULE INTEGER FUNCTION LM_ELEM_ASMK(SELF, HMTRX, F_ASM)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_ELEM_ASMK

         MODULE INTEGER FUNCTION LM_ELEM_ASMKT(SELF, HMTRX, F_ASM)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_ELEM_ASMKT

         MODULE INTEGER FUNCTION LM_ELEM_ASMM(SELF, HMTRX, F_ASM)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            INTEGER, INTENT(IN) :: HMTRX
            INTEGER F_ASM
            EXTERNAL F_ASM
         END FUNCTION LM_ELEM_ASMM

         MODULE INTEGER FUNCTION LM_ELEM_ASMMU(SELF, VRES)
            CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
            REAL*8, INTENT(INOUT) :: VRES(*)
         END FUNCTION LM_ELEM_ASMMU

         ! ---  CLC
         MODULE INTEGER FUNCTION LM_ELEM_CLC(SELF)
            CLASS (LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_ELEM_CLC

         MODULE INTEGER FUNCTION LM_ELEM_CLCPRE(SELF)
            CLASS (LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_ELEM_CLCPRE

         MODULE INTEGER FUNCTION LM_ELEM_CLCPST(SELF)
            CLASS (LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_ELEM_CLCPST

         MODULE INTEGER FUNCTION LM_ELEM_PASDEB(SELF, TSIM)
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
            REAL*8, INTENT(IN) :: TSIM
         END FUNCTION LM_ELEM_PASDEB

         MODULE INTEGER FUNCTION LM_ELEM_PASFIN(SELF, TSIM)
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
            REAL*8, INTENT(IN) :: TSIM
         END FUNCTION LM_ELEM_PASFIN

         MODULE INTEGER FUNCTION LM_ELEM_PASMAJ(SELF)
            CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_ELEM_PASMAJ

         MODULE INTEGER FUNCTION LM_ELEM_PRN(SELF)
            CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
         END FUNCTION LM_ELEM_PRN

      END INTERFACE

      ! ---  Énumération des états
      INTEGER, PARAMETER :: LM_ELEM_STTUS_000 = 0
      INTEGER, PARAMETER :: LM_ELEM_STTUS_INI = 1
      INTEGER, PARAMETER :: LM_ELEM_STTUS_LIS = 2
      INTEGER, PARAMETER :: LM_ELEM_STTUS_PRC = 3
      INTEGER, PARAMETER :: LM_ELEM_STTUS_CLC = 4
      INTEGER, PARAMETER :: LM_ELEM_STTUS_PSC = 5

      CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     Le constructeur de la classe LM_ELEM_CTR fait toutes les
C     connections liées à la structure de la hiérarchie des classes.
C
C Entrée:
C     HKID     Handle sur l'héritier
C     HGEO     Handle sur l'élément géométrique parent
C
C Sortie:
C     HOBJ     Handle sur l'objet créé
C
C Notes:
C     La classe est abstraite, OKID & OGEO doivent exister.
C************************************************************************
      FUNCTION LM_ELEM_CTR(OKID, OGEO) RESULT(SELF)

      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: OKID
      CLASS(LM_GEOM_T), INTENT(IN), TARGET :: OGEO

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_ELEM_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cast le pointeur
      SELF => OKID

C---     Initialise
      IF (ERR_GOOD()) IERR = SELF%RAZ()
      IF (ERR_GOOD()) THEN
         SELF%OGEO => OGEO
         SELF%GDTA => OGEO%GDTA
      ENDIF

      RETURN
      END FUNCTION LM_ELEM_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_DTR(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()
      IERR = SELF%DTR()

C---     Déconnecte de l'élément géométrique
      SELF%OGEO => NULL()
      SELF%GDTA => NULL()

      LM_ELEM_DTR = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     SELF est passé par pointeur pour permettre la précondition
C************************************************************************
      INTEGER FUNCTION LM_ELEM_DEL(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(ASSOCIATED(SELF))
C------------------------------------------------------------------------

      LM_ELEM_DEL = SELF%DTR()
      RETURN
      END FUNCTION LM_ELEM_DEL

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il y a un lien 1-1 entre HSIMD et l'élément. Il faut quand même
C     réassigner le LM_EDTA_T à chaque appel, car les données
C     peuvent ne pas être encore toutes chargées, ou alors elle changent
C     (en particulier VDLG).
C************************************************************************
      INTEGER FUNCTION LM_ELEM_INI(SELF,
     &                             HCONF,
     &                             HGRID,
     &                             HDLIB,
     &                             HCLIM,
     &                             HSOLC,
     &                             HSOLR,
     &                             HPRGL,
     &                             HPRNO,
     &                             HPREV)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_INI
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF
      INTEGER, INTENT(IN) :: HCONF
      INTEGER, INTENT(IN) :: HGRID
      INTEGER, INTENT(IN) :: HDLIB
      INTEGER, INTENT(IN) :: HCLIM
      INTEGER, INTENT(IN) :: HSOLC
      INTEGER, INTENT(IN) :: HSOLR
      INTEGER, INTENT(IN) :: HPRGL
      INTEGER, INTENT(IN) :: HPRNO
      INTEGER, INTENT(IN) :: HPREV

      INCLUDE 'err.fi'
      INCLUDE 'dtclim.fi'

      INTEGER IERR
      INTEGER HLIMT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_CLIM_HVALIDE(HCLIM))
C-----------------------------------------------------------------------

C---     Reset les données
      IERR = SELF%RST()

C---     Initialise les parents
      HLIMT = DT_CLIM_REQHLIMT(HCLIM)
      IF (ERR_GOOD()) IERR = SELF%OGEO%INI(HCONF,
     &                                     HGRID,
     &                                     HLIMT)

C---     Construis les données de simulation
      IF (ERR_GOOD()) IERR = SELF%INIHS(HDLIB,
     &                                  HCLIM,
     &                                  HSOLC,
     &                                  HSOLR,
     &                                  HPRGL,
     &                                  HPRNO,
     &                                  HPREV)
      IF (ERR_GOOD()) IERR = SELF%CP2EDTA()

      LM_ELEM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée LM_ELEM_INIHS
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_INIHS(SELF,
     &                               HDLIB,
     &                               HCLIM,
     &                               HSOLC,
     &                               HSOLR,
     &                               HPRGL,
     &                               HPRNO,
     &                               HPREV)

      USE GR_GRID_M, ONLY: GR_GRID_T
      USE HS_CLIM_M, ONLY: HS_CLIM_T, HS_CLIM_CTR
      USE HS_DLIB_M, ONLY: HS_DLIB_T, HS_DLIB_CTR
      USE HS_PRGL_M, ONLY: HS_PRGL_T, HS_PRGL_CTR
      USE HS_PRNO_M, ONLY: HS_PRNO_T, HS_PRNO_CTR
      USE HS_PRES_M, ONLY: HS_PRES_T, HS_PRES_CTR
      USE HS_PREV_M, ONLY: HS_PREV_T, HS_PREV_CTR
      USE HS_SOLC_M, ONLY: HS_SOLC_T, HS_SOLC_CTR
      USE HS_SOLR_M, ONLY: HS_SOLR_T, HS_SOLR_CTR

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF
      INTEGER, INTENT(IN) :: HDLIB
      INTEGER, INTENT(IN) :: HCLIM
      INTEGER, INTENT(IN) :: HSOLC
      INTEGER, INTENT(IN) :: HSOLR
      INTEGER, INTENT(IN) :: HPRGL
      INTEGER, INTENT(IN) :: HPRNO
      INTEGER, INTENT(IN) :: HPREV

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(GR_GRID_T), POINTER :: OGRID
      CLASS(HS_CLIM_T), POINTER :: OCLIM
      CLASS(HS_DLIB_T), POINTER :: ODLIB
      CLASS(HS_PRGL_T), POINTER :: OPRGL
      CLASS(HS_PRNO_T), POINTER :: OPRNO
      CLASS(HS_PRES_T), POINTER :: OPRES
      CLASS(HS_PREV_T), POINTER :: OPREV
      CLASS(HS_SOLC_T), POINTER :: OSOLC
      CLASS(HS_SOLR_T), POINTER :: OSOLR
      INTEGER HFRML
C-----------------------------------------------------------------------

C---     Les attributs
      OGRID => SELF%OGEO%OGRID

C---     Crée des DLIB
      IF (ERR_GOOD()) THEN
         ODLIB => HS_DLIB_CTR()
         IERR = ODLIB%INI(SELF%EDTA, OGRID, HDLIB)
         IF (ERR_GOOD()) SELF%ODLIB => ODLIB
      ENDIF

C---     Crée des CLIM
      IF (ERR_GOOD()) THEN
         OCLIM => HS_CLIM_CTR()
         IERR = OCLIM%INI(SELF%EDTA, OGRID, HCLIM)
         IF (ERR_GOOD()) SELF%OCLIM => OCLIM
      ENDIF

C---     Crée des SOLC
      IF (ERR_GOOD()) THEN
         OSOLC => HS_SOLC_CTR()
         IERR = OSOLC%INI(SELF%EDTA, OGRID, HSOLC)
         IF (ERR_GOOD()) SELF%OSOLC => OSOLC
      ENDIF

C---     Crée des SOLR
      IF (ERR_GOOD()) THEN
         OSOLR => HS_SOLR_CTR()
         IERR = OSOLR%INI(SELF%EDTA, OGRID, HSOLR)
         IF (ERR_GOOD()) SELF%OSOLR => OSOLR
      ENDIF

C---     Crée des PRGL
      IF (ERR_GOOD()) THEN
         OPRGL => HS_PRGL_CTR()
         IERR = OPRGL%INI(SELF%EDTA, OGRID, HPRGL)
         IF (ERR_GOOD()) SELF%OPRGL => OPRGL
      ENDIF

C---     Crée des PRNO
      IF (ERR_GOOD()) THEN
         OPRNO => HS_PRNO_CTR()
         IERR = OPRNO%INI(SELF%EDTA, OGRID, HPRNO)
         IF (ERR_GOOD()) SELF%OPRNO => OPRNO
      ENDIF

C---     Crée des PREV
      IF (ERR_GOOD()) THEN
         OPREV => HS_PREV_CTR()
         IERR = OPREV%INI(SELF%EDTA, OGRID, HPREV)
         IF (ERR_GOOD()) SELF%OPREV => OPREV
      ENDIF

C---     Crée des PRES
      IF (ERR_GOOD()) THEN
         OPRES => HS_PRES_CTR()
         IERR = OPRES%INI(SELF%EDTA, OGRID)
         IF (ERR_GOOD()) SELF%OPRES => OPRES
      ENDIF

!!!C---     En cas d'erreur, imprime les prop
!!!      IF (ERR_BAD()) THEN
!!!         CALL ERR_PUSH()
!!!         IERR = LM_ELEM_DMPPROP(HFRML)
!!!         CALL ERR_POP()
!!!      ENDIF

C---     Statut
      IF (ERR_GOOD()) SELF%STTUS = LM_ELEM_STTUS_000

      LM_ELEM_INIHS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_RST(SELF)

      USE HS_BASE_M, ONLY: DEL

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'gpclcl.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IF (ASSOCIATED(SELF%ODLIB)) IERR = DEL(SELF%ODLIB)
      IF (ASSOCIATED(SELF%OCLIM)) IERR = DEL(SELF%OCLIM)
      IF (ASSOCIATED(SELF%OSOLC)) IERR = DEL(SELF%OSOLC)
      IF (ASSOCIATED(SELF%OSOLR)) IERR = DEL(SELF%OSOLR)
      IF (ASSOCIATED(SELF%OPRGL)) IERR = DEL(SELF%OPRGL)
      IF (ASSOCIATED(SELF%OPRNO)) IERR = DEL(SELF%OPRNO)
      IF (ASSOCIATED(SELF%OPREV)) IERR = DEL(SELF%OPREV)
      IF (ASSOCIATED(SELF%OPRES)) IERR = DEL(SELF%OPRES)

      IF (GP_CLCL_HVALIDE(SELF%HCALC)) IERR = GP_CLCL_DTR(SELF%HCALC)

      IF (ASSOCIATED(SELF%OGEO)) IERR = SELF%OGEO%RST()

      IERR = SELF%RAZ()

      LM_ELEM_RST = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_RST

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_RAZ(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      SELF%STTUS = LM_ELEM_STTUS_000
      SELF%ODLIB => NULL()
      SELF%OCLIM => NULL()
      SELF%OSOLC => NULL()
      SELF%OSOLR => NULL()
      SELF%OPRGL => NULL()
      SELF%OPRNO => NULL()
      SELF%OPREV => NULL()
      SELF%OPRES => NULL()
      SELF%HCALC = 0

      IERR = SELF%EDTA%RST()

      LM_ELEM_RAZ = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_RAZ

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_REQPRM(SELF, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQPRM
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(IN) :: IPRM

      INCLUDE 'lmelem.fi'  ! Pour les enum
      INCLUDE 'lmgeom.fi'

      TYPE (LM_EDTA_T), POINTER :: EDTA
      INTEGER IVAL
C-----------------------------------------------------------------------

      IF  (IPRM .LT. LM_GEOM_PRM_LAST) THEN
         IVAL = SELF%OGEO%REQPRM(IPRM)
      ELSE
         EDTA => SELF%EDTA

         SELECT CASE(IPRM)
            CASE(LM_ELEM_PRM_NDLN);  IVAL = EDTA%NDLN
            CASE(LM_ELEM_PRM_NDLEV); IVAL = EDTA%NDLEV
            CASE(LM_ELEM_PRM_NDLES); IVAL = EDTA%NDLES
            CASE(LM_ELEM_PRM_NPRGL); IVAL = EDTA%NPRGL
            CASE(LM_ELEM_PRM_NPRGLL);IVAL = EDTA%NPRGLL
            CASE(LM_ELEM_PRM_NPRNO); IVAL = EDTA%NPRNO
            CASE(LM_ELEM_PRM_NPRNOL);IVAL = EDTA%NPRNOL
            CASE(LM_ELEM_PRM_NPREV); IVAL = EDTA%NPREV
            CASE(LM_ELEM_PRM_NPREVL);IVAL = EDTA%NPREVL
            CASE(LM_ELEM_PRM_NPRES); IVAL = EDTA%NPRES
            CASE(LM_ELEM_PRM_NSOLC); IVAL = EDTA%NSOLC
            CASE(LM_ELEM_PRM_NSOLCL);IVAL = EDTA%NSOLCL
            CASE(LM_ELEM_PRM_NSOLR); IVAL = EDTA%NSOLR
            CASE(LM_ELEM_PRM_NSOLRL);IVAL = EDTA%NSOLRL

            CASE(LM_ELEM_PRM_NDLP);  IVAL = EDTA%NDLP
            CASE(LM_ELEM_PRM_NDLL);  IVAL = EDTA%NDLL
            CASE(LM_ELEM_PRM_NDLT);  IVAL = EDTA%NDLT
            CASE(LM_ELEM_PRM_NEQP);  IVAL = EDTA%NEQP
            CASE(LM_ELEM_PRM_NEQL);  IVAL = EDTA%NEQL
            CASE(LM_ELEM_PRM_NEQT);  IVAL = EDTA%NEQT

            CASE default
               CALL ERR_ASR(.false.)
         END SELECT
      ENDIF

      LM_ELEM_REQPRM = IVAL
      RETURN
      END FUNCTION LM_ELEM_REQPRM

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION LM_ELEM_ESTINI(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ESTINI
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      LM_ELEM_ESTINI = (SELF%STTUS .GE. LM_ELEM_STTUS_INI)
      RETURN
      END FUNCTION LM_ELEM_ESTINI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION LM_ELEM_ESTLIS(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ESTLIS
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_ESTLIS = (SELF%STTUS .GE. LM_ELEM_STTUS_LIS)
      RETURN
      END FUNCTION LM_ELEM_ESTLIS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION LM_ELEM_ESTPRC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ESTPRC
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_ESTPRC = (SELF%STTUS .GE. LM_ELEM_STTUS_PRC)
      RETURN
      END FUNCTION LM_ELEM_ESTPRC

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION LM_ELEM_ESTCLC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ESTCLC
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_ESTCLC = (SELF%STTUS .GE. LM_ELEM_STTUS_CLC)
      RETURN
      END FUNCTION LM_ELEM_ESTCLC

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION LM_ELEM_ESTPSC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ESTPSC
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      LM_ELEM_ESTPSC = (SELF%STTUS .GE. LM_ELEM_STTUS_PSC)
      RETURN
      END FUNCTION LM_ELEM_ESTPSC

C************************************************************************
C Sommaire: LM_ELEM_PUSHLDLG
C
C Description:
C     La fonction LM_ELEM_PUSHLDLG permet d'assigner à l'objet une table
C     externe comme VDLG. L'ancienne table est poussée sur une pile et
C     peut être dépilée par l'appel symétrique à LM_ELEM_POPLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_PUSHLDLG (SELF, LDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PUSHLDLG
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF
      INTEGER LDLG

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SELF%ESTPRC())
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Assigne les DDL
      IF (ERR_GOOD()) IERR = SELF%ODLIB%PUSHLDLG(LDLG)

C---     Recalcule les prop avec les DDL modifiés
      IF (ERR_GOOD()) IERR = SELF%CLC()

C---     Status
      IF (ERR_GOOD()) THEN
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_CLC)
      ENDIF

      LM_ELEM_PUSHLDLG = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_PUSHLDLG

C************************************************************************
C Sommaire: LM_ELEM_POPLDLG
C
C Description:
C     La fonction LM_ELEM_POPLDLG restaure à l'objet la table poussée
C     sur la pile par LM_ELEM_PUSHLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_POPLDLG (SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_POPLDLG
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SELF%ESTPRC())
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Assigne les DDL
      IF (ERR_GOOD()) IERR = SELF%ODLIB%POPLDLG()

C---     Recalcule les prop avec les DDL modifiés
      IF (ERR_GOOD()) IERR = SELF%CLC()

C---     Status
      IF (ERR_GOOD()) THEN
         SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_CLC)
      ENDIF

      LM_ELEM_POPLDLG = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_POPLDLG

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_INIFIC(SELF, TSIM, FILNAM, ISTAT)

      USE HS_DLIB_M

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF
      REAL*8,        INTENT(IN) :: TSIM
      CHARACTER*(*), INTENT(IN) :: FILNAM
      INTEGER,       INTENT(IN) :: ISTAT

      INCLUDE 'err.fi'

      REAL*8  TINI
      INTEGER IERR
C------------------------------------------------------------------------

C---     Initialise
      TINI = TSIM
      IERR = SELF%ODLIB%INIFIC(TINI, FILNAM, ISTAT)

C---     Met à jour l'état
      SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_INI)
      SELF%EDTA%TEMPS = TSIM

C---     Lis les données
      IF (ERR_GOOD()) IERR = SELF%PASDEB(TSIM)

      LM_ELEM_INIFIC = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_INIFIC

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_INIVAL(SELF, TSIM, VVAL)

      USE HS_DLIB_M

      CLASS(LM_ELEM_T), INTENT(INOUT) :: SELF
      REAL*8,  INTENT(IN) :: TSIM
      REAL*8,  INTENT(IN) :: VVAL(:)

      INCLUDE 'err.fi'

      REAL*8  TINI
      INTEGER IERR
C------------------------------------------------------------------------

C---     Initialise
      TINI = TSIM
      IERR = SELF%ODLIB%INIVAL(TINI, VVAL)

C---     Met à jour l'état
      SELF%STTUS = MAX(SELF%STTUS, LM_ELEM_STTUS_INI)
      SELF%EDTA%TEMPS = TSIM

C---     Lis les données
      IF (ERR_GOOD()) IERR = SELF%PASDEB(TSIM)

      LM_ELEM_INIVAL = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_INIVAL

C************************************************************************
C Sommaire: HS_DLIB_SAUVE
C
C Description:
C     La fonction <code>HS_DLIB_SAUVE(...)</code> sauve les degrés de
C     liberté de l'objet dans le fichier de nom FILNAM.
C     Si ISTAT .EQ. IO_AJOUT les données sont ajoutées au fichier déjà
C     existant.
C     Si ISTAT .EQ. IO_ECRITURE, le contenu du fichier déjà existant est
C     écrasé.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     FILNAM      Nom du fichier de sauvegarde
C     ISTAT       IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_SAUVE (SELF, FILNAM, ISTAT)

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
      CHARACTER*(*),    INTENT(IN) :: FILNAM
      INTEGER,          INTENT(IN) :: ISTAT

      INCLUDE 'err.fi'
!!      INCLUDE 'dtvnod.fi'
!!      INCLUDE 'spstrn.fi'
!!
!!      INTEGER IERR
!!      INTEGER HVNOD, HNUMR
!!      INTEGER NNT
!!      INTEGER NDLN
!!C-----------------------------------------------------------------------
!!
!!      IERR = ERR_OK
!!
!!C---     Récupère les attributs
!!      NNT   = SELF%REQNNT ()
!!      NDLN  = SELF%REQNDLN()
!!      HVNOD = SELF%HVNOD
!!D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))
!!
!!C---     Contrôle les dimensions
!!      IF (ERR_GOOD() .AND. DT_VNOD_REQNVNO(HVNOD) .NE. NDLN) GOTO 9900
!!      IF (ERR_GOOD() .AND. DT_VNOD_REQNNT (HVNOD) .NE. NNT)  GOTO 9901
!!
!!C---     Assigne le fichier d'écriture
!!      IF (ERR_GOOD())
!!     &   IERR = DT_VNOD_ASGFICECR(HVNOD,
!!     &                            FILNAM(1:SP_STRN_LEN(FILNAM)),
!!     &                            ISTAT,
!!     &                            0)   !No handle, build with name
!!
!!C---     Sauve les données
!!      IF (ERR_GOOD()) HNUMR = SELF%OGRID%REQHNUMC()
!!      IF (ERR_GOOD()) IERR = DT_VNOD_SAUVE (HVNOD, HNUMR)
!!
!!      GOTO 9999
!!C------------------------------------------------------------------------
!!9900  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_NDLN_INCOMPATIBLES',':',
!!     &       DT_VNOD_REQNVNO(HVNOD), '/', NDLN
!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!      GOTO 9999
!!9901  WRITE(ERR_BUF,'(2A,I9,A,I9)')'ERR_NNT_INCOMPATIBLES',':',
!!     &       DT_VNOD_REQNNT(HVNOD), '/', NNT
!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!      GOTO 9999
!!
!!9999  CONTINUE
      LM_ELEM_SAUVE = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_SAUVE

C************************************************************************
C Sommaire: Retourne le hash (MD5) de la table KLOCN.
C
C Description:
C     La méthode LM_ELEM_REQHASH retourne le hash (MD5) de la table KLOCN.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_REQHASH(SELF) RESULT(OHASH)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQHASH
CDEC$ ENDIF

      USE SO_HASH_M, ONLY: SO_HASH_T, SO_HASH_CTR_SELF
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T, SO_ALLC_KPTR2

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF

      INTEGER IERR
      TYPE(SO_HASH_T) :: OHASH
      TYPE(SO_ALLC_KPTR2_T) :: PLOCN
C-----------------------------------------------------------------------

      PLOCN = SO_ALLC_KPTR2(SELF%EDTA%KLOCN)

      OHASH = SO_HASH_CTR_SELF()
      IERR = OHASH%INI(PLOCN)

      RETURN
      END FUNCTION LM_ELEM_REQHASH

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION LM_ELEM_REQHLMGO(HOBJ)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQHLMGO
!!!CDEC$ ENDIF
!!!
!!!      USE LM_ELEM_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!
!!!      INCLUDE 'lmelem.fi'
!!!      INCLUDE 'lmutil.fi'
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      LM_ELEM_REQHLMGO = LM_UTIL_REQxxx(HOBJ, IX_GEO)
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C     La méthode LM_ELEM_REQHDLIB retourne le handle au DTVNO qui contient
!!!C     les DDL.
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION LM_ELEM_REQHDLIB(HOBJ)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQHDLIB
!!!CDEC$ ENDIF
!!!
!!!      USE LM_ELEM_M
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!
!!!      INCLUDE 'lmelem.fi'
!!!      INCLUDE 'hsdlib.fi'
!!!
!!!      TYPE (LM_ELEM_T), POINTER :: SELF
!!!C-----------------------------------------------------------------------
!!!D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      SELF => LM_ELEM_REQSELF(HOBJ)
!!!      LM_ELEM_REQHDLIB = HS_DLIB_REQHVNOD(SELF%HDLIB)
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_REQHNUMC(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQHNUMC
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_REQHNUMC = SELF%OGEO%REQHNUMC()
      RETURN
      END FUNCTION LM_ELEM_REQHNUMC

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_ELEM_REQHNUME(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQHNUME
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_REQHNUME = SELF%OGEO%REQHNUME()
      RETURN
      END FUNCTION LM_ELEM_REQHNUME

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION LM_ELEM_REQTEMPS(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_REQTEMPS
CDEC$ ENDIF

      CLASS(LM_ELEM_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      LM_ELEM_REQTEMPS = SELF%ODLIB%REQTEMPS()
      RETURN
      END FUNCTION LM_ELEM_REQTEMPS

      END MODULE LM_ELEM_M


