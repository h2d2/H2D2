C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_ASMKU
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_ASMKU_M

      IMPLICIT NONE
      
      CONTAINS

C************************************************************************
C Sommaire: Assemble le produit [K].{U}
C
C Description:
C     La fonction LM_ELEM_ASMKU assemble le produit [K].{U} dans
C     le tableau VRES. Celui-ci est initialisé à zéro.
C
C Entrée:
C     SELF     L'objet
C
C Sortie:
C     VRES
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_ASMKU(SELF, VRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_ASMKU
CDEC$ ENDIF

      CLASS (LM_ELEM_T), INTENT(IN), TARGET :: SELF
      REAL*8, INTENT(INOUT) :: VRES(*)

      INCLUDE 'err.fi'
      INCLUDE 'eacnst.fi'

      INTEGER IERR
      INTEGER NEQL
C-----------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.residu')

C---     Assemble
      NEQL = SELF%EDTA%NEQL
      VRES(1:NEQL) = ZERO
      IF (ERR_GOOD()) IERR = SELF%CLCKU(VRES(1:NEQL))

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.reso.residu')

      LM_ELEM_ASMKU = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASMKU

      END SUBMODULE LM_ELEM_ASMKU_M
