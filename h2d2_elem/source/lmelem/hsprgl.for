C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER HS_PRGL_000
C     INTEGER HS_PRGL_999
C     INTEGER HS_PRGL_CTR
C     INTEGER HS_PRGL_DTR
C     INTEGER HS_PRGL_INI
C     INTEGER HS_PRGL_RST
C     INTEGER HS_PRGL_REQHBASE
C     LOGICAL HS_PRGL_HVALIDE
C     INTEGER HS_PRGL_PASDEB
C     INTEGER HS_PRGL_PASMAJ
C     INTEGER HS_PRGL_PASFIN
C     INTEGER HS_PRGL_REQHLMAP
C     INTEGER HS_PRGL_REQHGRID
C     INTEGER HS_PRGL_REQHPRGL
C     INTEGER HS_PRGL_REQNPRGL
C     INTEGER HS_PRGL_REQNPRGLL
C     INTEGER HS_PRGL_REQLPRGL
C   Private:
C
C************************************************************************

      MODULE HS_PRGL_M
      
      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_PRGL_T
         TYPE(SO_ALLC_VPTR1_T) :: PPRGL
         INTEGER, PRIVATE :: HPRGL
         INTEGER, PRIVATE :: NPRGL
         INTEGER, PRIVATE :: NPRGLL
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => HS_PRGL_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_PRGL_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_PRGL_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_PRGL_PASFIN
                                       
         ! ---  Méthodes publiques     
         PROCEDURE, PUBLIC :: INI      => HS_PRGL_INI
         PROCEDURE, PUBLIC :: RST      => HS_PRGL_RST
         
         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQHPRGL => HS_PRGL_REQHPRGL
         PROCEDURE, PUBLIC :: REQNPRGL => HS_PRGL_REQNPRGL
         PROCEDURE, PUBLIC :: REQNPRGLL=> HS_PRGL_REQNPRGLL
         PROCEDURE, PUBLIC :: REQLPRGL => HS_PRGL_REQLPRGL
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_PRGL_RAZ
      END TYPE HS_PRGL_T

      ! ---  Constructor
      PUBLIC :: HS_PRGL_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_PRGL_CTR
C
C Description:
C     Le constructeur <code>HS_PRGL_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_PRGL_CTR() RESULT(SELF)

      TYPE(HS_PRGL_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_PRGL_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_PRGL_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_DTR(SELF)
      
      CLASS(HS_PRGL_T), INTENT(INOUT), TARGET :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_PRGL_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_PRGL_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_PRGL_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HPRGL       Handle sur les propriétés globales (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_INI(SELF, EDTA, OGRID, HPRGL)

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      
      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HPRGL

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NPRGL, NPRGLL
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (HPRGL .NE. 0 .AND. .NOT. DT_PRGL_HVALIDE(HPRGL)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NPRGL  = EDTA%NPRGL
      NPRGLL = EDTA%NPRGLL
      IF (NPRGL .LT. NPRGLL) GOTO 9902

C---     Contrôles
      NLCL = 0
      IF (HPRGL .NE. 0) NLCL = DT_PRGL_REQNPRGL(HPRGL)
      IF (NLCL .NE. NPRGLL) GOTO 9903

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID  => OGRID
         SELF%HPRGL  = HPRGL
         SELF%NPRGL  = NPRGL
         SELF%NPRGLL = NPRGLL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HPRGL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_PRGL_INVALIDE', ': ',
     &   NPRGL, ' > ', NPRGLL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_PRGL_INVALIDE', ': ',
     &   NLCL, ' /', NPRGLL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_PRGL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1
      
      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS  = -1.0D0
      SELF%OGRID  => NULL()
      SELF%HPRGL  = 0
      SELF%NPRGL  = 0
      SELF%NPRGLL = 0
      SELF%PPRGL  = SO_ALLC_VPTR1()

      HS_PRGL_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_RST(SELF)

      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_PRGL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_PRGL_PASDEB
C
C Description:
C     La méthode HS_PRGL_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1

      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER HPRGL
      INTEGER NPRGL
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HPRGL = SELF%HPRGL
      NPRGL = SELF%NPRGL

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PPRGL = SO_ALLC_VPTR1(NPRGL)

C---     Lis les propriétés nodales
      IF (HPRGL .NE. 0) THEN
C---        Lis
         IF (ERR_GOOD()) THEN
            IERR = DT_PRGL_CHARGE(HPRGL, TNEW)
         ENDIF
C---        Gather
         IF (ERR_GOOD()) THEN
            IERR = DT_PRGL_GATHER(HPRGL,
     &                            NPRGL,
     &                            SELF%PPRGL%VPTR,
     &                            TGET)
         ENDIF
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_PRGL_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_PRGL_PASMAJ
C
C Description:
C     La méthode HS_PRGL_PASMAJ 
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_PASMAJ(SELF)

      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER HPRGL
      INTEGER NPRGL
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HPRGL = SELF%HPRGL
      NPRGL = SELF%NPRGL
D     CALL ERR_ASR(SELF%PPRGL%ISVALID())

C---     Lis les propriétés nodales
      IF (ERR_GOOD() .AND. HPRGL .NE. 0) THEN
            IERR = DT_PRGL_GATHER(HPRGL,
     &                            NPRGL,
     &                            SELF%PPRGL%VPTR,
     &                            TGET)
      ENDIF

      HS_PRGL_PASMAJ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_PRGL_PASFIN
C
C Description:
C     La méthode HS_PRGL_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_PRGL_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_PRGL_PASFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Valeurs globales associées.
C
C Description:
C     La méthode HS_PRGL_REQHPRGL retourne le handle sur les propriétés
C     globales.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_REQHPRGL(SELF)

      CLASS(HS_PRGL_T), INTENT(IN) :: SELF
C-----------------------------------------------------------------------

      HS_PRGL_REQHPRGL = SELF%HPRGL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_REQNPRGL(SELF)

      CLASS(HS_PRGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRGL_REQNPRGL = SELF%NPRGL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_REQNPRGLL(SELF)

      CLASS(HS_PRGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRGL_REQNPRGLL = SELF%NPRGLL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_PRGL_REQLPRGL retourne le pointeur à la table des
C     propriétés maintenues par l'objet. La table est de dimensions
C     (NPRGL). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_PRGL_REQLPRGL(SELF)

      CLASS(HS_PRGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_PRGL_REQLPRGL = SELF%PPRGL%REQHNDL()
      RETURN
      END

      END MODULE HS_PRGL_M
