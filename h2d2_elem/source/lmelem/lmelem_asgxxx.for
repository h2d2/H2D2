C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C     INTEGER LM_ELEM_ASGSIMD
C     INTEGER LM_ELEM_ASGCLIM
C     INTEGER LM_ELEM_ASGDLIB
C     INTEGER LM_ELEM_ASGPRES
C     INTEGER LM_ELEM_ASGPREV
C     INTEGER LM_ELEM_ASGPRGL
C     INTEGER LM_ELEM_ASGPRNO
C     INTEGER LM_ELEM_ASGSOLC
C     INTEGER LM_ELEM_ASGSOLR
C
C************************************************************************

      SUBMODULE(LM_ELEM_M) LM_ELEM_2GDTA_M

      IMPLICIT NONE
      
      CONTAINS
      
C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée LM_ELEM_ASGSIMD assigne à l'objet HOBJ les données
C     de simulation.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     LM_GDTA doit être à jour
C************************************************************************
      MODULE INTEGER FUNCTION LM_ELEM_CP2EDTA(SELF)

      USE LM_GDTA_M, ONLY: LM_GDTA_T
      
      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Fait les assignations
      IERR = LM_ELEM_ASGDLIB(SELF)
      IERR = LM_ELEM_ASGCLIM(SELF)
      IERR = LM_ELEM_ASGSOLC(SELF)
      IERR = LM_ELEM_ASGSOLR(SELF)
      IERR = LM_ELEM_ASGPRGL(SELF)
      IERR = LM_ELEM_ASGPRNO(SELF)
      IERR = LM_ELEM_ASGPREV(SELF)
      IERR = LM_ELEM_ASGPRES(SELF)

      LM_ELEM_CP2EDTA = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_CP2EDTA

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire LM_ELEM_ASGCLIM
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGCLIM(SELF)

      USE HS_CLIM_M, ONLY: HS_CLIM_T
      
      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_CLIM_T), POINTER :: OCLIM
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OCLIM => SELF%OCLIM

C---     Assigne
      EDTA%NCLCND =  OCLIM%REQNCLCND()
      EDTA%NCLCNV =  OCLIM%REQNCLCNV()
      EDTA%KCLCND => OCLIM%PCLCND%KPTR
      EDTA%VCLCNV => OCLIM%PCLCNV%VPTR
      EDTA%KDIMP  => OCLIM%PDIMP%KPTR
      EDTA%VDIMP  => OCLIM%PDIMV%VPTR
      EDTA%KEIMP  => OCLIM%PEIMP%KPTR

      LM_ELEM_ASGCLIM = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGCLIM

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction utilitaire LM_ELEM_ASGDLIB
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGDLIB(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_DLIB_T), POINTER :: ODLIB
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      ODLIB => SELF%ODLIB

C---     Assigne
      EDTA%NDLN =  ODLIB%REQNDLN()
      EDTA%NDLL =  ODLIB%REQNDLL()
      EDTA%NEQL =  ODLIB%REQNEQL()
      EDTA%NEQP =  ODLIB%REQNEQP()
      EDTA%VDLG => ODLIB%PDLG%VPTR
      EDTA%KLOCN=> ODLIB%PLOCN%KPTR

      LM_ELEM_ASGDLIB = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGDLIB

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGPRES(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_PRES_T), POINTER :: OPRES
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OPRES => SELF%OPRES

C---     Assigne
      EDTA%NPRES =  OPRES%REQNPREL()
      EDTA%VPRES => OPRES%PPRES%VPTR

      LM_ELEM_ASGPRES = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGPRES

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGPREV(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_PREV_T), POINTER :: OPREV
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OPREV => SELF%OPREV
      
C---     Assigne
      EDTA%NPREV  = OPREV%REQNPREL ()
      EDTA%NPREVL = OPREV%REQNPRELL()
      EDTA%VPREV => OPREV%PPREV%VPTR

      LM_ELEM_ASGPREV = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGPREV

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGPRGL(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_PRGL_T), POINTER :: OPRGL
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OPRGL => SELF%OPRGL
      
C---     Assigne
      EDTA%NPRGL =  OPRGL%REQNPRGL ()
      EDTA%NPRGLL=  OPRGL%REQNPRGLL()
      EDTA%VPRGL => OPRGL%PPRGL%VPTR

      LM_ELEM_ASGPRGL = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGPRGL

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGPRNO(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_PRNO_T), POINTER :: OPRNO
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OPRNO => SELF%OPRNO
      
C---     Assigne
      EDTA%NPRNO =  OPRNO%REQNPRNO ()
      EDTA%NPRNOL=  OPRNO%REQNPRNOL()
      EDTA%VPRNO => OPRNO%PPRNO%VPTR

      LM_ELEM_ASGPRNO = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGPRNO

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGSOLC(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_SOLC_T), POINTER :: OSOLC
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OSOLC => SELF%OSOLC
      
C---     Assigne
      EDTA%NSOLC =  OSOLC%REQNSOLC ()
      EDTA%NSOLCL=  OSOLC%REQNSOLCL()
      EDTA%VSOLC => OSOLC%PSOLC%VPTR

      LM_ELEM_ASGSOLC = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGSOLC

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION LM_ELEM_ASGSOLR(SELF)

      CLASS(LM_ELEM_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      TYPE(LM_EDTA_T), POINTER :: EDTA
      TYPE(HS_SOLR_T), POINTER :: OSOLR
C-----------------------------------------------------------------------

C---     Récupère les données
      EDTA  => SELF%EDTA
      OSOLR => SELF%OSOLR
      
C---     Assigne
      EDTA%NSOLR =  OSOLR%REQNSOLR ()
      EDTA%NSOLRL=  OSOLR%REQNSOLRL()
      EDTA%VSOLR => OSOLR%PSOLR%VPTR

      LM_ELEM_ASGSOLR = ERR_TYP()
      RETURN
      END FUNCTION LM_ELEM_ASGSOLR

      END SUBMODULE LM_ELEM_2GDTA_M
      