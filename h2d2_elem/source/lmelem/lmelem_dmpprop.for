C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_DMPPROP
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Imprime les propriétés globales
C
C Description:
C     La fonction LM_ELEM_DMPPROP dump la configuration des propriétés
C     de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_ELEM_DMPPROP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_DMPPROP
CDEC$ ENDIF

      USE SO_FUNC_M
      USE LM_ELEM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'lmelem.fc'

      INTEGER IERR
      INTEGER HMETH
      TYPE (LM_ELEM_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => LM_ELEM_REQSELF(HOBJ)

!!!C---     Transfert les valeurs
!!!      IERR = LM_ELEM_ASGSIMD(HOBJ)

C---     La méthode
      HMETH = 0
      IF (ERR_GOOD()) IERR = OB_VTBL_REQMTH (SELF%HVTBL,
     &                                       LM_VTBL_FNC_DMPPROP,
     &                                       HMETH)

C---     Fait l'appel de méthode
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL0 (HMETH)

      LM_ELEM_DMPPROP = ERR_TYP()
      RETURN
      END
