C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: HS
C      H2D2 Class: HS_DLIB
C         FTN Module: HS_DLIB_M
C            Public:
C            Private:
C               SUBROUTINE HS_DLIB_CTR
C               INTEGER HS_DLIB_CNTNEQP
C               LOGICAL HS_DLIB_ESTINIT
C            
C            FTN Type: HS_DLIB_T
C               Public:
C                  INTEGER HS_DLIB_DTR
C                  INTEGER HS_DLIB_INI
C                  INTEGER HS_DLIB_RST
C                  INTEGER HS_DLIB_MAJNEQ
C                  INTEGER HS_DLIB_PASDEB
C                  INTEGER HS_DLIB_PASMAJ
C                  INTEGER HS_DLIB_PASFIN
C                  INTEGER HS_DLIB_SAUVE
C                  INTEGER HS_DLIB_PUSHLDLG
C                  INTEGER HS_DLIB_POPLDLG
C                  INTEGER HS_DLIB_REQHVNOD
C                  INTEGER HS_DLIB_REQNEQL
C                  INTEGER HS_DLIB_REQNEQP
C                  INTEGER HS_DLIB_REQNDLN
C                  INTEGER HS_DLIB_REQNDLL
C                  INTEGER HS_DLIB_REQNDLP
C                  INTEGER HS_DLIB_REQNDLT
C                  INTEGER HS_DLIB_REQLDLG
C                  INTEGER HS_DLIB_REQLLOCN
C               Private:
C                  INTEGER HS_DLIB_RAZ
C                  INTEGER HS_DLIB_INIDDL
C                  INTEGER HS_DLIB_INIFIC
C                  INTEGER HS_DLIB_INIVAL
C
C************************************************************************

      MODULE HS_DLIB_M

      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T,
     &                     SO_ALLC_KPTR2_T,
     &                     SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC

      TYPE, EXTENDS(HS_BASE_T) :: HS_DLIB_T
         TYPE(SO_ALLC_VPTR2_T) :: PDLG
         TYPE(SO_ALLC_VPTR2_T) :: PPUSH
         TYPE(SO_ALLC_KPTR2_T) :: PLOCN
         INTEGER, PRIVATE :: HVNOD
         INTEGER, PRIVATE :: NEQL
         INTEGER, PRIVATE :: NEQP
         INTEGER, PRIVATE :: NDLN
         LOGICAL, PRIVATE :: INIT
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => HS_DLIB_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_DLIB_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_DLIB_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_DLIB_PASFIN

         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_DLIB_INI
         PROCEDURE, PUBLIC :: RST      => HS_DLIB_RST
         PROCEDURE, PUBLIC :: MAJNEQ   => HS_DLIB_MAJNEQ
         PROCEDURE, PUBLIC :: INIFIC   => HS_DLIB_INIFIC
         PROCEDURE, PUBLIC :: INIVAL   => HS_DLIB_INIVAL
         PROCEDURE, PUBLIC :: SAUVE    => HS_DLIB_SAUVE
         PROCEDURE, PUBLIC :: PUSHLDLG => HS_DLIB_PUSHLDLG
         PROCEDURE, PUBLIC :: POPLDLG  => HS_DLIB_POPLDLG

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQHVNOD => HS_DLIB_REQHVNOD
         PROCEDURE, PUBLIC :: REQNEQL  => HS_DLIB_REQNEQL
         PROCEDURE, PUBLIC :: REQNEQP  => HS_DLIB_REQNEQP
         PROCEDURE, PUBLIC :: REQNDLN  => HS_DLIB_REQNDLN
         PROCEDURE, PUBLIC :: REQNDLL  => HS_DLIB_REQNDLL
         PROCEDURE, PUBLIC :: REQNDLP  => HS_DLIB_REQNDLP
         PROCEDURE, PUBLIC :: REQNDLT  => HS_DLIB_REQNDLT
         PROCEDURE, PUBLIC :: REQLDLG  => HS_DLIB_REQLDLG
         PROCEDURE, PUBLIC :: REQLLOCN => HS_DLIB_REQLLOCN

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_DLIB_RAZ
         PROCEDURE, PRIVATE :: INIDDL  => HS_DLIB_INIDDL
      END TYPE HS_DLIB_T

      ! ---  Constructor
      PUBLIC :: HS_DLIB_CTR

      CONTAINS

C************************************************************************
C Sommaire:  HS_DLIB_CTR
C
C Description:
C     Le constructeur <code>HS_DLIB_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_DLIB_CTR() RESULT(SELF)

      TYPE(HS_DLIB_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure
      IERR = SELF%RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_DLIB_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_DLIB_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_DTR(SELF)

      CLASS(HS_DLIB_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET
      CLASS(HS_DLIB_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_DLIB_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     KDIM     Table des dimensions
C     HGRID    Handle sur le maillage
C     HVNOD    Handle sur les données
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_INI(SELF, EDTA, OGRID, HVNOD)

      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2,
     &                     SO_ALLC_VPTR2

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HVNOD

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER NNL, NDLN
      INTEGER LDLG, LLOCN, LVINI
      LOGICAL DOINIT
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. DT_VNOD_HVALIDE(HVNOD)) GOTO 9900

C---     Reset les données
      IERR = HS_DLIB_RST(SELF)

C---     Récupère les dimensions
      NDLN = EDTA%NDLN
      NNL  = OGRID%REQNNL()

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PDLG  = SO_ALLC_VPTR2(NNL, NDLN)
      IF (ERR_GOOD()) SELF%PLOCN = SO_ALLC_KPTR2(NNL, NDLN)

C---     Assigne les valeurs
      IERR = SELF%RAZ()
      IF (ERR_GOOD()) THEN
         SELF%TEMPS = -1.0D0
         SELF%OGRID => OGRID
         SELF%HVNOD = HVNOD
         SELF%NEQL  = 0
         SELF%NEQP  = 0
         SELF%NDLN  = NDLN
         SELF%INIT  = .FALSE.
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HVNOD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_DLIB_INI = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2,
     &                     SO_ALLC_VPTR2

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS = -1.0D0
      SELF%OGRID => NULL()
      SELF%HVNOD = 0
      SELF%NEQL  = 0
      SELF%NEQP  = 0
      SELF%NDLN  = 0
      SELF%PDLG  = SO_ALLC_VPTR2()
      SELF%PLOCN = SO_ALLC_KPTR2()
      SELF%INIT  = .FALSE.

      HS_DLIB_RAZ = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_RAZ

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      INTEGER FUNCTION HS_DLIB_RST(SELF)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_DLIB_RST = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_RST

C************************************************************************
C Sommaire: HS_DLIB_INIDDL
C
C Description:
C     La fonction HS_DLIB_INIDDL initialise les données de l'objet
C     au temps TEMPS.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_INIDDL(SELF, TEMPS)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TEMPS

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'

      REAL*8  TTMP
      INTEGER IERR
      INTEGER HVNOD, HNUMR
      LOGICAL MODIF
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle des paramètres
      IF (TEMPS .LT. 0.0D0) GOTO 9900

C---     Récupère les attributs
      HVNOD = SELF%HVNOD
D     CALL ERR_ASR(SELF%PDLG%ISVALID())
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))
D     CALL ERR_ASR(DT_VNOD_REQNVNO(HVNOD) .EQ. SELF%NDLN)
D     CALL ERR_ASR(DT_VNOD_REQNNT (HVNOD) .EQ. SELF%REQNNT())

C---     Charge les données
      HNUMR = SELF%OGRID%REQHNUMC()
      IERR = DT_VNOD_CHARGE(HVNOD, HNUMR, TEMPS, MODIF)

C---     Copie à partir de DT_VNOD
      IF (ERR_GOOD()) THEN
         TTMP = TEMPS
         IERR = DT_VNOD_REQVALS(HVNOD,
     &                          HNUMR,
     &                          SELF%PDLG%VPTR,
     &                          TTMP)     ! Temps tout juste chargé
      ENDIF

C---     Actualise les attributs
      IF (ERR_GOOD()) THEN
         SELF%TEMPS = TEMPS
         SELF%INIT  = .TRUE.
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,1PE14.6E3)') 'ERR_TEMPS_NEGATIF',':',TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_DLIB_INIDDL = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_INIDDL

C************************************************************************
C Sommaire: HS_DLIB_INIFIC
C
C Description:
C     La fonction HS_DLIB_INIFIC initialise les données de l'objet
C     à partir du fichier dont le nom est passé en paramètre.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_INIFIC(SELF, TEMPS, FILNAM, ISTAT)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      REAL*8,           INTENT(INOUT) :: TEMPS
      CHARACTER*(*),    INTENT(IN)    :: FILNAM
      INTEGER,          INTENT(IN)    :: ISTAT

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HVNOD, HNUMR
      INTEGER NDLN, NNL, NNT
      LOGICAL MODIF
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle des paramètres
      IF (TEMPS .LT. 0) GOTO 9900

C---     Récupère les attributs
      NNT      = SELF%REQNNT ()
      NDLN     = SELF%REQNDLN()
      HVNOD    = SELF%HVNOD
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))
D     CALL ERR_ASR(SELF%PDLG%ISVALID())

C---     Initialise les données
      IF (ERR_GOOD()) IERR = DT_VNOD_INIFIC(HVNOD,
     &                                      0,        ! HLIST
     &                                      FILNAM,
     &                                      ISTAT)

C---     Contrôle les dimensions
      IF (ERR_GOOD()) THEN
         IF (DT_VNOD_REQNVNO(HVNOD) .NE. NDLN) GOTO 9901
         IF (DT_VNOD_REQNNL (HVNOD) .NE. NNL)  GOTO 9902
         IF (DT_VNOD_REQNNT (HVNOD) .NE. NNT)  GOTO 9902
      ENDIF

C---     Charge les données
      IF (ERR_GOOD()) IERR = SELF%INIDDL(TEMPS)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,1PE14.6E3)') 'ERR_TEMPS_NEGATIF',':',TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_NDLN_INCOMPATIBLES',':',
     &       DT_VNOD_REQNVNO(HVNOD), '/', SELF%NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I9,A,I9)')'ERR_NNT_INCOMPATIBLES',':',
     &       DT_VNOD_REQNNT(HVNOD), '/', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_DLIB_INIFIC = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_INIFIC

C************************************************************************
C Sommaire: HS_DLIB_INIVAL
C
C Description:
C     La fonction HS_DLIB_INIVAL initialise les données de l'objet
C     à partir des valeurs passées en paramètre.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_INIVAL(SELF, TEMPS, VVAL)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(INOUT) :: TEMPS
      REAL*8, INTENT(IN)    :: VVAL(:)

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HVNOD
      INTEGER NNL, NNT, NDLN
      LOGICAL DOINIT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SIZE(VVAL, 1) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle des paramètres
      IF (TEMPS .LT. 0) GOTO 9900

C---     Récupère les attributs
      HVNOD = SELF%HVNOD
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))

C---     Récupère les données
      NNL  = SELF%REQNNL ()
      NNT  = SELF%REQNNT ()
      NDLN = SELF%REQNDLN()

C---     Contrôle les dimensions
      IF (NDLN .NE. SIZE(VVAL,1)) GOTO 9901

C---     Initialise les données
      IF (ERR_GOOD()) THEN
         DOINIT = .TRUE.
         IERR = DT_VNOD_INIDIM(HVNOD, NDLN, NNL, NNT, DOINIT, VVAL)
      ENDIF

C---     Charge les données
      IF (ERR_GOOD()) IERR = SELF%INIDDL(TEMPS)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,1PE14.6E3)') 'ERR_TEMPS_NEGATIF',':',TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_NDLN_INCOMPATIBLES',':',
     &       NDLN, '/', SIZE(VVAL, 1)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_DLIB_INIVAL = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_INIVAL

C************************************************************************
C Sommaire: HS_DLIB_MAJNEQ
C
C Description:
C     La fonction HS_DLIB_MAJNEQ met à jour le nombre d'équations. Cette
C     fonction doit être appelée suite à un changement à la table LLOCN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_MAJNEQ(SELF)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER HNUMR
      INTEGER NEQL, NEQP
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.data.dof'

C---     Compte NEQL
      NEQL = COUNT(SELF%PLOCN%KPTR .NE. 0)

C---     Compte NEQP
      HNUMR = SELF%OGRID%REQHNUMC()
      NEQP = HS_DLIB_CNTNEQP(HNUMR,
     &                       SELF%REQNDLN(),
     &                       SELF%REQNNL (),
     &                       SELF%PLOCN%KPTR)

C---     LOG
      WRITE(LOG_BUF, '(2A,I12)') 'MSG_NEQL', ': ', NEQL
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,I12)') 'MSG_NEQP', ': ', NEQP
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     Conserve la valeur
      IF (ERR_GOOD()) SELF%NEQL = NEQL
      IF (ERR_GOOD()) SELF%NEQP = NEQP

      HS_DLIB_MAJNEQ = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_MAJNEQ

C************************************************************************
C Sommaire: HS_DLIB_CNTNEQP
C
C Description:
C     La fonction privée HS_DLIB_CNTNEQP compte le nombre d'équations
C     privées au process.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_CNTNEQP(HNUMR, NDLN, NNL, KLOCN)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      INTEGER KLOCN(NDLN, NNL)

      INCLUDE 'nmnumr.fi'

      INTEGER IN, ID
      INTEGER NEQ
C-----------------------------------------------------------------------
C     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

      NEQ = 0
      DO IN=1, NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DO ID=1, NDLN
               IF (KLOCN(ID, IN) .NE. 0) NEQ = NEQ + 1
            ENDDO
         ENDIF
      ENDDO

      HS_DLIB_CNTNEQP = NEQ
      RETURN
      END FUNCTION HS_DLIB_CNTNEQP

C************************************************************************
C Sommaire: HS_DLIB_PASDEB
C
C Description:
C     La méthode HS_DLIB_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_PASDEB(SELF, TNEW)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      REAL*8  TDLL
      LOGICAL MODIF
C-----------------------------------------------------------------------

      IERR = ERR_OK

      LOG_ZNE = 'h2d2.data.dof'

C---     Initialise au besoin
      IF (.NOT. SELF%INIT) THEN
         IERR = SELF%INIDDL(TNEW)
      ELSE
         TDLL =SELF%TEMPS
         IF (TDLL .NE. TNEW) THEN
            WRITE(LOG_BUF,'(A)') 'WRN_TEMPS_DIVERGENTS'
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
            WRITE(LOG_BUF,'(2A,1PE25.17E3)')
     &         'MSG_DLIB_TEMPS_ACTU', ': ', TDLL
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
            WRITE(LOG_BUF,'(2A,1PE25.17E3)')
     &         'MSG_DLIB_TEMPS_SIMU', ': ', TNEW
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDIF
      ENDIF

C---     Conserve le temps
      SELF%TEMPS = TNEW

9999  CONTINUE
      HS_DLIB_PASDEB = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_PASDEB

C************************************************************************
C Sommaire: HS_DLIB_PASMAJ
C
C Description:
C     La méthode HS_DLIB_PASMAJ fait la mise à jour des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_PASMAJ(SELF)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_DLIB_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_PASMAJ

C************************************************************************
C Sommaire: HS_DLIB_PASFIN
C
C Description:
C     La méthode HS_DLIB_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN. Elle met à jour les données de l'objet et les données
C     associées.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER HNUMR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les données
      HNUMR = SELF%OGRID%REQHNUMC()

C---     Met à jour le DT_VNOD associé
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(SELF%HVNOD,
     &                          HNUMR,
     &                          SELF%PDLG%VPTR,
     &                          TFIN)
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TFIN

      HS_DLIB_PASFIN = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_PASFIN

C************************************************************************
C Sommaire: HS_DLIB_SAUVE
C
C Description:
C     La fonction <code>HS_DLIB_SAUVE(...)</code> sauve les degrés de
C     liberté de l'objet dans le fichier de nom FILNAM.
C     Si ISTAT .EQ. IO_AJOUT les données sont ajoutées au fichier déjà
C     existant.
C     Si ISTAT .EQ. IO_ECRITURE, le contenu du fichier déjà existant est
C     écrasé.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     FILNAM      Nom du fichier de sauvegarde
C     ISTAT       IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_SAUVE (SELF, FILNAM, ISTAT)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
      CHARACTER*(*) FILNAM
      INTEGER       ISTAT

      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HVNOD, HNUMR
      INTEGER NNT
      INTEGER NDLN
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      NNT   = SELF%REQNNT ()
      NDLN  = SELF%REQNDLN()
      HVNOD = SELF%HVNOD
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNOD))

C---     Contrôle les dimensions
      IF (ERR_GOOD() .AND. DT_VNOD_REQNVNO(HVNOD) .NE. NDLN) GOTO 9900
      IF (ERR_GOOD() .AND. DT_VNOD_REQNNT (HVNOD) .NE. NNT)  GOTO 9901

C---     Assigne le fichier d'écriture
      IF (ERR_GOOD())
     &   IERR = DT_VNOD_ASGFICECR(HVNOD,
     &                            FILNAM(1:SP_STRN_LEN(FILNAM)),
     &                            ISTAT,
     &                            0)   !No handle, build with name

C---     Sauve les données
      IF (ERR_GOOD()) HNUMR = SELF%OGRID%REQHNUMC()
      IF (ERR_GOOD()) IERR = DT_VNOD_SAUVE (HVNOD, HNUMR)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3,A,I3)') 'ERR_NDLN_INCOMPATIBLES',':',
     &       DT_VNOD_REQNVNO(HVNOD), '/', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I9,A,I9)')'ERR_NNT_INCOMPATIBLES',':',
     &       DT_VNOD_REQNNT(HVNOD), '/', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_DLIB_SAUVE = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_SAUVE

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      LOGICAL FUNCTION HS_DLIB_ESTINIT(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_ESTINIT = SELF%INIT
      RETURN
      END FUNCTION HS_DLIB_ESTINIT

C************************************************************************
C Sommaire: Assigne une table externe comme VDLG
C
C Description:
C     La fonction HS_DLIB_PUSHLDLG permet d'assigner à l'objet une table
C     externe comme VDLG. L'ancienne table est poussée sur une pile et
C     peut être dépilée par l'appel symétrique à HS_DLIB_POPLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     LDLG     Pointeur à la nouvelle table
C
C Sortie:
C
C Notes:
C     Fonction fragile car on ne peut faire que très peux de validation
C     La pile a une profondeur de 1!!
C************************************************************************
      INTEGER FUNCTION HS_DLIB_PUSHLDLG(SELF, LDLG)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF
      INTEGER LDLG

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SO_ALLC_HEXIST(LDLG))
D     CALL ERR_PRE(SELF%PPUSH%ISVALID())
C------------------------------------------------------------------------

      SELF%PPUSH = SELF%PDLG
      SELF%PDLG  = SO_ALLC_VPTR2(LDLG, SELF%NDLN, SELF%REQNNL())

      HS_DLIB_PUSHLDLG = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_PUSHLDLG

C************************************************************************
C Sommaire: Restaure la table VDLG précédente
C
C Description:
C     La fonction HS_DLIB_POPLDLG restaure à l'objet la table poussée
C     sur la pile par HS_DLIB_PUSHLDLG.
C     Il est de la responsabilité des fonctions appelantes de gérer
C     la mémoire et de laisser l'objet dans un état cohérent.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C     La pile a une profondeur de 1!!
C************************************************************************
      INTEGER FUNCTION HS_DLIB_POPLDLG(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_DLIB_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      SELF%PDLG  = SELF%PPUSH
      SELF%PPUSH = SO_ALLC_VPTR2()

      HS_DLIB_POPLDLG = ERR_TYP()
      RETURN
      END FUNCTION HS_DLIB_POPLDLG

C************************************************************************
C Sommaire: Valeurs nodales associées.
C
C Description:
C     La méthode HS_DLIB_REQHVNOD retourne le handle sur les valeurs
C     nodales.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQHVNOD(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQHVNOD = SELF%HVNOD
      RETURN
      END FUNCTION HS_DLIB_REQHVNOD

C************************************************************************
C Sommaire: Nombre d'équations local au process
C
C Description:
C     La méthode HS_DLIB_REQNEQL retourne le nombre d'équations local au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNEQL(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNEQL = SELF%NEQL
      RETURN
      END FUNCTION HS_DLIB_REQNEQL

C************************************************************************
C Sommaire: Nombre d'équations local au process
C
C Description:
C     La méthode HS_DLIB_REQNEQP retourne le nombre d'équations local au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNEQP(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNEQP = SELF%NEQP
      RETURN
      END FUNCTION HS_DLIB_REQNEQP

C************************************************************************
C Sommaire: Nombre de degrés de liberté par noeud
C
C Description:
C     La méthode HS_DLIB_REQNDLN retourne le nombre de degrés de
C     liberté par noeud.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNDLN(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNDLN = SELF%NDLN
      RETURN
      END FUNCTION HS_DLIB_REQNDLN

C************************************************************************
C Sommaire: Retourne le Nombre de Degré de Liberté Local.
C
C Description:
C     La méthode HS_DLIB_REQNDLL retourne le nombre de degrés de
C     liberté local.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNDLL(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNDLL = SELF%REQNNL() * SELF%REQNDLN()
      RETURN
      END FUNCTION HS_DLIB_REQNDLL

C************************************************************************
C Sommaire: Retourne le Nombre de Degré de Liberté Privés.
C
C Description:
C     La méthode HS_DLIB_REQNDLP retourne le nombre de degrés de
C     liberté local.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNDLP(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNDLP = SELF%REQNNP() * SELF%REQNDLN()
      RETURN
      END FUNCTION HS_DLIB_REQNDLP

C************************************************************************
C Sommaire: Retourne le Nombre de Degré de Liberté Total.
C
C Description:
C     La méthode HS_DLIB_REQNDLT retourne le nombre de degrés de
C     liberté total.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQNDLT(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQNDLT = SELF%REQNNT() * SELF%REQNDLN()
      RETURN
      END FUNCTION HS_DLIB_REQNDLT

C************************************************************************
C Sommaire: Pointeur à la table des degrés de liberté.
C
C Description:
C     La méthode HS_DLIB_REQLLOCN retourne le pointeur à la table des
C     degrés de liberté des noeuds.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQLDLG(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQLDLG = SELF%PDLG%REQHNDL()
      RETURN
      END FUNCTION HS_DLIB_REQLDLG

C************************************************************************
C Sommaire: Pointeur à la table de localisation.
C
C Description:
C     La méthode HS_DLIB_REQLLOCN retourne le pointeur à la table de
C     localisation des noeuds.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_DLIB_REQLLOCN(SELF)

      CLASS(HS_DLIB_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_DLIB_REQLLOCN = SELF%PLOCN%REQHNDL()
      RETURN
      END FUNCTION HS_DLIB_REQLLOCN

      END MODULE HS_DLIB_M

