C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER HS_SOLR_000
C     INTEGER HS_SOLR_999
C     INTEGER HS_SOLR_CTR
C     INTEGER HS_SOLR_DTR
C     INTEGER HS_SOLR_INI
C     INTEGER HS_SOLR_RST
C     INTEGER HS_SOLR_REQHBASE
C     LOGICAL HS_SOLR_HVALIDE
C     INTEGER HS_SOLR_PASDEB
C     INTEGER HS_SOLR_PASFIN
C     INTEGER HS_SOLR_REQHLMAP
C     INTEGER HS_SOLR_REQHGRID
C     INTEGER HS_SOLR_REQHSOLR
C     INTEGER HS_SOLR_REQNNL
C     INTEGER HS_SOLR_REQNSOLR
C     INTEGER HS_SOLR_REQNSOLRL
C     INTEGER HS_SOLR_REQLSOLR
C   Private:
C
C************************************************************************

      MODULE HS_SOLR_M
      
      USE HS_BASE_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC
      
      TYPE, EXTENDS(HS_BASE_T) :: HS_SOLR_T
         TYPE(SO_ALLC_VPTR2_T) :: PSOLR
         INTEGER, PRIVATE :: HSOLR
         INTEGER, PRIVATE :: NSOLR
         INTEGER, PRIVATE :: NSOLRL
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => HS_SOLR_DTR
         PROCEDURE, PUBLIC :: PASDEB   => HS_SOLR_PASDEB
         PROCEDURE, PUBLIC :: PASMAJ   => HS_SOLR_PASMAJ
         PROCEDURE, PUBLIC :: PASFIN   => HS_SOLR_PASFIN
      
         ! ---  Méthodes publiques
         PROCEDURE, PUBLIC :: INI      => HS_SOLR_INI
         PROCEDURE, PUBLIC :: RST      => HS_SOLR_RST
         
         ! ---  Getter        
         PROCEDURE, PUBLIC :: REQHSOLR => HS_SOLR_REQHSOLR
         PROCEDURE, PUBLIC :: REQNSOLR => HS_SOLR_REQNSOLR
         PROCEDURE, PUBLIC :: REQNSOLRL=> HS_SOLR_REQNSOLRL
         PROCEDURE, PUBLIC :: REQLSOLR => HS_SOLR_REQLSOLR
         
         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => HS_SOLR_RAZ
      END TYPE HS_SOLR_T

      ! ---  Constructor
      PUBLIC :: HS_SOLR_CTR
      
      CONTAINS
      
C************************************************************************
C Sommaire:  HS_SOLR_CTR
C
C Description:
C     Le constructeur <code>HS_SOLR_CTR</code> construit
C
C Entrée:
C
C Sortie:
C     SELF     Objet nouvellement alloué
C
C Notes:
C************************************************************************
      FUNCTION HS_SOLR_CTR() RESULT(SELF)

      TYPE(HS_SOLR_T), POINTER :: SELF

      INCLUDE 'err.fi'

      INTEGER IRET, IERR
C-----------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise la structure      
      IERR = SELF%RAZ()
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION HS_SOLR_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur HS_SOLR_DTR
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_DTR(SELF)
      
      CLASS(HS_SOLR_T), INTENT(INOUT), TARGET :: SELF
      
      INCLUDE 'err.fi'
      
      INTEGER IRET
      CLASS(HS_SOLR_T), POINTER :: SELF_P
C-----------------------------------------------------------------------

C---     Désalloue la mémoire
      SELF_P => SELF
      DEALLOCATE (SELF_P, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      HS_SOLR_DTR = ERR_TYP()
      RETURN
      END FUNCTION HS_SOLR_DTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HLMAP       Handle sur l'élément
C     HGRID       Handle sur le maillage (GR_)
C     HSOLR       Handle sur les sollicitations réparties (DT_)
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_INI(SELF, EDTA, OGRID, HSOLR)

      USE LM_EDTA_M, ONLY: LM_EDTA_T

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF
      CLASS(LM_EDTA_T), INTENT(IN)    :: EDTA
      CLASS(GR_GRID_T), INTENT(IN), TARGET :: OGRID
      INTEGER HSOLR

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER NNT, NSOLR, NSOLRL
      INTEGER NLCL
C-----------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (HSOLR .NE. 0 .AND. .NOT. DT_PRNO_HVALIDE(HSOLR)) GOTO 9900

C---     Reset les données
      IERR = SELF%RST()

C---     Recherche les dimensions
      NNT   = OGRID%REQNNT()
      NSOLR = EDTA%NSOLR
      NSOLRL= EDTA%NSOLRL
      IF (NSOLR .LT. NSOLRL) GOTO 9902

C---     Contrôles
      NLCL = NNT
      IF (HSOLR .NE. 0) NLCL = DT_PRNO_REQNNT  (HSOLR)
      IF (NLCL .NE. NNT)  GOTO 9903
      NLCL = NSOLRL
      IF (HSOLR .NE. 0) NLCL = DT_PRNO_REQNPRN(HSOLR)
      IF (NLCL .NE. NSOLRL) GOTO 9904

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%OGRID  => OGRID
         SELF%HSOLR  = HSOLR
         SELF%NSOLR  = NSOLR
         SELF%NSOLRL = NSOLRL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HSOLR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_DIM_SOLR_INVALIDE', ': ',
     &   NSOLR, ' > ', NSOLRL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NBR_NOEUD_INVALIDE', ': ',
     &   NLCL, ' /', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I2,A,I2)') 'ERR_NBR_SOLR_INVALIDE', ': ',
     &   NLCL, ' /', NSOLRL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HS_SOLR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

C---     Reset les paramètres
      SELF%TEMPS = -1.0D0
      SELF%OGRID => NULL()
      SELF%HSOLR = 0
      SELF%NSOLR = 0
      SELF%NSOLR = 0
      SELF%PSOLR = SO_ALLC_VPTR2()

      HS_SOLR_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_RST(SELF)

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
      
      INTEGER :: IERR
C-----------------------------------------------------------------------

C---     Reset les paramètres
      IERR = SELF%RAZ()

      HS_SOLR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_SOLR_PASDEB
C
C Description:
C     La méthode HS_SOLR_PASDEB fait le traitement initial du pas
C     au temps TNEW. Elle charge les données puis les rassemble.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     TNEW        Nouveau temps de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_PASDEB(SELF, TNEW)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'

      REAL*8  TGET
      INTEGER IERR
      INTEGER HSOLR, HNUMR
      INTEGER NNL, NSOLR
      INTEGER, PARAMETER :: NSOLR_MAX = 8
      LOGICAL DOGTHR(NSOLR_MAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      HSOLR = SELF%HSOLR
      NSOLR = SELF%NSOLR

C---     Récupère les données
      NNL   = SELF%REQNNL  ()

C---     Alloue l'espace
      IF (ERR_GOOD()) SELF%PSOLR = SO_ALLC_VPTR2(NSOLR, NNL)

C---     Lis les propriétés nodales
      IF (HSOLR .NE. 0) THEN
         HNUMR = SELF%OGRID%REQHNUMC()
C---        Lis
         IF (ERR_GOOD()) THEN
            IERR = DT_PRNO_CHARGE(HSOLR, HNUMR, TNEW)
         ENDIF
C---        Gather
         IF (ERR_GOOD()) THEN
            DOGTHR(1:NSOLR) = .TRUE.
            IERR = DT_PRNO_GATHER(HSOLR,
     &                            HNUMR,
     &                            NSOLR,
     &                            NNL,
     &                            SELF%PSOLR%VPTR,
     &                            DOGTHR,
     &                            TGET)
         ENDIF
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) SELF%TEMPS = TNEW

      HS_SOLR_PASDEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: HS_SOLR_PASMAJ
C
C Description:
C     La méthode HS_SOLR_PASMAJ fait la mise à jour des données.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_PASMAJ(SELF)

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_SOLR_PASMAJ = ERR_TYP()
      RETURN
      END FUNCTION HS_SOLR_PASMAJ

C************************************************************************
C Sommaire: HS_SOLR_PASFIN
C
C Description:
C     La méthode HS_SOLR_PASFIN fait le traitement final du pas arrivé au
C     temps TFIN.
C
C Entrée:
C
C Sortie:
C
C Notes:
C   Pas de modification des SOLR possible
C************************************************************************
      INTEGER FUNCTION HS_SOLR_PASFIN(SELF, TFIN, MODIF)

      CLASS(HS_SOLR_T), INTENT(INOUT) :: SELF
      REAL*8, INTENT(IN) :: TFIN
      LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      HS_SOLR_PASFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Sollicitations réparties associées.
C
C Description:
C     La méthode HS_SOLR_REQHSOLR retourne le handle sur les sollicitations
C     réparties.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_REQHSOLR(SELF)

      CLASS(HS_SOLR_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLR_REQHSOLR = SELF%HSOLR
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sollicitations.
C
C Description:
C     La fonction HS_SOLR_REQNSOLR retourne le nombre de sollicitations de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_REQNSOLR(SELF)

      CLASS(HS_SOLR_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLR_REQNSOLR = SELF%NSOLR
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sollicitations lues.
C
C Description:
C     La fonction HS_SOLR_REQNSOLRL retourne le nombre de sollicitations lues de
C     l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_REQNSOLRL(SELF)

      CLASS(HS_SOLR_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLR_REQNSOLRL = SELF%NSOLRL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction HS_SOLR_REQLSOLR retourne le pointeur à la table des
C     sollicitations maintenues par l'objet. La table est de dimensions
C     (NDLN, NNL). Le pointeur peut être nul.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_SOLR_REQLSOLR(SELF)

      CLASS(HS_SOLR_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_SOLR_REQLSOLR = SELF%PSOLR%REQHNDL()
      RETURN
      END

      END MODULE HS_SOLR_M
