C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE HS_BASE_M

      USE GR_GRID_M, ONLY: GR_GRID_T
      IMPLICIT NONE
      PUBLIC

      TYPE, ABSTRACT :: HS_BASE_T
         CLASS(GR_GRID_T), POINTER :: OGRID
         REAL*8 :: TEMPS
      CONTAINS
         ! ---  Méthodes virtuelles publiques
         PROCEDURE(DTR_GEN),    PUBLIC, DEFERRED :: DTR
         PROCEDURE(PASDEB_GEN), PUBLIC, DEFERRED :: PASDEB
         PROCEDURE(PASMAJ_GEN), PUBLIC, DEFERRED :: PASMAJ
         PROCEDURE(PASFIN_GEN), PUBLIC, DEFERRED :: PASFIN

         ! ---  Getter
         PROCEDURE, PUBLIC :: REQNNL   => HS_BASE_REQNNL
         PROCEDURE, PUBLIC :: REQNNP   => HS_BASE_REQNNP
         PROCEDURE, PUBLIC :: REQNNT   => HS_BASE_REQNNT
         PROCEDURE, PUBLIC :: REQTEMPS => HS_BASE_REQTEMPS
      END TYPE HS_BASE_T

      ! ---  Destructor
      INTERFACE DEL
         MODULE PROCEDURE HS_BASE_DEL
      END INTERFACE DEL
      
      ! ---  Abstract interface
      ABSTRACT INTERFACE
         INTEGER FUNCTION DTR_GEN(SELF)
            IMPORT :: HS_BASE_T
            CLASS(HS_BASE_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION DTR_GEN

         INTEGER FUNCTION PASDEB_GEN(SELF, TNEW)
            IMPORT :: HS_BASE_T
            CLASS(HS_BASE_T), INTENT(INOUT) :: SELF
            REAL*8, INTENT(IN) :: TNEW
         END FUNCTION PASDEB_GEN

         INTEGER FUNCTION PASMAJ_GEN(SELF)
            IMPORT :: HS_BASE_T
            CLASS(HS_BASE_T), INTENT(INOUT) :: SELF
         END FUNCTION PASMAJ_GEN

         INTEGER FUNCTION PASFIN_GEN(SELF, TFIN, MODIF)
            IMPORT :: HS_BASE_T
            CLASS(HS_BASE_T), INTENT(INOUT) :: SELF
            REAL*8,  INTENT(IN) :: TFIN
            LOGICAL, INTENT(IN), OPTIONAL :: MODIF(:)
         END FUNCTION PASFIN_GEN

      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_BASE_DEL(SELF)

      CLASS(HS_BASE_T), INTENT(INOUT) :: SELF
C-----------------------------------------------------------------------

      HS_BASE_DEL = SELF%DTR()
      RETURN
      END FUNCTION HS_BASE_DEL

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_BASE_REQNNL(SELF)

      CLASS(HS_BASE_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_BASE_REQNNL = SELF%OGRID%REQNNL()
      RETURN
      END FUNCTION HS_BASE_REQNNL

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_BASE_REQNNP(SELF)

      CLASS(HS_BASE_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_BASE_REQNNP = SELF%OGRID%REQNNP()
      RETURN
      END FUNCTION HS_BASE_REQNNP

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_BASE_REQNNT(SELF)

      CLASS(HS_BASE_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_BASE_REQNNT = SELF%OGRID%REQNNT()
      RETURN
      END FUNCTION HS_BASE_REQNNT

C************************************************************************
C Sommaire: Temps.
C
C Description:
C     La méthode HS_PRES_REQTEMPS retourne le temps des données.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION HS_BASE_REQTEMPS(SELF)

      CLASS(HS_BASE_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      HS_BASE_REQTEMPS = SELF%TEMPS
      RETURN
      END FUNCTION HS_BASE_REQTEMPS

      END MODULE HS_BASE_M
