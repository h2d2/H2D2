C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LM_ELEM_PRNPRGL
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRC   PRE-CALCUL
C           CLC   CALCULE
C           PSL   POST-LECTURE
C           PST   POST-TRAITEMENT
C           PRN   PRINT
C
C************************************************************************

C************************************************************************
C Sommaire: Imprime les propriétés globales
C
C Description:
C     La fonction LM_ELEM_PRINTPRGL imprime les propriétés globales
C     de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La version stricte impose que l'élément soit initialisé ce qui
C     est un peu abusif pour les PRGL et qui n'est pas le comportement
C     'historique' de H2D2
C************************************************************************
      FUNCTION LM_ELEM_PRNPRGL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_ELEM_PRNPRGL
CDEC$ ENDIF

      USE LM_ELEM_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'lmelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hsprgl.fi'
      INCLUDE 'lmfcod.fi'
      INCLUDE 'obvtbl.fi'
      INCLUDE 'lmelem.fc'

      REAL*8  TSIM
      INTEGER IERR
      INTEGER HMETH
      TYPE (LM_ELEM_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      SELF => LM_ELEM_REQSELF(HOBJ)

!!!      IF (.NOT. LM_ELEM_ESTINI(HOBJ)) GOTO 9900    !! c.f. Note
      IF (.NOT. LM_ELEM_ESTINI(HOBJ)) THEN
         TSIM = 0.0D0
         IERR = HS_PRGL_PASDEB(SELF%HPRGL, TSIM)
      ENDIF

C---     Traitement post-lecture des données lues
      IF (ERR_GOOD()) IERR = HS_PRGL_PASMAJ(SELF%HPRGL)
      IF (ERR_GOOD()) IERR = LM_ELEM_PSLPRGL(HOBJ)    !! c.f. note

C---     Transfert les valeurs
      IF (ERR_GOOD()) IERR = LM_ELEM_ASGSIMD(HOBJ)

C---     La méthode
      HMETH = 0
      IF (ERR_GOOD()) IERR = OB_VTBL_REQMTH (SELF%HVTBL,
     &                                       LM_VTBL_FNC_PRNPRGL,
     &                                       HMETH)

C---     Fait l'appel de méthode
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL0 (HMETH)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_SIMD_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LM_ELEM_PRNPRGL = ERR_TYP()
      RETURN
      END
