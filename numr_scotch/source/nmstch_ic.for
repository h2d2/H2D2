C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_NM_STCH_XEQCTR
C     INTEGER IC_NM_STCH_XEQMTH
C     CHARACTER*(32) IC_NM_STCH_REQCLS
C     INTEGER IC_NM_STCH_REQHDL
C   Private:
C     SUBROUTINE IC_NM_STCH_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_STCH_XEQCTR construit un objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_STCH_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_STCH_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'nmstch_ic.fi'
      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_NM_STCH_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.scotch'

C---     En-tête de commande
      CALL LOG_INFO(LOG_ZNE, ' ')
      CALL LOG_INFO(LOG_ZNE, 'MSG_PTSCOTCH')
      CALL LOG_INCIND()

C---     Construis, initialise et charge l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = NM_STCH_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = NM_STCH_INI(HOBJ)

C---     Impression des paramètres de l'objet
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
         LTXT = SP_STRN_LEN(TXT)
         WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ': ', TXT(1:LTXT)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>scotch</b> constructs an object and returns a handle on this
C  object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NM_STCH_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_NM_STCH_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_STCH_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_STCH_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'nmstch_ic.fi'
      INCLUDE 'nmstch.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HELEM, HNUM
      INTEGER NPROC
      CHARACTER*(256) NOMFIC
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     METHODES

C     <comment>
C     The method <b>part</b> partitions the finite-element mesh in
C     sub-meshes, suitable for distributed computing. The sub-meshes
C     are also automatically renumbered.
C     <br>
C     Only one of <b>part</b> or <b>renum</b> shall be used on an object.
C     </comment>
      IF (IMTH .EQ. 'part') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Handle on the grid connectivities</comment>
         IERR = SP_STRN_TKI(IPRM, ',', 1, HELEM)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Number of domains (default = number of process)</comment>
         IERR = SP_STRN_TKI(IPRM, ',', 2, NPROC)
         IF (IERR .NE. 0) NPROC = -1
         IERR = NM_STCH_PART(HOBJ, HELEM, NPROC)

C     <comment>
C     The method <b>renum</b> renumbers the nodes of the finite element grid.
C     <br>
C     Only one of <b>part</b> or <b>renum</b> shall be used on an object.
C     </comment>
      ELSEIF (IMTH .EQ. 'renum') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>Handle on the grid connectivities</comment>
         IERR = SP_STRN_TKI(IPRM, ',', 1, HELEM)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NM_STCH_RENUM(HOBJ, HELEM)

C     <comment>The method <b>gen_num</b> generates a global renumbering object.</comment>
      ELSEIF (IMTH .EQ. 'gen_num') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         HNUM = 0
         IERR = NM_STCH_GENNUM(HOBJ, HNUM)
C        <comment>Handle on new renumbering.</comment>
         IF (ERR_GOOD())  WRITE(IPRM, '(2A,I12)') 'H', ',', HNUM

C     <comment>The method <b>save</b> saves the redistribution table to the specified file.</comment>
      ELSEIF (IMTH .EQ. 'save') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C        <comment>File name</comment>
         IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NM_STCH_SAUVE(HOBJ, NOMFIC)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = NM_STCH_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(NM_STCH_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = NM_STCH_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test NM_STCH_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_NM_STCH_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NM_STCH_AID()

9999  CONTINUE
      IC_NM_STCH_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_STCH_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_STCH_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmstch_ic.fi'
C-------------------------------------------------------------------------

C     <comment>
C     The class <b>scotch</b> represents the PT-Scotch algorithm to partition or
C     renumber a finite-element mesh. PT-Scotch is a multi-level algorithm
C     used to partition graphs in parallel on many processors.
C     The algorithme will build a redistribution table that can be saved to file
C     or used to generate a renumbering to read a mesh.
C     </comment>
      IC_NM_STCH_REQCLS = 'scotch'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_STCH_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_STCH_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmstch_ic.fi'
      INCLUDE 'nmstch.fi'
C-------------------------------------------------------------------------

      IC_NM_STCH_REQHDL = NM_STCH_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_STCH_AID écris dans le log l'aide relative
C     à la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NM_STCH_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('nmstch_ic.hlp')

      RETURN
      END

