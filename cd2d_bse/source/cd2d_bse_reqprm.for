C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulérienne 2-D
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C     Subroutines de base.
C************************************************************************

C************************************************************************
C Sommaire: Retourne les paramètres de l'élément.
C
C Description:
C     La fonction <code>CD2D_BSE_REQPRM</code> retourne tous les
C     paramètres caractéristiques de l'élément.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_REQPRM(TGELV,
     &                           NPRGL,
     &                           NPRGLL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NSOLC,
     &                           NSOLCL,
     &                           NSOLR,
     &                           NSOLRL,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           ASURFACE,
     &                           ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_REQPRM
CDEC$ ENDIF

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES

      LOGICAL ASURFACE
      LOGICAL ESTLIN

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtpgeo.fi'

      INCLUDE 'cd2d_bse.fi'
C-----------------------------------------------------------------------

C---     INITIALISATION DES PARAMETRES INVARIANT DE L'ÉLÉMENT PARENT VIRTUEL
      TGELV  =  EG_TPGEO_T3           ! TYPE DE GEOMETRIE DE L'ELEMENT DE VOLUME

C---     INITIALISATION DES PARAMETRES VARIABLES SUJETS À CHANGEMENT CHEZ L'ÉLÉMENT HÉRITIER
      NPRGLL =  CD2D_BSE_NPRGLL       ! NB DE PROPRIÉTÉS GLOBALES LUES
      NPRGL  =  CD2D_BSE_NPRGL        ! NB DE PROPRIÉTÉS GLOBALES
      NPRNOL =  5                     ! NB DE PROPRIÉTÉS NODALES LUES
      NPRNO  =  NPRNOL + 5            ! NB DE PROPRIÉTÉS NODALES
      NPREVL =  0                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME LUES
      NPREV  =  NPREVL + 6            ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE VOLUME
      NPRES  =  8                     ! NB DE PROPRIÉTÉS ÉLÉMENTAIRES DE SURFACE
      NSOLCL =  1                     ! NB DE SOLLICITATIONS CONCENTRÉES LUES
      NSOLC  =  NSOLCL                ! NB DE SOLLICITATIONS CONCENTRÉES
      NSOLRL =  1                     ! NB DE SOLLICITATIONS RÉPARTIES LUES
      NSOLR  =  NSOLRL                ! NB DE SOLLICITATIONS RÉPARTIES
      NDLN   =  0                     ! NB DE DDL PAR NOEUD
      NDLEV  =  0                     ! NB DE DDL PAR ELEMENT DE VOLUME
      NDLES  =  0                     ! NB DE DDL PAR ELEMENT DE SURFACE

      ASURFACE = .TRUE.
      ESTLIN   = .TRUE.

      RETURN
      END

