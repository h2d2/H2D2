C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE CD2D_BSE_HLPPRGL
C   Private:
C     INTEGER CD2D_BSE_HLP1
C
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSE_HLPPRGL
C
C Description:
C     Aide sur les propriétés globales
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                POUR  (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C     MÉTHODE DE GALERKIN STANDARD
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_HLPPRGL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_HLPPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

C---     Imprime entête
      CALL LOG_ECRIS(' ')
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_CD2D:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Groupe des propriétés physiques
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_PHYSIQUES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DIFFU_MOLECULAIRE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DIFFU_CMULT_VERT')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DIFFU_CMULT_HORIZ')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DIFFU_CMULT_LONG')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DEFLT_CLIM')

C---     Groupe des propriétés du découvrement
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_DECOUVREMENT:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_HTRIG')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_HMIN')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_CONV')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_CLIM')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_VDIMP')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_POROSITE')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_DIFFU_HORIZ')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_DECOU_CMULT_LONG')

C---     Groupe des propriétés de stabilisation
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_STABILISATION:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_PECLET')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_LAPIDUS')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_DMPG_VMIN')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_DMPG_VMAX')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_DMPG_STFF')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_STABI_DMPG_DLIN')

C---     Groupe des prop d'activation-désactivation des termes
      WRITE (LOG_BUF, '(A)') 'MSG_PRGL_ACTIVATION_TERMES:'
      CALL LOG_ECRIS(LOG_BUF)
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_CONV')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I,'MSG_CMULT_DIVV')

      CALL LOG_DECIND()

      RETURN
      END
