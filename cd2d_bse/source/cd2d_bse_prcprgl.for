C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BSE_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_PRCPRGL (VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'eacnst.fi'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fc'
C-----------------------------------------------------------------------

C---     Affecte les valeurs aux propriétés globales
      CD2D_DIFFU_MOLEC       = VPRGL( 1)
      CD2D_DIFFU_CMULT_VERT  = VPRGL( 2)
      CD2D_DIFFU_CMULT_HORIZ = VPRGL( 3)
      CD2D_DIFFU_CMULT_LONG  = VPRGL( 4)
      CD2D_DEFLT_CLIM        = VPRGL( 5)

      CD2D_DECOU_HTRIG       = VPRGL( 6)
      CD2D_DECOU_HMIN        = VPRGL( 7)
      CD2D_DECOU_CONV        = VPRGL( 8)
      CD2D_DECOU_CLIM        = VPRGL( 9)
      CD2D_DECOU_VDIMP       = VPRGL(10)
      CD2D_DECOU_POROSITE    = VPRGL(11)
      CD2D_DECOU_DIFFU_HORIZ = VPRGL(12)
      CD2D_DECOU_CMULT_LONG  = VPRGL(13)
      CD2D_DECOU_TORTUOSITE  = VPRGL(LM_CMMN_NPRGLL+1)

      CD2D_STABI_PECLET      = VPRGL(14)
      CD2D_STABI_LAPIDUS     = VPRGL(15)
      CD2D_STABI_DMP_MIN     = VPRGL(16)
      CD2D_STABI_DMP_MAX     = VPRGL(17)
      CD2D_STABI_DMP_RGD     = VPRGL(18)
      CD2D_STABI_DMP_LIN     = VPRGL(19)
      CD2D_STABI_DMP_EXP     = 2.0D+00

      CD2D_CMULT_CONV        = VPRGL(20)
      CD2D_CMULT_DIVV        = VPRGL(21)

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : CD2D_BSE_CPYPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_CPYPRGL (CD2D_VA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CPYPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  CD2D_VA(*)

      INCLUDE 'cd2d_bse.fc'

      INTEGER I
C-----------------------------------------------------------------------

      DO I=1,CD2D_CBS_VA_DIM
         CD2D_VA(I) = CD2D_CBS_VA(I)
      ENDDO

      RETURN
      END
