C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_BSE_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER, PARAMETER :: KCL_DEFLT(4) = (/-1, 0, 1, 2 /)
      INTEGER, PARAMETER :: KCL_DECOU(5) = (/ 1, 2, 3, 12, 13 /)
      INTEGER I, ICL
      REAL*8  PORO
      LOGICAL ENERREUR
C-----------------------------------------------------------------------

      ENERREUR = .FALSE.
C---     Diffusivité
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(1, VPRGL(1),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DIFFU_MOLECULAIRE')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(2, VPRGL(2),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DIFFU_CMULT_VERT')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(3, VPRGL(3),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DIFFU_CMULT_HORIZ')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(4, VPRGL(4),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DIFFU_CMULT_LONG')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF

C---     C.L. par défaut
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_INTLST(5, VPRGL(5),
     &                               4, KCL_DEFLT,
     &                               'MSG_DEFLT_CLIM')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF

C---     Découvrement
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(6, VPRGL(6),
     &                               0.0D+00, 1.0D+02,
     &                               'MSG_DECOU_HTRIG')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(7, VPRGL(7),
     &                               0.0D+00, (1.0D0-1.0D-12)*VPRGL(6),
     &                               'MSG_DECOU_HMIN')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(8, VPRGL(8),
     &                               0.0D+00, 1.0D+01,
     &                               'MSG_DECOU_CONV')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_INTLST(9, VPRGL(9),
     &                               5, KCL_DECOU,
     &                               'MSG_DECOU_CLIM')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(10, VPRGL(10),
     &                              -1.0D+99, 1.0D+99,
     &                               'MSG_DECOU_VDIMP')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(11, VPRGL(11),
     &                               0.0D+00, 1.0D+00,
     &                               'MSG_DECOU_POROSITE')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(12, VPRGL(12),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DECOU_DIFFU_HORIZ')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(13, VPRGL(13),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_DECOU_CMULT_LONG')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF

C---     Stabilisation
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(14, VPRGL(14),
     &                               1.0D-16, 1.0D+99,
     &                               'MSG_STABI_PECLET')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(15, VPRGL(15),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_STABI_LAPIDUS')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(16, VPRGL(16),
     &                               -1.0D+99, 1.0D+99,
     &                               'MSG_STABI_DMPG_VMIN')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(17, VPRGL(17),
     &                               VPRGL(15), 1.0D+99,
     &                               'MSG_STABI_DMPG_VMAX')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(18, VPRGL(18),
     &                               0.0D+00, 1.0D+16,
     &                               'MSG_STABI_DMPG_STFF')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(19, VPRGL(19),
     &                               0.0D+00, 1.0D+99,
     &                               'MSG_STABI_DMPG_DLIN')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF

C---     Facteurs multiplicatifs
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(20, VPRGL(20),
     &                               0.0D+00, 1.0D+00,
     &                               'MSG_CMULT_CONV')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = CD2D_BSE_CTRL_MINMAX(21, VPRGL(21),
     &                               0.0D+00, 1.0D+00,
     &                               'MSG_CMULT_DIVV')
         IF (ERR_BAD()) THEN
            ENERREUR = .TRUE.
            CALL LOG_MSG_ERR()
            CALL ERR_RESET()
         ENDIF
      ENDIF

C---     Calcule la tortuosité
      IF (ERR_GOOD()) THEN
         PORO = VPRGL(11)
         VPRGL(LM_CMMN_NPRGLL+1) = UN / (UN - DEUX*LOG(PORO))
      ENDIF

C---     Type de CL par défaut
      IF (ERR_GOOD()) THEN
         ICL = NINT(VPRGL(5))
         IF (.NOT. ANY(KCL_DEFLT .EQ. ICL)) THEN
            WRITE(ERR_BUF,'(2A, 1PE14.6E3)')
     &         'ERR_CODE_CL_INVALIDE', ': ', VPRGL(5)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            CALL ERR_AJT('MSG_DEFLT_CLIM:')
            CALL ERR_AJT('    -1 : weak form integration')
            CALL ERR_AJT('     0 : Neumann')
            CALL ERR_AJT('     1 : Cauchy')
            CALL ERR_AJT('     2 : Open')
         ELSE
            VPRGL(5) = ICL
         ENDIF
      ENDIF

C---     Type de CL de découvrement
      IF (ERR_GOOD()) THEN
         ICL = NINT(VPRGL(9))
         IF (.NOT. ANY(KCL_DECOU .EQ. ICL)) THEN
            WRITE(ERR_BUF,'(2A, 1PE14.6E3)')
     &         'ERR_CODE_CL_INVALIDE', ': ', VPRGL(9)
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            CALL ERR_AJT('MSG_CODE_CL_DECOUVREMENT:')
            CALL ERR_AJT('    1 : Dirichlet')
            CALL ERR_AJT('    2 : Neumann')
            CALL ERR_AJT('    3 : Pass through')
            CALL ERR_AJT('   12 : Weak Dirichlet(10) + Neumann(2)')
            CALL ERR_AJT('   13 : Weak Dirichlet(10) + Pass through(3)')
         ELSE
            VPRGL(9) = ICL
         ENDIF
      ENDIF

C---     En erreur, met un message global générique
      IF (ENERREUR) THEN
         CALL ERR_ASG(ERR_ERR, "MSG_LECTURE_PRGL")
      ENDIF
      
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : CD2D_BSE_CTRL_MINMAX
C
C Description:
C     Contrôle qu'une PRGL soit bien entre les valeurs MIN et MAX.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_CTRL_MINMAX (I, V, VMIN, VMAX, TXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CTRL_MINMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      REAL*8  V
      REAL*8  VMIN
      REAL*8  VMAX
      CHARACTER*(*) TXT

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IF (V .LT. VMIN .OR. V .GT. VMAX) THEN
         WRITE(ERR_BUF,'(A)') 'ERR_VALEUR_INVALIDE'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         WRITE(ERR_BUF,'(3A)') '#<3>#MSG_VARIABLE', '#<18>#= ', TXT
         CALL ERR_AJT(ERR_BUF)

         WRITE(ERR_BUF,'(2A,I6)')       '#<3>#MSG_INDICE', '#<18>#= ', I
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_VALEUR', '#<18>#= ', V
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_MIN', '#<18>#= ', VMIN
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_MAX', '#<18>#= ', VMAX
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      CD2D_BSE_CTRL_MINMAX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire : CD2D_BSE_CTRL_INTLST
C
C Description:
C     Contrôle qu'une PRGL soit bien dans les valeurs de la liste
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_CTRL_INTLST(I, V, NVAL, KVAL, TXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CTRL_INTLST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      REAL*8  V
      INTEGER NVAL
      INTEGER KVAL(NVAL)
      CHARACTER*(*) TXT

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      
      INTEGER IV
C-----------------------------------------------------------------------

      IF (.NOT. ANY(KVAL .EQ. NINT(V))) THEN
         WRITE(ERR_BUF,'(A)') 'ERR_VALEUR_INVALIDE'
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         WRITE(ERR_BUF,'(3A)') '#<3>#MSG_VARIABLE', '#<18>#= ', TXT
         CALL ERR_AJT(ERR_BUF)

         WRITE(ERR_BUF,'(2A,I6)')       '#<3>#MSG_INDICE', '#<18>#= ', I
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_VALEUR', '#<18>#= ', V
         CALL ERR_AJT(ERR_BUF)
         WRITE(ERR_BUF,'(2A,1PE14.6E3)')'#<3>#MSG_VALEURS_VALIDES'
         CALL ERR_AJT(ERR_BUF)
         DO IV=1,NVAL
            WRITE(ERR_BUF,'(A,I12)') '#<40>#= ', KVAL(IV)
            CALL ERR_AJT(ERR_BUF)
         ENDDO
      ENDIF

      CD2D_BSE_CTRL_INTLST = ERR_TYP()
      RETURN
      END
      