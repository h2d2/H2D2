//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_bse
// Entry point: extern "C" void fake_dll_cd2d_bse()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:55.899496
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_BSE
F_PROT(CD2D_BSE_ASGCMN, cd2d_bse_asgcmn);
F_PROT(CD2D_BSE_CLCCLIM, cd2d_bse_clcclim);
F_PROT(CD2D_BSE_CLCDLIB, cd2d_bse_clcdlib);
F_PROT(CD2D_BSE_CLCPRES, cd2d_bse_clcpres);
F_PROT(CD2D_BSE_CLCPREV, cd2d_bse_clcprev);
F_PROT(CD2D_BSE_CLCPRNO, cd2d_bse_clcprno);
F_PROT(CD2D_BSE_HLPCLIM, cd2d_bse_hlpclim);
F_PROT(CD2D_BSE_HLPPRGL, cd2d_bse_hlpprgl);
F_PROT(CD2D_BSE_HLPPRNO, cd2d_bse_hlpprno);
F_PROT(CD2D_BSE_PRCCLIM, cd2d_bse_prcclim);
F_PROT(CD2D_BSE_PRCCLIM_CN, cd2d_bse_prcclim_cn);
F_PROT(CD2D_BSE_PRCDLIB, cd2d_bse_prcdlib);
F_PROT(CD2D_BSE_PRCPRGL, cd2d_bse_prcprgl);
F_PROT(CD2D_BSE_CPYPRGL, cd2d_bse_cpyprgl);
F_PROT(CD2D_BSE_PRCSOLC, cd2d_bse_prcsolc);
F_PROT(CD2D_BSE_PRCSOLR, cd2d_bse_prcsolr);
F_PROT(CD2D_BSE_PRNCLIM, cd2d_bse_prnclim);
F_PROT(CD2D_BSE_PRNPRGL, cd2d_bse_prnprgl);
F_PROT(CD2D_BSE_PRNPRNO, cd2d_bse_prnprno);
F_PROT(CD2D_BSE_PSCPRNO, cd2d_bse_pscprno);
F_PROT(CD2D_BSE_PSLCLIM, cd2d_bse_pslclim);
F_PROT(CD2D_BSE_PSLDLIB, cd2d_bse_psldlib);
F_PROT(CD2D_BSE_PSLPREV, cd2d_bse_pslprev);
F_PROT(CD2D_BSE_PSLPRGL, cd2d_bse_pslprgl);
F_PROT(CD2D_BSE_CTRL_MINMAX, cd2d_bse_ctrl_minmax);
F_PROT(CD2D_BSE_CTRL_INTLST, cd2d_bse_ctrl_intlst);
F_PROT(CD2D_BSE_PSLPRNO, cd2d_bse_pslprno);
F_PROT(CD2D_BSE_PSLSOLC, cd2d_bse_pslsolc);
F_PROT(CD2D_BSE_PSLSOLR, cd2d_bse_pslsolr);
F_PROT(CD2D_BSE_REQNFN, cd2d_bse_reqnfn);
F_PROT(CD2D_BSE_REQPRM, cd2d_bse_reqprm);
F_PROT(CD2D_BSE_HLP1, cd2d_bse_hlp1);
F_PROT(CD2D_BSE_PRN1PRGL, cd2d_bse_prn1prgl);
F_PROT(CD2D_BSE_PST_DIF_000, cd2d_bse_pst_dif_000);
F_PROT(CD2D_BSE_PST_DIF_999, cd2d_bse_pst_dif_999);
F_PROT(CD2D_BSE_PST_DIF_CTR, cd2d_bse_pst_dif_ctr);
F_PROT(CD2D_BSE_PST_DIF_DTR, cd2d_bse_pst_dif_dtr);
F_PROT(CD2D_BSE_PST_DIF_INI, cd2d_bse_pst_dif_ini);
F_PROT(CD2D_BSE_PST_DIF_RST, cd2d_bse_pst_dif_rst);
F_PROT(CD2D_BSE_PST_DIF_REQHBASE, cd2d_bse_pst_dif_reqhbase);
F_PROT(CD2D_BSE_PST_DIF_HVALIDE, cd2d_bse_pst_dif_hvalide);
F_PROT(CD2D_BSE_PST_DIF_ACC, cd2d_bse_pst_dif_acc);
F_PROT(CD2D_BSE_PST_DIF_FIN, cd2d_bse_pst_dif_fin);
F_PROT(CD2D_BSE_PST_DIF_XEQ, cd2d_bse_pst_dif_xeq);
F_PROT(CD2D_BSE_PST_DIF_ASGHSIM, cd2d_bse_pst_dif_asghsim);
F_PROT(CD2D_BSE_PST_DIF_REQHVNO, cd2d_bse_pst_dif_reqhvno);
F_PROT(CD2D_BSE_PST_DIF_REQNOMF, cd2d_bse_pst_dif_reqnomf);
F_PROT(CD2D_BSE_PST_DIF_CLC, cd2d_bse_pst_dif_clc);
F_PROT(CD2D_BSE_PST_SIM_000, cd2d_bse_pst_sim_000);
F_PROT(CD2D_BSE_PST_SIM_999, cd2d_bse_pst_sim_999);
F_PROT(CD2D_BSE_PST_SIM_CTR, cd2d_bse_pst_sim_ctr);
F_PROT(CD2D_BSE_PST_SIM_DTR, cd2d_bse_pst_sim_dtr);
F_PROT(CD2D_BSE_PST_SIM_INI, cd2d_bse_pst_sim_ini);
F_PROT(CD2D_BSE_PST_SIM_INIH, cd2d_bse_pst_sim_inih);
F_PROT(CD2D_BSE_PST_SIM_RST, cd2d_bse_pst_sim_rst);
F_PROT(CD2D_BSE_PST_SIM_REQHBASE, cd2d_bse_pst_sim_reqhbase);
F_PROT(CD2D_BSE_PST_SIM_HVALIDE, cd2d_bse_pst_sim_hvalide);
F_PROT(CD2D_BSE_PST_SIM_ACC, cd2d_bse_pst_sim_acc);
F_PROT(CD2D_BSE_PST_SIM_FIN, cd2d_bse_pst_sim_fin);
F_PROT(CD2D_BSE_PST_SIM_XEQ, cd2d_bse_pst_sim_xeq);
F_PROT(CD2D_BSE_PST_SIM_ASGHSIM, cd2d_bse_pst_sim_asghsim);
F_PROT(CD2D_BSE_PST_SIM_REQHVNO, cd2d_bse_pst_sim_reqhvno);
F_PROT(CD2D_BSE_PST_SIM_REQNOMF, cd2d_bse_pst_sim_reqnomf);
 
// ---  class CD2D_BSEC
F_PROT(CD2D_BSEC_ASGCMN, cd2d_bsec_asgcmn);
F_PROT(CD2D_BSEC_ASMF, cd2d_bsec_asmf);
F_PROT(CD2D_BSEC_ASMK, cd2d_bsec_asmk);
F_PROT(CD2D_BSEC_ASMKT, cd2d_bsec_asmkt);
F_PROT(CD2D_BSEC_ASMKU, cd2d_bsec_asmku);
F_PROT(CD2D_BSEC_ASMM, cd2d_bsec_asmm);
F_PROT(CD2D_BSEC_ASMMU, cd2d_bsec_asmmu);
F_PROT(CD2D_BSEC_PRCCLIM, cd2d_bsec_prcclim);
F_PROT(CD2D_BSEC_PRCPRES, cd2d_bsec_prcpres);
F_PROT(CD2D_BSEC_PRCPREV, cd2d_bsec_prcprev);
F_PROT(CD2D_BSEC_PRCPRNO, cd2d_bsec_prcprno);
F_PROT(CD2D_BSEC_REQNFN, cd2d_bsec_reqnfn);
F_PROT(CD2D_BSEC_REQPRM, cd2d_bsec_reqprm);
 
// ---  class CD2D_BSEN
F_PROT(CD2D_BSEN_ASGCMN, cd2d_bsen_asgcmn);
F_PROT(CD2D_BSEN_ASMF, cd2d_bsen_asmf);
F_PROT(CD2D_BSEN_ASMK, cd2d_bsen_asmk);
F_PROT(CD2D_BSEN_ASMKT, cd2d_bsen_asmkt);
F_PROT(CD2D_BSEN_ASMKU, cd2d_bsen_asmku);
F_PROT(CD2D_BSEN_ASMM, cd2d_bsen_asmm);
F_PROT(CD2D_BSEN_ASMMU, cd2d_bsen_asmmu);
F_PROT(CD2D_BSEN_PRCCLIM, cd2d_bsen_prcclim);
F_PROT(CD2D_BSEN_PRCPRES, cd2d_bsen_prcpres);
F_PROT(CD2D_BSEN_PRCPREV, cd2d_bsen_prcprev);
F_PROT(CD2D_BSEN_PRCPRNO, cd2d_bsen_prcprno);
F_PROT(CD2D_BSEN_REQNFN, cd2d_bsen_reqnfn);
F_PROT(CD2D_BSEN_REQPRM, cd2d_bsen_reqprm);
 
// ---  class IC_CD2D
F_PROT(IC_CD2D_BSE_PST_DIF_CMD, ic_cd2d_bse_pst_dif_cmd);
F_PROT(IC_CD2D_BSE_PST_DIF_REQCMD, ic_cd2d_bse_pst_dif_reqcmd);
F_PROT(IC_CD2D_BSE_PST_SIM_CMD, ic_cd2d_bse_pst_sim_cmd);
F_PROT(IC_CD2D_BSE_PST_SIM_REQCMD, ic_cd2d_bse_pst_sim_reqcmd);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_bse()
{
   static char libname[] = "cd2d_bse";
 
   // ---  class CD2D_BSE
   F_RGST(CD2D_BSE_ASGCMN, cd2d_bse_asgcmn, libname);
   F_RGST(CD2D_BSE_CLCCLIM, cd2d_bse_clcclim, libname);
   F_RGST(CD2D_BSE_CLCDLIB, cd2d_bse_clcdlib, libname);
   F_RGST(CD2D_BSE_CLCPRES, cd2d_bse_clcpres, libname);
   F_RGST(CD2D_BSE_CLCPREV, cd2d_bse_clcprev, libname);
   F_RGST(CD2D_BSE_CLCPRNO, cd2d_bse_clcprno, libname);
   F_RGST(CD2D_BSE_HLPCLIM, cd2d_bse_hlpclim, libname);
   F_RGST(CD2D_BSE_HLPPRGL, cd2d_bse_hlpprgl, libname);
   F_RGST(CD2D_BSE_HLPPRNO, cd2d_bse_hlpprno, libname);
   F_RGST(CD2D_BSE_PRCCLIM, cd2d_bse_prcclim, libname);
   F_RGST(CD2D_BSE_PRCCLIM_CN, cd2d_bse_prcclim_cn, libname);
   F_RGST(CD2D_BSE_PRCDLIB, cd2d_bse_prcdlib, libname);
   F_RGST(CD2D_BSE_PRCPRGL, cd2d_bse_prcprgl, libname);
   F_RGST(CD2D_BSE_CPYPRGL, cd2d_bse_cpyprgl, libname);
   F_RGST(CD2D_BSE_PRCSOLC, cd2d_bse_prcsolc, libname);
   F_RGST(CD2D_BSE_PRCSOLR, cd2d_bse_prcsolr, libname);
   F_RGST(CD2D_BSE_PRNCLIM, cd2d_bse_prnclim, libname);
   F_RGST(CD2D_BSE_PRNPRGL, cd2d_bse_prnprgl, libname);
   F_RGST(CD2D_BSE_PRNPRNO, cd2d_bse_prnprno, libname);
   F_RGST(CD2D_BSE_PSCPRNO, cd2d_bse_pscprno, libname);
   F_RGST(CD2D_BSE_PSLCLIM, cd2d_bse_pslclim, libname);
   F_RGST(CD2D_BSE_PSLDLIB, cd2d_bse_psldlib, libname);
   F_RGST(CD2D_BSE_PSLPREV, cd2d_bse_pslprev, libname);
   F_RGST(CD2D_BSE_PSLPRGL, cd2d_bse_pslprgl, libname);
   F_RGST(CD2D_BSE_CTRL_MINMAX, cd2d_bse_ctrl_minmax, libname);
   F_RGST(CD2D_BSE_CTRL_INTLST, cd2d_bse_ctrl_intlst, libname);
   F_RGST(CD2D_BSE_PSLPRNO, cd2d_bse_pslprno, libname);
   F_RGST(CD2D_BSE_PSLSOLC, cd2d_bse_pslsolc, libname);
   F_RGST(CD2D_BSE_PSLSOLR, cd2d_bse_pslsolr, libname);
   F_RGST(CD2D_BSE_REQNFN, cd2d_bse_reqnfn, libname);
   F_RGST(CD2D_BSE_REQPRM, cd2d_bse_reqprm, libname);
   F_RGST(CD2D_BSE_HLP1, cd2d_bse_hlp1, libname);
   F_RGST(CD2D_BSE_PRN1PRGL, cd2d_bse_prn1prgl, libname);
   F_RGST(CD2D_BSE_PST_DIF_000, cd2d_bse_pst_dif_000, libname);
   F_RGST(CD2D_BSE_PST_DIF_999, cd2d_bse_pst_dif_999, libname);
   F_RGST(CD2D_BSE_PST_DIF_CTR, cd2d_bse_pst_dif_ctr, libname);
   F_RGST(CD2D_BSE_PST_DIF_DTR, cd2d_bse_pst_dif_dtr, libname);
   F_RGST(CD2D_BSE_PST_DIF_INI, cd2d_bse_pst_dif_ini, libname);
   F_RGST(CD2D_BSE_PST_DIF_RST, cd2d_bse_pst_dif_rst, libname);
   F_RGST(CD2D_BSE_PST_DIF_REQHBASE, cd2d_bse_pst_dif_reqhbase, libname);
   F_RGST(CD2D_BSE_PST_DIF_HVALIDE, cd2d_bse_pst_dif_hvalide, libname);
   F_RGST(CD2D_BSE_PST_DIF_ACC, cd2d_bse_pst_dif_acc, libname);
   F_RGST(CD2D_BSE_PST_DIF_FIN, cd2d_bse_pst_dif_fin, libname);
   F_RGST(CD2D_BSE_PST_DIF_XEQ, cd2d_bse_pst_dif_xeq, libname);
   F_RGST(CD2D_BSE_PST_DIF_ASGHSIM, cd2d_bse_pst_dif_asghsim, libname);
   F_RGST(CD2D_BSE_PST_DIF_REQHVNO, cd2d_bse_pst_dif_reqhvno, libname);
   F_RGST(CD2D_BSE_PST_DIF_REQNOMF, cd2d_bse_pst_dif_reqnomf, libname);
   F_RGST(CD2D_BSE_PST_DIF_CLC, cd2d_bse_pst_dif_clc, libname);
   F_RGST(CD2D_BSE_PST_SIM_000, cd2d_bse_pst_sim_000, libname);
   F_RGST(CD2D_BSE_PST_SIM_999, cd2d_bse_pst_sim_999, libname);
   F_RGST(CD2D_BSE_PST_SIM_CTR, cd2d_bse_pst_sim_ctr, libname);
   F_RGST(CD2D_BSE_PST_SIM_DTR, cd2d_bse_pst_sim_dtr, libname);
   F_RGST(CD2D_BSE_PST_SIM_INI, cd2d_bse_pst_sim_ini, libname);
   F_RGST(CD2D_BSE_PST_SIM_INIH, cd2d_bse_pst_sim_inih, libname);
   F_RGST(CD2D_BSE_PST_SIM_RST, cd2d_bse_pst_sim_rst, libname);
   F_RGST(CD2D_BSE_PST_SIM_REQHBASE, cd2d_bse_pst_sim_reqhbase, libname);
   F_RGST(CD2D_BSE_PST_SIM_HVALIDE, cd2d_bse_pst_sim_hvalide, libname);
   F_RGST(CD2D_BSE_PST_SIM_ACC, cd2d_bse_pst_sim_acc, libname);
   F_RGST(CD2D_BSE_PST_SIM_FIN, cd2d_bse_pst_sim_fin, libname);
   F_RGST(CD2D_BSE_PST_SIM_XEQ, cd2d_bse_pst_sim_xeq, libname);
   F_RGST(CD2D_BSE_PST_SIM_ASGHSIM, cd2d_bse_pst_sim_asghsim, libname);
   F_RGST(CD2D_BSE_PST_SIM_REQHVNO, cd2d_bse_pst_sim_reqhvno, libname);
   F_RGST(CD2D_BSE_PST_SIM_REQNOMF, cd2d_bse_pst_sim_reqnomf, libname);
 
   // ---  class CD2D_BSEC
   F_RGST(CD2D_BSEC_ASGCMN, cd2d_bsec_asgcmn, libname);
   F_RGST(CD2D_BSEC_ASMF, cd2d_bsec_asmf, libname);
   F_RGST(CD2D_BSEC_ASMK, cd2d_bsec_asmk, libname);
   F_RGST(CD2D_BSEC_ASMKT, cd2d_bsec_asmkt, libname);
   F_RGST(CD2D_BSEC_ASMKU, cd2d_bsec_asmku, libname);
   F_RGST(CD2D_BSEC_ASMM, cd2d_bsec_asmm, libname);
   F_RGST(CD2D_BSEC_ASMMU, cd2d_bsec_asmmu, libname);
   F_RGST(CD2D_BSEC_PRCCLIM, cd2d_bsec_prcclim, libname);
   F_RGST(CD2D_BSEC_PRCPRES, cd2d_bsec_prcpres, libname);
   F_RGST(CD2D_BSEC_PRCPREV, cd2d_bsec_prcprev, libname);
   F_RGST(CD2D_BSEC_PRCPRNO, cd2d_bsec_prcprno, libname);
   F_RGST(CD2D_BSEC_REQNFN, cd2d_bsec_reqnfn, libname);
   F_RGST(CD2D_BSEC_REQPRM, cd2d_bsec_reqprm, libname);
 
   // ---  class CD2D_BSEN
   F_RGST(CD2D_BSEN_ASGCMN, cd2d_bsen_asgcmn, libname);
   F_RGST(CD2D_BSEN_ASMF, cd2d_bsen_asmf, libname);
   F_RGST(CD2D_BSEN_ASMK, cd2d_bsen_asmk, libname);
   F_RGST(CD2D_BSEN_ASMKT, cd2d_bsen_asmkt, libname);
   F_RGST(CD2D_BSEN_ASMKU, cd2d_bsen_asmku, libname);
   F_RGST(CD2D_BSEN_ASMM, cd2d_bsen_asmm, libname);
   F_RGST(CD2D_BSEN_ASMMU, cd2d_bsen_asmmu, libname);
   F_RGST(CD2D_BSEN_PRCCLIM, cd2d_bsen_prcclim, libname);
   F_RGST(CD2D_BSEN_PRCPRES, cd2d_bsen_prcpres, libname);
   F_RGST(CD2D_BSEN_PRCPREV, cd2d_bsen_prcprev, libname);
   F_RGST(CD2D_BSEN_PRCPRNO, cd2d_bsen_prcprno, libname);
   F_RGST(CD2D_BSEN_REQNFN, cd2d_bsen_reqnfn, libname);
   F_RGST(CD2D_BSEN_REQPRM, cd2d_bsen_reqprm, libname);
 
   // ---  class IC_CD2D
   F_RGST(IC_CD2D_BSE_PST_DIF_CMD, ic_cd2d_bse_pst_dif_cmd, libname);
   F_RGST(IC_CD2D_BSE_PST_DIF_REQCMD, ic_cd2d_bse_pst_dif_reqcmd, libname);
   F_RGST(IC_CD2D_BSE_PST_SIM_CMD, ic_cd2d_bse_pst_sim_cmd, libname);
   F_RGST(IC_CD2D_BSE_PST_SIM_REQCMD, ic_cd2d_bse_pst_sim_reqcmd, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
