C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes : Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CNF2RES
C
C Description:
C     ASSEMBLAGE DU RESIDU COMPLET DÉPENDANT DU SCHEMA TEMPOREL
C     EQUATION : TRANSPORT-DIFFUSION 2D FORMULATION NON CONSERVATIVE
C     ELEMENT  : T3
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_ASMFKU(VCORG,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           KISLC,
     &                           VDSLC,
     &                           KISLR,
     &                           VDSLR,
     &                           VDLG,
     &                           VRES)

      INCLUDE 'lmcnst.fi'
      INCLUDE 'lmcmmn.fc'
      REAL*8  VCORG(JJ$NDIM, JJ$NNT)
      INTEGER KNGV (JJ$NNELV,JJ$NELV)
      INTEGER KNGS (JJ$NNELS,JJ$NELS)
      REAL*8  VDJV (JJ$NDJV, JJ$NELV)
      REAL*8  VDJS (JJ$NDJS, JJ$NELS)
      REAL*8  VPRGL(JJ$NPRGL)
      REAL*8  VPRNO(JJ$NPRNO,JJ$NNT)
      REAL*8  VPREV(JJ$NPREV,JJ$NELV)
      REAL*8  VPRES(JJ$NPREV,JJ$NELV)
      INTEGER KISLC(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLC(JJ$NDLN, JJ$NNT)
      INTEGER KISLR(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLR(JJ$NDLN, JJ$NNT)
      REAL*8  VDLG (JJ$NDLN, JJ$NNT)
      REAL*8  VRES (JJ$NDLN, JJ$NNT)
C-----------------------------------------------------------------------

      CALL CD2D_BSE_ASMF (VCORG,
     &                    KNGV,
     &                    KNGS,
     &                    VDJV,
     &                    VDJS,
     &                    VPRGL,
     &                    VPRNO,
     &                    VPREV,
     &                    VPRES,
     &                    VDSLC,
     &                    VDSLR,
     &                    VRES)

      CALL CD2D_BSE_ASMKU(VCORG,
     &                    KNGV,
     &                    KNGS,
     &                    VDJV,
     &                    VDJS,
     &                    VPRGL,
     &                    VPRNO,
     &                    VPREV,
     &                    VPRES,
     &                    VDSLR,
     &                    VDLG,
     &                    VRES)

      CALL SSCAL  (NDLT,-UN,VRES,1)
      RETURN
      END

