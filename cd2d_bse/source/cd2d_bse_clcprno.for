C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C Entrée: VCORG,KNG,VDJ,VPRGL,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_CLCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END

