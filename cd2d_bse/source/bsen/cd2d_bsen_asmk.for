C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D.
C                         FORMULATION NON-CONSERVATIVE POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C        transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSEN_ASMK
C
C Description:
C     La fonction CD2D_BSEN_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMK(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'

      REAL*8  VKE  (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VDLE (CD2D_BSE_NDLEMAX)
      INTEGER KLOCE(CD2D_BSE_NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. CD2D_BSE_NDLNMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VDLE, KLOCE)
         CALL CD2D_BSEN_ASMK_V(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

      IF (ERR_GOOD()) THEN
         CALL CD2D_BSEN_ASMK_S(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         KEIMP,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMK_V
C
C Description:
C     La fonction CD2D_BSEN_ASMK_V calcul le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMK_V(KLOCE,
     &                            VDLE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER IERR
      INTEGER IC, IE, IN, ID, II, NO

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = CD2D_BSE_NDLEMAX)
      PARAMETER (NPRN_LCL = 50)

      INTEGER KNE (NNEL_LCL)
      REAL*8  VPRN(NPRN_LCL * NNEL_LCL)
      REAL*8  VSOL(NDLE_LCL * NNEL_LCL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Initialise la matrice élémentaire
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)

C---        Transfert des connectivités élémentaires
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        Transfert des DDL
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NDLN, VDLG(1,NO), 1, VDLE(1,IN), 1)
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Transfert des SOLR
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NSOLR, VSOLR(1,NO), 1, VSOL(II), 1)
            II=II+LM_CMMN_NSOLR
         ENDDO

C---        Calcul de la matrice élémentaire de volume
         CALL CD2D_BSEN_ASMKE_V(VKE,
     &                          VDJV(1,IE),
     &                          VPRGL,
     &                          VPRN,
     &                          VPREV(1,IE),
     &                          VSOL,
     &                          VDLE)

C---        Terme petit sur la diagonale si zero
         DO ID=1,LM_CMMN_NDLEV
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = PETIT
         ENDDO

C---        Table KLOCE de l'élément
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL ICOPY(LM_CMMN_NDLN, KLOCN(1,NO), 1, KLOCE(1,IN), 1)
         ENDDO

C---        ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM', ': ', IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKT_S
C
C Description:
C     La fonction CD2D_BSEN_ASMKT_S calcul la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. CD2D_BSEN_ASMKT_S
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMK_S(KLOCE,
     &                            VDLEV,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES, ! pas utilisé
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLEV (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NPRE_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = 18)
      PARAMETER (NPRN_LCL = 18)
      PARAMETER (NPRE_LCL =  8)

      REAL*8  VPRNV(NPRN_LCL * NNEL_LCL)
      REAL*8  VPRE (NPRE_LCL)
      REAL*8  VRESE(NDLE_LCL), VRESEP(NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IN, ID, II, NO, I
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNE, HPREVE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et côté
         IEV = KNGS(3,IES)
         ICT = KNGS(4,IES)

C---        Transfert des DDL de volume
         VDLEV(:,:) = VDLG(:,KNGV(:,IEV))

C---        Transfert des PRNO
!!!         VPRNV(:,:) = VPRNO(:,KNGV(:,IEV))
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Initialise la matrice élémentaire
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKE, 1)

C---        [K(u)]
         CALL CD2D_BSEN_ASMKE_S(VKE,
     &                          VDJV(1,IEV),
     &                          VDJS(1,IES),
     &                          VPRGL,
     &                          VPRNV,
     &                          VPREV(1,IEV),
     &                          VPRES(1,IES), ! pas utilisé
     &                          VDLEV,
     &                          KEIMP(IES),
     &                          ICT)

C---        [K(u)]{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKE,               ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLEV,             ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VRESE,             ! y
     &              1)                 ! incy

C---       Assemble de la matrice
         KLOCE(:,:) = KLOCN(:,KNGV(:,IEV))
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMK_V
C
C Description:
C     La fonction CD2D_BSEN_ASMK_V calcul le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMK0_V(KLOCE,
     &                            VDLE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  CPEC, CLAP, DISP, CONV, SOLR, SPEC, SLAP
      REAL*8  DELX, DELY, AKXX, AKXY, AKYY
      REAL*8  C1, C2, C3, HU1, HU2, HU3, HV1, HV2, HV3
      REAL*8  SHU, SHV, ASHU, ASHV
      REAL*8  DFX1, DFX2, DFX3, DFY1, DFY2, DFY3
      REAL*8  CUX1, CUX2, CUX3, CUY1, CUY2, CUY3
      REAL*8  STBX, STBY, STBX1, STBX2, STBX3, STBY1, STBY2, STBY3
      REAL*8  CX, CY, DCNRM
      REAL*8  SOL11,SOL12,SOL13, SOL22,SOL23, SOL33
      REAL*8  VKE11,VKE21,VKE31, VKE12,VKE22,VKE32, VKE13,VKE23,VKE33
      REAL*8  QH1, QH2, QH3, SQH
      REAL*8  DIVQ, SDIV
      INTEGER IERR
      INTEGER IC, IE, ID, ID1, ID2, ID3
      INTEGER IPHU, IPHV
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Indices dans VPRNO
      IPHU = LM_CMMN_NPRNOL + 4
      IPHV = LM_CMMN_NPRNOL + 5

C---     Prop. globales
      CPEC = UN / CD2D_STABI_PECLET    ! Peclet
      CLAP = CD2D_STABI_LAPIDUS        ! Lapidus

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     Boucle sur les éléments
C        =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES DU T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)

C---        MÉTRIQUES DU T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         DETJ = VDJV(5,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)

C---        CONSTANTES DÉPENDANT DU VOLUME
         DISP  = UN_24/DETJ            ! Dispersion
         CONV  = UN_24*CD2D_CMULT_CONV ! Convection
         SDIV  = UN_24*CD2D_CMULT_DIVV ! Divergence
         SOLR  = UN_120*DETJ           ! Sol. réparties
         SPEC  = UN_6*CPEC/DETJ        ! Stabilisation Peclet
         SLAP  = UN_2*CLAP/DETJ        ! Stabilisation Lapidus

C---        Initialisation des propriétés élémentaires
         DELX = VPREV(1,IE)
         DELY = VPREV(2,IE)
         AKXX = VPREV(4,IE)
         AKXY = VPREV(5,IE)
         AKYY = VPREV(6,IE)

C---        Initialisation des propriétés nodales
         HU1 = VPRNO(IPHU,NO1)
         HV1 = VPRNO(IPHV,NO1)
         HU2 = VPRNO(IPHU,NO2)
         HV2 = VPRNO(IPHV,NO2)
         HU3 = VPRNO(IPHU,NO3)
         HV3 = VPRNO(IPHV,NO3)
         SHU = HU1 + HU2 + HU3
         SHV = HV1 + HV2 + HV3
         ASHU = ABS(HU1) + ABS(HU2) + ABS(HU3)
         ASHV = ABS(HV1) + ABS(HV2) + ABS(HV3)

C---        Dispersion
         DFX1 = DISP*(AKXX*VSX + AKXY*VSY)
         DFX2 = DISP*(AKXX*VKX + AKXY*VKY)
         DFX3 = DISP*(AKXX*VEX + AKXY*VEY)
         DFY1 = DISP*(AKXY*VSX + AKYY*VSY)
         DFY2 = DISP*(AKXY*VKX + AKYY*VKY)
         DFY3 = DISP*(AKXY*VEX + AKYY*VEY)

C---        Convection
         CUX1 = CONV*(SHU+HU1)
         CUX2 = CONV*(SHU+HU2)
         CUX3 = CONV*(SHU+HU3)
         CUY1 = CONV*(SHV+HV1)
         CUY2 = CONV*(SHV+HV2)
         CUY3 = CONV*(SHV+HV3)

C---        Stabilisation de la convection (Peclet)
         STBX  = SPEC * DELX * ASHU
         STBY  = SPEC * DELY * ASHV
         STBX1 = STBX * VSX
         STBX2 = STBX * VKX
         STBX3 = STBX * VEX
         STBY1 = STBY * VSY
         STBY2 = STBY * VKY
         STBY3 = STBY * VEY

C---        Divergence du débit
         DIVQ = SDIV * (VKX*(HU2-HU1) + VEX*(HU3-HU1) +
     &                  VKY*(HV2-HV1) + VEY*(HV3-HV1))

C---        Termes communs à tous les ddl
         VKE11 = (VSX*DFX1 + VSY*DFY1) + (VSX*CUX1 + VSY*CUY1) +
     .           (VSX*STBX1 + VSY*STBY1)
         VKE21 = (VKX*DFX1 + VKY*DFY1) + (VSX*CUX2 + VSY*CUY2) +
     .           (VKX*STBX1 + VKY*STBY1)
         VKE31 = (VEX*DFX1 + VEY*DFY1) + (VSX*CUX3 + VSY*CUY3) +
     .           (VEX*STBX1 + VEY*STBY1)
         VKE12 = (VSX*DFX2 + VSY*DFY2) + (VKX*CUX1 + VKY*CUY1) +
     .           (VSX*STBX2 + VSY*STBY2)
         VKE22 = (VKX*DFX2 + VKY*DFY2) + (VKX*CUX2 + VKY*CUY2) +
     .           (VKX*STBX2 + VKY*STBY2)
         VKE32 = (VEX*DFX2 + VEY*DFY2) + (VKX*CUX3 + VKY*CUY3) +
     .           (VEX*STBX2 + VEY*STBY2)
         VKE13 = (VSX*DFX3 + VSY*DFY3) + (VEX*CUX1 + VEY*CUY1) +
     .           (VSX*STBX3 + VSY*STBY3)
         VKE23 = (VKX*DFX3 + VKY*DFY3) + (VEX*CUX2 + VEY*CUY2) +
     .           (VKX*STBX3 + VKY*STBY3)
         VKE33 = (VEX*DFX3 + VEY*DFY3) + (VEX*CUX3 + VEY*CUY3) +
     .           (VEX*STBX3 + VEY*STBY3)

C---        Boucle sur les DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN

C---           Degrés de liberté (pour Lapidus)
            C1 = VDLG(ID, NO1)
            C2 = VDLG(ID, NO2)
            C3 = VDLG(ID, NO3)
            CX = (VSX*C1 + VKX*C2 + VEX*C3)
            CY = (VSY*C1 + VKY*C2 + VEY*C3)

C---           Stabilisation de Lapidus
            DCNRM = SLAP * MAX(HYPOT(CX, CY), 1.0D-12)
            STBX1 = DCNRM * VSX
            STBX2 = DCNRM * VKX
            STBX3 = DCNRM * VEX
            STBY1 = DCNRM * VSY
            STBY2 = DCNRM * VKY
            STBY3 = DCNRM * VEY

C---           Sollicitations réparties (Q en entrée)
            QH1   = SOLR * VSOLR(ID,NO1)
            QH2   = SOLR * VSOLR(ID,NO2)
            QH3   = SOLR * VSOLR(ID,NO3)
            SQH   = DEUX*(QH1 + QH2 + QH3)
            SOL11 = (SQH + QUATRE*QH1) + DIVQ+DIVQ
            SOL12 = (SQH - QH3)        + DIVQ
            SOL13 = (SQH - QH2)        + DIVQ
            SOL22 = (SQH + QUATRE*QH2) + DIVQ+DIVQ
            SOL23 = (SQH - QH1)        + DIVQ
            SOL33 = (SQH + QUATRE*QH3) + DIVQ+DIVQ

C---           Assemblage des contributions
            VKE(ID1,ID1) = VKE11 + (SOL11 + VSX*STBX1 + VSY*STBY1)
            VKE(ID2,ID1) = VKE21 + (SOL12 + VKX*STBX1 + VKY*STBY1)
            VKE(ID3,ID1) = VKE31 + (SOL13 + VEX*STBX1 + VEY*STBY1)
            VKE(ID1,ID2) = VKE12 + (SOL12 + VSX*STBX2 + VSY*STBY2)
            VKE(ID2,ID2) = VKE22 + (SOL22 + VKX*STBX2 + VKY*STBY2)
            VKE(ID3,ID2) = VKE32 + (SOL23 + VEX*STBX2 + VEY*STBY2)
            VKE(ID1,ID3) = VKE13 + (SOL13 + VSX*STBX3 + VSY*STBY3)
            VKE(ID2,ID3) = VKE23 + (SOL23 + VKX*STBX3 + VKY*STBY3)
            VKE(ID3,ID3) = VKE33 + (SOL33 + VEX*STBX3 + VEY*STBY3)
         ENDDO

C---        Terme petit sur la diagonale si zero
         DO ID=1,LM_CMMN_NDLEV
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = DETJ*PETIT
         ENDDO

C---        Table KLOCE de localisation des DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NO2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        Assemblage de la matrice
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_V',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMK_S
C
C Description:
C     La fonction CD2D_BSEN_ASMK_S calcule le matrice de rigidité
C     élémentaire due aux éléments de surface. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMK0_S(KLOCE,
     &                            VDLE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IES, IEV
      INTEGER ID, ID1, ID2, ID3
      INTEGER NO1, NO2, NO3, NP1, NP2, NP3
      REAL*8  VSX, VKX, VEX, VNX
      REAL*8  VSY, VKY, VEY, VNY
      REAL*8  DETJL2, DETJT3
      REAL*8  QN1, QN2, SQN
      REAL*8  DISC, REP
      REAL*8  AKXXH1, AKXYH1, AKYYH1
      REAL*8  AKXXH2, AKXYH2, AKYYH2
      REAL*8  DIF11, DIF12, DIF13, DIF21, DIF22, DIF23
      REAL*8  VKE11, VKE21, VKE12, VKE22, VKE13, VKE23
C-----------------------------------------------------------------------
      INTEGER IE
      LOGICAL ELE_ESTTYPE1
      LOGICAL ELE_ESTTYPE2
      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_ENTRANT) .AND.
     &                   (BTEST(KEIMP(IE), EA_TPCL_CAUCHY) .OR.
     &                    BTEST(KEIMP(IE), EA_TPCL_OUVERT))
      ELE_ESTTYPE2(IE) = BTEST(KEIMP(IE), EA_TPCL_SORTANT) .AND.
     &                   BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR DE CAUCHY "ENTRANTS"
C        ================================================================
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE1(IES)) GOTO 199

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)

C---        Constante dépendant du volume
         DETJL2 = VDJS(3,IES)
         REP = -UN_6*DETJL2

C---        Flux normal (Vn*H)
         QN1 = REP*VPRES(1,IES)
         QN2 = REP*VPRES(2,IES)
         SQN = QN1 + QN2

C---        Matrice de rigidite élémentaire de CAUCHY-ENTRANT
!         VKE11 = SQN + QN1+QN1
!         VKE21 = SQN
!         VKE12 = SQN
!         VKE22 = SQN + QN2+QN2
         VKE11 = SQN+SQN + QN1+QN1    ! Matrice lumpée
         VKE22 = SQN+SQN + QN2+QN2

C---        Boucle sur les ddl
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN

            VKE(ID1,ID1) = VKE11
!            VKE(ID2,ID1) = VKE21
!            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
         ENDDO

C---        Table KLOCE de localisation des ddls
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO

C---        ASSEMBLAGE DE LA MATRICE
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_S1',': ',IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,'(2A,I9)')
D    &            'ERR_CALCUL_MATRICE_K_CAUCHY_ENTRANT', ': ', IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

199      CONTINUE
      ENDDO


C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR OUVERTS "SORTANTS"
C        ==============================================================
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE2(IES)) GOTO 299

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)
         IEV = KNGS(3,IES)
         NO1 = KNGV(1,IEV)
         NO2 = KNGV(2,IEV)
         NO3 = KNGV(3,IEV)

C---        Recherche le troisième noeud de l'élément de volume
         IF    (NP1 .EQ. NO1 .AND. NP2 .EQ. NO2) THEN
            NP3 = NO3
         ELSEIF(NP1 .EQ. NO2 .AND. NP2 .EQ. NO3) THEN
            NP3 = NO1
         ELSEIF(NP1 .EQ. NO3 .AND. NP2 .EQ. NO1) THEN
            NP3 = NO2
D        ELSE
D           CALL ERR_ASR(.FALSE.)
         ENDIF

C---        Métriques du T3 parent (recalculées car permutés)
         VKX    = VCORG(2,NP3) - VCORG(2,NP1)               ! Ksi,x
         VEX    = VCORG(2,NP1) - VCORG(2,NP2)               ! Eta,x
         VKY    = VCORG(1,NP1) - VCORG(1,NP3)               ! Ksi,y
         VEY    = VCORG(1,NP2) - VCORG(1,NP1)               ! Eta,y
         DETJT3 = VDJV(5,IEV)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)

C---        MÉTRIQUES DE L'ÉLÉMENT - COMPOSANTES DE LA NORMALE EXTÉRIEURE
         VNY    = -VDJS(1,IES)
         VNX    =  VDJS(2,IES)
         DETJL2 =  VDJS(3,IES)

C---        Constante dépendant du contour
         DISC = -UN_2*DETJL2/DETJT3

C---        Propriétés nodales du contour
         AKXXH1 = VPRES(3,IES)
         AKXXH2 = VPRES(4,IES)
         AKXYH1 = VPRES(5,IES)
         AKXYH2 = VPRES(6,IES)
         AKYYH1 = VPRES(7,IES)
         AKYYH2 = VPRES(8,IES)

C---        Termes de contour
         DIF11 = DISC*( (AKXXH1 * VSX + AKXYH1 * VSY ) * VNX
     &         +        (AKXYH1 * VSX + AKYYH1 * VSY ) * VNY )
         DIF12 = DISC*( (AKXXH1 * VKX + AKXYH1 * VKY ) * VNX
     &         +        (AKXYH1 * VKX + AKYYH1 * VKY ) * VNY )
         DIF13 = DISC*( (AKXXH1 * VEX + AKXYH1 * VEY ) * VNX
     &         +        (AKXYH1 * VEX + AKYYH1 * VEY ) * VNY )
         DIF21 = DISC*( (AKXXH2 * VSX + AKXYH2 * VSY ) * VNX
     &         +        (AKXYH2 * VSX + AKYYH2 * VSY ) * VNY )
         DIF22 = DISC*( (AKXXH2 * VKX + AKXYH2 * VKY ) * VNX
     &         +        (AKXYH2 * VKX + AKYYH2 * VKY ) * VNY )
         DIF23 = DISC*( (AKXXH2 * VEX + AKXYH2 * VEY ) * VNX
     &         +        (AKXYH2 * VEX + AKYYH2 * VEY ) * VNY )

C---        MATRICE ELEMENTAIRE DE FRONTIERE OUVERTE
         VKE11 = DIF11
         VKE21 = DIF21
         VKE12 = DIF12
         VKE22 = DIF22
         VKE13 = DIF13
         VKE23 = DIF23

C---        BOUCLE D'ASSEMBLAGE SUR LES DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN
            VKE(ID1,ID1) = VKE11
            VKE(ID2,ID1) = VKE21
            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
            VKE(ID1,ID3) = VKE13
            VKE(ID2,ID3) = VKE23
         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NP3)
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_S2',': ',IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_CAUCHY_SORTANT', ': ', IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

299      CONTINUE
      ENDDO

      RETURN
      END

