C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CNF2KT
C
C Description: ASSEMBLAGE DE LA MATRICE TANGENTE
C              INVERSION / MATRICE DE PRÉ-CONDITIONNEMENT
C
C Entrée: VCORG,VDJ,VPRGL,VPRNO,VPREG,KNGV,VDLG,JR,IAP,JAP
C
C Sortie: VKG
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKT(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEN_ASMKT
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC, EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR, EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      REAL*8  VKT1 (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VKT2 (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VDLE (CD2D_BSE_NDLEMAX)
      INTEGER KLOCE(CD2D_BSE_NDLEMAX)
      INTEGER IERR
C-----------------------------------------------------------------------

      IF (CD2D_STABI_LAPIDUS .LT. 1.0D-12) THEN
         CALL CD2D_BSEN_ASMK(VCORG,
     &                    KLOCN,
     &                    KNGV,
     &                    KNGS,
     &                    VDJV,
     &                    VDJS,
     &                    VPRGL,
     &                    VPRNO,
     &                    VPREV,
     &                    VPRES,
     &                    VSOLC,
     &                    VSOLR,
     &                    KCLCND,
     &                    VCLCNV,
     &                    KCLLIM,
     &                    KCLNOD,
     &                    KCLELE,
     &                    VCLDST,
     &                    KDIMP,
     &                    VDIMP,
     &                    KEIMP,
     &                    VDLG,
     &                    HMTX,
     &                    F_ASM)
      ELSE
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKT1, VKT2, VDLE, KLOCE)
         CALL CD2D_BSEN_ASMKT_V(KLOCE,
     &                          VDLE,
     &                          VKT1,
     &                          VKT2,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES, !pas utilisé
     &                          VSOLR,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel

C---     Contributions de surface
         CALL CD2D_BSEN_ASMKT_S(KLOCE,
     &                          VDLE,
     &                          VKT1,
     &                          VKT2,
     &                          VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKT_V
C
C Description:
C     La fonction CD2D_BSEN_ASMKT_V calcule la matrice tangente
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) La matrice tangente est donnée par (Gouri Dhatt p. 342):
C        Kt_ij = K_ij + Somme_sur_l ( (dK_il / du_j) * u_l )
C     avec:
C        dK_il/du_j ~= (K(u+delu_j) - K(u) ) / delu_j
C
C     2) La version d'Hydrosim est plus efficace. Elle calcule la partie
C     tangente comme:
C        dK_il/du_j ~= ([K(u+delu_j)]{u+delu_j} - [K(u)]{u} ) / delu_j
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKT_V(KLOCE,
     &                             VDLE,
     &                             VKT,
     &                             VKT_TR,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, !pas utilisé
     &                             VSOLR,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLEV)
      REAL*8   VDLE  (LM_CMMN_NDLEV)
      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) !pas utilisé
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = CD2D_BSE_NDLEMAX)
      PARAMETER (NPRN_LCL = 50)

      REAL*8  CD2D_PNUMR_DELPRT
      REAL*8  CD2D_PNUMR_DELMIN
      REAL*8  CD2D_PNUMR_PENALITE
      PARAMETER(CD2D_PNUMR_DELPRT   = 1.0D-07)
      PARAMETER(CD2D_PNUMR_DELMIN   = 1.0D-15)
      PARAMETER(CD2D_PNUMR_PENALITE = 1.0D-18)

      REAL*8  VPRN(NPRN_LCL * NNEL_LCL)
      REAL*8  VSOL(NDLE_LCL * NNEL_LCL)
      REAL*8  VRESE(NDLE_LCL), VRESEP(NDLE_LCL)

      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      REAL*8  PENA
      INTEGER IERR
      INTEGER IC, IE, IN, ID, II, NO, I
      INTEGER KNE(NNEL_LCL)
      INTEGER HVFT, HPRNE, HPREVE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp  do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        Transfert des connectivités élémentaires
         DO IN=1,EG_CMMN_NNELV
            KNE(IN) = KNGV(IN,IE)
         ENDDO

C---        Table kloce de l'élément
         II = 1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL ICOPY(LM_CMMN_NDLN, KLOCN(1,NO), 1, KLOCE(II), 1)
            II = II+LM_CMMN_NDLN
         ENDDO

C---        Transfert des DDL
         II = 1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NDLN, VDLG(1,NO), 1, VDLE(II), 1)
            II = II+LM_CMMN_NDLN
         ENDDO

C---        Transfert des PRNO
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRN(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Transfert des SOLR
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNE(IN)
            CALL DCOPY(LM_CMMN_NSOLR, VSOLR(1,NO), 1, VSOL(II), 1)
            II=II+LM_CMMN_NSOLR
         ENDDO

C---        Initialise la matrice elementaire
D        CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT_TR,1)

C---        [K(u)]
         CALL CD2D_BSEN_ASMKE_V(VKT_TR,
     &                          VDJV(1,IE),
     &                          VPRGL,
     &                          VPRN,
     &                          VPREV(1,IE),
     &                          VSOL,
     &                          VDLE)

C---        [K(u)]{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKT_TR,            ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLE,              ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VRESE,             ! y
     &              1)                 ! incy

C---        Initialise la matrice des dérivées
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT,1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,LM_CMMN_NDLEV
            IF (KLOCE(ID) .EQ. 0) GOTO 199

C---           PERTURBE LE DDL
            VDL_ORI = VDLE(ID)
            VDL_DEL = CD2D_PNUMR_DELPRT * VDLE(ID)
     &              + SIGN(CD2D_PNUMR_DELMIN, VDLE(ID))
            VDL_INV = 1.0D0 / VDL_DEL
            VDLE(ID) = VDLE(ID) + VDL_DEL

C---           Initialise la matrice de travail
            CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKT_TR, 1)

C---           [K(u + du_id)]
            CALL CD2D_BSEN_ASMKE_V(VKT_TR,
     &                             VDJV(1,IE),
     &                             VPRGL,
     &                             VPRN,
     &                             VPREV(1,IE),
     &                             VSOL,
     &                             VDLE)

C---           [K(u + du_id)]{u+du_id}
            CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &                 LM_CMMN_NDLEV,     ! m rows
     &                 LM_CMMN_NDLEV,     ! n columns
     &                 UN,                ! alpha
     &                 VKT_TR,            ! a
     &                 LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &                 VDLE,              ! x
     &                 1,                 ! incx
     &                 ZERO,              ! beta
     &                 VRESEP,            ! y
     &                 1)                 ! incy

C---           Restaure la valeur originale
            VDLE(ID) = VDL_ORI

C---           ([K(u + du_id)]{u+du_id} - [K(u)]{u})/du
            DO I=1,LM_CMMN_NDLEV
               VKT(I,ID) = (VRESEP(I) - VRESE(I)) * VDL_INV
            ENDDO

199         CONTINUE
         ENDDO

C---        TERME PETIT SUR LA DIAGONALE
         PENA = CD2D_PNUMR_PENALITE*VDJV(5,IE)
         DO ID=1,LM_CMMN_NDLEV
            IF (VKT(ID,ID) .EQ. ZERO) VKT(ID,ID) = PENA
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKT)
D        IF (ERR_BAD()) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEN_ASMKT_S
C
C Description:
C     La fonction CD2D_BSEN_ASMKT_S calcul la matrice tangente élémentaire
C     due à un élément de surface.
C     L'assemblage est fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     c.f. CD2D_BSEN_ASMKT_S
C
C     Le noeud opposé à l'élément de surface est perturbé en trop.
C     La matrice Ke peut être nulle.
C************************************************************************
      SUBROUTINE CD2D_BSEN_ASMKT_S(KLOCE,
     &                             VDLEV,
     &                             VKT,
     &                             VKT_TR,
     &                             VCORG,
     &                             KLOCN,
     &                             KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES, ! pas utilisé
     &                             KEIMP,
     &                             VDLG,
     &                             HMTX,
     &                             F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLEV)
      REAL*8   VDLEV (LM_CMMN_NDLEV)
      REAL*8   VKT   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VKT_TR(LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS) ! pas utilisé
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER NNEL_LCL
      INTEGER NDLE_LCL
      INTEGER NPRN_LCL
      INTEGER NPRE_LCL
      PARAMETER (NNEL_LCL =  3)
      PARAMETER (NDLE_LCL = 18)
      PARAMETER (NPRN_LCL = 18)
      PARAMETER (NPRE_LCL =  8)

      REAL*8  CD2D_PNUMR_DELPRT
      REAL*8  CD2D_PNUMR_DELMIN
      PARAMETER(CD2D_PNUMR_DELPRT = 1.0D-07)
      PARAMETER(CD2D_PNUMR_DELMIN = 1.0D-15)

      REAL*8  VPRNV(NPRN_LCL * NNEL_LCL)
      REAL*8  VRESE(NDLE_LCL), VRESEP(NDLE_LCL)
      REAL*8  VDL_ORI, VDL_DEL, VDL_INV
      INTEGER IERR
      INTEGER IN, ID, II, NO, I
      INTEGER IES, IEV, ICT
      INTEGER HVFT, HPRNE, HPREVE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNEL_LCL .GE. EG_CMMN_NNELV)
D     CALL ERR_PRE(NDLE_LCL .GE. LM_CMMN_NDLEV)
D     CALL ERR_PRE(NPRN_LCL .GE. LM_CMMN_NPRNO)
D     CALL ERR_PRE(NPRE_LCL .GE. LM_CMMN_NPREV)
C-----------------------------------------------------------------------

C-------  BOUCLE SUR LES ELEMENTS
C         =======================
      DO IES=1,EG_CMMN_NELS

C---        Élément parent et coté
         IEV = KNGS(3,IES)
         ICT = KNGS(4,IES)

C---        Table KLOCE de l'élément de volume
         II = 1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL ICOPY(LM_CMMN_NDLN, KLOCN(1,NO), 1, KLOCE(II), 1)
            II = II+LM_CMMN_NDLN
         ENDDO

C---        Transfert des DDL de volume
         II = 1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL DCOPY(LM_CMMN_NDLN, VDLG(1,NO), 1, VDLEV(II), 1)
            II = II+LM_CMMN_NDLN
         ENDDO

C---        Transfert des PRNO de volume
         II=1
         DO IN=1,EG_CMMN_NNELV
            NO = KNGV(IN,IEV)
            CALL DCOPY(LM_CMMN_NPRNO, VPRNO(1,NO), 1, VPRNV(II), 1)
            II=II+LM_CMMN_NPRNO
         ENDDO

C---        Initialise la matrice élémentaire
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT_TR,1)

C---        [K(u)]
         CALL CD2D_BSEN_ASMKE_S(VKT_TR,
     &                          VDJV(1,IEV),
     &                          VDJS(1,IES),
     &                          VPRGL,
     &                          VPRNV,
     &                          VPREV(1,IEV),
     &                          VPRES(1,IES), ! pas utilisé
     &                          VDLEV,
     &                          KEIMP(IES),
     &                          ICT)

C---        [K(u)]{u}
         CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &              LM_CMMN_NDLEV,     ! m rows
     &              LM_CMMN_NDLEV,     ! n columns
     &              UN,                ! alpha
     &              VKT_TR,            ! a
     &              LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &              VDLEV,             ! x
     &              1,                 ! incx
     &              ZERO,              ! beta
     &              VRESE,             ! y
     &              1)                 ! incy

C---        Initialise la matrice des dérivées
         CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKT,1)

C---        BOUCLE DE PERTURBATION SUR LES DDL
C           ==================================
         DO ID=1,LM_CMMN_NDLEV
            IF (KLOCE(ID) .EQ. 0) GOTO 199

C---           Perturbe le ddl
            VDL_ORI = VDLEV(ID)
            VDL_DEL = CD2D_PNUMR_DELPRT * VDLEV(ID)
     &              + SIGN(CD2D_PNUMR_DELMIN, VDLEV(ID))
            VDL_INV = 1.0D0 / VDL_DEL
            VDLEV(ID) = VDLEV(ID) + VDL_DEL

C---           Initialise la matrice de travail
            CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV, ZERO, VKT_TR, 1)

C---           [K(u + du_id)]
            CALL CD2D_BSEN_ASMKE_S(VKT_TR,
     &                             VDJV(1,IEV),
     &                             VDJS(1,IES),
     &                             VPRGL,
     &                             VPRNV,
     &                             VPREV(1,IEV),
     &                             VPRES(1,IES), ! pas utilisé
     &                             VDLEV,
     &                             KEIMP(IES),
     &                             ICT)

C---           [K(u + du_id)]{u+du_id}
            CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &                 LM_CMMN_NDLEV,     ! m rows
     &                 LM_CMMN_NDLEV,     ! n columns
     &                 UN,                ! alpha
     &                 VKT_TR,            ! a
     &                 LM_CMMN_NDLEV,     ! lda : vke(lda, 1)
     &                 VDLEV,             ! x
     &                 1,                 ! incx
     &                 ZERO,              ! beta
     &                 VRESEP,            ! y
     &                 1)                 ! incy

C---           ([K(u + du_id)]{u+du_id} - [K(u)]{u})/du
            DO I=1,LM_CMMN_NDLEV
               VKT(I,ID) = (VRESEP(I) - VRESE(I)) * VDL_INV
            ENDDO

C---           Restaure la valeur originale
            VDLEV(ID) = VDL_ORI

199         CONTINUE
         ENDDO

C---       Assemble de la matrice
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKT)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,*) 'ERR_CALCUL_MATRICE_K: ',IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

      ENDDO

      IF (.NOT. ERR_GOOD() .AND.
     &    ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
         CALL ERR_RESET()
      ENDIF

      RETURN
      END
     