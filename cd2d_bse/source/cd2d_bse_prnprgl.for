C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de transport-diffusion
C         de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIÉTÉS GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_PRNPRGL(NPRGL, VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(NPRGL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NPRGL .GE. 6)
C-----------------------------------------------------------------------

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_TRANSPORT_DIFFUSION_LUES:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DIFFU_MOLECULAIRE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DIFFU_CMULT_VERT',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DIFFU_CMULT_HORIZ',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DIFFU_CMULT_LONG',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DEFLT_CLIM',VPRGL(I))

      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_HTRIG',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_HMIN',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_CONV',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_CLIM',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_VDIMP',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_POROSITE',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_DIFFU_HORIZ',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_CMULT_LONG',VPRGL(I))

      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_PECLET',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_LAPIDUS',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_DMPG_VMIN',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_DMPG_VMAX',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_DMPG_STFF',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_STABI_DMPG_DLIN',VPRGL(I))

      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_CONV',VPRGL(I))
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_CMULT_DIVV',VPRGL(I))

      CALL LOG_DECIND()

C---     IMPRESSION DU TITRE
      I = LM_CMMN_NPRGLL
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_TRANSPORT_DIFFUSION_CALCULEES:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = I + 1
      IERR = CD2D_BSE_PRN1PRGL(I,'MSG_DECOU_TORTUOSITE',VPRGL(I))

      CALL LOG_DECIND()

      RETURN
      END
