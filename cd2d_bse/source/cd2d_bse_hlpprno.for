C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de transport-diffusion
C         de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_HLPPRNO
C
C Description:
C     Aide sur les propriétés nodales:
C     Propriétés nodales initiales:  1) u  2) v  3) H  4) Dv  5) Dh
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_HLPPRNO ()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_HLPPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'

      INTEGER I
      INTEGER IERR
C-----------------------------------------------------------------------

      I = 0

C---     IMPRESSION DE L'INFO
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('PROPRIETES NODALES DE TRANSPORT-DIFFUSION:')
      CALL LOG_INCIND()

      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'COMPOSANTE DE VITESSE U SUIVANT X;')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'COMPOSANTE DE VITESSE V SUIVANT Y;')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'PROFONDEUR H;')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'DIFFUSIVITE VERTICALE;')
      I = I + 1
      IERR = CD2D_BSE_HLP1(I, 'DIFFUSIVITE HORIZONTALE.')

      CALL LOG_DECIND()

      RETURN
      END

