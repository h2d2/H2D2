C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de transport-diffusion
C         de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_PRNCLIM
C
C Description:
C     CALCUL DES CONDITIONS AUX LIMITES
C
C Entrée:   KDIMP,VDIMP
C
C Sortie:   VDLG
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_BSE_PRNCLIM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRNCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER MP
C-----------------------------------------------------------------------

      MP = 6

      WRITE(MP,1000)
      WRITE(MP,1001)'DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:'
      WRITE(MP,1001)'CODE C.L.=1 CONDITION DE DIRICHLET;'
      WRITE(MP,1001)'CODE C.L.=2 CONDITION DE CAUCHY;'
      WRITE(MP,1001)'CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.'
1000  FORMAT(/)
1001  FORMAT(15X,A)

      RETURN
      END
