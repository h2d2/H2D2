C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM
C
C Description:
C     La sous-routine CD2D_BSE_PRCCLIM fait tout le traitement des
C     conditions limites qui ne dépend pas de VDLG. On réinitialise
C     les codes de KDIMP, contrôle la cohérence, traite les profondeurs
C     négatives, assigne les conditions sur les éléments de contour.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les conditions limites imposées doivent être de même type pour tous
C     les degrés de libertés considérés (pour toutes les variables simulées)
C************************************************************************
      SUBROUTINE CD2D_BSE_PRCCLIM(CD2D_BSE_PRCCLIM_CN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KCLCND,
     &                            VCLCNV,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE,
     &                            VCLDST,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRCCLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  CD2D_BSE_PRCCLIM_CN
      EXTERNAL CD2D_BSE_PRCCLIM_CN
      INTEGER KNGV  (EG_CMMN_NCELV, EG_CMMN_NELV)
      INTEGER KNGS  (EG_CMMN_NCELS, EG_CMMN_NELS)
      REAL*8  VDJV  (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VDJS  (EG_CMMN_NDJS,  EG_CMMN_NELS)
      REAL*8  VPRGL (LM_CMMN_NPRGL)
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VPREV (LM_CMMN_NPREV, EG_CMMN_NELV)
      REAL*8  VPRES (LM_CMMN_NPRES, EG_CMMN_NELS)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER KEIMP (EG_CMMN_NELS)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER CD2D_BSE_PRCCLIM_NS
      INTEGER CD2D_BSE_PRCCLIM_EC
      INTEGER NEQ, NCLT, ID, NCLTI, IN, ICOD
      INTEGER IL
C-----------------------------------------------------------------------

C---- INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'DESCRIPTION DES CODES DE CONDITIONS AUX LIMITES:'
C      WRITE(MP,1001)'CODE C.L.=1 CONDITION DE DIRICHLET;'
C      WRITE(MP,1001)'CODE C.L.=2 CONDITION DE CAUCHY;'
C      WRITE(MP,1001)'CODE C.L.=3 CONDITION DE FRONTIERE OUVERTE.'
C1000  FORMAT(/)
C1001  FORMAT(15X,A)

C---     INITIALISE
      NEQ  = LM_CMMN_NDLL
      NCLT = 0

C---     RESET LES C.L.
      CALL IINIT(LM_CMMN_NDLL,    0, KDIMP, 1)
      CALL IINIT(EG_CMMN_NELS,    0, KEIMP, 1)
      CALL DINIT(LM_CMMN_NDLL, ZERO, VDIMP, 1)

C---     Traite les profondeurs nulles ou négatives
      IF (ERR_GOOD()) THEN
!$omp master
         IERR = CD2D_BSE_PRCCLIM_NS(KNGV,
     &                              VPRNO,
     &                              KDIMP,
     &                              VDIMP)
!$omp end master
      ENDIF

C---     Boucle sur les limites pour assigner les codes et valeurs de condition
      DO IL=1, EG_CMMN_NCLLIM
         IERR = CD2D_BSE_PRCCLIM_CN(IL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP)
      ENDDO

C---     Contrôle les C.L. pour chaque type de DDL
      DO ID=1,LM_CMMN_NDLN
         NCLTI = 0
         DO IN=1,EG_CMMN_NNL
            IF (KDIMP(ID,IN) .NE. 0) NCLTI = NCLTI + 1
         ENDDO
         IF (NCLTI .EQ. 0) THEN
            WRITE(LOG_BUF,'(2A,I12)') 'MSG_DDL_NON_IMPOSE',': ',ID
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
         NCLT = NCLT + NCLTI
      ENDDO

C---     Contrôle la cohérence des C.L.
      IF (LM_CMMN_NDLN .GT. 1) THEN
         IERR = 0
         DO IN=1,EG_CMMN_NNL
            NCLTI = 0
            ICOD = KDIMP(1,IN)
            DO ID=2,LM_CMMN_NDLN
               IF (KDIMP(ID,IN) .NE. ICOD) NCLTI = NCLTI+1
            ENDDO
            IF (NCLTI .NE. 0) THEN
               WRITE(LOG_BUF,'(2A, I12)')
     &               'MSG_CLIM_MAL_IMPOSEES_SUR_NOEUD', ': ', IN
               CALL LOG_ECRIS(LOG_BUF)
               WRITE(ERR_BUF,'(A)') 'ERR_CLIM_MAL_IMPOSEES'
               CALL ERR_ASG(ERR_ERR, ERR_BUF)
            ENDIF
         ENDDO
         IF (ERR_BAD()) GOTO 9999
      ENDIF

C---     Assigne les C.L. sur les éléments de contour
      IF (ERR_GOOD()) THEN
!$omp master
         IERR = CD2D_BSE_PRCCLIM_EC(KNGV,
     &                              KNGS,
     &                              VDJV,
     &                              VDJS,
     &                              VPRGL,
     &                              VPRNO,
     &                              VPREV,
     &                              VPRES,
     &                              KDIMP,
     &                              KEIMP)
!$omp end master
      ENDIF

C---     Contrôle NEQ
      DO IN = 1,EG_CMMN_NNL
         DO ID=1,LM_CMMN_NDLN
            IF (BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)) NEQ = NEQ-1
         ENDDO
      ENDDO
      IF (NEQ. LE. 0) THEN
         WRITE(ERR_BUF,'(2A,I12)') 'ERR_CLIM_NBR_EQUATIONS',': ',NEQ
         CALL ERR_ASG(ERR_ERR, ERR_BUF)
         GOTO 9999
      ENDIF

9999  CONTINUE
      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_CN
C
C Description:
C     La fonction CD2D_BSE_PRCCLIM_CN fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.<p>
C     Elle peut être spécialisée par les héritiers.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_CN(IL,
     &                             VPRNO,
     &                             KCLCND,
     &                             VCLCNV,
     &                             KCLLIM,
     &                             KCLNOD,
     &                             KCLELE,
     &                             VCLDST,
     &                             KDIMP,
     &                             VDIMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_PRCCLIM_CN
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_CN
      INTEGER IL
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'

      INTEGER CD2D_BSE_PRCCLIM_001
      INTEGER CD2D_BSE_PRCCLIM_002
      INTEGER CD2D_BSE_PRCCLIM_003
      INTEGER CD2D_BSE_PRCCLIM_011
      INTEGER IC
      INTEGER IT
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
      IT = KCLCND(2, IC)
      IF (IT .EQ. 1) THEN
         IERR = CD2D_BSE_PRCCLIM_001(IL,
     &                               VPRNO,
     &                               KCLCND, VCLCNV,
     &                               KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                               KDIMP,  VDIMP)
      ELSEIF (IT .EQ. 2) THEN
         IERR = CD2D_BSE_PRCCLIM_002(IL,
     &                               VPRNO,
     &                               KCLCND, VCLCNV,
     &                               KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                               KDIMP,  VDIMP)
      ELSEIF (IT .EQ. 3) THEN
         IERR = CD2D_BSE_PRCCLIM_003(IL,
     &                               VPRNO,
     &                               KCLCND, VCLCNV,
     &                               KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                               KDIMP,  VDIMP)
      ELSEIF (IT .EQ. 11) THEN
         IERR = CD2D_BSE_PRCCLIM_011(IL,
     &                               VPRNO,
     &                               KCLCND, VCLCNV,
     &                               KCLLIM, KCLNOD, KCLELE, VCLDST,
     &                               KDIMP,  VDIMP)
      ENDIF

      CD2D_BSE_PRCCLIM_CN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_001
C
C Description:
C     La sous-routine privée CD2D_BSE_PRCCLIM_001 impose les
C     conditions de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_001(IL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_001
      INTEGER IL
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'eacdcl.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER I, IC, ID, IN, INDEB, INFIN, IV
C-----------------------------------------------------------------------
      LOGICAL DDL_ESTSEC
      DDL_ESTSEC(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_SEC)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 1)     ! (ITYP .EQ. 1)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. LM_CMMN_NDLN)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            IV = KCLCND(3,IC)
            DO ID=1,LM_CMMN_NDLN
               KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
               IF (DDL_ESTSEC(ID,IN)) THEN
                  VDIMP(ID,IN) = CD2D_DECOU_VDIMP
               ELSE
                  VDIMP(ID,IN) = VCLCNV(IV)
               ENDIF
               IV = IV + 1
            ENDDO
         ENDIF
      ENDDO

      CD2D_BSE_PRCCLIM_001 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_002
C
C Description:
C     La sous-routine privée CD2D_BSE_PRCCLIM_002 impose les
C     conditions de Cauchy sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_002(IL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_002
      INTEGER IL
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, ID, IN, INDEB, INFIN, IV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 2)     ! (ITYP .EQ. 2)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. LM_CMMN_NDLN)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            IV = KCLCND(3,IC)
            DO ID=1,LM_CMMN_NDLN
               KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_CAUCHY)
               VDIMP(ID,IN) = VCLCNV(IV)
               IV = IV + 1
            ENDDO
         ENDIF
      ENDDO

      CD2D_BSE_PRCCLIM_002 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_003
C
C Description:
C     La sous-routine privée CD2D_BSE_PRCCLIM_003 impose les
C     conditions de type Ouvert sur la limite <code>IL</code>.
C
C Entrée:
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_003(IL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_003
      INTEGER IL
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      INTEGER I, IC, ID, IN, INDEB, INFIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 3)     ! (ITYP .EQ. 3)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
!!!D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .EQ. 0)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            DO ID=1,LM_CMMN_NDLN
               KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_OUVERT)
               VDIMP(ID,IN) = 0.0D0
            ENDDO
         ENDIF
      ENDDO

      CD2D_BSE_PRCCLIM_003 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_011
C
C Description:
C     La sous-routine privée CD2D_BSE_PRCCLIM_011 impose les
C     conditions de Dirichlet sur la limite <code>IL</code>.
C
C Entrée:
C     VPRNO          Table des PRopriétés NOdales
C     KCLCND         Liste des conditions
C     VCLCNV         Valeurs associées aux conditions
C     KCLLIM         Liste des limites
C     KCLNOD         Noeuds des limites
C     KCLELE         Éléments des limites
C
C Sortie:
C     KDIMP          Codes des DDL imposés
C     VDIMP          Valeurs des DDL imposés
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_011(IL,
     &                              VPRNO,
     &                              KCLCND,
     &                              VCLCNV,
     &                              KCLLIM,
     &                              KCLNOD,
     &                              KCLELE,
     &                              VCLDST,
     &                              KDIMP,
     &                              VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_011
      INTEGER IL
      REAL*8  VPRNO (LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8  VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)
      INTEGER KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)

      INCLUDE 'err.fi'

      REAL*8  H
      INTEGER I, IC, ID, IN, INDEB, INFIN, IV

      INTEGER IPH
      PARAMETER (IPH = 3)     ! Indices dans VPRNO
C-----------------------------------------------------------------------
D     CALL ERR_PRE(KCLCND(2,KCLLIM(2,IL)) .EQ. 11)    ! (ITYP .EQ. 11)
C-----------------------------------------------------------------------

      IC = KCLLIM(2, IL)
D     CALL ERR_ASR(KCLCND(4,IC)-KCLCND(3,IC)+1 .GE. LM_CMMN_NDLN)

      INDEB = KCLLIM(3, IL)
      INFIN = KCLLIM(4, IL)
      DO I = INDEB, INFIN
         IN = KCLNOD(I)
         IF (IN .GT. 0) THEN
            IV = KCLCND(3,IC)
            H  = VPRNO(IPH,IN)
            DO ID=1,LM_CMMN_NDLN
               KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
               VDIMP(ID,IN) = H * VCLCNV(IV)
               IV = IV + 1
            ENDDO
         ENDIF
      ENDDO

      CD2D_BSE_PRCCLIM_011 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_NS
C
C Description:
C     La sous-routine privée CD2D_BSE_PRCCLIM_NS flag les noeuds secs,
C     noeuds qui ne sont pas liés à un éléments couvert ou à un élément
C     de transition.
C     Si le type de CL de découvrement est Dirichlet, elle impose
C     les concentrations à la valeur de découvrement.
C
C Entrée:
C     KNGV        Connectivités de volume
C     VPRNO       Propriétés nodales
C     KDIMP       Codes des DDL imposés
C     VDIMP       Valeur des DDL imposés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_NS(KNGV,
     &                             VPRNO,
     &                             KDIMP,
     &                             VDIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_NS
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN,  EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER ID, IE, IN, IPH, ISEC, NO1, NO2, NO3
      REAL*8  PROF, PROF1, PROF2, PROF3
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPH = 3

C---     Flag les noeuds secs
      DO IN=1,EG_CMMN_NNL
         PROF = VPRNO(IPH,IN)
         IF (PROF .LE. CD2D_DECOU_HMIN) THEN
            DO ID=1,LM_CMMN_NDLN
               KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_SEC)
            ENDDO
         ENDIF
      ENDDO

C---     Libère les noeuds de transition
      DO IE=1,EG_CMMN_NELV
         NO1   = KNGV(1,IE)
         NO2   = KNGV(2,IE)
         NO3   = KNGV(3,IE)
         PROF1 = VPRNO(IPH,NO1)
         PROF2 = VPRNO(IPH,NO2)
         PROF3 = VPRNO(IPH,NO3)
         ISEC  = 0
         IF (PROF1 .GT. CD2D_DECOU_HMIN) ISEC = ISEC + 1
         IF (PROF2 .GT. CD2D_DECOU_HMIN) ISEC = ISEC + 1
         IF (PROF3 .GT. CD2D_DECOU_HMIN) ISEC = ISEC + 1
         IF (ISEC .EQ. 0) GOTO 199        ! ELEMENT SEC
         IF (ISEC .EQ. 3) GOTO 199        ! ELEMENT MOUILLE
         CONTINUE                         ! ELEMENT DE TRANSITION
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,NO1) = IBCLR(KDIMP(ID,NO1), EA_TPCL_SEC)
            KDIMP(ID,NO2) = IBCLR(KDIMP(ID,NO2), EA_TPCL_SEC)
            KDIMP(ID,NO3) = IBCLR(KDIMP(ID,NO3), EA_TPCL_SEC)
         ENDDO
199      CONTINUE
      ENDDO

      IF (NINT(CD2D_DECOU_CLIM) .NE. 1) GOTO 9999

C---     Impose les noeuds secs comme CL de Dirichlet
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_LST_NOEUDS_SECS_IMPOSES'
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      ISEC = 0
      DO IN=1,EG_CMMN_NNL
         !PROF = VPRNO(IPH,IN)
         !IF (PROF .GT. CD2D_DECOU_HMIN) GOTO 200
         IF (.NOT. BTEST(KDIMP(1,IN), EA_TPCL_SEC)) GOTO 200
         DO ID=1,LM_CMMN_NDLN
            KDIMP(ID,IN) = IBSET(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
            VDIMP(ID,IN) = CD2D_DECOU_VDIMP
         ENDDO
         ISEC = ISEC + 1
         IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
            WRITE(LOG_BUF,'(15X,I12)') IN
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
200      CONTINUE
      ENDDO
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         WRITE(LOG_BUF,'(A,I12)') 'MSG_CL_DIRICHLET_NOEUDS_SECS:', ISEC
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF
      IF (ISEC .EQ. EG_CMMN_NNL) GOTO 9900

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOEUDS_TOUS_SECS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CD2D_BSE_PRCCLIM_NS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  CD2D_BSE_PRCCLIM_EC
C
C Description:
C     La fonction privée CD2D_BSE_PRCCLIM_EC assigne les codes
C     des éléments de contour en fonction des C.L. imposées aux
C     noeuds.
C
C Entrée:
C
C Sortie:   KNG(), VDJ() et VPREG()
C
C Notes:    VDJ (1)TXN  (2)TYN  (3)Le  (4)QN1  (5)QN2
C           *** CONCENTRATION NULLE SI NOEUD DÉCOUVERT
C
C     Les conditions limites imposées doivent être de même type pour tous
C     les degrés de libertés considérés (pour toutes les variables simulées)
C     2) La bascule en Neumann des éléments couverts semble faire
C        plus de mal que de bien
C************************************************************************
      FUNCTION CD2D_BSE_PRCCLIM_EC(KNGV,
     &                             KNGS,
     &                             VDJV,
     &                             VDJS,
     &                             VPRGL,
     &                             VPRNO,
     &                             VPREV,
     &                             VPRES,
     &                             KDIMP,
     &                             KEIMP)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER CD2D_BSE_PRCCLIM_EC
      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KNGS (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8  VDJV (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8  VDJS (EG_CMMN_NDJV, EG_CMMN_NELS)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8  VPREV(LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8  VPRES(LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KEIMP(EG_CMMN_NELS)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cd2d_bse.fc'

      CHARACTER*(48) TYPCL
      LOGICAL ESTCAUCHY
      LOGICAL ESTOUVERT
      LOGICAL ESTENTRANT
      LOGICAL ESTCOUVERT
      LOGICAL ESTLIBRE
      LOGICAL DOCL_DECOU

      INTEGER NELC
      INTEGER IES, IEV, IPH
      INTEGER ICL_DECOU, ICL_DEFLT
      INTEGER ICOD
      INTEGER NP1, NP2
      REAL*8  HMIN, H1, H2, QN1, QN2
C-----------------------------------------------------------------------
      INTEGER IN, ID
      LOGICAL DDL_ESTDRCHLT
      LOGICAL DDL_ESTCAUCHY
      LOGICAL DDL_ESTOUVERT
      LOGICAL DDL_ESTLIBRE
      DDL_ESTDRCHLT(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET)
      DDL_ESTCAUCHY(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_CAUCHY)
     &          .AND. (.NOT. BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET))
      DDL_ESTOUVERT(ID,IN) = BTEST(KDIMP(ID,IN), EA_TPCL_OUVERT)
     &          .AND. (.NOT. BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET))
      DDL_ESTLIBRE(ID,IN) =
     &                (.NOT. BTEST(KDIMP(ID,IN), EA_TPCL_DIRICHLET))
     &          .AND. (.NOT. BTEST(KDIMP(ID,IN), EA_TPCL_CAUCHY))
     &          .AND. (.NOT. BTEST(KDIMP(ID,IN), EA_TPCL_OUVERT))
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IPH  = 3
      HMIN = CD2D_DECOU_HMIN
      ICL_DECOU = NINT(CD2D_DECOU_CLIM)
      ICL_DEFLT = NINT(CD2D_DEFLT_CLIM)

C---     Assigne les conditions par défaut Cauchy-Ouvert
      ICOD = -1
      IF     (ICL_DEFLT .EQ. 1) THEN
         ICOD = EA_TPCL_CAUCHY
      ELSEIF (ICL_DEFLT .EQ. 2) THEN
         ICOD = EA_TPCL_OUVERT
      ENDIF
      IF (ICOD .NE. -1) THEN
         DO IES=1,EG_CMMN_NELS
            NP1 = KNGS(1,IES)
            NP2 = KNGS(2,IES)
            ESTLIBRE = DDL_ESTLIBRE(1,NP1)
            IF (ESTLIBRE) KDIMP(1,NP1) = IBSET(KDIMP(1,NP1), ICOD)
            ESTLIBRE = DDL_ESTLIBRE(1,NP2)
            IF (ESTLIBRE) KDIMP(1,NP2) = IBSET(KDIMP(1,NP2), ICOD)
         ENDDO
      ENDIF

C---     Boucle sur les éléments de surface
C        ==================================
      DOCL_DECOU = (ICL_DECOU .EQ. 3 .OR. ICL_DECOU .EQ. 13)   ! Pass-through
      NELC = 0
      DO IES=1,EG_CMMN_NELS
         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)
         IEV = KNGS(3,IES)

         H1  = VPRNO(IPH,NP1)
         H2  = VPRNO(IPH,NP2)
         QN1 = VPRES(1,IES)
         QN2 = VPRES(2,IES)

C---        Les états
         ESTCOUVERT = DOCL_DECOU .OR. (H1 .GT. HMIN .OR. H2 .GT. HMIN)

C---        Les limites de CAUCHY
         ESTCAUCHY  = (DDL_ESTCAUCHY(1,NP1) .AND. DDL_ESTCAUCHY(1,NP2))
         ESTENTRANT = (QN1 .LE. ZERO .OR. QN2 .LE. ZERO)
         IF (ESTCOUVERT .AND. ESTCAUCHY .AND. ESTENTRANT) THEN
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_CAUCHY)
            IF (ESTENTRANT) THEN
               KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_ENTRANT)
            ELSE
               KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_SORTANT)
            ENDIF
         ENDIF

C---        Les limites "OUVERT" - ENTRANT
         ESTOUVERT  = (DDL_ESTOUVERT(1,NP1) .AND. DDL_ESTOUVERT(1,NP2))
         ESTENTRANT = (QN1 .LT. ZERO .AND. QN2 .LT. ZERO)
         IF (ESTCOUVERT .AND. ESTOUVERT .AND. ESTENTRANT) THEN
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_OUVERT)
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_ENTRANT)
         ENDIF

C---        Les limites "OUVERT" - SORTANT
         ESTOUVERT  = (DDL_ESTOUVERT(1,NP1) .OR.  DDL_ESTOUVERT(1,NP2))
         ESTENTRANT = (QN1 .LT. ZERO .AND. QN2 .LT. ZERO)
         IF (ESTCOUVERT .AND. ESTOUVERT .AND. .NOT. ESTENTRANT) THEN
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_OUVERT)
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_SORTANT)
         ENDIF

C---        Les limites par défaut: intégration du terme de contour (i.e. ouvert-sortant)
         ESTOUVERT = (ICL_DEFLT .EQ. -1)
         IF (DOCL_DECOU .AND. ESTOUVERT .AND. KEIMP(IES) .EQ. 0) THEN
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_OUVERT)
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_SORTANT)
          ENDIF

C---        Les limites par défaut: intégration du terme de contour (i.e. ouvert-sortant)
         ESTOUVERT = (ICL_DEFLT .EQ. -1)
         IF (DOCL_DECOU .AND. ESTOUVERT .AND. KEIMP(IES) .EQ. 0) THEN
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_OUVERT)
            KEIMP(IES) = IBSET(KEIMP(IES), EA_TPCL_SORTANT)
          ENDIF

      ENDDO

C---     Log des connectivités de la peau des flux de CAUCHY(ENTRE),
C        OUVERT(NUL+SORT), DIRICHLET+NEUMAN(DÉFAUT)
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         CALL LOG_ECRIS(' ')
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,'(A)') 'MSG_TYPE_CLIM_ELEMENTS_NOEUDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()
         DO IES=1,EG_CMMN_NELS

            IF (BTEST(KEIMP(IES), EA_TPCL_CAUCHY) .AND.
     &          BTEST(KEIMP(IES), EA_TPCL_SORTANT)) THEN
               TYPCL = 'MSG_CL_CAUCHY_SORTANT-->NEUMANN#<40>#'
            ELSEIF (BTEST(KEIMP(IES), EA_TPCL_CAUCHY) .AND.
     &              BTEST(KEIMP(IES), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_CAUCHY_ENTRANT#<40>#'
            ELSEIF (BTEST(KEIMP(IES), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IES), EA_TPCL_ENTRANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_ENTRANT-->CAUCHY#<40>#'
            ELSEIF (BTEST(KEIMP(IES), EA_TPCL_OUVERT) .AND.
     &              BTEST(KEIMP(IES), EA_TPCL_SORTANT)) THEN
               TYPCL = 'MSG_CL_OUVERT_SORTANT#<40>#'
            ELSE
               TYPCL = 'MSG_CL_DEFAUT#<40>#'
            ENDIF

            WRITE(LOG_BUF,'(A,2X,I9,3X,2(1X,I9))')
     &            TYPCL(1:SP_STRN_LEN(TYPCL)),
     &            IES, KNGS(1,IES), KNGS(2,IES)
            CALL LOG_ECRIS(LOG_BUF)

!!            IEV = KNGS(3,IES)
!!            WRITE(LOG_BUF,2050) IEV,KNGV(1,IEV),KNGV(2,IEV),KNGV(3,IEV)
!!            CALL LOG_ECRIS(LOG_BUF)

         ENDDO
         CALL LOG_DECIND()
      ENDIF

C---  IMPRESSION DE L'INFO
C      WRITE(MP,1000)
C      WRITE(MP,1001)'TRAITEMENT DES ELEMENTS DE CONTOUR:'
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DE CAUCHY       =',
C     &               NELC
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. OUVERT          =',
C     &               NELS
C      WRITE(MP,1002)'NOMBRE D''ELEMENTS DE C.L. DIRICHLET-NEUMAN=',
C     &               EG_CMMN_NELS-NELC-NELS
C      WRITE(MP,1000)

C---------------------------------------------------------------
C1000  FORMAT(/)
C1001  FORMAT(15X,A)
C1002  FORMAT(15X,A,I12)
C2050  FORMAT(2X,'ELEMENT PARENT',I12,
C     &       2X,'CONNECTIVITE DU PARENT',3(1X,I12))
C---------------------------------------------------------------

      CD2D_BSE_PRCCLIM_EC = ERR_TYP()
      RETURN
      END
