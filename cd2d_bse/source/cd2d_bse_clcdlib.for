C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSE_CLCDLIB
C
C Description:
C     La fonction CD2D_BSE_CLCDLIB
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSE_CLCDLIB(KNGV,
     &                            KLOCN,
     &                            KDIMP,
     &                            VDIMP,
     &                            VDLG,
     &                            IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSE_CLCDLIB
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER KNGV (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER KLOCN(LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER KDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDIMP(LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      IERR = ERR_TYP()
      RETURN
      END
