C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D.
C                         FORMULATION CONSERVATIVE POUR (HC).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes: Fichier contenant les subroutines de base pour le calcul de
C        transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire: CD2D_BSEC_ASMK
C
C Description:
C     La fonction CD2D_BSEC_ASMK calcule le matrice de rigidité
C     élémentaire. L'assemblage de la matrice globale est fait
C     par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On ne peut pas paralléliser l'assemblage des surfaces car elles ne
C     sont pas coloriées
C************************************************************************
      SUBROUTINE CD2D_BSEC_ASMK(VCORG,
     &                          KLOCN,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VPRGL,
     &                          VPRNO,
     &                          VPREV,
     &                          VPRES,
     &                          VSOLC,
     &                          VSOLR,
     &                          KCLCND,
     &                          VCLCNV,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST,
     &                          KDIMP,
     &                          VDIMP,
     &                          KEIMP,
     &                          VDLG,
     &                          HMTX,
     &                          F_ASM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_BSEC_ASMK
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLC (LM_CMMN_NSOLC,EG_CMMN_NNL)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      INTEGER  KCLCND( 4, LM_CMMN_NCLCND)
      REAL*8   VCLCNV(    LM_CMMN_NCLCNV)
      INTEGER  KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER  KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER  KCLELE(    EG_CMMN_NCLELE)
      REAL*8   VCLDST(    EG_CMMN_NCLNOD)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'cd2d_bse.fi'

      REAL*8  VKE  (CD2D_BSE_NDLEMAX, CD2D_BSE_NDLEMAX)
      REAL*8  VDLE (CD2D_BSE_NDLEMAX)
      INTEGER KLOCE(CD2D_BSE_NDLEMAX)
      INTEGER IERR
C----------------------------------------------------------------
D     CALL ERR_PRE(LM_CMMN_NDLN .LE. CD2D_BSE_NDLNMAX)
C-----------------------------------------------------------------

      IERR = ERR_OK

      IF (ERR_GOOD()) THEN
!$omp  parallel
!$omp& default(shared)
!$omp& private(IERR)
!$omp& private(VKE, VDLE, KLOCE)
         CALL CD2D_BSEC_ASMK_V(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         VSOLR,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)
         IERR = ERR_OMP_RDC()
!$omp end parallel
      ENDIF

      IF (ERR_GOOD())
     &   CALL CD2D_BSEC_ASMK_S(KLOCE,
     &                         VDLE,
     &                         VKE,
     &                         VCORG,
     &                         KLOCN,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VPRGL,
     &                         VPRNO,
     &                         VPREV,
     &                         VPRES,
     &                         KDIMP,
     &                         VDIMP,
     &                         KEIMP,
     &                         VDLG,
     &                         HMTX,
     &                         F_ASM)

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEC_ASMK_V
C
C Description:
C     La fonction CD2D_BSEC_ASMK_V calcule le matrice de rigidité
C     élémentaire due aux éléments de volume. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEC_ASMK_V(KLOCE,
     &                            VDLE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            VSOLR,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      REAL*8   VSOLR (LM_CMMN_NSOLR,EG_CMMN_NNL)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fc'

      REAL*8  VKX, VEX, VSX, VKY, VEY, VSY, DETJ
      REAL*8  CPEC, CLAP, DISP, CONV, SOLR, SPEC, SLAP
      REAL*8  DELX, DELY
      REAL*8  HC1, HC2, HC3, H1, H2, H3, HU1, HU2, HU3, HV1, HV2, HV3
      REAL*8  HKXX1, HKXY1, HKYY1
      REAL*8  HKXX2, HKXY2, HKYY2
      REAL*8  HKXX3, HKXY3, HKYY3
      REAL*8  SHU, SHV, ASHU, ASHV, SHKXX, SHKXY, SHKYY
      REAL*8  DFX1, DFX2, DFX3, DFY1, DFY2, DFY3
      REAL*8  CUX1, CUX2, CUX3, CUY1, CUY2, CUY3
      REAL*8  STBX, STBY, STBX1, STBX2, STBX3, STBY1, STBY2, STBY3
      REAL*8  HCX, HCY, DCNRM
      REAL*8  SOL11,SOL12,SOL13, SOL22,SOL23, SOL33
      REAL*8  VKEX1,VKEY1, VKEX2,VKEY2, VKEX3,VKEY3
      REAL*8  VKE11,VKE21,VKE31, VKE12,VKE22,VKE32, VKE13,VKE23,VKE33
      REAL*8  QH1, QH2, QH3, SQH
      REAL*8  DIVQ, SDIV
      INTEGER IERR
      INTEGER IC, IE, ID, ID1, ID2, ID3
      INTEGER IPH, IPHU, IPHV
      INTEGER NO1, NO2, NO3
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Indices dans VPRNO
      IPH  = 3
      IPHU = LM_CMMN_NPRNOL + 4
      IPHV = LM_CMMN_NPRNOL + 5

C---     Prop. globales
      CPEC = UN / CD2D_STABI_PECLET    ! Peclet
      CLAP = CD2D_STABI_LAPIDUS        ! Lapidus

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     Boucle sur les éléments
C        =======================
      DO 10 IC=1,EG_CMMN_NELCOL
!$omp do
      DO 20 IE=EG_CMMN_KELCOL(1,IC),EG_CMMN_KELCOL(2,IC)

C---        CONNECTIVITES DU T3
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         NO3  = KNGV(3,IE)

C---        MÉTRIQUES DU T3
         VKX  = VDJV(1,IE)
         VEX  = VDJV(2,IE)
         VKY  = VDJV(3,IE)
         VEY  = VDJV(4,IE)
         DETJ = VDJV(5,IE)
         VSX  = -(VKX+VEX)
         VSY  = -(VKY+VEY)

C---        Constantes dépendant du volume
         CONV  = UN_24*CD2D_CMULT_CONV ! Convection
         DISP  = UN_6/DETJ             ! Dispersion
         SDIV  = UN_24*CD2D_CMULT_DIVV ! Divergence
         SOLR  = UN_120*DETJ           ! Sol. réparties
         SPEC  = UN_6*CPEC/DETJ        ! Stabilisation Peclet
         SLAP  = UN_2*CLAP/DETJ        ! Stabilisation Lapidus

C---        Les propriétés nodales
         H1    = VPRNO(IPH,  NO1)
         HU1   = VPRNO(IPHU, NO1)
         HV1   = VPRNO(IPHV, NO1)
         H2    = VPRNO(IPH,  NO2)
         HU2   = VPRNO(IPHU, NO2)
         HV2   = VPRNO(IPHV, NO2)
         H3    = VPRNO(IPH,  NO3)
         HU3   = VPRNO(IPHU, NO3)
         HV3   = VPRNO(IPHV, NO3)

C---        Les propriétés élémentaires
         DELX  = VPREV(1,IE)
         DELY  = VPREV(2,IE)
         SHKXX = VPREV(4,IE)
         SHKXY = VPREV(5,IE)
         SHKYY = VPREV(6,IE)

C---        Sommes
         SHU = HU1 + HU2 + HU3
         SHV = HV1 + HV2 + HV3
         ASHU = ABS(HU1) + ABS(HU2) + ABS(HU3)
         ASHV = ABS(HV1) + ABS(HV2) + ABS(HV3)

C---        Convection
         CUX1 = CONV*(SHU+HU1)
         CUX2 = CONV*(SHU+HU2)
         CUX3 = CONV*(SHU+HU3)
         CUY1 = CONV*(SHV+HV1)
         CUY2 = CONV*(SHV+HV2)
         CUY3 = CONV*(SHV+HV3)

C---        Dispersion
         DFX1 = DISP*(SHKXX*VSX + SHKXY*VSY)
         DFX2 = DISP*(SHKXX*VKX + SHKXY*VKY)
         DFX3 = DISP*(SHKXX*VEX + SHKXY*VEY)
         DFY1 = DISP*(SHKXY*VSX + SHKYY*VSY)
         DFY2 = DISP*(SHKXY*VKX + SHKYY*VKY)
         DFY3 = DISP*(SHKXY*VEX + SHKYY*VEY)

C---        Stabilisation de la convection (Peclet)
         STBX  = SPEC * DELX * ASHU
         STBY  = SPEC * DELY * ASHV
         STBX1 = STBX * VSX
         STBX2 = STBX * VKX
         STBX3 = STBX * VEX
         STBY1 = STBY * VSY
         STBY2 = STBY * VKY
         STBY3 = STBY * VEY

C---        Divergence du débit
         DIVQ = 0.0D0
!         DIVQ = SDIV * (VKX*(HU2-HU1) + VEX*(HU3-HU1) +
!     &                  VKY*(HV2-HV1) + VEY*(HV3-HV1))

C---        Termes communs à tous les ddl
         VKEX1 = CUX1 + STBX1
         VKEX2 = CUX2 + STBX2
         VKEX3 = CUX3 + STBX3
         VKEY1 = CUY1 + STBY1
         VKEY2 = CUY2 + STBY2
         VKEY3 = CUY3 + STBY3

         VKE11 = (VKEX1 + DFX1/H1)*VSX + (VKEY1 + DFY1/H1)*VSY
         VKE21 = (VKEX2 + DFX2/H1)*VSX + (VKEY2 + DFY2/H1)*VSY
         VKE31 = (VKEX3 + DFX3/H1)*VSX + (VKEY3 + DFY3/H1)*VSY
         VKE12 = (VKEX1 + DFX1/H2)*VKX + (VKEY1 + DFY1/H2)*VKY
         VKE22 = (VKEX2 + DFX2/H2)*VKX + (VKEY2 + DFY2/H2)*VKY
         VKE32 = (VKEX3 + DFX3/H2)*VKX + (VKEY3 + DFY3/H2)*VKY
         VKE13 = (VKEX1 + DFX1/H3)*VEX + (VKEY1 + DFY1/H3)*VEY
         VKE23 = (VKEX2 + DFX2/H3)*VEX + (VKEY2 + DFY2/H3)*VEY
         VKE33 = (VKEX3 + DFX3/H3)*VEX + (VKEY3 + DFY3/H3)*VEY


C---        Boucle sur les DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN

C---           Degrés de liberté (pour Lapidus)
            HC1 = VDLG(ID, NO1)
            HC2 = VDLG(ID, NO2)
            HC3 = VDLG(ID, NO3)
            HCX = (VSX*HC1 + VKX*HC2 + VEX*HC3)
            HCY = (VSY*HC1 + VKY*HC2 + VEY*HC3)

C---           Stabilisation de Lapidus
            DCNRM = SLAP * MAX(HYPOT(HCX, HCY), 1.0D-12)
            STBX1 = DCNRM * VSX
            STBX2 = DCNRM * VKX
            STBX3 = DCNRM * VEX
            STBY1 = DCNRM * VSY
            STBY2 = DCNRM * VKY
            STBY3 = DCNRM * VEY

C---           Sollicitations réparties (Q en entrée)
            QH1   = SOLR * VSOLR(ID,NO1)
            QH2   = SOLR * VSOLR(ID,NO2)
            QH3   = SOLR * VSOLR(ID,NO3)
            SQH   = DEUX*(QH1 + QH2 + QH3)
            SOL11 = (SQH + QUATRE*QH1) + DIVQ+DIVQ
            SOL12 = (SQH - QH3)        + DIVQ
            SOL13 = (SQH - QH2)        + DIVQ
            SOL22 = (SQH + QUATRE*QH2) + DIVQ+DIVQ
            SOL23 = (SQH - QH1)        + DIVQ
            SOL33 = (SQH + QUATRE*QH3) + DIVQ+DIVQ

C---           Assemblage des contributions
            VKE(ID1,ID1) = VKE11 + (SOL11 + VSX*STBX1 + VSY*STBY1)
            VKE(ID2,ID1) = VKE21 + (SOL12 + VKX*STBX1 + VKY*STBY1)
            VKE(ID3,ID1) = VKE31 + (SOL13 + VEX*STBX1 + VEY*STBY1)
            VKE(ID1,ID2) = VKE12 + (SOL12 + VSX*STBX2 + VSY*STBY2)
            VKE(ID2,ID2) = VKE22 + (SOL22 + VKX*STBX2 + VKY*STBY2)
            VKE(ID3,ID2) = VKE32 + (SOL23 + VEX*STBX2 + VEY*STBY2)
            VKE(ID1,ID3) = VKE13 + (SOL13 + VSX*STBX3 + VSY*STBY3)
            VKE(ID2,ID3) = VKE23 + (SOL23 + VKX*STBX3 + VKY*STBY3)
            VKE(ID3,ID3) = VKE33 + (SOL33 + VEX*STBX3 + VEY*STBY3)
         ENDDO

C---        Terme petit sur la diagonale si zero
         DO ID=1,LM_CMMN_NDLEV
            IF (VKE(ID,ID) .EQ. ZERO) VKE(ID,ID) = DETJ*PETIT
         ENDDO

C---        Table KLOCE de localisation des DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NO1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NO2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NO3)
         ENDDO

C---        Assemblage de la matrice
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_V',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_ELEM',': ',IE
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

20    CONTINUE
!$omp end do
10    CONTINUE

      RETURN
      END

C************************************************************************
C Sommaire: CD2D_BSEC_ASMK_S
C
C Description:
C     La fonction CD2D_BSEC_ASMK_S calcule le matrice de rigidité
C     élémentaire due aux éléments de surface. L'assemblage est
C     fait par call-back à la fonction paramètre F_ASM.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_BSEC_ASMK_S(KLOCE,
     &                            VDLE,
     &                            VKE,
     &                            VCORG,
     &                            KLOCN,
     &                            KNGV,
     &                            KNGS,
     &                            VDJV,
     &                            VDJS,
     &                            VPRGL,
     &                            VPRNO,
     &                            VPREV,
     &                            VPRES,
     &                            KDIMP,
     &                            VDIMP,
     &                            KEIMP,
     &                            VDLG,
     &                            HMTX,
     &                            F_ASM)

      IMPLICIT NONE

      INCLUDE 'eacdcl.fi'
      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER  KLOCE (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VDLE  (LM_CMMN_NDLN, LM_CMMN_NNELV)
      REAL*8   VKE   (LM_CMMN_NDLEV,LM_CMMN_NDLEV)
      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KLOCN (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VPRGL (LM_CMMN_NPRGL)
      REAL*8   VPRNO (LM_CMMN_NPRNO,EG_CMMN_NNL)
      REAL*8   VPREV (LM_CMMN_NPREV,EG_CMMN_NELV)
      REAL*8   VPRES (LM_CMMN_NPRES,EG_CMMN_NELS)
      INTEGER  KDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      REAL*8   VDIMP (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  KEIMP (EG_CMMN_NELS)
      REAL*8   VDLG  (LM_CMMN_NDLN, EG_CMMN_NNL)
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER IPH
      INTEGER IES, IEV
      INTEGER ID, ID1, ID2, ID3
      INTEGER NO1, NO2, NO3, NP1, NP2, NP3
      REAL*8  H1, H2, H3
      REAL*8  VSX, VKX, VEX, VNX
      REAL*8  VSY, VKY, VEY, VNY
      REAL*8  DETJL2, DETJT3
      REAL*8  QN1, QN2, SQN
      REAL*8  DISC, REP
      REAL*8  SHKXX1, SHKXY1, SHKYY1
      REAL*8  SHKXX2, SHKXY2, SHKYY2
      REAL*8  DIF11, DIF12, DIF13, DIF21, DIF22, DIF23
      REAL*8  VKE11, VKE21, VKE12, VKE22, VKE13, VKE23
C-----------------------------------------------------------------------
      INTEGER IE
      LOGICAL ELE_ESTTYPE1
      LOGICAL ELE_ESTTYPE2
      ELE_ESTTYPE1(IE) = BTEST(KEIMP(IE), EA_TPCL_ENTRANT) .AND.
     &                   (BTEST(KEIMP(IE), EA_TPCL_CAUCHY) .OR.
     &                    BTEST(KEIMP(IE), EA_TPCL_OUVERT))
      ELE_ESTTYPE2(IE) = BTEST(KEIMP(IE), EA_TPCL_SORTANT) .AND.
     &                   BTEST(KEIMP(IE), EA_TPCL_OUVERT)
C-----------------------------------------------------------------------

C---     Indices dans vprno
      IPH   = 3

C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR DE CAUCHY "ENTRANTS"
C        ================================================================
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE1(IES)) GOTO 199

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)

C---        Constante dépendant du volume
         DETJL2 = VDJS(3,IES)
         REP = -UN_6*DETJL2

C---        Flux normal (Vn*H)
         QN1 = REP*VPRES(1,IES)
         QN2 = REP*VPRES(2,IES)
         SQN = QN1 + QN2

C---        Matrice de rigidite élémentaire de CAUCHY-ENTRANT
!         VKE11 = SQN + QN1+QN1
!         VKE21 = SQN
!         VKE12 = SQN
!         VKE22 = SQN + QN2+QN2
         VKE11 = SQN+SQN + QN1+QN1    ! Matrice lumpée
         VKE22 = SQN+SQN + QN2+QN2

C---        Boucle sur les ddl
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN

            VKE(ID1,ID1) = VKE11
!            VKE(ID2,ID1) = VKE21
!            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
         ENDDO

C---        Table KLOCE de localisation des ddls
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO

C---        ASSEMBLAGE DE LA MATRICE
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_S1',': ',IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (ERR_BAD()) THEN
D           IF (ERR_ESTMSG('ERR_MATRICE_ELEMENTAIRE_NULLE')) THEN
D              CALL ERR_RESET()
D           ELSE
D              WRITE(LOG_BUF,'(2A,I9)')
D    &            'ERR_CALCUL_MATRICE_K_CAUCHY_ENTRANT', ': ', IES
D              CALL LOG_ECRIS(LOG_BUF)
D           ENDIF
D        ENDIF

199      CONTINUE
      ENDDO


C---     Initialise
      CALL DINIT(LM_CMMN_NDLEV*LM_CMMN_NDLEV,ZERO,VKE,1)
      CALL DINIT(LM_CMMN_NDLEV,ZERO,VDLE, 1)
      CALL IINIT(LM_CMMN_NDLEV,   0,KLOCE,1)

C---     BOUCLE POUR ASSEMBLER LES TERMES DE CONTOUR OUVERTS "SORTANTS"
C        ==============================================================
      DO IES=1,EG_CMMN_NELS
         IF (.NOT. ELE_ESTTYPE2(IES)) GOTO 299

         NP1 = KNGS(1,IES)
         NP2 = KNGS(2,IES)
         IEV = KNGS(3,IES)
         NO1 = KNGV(1,IEV)
         NO2 = KNGV(2,IEV)
         NO3 = KNGV(3,IEV)

C---        Recherche le troisième noeud de l'élément de volume
         IF    (NP1 .EQ. NO1 .AND. NP2 .EQ. NO2) THEN
            NP3 = NO3
         ELSEIF(NP1 .EQ. NO2 .AND. NP2 .EQ. NO3) THEN
            NP3 = NO1
         ELSEIF(NP1 .EQ. NO3 .AND. NP2 .EQ. NO1) THEN
            NP3 = NO2
D        ELSE
D           CALL ERR_ASR(.FALSE.)
         ENDIF

C---        Métriques du T3 parent (recalculées car permutés)
         VKX    = VCORG(2,NP3) - VCORG(2,NP1)               ! Ksi,x
         VEX    = VCORG(2,NP1) - VCORG(2,NP2)               ! Eta,x
         VKY    = VCORG(1,NP1) - VCORG(1,NP3)               ! Ksi,y
         VEY    = VCORG(1,NP2) - VCORG(1,NP1)               ! Eta,y
         DETJT3 = VDJV(5,IEV)
         VSX    = -(VKX+VEX)
         VSY    = -(VKY+VEY)

C---        MÉTRIQUES DE L'ÉLÉMENT - COMPOSANTES DE LA NORMALE EXTÉRIEURE
         VNY    = -VDJS(1,IES)
         VNX    =  VDJS(2,IES)
         DETJL2 =  VDJS(3,IES)

C---        Constante dépendant du contour
         DISC = -UN_2*DETJL2/DETJT3

C---        Propriétés nodales
         H1 = VPRNO(IPH, NP1)
         H2 = VPRNO(IPH, NP2)
         H3 = VPRNO(IPH, NP3)

C---        Propriétés élémentaires du contour
         SHKXX1 = VPRES(3,IES)
         SHKXX2 = VPRES(4,IES)
         SHKXY1 = VPRES(5,IES)
         SHKXY2 = VPRES(6,IES)
         SHKYY1 = VPRES(7,IES)
         SHKYY2 = VPRES(8,IES)

C---        Termes de contour
         DIF11 = DISC*( (SHKXX1 * VSX + SHKXY1 * VSY ) * VNX
     &         +        (SHKXY1 * VSX + SHKYY1 * VSY ) * VNY ) / H1
         DIF12 = DISC*( (SHKXX1 * VKX + SHKXY1 * VKY ) * VNX
     &         +        (SHKXY1 * VKX + SHKYY1 * VKY ) * VNY ) / H2
         DIF13 = DISC*( (SHKXX1 * VEX + SHKXY1 * VEY ) * VNX
     &         +        (SHKXY1 * VEX + SHKYY1 * VEY ) * VNY ) / H3
         DIF21 = DISC*( (SHKXX2 * VSX + SHKXY2 * VSY ) * VNX
     &         +        (SHKXY2 * VSX + SHKYY2 * VSY ) * VNY ) / H1
         DIF22 = DISC*( (SHKXX2 * VKX + SHKXY2 * VKY ) * VNX
     &         +        (SHKXY2 * VKX + SHKYY2 * VKY ) * VNY ) / H2
         DIF23 = DISC*( (SHKXX2 * VEX + SHKXY2 * VEY ) * VNX
     &         +        (SHKXY2 * VEX + SHKYY2 * VEY ) * VNY ) / H3

C---        MATRICE ELEMENTAIRE DE FRONTIERE OUVERTE
         VKE11 = DIF11
         VKE21 = DIF21
         VKE12 = DIF12
         VKE22 = DIF22
         VKE13 = DIF13
         VKE23 = DIF23

C---        BOUCLE D'ASSEMBLAGE SUR LES DDL
         DO ID=1,LM_CMMN_NDLN
            ID1 = ID
            ID2 = ID1+LM_CMMN_NDLN
            ID3 = ID2+LM_CMMN_NDLN
            VKE(ID1,ID1) = VKE11
            VKE(ID2,ID1) = VKE21
            VKE(ID1,ID2) = VKE12
            VKE(ID2,ID2) = VKE22
            VKE(ID1,ID3) = VKE13
            VKE(ID2,ID3) = VKE23
         ENDDO

C---       TABLE KLOCE DE LOCALISATION DES DDLS
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 1)= KLOCN(ID, NP1)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 2)= KLOCN(ID, NP2)
         ENDDO
         DO ID=1,LM_CMMN_NDLN
            KLOCE(ID, 3)= KLOCN(ID, NP3)
         ENDDO

C---       ASSEMBLAGE DE LA MATRICE
D        IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
D           WRITE(LOG_BUF,'(2A,I9)') 'CALCUL_MATRICE_K_S2',': ',IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF
         IERR = F_ASM(HMTX, LM_CMMN_NDLEV, KLOCE, VKE)
D        IF (IERR .NE. ERR_OK) THEN
D           WRITE(LOG_BUF,'(2A,I9)')
D    &         'ERR_CALCUL_MATRICE_K_CAUCHY_SORTANT', ': ', IES
D           CALL LOG_ECRIS(LOG_BUF)
D        ENDIF

299      CONTINUE
      ENDDO

      RETURN
      END

