//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#ifndef C_PETSC_H_DEJA_INCLU
#define C_PETSC_H_DEJA_INCLU

#ifndef MODULE_PETSC
#  define MODULE_PETSC 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_PTSC_INITIALIZE   C_PTSC_INITIALIZE
#  define C_PTSC_FINALIZE     C_PTSC_FINALIZE
#  define C_PTSC_ERRMSG       C_PTSC_ERRMSG
#  define C_PTSC_MAT_CREATE   C_PTSC_MAT_CREATE
#  define C_PTSC_MAT_DESTROY  C_PTSC_MAT_DESTROY
#  define C_PTSC_MAT_INIT     C_PTSC_MAT_INITZERO
#  define C_PTSC_MAT_ASMKE    C_PTSC_MAT_ASMKE
#  define C_PTSC_MAT_ASMBEGIN C_PTSC_MAT_ASMBEGIN
#  define C_PTSC_MAT_ASMEND   C_PTSC_MAT_ASMEND
#  define C_PTSC_VEC_CREATE   C_PTSC_VEC_CREATE
#  define C_PTSC_VEC_DESTROY  C_PTSC_VEC_DESTROY
#  define C_PTSC_VEC_GETVAL   C_PTSC_VEC_GETVAL
#  define C_PTSC_VEC_SETVAL   C_PTSC_VEC_SETVAL
#  define C_PTSC_SLES_CREATE  C_PTSC_SLES_CREATE
#  define C_PTSC_SLES_DESTROY C_PTSC_SLES_DESTROY
#  define C_PTSC_SLES_SOLVE   C_PTSC_SLES_SOLVE
#  define C_PTSC_PART         C_PTSC_PART
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_PTSC_INITIALIZE   c_ptsc_initialize_
#  define C_PTSC_FINALIZE     c_ptsc_finalize_
#  define C_PTSC_ERRMSG       c_ptsc_errmsg_
#  define C_PTSC_MAT_CREATE   c_ptsc_mat_create_
#  define C_PTSC_MAT_DESTROY  c_ptsc_mat_destroy_
#  define C_PTSC_MAT_INIT     c_ptsc_mat_init_
#  define C_PTSC_MAT_ASMKE    c_ptsc_mat_asmke_
#  define C_PTSC_MAT_ASMBEGIN c_ptsc_mat_asmbegin_
#  define C_PTSC_MAT_ASMEND   c_ptsc_mat_asmend_
#  define C_PTSC_VEC_CREATE   c_ptsc_vec_create_
#  define C_PTSC_VEC_DESTROY  c_ptsc_vec_destroy_
#  define C_PTSC_VEC_GETVAL   c_ptsc_vec_getval_
#  define C_PTSC_VEC_SETVAL   c_ptsc_vec_setval_
#  define C_PTSC_SLES_CREATE  c_ptsc_sles_create_
#  define C_PTSC_SLES_DESTROY c_ptsc_sles_destroy_
#  define C_PTSC_SLES_SOLVE   c_ptsc_sles_solve_
#  define C_PTSC_PART         c_ptsc_part_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_PTSC_INITIALIZE   c_ptsc_initialize__
#  define C_PTSC_FINALIZE     c_ptsc_finalize__
#  define C_PTSC_ERRMSG       c_ptsc_errmsg__
#  define C_PTSC_MAT_CREATE   c_ptsc_mat_create__
#  define C_PTSC_MAT_DESTROY  c_ptsc_mat_destroy__
#  define C_PTSC_MAT_INIT     c_ptsc_mat_init__
#  define C_PTSC_MAT_ASMKE    c_ptsc_mat_asmke__
#  define C_PTSC_MAT_ASMBEGIN c_ptsc_mat_asmbegin__
#  define C_PTSC_MAT_ASMEND   c_ptsc_mat_asmend__
#  define C_PTSC_VEC_CREATE   c_ptsc_vec_create__
#  define C_PTSC_VEC_DESTROY  c_ptsc_vec_destroy__
#  define C_PTSC_VEC_GETVAL   c_ptsc_vec_getval_
#  define C_PTSC_VEC_SETVAL   c_ptsc_vec_setval_
#  define C_PTSC_SLES_CREATE  c_ptsc_sles_create__
#  define C_PTSC_SLES_DESTROY c_ptsc_sles_destroy__
#  define C_PTSC_SLES_SOLVE   c_ptsc_sles_solve__
#  define C_PTSC_PART         c_ptsc_part__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_PETSC)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_INITIALIZE  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_FINALIZE    ();
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_ERRMSG      (fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_CREATE  (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_DESTROY (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_INIT    (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMKE   (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, double*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMBEGIN(hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMEND  (hndl_t*);

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_CREATE  (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_DESTROY (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_GETVAL  (hndl_t*, fint_t*, fint_t*, double*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_SETVAL  (hndl_t*, fint_t*, fint_t*, double*);

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_CREATE (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_DESTROY(hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_SOLVE  (hndl_t*, hndl_t*, hndl_t*, hndl_t*);


F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_PART        (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_PETSC_H_DEJA_INCLU
