//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#include "c_petsc.h"

#ifdef __cplusplus__DEACTIVATED
extern "C"
{
#endif

#include "petsc.h"
#include "petscerror.h"
#include "petscmat.h"
#include "petscvec.h"
#include "petscksp.h"
#include "petscdmplex.h"   

#ifdef __cplusplus__DEACTIVATED
}
#endif

#include <algorithm>
#include <string>

#ifdef MODE_DEBUG
#  include <iostream>
#  include <assert.h>
#endif

// ---  Redéfinition de std::min local à cause de msvc win64
template<typename Tp> inline const Tp& std_min(const Tp& l, const Tp& r)
{
   return (l < r) ? l : r;
}

static int nbrInit = 0;

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#endif

//Maintenir un map id -> (dm)

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_INITIALIZE (F2C_CONF_STRING  fic
                                                                 F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_INITIALIZE (F2C_CONF_STRING  fic)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* fP = fic;
   int   l  = len;
#else
   char* fP = fic->strP;
   int   l  = fic->len;
#endif

   int ierr = 0;
   if (nbrInit == 0)
   {
      int argc = 0;
      if (l <= 0)
      {
         ierr = PetscInitialize(&argc, NULL, NULL, NULL);
      }
      else
      {
         std::string f(fP, l);
         ierr = PetscInitialize(&argc, NULL, f.c_str(), NULL);
      }
   }
   ++nbrInit;

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_FINALIZE()
{
   int ierr = 0;
   if (nbrInit == 1) ierr = PetscFinalize();
   if (nbrInit  > 0) --nbrInit;
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_ERRMSG (fint_t* ierr,
                                                             F2C_CONF_STRING  str
                                                             F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_ERRMSG (fint_t* ierr,
                                                             F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif
#ifdef MODE_DEBUG
   assert(l >= 0);
#endif   // MODE_DEBUG

   memset(sP, ' ', l);
   if (*ierr != 0)
   {
      const char* text = NULL;
      (void) PetscErrorMessage(*ierr, &text, PETSC_NULL);
      memcpy(sP, text, std_min(static_cast<size_t>(l), strlen(text)));
   }

   return 0;
}

//************************************************************************
// Sommaire:   Crée une matrice distribuée PETSc
//
// Description:
//    La fonction <code>C_PTSC_MAT_CREATE(...)</code> crée une matrice
//    distribuée PETSc.
//
// Entrée:
//    int  neqp          Nombre d'équations privées au process
//    int  neq           Nombre d'équations assemblées par le process
//    int  nnz           Nombre de termes non nuls
//    int* iap           Table cumulatives des pointeurs aux indices en j
//    int* jap           Table des indices en j
//    int* colg          Table de renumérotation local-global
//    int* ling          Table des dll privés au process (num. globale)
//
// Sortie:
//    hndl_t* hMat          Handle sur la matrice créée
//
// Notes:
//    ling doit être en ordre croissant, mais peut contenir des trous
//    pour les ddl qui ne sont pas privés.
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_CREATE(hndl_t* hMat,
                                                                fint_t* neqP,
                                                                fint_t* nnzP,
                                                                fint_t* iap,
                                                                fint_t* jap,
                                                                fint_t* colg,
                                                                fint_t* ling)
{
#ifdef MODE_DEBUG
   assert(*neqP > 0);
   assert(*nnzP >= *neqP);
   assert(iap  != NULL);
   assert(jap  != NULL);
   assert(colg != NULL);
   assert(ling != NULL);
   int idllMin = 0;
   while (idllMin < *neqP && ling[idllMin] < 0) ++idllMin;
   int idllMax = (*neqP)-1;
   while (idllMax >= 0 && ling[idllMax] < 0) --idllMax;
   assert(idllMin < *neqP);
   assert(idllMax < *neqP);
   assert(idllMin <= idllMax);
#endif   // MODE_DEBUG

   int ierr = 0;
   Mat A = NULL;

   //---  Balance les indices en base 0
   const fint_t neq = *neqP;
   const fint_t nnz = *nnzP;
   for (int i = 0; i <= neq; ++i) --iap[i];
   for (int i = 0; i <  nnz; ++i) --jap[i];
   for (int i = 0; i <  neq; ++i) --colg[i];
   for (int i = 0; i <  neq; ++i) --ling[i];

   //---  dll du process en base 0 (num global)
   int dllMin = ling[0];
   for (int i = 1; i < neq && dllMin < 0; ++i) dllMin = ling[i];
   int dllMax = ling[neq-1];
   for (int i = neq-2; i >= 0 && dllMax < 0; --i) dllMax = ling[i];

   //---  Alloue et initialise l'espace pour les tables
   int* kdnz = new int[neq];
   int* konz = new int[neq];
   for (int i = 0; i < neq; ++i) kdnz[i] = 0;
   for (int i = 0; i < neq; ++i) konz[i] = 0;

   //---  Somme le nombre de termes non nuls sur chaque ligne
   int neqp = 0;
   int nnzp = 0;
   for (int il = 0; il < neq; ++il)    // dll i local
   {
      if (ling[il] < 0) continue;
      const int ig = colg[il];         // dll i global

      const int i1 = iap[il];
      const int i2 = iap[il+1]-1;
      for (int ii = i1; ii <= i2; ++ii)
      {
         const int jl = jap[ii];       // dll j local
         const int jg = colg[jl];      // dll j global
         if (jg < 0)
            ;
         else if (jg <  dllMin)
            ++konz[neqp];             // Bloc hors diagonal
         else if (jg <= dllMax)
            ++kdnz[neqp];             // Bloc diagonal
         else
            ++konz[neqp];             // Bloc hors diagonal
      }
      nnzp += konz[neqp];
      nnzp += kdnz[neqp];
      ++neqp;
   }

   // ---  Tests valides si on a un seul proc
/*
   assert(neqp == neq);
   assert(nnzp == nnz);
   for (int il = 0; il < neq; ++il)
   {
      assert(konz[il] == 0);
      assert(kdnz[il] == iap[il+1]-iap[il]);
   }
*/

   // ---  Dimensionne la matrice
   if (!ierr) ierr = MatCreateAIJ(MPI_COMM_WORLD,
                                  neqp,             // Number of local row
                                  neqp,             // Number of local columns
                                  PETSC_DETERMINE,  // Number of global rows
                                  PETSC_DETERMINE,  // Number of global columns
                                  PETSC_DECIDE,
                                  kdnz,
                                  PETSC_DECIDE,
                                  konz,
                                  &A);
   if (!ierr) ierr = MatSetOption(A, MAT_ROW_ORIENTED, PETSC_FALSE); // Column Oriented

   //---  Désalloue l'espace pour les tables
   delete[] kdnz;
   delete[] konz;

   //---  Re-balance les indice en base 1
   for (int i = 0; i <= neq; ++i) ++iap[i];
   for (int i = 0; i <  nnz; ++i) ++jap[i];
   for (int i = 0; i <  neq; ++i) ++colg[i];
   for (int i = 0; i <  neq; ++i) ++ling[i];

   *hMat = reinterpret_cast<hndl_t>(A);
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_DESTROY(hndl_t* hMat)
{
   int ierr = 0;
   if (hMat != NULL)
   {
      Mat A = reinterpret_cast<Mat>(*hMat);
      ierr = MatDestroy(&A);
   }
   return ierr;
}

//************************************************************************
// Sommaire: C_PTSC_MAT_INITZERO
//
// Description:
//     La fonction <code>C_PTSC_MAT_INIT(...)</code> initialise la
//     matrice à zéro.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_INIT(hndl_t* hMat)
{
   int ierr = 0;
   Mat A = reinterpret_cast<Mat>(*hMat);
   return ierr = MatZeroEntries(A);
}

//************************************************************************
// Sommaire: PETSc_MAT_ASMKE
//
// Description:
//     La fonction <code>PETSc_MAT_ASMKE(...)</code> assemble la
//     matrice élémentaire dans une matrice globale stockée par
//     compression des lignes.
//
// Entrée:
//    hndl_t* hMat      Handle sur la matrice
//    fint_t* colg      Table des indices des colonnes
//    fint_t* ling      Table des indices des lignes
//    fint_t* ndleP     Dim
//    fint_t* kloce     Table de localisation
//    double* vke       Table des valeurs
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMKE(hndl_t* hMat,
                                                               fint_t* colg,
                                                               fint_t* ling,
                                                               fint_t* ndleP,
                                                               fint_t* kloce,
                                                               double* vke)
{
   int ierr = 0;

   const fint_t ndle = *ndleP;
   int* kloce_i = new int[ndle];
   int* kloce_j = new int[ndle];

   //---  Indices locaux en base 0
   for (int i = 0; i < ndle; ++i) --kloce[i];

   //---  Indices globaux en base 1
   for (int i = 0; i < ndle; ++i) kloce_i[i] = (kloce[i] < 0) ? 0 : colg[kloce[i]];
   for (int i = 0; i < ndle; ++i) kloce_j[i] = (kloce[i] < 0) ? 0 : ling[kloce[i]];

   //---  Indices globaux en base 0
   for (int i = 0; i < ndle; ++i) --kloce_i[i];
   for (int i = 0; i < ndle; ++i) --kloce_j[i];

   //---  Contrôle de la matrice
   int k = 0;
   const double* vkeEndI = vke + (ndle*ndle);
   for (double* vI = vke; vI != vkeEndI; ++vI)
      if (*vke == 0.0e0) ++k;
   if (k == ndle*ndle) ierr = -1;

   //---  Assemble
   if (ierr == 0)
   {
      Mat A = reinterpret_cast<Mat>(*hMat);
      ierr = MatSetValues(A,
                          ndle,
                          kloce_i,
                          ndle,
                          kloce_j,
                          vke,
                          ADD_VALUES);
   }

   //---  Indices locaux en base 1
   for (int i = 0; i < ndle; ++i) ++kloce[i];

   //---
   delete[] kloce_j;
   delete[] kloce_i;

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMBEGIN(hndl_t* hMat)
{
   int ierr = 0;
   if (hMat != NULL)
   {
      Mat A = reinterpret_cast<Mat>(*hMat);
      ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
   }
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_MAT_ASMEND(hndl_t* hMat)
{
   int ierr = 0;
   if (hMat != NULL)
   {
      Mat A = reinterpret_cast<Mat>(*hMat);
      ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
   }
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_CREATE(hndl_t* hVec,
                                                                fint_t* neqP,
                                                                fint_t* ling)
{
   int ierr = 0;
   Vec V = NULL;

   //---  Somme le nombre d'équations (base 1 !!)
   const fint_t neq = *neqP;
   int neqp = 0;
   for (int i = 0; i < neq; ++i) if (ling[i] > 0) ++neqp;

   //---  Crée le vecteur
   if (ierr == 0)
   {
      ierr = VecCreateMPI(MPI_COMM_WORLD,
                          neqp,             // Number of local row
                          PETSC_DECIDE,     // Number of global rows
                          &V);
   }

   *hVec = reinterpret_cast<hndl_t>(V);
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_DESTROY(hndl_t* hVec)
{
   int ierr = 0;
   if (hVec != NULL)
   {
      Vec V = reinterpret_cast<Vec>(*hVec);
      ierr = VecDestroy(&V);
   }
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_GETVAL(hndl_t* hVec,
                                                                fint_t* neqP,
                                                                fint_t* ling,
                                                                double* val)
{
   int ierr = 0;
   Vec V = reinterpret_cast<Vec>(*hVec);

   //---  Balance les indices en base 0
   const fint_t neq = *neqP;
   for (int i = 0; i < neq; ++i) --ling[i];

   PetscScalar *v = NULL;
   if (ierr == 0)
   {
      ierr = VecGetArray(V, &v);
   }
   if (ierr == 0)
   {
      for (int ival = 0, iv = 0; ival < neq; ++ival)
         if (ling[ival] >= 0)
            val[ival] = v[iv++];
   }
   if (v != NULL)
   {
      (void) VecRestoreArray(V, &v);
   }

   //---  Balance les indices en base 1
   for (int i = 0; i <  neq; ++i) ++ling[i];

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_VEC_SETVAL(hndl_t* hVec,
                                                                fint_t* neqP,
                                                                fint_t* ling,
                                                                double* val)
{
   int ierr = 0;
   Vec V = reinterpret_cast<Vec>(*hVec);

   //---  Balance les indices en base 0
   const fint_t neq = *neqP;
   for (int i = 0; i < neq; ++i) --ling[i];

   if (ierr == 0)
   {
      ierr = VecAssemblyBegin(V);
   }
   if (ierr == 0)
   {
      ierr = VecSetValues(V,
                          neq,
                          ling,
                          val,
                          ADD_VALUES);
   }
   if (ierr == 0)
   {
      ierr = VecAssemblyEnd(V);
   }

   //---  Balance les indices en base 1
   for (int i = 0; i <  neq; ++i) ++ling[i];

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_CREATE(hndl_t* hRes)
{
   int ierr;
   KSP ksp = NULL;

   ierr = KSPCreate(MPI_COMM_WORLD, &ksp);
   ierr = KSPSetFromOptions(ksp);

   *hRes = reinterpret_cast<hndl_t>(ksp);
   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_DESTROY(hndl_t* hRes)
{
   int ierr = 0;
   if (hRes != NULL)
   {
      KSP ksp = reinterpret_cast<KSP>(*hRes);
      ierr = KSPDestroy(&ksp);
   }
   return ierr;
}

//************************************************************************
// Sommaire:   Résous le système linéaire.
//
// Description:
//    La fonction <code>PETSc_MPI_SLES_Solve</code> résous le système
//    linéaire. En sortie, le membre de droite hRhs est remplacé par la
//    solution.
//
// Entrée:
//    int hRes    Handle sur le solveur PETSC
//    int hMat    Handle sur la matrice
//    int hRhs    Handle sur le rhs (modifié en sortie)
//    int hIni    Handle sur la solution initiale
//
// Sortie:
//
// Notes:
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_SLES_SOLVE(hndl_t* hRes,
                                                                hndl_t* hMat,
                                                                hndl_t* hRhs,
                                                                hndl_t* hIni)
{
   PetscErrorCode ierr;
   KSP ksp = reinterpret_cast<KSP>(*hRes);
   Mat A   = reinterpret_cast<Mat>(*hMat);
   Vec rhs = reinterpret_cast<Vec>(*hRhs);
   Vec ini = reinterpret_cast<Vec>(*hIni);
   Vec sol = PETSC_NULL;

   // ---  Crée la solution
   ierr = VecDuplicate(rhs, &sol);
   ierr = VecCopy(ini, sol);

   // ---  Résous
   ierr = KSPSetOperators(ksp, A, A);
   ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE);
   ierr = KSPSolve(ksp, rhs, sol);

   // ---  Ecrase le rhs avec la solution
   ierr = VecCopy(sol, rhs);
   ierr = VecDestroy(&sol);

   return ierr;
}

//************************************************************************
// Sommaire:   Partitionne un maillage éléments finis.
//
// Description:
//    La fonction <code>C_PTSC_PART(...)</code> partitionne le maillage
//    décrit par <code>nnt, nnel, nelt, kng</code>. Elle retourne dans la
//    table <code>kdis</code> les numéros des sous-domaine associé à chacun
//    des noeuds du maillage.
//
// Entrée:
//    fint_t* comm      Communicateur MPI
//    fint_t* npart     Nombre de sous-domaines
//    fint_t* nnt       Nombre de noeuds total
//    fint_t* nnel      Nombre de noeuds par élément
//    fint_t* nelt      Nombre d'éléments total
//    fint_t* kng       Table des connectivités kng[nelt][nnel]
//
// Sortie:
//    fint_t* kdis      Table(nnt) des numéros de sous-domaine des noeuds
//
// Notes:
// http://www.mcs.anl.gov/petsc/petsc-current/src/dm/impls/plex/examples/tutorials/ex1.c.html
//************************************************************************
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PTSC_PART(fint_t* comm,
                                                          fint_t* npart,
                                                          fint_t* kdis,
                                                          fint_t* ndim,
                                                          fint_t* nnt,
                                                          double* vcorg,
                                                          fint_t* nnel,
                                                          fint_t* nelt,
                                                          fint_t* kng)
{
#ifdef MODE_DEBUG
   assert(*npart> 0);
   assert(*nnel > 0);
   assert(*nelt > 0);
   assert(kng != NULL);
   assert(*nnt > 0);
   int mpi_init = 0;
   MPI_Initialized(&mpi_init);
   assert(mpi_init > 0);
#endif   // MODE_DEBUG

   PetscErrorCode perr = 0;

   setvbuf(stdout, NULL, _IONBF, 0);
   setvbuf(stderr, NULL, _IONBF, 0);

   int isize = -1;
   int irank = -1;
   MPI_Comm comm_c = MPI_Comm_f2c(*comm);
   (void) MPI_Comm_size(comm_c, &isize);
   (void) MPI_Comm_rank(comm_c, &irank);

   // ---  Passe en numérotation C
   const fint_t nbrConnecGlb = (*nnel)*(*nelt);
   for (fint_t i = 0; i < nbrConnecGlb; ++i) --kng[i];

   // ---  Crée le DM
   DM dm = NULL;
   if (!perr)
   {
      perr = DMPlexCreateFromCellList(comm_c,      // comm - The communicator
                                      *ndim,       // dim - The topological dimension of the mesh
                                      *nelt,       // numCells - The number of cells
                                      *nnt,        // numVertices - The number of vertices
                                      *nnel,       // numCorners - The number of vertices for each cell
                                       PETSC_FALSE,// interpolate - Flag indicating that intermediate mesh entities(faces, edges) should be created automatically
                                       kng,        // cells - An array of numCells*numCorners numbers, the vertices for each cell
                                      *ndim,       // spaceDim - The spatial dimension used for coordinates
                                       vcorg,      // vertexCoords - An array of numVertices*spaceDim numbers, the coordinates of each vertex
                                       &dm);
   }

   // ---  Assigne les options
   if (!perr)
   {
      perr = DMSetFromOptions(dm);
   }

   // ---  Crée la section
   PetscSection s = NULL;
   if (!perr)
   {
      const PetscInt numFields = 1;
      const PetscInt numCmp[1] = {1};
      const PetscInt numDof[3] = {1, 0, 0};    /*ndof for (vertex, edge, cell) for each field*/
      perr = DMPlexCreateSection(dm, *ndim, numFields, numCmp, numDof, 0, NULL, NULL, NULL, NULL, &s);
      if (s) 
      { 
         if (!perr) perr = DMSetDefaultSection(dm, s);
         if (!perr) perr = PetscSectionDestroy(&s);
      }
   }

   // overlap, c'est le résultat final mais pas celui attendu ici. on devrait retourner
   // le process qui possède le noeud.
   /*
   if (!perr)
   {
      DM dmDist = NULL;
      perr = DMPlexDistributeOverlap(dm, 1, NULL, &dmDist);
      if (dmDist) { DMDestroy(&dm); dm = dmDist; }
   }
   */

   // ---  Récupère la distribution
   ISLocalToGlobalMapping l2g = NULL;
   if (!perr)
   {
      perr = DMGetLocalToGlobalMapping(dm, &l2g);
   }

   // ---  Monte la table globale de distribution
   PetscInt lclSize = -1;
   PetscInt const* lclInd = NULL;
   if (!perr)
   {
      perr = ISLocalToGlobalMappingGetSize   (l2g, &lclSize);
      perr = ISLocalToGlobalMappingGetIndices(l2g, &lclInd);
   }

   // ---  Monte la table globale de distribution
   if (!perr)
   {
      fint_t* k0 = kdis + irank*(*nnt);
      for (PetscInt il = 0; il < lclSize; ++il)
      {
         PetscInt ig = lclInd[il];
         assert(ig >= 0 && ig < *nnt);
         k0[ig] = il;
         //kdis[iproc][ig] = il;
      }
   }

   // ---  Repasse en numérotation FORTRAN
   for (fint_t i = 0; i < nbrConnecGlb; ++i) ++kng[i];
   for (fint_t i = 0; i < *nnt; ++i) ++kdis[i];

   return perr;
}
