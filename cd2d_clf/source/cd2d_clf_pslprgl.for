C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Coliformes fécaux (CLF)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_CLF_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_CLF_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CLF_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IP
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     CONTROLE DE POSITIVITE
      IP = CD2D_BSE_NPRGLL + 1
      IF (ERR_GOOD())
     &   IERR = CD2D_BSE_CTRL_MINMAX(IP,
     &                               VPRGL(IP),
     &                               0.0D+00,
     &                               1.0D+99,
     &                               'MSG_COEF_DEGRAD')
      IP = IP + 1
      IF (ERR_GOOD())
     &   IERR = CD2D_BSE_CTRL_MINMAX(IP,
     &                               VPRGL(IP),
     &                               0.0D+00,
     &                               1.0D+99,
     &                               'MSG_DECOU_D10')
      IP = IP + 1
      IF (ERR_GOOD())
     &   IERR = CD2D_BSE_CTRL_MINMAX(IP,
     &                               VPRGL(IP),
     &                               0.0D+00,
     &                               1.0D+00,
     &                               'MSG_DECOU_VSED')

C---     APPEL L'ELEMENT DE BASE
      CALL CD2D_BSE_PSLPRGL (VPRGL, IERR)

      IERR = ERR_TYP()
      RETURN
      END

