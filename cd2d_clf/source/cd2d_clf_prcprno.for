C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Coliformes fécaux (CLF)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_CLF_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_CLF_PRCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CLF_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSEN_PRCPRNO(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_CLF_PRCTPS (VPRGL,
     &                      VPRNO)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_CLF_PRCTPS
C
C Description:
C     PRE-CALCUL DU TERME PUITS-SOURCE
C
C Entrée:
C   VPRGL       Propriétés globales
C   VPRNO       Propriétés nodales
C
C Sortie:
C   VPRNO
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_CLF_PRCTPS(VPRGL, VPRNO)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_bse.fc'

      INTEGER IN
      INTEGER IPH, IPA, IPB
      INTEGER IDGR, ID10, IVSD
      REAL*8  UN_DH, ALFA, VN1, VN2
      REAL*8  D10, PHI, CK_C, CK_D, H

      REAL*8  BETA
      PARAMETER (BETA = 2.11D+05)
C-----------------------------------------------------------------------

C---     Indices dans VPRNO
      IPH = 3
      IPA = LM_CMMN_NPRNOL + 6
      IPB = LM_CMMN_NPRNOL + 7

C---     Indices dans VPRGL
      IDGR = CD2D_BSE_NPRGLL + 1
      ID10 = IDGR + 1
      IVSD = ID10 + 1

C---     Paramètres de découvrement
      D10 =  VPRGL(ID10)
      PHI = (UN-CD2D_DECOU_POROSITE) / (BETA*D10*D10)

C---     Coefficients
      CK_C = - VPRGL(IDGR)       ! coef. de degradation
      CK_D = - PHI*VPRGL(IVSD)   ! découvrement

C---     Boucle sur les noeuds
      UN_DH = UN / (CD2D_DECOU_HTRIG - CD2D_DECOU_HMIN)
      DO IN=1,EG_CMMN_NNL
          H = VPRNO(IPH, IN)

C---        ALFA, facteur de découvrement
         ALFA = (H - CD2D_DECOU_HMIN) * UN_DH
         ALFA = MIN(UN, MAX(ZERO, ALFA))
         VN1 = UN - ALFA
         VN2 = ALFA

C---        Assemble
         H = MAX(H, CD2D_DECOU_HMIN)
         VPRNO(IPA, IN) = H * (VN1*CK_D + VN2*CK_C)   ! Terme linéaire
         VPRNO(IPB, IN) = ZERO                        ! Terme constant
      ENDDO

      RETURN
      END
