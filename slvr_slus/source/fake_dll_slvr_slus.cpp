//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_slus
// Entry point: extern "C" void fake_dll_slvr_slus()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:05.582000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SLUS
F_PROT(IC_SLUS_CMD, ic_slus_cmd);
F_PROT(IC_SLUS_REQCMD, ic_slus_reqcmd);
 
// ---  class MR_SLUS
F_PROT(MR_SLUS_000, mr_slus_000);
F_PROT(MR_SLUS_999, mr_slus_999);
F_PROT(MR_SLUS_CTR, mr_slus_ctr);
F_PROT(MR_SLUS_DTR, mr_slus_dtr);
F_PROT(MR_SLUS_INI, mr_slus_ini);
F_PROT(MR_SLUS_RST, mr_slus_rst);
F_PROT(MR_SLUS_REQHBASE, mr_slus_reqhbase);
F_PROT(MR_SLUS_HVALIDE, mr_slus_hvalide);
F_PROT(MR_SLUS_ESTDIRECTE, mr_slus_estdirecte);
F_PROT(MR_SLUS_ASMMTX, mr_slus_asmmtx);
F_PROT(MR_SLUS_FCTMTX, mr_slus_fctmtx);
F_PROT(MR_SLUS_RESMTX, mr_slus_resmtx);
F_PROT(MR_SLUS_XEQ, mr_slus_xeq);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_slus()
{
   static char libname[] = "slvr_slus";
 
   // ---  class IC_SLUS
   F_RGST(IC_SLUS_CMD, ic_slus_cmd, libname);
   F_RGST(IC_SLUS_REQCMD, ic_slus_reqcmd, libname);
 
   // ---  class MR_SLUS
   F_RGST(MR_SLUS_000, mr_slus_000, libname);
   F_RGST(MR_SLUS_999, mr_slus_999, libname);
   F_RGST(MR_SLUS_CTR, mr_slus_ctr, libname);
   F_RGST(MR_SLUS_DTR, mr_slus_dtr, libname);
   F_RGST(MR_SLUS_INI, mr_slus_ini, libname);
   F_RGST(MR_SLUS_RST, mr_slus_rst, libname);
   F_RGST(MR_SLUS_REQHBASE, mr_slus_reqhbase, libname);
   F_RGST(MR_SLUS_HVALIDE, mr_slus_hvalide, libname);
   F_RGST(MR_SLUS_ESTDIRECTE, mr_slus_estdirecte, libname);
   F_RGST(MR_SLUS_ASMMTX, mr_slus_asmmtx, libname);
   F_RGST(MR_SLUS_FCTMTX, mr_slus_fctmtx, libname);
   F_RGST(MR_SLUS_RESMTX, mr_slus_resmtx, libname);
   F_RGST(MR_SLUS_XEQ, mr_slus_xeq, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
