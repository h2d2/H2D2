C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  fonctions Fortran
C Sous-Groupe:  OS
C Sommaire: Fonctions F du système d'opération (Operating System)
C************************************************************************

C************************************************************************
C Sommaire:  Fonction pour récupérer argc.
C
C Description:
C    La fonction privée <code>F_OS_NARGLC()</code> permet de récupérer argc,
C    le nombre de paramètres de la ligne de commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les 2 fonctions permettent de déclarer de vraies fonctions Fortran,
C     appelable du C, alors que iargc et getarg sont généralement des
C     INTRINSIC qui sont encodées par le compilateur.
C************************************************************************
      FUNCTION F_OS_NARGLC()

      IMPLICIT NONE

      INCLUDE 'f_os.fi'

      INTEGER IARGC
C-----------------------------------------------------------------------

      F_OS_NARGLC = IARGC()
      RETURN
      END

C************************************************************************
C Sommaire:  Fonctions pour récupérer argv.
C
C Description:
C    La fonction privée <code>F_OS_IARGLC(...)</code> permet de récupérer le
C    i.ème argument de la ligne de commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE F_OS_IARGLC(I, BUF)

      IMPLICIT NONE

      INTEGER       I
      CHARACTER*(*) BUF

      INCLUDE 'f_os.fi'
C-----------------------------------------------------------------------

      BUF = ' '
      CALL GETARG(I, BUF)

      RETURN
      END
