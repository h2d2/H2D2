C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  fonctions Fortran
C Sous-Groupe:  LC
C Sommaire: Fonctions F de cast depuis et vers des LOGICAL
C************************************************************************

C************************************************************************
C Sommaire:  Cast un LOGICAL en INTEGER.
C
C Description:
C     La fonction <code>F_LC_L2I()</code> cast un LOGICAL en INTEGER.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_LC_L2I(L)

      IMPLICIT NONE
      LOGICAL L

      INCLUDE 'f_lc.fi'
C-----------------------------------------------------------------------

      F_LC_L2I = -1
      IF (.NOT. L) F_LC_L2I = 0
      RETURN
      END

C************************************************************************
C Sommaire:  Cast un INTEGER en LOGICAL.
C
C Description:
C     La fonction <code>F_LC_I2L()</code> cast un INTEGER en LOGICAL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_LC_I2L(I)

      IMPLICIT NONE
      INTEGER I

      INCLUDE 'f_lc.fi'
C-----------------------------------------------------------------------

      F_LC_I2L = .TRUE.
      IF (I .EQ. 0) F_LC_I2L = .FALSE.
      RETURN
      END

C************************************************************************
C Sommaire:  Cast un LOGICAL en INTEGER.
C
C Description:
C     La fonction <code>F_LC_L2R()</code> cast un LOGICAL en REAL*8.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_LC_L2R(L)

      IMPLICIT NONE
      LOGICAL L

      INCLUDE 'f_lc.fi'
C-----------------------------------------------------------------------

      F_LC_L2R = DBLE( F_LC_L2I(L) )
      RETURN
      END

C************************************************************************
C Sommaire:  Cast un REAL*8 en LOGICAL.
C
C Description:
C     La fonction <code>F_LC_R2L()</code> cast un REAL*8 en LOGICAL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_LC_R2L(R)

      IMPLICIT NONE
      REAL*8  R

      INCLUDE 'f_lc.fi'
C-----------------------------------------------------------------------

      F_LC_R2L = F_LC_I2L( NINT(R) )
      RETURN
      END
