//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonction C de lecture/écriture dans des fichiers binaires
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_FB_H_DEJA_INCLU
#define C_FB_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_FB_OUVRE    F2C_CONF_DECOR_FNC(C_FB_OUVRE,  c_fb_ouvre)
#define C_FB_FERME    F2C_CONF_DECOR_FNC(C_FB_FERME,  c_fb_ferme)
#define C_FB_ECRIS    F2C_CONF_DECOR_FNC(C_FB_ECRIS,  c_fb_ecris)
#define C_FB_LIS      F2C_CONF_DECOR_FNC(C_FB_LIS,    c_fb_lis)
#define C_FB_REQDIM   F2C_CONF_DECOR_FNC(C_FB_REQDIM, c_fb_reqdim)
#define C_FB_INVERSE  F2C_CONF_DECOR_FNC(C_FB_INVERSE,c_fb_inverse)


F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_OUVRE   (F2C_CONF_STRING, fint_t* F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_FERME   (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_ECRIS   (hndl_t*, fint_t*, double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_LIS     (hndl_t*, fint_t*, double*);
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_REQDIM  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_INVERSE (F2C_CONF_STRING, F2C_CONF_STRING, fint_t*, fint_t*, double* F2C_CONF_SUP_INT F2C_CONF_SUP_INT);


#ifdef __cplusplus
}
#endif

#endif   // C_FB_H_DEJA_INCLU

