//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire:
//
// Description:
//    Le fichier c_so.h
//
// Notes:
//
//************************************************************************
#ifndef C_SO_H_DEJA_INCLU
#define C_SO_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_SO_LOADMDL  F2C_CONF_DECOR_FNC(C_SO_LOADMDL, c_so_loadmdl)
#define C_SO_FREEMDL  F2C_CONF_DECOR_FNC(C_SO_FREEMDL, c_so_freemdl)
#define C_SO_LOADFNC  F2C_CONF_DECOR_FNC(C_SO_LOADFNC, c_so_loadfnc)
#define C_SO_FREEFNC  F2C_CONF_DECOR_FNC(C_SO_FREEFNC, c_so_freefnc)
#define C_SO_REQHFNC  F2C_CONF_DECOR_FNC(C_SO_REQHFNC, c_so_reqhfnc)
#define C_SO_REQFNC   F2C_CONF_DECOR_FNC(C_SO_REQFNC,  c_so_reqfnc)

#define C_SO_CALL0    F2C_CONF_DECOR_FNC(C_SO_CALL0,   c_so_call0)
#define C_SO_CALL1    F2C_CONF_DECOR_FNC(C_SO_CALL1,   c_so_call1)
#define C_SO_CALL2    F2C_CONF_DECOR_FNC(C_SO_CALL2,   c_so_call2)
#define C_SO_CALL3    F2C_CONF_DECOR_FNC(C_SO_CALL3,   c_so_call3)
#define C_SO_CALL4    F2C_CONF_DECOR_FNC(C_SO_CALL4,   c_so_call4)
#define C_SO_CALL5    F2C_CONF_DECOR_FNC(C_SO_CALL5,   c_so_call5)
#define C_SO_CALL6    F2C_CONF_DECOR_FNC(C_SO_CALL6,   c_so_call6)
#define C_SO_CALL7    F2C_CONF_DECOR_FNC(C_SO_CALL7,   c_so_call7)
#define C_SO_CALL8    F2C_CONF_DECOR_FNC(C_SO_CALL8,   c_so_call8)
#define C_SO_CALL9    F2C_CONF_DECOR_FNC(C_SO_CALL9,   c_so_call9)

#define C_SO_CBFNC0   F2C_CONF_DECOR_FNC(C_SO_CBFNC0,  c_so_cbfnc0)
#define C_SO_CBFNC1   F2C_CONF_DECOR_FNC(C_SO_CBFNC1,  c_so_cbfnc1)
#define C_SO_CBFNC2   F2C_CONF_DECOR_FNC(C_SO_CBFNC2,  c_so_cbfnc2)
#define C_SO_CBFNC3   F2C_CONF_DECOR_FNC(C_SO_CBFNC3,  c_so_cbfnc3)
#define C_SO_CBFNC4   F2C_CONF_DECOR_FNC(C_SO_CBFNC4,  c_so_cbfnc4)
#define C_SO_CBFNC5   F2C_CONF_DECOR_FNC(C_SO_CBFNC5,  c_so_cbfnc5)
#define C_SO_CBFNC6   F2C_CONF_DECOR_FNC(C_SO_CBFNC6,  c_so_cbfnc6)
#define C_SO_CBFNC7   F2C_CONF_DECOR_FNC(C_SO_CBFNC7,  c_so_cbfnc7)
#define C_SO_CBFNC8   F2C_CONF_DECOR_FNC(C_SO_CBFNC8,  c_so_cbfnc8)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADMDL  (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_FREEMDL  (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADFNC  (hndl_t*, hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_FREEFNC  (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_REQHFNC  (hndl_t*, fint_t (F2C_CONF_CNV_APPEL *)());
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_REQFNC   (fint_t (F2C_CONF_CNV_APPEL *)(), hndl_t*);

F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL0    (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL1    (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL2    (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL3    (hndl_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL4    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL5    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL6    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL7    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL8    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL9    (hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);

F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC0   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*), hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC1   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*), hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC2   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC3   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC4   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC5   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC6   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC7   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC8   (fint_t (F2C_CONF_CNV_APPEL *)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*), hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);

#ifdef __cplusplus
}
#endif

#endif   // C_SO_H_DEJA_INCLU

