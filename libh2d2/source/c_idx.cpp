//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  InDeXation
// Sommaire: Fonctions C d'indexation
//************************************************************************
#include "c_idx.h"

#include <algorithm>
#include <numeric>
#include <vector>

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTData, typename TTIndx>
struct indirect_less
{
   TTData* m_values;
   
   indirect_less(TTData* values) : m_values(values) {};

   bool operator() (const TTIndx& i1, const TTIndx& i2) const
   {
      return (m_values)[i1] < (m_values)[i2];
   }
};

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTData, typename TTIndx>
void sort_idx(size_t count, TTData* dtaTbl, TTIndx* idxTbl)
{
   typedef indirect_less<TTData, TTIndx> TTCompare;

   TTIndx* idxBgn = idxTbl;
   TTIndx* idxEnd = idxTbl+count;
   std::iota(idxBgn, idxEnd, 0);
   std::sort(idxBgn, idxEnd, TTCompare(dtaTbl));
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTData, typename TTIndx>
void swap_fwd(size_t d1, size_t d2, TTData* dtaTbl, TTIndx* idxTbl)
{
   std::vector<TTData> tmp(d1*d2);
   for (size_t i = 0; i < d2; ++i)
   {
      size_t ii = d1 * i;
      size_t jj = d1 * idxTbl[i];
      for (size_t j = 0; j < d1; ++j, ++ii, ++jj)
         tmp[ii] = dtaTbl[jj];
   }
   std::copy(tmp.begin(), tmp.end(), dtaTbl);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
template <typename TTData, typename TTIndx>
void swap_bck(size_t d1, size_t d2, TTData* dtaTbl, TTIndx* idxTbl)
{
   std::vector<TTData> tmp(d1*d2);
   for (size_t i = 0; i < d2; ++i)
   {
      size_t ii = d1 * i;
      size_t jj = d1 * idxTbl[i];
      for (size_t j = 0; j < d1; ++j, ++ii, ++jj)
         tmp[jj] = dtaTbl[ii];
   }
   std::copy(tmp.begin(), tmp.end(), dtaTbl);
}

//************************************************************************
// Sommaire:   Monte l'index d'une table entière
//
// Description:
//    La fonction IINDEX(...) monte dans I l'index de la table K. Les entrées
//    de I sont des indices des éléments de K en ordre ascendant.
//    La table K est unidimensionnelle.
//
// Entrée:
//    fint_t* N      Dimension des tables
//    fint_t* K      La table à indexer
//
// Sortie:
//    fint_t* I      La table d'index
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL IINDEX(fint_t* N, fint_t* K, fint_t* I)
{
   sort_idx(*N, K, I);
}

//************************************************************************
// Sommaire:   Trie directe de la table
//
// Description:
//    La fonction ISWAPF(...) trie la table K(N1, N2) en fonction de l'index I.
//    Le tri est directe, ie. K(:, I(JJ)) = K(:, JJ)
//
// Entrée:
//    fint_t* N1     Dimensions des tables
//    fint_t* N2
//    fint_t* K      La table à trier K(N1,N2)
//    fint_t* I      La table d'index I(N2)
//
// Sortie:
//    fint_t* K      La table triée
//
// Notes:
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL ISWAPF(fint_t* N1, fint_t* N2, fint_t* K, fint_t* I)
{
   swap_fwd(*N1, *N2, K, I);
}

//************************************************************************
// Sommaire: Trie inversé de la table
//
// Description:
//    La fonction ISWAPB(...) trie la table K(N1, N2) en fonction de l'index I.
//    Le tri est inverse, ie. K(:, JJ) = K(:, I(JJ))
//
// Entrée:
//    fint_t* N1     Dimensions des tables
//    fint_t* N2
//    fint_t* K      La table à trier K(N1,N2)
//    fint_t* I      La table d'index I(N2)
//
// Sortie:
//    fint_t* K      La table triée
//
// Notes:
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL ISWAPB(fint_t* N1, fint_t* N2, fint_t* K, fint_t* I)
{
   swap_bck(*N1, *N2, K, I);
}

//************************************************************************
// Sommaire:   Monte l'index d'une table double
//
// Description:
//    La fonction IINDEX(...) monte dans I l'index de la table V. Les entrées
//    de I sont des indices des éléments de V en ordre ascendant.
//    La table V est unidimensionnelle.
//
// Entrée:
//    fint_t* N      Dimension des tables
//    double* V      La table à indexer
//
// Sortie:
//    fint_t* I      La table d'index
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DINDEX(fint_t* N, double* V, fint_t* I)
{
   sort_idx(*N, V, I);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DSWAPF(fint_t* N1, fint_t* N2, double* V, fint_t* I)
{
   swap_fwd(*N1, *N2, V, I);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL DSWAPB(fint_t* N1, fint_t* N2, double* V, fint_t* I)
{
   swap_bck(*N1, *N2, V, I);
}

//************************************************************************
// Sommaire:   Monte l'index d'une table entière
//
// Description:
//    La fonction XINDEX(...) monte dans I l'index de la table K. Les entrées
//    de I sont des indices des éléments de K en ordre ascendant.
//    La table V est unidimensionnelle.
//
// Entrée:
//    fint_t* N      Dimension des tables
//    int64_t* K     La table à indexer
//
// Sortie:
//    fint_t* I      La table d'index
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XINDEX(fint_t* N, int64_t* K, fint_t* I)
{
   sort_idx(*N, K, I);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XSWAPF(fint_t* N1, fint_t* N2, int64_t* K, fint_t* I)
{
   swap_fwd(*N1, *N2, K, I);
}

//************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL XSWAPB(fint_t* N1, fint_t* N2, int64_t* K, fint_t* I)
{
   swap_bck(*N1, *N2, K, I);
}
