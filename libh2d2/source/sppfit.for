C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Notes:
C     Exemple d'utilisation:
C        USE SP_PFIT_M
C        TYPE(SP_PFIT_T) :: PFIT
C        IERR = SP_PFIT_CTR (PFIT, SP_PFIT_ITYP_BILINEAR)
C        IERR = SP_PFIT_CALC(PFIT, N, XY, Z)
C        IERR = SP_PFIT_DFDY(PFIT, N, XY, ZN)
C        IERR = SP_PFIT_DTR (PFIT)
C
C     OMP ne supporte pas les types avec allocation. On passe donc
C     d'une allocation dynamique à un dimensionnement statique.
C
C Functions:
C   Public:
C   Private:
C     INTEGER PFIT_F
C     INTEGER PFIT_D2FDYY
C     INTEGER PFIT_D2FDXY
C     INTEGER PFIT_D2FDXX
C     INTEGER PFIT_CTR
C     INTEGER PFIT_DTR
C     REAL*8 PFIT_CMP_F
C     REAL*8 PFIT_CMP_DFDX
C     REAL*8 PFIT_CMP_DFDY
C     REAL*8 PFIT_CMP_D2FDXX
C     REAL*8 PFIT_CMP_D2FDXY
C     REAL*8 PFIT_CMP_D2FDYY
C     INTEGER PFIT_CALC
C     INTEGER PFIT_EVAL
C     INTEGER PFIT_DFDY
C     INTEGER PFIT_DFDX
C
C************************************************************************

      MODULE SP_PFIT_private

      IMPLICIT NONE
      PUBLIC

      INTEGER, PARAMETER :: IERR_INVALID_TYPE       = -1
      INTEGER, PARAMETER :: IERR_INVALID_STRUCT     = -2
      INTEGER, PARAMETER :: IERR_INVALID_NBR_POINTS = -3
      INTEGER, PARAMETER :: IERR_ALLOCATION_ERROR   = -4

      INTEGER, PARAMETER :: ITYP_LINEAR   = 1
      INTEGER, PARAMETER :: ITYP_BILINEAR = 2
      INTEGER, PARAMETER :: ITYP_QUAD     = 3
      INTEGER, PARAMETER :: ITYP_BIQUAD   = 4
      INTEGER, PARAMETER :: ITYP_CUBIC    = 5

      INTEGER, PARAMETER :: I_C    =  1
      INTEGER, PARAMETER :: I_X    =  2
      INTEGER, PARAMETER :: I_Y    =  3
      INTEGER, PARAMETER :: I_XY   =  4
      INTEGER, PARAMETER :: I_XX   =  5
      INTEGER, PARAMETER :: I_YY   =  6
      INTEGER, PARAMETER :: I_XXY  =  7
      INTEGER, PARAMETER :: I_XYY  =  8
      INTEGER, PARAMETER :: I_XXYY =  9
      INTEGER, PARAMETER :: I_XXX  = 10
      INTEGER, PARAMETER :: I_YYY  = 11

      INTEGER, PARAMETER :: KCMP_LINEAR(3)   =
     &   (/I_C, I_X, I_Y/)
      INTEGER, PARAMETER :: KCMP_BILINEAR(4) =
     &   (/I_C, I_X, I_Y, I_XY/)
      INTEGER, PARAMETER :: KCMP_QUAD(6)     =
     &   (/I_C, I_X, I_Y, I_XY, I_XX, I_YY/)
      INTEGER, PARAMETER :: KCMP_BIQUAD(9)   =
     &   (/I_C, I_X, I_Y, I_XY, I_XX, I_YY, I_XXY, I_XYY, I_XXYY/)
      INTEGER, PARAMETER :: KCMP_CUBIC(10)   =
     &   (/I_C, I_X, I_Y, I_XY, I_XX, I_YY, I_XXY, I_XYY, I_XXX, I_YYY/)

      TYPE :: PFIT_T
         REAL*8  :: CMP_V(10)    ! 10 = max de ITYP_CUBIC
         INTEGER :: CMP_I(10)
         INTEGER :: CMP_N = -1
      END TYPE PFIT_T

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_F(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 1.0D0
         CASE (I_X)
            V = X
         CASE (I_Y)
            V = Y
         CASE (I_XY)
            V = X*Y
         CASE (I_XX)
            V = X*X
         CASE (I_YY)
            V = Y*Y
         CASE (I_XXY)
            V = X*X*Y
         CASE (I_XYY)
            V = X*Y*Y
         CASE (I_XXYY)
            V = X*X*Y*Y
         CASE (I_XXX)
            V = X*X*X
         CASE (I_YYY)
            V = Y*Y*Y
      END SELECT

      PFIT_CMP_F = V
      RETURN
      END FUNCTION PFIT_CMP_F

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_DFDX(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 0.0D0
         CASE (I_X)
            V = 1.0D0
         CASE (I_Y)
            V = 0.0D0
         CASE (I_XY)
            V = Y
         CASE (I_XX)
            V = 2*X
         CASE (I_YY)
            V = 0.0D0
         CASE (I_XXY)
            V = 2*X*Y
         CASE (I_XYY)
            V = Y*Y
         CASE (I_XXYY)
            V = 2*X*Y*Y
         CASE (I_XXX)
            V = 3*X*X
         CASE (I_YYY)
            V = 0.0D0
      END SELECT

      PFIT_CMP_DFDX = V
      RETURN
      END FUNCTION PFIT_CMP_DFDX

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_DFDY(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 0.0D0
         CASE (I_X)
            V = 0.0D0
         CASE (I_Y)
            V = 1.0D0
         CASE (I_XY)
            V = X
         CASE (I_XX)
            V = 0.0D0
         CASE (I_YY)
            V = 2*Y
         CASE (I_XXY)
            V = X*X
         CASE (I_XYY)
            V = 2*X*Y
         CASE (I_XXYY)
            V = 2*X*X*Y
         CASE (I_XXX)
            V = 0.0D0
         CASE (I_YYY)
            V = 3*Y*Y
      END SELECT

      PFIT_CMP_DFDY = V
      RETURN
      END FUNCTION PFIT_CMP_DFDY

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_D2FDXX(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 0.0D0
         CASE (I_X)
            V = 0.0D0
         CASE (I_Y)
            V = 0.0D0
         CASE (I_XY)
            V = 0.0D0
         CASE (I_XX)
            V = 2.0D0
         CASE (I_YY)
            V = 0.0D0
         CASE (I_XXY)
            V = 2*Y
         CASE (I_XYY)
            V = 0.0D0
         CASE (I_XXYY)
            V = 2*Y*Y
         CASE (I_XXX)
            V = 6*X
         CASE (I_YYY)
            V = 0.0D0
      END SELECT

      PFIT_CMP_D2FDXX = V
      RETURN
      END FUNCTION PFIT_CMP_D2FDXX

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_D2FDXY(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 0.0D0
         CASE (I_X)
            V = 0.0D0
         CASE (I_Y)
            V = 0.0D0
         CASE (I_XY)
            V = 1.0D0
         CASE (I_XX)
            V = 0.0D0
         CASE (I_YY)
            V = 0.0D0
         CASE (I_XXY)
            V = 2*X
         CASE (I_XYY)
            V = 2*Y
         CASE (I_XXYY)
            V = 4*X*Y
         CASE (I_XXX)
            V = 0.0D0
         CASE (I_YYY)
            V = 0.0D0
      END SELECT

      PFIT_CMP_D2FDXY = V
      RETURN
      END FUNCTION PFIT_CMP_D2FDXY

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      REAL*8 FUNCTION PFIT_CMP_D2FDYY(X, Y, IP)

      REAL*8,  INTENT(IN) :: X, Y
      INTEGER, INTENT(IN) :: IP

      REAL*8 :: V
C-----------------------------------------------------------------------

      SELECT CASE (IP)
         CASE (I_C)
            V = 0.0D0
         CASE (I_X)
            V = 0.0D0
         CASE (I_Y)
            V = 0.0D0
         CASE (I_XY)
            V = 0.0D0
         CASE (I_XX)
            V = 0.0D0
         CASE (I_YY)
            V = 2.0D0
         CASE (I_XXY)
            V = 0.0D0
         CASE (I_XYY)
            V = 2*X
         CASE (I_XXYY)
            V = 2*X*X
         CASE (I_XXX)
            V = 0.0D0
         CASE (I_YYY)
            V = 6*Y
      END SELECT

      PFIT_CMP_D2FDYY = V
      RETURN
      END FUNCTION PFIT_CMP_D2FDYY

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Polynomial fit.
C
C Entrée:
C     XY(2,NPNTS)     Position of the points
C     Z(NPNTS)        Value at the points
C
C Sortie:
C     PFT             Internal PFT structure
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_CTR(PFT, ITYP)

      TYPE(PFIT_T), INTENT(OUT), TARGET :: PFT
      INTEGER, INTENT(IN) :: ITYP

      INTEGER :: IERR
      INTEGER :: NCMP
      INTEGER :: KCMP(16)
C-----------------------------------------------------------------------

      IERR = 0

C---     Number of components
      SELECT CASE (ITYP)
         CASE(ITYP_LINEAR)
            NCMP = SIZE(KCMP_LINEAR)
            KCMP(1:NCMP) = KCMP_LINEAR(:)
         CASE(ITYP_BILINEAR)
            NCMP = SIZE(KCMP_BILINEAR)
            KCMP(1:NCMP) = KCMP_BILINEAR(:)
         CASE(ITYP_QUAD)
            NCMP = SIZE(KCMP_QUAD)
            KCMP(1:NCMP) = KCMP_QUAD(:)
         CASE(ITYP_BIQUAD)
            NCMP = SIZE(KCMP_BIQUAD)
            KCMP(1:NCMP) = KCMP_BIQUAD(:)
         CASE(ITYP_CUBIC)
            NCMP = SIZE(KCMP_CUBIC)
            KCMP(1:NCMP) = KCMP_CUBIC(:)
         CASE DEFAULT
            GOTO 9900
      END SELECT

C---     Fill structure
      PFT%CMP_V(1:NCMP) = 0.0D0
      PFT%CMP_I(1:NCMP) = KCMP(1:NCMP)
      PFT%CMP_N = NCMP

      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = IERR_INVALID_TYPE
      GOTO 9999
9901  IERR = IERR_ALLOCATION_ERROR
      GOTO 9999

9999  CONTINUE
      PFIT_CTR = IERR
      RETURN
      END FUNCTION PFIT_CTR

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     PFT             Internal PFT structure
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_DTR(PFT)

      TYPE(PFIT_T), INTENT(INOUT), TARGET :: PFT
C-----------------------------------------------------------------------

      PFT%CMP_V(:) = 0.0D0
      PFT%CMP_I(:) = 0
      PFT%CMP_N = -1

      PFIT_DTR = 0
      RETURN
      END FUNCTION PFIT_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Polynomial fit.
C
C Entrée:
C     XY(2,NPNTS)     Position of the points
C     Z(NPNTS)        Value at the points
C
C Sortie:
C     PFT             Internal PFT structure
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_CALC(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(INOUT), TARGET :: PFT
      INTEGER, INTENT(IN) :: N
      REAL*8,  INTENT(IN) :: XY(2,N)
      REAL*8,  INTENT(IN) :: Z(N)

      REAL*8  X, Y
      INTEGER IP, IC
      INTEGER IERR
      INTEGER NCMP
      INTEGER NPTS

      INTEGER, DIMENSION(:), POINTER :: CMP_I
      REAL*8,  DIMENSION(:,:), ALLOCATABLE :: MTX_A
      REAL*8,  DIMENSION(:),   ALLOCATABLE :: MTX_B
      REAL*8 :: WORK(256)
C-----------------------------------------------------------------------

      IERR = 0

C---     Controls
      IF (PFT%CMP_N .LE. 0) GOTO 9900
      IF (SIZE(XY,2) .LT. PFT%CMP_N) GOTO 9901

C---     Component pointers
      CMP_I => PFT%CMP_I

C---     Sizes
      NPTS = N             ! Number of points
      NCMP = PFT%CMP_N     ! Number of components

C---     Allocate temporary matrix and RHS
      ALLOCATE(MTX_A(NPTS, NCMP), STAT=IERR); IF (IERR .NE. 0) GOTO 9902
      ALLOCATE(MTX_B(NPTS), STAT=IERR);       IF (IERR .NE. 0) GOTO 9902

C---     Fill Vandermonde matrix
      DO IP=1,NPTS
         X = XY(1,IP)
         Y = XY(2,IP)
         DO IC=1,NCMP
            MTX_A(IP,IC) = PFIT_CMP_F(X, Y, CMP_I(IC))
         ENDDO
      ENDDO

C---     Fill the right hand side
      MTX_B(:) = Z(:)

C---     Least-Square solution
      CALL DGELS('N', NPTS, NCMP, 1,
     &            MTX_A, SIZE(MTX_A,1),
     &            MTX_B, SIZE(MTX_B,1),
     &            WORK, 256, IERR)

C---     Keep results
      IF (IERR .EQ. 0) PFT%CMP_V(:) = MTX_B(1:NCMP)

      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = IERR_INVALID_STRUCT
      GOTO 9999
9901  IERR = IERR_INVALID_NBR_POINTS
      GOTO 9999
9902  IERR = IERR_ALLOCATION_ERROR
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(MTX_B)) DEALLOCATE(MTX_B)
      IF (ALLOCATED(MTX_A)) DEALLOCATE(MTX_A)
      PFIT_CALC = IERR
      RETURN
      END FUNCTION PFIT_CALC

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_EVAL(PFT, N, XY, Z, F)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
      REAL*8 :: F
      EXTERNAL F

      REAL*8  X, Y, H
      INTEGER IERR
      INTEGER IP, IC
      INTEGER NCMP

      REAL*8,  DIMENSION(:), POINTER :: CMP_V
      INTEGER, DIMENSION(:), POINTER :: CMP_I
C-----------------------------------------------------------------------

      IERR = 0

C---     Controls
      IF (PFT%CMP_N .LE. 0) GOTO 9900

C---     Component pointers
      CMP_V => PFT%CMP_V
      CMP_I => PFT%CMP_I

C---     Sizes
      NCMP = PFT%CMP_N

C---     Interpolate points
      DO IP=1,N
         X = XY(1,IP)
         Y = XY(2,IP)

         H = 0.0D0
         DO IC=1,NCMP
            H = H + CMP_V(IC)*F(X, Y, CMP_I(IC))
         ENDDO
         Z(IP) = H
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = IERR_INVALID_STRUCT
      GOTO 9999

9999  CONTINUE
      PFIT_EVAL = IERR
      RETURN
      END FUNCTION PFIT_EVAL

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_F(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_F = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_F)
      RETURN
      END FUNCTION PFIT_F

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_DFDX(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_DFDX = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_DFDX)
      RETURN
      END FUNCTION PFIT_DFDX

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_DFDY(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_DFDY = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_DFDY)
      RETURN
      END FUNCTION PFIT_DFDY

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_D2FDXX(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_D2FDXX = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_D2FDXX)
      RETURN
      END FUNCTION PFIT_D2FDXX

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_D2FDXY(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_D2FDXY = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_D2FDXY)
      RETURN
      END FUNCTION PFIT_D2FDXY

C************************************************************************
C Sommaire:
C
C Description:
C     Calculate Thin Plate Spline (PFT) weights from control points.
C
C Entrée:
C     PFT             Internal PFT structure
C     XY(2:NPNTS)     Position of the points
C
C Sortie:
C     Z(NPNTS)        Value at the points
C
C Notes:
C************************************************************************
      INTEGER FUNCTION PFIT_D2FDYY(PFT, N, XY, Z)

      TYPE(PFIT_T), INTENT(IN), TARGET :: PFT
      INTEGER, INTENT(IN)  :: N
      REAL*8,  INTENT(IN)  :: XY(2,N)
      REAL*8,  INTENT(OUT) :: Z(N)
C-----------------------------------------------------------------------

      PFIT_D2FDYY = PFIT_EVAL(PFT, N, XY, Z, PFIT_CMP_D2FDYY)
      RETURN
      END FUNCTION PFIT_D2FDYY

      END MODULE SP_PFIT_private


      MODULE SP_PFIT_M
         USE SP_PFIT_private, ONLY:
     &   SP_PFIT_ERR_INVALID_TYPE       => IERR_INVALID_TYPE,
     &   SP_PFIT_ERR_INVALID_STRUCT     => IERR_INVALID_STRUCT,
     &   SP_PFIT_ERR_INVALID_NBR_POINTS => IERR_INVALID_NBR_POINTS,
     &   SP_PFIT_ERR_ALLOCATION_ERROR   => IERR_ALLOCATION_ERROR,

     &   SP_PFIT_T             => PFIT_T,
     &   SP_PFIT_ITYP_LINEAR   => ITYP_LINEAR,
     &   SP_PFIT_ITYP_BILINEAR => ITYP_BILINEAR,
     &   SP_PFIT_ITYP_QUAD     => ITYP_QUAD,
     &   SP_PFIT_ITYP_BIQUAD   => ITYP_BIQUAD,
     &   SP_PFIT_ITYP_CUBIC    => ITYP_CUBIC,

     &   SP_PFIT_CTR    => PFIT_CTR,
     &   SP_PFIT_DTR    => PFIT_DTR,
     &   SP_PFIT_CALC   => PFIT_CALC,
     &   SP_PFIT_F      => PFIT_F,
     &   SP_PFIT_DFDX   => PFIT_DFDX,
     &   SP_PFIT_DFDY   => PFIT_DFDY,
     &   SP_PFIT_D2FDXX => PFIT_D2FDXX,
     &   SP_PFIT_D2FDXY => PFIT_D2FDXY,
     &   SP_PFIT_D2FDYY => PFIT_D2FDYY
      END MODULE SP_PFIT_M
