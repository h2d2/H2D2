//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonction C de structures de données
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_DS_H_DEJA_INCLU
#define C_DS_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_MAP_CTR         F2C_CONF_DECOR_FNC(C_MAP_CTR,    c_map_ctr)
#define C_MAP_DTR         F2C_CONF_DECOR_FNC(C_MAP_DTR,    c_map_dtr)
#define C_MAP_ASGVAL      F2C_CONF_DECOR_FNC(C_MAP_ASGVAL, c_map_asgval)
#define C_MAP_EFFCLF      F2C_CONF_DECOR_FNC(C_MAP_EFFCLF, c_map_effclf)
#define C_MAP_REQCLF      F2C_CONF_DECOR_FNC(C_MAP_REQCLF, c_map_reqclf)
#define C_MAP_REQVAL      F2C_CONF_DECOR_FNC(C_MAP_REQVAL, c_map_reqval)
#define C_MAP_REQDIM      F2C_CONF_DECOR_FNC(C_MAP_REQDIM, c_map_reqdim)
#define C_MAP_ESTCLF      F2C_CONF_DECOR_FNC(C_MAP_ESTCLF, c_map_estclf)

#define C_MIV_CTR         F2C_CONF_DECOR_FNC(C_MIV_CTR,    c_miv_ctr)
#define C_MIV_DTR         F2C_CONF_DECOR_FNC(C_MIV_DTR,    c_miv_dtr)
#define C_MIV_ASGVAL      F2C_CONF_DECOR_FNC(C_MIV_ASGVAL, c_miv_asgval)
#define C_MIV_EFFCLF      F2C_CONF_DECOR_FNC(C_MIV_EFFCLF, c_miv_effclf)
#define C_MIV_ITER        F2C_CONF_DECOR_FNC(C_MIV_ITER,   c_miv_iter)
#define C_MIV_REQCLF      F2C_CONF_DECOR_FNC(C_MIV_REQCLF, c_miv_reqclf)
#define C_MIV_REQVAL      F2C_CONF_DECOR_FNC(C_MIV_REQVAL, c_miv_reqval)
#define C_MIV_REQDIM      F2C_CONF_DECOR_FNC(C_MIV_REQDIM, c_miv_reqdim)
#define C_MIV_ESTCLF      F2C_CONF_DECOR_FNC(C_MIV_ESTCLF, c_miv_estclf)

#define C_LST_CTR         F2C_CONF_DECOR_FNC(C_LST_CTR,    c_lst_ctr)
#define C_LST_DTR         F2C_CONF_DECOR_FNC(C_LST_DTR,    c_lst_dtr)
#define C_LST_AJTVAL      F2C_CONF_DECOR_FNC(C_LST_AJTVAL, c_lst_ajtval)
#define C_LST_ASGVAL      F2C_CONF_DECOR_FNC(C_LST_ASGVAL, c_lst_asgval)
#define C_LST_CLR         F2C_CONF_DECOR_FNC(C_LST_CLR,    c_lst_clr)
#define C_LST_EFFVAL      F2C_CONF_DECOR_FNC(C_LST_EFFVAL, c_lst_effval)
#define C_LST_REQVAL      F2C_CONF_DECOR_FNC(C_LST_REQVAL, c_lst_reqval)
#define C_LST_TRVVAL      F2C_CONF_DECOR_FNC(C_LST_TRVVAL, c_lst_trvval)
#define C_LST_REQDIM      F2C_CONF_DECOR_FNC(C_LST_REQDIM, c_lst_reqdim)


F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MAP_CTR     ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_DTR     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ASGVAL  (hndl_t*, F2C_CONF_STRING, F2C_CONF_STRING F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_EFFCLF  (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQCLF  (hndl_t*, fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQVAL  (hndl_t*, F2C_CONF_STRING, F2C_CONF_STRING F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_REQDIM  (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MAP_ESTCLF  (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);

F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MIV_CTR     ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_DTR     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ASGVAL  (hndl_t*, fint_t*, hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_EFFCLF  (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ITER    (hndl_t*, hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQCLF  (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQVAL  (hndl_t*, fint_t*, hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_REQDIM  (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MIV_ESTCLF  (hndl_t*, fint_t*);

F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_LST_CTR     ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_DTR     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_AJTVAL  (hndl_t*, F2C_CONF_STRING  F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_ASGVAL  (hndl_t*, fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_CLR     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_EFFVAL  (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_REQVAL  (hndl_t*, fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_TRVVAL  (hndl_t*, fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_LST_REQDIM  (hndl_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_DS_H_DEJA_INCLU
