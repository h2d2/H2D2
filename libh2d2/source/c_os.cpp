//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Operating System
// Sommaire: Fonctions C du système d'opération (Operating System)
//************************************************************************
#include "c_os.h"
#include "f_os.h"

#include <algorithm>
#include <iterator>
#include <fstream>
#include <list>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <string>
#if defined(H2D2_WINDOWS)
#  include <direct.h>
#  include <io.h>
#  include <fcntl.h>
#  include <process.h>
#  include <sys/stat.h>
#  include <windows.h>
#  include <winver.h>
#elif defined(H2D2_UNIX)
#  include <sys/param.h>
#  include <sys/stat.h>
#  include <errno.h>
#  include <dirent.h>
#  include <fnmatch.h>
#  include <unistd.h>
#else
#  error Invalid operating system
#endif
#ifndef S_ISDIR
#  define S_ISDIR(m)  (((m) & _S_IFMT) == _S_IFDIR)
#endif
#ifndef S_ISREG
#  define S_ISREG(m)  (((m) & _S_IFMT) == _S_IFREG)
#endif

// ---  Redéfinition de std::min local à cause de MSVC win64
template<typename Tp> inline const Tp& std_min(const Tp& l, const Tp& r)
{
   return (l < r) ? l : r;
}

typedef std::string           TTDirEntry;
typedef std::list<TTDirEntry> TTDirEntries;

// ---  Prototypes des fonctions privées
int  fullPath(char const*,  char*, int);
int  listDir (TTDirEntries&, const std::string&, const std::string&, bool recurse = false);
void reqArgv (fint_t, char*, int);

fint_t C_OS_SIGINT  = SIGINT;
fint_t C_OS_SIGILL  = SIGILL;
fint_t C_OS_SIGABRT = SIGABRT;
fint_t C_OS_SIGFPE  = SIGFPE;
fint_t C_OS_SIGSEGV = SIGSEGV;
fint_t C_OS_SIGTERM = SIGTERM;

//************************************************************************
// Sommaire: Normalisation de chemin de fichier.
//
// Description:
//    La fonction privée <code>fullPath(...)</code> normalise le chemin du
//    fichier. Le chemin normalisé est retourné dans le buffer bP comme
//    une chaîne C, terminée par 0x0.
//
// Entrée:
//    char* pP;      Null terminated partial path
//    int   n;       Max buffer length
//
// Sortie:
//    char* bP;      Buffer
//
// Notes:
//
//************************************************************************
int fullPath(char const* pP, char* bP, int n)
{

   int ierr = -1;

#if defined(H2D2_WINDOWS)
   CHAR buf[MAX_PATH];

   if (GetFullPathNameA(pP, MAX_PATH, buf, NULL) != 0)
   {
      if (n > 0 && static_cast<size_t>(n) >= strlen(buf))
      {
         memset(bP, 0x0, n);
         memcpy(bP, buf, strlen(buf));
         ierr = 0;
      }
   }

#elif defined(H2D2_UNIX)
   int path_max = -1;
   #ifdef PATH_MAX
      path_max = PATH_MAX;
   #else
      path_max = pathconf(pP, _PC_PATH_MAX);
      if (path_max <= 0)
         path_max = 4096;
   #endif

   char* buf = new char[path_max];

   ierr = (realpath(pP, buf) == NULL) ? -1 : 0;
   if (ierr == -1)   
   {
      printf("fullPath: realpath ERROR\n");
      printf("   path: %s\n", pP);
      perror("   ERROR:");
      memset(buf, 0x0, path_max);
      ssize_t r = readlink(pP, buf, path_max-1);
      ierr = (r == -1 || r == (path_max-1)) ? -1 : 0;
      if (ierr == -1)
      {
         printf("fullPath: readlink ERROR\n");
         printf("   path: %s\n", pP);
         perror("   ERROR:");
      }
   }

   if (ierr == 0)
   {
      ierr = -1;
      if (n > 0 && static_cast<size_t>(n) >= strlen(buf))
      {
         memset(bP, 0x0, n);
         memcpy(bP, buf, strlen(buf));
         ierr = 0;
      }
   }

   delete[] buf;

#else
#  error Invalid operating system
#endif

    return ierr;
}

//************************************************************************
// Sommaire: Directory.
//
// Description:
//    La fonction privée <code>listDir(...)</code> retourne dans 
//    <code>dirEntires</code> la liste non triée des fichiers de 
//    <code>dir</code> correspondant au patron <code>pat>.
//    Si <code>recurse</code> est <code>true</code> la recherche
//    est récursive.
//    La fonction retourne:
//       0     OK
//      -3     Erreur dans le scan
//
// Entrée:
//    const std::string& dir      Répertoire à lister
//    const std::string& pat      Patron de recherche
//    bool recurse
//
// Sortie:
//    TTDirEntries& dirEntries
//
// Notes:
//
//************************************************************************
int listDir(TTDirEntries& dirEntries, const std::string& dir_, const std::string& pat, bool recurse)
{

   fint_t ierr = 0;

   const std::string dir = (! dir_.empty()) ? dir_ : ".";

   // ---  Monte et execute la commande
#if defined(H2D2_WINDOWS)
   WIN32_FIND_DATAA ffd;
   HANDLE hFind = INVALID_HANDLE_VALUE;

   if (recurse)
   {
      if (ierr == 0)
      {
         const std::string path = dir + "\\*.*";
         hFind = FindFirstFileA(path.c_str(), &ffd);
         if (hFind == INVALID_HANDLE_VALUE &&
             GetLastError() != ERROR_FILE_NOT_FOUND) ierr = -3;
      }

      if (hFind != INVALID_HANDLE_VALUE)
      {
         do
         {
            if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
               if (strcmp(ffd.cFileName, ".") == 0) continue;
               if (strcmp(ffd.cFileName, "..")== 0) continue;
               const std::string path = dir + "\\" + ffd.cFileName;
               ierr = listDir(dirEntries, path, pat, recurse);
            }
         } while (ierr == 0 && FindNextFileA(hFind, &ffd) != 0);
      }

      if (hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
   }

   if (ierr == 0)
   {
      if (ierr == 0)
      {
         const std::string path = dir + "\\" + pat;
         hFind = FindFirstFileA(path.c_str(), &ffd);
         if (hFind == INVALID_HANDLE_VALUE &&
            GetLastError() != ERROR_FILE_NOT_FOUND) ierr = -3;
      }

      if (hFind != INVALID_HANDLE_VALUE)
      {
         do
         {
            if (strcmp(ffd.cFileName, ".") == 0) continue;
            if (strcmp(ffd.cFileName, "..")== 0) continue;
            const std::string path = dir + "\\" + ffd.cFileName;
            dirEntries.push_back(path);
         } while (FindNextFileA(hFind, &ffd) != 0);
      }

      if (hFind != INVALID_HANDLE_VALUE) FindClose(hFind);
   }
#elif defined(H2D2_UNIX)
   if (recurse)
   {
      DIR* rep = opendir(dir.c_str());
      if (rep == NULL) ierr = -3;

      if (rep != NULL)
      {
         dirent* de;
         while (ierr == 0 && (de = readdir(rep)) != NULL)
         {
            if (de->d_type == DT_DIR)
            {
               if (strcmp(de->d_name, ".") == 0) continue;
               if (strcmp(de->d_name, "..")== 0) continue;
               const std::string path = dir + "/" + de->d_name;
               ierr = listDir(dirEntries, path, pat, recurse);
            }
         }
      }
      if (rep != NULL) closedir(rep);
   }

   if (ierr == 0)
   {
      DIR* rep = opendir(dir.c_str());
      if (rep == NULL) ierr = -3;

      if (rep != NULL)
      {
         dirent* de;
         while ((de = readdir(rep)) != NULL)
         {
            if (fnmatch(pat.c_str(), de->d_name, FNM_PATHNAME | FNM_PERIOD) == 0)
            {
               const std::string path = dir + "/" + de->d_name;
               dirEntries.push_back(path);
            }
         }
      }
      if (rep != NULL) closedir(rep);
   }
#else
#  error Invalid operating system
#endif
   return ierr;
}

//************************************************************************
// Sommaire:  Fonctions pour récupérer argv.
//
// Description:
//    La fonction privée <code>reqArgv(...)</code> permet de récupérer le
//    i.ème argument de la ligne de commande.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
void reqArgv(fint_t i, char* buf, int lbuf)
{
   char arg[256];
   F_OS_IARGLC(&i, arg, 256);
   for (char* cP = arg+255; *cP == ' ' && cP != arg; --cP) *cP = 0x0;
   memset(buf, 0x0, lbuf);
   memcpy(buf, arg, std_min(static_cast<int>(strlen(arg)), lbuf-1));
   return;
}

//************************************************************************
// Sommaire: Retourne le message d'erreur système.
//
// Description:
//    La fonction <code>C_OS_ERRMSG(...)</code> retourne dans <code>str</code>
//    le message d'erreur système.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le message
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ERRMSG (F2C_CONF_STRING  str
                                                           F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ERRMSG (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   char* buf = strerror(errno);
   memset(sP, ' ', l);
   memcpy(sP, buf, std_min(static_cast<size_t>(l), strlen(buf)));

   return 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_OS_ABORT(...)</code> lève le signal SIGABRT.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ABORT()
{
   return C_OS_RAISE(&C_OS_SIGABRT);
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_OS_RAISE(...)</code> lève le signal sig.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_RAISE(fint_t* sig)
{
#if defined(H2D2_WINDOWS)
   RaiseException(static_cast<DWORD>(*sig), 0, 0, NULL);
#elif defined(H2D2_UNIX)
   raise(*sig);
#else
#  error Invalid operating system
#endif
   return -1;
}

//************************************************************************
// Sommaire: Retourne le répertoire complet du programme.
//
// Description:
//    La fonction <code>C_OS_REPEXE(...)</code> retourne dans <code>str</code>
//    le répertoire complet (full path) du programme.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom de répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REPEXE (F2C_CONF_STRING  str
                                                           F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REPEXE (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   char path[256];

#if defined(H2D2_WINDOWS)
   reqArgv(0, path, 256);
#elif defined(H2D2_UNIX)
   sprintf(path, "/proc/self/exe");
#else
#  error Invalid operating system
#endif
   if (0 == fullPath(path, path, 256))
   {
      if (l > 0)
      {
         char* endP = strrchr(path, '/');
         char* bckP = strrchr(path, '\\');
         char* ddtP = strrchr(path, ':');
         endP = (endP > bckP) ? endP : bckP;
         endP = (endP > ddtP) ? endP : ddtP;
         if (endP != NULL && (endP-path) < static_cast<ptrdiff_t>(l))
         {
            memset(sP, ' ', l);
            memcpy(sP, path, endP-path);
            ierr = 0;
         }
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire: Retourne le nom du programme.
//
// Description:
//    La fonction <code>C_OS_NOMEXE(...)</code> retourne dans <code>str</code>
//    le nom du programme tel que passé en argument au process.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom de l’exécutable
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NOMEXE (F2C_CONF_STRING  str
                                                           F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NOMEXE (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   char path[256];
#if defined(H2D2_WINDOWS)
   reqArgv(0, path, 256);
#elif defined(H2D2_UNIX)
   sprintf(path, "/proc/self/exe");
#else
#  error Invalid operating system
#endif
   if (0 == fullPath(path, path, 256))
   {
      if (l > 0)
      {
         char* endP = strrchr(path, '/');
         char* bckP = strrchr(path, '\\');
         char* ddtP = strrchr(path, ':');
         endP = (endP > bckP) ? endP : bckP;
         endP = (endP > ddtP) ? endP : ddtP;
         if (endP == NULL && strlen(path) < static_cast<size_t>(l))
         {
            memset(sP, ' ', l);
            memcpy(sP, path, strlen(path));
         }
         else
         {
            int lpath = static_cast<int>(path + strlen(path) - endP - 1);
            if (lpath < l)
            {
               memset(sP, ' ', l);
               memcpy(sP, endP+1, lpath);
            }
         }
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire: Assigne une valeur à une variable d'environnement
//
// Description:
//    La fonction <code>C_OS_ASGVARENV(...)</code> assigne la valeur
//    <code>val</code> à la variable d'environnement <code>var</code>
//    passée en argument.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    WatForStr* var;      Le nom de la variable
//    WatForStr* val;      La valeur de la variable
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ASGVARENV (F2C_CONF_STRING  varP,
                                                              F2C_CONF_STRING  valP
                                                              F2C_CONF_SUP_INT lvar
                                                              F2C_CONF_SUP_INT lval)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ASGVARENV (F2C_CONF_STRING  var,
                                                              F2C_CONF_STRING  val)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
#else
   char* varP  = var->strP;
   int   lvar  = var->len;
   char* valP  = val->strP;
   int   lval  = val->len;
#endif

   fint_t ierr = -1;

   // ---  Monte la nouvelle définition
   std::string newEnv = std::string(varP, lvar) + '=' + std::string(valP, lval);

   // ---  Assigne la valeur
   ierr = putenv( const_cast<char*>(newEnv.c_str()) );

   return ierr;
}

//************************************************************************
// Sommaire: Retourne le valeur de la variable d'environnement
//
// Description:
//    La fonction <code>C_OS_REQVARENV(...)</code> retourne dans
//    <code>val</code> la valeur de la variable d'environnement
//    <code>var</code> passée en argument.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    WatForStr* var;      Le nom de la variable
//
// Sortie:
//    WatForStr* val;      La valeur de la variable
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REQVARENV (F2C_CONF_STRING  varP,
                                                              F2C_CONF_STRING  valP
                                                              F2C_CONF_SUP_INT lvar
                                                              F2C_CONF_SUP_INT lval)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REQVARENV (F2C_CONF_STRING  var,
                                                              F2C_CONF_STRING  val)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
#else
   char* varP  = var->strP;
   int   lvar  = var->len;
   char* valP  = val->strP;
   int   lval  = val->len;
#endif

   char*  resP;
   fint_t ierr = -1;

   std::string var(varP, lvar);
   resP = getenv(var.c_str());

   if (resP != NULL && strlen(resP) <= static_cast<size_t>(lval))
   {
      memset(valP, ' ', lval);
      memcpy(valP, resP, strlen(resP));
      ierr = 0;
   }

   return ierr;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_OS_VERSION()</code> retourne le nom de la version
//    du module dont le nom est passé en argument.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_VERSION (F2C_CONF_STRING  mdl,
                                                            F2C_CONF_STRING  ver
                                                            F2C_CONF_SUP_INT lmdl
                                                            F2C_CONF_SUP_INT lver)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_VERSION (F2C_CONF_STRING  mdl,
                                                            F2C_CONF_STRING  ver)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
#  if defined(H2D2_WINDOWS)
      char* mP = mdl;
      int   lm = lmdl;
#  endif
   char* vP = ver;
   int   lv = lver;
#else
#  if defined(H2D2_WINDOWS)
      char* mP = mdl->strP;
      int   lm = mdl->len;
#  endif
   char* vP = ver->strP;
   int   lv = ver->len;
#endif

   bool ierr = false;

#if defined(H2D2_WINDOWS)
   HMODULE hmdl = NULL;
   if (! ierr)
   {
      std::string m(mP, lm);
      hmdl = GetModuleHandleA(m.c_str());
      ierr = (hmdl == NULL);
   }

   LPTSTR lptstrFilename = NULL;
   if (! ierr)
   {
      DWORD bufSize = 0;
      DWORD ret = 0;
      do
      {
         if (lptstrFilename != NULL) delete[] lptstrFilename;
         bufSize += 1024;
         lptstrFilename = new TCHAR[bufSize];
         ret = GetModuleFileName(hmdl, lptstrFilename, bufSize);
      } while (ret == bufSize);
      ierr = (ret == 0);
   }

   DWORD dummy = 0;
   DWORD infoSize = 0;
   if (! ierr)
   {
      infoSize = GetFileVersionInfoSize(lptstrFilename, &dummy);
      ierr = (infoSize <= 0);
   }

   char* infoBuff = NULL;
   if (! ierr)
   {
      infoBuff = new char[infoSize];
      BOOL ret = GetFileVersionInfo(lptstrFilename,      // file name
                                    dummy,               // ignored
                                    infoSize,            // size of buffer
                                    infoBuff);           // version information buffer
      ierr = (ret == 0);
   }

   PCHAR verVal = NULL;
   UINT  verLen = 0;
   struct LANGANDCODEPAGE
   {
      WORD wLanguage;
      WORD wCodePage;
   } *lpTranslate;

   // Read the list of languages and code pages
   if (! ierr)
   {
      UINT cbTranslate;
      BOOL ret = VerQueryValue(infoBuff,
                               TEXT("\\VarFileInfo\\Translation"),
                               (LPVOID*)&lpTranslate,
                               &cbTranslate);
      ierr = (ret == 0);
   }

   // Retrieve file description for language and code page "i".
   if (! ierr)
   {
      CHAR subBlock[256];
      int n = sprintf(subBlock,
                      "\\StringFileInfo\\%04x%04x\\FileVersion",
                      lpTranslate[0].wLanguage,
                      lpTranslate[0].wCodePage);
      ierr = (n < 0);

      BOOL ret;
      if (! ierr)
      {
         ret = VerQueryValueA(infoBuff,
                              subBlock,
                              (LPVOID*)&verVal,
                              &verLen);
         ierr = (ret == 0);
      }
   }

   if (! ierr)
   {
      memset(vP, ' ', lv);
      memcpy(vP, verVal, std_min(static_cast<int>(verLen)-1,lv));
   }

   if (infoBuff != NULL) delete[] infoBuff;
   if (lptstrFilename != NULL) delete[] lptstrFilename;

#else
   memset(vP, ' ', lv);

#endif

   return (ierr) ? -1 : 0;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_OS_HOSTNAME()</code> retourne le nom de
//    l'ordinateur sur lequel s’exécute le process.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_HOSTNAME(F2C_CONF_STRING  name
                                                            F2C_CONF_SUP_INT lname)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_HOSTNAME(F2C_CONF_STRING  name)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* nP = name;
   int   lp = lname;
#else
   char* nP = name->strP;
   int   lp = name->len;
#endif

   int ierr = -1;
   char buf[256];

#if   defined(H2D2_WINDOWS)
   DWORD lbuf = sizeof(buf);
   ierr = GetComputerNameA(buf, &lbuf) ? 0 : -1;
#elif defined(H2D2_UNIX)
   ierr = gethostname(buf, sizeof(buf));
#else
#  error Invalid operating system
#endif

   if (ierr == 0)
   {
      memset(nP, ' ', lp);
      memcpy(nP, buf, std_min(static_cast<int>(strlen(buf)),lp));
   }

   return (ierr == 0) ? 0 : -1;
}

//************************************************************************
// Sommaire:
//
// Description:
//    La fonction <code>C_OS_PLATFORM()</code> retourne le nom de la
//    plate-forme de compilation.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_PLATFORM(F2C_CONF_STRING  ptf
                                                            F2C_CONF_SUP_INT lptf)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_PLATFORM(F2C_CONF_STRING  ptf)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* pP = ptf;
   int   lp = lptf;
#else
   char* pP = ptf->strP;
   int   lp = ptf->len;
#endif

   bool ierr = false;

   std::ostringstream os;

#if   defined(H2D2_WINDOWS)
   os << "OS type: win";
#elif defined(H2D2_UNIX)
   os << "OS type: unix/linux";
#endif

#if defined(H2D2_ARCH_NAME)
   os << "; Architecture: " << H2D2_ARCH_NAME;
#endif
#if   defined(H2D2_INTSIZE)
   os << "; Integer size: " << H2D2_INTSIZE;
#endif


#if defined(H2D2_CMPLR_NAME)
   os << "; Compiler: " << H2D2_CMPLR_NAME;
#endif
#if defined(H2D2_CMPLR_VFMT) && defined(H2D2_CMPLR_VERS)
   os << " (" << H2D2_CMPLR_VFMT << H2D2_CMPLR_VERS << std::dec << ")";
#endif

#if defined(MODE_DEBUG)
   os << "; Compile mode: DEBUG";
#endif

   os << std::ends;
   if (! ierr)
   {
      const std::string s = os.str();
      memset(pP, ' ', lp);
      memcpy(pP, s.c_str(), std_min(static_cast<int>(s.size()-1),lp));
   }

   return (ierr) ? -1 : 0;
}

//************************************************************************
// Sommaire: Retourne le répertoire courant.
//
// Description:
//    La fonction <code>C_OS_CWD(...)</code> retourne le nom du répertoire
//    courant.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_CWD (F2C_CONF_STRING  str
                                                        F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_CWD (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   char path[256];
   char* p = NULL;

#if defined(H2D2_WINDOWS)
   p = _getcwd(path, 256);
#elif defined(H2D2_UNIX)
   p =  getcwd(path, 256);
#else
#  error Invalid operating system
#endif

   if (p != NULL)
   {
      memset(sP, ' ', l);
      memcpy(sP, path, std_min(static_cast<int>(strlen(path)),l));
   }

   return (p != NULL) ? 0 : -1;
}

//************************************************************************
// Sommaire: Retourne un nom de fichier temporaire.
//
// Description:
//    La fonction <code>C_OS_FICTMP(...)</code> retourne dans <code>str</code>
//    le nom d'un fichier temporaire.
//    Le fichier n'est pas détruis automatiquement. Il est donc de la
//    responsabilité du code appelant de disposer du fichier.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du fichier temporaire
//
// Notes:
//    Lorsqu'on travaille sur un répertoire NFS (souvent le cas avec plusieurs
//    noeuds de calcul), il est possible que l'appel à mkstemp échoue.
//    On peut vérifier le contenu de errno (include errno.h) pour connaître
//    l'erreur. Il serait bien d'ajouter une fonction pour gérer ces erreurs.
//    L'erreur qui a déjà été obtenue est "Stale NFS file handle".
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_FICTMP(F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_FICTMP(F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   char* fP = NULL;
   int fd = -1;

#if defined (H2D2_WINDOWS)
   static unsigned uniquer = 0;
   char tmplt[MAX_PATH]; tmplt[0] = 0x0;
   char* tmpDir = getenv("TMP");
   if (! tmpDir) tmpDir = getenv("TEMP");
   if (! tmpDir) tmpDir = getenv("LOCALAPPDATA");
   if (! tmpDir) tmpDir = getenv("APPDATA");
   int nprint = -1;
   if (tmpDir)
      nprint = sprintf(tmplt, "%s\\~inrs_%i_%03x_XXXXXX", tmpDir, getpid(), uniquer++);
   else
      nprint = sprintf(tmplt, "%s\\~inrs_%i_%03x_XXXXXX", ".", getpid(), uniquer++);
   // TODO: Début section critique (mutex)
   //!$omp critical(OMP_CS_COS)
   if (nprint > 0) fP = _mktemp(tmplt);
   if (fP != NULL)
   {
      fullPath(tmplt, tmplt, MAX_PATH);
      fP = tmplt;
      fd = open(fP, O_CREAT | O_EXCL | O_RDWR , S_IREAD | S_IWRITE );
   }
   else
   {
      fP = NULL;
   }
   //!$omp end critical(OMP_CS_COS)
   // TODO: Fin section critique
#else
   char tmplt[256];
   char* tmpDir = getenv("TMP");
   if (! tmpDir) tmpDir = getenv("TEMP");
   if (tmpDir)
      sprintf(tmplt, "%s/~inrs_%i_XXXXXX", tmpDir, getpid());
   else
      sprintf(tmplt, "%s/~inrs_%i_XXXXXX", ".", getpid());
   fd = mkstemp(tmplt);
   fullPath(tmplt, tmplt, 256);
   fP = tmplt;
#endif

   // ---  Ferme le fichier
   if (fd != -1 && close(fd) == -1)
   {
      fd = -1;
   }
   // ---  Transfert le nom
   if (fd != -1)
   {
      if (l > 0 && static_cast<size_t>(l) >= strlen(fP))
      {
         if (fP[0] == '\\' && strchr(fP+1,'\\') == NULL) ++fP;
         memset(sP, ' ', l);
         memcpy(sP, fP, strlen(fP));
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire: Efface un fichier.
//
// Description:
//    La fonction <code>C_OS_DELFIC(...)</code> efface le fichier <code>str</code>.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du fichier
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DELFIC(F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DELFIC(F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   if (l > 0)
   {
      char last = sP[l];
      if (last != 0x0) sP[l] = 0x0;
      ierr = remove(sP);
      if (last != 0x0) sP[l] = last;
   }

   return ierr;
}

//************************************************************************
// Sommaire: Crée un répertoire.
//
// Description:
//    La fonction <code>C_OS_MKDIR(...)</code> crée le répertoire <code>str</code>.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_MKDIR (F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_MKDIR (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   std::string path(sP, l);

#if defined(H2D2_WINDOWS)
   ierr = _mkdir(path.c_str());
#elif defined(H2D2_UNIX)
   ierr = mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP);
#else
#  error Invalid operating system
#endif

   if (ierr)
   {
      const char* eP = strerror(errno);
      printf("mkdir reported an error: %s\n", eP);
   }

   return ierr;
}

//************************************************************************
// Sommaire: Supprime un répertoire.
//
// Description:
//    La fonction <code>C_OS_RMDIR(...)</code> supprime le répertoire
//    <code>str</code>. Le répertoire doit être vide.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_RMDIR (F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_RMDIR (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = -1;

   std::string path(sP, l);

#if defined(H2D2_WINDOWS)
   ierr = _rmdir(path.c_str());
#elif defined(H2D2_UNIX)
   ierr = rmdir(path.c_str());
#else
#  error Invalid operating system
#endif

   return ierr;
}

//************************************************************************
// Sommaire: Directory.
//
// Description:
//    La fonction <code>C_OS_DIR(...)</code> retourne la liste des fichiers
//    dans <code>dir</code> correspondant au patron <code>pat</code>. La
//    traversée des répertoires est recursive si <code>rcr</code> est non nul.
//    Le résultat est envoyé dans le fichier <code>fic</code>, un nom
//    de fichier par ligne.
//    Retourne:
//       0     OK
//      -1     Erreur dans les paramètres
//      -2     Erreur écriture fichier
//      -3     Erreur dans le scan
//
// Entrée:
//    WatForStr* dir;      Répertoire à lister
//    WatForStr* pat;      Patron de recherche
//    WatForStr* fic;      Fichier résultat
//    fint_t*    rcr;      Récursif si (*rcr != 0)
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIR (F2C_CONF_STRING  dir,
                                                        F2C_CONF_STRING  pat,
                                                        F2C_CONF_STRING  fic,
                                                        fint_t*          rcr
                                                        F2C_CONF_SUP_INT ldir
                                                        F2C_CONF_SUP_INT lpat
                                                        F2C_CONF_SUP_INT lfic)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIR (F2C_CONF_STRING  dir,
                                                        F2C_CONF_STRING  pat,
                                                        F2C_CONF_STRING  fic,
                                                        fint_t*          rcr)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* dP = dir;
   int   ld = ldir;
   char* pP = pat;
   int   lp = lpat;
   char* fP = fic;
   int   lf = lfic;
#else
   char* dP = dir->strP;
   int   ld = dir->len;
   char* pP = pat->strP;
   int   lp = pat->len;
   char* fP = fic->strP;
   int   lf = fic->len;
#endif

   fint_t ierr = 0;

   // ---  Contrôle les paramètres
   // if (ld <= 0) ierr = -1; // dir can be empty
   if (lp <= 0) ierr = -1;
   if (lf <= 0) ierr = -1;

   // ---  Passe les chaînes en C
   if (dP[ld-1] == '\\' || dP[ld-1] == '/') ld -= 1;
   const std::string d(dP, ld);
   const std::string p(pP, lp);
   const std::string f(fP, lf);

   // ---  Exécute la commande
   bool r = (*rcr != 0);
   TTDirEntries dirEntries;
   if (ierr == 0)
      ierr = listDir(dirEntries, d, p, r);
   if (ierr == 0)
      dirEntries.sort();

   // ---  Écris le fichier de retour
   if (ierr == 0)
   {
      std::ofstream os = std::ofstream(f);
      if (os)
         std::copy(dirEntries.begin(),
                   dirEntries.end(), 
                   std::ostream_iterator<TTDirEntry>(os, "\n"));
      if (os) os.close();
      if (!os) ierr = -2;
   }

   return ierr;
}

//************************************************************************
// Sommaire: Test si un chemin est un répertoire.
//
// Description:
//    La fonction <code>C_OS_ISDIR(...)</code> retourne 1 si le chemin
//    <code>str</code> représente un répertoire, 0 sinon.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISDIR (F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISDIR (F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   std::string path(sP, l);
   struct stat m;

   return (stat(path.c_str(),&m) == 0 && S_ISDIR(m.st_mode)) ? 1 : 0;
}

//************************************************************************
// Sommaire: Test si un chemin est un fichier.
//
// Description:
//    La fonction <code>C_OS_ISFILE(...)</code> retourne 1 si le chemin
//    <code>str</code> représente un fichier, 0 sinon.
//
// Entrée:
//
// Sortie:
//    WatForStr* str;      Le nom du fichier
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISFILE(F2C_CONF_STRING  str
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISFILE(F2C_CONF_STRING  str)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   std::string path(sP, l);
   struct stat m;

   return (stat(path.c_str(),&m) == 0 && S_ISREG(m.st_mode)) ? 1 : 0;
}

//************************************************************************
// Sommaire: Normalisation de chemin de fichier.
//
// Description:
//    La fonction <code>C_OS_NORMPATH(...)</code> normalise le chemin du
//    fichier.
//
// Entrée:
//    WatForStr* dir;      Chemin du fichier à normaliser
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NORMPATH(F2C_CONF_STRING  dP
                                                            F2C_CONF_SUP_INT ld)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NORMPATH(F2C_CONF_STRING  dir)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
#else
   char* dP = dir->strP;
   int   ld = dir->len;
#endif

   std::string p(dP, ld);
   // --- rtrim car dP est plus long pour contenir le retour
   p = p.substr(0, p.find_last_not_of(' ') + 1);

   char path[256];
   fint_t ierr = fullPath(p.c_str(), path, 256);

   if (ierr == 0)
   { 
      ierr = -1;
      if (strlen(path) <= static_cast<size_t>(ld))
      {
         memset(dP, ' ', ld);
         memcpy(dP, path, strlen(path));
         ierr = 0;
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire: Extrait le répertoire du chemin.
//
// Description:
//    La fonction <code>C_OS_DIRNAME(...)</code> retourne dans <code>pth</code>
//    le répertoire du chemin passé en argument.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    WatForStr* pth;      Nom du chemin (path complet)
//
// Sortie:
//    WatForStr* pth;      Nom du répertoire
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIRNAME(F2C_CONF_STRING  pth
                                                           F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIRNAME(F2C_CONF_STRING  pth)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = pth;
   int   l = len;
#else
   char* sP = pth->strP;
   int   l = pth->len;
#endif

   fint_t ierr = -1;

   char path[256];
   std::string p(sP, l);
   p = p.substr(0, p.find_last_not_of(' ') + 1);

   if (0 == fullPath(p.c_str(), path, 256))
   {
      if (l > 0)
      {
         char* endP = strrchr(path, '/');
         char* bckP = strrchr(path, '\\');
         char* ddtP = strrchr(path, ':');
         endP = (endP > bckP) ? endP : bckP;
         endP = (endP > ddtP) ? endP : ddtP;
         if (endP != NULL)
         {
            memset(sP, ' ', l);
            if (endP == ddtP)    // Keep the ':'
               memcpy(sP, path, endP - path + 1);
            else                 // Skip '/' or '\\'
               memcpy(sP, path, endP - path);
            ierr = 0;
         }
      }
   }

   return ierr;
}

//************************************************************************
// Sommaire: Retourne le fichier du chemin.
//
// Description:
//    La fonction <code>C_OS_BASENAME(...)</code> retourne dans <code>pth</code>
//    le fichier du chemin passé en argument.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    WatForStr* pth;      Nom du chemin (path complet)
//
// Sortie:
//    WatForStr* pth;      Nom du fichier
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_BASENAME(F2C_CONF_STRING  pth
                                                            F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_BASENAME(F2C_CONF_STRING  pth)
#endif
{
#if defined(F2C_CONF_A_SUP_INT)
   char* sP = pth;
   int   l = len;
#else
   char* sP = pth->strP;
   int   l = pth->len;
#endif

   fint_t ierr = -1;

   char path[256];
   std::string p(sP, l);
   p = p.substr(0, p.find_last_not_of(' ') + 1);

   if (0 == fullPath(p.c_str(), path, 256))
   {
      if (l > 0)
      {
         char* endP = strrchr(path, '/');
         char* bckP = strrchr(path, '\\');
         char* ddtP = strrchr(path, ':');
         endP = (endP > bckP) ? endP : bckP;
         endP = (endP > ddtP) ? endP : ddtP;
         if (endP != NULL)
         {
            memset(sP, ' ', l);
            memcpy(sP, endP + 1, strlen(endP)-1);
            ierr = 0;
         }
      }
   }

   return ierr;
}
