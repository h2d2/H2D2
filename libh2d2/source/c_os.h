//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Groupe:  fonctions C
// Sous-Groupe:  Operating System
// Sommaire: Fonctions C du système d'opération (Operating System)
//************************************************************************
#ifndef C_OS_H_DEJA_INCLU
#define C_OS_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_OS_ERRMSG    F2C_CONF_DECOR_FNC(C_OS_ERRMSG,   c_os_errmsg)
#define C_OS_ABORT     F2C_CONF_DECOR_FNC(C_OS_ABORT,    c_os_abort)
#define C_OS_RAISE     F2C_CONF_DECOR_FNC(C_OS_RAISE,    c_os_raise)
#define C_OS_REPEXE    F2C_CONF_DECOR_FNC(C_OS_REPEXE,   c_os_repexe)
#define C_OS_NOMEXE    F2C_CONF_DECOR_FNC(C_OS_NOMEXE,   c_os_nomexe)
#define C_OS_ASGVARENV F2C_CONF_DECOR_FNC(C_OS_ASGVARENV,c_os_asgvarenv)
#define C_OS_REQVARENV F2C_CONF_DECOR_FNC(C_OS_REQVARENV,c_os_reqvarenv)
#define C_OS_VERSION   F2C_CONF_DECOR_FNC(C_OS_VERSION,  c_os_version)
#define C_OS_HOSTNAME  F2C_CONF_DECOR_FNC(C_OS_HOSTNAME, c_os_hostname)
#define C_OS_PLATFORM  F2C_CONF_DECOR_FNC(C_OS_PLATFORM, c_os_platform)
#define C_OS_CWD       F2C_CONF_DECOR_FNC(C_OS_CWD,      c_os_cwd)
#define C_OS_DELFIC    F2C_CONF_DECOR_FNC(C_OS_DELFIC,   c_os_delfic)
#define C_OS_DIR       F2C_CONF_DECOR_FNC(C_OS_DIR,      c_os_dir)
#define C_OS_FICTMP    F2C_CONF_DECOR_FNC(C_OS_FICTMP,   c_os_fictmp)
#define C_OS_MKDIR     F2C_CONF_DECOR_FNC(C_OS_MKDIR,    c_os_mkdir)
#define C_OS_RMDIR     F2C_CONF_DECOR_FNC(C_OS_RMDIR,    c_os_rmdir)
#define C_OS_ISDIR     F2C_CONF_DECOR_FNC(C_OS_ISDIR,    c_os_isdir)
#define C_OS_ISFILE    F2C_CONF_DECOR_FNC(C_OS_ISFILE,   c_os_isfile)
#define C_OS_NORMPATH  F2C_CONF_DECOR_FNC(C_OS_NORMPATH, c_os_normpath)
#define C_OS_BASENAME  F2C_CONF_DECOR_FNC(C_OS_BASENAME, c_os_basename)
#define C_OS_DIRNAME   F2C_CONF_DECOR_FNC(C_OS_DIRNAME,  c_os_dirname)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ERRMSG    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ABORT     ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_RAISE     (fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REPEXE    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NOMEXE    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ASGVARENV (F2C_CONF_STRING, F2C_CONF_STRING F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_REQVARENV (F2C_CONF_STRING, F2C_CONF_STRING F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_VERSION   (F2C_CONF_STRING, F2C_CONF_STRING F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_HOSTNAME  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_PLATFORM  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_CWD       (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DELFIC    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIR       (F2C_CONF_STRING, F2C_CONF_STRING, F2C_CONF_STRING, fint_t* F2C_CONF_SUP_INT F2C_CONF_SUP_INT F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_FICTMP    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_MKDIR     (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_RMDIR     (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISDIR     (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_ISFILE    (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_NORMPATH  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_BASENAME  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_OS_DIRNAME   (F2C_CONF_STRING F2C_CONF_SUP_INT);

extern fint_t C_OS_SIGINT;
extern fint_t C_OS_SIGILL;
extern fint_t C_OS_SIGABRT;
extern fint_t C_OS_SIGFPE;
extern fint_t C_OS_SIGSEGV;
extern fint_t C_OS_SIGTERM;

#ifdef __cplusplus
}
#endif

#endif   // C_OS_H_DEJA_INCLU
