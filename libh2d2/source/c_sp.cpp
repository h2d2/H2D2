//************************************************************************
// --- Copyright (c) INRS 2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sousroutines:
//************************************************************************
#include "c_sp.h"

#include <assert.h>

#if defined(H2D2_WINDOWS)
#  if defined( __WINDOWS_H )
#    if !defined( STRICT )
#      error #include <windows.h> must be preceeded by #define STRICT
#    endif
#  else
#    if !defined( STRICT )
#      define STRICT
#    endif
#    include <windows.h>
#  endif
#elif defined(H2D2_UNIX)
#  include <pthread.h>
#else
#  error Invalid operating system
#endif

#if defined(H2D2_UNIX)
//http://stackoverflow.com/questions/1606400/how-to-sleep-or-pause-a-pthread-in-c-on-linux

struct pt_str
{
   pthread_t       t_id;
   pthread_mutex_t lock;
   pthread_cond_t  cond;
   int play;

   pt_str() :
//      lock(PTHREAD_MUTEX_INITIALIZER),
//      cond(PTHREAD_COND_INITIALIZER),
      play(0) {}
};

#endif

//**************************************************************
// Description:
//    Lance l'exécution d'une tâche.
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_START(hndl_t* handle,
                                                         hndl_t* fptr)
{
#ifdef MODE_DEBUG
   assert(fptr != NULL);
#endif  // ifdef MODE_DEBUG

   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   DWORD idProcessus;
   HANDLE h = NULL;
   h = CreateThread(0, 0, LPTHREAD_START_ROUTINE(fptr), 0, 0, &idProcessus);
   if (h == 0)
      ierr = -1; // "ERR_EXECUTION_TACHE_IMPOSSIBLE"
   else
      *handle = reinterpret_cast<hndl_t>(h);
#elif defined(H2D2_UNIX)
   typedef void* (F2C_CONF_CNV_APPEL *func_t) (void*);
   pt_str* psP = new pt_str();
   int ret = pthread_create(&psP->t_id, NULL, reinterpret_cast<func_t>(*fptr), NULL);
   if (ret != 0)
      ierr = -1;  // "ERR_EXECUTION_TACHE_IMPOSSIBLE"
   else
      *handle = reinterpret_cast<hndl_t>(psP);
#else
#  error Invalid operating system
#endif

   return ierr;
}

//**************************************************************
// Description:
//    Méthode statique appelée par la tâche elle-même lorsqu'elle
//    a terminé son exécution.
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Notes:
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_EXIT(fint_t* exitCode)
{

#if defined(H2D2_WINDOWS)
   DWORD e = static_cast<DWORD>(*exitCode);
   ExitThread(e);
#elif defined(H2D2_UNIX)
   void* e = reinterpret_cast<void*>(*exitCode);
   pthread_exit(e);
#else
#  error Invalid operating system
#endif

   return 0;
}

//**************************************************************
// Description:
//    Tue cette tâche de façon brutale. La tâche n'est jamais avertie
//    de quoi que ce soit.
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_KILL(hndl_t* handle)
{
   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   DWORD exitCode;
   HANDLE h = reinterpret_cast<HANDLE>(*handle);
   GetExitCodeThread(h, &exitCode);
   TerminateThread(h, exitCode);
#elif defined(H2D2_UNIX)
   pt_str* psP = reinterpret_cast<pt_str*>(*handle);
   ierr = pthread_cancel(psP->t_id);
#else
#  error Invalid operating system
#endif

   return ierr;
}

//**************************************************************
//
// Description:
//    Suspend temporairement l'exécution de cette tâche. L'exécution
//    est suspendue jusqu'à un appel à C_SP_RESUME().
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Codes d'erreur:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_SUSPEND(hndl_t* handle)
{
   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   HANDLE h = reinterpret_cast<HANDLE>(*handle);
   if ( SuspendThread(h))
   {
      ierr = -1; // "ERR_SUSPENSION_TACHE"
   }
#elif defined(H2D2_UNIX)
   pt_str* psP = reinterpret_cast<pt_str*>(*handle);
   pthread_mutex_lock(&psP->lock);
   psP->play = 0;
   pthread_mutex_unlock(&psP->lock);
#else
#  error Invalid operating system
#endif

   return ierr;
}

//**************************************************************
// Description:
//    Continue l'exécution de la tâche qui doit d'abord avoir été
//    suspendue avec un appel à C_SP_SUSPEND().
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Notes:
//
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_RESUME(hndl_t* handle)
{
   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   HANDLE h = reinterpret_cast<HANDLE>(*handle);
   if (! ResumeThread(h))
   {
//      msg = ERMsg(ERMsg::ERREUR, "ERR_CONTINUE_TACHE");
   }
#elif defined(H2D2_UNIX)
   pt_str* psP = reinterpret_cast<pt_str*>(*handle);
   pthread_mutex_lock  (&psP->lock);
   psP->play = 1;
   pthread_cond_signal (&psP->cond);
   pthread_mutex_unlock(&psP->lock);
#else
#  error Invalid operating system
#endif

   return ierr;
}

//**************************************************************
// Sommaire:
//
// Description:
//    La fonction C_SP_WAIT est appelée par la tâche. Elle se met
//    en attente tant que le tâche est suspendue et retourne dès
//    que l'exécution de la tâche peut continuer.
//
// Entrée:
//    hndl_t* handle       Pointeur sur la tâche
//
// Sortie:
//
// Notes:
//    Comment la tâche et le code qui la gère peuvent utiliser en
//    même temps le mutex?
//**************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_WAIT(hndl_t* handle)
{
   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   HANDLE h = reinterpret_cast<HANDLE>(*handle);
   if (! ResumeThread(h))
   {
//      msg = ERMsg(ERMsg::ERREUR, "ERR_CONTINUE_TACHE");
   }
#elif defined(H2D2_UNIX)
   pt_str* psP = reinterpret_cast<pt_str*>(*handle);
   pthread_mutex_lock(&psP->lock);
   while (!psP->play)
   {
       pthread_cond_wait(&psP->cond, &psP->lock);
   }
   pthread_mutex_unlock(&psP->lock);
#else
#  error Invalid operating system
#endif

   return ierr;
}

