C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  Integer BLAS
C Sommaire: Fonctions BLAS sur des INTEGER
C************************************************************************

C************************************************************************
C Sommaire: Copie un vecteur d'entiers
C
C Description:
C     La sous-routine ICOPY effectue l'opération KY = KX. Elle est
C     l'équivalent pour des entiers des routines BLAS xCOPY.
C
C Entrée:
C     N        Nombre d'éléments à copier
C     KX       Vecteur source
C     IX       Incrément dans le vecteur source
C     IY       Incrément dans le vecteur destination
C
C Sortie:
C     KY       Vecteur destination
C
C Notes:
C
C************************************************************************
      SUBROUTINE ICOPY(N,KX,IX,KY,IY)

      IMPLICIT NONE

      INTEGER N
      INTEGER KX(*)
      INTEGER IX
      INTEGER KY(*)
      INTEGER IY

      INTEGER I
      INTEGER JX
      INTEGER JY
C--------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         IF (IY .EQ. 1) THEN
            DO I=1,N
               KY(I) = KX(I)
            ENDDO
         ELSE
            JY = 1 - IY
            DO I=1,N
               JY = JY + IY
               KY(JY) = KX(I)
            ENDDO
         ENDIF
      ELSE
         IF (IY .EQ. 1) THEN
            JX = 1 - IX
            DO I=1,N
               JX = JX + IX
               KY(I) = KX(JX)
            ENDDO
        ELSE
            JX = 1 - IX
            JY = 1 - IY
            DO I=1,N
               JX = JX + IX
               JY = JY + IY
               KY(JY) = KX(JX)
            ENDDO
         ENDIF
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Initialise un vecteur d'entiers
C
C Description:
C     La sous-routine IINIT initialise le vecteur d'entiers KX
C     à la valeur IA.
C
C Entrée:
C     N        Nombre d'éléments à initialiser
C     IA       Valeur d'initialisation
C     IX       Incrément dans le vecteur
C
C Sortie:
C     KX       Vecteur à initialiser
C
C Notes:
C
C************************************************************************
      SUBROUTINE IINIT(N,IA,KX,IX)

      IMPLICIT NONE

      INTEGER N
      INTEGER IA
      INTEGER KX(*)
      INTEGER IX

      INTEGER I
      INTEGER JX
C--------------------------------------------------------------------------

      IF (IX .EQ. 1) THEN
         DO I=1,N
            KX(I) = IA
         ENDDO
      ELSE
         JX = 1 - IX
         DO I=1,N
            JX = JX + IX
            KX(JX) = IA
         ENDDO
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Integer GaTHeR.
C
C Description:
C     La sous-routine IGTHR fait l'opération X(I) = Y(IND(I)).
C
C Entrée:
C     N        Nombre d'éléments
C     KY       Table INTEGER en format compressé  (Y,INDX).
C
C Sortie:
C     KX       Table INTEGER pleine.
C
C Notes:
C
C************************************************************************
      SUBROUTINE IGTHR(N, KY, KX, INDX)

      IMPLICIT NONE

      INTEGER N
      INTEGER KY(*)
      INTEGER KX(N)
      INTEGER INDX(N)

      INTEGER I
C--------------------------------------------------------------------------

      IF (N .LE. 0)  RETURN
      DO I=1, N
         KX(I) = KY(INDX(I))
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: Integer SCaTeR.
C
C Description:
C     La sous-routine ISCTR fait l'opération Y(INDX(I)) = X(I).
C
C Entrée:
C     N        Nombre d'éléments
C     KX       Table INTEGER pleine.
C
C Sortie:
C     KY       Table INTEGER en format compressé  (Y,INDX).
C
C Notes:
C
C************************************************************************
      SUBROUTINE ISCTR(N, KX, INDX, KY)

      IMPLICIT NONE

      INTEGER N
      INTEGER KY(*)
      INTEGER KX(N)
      INTEGER INDX(N)

      INTEGER I
C--------------------------------------------------------------------------

      IF (N .LE. 0)  RETURN
      DO I=1, N
         KY(INDX(I)) = KX(I)
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire: Integer Absolute max.
C
C Description:
C     La sous-routine IIAMAX retourne l'indice de l'élément qui à la
C     valeur absolue max.
C
C Entrée:
C     N        Nombre d'éléments
C     KX       Table INTEGER pleine.
C     IX       Incrément dans le vecteur
C
C Sortie:
C
C Notes:
C
C************************************************************************
      INTEGER FUNCTION IIAMAX(N, KX, IX)

      IMPLICIT NONE

      INTEGER N
      INTEGER KX(*)
      INTEGER IX

      INTEGER IMAX
      INTEGER I, JX
C--------------------------------------------------------------------------

      IIAMAX = 0
      IF (N .LT. 1 .OR. IX .LE. 0) RETURN
      IIAMAX = 1
      IF (N .EQ. 1) RETURN

      IF (IX .EQ. 1) THEN
         IMAX = ABS(KX(1))
         DO I=2,N
            IF (ABS(KX(I)) .GT. IMAX) THEN
               IIAMAX = I
               IMAX = ABS(KX(I))
            ENDIF
         ENDDO
      ELSE
         JX = 1
         IMAX = ABS(KX(1))
         JX = JX + IX
         DO I=2,N
            IF (ABS(KX(JX)) .GT. IMAX) THEN
               IIAMAX = I
               IMAX = ABS(KX(JX))
            ENDIF
            JX = JX + IX
         ENDDO
      ENDIF

      RETURN
      END
