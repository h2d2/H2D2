C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  STRiNg
C Sommaire: Fonctions de traitement de chaînes
C
C Scope: SP_STRN
C     LOGICAL SP_STRN_AZS
C     INTEGER SP_STRN_BLK
C     SUBROUTINE SP_STRN_CLP
C     SUBROUTINE SP_STRN_DLC
C     SUBROUTINE SP_STRN_DMC
C     LOGICAL SP_STRN_DBL
C     LOGICAL SP_STRN_INT
C     SUBROUTINE SP_STRN_LCS
C     INTEGER SP_STRN_LEN
C     INTEGER SP_STRN_LFT
C     INTEGER SP_STRN_RHT
C     SUBROUTINE SP_STRN_SBC
C     INTEGER SP_STRN_TKI
C     INTEGER SP_STRN_TKR
C     INTEGER SP_STRN_TKS
C     INTEGER SP_STRN_TOK
C     INTEGER SP_STRN_NTOK
C     SUBROUTINE SP_STRN_TRM
C     SUBROUTINE SP_STRN_UCS
C     LOGICAL SP_STRN_VAR
C     SUBROUTINE SP_STRN_TCM
C
C************************************************************************

C************************************************************************
C Sommaire: SP_STRN_AZS
C
C Description:
C     La fonction SP_STRN_AZS retourne .TRUE. si la chaîne STRING
C     a tous ses caractères compris entre 'a' et 'z', 'A' et 'Z'
C     ou est '_'.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_AZS(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER   I, LSTR
      CHARACTER C
      LOGICAL ESTAZS
C-----------------------------------------------------------------------

      ESTAZS = .FALSE.

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ESTAZS = .TRUE.
      DO I=1, LSTR
         C = STRING(I:I)
         IF ((C .LT. 'a' .OR. C .GT. 'z') .AND.
     &       (C .LT. 'A' .OR. C .GT. 'Z') .AND.
     &       (C .NE. '_')) ESTAZS = .FALSE.
      ENDDO

9999  CONTINUE
      SP_STRN_AZS = ESTAZS
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_BLK
C
C Description:
C     La fonction SP_STRN_BLK réduis la chaîne STRING au bloc
C     compris entre les tokens LTOK et RTOK.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) LTOK
C     CHARACTER*(*) RTOK
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_BLK(STRING, LTOK, RTOK)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) LTOK
      CHARACTER*(*) RTOK

      INCLUDE 'spstrn.fi'

      INTEGER LSTR
      INTEGER I1, ILFT, IRHT
C-----------------------------------------------------------------------

      ILFT = 0
      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      IRHT = INDEX(STRING(1:LSTR), RTOK)

      IF (IRHT .GT. 1 .AND. IRHT .LE. LSTR) THEN
         ILFT = 0
100      CONTINUE
            I1 = INDEX(STRING(ILFT+1:IRHT), LTOK)
            IF (I1 .LT. 1 .OR. I1 .GE. IRHT) GOTO 199
            ILFT = ILFT + I1
         GOTO 100
199      CONTINUE
         IF (ILFT .GE. 1 .AND. ILFT .LT. IRHT) THEN
            STRING = STRING(ILFT+1:IRHT-1)
         ELSE
            ILFT = 0
            STRING = ' '
         ENDIF
      ELSE
         ILFT = 0
         STRING = ' '
      ENDIF

9999  CONTINUE
      SP_STRN_BLK = ILFT
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_CLP
C
C Description:
C     La sous-routine SP_STRN_CLP réduis la chaîne STRING à une
C     chaîne de longueur LEN.
C
C Entrée:
C     CHARACTER*(*) STRING
C     INTEGER       LEN
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_CLP(STRING, LEN)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      INTEGER       LEN

      INCLUDE 'spstrn.fi'

      INTEGER LSTR, ILFT, IRHT
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .GT. LEN) THEN
         ILFT = LEN / 3
         IRHT = LSTR - (LEN-ILFT-3) + 1
         STRING = STRING(1:ILFT) //
     &            '...' //
     &            STRING(IRHT:LSTR)
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_DLC
C
C Description:
C     La sous-routine SP_STRN_DLC efface de la chaîne STRING tous les
C     caractères C.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER     C
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_DLC(STRING, C)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER     C

      INCLUDE 'spstrn.fi'

      INTEGER I1, I2, LSTR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ITRUP = .TRUE.
      I1 = 1
      DO I2=1,LSTR
         IF (STRING(I2:I2) .EQ. '''') ITRUP = .NOT. ITRUP
         IF (.NOT. ITRUP .OR. STRING(I2:I2) .NE. C) THEN
            IF (I1 .NE. I2) STRING(I1:I1) = STRING(I2:I2)
            I1 = I1 + 1
         ENDIF
      ENDDO
      IF (I1 .GT. 1) THEN
         STRING = STRING(1:I1-1)
      ELSE
         STRING = ' '
      ENDIF

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_DMC
C
C Description:
C     La sous-routine SP_STRN_DMC efface de la chaîne STRING toutes les
C     multiples occurrences du caractères C.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER     C
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_DMC(STRING, C)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER     C

      INCLUDE 'spstrn.fi'

      INTEGER   I1, I2, LSTR
      CHARACTER CACTU, CPREC
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ITRUP = .TRUE.
      I1 = 1
      CPREC = C
      DO I2=1,LSTR
         CACTU = STRING(I2:I2)
         IF (CACTU .EQ. '''') ITRUP = .NOT. ITRUP
         IF (.NOT. ITRUP .OR.
     &       CACTU .NE. C .OR.
     &       CACTU .EQ. C .AND. CACTU .NE. CPREC) THEN
            STRING(I1:I1) = CACTU
            I1 = I1 + 1
         ENDIF
         CPREC = CACTU
      ENDDO
      IF (I1 .GT. 1) THEN
         STRING = STRING(1:I1-1)
      ELSE
         STRING = ' '
      ENDIF

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_DBL
C
C Description:
C     La fonction SP_STRN_DBL retourne .TRUE. si la chaîne STRING est un
C     REAL.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_DBL(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER   I, LSTR
      INTEGER   IDEB, IDOT, IEXP
      CHARACTER C
      LOGICAL   ESTDBL
C-----------------------------------------------------------------------

      ESTDBL = .FALSE.
      IDOT = 0
      IEXP = 0

C---     Longueur de la chaîne
      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999
      ESTDBL = .TRUE.

C---     Indice du point et de l'exposant
      IF (IEXP .LE. 0) IEXP = INDEX(STRING, 'D')
      IF (IEXP .LE. 0) IEXP = INDEX(STRING, 'd')
      IF (IEXP .LE. 0) IEXP = INDEX(STRING, 'E')
      IF (IEXP .LE. 0) IEXP = INDEX(STRING, 'e')
      IF (IDOT .LE. 0) IDOT = INDEX(STRING, '.')
      IF (IDOT .LE. 0) IDOT = IEXP
      IF (IDOT .LE. 0) IDOT = LSTR

C---     Saute le signe initial
      IDEB = 1
      C = STRING(IDEB:IDEB)
      IF (C .EQ. '+' .OR. C .EQ. '-') IDEB = IDEB + 1

C---     Partie avant le point
      IF (IDOT .GT. IDEB) THEN
         ESTDBL = ESTDBL .AND. SP_STRN_UINT(STRING(IDEB:IDOT-1))
      ENDIF

C---     Partie entre le point et l'exposant
      IF ((IEXP-1) .GE. (IDOT+1)) THEN
         ESTDBL = ESTDBL .AND. SP_STRN_UINT(STRING(IDOT+1:IEXP-1))
      ENDIF

C---     Saute le signe de l'exposant
      IF (IEXP .GT. 0) THEN
         IEXP = IEXP + 1
         C = STRING(IEXP:IEXP)
         IF (C .EQ. '+' .OR. C .EQ. '-') IEXP = IEXP + 1
      ENDIF

C---     L'exposant
      IF (IEXP .GT. IDEB .AND. IEXP .LE. LSTR) THEN
         ESTDBL = ESTDBL .AND. SP_STRN_UINT(STRING(IEXP:LSTR))
      ENDIF

9999  CONTINUE
      SP_STRN_DBL = ESTDBL
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_INT
C
C Description:
C     La fonction SP_STRN_INT retourne .TRUE. si la chaîne STRING est un
C     INTEGER donc si tous ses caractères sont compris entre '0' et '9',
C     possiblement précédé d'un signe.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_INT(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER   I, IDEB, LSTR
      CHARACTER C
      LOGICAL ESTINT
C-----------------------------------------------------------------------

      ESTINT = .FALSE.

C---     Longueur de la chaîne
      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

C---     Saute le signe initial
      IDEB = 1
      C = STRING(IDEB:IDEB)
      IF (C .EQ. '+' .OR. C .EQ. '-') IDEB = IDEB + 1
      IF (LSTR-IDEB+1 .LE. 0) GOTO 9999

C---     Le nombre
      ESTINT = SP_STRN_UINT(STRING(IDEB:LSTR))

9999  CONTINUE
      SP_STRN_INT = ESTINT
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_UINT
C
C Description:
C     La fonction SP_STRN_UINT retourne .TRUE. si la chaîne STRING est un
C     UNISGNED INTEGER donc si tous ses caractères sont compris entre
C     '0' et '9'.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_UINT(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER   I, LSTR
      CHARACTER C
      LOGICAL ESTINT
C-----------------------------------------------------------------------

      ESTINT = .FALSE.

C---     Longueur de la chaîne
      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

C---     Le nombre
      ESTINT = .TRUE.
      DO I=1, LSTR
         C = STRING(I:I)
         IF (C .LT. '0' .OR. C .GT. '9') ESTINT = .FALSE.
      ENDDO

9999  CONTINUE
      SP_STRN_UINT = ESTINT
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_LCS
C
C Description:
C     La sous-routine SP_STRN_LCS transforme en minuscule la chaîne
C     STRING.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_LCS(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER I, LSTR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ITRUP = .TRUE.
      DO I=1,LSTR
         IF (STRING(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STRING(I:I) .GE. 'A') .AND. (STRING(I:I) .LE. 'Z')) THEN
            IF (ITRUP) STRING(I:I) = CHAR(ICHAR(STRING(I:I))+32)
         ENDIF
      ENDDO

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_LEN
C
C Description:
C     La fonction SP_STRN_LEN retourne la longueur de la chaîne.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      FUNCTION SP_STRN_LEN(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

!      INTEGER I
!C-----------------------------------------------------------------------
!
!      DO I = LEN(STRING),1,-1
!         IF (STRING(I:I) .NE. ' ') GOTO 10
!      ENDDO
!10    SP_STRN_LEN = I

!      I = LEN(STRING) + 1
!20    CONTINUE
!         I = I - 1
!      IF (I .GT. 0) THEN
!         IF (STRING(I:I) .EQ. ' ') GOTO 20
!      ENDIF
!
!      SP_STRN_LEN = I
      SP_STRN_LEN = LEN_TRIM(STRING)
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_LFT
C
C Description:
C     La fonction SP_STRN_LFT garde la partie de la chaîne
C     STRING à gauche du token TOK. Si le token n'est pas trouvé,
C     la chaîne est retournée inchangée. Elle retourne la position du
C     début du token.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) TOK
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_LFT(STRING, TOK)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) TOK

      INCLUDE 'spstrn.fi'

      INTEGER LSTR
      INTEGER IPOS
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IPOS = INDEX(STRING(1:LSTR),TOK)

      IF (IPOS .LT. 1 .OR. IPOS .GT. LSTR) THEN
         IPOS = 0
      ELSE
         STRING = STRING(1:IPOS-1)
      ENDIF

      SP_STRN_LFT = IPOS
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_RHT
C
C Description:
C     La fonction SP_STRN_RHT garde la partie de la chaîne
C     STRING à droite du token TOK. Si le token n'est pas trouvé,
C     la chaîne retournée est vide. Elle retourne la position du
C     début de la chaîne.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) TOK
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_RHT(STRING, TOK)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) TOK

      INCLUDE 'spstrn.fi'

      INTEGER LSTR
      INTEGER IPOS
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IPOS = INDEX(STRING(1:LSTR),TOK)

      IF (IPOS .LT. 1 .OR. IPOS .GT. LSTR) THEN
         IPOS = 0
         STRING = ' '
      ELSE
         IPOS = IPOS + SP_STRN_LEN(TOK)
         STRING = STRING(IPOS:LSTR)
      ENDIF

      SP_STRN_RHT = IPOS
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_SBC
C
C Description:
C     La sous-routine SP_STRN_SBC substitue dans la chaîne STRING tous les
C     caractères S par des caractère D.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER     S
C     CHARACTER     D
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_SBC(STRING, S, D)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER     S
      CHARACTER     D

      INCLUDE 'spstrn.fi'

      INTEGER I, LSTR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ITRUP = .TRUE.
      DO I=1,LSTR
         IF (STRING(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF (ITRUP .AND. STRING(I:I) .EQ. S) THEN
            STRING(I:I) = D
         ENDIF
      ENDDO

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TKI
C
C Description:
C     La fonction SP_STRN_TKI retourne le n.ieme token de la chaîne
C     STRING comme INTEGER IVAL. Les tokens sont séparés par la chaîne
C     STOK.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) STOK
C     INTEGER       NTOK
C
C Sortie:
C     INTEGER       IVAL
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_TKI(STRING, STOK, NTOK, IVAL)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) STOK
      INTEGER NTOK
      INTEGER IVAL

      INCLUDE 'spstrn.fi'

      INTEGER ISTR
      INTEGER IEND
      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = 0

      ISTR = SP_STRN_TOK(STRING, STOK, NTOK)
      IF (ISTR .LE. 0) GOTO 9900
      IEND = SP_STRN_TOK(STRING, STOK, NTOK+1)
      IF (IEND .LE. 0) THEN
         IEND = SP_STRN_LEN(STRING)
      ELSE
         IEND = IEND - SP_STRN_LEN(STOK) - 1
      ENDIF
      IF (ISTR .GT. IEND) GOTO 9900

      IEND = ISTR + SP_STRN_LEN(STRING(ISTR:IEND)) - 1
      ISTR = ISTR-1
10    CONTINUE
         ISTR = ISTR + 1
      IF ((ISTR .LT. IEND) .AND. (STRING(ISTR:ISTR) .EQ. ' ')) GOTO 10

      IF (SP_STRN_INT(STRING(ISTR:IEND))) THEN
         READ(STRING(ISTR:IEND), *, ERR=9901) IVAL
      ELSE
         GOTO 9901
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = -1   ! ERR_TOKEN_INVALIDE
      GOTO 9999
9901  IERR = -2   ! ERR_LECTURE_TOKEN
      GOTO 9999

9999  CONTINUE
      SP_STRN_TKI = IERR
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TKR
C
C Description:
C     La fonction SP_STRN_TKR retourne le n.ieme token de la chaîne
C     STRING comme REAL*8 RVAL. Les tokens sont séparés par la chaîne
C     STOK.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) STOK
C     INTEGER       NTOK
C
C Sortie:
C     REAL*8        RVAL
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_TKR(STRING, STOK, NTOK, RVAL)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) STOK
      INTEGER NTOK
      REAL*8  RVAL

      INCLUDE 'spstrn.fi'

      INTEGER ISTR
      INTEGER IEND
      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = 0

      ISTR = SP_STRN_TOK(STRING, STOK, NTOK)
      IF (ISTR .LE. 0) GOTO 9900
      IEND = SP_STRN_TOK(STRING, STOK, NTOK+1)
      IF (IEND .LE. 0) THEN
         IEND = SP_STRN_LEN(STRING)
      ELSE
         IEND = IEND - SP_STRN_LEN(STOK) - 1
      ENDIF
      IF (ISTR .GT. IEND) GOTO 9900

      IEND = ISTR + SP_STRN_LEN(STRING(ISTR:IEND)) - 1
      ISTR = ISTR-1
10    CONTINUE
         ISTR = ISTR + 1
      IF ((ISTR .LT. IEND) .AND. (STRING(ISTR:ISTR) .EQ. ' ')) GOTO 10

      IF (SP_STRN_DBL(STRING(ISTR:IEND))) THEN
         READ(STRING(ISTR:IEND), *, ERR=9901) RVAL
      ELSE
         GOTO 9901
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = -1   ! ERR_TOKEN_INVALIDE
      GOTO 9999
9901  IERR = -2   ! ERR_LECTURE_TOKEN
      GOTO 9999

9999  CONTINUE
      SP_STRN_TKR = IERR
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TKS
C
C Description:
C     La fonction SP_STRN_TKS retourne le n.ieme token de la chaîne
C     STRING comme chaîne SVAL. Les tokens sont séparés par la chaîne
C     STOK.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) STOK
C     INTEGER       NTOK
C
C Sortie:
C     CHARACTER*(*) SVAL
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_TKS(STRING, STOK, NTOK, SVAL)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) STOK
      INTEGER NTOK
      CHARACTER*(*) SVAL

      INCLUDE 'spstrn.fi'

      INTEGER ISTR
      INTEGER IEND
      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = 0

      ISTR = SP_STRN_TOK(STRING, STOK, NTOK)
      IF (ISTR .LE. 0) GOTO 9900
      IEND = SP_STRN_TOK(STRING, STOK, NTOK+1)
      IF (IEND .LE. 0) THEN
         IEND = SP_STRN_LEN(STRING)
      ELSE
         IEND = IEND - SP_STRN_LEN(STOK) - 1
      ENDIF

      IF (IEND .GE. ISTR) THEN
         SVAL = STRING(ISTR:IEND)
         CALL SP_STRN_TRM(SVAL)
      ELSE
         SVAL = ' '
      ENDIF
      
      GOTO 9999
C-----------------------------------------------------------------------
9900  IERR = -1   ! ERR_TOKEN_INVALIDE
      GOTO 9999

9999  CONTINUE
      SP_STRN_TKS = IERR
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TOK
C
C Description:
C     La fonction SP_STRN_TOK retourne l'indice du début du n.ieme
C     token de la chaîne STRING. Les tokens sont séparés par la chaîne
C     STOK. La recherche se fait dans STRING sans prendre en compte les
C     espaces sur la droite. Pour STOK, on le considère de LEN(STOK) afin de
C     conserver les espaces.
C     La fonction retourne -1 si le token n'est pas trouvé.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) STOK
C     INTEGER       NTOK
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_TOK(STRING, STOK, NTOK)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) STOK
      INTEGER NTOK

      INCLUDE 'spstrn.fi'

      INTEGER   IPOS
      INTEGER   ISTR
      INTEGER   IEND
      INTEGER   ITOK, LTOK
      LOGICAL   ISQT, IDQT
      CHARACTER C
C-----------------------------------------------------------------------

      IPOS = -1
      IEND = SP_STRN_LEN(STRING)
      IF (IEND .LE. 0) GOTO 9999
      LTOK = LEN(STOK)
      IEND = IEND - LTOK + 1

      ISTR = 1 - LTOK
      ITOK = 1
      IF (ITOK .EQ. NTOK) GOTO 10

      ISQT = .FALSE.
      IDQT = .FALSE.
      DO ISTR=1,IEND
         C = STRING(ISTR:ISTR)
         IF (C .EQ. '''') THEN
            IF (.NOT. IDQT) ISQT = .NOT. ISQT
         ELSEIF (C .EQ. '"') THEN
            IF (.NOT. ISQT) IDQT = .NOT. IDQT
         ELSEIF (.NOT. ISQT .AND. .NOT. IDQT) THEN
            IF (STOK(1:LTOK) .EQ. STRING(ISTR:ISTR+LTOK-1)) THEN
               ITOK = ITOK + 1
               IF (ITOK .EQ. NTOK) GOTO 10
            ENDIF
         ENDIF
      ENDDO
      GOTO 9999

10    CONTINUE
      IPOS = ISTR+LTOK

9999  CONTINUE
      SP_STRN_TOK = IPOS
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_NTOK
C
C Description:
C     La fonction SP_STRN_NTOK retourne le nombre de token de la chaîne
C     STRING. Les tokens sont séparés par la chaîne STOK. La recherche se
C     fait dans STRING sans prendre en compte les espaces sur la droite.
C     Pour STOK, on le considère de LEN(STOK) afin de conserver les espaces.
C
C Entrée:
C     CHARACTER*(*) STRING
C     CHARACTER*(*) STOK
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_NTOK(STRING, STOK)

      IMPLICIT NONE
      CHARACTER*(*) STRING
      CHARACTER*(*) STOK

      INCLUDE 'spstrn.fi'

      INTEGER   ISTR
      INTEGER   IEND
      INTEGER   ITOK, LTOK, LSTR
      LOGICAL   ISQT, IDQT
      CHARACTER C
C-----------------------------------------------------------------------

      LTOK = LEN(STOK)
      LSTR = SP_STRN_LEN(STRING)
      IEND = LSTR - LTOK + 1

      ITOK = 0
      IF (LSTR .GT. 0) ITOK = 1

      ISQT  = .FALSE.
      IDQT  = .FALSE.
      DO ISTR=1,IEND
         C = STRING(ISTR:ISTR)
         IF (C .EQ. '''') THEN
            IF (.NOT. IDQT) ISQT = .NOT. ISQT
         ELSEIF (C .EQ. '"') THEN
            IF (.NOT. ISQT) IDQT = .NOT. IDQT
         ELSEIF (.NOT. ISQT .AND. .NOT. IDQT) THEN
            IF (STOK(1:LTOK) .EQ. STRING(ISTR:ISTR+LTOK-1)) THEN
               ITOK = ITOK + 1
            ENDIF
         ENDIF
      ENDDO

      SP_STRN_NTOK = ITOK
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TRM
C
C Description:
C     La sous-routine SP_STRN_TRM enlève les espaces au début et en
C     fin de la chaîne STRING. En fait, elle supprime tous les
C     caractères comparant inférieur ou égal à un espace.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C     CHARACTER*(*) STRING       La chaîne modifiée
C
C Notes:
C     Le FORTRAN ne garantit pas que dans une expression .AND., si le
C     premier terme est évalué à .FALSE., qu'alors le reste de l'expression
C     n'est pas évaluée. Il faut donc écrire une série de if imbriqués.
C************************************************************************
      SUBROUTINE SP_STRN_TRM(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER I, L
C-----------------------------------------------------------------------

      L = SP_STRN_LEN(STRING)
      IF (L .LE. 0) GOTO 9999

      L = L + 1
10    CONTINUE
         L = L - 1
      IF (L .GT. 0) THEN
         IF (STRING(L:L) .LE. ' ') GOTO 10
      ENDIF

      I = 0
20    CONTINUE
         I = I + 1
      IF ((I .LT. L) .AND. (STRING(I:I) .LE. ' ')) GOTO 20

      STRING = STRING(I:L)

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_UCS
C
C Description:
C     La sous-routine SP_STRN_UCS transforme la chaîne en majuscule.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_UCS(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER I, LSTR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ITRUP = .TRUE.
      DO I=1,LSTR
         IF (STRING(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STRING(I:I) .GE. 'a') .AND. (STRING(I:I) .LE. 'z')) THEN
            IF (ITRUP) STRING(I:I) = CHAR(ICHAR(STRING(I:I))-32)
         ENDIF
      ENDDO

9999  CONTINUE
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_VAR
C
C Description:
C     La fonction SP_STRN_VAR retourne .TRUE. si la chaîne STRING
C     est un nom de variable valide, donc commence par un caractère
C     entre 'a' et 'z', 'A' et 'Z' ou est '_', puis est formé de
C     caractères entre 'a' et 'z', 'A' et 'Z', '0' et '9' ou
C     est '_'.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION SP_STRN_VAR(STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER   I, LSTR
      CHARACTER C
      LOGICAL ESTVAR
C-----------------------------------------------------------------------

      ESTVAR = .FALSE.

      LSTR = SP_STRN_LEN(STRING)
      IF (LSTR .LE. 0) GOTO 9999

      ESTVAR = .TRUE.
      C = STRING(1:1)
      IF ((C .LT. 'a' .OR. C .GT. 'z') .AND.
     &    (C .LT. 'A' .OR. C .GT. 'Z') .AND.
     &    (C .NE. '_')) ESTVAR = .FALSE.
      DO I=2, LSTR
         C = STRING(I:I)
         IF ((C .LT. 'a' .OR. C .GT. 'z') .AND.
     &       (C .LT. 'A' .OR. C .GT. 'Z') .AND.
     &       (C .LT. '0' .OR. C .GT. '9') .AND.
     &       (C .NE. '_')) ESTVAR = .FALSE.
      ENDDO

9999  CONTINUE
      SP_STRN_VAR = ESTVAR
      RETURN
      END

C************************************************************************
C Sommaire: SP_STRN_TCM
C
C Description:
C     La sous-routine SP_STRN_TCM élimine de la chaîne les commentaires
C     marqués par un '#'.
C
C Entrée:
C     CHARACTER*(*) STRING
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE SP_STRN_TCM (STRING)

      IMPLICIT NONE
      CHARACTER*(*) STRING

      INCLUDE 'spstrn.fi'

      INTEGER ILLIN
      INTEGER I
      LOGICAL LGMLT
C-----------------------------------------------------------------------

      ILLIN = SP_STRN_LEN(STRING)

      LGMLT = .FALSE.
      DO I=1,ILLIN
         IF (STRING(I:I) .EQ. '''') LGMLT = .NOT. LGMLT
         IF ((.NOT. LGMLT) .AND. (STRING(I:I) .EQ. '#')) GOTO 19
      ENDDO
19    CONTINUE
      STRING = STRING(1:I-1)

      RETURN
      END
