//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Configuration pour les différents compilateurs
//
// Description:
//    Le fichier cconfig.h regroupe les configurations des différents
//    compilateurs et plateformes qui permettent d'appeler des fonctions C
//    à partir du FORTRAN
//
//    La configuration comprend des macros globales:
//       H2D2_WINDOWS                     : Défini sur plateforme Windows
//       H2D2_UNIX                        : Défini sur plateforme Unix
//       H2D2_INTSIZE                     : Taille d'un int: Integer
//       H2D2_ARCH_TYPE                   : Type de la plateforme: Integer
//       H2D2_ARCH_NAME                   : Nom de la plateforme: String
//       H2D2_BUILD_TYPE                  : Type de build: Integer
//       H2D2_CMPLR_NAME                  : Nom du compilateur: String
//       H2D2_CMPLR_VFMT                  : Format d'écriture iostream du format
//       H2D2_CMPLR_VERS                  : Version du compilateur: String
//       H2D2_CMPLR_INTEL                 : Défini si compilateur Intel
//       H2D2_CMPLR_SUN                   : Défini si compilateur Sun
//       H2D2_CMPLR_GCC                   : Défini si compilateur GCC
//       H2D2_CMPLR_MSVC                  : Défini si compilateur Microsoft
//       H2D2_CMPLR_WATCOM                : Défini si compilateur Intel
//    et les macros qui gèrent la compatibilité Fortran/C
//       F2C_CONF_NOM_FONCTION_MAJ        : Défini pour des noms en MAJ
//       F2C_CONF_NOM_FONCTION_MIN_       : Défini pour des noms en min avec 1 _
//       F2C_CONF_NOM_FONCTION_MIN__      : Défini pour des noms en min avec double _
//       F2C_CONF_DECOR_FNC               : Macro pour décorer une fonction
//       F2C_CONF_DECOR_MDL               : Macro pour décorer une fonction de module
//       F2C_CONF_STRING                  : Type pour les chaînes
//       F2C_CONF_SUP_INT                 : Type pour le taille des chaînes
//       F2C_CONF_A_SUP_INT               : Défini pour le param sup
//       F2C_CONF_DLL_IMPORT              : Mangling pour l'import
//       F2C_CONF_DLL_EXPORT              : Mangling pour l'export
//       F2C_CONF_CNV_APPEL               : Convention d'appel du Fortran
//
// Notes:
//
//************************************************************************
#ifndef CCONFIG_H_DEJA_INCLU
#define CCONFIG_H_DEJA_INCLU

#undef H2D2_WINDOWS
#undef H2D2_UNIX
#undef H2D2_INT_SIZE
#undef H2D2_ARCH_NAME
#undef H2D2_BUILD_TYPE
#undef H2D2_CMPLR_NAME
#undef H2D2_CMPLR_INTEL
#undef H2D2_CMPLR_SUN
#undef H2D2_CMPLR_GCC
#undef H2D2_CMPLR_MSVC
#undef H2D2_CMPLR_WATCOM

#undef F2C_CONF_NOM_FONCTION_MAJ
#undef F2C_CONF_NOM_FONCTION_MIN_
#undef F2C_CONF_NOM_FONCTION_MIN__
#undef F2C_CONF_DECOR_FNC
#undef F2C_CONF_DECOR_MDL
#undef F2C_CONF_STRING
#undef F2C_CONF_SUP_INT
#undef F2C_CONF_A_SUP_INT
#undef F2C_CONF_DLL_IMPORT
#undef F2C_CONF_DLL_EXPORT
#undef F2C_CONF_CNV_APPEL

#define H2D2_ARCH_UNSET     0
#define H2D2_ARCH_WIN_IA32 10
#define H2D2_ARCH_WIN_X64  11
#define H2D2_ARCH_WIN_IA64 12
#define H2D2_ARCH_LNX_IA32 21
#define H2D2_ARCH_LNX_X64  22
#define H2D2_ARCH_LNX_IA64 23

#define H2D2_BUILD_UNSET    0
#define H2D2_BUILD_CMC     10

// ---  Section des build
#define H2D2_BUILD_TYPE H2D2_BUILD_UNSET
#if defined(INRS_BUILD_CMC)
#  define H2D2_BUILD_TYPE H2D2_BUILD_CMC
#endif

// ---  Section des plateformes
#undef CCConfig_Plateforme_valide
#if defined(_M_X64)
#  define CCConfig_Plateforme_valide 1
#  if !defined(H2D2_WINDOWS)
#     define H2D2_WINDOWS 1
#     define H2D2_INTSIZE 32
#     define H2D2_ARCH_TYPE H2D2_ARCH_WIN_X64
#     define H2D2_ARCH_NAME "WIN_X64"
#  endif
#elif defined(_WIN64) || defined(WIN64) || defined(__WIN64__)
#  define CCConfig_Plateforme_valide 1
#  if !defined(H2D2_WINDOWS)
#     define H2D2_WINDOWS 1
#     define H2D2_INTSIZE 64
#     define H2D2_ARCH_TYPE H2D2_ARCH_WIN_IA64
#     define H2D2_ARCH_NAME "WIN_IA64"
#  endif
#elif defined(_WIN32) || defined(WIN32) || defined(__WIN32__)
#  define CCConfig_Plateforme_valide 1
#  if !defined(H2D2_WINDOWS)
#     define H2D2_WINDOWS 1
#     define H2D2_INTSIZE 32
#     define H2D2_ARCH_TYPE H2D2_ARCH_WIN_IA32
#     define H2D2_ARCH_NAME "WIN_IA32"
#  endif
#elif defined(unix) || defined(__unix__) || defined(__unix) || defined(linux) || defined(__linux__) || defined(__linux)
#  define CCConfig_Plateforme_valide 1
#  if defined(__i386__)
#     define H2D2_INTSIZE 32
#     define H2D2_ARCH_NAME "LNX_IA32"
#     define H2D2_ARCH_TYPE H2D2_ARCH_LNX_IA32
#  elif defined(__ia64__)
#     define H2D2_INTSIZE 64
#     define H2D2_ARCH_NAME "LNX_IA64"
#     define H2D2_ARCH_TYPE H2D2_ARCH_LNX_IA64
#  elif defined(__LP64__)
#     define H2D2_INTSIZE 32
#     define H2D2_ARCH_NAME "LNX_X64"
#     define H2D2_ARCH_TYPE H2D2_ARCH_LNX_X64
#  else
#     error Invalid integer size
#  endif
#  if !defined(H2D2_UNIX)
#     define H2D2_UNIX 1
#  endif
#endif
#ifndef CCConfig_Plateforme_valide
#  error Unsupported platform
#endif

// ---  Section des compilateurs
#if defined (__INTEL_COMPILER)
#  define H2D2_CMPLR_INTEL 1
#  define H2D2_CMPLR_NAME "Intel"
#  define H2D2_CMPLR_VFMT std::dec
#  define H2D2_CMPLR_VERS __INTEL_COMPILER
#  if defined(H2D2_WINDOWS)
#     define F2C_CONF_DECOR_FNC(F, f) F
#     define F2C_CONF_DECOR_MDL(M, m, F, f) M ## _mp_ ## F
#     define F2C_CONF_NOM_FONCTION_MAJ
#     define F2C_CONF_STRING   char*
#     define F2C_CONF_SUP_INT  , unsigned int
#     define F2C_CONF_A_SUP_INT
#  else
#     define F2C_CONF_DECOR_FNC(F, f) f ## _
#     define F2C_CONF_DECOR_MDL(M, m, F, f) m ## _mp_ ## f ## _
#     define F2C_CONF_NOM_FONCTION_MIN_
#     define F2C_CONF_STRING   char*
#     define F2C_CONF_SUP_INT  , int
#     define F2C_CONF_A_SUP_INT
#  endif

#  define F2C_CONF_CNV_APPEL
#  if !defined(H2D2_WINDOWS) || !defined(MODE_DYNAMIC)
#     define F2C_CONF_DLL_IMPORT
#     define F2C_CONF_DLL_EXPORT
#  else
#     define F2C_CONF_DLL_IMPORT __declspec(dllimport)
#     define F2C_CONF_DLL_EXPORT __declspec(dllexport)
#  endif

#elif defined (__OPEN64__)
#  define H2D2_CMPLR_OPEN64  1
#  define H2D2_CMPLR_NAME "Open64"
#  define H2D2_CMPLR_VFMT std::dec
#  define H2D2_CMPLR_VERS (__OPENCC__ * 100 +  __OPENCC_MINOR__)
#  define F2C_CONF_DECOR_FNC(F, f) f ## _
#  define F2C_CONF_DECOR_MDL(M, m, F, f) __ ## _MOD_ ## f ## _
#  define F2C_CONF_NOM_FONCTION_MIN_
#  define F2C_CONF_STRING   char*
#  define F2C_CONF_SUP_INT  , int
#  define F2C_CONF_A_SUP_INT

#  define F2C_CONF_CNV_APPEL
#  if !defined(H2D2_WINDOWS) || !defined(MODE_DYNAMIC)
#     define F2C_CONF_DLL_IMPORT
#     define F2C_CONF_DLL_EXPORT
#  else
#     define F2C_CONF_DLL_IMPORT __declspec(dllimport)
#     define F2C_CONF_DLL_EXPORT __declspec(dllexport)
#  endif

#elif defined (__SUNPRO_C) || defined (__SUNPRO_CC)
#  define H2D2_CMPLR_SUN  1
#  define H2D2_CMPLR_NAME "Sun"
#  define H2D2_CMPLR_VFMT std::hex
#  define H2D2_CMPLR_VERS __SUNPRO_CC
#  define F2C_CONF_DECOR_FNC(F, f) f ## _
/*
#  define F2C_CONF_DECOR_MDL(M, m, F, f) m ## . ## f ## _
   Le point '.' dans la décoration est interprété comme un opérateur d'accès
   et génère une erreur de syntaxe invalide.
   Patch: Comme les symboles de modules ne sont pas chargés dynamiquement,
   on les désactive simplement.
*/
#  undef  F2C_CONF_DECOR_MDL
#  define F2C_CONF_NOM_FONCTION_MIN_
#  define F2C_CONF_STRING   char*
#  define F2C_CONF_SUP_INT  , int
#  define F2C_CONF_A_SUP_INT

#  define F2C_CONF_CNV_APPEL
#  if !defined(H2D2_WINDOWS) || !defined(MODE_DYNAMIC)
#     define F2C_CONF_DLL_IMPORT
#     define F2C_CONF_DLL_EXPORT
#  else
#     define F2C_CONF_DLL_IMPORT __declspec(dllimport)
#     define F2C_CONF_DLL_EXPORT __declspec(dllexport)
#  endif

#elif defined (__GNUC__)
#  define H2D2_CMPLR_GCC  1
#  define H2D2_CMPLR_NAME "GCC"
#  define H2D2_CMPLR_VFMT std::dec
#  define H2D2_CMPLR_VERS (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#  define F2C_CONF_DECOR_FNC(F, f) f ## _
#  define F2C_CONF_DECOR_MDL(M, m, F, f) __ ## m ## _MOD_ ## f
#  define F2C_CONF_NOM_FONCTION_MIN_
#  define F2C_CONF_STRING   char*
#  define F2C_CONF_SUP_INT  , int
#  define F2C_CONF_A_SUP_INT

#  define F2C_CONF_CNV_APPEL
#  if !defined(H2D2_WINDOWS) || !defined(MODE_DYNAMIC)
#     define F2C_CONF_DLL_IMPORT
#     define F2C_CONF_DLL_EXPORT
#  else
#     define F2C_CONF_DLL_IMPORT __declspec(dllimport)
#     define F2C_CONF_DLL_EXPORT __declspec(dllexport)
#  endif

#elif defined (_MSC_VER)
#  define H2D2_CMPLR_MSVC 1
#  define H2D2_CMPLR_NAME "Microsoft"
#  define H2D2_CMPLR_VFMT std::dec
#  define H2D2_CMPLR_VERS _MSC_VER
#  define F2C_CONF_DECOR_FNC(F, f) F
//#  define F2C_CONF_DECOR_MDL(M, m, F, f) M ## _mp_ ## F
#  define F2C_CONF_NOM_FONCTION_MAJ
#  define F2C_CONF_STRING   char*
#  define F2C_CONF_SUP_INT  , unsigned int
#  define F2C_CONF_A_SUP_INT

#  define F2C_CONF_CNV_APPEL
#  if !defined(H2D2_WINDOWS) || !defined(MODE_DYNAMIC)
#     define F2C_CONF_DLL_IMPORT
#     define F2C_CONF_DLL_EXPORT
#  else
#     define F2C_CONF_DLL_IMPORT __declspec(dllimport)
#     define F2C_CONF_DLL_EXPORT __declspec(dllexport)
#  endif

#else
#  error Unsupported compiler
#endif

// ---  INTEGER par défaut du FORTRAN forcé par
//      la macro INRS_FORTRAN_INTEGER_SIZE
#if defined(INRS_FORTRAN_INTEGER_SIZE)
#  undef H2D2_INTSIZE
#  if   (INRS_FORTRAN_INTEGER_SIZE == 4)
#     define H2D2_INTSIZE 32
#  elif (INRS_FORTRAN_INTEGER_SIZE == 8)
#     define H2D2_INTSIZE 64
#  else
#     error Unsupported INRS_FORTRAN_INTEGER_SIZE value. Expected values are [4, 8]
#  endif
#endif

// ---  Type pour les int et les handle
#if defined (__GNUC__)
#  include <stdint.h>
#elif defined (__INTEL_COMPILER)
#  if defined(_M_IA64)
#     include <stddef.h>
      typedef __int32 int32_t;
      typedef __int64 int64_t;
#  elif defined(_M_X64)
#     include <stddef.h>
      typedef __int32 int32_t;
      typedef __int64 int64_t;
#  else
#     include <stddef.h>
      typedef __int32 int32_t;
      typedef __int64 int64_t;
#  endif
#elif defined (_MSC_VER)
#  include <stddef.h>
   typedef __int32 int32_t;
   typedef __int64 int64_t;
#elif defined (__SUNPRO_C) || defined (__SUNPRO_CC)
#  include <stdint.h>
#elif defined (__WATCOMC__)
#  include <stdint.h>
#else
#  error Configuring integer size - Unsupported compiler
#endif

#if defined(H2D2_INTSIZE)
#  if   (H2D2_INTSIZE == 32)
      typedef int32_t fint_t;
#  elif (H2D2_INTSIZE == 64)
      typedef int64_t fint_t;
#  else
#     error Invalid integer size
#  endif
#else
#  error Undefined integer size
#endif

//typedef intptr_t hndl_t;
typedef int64_t hndl_t;

// ---  Configuration générique pour les DLL
#define MODULE_ACTIF 2
#define CCConfig_dll_hlpr_1        F2C_CONF_DLL_IMPORT
#define CCConfig_dll_hlpr_2        F2C_CONF_DLL_EXPORT
#define CCConfig_dll_helper(mdl)   CCConfig_dll_hlpr_ ## mdl
#define F2C_CONF_DLL_DECLSPEC(mdl) CCConfig_dll_helper(mdl)

#endif   // CCONFIG_H_DEJA_INCLU
