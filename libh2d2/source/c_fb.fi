C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C   Fichier d'inclusion qui définit les prototypes des fonctions qui font le pont
C   avec du code écris en C et en C++.
C
C
C   Prototype: INTEGER C_FB_OUVRE(CHARACTER(*) NOM, INTEGER MODE);
C     La subroutine C_FB_OUVRE ouvre le fichier binaire en mode C_FB_ECRITURE
C     ou en mode C_FB_LECTURE. La fonction retourne un handle sur le fichier
C     qui doit être passé aux autres fonctions.
C           <> 0 s'il n'y a pas d'erreur.
C           0 s'il y a eu une erreur
C
C   Prototype: INTEGER C_FB_ECRIS(INTEGER HDL, INTEGER LEN, REAL*8(*) BUF);
C     La subroutine C_FB_ECRIS écris LEN REAL*8 contenu dans BUF dans le fichier
C     de handle HDL.
C            0 s'il n'y a pas d'erreur
C           -1 s'il y a eu une erreur
C
C   Prototype: INTEGER C_FB_LIS(INTEGER HDL, INTEGER LEN, REAL*8(*) BUF);
C     La subroutine C_FB_LIS lis LEN REAL*8 dans BUF à partir du fichier
C     de handle HDL.
C            0 s'il n'y a pas d'erreur
C           -1 s'il y a eu une erreur
C
C   Prototype: INTEGER C_FB_FERME(INTEGER HDL);
C     La subroutine C_FB_FERME ferme le fichier de handle HDL.
C            0 s'il n'y a pas d'erreur
C           -1 s'il y a eu une erreur
C
C   Prototype: INTEGER C_FB_INVERSE(CHARACTER*(*) FINP, CHARACTER*(*) FOUT,
C                                   INTEGER FLEN, INTEGER BLEN, REAL*8(*) BUF);
C     La subroutine C_FB_INVERSE va inverser le fichier de nom FINP et met le
C     résultat dans le fichier FOUT. BUF est un buffer de longueur BLEN.
C            0 s'il n'y a pas d'erreur
C           -1 s'il y a eu une erreur
C
C************************************************************************
C$NOREFERENCE

      INTEGER C_FB_ECRITURE
      INTEGER C_FB_LECTURE
      PARAMETER (C_FB_LECTURE  = 1)
      PARAMETER (C_FB_ECRITURE = 2)

      INTEGER*8 C_FB_OUVRE
      INTEGER   C_FB_ECRIS
      INTEGER   C_FB_LIS
      INTEGER   C_FB_FERME
      INTEGER   C_FB_REQDIM
      INTEGER   C_FB_INVERSE

C$REFERENCE

