//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Fichier Binaire
// Sommaire: Fonctions C de gestion de Fichiers Binaires
//************************************************************************
#include "c_fb.h"

#include <fcntl.h>
#include <string.h>

#if defined(H2D2_WINDOWS)
#  if   defined(__INTEL_COMPILER)
#    include <io.h>
#    include <stdio.h>
#    include <sys/stat.h>
#  elif defined(_MSC_VER)
#    include <io.h>
#    include <stdio.h>
#    include <sys/stat.h>
#  elif defined(__GNUC__)
#    include <unistd.h>
#    include <stdio.h>
#    include <sys/stat.h>
#  elif defined(__WATCOMC__)
#    include <io.h>
#    include <stdio.h>
#  endif
#elif defined(H2D2_UNIX)
#  include <unistd.h>
#  include <stdio.h>
#  include <sys/stat.h>
#else
#  error Invalid operating system
#endif

#ifndef O_BINARY        // Unix ne définit pas O_BINARY
#  define O_BINARY 0    // On l'ajoute comme NoOp
#endif

//************************************************************************
// Sommaire:   Ouvre un fichier binaire
//
// Description:
//    La fonction C_FB_OUVRE(...) ouvre le fichier binaire suivant le mode
//    demandé. En cas de succès, elle retourne un Handle sur le fichier
//    qui pourra être utilisé dans d'autres appels. En cas d'erreur, la
//    fonction retourne 0.
//
// Entrée:
//    WatForStr* fic;      Nom du fichier
//    fint_t*    mode;     Mode d'ouverture
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_OUVRE (F2C_CONF_STRING  fic,
                                                          fint_t*  mode
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_OUVRE (F2C_CONF_STRING  fic,
                                                          fint_t*  mode)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* fP = fic;
   int   l  = len;
#else
   char* fP = fic->strP;
   int   l  = fic->len;
#endif

   enum Mode {MODE_LECTURE = 1, MODE_ECRITURE= 2};

   int handle = -1;

   if (l < 1024)
   {
      char nom[1024];
      strncpy(nom, fP, l);
      nom[l] = 0x0;

      switch (*mode)
      {
         case MODE_LECTURE:
            handle = open(nom, O_RDONLY | O_BINARY);
         break;
         case MODE_ECRITURE:
            handle = open(nom, O_WRONLY | O_BINARY | O_CREAT, S_IREAD | S_IWRITE);
         break;
      }
   }

   return handle;
}

//************************************************************************
// Sommaire:   Écris un buffer dans un fichier binaire.
//
// Description:
//    La fonction C_FB_ECRIS(...) écris un buffer de "double" dans le fichier
//    binaire dont le handle est passé en argument. La fonction retourne -1
//    en cas d'erreur, sinon 0.
//
// Entrée:
//    fint_t*    handle;      Handle du fichier
//    fint_t*    len;         Longueur du buffer
//    double*    buf;         Buffer
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_ECRIS (hndl_t* handle,
                                                          fint_t* len,
                                                          double* buf)
{
   int fic = static_cast<int>(*handle);
   unsigned lbyte = (*len) * static_cast<unsigned>(sizeof(double));
   int l = write(fic, buf, lbyte);
   return (l >= 0 && static_cast<unsigned>(l) == lbyte) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Lis un buffer d'un fichier binaire.
//
// Description:
//    La fonction C_FB_LIS(...) lis un buffer de "double" du fichier binaire dont
//    le handle est passé en argument. La fonction retourne -1 en cas d'erreur,
//    sinon 0.
//
// Entrée:
//    fint_t*    handle;      Handle du fichier
//    fint_t*    len;         Longueur à lire
//
// Sortie:
//    double*    buf;         Buffer
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_LIS (hndl_t* handle,
                                                        fint_t* len,
                                                        double* buf)
{
   int fic = static_cast<int>(*handle);
   unsigned lbyte = (*len) * static_cast<unsigned>(sizeof(double));
   int l = read (fic, buf, lbyte);
   return (l >= 0 && static_cast<unsigned>(l) == lbyte) ? 0 : -1;
}

//************************************************************************
// Sommaire: Ferme le fichier binaire.
//
// Description:
//    La fonction C_FB_FERME(...) ferme le fichier binaire dont le Handle est
//    passé en argument. La fonction retourne -1 en cas d'erreur, sinon 0.
//
// Entrée:
//    hndl_t* handle;      Handle du fichier
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_FERME (hndl_t* handle)
{
   int fic = static_cast<int>(*handle);
   return close(fic);
}

//************************************************************************
// Sommaire:   Retourne la taille du fichier.
//
// Description:
//    La fonction C_FB_REQDIM(...) retourne la taille du fichier
//    La fonction retourne -1 en cas d'erreur, et sinon la taille.
//
// Entrée:
//    WatForStr* fic;      Nom du fichier
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_REQDIM(F2C_CONF_STRING  fic
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FB_REQDIM(F2C_CONF_STRING  fic)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* fP = fic;
   // int   l = len;
#else
   char* fP = fic->strP;
   // int   l = fic->len;
#endif

   long size = -1;
   FILE *f = fopen(fP, "rb");
   if (f)
   {
      fseek(f, 0, SEEK_END);
      size = ftell(f);
      fclose(f);
   }
   return size;
}

//************************************************************************
// Sommaire:  Inverse le fichier.
//
// Description:
//    La fonction FB_INVERSE(...) inverse double à double les données du
//    fichier ficInp et les écris dans le fichier ficOut. On inverse les
//    ficLen premières valeurs de ficInp.
//    La fonction retourne -1 en cas d'erreur, sinon 0.
//
// Entrée:
//    WatForStr* ficInp;      Nom du fichier source
//    WatForStr* ficOut;      Nom du fichier destination
//    fint_t*    ficlen;      Longueur du fichier
//    fint_t*    buflen;      Taille du buffer de travail
//    double*    buf;         Buffer de travail
//
// Sortie:
//
// Notes:
//    Cette fonction utilise fopen(), fread(), etc... car les f...()
//    supportent le positionnement dans les gros fichiers avec fsetpos()
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_INVERSE (F2C_CONF_STRING  ficInp,
                                                            F2C_CONF_STRING  ficOut,
                                                            fint_t*          ficlen,
                                                            fint_t*          buflen,
                                                            double*          buf
                                                            F2C_CONF_SUP_INT lenInp
                                                            F2C_CONF_SUP_INT lenOut)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FB_INVERSE (F2C_CONF_STRING  ficInp,
                                                            F2C_CONF_STRING  ficOut,
                                                            fint_t*          ficlen,
                                                            fint_t*          buflen,
                                                            double*          buf)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* fiP = ficInp;
   int   li  = lenInp;
   char* foP = ficOut;
   int   lo  = lenOut;
#else
   char* fiP = ficInp->strP;
   int   li  = ficInp->len;
   char* foP = ficOut->strP;
   int   lo  = ficOut->len;
#endif

   fint_t ierr = 0;
   char    nom[1024];

   if (li >= 1024) ierr = -1;
   if (lo >= 1024) ierr = -1;

   FILE* hInp = NULL;
   if (ierr == 0)
   {
      strncpy(nom, fiP, li);
      nom[li] = 0x0;
      hInp = fopen(nom, "rb");
      if (hInp == NULL) ierr = 1;
   }

   FILE* hOut = NULL;
   if (ierr == 0)
   {
      strncpy(nom, foP, lo);
      nom[lo] = 0x0;
      hOut = fopen(nom, "wb");
      if (hOut == NULL) ierr = 1;
   }

   if (ierr == 0)
   {
#ifdef H2D2_WINDOWS
      fpos_t offset = (*ficlen) * sizeof(double);
#else
      long offset = (*ficlen) * sizeof(double);
#endif
      size_t lenrest  = (*ficlen);
      size_t lenblock = (*buflen);

      do
      {
         if (lenrest < lenblock)
            lenblock = lenrest;

         offset -= lenblock*sizeof(double);
// fseek devrait également fonctionner sur Windows
// le ifdef préserve le code validé sur Windows
#ifdef H2D2_WINDOWS
         if (fsetpos(hInp, &offset) != 0) break;
#else
         if (fseek(hInp, offset, SEEK_SET) != 0) break;
#endif
         if (fread(buf, sizeof(double), lenblock, hInp) != lenblock) break;

         double* bufbeg = buf;
         double* bufend = buf + (lenblock-1);
         do
         {
            double tmp = *bufbeg;
            *bufbeg = *bufend;
            *bufend = tmp;
            ++bufbeg;
            --bufend;
         } while (bufbeg < bufend);

         if (fwrite(buf, sizeof(double), lenblock, hOut) != lenblock) break;

         lenrest -= lenblock;
      } while (lenrest > 0);
      ierr = (lenrest == 0) ? 0 : -1;
   }

   if (hInp != NULL) fclose(hInp);
   if (hOut != NULL) fclose(hOut);

   return ierr;
}
