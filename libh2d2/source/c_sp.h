//************************************************************************
// --- Copyright (c) INRS 2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Groupe:  fonctions C
// Sous-Groupe:  Sub-Process (task)
// Sommaire: Fonctions C de gestion des tâches
//************************************************************************
#ifndef C_SP_H_DEJA_INCLU
#define C_SP_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_SP_START    F2C_CONF_DECOR_FNC(C_SP_START,   c_sp_start)
#define C_SP_KILL     F2C_CONF_DECOR_FNC(C_SP_KILL,    c_sp_kill)
#define C_SP_SUSPEND  F2C_CONF_DECOR_FNC(C_SP_SUSPEND, c_sp_suspend)
#define C_SP_RESUME   F2C_CONF_DECOR_FNC(C_SP_RESUME,  c_sp_resume)
#define C_SP_WAIT     F2C_CONF_DECOR_FNC(C_SP_WAIT,    c_sp_wait)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_START    (hndl_t*, hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_KILL     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_SUSPEND  (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_RESUME   (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SP_WAIT     (hndl_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_SP_H_DEJA_INCLU
