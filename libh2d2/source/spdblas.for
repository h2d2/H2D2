C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  SousProgrammes utilitaires
C Sous-Groupe:  Double BLAS
C Sommaire: Fonctions BLAS complémentaires sur des REAL*8
C************************************************************************

C************************************************************************
C Sommaire: Initialise un vecteur de REAL*8
C
C Description:
C     La sousroutine DINIT initialise le vecteur de REAL*8 X
C     à la valeur A.
C
C Entrée:
C     N        Nombre d'éléments à initialiser
C     A        Valeur d'initialisation
C     IX       Incrément dans le vecteur
C
C Sortie:
C     X        Vecteur à initialiser
C
C Notes:
C
C************************************************************************
      SUBROUTINE DINIT(N,A,X,IX)

      IMPLICIT NONE
      INTEGER N, IX
      REAL*8  A, X(*)
C--------------------------------------------------------------------------

      CALL DCOPY(N, A, 0, X, IX)

      RETURN
      END

C************************************************************************
C Sommaire: Indice de la valeur max de la table
C
C Description:
C     La fonction IDMAX retourne la position de la première présence
C     d'un élément qui a la valeur max.
C
C Entrée:
C     N        Nombre d'éléments à initialiser
C     X        Vecteur X
C     IX       Incrément dans le vecteur X
C
C Sortie:
C     IDMAX    Indice de la valeur max
C
C Notes:
C
C************************************************************************
      FUNCTION IDMAX(N, X, INCX)

      IMPLICIT NONE

      INTEGER IDMAX
      INTEGER N
      REAL*8  X(*)
      INTEGER INCX

      REAL*8  DMAX
      INTEGER I, IX
C--------------------------------------------------------------------------

      IDMAX = 0
      IF (N.LT.1 .OR. INCX.LE.0) RETURN

      IDMAX = 1
      IF (N.EQ.1) RETURN

      IF (INCX.EQ.1) THEN
         DMAX = X(1)
         DO I=2,N
            IF (X(I) .GT. DMAX) THEN
               IDMAX = I
               DMAX  = X(I)
            ENDIF
         ENDDO
      ELSE
         IX = 1
         DMAX = X(1)
         IX = IX + INCX
         DO I=2,N
            IF (X(IX) .GT. DMAX) THEN
               IDMAX = I
               DMAX  = X(IX)
            ENDIF
            IX = IX + INCX
         ENDDO
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Indice de la valeur min de la table
C
C Description:
C     La fonction IDMIN retourne la position de la première présence
C     d'un élément qui a la valeur min.
C
C Entrée:
C     N        Nombre d'éléments à initialiser
C     X        Vecteur X
C     IX       Incrément dans le vecteur X
C
C Sortie:
C     IDMIN    Indice de la valeur min
C
C Notes:
C
C************************************************************************
      FUNCTION IDMIN(N, X, INCX)

      IMPLICIT NONE

      INTEGER IDMIN
      INTEGER N
      REAL*8  X(*)
      INTEGER INCX

      REAL*8  DMIN
      INTEGER I, IX
C--------------------------------------------------------------------------

      IDMIN = 0
      IF (N.LT.1 .OR. INCX.LE.0) RETURN

      IDMIN = 1
      IF (N.EQ.1) RETURN

      IF (INCX.EQ.1) THEN
         DMIN = X(1)
         DO I=2,N
            IF (X(I) .LT. DMIN) THEN
               IDMIN = I
               DMIN  = X(I)
            ENDIF
         ENDDO
      ELSE
         IX = 1
         DMIN = X(1)
         IX = IX + INCX
         DO I=2,N
            IF (X(IX) .LT. DMIN) THEN
               IDMIN = I
               DMIN  = X(IX)
            ENDIF
            IX = IX + INCX
         ENDDO
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Fait l'opération y = y * (a*x)
C
C Description:
C     La sousroutine DAXTY effectue l'opération y = y * (a*x) ou x, y sont
C     des vecteurs et A une constante. Inspirée de DAXPY elle fait un
C     produit vecteur-vecteur composante par composante.
C
C Entrée:
C     N        Nombre d'éléments à initialiser
C     A        Valeur multiplicative
C     X        Vecteur X
C     IX       Incrément dans le vecteur X
C     Y        Vecteur Y
C     IY       Incrément dans le vecteur Y
C
C Sortie:
C     Y        Vecteur Y mis à jour
C
C Notes:
C
C************************************************************************
      SUBROUTINE DAXTY(N, A, X, INCX, Y, INCY)

      IMPLICIT NONE

      INTEGER N
      REAL*8  A
      REAL*8  X(*)
      INTEGER INCX
      REAL*8  Y(*)
      INTEGER INCY

      INTEGER I, IX, IY
C--------------------------------------------------------------------------

      IF (N .LE. 0) RETURN

      IF (A .EQ. 0.0D0) THEN
         CALL DINIT(N, A, Y, INCY)
      ELSEIF (INCX .EQ. 1 .AND. INCY .EQ. 1) THEN
         DO I=1,N
            Y(I) = Y(I) * A*X(I)
         ENDDO
      ELSE
         IX = 1
         IY = 1
         IF (INCX .LT. 0) IX = (-N+1)*INCX + 1
         IF (INCY .LT. 0) IY = (-N+1)*INCY + 1
         DO I = 1,N
            Y(IY) = Y(IY) * A*X(IX)
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire: Fait l'opération x = (a*x)
C
C Description:
C     La sousroutine DSCALI effectue l'opération x = (a*x) ou x est
C     un vecteur morse d'indices J et A une constante.
C
C Entrée:
C     N        Nombre d'éléments
C     A        Valeur multiplicative
C     J        Tables des indices de X
C     X        Vecteur X
C
C Sortie:
C     X        Vecteur X mis à jour
C
C Notes:
C
C************************************************************************
      SUBROUTINE DSCALI(N, A, J, X)

      IMPLICIT NONE

      INTEGER N
      REAL*8  A
      INTEGER J(*)
      REAL*8  X(*)

      INTEGER I, IJ
C--------------------------------------------------------------------------

      IF (N .GT. 0) THEN
         DO I = 1,N
            IJ = J(I)
            X(IJ) = A*X(IJ)
         ENDDO
      ENDIF

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La sousroutine TRANS transpose la table A(M,N)
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE TRANS(A, M, N, MN, MOVE, IWRK, IOK)                    TRA   10
C---------  YSe begin change
      implicit real*8 (a-h,o-z)
      real*8 move
C---------  YSe end change
C *****
C  ALGORITHM 380 - REVISED
C *****
C  A IS A ONE-DIMENSIONAL ARRAY OF LENGTH MN=M*N, WHICH
C  CONTAINS THE MXN MATRIX TO BE TRANSPOSED (STORED
C  COLUMWISE). MOVE IS A ONE-DIMENSIONAL ARRAY OF LENGTH IWRK
C  USED TO STORE INFORMATION TO SPEED UP THE PROCESS.  THE
C  VALUE IWRK=(M+N)/2 IS RECOMMENDED. IOK INDICATES THE
C  SUCCESS OR FAILURE OF THE ROUTINE.
C  NORMAL RETURN  IOK=0
C  ERRORS         IOK=-1 ,MN NOT EQUAL TO M*N
C                 IOK=-2 ,IWRK NEGATIVE OR ZERO
C                 IOK.GT.0, (SHOULD NEVER OCCUR),IN THIS CASE
C  WE SET IOK EQUAL TO THE FINAL VALUE OF I WHEN THE SEARCH
C  IS COMPLETED BUT SOME LOOPS HAVE NOT BEEN MOVED
C  NOTE * MOVE(I) WILL STAY ZERO FOR FIXED POINTS
      DIMENSION A(MN), MOVE(IWRK)
C CHECK ARGUMENTS AND INITIALIZE.
      IF (M.LT.2 .OR. N.LT.2) GO TO 120
      IF (MN.NE.M*N) GO TO 180
      IF (IWRK.LT.1) GO TO 190
      IF (M.EQ.N) GO TO 130
      NCOUNT = 2
      K = MN - 1
      DO 10 I=1,IWRK
        MOVE(I) = 0
   10 CONTINUE
      IF (M.LT.3 .OR. N.LT.3) GO TO 30
C CALCULATE THE NUMBER OF FIXED POINTS, EUCLIDS ALGORITHM
C FOR GCD(M-1,N-1).
      IR2 = M - 1
      IR1 = N - 1
   20 IR0 = MOD(IR2,IR1)
      IR2 = IR1
      IR1 = IR0
      IF (IR0.NE.0) GO TO 20
      NCOUNT = NCOUNT + IR2 - 1
C SET INITIAL VALUES FOR SEARCH
   30 I = 1
      IM = M
C AT LEAST ONE LOOP MUST BE RE-ARRANGED
      GO TO 80
C SEARCH FOR LOOPS TO REARRANGE
   40 MAX = K - I
      I = I + 1
      IF (I.GT.MAX) GO TO 160
      IM = IM + M
      IF (IM.GT.K) IM = IM - K
      I2 = IM
      IF (I.EQ.I2) GO TO 40
      IF (I.GT.IWRK) GO TO 60
      IF (MOVE(I).EQ.0) GO TO 80
      GO TO 40
   50 I2 = M*I1 - K*(I1/N)
   60 IF (I2.LE.I .OR. I2.GE.MAX) GO TO 70
      I1 = I2
      GO TO 50
   70 IF (I2.NE.I) GO TO 40
C REARRANGE THE ELEMENTS OF A LOOP AND ITS COMPANION LOOP
   80 I1 = I
      KMI = K - I
      B = A(I1+1)
      I1C = KMI
      C = A(I1C+1)
   90 I2 = M*I1 - K*(I1/N)
      I2C = K - I2
      IF (I1.LE.IWRK) MOVE(I1) = 2
      IF (I1C.LE.IWRK) MOVE(I1C) = 2
      NCOUNT = NCOUNT + 2
      IF (I2.EQ.I) GO TO 110
      IF (I2.EQ.KMI) GO TO 100
      A(I1+1) = A(I2+1)
      A(I1C+1) = A(I2C+1)
      I1 = I2
      I1C = I2C
      GO TO 90
C FINAL STORE AND TEST FOR FINISHED
  100 D = B
      B = C
      C = D
  110 A(I1+1) = B
      A(I1C+1) = C
      IF (NCOUNT.LT.MN) GO TO 40
C NORMAL RETURN
  120 IOK = 0
      RETURN
C IF MATRIX IS SQUARE,EXCHANGE ELEMENTS A(I,J) AND A(J,I).
  130 N1 = N - 1
      DO 150 I=1,N1
        J1 = I + 1
        DO 140 J=J1,N
          I1 = I + (J-1)*N
          I2 = J + (I-1)*M
          B = A(I1)
          A(I1) = A(I2)
          A(I2) = B
  140   CONTINUE
  150 CONTINUE
      GO TO 120
C ERROR RETURNS.
  160 IOK = I
  170 RETURN
  180 IOK = -1
      GO TO 170
  190 IOK = -2
      GO TO 170
      END
