//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonction C de lecture/écriture dans des fichiers ASCII
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_FA_H_DEJA_INCLU
#define C_FA_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_FA_OUVRE    F2C_CONF_DECOR_FNC(C_FA_OUVRE,  c_fa_ouvre)
#define C_FA_FERME    F2C_CONF_DECOR_FNC(C_FA_FERME,  c_fa_ferme)
#define C_FA_ECRISLN  F2C_CONF_DECOR_FNC(C_FA_ECRISLN,c_fa_ecrisln)
#define C_FA_LISLN    F2C_CONF_DECOR_FNC(C_FA_LISLN,  c_fa_lisln)
#define C_FA_LISDBL   F2C_CONF_DECOR_FNC(C_FA_LISDBL, c_fa_lisdbl)
#define C_FA_LISINT   F2C_CONF_DECOR_FNC(C_FA_LISINT, c_fa_lisint)
#define C_FA_SKIPN    F2C_CONF_DECOR_FNC(C_FA_SKIPN,  c_fa_skipn)
#define C_FA_ASGPOS   F2C_CONF_DECOR_FNC(C_FA_ASGPOS, c_fa_asgpos)
#define C_FA_REQPOS   F2C_CONF_DECOR_FNC(C_FA_REQPOS, c_fa_reqpos)


F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FA_OUVRE   (F2C_CONF_STRING, fint_t* F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_FERME   (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_ECRISLN (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISLN   (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISDBL  (hndl_t*, double*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISINT  (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_SKIPN   (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_ASGPOS  (hndl_t*, int64_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_REQPOS  (hndl_t*, int64_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_FA_H_DEJA_INCLU

