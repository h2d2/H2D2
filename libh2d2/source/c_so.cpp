//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Shared Object
// Sommaire: Fonctions C de dll (Shared Object)
//************************************************************************
#include "c_so.h"

#include <string>
#include <algorithm>       // std::transform

#if defined(H2D2_WINDOWS)
#  if defined(__MINGW32__)
#    include "windll.h"
#  elif defined(__INTEL_COMPILER)
#    include "windll.h"
#  elif defined(_MSC_VER)
#    include "windll.h"
#  elif defined(__WATCOMC__)
#    include "windll.h"
#  elif defined(__GNUC__)
#    include "unxdl.h"
#    include <stdlib.h>
#    include <string.h>
#  endif
#elif defined(H2D2_UNIX)
#  include "unxdl.h"
#  include <stdio.h>       // printf
#  include <stdlib.h>
#  include <string.h>
#else
#  error Invalid operating system
#endif

#ifdef __cplusplus
extern "C"           // Exigence SunPro à cause des pointeurs aux fonctions
{
#endif

//************************************************************************
// Sommaire:   Charge un module.
//
// Description:
//    La fonction <code>C_SO_LOADMDL</code> charge le module ("dll") dont
//    la nom est donnée par <code>str</code>. Elle retourne dans
//    <code>hmodule</code> le handle sur le module.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    WatForStr* str;            Nom du module
//
// Sortie:
//    fint_t*    hmodule;        Handle sur le module
//
// Notes: il serait bien d'écrire une fonction pour gérer les erreurs de
//        dlopen
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADMDL (hndl_t* hmodule,
                                                            F2C_CONF_STRING  str
                                                            F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADMDL (hndl_t* hmodule,
                                                            F2C_CONF_STRING  str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = 0;
   std::string s(sP, l);

#if defined(H2D2_WINDOWS)
   HMODULE h = 0;

   s += ".dll";
   h = FAKE_DLL_LoadLibrary(s.c_str());
   if (h != NULL)
      *hmodule = reinterpret_cast<hndl_t>(h);
   else
      ierr = GetLastError();

#else    // Unix shared objects
   void* h = NULL;

   s = "lib" + s + ".so";
   h = FAKE_DLL_dlopen(s.c_str(), RTLD_NOW);
   if (h != NULL)
   {
      *hmodule = reinterpret_cast<hndl_t>(h);
   }
   else
   {
      char* dlerr = FAKE_DLL_dlerror();
      if (dlerr)
         printf("dlopen reported an error: %s\n", dlerr);
      else
         printf("dlopen reported an error\n");
      ierr = -1;
   }

#endif

   return ierr;
}

//************************************************************************
// Sommaire:   Libère le module.
//
// Description:
//    La fonction <code>C_SO_FREEMDL</code> libère le module dont le handle
//    <code>hmodule</code> est passé en argument.
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    fint_t*    hmodule;        Handle sur le module
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_FREEMDL (hndl_t* hmodule)
{
   fint_t ierr = 0;

#if defined(H2D2_WINDOWS)
   (void) FAKE_DLL_FreeLibrary(reinterpret_cast<HMODULE>(*hmodule));

#else    // Unix shared objects
   ierr = FAKE_DLL_dlclose( reinterpret_cast<void*>(*hmodule));
   ierr = (ierr == 0) ? 0 : -1;

#endif

   return ierr;
}

//************************************************************************
// Sommaire:   Charge une fonction d'un module.
//
// Description:
//    La fonction <code>C_SO_LOADFNC</code> charge la fonction de nom
//    <code>str</code> du module dont le handle est <code>hmodule</code>.
//    Elle retourne la fonction sous la forme du handle <code>fptr</code>
//    qui pourra être utilisé à l'aide des fonctions <code>C_SO_CBFNC0</code>...
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    hndl_t*    hmodule;        Handle sur le module
//    WatForStr* str;            Nom de la fonction
//
// Sortie:
//    hndl_t*    fptr;           Handle sur la fonction
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADFNC (hndl_t* fptr,
                                                            hndl_t* hmodule,
                                                            F2C_CONF_STRING  str
                                                            F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_LOADFNC (hndl_t* fptr,
                                                            hndl_t* hmodule,
                                                            F2C_CONF_STRING  str)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* sP = str;
   int   l  = len;
#else
   char* sP = str->strP;
   int   l  = str->len;
#endif

   fint_t ierr = 0;

#if defined(F2C_CONF_NOM_FONCTION_MAJ)
   std::string s = std::string(sP, l);
#elif defined(F2C_CONF_NOM_FONCTION_MIN_)
   std::string s = std::string(sP, l) + '_';
   std::transform(s.begin(), s.end(), s.begin(), tolower);
#elif defined(F2C_CONF_NOM_FONCTION_MIN__)
   std::string s = std::string(sP, l) + "__";
   std::transform(s.begin(), s.end(), s.begin(), tolower);
#else
#  error Unsupported FORTRAN to C naming convention
#endif

#if defined(H2D2_WINDOWS)
   FARPROC fp = NULL;

   fp = FAKE_DLL_GetProcAddress(reinterpret_cast<HMODULE>(*hmodule), s.c_str());
   if (fp != NULL)
      *fptr = reinterpret_cast<hndl_t>(fp);
   else
      ierr = GetLastError();

#else    // Unix shared objects
   void* fp = NULL;

   fp = FAKE_DLL_dlsym(reinterpret_cast<void*>(*hmodule), s.c_str());
   if (fp != NULL)
      *fptr = reinterpret_cast<hndl_t>(fp);
   else
      ierr = -1;

#endif

   return ierr;
}

//************************************************************************
// Sommaire:   Libère la fonction.
//
// Description:
//    La fonction <code>C_SO_FREEFNC</code> libère la fonction de handle
//    <code>fptr</code>.
//
// Entrée:
//    hndl_t*    fptr;           Handle sur la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_FREEFNC (hndl_t* fptr)
{
   fint_t ierr = 0;
   *fptr = 0;
   return ierr;
}

//************************************************************************
// Sommaire:   Donne le handle sur une fonction.
//
// Description:
//    La fonction <code>C_SO_REQHFNC</code> retourne le handle <code>fptr</code>
//    qui correspond à la fonction <code>fp</code>. Le handle <code>fptr</code>
//    pourra alors être utilisé à l'aide des fonctions <code>C_SO_CBFNC0</code>...
//    La fonction retourne 0 en cas de succès et -1 en cas d'échec.
//
// Entrée:
//    fint_t (TYPE_APPEL_FONCTION *fp)();    Fonction
//
// Sortie:
//    fint_t*    fptr;                       Handle sur la fonction
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_REQHFNC (hndl_t* fptr,
                                                            fint_t (F2C_CONF_CNV_APPEL *fp) ())
{
   fint_t ierr = 0;
   *fptr = reinterpret_cast<hndl_t>(fp);
   return ierr;
}

//************************************************************************
// Sommaire:   Retourne la fonction d'un handle.
//
// Description:
//    La fonction <code>C_SO_REQFNC</code> retourne la fonction <code>fp</code>
//    qui correspond au handle <code>fptr</code>.
//
// Entrée:
//    fint_t*    fptr;                       Handle sur la fonction
//
// Sortie:
//    fint_t (TYPE_APPEL_FONCTION *fp)();    Fonction
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_REQFNC (fint_t (F2C_CONF_CNV_APPEL *fp) (),
                                                           hndl_t* fptr)
{
   fint_t ierr = 0;
   fp = reinterpret_cast<fint_t (F2C_CONF_CNV_APPEL *)()>(*fptr);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL0</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 0 arguments qui retourne un integer.
//    <p>
//    L'utilisation de ces fonctionnalités à partir du FORTRAN est esquissée
//    par l'exemple suivant:
//    <pre>
//      FONCTION F0()
//         INTEGER F0
//         WRITE(6,*) 'HELLO WORLD'
//      RETURN
//
//      PROGRAM
//         INTEGER  F0
//         EXTERNAL F0
//         INTEGER HF0
//
//         HF0 = C_SO_REQHFNC(F0)    ! Handle sur la fonction
//         IERR = C_SO_CALL0(HF0)    ! Appel de F0 via HF0
//      RETURN
//      END
//    </pre>
//
// Entrée:
//    hndl_t* fptr;                             Handle sur la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL0 (hndl_t* fptr)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)();
   return reinterpret_cast<func_t>(*fptr)();
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL1</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 1 argument qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL1 (hndl_t* fptr,
                                                          fint_t* p1)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL2</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 2 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL2 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL3</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 3 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL3 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL4</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 4 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL4 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL5</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 5 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL5 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4,
                                                          fint_t* p5)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4, p5);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL6</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 6 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL6 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4,
                                                          fint_t* p5,
                                                          fint_t* p6)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4, p5, p6);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL7</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 7 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d’arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL7 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4,
                                                          fint_t* p5,
                                                          fint_t* p6,
                                                          fint_t* p7)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4, p5, p6, p7);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL8</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 8 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL8 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4,
                                                          fint_t* p5,
                                                          fint_t* p6,
                                                          fint_t* p7,
                                                          fint_t* p8)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4, p5, p6, p7, p8);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CALL9</code> appelle la fonction qui correspond
//    au handle <code>fptr</code>. Elle permet de traduire ce handle.
//    Cette version est pour une fonction à 9 arguments qui retourne un integer.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    hndl_t* fptr;                 Handle sur la fonction
//    fint_t* p1,...,pn;            Paramètres de la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CALL9 (hndl_t* fptr,
                                                          fint_t* p1,
                                                          fint_t* p2,
                                                          fint_t* p3,
                                                          fint_t* p4,
                                                          fint_t* p5,
                                                          fint_t* p6,
                                                          fint_t* p7,
                                                          fint_t* p8,
                                                          fint_t* p9)
{
   typedef fint_t (F2C_CONF_CNV_APPEL *func_t)(fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
   return reinterpret_cast<func_t>(*fptr)(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC0</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 0 arguments.
//    <p>
//    L'utilisation de ces fonctionnalités à partir du FORTRAN est esquissée
//    par l'exemple suivant:
//    <pre>
//      FONCTION F0()
//         INTEGER F0
//         WRITE(6,*) 'HELLO WORLD'
//      RETURN
//
//      FONCTION CB(F)
//         INTEGER CB
//         INTEGER F
//         EXTERNAL F
//         CB = F()
//      RETURN
//      END
//
//      PROGRAM
//         INTEGER  F0
//         EXTERNAL F0
//         INTEGER HF0
//
//         HF0 = C_SO_REQHFNC(F0)        ! Handle sur la fonction
//         IERR = C_SO_CBFNC0(CB, HF0)   ! Appel de F0 via HF0
//      RETURN
//      END
//    </pre>
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC0 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*),
                                                           hndl_t* fptr)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr));
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC1</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 1 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC1 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC2</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 2 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC2 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC3</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 3 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC3 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC4</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 4 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC4 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3,
                                                           fint_t* p4)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3, p4);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC5</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 5 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC5 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3,
                                                           fint_t* p4,
                                                           fint_t* p5)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3, p4, p5);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC6</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 6 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC6 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3,
                                                           fint_t* p4,
                                                           fint_t* p5,
                                                           fint_t* p6)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3, p4, p5, p6);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC7</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 7 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC7 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3,
                                                           fint_t* p4,
                                                           fint_t* p5,
                                                           fint_t* p6,
                                                           fint_t* p7)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3, p4, p5, p6, p7);
   return ierr;
}

//************************************************************************
// Sommaire: Résout un handle de fonction.
//
// Description:
//    La fonction <code>C_SO_CBFNC8</code> appelle la fonction <code>cb</code>
//    en lui passant la fonction qui correspond au handle <code>fptr</code>
//    comme premier argument. Elle permet de traduire le handle.
//    Cette version est pour une fonction call-back à 8 arguments.
//    <p>
//    Le type des arguments ne doit pas être CHARACTER*(*) à cause de
//    l'ajout possible par le FORTRAN d'arguments cachés pour la longueur
//    de la chaîne.
//
// Entrée:
//    fint_t (*cb) (fint_t*);                   Fonction call-back
//    hndl_t* fptr;                             Handle sur la fonction
//    fint_t* p1,...,pn;                        Paramètres du call-back
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_SO_CBFNC8 (fint_t (F2C_CONF_CNV_APPEL *cb)(hndl_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*),
                                                           hndl_t* fptr,
                                                           fint_t* p1,
                                                           fint_t* p2,
                                                           fint_t* p3,
                                                           fint_t* p4,
                                                           fint_t* p5,
                                                           fint_t* p6,
                                                           fint_t* p7,
                                                           fint_t* p8)
{
   fint_t ierr = 0;
   ierr = cb(reinterpret_cast<hndl_t*>(*fptr), p1, p2, p3, p4, p5, p6, p7, p8);
   return ierr;
}

#ifdef __cplusplus
}
#endif
