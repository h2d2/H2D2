//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Fonction C de gestion de mémoire
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_MM_H_DEJA_INCLU
#define C_MM_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define C_MM_CTR         F2C_CONF_DECOR_FNC(C_MM_CTR,       c_mm_ctr)
#define C_MM_DTR         F2C_CONF_DECOR_FNC(C_MM_DTR,       c_mm_dtr)
#define C_MM_ALLOC       F2C_CONF_DECOR_FNC(C_MM_ALLOC,     c_mm_alloc)
#define C_MM_REALL       F2C_CONF_DECOR_FNC(C_MM_REALL,     c_mm_reall) 
#define C_MM_FREE        F2C_CONF_DECOR_FNC(C_MM_FREE,      c_mm_free)
#define C_MM_INCREF      F2C_CONF_DECOR_FNC(C_MM_INCREF,    c_mm_incref)
#define C_MM_DECREF      F2C_CONF_DECOR_FNC(C_MM_DECREF,    c_mm_decref)
#define C_MM_EXIST       F2C_CONF_DECOR_FNC(C_MM_EXIST,     c_mm_exist)
#define C_MM_REQLEN      F2C_CONF_DECOR_FNC(C_MM_REQLEN,    c_mm_reqlen)
#define C_MM_REQTYP      F2C_CONF_DECOR_FNC(C_MM_REQTYP,    c_mm_reqtyp)
#define C_MM_REQDTA      F2C_CONF_DECOR_FNC(C_MM_REQDTA,    c_mm_reqdta)
#define C_MM_ADDPTR      F2C_CONF_DECOR_FNC(C_MM_ADDPTR,    c_mm_addptr)
#define C_MM_REMPTR      F2C_CONF_DECOR_FNC(C_MM_REMPTR,    c_mm_remptr)
#define C_MM_REQINDBYT   F2C_CONF_DECOR_FNC(C_MM_REQINDBYT, c_mm_reqindbyt)
#define C_MM_REQINDIN4   F2C_CONF_DECOR_FNC(C_MM_REQINDIN4, c_mm_reqindin4)
#define C_MM_REQINDIN8   F2C_CONF_DECOR_FNC(C_MM_REQINDIN8, c_mm_reqindin8)
#define C_MM_REQINDRE8   F2C_CONF_DECOR_FNC(C_MM_REQINDRE8, c_mm_reqindre8)
#define C_MM_CHKCRC32    F2C_CONF_DECOR_FNC(C_MM_CHKCRC32,  c_mm_chkcrc32)
#define C_MM_CLCCRC32    F2C_CONF_DECOR_FNC(C_MM_CLCCRC32,  c_mm_clccrc32)
#define C_MM_TEST        F2C_CONF_DECOR_FNC(C_MM_TEST,      c_mm_test)
#define C_MM_REQINTSIZ   F2C_CONF_DECOR_FNC(C_MM_REQINTSIZ, c_mm_reqintsiz)
#define C_MM_CHKINTSIZ   F2C_CONF_DECOR_FNC(C_MM_CHKINTSIZ, c_mm_chkintsiz)
#define C_MM_CHKHDLSIZ   F2C_CONF_DECOR_FNC(C_MM_CHKHDLSIZ, c_mm_chkhdlsiz)


F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_CTR      ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_DTR      (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_ALLOC    (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REALL    (hndl_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_FREE     (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_INCREF   (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_DECREF   (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_EXIST    (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQLEN   (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQTYP   (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQDTA   (hndl_t*, fint_t*, void**);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_ADDPTR   (hndl_t*, void*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REMPTR   (hndl_t*, void*);
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDBYT(hndl_t*, fint_t*, char*);
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDIN4(hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDIN8(hndl_t*, fint_t*, hndl_t*);
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_MM_REQINDRE8(hndl_t*, fint_t*, double*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKCRC32 (hndl_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CLCCRC32 (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_TEST     (hndl_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_REQINTSIZ();
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKINTSIZ(fint_t*, fint_t*);
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_MM_CHKHDLSIZ(hndl_t*, hndl_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_MM_H_DEJA_INCLU
