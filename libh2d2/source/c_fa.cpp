//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Groupe:  fonctions C
// Sous-Groupe:  Fichier ASCII
// Sommaire: Fonctions C de gestion de Fichiers ASCII
//************************************************************************
#include "c_fa.h"

//#include <fcntl.h>
#include <stdio.h>
#include <string.h>
//#include <sys/stat.h>

#if defined(__GNUC__)
#  include <unistd.h>
#endif

#if defined(_MSC_VER)
#  pragma warning( push )
#  pragma warning( disable : 1195 )  // conversion from integer to smaller pointer
#  pragma warning( disable : 1684 )  // conversion from pointer to same-size integral type
#endif

//************************************************************************
// Sommaire:   Ouvre un fichier ASCII
//
// Description:
//    La fonction C_FA_OUVRE(...) ouvre le fichier ASCII suivant le mode
//    demandé. En cas de succès, elle retourne un Handle sur le fichier
//    qui pourra être utilisé dans d'autres appels. En cas d'erreur, la
//    fonction retourne 0.
//
// Entrée:
//    WatForStr* fic;      Nom du fichier
//    fint_t*    mode;     Mode d'ouverture
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FA_OUVRE(F2C_CONF_STRING  fic,
                                                         fint_t*  mode
                                                         F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT hndl_t F2C_CONF_CNV_APPEL C_FA_OUVRE(F2C_CONF_STRING  fic,
                                                         fint_t*  mode)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* fP = fic;
   int   l  = len;
#else
   char* fP = fic->strP;
   int   l  = fic->len;
#endif

   enum Mode {MODE_LECTURE = 1, MODE_ECRITURE= 2};

   FILE* fHandle = NULL;

   if (l < 1024)
   {
      char nom[1024];
      strncpy(nom, fP, l);
      nom[l] = 0x0;

      switch (*mode)
      {
         case MODE_LECTURE:
            fHandle = fopen(nom, "rt");
         break;
         case MODE_ECRITURE:
            fHandle = fopen(nom, "a+t");
         break;
      }
   }

   return reinterpret_cast<hndl_t>(fHandle);
}

//************************************************************************
// Sommaire: Ferme le fichier ASCII.
//
// Description:
//    La fonction C_FA_FERME(...) ferme le fichier ASCII dont le Handle est
//    passé en argument. La fonction retourne -1 en cas d'erreur, sinon 0.
//
// Entrée:
//    hndl_t*  handle;      Handle du fichier
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_FERME (hndl_t* handle)
{
   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   return (fclose(fHandle) == 0) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Écris une ligne dans un fichier ASCII.
//
// Description:
//    La fonction C_FA_ECRISLN(...) écris une ligne dans le fichier ASCII dont
//    le handle est passé en argument. La fonction retourne -1 en cas d'erreur,
//    sinon 0.
//
// Entrée:
//    hndl_t*    handle;      Handle du fichier
//    WatForStr* buf;         Ligne à écrire
//
// Sortie:
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_ECRISLN (hndl_t* handle,
                                                            F2C_CONF_STRING  buf
                                                            F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_ECRISLN (hndl_t* handle,
                                                            F2C_CONF_STRING  buf)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* bP = buf;
   int   bl = len;
#else
   char* bP = buf->strP;
   int   bl = buf->len;
#endif

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   size_t n = fwrite(bP, bl, 1, fHandle);
   return (n == 1) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Lis une ligne d'un fichier ASCII.
//
// Description:
//    La fonction C_FA_LISLN(...) lis une ligne dans le fichier ASCII dont
//    le handle est passé en argument. La fonction retourne:
//        0 en cas de succès,
//       -1 si la fin de fichier est atteinte,
//       -2 en cas d'erreur,
//       -3 si le buffer est trop petit pour contenir toute la ligne
//
// Entrée:
//    hndl_t*  handle;      Handle du fichier
//
// Sortie:
//    buf;         Ligne lue
//
// Notes:
//
//************************************************************************
#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISLN (hndl_t* handle,
                                                          F2C_CONF_STRING  buf
                                                          F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISLN (hndl_t* handle,
                                                          F2C_CONF_STRING  buf)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* bP = buf;
   int   bl = len;
#else
   char* bP = buf->strP;
   int   bl = buf->len;
#endif

   fint_t ret = 0;

   memset(bP, ' ', bl);
   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   char* rP = fgets (bP, bl, fHandle);
   if (rP)
   {
      const size_t blen = strlen(bP);
      if (blen+1 >= bl) ret = -3;                              // buf to small
      if (blen > 1 && bP[blen-2] == '\r') bP[blen-2] = ' ';    // DOS cr-lf
      if (blen > 0 && bP[blen-1] == '\n') bP[blen-1] = ' ';
      bP[blen] = ' ';
   }
   else if (feof(fHandle))
      ret = -1;
   else
      ret = -2;

   return ret;
}

//************************************************************************
// Sommaire:   Lis une table de double d'un fichier ASCII.
//
// Description:
//    La fonction C_FA_LISDBL(...) lis une table de double du fichier dont
//    le handle est passé en argument. La fonction retourne:
//        0 en cas de succès,
//       -1 si la fin de fichier est atteinte,
//       -2 en cas d'erreur,
//
// Entrée:
//    hndl_t*  handle;     Handle du fichier
//    fint_t*  n;          Nombre de valeur à lire
//
// Sortie:
//    double*  v;          Valeurs lues
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISDBL(hndl_t* handle,
                                                          double* v,
                                                          fint_t* n)
{
   fint_t ret = 1;

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   switch (*n)
   {
      case 1: ret = fscanf(fHandle, "%lf", v+0); break;
      case 2: ret = fscanf(fHandle, "%lf %lf", v+0, v+1); break;
      case 3: ret = fscanf(fHandle, "%lf %lf %lf", v+0, v+1, v+2); break;
      default:
         for (fint_t i = 0; ret == 1 && i < *n; ++i)
            ret = fscanf(fHandle, "%lf", v + i);
   }
   if (ret == *n || ret == 1)
      ret = 0;
   else if (feof(fHandle))
      ret = -1;
   else
      ret = -2;

   return ret;
}

//************************************************************************
// Sommaire:   Lis une table de double d'un fichier ASCII.
//
// Description:
//    La fonction C_FA_LISDBL(...) lis une table de double du fichier dont
//    le handle est passé en argument. La fonction retourne:
//        0 en cas de succès,
//       -1 si la fin de fichier est atteinte,
//       -2 en cas d'erreur,
//
// Entrée:
//    hndl_t*  handle;     Handle du fichier
//    fint_t*  n;          Nombre de valeur à lire
//
// Sortie:
//    double*  v;          Valeurs lues
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_LISINT(hndl_t* handle,
                                                          fint_t* v,
                                                          fint_t* n)
{
   fint_t ret = 1;

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   switch (*n)
   {
      case 1: ret = fscanf(fHandle, "%d", v+0); break;
      case 2: ret = fscanf(fHandle, "%d %d", v+0, v+1); break;
      case 3: ret = fscanf(fHandle, "%d %d %d", v+0, v+1, v+2); break;
      case 4: ret = fscanf(fHandle, "%d %d %d %d", v+0, v+1, v+2, v+3); break;
      case 5: ret = fscanf(fHandle, "%d %d %d %d %d", v+0, v+1, v+2, v+3, v+4); break;
      case 6: ret = fscanf(fHandle, "%d %d %d %d %d %d", v+0, v+1, v+2, v+3, v+4, v+5); break;
      default:
         for (fint_t i = 0; ret == 1 && i < *n; ++i)
            ret = fscanf(fHandle, "%d", v + i);
   }
   if (ret == *n || ret == 1)
      ret = 0;
   else if (feof(fHandle))
      ret = -1;
   else
      ret = -2;

   return ret;
}

//************************************************************************
// Sommaire:   Saute n lignes d'un fichier ASCII.
//
// Description:
//    La fonction C_FA_SKIPN(...) saute n lignes dans le fichier ASCII dont
//    le handle est passé en argument. La fonction retourne 0 en cas de succès,
//    -1 si la fin de fichier est atteinte, et -2 en cas d'erreur,
//
// Entrée:
//    hndl_t*  handle;      Handle du fichier
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_SKIPN(hndl_t* handle,
                                                         fint_t* n)
{
   fint_t ret = 0;

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);

   const int BUF_SIZE = 2048;
   char buf[BUF_SIZE];
   char* bP = buf;
   for (fint_t i = 0; i < *n && bP; ++i)
      bP = fgets (buf, BUF_SIZE, fHandle);

   if (bP)
      ret = 0;
   else if (feof(fHandle))
      ret = -1;
   else
      ret = -2;

   return ret;
}

//************************************************************************
// Sommaire: Positionne le fichier ASCII.
//
// Description:
//    La fonction C_FA_ASGPOS(...) positionne le fichier ASCII, dont le handle
//    est passé en argument, à la position demandée. La position doit être
//    le résultat d'un appel antérieur à C_FA_REQPOS(...) pour le même fichier.
//    La fonction retourne -1 en cas d'erreur, sinon 0.
//
// Entrée:
//    hndl_t*  handle;      Handle du fichier
//    int64_t* p;           Position dans le fichier
//
// Sortie:
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_ASGPOS (hndl_t*  handle,
                                                           int64_t* p)
{

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   long offset = static_cast<long>(*p);
   int ierr = fseek (fHandle, offset, SEEK_SET);

   return (ierr == 0) ? 0 : -1;
}

//************************************************************************
// Sommaire:   Donne la position dans le fichier ASCII.
//
// Description:
//    La fonction C_FA_REQPOS(...) retourne la position actuelle dans le fichier
//    ASCII dont le handle est passé en argument. La position peut servir dans
//    un appel à C_FA_ASGPOS(...) pour le même fichier.
//    La fonction retourne -1 en cas d'erreur, sinon 0.
//
// Entrée:
//    hndl_t*  handle;      Handle du fichier
//
// Sortie:
//    int64_t*  p;          Position dans le fichier
//
// Notes:
//
//************************************************************************
F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL C_FA_REQPOS (hndl_t*  handle,
                                                           int64_t* p)
{

   FILE* fHandle = reinterpret_cast<FILE*>(*handle);
   *p = ftell(fHandle);

   return (*p != -1) ? 0 : -1;
}

#if defined(_MSC_VER)
#  pragma warning( pop )
#endif
