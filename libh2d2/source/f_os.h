//************************************************************************
// --- Copyright (c) INRS 2003-2017
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Contrôle du coprocesseur mathématique
//
// Description:
//    Fonctions Fortran du Système d'exploitation (Operating System)
//
// Notes:
//
//************************************************************************
#ifndef F_OS_H_DEJA_INCLU
#define F_OS_H_DEJA_INCLU

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define F_OS_NARGLC    F2C_CONF_DECOR_FNC(F_OS_NARGLC, f_os_narglc)
#define F_OS_IARGLC    F2C_CONF_DECOR_FNC(F_OS_IARGLC, f_os_iarglc)


F2C_CONF_DLL_EXPORT fint_t F2C_CONF_CNV_APPEL F_OS_NARGLC  ();
F2C_CONF_DLL_EXPORT void   F2C_CONF_CNV_APPEL F_OS_IARGLC  (fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);


#ifdef __cplusplus
}
#endif

#endif   // F_OS_H_DEJA_INCLU
