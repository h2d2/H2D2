//************************************************************************
// H2D2 - External declaration of public symbols
// Module: lmgo_l2
// Entry point: extern "C" void fake_dll_lmgo_l2()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:02.520000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class LM_GOL2
F_PROT(LM_GOL2_000, lm_gol2_000);
F_PROT(LM_GOL2_999, lm_gol2_999);
F_PROT(LM_GOL2_CTR, lm_gol2_ctr);
F_PROT(LM_GOL2_DTR, lm_gol2_dtr);
F_PROT(LM_GOL2_INI, lm_gol2_ini);
F_PROT(LM_GOL2_RST, lm_gol2_rst);
F_PROT(LM_GOL2_REQHBASE, lm_gol2_reqhbase);
F_PROT(LM_GOL2_HVALIDE, lm_gol2_hvalide);
F_PROT(LM_GOL2_CLCJELV, lm_gol2_clcjelv);
 
// ---  class LMGO_L2
F_PROT(LMGO_L2_ASGCMN, lmgo_l2_asgcmn);
F_PROT(LMGO_L2_ASMESCL, lmgo_l2_asmescl);
F_PROT(LMGO_L2_CLCJELS, lmgo_l2_clcjels);
F_PROT(LMGO_L2_CLCJELV, lmgo_l2_clcjelv);
F_PROT(LMGO_L2_REQNFN, lmgo_l2_reqnfn);
F_PROT(LMGO_L2_REQPRM, lmgo_l2_reqprm);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_lmgo_l2()
{
   static char libname[] = "lmgo_l2";
 
   // ---  class LM_GOL2
   F_RGST(LM_GOL2_000, lm_gol2_000, libname);
   F_RGST(LM_GOL2_999, lm_gol2_999, libname);
   F_RGST(LM_GOL2_CTR, lm_gol2_ctr, libname);
   F_RGST(LM_GOL2_DTR, lm_gol2_dtr, libname);
   F_RGST(LM_GOL2_INI, lm_gol2_ini, libname);
   F_RGST(LM_GOL2_RST, lm_gol2_rst, libname);
   F_RGST(LM_GOL2_REQHBASE, lm_gol2_reqhbase, libname);
   F_RGST(LM_GOL2_HVALIDE, lm_gol2_hvalide, libname);
   F_RGST(LM_GOL2_CLCJELV, lm_gol2_clcjelv, libname);
 
   // ---  class LMGO_L2
   F_RGST(LMGO_L2_ASGCMN, lmgo_l2_asgcmn, libname);
   F_RGST(LMGO_L2_ASMESCL, lmgo_l2_asmescl, libname);
   F_RGST(LMGO_L2_CLCJELS, lmgo_l2_clcjels, libname);
   F_RGST(LMGO_L2_CLCJELV, lmgo_l2_clcjelv, libname);
   F_RGST(LMGO_L2_REQNFN, lmgo_l2_reqnfn, libname);
   F_RGST(LMGO_L2_REQPRM, lmgo_l2_reqprm, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
