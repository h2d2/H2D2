C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction LMGO_L2_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION LMGO_L2_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_L2_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'lmgo_l2.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'err.fi'

      INTEGER        I
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
C-----------------------------------------------------------------------

      IF (IFNC .EQ. EG_FUNC_INDETERMINE) THEN
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ELSEIF (IFNC .EQ. EG_FUNC_ASMESCL) THEN
                                    NF = '---'
      ELSEIF (IFNC .EQ. EG_FUNC_ASMPEAU) THEN
                                    NF = 'LMGO_L2_ASMPEAU@lmgo_l2'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCJELV) THEN
                                    NF = 'LMGO_L2_CLCJELV@lmgo_l2'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCJELS) THEN
                                    NF = '---'
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ENDIF

      IF (ERR_GOOD()) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

      LMGO_L2_REQNFN = ERR_TYP()
      RETURN
      END
