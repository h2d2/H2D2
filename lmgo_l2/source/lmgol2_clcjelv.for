C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOL2
C         FTN (Sub)Module: LM_GOL2_CLCJELV_M
C            Public:
C            Private:
C               MODULE INTEGER LM_GOL2_CLCJELV
C
C************************************************************************

      SUBMODULE(LM_GOL2_M) LM_GOL2_CLCJELV_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  LM_GOL2_CLCJELV
C
C Description:
C     La fonction LM_GOL2_CLCJELV calcule les métriques pour des
C     éléments de volume de type L2.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOL2_CLCJELV(SELF)

      CLASS(LM_GOL2_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      REAL*8,  DIMENSION(:,:), POINTER :: VDJV
      REAL*8,  DIMENSION(:,:), POINTER :: VCORG
      INTEGER IE
      INTEGER NO1,  NO2
      INTEGER NCELV, NELLV

      REAL*8, PARAMETER :: ZERO = 0.0D0
C-----------------------------------------------------------------------

      CALL LOG_TODO('LM_GOL2_CLCJELV   jamais teste, SUREMENT FAUX!!!')
      CALL ERR_ASR(.FALSE.)

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 3)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 3)
      NELLV = GDTA%NNELV
      NCELV = GDTA%NCELV
      KNGV  => GDTA%KNGV
      VCORG => GDTA%VCORG
      VDJV  => GDTA%VDJV

C---     Boucle sur les éléments
      DO IE=1,NELLV
         NO1  = KNGV(1,IE)
         NO2  = KNGV(2,IE)
         VDJV(1,IE) = VCORG(2,NO1)
         VDJV(2,IE) = VCORG(2,NO1)

C---        Imprime les éléments à déterminant négatif
         IF (VDJV(2,IE) .LE. ZERO) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_DETJ_NEGATIF'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
            WRITE(LOG_BUF, '(A,I9)') 'ERR_DETJ_NEGATIF:', IE
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDDO

      LM_GOL2_CLCJELV = ERR_TYP()
      RETURN
      END FUNCTION LM_GOL2_CLCJELV

      END SUBMODULE LM_GOL2_CLCJELV_M
