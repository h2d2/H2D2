C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C
C Description :
C     Fournit les routines pour obtenir les point de Gauss
C     et les poids associés pour un élément P12L.
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Le bloc de données NI_IP2P1 initialise les point de Gauss et les
C     poids associés pour l'intégration sur un P12. C'est une combinaison
C     d'une intégration à 1 point sur chaque sous-triangle pour l'horizontal,
C     et à 1 point sur la verticale.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Dhatt, Touzot, Lefrançois, p 352,367.
C    17 septembre 2006 : les poids sont modifiés pour 1/4 plutôt que 1/2
C                        afin que les test d'intégrations soient valides.
C                        Considérer la possibilité de conserver les poids a
C                        1/2 et plutôt modifier l'intégration.
C************************************************************************
      BLOCK DATA NI_IP2P1

      IMPLICIT NONE

      INCLUDE 'ni_ip2p1.fc'

      REAL*8 ZERO
      REAL*8 R_1_6
      REAL*8 R_2_6
      REAL*8 R_4_6
      REAL*8 R_1_4

      PARAMETER (ZERO  = 0.0000000000000000000D+00,
     &           R_1_6 = 1.0D-00 / 6.0D-00,
     &           R_2_6 = 2.0D-00 / 6.0D-00,
     &           R_4_6 = 4.0D-00 / 6.0D-00,
     &           R_1_4 = 0.2500000000000000000D+00)
C------------------------------------------------------------------------

      DATA NI_IP2P1_CPG
     &     /
     &     R_1_6, R_1_6, ZERO,
     &     R_4_6, R_1_6, ZERO,
     &     R_1_6, R_4_6, ZERO,
     &     R_2_6, R_2_6, ZERO
     &     /

      DATA NI_IP2P1_WPG
     &      /
     &       R_1_4,
     &       R_1_4,
     &       R_1_4,
     &       R_1_4
     &      /

      END

C************************************************************************
C Sommaire: NI_IP2P1_REQNPG
C
C Description:
C     La fonction NI_IP2P1_REQNPTS retourne le nombre de points de Gauss.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NI_IP2P1_REQNPG()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_IP2P1_REQNPG
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ni_ip2p1.fi'
      INCLUDE 'ni_ip2p1.fc'
C------------------------------------------------------------------------

      NI_IP2P1_REQNPG = NI_IP2P1_NPG
      RETURN
      END

C************************************************************************
C Sommaire: NI_IP2P1_REQCPG
C
C Description:
C     La fonction NI_IP2P1_REQCPG(...) retourne les coordonnées du point
C     d'intégration d'indice "I".
C
C Entrée:
C     I    : indice du point pour lequel on souhaite obtenir la coordonnée
C     NDIM : nombre de dimensions attendues pour la coordonnée
C
C Sortie:
C     VCPG : Coordonnée du point sous la forme (xi, eta, zeta)
C
C
C Notes:
C************************************************************************
      FUNCTION  NI_IP2P1_REQCPG(I, NDIM, VCPG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_IP2P1_REQCPG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      INTEGER NDIM
      REAL*8  VCPG(NDIM)

      INCLUDE 'ni_ip2p1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_ip2p1.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_IP2P1_NPG)
D     CALL ERR_PRE(NDIM .GE. NI_IP2P1_NDIM)
C------------------------------------------------------------------------

      VCPG(1) = NI_IP2P1_CPG(1,I)
      VCPG(2) = NI_IP2P1_CPG(2,I)
      VCPG(3) = NI_IP2P1_CPG(3,I)

      NI_IP2P1_REQCPG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NI_IP2P1_REQWPG(W,I)
C
C Description:
C     La fonction NI_IP2P1_REQWPG retourne le poids du point I
C     pour effectuer l'intégration numérique.
C
C Entrée:
C     I : indice du point d'intégration concerné
C
C Sortie:
C     W : poids du point d'intégration I
C
C Notes:
C************************************************************************
      FUNCTION NI_IP2P1_REQWPG(I, W)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NI_IP2P1_REQWPG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER I
      REAL*8  W

      INCLUDE 'ni_ip2p1.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ni_ip2p1.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(I .GE. 1 .AND. I .LE. NI_IP2P1_NPG)
C------------------------------------------------------------------------

      W = NI_IP2P1_WPG(I)

      NI_IP2P1_REQWPG = ERR_TYP()
      RETURN
      END

