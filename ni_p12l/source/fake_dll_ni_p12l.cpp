//************************************************************************
// H2D2 - External declaration of public symbols
// Module: ni_p12l
// Entry point: extern "C" void fake_dll_ni_p12l()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:03.364000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class NI_IP2P1
F_PROT(NI_IP2P1_REQNPG, ni_ip2p1_reqnpg);
F_PROT(NI_IP2P1_REQCPG, ni_ip2p1_reqcpg);
F_PROT(NI_IP2P1_REQWPG, ni_ip2p1_reqwpg);
 
// ---  class NI_IP2P3
F_PROT(NI_IP2P3_REQNPG, ni_ip2p3_reqnpg);
F_PROT(NI_IP2P3_REQCPG, ni_ip2p3_reqcpg);
F_PROT(NI_IP2P3_REQWPG, ni_ip2p3_reqwpg);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_ni_p12l()
{
   static char libname[] = "ni_p12l";
 
   // ---  class NI_IP2P1
   F_RGST(NI_IP2P1_REQNPG, ni_ip2p1_reqnpg, libname);
   F_RGST(NI_IP2P1_REQCPG, ni_ip2p1_reqcpg, libname);
   F_RGST(NI_IP2P1_REQWPG, ni_ip2p1_reqwpg, libname);
 
   // ---  class NI_IP2P3
   F_RGST(NI_IP2P3_REQNPG, ni_ip2p3_reqnpg, libname);
   F_RGST(NI_IP2P3_REQCPG, ni_ip2p3_reqcpg, libname);
   F_RGST(NI_IP2P3_REQWPG, ni_ip2p3_reqwpg, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
