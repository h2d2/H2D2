C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOT6L
C         FTN (Sub)Module: LM_GOT6L_CLCSPLT_M
C            Public:
C               MODULE INTEGER LM_GOT6L_CLCSPLT
C            Private:
C
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_CLCSPLT_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  LM_GOT6L_CLCSPLT
C
C Description:
C     La fonction LM_GOT6L_CLCSPLT divise les éléments T6L en
C     éléments T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER
     &FUNCTION LM_GOT6L_CLCSPLT(SELF, KNGZ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_CLCSPLT
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
      INTEGER, INTENT(OUT) :: KNGZ(:, :)

      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER IEZ, IEV
C----------------------------------------------------------------------
D     CALL ERR_PRE(SIZE(KNGZ, 1) .GE. 3)
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_ASR(SIZE(KNGZ, 2) .EQ. GDTA%NELLV*4)

C---     Boucle sur les éléments
      IEZ = 0
      DO IEV=1,GDTA%NELLV
         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(1,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(2,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(6,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(2,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(3,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(4,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(6,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(4,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(5,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(4,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(6,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(2,IEV)
      ENDDO

      LM_GOT6L_CLCSPLT = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_CLCSPLT

      END SUBMODULE LM_GOT6L_CLCSPLT_M

