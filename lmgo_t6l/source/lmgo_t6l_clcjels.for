C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_CLCJELS
C
C Description:
C     La fonction LMGO_T6L_CLCJELS calcule les métriques de pour les
C     éléments de surface de type L3L, surface d'éléments T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_CLCJELS(NDIM,
     &                          NNT,
     &                          NCELV,
     &                          NCELS,
     &                          NELV,
     &                          NELS,
     &                          NDJV,
     &                          NDJS,
     &                          VCORG,
     &                          KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCJELS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNT
      INTEGER NCELV
      INTEGER NCELS
      INTEGER NELV
      INTEGER NELS
      INTEGER NDJV
      INTEGER NDJS
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNGV (NCELV,NELV)
      INTEGER KNGS (NCELS,NELS)
      REAL*8  VDJV (NDJV, NELV)
      REAL*8  VDJS (NDJS, NELS)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      REAL*8  UN_2
      PARAMETER (UN_2 = 5.0000000000000000000D-01)

      REAL*8  TX, TY, TL
      REAL*8  TXN, TYN
      INTEGER IEP, IE
      INTEGER NP1, NP3, NO1
C-----------------------------------------------------------------------

      DO 10 IEP=1,NELS
         NP1 = KNGS(1,IEP)
         NP3 = KNGS(3,IEP)
         IE  = KNGS(4,IEP)
         NO1 = KNGV(1,IE)
         IF (NP1 .EQ. NO1) THEN            ! COTE 1-3
            TX =   VDJV(4,IE)
            TY = - VDJV(2,IE)
         ELSEIF (NP3 .EQ. NO1) THEN        ! COTE 5-1
            TX =   VDJV(3,IE)
            TY = - VDJV(1,IE)
         ELSE                              ! COTE 3-5
            TX = - (VDJV(3,IE) + VDJV(4,IE))
            TY =   (VDJV(1,IE) + VDJV(2,IE))
         ENDIF
         TL  = HYPOT(TX, TY)
         TXN = TX/TL
         TYN = TY/TL
         VDJS(1,IEP) = TXN
         VDJS(2,IEP) = TYN
         VDJS(3,IEP) = UN_2*TL
10    CONTINUE

      LMGO_T6L_CLCJELS = ERR_TYP()
      RETURN
      END

