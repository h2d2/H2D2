C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T6L_REQNFN
C   Private:
C
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Retourne un nom de fonction
C
C Description:
C     La fonction LMGO_T6L_REQNFN retourne le nom de la fonction
C     associé au code demandé. Le nom combine le nom de la DLL et
C     de la fonction sous la forme "nomFonction@nomDll".
C
C Entrée:
C     IFNC     Code de la fonction
C
C Sortie:
C     KFNC     Nom de la fonction stocké dans une table INTEGER
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_REQNFN(KFNC, IFNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_REQNFN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER KFNC(*)
      INTEGER IFNC

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'egfunc.fi'
      INCLUDE 'err.fi'

      INTEGER        I
      INTEGER        JF(16)
      CHARACTER*(64) NF
      EQUIVALENCE (NF, JF)
C-----------------------------------------------------------------------

      IF (IFNC .EQ. EG_FUNC_INDETERMINE) THEN
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ELSEIF (IFNC .EQ. EG_FUNC_ASMESCL) THEN
                                    NF = 'LMGO_T6L_ASMESCL@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_ASMPEAU) THEN
                                    NF = 'LMGO_T6L_ASMPEAU@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCERR) THEN
                                    NF = 'LMGO_T6L_CLCERR@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCJELV) THEN
                                    NF = 'LMGO_T6L_CLCJELV@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCJELS) THEN
                                    NF = 'LMGO_T6L_CLCJELS@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_LCLELV) THEN
                                    NF = 'LMGO_T6L_LCLELE@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_INTRP) THEN
                                    NF = 'LMGO_T6L_INTRP@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_CLCSPLIT) THEN
                                    NF = 'LMGO_T6L_CLCSPLIT@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_REQITPEZ) THEN
                                    NF = 'LMGO_T6L_REQITPEZ@lmgo_t6l'
      ELSEIF (IFNC .EQ. EG_FUNC_REQNELZ) THEN
                                    NF = 'LMGO_T6L_REQNELZ@lmgo_t6l'
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_CODE_FNCT_INVALIDE')
      ENDIF

      IF (ERR_GOOD()) THEN
         DO I=1,16
            KFNC(I) = JF(I)
         ENDDO
      ENDIF

      LMGO_T6L_REQNFN = ERR_TYP()
      RETURN
      END
