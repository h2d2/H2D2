C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LM_GOT6L_CLCSPLT
C
C Description:
C     La fonction LM_GOT6L_CLCSPLT divise les éléments T6L en
C     éléments T3.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GOT6L_CLCSPLT(HOBJ,
     &                          NCELZ,
     &                          NELZ,
     &                          KNGZ)
CDEC$ATTRIBUTES DLLEXPORT :: LM_GOT6L_CLCSPLT

      USE LM_GEOM_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCELZ
      INTEGER NELZ
      INTEGER KNGZ (NCELZ, NELZ)

      INCLUDE 'lmgot6l.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_DATA_T), POINTER :: GDTA
      INTEGER IEZ, IEV
C----------------------------------------------------------------------
D     CALL ERR_PRE(NCELZ .GE. 3)
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => LM_GEOM_REQPDTA(HOBJ)
D     CALL ERR_ASR(NELZ .EQ. GDTA%NELLV*4)

C---     Boucle sur les éléments
      IEZ = 0
      DO IEV=1,GDTA%NELLV
         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(1,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(2,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(6,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(2,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(3,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(4,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(6,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(4,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(5,IEV)

         IEZ = IEZ + 1
         KNGZ(1,IEZ) = GDTA%KNGV(4,IEV)
         KNGZ(2,IEZ) = GDTA%KNGV(6,IEV)
         KNGZ(3,IEZ) = GDTA%KNGV(2,IEV)
      ENDDO

      LM_GOT6L_CLCSPLT = ERR_TYP()
      RETURN
      END
