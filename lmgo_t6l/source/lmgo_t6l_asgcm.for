C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Assemble
C
C Description:
C     La fonction LMGO_T6L_ASGCMN copie les valeurs du common externe
C     passés en paramètre sous la forme d'un vecteur sur les valeurs
C     du common local à la DLL.
C
C Entrée:
C     KA       Les valeurs du common
C
C Sortie:
C
C Notes:
C     les common(s) sont privés au DLL, il faut donc propager les
C     changement entre l'externe et l'interne.
C
C************************************************************************
      FUNCTION LMGO_T6L_ASGCMN(KA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASGCMN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER LMGO_T6L_ASGCMN
      INTEGER KA(*)

      INCLUDE 'err.fi'
      INCLUDE 'egcmmn.fc'

      INTEGER I
C-----------------------------------------------------------------------

      DO I=1, EG_CMMN_KA_DIM
         EG_CMMN_KA(I) = KA(I)
      ENDDO

      LMGO_T6L_ASGCMN = ERR_TYP()
      RETURN
      END
