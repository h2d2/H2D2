C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_ASMPEAU
C
C Description:
C     La fonction LMGO_T6L_ASMPEAU assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ASMPEAU(NCELV,
     &                          NELV,
     &                          KNGV,
     &                          NNT,
     &                          KPNT,
     &                          NLIEN,
     &                          KLIEN,
     &                          INEXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASMPEAU
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      INFO(2) = 0
      DO IE=1,NELV

C---        SIDE 1 -- NODES 1-3
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         CALL LOG_TODO('LMGO_T6L_ASMPEAU : NVLNK used before set')
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 3-5
         IF (KNGV(2,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 5-1
         IF (KNGV(3,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LMGO_T6L_ASMPEAU = ERR_TYP()
      RETURN
      END

