C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_CLCSPLIT
C
C Description:
C     La fonction LMGO_T6L_CLCSPLIT assemble la peau pour des
C     éléments de volume de type T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_CLCSPLIT(NCELV,
     &                           NELV,
     &                           KNGV,
     &                           NCELS,
     &                           NELS,
     &                           KNGS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCSPLIT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV, NELV)
      INTEGER NCELS
      INTEGER NELS
      INTEGER KNGS (NCELS, NELS)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IES, IEV
C----------------------------------------------------------------------
D     CALL ERR_ASR(NCELS .GE. 3)
D     CALL ERR_ASR(NELS  .EQ. NELV*4)
C----------------------------------------------------------------------

      IES = 0
      DO IEV=1,NELV
         IES = IES + 1
         KNGS(1,IES) = KNGV(1,IEV)
         KNGS(2,IES) = KNGV(2,IEV)
         KNGS(3,IES) = KNGV(6,IEV)

         IES = IES + 1
         KNGS(1,IES) = KNGV(2,IEV)
         KNGS(2,IES) = KNGV(3,IEV)
         KNGS(3,IES) = KNGV(4,IEV)

         IES = IES + 1
         KNGS(1,IES) = KNGV(6,IEV)
         KNGS(2,IES) = KNGV(4,IEV)
         KNGS(3,IES) = KNGV(5,IEV)

         IES = IES + 1
         KNGS(1,IES) = KNGV(4,IEV)
         KNGS(2,IES) = KNGV(6,IEV)
         KNGS(3,IES) = KNGV(2,IEV)
      ENDDO

      LMGO_T6L_CLCSPLIT = ERR_TYP()
      RETURN
      END
