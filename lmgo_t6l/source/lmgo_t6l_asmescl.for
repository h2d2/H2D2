C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

      ! GCC ne défini pas FINDLOC
      MODULE FINDLOC_M

      INTERFACE FINDLOC
         MODULE PROCEDURE FINDLOC_L
         MODULE PROCEDURE FINDLOC_I
      END INTERFACE FINDLOC

      CONTAINS

      !-----------------------------------------------------------
      !-----------------------------------------------------------
      FUNCTION FINDLOC_L(ARRAY, VALUE) RESULT(RES)

      INTEGER, DIMENSION(1) :: RES
      LOGICAL, INTENT(IN)   :: ARRAY(:)
      LOGICAL, INTENT(IN)   :: VALUE
      INTEGER I

      DO I=1,SIZE(ARRAY)
         IF (ARRAY(I) .EQV. VALUE) EXIT
      ENDDO

      IF (I > SIZE(ARRAY)) I = 0
      RES = (/ I /)
      RETURN
      END FUNCTION FINDLOC_L

      !-----------------------------------------------------------
      !-----------------------------------------------------------
      FUNCTION FINDLOC_I(ARRAY, VALUE) RESULT(RES)

      INTEGER :: RES(1)
      INTEGER, INTENT(IN) :: ARRAY(:)
      INTEGER, INTENT(IN) :: VALUE
      INTEGER I

      DO I=1,SIZE(ARRAY)
         IF (ARRAY(I) == VALUE) EXIT
      ENDDO

      IF (I > SIZE(ARRAY)) I = 0
      RES = (/ I /)
      RETURN
      END FUNCTION FINDLOC_I

      END MODULE FINDLOC_M

C************************************************************************
C Sommaire:  LMGO_T6L_ASMESCL
C
C Description:
C     La fonction LMGO_T6L_ASMESCL assemble les éléments de surface
C     des conditions limites. Pour un élément L3L, il suffit que les
C     deux noeuds sommets d'un élément se trouvent dans la liste des
C     noeuds de CL pour que l'élément soit reconnu.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ASMESCL(VCORG,
     &                          KNGS,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_ASMESCL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR, IRET
      INTEGER ICL, ID1
      INTEGER NMAX, NNCL
      INTEGER, ALLOCATABLE :: KNCL(:)

      INTEGER LMGO_T6L_ASMESCL_0
      INTEGER LMGO_T6L_ASMESCL_1
      INTEGER LMGO_T6L_ASMESCL_2
      INTEGER LMGO_T6L_ASMESCL_3
C-----------------------------------------------------------------------

C---     Dimensionne les tables de travail
      NMAX = -1
      DO ICL=1, EG_CMMN_NCLLIM
         NNCL = KCLLIM(4,ICL) - KCLLIM(3,ICL) + 1
         NMAX = MAX(NMAX, NNCL)
      ENDDO
      IRET = 0
      IF (IRET .EQ. 0) ALLOCATE(KNCL(NMAX),   STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise les distances
      CALL DINIT(EG_CMMN_NCLNOD, 0.0D0, VCLDST, 1)

C---     Boucle sur les limites
      DO ICL=1, EG_CMMN_NCLLIM
         KNCL(:)   = -99
         NNCL = KCLLIM(4,ICL) - KCLLIM(3,ICL) + 1

C---        Détermine les éléments de la limite
         IF (ERR_GOOD())
     &      IERR = LMGO_T6L_ASMESCL_0(ICL, KNCL,
     &                             KNGS, KCLLIM, KCLNOD, KCLELE)

C---        Filtre l'élément de début
         ID1 = -1
         IF (ERR_GOOD())
     &      IERR = LMGO_T6L_ASMESCL_1(ID1, ICL, NNCL, KNCL, KCLLIM)

C---        Réordonne les éléments et les noeuds
         IF (ERR_GOOD() .AND. KCLLIM(7,ICL) .EQ. EG_TPLMT_1SGMT)
     &      IERR = LMGO_T6L_ASMESCL_2(ID1, ICL, KNCL,
     &                             KNGS, KCLLIM, KCLNOD, KCLELE)

C---        Calcule les distances relatives
         IF (ERR_GOOD() .AND. KCLLIM(7,ICL) .EQ. EG_TPLMT_1SGMT)
     &      IERR = LMGO_T6L_ASMESCL_3(ICL,
     &                             VCORG, KCLLIM, KCLNOD,
     &                             VCLDST)

         IF (ERR_ESTMSG('ERR_LIMITE_COMPLEXE')) THEN
            CALL ERR_RESET()
         ENDIF
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(KNCL)) DEALLOCATE(KNCL)
      LMGO_T6L_ASMESCL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_T6L_ASMESCL_0
C
C Description:
C     La fonction privée LMGO_T6L_ASMESCL_0 extrait les éléments de la
C     limite ICL en se basant sur les noeuds de cette limite.
C     Elle monte également la table KNCL qui associe à l'indice relatif de NO1
C     l'indice relatif de NO3 des l'élément de la limite. Les indices
C     sont relatifs au début de la table KCLNOD pour la limite.
C
C Entrée:
C
C Sortie:
C     KCLELE      Table des éléments des limites
C
C Notes:
C     Code KNCL:
C       -99    Absent
C       -22    Noeud 2 présent dans la liste
C         0    Noeuds fin de chaîne
C       > 0    Noeud 3
C************************************************************************
      FUNCTION LMGO_T6L_ASMESCL_0(ICL,
     &                            KNCL,
     &                            KNGS,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE)

      USE FINDLOC_M, ONLY: FINDLOC
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMESCL_0
      INTEGER ICL
      INTEGER KNCL  (*)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_COMM, I_ERROR

      INTEGER IEP, IE, IN
      INTEGER INOCLINF, INOCLSUP
      INTEGER NNOCL
      INTEGER IELCLINF
      INTEGER IELE
      INTEGER NP1, NP2, NP3, ID1, ID2, ID3, ID(1)
C-----------------------------------------------------------------------

      IF (ICL .EQ. 1) THEN
         IELE = 0
      ELSE
         IELE = KCLLIM(6,ICL-1)
      ENDIF

      KCLLIM(5,ICL) = IELE + 1

      INOCLINF = KCLLIM(3,ICL)
      INOCLSUP = KCLLIM(4,ICL)
      IELCLINF = KCLLIM(5,ICL)
      NNOCL = INOCLSUP-INOCLINF+1

C---     Boucle sur les éléments de surface
      DO IEP=1,EG_CMMN_NELS
         NP1 = KNGS(1,IEP)
         NP2 = KNGS(2,IEP)
         NP3 = KNGS(3,IEP)

         ! ---  Cherche les noeuds
         ID = FINDLOC(KCLNOD(INOCLINF:INOCLSUP), NP1); ID1 = ID(1)
         ID = FINDLOC(KCLNOD(INOCLINF:INOCLSUP), NP2); ID2 = ID(1)
         ID = FINDLOC(KCLNOD(INOCLINF:INOCLSUP), NP3); ID3 = ID(1)

         ! ---   Ajoute l'élément de limite
         IF (ID1 .GT. 0 .AND. ID3 .GT. 0) THEN
D           CALL ERR_ASR(ID1 .GE. 1 .AND. ID1 .LE. NNOCL)
D           CALL ERR_ASR(ID3 .GE. 1 .AND. ID3 .LE. NNOCL)
            ! ---  Contrôle les dédoublements
            IF (ANY(KCLELE(IELCLINF:IELE) .EQ. IEP)) GOTO 9900
            ! ---   Ajoute l'élément
            IELE = IELE + 1
            KCLELE(IELE) = IEP
            KNCL(ID1) = ID3
            IF (ID2 .GT. 0) KNCL(ID2) = -22
            KNCL(ID3) = MAX(KNCL(ID3), 0)    ! Fin de chaîne possible
         ENDIF
      ENDDO

      KCLLIM(6,ICL) = IELE
      KCLLIM(7,ICL) = EG_TPLMT_INDEFINI

C---     Réduis KNCL
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, KNCL, NNOCL, MP_TYPE_INT(),
     &                   MPI_MAX, I_COMM, I_ERROR)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ELEMENT_LIMITE_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LMGO_T6L_ASMESCL_0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_T6L_ASMESCL_1
C
C Description:
C     La fonction privée LMGO_T6L_ASMESCL_1 détermine le type de limite.
C     S'il n'y a qu'un seul segment, elle filtre les noeuds de bout.
C     Comme les éléments de peau sont numérotés anti-horaire, le noeud
C     de début est le noeud 1 de l'élément retourné.
C
C Entrée:
C
C Sortie:
C     IEL      ! Indice dans KCLELE de l'élément de début
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ASMESCL_1(ID1, ICL, NNOCL, KNCL, KCLLIM)

      USE FINDLOC_M, ONLY: FINDLOC
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMESCL_1
      INTEGER ID1
      INTEGER ICL
      INTEGER NNOCL
      INTEGER KNCL(NNOCL)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IDS(1), ID
      INTEGER ICOD
      INTEGER N, NELE, NSGL, NSEG, NMED
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.geom.t6l'

C---     Nombre de noeuds pas sur des elem
      NSGL = COUNT(KNCL(1:NNOCL) .EQ.-99)    ! Nombre de noeuds single
      NMED = COUNT(KNCL(1:NNOCL) .EQ.-22)    ! Nombre de noeuds milieux
      NSEG = COUNT(KNCL(1:NNOCL) .EQ.  0)    ! Nombre de segments
      NELE = COUNT(KNCL(1:NNOCL) .GT.  0)    ! Nombre d'éléments
      IF (NMED .NE. 0 .AND. NMED .NE. NELE) GOTO 9901

C---     Déduis le type
      ICOD = EG_TPLMT_CMPLX
      IF (NSGL .EQ. NNOCL) ICOD = EG_TPLMT_NOEUDS
      IF (NSGL .EQ. 0 .AND. NSEG .EQ. 1) ICOD = EG_TPLMT_1SGMT
      IF (NSGL .EQ. 0 .AND. NSEG .GT. 1) ICOD = EG_TPLMT_nSGMT
      KCLLIM(7, ICL) = ICOD
      IF (ICOD .NE. EG_TPLMT_1SGMT) GOTO 9999

C---     Cherche le début de la chaîne
      IDS = MAXLOC(KNCL); ID1 = IDS(1)
      DO WHILE (KNCL(ID1) .GT. 0)   ! Fin de chaîne
         ID1 = KNCL(ID1)
      ENDDO
      DO                            ! A rebours vers le début
         IDS = FINDLOC(KNCL, ID1)
         IF (IDS(1) .LE. 0) EXIT
         ID1 = IDS(1)
      ENDDO

C---     Contrôle le nombre d'éléments
      N = 0
      ID = ID1
      DO WHILE (KNCL(ID) .GT. 0)
         N  = N + 1
         ID = KNCL(ID)
      ENDDO
      IF (N .NE. NELE) GOTO 9901

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_LIMITE_INCOHERENTE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_NOEUDS_LIMITE', ': ', NNOCL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_ELEMENTS_LIMITE', ': ', NELE
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_NOEUDS_MILIEUX', ': ', NMED
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_NOEUDS_UNIQUES', ': ', NSGL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_SEGMENTS_LIMITE', ': ', NSEG
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_ELEMENTS_SEGMENTS', ': ', N
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LMGO_T6L_ASMESCL_1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_T6L_ASMESCL_2
C
C Description:
C     La fonction privée LMGO_T6L_ASMESCL_2 réordonne les éléments et
C     les noeuds de la limite ICL
C
C Entrée:
C     IEL      ! Indice dans KCLELE de l'élément de début
C
C Sortie:
C     KCLNOD         Table des noeuds triée
C     KCLELE         Table des noeuds triée
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ASMESCL_2(ID1,
     &                            ICL,
     &                            KNCL,
     &                            KNGS,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            KCLELE)

      USE FINDLOC_M, ONLY: FINDLOC
      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMESCL_2
      INTEGER ID1
      INTEGER ICL
      INTEGER KNCL  (*)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IRET
      INTEGER INOCLINF, INOCLSUP, NNOCL
      INTEGER IELCLINF, IELCLSUP
      INTEGER IECL, INCL, ISWP
      INTEGER IDX1, IDX3
      INTEGEr KSWP
      INTEGER INCL1, INCL3
      INTEGER NOP1, NOP2, NOP3
      INTEGER ILOC1(1)
D     INTEGER ILOC3(1)
      LOGICAL ANOMED
      INTEGER, ALLOCATABLE :: KTMP(:)
C-----------------------------------------------------------------------

      INOCLINF = KCLLIM(3,ICL)
      INOCLSUP = KCLLIM(4,ICL)
      IELCLINF = KCLLIM(5,ICL)
      IELCLSUP = KCLLIM(6,ICL)
      NNOCL = INOCLSUP - INOCLINF + 1
      ANOMED = ANY(KNCL(1:NNOCL) .EQ. -22)

C---     Réordonne les éléments
      IECL = IELCLINF
      IDX1 = ID1
      DO WHILE (KNCL(IDX1) .GT. 0)
         !! ID1 =
         IDX3  = KNCL(IDX1)
         INCL1 = IDX1 + INOCLINF - 1
         INCL3 = IDX3 + INOCLINF - 1
         NOP1  = KCLNOD(INCL1)
         NOP3  = KCLNOD(INCL3)

         IF (NOP1 .GT. 0 .AND. NOP3 .GT. 0) THEN
            ILOC1 = FINDLOC(KNGS(1, KCLELE(IELCLINF:IELCLSUP)), NOP1)    ! Est unique
D           ILOC3 = FINDLOC(KNGS(3, KCLELE(IELCLINF:IELCLSUP)), NOP3)
D           CALL ERR_ASR(ALL(ILOC1 .GT. 0))
D           CALL ERR_ASR(ALL(ILOC3 .EQ. ILOC1))

            ISWP = ILOC1(1) + IELCLINF - 1
            KSWP = KCLELE(IECL)
            KCLELE(IECL) = KCLELE(ISWP)
            KCLELE(ISWP) = KSWP
            IECL = IECL + 1
         ENDIF

         IDX1  = IDX3
      END DO

C---     Alloue la table de travail
      ALLOCATE(KTMP(INOCLINF:INOCLSUP), STAT=IRET)
      IF (IRET. NE. 0) GOTO 9900
      KTMP(INOCLINF:INOCLSUP) = KCLNOD(INOCLINF:INOCLSUP)
      KCLNOD(INOCLINF:INOCLSUP) = 0

C---     Réordonne les noeuds
      INCL = INOCLINF
      IECL = IELCLINF
      IDX1 = ID1
      DO WHILE (KNCL(IDX1) .GT. 0)
         !! ID1 =
         IDX3  = KNCL(IDX1)
         INCL1 = IDX1 + INOCLINF - 1
         INCL3 = IDX3 + INOCLINF - 1
         NOP1  = KTMP(INCL1)
         NOP3  = KTMP(INCL3)
         IF (NOP1 .GT. 0 .AND. NOP3 .GT. 0) THEN
D           CALL ERR_ASR(NOP1 .EQ. KNGS(1, KCLELE(IECL)))
D           CALL ERR_ASR(NOP3 .EQ. KNGS(3, KCLELE(IECL)))

            ILOC1 = FINDLOC(KTMP(INOCLINF:INOCLSUP), NOP1)
            ISWP  = ILOC1(1) + INOCLINF - 1
            KCLNOD(INCL) = KTMP(ISWP)
            INCL = INCL + 1

            IF (ANOMED) THEN
               NOP2  = KNGS(2, KCLELE(IECL))
               ILOC1 = FINDLOC(KTMP(INOCLINF:INOCLSUP), NOP2)
               ISWP  = ILOC1(1) + INOCLINF - 1
               KCLNOD(INCL) = KTMP(ISWP)
               INCL = INCL + 1
            ENDIF

            ! NOP3 est écrasé par le prochain NOP1 sauf pour le dernier elem
            ILOC1 = FINDLOC(KTMP(INOCLINF:INOCLSUP), NOP3)
            ISWP  = ILOC1(1) + INOCLINF - 1
            KCLNOD(INCL) = KTMP(ISWP)
            !INCL = INCL + 1     ! Pour se faire écraser

            IECL = IECL + 1
         ENDIF

         IDX1  = IDX3
      END DO

C---     Récupère la mémoire
      IF (ALLOCATED(KTMP)) DEALLOCATE(KTMP)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_WRN, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      LMGO_T6L_ASMESCL_2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_T6L_ASMESCL_3
C
C Description:
C     La fonction privée LMGO_T6L_ASMESCL_3 calcule la distance relative
C     des noeuds de la limite ICL. Les noeuds doivent être ordonnés.
C
C Entrée:
C
C Sortie:
C     VCLDST         Table des distances
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ASMESCL_3(ICL,
     &                            VCORG,
     &                            KCLLIM,
     &                            KCLNOD,
     &                            VCLDST)

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ASMESCL_3
      INTEGER ICL
      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_COMM, I_ERROR

      INTEGER INOCLINF, INOCLSUP, NNOCL
      INTEGER INL, NO1, NO2
      REAL*8  DS
      REAL*8  X1, Y1, X2, Y2
C-----------------------------------------------------------------------

      INOCLINF = KCLLIM(3,ICL)
      INOCLSUP = KCLLIM(4,ICL)
      NNOCL = INOCLSUP - INOCLINF + 1

C---     Calcule la distance au noeud précédant
      NO1 = KCLNOD(INOCLINF)
      VCLDST(INOCLINF) = 0.0D0
      DO INL=INOCLINF+1, INOCLSUP
         NO2 = KCLNOD(INL)
         IF (NO1 .GT. 0 .AND. NO2 .GT. 0) THEN
            X1 = VCORG(1, NO1)
            Y1 = VCORG(2, NO1)
            X2 = VCORG(1, NO2)
            Y2 = VCORG(2, NO2)
            DS = HYPOT(X2-X1, Y2-Y1)
         ELSE
            DS = 0.0D0
         ENDIF
         VCLDST(INL) = DS
         NO1 = NO2
      ENDDO

C---     Réduis VCLDST
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, VCLDST(INOCLINF), NNOCL,
     &                   MP_TYPE_RE8(), MPI_MAX, I_COMM, I_ERROR)

C---     Cumule la distance
      DO INL=INOCLINF+1, INOCLSUP
         VCLDST(INL) = VCLDST(INL) + VCLDST(INL-1)
      ENDDO

C---     Calcule la distance cumulée relative
      DS = VCLDST(INOCLSUP)
      DO INL=INOCLINF+1, INOCLSUP-1
         VCLDST(INL) = VCLDST(INL) / DS
      ENDDO
      VCLDST(INOCLSUP) = 1.0D0


      LMGO_T6L_ASMESCL_3 = ERR_TYP()
      RETURN
      END
