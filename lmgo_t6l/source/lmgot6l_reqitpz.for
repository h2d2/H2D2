C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LM_GOT6L_REQITPZ
C
C Description:
C     La fonction LM_GOT6L_REQITPZ retourne le type d'éléments généré
C     par l'opérations de split du maillage formé d'éléments T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GOT6L_REQITPZ(HOBJ, ITPE)
CDEC$ATTRIBUTES DLLEXPORT :: LM_GOT6L_REQITPZ

      IMPLICIT NONE

      INCLUDE 'lmgeom.fi'

      INTEGER HOBJ
      INTEGER ITPE

      INCLUDE 'lmgot6l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'

C----------------------------------------------------------------------
D     CALL ERR_PRE(LM_GOT6L_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      ITPE = EG_TPGEO_T3

      LM_GOT6L_REQITPZ = ERR_TYP()
      RETURN
      END
