C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER LMGO_T6L_REQITPEZ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_T6L_REQITPEZ
C
C Description:
C     La fonction LMGO_T6L_REQITPEZ retourne le type d'éléments généré
C     par l'opération de split du maillage formé d'éléments T6L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_REQITPEZ(ITPE)
CDEC$ATTRIBUTES DLLEXPORT :: LMGO_T6L_REQITPEZ

      INTEGER ITPE

      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'
C----------------------------------------------------------------------
C----------------------------------------------------------------------

      ITPE = EG_TPGEO_T3

      LMGO_T6L_REQITPEZ = ERR_TYP()
      RETURN
      END
