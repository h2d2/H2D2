C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C**************************************************************
C Sommaire: LMGO_T6_CLCERR
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C     VCORG       Table des COoRdonnées Globales
C     KNGV        Table des coNectivités Globales de Volume
C     KNGS        Table des coNectivités Globales de Surface
C     VDJV        Table des métriques de l'élément de Volume
C     VDJS        Table des métriques de l'élément de Surface
C     VFRM        Table des valeurs nodales de la fonction
C     ITPCLC      Bit-set du type de calcul
C
C Sortie:
C     VHESS       Table du hessien ou de l'ellipse
C     VHNRM       Table de la norme du hessien
C
C Notes:
C
C****************************************************************
      FUNCTION LMGO_T6L_CLCERR(VCORG,
     &                         KNGV,
     &                         KNGS,
     &                         VDJV,
     &                         VDJS,
     &                         VFRM,
     &                         VHESS,
     &                         VHNRM,
     &                         ITPCLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_T6L_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      REAL*8   VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (3, EG_CMMN_NNL)
      REAL*8   VHNRM (EG_CMMN_NNL)
      INTEGER  ITPCLC

      INCLUDE 'eacnst.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IN

      INTEGER LMGO_T6L_ERRHESS
      INTEGER LMGO_T6L_ERRDXDX
      INTEGER LMGO_T6L_ERRRCOV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Initialise
      CALL DINIT (3*EG_CMMN_NNL, ZERO, VHESS, 1)
      CALL DINIT (  EG_CMMN_NNL, ZERO, VHNRM, 1)

C---     Calcule le Hessien
      IF     (BTEST(ITPCLC, EG_TPERR_HESSGREEN)) THEN
         IERR = LMGO_T6L_ERRHESS(KNGV,KNGS,VDJV,VDJS,VFRM,VHESS,VHNRM)
      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSDXDX)) THEN
         IERR = LMGO_T6L_ERRDXDX(KNGV,KNGS,VDJV,VDJS,VFRM,VHESS,VHNRM)
      ELSEIF (BTEST(ITPCLC, EG_TPERR_HESSRECOV)) THEN
         IERR = LMGO_T6L_ERRRCOV(VCORG,KNGV,VFRM,VHESS)
      ELSE
         CALL ERR_ASR(.FALSE.)
      ENDIF

C---     Transforme le Hessien
      IF     (BTEST(ITPCLC, EG_TPERR_NRMHESS)) THEN
         DO IN=1,EG_CMMN_NNL
            VHNRM(IN) = SQRT(VHESS(1,IN)*VHESS(1,IN) +
     &                       VHESS(2,IN)*VHESS(2,IN)*DEUX +
     &                       VHESS(3,IN)*VHESS(3,IN))
         ENDDO
      ELSEIF (BTEST(ITPCLC, EG_TPERR_ELLIPSE)) THEN
         CALL SP_ELEM_HESSELLPS(3, EG_CMMN_NNL, VHESS)
      ENDIF

      LMGO_T6L_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  LMGO_T6L_ERRHESS
C
C Description:
C     La fonction LMGO_T6L_ERRHESS calcule l'erreur d'approximation pour
C     des éléments de volume de type T6L.
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ERRHESS(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VFRM,
     &                          VHESS,
     &                          VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ERRHESS

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (3, EG_CMMN_NNL)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t6l.fi'
      INCLUDE 'err.fi'

      INTEGER IERR

      INTEGER LMGO_T6L_CLCHESS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Calcule le Hessien
      IERR = LMGO_T6L_CLCHESS(KNGV,
     &                        KNGS,
     &                        VDJV,
     &                        VDJS,
     &                        VFRM,
     &                        VHESS,
     &                        VTRV)

      LMGO_T6L_ERRHESS = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire: LMGO_T6L_ERRDXDX
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage
C     EQUATION : NORME DE LA 2. DERIVEE
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La table est reçue inversée (nnl,3) plutôt que (3,nnl) pour
C     faciliter les calculs. A la fin elle est rebasculée.
C****************************************************************
      FUNCTION LMGO_T6L_ERRDXDX(KNGV,
     &                          KNGS,
     &                          VDJV,
     &                          VDJS,
     &                          VFRM,
     &                          VHESS,
     &                          VTRV)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ERRDXDX

      INTEGER  KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      INTEGER  KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      REAL*8   VDJV  (EG_CMMN_NDJV, EG_CMMN_NELV)
      REAL*8   VDJS  (EG_CMMN_NDJS, EG_CMMN_NELS)
      REAL*8   VFRM  (EG_CMMN_NNL)
      REAL*8   VHESS (EG_CMMN_NNL, 3)
      REAL*8   VTRV  (EG_CMMN_NNL)

      INCLUDE 'eacnst.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      INTEGER IN

      INTEGER LMGO_T6L_CLCDDX
      INTEGER LMGO_T6L_CLCDDY
      INTEGER LMGO_T6L_ASMDDX
      INTEGER LMGO_T6L_ASMDDY
      INTEGER LMGO_T6L_ASMML
C-----------------------------------------------------------------------
D     CALL ERR_PRE(EG_CMMN_NNL   .GE. 6)
D     CALL ERR_PRE(EG_CMMN_NCELV .EQ. 6)
C-----------------------------------------------------------------------

C---     Calcule f,y dans VHESS(.,1)
      IERR = LMGO_T6L_CLCDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VFRM,
     &                       VHESS(1,1),
     &                       VTRV)

C---     Assemble f,yy dans VHESS(.,3)
      IERR = LMGO_T6L_ASMDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VHESS(1,1),
     &                       VHESS(1,3))

C---     Calcule f,x dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      IERR = LMGO_T6L_CLCDDX(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VFRM,
     &                       VTRV,
     &                       VHESS(1,2))

C---     Assemble f,xx dans VHESS(.,1)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,1), 1)
      IERR = LMGO_T6L_ASMDDX(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV,
     &                       VHESS(1,1))

C---     Assemble f,xy dans VHESS(.,2)
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VHESS(1,2), 1)
      IERR = LMGO_T6L_ASMDDY(KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV,
     &                       VHESS(1,2))

C---     Assemble Ml dans VTRV
      CALL DINIT(EG_CMMN_NNL, 0.0D0, VTRV, 1)
      IERR = LMGO_T6L_ASMML (KNGV,
     &                       KNGS,
     &                       VDJV,
     &                       VDJS,
     &                       VTRV)

C---     Finalise
      DO IN=1,EG_CMMN_NNL
         VTRV(IN) = UN / VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,1) = VHESS(IN,1) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,2) = VHESS(IN,2) * VTRV(IN)
      ENDDO
      DO IN=1,EG_CMMN_NNL
         VHESS(IN,3) = VHESS(IN,3) * VTRV(IN)
      ENDDO

C---     Transpose la table VHESS
      CALL TRANS(VHESS,EG_CMMN_NNL, 3, 3*EG_CMMN_NNL,
     &           VTRV, EG_CMMN_NNL, IERR)
      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_TRANSPOSE_TABLE')
      ENDIF

      LMGO_T6L_ERRDXDX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: LMGO_T6L_ERRRCOV
C
C Description:
C     Calcul de l'erreur due à VFRM pour le raffinement de maillage.
C     Calcul du hessien par "least-square polynomial fit' i.e. recovery.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_T6L_ERRRCOV(VCORG,
     &                          KNGV,
     &                          VFRM,
     &                          VHESS)

      IMPLICIT NONE

      INCLUDE 'egcmmn.fc'

      INTEGER LMGO_T6L_ERRRCOV

      REAL*8,  INTENT(IN) :: VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER, INTENT(IN) :: KNGV  (EG_CMMN_NCELV,EG_CMMN_NELV)
      REAL*8,  INTENT(IN) :: VFRM  (EG_CMMN_NNL)
      REAL*8,  INTENT(OUT):: VHESS (3, EG_CMMN_NNL)

      INCLUDE 'err.fi'
      INCLUDE 'lmgo_t3.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR, IRET
      INTEGER IET6, IET3
      INTEGER NCELV_0, NNELV_0, NELV_0
      INTEGER, DIMENSION(:,:), ALLOCATABLE :: KNGT3

      INTEGER, PARAMETER, DIMENSION(3,4) :: KT3 =
     &                 RESHAPE((/ 1, 2, 6,
     &                            2, 3, 4,
     &                            6, 4, 5,
     &                            4, 6, 2/), (/3, 4/))

      INTEGER LMGO_T3_ASGCMN
      INTEGER LMGO_T3_ERRRCOV
C----------------------------------------------------------------------

C---     Alloue la table pour les connec des T3
      ALLOCATE( KNGT3(3, 4*EG_CMMN_NELV), STAT=IRET )
      IF (IRET .NE. 0) GOTO 9900

C---     Split les éléments
      IET3 = 0
      DO IET6=1, EG_CMMN_NELV
         IET3 = IET3 + 1
         KNGT3(:,IET3) = KNGV(KT3(:,1), IET6)
         IET3 = IET3 + 1
         KNGT3(:,IET3) = KNGV(KT3(:,2), IET6)
         IET3 = IET3 + 1
         KNGT3(:,IET3) = KNGV(KT3(:,3), IET6)
         IET3 = IET3 + 1
         KNGT3(:,IET3) = KNGV(KT3(:,4), IET6)
      ENDDO

C---     Modifie les dim & assigne au T3
      NCELV_0 = EG_CMMN_NCELV
      NNELV_0 = EG_CMMN_NNELV
      NELV_0  = EG_CMMN_NELV
      EG_CMMN_NCELV = 3
      EG_CMMN_NNELV = 3
      EG_CMMN_NELV  = 4 * EG_CMMN_NELV
      IERR = LMGO_T3_ASGCMN (EG_CMMN_KA)

C---     Calcule
      IERR = LMGO_T3_ERRRCOV(VCORG, KNGT3, VFRM, VHESS)

C---     Rétablis les dim
      EG_CMMN_NCELV = NCELV_0
      EG_CMMN_NNELV = NNELV_0
      EG_CMMN_NELV  = NELV_0

C---     Désalloue la table
      IF (ALLOCATED(KNGT3)) DEALLOCATE(KNGT3)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      LMGO_T6L_ERRRCOV = ERR_TYP()
      RETURN
      END
