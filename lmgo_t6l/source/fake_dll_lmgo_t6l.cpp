//************************************************************************
// H2D2 - External declaration of public symbols
// Module: lmgo_t6l
// Entry point: extern "C" void fake_dll_lmgo_t6l()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:03.332000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class LM_GOT6L
F_PROT(LM_GOT6L_000, lm_got6l_000);
F_PROT(LM_GOT6L_999, lm_got6l_999);
F_PROT(LM_GOT6L_CTR, lm_got6l_ctr);
F_PROT(LM_GOT6L_DTR, lm_got6l_dtr);
F_PROT(LM_GOT6L_INI, lm_got6l_ini);
F_PROT(LM_GOT6L_RST, lm_got6l_rst);
F_PROT(LM_GOT6L_REQHBASE, lm_got6l_reqhbase);
F_PROT(LM_GOT6L_HVALIDE, lm_got6l_hvalide);
F_PROT(LM_GOT6L_ASMESCL, lm_got6l_asmescl);
F_PROT(LM_GOT6L_ASMPEAU, lm_got6l_asmpeau);
F_PROT(LM_GOT6L_CLCERR, lm_got6l_clcerr);
F_PROT(LM_GOT6L_CLCJELS, lm_got6l_clcjels);
F_PROT(LM_GOT6L_CLCJELV, lm_got6l_clcjelv);
F_PROT(LM_GOT6L_CLCSPLT, lm_got6l_clcsplt);
F_PROT(LM_GOT6L_INTRP, lm_got6l_intrp);
F_PROT(LM_GOT6L_LCLELV, lm_got6l_lclelv);
 
// ---  class LMGO_T6L
F_PROT(LMGO_T6L_ASGCMN, lmgo_t6l_asgcmn);
F_PROT(LMGO_T6L_ASMESCL, lmgo_t6l_asmescl);
F_PROT(LMGO_T6L_ASMPEAU, lmgo_t6l_asmpeau);
F_PROT(LMGO_T6L_CLCERR, lmgo_t6l_clcerr);
F_PROT(LMGO_T6L_CLCJELS, lmgo_t6l_clcjels);
F_PROT(LMGO_T6L_CLCJELV, lmgo_t6l_clcjelv);
F_PROT(LMGO_T6L_CLCSPLIT, lmgo_t6l_clcsplit);
F_PROT(LMGO_T6L_INTRP, lmgo_t6l_intrp);
F_PROT(LMGO_T6L_LCLELE, lmgo_t6l_lclele);
F_PROT(LMGO_T6L_REQNFN, lmgo_t6l_reqnfn);
F_PROT(LMGO_T6L_REQPRM, lmgo_t6l_reqprm);
F_PROT(LMGO_T6L_CLCGRAD, lmgo_t6l_clcgrad);
F_PROT(LMGO_T6L_CLCDDX, lmgo_t6l_clcddx);
F_PROT(LMGO_T6L_CLCDDY, lmgo_t6l_clcddy);
F_PROT(LMGO_T6L_CLCHESS, lmgo_t6l_clchess);
F_PROT(LMGO_T6L_ASMDDX, lmgo_t6l_asmddx);
F_PROT(LMGO_T6L_ASMDDY, lmgo_t6l_asmddy);
F_PROT(LMGO_T6L_ASMML, lmgo_t6l_asmml);
F_PROT(LMGO_T6L_KUEHSSV, lmgo_t6l_kuehssv);
F_PROT(LMGO_T6L_KUEHSSS, lmgo_t6l_kuehsss);
F_PROT(LMGO_T6L_KUEGRDV, lmgo_t6l_kuegrdv);
F_PROT(LMGO_T6L_KUEDDX, lmgo_t6l_kueddx);
F_PROT(LMGO_T6L_KUEDDY, lmgo_t6l_kueddy);
F_PROT(LMGO_T6L_KUEML, lmgo_t6l_kueml);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_lmgo_t6l()
{
   static char libname[] = "lmgo_t6l";
 
   // ---  class LM_GOT6L
   F_RGST(LM_GOT6L_000, lm_got6l_000, libname);
   F_RGST(LM_GOT6L_999, lm_got6l_999, libname);
   F_RGST(LM_GOT6L_CTR, lm_got6l_ctr, libname);
   F_RGST(LM_GOT6L_DTR, lm_got6l_dtr, libname);
   F_RGST(LM_GOT6L_INI, lm_got6l_ini, libname);
   F_RGST(LM_GOT6L_RST, lm_got6l_rst, libname);
   F_RGST(LM_GOT6L_REQHBASE, lm_got6l_reqhbase, libname);
   F_RGST(LM_GOT6L_HVALIDE, lm_got6l_hvalide, libname);
   F_RGST(LM_GOT6L_ASMESCL, lm_got6l_asmescl, libname);
   F_RGST(LM_GOT6L_ASMPEAU, lm_got6l_asmpeau, libname);
   F_RGST(LM_GOT6L_CLCERR, lm_got6l_clcerr, libname);
   F_RGST(LM_GOT6L_CLCJELS, lm_got6l_clcjels, libname);
   F_RGST(LM_GOT6L_CLCJELV, lm_got6l_clcjelv, libname);
   F_RGST(LM_GOT6L_CLCSPLT, lm_got6l_clcsplt, libname);
   F_RGST(LM_GOT6L_INTRP, lm_got6l_intrp, libname);
   F_RGST(LM_GOT6L_LCLELV, lm_got6l_lclelv, libname);
 
   // ---  class LMGO_T6L
   F_RGST(LMGO_T6L_ASGCMN, lmgo_t6l_asgcmn, libname);
   F_RGST(LMGO_T6L_ASMESCL, lmgo_t6l_asmescl, libname);
   F_RGST(LMGO_T6L_ASMPEAU, lmgo_t6l_asmpeau, libname);
   F_RGST(LMGO_T6L_CLCERR, lmgo_t6l_clcerr, libname);
   F_RGST(LMGO_T6L_CLCJELS, lmgo_t6l_clcjels, libname);
   F_RGST(LMGO_T6L_CLCJELV, lmgo_t6l_clcjelv, libname);
   F_RGST(LMGO_T6L_CLCSPLIT, lmgo_t6l_clcsplit, libname);
   F_RGST(LMGO_T6L_INTRP, lmgo_t6l_intrp, libname);
   F_RGST(LMGO_T6L_LCLELE, lmgo_t6l_lclele, libname);
   F_RGST(LMGO_T6L_REQNFN, lmgo_t6l_reqnfn, libname);
   F_RGST(LMGO_T6L_REQPRM, lmgo_t6l_reqprm, libname);
   F_RGST(LMGO_T6L_CLCGRAD, lmgo_t6l_clcgrad, libname);
   F_RGST(LMGO_T6L_CLCDDX, lmgo_t6l_clcddx, libname);
   F_RGST(LMGO_T6L_CLCDDY, lmgo_t6l_clcddy, libname);
   F_RGST(LMGO_T6L_CLCHESS, lmgo_t6l_clchess, libname);
   F_RGST(LMGO_T6L_ASMDDX, lmgo_t6l_asmddx, libname);
   F_RGST(LMGO_T6L_ASMDDY, lmgo_t6l_asmddy, libname);
   F_RGST(LMGO_T6L_ASMML, lmgo_t6l_asmml, libname);
   F_RGST(LMGO_T6L_KUEHSSV, lmgo_t6l_kuehssv, libname);
   F_RGST(LMGO_T6L_KUEHSSS, lmgo_t6l_kuehsss, libname);
   F_RGST(LMGO_T6L_KUEGRDV, lmgo_t6l_kuegrdv, libname);
   F_RGST(LMGO_T6L_KUEDDX, lmgo_t6l_kueddx, libname);
   F_RGST(LMGO_T6L_KUEDDY, lmgo_t6l_kueddy, libname);
   F_RGST(LMGO_T6L_KUEML, lmgo_t6l_kueml, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
