C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOT6L
C         FTN (Sub)Module: LM_GOT6L_CLCJELS_M
C            Public:
C               MODULE INTEGER LM_GOT6L_CLCJELS
C            Private:
C
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_CLCJELS_M

      IMPLICIT NONE

      CONTAINS

C************************************************************************
C Sommaire:  LM_GOT6L_CLCJELS
C
C Description:
C     La fonction LM_GOT6L_CLCJELS calcule les métriques de pour les
C     éléments de surface de type L3L, surface d'éléments T6L.
C
C Entrée:
C  HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      MODULE INTEGER FUNCTION LM_GOT6L_CLCJELS(SELF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_CLCJELS
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      CLASS(LM_GOT6L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      REAL*8  TX, TY, TL
      REAL*8  TXN, TYN
      INTEGER IEP, IE
      INTEGER NP1, NP3, NO1

      REAL*8, PARAMETER :: UN_2 = 5.0000000000000000000D-01
C-----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     CALL ERR_PRE(GDTA%NNL   .GE. 6)
D     CALL ERR_PRE(GDTA%NCELS .GE. 5)
D     CALL ERR_PRE(GDTA%NDJV  .GE. 5)
D     CALL ERR_PRE(GDTA%NDJS  .GE. 3)

C---     Boucle sur les éléments
      DO 10 IEP=1,GDTA%NELLS
         NP1 = GDTA%KNGS(1,IEP)
         NP3 = GDTA%KNGS(3,IEP)
         IE  = GDTA%KNGS(4,IEP)
         NO1 = GDTA%KNGV(1,IE)
         IF (NP1 .EQ. NO1) THEN            ! COTE 1-3
            TX =   GDTA%VDJV(4,IE)
            TY = - GDTA%VDJV(2,IE)
         ELSEIF (NP3 .EQ. NO1) THEN        ! COTE 5-1
            TX =   GDTA%VDJV(3,IE)
            TY = - GDTA%VDJV(1,IE)
         ELSE                              ! COTE 3-5
            TX = - (GDTA%VDJV(3,IE) + GDTA%VDJV(4,IE))
            TY =   (GDTA%VDJV(1,IE) + GDTA%VDJV(2,IE))
         ENDIF
         TL  = HYPOT(TX, TY)
         TXN = TX/TL
         TYN = TY/TL
         GDTA%VDJS(1,IEP) = TXN
         GDTA%VDJS(2,IEP) = TYN
         GDTA%VDJS(3,IEP) = UN_2*TL
10    CONTINUE

      LM_GOT6L_CLCJELS = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_CLCJELS

      END SUBMODULE LM_GOT6L_CLCJELS_M
