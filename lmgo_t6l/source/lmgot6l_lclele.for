C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

      SUBMODULE(LM_GOT6L_M) LM_GOT6L_LCLELV_M
      
      IMPLICIT NONE

      CONTAINS
      
C************************************************************************
C Sommaire:  LM_GOT6L_LCLELV
C
C Description:
C     La fonction LM_GOT6L_LCLELV localise l'élément sous les points
C     de la liste VCORP. Elle retourne dans KELEP les numéros d'élément
C     et dans VCORE les coordonnées sur l'élément de référence du point.
C     Si un point n'est pas trouvé son numéro d'élément est 0.
C
C Entrée:
C     NPNT        Nombre de points
C     VCORP       Coordonnées des points
C
C Sortie:
C     KELEP       Table des numéros d'élément
C     VCORE       Coordonnées sur l'élément de référence
C
C Notes:
C************************************************************************
      MODULE INTEGER 
     &FUNCTION LM_GOT6L_LCLELV(SELF,
     &                         TOL,
     &                         VCORP,
     &                         KELEP,
     &                         VCORE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LM_GOT6L_LCLELV
CDEC$ ENDIF

      USE LM_GDTA_M, ONLY: LM_GDTA_T

      CLASS(LM_GOT6L_T), INTENT(IN), TARGET :: SELF
      REAL*8,  INTENT(IN)  :: TOL
      REAL*8,  INTENT(IN)  :: VCORP(:, :)
      INTEGER, INTENT(OUT) :: KELEP(:)
      REAL*8,  INTENT(OUT) :: VCORE(:, :)

      INCLUDE 'err.fi'

      INTEGER IERR
D     INTEGER NDIM, NPNT
      TYPE (LM_GDTA_T), POINTER :: GDTA
      
      INTEGER LM_GOT3_LOCE
C----------------------------------------------------------------------

C---     Récupère les attributs
      GDTA => SELF%GDTA
D     NDIM = GDTA%NDIM
D     NPNT = SIZE(VCORP, 2)
D     CALL ERR_PRE(NDIM .EQ. SIZE(VCORP, 1))
D     CALL ERR_PRE(NDIM .EQ. SIZE(VCORE, 1))
D     CALL ERR_PRE(NPNT .EQ. SIZE(KELEP, 1))
D     CALL ERR_PRE(NPNT .EQ. SIZE(VCORE, 2))

C---     Sous-traite au T3
      IERR = LM_GOT3_LOCE(TOL,
     &                    GDTA%VCORG,
     &                    GDTA%KNGV,
     &                    GDTA%VDJV,
     &                    VCORP,
     &                    KELEP,
     &                    VCORE)

      LM_GOT6L_LCLELV = ERR_TYP()
      RETURN
      END FUNCTION LM_GOT6L_LCLELV
      
      END SUBMODULE LM_GOT6L_LCLELV_M
