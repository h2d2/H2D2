C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_P12L_ASMPEAU
C
C Description:
C     La fonction LMGO_P12L_ASMPEAU assemble la peau pour des
C     éléments de volume de type P12L.
C
C Entrée: (et sortie)
C     int NCELV      Nombre de Connectivité par Élément de Volume
C     int NELV       Nombre d'ÉLément de Volcume
C     int KNGV(NCELV,NELV) : Table des Noeuds ... ??
C     int NNT        Nombre de noeuds au total
C     int KPNT(*)
C     int NLIEN
C     int KLIEN(4,*)
C     int INEXT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_P12L_ASMPEAU (NCELV,
     &                            NELV,
     &                            KNGV,
     &                            NNT,
     &                            KPNT,
     &                            NLIEN,
     &                            KLIEN,
     &                            INEXT)

      IMPLICIT NONE

      INTEGER NCELV
      INTEGER NELV
      INTEGER KNGV (NCELV,NELV)
      INTEGER NNT
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'lmgo_p12l.fi'
      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_P12L_ASMPEAU   encore le code du 2d à coder')

      INFO(2) = 0
      DO IE=1,NELV

C---        SIDE 1 -- NODES 1-3
         IF (KNGV(1,IE) .LT. KNGV(2,IE)) THEN
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = -IE
         ENDIF
         CALL LOG_TODO('LMGO_P12L_ASMPEAU : NVLNK used before set')
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 2 -- NODES 3-5
         IF (KNGV(2,IE) .LT. KNGV(3,IE)) THEN
            ICLEF   = KNGV(3, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(3, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        SIDE 3 -- NODES 5-1
         IF (KNGV(3,IE) .LT. KNGV(1,IE)) THEN
            ICLEF   = KNGV(5, IE)
            INFO(1) = KNGV(1, IE)
            INFO(3) = IE
         ELSE
            ICLEF   = KNGV(1, IE)
            INFO(1) = KNGV(5, IE)
            INFO(3) = -IE
         ENDIF
         IERR = SP_LSTC_AJTCHN (NNT, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LMGO_P12L_ASMPEAU = ERR_TYP()
      RETURN
      END

