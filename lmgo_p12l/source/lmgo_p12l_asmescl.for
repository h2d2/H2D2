C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_P12L_ASMESCL
C
C Description:
C     La fonction LMGO_P12L_ASMESCL assemble les éléments de surface
C     des conditions limites.
C
C Entrée(et sorties)
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_P12L_ASMESCL(VCORG,
     &                          KNGS,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_P12L_ASMESCL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)

      INCLUDE 'lmgo_p12l.fi'
      INCLUDE 'egtplmt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ICL, IEP, IN
      INTEGER INOCLINF, INOCLSUP
      INTEGER IELE
      INTEGER NELE
      INTEGER NHIT
      INTEGER NP1, NP2, NP3
C-----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_P12L_ASMESCL jamais teste - meme pas le 2d' //
     &              ', PEUT-ETRE OK!!')

C---     BOUCLE SUR LES (CONDITIONS) LIMITES
      IELE = 0
      DO ICL=1, EG_CMMN_NCLLIM
         KCLLIM(5,ICL) = IELE + 1

         INOCLINF = KCLLIM(3,ICL)
         INOCLSUP = KCLLIM(4,ICL)

         NELE = 0
         DO IEP=1,EG_CMMN_NELS
            NP1 = KNGS(1,IEP)
            NP2 = KNGS(2,IEP)
            NP3 = KNGS(3,IEP)

            NHIT = 0
            DO IN=INOCLINF,INOCLSUP
               IF (KCLNOD(IN) .EQ. NP1) NHIT=NHIT + 1
               IF (KCLNOD(IN) .EQ. NP2) NHIT=NHIT + 1
               IF (KCLNOD(IN) .EQ. NP3) NHIT=NHIT + 1
               IF (NHIT .EQ. 3) GOTO 199
            ENDDO
199         CONTINUE
            IF (NHIT .EQ. 3) THEN
               NELE = NELE + 1
               IELE = IELE + 1
               KCLELE(IELE) = IEP
            ENDIF
         ENDDO

         KCLLIM(6,ICL) = IELE
         KCLLIM(7,ICL) = EG_TPLMT_INDEFINI
      ENDDO

      LMGO_P12L_ASMESCL = ERR_TYP()
      RETURN
      END
