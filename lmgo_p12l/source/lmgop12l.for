C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOP12L
C         FTN (Sub)Module: LM_GOP12L_M
C            Public:
C            Private:
C               SUBROUTINE LM_GOP12L_CTR
C            
C            FTN Type: LM_GOP12L_T
C               Public:
C                  INTEGER LM_GOP12L_DTR
C                  INTEGER LM_GOP12L_CLCERR
C                  INTEGER LM_GOP12L_CLCJELS
C                  INTEGER LM_GOP12L_CLCSPLT
C                  INTEGER LM_GOP12L_INTRP
C                  INTEGER LM_GOP12L_LCLELV
C               Private:
C                  INTEGER LM_GOP12L_INIPRMS
C                  INTEGER LM_GOP12L_DUMMY
C
C************************************************************************

      MODULE LM_GOP12L_M

      USE LM_GEOM_M, ONLY : LM_GEOM_T
      USE LM_GDTA_M, ONLY : LM_GDTA_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---   Classe
      !========================================================================
      TYPE, EXTENDS(LM_GEOM_T) :: LM_GOP12L_T
!        pass
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => LM_GOP12L_DTR
         PROCEDURE, PUBLIC :: ASMESCL  => LM_GOP12L_ASMESCL
         !!!PROCEDURE, PUBLIC :: ASMPEAU  => LM_GOP12L_ASMPEAU
         PROCEDURE, PUBLIC :: CLCERR   => LM_GOP12L_CLCERR
         PROCEDURE, PUBLIC :: CLCJELS  => LM_GOP12L_CLCJELS
         PROCEDURE, PUBLIC :: CLCJELV  => LM_GOP12L_CLCJELV
         PROCEDURE, PUBLIC :: CLCSPLT  => LM_GOP12L_CLCSPLT
         PROCEDURE, PUBLIC :: INTRP    => LM_GOP12L_INTRP
         PROCEDURE, PUBLIC :: LCLELV   => LM_GOP12L_LCLELV

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: INIPRMS => LM_GOP12L_INIPRMS
         PROCEDURE, PRIVATE :: DUMMY   => LM_GOP12L_DUMMY
      END TYPE LM_GOP12L_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: LM_GOP12L_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: LM_GOP12L_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
         MODULE INTEGER FUNCTION LM_GOP12L_ASMESCL(SELF)
            CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOP12L_ASMESCL

!!!         MODULE INTEGER FUNCTION LM_GOP12L_ASMPEAU(SELF)
!!!            IMPORT :: LM_GOP12L_T
!!!            CLASS(LM_GOP12L_T), INTENT(IN) :: SELF
!!!         END FUNCTION LM_GOP12L_ASMPEAU

         MODULE INTEGER FUNCTION LM_GOP12L_CLCJELV(SELF)
            CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOP12L_CLCJELV

      END INTERFACE


      CONTAINS

C************************************************************************
C Sommaire:
C
C Description: Constructeur de la classe
C
C Entrée:
C     HKID     Handle sur l'héritier qui a alloué la structure
C
C Sortie:
C     HOBJ     Handle sur l'objet créé
C
C Notes:
C     l'élément est concret
C************************************************************************
      FUNCTION LM_GOP12L_CTR() RESULT(SELF)

      USE LM_GEOM_M, ONLY: LM_GEOM_CTR

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      CLASS(LM_GEOM_T), POINTER :: OPRNT
      TYPE (LM_GOP12L_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900

C---     Construis le parent
      IF (ERR_GOOD()) OPRNT => LM_GEOM_CTR(SELF)

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION LM_GOP12L_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur LM_GOP12L_DTR est la partie virtuelle du processus
C     de destruction. Il est appelé par le parent sur appel de DEL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOP12L_DTR(SELF)

      USE LM_GEOM_M, ONLY: LM_GEOM_DTR

      CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GOP12L_T), POINTER :: SELF_P
C------------------------------------------------------------------------

C---     Détruis le parent
      IERR = LM_GEOM_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      LM_GOP12L_DTR = ERR_TYP()
      RETURN
      END FUNCTION LM_GOP12L_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode LM_GOP12L_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOP12L_INIPRMS(SELF)

      CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA

C---     Initialise les paramètres invariant
      GDTA%NDIM  = 3

      GDTA%ITPEV = EG_TPGEO_P12L
      GDTA%NNELV = 6
      GDTA%NCELV = 6        ! NNELV
      GDTA%NDJV  = 3*2 + 1  ! NDIM*2 + 1

      GDTA%ITPES = EG_TPGEO_L3L
      GDTA%NNELS = 3
      GDTA%NCELS = 3 + 2    ! NNELS + 2
      GDTA%NDJS  = 3

      GDTA%ITPEZ = EG_TPGEO_INDEFINI
      GDTA%NNELZ = 0
      GDTA%NCELZ = 0        ! NNELZ
      GDTA%NDJZ  = 0
      GDTA%NEV2EZ= 0

      LM_GOP12L_INIPRMS = ERR_TYP()
      RETURN
      END FUNCTION LM_GOP12L_INIPRMS

C************************************************************************
C Sommaire:  Fonction virtuelles non implantées
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOP12L_DUMMY(SELF)

      CLASS(LM_GOP12L_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'Function shall never be called')

      LM_GOP12L_DUMMY = ERR_TYP()
      RETURN
      END FUNCTION LM_GOP12L_DUMMY

      INTEGER FUNCTION LM_GOP12L_CLCERR(SELF,VFRM,VHESS,VHNRM,ITPCLC)
         CLASS(LM_GOP12L_T), INTENT(IN), TARGET :: SELF
         REAL*8,  INTENT(IN)  :: VFRM (:)
         REAL*8,  INTENT(OUT) :: VHESS(:,:)
         REAL*8,  INTENT(OUT) :: VHNRM(:)
         INTEGER, INTENT(IN)  :: ITPCLC
         LM_GOP12L_CLCERR = SELF%DUMMY()
      END FUNCTION LM_GOP12L_CLCERR

      INTEGER FUNCTION LM_GOP12L_CLCJELS(SELF)
         CLASS(LM_GOP12L_T), INTENT(INOUT), TARGET :: SELF
         LM_GOP12L_CLCJELS = SELF%DUMMY()
      END FUNCTION LM_GOP12L_CLCJELS

      INTEGER FUNCTION LM_GOP12L_CLCSPLT(SELF, KNGZ)
         CLASS(LM_GOP12L_T), INTENT(IN), TARGET :: SELF
         INTEGER, INTENT(OUT) :: KNGZ(:, :)
         LM_GOP12L_CLCSPLT = SELF%DUMMY()
      END FUNCTION LM_GOP12L_CLCSPLT

      INTEGER FUNCTION LM_GOP12L_INTRP(SELF,
     &                           KELE, VCORE,
     &                           INDXS, VSRC,
     &                           INDXD, VDST)
         CLASS(LM_GOP12L_T), INTENT(IN), TARGET :: SELF
         INTEGER, INTENT(IN)  :: KELE (:)
         REAL*8,  INTENT(IN)  :: VCORE(:, :)
         INTEGER, INTENT(IN)  :: INDXS
         REAL*8,  INTENT(IN)  :: VSRC (:, :)
         INTEGER, INTENT(IN)  :: INDXD
         REAL*8,  INTENT(OUT) :: VDST (:, :)
         LM_GOP12L_INTRP = SELF%DUMMY()
      END FUNCTION LM_GOP12L_INTRP

      INTEGER FUNCTION LM_GOP12L_LCLELV(SELF, TOL, VCORP, KELEP, VCORE)
         CLASS(LM_GOP12L_T), INTENT(IN), TARGET :: SELF
         REAL*8,  INTENT(IN)  :: TOL
         REAL*8,  INTENT(IN)  :: VCORP(:, :)
         INTEGER, INTENT(OUT) :: KELEP(:)
         REAL*8,  INTENT(OUT) :: VCORE(:, :)
         LM_GOP12L_LCLELV = SELF%DUMMY()
      END FUNCTION LM_GOP12L_LCLELV

      END MODULE LM_GOP12L_M
