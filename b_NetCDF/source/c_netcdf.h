//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
// Sousroutines:
//************************************************************************
#ifndef C_NETCDF_H_DEJA_INCLU
#define C_NETCDF_H_DEJA_INCLU

#ifndef MODULE_NETCDF
#  define MODULE_NETCDF 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_NETCDF_ERRMSG   C_NETCDF_ERRMSG
#  define C_NETCDF_OPEN     C_NETCDF_OPEN
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_NETCDF_ERRMSG   c_netcdf_errmsg_
#  define C_NETCDF_OPEN     c_netcdf_open_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_NETCDF_ERRMSG   c_netcdf_errmsg__
#  define C_NETCDF_OPEN     c_netcdf_open__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_PETSC)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_NETCDF_ERRMSG(fint_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_NETCDF_OPEN  (hndl_t*, F2C_CONF_STRING F2C_CONF_SUP_INT);

#ifdef __cplusplus
}
#endif

#endif   // C_NETCDF_H_DEJA_INCLU
