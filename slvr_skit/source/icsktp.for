C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SKTP_CMD(IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_SKTP_CMD

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icsktp.fi'
      INCLUDE 'grgrid.fi'
      INCLUDE 'prskit.fi'
      INCLUDE 'prprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HRES
      INTEGER HGRD
      INTEGER IDEB2, IFIN2
C------------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_SKIT_PRECOND'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
C     <comment>Handle on the grid</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HGRD)
      IF (IERR .NE. 0) GOTO 9900
      IDEB2 = SP_STRN_TOK(IPRM, ',', 2)
      IFIN2 = SP_STRN_LEN(IPRM)
      IF (IDEB2 .LE. 0) GOTO 9900
      IF (IFIN2 .LT. IDEB2) GOTO 9900

C---     CONTROLE QUE LES PARAM SONT VALIDES
      IF (.NOT. GR_GRID_HVALIDE(HGRD)) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET CONCRET
      IF (ERR_GOOD()) IERR = PR_SKIT_CTR(HRES)
      IF (ERR_GOOD()) IERR = PR_SKIT_INI(HRES, HGRD, IPRM(IDEB2:IFIN2))

C---     CONSTRUIS ET INITIALISE LE PROXY
      IF (ERR_GOOD()) IERR = PR_PRXY_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PR_PRXY_INI(HOBJ, HRES)

C-------  IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_PRECOND[25] = ',
C     &                                 NPREC
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_REDEMARAGES[25] = ',
C     &                                 NRDEM
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_ITERATIONS[25] = ',
C     &                                 NITER
C         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C	     <comment>Return value: Handle on the preconditioner</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'ERR_PARAMETRES_INVALIDES',': ',
     &                  IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SKTP_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SKTP_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SKTP_REQCMD()
CDEC$ATTRIBUTES DLLEXPORT :: IC_SKTP_REQCMD

      IMPLICIT NONE

      INCLUDE 'icsktp.fi'
C-------------------------------------------------------------------------

C   <comment>
C   The command <b>skit_preconditioner</b> constructs the preconditioner for the
C   SPARSKIT iteration solver. (Status: prototype)
C   </comment>
      IC_SKTP_REQCMD = 'skit_preconditioner'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SKTP_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icsktp.hlp')

      RETURN
      END

