C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SKIT_000()
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_000

      IMPLICIT NONE

      INCLUDE 'mrskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_SKIT_NOBJMAX,
     &                   MR_SKIT_HBASE,
     &                   'MR_SKIT')

      DO I=1, MR_SKIT_NOBJMAX
         MR_SKIT_HMTX(I) = 0
         MR_SKIT_HASM(I) = 0
         MR_SKIT_HPRC(I) = 0
      ENDDO

      MR_SKIT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SKIT_999()
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_999

      IMPLICIT NONE

      INCLUDE 'mrskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrskit.fc'

      INTEGER  IERR
      EXTERNAL MR_SKIT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_SKIT_NOBJMAX,
     &                   MR_SKIT_HBASE,
     &                   MR_SKIT_DTR)

      MR_SKIT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR__(HOBJ,
     &                   MR_SKIT_NOBJMAX,
     &                   MR_SKITHBASEJ)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_SKIT_HVALIDE(HOBJ))
         IOB = HOBJ - MR_SKIT_HBASE
      ENDIF

      MR_SKIT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrskit.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_SKIT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ-MR_SKIT_HBASE,
     &                   MR_SKIT_NOBJMAX,
     &                   MR_SKIT_HBASE)

      MR_SKIT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensione
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_INI(HOBJ, HPRC, ISTR)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_INI

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HPRC
      CHARACTER*(*) ISTR

      INCLUDE 'mrskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ILU
      INTEGER HMTX
      INTEGER HASM
      PARAMETER (ILU = 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
CD     CALL ERR_PRE(PR_PREC_HVALIDE(HPRC))
D     CALL ERR_PRE(SP_STRN_LEN(ISTR) .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_SKIT_RST(HOBJ)

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     INITIALISE L'ASSEMBLEUR
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_SKIT_HBASE
         MR_SKIT_HMTX(IOB) = HMTX
         MR_SKIT_HASM(IOB) = HASM
         MR_SKIT_HPRC(IOB) = HPRC
      ENDIF

      MR_SKIT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
      INTEGER HASM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_SKIT_HBASE

      HASM = MR_SKIT_HASM(IOB)
      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)

      HMTX = MR_SKIT_HMTX(IOB)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

      MR_SKIT_HMTX(IOB) = 0
      MR_SKIT_HASM(IOB) = 0
      MR_SKIT_HPRC(IOB) = 0

      MR_SKIT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_SKIT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_SKIT_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrskit.fc'
C------------------------------------------------------------------------

      MR_SKIT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_SKIT_NOBJMAX,
     &                                  MR_SKIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction MR_LMPD_RESMTX résoud le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résoud directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     HSIM     HANDLE DE LA SIMULATION
C     HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C     F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE
C     F_KU     FONCTION A APPELER POUR ASSEMBLER LE PRODUIT K.U
C     F_F      FONCTION A APPELER POUR ASSEMBLER LE SECOND MEMBRE
C
C Sortie:
C     VSOL     SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     ITPRES,
     &                     VSOL)
CDEC$ATTRIBUTES DLLEXPORT :: MR_SKIT_XEQ

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  ITPRES
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrskit.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM
      INTEGER HMTX
      INTEGER HPRC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('MR_SKIT_XEQ: adapter aux modifs')
      CALL ERR_ASR(.FALSE.)

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SKIT_HBASE
      HMTX = MR_SKIT_HMTX(IOB)
      HASM = MR_SKIT_HASM(IOB)
      HPRC = MR_SKIT_HPRC(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))
CD     CALL ERR_ASR(PR_PREC_HVALIDE(HPRC))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND HALG .NE. 0)
     &   IERR = MA_MORS_ASMRHS(HASM, HSIM, HALG, F_F, VSOL)

C---     ASSEMBLE LE PRECONDITIONNEUR
      IF (ERR_GOOD()) IERR = PR_PREC_ASM(HPRC, HSIM, HALG, F_K, F_KU)

C---     RESOUS
      IF (ERR_GOOD()) IERR = MR_SKIT_RESMTX(HOBJ, ITPRES, VSOL)

      MR_SKIT_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction MR_SKIT_RESMTX resoud le système matriciel
C     pour le second membre qui lui est passé.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     ITPRES   TYPE DE RESOLUTION (MR_RESO_TYPERESO_U, MR_RESO_TYPERESO_DU)
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Sortie:
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_RESMTX(HOBJ, ITPRES, VFG)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      REAL*8   VFG(*)

      INCLUDE 'mrskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hsclim.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER HPRC
      INTEGER KA(1)
      INTEGER LDIMP
      INTEGER LDIMV
      INTEGER LIAP
      INTEGER LJAP
      INTEGER LKG
      INTEGER L_W
      INTEGER NDLT
      INTEGER NITER
      INTEGER NRDEM
      INTEGER NWORK

      INTEGER IPAR(16)
      REAL*8  FPAR(16)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

      CALL LOG_TODO('MR_SLUM_RESMTX: A REVISER, LES PARAM ONT CHANGE!')
      CALL ERR_ASR(.FALSE.)

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SKIT_HBASE
      HMTX  = MR_SKIT_HMTX(IOB)
      HPRC  = MR_SKIT_HPRC(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES VALEURS
      NDLT  = MX_MORS_REQDIM (HMTX)

C---     ALLOUE L'ESPACE DE TRAVAIL
      NITER = 10
      NRDEM = 10
      NWORK  = (NDLT+3)*(NITER+2) + (NITER+1)*NITER/2
      L_W = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NWORK, L_W)

C---     INITIALISE
      IPAR(1) = 0               ! ALWAYS 0 TO START AN ITERATIVE SOLVER
      IPAR(2) = 1               ! LEFT PRECONDITIONING
      IPAR(3) = 1               ! USE CONVERGENCE TEST SCHEME 1
      IPAR(4) = NWORK         ! THE 'W' HAS 10,000 ELEMENTS
      IPAR(5) = NITER         ! USE *GMRES(10) (E.G. FGMRES(10))
      IPAR(6) = NITER*NRDEM   ! USE AT MOST 100 MATVEC'S
      FPAR(1) = 1.0D-6         ! RELATIVE TOLERANCE 1.0E-6
      FPAR(2) = 1.0D-10       ! ABSOLUTE TOLERANCE 1.0E-10
      FPAR(11) = 0.0D0        ! CLEARING THE FLOPS COUNTER

C---     RESOUS PAR COMMUNICATION INVERSE
C      subroutine gmres(n, rhs, sol, ipar, fpar, w)
10    IF (ERR_BAD()) GOTO 19
      CALL GMRES(NDLT,
     &         VFG,     ! input
     &         VFG,     ! output sol,
     &         ipar,
     &         fpar,
     &         KA(SO_ALLC_REQIND(KA,L_W)))
      IF (IPAR(1).EQ.1) THEN
         IERR = MR_SKIT_MATMUL(HOBJ,
     &                         IPAR(8),
     &                         KA(SO_ALLC_REQIND(KA,L_W)),
     &                         IPAR(9),
     &                         KA(SO_ALLC_REQIND(KA,L_W)))
         GOTO 10
      ELSEIF (IPAR(1).EQ.2) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
c         call atmux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
c         goto 10
      ELSEIF (IPAR(1).EQ.3) THEN      ! left preconditioner solver
         IERR = PR_PREC_PRC(HPRC, VFG)
         GOTO 10
      ELSEIF (IPAR(1).EQ.4) THEN      ! left preconditioner transposed solve
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
         GOTO 10
      ELSEIF (IPAR(1).EQ.5) THEN      ! right preconditioner solve
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
         GOTO 10
      ELSEIF (IPAR(1).EQ.6) THEN      ! right preconditioner transposed solve
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
         GOTO 10
      ELSEIF (IPAR(1).EQ.10) THEN     !  call my own stopping test routine
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
         GOTO 10
      ELSEIF (IPAR(1).GT.0) THEN      !  ipar(1) is an unspecified code
         CALL ERR_ASG(ERR_ERR, 'ERR_MRSKIT_FNCT_ABSENTE')
         GOTO 10
      ELSEIF (IPAR(1).EQ.-1) THEN     ! iteration number greater than preset limit
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_MAX_ITER_ATTEINT')
      ELSEIF (IPAR(1).EQ.-2) THEN     ! insufficient work space
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_ESPACE_TRAVAIL_INSUFISANT')
      ELSEIF (IPAR(1).EQ.-3) THEN     ! anticipated break-down / divide by zero
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_DIVISION_ZERO')
      ELSEIF (IPAR(1).EQ.-4) THEN     ! fpar(1) and fpar(2) are both <= 0
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_PARAM_TOLERANCE_INVALIDES')
      ELSEIF (IPAR(1).EQ.-9) THEN     ! abnormal number detected
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_EXCEPTION_NUMERIQUE')
      ELSEIF (IPAR(1).EQ.-10)THEN     ! non-numerical reasons, e.g. invalid floating-point numbers
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_ERREUR_NON_NUMERIQUE')
      ELSEIF (IPAR(1).EQ.0)  THEN     !  normal termination
C
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_ERREUR_INCONNUE')
      ENDIF
19    CONTINUE

C---     IMPOSE LES CONDITIONS LIMITES
      IF (ERR_GOOD()) THEN
         LDIMP = HS_CLIM_REQLDIMP(HS_SIMD_REQHCLIM(HSIM))
         LDIMV = HS_CLIM_REQLDIMV(HS_SIMD_REQHCLIM(HSIM))
      ENDIF

C---     DESALLOUE L'ESPACE
      IF (L_W  .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_W)

      MR_SKIT_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction MR_SKIT_RESMTX resoud le système matriciel
C     pour le second membre qui lui est passé.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     HSIM     HANDLE DE LA SIMULATION
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Sortie:
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION MR_SKIT_MATMUL(HOBJ, II, VI, IO, VO)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  II
      REAL*8   VI(*)
      INTEGER  IO
      REAL*8   VO(*)

      INCLUDE 'mrskit.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER KA(1)
      INTEGER LIAP
      INTEGER LJAP
      INTEGER LKG
      INTEGER NDLT
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_SKIT_HBASE
      HMTX  = MR_SKIT_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      NDLT  = MX_MORS_REQDIM (HMTX)
      LIAP  = MX_MORS_REQLIAP(HMTX)
      LJAP  = MX_MORS_REQLJAP(HMTX)
      LKG   = MX_MORS_REQLKG (HMTX)

      CALL AMUX(NDLT,
     &          VI(II),
     &          VO(IO),
     &          KA(SO_ALLC_REQIND(KA,LKG)),
     &          KA(SO_ALLC_REQIND(KA,LJAP)),
     &          KA(SO_ALLC_REQIND(KA,LIAP)))

      MR_SKIT_MATMUL = ERR_TYP()
      RETURN
      END
