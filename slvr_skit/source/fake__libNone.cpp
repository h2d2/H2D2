//************************************************************************
// H2D2 - External declaration of public symbols
// Module: None
// Entry point: extern "C" void fake_dll_libNone()
//
// This file is generated automatically, any change will be lost.
// Generated 2008-01-16 21:56:16.923509
//************************************************************************
#include "cconfig.h"

#undef DSYM
#undef DNAM
#undef DNAM_UTL

#define DNAM_UTL(f) #f
#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define DSYM(fmaj, fmin) fmaj
#  define DNAM(fmaj, fmin) DNAM_UTL( fmaj )
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define DSYM(fmaj, fmin) fmin ## _
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## _ )
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define DSYM(fmaj, fmin) fmin ## __
#  define DNAM(fmaj, fmin) DNAM_UTL( fmin ## __ )
#else
#  error Invalid Fortran to C mangling convention
#endif

#ifdef __cplusplus
extern "C"
{
#endif


// ---  class PR_SKIT
void DSYM(PR_SKIT_000, pr_skit_000)();
void DSYM(PR_SKIT_999, pr_skit_999)();
void DSYM(PR_SKIT_CTR, pr_skit_ctr)();
void DSYM(PR_SKIT_DTR, pr_skit_dtr)();
void DSYM(PR_SKIT_INI, pr_skit_ini)();
void DSYM(PR_SKIT_RST, pr_skit_rst)();
void DSYM(PR_SKIT_HVALIDE, pr_skit_hvalide)();
void DSYM(PR_SKIT_ASM, pr_skit_asm)();
void DSYM(PR_SKIT_PRC, pr_skit_prc)();

// ---  class IC_SKTR
void DSYM(IC_SKTR_CMD, ic_sktr_cmd)();
void DSYM(IC_SKTR_REQCMD, ic_sktr_reqcmd)();

// ---  class IC_SKTP
void DSYM(IC_SKTP_CMD, ic_sktp_cmd)();
void DSYM(IC_SKTP_REQCMD, ic_sktp_reqcmd)();

// ---  class MR_SKIT
void DSYM(MR_SKIT_000, mr_skit_000)();
void DSYM(MR_SKIT_999, mr_skit_999)();
void DSYM(MR_SKIT_CTR, mr_skit_ctr)();
void DSYM(MR_SKIT_DTR, mr_skit_dtr)();
void DSYM(MR_SKIT_INI, mr_skit_ini)();
void DSYM(MR_SKIT_RST, mr_skit_rst)();
void DSYM(MR_SKIT_HVALIDE, mr_skit_hvalide)();
void DSYM(MR_SKIT_XEQ, mr_skit_xeq)();

void fake_dll_lib_reg(void (*)(), const char*, const char*);

#ifdef FAKE_DLL
void fake_dll_libNone()
{
   static char libname[] = "libNone.so";

   // ---  class PR_SKIT
   fake_dll_lib_reg(&DSYM(PR_SKIT_000, pr_skit_000), DNAM(PR_SKIT_000, pr_skit_000), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_999, pr_skit_999), DNAM(PR_SKIT_999, pr_skit_999), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_CTR, pr_skit_ctr), DNAM(PR_SKIT_CTR, pr_skit_ctr), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_DTR, pr_skit_dtr), DNAM(PR_SKIT_DTR, pr_skit_dtr), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_INI, pr_skit_ini), DNAM(PR_SKIT_INI, pr_skit_ini), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_RST, pr_skit_rst), DNAM(PR_SKIT_RST, pr_skit_rst), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_HVALIDE, pr_skit_hvalide), DNAM(PR_SKIT_HVALIDE, pr_skit_hvalide), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_ASM, pr_skit_asm), DNAM(PR_SKIT_ASM, pr_skit_asm), libname);
   fake_dll_lib_reg(&DSYM(PR_SKIT_PRC, pr_skit_prc), DNAM(PR_SKIT_PRC, pr_skit_prc), libname);

   // ---  class IC_SKTR
   fake_dll_lib_reg(&DSYM(IC_SKTR_CMD, ic_sktr_cmd), DNAM(IC_SKTR_CMD, ic_sktr_cmd), libname);
   fake_dll_lib_reg(&DSYM(IC_SKTR_REQCMD, ic_sktr_reqcmd), DNAM(IC_SKTR_REQCMD, ic_sktr_reqcmd), libname);

   // ---  class IC_SKTP
   fake_dll_lib_reg(&DSYM(IC_SKTP_CMD, ic_sktp_cmd), DNAM(IC_SKTP_CMD, ic_sktp_cmd), libname);
   fake_dll_lib_reg(&DSYM(IC_SKTP_REQCMD, ic_sktp_reqcmd), DNAM(IC_SKTP_REQCMD, ic_sktp_reqcmd), libname);

   // ---  class MR_SKIT
   fake_dll_lib_reg(&DSYM(MR_SKIT_000, mr_skit_000), DNAM(MR_SKIT_000, mr_skit_000), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_999, mr_skit_999), DNAM(MR_SKIT_999, mr_skit_999), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_CTR, mr_skit_ctr), DNAM(MR_SKIT_CTR, mr_skit_ctr), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_DTR, mr_skit_dtr), DNAM(MR_SKIT_DTR, mr_skit_dtr), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_INI, mr_skit_ini), DNAM(MR_SKIT_INI, mr_skit_ini), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_RST, mr_skit_rst), DNAM(MR_SKIT_RST, mr_skit_rst), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_HVALIDE, mr_skit_hvalide), DNAM(MR_SKIT_HVALIDE, mr_skit_hvalide), libname);
   fake_dll_lib_reg(&DSYM(MR_SKIT_XEQ, mr_skit_xeq), DNAM(MR_SKIT_XEQ, mr_skit_xeq), libname);
}
#endif    // FAKE_DLL

#ifdef __cplusplus
}
#endif

