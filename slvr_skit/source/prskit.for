C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SKIT_000()
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_000

      IMPLICIT NONE

      INCLUDE 'prskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_SKIT_NOBJMAX,
     &                   PR_SKIT_HBASE,
     &                   'PR_SKIT')

      DO I=1, PR_SKIT_NOBJMAX
         PR_SKIT_HGRD(I) = 0
         PR_SKIT_HMTX(I) = 0
         PR_SKIT_HASM(I) = 0
         PR_SKIT_ITYP(I) = PR_SKIT_TYP_INDEFINI
         PR_SKIT_LALU(I) = 0
         PR_SKIT_LJLU(I) = 0
         PR_SKIT_LJU (I) = 0
         PR_SKIT_ISTR(I) = ' '
      ENDDO

      PR_SKIT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SKIT_999()
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_999

      IMPLICIT NONE

      INCLUDE 'prskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      EXTERNAL PR_SKIT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_SKIT_NOBJMAX,
     &                   PR_SKIT_HBASE,
     &                   PR_SKIT_DTR)

      PR_SKIT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR__(HOBJ,
     &                   PR_SKIT_NOBJMAX,
     &                   PR_SKITHBASEJ)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_SKIT_HVALIDE(HOBJ))
         IOB = HOBJ - PR_SKIT_HBASE
      ENDIF

      PR_SKIT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prskit.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_SKIT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ-PR_SKIT_HBASE,
     &                   PR_SKIT_NOBJMAX,
     &                   PR_SKIT_HBASE)

      PR_SKIT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensione
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_INI(HOBJ, HGRD, ISTR)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_INI

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRD
      CHARACTER*(*) ISTR

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'grgrid.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ILU
      INTEGER IDEB2, IFIN2
      INTEGER ITYP
      INTEGER HMTX
      INTEGER HASM
      INTEGER LTYP
      CHARACTER*(16) MTYP
      PARAMETER (ILU = 0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
D     CALL ERR_PRE(GR_GRID_HVALIDE(HGRD))
D     CALL ERR_PRE(SP_STRN_LEN(ISTR) .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_SKIT_RST(HOBJ)

C---     NOM ET PARAMETRES DU PRECONDITIONNEUR
      IF (ERR_GOOD()) IERR = SP_STRN_TKS(ISTR, ',', 1, MTYP)
      IF (ERR_GOOD()) THEN
         CALL SP_STRN_TRM(MTYP)
         CALL SP_STRN_UCS(MTYP)
         LTYP = SP_STRN_LEN(MTYP)
         IF (LTYP .LE. 0) GOTO 9900
      ENDIF
      IF (ERR_GOOD()) THEN
         IDEB2 = SP_STRN_TOK(ISTR, ',', 2)
         IFIN2 = SP_STRN_LEN(ISTR)
         IF (IDEB2 .LE. 0) GOTO 9900
         IF (IFIN2 .LT. IDEB2) GOTO 9900
      ENDIF

C---     DÉDUIS LE TYPE
      IF (ERR_GOOD()) THEN
         ITYP = PR_SKIT_TYP_INDEFINI
         IF (MTYP(1:LTYP) .EQ. 'ILUT' ) ITYP = PR_SKIT_TYP_ILUT
         IF (MTYP(1:LTYP) .EQ. 'ILUTP') ITYP = PR_SKIT_TYP_ILUTP
         IF (MTYP(1:LTYP) .EQ. 'ILUD' ) ITYP = PR_SKIT_TYP_ILUD
         IF (MTYP(1:LTYP) .EQ. 'ILUDP') ITYP = PR_SKIT_TYP_ILUDP
         IF (MTYP(1:LTYP) .EQ. 'ILUK' ) ITYP = PR_SKIT_TYP_ILUK
         IF (MTYP(1:LTYP) .EQ. 'ILU0' ) ITYP = PR_SKIT_TYP_ILU0
         IF (MTYP(1:LTYP) .EQ. 'MILU0') ITYP = PR_SKIT_TYP_MILU0
         IF (ITYP .EQ. PR_SKIT_TYP_INDEFINI) GOTO 9901
      ENDIF

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     INITIALISE L'ASSEMBLEUR
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - PR_SKIT_HBASE
         PR_SKIT_HGRD(IOB) = HGRD
         PR_SKIT_HMTX(IOB) = HMTX
         PR_SKIT_HASM(IOB) = HASM
         PR_SKIT_ITYP(IOB) = ITYP
         PR_SKIT_LALU(IOB) = 0
         PR_SKIT_LJLU(IOB) = 0
         PR_SKIT_LJU (IOB) = 0
         PR_SKIT_ISTR(IOB) = ISTR(IDEB2:IFIN2)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'ERR_PARAMETRES_INVALIDES',': ',
     &                  ISTR(1:SP_STRN_LEN(ISTR))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_SKIT_PRECOND_INVALIDE',': ',
     &                      MTYP(1:SP_STRN_LEN(MTYP))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PR_SKIT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER HASM
      INTEGER LALU, LJLU, LJU
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_SKIT_HBASE

C---    RECUPERE LA MEMOIRE
      LALU = PR_SKIT_LALU(IOB)
      IF (LALU  .NE. 0) IERR = SO_ALLC_ALLRE8(0, LALU)
      LJLU = PR_SKIT_LJLU(IOB)
      IF (LJLU  .NE. 0) IERR = SO_ALLC_ALLIN4(0, LJLU)
      LJU  = PR_SKIT_LJU (IOB)
      IF (LJU   .NE. 0) IERR = SO_ALLC_ALLIN4(0, LJU)

C---     DETRUIS L'ASSEMBLEUR
      HASM = PR_SKIT_HASM(IOB)
      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)

C---     DETRUIS LA MATRICE
      HMTX = PR_SKIT_HMTX(IOB)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

C---     RESET
      PR_SKIT_HGRD(IOB) = 0
      PR_SKIT_HMTX(IOB) = 0
      PR_SKIT_HASM(IOB) = 0
      PR_SKIT_ITYP(IOB) = PR_SKIT_TYP_INDEFINI
      PR_SKIT_LALU(IOB) = 0
      PR_SKIT_LJLU(IOB) = 0
      PR_SKIT_LJU (IOB) = 0
      PR_SKIT_ISTR(IOB) = ' '

      PR_SKIT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_SKIT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SKIT_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prskit.fc'
C------------------------------------------------------------------------

      PR_SKIT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_SKIT_NOBJMAX,
     &                                  PR_SKIT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C  HSIM     HANDLE DE LA SIMULATION
C  HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C  F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE (DEPEND DE L'ELEMENT)
C  F_KU     FONCTION A APPELER POUR ASSEMBLER LA RESIDU  (DEPEND DE L'ELEMENT)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_ASM

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = PR_SKIT_ASMMTX(HOBJ,
     &                                      HSIM,
     &                                      HALG,
     &                                      F_K)

C---     FACTORISE LA MATRICE
      IF (ERR_GOOD()) IERR = PR_SKIT_FCTMTX(HOBJ)

      PR_SKIT_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C  HSIM     HANDLE DE LA SIMULATION
C  HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C  F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE (DEPEND DE L'ELEMENT)
C  F_KU     FONCTION A APPELER POUR ASSEMBLER LA RESIDU  (DEPEND DE L'ELEMENT)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      EXTERNAL F_K

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hssimd.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
D     CALL ERR_PRE(HS_SIMD_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SKIT_HBASE
      HMTX = PR_SKIT_HMTX(IOB)
      HASM = PR_SKIT_HASM(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

      PR_SKIT_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_FCTMTX(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITYP
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SKIT_HBASE
      ITYP = PR_SKIT_ITYP(IOB)

C---     DISPATCH
      IF     (ITYP .EQ. PR_SKIT_TYP_ILUT) THEN
         IERR = PR_SKIT_ILUT (HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_ILUTP) THEN
D        CALL ERR_ASR(.TRUE.)
C         IERR = PR_SKIT_ILUTP(HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_ILUD) THEN
D        CALL ERR_ASR(.TRUE.)
C         IERR = PR_SKIT_ILUD (HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_ILUDP) THEN
D        CALL ERR_ASR(.TRUE.)
C         IERR = PR_SKIT_ILUDP(HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_ILUK) THEN
D        CALL ERR_ASR(.TRUE.)
C         IERR = PR_SKIT_ILUK (HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_ILU0) THEN
D        CALL ERR_ASR(.TRUE.)
C         IERR = PR_SKIT_ILU0 (HOBJ)
      ELSEIF (ITYP .EQ. PR_SKIT_TYP_MILU0) THEN
         IERR = PR_SKIT_MILU0(HOBJ)
      ELSE
D        CALL ERR_ASR(.TRUE.)
      ENDIF

      PR_SKIT_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée PR_SKIT_ILUT implante la factorisation
C     imcompléte ILUT de SPARSKIT.
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C
C Sortie:
C
C Notes:
C     Le dimensionnement de la table de travail n'est pas exact.
C************************************************************************
      FUNCTION PR_SKIT_ILUT(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR, IERR_ILU
      INTEGER IOB
      INTEGER HMTX
      INTEGER LALU, LJLU, LJU
      INTEGER NDLT
      INTEGER LIAP, LJAP, LKG
      INTEGER IWK, L_JW, L_W
      INTEGER KA(1)

      INTEGER LFIL
      REAL*8  DROPTOL
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SKIT_HBASE
      HMTX = PR_SKIT_HMTX(IOB)
      LALU = PR_SKIT_LALU(IOB)
      LJLU = PR_SKIT_LJLU(IOB)
      LJU  = PR_SKIT_LJU (IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      NDLT = MX_MORS_REQDIM (HMTX)
      LIAP = MX_MORS_REQLIAP(HMTX)
      LJAP = MX_MORS_REQLJAP(HMTX)
      LKG  = MX_MORS_REQLKG (HMTX)

C---     PARAMETRES DU PRECONDITIONNEUR
      IF (ERR_GOOD())
     &   IERR = SP_STRN_TKI(PR_SKIT_ISTR(IOB), ',', 1, LFIL)
      IF (ERR_GOOD())
     &   IERR = SP_STRN_TKR(PR_SKIT_ISTR(IOB), ',', 2, DROPTOL)

C---     ALLOUE L'ESPACE POUR LA MATRICE PRECONDITIONNEE
      IWK = (2*LFIL)*NDLT     ! NON EXACT
      IF (LALU .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(IWK, LALU)
         IF (ERR_GOOD()) PR_SKIT_LALU(IOB) = LALU
      ENDIF
      IF (LJLU .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(IWK, LJLU)
         IF (ERR_GOOD()) PR_SKIT_LJLU(IOB) = LJLU
      ENDIF
      IF (LJU .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NDLT, LJU)
         IF (ERR_GOOD()) PR_SKIT_LJU(IOB) = LJU
      ENDIF

C---     ALLOUE L'ESPACE DE TRAVAIL
      L_JW = 0
      L_W = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NDLT+NDLT, L_JW)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLT+1, L_W)

C---     FACTORISE
      IERR_ILU = 0
      IF (ERR_GOOD()) THEN
         CALL ILUT(NDLT,
     &             KA(SO_ALLC_REQIND(KA,LKG)),
     &             KA(SO_ALLC_REQIND(KA,LJAP)),
     &             KA(SO_ALLC_REQIND(KA,LIAP)),
     &             LFIL,
     &             DROPTOL,
     &             KA(SO_ALLC_REQIND(KA,LALU)),
     &             KA(SO_ALLC_REQIND(KA,LJLU)),
     &             KA(SO_ALLC_REQIND(KA,LJU)),
     &             IWK,
     &             KA(SO_ALLC_REQIND(KA,L_JW)),
     &             KA(SO_ALLC_REQIND(KA,L_W)),
     &             IERR_ILU)
      ENDIF

C---     DESALLOUE L'ESPACE
      IF (L_W  .NE. 0) IERR = SO_ALLC_ALLRE8(0, L_W)
      IF (L_JW .NE. 0) IERR = SO_ALLC_ALLIN4(0, L_JW)

C---     ERREUR ILU
      IF     (IERR_ILU .GT.  0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_PIVOT_NUL')
      ELSEIF (IERR_ILU .EQ. -1) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_MATRICE_INVALIDE')
      ELSEIF (IERR_ILU .EQ. -2) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_DEBORDEMENT_L')
      ELSEIF (IERR_ILU .EQ. -3) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_DEBORDEMENT_U')
      ELSEIF (IERR_ILU .EQ. -4) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_PARAMETRE_LFIL_INVALIDE')
      ELSEIF (IERR_ILU .EQ. -5) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_LIGNE_NULLE')
      ENDIF

      PR_SKIT_ILUT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée PR_SKIT_MILU0 implante la factorisation
C     imcompléte MILU0 de SPARSKIT.
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_SKIT_MILU0(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR, IERR_ILU
      INTEGER IOB
      INTEGER HMTX
      INTEGER LALU, LJLU, LJU
      INTEGER NDLT, NKGP
      INTEGER LIAP, LJAP, LKG
      INTEGER L_JW
      INTEGER KA(1)
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SKIT_HBASE
      HMTX = PR_SKIT_HMTX(IOB)
      LALU = PR_SKIT_LALU(IOB)
      LJLU = PR_SKIT_LJLU(IOB)
      LJU  = PR_SKIT_LJU (IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      NDLT = MX_MORS_REQDIM (HMTX)
      NKGP = MX_MORS_REQNKGP(HMTX)
      LIAP = MX_MORS_REQLIAP(HMTX)
      LJAP = MX_MORS_REQLJAP(HMTX)
      LKG  = MX_MORS_REQLKG (HMTX)

C---     ALLOUE L'ESPACE POUR LA MATRICE PRECONDITIONNEE
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGP, LALU)
      IF (ERR_GOOD()) PR_SKIT_LALU(IOB) = LALU
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NKGP, LJLU)
      IF (ERR_GOOD()) PR_SKIT_LJLU(IOB) = LJLU
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NDLT, LJU)
      IF (ERR_GOOD()) PR_SKIT_LJU(IOB) = LJU

C---     ALLOUE L'ESPACE DE TRAVAIL
      L_JW = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NDLT, L_JW)

C---     FACTORISE
      IERR_ILU = 0
      IF (ERR_GOOD()) THEN
         CALL MILU0(NDLT,
     &              KA(SO_ALLC_REQIND(KA,LKG)),
     &              KA(SO_ALLC_REQIND(KA,LJAP)),
     &              KA(SO_ALLC_REQIND(KA,LIAP)),
     &              KA(SO_ALLC_REQIND(KA,LALU)),
     &              KA(SO_ALLC_REQIND(KA,LJLU)),
     &              KA(SO_ALLC_REQIND(KA,LJU)),
     &              KA(SO_ALLC_REQIND(KA,L_JW)),
     &              IERR_ILU)
      ENDIF

C---     DESALLOUE L'ESPACE
      IF (L_JW .NE. 0) IERR = SO_ALLC_ALLIN4(0, L_JW)

C---     ERREUR ILU
      IF (IERR_ILU .GT.  0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_SKIT_PIVOT_NUL')
      ENDIF

      PR_SKIT_MILU0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_SKIT_PRC(HOBJ, V)
CDEC$ATTRIBUTES DLLEXPORT :: PR_SKIT_PRC

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   V(*)

      INCLUDE 'prskit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'prskit.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LALU, LJLU, LJU
      INTEGER NDLT
      INTEGER KA(1)
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_SKIT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_SKIT_HBASE
      HMTX = PR_SKIT_HMTX(IOB)
      LALU = PR_SKIT_LALU(IOB)
      LJLU = PR_SKIT_LJLU(IOB)
      LJU  = PR_SKIT_LJU (IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(LALU .NE. 0)

C---     RECUPERE LES POINTEURS
      NDLT = MX_MORS_REQDIM (HMTX)

C---     PRECONDITIONNE
   CALL LUSOL(NDLT,
     &           V,      ! y   = the right-hand-side vector
     &           V,      ! solution of LU x = y
     &           KA(SO_ALLC_REQIND(KA,LALU)),
     &           KA(SO_ALLC_REQIND(KA,LJLU)),
     &           KA(SO_ALLC_REQIND(KA,LJU)))

      PR_SKIT_PRC = ERR_TYP()
      RETURN
      END

