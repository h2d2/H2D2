C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_MMPS_000
C     INTEGER MR_MMPS_999
C     INTEGER MR_MMPS_CTR
C     INTEGER MR_MMPS_DTR
C     INTEGER MR_MMPS_INI
C     INTEGER MR_MMPS_RST
C     INTEGER MR_MMPS_REQHBASE
C     LOGICAL MR_MMPS_HVALIDE
C     LOGICAL MR_MMPS_ESTDIRECTE
C     INTEGER MR_MMPS_ASMMTX
C     INTEGER MR_MMPS_FCTMTX
C     INTEGER MR_MMPS_RESMTX
C     INTEGER MR_MMPS_XEQ
C   Private:
C     INTEGER MR_MMPS_ERRMMPS
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction <code>MR_MMPS_000(...)</code> initialise les tables
C     internes de l'objet. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrmmps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_MMPS_NOBJMAX,
     &                   MR_MMPS_HBASE,
     &                   'Solver MUMPS')

      MR_MMPS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrmmps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER  IERR
      EXTERNAL MR_MMPS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_MMPS_NOBJMAX,
     &                   MR_MMPS_HBASE,
     &                   MR_MMPS_DTR)

      MR_MMPS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_MMPS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrmmps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_MMPS_NOBJMAX,
     &                   MR_MMPS_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_MMPS_HVALIDE(HOBJ))
         IOB = HOBJ - MR_MMPS_HBASE

         MR_MMPS_HMPS(IOB) = 0
         MR_MMPS_HMTX(IOB) = 0
         MR_MMPS_ICRC(IOB) = 0
      ENDIF

      MR_MMPS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_MMPS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrmmps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_MMPS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_MMPS_NOBJMAX,
     &                   MR_MMPS_HBASE)

      MR_MMPS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_MMPS_INI(HOBJ, NITER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NITER

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_mumps.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IOB
      INTEGER IERR, IRET
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (NITER .LT. 0) GOTO 9900

C---     RESET LES DONNEES
      IERR = MR_MMPS_RST(HOBJ)

C---     INITIALISE MUMPS
      IF (ERR_GOOD()) THEN
         IRET = F_MMPS_INITIALIZE()
         IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     INITIALISE LA MATRICE DISTRIBUÉE
      HMTX = 0
      IF (ERR_GOOD()) IERR = MX_DIST_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_DIST_INI(HMTX,MX_DIST_NUM_GLOBAL,.TRUE.) !! Avec données

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_MMPS_HBASE
         MR_MMPS_HMTX(IOB) = HMTX
         MR_MMPS_NITR(IOB) = NITER
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_ITERATION_INVALIDE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MR_MMPS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_MMPS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_mumps.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IOB
      INTEGER IERR, IRET
      INTEGER HASM, HMTX, HMPS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MR_MMPS_HBASE
      HMTX = MR_MMPS_HMTX(IOB)
      HMPS = MR_MMPS_HMPS(IOB)

C---     DETRUIS LA MATRICE DISTRIBUEE
      IF (MX_DIST_HVALIDE(HMTX)) IERR = MX_DIST_DTR(HMTX)

C---     DETRUIS MUMPS
      IF (HMPS .NE. 0) THEN
         IRET = F_MMPS_MAT_DESTROY(HMPS)
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     RESET MUMPS
      IF (HMTX .NE. 0 .OR. HMPS .NE. 0) THEN
         IRET = F_MMPS_FINALIZE()
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     RESET LES ATTRIBUTS
      MR_MMPS_HMPS(IOB) = 0
      MR_MMPS_HMTX(IOB) = 0
      MR_MMPS_ICRC(IOB) = 0

      MR_MMPS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_MMPS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrmmps.fi'
      INCLUDE 'mrmmps.fc'
C------------------------------------------------------------------------

      MR_MMPS_REQHBASE = MR_MMPS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_MMPS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrmmps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrmmps.fc'
C------------------------------------------------------------------------

      MR_MMPS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_MMPS_NOBJMAX,
     &                                  MR_MMPS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_MMPS_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrmmps.fi'
C------------------------------------------------------------------------

      MR_MMPS_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_MMPS_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX

      EXTERNAL MX_DIST_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_MMPS_HBASE
      HMTX  = MR_MMPS_HMTX(IOB)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HMTX))

C---     Dimensionne la matrice
      IF (ERR_GOOD()) IERR = MX_DIST_DIMMAT(HMTX, HSIM)

C---     Assemble la matrice
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_DIST_ASMKE)

!!!C---     Pivot min
!!!      IF (ERR_GOOD()) IERR = MX_DIST_DIAGMIN(HMTX, DMIN)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_MMPS_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_MMPS_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_mumps.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER HMPS
      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HMTX, HMMR
      INTEGER ICRC
      INTEGER LIAP, LJAP, LCOLG, LKG
      INTEGER NEQL, NEQT, NKGP
      LOGICAL DODIM, DOINIT
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     Récupère les attributs
      IOB = HOBJ - MR_MMPS_HBASE
      HMPS = MR_MMPS_HMPS(IOB)
      HMTX = MR_MMPS_HMTX(IOB)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HMTX))

C---     Récupère les attributs de la matrice distribuée
      LCOLG = MX_DIST_REQLCOLG(HMTX)
      NEQL  = MX_DIST_REQNEQL (HMTX)
      NEQT  = MX_DIST_REQNEQT (HMTX)

C---     Récupère les attributs de la matrice morse
      HMMR = MX_DIST_REQHMORS(HMTX)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMMR))
D     CALL ERR_ASR(NEQL .EQ. MX_DIST_REQNEQL(HMTX))
      ICRC = MX_MORS_REQCRC32(HMMR)
      NKGP = MX_MORS_REQNKGP (HMMR)
      LIAP = MX_MORS_REQLIAP (HMMR)
      LJAP = MX_MORS_REQLJAP (HMMR)
      LKG  = MX_MORS_REQLKG  (HMMR)

C---     Test si la matrice doit être dimensionnée
      DODIM = (ICRC .NE. MR_MMPS_ICRC(IOB))

C---     Reset MUMPS si redimensionnement
      IF (DODIM .AND. HMPS .NE. 0) THEN
         IRET = F_MMPS_MAT_DESTROY(HMPS)
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
         HMPS = 0
         IF (ERR_GOOD()) MR_MMPS_HMPS(IOB) = HMPS
      ENDIF

C---     Crée la matrice MUMPS
      DOINIT = .FALSE.
      IF (HMPS .EQ. 0) THEN
         IRET = F_MMPS_MAT_CREATE(HMPS, NKGP, MP_UTIL_REQCOMM())
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
         IF (ERR_GOOD()) MR_MMPS_HMPS(IOB) = HMPS
         DOINIT = .TRUE.
      ENDIF

C---     Analyse
      IF (ERR_GOOD() .AND. DOINIT) THEN
         IRET = F_MMPS_MAT_ANALIZE(HMPS,
     &                             NEQL,
     &                             NEQT,
     &                             NKGP,
     &                             KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                             KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                             KA(SO_ALLC_REQKIND(KA,LCOLG)))
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     Factorise
      IF (ERR_GOOD()) THEN
         IRET = F_MMPS_MAT_FACT   (HMPS,
     &                             NKGP,
     &                             VA(SO_ALLC_REQVIND(VA,LKG)))
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

C---     Conserve le CRC
      IF (ERR_GOOD()) THEN
         MR_MMPS_ICRC(IOB) = ICRC
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_MMPS_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résout le système matriciel
C
C Description:
C     La fonction MR_MMPS_RESMTX résout le système matriciel avec
C     le second membre qu'elle assemble. La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'f_mumps.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HMPS, HMTX
      INTEGER NITR
      INTEGER NEQL
      INTEGER LCOLG
      REAL*8  VDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Récupère les attributs
      IOB = HOBJ - MR_MMPS_HBASE
      HMPS = MR_MMPS_HMPS(IOB)
      HMTX = MR_MMPS_HMTX(IOB)
      NITR = MR_MMPS_NITR(IOB)
D     CALL ERR_ASR(HMPS .NE. 0)
D     CALL ERR_ASR(MX_DIST_HVALIDE(HMTX))

C---     Récupère les attributs de la matrice
      NEQL = MX_DIST_REQNEQL (HMTX)
      LCOLG= MX_DIST_REQLCOLG(HMTX)

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD()) IERR = F_F(HALG, VSOL, VDUM, .FALSE.)

C---     Résous
      IF (ERR_GOOD()) THEN
         IRET = F_MMPS_MAT_SOLVE(HMPS,
     &                           NITR,
     &                           NEQL,
     &                           KA(SO_ALLC_REQKIND(KA, LCOLG)),
     &                           VSOL)
         IF (IRET .NE. 0) IERR = MR_MMPS_ERRMMPS(HOBJ, IRET)
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_MMPS_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résout le système matriciel.
C
C Description:
C     La fonction MR_MMPS_XEQ résout complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C     Attention, on ne récupère que les DDL des noeuds du process.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_MMPS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF(ERR_GOOD())IERR=MR_MMPS_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF(ERR_GOOD())IERR=MR_MMPS_FCTMTX(HOBJ)

C---     RESOUS
      IF(ERR_GOOD())IERR=MR_MMPS_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_MMPS_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_MMPS_ERRMMPS(HOBJ, IRET)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IRET

      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_mumps.fi'
      INCLUDE 'mrmmps.fc'

      INTEGER HMPS
      INTEGER IOB
      INTEGER IDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (IRET .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_MUMPS_RUN_TIME_ERROR')

         IOB = HOBJ - MR_MMPS_HBASE
         HMPS = MR_MMPS_HMPS(IOB)

         IF (HMPS .NE. 0) THEN
            IDUM = F_MMPS_ERRMSG(HMPS, ERR_BUF)
            IF (IDUM .EQ. 0) CALL ERR_AJT(ERR_BUF)
         ENDIF
      ENDIF

      MR_MMPS_ERRMMPS = ERR_TYP()
      RETURN
      END
