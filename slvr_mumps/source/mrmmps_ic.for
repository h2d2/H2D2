C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_MR_MMPS_XEQCTR
C     INTEGER IC_MR_MMPS_XEQMTH
C     CHARACTER*(32) IC_MR_MMPS_REQCLS
C     INTEGER IC_MR_MMPS_REQHDL
C   Private:
C     INTEGER IC_MR_MMPS_PRN
C     SUBROUTINE IC_MR_MMPS_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_MR_MMPS_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_MR_MMPS_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'mrmmps_ic.fi'
      INCLUDE 'mrmmps.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrmmps_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HRES
      INTEGER NITER
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_MR_MMPS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les paramètres
      IF (SP_STRN_NTOK(IPRM, ',') .GT. 1) GOTO 9900

C---     Lis les paramètres
C     <comment>Number of refinement iterations (default 9)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, NITER)
      IF (IERR .NE. 0) NITER = 9

C---     Construis et initialise l'objet concret
      HRES = 0
      IF (ERR_GOOD()) IERR = MR_MMPS_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_MMPS_INI(HRES, NITER)

C---     Construis et initialise le parent-proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = MR_RESO_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = MR_RESO_INI(HOBJ, HRES)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_MR_MMPS_PRN(HRES)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the resolution method</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HRES
      ENDIF

C<comment>
C  The constructor <b>mumps_linear_solver</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_MR_MMPS_AID()

9999  CONTINUE
      IC_MR_MMPS_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_MR_MMPS_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_MR_MMPS_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrmmps.fi'
      INCLUDE 'mrmmps.fc'
      INCLUDE 'mrmmps_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NITR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

!---     Prototype avec les objets qui s'impriment -- L'avenir !!!
!     ierr = mr_mmps_prn(hobj)

C---     Récupère les attributs
      IOB  = HOBJ - MR_MMPS_HBASE
      NITR = MR_MMPS_NITR(IOB)

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_MUMPS'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des paramètres du bloc
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,I6)') 'MSG_NITER_RAFF#<25>#= ', NITR
     &
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_MR_MMPS_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_MR_MMPS_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_MR_MMPS_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'mrmmps_ic.fi'
      INCLUDE 'mrmmps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrmmps_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = MR_MMPS_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(MR_MMPS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = MR_MMPS_PRN(HOBJ)
         IERR = IC_MR_MMPS_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_MR_MMPS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_MR_MMPS_AID()

9999  CONTINUE
      IC_MR_MMPS_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_MR_MMPS_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_MR_MMPS_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrmmps_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>mumps_linear_solver</b> represents the distributed memory
C  linear solver MUMPS, an external package. The class makes the bridge to
C  this package.
C</comment>
      IC_MR_MMPS_REQCLS = 'mumps_linear_solver'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_MR_MMPS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_MR_MMPS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrmmps_ic.fi'
      INCLUDE 'mrmmps.fi'
C-------------------------------------------------------------------------

      IC_MR_MMPS_REQHDL = MR_MMPS_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_MR_MMPS_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('mrmmps_ic.hlp')
      RETURN
      END

