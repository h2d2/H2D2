C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Age d'une particule (AGE)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_CCN_HLPPRNO
C
C Description:
C     Aide sur les propriétés globales
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_CCN_HLPPRNO()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CCN_HLPPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_bse.fi'
C-----------------------------------------------------------------------

C---     Appel le parent
      CALL CD2D_BSE_HLPPRNO()

      RETURN
      END

