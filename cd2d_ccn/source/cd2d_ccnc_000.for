C************************************************************************
C --- Copyright (c) INRS 2013-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Équation de convection-diffusion eulérienne 2-D
C     Contaminant conservatif (CCN)
C     Formulation conservative pour (QC).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C ACTIONS   ASM   ASSEMBLE
C           REQ   REQUETE
C           PRN   PRINT
C           CLC   CALCULE
C
C************************************************************************

C************************************************************************
C Sommaire: Block d'initialisation.
C
C Description:
C     Le block data privé <code>CD2D_CCNC_DATA_000()</code> initialise
C     les attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA CD2D_CCNC_DATA_000

      INCLUDE 'cd2d_ccnc.fc'

      DATA CD2D_CCNC_HBASE /0/

      END

C************************************************************************
C Sommaire: CD2D_CCNC_000
C
C Description:
C     La fonction CD2D_CCNC_000 est la fonction d'initialisation de
C     bas-niveau d'un module H2D2. Elle est appelée lors de l'enregistrement
C     du module.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_CCNC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CCNC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_ccnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cd2d_ccnc.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
C     CALL ERR_PRE(.NOT. SO_VTBL_HVALIDE(CD2D_CCNC_HVFT))
C-----------------------------------------------------------------------

C---     Enregistre la classe
      IERR = OB_OBJN_000(CD2D_CCNC_HBASE,
     &                   'Convection-Diffusion 2D - ' //
     &                   'Conservative contaminant -' //
     &                   'Conservative formulation')

      CD2D_CCNC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CD2D_CCNC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CD2D_CCNC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_CCNC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cd2d_ccnc.fi'
      INCLUDE 'cd2d_ccnc.fc'
C------------------------------------------------------------------------

      CD2D_CCNC_REQHBASE = CD2D_CCNC_HBASE
      RETURN
      END

