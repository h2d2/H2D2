class adaptative_relaxation
===========================
 
   The class **adaptative_relaxation** represents an adaptative relaxation
   coefficient to be applied to the increment (descent direction) of an
   algorithm like Picard or Newton. **adaptative_relaxation** is adapted from
   "MODFLOW-2000, THE U.S. GEOLOGICAL SURVEY MODULAR GROUND-WATER MODEL - USER
   GUIDE TO THE LINK-AMG (LMG) PACKAGE FOR SOLVING MATRIX EQUATIONS USING AN
   ALGEBRAIC MULTIGRID SOLVER, By STEFFEN W. MEHL and MARY C. HILL." (Status:
   Experimental)
    
   constructor handle adaptative_relaxation(dmin, dmax)
      The constructor **adaptative_relaxation** constructs an object, with the
      given arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the increment dU
                           adaptative
                               relaxation algorithm
         double   dmin        Minimal relaxation coefficient (default 0.1)
         double   dmax        Maximal relaxation coefficient (default 1.0)
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
