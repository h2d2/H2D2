Structure matricielle distribuée:
================================

Chaque noeud de calcul possède deux types de noeuds:
    des noeuds privés et
    des noeuds partagés avec d'autres noeuds de calcul
Les noeuds partagés correspondent aux noeuds des éléments nécessaires pour
assembler complètement des noeuds privés.
    NNP : Nombre de noeuds privés
    NNL : Nombre de nooeuds local à un processus
    NNT : Nombre de noeuds total

    On a NNT = sum(NNP)

De maniêre équivalente on définit:
    NDLP = NNP * NDLN
    NDLL = NNL * NDLN
    NDLT = NNT * NDLN

On aura également:
    NEQP    : Nombre d'équations privées
    NEQL    : Nombre d'équations locales
    NEQT    : Nombre d'équations total


Pour chaque noeuds de calcul, la matrice locale a pour dimensions
NEQL(lignes) x NEQL (colonnes), matrice carrée qui comprend tous les termes
assemblés. De cette matrice, seules les lignes des NEQP sont complètes.
La sous-matrice qui correspond à une portion de la matrice globale a alors pour
dimensions NEQP x NEQL.

Pour PETSc:
    En numérotation globale, les ddl privés doivent former un bloc sans trous et
    être en ordre croissant.


Après une phase de résolution, VDEL contient soit NEQP soit NEQL valeurs mises à
jour. Les (NEQL-NEQP) valeurs qui correspondent aux noeuds partagés vont être
modifiées lors de la synchro.
    NEQL (ou NEQP) modifiées par la résolution
    NEQL -> NDLL par NEQ2NDLT
    synchro écrase les noeuds (NNL-NNP) partagés avec les info des autres process
 