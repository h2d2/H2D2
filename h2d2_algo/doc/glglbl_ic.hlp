module glob
===========
 
   The module **glob** regroups the globalization algorithms.
    
   function handle glob_compose(kglbl(i))
      The class **glob_compose** represents the composition of globalization
      algorithm.
         handle   HOBJ        Return value: Handle on the algorithm
         integer  kglbl(i)    List of the handles on the globalization
                           algorithms, comma separated
    
   function handle glob_limiter(hele, vval(i-1))
      The class **glob_limiter** represents for each degree of freedom, the
      limiting values for the increments du and for the resulting value u on
      an iteration during a resolution. 
      du is limited in such a way that 
          
          umin < u+du < umax
      and 
          
          |du| < uinc
      .
         handle   HOBJ        Return value: Handle on the limiter
         handle   hele        Handle on the element
         double   vval(i-1)   List of limit values, comma separated. This can
                           be either the list
                              of limiting increments uinc, or a list of
                           triplets umin, umax, uinc,
                              for each degree of freedom.
    
   function handle backtracking(npas, tmin, tmax)
      The class **backtracking** represents a globalization algorithm by
      backtracking. It is a line search for the minimum residual along the
      descent direction represented by the increment on the solution. The
      search is exhaustive with **npas** regular steps between **tmin** and
      **tmax**. We seek **tmin** < t < **tmax** such as to minimize the
      residual R (u + t du).
         handle   HOBJ        Return value: Handle on the globalization
                           algorithm by backtracking
         integer  npas        Number of step (default 20)
         double   tmin        Min Theta (default 0.0)
         double   tmax        Max Theta (default 1.0)
    
   function handle backtracking_2(thmin, thmax)
      The class **backtracking_2** represents a globalization algorithm by
      backtracking. It is a search by second order polynomial approximation
      for the minimum residual along the descent direction represented by the
      increment on the solution. We seek **thmin** < t < **thmax** such as to
      minimize the residual R (u + t du).
         handle   HOBJ        Return value: Handle on the globalization
                               algorithm by second order backtracking
         double   thmin       Theta min (default 1.0D-3)
         double   thmax       Theta max (default 1.0)
    
   function handle backtracking_3(thmin, thmax)
      The class **backtracking_3** represents a globalization algorithm by
      backtracking. It is a search by third order polynomial approximation for
      the minimum residual along the descent direction represented by the
      increment on the solution. We seek **thmin** < t < **thmax** such as to
      minimize the residual R (u + t du).
         handle   HOBJ        Return value: Handle on the globalization
                               algorithm by third order backtracking
         double   thmin       Theta min (default 1.0D-3)
         double   thmax       Theta max (default 1.0)
    
   function handle constant_relaxation(rlx)
      The class **constant_relaxation** represents a constant relaxation
      coefficient to be applied to the increment (descent direction) of an
      algorithm like Picard or Newton.
         handle   HOBJ        Return value: Handle on the increment dU
                           constant
                               relaxation algorithm
         double   rlx         Constant relaxation coefficient
    
   function handle adaptative_relaxation(dmin, dmax)
      The class **adaptative_relaxation** represents an adaptative relaxation
      coefficient to be applied to the increment (descent direction) of an
      algorithm like Picard or Newton. **adaptative_relaxation** is adapted
      from "MODFLOW-2000, THE U.S. GEOLOGICAL SURVEY MODULAR GROUND-WATER
      MODEL - USER GUIDE TO THE LINK-AMG (LMG) PACKAGE FOR SOLVING MATRIX
      EQUATIONS USING AN ALGEBRAIC MULTIGRID SOLVER, By STEFFEN W. MEHL and
      MARY C. HILL." (Status: Experimental)
         handle   HOBJ        Return value: Handle on the increment dU
                           adaptative
                               relaxation algorithm
         double   dmin        Minimal relaxation coefficient (default 0.1)
         double   dmax        Maximal relaxation coefficient (default 1.0)
    
   function handle gl_bank_rose(delta)
      The class **gl_bank_rose** represents a globalization algorithm by
      backtracking. It is a search by second order polynomial approximation
      for the minimum residual along the descent direction represented by the
      increment on the solution. We seek **thmin** < t < **thmax** such as to
      minimize the residual R (u + t du).
         handle   HOBJ        Return value: Handle on the globalization
                               algorithm by second order backtracking
         double   delta       Delta (default 1.0)
    
   function handle damper(rmin(iv), rmax(iv), amin(iv), amax(iv), ilog)
      The class **damper** is to be used with a damped solver like
      `newton_damped` algorithm. The damping criterion is nodal and calculated
      for each dof. It is based on the residuum. (Status: Experimental)
         handle   HOBJ        Return value: Handle on the damper
                               to be used in an algorithm with damping
         double   rmin(iv)    List of residual and damping coefficient, comma
                           separated.
                              For each degree of freedom, 4 values for:  
          
                             
                             
           * Res min (default 1.0)
                             
           * Res max (default 100.0)
                             
           * Alfa min (default 0.0)
                             
           * Alfa max (default 1.0e-2)
         integer  ilog        Logarithmic variation (default 0 = False =
                           Linear)
    
