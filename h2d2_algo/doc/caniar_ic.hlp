class cria_infinity_1ddlrel
===========================
 
   The class **cria_infinity_1ddlrel** represents the stopping criterion in
   infinity norm, for one degree of freedom, for a relative increase. The
   condition on the solution increment is: 
       
          || dU / (eps_r*|u| + eps_a) || < 1.0 
   On a dof, it can be expressed as: 
       
          dU < eps_r*|u| + eps_a 
    
   constructor handle cria_infinity_1ddlrel(hele, iddl, epsr, epsa)
      The constructor **cria_infinity_1ddlrel** constructs an object, with the
      given arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the stopping criterion
         handle   hele        Handle on the element
         integer  iddl        Index of the degree of freedom to control
         double   epsr        Relative epsilon
         double   epsa        Absolute epsilon
    
   property ddl
      Index of the degree of freedom to control
    
   property eps_rel
      Relative epsilon
    
   property eps_abs
      Absolute epsilon
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
