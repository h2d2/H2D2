class glob_limiter
==================
 
   The class **glob_limiter** represents for each degree of freedom, the
   limiting values for the increments du and for the resulting value u on an
   iteration during a resolution. 
   du is limited in such a way that 
       
       umin < u+du < umax
   and 
       
       |du| < uinc
   .
    
   constructor handle glob_limiter(hele, vval(i-1))
      The constructor **limiter** constructs an object, with the given
      arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the limiter
         handle   hele        Handle on the element
         double   vval(i-1)   List of limit values, comma separated. This can
                           be either the list
                              of limiting increments uinc, or a list of
                           triplets umin, umax, uinc,
                              for each degree of freedom.
    
   operator (get) op[]
      Get a limiter value with its index (rvalue) (v=h_lim[2])
    
   getter helem
      Handle on the element
    
   getter ndln
      Number of degree of freedom per node
    
   getter nval
      Number of values
    
   operator (set) op[]
      Set a limiter value with its index (lvalue) (h_lim[2]=v)
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
