class cria_l2_allrel
====================
 
   The class **cria_ l2_allrel** represents the stopping criterion in L2 norm,
   for all degrees of freedom, for a relative increase. The condition on the
   solution increment is: 
       
          || dU / (eps_r*|u| + eps_a) || < 1.0 
   On a dof, it can be expressed as: 
       
          dU < eps_r*|u| + eps_a 
    
   constructor handle cria_l2_allrel(hele, vepsr(iv), vepsa(iv))
      The constructor **cria_l2_allrel** constructs an object, with the given
      arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the stopping criterion
         handle   hele        Handle on the element
         double   vepsr(iv)   List of epsilon, comma separated. For each
                           degree of freedom,
                              2 values for relative and absolute epsilon. The
                           special case
                              with only 2 values will attribute the same
                           values to all
                              degree of freedom.
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
