C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PR_ILUN_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_ILUN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prilun.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prilun.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_ILUN_NOBJMAX,
     &                   PR_ILUN_HBASE,
     &                   'Preconditioner ILU(n)')

      DO I=1, PR_ILUN_NOBJMAX
         PR_ILUN_HMTX(I) = 0
         PR_ILUN_HASM(I) = 0
      ENDDO

      PR_ILUN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_ILUN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prilun.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prilun.fc'

      INTEGER  IERR
      EXTERNAL PR_ILUN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_ILUN_NOBJMAX,
     &                   PR_ILUN_HBASE,
     &                   PR_ILUN_DTR)

      PR_ILUN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prilun.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_ILUN_NOBJMAX,
     &                   PR_ILUN_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_ILUN_HVALIDE(HOBJ))
         IOB = HOBJ - PR_ILUN_HBASE
      ENDIF

      PR_ILUN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prilun.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_ILUN_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_ILUN_NOBJMAX,
     &                   PR_ILUN_HBASE)

      PR_ILUN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_INI(HOBJ, ILU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ILU

      INCLUDE 'prilun.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_ILUN_RST(HOBJ)

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, ILU)

C---     INITIALISE L'ASSEMBLEUR
      IF (ERR_GOOD()) IERR = MA_MORS_CTR(HASM)
      IF (ERR_GOOD()) IERR = MA_MORS_INI(HASM, HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - PR_ILUN_HBASE
         PR_ILUN_HMTX(IOB) = HMTX
         PR_ILUN_HASM(IOB) = HASM
      ENDIF

      PR_ILUN_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HASM
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_ILUN_HBASE

      HASM = PR_ILUN_HASM(IOB)
      IF (MA_MORS_HVALIDE(HASM)) IERR = MA_MORS_DTR(HASM)

      HMTX = PR_ILUN_HMTX(IOB)
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

      PR_ILUN_HMTX(IOB) = 0
      PR_ILUN_HASM(IOB) = 0

      PR_ILUN_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PR_ILUN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_ILUN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prilun.fi'
      INCLUDE 'prilun.fc'
C------------------------------------------------------------------------

      PR_ILUN_REQHBASE = PR_ILUN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_ILUN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_ILUN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prilun.fc'
C------------------------------------------------------------------------

      PR_ILUN_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_ILUN_NOBJMAX,
     &                                  PR_ILUN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble et factorise le préconditionnement.
C
C Description:
C     La fonction PR_ILUN_ASM assemble et factorise la matrice de
C     préconditionnement.
C     La matrice est dimensionnée et assemblée, puis factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle de la simulation
C     HALG     Handle de l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prilun.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mamors.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HASM
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_ILUN_HBASE
      HMTX = PR_ILUN_HMTX(IOB)
      HASM = PR_ILUN_HASM(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D     CALL ERR_ASR(MA_MORS_HVALIDE(HASM))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MA_MORS_ASMMTX(HASM, HSIM, HALG, F_K)

C---     FACTORISE LA MATRICE
      IF (ERR_GOOD()) IERR = PR_ILUN_FCTMTX(HOBJ)

      PR_ILUN_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise la matrice
C
C Description:
C     La fonction PR_ILUN_FCMTX factorise la matrice.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_FCTMTX(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LJDP, LIAP, LJAP, LKG
      INTEGER L_JR, L_JR2
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_ILUN_HBASE
      HMTX  = PR_ILUN_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         LJDP = MX_MORS_REQLJDP(HMTX)
         LIAP = MX_MORS_REQLIAP(HMTX)
         LJAP = MX_MORS_REQLJAP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)
      ENDIF

C---     ALLOUE LES TABLES DE TRAVAIL
      L_JR  = 0
      L_JR2 = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, L_JR)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, L_JR2)

C---     FACTORISE
      IF (ERR_GOOD()) THEN
         IERR = SP_MORS_FACT(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,L_JR)),
     &                       KA(SO_ALLC_REQKIND(KA,L_JR2)),
     &                       KA(SO_ALLC_REQKIND(KA,LJDP)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKG)))
      ENDIF

C---     DESALLOUE LES TABLES DE TRAVAIL
      IF (L_JR2 .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JR2)
      IF (L_JR  .NE. 0) IERR = SO_ALLC_ALLINT(0, L_JR)

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      PR_ILUN_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction PR_ILUN_RESMTX resoud le système matriciel
C     pour le second membre qui lui est passé. La matrice doit
C     être factorisée.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     NEQ      DIMENSION DU SECOND MEMBRE
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Sortie:
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION PR_ILUN_PRC(HOBJ, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VFG(*)

      INCLUDE 'prilun.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LJDP, LIAP, LJAP, LKG
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_ILUN_HBASE
      HMTX  = PR_ILUN_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     RECUPERE LES DONNÉES
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         LJDP = MX_MORS_REQLJDP(HMTX)
         LIAP = MX_MORS_REQLIAP(HMTX)
         LJAP = MX_MORS_REQLJAP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)
      ENDIF

C---     RESOUS
      IF (ERR_GOOD()) THEN
         IERR = SP_MORS_SOLV(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LJDP)),
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       VFG)
      ENDIF

      PR_ILUN_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_ILUN_REQILU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_ILUN_REQILU
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prilun.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'prilun.fc'

      INTEGER IOB
      INTEGER HMTX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_ILUN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_ILUN_HBASE
      HMTX  = PR_ILUN_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

      PR_ILUN_REQILU = MX_MORS_REQILU(HMTX)
      RETURN
      END

