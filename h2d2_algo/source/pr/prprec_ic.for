C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRPREC_REQMDL(ISTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRPREC_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) ISTR

      INCLUDE 'prprec_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(ISTR) .GT. 0) THEN
         IF (ISTR(1:SP_STRN_LEN(ISTR)) .EQ. 'help') THEN
            CALL IC_PRPREC_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(ISTR) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_PRPREC_REQHDL()
         WRITE(ISTR, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  <b>prec</b> returns the handle on the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       ISTR(1:SP_STRN_LEN(ISTR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRPREC_AID()

9999  CONTINUE
      IC_PRPREC_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PRPREC_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRPREC_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'prprec_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'iciden.fi'
      INCLUDE 'icprilun.fi'
      INCLUDE 'icprktd.fi'
      INCLUDE 'icprmad.fi'
      INCLUDE 'prslvr_ic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'prprec_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_PRPREC_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <include>IC_PRIDEN_REQCLS@iciden.for</include>
      IF (IMTH .EQ. 'pr_iden') THEN
D        CALL ERR_ASR(IC_PRIDEN_REQCLS() .EQ. 'pr_iden')
C        <include>IC_PRIDEN_XEQCTR@iciden.for</include>
         IERR = IC_PRIDEN_XEQCTR(IPRM)

C     <include>IC_PRILUN_REQCLS@icprilun.for</include>
      ELSEIF (IMTH .EQ. 'pr_ilun') THEN
D        CALL ERR_ASR(IC_PRILUN_REQCLS() .EQ. 'pr_ilun')
C        <include>IC_PRILUN_XEQCTR@icprilun.for</include>
         IERR = IC_PRILUN_XEQCTR(IPRM)

C     <include>IC_PRKTD_REQCMD@icprktd.for</include>
      ELSEIF (IMTH .EQ. 'pr_kt_diag') THEN
D        CALL ERR_ASR(IC_PRKTD_REQCMD() .EQ. 'pr_kt_diag')
C        <include>IC_PRKTD_CMD@icprktd.for</include>
         IERR = IC_PRKTD_CMD(IPRM)

C     <include>IC_PRMASD_REQCMD@icprmad.for</include>
      ELSEIF (IMTH .EQ. 'pr_m_diag') THEN
D        CALL ERR_ASR(IC_PRMASD_REQCMD() .EQ. 'pr_m_diag')
C        <include>IC_PRMASD_CMD@icprmad.for</include>
         IERR = IC_PRMASD_CMD(IPRM)

C     <include>IC_PRSLVR_REQCLS@prslvr_ic.for</include>
      ELSEIF (IMTH .EQ. 'pr_slvr') THEN
D        CALL ERR_ASR(IC_PRSLVR_REQCLS() .EQ. 'pr_slvr')
C        <include>IC_PRSLVR_XEQCTR@prslvr_ic.for</include>
         IERR = IC_PRSLVR_XEQCTR(IPRM)

      ELSE
         GOTO 9903
      ENDIF


      GOTO 9999
C------------------------------------------------------------------------
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PRPREC_AID()

9999  CONTINUE
      IC_PRPREC_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRPREC_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRPREC_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prprec_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>prec</b> regroups the preconditionner.
C</comment>
      IC_PRPREC_REQNOM = 'prec'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PRPREC_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PRPREC_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prprec_ic.fi'
C-------------------------------------------------------------------------

      IC_PRPREC_REQHDL = 1999032000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PRPREC_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('prprec_ic.hlp')

      RETURN
      END
