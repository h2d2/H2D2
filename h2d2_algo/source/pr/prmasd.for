C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: PR
C      H2D2 Class: PR_MASD
C         INTEGER PR_MASD_000
C         INTEGER PR_MASD_999
C         INTEGER PR_MASD_CTR
C         INTEGER PR_MASD_DTR
C         INTEGER PR_MASD_INI
C         INTEGER PR_MASD_RST
C         INTEGER PR_MASD_REQHBASE
C         LOGICAL PR_MASD_HVALIDE
C         INTEGER PR_MASD_ASM
C         INTEGER PR_MASD_PRC
C         INTEGER PR_MASD_ASMM
C         INTEGER PR_MASD_DUMMY
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PR_MASD_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prmasd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_MASD_NOBJMAX,
     &                   PR_MASD_HBASE,
     &                   'Preconditioner Mass Diagonal')

      PR_MASD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prmasd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prmasd.fc'

      INTEGER  IERR
      EXTERNAL PR_MASD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_MASD_NOBJMAX,
     &                   PR_MASD_HBASE,
     &                   PR_MASD_DTR)

      PR_MASD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prmasd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_MASD_NOBJMAX,
     &                   PR_MASD_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_MASD_HVALIDE(HOBJ))
         IOB = HOBJ - PR_MASD_HBASE

         PR_MASD_HRES(IOB) = 0
      ENDIF

      PR_MASD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prmasd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prmasd.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_MASD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_MASD_NOBJMAX,
     &                   PR_MASD_HBASE)

      PR_MASD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_INI(HOBJ, IOPR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPR

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_MASD_RST(HOBJ)

C---     INITIALISE LA METHODE DE RÉSOLUTION
      IF (ERR_GOOD()) IERR = MR_LMPD_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_LMPD_INI(HRES,
     &                                   IOPR,     ! Op. de lump
     &                                   1,        ! NITER
     &                                   1.0D-32,  ! EPS0
     &                                   1.0D-32)  ! EPS1

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - PR_MASD_HBASE
         PR_MASD_HRES(IOB) = HRES
      ENDIF

      PR_MASD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_MASD_HBASE

      HRES = PR_MASD_HRES(IOB)
      IF (MR_LMPD_HVALIDE(HRES)) IERR = MR_LMPD_DTR(HRES)

      PR_MASD_HRES(IOB) = 0

      PR_MASD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PR_MASD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prmasd.fi'
      INCLUDE 'prmasd.fc'
C------------------------------------------------------------------------

      PR_MASD_REQHBASE = PR_MASD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_MASD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prmasd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prmasd.fc'
C------------------------------------------------------------------------

      PR_MASD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_MASD_NOBJMAX,
     &                                  PR_MASD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble et factorise le préconditionnement.
C
C Description:
C     La fonction PR_MASD_ASM assemble et factorise la matrice de
C     préconditionnement.
C     La matrice est dimensionnée et assemblée, puis factorisée.
C     <p>
C     Les paramètres HALG, F_K et F_KU ne sont pas utilisés. On utilise
C     une fonction local pour assembler la matrice masse.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle de la simulation
C     HALG     Handle de l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HRES
      EXTERNAL PR_MASD_ASMM
      EXTERNAL PR_MASD_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_MASD_HBASE
      HRES = PR_MASD_HRES(IOB)
D     CALL ERR_ASR(MR_LMPD_HVALIDE(HRES))

C---     CONSERVE LA SIMULATION POUR ASMM
      PR_MASD_HSIM(IOB) = HSIM

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) THEN
         IERR = MR_LMPD_ASMMTX(HRES, HSIM,
     &                         HOBJ, PR_MASD_ASMM, PR_MASD_DUMMY)
      ENDIF

C---     FACTORISE LA MATRICE
      IF (ERR_GOOD()) IERR = MR_LMPD_FCTMTX(HRES)

      PR_MASD_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction PR_MASD_RESMTX resoud le système matriciel
C     pour le second membre qui lui est passé. La matrice doit
C     être factorisée.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     NEQ      DIMENSION DU SECOND MEMBRE
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Sortie:
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION PR_MASD_PRC(HOBJ, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_MASD_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VFG(*)

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSLVR
      INTEGER HSIM, HALG
      EXTERNAL ERR_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_MASD_HBASE
      HSLVR = PR_MASD_HRES(IOB)
D     CALL ERR_ASR(MR_LMPD_HVALIDE(HSLVR))

C---     RESOUS
      HSIM = 0
      HALG = 0
      IF (ERR_GOOD()) THEN
         IERR = MR_LMPD_RESMTX(HSLVR,HSIM,HALG,ERR_DUMMY,VFG)
      ENDIF

      PR_MASD_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice masse.
C
C Description:
C
C Entrée:
C     INTEGER  HOBJ     ! Handle sur l'objet courant
C     INTEGER  HMTX     ! Handle sur la matrice, param de F_ASM
C     INTEGER  F_ASM    ! Fonction d'assemblage
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_ASMM(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'prmasd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - PR_MASD_HBASE
      HSIM = PR_MASD_HSIM(IOB)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_HELE_ASMM(HSIM, HMTX, F_ASM)

      PR_MASD_ASMM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice masse.
C
C Description:
C
C Entrée:
C     INTEGER  HOBJ     ! Handle sur l'objet courant
C     INTEGER  HMTX     ! Handle sur la matrice, param de F_ASM
C     INTEGER  F_ASM    ! Fonction d'assemblage
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_MASD_DUMMY(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'prmasd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'prmasd.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_MASD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_ECRIS('Appel interdit')
      CALL ERR_ASR(.FALSE.)

      PR_MASD_DUMMY = ERR_TYP()
      RETURN
      END

