C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PREC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PREC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prprec.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF (PR_PRXY_HVALIDE(HOBJ)) THEN
         IERR = PR_PRXY_DTR(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      PR_PREC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PREC_CTRLH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PREC_CTRLH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'prprec.fi'
      INCLUDE 'prprxy.fi'

      LOGICAL VALIDE
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      VALIDE = .FALSE.
      IF (PR_PRXY_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ENDIF

      PR_PREC_CTRLH = VALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     HSIM     HANDLE DE LA SIMULATION
C     HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C     F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE (DEPEND DE L'ELEMENT)
C     F_KU     FONCTION A APPELER POUR ASSEMBLER LA RESIDU  (DEPEND DE L'ELEMENT)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PREC_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PREC_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prprec.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (PR_PRXY_HVALIDE(HOBJ)) THEN
         IERR = PR_PRXY_ASM(HOBJ, HSIM, HALG, F_K, F_KU)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      PR_PREC_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C  V     VECTEUR A PRECONDITIONNER
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PREC_PRC(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PREC_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  V(*)

      INCLUDE 'prprec.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (PR_PRXY_HVALIDE(HOBJ)) THEN
         IERR = PR_PRXY_PRC(HOBJ, V)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      PR_PREC_PRC = ERR_TYP()
      RETURN
      END
