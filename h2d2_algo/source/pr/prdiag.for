C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PR_DIAG_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_DIAG_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_DIAG_NOBJMAX,
     &                   PR_DIAG_HBASE,
     &                   'Preconditioner Diagonal')

      PR_DIAG_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_DIAG_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prdiag.fc'

      INTEGER  IERR
      EXTERNAL PR_DIAG_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_DIAG_NOBJMAX,
     &                   PR_DIAG_HBASE,
     &                   PR_DIAG_DTR)

      PR_DIAG_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_DIAG_NOBJMAX,
     &                   PR_DIAG_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_DIAG_HVALIDE(HOBJ))
         IOB = HOBJ - PR_DIAG_HBASE

         PR_DIAG_HRES(IOB) = 0
      ENDIF

      PR_DIAG_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prdiag.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_DIAG_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_DIAG_NOBJMAX,
     &                   PR_DIAG_HBASE)

      PR_DIAG_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_INI(HOBJ, IOPR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPR

      INCLUDE 'prdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_DIAG_RST(HOBJ)

C---     INITIALISE LA METHODE DE RÉSOLUTION
      IF (ERR_GOOD()) IERR = MR_LMPD_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_LMPD_INI(HRES,
     &                                   IOPR,     ! Op. de lump
     &                                   1,        ! NITER
     &                                   1.0D-32,  ! EPS0
     &                                   1.0D-32)  ! EPS1

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - PR_DIAG_HBASE
         PR_DIAG_HRES(IOB) = HRES
      ENDIF

      PR_DIAG_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_DIAG_HBASE

      HRES = PR_DIAG_HRES(IOB)
      IF (MR_LMPD_HVALIDE(HRES)) IERR = MR_LMPD_DTR(HRES)

      PR_DIAG_HRES(IOB) = 0

      PR_DIAG_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PR_DIAG_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_DIAG_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prdiag.fi'
      INCLUDE 'prdiag.fc'
C------------------------------------------------------------------------

      PR_DIAG_REQHBASE = PR_DIAG_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_DIAG_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_DIAG_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prdiag.fc'
C------------------------------------------------------------------------

      PR_DIAG_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                 PR_DIAG_NOBJMAX,
     &                                 PR_DIAG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble et factorise le préconditionnement.
C
C Description:
C     La fonction PR_DIAG_ASM assemble et factorise la matrice de
C     préconditionnement.
C     La matrice est dimensionnée et assemblèe, puis factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle de la simulation
C     HALG     Handle de l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_DIAG_HBASE
      HRES = PR_DIAG_HRES(IOB)
D     CALL ERR_ASR(MR_LMPD_HVALIDE(HRES))

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = MR_LMPD_ASMMTX(HRES, HSIM, HALG, F_K, F_KU)

C---     FACTORISE LA MATRICE
      IF (ERR_GOOD()) IERR = MR_LMPD_FCTMTX(HRES)

      PR_DIAG_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Resous le système matriciel pour
C
C Description:
C     La fonction PR_DIAG_RESMTX resoud le système matriciel
C     pour le second membre qui lui est passé. La matrice doit
C     être factorisée.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     NEQ      DIMENSION DU SECOND MEMBRE
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Sortie:
C     VFG      SECOND MEMBRE ET SOLUTION DU SYSTEME MATRICIEL
C
C Notes:
C
C************************************************************************
      FUNCTION PR_DIAG_PRC(HOBJ, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_DIAG_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VFG(*)

      INCLUDE 'prdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'prdiag.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSLVR
      INTEGER HSIM, HALG
      EXTERNAL ERR_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PR_DIAG_HBASE
      HSLVR = PR_DIAG_HRES(IOB)
D     CALL ERR_ASR(MR_LMPD_HVALIDE(HSLVR))

C---     RESOUS
      HSIM = 0
      HALG = 0
      IF (ERR_GOOD()) THEN
         IERR = MR_LMPD_RESMTX(HSLVR,HSIM,HALG,ERR_DUMMY,VFG)
      ENDIF

      PR_DIAG_PRC = ERR_TYP()
      RETURN
      END
