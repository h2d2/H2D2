C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à PR_PRXY_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA PR_PRXY_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'prprxy.fc'

      DATA PR_PRXY_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PR_PRXY_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_PRXY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_PRXY_NOBJMAX,
     &                   PR_PRXY_HBASE,
     &                   'Proxy: Preconditioner')

      PR_PRXY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_PRXY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'prprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fc'

      INTEGER  IERR
      EXTERNAL PR_PRXY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_PRXY_NOBJMAX,
     &                   PR_PRXY_HBASE,
     &                   PR_PRXY_DTR)

      PR_PRXY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_PRXY_NOBJMAX,
     &                   PR_PRXY_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_PRXY_HVALIDE(HOBJ))
         IOB = HOBJ - PR_PRXY_HBASE

         PR_PRXY_HPRC(IOB) = 0
         PR_PRXY_HMDL(IOB) = 0
      ENDIF

      PR_PRXY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprxy.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_PRXY_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_PRXY_NOBJMAX,
     &                   PR_PRXY_HBASE)

      PR_PRXY_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_INI(HOBJ, HPRC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRC

      INCLUDE 'prprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'prprxy.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_PRXY_HVALIDE(HOBJ))
D     CALL ERR_PRE(HPRC .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_PRXY_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - PR_PRXY_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HPRC, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPRC))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HPRC)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         PR_PRXY_HPRC(IOB) = HPRC
         PR_PRXY_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HPRC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PR_PRXY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'prprxy.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HPRC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - PR_PRXY_HBASE
      HPRC = PR_PRXY_HPRC(IOB)
      HMDL = PR_PRXY_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPRC))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPRC))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      PR_PRXY_HPRC(IOB) = 0
      PR_PRXY_HMDL(IOB) = 0

      PR_PRXY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_PRXY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_PRXY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'prprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prprxy.fc'
C------------------------------------------------------------------------

      PR_PRXY_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_PRXY_NOBJMAX,
     &                                  PR_PRXY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_ASM
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'prprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'prprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRC
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_PRXY_HBASE

      HPRC = PR_PRXY_HPRC(IOB)
      HMDL = PR_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASM', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL5(HFNC,
     &                        SO_ALLC_CST2B(HPRC),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HALG),
     &                        SO_ALLC_CST2B(F_K ,0_1),
     &                        SO_ALLC_CST2B(F_KU,0_1))

      PR_PRXY_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_PRXY_PRC(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_PRXY_PRC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   V(*)

      INCLUDE 'prprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'prprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRC
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PR_PRXY_HBASE

      HPRC = PR_PRXY_HPRC(IOB)
      HMDL = PR_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'PRC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     SO_ALLC_CST2B(HPRC),
     &                                     SO_ALLC_CST2B(V(1)))

      PR_PRXY_PRC = ERR_TYP()
      RETURN
      END
