C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PR_IDEN_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_IDEN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'priden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PR_IDEN_NOBJMAX,
     &                   PR_IDEN_HBASE,
     &                   'Preconditioner Identity')

      DO I=1, PR_IDEN_NOBJMAX
      ENDDO

      PR_IDEN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_IDEN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'priden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

      INTEGER IERR
      EXTERNAL PR_IDEN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PR_IDEN_NOBJMAX,
     &                   PR_IDEN_HBASE,
     &                   PR_IDEN_DTR)

      PR_IDEN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'priden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PR_IDEN_NOBJMAX,
     &                   PR_IDEN_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PR_IDEN_HVALIDE(HOBJ))
         IOB = HOBJ - PR_IDEN_HBASE
      ENDIF

      PR_IDEN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'priden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PR_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PR_IDEN_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PR_IDEN_NOBJMAX,
     &                   PR_IDEN_HBASE)

      PR_IDEN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'priden.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PR_IDEN_RST(HOBJ)

      PR_IDEN_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'priden.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      PR_IDEN_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PR_IDEN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_IDEN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'priden.fi'
      INCLUDE 'priden.fc'
C------------------------------------------------------------------------

      PR_IDEN_REQHBASE = PR_IDEN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PR_IDEN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PR_IDEN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'priden.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'priden.fc'
C------------------------------------------------------------------------

      PR_IDEN_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PR_IDEN_NOBJMAX,
     &                                  PR_IDEN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C  HOBJ     HANDLE SUR L'OBJET COURANT
C  HSIM     HANDLE DE LA SIMULATION
C  HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C  F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE (DEPEND DE L'ELEMENT)
C  F_KU     FONCTION A APPELER POUR ASSEMBLER LA RESIDU  (DEPEND DE L'ELEMENT)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_ASM(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_ASM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'priden.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'priden.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      PR_IDEN_ASM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PR_IDEN_PRC(HOBJ, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PR_IDEN_PRC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   V(*)

      INCLUDE 'priden.fi'
      INCLUDE 'err.fi'
      INCLUDE 'priden.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(PR_IDEN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      PR_IDEN_PRC = ERR_TYP()
      RETURN
      END

