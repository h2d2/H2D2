C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes: Homotopy
C Objet:   PRopriétés GLobales
C Type:    ---
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_HMPRGL_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_HMPRGL_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'hmprgl_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'hmprgl.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HHOM, HOBJ
      INTEGER HPRGL, IPRGL
      REAL*8  AMIN, VMIN, AMAX, VMAX
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_HMPRGL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_HOMOTOPY_PRGL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Min: alfa</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, AMIN)
C     <comment>Min: value</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, VMIN)
C     <comment>Max: alfa</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, AMAX)
C     <comment>Max: value</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, VMAX)
C     <comment>Handle on the global properties</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HPRGL)
C     <comment>Index of the property to monitor</comment>
      IF (IRET .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, IPRGL)
      IF (IERR .NE.  0) GOTO 9902

C---     Construis l'objet
      HHOM = 0
      IF (ERR_GOOD()) IERR = HM_PRGL_CTR(HHOM)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = HM_HMTP_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = HM_HMTP_INI(HOBJ, HHOM)

C---     A cause du double rôle, parent et proxy,
C---     l'initialisation de l'héritier doit avoir
C---     lieux après celle du parent.
      IF (ERR_GOOD()) IERR = HM_PRGL_INI(HHOM,
     &                                   HOBJ,
     &                                   AMIN, VMIN,
     &                                   AMAX, VMAX,
     &                                   HPRGL, IPRGL)

C---     Impression des paramètres du bloc
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HHOM)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_AMIN#<35>#', '= ', AMIN
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_VMIN#<35>#', '= ', VMIN
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_AMAX#<35>#', '= ', AMAX
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)') 'MSG_VMAX#<35>#', '= ', VMAX
         CALL LOG_ECRIS(LOG_BUF)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HPRGL)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_HANDLE#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,I6)') 'MSG_INDICE_PRGL#<35>#', '= ', IPRGL
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the homotopy</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>homotopy_prgl</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_HMPRGL_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_HMPRGL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_HMPRGL_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type HM_HMTP.
C     Le proxy résout directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_HMPRGL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_HMPRGL_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'hmprgl_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmprgl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'hmprgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
      REAL*8  RVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_HMHMTP_XEQMTH@hm/hmhmtp_ic.for</include>

C     <comment>The method <b>print</b> prints information about the object.</comment>
      IF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(HM_PRGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = HM_PRGL_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_HMPRGL_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_HMPRGL_AID()

9999  CONTINUE
      IC_HMPRGL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_HMPRGL_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_HMPRGL_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmprgl_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>homotopy_prgl</b> represents load path on a single
C  global property (Status: Experimental).
C</comment>
      IC_HMPRGL_REQCLS = 'homotopy_prgl'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_HMPRGL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_HMPRGL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmprgl_ic.fi'
      INCLUDE 'hmprgl.fi'
C-------------------------------------------------------------------------

      IC_HMPRGL_REQHDL = HM_PRGL_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_HMCOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_HMPRGL_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('hmprgl_ic.hlp')
      RETURN
      END
      