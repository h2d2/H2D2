C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  HoMotopy
C Objet:   HoMoToPy
C Type:    Virtuel
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à HM_HMTP_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA HM_HMTP_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'hmhmtp.fc'

      DATA HM_HMTP_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>HM_HMTP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(HM_HMTP_NOBJMAX,
     &                   HM_HMTP_HBASE,
     &                   'Homotopy: Base class')

      HM_HMTP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
      EXTERNAL HM_HMTP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(HM_HMTP_NOBJMAX,
     &                   HM_HMTP_HBASE,
     &                   HM_HMTP_DTR)

      HM_HMTP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   HM_HMTP_NOBJMAX,
     &                   HM_HMTP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(HM_HMTP_HVALIDE(HOBJ))
         IOB = HOBJ - HM_HMTP_HBASE

         HM_HMTP_VMIN(IOB) = 1.0D0
         HM_HMTP_AMIN(IOB) = 0.0D0
         HM_HMTP_VMAX(IOB) = 1.0D0
         HM_HMTP_AMAX(IOB) = 1.0D0
         HM_HMTP_HOMG(IOB) = 0
         HM_HMTP_HMDL(IOB) = 0
      ENDIF

      HM_HMTP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = HM_HMTP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   HM_HMTP_NOBJMAX,
     &                   HM_HMTP_HBASE)

      HM_HMTP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = HM_HMTP_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - HM_HMTP_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         HM_HMTP_HOMG(IOB) = HOMG
         HM_HMTP_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HM_HMTP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - HM_HMTP_HBASE
      HOMG = HM_HMTP_HOMG(IOB)
      HMDL = HM_HMTP_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      HM_HMTP_VMIN(IOB) = 1.0D0
      HM_HMTP_AMIN(IOB) = 0.0D0
      HM_HMTP_VMAX(IOB) = 1.0D0
      HM_HMTP_AMAX(IOB) = 1.0D0
      HM_HMTP_HOMG(IOB) = 0
      HM_HMTP_HMDL(IOB) = 0

      HM_HMTP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction HM_HMTP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'hmhmtp.fc'
C------------------------------------------------------------------------

      HM_HMTP_REQHBASE = HM_HMTP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction HM_HMTP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmhmtp.fc'
C------------------------------------------------------------------------

      HM_HMTP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  HM_HMTP_NOBJMAX,
     &                                  HM_HMTP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les bornes d'interpolation.
C
C Description:
C     La méthode protégée HM_HMTP_ASGVAL permet d'assigner les bornes
C     entre lesquelles l'interpolation sera faite.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_ASGVAL(HOBJ, AMIN, VMIN, AMAX, VMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_ASGVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  AMIN, VMIN
      REAL*8  AMAX, VMAX

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (AMIN .LT. 0.0D0) GOTO 9900
      IF (AMIN .GT. 1.0D0) GOTO 9900
      IF (AMAX .LT. 0.0D0) GOTO 9900
      IF (AMAX .GT. 1.0D0) GOTO 9900
      IF (AMIN .GT. AMAX)  GOTO 9900

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - HM_HMTP_HBASE
         HM_HMTP_VMIN(IOB) = VMIN
         HM_HMTP_AMIN(IOB) = AMIN
         HM_HMTP_VMAX(IOB) = VMAX
         HM_HMTP_AMAX(IOB) = AMAX
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3)')'ERR_DOMAINE_INVALIDE',':',AMIN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HM_HMTP_ASGVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la valeur interpolée.
C
C Description:
C     La méthode protégée HM_HMTP_CLCVAL calcule la valeur interpolée
C     au point ALFA.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     ALFA        Point de calcul
C
C Sortie:
C     VVAL        Valeur interpolée
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_CLCVAL(HOBJ, ALFA, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_CLCVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA
      REAL*8  VVAL

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER IOB
      REAL*8  AMIN, AMAX, VMIN, VMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - HM_HMTP_HBASE
      AMIN = HM_HMTP_AMIN(IOB)
      VMIN = HM_HMTP_VMIN(IOB)
      AMAX = HM_HMTP_AMAX(IOB)
      VMAX = HM_HMTP_VMAX(IOB)

C---     Interpole
      IF (ALFA .LE. AMIN) THEN
         VVAL = VMIN
      ELSEIF (ALFA .GE. AMAX) THEN
         VVAL = VMAX
      ELSE
         VVAL = VMIN + (VMAX-VMIN)*((ALFA-AMIN)/(AMAX-AMIN))
      ENDIF

      HM_HMTP_CLCVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les bornes d'interpolation.
C
C Description:
C     La fonction <code>HM_HMTP_REQVAL(...)</code> retourne les bornes
C     d'interpolation.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_HMTP_REQVAL(HOBJ, VMIN, AMIN, VMAX, AMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_REQVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VMIN, AMIN
      REAL*8  VMAX, AMAX

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - HM_HMTP_HBASE
      AMIN = HM_HMTP_AMIN(IOB)
      VMIN = HM_HMTP_VMIN(IOB)
      AMAX = HM_HMTP_AMAX(IOB)
      VMAX = HM_HMTP_VMAX(IOB)

      HM_HMTP_REQVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute le calcul.
C
C Description:
C     La fonction <code>HM_HMTP_XEQ(...)</code> execute le calcul.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        L'objet courant
C     ALFA        Point de calcul
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_XEQ(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_XEQ
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - HM_HMTP_HBASE

      HOMG = HM_HMTP_HOMG(IOB)
      HMDL = HM_HMTP_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'XEQ', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL2(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(ALFA))
      ENDIF

      HM_HMTP_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne alfa.
C
C Description:
C     La fonction <code>HM_HMTP_REQALFA(...)</code> retourne alfa, le point
C     de calcul actuel.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C
C Sortie:
C     ALFA        Point de calcul
C
C Notes:
C************************************************************************
      FUNCTION HM_HMTP_REQALFA(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_HMTP_REQALFA
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'hmhmtp.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - HM_HMTP_HBASE

      HOMG = HM_HMTP_HOMG(IOB)
      HMDL = HM_HMTP_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'REQALFA', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL2(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(ALFA))
      ENDIF

      HM_HMTP_REQALFA = ERR_TYP()
      RETURN
      END
            