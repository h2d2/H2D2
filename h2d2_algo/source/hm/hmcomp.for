C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>HM_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(HM_COMP_NOBJMAX,
     &                   HM_COMP_HBASE,
     &                   'Homotopy - Compose')

      HM_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER  IERR
      EXTERNAL HM_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(HM_COMP_NOBJMAX,
     &                   HM_COMP_HBASE,
     &                   HM_COMP_DTR)

      HM_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_COMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   HM_COMP_NOBJMAX,
     &                   HM_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(HM_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - HM_COMP_HBASE

         HM_COMP_NCMP(IOB) = 0
         HM_COMP_LCMP(IOB) = 0
      ENDIF

      HM_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_COMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = HM_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   HM_COMP_NOBJMAX,
     &                   HM_COMP_HBASE)

      HM_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_COMP_INI(HOBJ, NCMP, KCMP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCMP
      INTEGER KCMP(*)

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (NCMP .LT. 0) GOTO 9901
      DO I=1, NCMP
         IF (.NOT. HM_HMTP_HVALIDE(KCMP(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = HM_COMP_RST(HOBJ)

C---     ALLOUE LA MÉMOIRE POUR LA TABLE
      LCMP = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NCMP, LCMP)

C---     REMPLIS LA TABLE
      IF (ERR_GOOD()) CALL ICOPY (NCMP,
     &                            KCMP, 1,
     &                            KA(SO_ALLC_REQKIND(KA,LCMP)), 1)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - HM_COMP_HBASE
         HM_COMP_NCMP(IOB) = NCMP
         HM_COMP_LCMP(IOB) = LCMP
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NCMP
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KCMP(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      HM_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION HM_COMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmcomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LCMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - HM_COMP_HBASE
      LCMP = HM_COMP_LCMP(IOB)

      IF (LCMP .NE. 0) IERR = SO_ALLC_ALLINT(0, LCMP)

      HM_COMP_NCMP(IOB) = 0
      HM_COMP_LCMP(IOB) = 0

      HM_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction HM_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'hmcomp.fi'
      INCLUDE 'hmcomp.fc'
C------------------------------------------------------------------------

      HM_COMP_REQHBASE = HM_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction HM_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'hmcomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'hmcomp.fc'
C------------------------------------------------------------------------

      HM_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  HM_COMP_NOBJMAX,
     &                                  HM_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>HM_COMP_XEQ(...)</code>
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_XEQ(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmcomp.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER NCMP, LCMP, HCMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - HM_COMP_HBASE
      NCMP = HM_COMP_NCMP(IOB)
      LCMP = HM_COMP_LCMP(IOB)
D     CALL ERR_ASR(NCMP .GT. 0)
D     CALL ERR_ASR(LCMP .NE. 0)

C---     BOUCLE SUR LES CRITERES
      CALL LOG_INCIND()
      DO IC = 1, NCMP
         IF (ERR_GOOD()) THEN
            HCMP = SO_ALLC_REQIN4(LCMP, IC)
            IERR = HM_HMTP_XEQ(HCMP, ALFA)
         ENDIF
      ENDDO
      CALL LOG_DECIND()

      HM_COMP_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne alfa.
C
C Description:
C     La fonction <code>HM_COMP_REQALFA(...)</code> retourne alfa, le point
C     de calcul actuel. Si tous les objets de la composition n'ont pas
C     le même alfa, la fonction retourne la valeur -1.0D0
C
C Entrée:
C
C Sortie:
C     ALFA        Point de calcul
C
C Notes:
C************************************************************************
      FUNCTION HM_COMP_REQALFA(HOBJ, ALFA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: HM_COMP_REQALFA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  ALFA

      INCLUDE 'hmcomp.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'hmcomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER NCMP, LCMP, HCMP
      REAL*8  A, AMIN, AMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(HM_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - HM_COMP_HBASE
      NCMP = HM_COMP_NCMP(IOB)
      LCMP = HM_COMP_LCMP(IOB)
D     CALL ERR_ASR(NCMP .GT. 0)
D     CALL ERR_ASR(LCMP .NE. 0)

C---     BOUCLE SUR LES CRITERES
      AMIN = 1.0D+99
      AMAX =-1.0D+99
      DO IC = 1, NCMP
         IF (ERR_GOOD()) THEN
            HCMP = SO_ALLC_REQIN4(LCMP, IC)
            IERR = HM_HMTP_REQALFA(HCMP, A)
            AMAX = MAX(AMAX, A)
            AMIN = MIN(AMIN, A)
         ENDIF
      ENDDO
      IF (AMAX .EQ. AMIN) THEN
         ALFA = AMIN
      ELSE
         ALFA = -1.0D0
      ENDIF

      HM_COMP_REQALFA = ERR_TYP()
      RETURN
      END
 