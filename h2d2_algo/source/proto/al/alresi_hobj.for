C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-LoCal
C Type:
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_RESI
C         SUBROUTINE AL_RESI_CTR
C         SUBROUTINE AL_RESI_DTR
C         SUBROUTINE AL_RESI_INI
C         SUBROUTINE AL_RESI_RST
C         SUBROUTINE AL_RESI_CLCRES
C         SUBROUTINE AL_RESI_CLCNRM
C         SUBROUTINE AL_RESI_000
C         SUBROUTINE AL_RESI_999
C         SUBROUTINE AL_RESI_REQHBASE
C         SUBROUTINE AL_RESI_HVALIDE
C         SUBROUTINE AL_RESI_TVALIDE
C         FTN (Sub)Module: AL_RESI_HOBJ_M
C            Public:
C            Private:
C               SUBROUTINE AL_RESI_REQSELF
C
C************************************************************************

      MODULE AL_RESI_HOBJ_M

      USE AL_RESI_M, ONLY : AL_RESI_T
      IMPLICIT NONE

      ! ---  Attributs statiques
      INTEGER, SAVE :: AL_RESI_HBASE = 0

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée AL_RESI_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_REQSELF(HOBJ)

      USE ISO_C_BINDING

      TYPE (AL_RESI_T), POINTER :: AL_RESI_REQSELF
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

D     CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
D     CALL ERR_ASR(ERR_GOOD())
D     CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      AL_RESI_REQSELF => SELF
      RETURN
      END FUNCTION AL_RESI_REQSELF

      END MODULE AL_RESI_HOBJ_M

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction AL_RESI_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_CTR
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      USE AL_RESI_M, ONLY : AL_RESI_T, AL_RESI
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER, INTENT(OUT) :: HOBJ

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      ALLOCATE(SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   AL_RESI_HBASE,
     &                                   C_LOC(SELF))

C---     Construis
      IERR = AL_RESI(SELF)

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      AL_RESI_CTR = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_CTR

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction AL_RESI_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_DTR
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      USE AL_RESI_M, ONLY : DEL
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (AL_RESI_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      CALL DEL(SELF)

      IERR = OB_OBJN_DTR(HOBJ, AL_RESI_HBASE)

      AL_RESI_DTR = ERR_TYP()
      RETURN
      END FUNCTION AL_RESI_DTR

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La méthode AL_RESI_INI initialise l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_INI(HOBJ, F_RES, HALG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_INI
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER  F_RES
      INTEGER  HALG
      EXTERNAL F_RES

      INCLUDE 'alresi.fi'
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      AL_RESI_INI = SELF%INI(F_RES, HALG)
      RETURN
      END FUNCTION AL_RESI_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_RESI_RST(HOBJ)

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'alresi.fi'
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      AL_RESI_RST = SELF%RST()
      RETURN
      END FUNCTION AL_RESI_RST

C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_UTIL_CLCRES_CLCRES calcule
C     le résidu pour une valeur de Theta.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     VDLG        Degrés de liberté
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCRES1(HOBJ, VDLG, VRES)

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: HOBJ
      REAL*8,  INTENT(IN)    :: VDLG(:,:)
      REAL*8,  INTENT(INOUT) :: VRES(:)

      ! INCLUDE 'alresi.fi'
      LOGICAL AL_RESI_HVALIDE
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      AL_RESI_CLCRES1 = SELF%CLCRES(VDLG, VRES)
      RETURN
      END FUNCTION AL_RESI_CLCRES1

C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_UTIL_CLCRES_CLCRES calcule
C     le résidu pour une valeur de Theta.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     THETA
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCRES2(HOBJ, THETA, VDLG, VDEL, VRES)

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: HOBJ
      REAL*8,  INTENT(IN)    :: THETA
      REAL*8,  INTENT(IN)    :: VDLG(:,:)
      REAL*8,  INTENT(IN)    :: VDEL(:,:)
      REAL*8,  INTENT(INOUT) :: VRES(:)

      ! INCLUDE 'alresi.fi'
      LOGICAL AL_RESI_HVALIDE
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)

      AL_RESI_CLCRES2 = SELF%CLCRES(THETA, VDLG, VDEL, VRES)
      RETURN
      END FUNCTION AL_RESI_CLCRES2

C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_UTIL_CLCRES_CLCRES calcule
C     la norme du résidu pour une valeur de Theta.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     THETA
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCNRM1(HOBJ, VDLG, VNRM)

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      REAL*8,  INTENT(IN)  :: VDLG(:,:)
      REAL*8,  INTENT(OUT) :: VNRM

      ! INCLUDE 'alresi.fi'
      LOGICAL AL_RESI_HVALIDE
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      AL_RESI_CLCNRM1 = SELF%CLCNRM(VDLG, VNRM)
      RETURN
      END FUNCTION AL_RESI_CLCNRM1
      
C************************************************************************
C Sommaire: Calcule le résidu.
C
C Description:
C     La fonction statique AL_UTIL_CLCRES_CLCRES calcule
C     la norme du résidu pour une valeur de Theta.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     THETA
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER
     &FUNCTION AL_RESI_CLCNRM2(HOBJ, THETA, VDLG, VDEL, VNRM)

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      REAL*8,  INTENT(IN)  :: THETA
      REAL*8,  INTENT(IN)  :: VDLG(:,:)
      REAL*8,  INTENT(IN)  :: VDEL(:,:)
      REAL*8,  INTENT(OUT) :: VNRM

      ! INCLUDE 'alresi.fi'
      LOGICAL AL_RESI_HVALIDE
      INCLUDE 'err.fi'

      TYPE (AL_RESI_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => AL_RESI_REQSELF(HOBJ)
      AL_RESI_CLCNRM2 = SELF%CLCNRM(THETA, VDLG, VDEL, VNRM)
      RETURN
      END FUNCTION AL_RESI_CLCNRM2


C************************************************************************
C************************************************************************
C************************************************************************
C************************************************************************


C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction AL_RESI_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_000
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(AL_RESI_HBASE,
     &                   'Algo de calcul de résidu')

      AL_RESI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction AL_RESI_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_999
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL AL_RESI_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(AL_RESI_HBASE, AL_RESI_DTR)

      AL_RESI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_RESI_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_REQHBASE
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'alresi.fi'
C------------------------------------------------------------------------

      AL_RESI_REQHBASE = AL_RESI_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_RESI_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_HVALIDE
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      AL_RESI_HVALIDE = OB_OBJN_HVALIDE(HOBJ, AL_RESI_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction AL_RESI_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_RESI_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_RESI_TVALIDE
CDEC$ ENDIF

      USE AL_RESI_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alresi.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      AL_RESI_TVALIDE = OB_OBJN_TVALIDE(HOBJ, AL_RESI_HBASE)
      RETURN
      END
