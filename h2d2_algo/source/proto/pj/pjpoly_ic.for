C************************************************************************
C --- Copyright (c) INRS 2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE IC_PJ_POLY_XEQCTR
C     SUBROUTINE IC_PJ_POLY_XEQMTH
C     SUBROUTINE IC_PJ_POLY_REQCLS
C     SUBROUTINE IC_PJ_POLY_REQHDL
C   Private:
C     SUBROUTINE IC_PJ_POLY_PRN
C     SUBROUTINE IC_PJ_POLY_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PJ_POLY_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POLY_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pjpoly_ic.fi'
      INCLUDE 'pjpoly.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpoly_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HALG
      INTEGER IDEG
      REAL*8  RRES
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PJ_POLY_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Polynomial order [1, 3] (default 1)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IDEG)
      IF (IERR .NE. 0) IDEG = 1
C     <comment>Residual drop ratio threshold for step rejection (default 1.0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, RRES)
      IF (IERR .NE. 0) RRES = 1.0D0

C---     Construis et initialise l'objet
      HALG = 0
      IF (ERR_GOOD()) IERR = PJ_POLY_CTR(HALG)
      IF (ERR_GOOD()) IERR = PJ_POLY_INI(HALG, RRES, IDEG)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PJ_PRJC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PJ_PRJC_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_PJ_POLY_PRN(HALG)
      END IF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>proj_polynomial</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PJ_POLY_AID()

9999  CONTINUE
      IC_PJ_POLY_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POLY_PRN(HOBJ)

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpoly.fc'
      INCLUDE 'pjpoly_ic.fc'

      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
      TYPE (PJ_POLY_SELF_T), POINTER :: PRJC
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_PROJECTION_POLYNOME'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      PRJC => PJ_POLY_REQSELF(HOBJ)

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,I6)') 'MSG_ORDRE#<35>#',  '= ', PRJC%NTRM-1
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_RATIO_RESIDU#<35>#',  '= ', 
     &                                PRJC%RRES
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_PJ_POLY_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PJ_POLY_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POLY_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pjpoly_ic.fi'
      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpoly_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = PJ_POLY_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_PJ_POLY_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PJ_POLY_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PJ_POLY_AID()

9999  CONTINUE
      IC_PJ_POLY_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POLY_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POLY_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pjpoly_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>proj_polynomial</b> projects the initial solution with an
C  polynomial extrapolation of order [1, 3] (Status: Experimental)
C</comment>
      IC_PJ_POLY_REQCLS = 'proj_polynomial'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POLY_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POLY_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pjpoly_ic.fi'
      INCLUDE 'pjpoly.fi'
C-------------------------------------------------------------------------

      IC_PJ_POLY_REQHDL = PJ_POLY_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PJ_POLY_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pjpoly_ic.hlp')
      RETURN
      END

