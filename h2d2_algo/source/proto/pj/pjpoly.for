C************************************************************************
C --- Copyright (c) INRS 2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  ProJection de solution
C Objet:   Explicit 2nd order
C Type:    Concret
C
C Functions:
C   Public:
C     SUBROUTINE PJ_POLY_000
C     SUBROUTINE PJ_POLY_999
C     SUBROUTINE PJ_POLY_CTR
C     SUBROUTINE PJ_POLY_DTR
C     SUBROUTINE PJ_POLY_INI
C     SUBROUTINE PJ_POLY_RST
C     SUBROUTINE PJ_POLY_REQHBASE
C     SUBROUTINE PJ_POLY_HVALIDE
C     SUBROUTINE PJ_POLY_XEQ
C   Private:
C     SUBROUTINE PJ_POLY_REQSELF
C     SUBROUTINE PJ_POLY_RAZ
C     SUBROUTINE PJ_POLY_ALLTBL
C     SUBROUTINE PJ_POLY_CLCEGV
C     SUBROUTINE PJ_POLY_CLCSOL
C     SUBROUTINE PJ_POLY_MVM
C     SUBROUTINE PJ_POLY_GMRS_SLV
C     SUBROUTINE PJ_POLY_GMRS_PRC
C     SUBROUTINE PJ_POLY_GMRS_RES
C     SUBROUTINE PJ_POLY_NRMRES
C
C************************************************************************

      MODULE PJ_POLY_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: PJ_POLY_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: PJ_POLY_SELF_T
         REAL*8  RRES      ! Rapport des résidus
         REAL*8  TSOL      ! Temps du dernier VSOL
         INTEGER NDLL      ! Nombre de Degrés de Libertés Locaux
         INTEGER NEQL      ! Nombre d'EQuations Local
         INTEGER NTRM      ! Nombre de termes du polynôme
         INTEGER NACC      ! Nbr de sol accumulées
         INTEGER IACC      ! Indice de la dernière accumulée
         INTEGER HSIM      ! Handle sur l'élément
         INTEGER HRES      ! Handle sur le CB de calcul du résidu GMRes
         REAL*8, DIMENSION(:,:), ALLOCATABLE :: VACC
         REAL*8, DIMENSION(:),   ALLOCATABLE :: VDLG
         REAL*8, DIMENSION(:),   ALLOCATABLE :: VRES
         REAL*8, DIMENSION(:),   ALLOCATABLE :: TACC
      END TYPE PJ_POLY_SELF_T

C---     Paramètres
      !!INTEGER, PARAMETER :: PJ_POLY_NACC = 3

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PJ_POLY_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PJ_POLY_SELF_T), POINTER :: PJ_POLY_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PJ_POLY_REQSELF => SELF
      RETURN
      END FUNCTION PJ_POLY_REQSELF

      END MODULE PJ_POLY_M

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PJ_POLY_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_000
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PJ_POLY_HBASE,
     &                   'Projection: Explicit 2nd order')

      PJ_POLY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>PJ_POLY_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_999
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PJ_POLY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PJ_POLY_HBASE, PJ_POLY_DTR)

      PJ_POLY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>PJ_POLY_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>PJ_POLY_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POLY_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_CTR
CDEC$ ENDIF

      USE PJ_POLY_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR, IRET
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PJ_POLY_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PJ_POLY_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PJ_POLY_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PJ_POLY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode PJ_POLY_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_RAZ(HOBJ)

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PJ_POLY_REQSELF(HOBJ)
      SELF%TSOL =-1.0D0
      SELF%RRES = 1.0D0
      SELF%NDLL = -1
      SELF%NEQL = -1
      SELF%NTRM =  0
      SELF%NACC =  0
      SELF%IACC =  0
      SELF%HSIM =  0
      SELF%HRES =  0

      PJ_POLY_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>PJ_POLY_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POLY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_DTR
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PJ_POLY_RST(HOBJ)

C---     Dé-alloue
      SELF => PJ_POLY_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PJ_POLY_HBASE)

      PJ_POLY_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>PJ_POLY_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     N        Nombre de vecteur à accumuler
C     M        Nombre de valeurs/vecteurs propres max
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POLY_INI(HOBJ, RRES, IDEG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_INI
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  RRES
      INTEGER IDEG

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
      INTEGER IRET
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles
      IF (RRES .LE. 1.0D-04) GOTO 9901
!!!      IF (RRES .GT. 2.0D-00) GOTO 9901
      IF (IDEG .LE. 0) GOTO 9902
      IF (IDEG .GE. 4) GOTO 9902

C---     Reset les données
      IERR = PJ_POLY_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => PJ_POLY_REQSELF(HOBJ)
         SELF%RRES = RRES
         SELF%NDLL = 0
         SELF%NEQL = 0
         SELF%NTRM = IDEG + 1
         SELF%NACC = 0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_RAP_INVALIDE', ': ', RRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_POLYNOME_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A, I6)') 'MSG_OBTIENT', ': ', IDEG
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A)') 'MSG_ATTEND', ': [1, 3]' 
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PJ_POLY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POLY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_RST
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Désalloue la mémoire
      IERR = PJ_POLY_ALLTBL(HOBJ, 0, 0)

C---     Reset les paramètres
      IERR = PJ_POLY_RAZ(HOBJ)

      PJ_POLY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PJ_POLY_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_REQHBASE
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INCLUDE 'pjpoly.fi'
C------------------------------------------------------------------------

      PJ_POLY_REQHBASE = PJ_POLY_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PJ_POLY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_HVALIDE
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpoly.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PJ_POLY_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PJ_POLY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une solution
C
C Description:
C     La méthode <code>PJ_POLY_AJT(...)</code> ajoute une solution de
C     à la base.
C     La méthode peut être appelée de manière répétitive avec la même
C     solution. Celle ne sera insérée qu'une seule fois dans la base.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     TSIM        Temps associé à VDLG
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_AJT(HOBJ,
     &                     HSIM,
     &                     TSIM,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_AJT
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      REAL*8 , INTENT(IN) :: TSIM
      REAL*8 , INTENT(IN) :: VDLG(*)

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
      INTEGER NDLL, NEQL
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POLY_REQSELF(HOBJ)

C---     Récupère les données de simulation
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
D     CALL ERR_ASR(NDLL .GT. 0)

C---     Alloue les tables
      IERR = PJ_POLY_ALLTBL(HOBJ, NDLL, NEQL)

C---     Accumule les solutions
      IF (TSIM .GT. SELF%TSOL) THEN
         IF (SELF%NACC .LT. SELF%NTRM) THEN
            SELF%IACC = SELF%NACC
            SELF%NACC = SELF%NACC + 1
         ELSE
            SELF%IACC = SELF%IACC + 1
            SELF%IACC = MOD(SELF%IACC, SELF%NTRM)
         ENDIF
         SELF%VACC(:, SELF%IACC) = VDLG(1:NDLL)
         SELF%TACC(SELF%IACC) = TSIM
         SELF%TSOL = TSIM
      ENDIF

      PJ_POLY_AJT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Projette la solution
C
C Description:
C     La méthode <code>PJ_POLY_XEQ(...)</code> projette la solution de
C     manière à minimiser le résidu dans l'espace POD.
C     La méthode peut être appelée de manière répétitive avec la même
C     solution. Celle ne sera insérée qu'une seule fois dans la base.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     HRES        Handle sur le CB de calcul du résidu
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_PRJ(HOBJ,
     &                     HSIM,
     &                     HRES,
     &                     TPRJ,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_PRJ
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      INTEGER, INTENT(IN) :: HRES
      REAL*8 , INTENT(IN) :: TPRJ
      REAL*8 , INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
      INTEGER NDLL, NEQL
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
      LOGICAL DO_EGV, DO_PRJ
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HRES))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.projection.polynomial'

C---     Entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_PROJECTION_POLYNOME'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Récupère les attributs
      SELF => PJ_POLY_REQSELF(HOBJ)

C---     Résous dans l'espace des EV
      DO_PRJ = (SELF%NACC .GE. 2)
      IF (ERR_GOOD() .AND. DO_PRJ) THEN
         SELF%HSIM = HSIM
         SELF%HRES = HRES
         IERR = PJ_POLY_CLCSOL(HOBJ, TPRJ, VDLG)
      ENDIF

      CALL LOG_DECIND()

      PJ_POLY_PRJ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Projette la solution
C
C Description:
C     La méthode <code>PJ_POLY_XEQ(...)</code> projette la solution de
C     manière à minimiser le résidu dans l'espace POD.
C     La méthode peut être appelée de manière répétitive avec la même
C     solution. Celle ne sera insérée qu'une seule fois dans la base.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     HRES        Handle sur le CB de calcul du résidu
C     TSIM        Temps associé à VDLG
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_XEQ(HOBJ,
     &                     HSIM,
     &                     HRES,
     &                     TSIM,
     &                     TPRJ,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POLY_XEQ
CDEC$ ENDIF

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      INTEGER, INTENT(IN) :: HRES
      REAL*8 , INTENT(IN) :: TSIM
      REAL*8 , INTENT(IN) :: TPRJ
      REAL*8 , INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = PJ_POLY_AJT(HOBJ, HSIM, TSIM, VDLG)
      IF (ERR_GOOD()) IERR = PJ_POLY_PRJ(HOBJ, HSIM, HRES, TPRJ, VDLG)

      PJ_POLY_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Alloue les tables de l'objet.
C
C Description:
C     La méthode privée <code>PJ_POLY_ALLTBL(...)</code> alloue les tables
C     de travail de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NDLL     Nombre Degrés de Liberté Locaux
C     NEQL     Nombre d'EQuations Locales
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POLY_ALLTBL(HOBJ, NDLL, NEQL)

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLL
      INTEGER, INTENT(IN) :: NEQL

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IRET, IERR
      INTEGER NSOL
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
      LOGICAL DOALLC, DOFREE
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POLY_REQSELF(HOBJ)

C---     Désalloue les tables
      DOFREE = (NDLL .NE. SELF%NDLL)
      IF (ERR_GOOD() .AND. DOFREE) THEN
         IRET = 0
         IF (ALLOCATED(SELF%VACC)) DEALLOCATE(SELF%VACC,STAT=IRET)
         IF (ALLOCATED(SELF%VDLG)) DEALLOCATE(SELF%VDLG,STAT=IRET)
         IF (ALLOCATED(SELF%VRES)) DEALLOCATE(SELF%VRES,STAT=IRET)
         IF (ALLOCATED(SELF%TACC)) DEALLOCATE(SELF%TACC,STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         SELF%NDLL = 0
         SELF%NEQL = 0
      ENDIF

C---     Alloue les tables
      DOALLC = (SELF%NDLL .EQ. 0 .AND. NDLL .GT. 0)
      IF (ERR_GOOD() .AND. DOALLC) THEN
         IRET = 0
         NSOL = SELF%NTRM
         IF (IRET .EQ. 0) ALLOCATE(SELF%VACC(NDLL,0:NSOL-1),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%VDLG(NDLL),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%VRES(NEQL),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%TACC(0:NSOL-1),STAT=IRET)
         SELF%NDLL = NDLL
         SELF%NEQL = NEQL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PJ_POLY_ALLTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée <code>PJ_POLY_CLCSOL(...)</code> calcule la solution
C     qui est la projection du
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C     VDLG(NDLL) a été introduit comme dernière solution dans VSOL(NEQL)
C************************************************************************
      FUNCTION PJ_POLY_CLCSOL(HOBJ, TPRJ, VDLG)

      USE PJ_POLY_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      REAL*8,  INTENT(IN) :: TPRJ
      REAL*8,  INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'pjpoly.fc'

      INTEGER IERR
      INTEGER II
      INTEGER HNUMR
      INTEGER NDLL
      INTEGER NDLN, NNL, ID
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
      REAL*8, DIMENSION(:), POINTER :: V0, V1, V2, V3
      REAL*8 :: T0, T1, T2, T3

      REAL*8 :: T, A0, A1, A2, A3
      REAL*8 :: RES1, RES2, VNRM
      REAL*8 :: SCLC, SRJT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.projection.polynomial'

C---     Récupère les attributs
      SELF => PJ_POLY_REQSELF(HOBJ)
      NDLL = SELF%NDLL

C---     Résidu initial
      RES1 = PJ_POLY_NRMRES(HOBJ, VDLG, SELF%VRES)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POLY_NORME_L2_RESIDU: ', RES1
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Les valeurs
      IF (SELF%NACC .GE. 4) THEN
         II = MOD(SELF%IACC+SELF%NTRM-3, SELF%NTRM)
         V3 => SELF%VACC(:, II)
         T3 =  SELF%TACC(II)
      ENDIF
      IF (SELF%NACC .GE. 3) THEN
         II = MOD(SELF%IACC+SELF%NTRM-2, SELF%NTRM)
         V2 => SELF%VACC(:, II)
         T2 =  SELF%TACC(II)
      ENDIF
      IF (SELF%NACC .GE. 2) THEN
         II = MOD(SELF%IACC+SELF%NTRM-1, SELF%NTRM)
         V1 => SELF%VACC(:, II)
         T1 =  SELF%TACC(II)
      ENDIF
      IF (SELF%NACC .GE. 1) THEN
         II = SELF%IACC
         V0 => SELF%VACC(:, II)
         T0 =  SELF%TACC(II)
      ENDIF
      
C---     Monte la projection (polynômes de Lagrange)
      IF (SELF%NACC .EQ. 2) THEN
        CALL ERR_ASR(T0 .GT. T1)
         T = TPRJ
         A0 = (T-T1) / (T0-T1)
         A1 = (T-T0) / (T1-T0)
         SELF%VDLG(:) = A0*V0(:) + A1*V1(:)
      ELSEIF (SELF%NACC .EQ. 3) THEN
         T = TPRJ
         A0 = ((T-T1)*(T-T2)) / ((T0-T1)*(T0-T2))
         A1 = ((T-T0)*(T-T2)) / ((T1-T0)*(T1-T2))
         A2 = ((T-T0)*(T-T1)) / ((T2-T0)*(T2-T1))
         SELF%VDLG(:) = A0*V0(:) + A1*V1(:) + A2*V2(:)
      ELSEIF (SELF%NACC .EQ. 4) THEN
D        CALL ERR_ASR(T0 .GT. T1)
D        CALL ERR_ASR(T1 .GT. T2)
         T = TPRJ
         A0 = ((T-T1)*(T-T2)*(T-T3)) / ((T0-T1)*(T0-T2)*(T0-T3))
         A1 = ((T-T0)*(T-T2)*(T-T3)) / ((T1-T0)*(T1-T2)*(T1-T3))
         A2 = ((T-T0)*(T-T1)*(T-T3)) / ((T2-T0)*(T2-T1)*(T2-T3))
         A3 = ((T-T0)*(T-T1)*(T-T2)) / ((T3-T0)*(T3-T1)*(T3-T2))
         SELF%VDLG(:) = A0*V0(:) + A1*V1(:) + A2*V2(:) + A3*V3(:)
D     ELSE
D        CALL ERR_ASG(ERR_FTL, 'ERR_INVALID_POLY_DEGREE')
D        CALL ERR_ASR(.FALSE.)
      ENDIF

C---     Résidu final
      RES2 = PJ_POLY_NRMRES(HOBJ, SELF%VDLG, SELF%VRES)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POLY_NORME_L2_RESIDU: ', RES2
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Affiche les normes
      HNUMR = LM_ELEM_REQHNUMC(SELF%HSIM)
      SELF%VDLG(:) = SELF%VDLG(:) - VDLG(1:NDLL)
      NDLN = 3
      NNL = NDLL / NDLN
      do id=1, ndln
         VNRM = NR_UTIL_N2SD(HNUMR, NDLN, NNL, SELF%VDLG, ID)
         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POLY_NORME_L2: ', VNRM
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      enddo
      do id=1, ndln
         VNRM = NR_UTIL_NISD(HNUMR, NDLN, NNL, SELF%VDLG, ID)
         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POLY_NORME_MAX:', VNRM
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      enddo

C---     Contrôle
      SRJT = SELF%RRES*RES1   ! Seuil de rejet
      IF (RES2 .GT. SRJT) THEN
         WRITE(LOG_BUF, '(A)') 'MSG_POLY_PROJECTION_REJETEE'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ELSE
         VDLG(1:NDLL) = VDLG(1:NDLL) + SELF%VDLG(:)
      ENDIF
      
      PJ_POLY_CLCSOL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme L2 du résidu.
C
C Description:
C     La méthode privée <code>PJ_POLY_NRMRES(...)</code> calcule la norme
C     L2 du résidu. Elle retourne également le résidu dans VRES.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     VDLG     Table entrée
C
C Sortie:
C     VRES     Table sortie
C
C Notes:
C************************************************************************
      FUNCTION PJ_POLY_NRMRES(HOBJ, VDLG, VRES)

      USE PJ_POLY_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: HOBJ
      REAL*8,  INTENT(IN)    :: VDLG(*)
      REAL*8,  INTENT(INOUT) :: VRES(*)

      INCLUDE 'pjpoly.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpoly.fc'

      REAL*8  VNRM
      INTEGER IERR
      LOGICAL, PARAMETER :: L = .TRUE.
      TYPE (PJ_POLY_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POLY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POLY_REQSELF(HOBJ)

C---     Calcul du résidu par call-back
      IERR = SO_FUNC_CALL3(SELF%HRES,
     &                     SO_ALLC_CST2B(VRES(1)),
     &                     SO_ALLC_CST2B(VDLG(1)),
     &                     SO_ALLC_CST2B(L))

C---     La norme
      VNRM = DOT_PRODUCT(VRES(1:SELF%NEQL), VRES(1:SELF%NEQL))
      VNRM = SQRT(VNRM / SELF%NEQL)

9999  CONTINUE
      PJ_POLY_NRMRES = VNRM
      RETURN
      END
