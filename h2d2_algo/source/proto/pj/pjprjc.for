C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ProJection de solution
C Objet:   PRoJeCtion de solution
C Type:    Virtuel
C************************************************************************

      MODULE PJ_PRJC_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: PJ_PRJC_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: PJ_PRJC_SELF_T
         INTEGER HOMG
         INTEGER HMDL
      END TYPE PJ_PRJC_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PJ_PRJC_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PJ_PRJC_SELF_T), POINTER :: PJ_PRJC_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PJ_PRJC_REQSELF => SELF
      RETURN
      END FUNCTION PJ_PRJC_REQSELF

      END MODULE PJ_PRJC_M

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PJ_PRJC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_000
CDEC$ ENDIF

      USE PJ_PRJC_M
      IMPLICIT NONE

      INCLUDE 'pjprjc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PJ_PRJC_HBASE, 'Proxy: Projection')

      PJ_PRJC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_999
CDEC$ ENDIF

      USE PJ_PRJC_M
      IMPLICIT NONE

      INCLUDE 'pjprjc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PJ_PRJC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PJ_PRJC_HBASE, PJ_PRJC_DTR)

      PJ_PRJC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_PRJC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_CTR
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE ISO_C_BINDING

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER IERR, IRET
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PJ_PRJC_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PJ_PRJC_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PJ_PRJC_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PJ_PRJC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode PJ_PRJC_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_RAZ(HOBJ)

      USE PJ_PRJC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER IERR
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PJ_PRJC_REQSELF(HOBJ)
      SELF%HOMG = 0
      SELF%HMDL = 0

      PJ_PRJC_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_PRJC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_DTR
CDEC$ ENDIF

      USE PJ_PRJC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PJ_PRJC_RST(HOBJ)

C---     Dé-alloue
      SELF => PJ_PRJC_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PJ_PRJC_HBASE)

      PJ_PRJC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C     NOMMDL      Nom du module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_INI
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER  IERR
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PJ_PRJC_RST(HOBJ)

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         SELF%HOMG = HOMG
         SELF%HMDL = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PJ_PRJC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_PRJC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_RST
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER IERR
      INTEGER HFNC
      INTEGER HMDL
      INTEGER HOMG
      LOGICAL HVALIDE
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)
      HOMG = SELF%HOMG
      HMDL = SELF%HMDL
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      IERR = PJ_PRJC_RAZ(HOBJ)

      PJ_PRJC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PJ_PRJC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_REQHBASE
CDEC$ ENDIF

      USE PJ_PRJC_M
      IMPLICIT NONE

      INCLUDE 'pjprjc.fi'
C------------------------------------------------------------------------

      PJ_PRJC_REQHBASE = PJ_PRJC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PJ_PRJC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_HVALIDE
CDEC$ ENDIF

      USE PJ_PRJC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PJ_PRJC_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PJ_PRJC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Projette la solution
C
C Description:
C     La fonction <code>PJ_PRJC_XEQ(...)</code> calcule le projection de
C     la solution VDLG.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     HRES        Handle sur le CB de calcul du résidu
C     TSIM        Temps de la simulation
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_AJT(HOBJ,
     &                     HSIM,
     &                     TSIM,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_AJT
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      REAL*8  TSIM
      REAL*8  VDLG(*)

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)
      HOMG = SELF%HOMG
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'AJT', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(TSIM),
     &                        SO_ALLC_CST2B(VDLG(1)))
      ENDIF

      PJ_PRJC_AJT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Projette la solution
C
C Description:
C     La fonction <code>PJ_PRJC_PRJ(...)</code> calcule le projection de
C     la solution VDLG.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     HRES        Handle sur le CB de calcul du résidu
C     TSIM        Temps de la simulation
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_PRJ(HOBJ,
     &                     HSIM,
     &                     HRES,
     &                     TPRJ,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_PRJ
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER HRES
      REAL*8  TPRJ
      REAL*8  VDLG(*)

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)
      HOMG = SELF%HOMG
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'PRJ', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL5(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HRES),
     &                        SO_ALLC_CST2B(TPRJ),
     &                        SO_ALLC_CST2B(VDLG(1)))
      ENDIF

      PJ_PRJC_PRJ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Projette la solution
C
C Description:
C     La fonction <code>PJ_PRJC_XEQ(...)</code> calcule le projection de
C     la solution VDLG.
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     HRES        Handle sur le CB de calcul du résidu
C     TSIM        Temps de la simulation
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_XEQ(HOBJ,
     &                     HSIM,
     &                     HRES,
     &                     TSIM,
     &                     TPRJ,
     &                     VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_XEQ
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER HRES
      REAL*8  TSIM
      REAL*8  TPRJ
      REAL*8  VDLG(*)

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)
      HOMG = SELF%HOMG
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'XEQ', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL6(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(HSIM),
     &                        SO_ALLC_CST2B(HRES),
     &                        SO_ALLC_CST2B(TSIM),
     &                        SO_ALLC_CST2B(TPRJ),
     &                        SO_ALLC_CST2B(VDLG(1)))
      ENDIF

      PJ_PRJC_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Marque le début d'une séquence.
C
C Description:
C     La méthode <code>PJ_PRJC_DEB(...)</code> marque le début d'une
C     séquence d'incréments qui vont être adaptés.
C
C Entrée:
C     HOBJ        L'objet courant
C     TINI        Temps initial de la séquence
C     TFIN        Temps final de la séquence
C     DTNM        Pas de temps nominal
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_PRJC_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_PRJC_DEB
CDEC$ ENDIF

      USE PJ_PRJC_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pjprjc.fc'

      INTEGER  IERR
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
      TYPE (PJ_PRJC_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_PRJC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_PRJC_REQSELF(HOBJ)
      HOMG = SELF%HOMG
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD())
     &   IERR = SO_MDUL_CCHFNC(HMDL, 'DEB', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HOMG))
      ENDIF

      PJ_PRJC_DEB = ERR_TYP()
      RETURN
      END
