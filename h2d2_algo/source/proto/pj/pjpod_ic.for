C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     SUBROUTINE IC_PJ_POD_XEQCTR
C     SUBROUTINE IC_PJ_POD_XEQMTH
C     SUBROUTINE IC_PJ_POD_REQCLS
C     SUBROUTINE IC_PJ_POD_REQHDL
C   Private:
C     SUBROUTINE IC_PJ_POD_PRN
C     SUBROUTINE IC_PJ_POD_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PJ_POD_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POD_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pjpod_ic.fi'
      INCLUDE 'pjpod.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpod_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HALG
      INTEGER N, M
      REAL*8  EPS, RRES
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PJ_POD_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Size of the solutions base</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, N)
C     <comment>Number of eigen-vectors</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, M)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Tolerance (default 1.0e-6)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, EPS)
      IF (IERR .NE. 0) EPS = 1.0D-6
C     <comment>Residual drop ratio threshold for step rejection (default 1.0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, RRES)
      IF (IERR .NE. 0) RRES = 1.0D0

C---     Construis et initialise l'objet
      HALG = 0
      IF (ERR_GOOD()) IERR = PJ_POD_CTR(HALG)
      IF (ERR_GOOD()) IERR = PJ_POD_INI(HALG, N, M, EPS, RRES)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PJ_PRJC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PJ_PRJC_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_PJ_POD_PRN(HALG)
      END IF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>proj_pod</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PJ_POD_AID()

9999  CONTINUE
      IC_PJ_POD_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POD_PRN(HOBJ)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpod.fc'
      INCLUDE 'pjpod_ic.fc'

      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
      TYPE (PJ_POD_SELF_T), POINTER :: PRJC
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_PROJECTION_POD'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      PRJC => PJ_POD_REQSELF(HOBJ)

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,I6)') 'MSG_NBR_SOL#<35>#', '= ', PRJC%NSOL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,I6)') 'MSG_NBR_EGV_MAX#<35>#', '= ', PRJC%NEMX
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_EPS#<35>#', '= ', PRJC%EPS
      CALL LOG_ECRIS(LOG_BUF)
      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_RATIO_RESIDU#<35>#',  '= ', 
     &                                PRJC%RRES
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_PJ_POD_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PJ_POD_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POD_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pjpod_ic.fi'
      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pjpod_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = PJ_POD_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_PJ_POD_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PJ_POD_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PJ_POD_AID()

9999  CONTINUE
      IC_PJ_POD_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POD_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POD_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pjpod_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>proj_pod</b> projects the initial solution with a
C  Proper Orthogonal Decomposition method (POD). It aims
C  at providing a better initial estimate at low cost (Status: Experimental)
C</comment>
      IC_PJ_POD_REQCLS = 'proj_pod'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PJ_POD_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PJ_POD_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pjpod_ic.fi'
      INCLUDE 'pjpod.fi'
C-------------------------------------------------------------------------

      IC_PJ_POD_REQHDL = PJ_POD_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PJ_POD_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pjpod_ic.hlp')
      RETURN
      END

