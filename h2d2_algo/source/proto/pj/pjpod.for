C************************************************************************
C --- Copyright (c) INRS 2016-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ProJection de solution
C Objet:   Proper-Orthogonal-Decomposition
C Type:    Concret
C
C Functions:
C   Public:
C     SUBROUTINE PJ_POD_000
C     SUBROUTINE PJ_POD_999
C     SUBROUTINE PJ_POD_CTR
C     SUBROUTINE PJ_POD_DTR
C     SUBROUTINE PJ_POD_INI
C     SUBROUTINE PJ_POD_RST
C     SUBROUTINE PJ_POD_REQHBASE
C     SUBROUTINE PJ_POD_HVALIDE
C     SUBROUTINE PJ_POD_XEQ
C   Private:
C     SUBROUTINE PJ_POD_REQSELF
C     SUBROUTINE PJ_POD_RAZ
C     SUBROUTINE PJ_POD_ALLTBL
C     SUBROUTINE PJ_POD_CLCEGV
C     SUBROUTINE PJ_POD_CLCSOL
C     SUBROUTINE PJ_POD_MVM
C     SUBROUTINE PJ_POD_GMRS_SLV
C     SUBROUTINE PJ_POD_GMRS_PRC
C     SUBROUTINE PJ_POD_GMRS_RES
C     SUBROUTINE PJ_POD_NRMRES
C
C************************************************************************

      MODULE PJ_POD_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: PJ_POD_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: PJ_POD_SELF_T
         REAL*8  EPS       ! Tolérance
         REAL*8  RRES      ! Rapport des résidus
         REAL*8  TSOL      ! Temps du dernier VSOL
         INTEGER NDLL      ! Nombre de Degrés de Libertés Locaux
         INTEGER NEQL      ! Nombre d'EQuations Local
         INTEGER NSOL      ! Nombre de solutions
         INTEGER NEMX      ! Nombre d'EV MaX
         INTEGER NEAC      ! Nombre d'EV ACtu
         INTEGER NACC      ! Nbr de sol accumulées
         INTEGER NVKR      ! Nombre de vec de la base de Ritz
         INTEGER NITR      ! Nombre max d'itération de GMREs
         INTEGER HSIM      ! Handle sur l'élément
         INTEGER HRES      ! Handle sur le CB de calcul du résidu GMRes
         INTEGER KGMRS(8)  ! Tables pour GMRes
         LOGICAL DOEGV     ! .TRUE. s'il faut réactualiser les EGV
         REAL*8, DIMENSION(:,:), ALLOCATABLE :: VSOL
         REAL*8, DIMENSION(:,:), ALLOCATABLE :: VEGV
         REAL*8, DIMENSION(:),   ALLOCATABLE :: VDLG
         REAL*8, DIMENSION(:),   ALLOCATABLE :: VRES
      END TYPE PJ_POD_SELF_T

C---     Paramètres
      INTEGER, PARAMETER :: PJ_POD_NEGVMAX = 64

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PJ_POD_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PJ_POD_SELF_T), POINTER :: PJ_POD_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PJ_POD_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PJ_POD_REQSELF => SELF
      RETURN
      END FUNCTION PJ_POD_REQSELF

      END MODULE PJ_POD_M

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PJ_POD_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_000
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PJ_POD_HBASE, 'Projection: POD')

      PJ_POD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>PJ_POD_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_999
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PJ_POD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PJ_POD_HBASE, PJ_POD_DTR)

      PJ_POD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>PJ_POD_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>PJ_POD_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_CTR
CDEC$ ENDIF

      USE PJ_POD_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR, IRET
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PJ_POD_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PJ_POD_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PJ_POD_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PJ_POD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode PJ_POD_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_RAZ(HOBJ)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PJ_POD_REQSELF(HOBJ)
      SELF%TSOL =-1.0D0
      SELF%EPS  = 0.0D0
      SELF%RRES = 1.0D0
      SELF%NDLL = -1
      SELF%NEQL = -1
      SELF%NSOL = -1
      SELF%NEMX = -1
      SELF%NEAC =  0
      SELF%NACC =  0
      SELF%NVKR =  0
      SELF%NITR =  0
      SELF%HSIM =  0
      SELF%HRES =  0
      SELF%KGMRS(:) = 0
      SELF%DOEGV    = .FALSE.

      PJ_POD_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>PJ_POD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_DTR
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PJ_POD_RST(HOBJ)

C---     Dé-alloue
      SELF => PJ_POD_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PJ_POD_HBASE)

      PJ_POD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>PJ_POD_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     N        Nombre de vecteur à accumuler
C     M        Nombre de valeurs/vecteurs propres max
C     EPS      Tolérance
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_INI(HOBJ, N, M, EPS, RRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_INI
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      INTEGER M
      REAL*8  EPS
      REAL*8  RRES

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      INTEGER IRET
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles
      IF (N    .LT. 1) GOTO 9900
      IF (N    .LT. M) GOTO 9900
      IF (EPS  .LE. 1.0D-16) GOTO 9900
      IF (RRES .LE. 1.0D-04) GOTO 9901
      IF (RRES .GT. 2.0D-00) GOTO 9901

C---     Reset les données
      IERR = PJ_POD_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => PJ_POD_REQSELF(HOBJ)
         SELF%EPS  = EPS
         SELF%RRES = RRES
         SELF%NDLL = 0
         SELF%NEQL = 0
         SELF%NSOL = N
         SELF%NEMX = M  ! Nombre Eigen-values MaX
         SELF%NEAC = 0  ! Nombre Eigen-values ACtu
         SELF%NACC = 0
         SELF%NVKR = M + 2
         SELF%NITR = 3 * SELF%NVKR
         SELF%DOEGV= .FALSE.
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_EPS_INVALIDE', ': ', EPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_RAP_INVALIDE', ': ', RRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PJ_POD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_RST
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Désalloue la mémoire
      IERR = PJ_POD_ALLTBL(HOBJ, 0, 0)

C---     Reset les paramètres
      IERR = PJ_POD_RAZ(HOBJ)

      PJ_POD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PJ_POD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_REQHBASE
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INCLUDE 'pjpod.fi'
C------------------------------------------------------------------------

      PJ_POD_REQHBASE = PJ_POD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PJ_POD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_HVALIDE
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PJ_POD_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PJ_POD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une solution
C
C Description:
C     La méthode <code>PJ_POD_AJT(...)</code> ajoute une solution de
C     à la base.
C     La méthode peut être appelée de manière répétitive avec la même
C     solution. Celle ne sera insérée qu'une seule fois dans la base.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     TSIM        Temps associé à VDLG
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_AJT(HOBJ,
     &                    HSIM,
     &                    TSIM,
     &                    VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_AJT
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      REAL*8 , INTENT(IN) :: TSIM
      REAL*8 , INTENT(IN) :: VDLG(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      INTEGER ISOL
      INTEGER NDLL, NEQL
      INTEGER LLOCN
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)

C---     Récupère les données de simulation
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
D     CALL ERR_ASR(NDLL .GT. 0)

C---     Alloue les tables
      IERR = PJ_POD_ALLTBL(HOBJ, NDLL, NEQL)

C---     Accumule NSOL solutions dans VSOL (table de NSOL * NEQL)
      IF (TSIM .GT. SELF%TSOL) THEN
         ISOL = MOD(SELF%NACC, SELF%NSOL) +  1
         IERR = SP_ELEM_NDLT2NEQ(SELF%NEQL,
     &                           SELF%NDLL,
     &                           KA(SO_ALLC_REQKIND(KA,LLOCN)),
     &                           VDLG,
     &                           SELF%VSOL(:,ISOL))
         SELF%NACC = SELF%NACC + 1
         SELF%TSOL = TSIM
         IF (MOD(SELF%NACC, SELF%NSOL) .EQ. 0) SELF%DOEGV = .TRUE.
      ENDIF

C---     Calcul des EV
      IF (ERR_GOOD() .AND. SELF%DOEGV) THEN
         IERR = PJ_POD_CLCEGV(HOBJ)
         SELF%DOEGV = .FALSE.
      ENDIF

      PJ_POD_AJT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Projette la solution
C
C Description:
C     La méthode <code>PJ_POD_PRJ(...)</code> projette la solution de
C     manière à minimiser le résidu dans l'espace POD.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     HRES        Handle sur le CB de calcul du résidu
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_PRJ(HOBJ,
     &                    HSIM,
     &                    HRES,
     &                    TPRJ,
     &                    VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_PRJ
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      INTEGER, INTENT(IN) :: HRES
      REAL*8 , INTENT(IN) :: TPRJ
      REAL*8 , INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
      LOGICAL DO_PRJ
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HRES))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.projection.pod'

C---     Entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_PROJECTION_POD'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)

C---     Résous dans l'espace des EV
      DO_PRJ = (SELF%NACC .GE. SELF%NSOL)
      IF (ERR_GOOD() .AND. DO_PRJ) THEN
         SELF%HSIM = HSIM
         SELF%HRES = HRES
         IERR = PJ_POD_CLCSOL(HOBJ, VDLG)
      ENDIF

      CALL LOG_DECIND()

      PJ_POD_PRJ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Projette la solution
C
C Description:
C     La méthode <code>PJ_POD_XEQ(...)</code> projette la solution de
C     manière à minimiser le résidu dans l'espace POD.
C     La méthode peut être appelée de manière répétitive avec la même
C     solution. Celle ne sera insérée qu'une seule fois dans la base.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur l'élément
C     HRES        Handle sur le CB de calcul du résidu
C     TSIM        Temps associé à VDLG
C     TPRJ        Temps pour lequel projeter
C     VDLG        Table des Degrés de Liberté Globaux
C
C Sortie:
C     VDLG        DDL modifiés
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_XEQ(HOBJ,
     &                    HSIM,
     &                    HRES,
     &                    TSIM,
     &                    TPRJ,
     &                    VDLG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PJ_POD_XEQ
CDEC$ ENDIF

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HSIM
      INTEGER, INTENT(IN) :: HRES
      REAL*8 , INTENT(IN) :: TSIM
      REAL*8 , INTENT(IN) :: TPRJ
      REAL*8 , INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = PJ_POD_AJT(HOBJ, HSIM, TSIM, VDLG)
      IF (ERR_GOOD()) IERR = PJ_POD_PRJ(HOBJ, HSIM, HRES, TPRJ, VDLG)

      PJ_POD_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Alloue les tables de l'objet.
C
C Description:
C     La méthode privée <code>PJ_POD_ALLTBL(...)</code> alloue les tables
C     de travail de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     NDLL     Nombre Degrés de Liberté Locaux
C     NEQL     Nombre d'EQuations Locales
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_ALLTBL(HOBJ, NDLL, NEQL)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLL
      INTEGER, INTENT(IN) :: NEQL

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spgmrs.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IRET, IERR
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
      LOGICAL DOALLC, DOFREE
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)

C---     Désalloue les tables
      DOFREE = (NDLL .NE. SELF%NDLL)
      IF (ERR_GOOD() .AND. DOFREE) THEN
         IRET = 0
         IF (ALLOCATED(SELF%VSOL)) DEALLOCATE(SELF%VSOL,STAT=IRET)
         IF (ALLOCATED(SELF%VEGV)) DEALLOCATE(SELF%VEGV,STAT=IRET)
         IF (ALLOCATED(SELF%VDLG)) DEALLOCATE(SELF%VDLG,STAT=IRET)
         IF (ALLOCATED(SELF%VRES)) DEALLOCATE(SELF%VRES,STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         IERR = SP_GMRS_DEALLOC(SELF%KGMRS)
         SELF%NDLL = 0
         SELF%NEQL = 0
      ENDIF

C---     Alloue les tables
      DOALLC = (SELF%NDLL .EQ. 0 .AND. NDLL .GT. 0)
      IF (ERR_GOOD() .AND. DOALLC) THEN
         IRET = 0
         IF (IRET .EQ. 0) ALLOCATE(SELF%VSOL(NEQL,SELF%NSOL),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%VEGV(NEQL,SELF%NEMX),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%VRES(NEQL),STAT=IRET)
         IF (IRET .EQ. 0) ALLOCATE(SELF%VDLG(NDLL),STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900
         IERR = SP_GMRS_ALLOC(SELF%NEMX, SELF%NVKR, SELF%KGMRS)
         SELF%NDLL = NDLL
         SELF%NEQL = NEQL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PJ_POD_ALLTBL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les EV.
C
C Description:
C     La méthode privée <code>PJ_POD_CLCEGV(...)</code> calcule les EV.
C     En sortie SELF%VEGV est modifié et contient les nouveaux EV.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     L'algo n'est pas sensible à l'ordre dans lequel les SOL sont dans
C     VSOL, ce qui permet des les accumuler en continu (modulo NSOL) et
C     de calculer les EGV n'importe quand.
C************************************************************************
      FUNCTION PJ_POD_CLCEGV(HOBJ)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'pjpod.fc'

      REAL*8, DIMENSION(:,:), ALLOCATABLE :: v     ! Vectors de Krylov
      REAL*8, DIMENSION(:),   ALLOCATABLE :: workl ! (ncv*(ncv+8)),
      REAL*8, DIMENSION(:),   ALLOCATABLE :: workd ! (3*maxn)
      REAL*8, DIMENSION(:),   ALLOCATABLE :: resid !
      REAL*8, DIMENSION(:,:), ALLOCATABLE :: d     ! Eigenvalues
      LOGICAL,DIMENSION(:),   ALLOCATABLE :: select

      TYPE (PJ_POD_SELF_T), POINTER :: SELF

      REAL*8  TOL, SIGMA
      INTEGER IPARAM(11), IPNTR(11)
      INTEGER IERR, IRET
      INTEGER IDO, INFO
      INTEGER NX, NEV
      INTEGER NCV
      INTEGER LDV, LWORKL
      INTEGER I, J
      LOGICAL RVEC
      CHARACTER BMAT*1, WHICH*2
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.projection.pod'

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)
      NX  = SELF%NEQL      ! Dimension du problème
      NEV = SELF%NEMX      ! Nombre de vecteur propres
      NCV = MIN(NX, 2*NEV) ! Nombre de vecteurs de base
      TOL = 1.0D-12

C---     Alloue les tables
      IRET = 0
      IF (IRET .EQ. 0) ALLOCATE( V(NX, NCV), STAT=IRET )
      IF (IRET .EQ. 0) ALLOCATE( WORKL(NCV * (NCV+8)), STAT=IRET )
      IF (IRET .EQ. 0) ALLOCATE( WORKD(3*NX), STAT=IRET )
      IF (IRET .EQ. 0) ALLOCATE( RESID(NX),   STAT=IRET )
      IF (IRET .EQ. 0) ALLOCATE( D(NEV,2),    STAT=IRET )
      IF (IRET .EQ. 0) ALLOCATE( SELECT(NCV), STAT=IRET )
      IF (IRET .NE. 0) GOTO 9900

C---     Dimensions auxiliaires
      LDV = NX             ! Leading dimension of V
      LWORKL = NCV*(NCV+8) ! Size of LWORK

C---     Specification of Algorithm Mode
      BMAT  = 'I'       ! Standard problem
      WHICH = 'LM'      ! EV of largest magnitude
      IPARAM(1) = 1     ! exact shift strategy
      IPARAM(3) = 300   ! max iter of Arnoldi iterations
      IPARAM(7) = 1     ! Mode 1

C---     Calcul des EV
      INFO = 0
      IDO  = 0
100   CONTINUE
         CALL DSAUPD(IDO,     ! R
     &               BMAT,    ! Standard problem
     &               NX,      ! Matrix size = NX*NX
     &               WHICH,   ! EV of largest magnitude
     &               NEV,     ! Number of EV
     &               TOL,     ! Tolerance
     &               RESID,   ! ??
     &               NCV,     ! Arnoldi
     &               V,
     &               LDV,     ! LDV = NX
     &               IPARAM,
     &               IPNTR,
     &               WORKD,
     &               WORKL,
     &               LWORKL,
     &               INFO)
         IF (IDO .NE. -1 .AND. IDO .NE. 1) GOTO 199
C---        Perform matrix vector multiplication
         IERR = PJ_POD_MVM(HOBJ, NX, WORKD(IPNTR(1)), WORKD(IPNTR(2)))
      GO TO 100
199   CONTINUE

      IF (INFO .LT. 0) THEN
         GOTO 9901
      ELSEIF (INFO .EQ. 1) THEN
         WRITE(ERR_BUF, '(A)') 'ARPACK: Max number of iter reached.'
         CALL ERR_ASG(ERR_WRN, ERR_BUF)
      ELSE IF (INFO .EQ. 3) THEN
         WRITE(ERR_BUF, '(A)')
     &      'ARPACK: No shifts could be applied' //
     &      'during implicit Arnoldi update, try increasing NCV.'
         CALL ERR_ASG(ERR_WRN, ERR_BUF)
      ENDIF

C---     Post-process les eigenvectors
      RVEC = .TRUE.
      CALL DSEUPD (RVEC,     ! Compute Ritz vectors
     &             'A',      ! 'A': compute NEV Ritz vectors
     &             SELECT,
     &             D,        ! Eigenvalues
     &             V,        ! Eigenvectors
     &             LDV,
     &             SIGMA,    !
     &             BMAT,     ! Same param as DSAUPD
     &             NX,
     &             WHICH,
     &             NEV,
     &             TOL,
     &             RESID,
     &             NCV,
     &             V,
     &             LDV,
     &             IPARAM,
     &             IPNTR,
     &             WORKD,
     &             WORKL,
     &             LWORKL,
     &             INFO)

c         %----------------------------------------------%
c         | Eigenvalues are returned in the first column |
c         | of the two dimensional array D and the       |
c         | corresponding eigenvectors are returned in   |
c         | the first NCONV (=IPARAM(5)) columns of the  |
c         | two dimensional array V if requested.        |
c         %----------------------------------------------%
c
      IF (INFO .NE. 0) GOTO 9902

C---     Transfert les EV
      SELF%NEAC = 0
      DO I=1,NEV
         J = NEV-I+1
         IF (D(J,1) .LT. SELF%EPS) GOTO 299
         SELF%NEAC = SELF%NEAC + 1
      ENDDO
299   CONTINUE
      SELF%NEAC = MAX(SELF%NEAC, 3)
      DO I=1,SELF%NEAC
         J = NEV-I+1
         SELF%VEGV(:,I) = V(:,J)
      ENDDO
      WRITE(LOG_BUF, '(2A,I3,A,I3)') 'MSG_POD_NEGV', ': ',
     &                                SELF%NEAC, ' / ', SELF%NEMX
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, *) 'ERR_ALLOCATION_MEMOIRE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, *) 'Error with ARPACK function daupd, info = ',info
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, *) 'Error with ARPACK function deupd, info = ',info
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (ALLOCATED(SELECT)) DEALLOCATE(SELECT)
      IF (ALLOCATED(D))      DEALLOCATE(D)
      IF (ALLOCATED(RESID))  DEALLOCATE(RESID)
      IF (ALLOCATED(WORKD))  DEALLOCATE(WORKD)
      IF (ALLOCATED(WORKL))  DEALLOCATE(WORKL)
      IF (ALLOCATED(V))      DEALLOCATE(V)

      PJ_POD_CLCEGV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée <code>PJ_POD_CLCSOL(...)</code> calcule la solution
C     qui est la projection du
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C     VDLG(NDLL) a été introduit comme dernière solution dans VSOL(NEQL)
C************************************************************************
      FUNCTION PJ_POD_CLCSOL(HOBJ, VDLG)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      REAL*8,  INTENT(INOUT) :: VDLG(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'spgmrs.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      INTEGER IEG, ISOL, ID
      INTEGER HSIM
      INTEGER NDLL, NEQL
      INTEGER LLOCN
      REAL*8  VINI(PJ_POD_NEGVMAX)
      REAL*8  VFIN(PJ_POD_NEGVMAX)
      TYPE (PJ_POD_SELF_T), POINTER :: SELF

      REAL*8 RES1, RES2
      REAL*8 SCLC, SRJT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.algo.projection.pod'

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)
      HSIM = SELF%HSIM
      NEQL = SELF%NEQL
      NDLL = SELF%NDLL

C---     Récupère les données de simulation
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)

C---     Résidu initial
      RES1 = PJ_POD_NRMRES(HOBJ, VDLG, SELF%VRES)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POD_NORME_L2_RESIDU: ', RES1
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Monte la solution initiale (c.f. note)
D     CALL ERR_ASR(SELF%NEAC .LE. PJ_POD_NEGVMAX)
      ISOL = MOD(SELF%NACC-1, SELF%NSOL) + 1
      DO IEG=1,SELF%NEAC
         VINI(IEG) = DOT_PRODUCT( SELF%VEGV(:,IEG), SELF%VSOL(:,ISOL) )
      ENDDO

C---     Résous par INB
      SELF%VDLG(:) = VDLG(1:NDLL)   ! Copie pour le CL
      IERR = PJ_POD_GMRS_SLV(HOBJ, SELF%NEAC, VINI, VFIN)

C---     Monte la projection
      DO ID=1,SELF%NEQL
         SELF%VRES(ID) = DOT_PRODUCT(SELF%VEGV(ID,:), VFIN(1:SELF%NEAC))
      ENDDO
      SELF%VDLG(:) = VDLG(1:NDLL)
      IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                        NDLL,
     &                        KA(SO_ALLC_REQKIND(KA,LLOCN)),
     &                        SELF%VRES,
     &                        SELF%VDLG)

C---     Résidu final
      RES2 = PJ_POD_NRMRES(HOBJ, SELF%VDLG, SELF%VRES)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_POD_NORME_L2_RESIDU: ', RES2
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Contrôle
      SRJT = SELF%RRES*RES1   ! Seuil de rejet
      SCLC = 0.5D0 * SRJT     ! Seuil de recalcul
      IF (RES2 .GT. SCLC) THEN
         SELF%DOEGV = .TRUE.
         WRITE(LOG_BUF, '(A)') 'MSG_POD_FORCE_RECALCUL_EGV'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (RES2 .GT. SRJT) THEN
         WRITE(LOG_BUF, '(A)') 'MSG_POD_PROJECTION_REJETEE'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ELSE
         VDLG(1:NDLL) = SELF%VDLG(:)
      ENDIF

      PJ_POD_CLCSOL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée <code>PJ_POD_MVM(...)</code> retourne le produit
C     matrice*vecteur VO = SELF%V_T * SELF%V * VI
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     N        Dimension des tables
C     VI       Table entrée
C
C Sortie:
C     VO       Table sortie
C
C Notes:
C     SELF%VDLG n'est utilisé que pour NSOL valeur
C************************************************************************
      FUNCTION PJ_POD_MVM(HOBJ, N, VI, VO)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: N
      REAL*8,  INTENT(IN) :: VI(N)
      REAL*8,  INTENT(OUT):: VO(N)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      INTEGER ISOL, I
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%NEQL .EQ. N)

C---     TMP = VVEC_T * VI
      DO ISOL=1,SELF%NSOL
         SELF%VDLG(ISOL) = DOT_PRODUCT(SELF%VSOL(:,ISOL), VI(:))
      ENDDO

C---     VVEC * TMP
      DO I=1,N
         VO(I) = DOT_PRODUCT(SELF%VSOL(I,:), SELF%VDLG(1:SELF%NSOL))
      ENDDO

      PJ_POD_MVM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution par GMRes
C
C Description:
C     La méthode <code>PJ_POD_GMRS_SLV(...)</code> est la méthode
C     résoudre le système par un algorithme itératif de GMRes non
C     linéaire.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_GMRS_SLV(HOBJ, N, VINI, VFIN)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: N
      REAL*8,  INTENT(IN) :: VINI(N)
      REAL*8,  INTENT(OUT):: VFIN(N)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spgmrs.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      INTEGER LDLG, LLOCN
      INTEGER L_SOL, L_DLG0
      INTEGER NEGV, NVKR, NRDM
      REAL*8  EPSGMR
      REAL*8  VEPS (6)
      REAL*8  VSTAT(7)
      INTEGER I
      INTEGER, PARAMETER :: KLOCN(PJ_POD_NEGVMAX)=(/
     &                                            (I,I=1,PJ_POD_NEGVMAX)
     &                                             /)

      TYPE (PJ_POD_SELF_T), POINTER :: SELF

      EXTERNAL PJ_POD_GMRS_PRC
      EXTERNAL PJ_POD_GMRS_RES
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)
      NEGV = SELF%NEAC
      NVKR = SELF%NVKR
      NRDM = MAX(1, SELF%NITR / SELF%NVKR)
D     CALL ERR_ASR(N .EQ. NEGV)

C---     Paramètres de précision de GMRes
      VEPS(1) = SELF%EPS * 0.1D0
      VEPS(2) = 0.0D0
      VEPS(3) = 0.0D0

C---     Résous par GMRes
      IERR = SP_GMRS_SOLV(NRDM,     ! Nb de redem
     &                    NVKR,     ! Dim de la base de Krylov
     &                    NEGV,
     &                    NEGV,
     &                    KLOCN,
     &                    VINI,     ! INOUT: Sol. ini. et fin
     &                    VFIN,     ! OUT: incrément
     &                    VEPS,
     &                    VSTAT,
     &                    SELF%KGMRS,
     &                    HOBJ,
     &                    PJ_POD_GMRS_PRC,
     &                    HOBJ,
     &                    PJ_POD_GMRS_RES)
      VFIN(:) = VINI(:)

      PJ_POD_GMRS_SLV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Préconditionnement pour GMRes.
C
C Description:
C     La méthode privée <code>PJ_POD_GMRS_PRC(...)</code> est le
C     préconditionneur pour GMRes. Ici, il est NOOP.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     N        Dimension des tables
C     VI       Table entrée
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PJ_POD_GMRS_PRC(HOBJ, V)

      USE PJ_POD_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      REAL*8,  INTENT(IN) :: V(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      PJ_POD_GMRS_PRC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcul du résidu pour GMRes
C
C Description:
C     La méthode privée <code>PJ_POD_GMRS_RES(...)</code> retourne le
C     résidu du système résolu par GMRes.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     VDLG     Table entrée
C
C Sortie:
C     VRES     Table sortie
C
C Notes:
C     SELF%VDLG est initialisé dans CLCSOL pour contenir les CL.
C     SP_ELEM_NEQ2NDLT va les préserver.
C************************************************************************
      FUNCTION PJ_POD_GMRS_RES(HOBJ, VRES, VDLG, L)

      USE PJ_POD_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      REAL*8,  INTENT(OUT):: VRES(*)
      REAL*8,  INTENT(IN) :: VDLG(*)
      LOGICAL, INTENT(IN) :: L

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'pjpod.fc'

      INTEGER IERR
      INTEGER ID, IEV
      INTEGER HSIM
      INTEGER LLOCN
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)
      HSIM = SELF%HSIM

C---     Récupère les données de simulation
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)

C---     TMP = VVEC_T * VI
      DO ID=1,SELF%NEQL
         SELF%VRES(ID) = DOT_PRODUCT(SELF%VEGV(ID,:), VDLG(1:SELF%NEAC))
      ENDDO
      IERR = SP_ELEM_NEQ2NDLT(SELF%NEQL,
     &                        SELF%NDLL,
     &                        KA(SO_ALLC_REQKIND(KA,LLOCN)),
     &                        SELF%VRES,
     &                        SELF%VDLG)

C---     Calcul du résidu par call-back
      IERR = SO_FUNC_CALL3(SELF%HRES,
     &                     SO_ALLC_CST2B(SELF%VRES),
     &                     SO_ALLC_CST2B(SELF%VDLG),
     &                     SO_ALLC_CST2B(L))

C---     VVEC * TMP
      DO IEV=1,SELF%NEAC
         VRES(IEV) = DOT_PRODUCT(SELF%VEGV(:,IEV), SELF%VRES(:))
      ENDDO

      PJ_POD_GMRS_RES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme L2 du résidu.
C
C Description:
C     La méthode privée <code>PJ_POD_NRMRES(...)</code> calcule la norme
C     L2 du résidu. Elle retourne également le résidu dans VRES.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     VDLG     Table entrée
C
C Sortie:
C     VRES     Table sortie
C
C Notes:
C************************************************************************
      FUNCTION PJ_POD_NRMRES(HOBJ, VDLG, VRES)

      USE PJ_POD_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)    :: HOBJ
      REAL*8,  INTENT(IN)    :: VDLG(*)
      REAL*8,  INTENT(INOUT) :: VRES(*)

      INCLUDE 'pjpod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pjpod.fc'

      REAL*8  VNRM
      INTEGER IERR
      LOGICAL, PARAMETER :: L = .TRUE.
      TYPE (PJ_POD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PJ_POD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PJ_POD_REQSELF(HOBJ)

C---     Calcul du résidu par call-back
      IERR = SO_FUNC_CALL3(SELF%HRES,
     &                     SO_ALLC_CST2B(VRES(1)),
     &                     SO_ALLC_CST2B(VDLG(1)),
     &                     SO_ALLC_CST2B(L))

C---     La norme
      VNRM = DOT_PRODUCT(VRES(1:SELF%NEQL), VRES(1:SELF%NEQL))
      VNRM = SQRT(VNRM / SELF%NEQL)

9999  CONTINUE
      PJ_POD_NRMRES = VNRM
      RETURN
      END
