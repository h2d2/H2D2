C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: ccincr.for,v 1.16 2008/12/23 14:22:20 secretyv Exp $
C Groupe:  Critère de Convergence
C Objet:   Norme Infinie sur accroissement Global Relatif (tous les DDL)
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CC_INCR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_INCR_000()
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_000

      IMPLICIT NONE

      INCLUDE 'ccincr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccincr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CC_INCR_NOBJMAX,
     &                   CC_INCR_HBASE,
     &                   'Convergence Criteria on increment')

      CC_INCR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_INCR_999()
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_999

      IMPLICIT NONE

      INCLUDE 'ccincr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccincr.fc'

      INTEGER  IERR
      EXTERNAL CC_INCR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CC_INCR_NOBJMAX,
     &                   CC_INCR_HBASE,
     &                   CC_INCR_DTR)

      CC_INCR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_INCR_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccincr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccincr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CC_INCR_NOBJMAX,
     &                   CC_INCR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CC_INCR_HVALIDE(HOBJ))
         IOB = HOBJ - CC_INCR_HBASE

         CC_INCR_EPSA(IOB) = 0.0D0
         CC_INCR_EPSR(IOB) = 0.0D0
      ENDIF

      CC_INCR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_INCR_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccincr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccincr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_INCR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CC_INCR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CC_INCR_NOBJMAX,
     &                   CC_INCR_HBASE)

      CC_INCR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_INCR_INI(HOBJ, HSIM, EPSR, EPSA)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_INI

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      REAL*8  EPSR
      REAL*8  EPSA

      INCLUDE 'ccincr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'ccincr.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_INCR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES DONNÉES
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9900
      IF (EPSA  .LT. 0.0D0) GOTO 9902
      IF (EPSR  .LT. 0.0D0) GOTO 9902
      IF (EPSA  .EQ. 0.0D0 .AND. EPSR .EQ. 0.0D0) GOTO 9902

C---     RESET LES DONNEES
      IERR = CC_INCR_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - CC_INCR_HBASE
      CC_INCR_EPSA(IOB) = EPSA
      CC_INCR_EPSR(IOB) = EPSR

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,2PE12.5)') 'ERR_EPS_NEGATIF_NULL',':',
     &                           EPSR, EPSA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

      GOTO 9999

9999  CONTINUE
      CC_INCR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_INCR_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccincr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccincr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_INCR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CC_INCR_HBASE
      CC_INCR_EPSA(IOB) = 0.0D0
      CC_INCR_EPSR(IOB) = 0.0D0

      CC_INCR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CC_INCR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_INCR_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_REQHBASE

      IMPLICIT NONE

      INCLUDE 'ccincr.fi'
      INCLUDE 'ccincr.fc'
C------------------------------------------------------------------------

      CC_INCR_REQHBASE = CC_INCR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CC_INCR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_INCR_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccincr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ccincr.fc'
C------------------------------------------------------------------------

      CC_INCR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CC_INCR_NOBJMAX,
     &                                  CC_INCR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère de convergence est satisfait.
C
C Description:
C     La fonction <code>CC_N2GR_ESTSATISFAIT(...)</code> retourne .TRUE.
C     si le critère de convergence est satisfait.
C     Le critère est la norme infinie sur l'accroissement global relatif
C     (tous les ddl).
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_INCR_ESTSATISFAIT(HOBJ, HNUMR, NDLN, NNL, VDLG, VDEL)
CDEC$ATTRIBUTES DLLEXPORT :: CC_INCR_ESTSATISFAIT

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN*NNL)
      REAL*8  VDEL(NDLN*NNL)

      INCLUDE 'ccincr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'ccincr.fc'

      INTEGER I_ERROR
      INTEGER IERR
      
      INTEGER IOB
      REAL*8  EPSA, EPSR
      REAL*8  DRM
      LOGICAL FINI
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_INCR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - CC_INCR_HBASE
      EPSA = CC_INCR_EPSA(IOB)
      EPSR = CC_INCR_EPSR(IOB)

C---     Calcule la norme
      DRM = NR_UTIL_NIGR(HNUMR, NDLN, NNL, VDEL, VDLG, EPSA, EPSR)
      
C---     Calcule le critère
      FINI = (DRM .LT. 1.0D0)

      WRITE(LOG_BUF, '(A,1PE14.6E3,A,1PE14.6E3,A,L2)')
     &   'MSG_NRM_MAX_GLB#<20>#: ', DRM, ' /', 1.0D0, ', MSG_OK =', FINI
      CALL LOG_ECRIS(LOG_BUF)

      CC_INCR_ESTSATISFAIT = FINI
      RETURN
      END
