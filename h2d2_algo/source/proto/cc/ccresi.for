C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: ccresi.for,v 1.14 2008/12/23 14:22:20 secretyv Exp $
C Groupe:  Critère de Convergence
C Objet:   Norme L2 sur Accroissement Relatif (1 ddl)
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CC_RESI_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_RESI_000()
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_000

      IMPLICIT NONE

      INCLUDE 'ccresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccresi.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CC_RESI_NOBJMAX,
     &                   CC_RESI_HBASE,
     &                   'Convergence Criteria')

      CC_RESI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_RESI_999()
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_999

      IMPLICIT NONE

      INCLUDE 'ccresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccresi.fc'

      INTEGER  IERR
      EXTERNAL CC_RESI_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CC_RESI_NOBJMAX,
     &                   CC_RESI_HBASE,
     &                   CC_RESI_DTR)

      CC_RESI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_RESI_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccresi.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CC_RESI_NOBJMAX,
     &                   CC_RESI_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CC_RESI_HVALIDE(HOBJ))
         IOB = HOBJ - CC_RESI_HBASE

         CC_RESI_EPSA(IOB) = 0.0D0
         CC_RESI_EPSR(IOB) = 0.0D0
      ENDIF

      CC_RESI_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_RESI_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccresi.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CC_RESI_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CC_RESI_NOBJMAX,
     &                   CC_RESI_HBASE)

      CC_RESI_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_RESI_INI(HOBJ, HSIM, EPSR, EPSA)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_INI

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      REAL*8  EPSR
      REAL*8  EPSA

      INCLUDE 'ccresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'ccresi.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES DONNÉES
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9900
      NDLN = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
      IF (EPSA .LT. 0.0D0) GOTO 9902
      IF (EPSR .LT. 0.0D0) GOTO 9902
      IF (EPSA .EQ. 0.0D0 .AND. EPSR .EQ. 0.0D0) GOTO 9902

C---     RESET LES DONNEES
      IERR = CC_RESI_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - CC_RESI_HBASE
      CC_RESI_EPSA(IOB) = EPSA
      CC_RESI_EPSR(IOB) = EPSR

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,2PE12.5)') 'ERR_EPS_NEGATIF_NULL',':',
     &                           EPSR, EPSA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

      GOTO 9999

9999  CONTINUE
      CC_RESI_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_RESI_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ccresi.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CC_RESI_HBASE
      CC_RESI_EPSA(IOB) = 0.0D0
      CC_RESI_EPSR(IOB) = 0.0D0

      CC_RESI_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CC_RESI_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_RESI_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_REQHBASE

      IMPLICIT NONE

      INCLUDE 'ccresi.fi'
      INCLUDE 'ccresi.fc'
C------------------------------------------------------------------------

      CC_RESI_REQHBASE = CC_RESI_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CC_RESI_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_RESI_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ccresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ccresi.fc'
C------------------------------------------------------------------------

      CC_RESI_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CC_RESI_NOBJMAX,
     &                                  CC_RESI_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère de convergence est satisfait.
C
C Description:
C     La fonction <code>CC_N2GR_ESTSATISFAIT(...)</code> retourne .TRUE.
C     si le critère de convergence est satisfait.
C     Le critère est la norme L2 sur l'accroissement relatif (1 ddl).
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_RESI_ESTSATISFAIT(HOBJ, HNUMR, NEQL, VRES)
CDEC$ATTRIBUTES DLLEXPORT :: CC_RESI_ESTSATISFAIT

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NEQL
      REAL*8  VRES(NEQL)

      INCLUDE 'ccresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'ccresi.fc'

      INTEGER I_ERROR
      INTEGER IERR

      INTEGER IOB
      INTEGER IN, NNT, NNPP
      REAL*8  EPSA, EPSR
      REAL*8  DU, U, DR, DRM, DRT
      LOGICAL FINI
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - CC_RESI_HBASE
      EPSA = CC_RESI_EPSA(IOB)
      EPSR = CC_RESI_EPSR(IOB)

C---     CALCULE LE CRITÈRE
      NNPP = 0
      DRM = 0.0D0
      DO IN=1,NEQL
         NNPP = NNPP + 1
         DU = VRES(IN)
         DR = DU / EPSA
         DRM = DRM + DR*DR
      ENDDO

C---     SYNCHRONISE
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(DRM, DRT, 1, MPI_DOUBLE_PRECISION,
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(NNPP, NNT, 1, MPI_INTEGER,
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     CALCULE LE CRITERE
      DRM = SQRT(DRT / NNT)
      FINI = (DRM .LT. 1.0D0)

      WRITE(LOG_BUF, '(A,1PE14.6E3,A,1PE14.6E3,A,L2)')
     &   'MSG_NRM_L2_1DDL#<20>#: ', DRM, ' /', 1.0D0, ', MSG_OK =', FINI
      CALL LOG_ECRIS(LOG_BUF)

      CC_RESI_ESTSATISFAIT = FINI
      RETURN
      END
