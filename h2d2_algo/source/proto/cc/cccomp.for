C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: cccomp.for,v 1.14 2008/10/08 20:10:42 secretyv Exp $
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CC_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_COMP_000()
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_000

      IMPLICIT NONE

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CC_COMP_NOBJMAX,
     &                   CC_COMP_HBASE,
     &                   'Critere de convergence - Compose')

      CC_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_COMP_999()
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_999

      IMPLICIT NONE

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER  IERR
      EXTERNAL CC_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CC_COMP_NOBJMAX,
     &                   CC_COMP_HBASE,
     &                   CC_COMP_DTR)

      CC_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_COMP_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CC_COMP_NOBJMAX,
     &                   CC_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CC_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - CC_COMP_HBASE

         CC_COMP_IOPB (IOB) = CC_COMP_OPB_INDEFINI
         CC_COMP_NCRIA(IOB) = 0
         CC_COMP_LCRIA(IOB) = 0
      ENDIF

      CC_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_COMP_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CC_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CC_COMP_NOBJMAX,
     &                   CC_COMP_HBASE)

      CC_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_COMP_INI(HOBJ, IOPB, NCRIA, KCRIA)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_INI

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPB
      INTEGER NCRIA
      INTEGER KCRIA(*)

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCRIA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (IOPB .NE. CC_COMP_OPB_AND .AND.
     &    IOPB .NE. CC_COMP_OPB_OR) GOTO 9900
      IF (NCRIA .LT. 0) GOTO 9901
      DO I=1, NCRIA
         IF (.NOT. CC_CRIC_HVALIDE(KCRIA(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = CC_COMP_RST(HOBJ)

C---     ALLOUE LA MÉMOIRE POUR LA TABLE
      LCRIA = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NCRIA, LCRIA)

C---     REMPLIS LA TABLE
      IF (ERR_GOOD()) CALL ICOPY (NCRIA,
     &                            KCRIA, 1,
     &                            KA(SO_ALLC_REQKIND(KA,LCRIA)), 1)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CC_COMP_HBASE
         CC_COMP_IOPB (IOB) = IOPB
         CC_COMP_NCRIA(IOB) = NCRIA
         CC_COMP_LCRIA(IOB) = LCRIA
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)')'ERR_OPERATION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NCRIA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KCRIA(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CC_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_COMP_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LCRIA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CC_COMP_HBASE
      LCRIA = CC_COMP_LCRIA(IOB)

      IF (LCRIA .NE. 0) IERR = SO_ALLC_ALLIN4(0, LCRIA)

      CC_COMP_IOPB (IOB) = CC_COMP_OPB_INDEFINI
      CC_COMP_NCRIA(IOB) = 0
      CC_COMP_LCRIA(IOB) = 0

      CC_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CC_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_COMP_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_REQHBASE

      IMPLICIT NONE

      INCLUDE 'cccomp.fi'
      INCLUDE 'cccomp.fc'
C------------------------------------------------------------------------

      CC_COMP_REQHBASE = CC_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CC_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_COMP_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccomp.fc'
C------------------------------------------------------------------------

      CC_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CC_COMP_NOBJMAX,
     &                                  CC_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère de convergence est satisfait.
C
C Description:
C     La fonction <code>CC_COMP_ESTSATISFAIT(...)</code> retourne .TRUE.
C     si le critère de convergence est satisfait.
C     On compose les contributions des composantes.
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_COMP_ESTSATISFAIT(HOBJ, HNUMR, NDLN, NNL, VDLG, VDEL)
CDEC$ATTRIBUTES DLLEXPORT :: CC_COMP_ESTSATISFAIT

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)

      INCLUDE 'cccomp.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cccomp.fc'

      INTEGER IOB
      INTEGER IOPB
      INTEGER IC
      INTEGER NCRIA, LCRIA, HCRIA
      LOGICAL RESG, RESL
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - CC_COMP_HBASE
      IOPB  = CC_COMP_IOPB (IOB)
      NCRIA = CC_COMP_NCRIA(IOB)
      LCRIA = CC_COMP_LCRIA(IOB)
D     CALL ERR_ASR(NCRIA .GT. 0)
D     CALL ERR_ASR(LCRIA .NE. 0)

C---     INITIALISE LE RÉSULTAT GLOBAL
      IF     (IOPB .EQ. CC_COMP_OPB_AND) THEN
         RESG = .TRUE.
         WRITE(LOG_BUF, '(A)') 'MSG_NRM_COMP#<20>#: MSG_AND'
      ELSEIF (IOPB .EQ. CC_COMP_OPB_OR) THEN
         RESG = .FALSE.
         WRITE(LOG_BUF, '(A)') 'MSG_NRM_COMP#<20>#: MSG_OR'
      ENDIF
      CALL LOG_ECRIS(LOG_BUF)

C---     BOUCLE SUR LES CRITERES
      CALL LOG_INCIND()
      DO IC = 1, NCRIA
         HCRIA = SO_ALLC_REQIN4(LCRIA, IC)
         RESL = CC_CRIC_ESTSATISFAIT(HCRIA,
     &                               HNUMR,
     &                               NDLN,
     &                               NNL,
     &                               VDLG,
     &                               VDEL)

         IF (IOPB .EQ. CC_COMP_OPB_AND) THEN
            RESG = RESG .AND. RESL
            IF (.NOT. RESG) GOTO 199      ! COURT-CIRCUIT
         ELSEIF (IOPB .EQ. CC_COMP_OPB_OR) THEN
            RESG = RESG .OR. RESL
            IF (RESG) GOTO 199            ! COURT-CIRCUIT
D        ELSE
D           CALL ERR_ASR(.FALSE.)
         ENDIF

      ENDDO
199   CONTINUE
      CALL LOG_DECIND()
      WRITE(LOG_BUF, '(2A,L2)') 'MSG_NRM_COMP#<20>#: ', 'MSG_OK =', RESG
      CALL LOG_ECRIS(LOG_BUF)

      CC_COMP_ESTSATISFAIT = RESG
      RETURN
      END
