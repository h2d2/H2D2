C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: ccresi_ic.for,v 1.14 2009/03/17 21:08:37 dubepa Exp $
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_RESI_XEQCTR(IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_RESI_XEQCTR

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'ccresi_ic.fi'
      INCLUDE 'ccresi.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IDDL
      INTEGER HOBJ, HELE, HCRC
      REAL*8  EPSR, EPSA
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_RESI_AID()
            GOTO 9999
         ENDIF
      ENDIF
      
C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CRITERE_CONVERGENCE' //
     &                       '_L2_ACCROISSEMENT_RELATIF_UN_DDL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      HSIM = 0
      EPSR = 0
      EPSA = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C     <comment>Index of the degree of freedom to control</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, IDDL)
C     <comment>Relative epsilon</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, EPSR)
C     <comment>Absolute epsilon</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, EPSA)
      IF (IERR .NE. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET CONCRET
      HCRC = 0
      IF (ERR_GOOD()) IERR = CC_RESI_CTR(HCRC)
      IF (ERR_GOOD()) IERR = CC_RESI_INI(HCRC, HELE, IDDL, EPSR, EPSA)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
!!!      IF (ERR_GOOD()) IERR = CC_CRIC_CTR(HOBJ)
!!!      IF (ERR_GOOD()) IERR = CC_CRIC_INI(HOBJ, HCRC)

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HSIM)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SIMULATION#<25>#', '= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,I12)') 'MSG_DDL#<25>#', '= ',
     &                               IDDL
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_PRECISION_REL#<25>#','= ',
     &                               EPSR
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_PRECISION_ABS#<25>#','= ',
     &                               EPSA
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the stopping criterion</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>      
C  The constructor <b>cria_l2_1ddlrel</b> constructs an object, with the 
C  given arguments, and returns a handle on this object. 
C</comment>
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_RESI_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_RESI_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_RESI_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_RESI_XEQMTH

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'ccresi_ic.fi'
      INCLUDE 'ccresi.fi'
      INCLUDE 'ccresi.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
         IOB = HOBJ - CC_RESI_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'eps_rel') THEN
C           <comment>Relative epsilon</comment>
            RVAL = CC_RESI_EPSR(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
         ELSEIF (PROP .EQ. 'eps_abs') THEN
C           <comment>Absolute epsilon</comment>
            RVAL = CC_RESI_EPSA(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
         IOB = HOBJ - CC_RESI_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'eps_rel') THEN
C           <comment>Relative epsilon</comment>
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            CC_RESI_EPSR(IOB) = RVAL
         ELSEIF (PROP .EQ. 'eps_abs') THEN
C           <comment>Absolute epsilon</comment>
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            CC_RESI_EPSA(IOB) = RVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CC_RESI_DTR(HOBJ)
         
C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CC_RESI_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = CC_RESI_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test CC_RESI_PRN(HOBJ) -->')
         
C     <comment>The method <b>help</b> displays the help content for the class.</comment>         
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_RESI_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_FTL, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_RESI_AID()

9999  CONTINUE
      IC_RESI_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_RESI_REQCLS()
CDEC$ATTRIBUTES DLLEXPORT :: IC_RESI_REQCLS

      IMPLICIT NONE

      INCLUDE 'ccresi_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cria_l2_1ddlrel</b> represents the stopping criterion in L2 
C  norm, for one degree of freedom, for a relative increase. 
C  The condition on the solution increment is: 
C  || dU / (eps_r*|u| + eps_a) || < 1.0 
C  On a dof, it can be expressed as: dU < eps_r*|u| + eps_a
C</comment>
      IC_RESI_REQCLS = 'cria_l2_1ddlrel'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_RESI_REQHDL()
CDEC$ATTRIBUTES DLLEXPORT :: IC_RESI_REQHDL

      IMPLICIT NONE

      INCLUDE 'ccresi_ic.fi'
      INCLUDE 'ccresi.fi'
C-------------------------------------------------------------------------

      IC_RESI_REQHDL = CC_RESI_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description: 
C     La fonction IC_RESI_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_RESI_AID()

      INCLUDE 'log.fi'
     
      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('ccresi_ic.hlp')

      RETURN
      END
