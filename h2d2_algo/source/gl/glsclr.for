C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER GL_SCLR_000
C     INTEGER GL_SCLR_999
C     INTEGER GL_SCLR_CTR
C     INTEGER GL_SCLR_DTR
C     INTEGER GL_SCLR_INI
C     INTEGER GL_SCLR_RST
C     INTEGER GL_SCLR_REQHBASE
C     LOGICAL GL_SCLR_HVALIDE
C     INTEGER GL_SCLR_DEB
C     INTEGER GL_SCLR_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_SCLR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glsclr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_SCLR_NOBJMAX,
     &                   GL_SCLR_HBASE,
     &                   'Solution Scaler')

      GL_SCLR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glsclr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER  IERR
      EXTERNAL GL_SCLR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_SCLR_NOBJMAX,
     &                   GL_SCLR_HBASE,
     &                   GL_SCLR_DTR)

      GL_SCLR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_SCLR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_SCLR_NOBJMAX,
     &                   GL_SCLR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_SCLR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_SCLR_HBASE

         GL_SCLR_HSIM(IOB) = 0
         GL_SCLR_NVAL(IOB) = 0
      ENDIF

      GL_SCLR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_SCLR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_SCLR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_SCLR_NOBJMAX,
     &                   GL_SCLR_HBASE)

      GL_SCLR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_SCLR_INI(HOBJ, HSIM, NVAL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NVAL
      REAL*8  VVAL(NVAL)

      INCLUDE 'glsclr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glsclr.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IV
      INTEGER HDDL
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (.NOT. LM_HELE_HVALIDE(HSIM)) GOTO 9901
      NDLN = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
      IF (NVAL .NE. NDLN) GOTO 9902
      DO IV=1, NVAL
         IF (VVAL(IV) .LE. 0.0D0) GOTO 9903
      ENDDO

C---     Reset les données
      IERR = GL_SCLR_RST(HOBJ)

C---     Stocke les attributs
      IOB = HOBJ - GL_SCLR_HBASE
      GL_SCLR_HSIM(IOB) = HSIM
      GL_SCLR_NVAL(IOB) = NVAL
      DO IV=1, NVAL
         GL_SCLR_VDEL(IV,IOB) = VVAL(IV)
      ENDDO

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HSIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NBR_VALEURS_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3,A,1PE14.6E3)') 'ERR_DEL_LT_0',
     &                           ':', VVAL(IV), ' <= ', 0.0D0
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_SCLR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_SCLR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - GL_SCLR_HBASE
      GL_SCLR_HSIM(IOB) = 0
      GL_SCLR_NVAL(IOB) = 0

      GL_SCLR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_SCLR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glsclr.fi'
      INCLUDE 'glsclr.fc'
C------------------------------------------------------------------------

      GL_SCLR_REQHBASE = GL_SCLR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_SCLR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glsclr.fc'
C------------------------------------------------------------------------

      GL_SCLR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_SCLR_NOBJMAX,
     &                                  GL_SCLR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_SCLR_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaires et doit donc être appelée avant
C     GL_SCLR_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_SCLR_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Limite la solution
C
C Description:
C     La fonction GL_SCLR_XEQ retourne un incrément de solution limité en
C     fonction des paramètres de l'objet. Elle retourne le nombre de valeurs
C     limitées.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C************************************************************************
      FUNCTION GL_SCLR_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_SCLR_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glsclr.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glsclr.fc'

      REAL*8 UN
      PARAMETER (UN = 1.0D0)

      REAL*8  VTGT, VMAX_L, VSCL_L(2), VSCL_G(2)

      INTEGER IERR
      INTEGER IOB
      INTEGER ID, IN, NOMX_L
      INTEGER NVAL
      INTEGER I_ERROR

      INTEGER IDAMAX    ! Fonction BLAS
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.glob'

C---     Récupère les attributs
      IOB = HOBJ - GL_SCLR_HBASE
      NVAL = GL_SCLR_NVAL(IOB)

C---     Contrôles de cohérence
      IF (HSIM .NE. GL_SCLR_HSIM(IOB)) GOTO 9900
      IF (NVAL .NE. NDLN) GOTO 9901

C---     Calcule le facteur local
      VSCL_L(1) = UN
      VSCL_L(2) = -11   ! Pour avoir in=-1 et id=-1
      WRITE(LOG_BUF,'(A)') 'MSG_FACTEUR_LIMITEUR_PAR_DDL'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      DO ID=1, NDLN
         VTGT = GL_SCLR_VDEL(ID,IOB)

         NOMX_L = IDAMAX(NNL, VDEL(ID,1), NDLN)
         VMAX_L = ABS(VDEL(ID,NOMX_L))

         IF (VTGT/VMAX_L .LT. VSCL_L(1)) THEN
            VSCL_L(1) = VTGT/VMAX_L
            VSCL_L(2) = NOMX_L*10 + ID
         ENDIF
         WRITE(LOG_BUF,'(A,I6,1X,1PE14.6E3)') '#<3>#', ID, VSCL_L(1)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

C---     Synchronise
      VSCL_G(1) = 1.0D99
      VSCL_G(2) = -11
      CALL MPI_ALLREDUCE(VSCL_L, VSCL_G, 1, MPI_2DOUBLE_PRECISION,
     &                   MPI_MINLOC, MP_UTIL_REQCOMM(), I_ERROR)
     
C---     Limite
      IF (VSCL_G(1) .NE. UN) THEN
         CALL DSCAL(NNL*NDLN, VSCL_G(1), VDEL, 1)
      ENDIF

C---     L'état
      ISTTS = GL_STTS_OK
      IF (VSCL_G(1) .NE. UN) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (VSCL_G(1) .NE. UN) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     Log     
      IN = NINT(VSCL_G(2)/10)
      ID = NINT(VSCL_G(2)-IN*10)
      WRITE(LOG_BUF,'(A,1PE14.6E3,A,I2,A,I7,A)')
     &   'MSG_FACTEUR_LIMITEUR#<35>#= ', VSCL_G(1),
     &   ' ; DDL: (', ID, ', ', IN, ')'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_HANDLE_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSIM, 'MSG_SIMULATION')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NBR_VALEURS_INVALIDE',':',
     &                           NVAL, ' / ', NDLN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_SCLR_XEQ = ERR_TYP()
      RETURN
      END
