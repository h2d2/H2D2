C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLDMPR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLDMPR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'gldmpr_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fi'
      INCLUDE 'spstrn.fi'

      INTEGER, PARAMETER :: NVALMAX = 3

      REAL*8  AMIN(NVALMAX), AMAX(NVALMAX)
      REAL*8  RMIN(NVALMAX), RMAX(NVALMAX)
      INTEGER IERR
      INTEGER HOBJ
      INTEGER ILOG, I, IV
      INTEGER NTOK, NVAL
      LOGICAL LLOG
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GLDMPR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Les dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      NVAL = (NTOK-1) / 4
      IF (NVAL .GT. NVALMAX) GOTO 9902

C---     Lis les paramètres
      I = 0
      DO IV = 1,NVAL
C        <comment>
C        List of residual and damping coefficient, comma separated. 
C        For each degree of freedom, 4 values for:<br>
C        <ul>
C        <li>Res min (default 1.0)</li>
C        <li>Res max (default 100.0)</li>
C        <li>Alfa min (default 0.0)</li>
C        <li>Alfa max (default 1.0e-2)</li>
C        </ul>
C        </comment>
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, RMIN(IV))
         IF (IERR .NE. 0) RMIN = 1.0D0
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, RMAX(IV))
         IF (IERR .NE. 0) RMAX = 100.0D0
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, AMIN(IV))
         IF (IERR .NE. 0) AMIN = 0.0D0
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, AMAX(IV))
         IF (IERR .NE. 0) AMAX = 1.0D-2
      ENDDO
C     <comment>Logarithmic variation (default 0 = False = Linear)</comment>
      I = I + 1
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, ILOG)
      IF (IERR .NE. 0) ILOG = 0
      LLOG = (ILOG .NE. 0)

C---     Construis et initialise l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = GL_DMPR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = GL_DMPR_INI(HOBJ,
     &                                   NVAL,
     &                                   RMIN, RMAX,
     &                                   AMIN, AMAX,
     &                                   LLOG)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the damper
C        to be used in an algorithm with damping</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = GL_DMPR_PRN(HOBJ)
      ENDIF

C<comment>
C  The constructor <b>damper</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLDMPR_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_GLDMPR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLDMPR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLDMPR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'gldmpr_ic.fi'
      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     DISPATCH LES MÉTHODES, PROPRIÉTÉS ET OPERATEURS
!!!      IF (IMTH .EQ. '##property_get##') THEN
!!!D        CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
!!!         IOB = HOBJ - GL_DMPR_HBASE
!!!
!!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!!         IF (IERR .NE. 0) GOTO 9901
!!!
!!!C        <comment>Min damping, corresponding to min residual</comment>
!!!         IF     (PROP .EQ. 'amin') THEN
!!!            RVAL = GL_DMPR_AMIN(IOB)
!!!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!!!C        <comment>Max damping, corresponding to max residual</comment>
!!!         ELSEIF (PROP .EQ. 'amax') THEN
!!!            RVAL = GL_DMPR_AMAX(IOB)
!!!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!!!C        <comment>Min residual</comment>
!!!         ELSEIF (PROP .EQ. 'rmin') THEN
!!!            RVAL = GL_DMPR_RMIN(IOB)
!!!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!!!C        <comment>Max residual</comment>
!!!         ELSEIF (PROP .EQ. 'rmax') THEN
!!!            RVAL = GL_DMPR_RMAX(IOB)
!!!            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
!!!         ELSE
!!!            GOTO 9902
!!!         ENDIF
!!!
!!!      ELSEIF (IMTH .EQ. '##property_set##') THEN
!!!D        CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
!!!         IOB = HOBJ - GL_DMPR_HBASE
!!!
!!!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!!!         IF (IERR .NE. 0) GOTO 9901
!!!
!!!C        <comment>Min damping, corresponding to min residual</comment>
!!!         IF     (PROP .EQ. 'amin') THEN
!!!            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
!!!            GL_DMPR_AMIN(IOB) = RVAL
!!!C        <comment>Max damping, corresponding to max residual</comment>
!!!         ELSEIF (PROP .EQ. 'amax') THEN
!!!            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
!!!            GL_DMPR_AMAX(IOB) = RVAL
!!!C        <comment>Min residual</comment>
!!!         ELSEIF (PROP .EQ. 'rmin') THEN
!!!            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
!!!            GL_DMPR_RMIN(IOB) = RVAL
!!!C        <comment>Max residual</comment>
!!!         ELSEIF (PROP .EQ. 'rmax') THEN
!!!            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
!!!            GL_DMPR_RMAX(IOB) = RVAL
!!!         ELSE
!!!            GOTO 9902
!!!         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall
C     not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_DMPR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_DMPR_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GLDMPR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLDMPR_AID()

9999  CONTINUE
      IC_GLDMPR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLDMPR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLDMPR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gldmpr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>damper</b> is to be used with a damped solver like
C  <code>newton_damped</code> algorithm.
C  The damping criterion is nodal and calculated for each dof. It is based on
C  the residuum. (Status: Experimental)
C</comment>
      IC_GLDMPR_REQCLS = 'damper'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLDMPR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLDMPR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gldmpr_ic.fi'
      INCLUDE 'gldmpr.fi'
C-------------------------------------------------------------------------

      IC_GLDMPR_REQHDL = GL_DMPR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_GLDMPR_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GLDMPR_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('gldmpr_ic.hlp')

      RETURN
      END
