C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   Constant ReLaXation
C Type:    Concret
C
C Note:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_CRLX_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcrlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_CRLX_NOBJMAX,
     &                   GL_CRLX_HBASE,
     &                   'Constant Relaxation')

      GL_CRLX_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcrlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER  IERR
      EXTERNAL GL_CRLX_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_CRLX_NOBJMAX,
     &                   GL_CRLX_HBASE,
     &                   GL_CRLX_DTR)

      GL_CRLX_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_CRLX_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_CRLX_NOBJMAX,
     &                   GL_CRLX_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_CRLX_HVALIDE(HOBJ))
         IOB = HOBJ - GL_CRLX_HBASE

         GL_CRLX_RLAX(IOB) =  1.0D0
      ENDIF

      GL_CRLX_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_CRLX_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_CRLX_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_CRLX_NOBJMAX,
     &                   GL_CRLX_HBASE)

      GL_CRLX_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_CRLX_INI(HOBJ, RLAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  RLAX

      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (RLAX .LE. 0.0D0) GOTO 9900
      IF (RLAX .GE. 2.0D0) GOTO 9900

C---     RESET LES DONNEES
      IERR = GL_CRLX_RST(HOBJ)

C---     ENREGISTRE LES DONNEES
      IOB = HOBJ - GL_CRLX_HBASE
      GL_CRLX_RLAX(IOB) = RLAX

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              RLAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '[0, 2]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_CRLX_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_CRLX_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_CRLX_HBASE

C---     RESET
      GL_CRLX_RLAX(IOB) = 1.0D0

      GL_CRLX_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_CRLX_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcrlx.fi'
      INCLUDE 'glcrlx.fc'
C------------------------------------------------------------------------

      GL_CRLX_REQHBASE = GL_CRLX_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_CRLX_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glcrlx.fc'
C------------------------------------------------------------------------

      GL_CRLX_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_CRLX_NOBJMAX,
     &                                  GL_CRLX_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_CRLX_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_CRLX_HBASE

C---     EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_CONSTANT_RELAXATION'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_RLAX#<35>#= ',
     &                                 GL_CRLX_RLAX(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_CRLX_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_CRLX_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_CRLX_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_CRLX_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C************************************************************************
      FUNCTION GL_CRLX_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glcrlx.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glcrlx.fc'

      INTEGER IERR
      INTEGER IOB
      REAL*8  RLAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.glob'

C---     Récupère les attributs
      IOB  = HOBJ - GL_CRLX_HBASE
      RLAX = GL_CRLX_RLAX(IOB)

C---     Incrémente la solution
      IF (ERR_GOOD()) CALL DSCAL(NDLN*NNL, RLAX, VDEL, 1)

C---     L'état
      ISTTS = GL_STTS_OK
      ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      !! ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

      GL_CRLX_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La méthode GL_CRLX_REQTHETA retourne la valeur du facteur
C     d'amortissement.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     THETA       Facteur
C
C Sortie:
C
C Notes:
C     Le facteur est passé en argument pour être compatible avec
C     les appels via call-back du proxy.
C************************************************************************
      FUNCTION GL_CRLX_REQTHETA(HOBJ, THETA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_CRLX_REQTHETA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   THETA

      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      THETA = GL_CRLX_RLAX(HOBJ-GL_CRLX_HBASE)

      GL_CRLX_REQTHETA = ERR_TYP()
      RETURN
      END
