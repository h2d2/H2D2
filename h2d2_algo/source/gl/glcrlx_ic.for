C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLCRLX_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLCRLX_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'glcrlx_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcrlx.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HGLB, HOBJ
      REAL*8  RLX
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GLCRLX_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Constant relaxation coefficient</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, RLX)
      IF (IERR .NE. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET
      HGLB = 0
      IF (ERR_GOOD()) IERR = GL_CRLX_CTR(HGLB)
      IF (ERR_GOOD()) IERR = GL_CRLX_INI(HGLB, RLX)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = GL_GLBL_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = GL_GLBL_INI(HOBJ, HGLB)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the increment dU constant
C        relaxation algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = GL_CRLX_PRN(HGLB)
      ENDIF

C<comment>
C  The constructor <b>constant_relaxation</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLCRLX_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_GLCRLX_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLCRLX_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLCRLX_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'glcrlx_ic.fi'
      INCLUDE 'glcrlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall
C     not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_CRLX_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(GL_CRLX_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_CRLX_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GLCRLX_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLCRLX_AID()

9999  CONTINUE
      IC_GLCRLX_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLCRLX_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLCRLX_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcrlx_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>constant_relaxation</b> represents a constant relaxation
C  coefficient to be applied to the increment (descent direction) of an
C  algorithm like Picard or Newton.
C</comment>
      IC_GLCRLX_REQCLS = 'constant_relaxation'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLCRLX_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLCRLX_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glcrlx_ic.fi'
      INCLUDE 'glcrlx.fi'
C-------------------------------------------------------------------------

      IC_GLCRLX_REQHDL = GL_CRLX_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_GLCRLX_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GLCRLX_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('_GLCRLX_.hlp')

      RETURN
      END
