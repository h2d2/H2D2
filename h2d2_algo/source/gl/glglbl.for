C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   PRoXY
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à GL_GLBL_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA GL_GLBL_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'glglbl.fc'

      DATA GL_GLBL_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_GLBL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_GLBL_NOBJMAX,
     &                   GL_GLBL_HBASE,
     &                   'Proxy: Backtracking')

      GL_GLBL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fc'

      INTEGER  IERR
      EXTERNAL GL_GLBL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_GLBL_NOBJMAX,
     &                   GL_GLBL_HBASE,
     &                   GL_GLBL_DTR)

      GL_GLBL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_GLBL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_GLBL_NOBJMAX,
     &                   GL_GLBL_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_GLBL_HVALIDE(HOBJ))
         IOB = HOBJ - GL_GLBL_HBASE

         GL_GLBL_HOMG(IOB) = 0
         GL_GLBL_HMDL(IOB) = 0
         GL_GLBL_STTS(IOB) = GL_STTS_INDEFINI
      ENDIF

      GL_GLBL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_GLBL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_GLBL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_GLBL_NOBJMAX,
     &                   GL_GLBL_HBASE)

      GL_GLBL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C     NOMMDL      Nom du module
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'glglbl.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = GL_GLBL_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - GL_GLBL_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         GL_GLBL_HOMG(IOB) = HOMG
         GL_GLBL_HMDL(IOB) = HMDL
         GL_GLBL_STTS(IOB) = GL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_GLBL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_GLBL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'glglbl.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_GLBL_HBASE
      HOMG = GL_GLBL_HOMG(IOB)
      HMDL = GL_GLBL_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      GL_GLBL_HOMG(IOB) = 0
      GL_GLBL_HMDL(IOB) = 0
      GL_GLBL_STTS(IOB) = GL_STTS_INDEFINI

      GL_GLBL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_GLBL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glglbl.fi'
      INCLUDE 'glglbl.fc'
C------------------------------------------------------------------------

      GL_GLBL_REQHBASE = GL_GLBL_HBASE
      RETURN
      END

CC************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_GLBL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glglbl.fc'
C------------------------------------------------------------------------

      GL_GLBL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_GLBL_NOBJMAX,
     &                                  GL_GLBL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La fonction <code>GL_GLBL_DEB(...)</code>
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_DEB
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'glglbl.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG
      INTEGER  HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - GL_GLBL_HBASE

      HOMG = GL_GLBL_HOMG(IOB)
      HMDL = GL_GLBL_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DEB', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL1(HFNC,
     &                        SO_ALLC_CST2B(HOMG))
      ENDIF

      GL_GLBL_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne l'état de la globalisation.
C
C Description:
C     La fonction <code>GL_GLBL_REQSTATUS(...)</code> retourne 
C     le bit-set de l'état de la globalisation.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_GLBL_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glglbl.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_GLBL_REQSTATUS = GL_GLBL_STTS(HOBJ-GL_GLBL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La fonction <code>GL_GLBL_XEQ(...)</code>
C     On transmet directement l'appel à l'objet géré.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HRES,
     &                     F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  NDLN
      INTEGER  NNL
      REAL*8   VDLG(NDLN, NNL)
      REAL*8   VDEL(NDLN, NNL)
      REAL*8   ETA
      INTEGER  HRES
      INTEGER  F_RES
      EXTERNAL F_RES

      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'glglbl.fc'

      INTEGER IERR
      INTEGER HFRES, HFSLV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Temp - Construit les callable
      IERR = SO_FUNC_CTR   (HFRES)
      IERR = SO_FUNC_INIMTH(HFRES, HRES, F_RES)
      HFSLV = 0
!!!      IERR = SO_FUNC_CTR   (HFSLV)
!!!      IERR = SO_FUNC_INIMTH(HFSLV, HRES, F_RES)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_XEQ2(HOBJ,
     &                       HSIM,
     &                       NDLN,
     &                       NNL,
     &                       VDLG,
     &                       VDEL,
     &                       ETA,
     &                       HFRES,
     &                       HFSLV)
      ENDIF

C---     Détruis les callable
      IF (SO_FUNC_HVALIDE(HFRES)) IERR = SO_FUNC_DTR(HFRES)
      IF (SO_FUNC_HVALIDE(HFSLV)) IERR = SO_FUNC_DTR(HFSLV)

      GL_GLBL_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La fonction <code>GL_GLBL_XEQ(...)</code>
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     Temporaire
C     Nécessaire pour GL_COMP qui appelle de manière virtuelle avec
C     les callables et non les pointeurs aux fonctions.
C************************************************************************
      FUNCTION GL_GLBL_XEQ2(HOBJ,
     &                      HSIM,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ETA,
     &                      HFRES,
     &                      HFSLV)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV

      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcomp.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'glglbl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
      INTEGER ISTTS
      INTEGER KI(9)

      INTEGER  GL_GLBL_XEQ_CB
      EXTERNAL GL_GLBL_XEQ_CB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_GLBL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - GL_GLBL_HBASE
      HOMG = GL_GLBL_HOMG(IOB)
      HMDL = GL_GLBL_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'XEQ', HFNC)

C---     Charge la table avec les param pour le call-back
      KI(1) = HOMG
      KI(2) = HSIM
      KI(3) = NDLN
      KI(4) = NNL
      KI(5) = SO_ALLC_REQVHND(VDLG)
      KI(6) = SO_ALLC_REQVHND(VDEL)
      KI(7) = SO_ALLC_REQVHND(ETA)
      KI(8) = HFRES
      KI(9) = HFSLV

C---     Fait l'appel
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CBFNC2(HFNC,
     &                         GL_GLBL_XEQ_CB,
     &                         SO_ALLC_CST2B(KI),
     &                         SO_ALLC_CST2B(ISTTS))
      ENDIF

C----    Conserve l'état
      IF (ERR_GOOD()) THEN
         GL_GLBL_STTS(IOB) = ISTTS
      ENDIF

      GL_GLBL_XEQ2 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  GL_GLBL_XEQ_CB
C
C Description:
C     La fonction GL_GLBL_XEQ_CB fait le dispatch pour
C     l'assignation de C.L. suivant le type de condition. Elle
C     est appelée pour chaque limite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_XEQ_CB(F, KI, STTS)

      IMPLICIT NONE

      INTEGER  GL_GLBL_XEQ_CB
      INTEGER  F
      EXTERNAL F
      INTEGER  IL
      INTEGER  KI(9)
      INTEGER  STTS

      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = F(KI(1),
     &         KI(2),
     &         KI(3),
     &         KI(4),
     &         KA(SO_ALLC_REQKIND(KA, KI(5))),
     &         KA(SO_ALLC_REQKIND(KA, KI(6))),
     &         KA(SO_ALLC_REQKIND(KA, KI(7))),
     &         KI(8),
     &         KI(9),
     &         STTS)

      GL_GLBL_XEQ_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme du résidu.
C
C Description:
C     La fonction statique GL_GLBL_CLCRES calcule la norme du résidu pour une
C     valeur de Theta.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     NDLL        Nombre de Degrés de Liberté Locaux
C     NEQ         Nombre d'EQuations
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     HRES        Handle sur le résidu
C     HFRES       Fonction call-back de calcul de résidu
C
C Sortie:
C     Po          Norme du résidu
C
C Notes:
C************************************************************************
      FUNCTION GL_GLBL_CLCRES(VNRM,
     &                        THETA,
     &                        NDLL,
     &                        NEQ,
     &                        VDLG,
     &                        VDEL,
     &                        VTRV,
     &                        VRES,
     &                        HFRES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_GLBL_CLCRES
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      REAL*8   VNRM
      REAL*8   THETA
      INTEGER  HOBJ
      INTEGER  NDLL
      INTEGER  NEQ
      REAL*8   VDLG(NDLL)
      REAL*8   VDEL(NDLL)
      REAL*8   VTRV(NDLL)
      REAL*8   VRES(NEQ)
      INTEGER  HFRES

      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_ERROR

      INTEGER IERR
      LOGICAL ADLG
      REAL*8  DN

      REAL*8  DNRM2     ! Fonction BLAS
C------------------------------------------------------------------------

C---     Assemble K(U_0) dans VRES
      ADLG = .TRUE.
      IF (THETA .EQ. 0.0D0) THEN
         IERR = SO_FUNC_CALL3(HFRES,
     &                        SO_ALLC_CST2B(VRES(:)),
     &                        SO_ALLC_CST2B(VDLG(:)),
     &                        SO_ALLC_CST2B(ADLG))

C---     Assemble K(U_0 + Theta*dU) dans VRES
      ELSE
C---        U_0 + Theta*dU dans VTRV
         CALL DCOPY(NDLL, VDLG, 1, VTRV, 1)
         CALL DAXPY(NDLL, THETA, VDEL, 1, VTRV, 1)

C---        Assemble K(U_0 + Theta*dU) dans VRES
         IERR = SO_FUNC_CALL3(HFRES,
     &                        SO_ALLC_CST2B(VRES),
     &                        SO_ALLC_CST2B(VTRV),
     &                        SO_ALLC_CST2B(ADLG))
      ENDIF

C---     Calcule et réduis la norme
      IF (ERR_GOOD()) THEN
         DN = DNRM2(NEQ, VRES, 1)
         DN = DN * DN
         CALL MPI_ALLREDUCE(DN, VNRM, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
         VNRM = SQRT(VNRM)
      ENDIF

      GL_GLBL_CLCRES = ERR_TYP()
      RETURN
      END
       