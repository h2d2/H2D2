C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C      FUNCTION IC_GLSCLR_XEQCTR ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLSCLR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLSCLR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE
      CHARACTER*(*) IPRM

      INCLUDE 'glsclr_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glsclr_ic.fc'

      INTEGER IERR
      INTEGER HGLB, HOBJ
      INTEGER I
      INTEGER HELE
      INTEGER NVAL
      INTEGER NVALMAX
      PARAMETER (NVALMAX = 24)
      REAL*8  VVAL(NVALMAX)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GLSCLR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 1
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HELE)
      IF (IERR .NE. 0) GOTO 9901
100   CONTINUE
         IF (I.EQ.NVALMAX) GOTO 9902
         I = I + 1
C        <comment>
C        List of increment limit values, comma separated.
C        </comment>
         IERR = SP_STRN_TKR(IPRM, ',', I, VVAL(I-1))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9901
      GOTO 100
199   CONTINUE
      NVAL = I-2

C---     Controle
      IF (NVAL .LE. 0) GOTO 9904

C---     Construis et initialise l'objet
      HGLB = 0
      IF (ERR_GOOD()) IERR = GL_SCLR_CTR(HGLB)
      IF (ERR_GOOD()) IERR = GL_SCLR_INI(HGLB, HELE, NVAL, VVAL)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = GL_GLBL_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = GL_GLBL_INI(HOBJ, HGLB)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the limiter</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_GLSCLR_PRN(HGLB)
      ENDIF

C<comment>
C  The constructor <b>limiter</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLSCLR_AID()

9999  CONTINUE
      IC_GLSCLR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_GLSCLR_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLSCLR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glsclr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glsclr.fc'
      INCLUDE 'glsclr_ic.fc'

      INTEGER I, IOB
      INTEGER IERR
      INTEGER HSIM
      INTEGER NVAL
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_SCALER'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRIME LES PARAMETRES
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - GL_SCLR_HBASE
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         HSIM = GL_SCLR_HSIM(IOB)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HSIM)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SIMULATION#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         NVAL = GL_SCLR_NVAL(IOB)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_VAL#<35>#= ', NVAL
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,1PE14.6E3)')
     &      'MSG_VAL (del)#<35>#= ', GL_SCLR_VDEL(1,IOB)
         CALL LOG_ECRIS(LOG_BUF)
         DO I=2, NVAL
            WRITE (LOG_BUF,'(A,1PE14.6E3)')
     &       '#<35>#  ', GL_SCLR_VDEL(I,IOB)
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
      ENDIF

      CALL LOG_DECIND()

      IC_GLSCLR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLSCLR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLSCLR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'glsclr_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glsclr.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glsclr.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      HSIM
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------
C-----------------------------------------------------------------------
      IERR = ERR_OK

C---     opb_[]
C     <comment>Get a limiter value with its index (rvalue) (v=h_lim[2])</comment>
      IF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_SCLR_HBASE

C        <comment>Index of the value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (IVAL .LE. 0) GOTO 9902
         IF (IVAL .GT. GL_SCLR_NVAL(IOB)) GOTO 9902
         IF (ERR_GOOD()) RVAL = GL_SCLR_VDEL(IVAL, IOB)
         IF (ERR_GOOD()) WRITE(IPRM, '(2A, 1PE25.17E3)') 'R', ',', RVAL

      ELSEIF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_SCLR_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the element</comment>
         IF (PROP .EQ. 'helem') THEN
            IVAL = GL_SCLR_HSIM(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Number of degree of freedom per node</comment>
         ELSEIF (PROP .EQ. 'ndln') THEN
            HSIM = GL_SCLR_HSIM(IOB)
            IVAL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Number of values</comment>
         ELSEIF (PROP .EQ. 'nval') THEN
            IVAL = GL_SCLR_NVAL(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C---     opb_[]
C     <comment>Set a limiter value with its index (lvalue) (h_lim[2]=v)</comment>
      ELSEIF (IMTH .EQ. '##opb_[]_set##') THEN
D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_SCLR_HBASE

C        <comment>Index of the value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IVAL .LE. 0) GOTO 9902
         IF (IVAL .GT. GL_SCLR_NVAL(IOB)) GOTO 9902
C        <comment>Value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) GL_SCLR_VDEL(IVAL, IOB) = RVAL

C---     SET
!      ELSEIF (IMTH .EQ. '##property_set##') THEN
!        set
!D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
!         IOB = HOBJ - GL_SCLR_HBASE
!
!         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
!         IF (IERR .NE. 0) GOTO 9901
!
!         IF (PROP .EQ. 'ndln') THEN
!C           <comment>Number of degree of freedom per node</comment>
!            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
!            IF (IERR .NE. 0) GOTO 9901
!            GL_SCLR_HSIM(IOB) = IVAL
!         ELSEIF (PROP .EQ. 'nval') THEN
!C           <comment>Number of values</comment>
!            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
!            IF (IERR .NE. 0) GOTO 9901
!            GL_SCLR_NVAL(IOB) = IVAL
!         ELSE
!            GOTO 9903
!         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = GL_SCLR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(GL_SCLR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = GL_SCLR_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test GL_SCLR_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_GLSCLR_AID()

      ELSE
         GOTO 9904
      ENDIF

      GOTO 9999

C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A,I6)') 'ERR_INDICE_INVALIDE',': ', IVAL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLSCLR_AID()

9999  CONTINUE
      IC_GLSCLR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLSCLR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLSCLR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glsclr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>glob_scaler</b> represents for each degree of freedom, the
C  maximum increment allowed. If any increment is larger than the corresponding
C  maximum value, all the increments will be down-scaled.
C  <p>
C  if (du > du_max), du is limited in such a way that max(du) = du_max.
C</comment>
      IC_GLSCLR_REQCLS = 'glob_scaler'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLSCLR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLSCLR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glsclr_ic.fi'
      INCLUDE 'glsclr.fi'
C-------------------------------------------------------------------------

      IC_GLSCLR_REQHDL = GL_SCLR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_GLSCLR_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GLSCLR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('glsclr_ic.hlp')

      RETURN
      END

