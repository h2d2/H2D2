C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   BackTracKing 2.nd Order
C Type:    Concret
C
C Note:
C     "Globalization techniques for Newton-Krylov methods and applications
C     to the fully-coupled solution of the Navier-Stokes equations"
C     Roger P. Pawlowski, John N. Shadid,
C     Joseph P. Simonis, and Homer F. Walker
C     SIAM Review
C     Volume 48, Issue 4 (November 2006), Pages: 700-721
C     ISSN: 0036-1445
C
C     Inspiré de sandia.gov, class NOX_LineSearch_Polynomial.C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_BTK2_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtk2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_BTK2_NOBJMAX,
     &                   GL_BTK2_HBASE,
     &                   'Backtracking 2.nd Order')

      GL_BTK2_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtk2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER  IERR
      EXTERNAL GL_BTK2_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_BTK2_NOBJMAX,
     &                   GL_BTK2_HBASE,
     &                   GL_BTK2_DTR)

      GL_BTK2_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTK2_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_BTK2_NOBJMAX,
     &                   GL_BTK2_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_BTK2_HVALIDE(HOBJ))
         IOB = HOBJ - GL_BTK2_HBASE

         GL_BTK2_LRES(IOB) = 0
         GL_BTK2_LTRV(IOB) = 0
         GL_BTK2_TMIN(IOB) = 0.0D0
         GL_BTK2_TMAX(IOB) = 1.0D0
      ENDIF

      GL_BTK2_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTK2_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_BTK2_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_BTK2_NOBJMAX,
     &                   GL_BTK2_HBASE)

      GL_BTK2_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTK2_INI(HOBJ, TMIN, TMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TMIN
      REAL*8  TMAX

      INCLUDE 'glbtk2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (TMIN .LE. 0.0D0) GOTO 9900
      IF (TMAX .LE. 0.0D0) GOTO 9900
      IF (TMAX .GT. 1.0D1) GOTO 9900
      IF (TMIN .GE. TMAX)  GOTO 9900

C---     RESET LES DONNEES
      IERR = GL_BTK2_RST(HOBJ)

C---     ENREGISTRE LES DONNEES
      IOB = HOBJ - GL_BTK2_HBASE
      GL_BTK2_TMIN(IOB) = TMIN
      GL_BTK2_TMAX(IOB) = TMAX

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              TMIN, TMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(AMIN,AMAX) DANS [0, 10]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_BTK2_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTK2_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LRES, LTRV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_BTK2_HBASE
      LRES = GL_BTK2_LRES(IOB)
      LTRV = GL_BTK2_LTRV(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LTRV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTRV)
      IF (LRES .NE. 0) IERR = SO_ALLC_ALLRE8(0, LRES)

C---     RESET
      GL_BTK2_LRES(IOB) = 0
      GL_BTK2_LTRV(IOB) = 0
      GL_BTK2_TMIN(IOB) = 0.0D0
      GL_BTK2_TMAX(IOB) = 1.0D0

      GL_BTK2_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_BTK2_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtk2.fi'
      INCLUDE 'glbtk2.fc'
C------------------------------------------------------------------------

      GL_BTK2_REQHBASE = GL_BTK2_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_BTK2_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glbtk2.fc'
C------------------------------------------------------------------------

      GL_BTK2_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_BTK2_NOBJMAX,
     &                                  GL_BTK2_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_BTK2_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_BTK2_HBASE

C-------  EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob.btrk2'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_BACKTRACKING_2ND'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_AMIN#<35>#= ',
     &                                 GL_BTK2_TMIN(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_AMAX#<35>#= ',
     &                                 GL_BTK2_TMAX(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_BTK2_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_BTK2_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_BTK2_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTK2_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glbtk2.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_BTK2_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     1) Le broadcast de THETA, c'est ceinture et bretelles
C************************************************************************
      FUNCTION GL_BTK2_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTK2_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glbtk2.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbtk2.fc'

      INTEGER I_MASTER
      INTEGER I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER HDDL
      INTEGER ITER, NITER_MAX
      INTEGER NEQL, NDLL
      INTEGER LRES, LTRV

      REAL*8  P0, P1, DP0, PM1
      REAL*8  THETA, TSTEP, TMIN, TMAX, TM1
      REAL*8  A

      REAL*8  ALFA
      REAL*8  EPS
      REAL*8  THETA_MIN, THETA_MAX
      REAL*8  UN, ZERO
      PARAMETER (ALFA      = 1.0D-04)     ! t, dans le papier
      PARAMETER (EPS       = 1.0D-07)
      PARAMETER (THETA_MIN = 0.1D+00)
      PARAMETER (THETA_MAX = 0.5D+00)
      PARAMETER (UN        = 1.0D+00)
      PARAMETER (ZERO      = 0.0D+00)
      PARAMETER (NITER_MAX = 10)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTK2_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.glob.btrk2'

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_BTK2_HBASE
      LRES = GL_BTK2_LRES(IOB)
      LTRV = GL_BTK2_LTRV(IOB)
      TMIN = GL_BTK2_TMIN(IOB)
      TMAX = GL_BTK2_TMAX(IOB)

C---     RECUPERE LES DONNEES
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      NDLL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLL)
D     CALL ERR_ASR(NDLL .EQ. NDLN*NNL)

C---     Initialise
      THETA = TMAX
      ITER  = 0

C---     (Re-)Alloue la mémoire
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NEQL, LRES)
         GL_BTK2_LRES(IOB) = LRES
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NDLL, LTRV)
         GL_BTK2_LTRV(IOB) = LTRV
      ENDIF

C---     || U_0 ||
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_CLCRES(P0,
     &                         ZERO,
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Backtracking P0: ', P0
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     || U_0 + dU ||
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_CLCRES(P1,
     &                         THETA,  ! TMAX
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Backtracking P1: ', P1
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Teste pour voir si on sort tout de suite
      IF (ERR_GOOD()) THEN
         A = UN - ALFA*(UN - ETA)
         IF (P1 .LE. A*P0) GOTO 200
      ENDIF

C---     || U_0 + EPS*dU ||
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_CLCRES(DP0,
     &                         EPS,
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)
      ENDIF
      IF (ERR_GOOD()) THEN
         DP0 = (DP0-P0) / EPS
         IF (DP0 .GE. ZERO) GOTO 199   ! PENTE > 0 ==> NON CONVERGENCE
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Backtracking dP: ', DP0
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     BOUCLE WHILE
100   CONTINUE
         ITER = ITER + 1

C---        MINIMISE LE POLYNOME DES RÉSIDUS
         IF (ERR_GOOD()) THEN
!            TM1 = THETA
!            PM1 = P1
!            TSTEP = - 0.5D0 * DP0*TM1*TM1 / (PM1 - P0 - DP0*TM1)
            TSTEP = - 0.5D0 * DP0 / (P1 - P0 - DP0)
            TSTEP = MAX(TSTEP, THETA_MIN)
            TSTEP = MIN(TSTEP, THETA_MAX)
            THETA = THETA*TSTEP
         ENDIF
         IF (ITER  .EQ. NITER_MAX) GOTO 199
         IF (THETA .LE. TMIN) GOTO 199

C---        || U_0 + Theta*dU ||
         IF (ERR_GOOD()) THEN
            IERR = GL_GLBL_CLCRES(P1,
     &                            THETA,
     &                            NDLL,
     &                            NEQL,
     &                            VDLG,
     &                            VDEL,
     &                            VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                            VA(SO_ALLC_REQVIND(VA, LRES)),
     &                            HFRES)
         ENDIF

C---        Les critères
         ETA = UN - THETA*(UN - ETA)
         A = UN - ALFA*(UN - ETA)

C---        Log
         WRITE(LOG_BUF,'(A,I3,3(1X,1PE14.6E3))')
     &                  '#<2>#Backtracking Iter: ',ITER, THETA, P1, ETA
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

         IF (P1 .LE. A*P0) GOTO 200
      GOTO 100    ! END WHILE

C---     Non convergence
199   CONTINUE
      THETA = TMIN
      ITER  = -1

C---     Incrémente la solution
200   CONTINUE
      IF (ERR_GOOD() .AND. THETA .NE. 1.0D0) THEN
         CALL MPI_BCAST(THETA, 1, MP_TYPE_RE8(),
     &                  I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
         CALL DSCAL(NDLL, THETA, VDEL, 1)
      ENDIF

C---     L'état
      ISTTS = GL_STTS_OK
      IF (ITER .NE. 0) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (ITER .LT. 0) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     LOG
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3,A, I3)')
     &         'Backtracking with ALFA#<35>#= ',THETA, ' iter = ', ITER
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

      GL_BTK2_XEQ = ERR_TYP()
      RETURN
      END
