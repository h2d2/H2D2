C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   BackTRaKing
C Type:    Concret
C
C Note:
C     "Globalization techniques for Newton-Krylov methods and applications
C     to the fully-coupled solution of the Navier-Stokes equations"
C     Roger P. Pawlowski, John N. Shadid,
C     Joseph P. Simonis, and Homer F. Walker
C     SIAM Review
C     Volume 48, Issue 4 (November 2006), Pages: 700-721
C     ISSN: 0036-1445
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_BTRK_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtrk.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_BTRK_NOBJMAX,
     &                   GL_BTRK_HBASE,
     &                   'Backtracking exact')

      GL_BTRK_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtrk.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER  IERR
      EXTERNAL GL_BTRK_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_BTRK_NOBJMAX,
     &                   GL_BTRK_HBASE,
     &                   GL_BTRK_DTR)

      GL_BTRK_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTRK_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_BTRK_NOBJMAX,
     &                   GL_BTRK_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_BTRK_HVALIDE(HOBJ))
         IOB = HOBJ - GL_BTRK_HBASE

         GL_BTRK_LRES(IOB) = 0
         GL_BTRK_LTRV(IOB) = 0
         GL_BTRK_NPAS(IOB) = 0
         GL_BTRK_TMIN(IOB) = 0.0D0
         GL_BTRK_TMAX(IOB) = 0.0D0
      ENDIF

      GL_BTRK_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTRK_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_BTRK_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_BTRK_NOBJMAX,
     &                   GL_BTRK_HBASE)

      GL_BTRK_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTRK_INI(HOBJ, NPAS, TMIN, TMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPAS
      REAL*8  TMIN
      REAL*8  TMAX

      INCLUDE 'glbtrk.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (NPAS .LE.  0) GOTO 9900
      IF (NPAS .GT. 50) GOTO 9900
      IF (TMIN .LT. 0.0D0) GOTO 9901
      IF (TMAX .LE. 0.0D0) GOTO 9901
      IF (TMAX .GT. 2.0D0) GOTO 9901
      IF (TMIN .GE. TMAX)  GOTO 9901

C---     RESET LES DONNEES
      IERR = GL_BTRK_RST(HOBJ)

C---     ENREGISTRE LES DONNEES
      IOB = HOBJ - GL_BTRK_HBASE
      GL_BTRK_NPAS(IOB) = NPAS
      GL_BTRK_TMIN(IOB) = TMIN
      GL_BTRK_TMAX(IOB) = TMAX

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPAS_INVALIDE',': ', NPAS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(NPAS) DANS [1, 50]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              TMIN, TMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(TMIN,TMAX) DANS [0, 2]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_BTRK_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BTRK_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LRES, LTRV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_BTRK_HBASE
      LRES = GL_BTRK_LRES(IOB)
      LTRV = GL_BTRK_LTRV(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LTRV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTRV)
      IF (LRES .NE. 0) IERR = SO_ALLC_ALLRE8(0, LRES)

C---     RESET
      GL_BTRK_LRES(IOB) = 0
      GL_BTRK_LTRV(IOB) = 0
      GL_BTRK_NPAS(IOB) = 0
      GL_BTRK_TMIN(IOB) = 0.0D0
      GL_BTRK_TMAX(IOB) = 0.0D0

      GL_BTRK_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_BTRK_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbtrk.fi'
      INCLUDE 'glbtrk.fc'
C------------------------------------------------------------------------

      GL_BTRK_REQHBASE = GL_BTRK_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_BTRK_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glbtrk.fc'
C------------------------------------------------------------------------

      GL_BTRK_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_BTRK_NOBJMAX,
     &                                  GL_BTRK_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_BTRK_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_BTRK_HBASE

C---     EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob.step'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_BACKTRACKING'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      WRITE (LOG_BUF,'(A,I12)')       'MSG_NPAS#<35>#= ',
     &                                 GL_BTRK_NPAS(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_AMIN#<35>#= ',
     &                                 GL_BTRK_TMIN(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_AMAX#<35>#= ',
     &                                 GL_BTRK_TMAX(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_BTRK_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_BTRK_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_BTRK_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BTRK_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glbtrk.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_BTRK_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     1) Le broadcast de THETA, c'est ceinture et bretelles
C     2) On compare les normes au carré
C************************************************************************
      FUNCTION GL_BTRK_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BTRK_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glbtrk.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbtrk.fc'

      INTEGER I_MASTER
      INTEGER I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER IT, IOPTI
      INTEGER HDDL
      INTEGER NEQL, NDLL
      INTEGER LRES, LTRV
      INTEGER NPAS
      REAL*8  P, PMIN
      REAL*8  TMIN, TMAX, TOPTI
      REAL*8  THETA, DT
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BTRK_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.glob.step'

C---     Récupère les attributs
      IOB  = HOBJ - GL_BTRK_HBASE
      LRES = GL_BTRK_LRES(IOB)
      LTRV = GL_BTRK_LTRV(IOB)
      NPAS = GL_BTRK_NPAS(IOB)
      TMIN = GL_BTRK_TMIN(IOB)
      TMAX = GL_BTRK_TMAX(IOB)

C---     Récupère les données
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
D     CALL ERR_ASR(NDLL .EQ. NDLN*NNL)

C---     Alloue la mémoire
      IF (ERR_GOOD() .AND. LRES .EQ. 0) THEN
         IERR = SO_ALLC_ALLRE8(NEQL, LRES)
         GL_BTRK_LRES(IOB) = LRES
      ENDIF
      IF (ERR_GOOD() .AND. LTRV .EQ. 0) THEN
         IERR = SO_ALLC_ALLRE8(NDLL, LTRV)
         GL_BTRK_LTRV(IOB) = LTRV
      ENDIF

C---     Boucle
D     CALL ERR_ASR(NPAS .GT. 0)
      DT = (TMAX-TMIN)/NPAS
      PMIN  = 1.0D+99
      TOPTI = (TMAX+TMIN)*0.5D0
      THETA = TMIN
      DO IT=1,NPAS+1
         IERR = GL_GLBL_CLCRES(P,
     &                         THETA,
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)

         IF (P .LT. PMIN) THEN
            IOPTI = IT
            TOPTI = THETA
            PMIN  = P
         ENDIF

         WRITE(LOG_BUF,'(A,2(1PE14.6E3))')'Backtracking:',THETA,P
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

         THETA = THETA + DT
      ENDDO
      TOPTI = MAX(TOPTI, DT)

C---     Incrémente la solution
      IF (ERR_GOOD() .AND. THETA .NE. 1.0D0) THEN
         CALL MPI_BCAST(TOPTI, 1, MP_TYPE_RE8(),
     &                  I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
         CALL DSCAL(NDLL, TOPTI, VDEL, 1)
      ENDIF

C---     L'état
      ISTTS = GL_STTS_OK
      IF (IOPTI .GE.      1) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (IOPTI .LT. NPAS+1) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (IOPTI .EQ. 1) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     LOG
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3)')
     &         'Backtracking with ALFA#<35>#= ',TOPTI
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

      GL_BTRK_XEQ = ERR_TYP()
      RETURN
      END
