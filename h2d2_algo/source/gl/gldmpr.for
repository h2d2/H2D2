C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  GLobalisation
C Objet:   Kt Damper
C Type:    Concret
C Note:
C
C Interface:
C   H2D2 Module: GL
C      H2D2 Class: GL_DMPR
C         SUBROUTINE GL_DMPR_000
C         SUBROUTINE GL_DMPR_999
C         SUBROUTINE GL_DMPR_CTR
C         SUBROUTINE GL_DMPR_DTR
C         SUBROUTINE GL_DMPR_INI
C         SUBROUTINE GL_DMPR_RST
C         SUBROUTINE GL_DMPR_REQHBASE
C         SUBROUTINE GL_DMPR_HVALIDE
C         SUBROUTINE GL_DMPR_PRN
C         SUBROUTINE GL_DMPR_DEB
C         SUBROUTINE GL_DMPR_XEQ
C         SUBROUTINE GL_DMPR_REQTHETA
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_DMPR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gldmpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_DMPR_NOBJMAX,
     &                   GL_DMPR_HBASE,
     &                   'Kt Damper')

      GL_DMPR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gldmpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER  IERR
      EXTERNAL GL_DMPR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_DMPR_NOBJMAX,
     &                   GL_DMPR_HBASE,
     &                   GL_DMPR_DTR)

      GL_DMPR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_DMPR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_DMPR_NOBJMAX,
     &                   GL_DMPR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_DMPR_HVALIDE(HOBJ))
         IOB = HOBJ - GL_DMPR_HBASE

         GL_DMPR_AMIN(:,IOB) = 0.0D0
         GL_DMPR_AMAX(:,IOB) = 1.0D0
         GL_DMPR_RMIN(:,IOB) = 0.0D0
         GL_DMPR_RMAX(:,IOB) = 1.0D0
         GL_DMPR_ALFA(IOB) = 1.0D0
         GL_DMPR_NVAL(IOB) = 0
         GL_DMPR_LNEQ(IOB) = 0
         GL_DMPR_LDLG(IOB) = 0
         GL_DMPR_LLOG(IOB) = .FALSE.
      ENDIF

      GL_DMPR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_DMPR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_DMPR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_DMPR_NOBJMAX,
     &                   GL_DMPR_HBASE)

      GL_DMPR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_DMPR_INI(HOBJ, NVAL, RMIN, RMAX, AMIN, AMAX, LLOG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      REAL*8  RMIN(NVAL)
      REAL*8  RMAX(NVAL)
      REAL*8  AMIN(NVAL)
      REAL*8  AMAX(NVAL)
      LOGICAL LLOG

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (MINVAL(RMIN) .LE. 0.0D0) GOTO 9900
      IF (MAXVAL(RMAX) .LE. 0.0D0) GOTO 9900
!!      IF (RMAX .GT. 1.0D1) GOTO 9900
      IF (ANY(RMIN .GE. RMAX))  GOTO 9900
      IF (MINVAL(AMIN) .LT. 0.0D0) GOTO 9901
      IF (MAXVAL(AMAX) .LE. 0.0D0) GOTO 9901
      IF (MAXVAL(AMAX) .GT. 1.0D0) GOTO 9901
      IF (ANY(AMIN .GE. AMAX))  GOTO 9901

C---     Reset les données
      IERR = GL_DMPR_RST(HOBJ)

C---     Enregistre les données
      IOB = HOBJ - GL_DMPR_HBASE
      GL_DMPR_AMIN(:,IOB) = AMIN
      GL_DMPR_AMAX(:,IOB) = AMAX
      GL_DMPR_RMIN(:,IOB) = RMIN
      GL_DMPR_RMAX(:,IOB) = RMAX
      GL_DMPR_NVAL(IOB) = NVAL
      GL_DMPR_LLOG(IOB) = LLOG

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              RMIN, RMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(RMIN,RMAX) DANS [0, oo['
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              AMIN, AMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(AMIN,AMAX) DANS [0, 1]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_DMPR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_DMPR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LNEQ, LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - GL_DMPR_HBASE
      LNEQ = GL_DMPR_LNEQ(IOB)
      LDLG = GL_DMPR_LDLG(IOB)

C---     Désalloue la mémoire
      IF (LNEQ .NE. 0) IERR = SO_ALLC_ALLRE8(0, LNEQ)
      IF (LDLG .NE. 0) IERR = SO_ALLC_ALLRE8(0, LDLG)

C---     Reset
      GL_DMPR_AMIN(:,IOB) = 0.0D0
      GL_DMPR_AMAX(:,IOB) = 1.0D0
      GL_DMPR_RMIN(:,IOB) = 0.0D0
      GL_DMPR_RMAX(:,IOB) = 1.0D0
      GL_DMPR_ALFA(IOB) = 1.0D0
      GL_DMPR_NVAL(IOB) = 0
      GL_DMPR_LNEQ(IOB) = 0
      GL_DMPR_LDLG(IOB) = 0
      GL_DMPR_LLOG(IOB) = .FALSE.

      GL_DMPR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_DMPR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'gldmpr.fi'
      INCLUDE 'gldmpr.fc'
C------------------------------------------------------------------------

      GL_DMPR_REQHBASE = GL_DMPR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_DMPR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'gldmpr.fc'
C------------------------------------------------------------------------

      GL_DMPR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_DMPR_NOBJMAX,
     &                                  GL_DMPR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_DMPR_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - GL_DMPR_HBASE

C-------  En-tête de commande
      LOG_ZNE = 'h2d2.glob.ktdamper'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_KT_DAMPER'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Impression des composantes
      WRITE (LOG_BUF,'(A,I6)') 'MSG_NBR_VAL#<35>#= ',
     &                           GL_DMPR_NVAL(IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,3(1PE14.6E3))') 'MSG_RMIN#<35>#= ',
     &                                 GL_DMPR_RMIN(:,IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,3(1PE14.6E3))') 'MSG_RMAX#<35>#= ',
     &                                 GL_DMPR_RMAX(:,IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,3(1PE14.6E3))') 'MSG_AMIN#<35>#= ',
     &                                 GL_DMPR_AMIN(:,IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,3(1PE14.6E3))') 'MSG_AMAX#<35>#= ',
     &                                 GL_DMPR_AMAX(:,IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_DMPR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_DMPR_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_DMPR_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_DMPR_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     HRES        Handle sur le résidu
C     F_RES       Fonction call-back de calcul de résidu
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     2) Le broadcast de THETA, c'est ceinture et bretelles
C************************************************************************
      FUNCTION GL_DMPR_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     HRES,
     &                     F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_XEQ
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_PTR_T, SO_ALLC_PTR
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      INTEGER HRES
      INTEGER F_RES
      EXTERNAL F_RES

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'gldmpr.fc'

      INTEGER I_ERROR

      REAL*8, PARAMETER :: UN   = 1.0D+00
      REAL*8, PARAMETER :: ZERO = 0.0D+00

      REAL*8  DN
      REAL*8  R(GL_DMPR_NMAX), RMIN(GL_DMPR_NMAX), RMAX(GL_DMPR_NMAX)
      REAL*8  A(GL_DMPR_NMAX), AMIN(GL_DMPR_NMAX), AMAX(GL_DMPR_NMAX)
      REAL*8  SBUF(2), RBUF(2)

      INTEGER IERR
      INTEGER IOB
      INTEGER ID, IN
      INTEGER HDDL
      INTEGER NEQL, NDLL
      INTEGER NVAL
      INTEGER LNEQ, LDLG, LLOCN
      LOGICAL LLOG

      TYPE(SO_ALLC_PTR_T) :: P_NEQ, P_TMP
      REAL*8, POINTER :: V_NEQ(:), V_TMP(:,:)

!!!      REAL*8  DNRM2     ! Fonction BLAS
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.glob.ktdamper'

C---     Récupère les attributs
      IOB  = HOBJ - GL_DMPR_HBASE
      AMIN = GL_DMPR_AMIN(:,IOB)
      AMAX = GL_DMPR_AMAX(:,IOB)
      RMIN = GL_DMPR_RMIN(:,IOB)
      RMAX = GL_DMPR_RMAX(:,IOB)
      NVAL = GL_DMPR_NVAL(IOB)
      LNEQ = GL_DMPR_LNEQ(IOB)
      LDLG = GL_DMPR_LDLG(IOB)
      LLOG = GL_DMPR_LLOG(IOB)
      IF (LLOG) THEN
         RMIN = LOG(RMIN)
         RMAX = LOG(RMAX)
      ENDIF
      CALL ERR_ASR(NVAL .EQ. NDLN)     ! TODO: changer en erreur

C---     Récupère les données
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
D     CALL ERR_ASR(NDLL .EQ. NDLN*NNL)
D     CALL ERR_ASR(NNL  .EQ. LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL))
D     CALL ERR_ASR(NDLN .EQ. LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN))

C---     (Re-)Alloue la mémoire
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NEQL, LNEQ)
         IERR = SO_ALLC_ALLRE8(NDLL, LDLG)
         GL_DMPR_LNEQ(IOB) = LNEQ
         GL_DMPR_LDLG(IOB) = LDLG
      ENDIF

C---     Calcule et réduis la norme
      P_NEQ = SO_ALLC_PTR(LNEQ)
      P_TMP = SO_ALLC_PTR(LDLG)
      V_NEQ(1:NEQL) => P_NEQ%CST2V()
      V_TMP(1:NDLN, 1:NNL) => P_TMP%CST2V()
      IF (ERR_GOOD()) THEN
         IERR = F_RES(HRES,
     &                V_NEQ,
     &                VDLG,
     &                .TRUE.)   ! ADLG
         V_NEQ(1:NEQL) = ABS(V_NEQ(1:NEQL)) + 1.0D-99
      ENDIF

C---     Passe de NEQL à NDLL
      IF (ERR_GOOD()) THEN
         CALL DINIT(NDLL, 1.0D-99, V_TMP, 1)
         IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           V_NEQ,
     &                           V_TMP)
      ENDIF
      
!!!      IF (ERR_GOOD()) THEN
!!!         DN = DNRM2(NEQL, VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
!!!         SBUF(1) = DN * DN
!!!         SBUF(2) = NEQL
!!!         CALL MPI_ALLREDUCE(SBUF, RBUF, 2, MP_TYPE_RE8(),
!!!     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
!!!         R = SQRT(RBUF(1)/RBUF(2))
!!!      ENDIF
!!!      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'Kt Damping: Residual#<35>#= ', R
!!!      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      IF (ERR_GOOD() .AND. 
     &    LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_DEBUG)) THEN
         WRITE(LOG_BUF,'(A)') 'Kt Damping: Residual'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = MINVAL(V_TMP(:,1:NNL), 2)
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># min  = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = MAXVAL(V_TMP(:,1:NNL), 2)
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># max  = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = SUM(V_TMP(:,1:NNL), 2) / NNL
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># mean = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         DO IN=1, NDLN
            R(IN) = SUM( (V_TMP(IN,1:NNL)-R(IN))**2 ) / (NNL-1)
         ENDDO
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># sdev = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Loi - Variation linéaire entre min et max
      IF (LLOG) V_TMP(:,:) = LOG(V_TMP(:,:))
      DO IN=1,NNL
         R = V_TMP(:,IN)
         R = MAX(R, RMIN)
         R = MIN(R, RMAX)
         R = (R-RMIN) / (RMAX-RMIN)
         V_TMP(:,IN) = AMIN + (AMAX-AMIN) * R
      ENDDO

C---     LOG
!!!      IF (ERR_GOOD()) THEN
!!!         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'Kt Damping with ALFA#<35>#= ',A
!!!         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
!!!      ENDIF
      IF (ERR_GOOD() .AND. 
     &    LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_DEBUG)) THEN
         WRITE(LOG_BUF,'(A)') 'Kt Damping: ALFA'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = MINVAL(V_TMP(:,1:NNL), 2)
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># min  = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = MAXVAL(V_TMP(:,1:NNL), 2)
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># max  = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         R = SUM(V_TMP(:,1:NNL), 2)/NEQL
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># mean = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         DO IN=1, NDLN
            R(IN) = SUM( (V_TMP(IN,1:NNL)-R(IN))**2 ) / (NNL-1)
         ENDDO
         WRITE(LOG_BUF,'(A,3(1PE14.6E3))') '#<5># sdev = ', R(1:NDLN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Repasse de NDLL à NEQL
      IF (ERR_GOOD()) THEN
         IERR = SP_ELEM_NDLT2NEQ(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           V_TMP,
     &                           V_NEQ)
      ENDIF

C---     Conserve le résultat
      IF (ERR_GOOD()) THEN
         GL_DMPR_ALFA(IOB) = V_TMP(1,1)  !! A(1)
      ENDIF

      GL_DMPR_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Facteur d'amortissement
C
C Description:
C     La méthode GL_DMPR_REQTHETA retourne la valeur du facteur
C     d'amortissement.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     THETA       Facteur
C
C Sortie:
C
C Notes:
C     Le facteur est passé en argument pour être compatible avec
C     les appels via call-back du proxy.
C************************************************************************
      FUNCTION GL_DMPR_REQTHETA(HOBJ, THETA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_REQTHETA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   THETA

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      THETA = GL_DMPR_ALFA(HOBJ-GL_DMPR_HBASE)

      GL_DMPR_REQTHETA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Handle sur la table des amortissement.
C
C Description:
C     La méthode GL_DMPR_REQLTHT retourne le handle sur la table
C     des coefficients d'amortissement.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_DMPR_REQLTHT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_DMPR_REQLTHT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'gldmpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'gldmpr.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_DMPR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      GL_DMPR_REQLTHT = GL_DMPR_LNEQ(HOBJ-GL_DMPR_HBASE)
      RETURN
      END
      