C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   Adaptative ReLaXation
C Type:    Concret
C
C Note:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_ARLX_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glarlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_ARLX_NOBJMAX,
     &                   GL_ARLX_HBASE,
     &                   'Adaptive Relaxation')

      GL_ARLX_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glarlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER  IERR
      EXTERNAL GL_ARLX_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_ARLX_NOBJMAX,
     &                   GL_ARLX_HBASE,
     &                   GL_ARLX_DTR)

      GL_ARLX_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_ARLX_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_ARLX_NOBJMAX,
     &                   GL_ARLX_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_ARLX_HVALIDE(HOBJ))
         IOB = HOBJ - GL_ARLX_HBASE

         GL_ARLX_RMIN(IOB) =  0.0D0
         GL_ARLX_RMAX(IOB) =  0.0D0
         GL_ARLX_RLX2(IOB) =  1.0D0
         GL_ARLX_RLX3(IOB) =  1.0D0
         GL_ARLX_RES2(IOB) = -1.0D0
         GL_ARLX_RES3(IOB) = -1.0D0
         GL_ARLX_LRES(IOB) = 0
         GL_ARLX_LTRV(IOB) = 0
      ENDIF

      GL_ARLX_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_ARLX_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_ARLX_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_ARLX_NOBJMAX,
     &                   GL_ARLX_HBASE)

      GL_ARLX_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_ARLX_INI(HOBJ, RMIN, RMAX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  RMIN
      REAL*8  RMAX

      INCLUDE 'glarlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (RMIN .LE. 0.0D0) GOTO 9900
      IF (RMAX .LE. 0.0D0) GOTO 9900
      IF (RMAX .GT. 1.0D0) GOTO 9900
      IF (RMIN .GE. RMAX)  GOTO 9900

C---     RESET LES DONNEES
      IERR = GL_ARLX_RST(HOBJ)

C---     ENREGISTRE LES DONNEES
      IOB = HOBJ - GL_ARLX_HBASE
      GL_ARLX_RMIN(IOB) = RMIN
      GL_ARLX_RMAX(IOB) = RMAX

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_BORNES_INVALIDES',': ',
     &                              RMIN, RMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        '(RMIN,RMAX) DANS [0, 1]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_ARLX_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_ARLX_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LRES, LTRV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_ARLX_HBASE
      LRES = GL_ARLX_LRES(IOB)
      LTRV = GL_ARLX_LTRV(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LTRV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTRV)
      IF (LRES .NE. 0) IERR = SO_ALLC_ALLRE8(0, LRES)

C---     RESET
      GL_ARLX_RMIN(IOB) =  0.0D0
      GL_ARLX_RMAX(IOB) =  0.0D0
      GL_ARLX_RLX2(IOB) =  1.0D0
      GL_ARLX_RLX3(IOB) =  1.0D0
      GL_ARLX_RES2(IOB) = -1.0D0
      GL_ARLX_RES3(IOB) = -1.0D0
      GL_ARLX_LRES(IOB) = 0
      GL_ARLX_LTRV(IOB) = 0

      GL_ARLX_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_ARLX_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glarlx.fi'
      INCLUDE 'glarlx.fc'
C------------------------------------------------------------------------

      GL_ARLX_REQHBASE = GL_ARLX_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_ARLX_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glarlx.fc'
C------------------------------------------------------------------------

      GL_ARLX_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_ARLX_NOBJMAX,
     &                                  GL_ARLX_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_ARLX_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_ARLX_HBASE

C-------  EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_ADAPTATIVE_RELAXATION'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_RMIN#<35>#= ',
     &                                 GL_ARLX_RMIN(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_RMAX#<35>#= ',
     &                                 GL_ARLX_RMAX(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_ARLX_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_ARLX_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_ARLX_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_ARLX_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glarlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_ARLX_HBASE
      GL_ARLX_RLX2(IOB) =  1.0D0
      GL_ARLX_RES2(IOB) = -1.0D0

      GL_ARLX_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HSIM        Handle sur les données de simulation
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Degrés de liberté
C     VDEL        Incrément de solution
C     ETA         Adapting Forcing Term
C     HFRES       Fonction call-back de calcul de résidu
C     HFSLV       Fonction call-back de résolution
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     Référence:
C        MODFLOW-2000, THE U.S. GEOLOGICAL SURVEY
C        MODULAR GROUND-WATER MODEL - USER GUIDE TO THE
C        LINK-AMG (LMG) PACKAGE FOR SOLVING MATRIX
C        EQUATIONS USING AN ALGEBRAIC MULTIGRID SOLVER
C        By STEFFEN W. MEHL and MARY C. HILL
C        U.S. GEOLOGICAL SURVEY
C        Open-File Report 01-177
C        Prepared in cooperation with the
C        U.S. Department of Energy
C        Denver, Colorado
C        2001
C
C     Le broadcast de THETA, c'est ceinture et bretelles
C************************************************************************
      FUNCTION GL_ARLX_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glarlx.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glarlx.fc'

      INTEGER I_MASTER, I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER HDDL
      INTEGER NEQL, NDLL
      INTEGER LRES, LTRV

      REAL*8  RMIN, RMAX
      REAL*8  RLX1, RLX2, RLX3
      REAL*8  RES1, RES2, RES3
      REAL*8  VDUM(1)
      REAL*8  RRED, DREL, RREL

      REAL*8  UN, ZERO, PETIT
      PARAMETER (UN   = 1.0D+00)
      PARAMETER (ZERO = 0.0D+00)
      PARAMETER (PETIT= 1.0D-16)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.glob.arlx'

C---     Récupère les attributs
      IOB  = HOBJ - GL_ARLX_HBASE
      RMIN = GL_ARLX_RMIN(IOB)
      RMAX = GL_ARLX_RMAX(IOB)
      RLX2 = GL_ARLX_RLX2(IOB)
      RLX3 = GL_ARLX_RLX3(IOB)
      RES2 = GL_ARLX_RES2(IOB)
      RES3 = GL_ARLX_RES3(IOB)
      LRES = GL_ARLX_LRES(IOB)
      LTRV = GL_ARLX_LTRV(IOB)

C---     Récupère les données
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
D     CALL ERR_ASR(NDLL .EQ. NDLN*NNL)

C---     (Re-)Alloue la mémoire
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NEQL, LRES)
         GL_ARLX_LRES(IOB) = LRES
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NDLL, LTRV)
         GL_ARLX_LTRV(IOB) = LTRV
      ENDIF

C---     || U_0 ||
      IF (ERR_GOOD()) THEN
         IF (RES2 .LT. 0.0D0) THEN
            IERR = GL_GLBL_CLCRES(RES2,
     &                            ZERO,
     &                            NDLL,
     &                            NEQL,
     &                            VDLG,
     &                            VDEL,      ! Not used
     &                            VDUM,      ! Dummy, not used
     &                            VA(SO_ALLC_REQVIND(VA, LRES)),
     &                            HFRES)
         ENDIF
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Residual (u): ', RES2
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     || U_0 + dU ||
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_CLCRES(RES1,
     &                         UN,  ! RMAX??
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Residual (u+du): ', RES1
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Loi d'amortissement
      IF (ERR_GOOD()) THEN
         IF (ABS(RES2-RES1) .GT. PETIT) THEN
            RRED = ((RES2-RES1) / (RES2+PETIT)) / RLX2
            IF (RRED .GT. 0.5D0) THEN
               RLX1 = RRED
            ELSE
               RLX1 = 0.075D0 / (0.75D0 - RRED) + RMIN ! [0.1, 0.3] + RMIN
            ENDIF
         ELSE
            RLX1 = 1.0D0
         ENDIF
         RLX1 = MIN(RMAX, MAX(RMIN, RLX1))
      ENDIF

C---     Détection d'oscillations
      IF (ERR_GOOD() .AND. RES3 .GT. 0.0D0) THEN
         RREL = ABS((RES3-RES1) / RES3)
         DREL = ABS((RLX3-RLX1) / RLX3)
         IF ((RREL .LT. 0.1D0) .AND. (DREL .LT. 0.01D0)) THEN
!!!            RLX1 = RAND() .... 1.0D0
            RLX1 = MIN(0.5D0, RMAX)
         ENDIF
      ENDIF

C---     Incrémente la solution
      IF (ERR_GOOD() .AND. RLX1 .NE. 1.0D0) THEN
         CALL MPI_BCAST(RLX1, 1, MP_TYPE_RE8(),
     &                  I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
         CALL DSCAL(NDLL, RLX1, VDEL, 1)
      ENDIF

C---     L'état
      ISTTS = GL_STTS_OK
      IF (RLX1 .GE. RMAX) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      IF (RLX1 .LE. RMIN) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     Conserve le résultat
      IF (ERR_GOOD()) THEN
         GL_ARLX_RLX3(IOB) = RLX2
         GL_ARLX_RLX2(IOB) = RLX1
         GL_ARLX_RES3(IOB) = RES2
         GL_ARLX_RES2(IOB) = RES1
      ENDIF

C---     LOG
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3)') 'Adaptative Relaxation#<35>#= ',
     &                                 RLX1
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

      GL_ARLX_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C     La méthode GL_ARLX_REQTHETA retourne la valeur du facteur
C     d'amortissement.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     THETA       Facteur
C
C Sortie:
C
C Notes:
C     Le facteur est passé en argument pour être compatible avec
C     les appels via call-back du proxy.
C************************************************************************
      FUNCTION GL_ARLX_REQTHETA(HOBJ, THETA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_ARLX_REQTHETA
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   THETA

      INCLUDE 'glarlx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glarlx.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_ARLX_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      THETA = GL_ARLX_RLX2(HOBJ-GL_ARLX_HBASE)

      GL_ARLX_REQTHETA = ERR_TYP()
      RETURN
      END
