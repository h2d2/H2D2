C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLGLBL_REQMDL(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLGLBL_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'glglbl_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_GLGLBL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         HOBJ = IC_GLGLBL_REQHDL()
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  <b>glob</b> returns the handle of the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLGLBL_AID()

9999  CONTINUE
      IC_GLGLBL_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_GLGLBL_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLGLBL_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'glglbl_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glcomp_ic.fi'
      INCLUDE 'gllmtr_ic.fi'
      INCLUDE 'glbtrk_ic.fi'
      INCLUDE 'glbtk2_ic.fi'
      INCLUDE 'glbtk3_ic.fi'
      INCLUDE 'glcrlx_ic.fi'
      INCLUDE 'glarlx_ic.fi'
      INCLUDE 'glbkrs_ic.fi'
      INCLUDE 'gldmpr_ic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glglbl_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_GLGLBL_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <include>IC_GLCOMP_REQCLS@glcomp_ic.for</include>
      IF (IMTH .EQ. 'glob_compose') THEN
C        <include>IC_GLCOMP_XEQCTR@glcomp_ic.for</include>
D        CALL ERR_ASR(IC_GLCOMP_REQCLS() .EQ. 'glob_compose')
         IERR = IC_GLCOMP_XEQCTR(IPRM)

C     <include>IC_GLLMTR_REQCLS@gllmtr_ic.for</include>
      ELSEIF (IMTH .EQ. 'glob_limiter') THEN
C        <include>IC_GLLMTR_XEQCTR@gllmtr_ic.for</include>
D        CALL ERR_ASR(IC_GLLMTR_REQCLS() .EQ. 'glob_limiter')
         IERR = IC_GLLMTR_XEQCTR(IPRM)

C     <include>IC_GLBTRK_REQCLS@glbtrk_ic.for</include>
      ELSEIF (IMTH .EQ. 'backtracking') THEN
C        <include>IC_GLBTRK_XEQCTR@glbtrk_ic.for</include>
D        CALL ERR_ASR(IC_GLBTRK_REQCLS() .EQ. 'backtracking')
         IERR = IC_GLBTRK_XEQCTR(IPRM)

C     <include>IC_GLBTK2_REQCLS@glbtk2_ic.for</include>
      ELSEIF (IMTH .EQ. 'backtracking_2') THEN
C        <include>IC_GLBTK2_XEQCTR@glbtk2_ic.for</include>
D        CALL ERR_ASR(IC_GLBTK2_REQCLS() .EQ. 'backtracking_2')
         IERR = IC_GLBTK2_XEQCTR(IPRM)

C     <include>IC_GLBTK3_REQCLS@glbtk3_ic.for</include>
      ELSEIF (IMTH .EQ. 'backtracking_3') THEN
C        <include>IC_GLBTK3_XEQCTR@glbtk3_ic.for</include>
D        CALL ERR_ASR(IC_GLBTK3_REQCLS() .EQ. 'backtracking_3')
         IERR = IC_GLBTK3_XEQCTR(IPRM)

C     <include>IC_GLCRLX_REQCLS@glcrlx_ic.for</include>
      ELSEIF (IMTH .EQ. 'constant_relaxation') THEN
C        <include>IC_GLCRLX_XEQCTR@glcrlx_ic.for</include>
D        CALL ERR_ASR(IC_GLCRLX_REQCLS() .EQ. 'constant_relaxation')
         IERR = IC_GLCRLX_XEQCTR(IPRM)

C     <include>IC_GLARLX_REQCLS@glarlx_ic.for</include>
      ELSEIF (IMTH .EQ. 'adaptative_relaxation') THEN
C        <include>IC_GLARLX_XEQCTR@glarlx_ic.for</include>
D        CALL ERR_ASR(IC_GLARLX_REQCLS() .EQ. 'adaptative_relaxation')
         IERR = IC_GLARLX_XEQCTR(IPRM)

C     <include>IC_GLBKRS_REQCLS@glbkrs_ic.for</include>
      ELSEIF (IMTH .EQ. 'gl_bank_rose') THEN
C        <include>IC_GLBKRS_XEQCTR@glbkrs_ic.for</include>
D        CALL ERR_ASR(IC_GLBKRS_REQCLS() .EQ. 'gl_bank_rose')
         IERR = IC_GLBKRS_XEQCTR(IPRM)

C     <include>IC_GLDMPR_REQCLS@gldmpr_ic.for</include>
      ELSEIF (IMTH .EQ. 'damper') THEN
C        <include>IC_GLDMPR_XEQCTR@gldmpr_ic.for</include>
D        CALL ERR_ASR(IC_GLDMPR_REQCLS() .EQ. 'damper')
         IERR = IC_GLDMPR_XEQCTR(IPRM)

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_GLGLBL_AID()

9999  CONTINUE
      IC_GLGLBL_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLGLBL_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLGLBL_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glglbl_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>glob</b> regroups the globalization algorithms.
C</comment>
      IC_GLGLBL_REQNOM = 'glob'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_GLGLBL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_GLGLBL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glglbl_ic.fi'
C-------------------------------------------------------------------------

      IC_GLGLBL_REQHDL = 1999033000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_GLGLBL_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('glglbl_ic.hlp')

      RETURN
      END
