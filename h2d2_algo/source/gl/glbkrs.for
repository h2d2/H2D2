C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  GLobalisation
C Objet:   BanK and RoSe
C Type:    Concret
C
C Note:
C     "Global Approximation Newton Methods"
C     R.E. Bank, D.J. Rose
C     Numer. Math 37, 279-295 (1981)
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>GL_BKRS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbkrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(GL_BKRS_NOBJMAX,
     &                   GL_BKRS_HBASE,
     &                   'Backtracking Banck&Rose')

      GL_BKRS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbkrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER  IERR
      EXTERNAL GL_BKRS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(GL_BKRS_NOBJMAX,
     &                   GL_BKRS_HBASE,
     &                   GL_BKRS_DTR)

      GL_BKRS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BKRS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   GL_BKRS_NOBJMAX,
     &                   GL_BKRS_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(GL_BKRS_HVALIDE(HOBJ))
         IOB = HOBJ - GL_BKRS_HBASE

         GL_BKRS_LRES(IOB) = 0
         GL_BKRS_LTRV(IOB) = 0
         GL_BKRS_DLTA(IOB) = 1.0D0
         GL_BKRS_VK  (IOB) = 0.0D0
      ENDIF

      GL_BKRS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BKRS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = GL_BKRS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   GL_BKRS_NOBJMAX,
     &                   GL_BKRS_HBASE)

      GL_BKRS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BKRS_INI(HOBJ, DLTA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DLTA

      INCLUDE 'glbkrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (DLTA .LE. 0.0D0) GOTO 9900
      IF (DLTA .GT. 1.0D0) GOTO 9900

C---     RESET LES DONNEES
      IERR = GL_BKRS_RST(HOBJ)

C---     ENREGISTRE LES DONNEES
      IOB = HOBJ - GL_BKRS_HBASE
      GL_BKRS_DLTA(IOB) = DLTA
      GL_BKRS_VK  (IOB) = 0.0D0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_VALEUR_INVALIDE',': ',
     &                              DLTA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(3A)') 'MSG_DOMAINE_VALIDE',': ',
     &                        'DELTA DANS [0, 1]'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_BKRS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION GL_BKRS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LRES, LTRV
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - GL_BKRS_HBASE
      LRES = GL_BKRS_LRES(IOB)
      LTRV = GL_BKRS_LTRV(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LTRV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTRV)
      IF (LRES .NE. 0) IERR = SO_ALLC_ALLRE8(0, LRES)

C---     RESET
      GL_BKRS_LRES(IOB) = 0
      GL_BKRS_LTRV(IOB) = 0
      GL_BKRS_DLTA(IOB) = 1.0D0
      GL_BKRS_VK  (IOB) = 0.0D0

      GL_BKRS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction GL_BKRS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'glbkrs.fi'
      INCLUDE 'glbkrs.fc'
C------------------------------------------------------------------------

      GL_BKRS_REQHBASE = GL_BKRS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction GL_BKRS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'glbkrs.fc'
C------------------------------------------------------------------------

      GL_BKRS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  GL_BKRS_NOBJMAX,
     &                                  GL_BKRS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Imprime l'objet dans le log.
C
C Description:
C     La fonction GL_BKRS_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IOB, IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - GL_BKRS_HBASE

C-------  EN-TETE DE COMMANDE
      LOG_ZNE = 'h2d2.glob.btrk2'
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_GLBL_BACKTRACKING_BANCK&ROSE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     IMPRESSION DES COMPOSANTES
      WRITE (LOG_BUF,'(A,1PE14.6E3)') 'MSG_DELTA#<35>#= ',
     &                                GL_BKRS_DLTA(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      GL_BKRS_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Marque le début d'une séquence d'itérations.
C
C Description:
C     La fonction GL_BKRS_DEB marque le début d'une séquence d'itérations.
C     Elle fait les traitements nécessaire et doit donc être appelée avant
C     GL_BKRS_XEQ.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION GL_BKRS_DEB(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'glbkrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - GL_BKRS_HBASE
      GL_BKRS_VK(IOB) = 0.0D0

      GL_BKRS_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Execute l'algo de globalisation.
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     NDLL        Nombre de Degrés de Liberté Locaux
C     NEQ         Nombre d'EQuations
C     VDLG        Degrés de liberté
C     VDELU       Incrément de solution
C     ETA         Adapting Forcing Term
C     HRES        Handle sur le résidu
C     F_RES       Fonction call-back de calcul de résidu
C
C Sortie:
C     VDEL        Incrément de solution modifié
C
C Notes:
C     2) Le broadcast de THETA, c'est ceinture et bretelles
C************************************************************************
      FUNCTION GL_BKRS_XEQ(HOBJ,
     &                     HSIM,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL,
     &                     ETA,
     &                     HFRES,
     &                     HFSLV,
     &                     ISTTS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: GL_BKRS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  ETA
      INTEGER HFRES
      INTEGER HFSLV
      INTEGER ISTTS

      INCLUDE 'glbkrs.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'glbkrs.fc'

      INTEGER I_MASTER
      INTEGER I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER HDDL
      INTEGER NEQL, NDLL
      INTEGER ITER, NITER_MAX
      INTEGER LRES, LTRV
      LOGICAL DONE

      REAL*8  P0, P1
      REAL*8  DLTA
      REAL*8  THETA, R, VK

      REAL*8  UN, ZERO
      PARAMETER (UN        = 1.0D+00)
      PARAMETER (ZERO      = 0.0D+00)
      PARAMETER (NITER_MAX = 10)
C------------------------------------------------------------------------
D     CALL ERR_PRE(GL_BKRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.glob.bkrs'

C---     Récupère les attributs
      IOB  = HOBJ - GL_BKRS_HBASE
      LRES = GL_BKRS_LRES(IOB)
      LTRV = GL_BKRS_LTRV(IOB)
      DLTA = GL_BKRS_DLTA(IOB)
      VK   = GL_BKRS_VK  (IOB)

C---     Récupère les données
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
D     CALL ERR_ASR(NDLL .EQ. NDLN*NNL)

C---     Initialise
      ITER  = 0

C---     (Re-)Alloue la mémoire
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NEQL, LRES)
         GL_BKRS_LRES(IOB) = LRES
      ENDIF
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLRE8(NDLL, LTRV)
         GL_BKRS_LTRV(IOB) = LTRV
      ENDIF

C---     || U_0 ||
      IF (ERR_GOOD()) THEN
         IERR = GL_GLBL_CLCRES(P0,
     &                         ZERO,
     &                         NDLL,
     &                         NEQL,
     &                         VDLG,
     &                         VDEL,
     &                         VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                         VA(SO_ALLC_REQVIND(VA, LRES)),
     &                         HFRES)
      ENDIF
      WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Backtracking P0: ', P0
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Boucle WHILE
100   CONTINUE
         ITER = ITER + 1

         THETA = UN / (UN + VK*P0)
         IF (ITER .EQ. NITER_MAX) GOTO 199

C---        || U_0 + dU ||
         IF (ERR_GOOD()) THEN
            IERR = GL_GLBL_CLCRES(P1,
     &                            THETA,
     &                            NDLL,
     &                            NEQL,
     &                            VDLG,
     &                            VDEL,
     &                            VA(SO_ALLC_REQVIND(VA, LTRV)),
     &                            VA(SO_ALLC_REQVIND(VA, LRES)),
     &                            HFRES)
         ENDIF
         WRITE(LOG_BUF,'(A,1PE14.6E3)') '#<2>#Backtracking P1: ', P1
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

         R  = (UN - P1/P0) / THETA
         IF (R .LT. DLTA) THEN
            IF (VK .LE. ZERO) THEN
               VK = UN
            ELSE
               VK = 10.0D0*VK
            ENDIF
            DONE = .FALSE.
         ELSE
            VK = VK / 10.0D0
            DONE = .TRUE.
         ENDIF

C---        Log
         WRITE(LOG_BUF,'(A,I3,3(1X,1PE14.6E3))')
     &                  '#<2>#Backtracking Iter: ',ITER, THETA, P1
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

         IF (DONE) GOTO 200
      GOTO 100    ! END WHILE

C---     NON CONVERGENCE
199   CONTINUE
      GOTO 9900
!      THETA = TMIN
!      ITER  = -1

C---     Incrémente la solution
200   CONTINUE
      IF (ERR_GOOD() .AND. THETA .NE. 1.0D0) THEN
         CALL MPI_BCAST(THETA, 1, MP_TYPE_RE8(),
     &                  I_MASTER, MP_UTIL_REQCOMM(), I_ERROR)
         CALL DSCAL(NDLL, THETA, VDEL, 1)
      ENDIF

C---     Conserve la valeur de K
      GL_BKRS_VK  (IOB) = VK

C---     L'état
      ISTTS = GL_STTS_OK
      IF (ITER .NE. 0) ISTTS = IBSET(ISTTS, GL_STTS_MODIF)
      !! IF (ITER .LT. 0) ISTTS = IBSET(ISTTS, GL_STTS_BMIN)

C---     LOG
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A,1PE14.6E3,A, I3)')
     &         'Backtracking with ALFA#<35>#= ',THETA, ' iter = ', ITER
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,2(1PE14.6E3))') 'ERR_VALEUR_INVALIDE',': ',
     &                              DLTA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      GL_BKRS_XEQ = ERR_TYP()
      RETURN
      END
