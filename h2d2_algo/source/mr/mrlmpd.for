C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_LMPD_000
C     INTEGER MR_LMPD_999
C     INTEGER MR_LMPD_CTR
C     INTEGER MR_LMPD_DTR
C     INTEGER MR_LMPD_INI
C     INTEGER MR_LMPD_RST
C     INTEGER MR_LMPD_REQHBASE
C     LOGICAL MR_LMPD_HVALIDE
C     LOGICAL MR_LMPD_ESTDIRECTE
C     INTEGER MR_LMPD_ASMMTX
C     INTEGER MR_LMPD_FCTMTX
C     INTEGER MR_LMPD_RESMTX
C     INTEGER MR_LMPD_XEQ
C   Private:
C     INTEGER MR_LMPD_FCTMTX_AUX
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MR_LMPD_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_LMPD_NOBJMAX,
     &                   MR_LMPD_HBASE,
     &                   'Matrix Solver Lumped')

      MR_LMPD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER  IERR
      EXTERNAL MR_LMPD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_LMPD_NOBJMAX,
     &                   MR_LMPD_HBASE,
     &                   MR_LMPD_DTR)

      MR_LMPD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LMPD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_LMPD_NOBJMAX,
     &                   MR_LMPD_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_LMPD_HVALIDE(HOBJ))
         IOB = HOBJ - MR_LMPD_HBASE

         MR_LMPD_HMTX (IOB) = 0
         MR_LMPD_LRHS (IOB) = 0
         MR_LMPD_LTV1 (IOB) = 0
         MR_LMPD_LTV2 (IOB) = 0
         MR_LMPD_NITER(IOB) = 0
         MR_LMPD_EPS0 (IOB) = 0.0D0
         MR_LMPD_EPS1 (IOB) = 0.0D0
      ENDIF

      MR_LMPD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LMPD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_LMPD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_LMPD_NOBJMAX,
     &                   MR_LMPD_HBASE)

      MR_LMPD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LMPD_INI(HOBJ, IOPR, NITER, EPS0, EPS1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPR
      INTEGER NITER
      REAL*8  EPS0
      REAL*8  EPS1

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_LMPD_RST(HOBJ)

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_DIAG_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_DIAG_INI(HMTX, IOPR)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_LMPD_HBASE
         MR_LMPD_HMTX (IOB) = HMTX
         MR_LMPD_LRHS (IOB) = 0
         MR_LMPD_LTV1 (IOB) = 0
         MR_LMPD_LTV2 (IOB) = 0
         MR_LMPD_NITER(IOB) = NITER
         MR_LMPD_EPS0 (IOB) = EPS0
         MR_LMPD_EPS1 (IOB) = EPS1
      ENDIF

      MR_LMPD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LMPD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
      INTEGER LRHS, LTV1, LTV2
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_LMPD_HBASE

      LTV2 = MR_LMPD_LTV2(IOB)
      IF (SO_ALLC_HEXIST(LTV2)) IERR = SO_ALLC_ALLRE8(0, LTV2)
      LTV1 = MR_LMPD_LTV1(IOB)
      IF (SO_ALLC_HEXIST(LTV1)) IERR = SO_ALLC_ALLRE8(0, LTV1)
      LRHS = MR_LMPD_LRHS(IOB)
      IF (SO_ALLC_HEXIST(LRHS)) IERR = SO_ALLC_ALLRE8(0, LRHS)
      HMTX = MR_LMPD_HMTX(IOB)
      IF (MX_DIAG_HVALIDE(HMTX)) IERR = MX_DIAG_DTR(HMTX)

      MR_LMPD_HMTX (IOB) = 0
      MR_LMPD_LRHS (IOB) = 0
      MR_LMPD_LTV1 (IOB) = 0
      MR_LMPD_LTV2 (IOB) = 0
      MR_LMPD_NITER(IOB) = 0
      MR_LMPD_EPS0 (IOB) = 0.0D0
      MR_LMPD_EPS1 (IOB) = 0.0D0

      MR_LMPD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_LMPD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'mrlmpd.fc'
C------------------------------------------------------------------------

      MR_LMPD_REQHBASE = MR_LMPD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_LMPD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrlmpd.fc'
C------------------------------------------------------------------------

      MR_LMPD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_LMPD_NOBJMAX,
     &                                  MR_LMPD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_LMPD_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrlmpd.fi'
C------------------------------------------------------------------------

      MR_LMPD_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_LMPD_ASMMTX assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX

      EXTERNAL MX_DIAG_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LMPD_HBASE
      HMTX = MR_LMPD_HMTX (IOB)
D     CALL ERR_ASR(MX_DIAG_HVALIDE(HMTX))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_DIAG_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_DIAG_ASMKE)

C---     SIGNALE LA FIN DE L'ASSEMBLAGE
      IF (ERR_GOOD()) IERR = MX_DIAG_ASMFIN(HMTX)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_LMPD_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_LMPD_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Demarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LMPD_HBASE
      HMTX = MR_LMPD_HMTX (IOB)

C---     RECUPERE LES PARAM
      NEQL = MX_DIAG_REQNEQL(HMTX)
      LKG  = MX_DIAG_REQLKG (HMTX)

C---     FACTORISE
      IERR = MR_LMPD_FCTMTX_AUX(NEQL, VA(SO_ALLC_REQVIND(VA,LKG)))

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_LMPD_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel
C
C Description:
C     La fonction MR_LMPD_RESMTX résout le système matriciel avec
C     le second membre qu'elle assemble. La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
      INTEGER IT
      INTEGER IOB
      INTEGER HMTX, HMRS, HDDL
      INTEGER LRHS, LTV1, LTV2
      INTEGER NEQL
      INTEGER NITER
      REAL*8  EPS0, EPS1, EPS
      REAL*8  V0, V1
      REAL*8  VDUM

      REAL*8  DNRM2              ! Fonction BLAS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     Récupère les attributs
      IOB = HOBJ - MR_LMPD_HBASE
      HMTX  = MR_LMPD_HMTX (IOB)
      LRHS  = MR_LMPD_LRHS (IOB)
      LTV1  = MR_LMPD_LTV1 (IOB)
      LTV2  = MR_LMPD_LTV2 (IOB)
      NITER = MR_LMPD_NITER(IOB)
      EPS0  = MR_LMPD_EPS0 (IOB)
      EPS1  = MR_LMPD_EPS1 (IOB)
D     CALL ERR_ASR(MX_DIAG_HVALIDE(HMTX))

C---     Récupère la matrice morse de base
      HMRS = MX_DIAG_REQHMORS(HMTX)

C---     Récupère les données de simulation
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
D     CALL ERR_ASR(NEQL .EQ. MX_DIAG_REQNEQL(HMTX))

C---     Alloue la table de travail au besoin
      IF (LRHS .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LRHS)
         IF (ERR_GOOD()) MR_LMPD_LRHS(IOB) = LRHS
      ENDIF
      IF (LTV1 .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LTV1)
         IF (ERR_GOOD()) MR_LMPD_LTV1(IOB) = LTV1
      ENDIF
      IF (LTV2 .EQ. 0) THEN
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LTV2)
         IF (ERR_GOOD()) MR_LMPD_LTV2(IOB) = LTV2
      ENDIF

C---     Assemble le membre de droite dans RHS
      IF (ERR_GOOD() .AND. HALG .NE. 0)
     &   IERR = F_F(HALG, VA(SO_ALLC_REQVIND(VA, LRHS)), VDUM, .FALSE.)

C---     Boucle sur les itérations
      CALL DINIT(NEQL, 0.0D0, VSOL, 1)
      V0 = 1.0D99
      DO IT=1,NITER

C---        Résous pour du(i) dans LTV1
         IF (ERR_GOOD())
     &      IERR = MX_DIAG_MULVEC(HMTX,
     &                            NEQL,
     &                            VA(SO_ALLC_REQVIND(VA, LRHS)),
     &                            VA(SO_ALLC_REQVIND(VA, LTV1)))

C---        Somme les du dans VSOL
         IF (ERR_GOOD())
     &      CALL DAXPY(NEQL,
     &                 1.0D0,
     &                 VA(SO_ALLC_REQVIND(VA, LTV1)), 1,
     &                 VSOL, 1)

C---        Monitoring
         IF (ERR_GOOD() .AND. IT .NE. NITER) THEN
            V1 = DNRM2(NEQL, VA(SO_ALLC_REQVIND(VA, LTV1)), 1)
            IF (IT .EQ. 1) EPS = MAX(EPS0, EPS1*V1)
            IF (V1 .LT. EPS) GOTO 199
C            IF (V1 .GT. V0)  GOTO 9900 ! C.f. Note
            V0 = V1
         ENDIF

C---        M.du(i)
         IF (ERR_GOOD() .AND. IT .NE. NITER)
     &      IERR = MX_MORS_MULVEC(HMRS,
     &                            NEQL,
     &                            VA(SO_ALLC_REQVIND(VA, LTV1)),
     &                            VA(SO_ALLC_REQVIND(VA, LTV2)))

C---        RHS - M.du(i)
         IF (ERR_GOOD() .AND. IT .NE. NITER)
     &      CALL DAXPY(NEQL,
     &                 -1.0D0,
     &                 VA(SO_ALLC_REQVIND(VA, LTV2)), 1,
     &                 VA(SO_ALLC_REQVIND(VA, LRHS)), 1)

      ENDDO
199   CONTINUE

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DIVERGENCE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_LMPD_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_LMPD_XEQ resoud complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LMPD_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LMPD_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrlmpd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LMPD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ASSEMBLE LA MATRICE
      IF(ERR_GOOD())IERR=MR_LMPD_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     FACTORISE LA MATRICE
      IF(ERR_GOOD())IERR=MR_LMPD_FCTMTX(HOBJ)

C---     RESOUS
      IF(ERR_GOOD())IERR=MR_LMPD_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_LMPD_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_LMPD_FCTMTX_AUX factorise le système matriciel diagonal.
C     C'est une fonction auxiliaire qui permet de résoudre l'adresse de
C     la table VDIAG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LMPD_FCTMTX_AUX(NEQ, VDIAG)

      IMPLICIT NONE

      INTEGER NEQ
      REAL*8  VDIAG(NEQ)

      INCLUDE 'err.fi'
      INCLUDE 'mrlmpd.fc'

      INTEGER I
      REAL*8  UN
      PARAMETER (UN = 1.0D0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(NEQ .GT. 0)
C------------------------------------------------------------------------

      DO I=1,NEQ
         VDIAG(I) = UN / VDIAG(I)
      ENDDO

      MR_LMPD_FCTMTX_AUX = ERR_TYP()
      RETURN
      END

