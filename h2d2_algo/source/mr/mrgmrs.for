C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_GMRS_000
C     INTEGER MR_GMRS_999
C     INTEGER MR_GMRS_CTR
C     INTEGER MR_GMRS_DTR
C     INTEGER MR_GMRS_INI
C     INTEGER MR_GMRS_RST
C     LOGICAL MR_GMRS_HVALIDE
C     LOGICAL MR_GMRS_ESTDIRECTE
C     INTEGER MR_GMRS_XEQ
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MR_GMRS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_GMRS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_GMRS_NOBJMAX,
     &                   MR_GMRS_HBASE,
     &                   'MR_GMRS')

      DO I=1, MR_GMRS_NOBJMAX
         MR_GMRS_HPRC(I) = 0
      ENDDO

      MR_GMRS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_GMRS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER  IERR
      EXTERNAL MR_GMRS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_GMRS_NOBJMAX,
     &                   MR_GMRS_HBASE,
     &                   MR_GMRS_DTR)

      MR_GMRS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_GMRS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_GMRS_NOBJMAX,
     &                   MR_GMRS_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_GMRS_HVALIDE(HOBJ))
         IOB = HOBJ - MR_GMRS_HBASE
      ENDIF

      MR_GMRS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_GMRS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_GMRS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_GMRS_NOBJMAX,
     &                   MR_GMRS_HBASE)

      MR_GMRS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_GMRS_INI(HOBJ, HPRC, NITER, NRDEM, NPREC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRC
      INTEGER NITER
      INTEGER NRDEM
      INTEGER NPREC

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_GMRS_HVALIDE(HOBJ))
D     CALL ERR_PRE(HPRC .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_GMRS_RST(HOBJ)

C---     CONTROLES
      IF (ERR_GOOD()) THEN
         IF (MP_UTIL_ESTMPROC()) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_METHODE_RESO_INVALIDE_EN_MPROC')
         ENDIF
      ENDIF

      IOB  = HOBJ - MR_GMRS_HBASE
      MR_GMRS_HPRC(IOB) = HPRC

C      GOTO 9999
CC------------------------------------------------------------------------
C9900  WRITE(ERR_BUF, '(A)') 'ERR_INVALID_MATRIX_SOLVER'
C      CALL ERR_ASG(ERR_FTL, ERR_BUF)
C      GOTO 9999

C9999  CONTINUE
      MR_GMRS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_GMRS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_GMRS_HBASE
      MR_GMRS_HPRC(IOB) = 0

      MR_GMRS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_GMRS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_GMRS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrgmrs.fc'
C------------------------------------------------------------------------

      MR_GMRS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_GMRS_NOBJMAX,
     &                                  MR_GMRS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_GMRS_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_GMRS_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrgmrs.fi'
C------------------------------------------------------------------------

      MR_GMRS_ESTDIRECTE = .FALSE.
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_GMRS_XEQ resoud complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du systeme matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_GMRS_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_GMRS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrgmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrgmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRC
      REAL*8 V, R
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_GMRS_HBASE
      HPRC = MR_GMRS_HPRC(IOB)

      IERR = F_KU(HALG, V, R)

      MR_GMRS_XEQ = ERR_TYP()
      RETURN
      END
