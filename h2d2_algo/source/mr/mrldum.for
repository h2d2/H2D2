C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MR_LDUM_000
C     INTEGER MR_LDUM_999
C     INTEGER MR_LDUM_CTR
C     INTEGER MR_LDUM_DTR
C     INTEGER MR_LDUM_INI
C     INTEGER MR_LDUM_RST
C     INTEGER MR_LDUM_REQHBASE
C     LOGICAL MR_LDUM_HVALIDE
C     LOGICAL MR_LDUM_ESTDIRECTE
C     INTEGER MR_LDUM_ASMMTX
C     INTEGER MR_LDUM_FCTMTX
C     INTEGER MR_LDUM_RESMTX
C     INTEGER MR_LDUM_XEQ
C   Private:
C     INTEGER MR_LDUM_ASMRHS
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MR_LDUM_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldum.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_LDUM_NOBJMAX,
     &                   MR_LDUM_HBASE,
     &                   'Matrix Solver LDU-Memory')

      MR_LDUM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldum.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldum.fc'

      INTEGER  IERR
      EXTERNAL MR_LDUM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_LDUM_NOBJMAX,
     &                   MR_LDUM_HBASE,
     &                   MR_LDUM_DTR)

      MR_LDUM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_LDUM_NOBJMAX,
     &                   MR_LDUM_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_LDUM_HVALIDE(HOBJ))
         IOB = HOBJ - MR_LDUM_HBASE

         MR_LDUM_HMTX(IOB) = 0
      ENDIF

      MR_LDUM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldum.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_LDUM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_LDUM_NOBJMAX,
     &                   MR_LDUM_HBASE)

      MR_LDUM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUM_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_LDUM_RST(HOBJ)

C---     CONTROLES
      IF (ERR_GOOD()) THEN
         IF (MP_UTIL_ESTMPROC()) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_METHODE_RESO_INVALIDE_EN_MPROC')
         ENDIF
      ENDIF

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_SKYL_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_SKYL_INI(HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_LDUM_HBASE
         MR_LDUM_HMTX(IOB) = HMTX
      ENDIF

      MR_LDUM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_LDUM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MR_LDUM_HBASE

      HMTX = MR_LDUM_HMTX(IOB)
      IF (MX_SKYL_HVALIDE(HMTX)) IERR = MX_SKYL_DTR(HMTX)

      MR_LDUM_HMTX(IOB) = 0

      MR_LDUM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_LDUM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrldum.fi'
      INCLUDE 'mrldum.fc'
C------------------------------------------------------------------------

      MR_LDUM_REQHBASE = MR_LDUM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_LDUM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrldum.fc'
C------------------------------------------------------------------------

      MR_LDUM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_LDUM_NOBJMAX,
     &                                  MR_LDUM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_LDUM_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrldum.fi'
C------------------------------------------------------------------------

      MR_LDUM_ESTDIRECTE = .TRUE.
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice
C
C Description:
C     La fonction MR_LDUM_ASMMTX assemble la matrice.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     HSIM     HANDLE DE LA SIMULATION
C     HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C     F_K      FONCTION A APPELER POUR ASSEMBLER LA MATRICE
C     F_KU     FONCTION A APPELER POUR ASSEMBLER LA PRODUIT K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_ASMMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_K,
     &                        F_KU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      EXTERNAL F_K
      EXTERNAL F_KU

      INCLUDE 'mrldum.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX

      EXTERNAL MX_SKYL_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      
C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.assemblage')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUM_HBASE
      HMTX  = MR_LDUM_HMTX(IOB)
D     CALL ERR_ASR(MX_SKYL_HVALIDE(HMTX))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_SKYL_DIMMAT(HMTX, HSIM)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_SKYL_ASMKE)

      CALL TR_CHRN_STOP('h2d2.reso.mat.assemblage')
      MR_LDUM_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le système matriciel.
C
C Description:
C     La fonction MR_LDUM_ASMRHS assemble le système matriciel.
C     La matrice est dimensionnée et assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_ASMRHS(HOBJ, HSIM, HALG, F_F, VFG)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VFG(*)
      EXTERNAL F_F

      INCLUDE 'mrldum.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
      REAL*8  VDUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUM_HBASE
      HMTX  = MR_LDUM_HMTX(IOB)
D     CALL ERR_ASR(MX_SKYL_HVALIDE(HMTX))

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD()) IERR = F_F(HALG, VFG, VDUM, .FALSE.)

      MR_LDUM_ASMRHS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Factorise le système matriciel
C
C Description:
C     La fonction MR_LDUM_FCTMTX factorise le système matriciel. La matrice
C     doit être assemblée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_FCTMTX(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_FCTMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'mrldum.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LIAP, LJAP
      INTEGER LKGS, LKGD, LKGI
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.factorisation')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUM_HBASE
      HMTX  = MR_LDUM_HMTX(IOB)
D     CALL ERR_ASR(MX_SKYL_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL = MX_SKYL_REQNEQL(HMTX)
         LIAP = MX_SKYL_REQLIAP(HMTX)
         LJAP = MX_SKYL_REQLJAP(HMTX)
         LKGS = MX_SKYL_REQLKGS(HMTX)
         LKGD = MX_SKYL_REQLKGD(HMTX)
         LKGI = MX_SKYL_REQLKGI(HMTX)
      ENDIF

C---     FACTORISE
      IF (ERR_GOOD()) THEN
         IERR = SP_SKYL_FACT(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGS)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGD)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGI)))
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.factorisation')
      MR_LDUM_FCTMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel
C
C Description:
C     La fonction MR_LDUM_RESMTX résout le système matriciel avec
C     un second membre. Le second membre est assemblé si les handles
C     sur la simulation HSIM et sur l'algorithme HALG sont tous deux non nuls,
C     sinon on résout directement avec VSOL.
C     La matrice doit être factorisée.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_RESMTX(HOBJ,
     &                        HSIM,
     &                        HALG,
     &                        F_F,
     &                        VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_RESMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_F

      INCLUDE 'mrldum.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'spskyl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LIAP
      INTEGER LJAP
      INTEGER LKGS, LKGD, LKGI
      INTEGER NEQL
      INTEGER NKGP
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso.mat.resolution')

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_LDUM_HBASE
      HMTX  = MR_LDUM_HMTX(IOB)
D     CALL ERR_ASR(MX_SKYL_HVALIDE(HMTX))

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL  = MX_SKYL_REQNEQL(HMTX)
         NKGP  = MX_SKYL_REQNKGP(HMTX)
         LIAP  = MX_SKYL_REQLIAP(HMTX)
         LJAP  = MX_SKYL_REQLJAP(HMTX)
         LKGS  = MX_SKYL_REQLKGS(HMTX)
         LKGD  = MX_SKYL_REQLKGD(HMTX)
         LKGI  = MX_SKYL_REQLKGI(HMTX)
      ENDIF

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD() .AND. HSIM .NE. 0 .AND. HALG .NE. 0)
     &   IERR = MR_LDUM_ASMRHS(HOBJ, HSIM, HALG, F_F, VSOL)

C---     RESOUS
      IF (ERR_GOOD()) THEN
         IERR = SP_SKYL_SOLV(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGS)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGD)),
     &                       VA(SO_ALLC_REQVIND(VA,LKGI)),
     &                       VSOL)
      ENDIF

      CALL TR_CHRN_STOP('h2d2.reso.mat.resolution')
      MR_LDUM_RESMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_LDUM_XEQ resoud complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur l'élément
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_LDUM_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_LDUM_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrldum.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrldum.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_LDUM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      
C---     Assemble la matrice
      IF(ERR_GOOD()) IERR = MR_LDUM_ASMMTX(HOBJ,HSIM,HALG,F_K,F_KU)

C---     Factorise la matrice
      IF(ERR_GOOD()) IERR = MR_LDUM_FCTMTX(HOBJ)

C---     Résous
      IF(ERR_GOOD()) IERR = MR_LDUM_RESMTX(HOBJ,HSIM,HALG,F_F,VSOL)

      MR_LDUM_XEQ = ERR_TYP()
      RETURN
      END

