C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_DNSE_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdnse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_DNSE_NOBJMAX,
     &                   MX_DNSE_HBASE,
     &                   'Dense matrix')

      MX_DNSE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdnse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER  IERR
      EXTERNAL MX_DNSE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_DNSE_NOBJMAX,
     &                   MX_DNSE_HBASE,
     &                   MX_DNSE_DTR)

      MX_DNSE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_DNSE_NOBJMAX,
     &                   MX_DNSE_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_DNSE_HVALIDE(HOBJ))
         IOB = HOBJ - MX_DNSE_HBASE

         MX_DNSE_HGRD (IOB) = 0
         MX_DNSE_NEQL (IOB) = 0
         MX_DNSE_LKG  (IOB) = 0
      ENDIF

      MX_DNSE_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_DNSE_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_DNSE_NOBJMAX,
     &                   MX_DNSE_HBASE)

      MX_DNSE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_INI(HOBJ, HGRD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HGRD

      INCLUDE 'mxdnse.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_GRID_HVALIDE(HGRD))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_DNSE_RST(HOBJ)

      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - MX_DNSE_HBASE
         MX_DNSE_HGRD(IOB) = HGRD
      ENDIF

      MX_DNSE_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_DNSE_HBASE

C---     RECUPERE LA MEMOIRE
      LKG = MX_DNSE_LKG(IOB)
      IF (LKG .NE. 0) IERR = SO_ALLC_ALLRE8(0, LKG)

C---     RESET
      MX_DNSE_HGRD(IOB) = 0
      MX_DNSE_NEQL(IOB) = 0
      MX_DNSE_LKG (IOB) = 0

      MX_DNSE_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_DNSE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdnse.fc'
C------------------------------------------------------------------------

      MX_DNSE_REQHBASE = MX_DNSE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_DNSE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxdnse.fc'
C------------------------------------------------------------------------

      MX_DNSE_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_DNSE_NOBJMAX,
     &                                  MX_DNSE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DNSE_HBASE
      NEQL = MX_DNSE_NEQL(IOB)
      LKG  = MX_DNSE_LKG (IOB)

C---        ASSEMBLAGE DANS LA MATRICE
      WRITE (6,*) '-----------------------------------------'
      WRITE (6,*) '----  Manque assemblage matrice dense'
      WRITE (6,*) '-----------------------------------------'
C      IERR = SP$DNSE$ASMKE(NDLE,
C     &                     KLOCE,
C     &                     VKE,
C     &                     NEQL,
C     &                     H2$ALLC$KA(LKG))

      MX_DNSE_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     DIMENSIONNE
C     CALCUL DES POINTEURS DU STOCKAGE MORSE DU TYPE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER NEQL
      INTEGER LKG
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     DIMENSIONNE LA MATRICE
      IOB = HOBJ - MX_DNSE_HBASE
      NEQL = MX_DNSE_NEQL(IOB)
      LKG  = MX_DNSE_LKG (IOB)
      IERR = SO_ALLC_ALLRE8(NEQL*NEQL, LKG)

C---     STOKE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         MX_DNSE_LKG  (IOB) = LKG
      ENDIF

      MX_DNSE_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_ADDMAT(HOBJ, HRHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRHS

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('MX_DNSE_ADDMAT not yet implemented')
      CALL ERR_ASR(.FALSE.)

      MX_DNSE_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER LKG
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DNSE_HBASE
      NEQL = MX_DNSE_NEQL(IOB)
      LKG  = MX_DNSE_LKG (IOB)

      CALL DSCAL(NEQL*NEQL, A, VA(SO_ALLC_REQVIND(VA, LKG)), 1)

      MX_DNSE_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DNSE_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxdnse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdnse.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DNSE_HBASE
      NEQL = MX_DNSE_NEQL(IOB)
      LKG  = MX_DNSE_LKG (IOB)

      CALL LOG_TODO('MX_DNSE_MULVEC jamais teste, PEUT-ETRE OK!!')
      CALL ERR_ASR(.FALSE.)

C---        PRODUIT MATRICE-VECTEUR
      CALL DGEMV('N',               ! y:= alpha*a*x + beta*y
     &           NEQL,              ! m rows
     &           NEQL,              ! n columns
     &           1.0D0,             ! alpha
     &           VA(SO_ALLC_REQVIND(VA, LKG)),               ! a
     &           NEQL,              ! lda : vke(lda, 1)
     &           V,                 ! x
     &           1,                 ! incx
     &           0.0D0,             ! beta
     &           R,                 ! y
     &           1)                 ! incy

      MX_DNSE_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdnse.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DNSE_NEQL(HOBJ-MX_DNSE_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DNSE_REQNEQL = MX_DNSE_NEQL(HOBJ-MX_DNSE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DNSE_REQLKG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DNSE_REQLKG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdnse.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DNSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DNSE_LKG(HOBJ-MX_DNSE_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DNSE_REQLKG = MX_DNSE_LKG(HOBJ-MX_DNSE_HBASE)
      RETURN
      END

