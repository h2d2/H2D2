C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Description:
C     Classe utilitaire pour les méthode de résolution qui permet
C     d'assembler la matrice et le membre de droite pour une matrice
C     morse.
C
C Functions:
C   Public:
C     INTEGER MA_MORS_000
C     INTEGER MA_MORS_999
C     INTEGER MA_MORS_CTR
C     INTEGER MA_MORS_DTR
C     INTEGER MA_MORS_INI
C     INTEGER MA_MORS_RST
C     INTEGER MA_MORS_REQHBASE
C     LOGICAL MA_MORS_HVALIDE
C     INTEGER MA_MORS_ASMMTX
C     INTEGER MA_MORS_ASMRHS
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MA_MORS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MA_MORS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mamors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MA_MORS_NOBJMAX,
     &                   MA_MORS_HBASE,
     &                   'MA_MORS')

      DO I=1, MA_MORS_NOBJMAX
         MA_MORS_HMTX(I) = 0
      ENDDO

      MA_MORS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MA_MORS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mamors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER  IERR
      EXTERNAL MA_MORS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MA_MORS_NOBJMAX,
     &                   MA_MORS_HBASE,
     &                   MA_MORS_DTR)

      MA_MORS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MA_MORS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mamors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MA_MORS_NOBJMAX,
     &                   MA_MORS_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MA_MORS_HVALIDE(HOBJ))
         IOB = HOBJ - MA_MORS_HBASE
      ENDIF

      MA_MORS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MA_MORS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mamors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MA_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MA_MORS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MA_MORS_NOBJMAX,
     &                   MA_MORS_HBASE)

      MA_MORS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MA_MORS_INI(HOBJ, HMTX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMTX

      INCLUDE 'mamors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mamors.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MA_MORS_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_MORS_HVALIDE(HMTX))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MA_MORS_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - MA_MORS_HBASE
      MA_MORS_HMTX(IOB) = HMTX

      MA_MORS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MA_MORS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mamors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(MA_MORS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MA_MORS_HBASE
      MA_MORS_HMTX(IOB) = 0

      MA_MORS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MA_MORS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MA_MORS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mamors.fi'
      INCLUDE 'mamors.fc'
C------------------------------------------------------------------------

      MA_MORS_REQHBASE = MA_MORS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MA_MORS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MA_MORS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mamors.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mamors.fc'
C------------------------------------------------------------------------

      MA_MORS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MA_MORS_NOBJMAX,
     &                                  MA_MORS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice
C
C Description:
C     La fonction MA_MORS_ASMMTX assemble la matrice.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle de la simulation
C     HALG     Handle de l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MA_MORS_ASMMTX(HOBJ, HSIM, HALG, F_K)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_ASMMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      EXTERNAL F_K

      INCLUDE 'mamors.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mamors.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      LOGICAL I_DUMP

      EXTERNAL MX_MORS_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MA_MORS_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MA_MORS_HBASE
      HMTX  = MA_MORS_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_MORS_ASMKE)

C---     DUMP LA MATRICE
D     I_DUMP = .FALSE.
D     IF (I_DUMP) THEN
D        IERR = MX_MORS_DMPMAT(HMTX)
D     ENDIF

      MA_MORS_ASMMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assemble le membre de droite
C
C Description:
C     La fonction MA_MORS_ASMRHS assemble le membre de droite du
C     système matriciel. Elle n'impose pas les conditions limites.
C
C Entrée:
C     HOBJ     HANDLE SUR L'OBJET COURANT
C     HSIM     HANDLE DE LA SIMULATION
C     HALG     HANDLE DE L'ALGORITHME, PARAMETRE DES FONCTIONS F_xx
C     F_F      FONCTION A APPELER POUR ASSEMBLER LE SECOND MEMBRE
C
C Sortie:
C     VFG      MEMBRE DE DROITE
C
C Notes:
C
C************************************************************************
      FUNCTION MA_MORS_ASMRHS(HOBJ, HSIM, HALG, F_F, VFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MA_MORS_ASMRHS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_F
      REAL*8   VFG(*)
      EXTERNAL F_F

      INCLUDE 'mamors.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mamors.fc'

      REAL*8  VDUM
      INTEGER IOB
      INTEGER IERR
      INTEGER NEQL
      LOGICAL I_DUMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(MA_MORS_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MA_MORS_HBASE

C---     Assemble le membre de droite
      IF (ERR_GOOD()) IERR = F_F(HALG, VFG, VDUM, .FALSE.)

C---     Dump le membre de droite
D     I_DUMP = .FALSE.
D     IF (I_DUMP) THEN
D        NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
D        CALL DBG_DMP_DTBL(NEQL, VFG)
D     ENDIF

      MA_MORS_ASMRHS = ERR_TYP()
      RETURN
      END
