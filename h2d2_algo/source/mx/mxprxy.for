C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à MX_PRXY_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA MX_PRXY_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'mxprxy.fc'

      DATA MX_PRXY_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_PRXY_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PRXY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_PRXY_NOBJMAX,
     &                   MX_PRXY_HBASE,
     &                   'Proxy: Matrix')

      MX_PRXY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PRXY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER  IERR
      EXTERNAL MX_PRXY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_PRXY_NOBJMAX,
     &                   MX_PRXY_HBASE,
     &                   MX_PRXY_DTR)

      MX_PRXY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_PRXY_NOBJMAX,
     &                   MX_PRXY_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_PRXY_HVALIDE(HOBJ))
         IOB = HOBJ - MX_PRXY_HBASE

         MX_PRXY_HOMG(IOB) = 0
         MX_PRXY_HMDL(IOB) = 0
      ENDIF

      MX_PRXY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_PRXY_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_PRXY_NOBJMAX,
     &                   MX_PRXY_HBASE)

      MX_PRXY_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_PRXY_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - MX_PRXY_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         MX_PRXY_HOMG(IOB) = HOMG
         MX_PRXY_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_PRXY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MX_PRXY_HBASE
      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
      HFNC = 0
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      MX_PRXY_HOMG(IOB) = 0
      MX_PRXY_HMDL(IOB) = 0

      MX_PRXY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_PRXY_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PRXY_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxprxy.fc'
C------------------------------------------------------------------------

      MX_PRXY_REQHBASE = MX_PRXY_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_PRXY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_PRXY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxprxy.fc'
C------------------------------------------------------------------------

      MX_PRXY_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_PRXY_NOBJMAX,
     &                                  MX_PRXY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_ASMKE
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASMKE', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(NDLE),
     &                        SO_ALLC_CST2B(KLOCE(1)),
     &                        SO_ALLC_CST2B(VKE(1)))

      MX_PRXY_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_DIMMAT
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DIMMAT', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     SO_ALLC_CST2B(HOMG),
     &                                     SO_ALLC_CST2B(HSIM))

      MX_PRXY_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_ADDMAT(HOBJ, HRHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_ADDMAT
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRHS

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ADDMAT', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     SO_ALLC_CST2B(HOMG),
     &                                     SO_ALLC_CST2B(HRHS))

      MX_PRXY_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     MULTIPLIE LA MATRICE PAR UN SCALAIRE
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_MULVAL(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_MULVAL
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VAL

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'MULVAL', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2(HFNC,
     &                                     SO_ALLC_CST2B(HOMG),
     &                                     SO_ALLC_CST2B(VAL))

      MX_PRXY_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     FAIT L'OPERATION {RES} = [M]{VEC}
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_MULVEC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'MULVEC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(N),
     &                        SO_ALLC_CST2B(V(1)),
     &                        SO_ALLC_CST2B(R(1)))

      MX_PRXY_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_PRXY_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_PRXY_REQNEQL
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'mxprxy.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG
      INTEGER HMDL, HFNC
      INTEGER N
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_PRXY_HBASE

      HOMG = MX_PRXY_HOMG(IOB)
      HMDL = MX_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQNEQL', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) N = SO_FUNC_CALL1(HFNC,
     &                                  SO_ALLC_CST2B(HOMG))

      MX_PRXY_REQNEQL = N
      RETURN
      END
