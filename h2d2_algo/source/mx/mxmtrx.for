C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Destructeur de l'objet
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_DTR(HOBJ)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_DTR(HOBJ)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_DTR(HOBJ)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_DTR(HOBJ)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_DTR(HOBJ)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_DTR(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction MX_MTRX_CTRLH permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_CTRLH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_CTRLH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      LOGICAL VALIDE
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      VALIDE = .FALSE.
      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ENDIF

      MX_MTRX_CTRLH = VALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_ASMKE(HOBJ, NDLE, KLOCE, VKE)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_DIMMAT(HOBJ, HSIM)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_DIMMAT(HOBJ, HSIM)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_DIMMAT(HOBJ, HSIM)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_DIMMAT(HOBJ, HSIM)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_DIMMAT(HOBJ, HSIM)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_DIMMAT(HOBJ, HSIM)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_ADDMAT(HOBJ, HRHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRHS

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_ADDMAT(HOBJ, HRHS)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_ADDMAT(HOBJ, HRHS)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_ADDMAT(HOBJ, HRHS)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_ADDMAT(HOBJ, HRHS)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_ADDMAT(HOBJ, HRHS)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_ADDMAT(HOBJ, HRHS)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     MULTIPLIE LA MATRICE PAR UN SCALAIRE
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_MULVAL(HOBJ, VAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VAL

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_MULVAL(HOBJ, VAL)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_MULVAL(HOBJ, VAL)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_MULVAL(HOBJ, VAL)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_MULVAL(HOBJ, VAL)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_MULVAL(HOBJ, VAL)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_MULVAL(HOBJ, VAL)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MX_MTRX_MULVEC
C
C Description:
C     La méthode MX_MTRX_MULVEC fait l'opération {R} = [M]{V}.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     N           Dimension des vecteurs
C     V           Vecteur à multiplier
C
C Sortie:
C     R           Vecteur résultat
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         IERR = MX_PRXY_MULVEC(HOBJ, N, V, R)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         IERR = MX_MORS_MULVEC(HOBJ, N, V, R)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         IERR = MX_SKYL_MULVEC(HOBJ, N, V, R)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         IERR = MX_DNSE_MULVEC(HOBJ, N, V, R)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         IERR = MX_DIAG_MULVEC(HOBJ, N, V, R)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         IERR = MX_DIST_MULVEC(HOBJ, N, V, R)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction MX_MTRX_REQNEQL retourne la dimension de la matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_MTRX_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_MTRX_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxmtrx.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxprxy.fi'
      INCLUDE 'mxskyl.fi'
      INCLUDE 'mxdnse.fi'
      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdist.fi'

      INTEGER N
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (MX_PRXY_HVALIDE(HOBJ)) THEN
         N = MX_PRXY_REQNEQL(HOBJ)
      ELSEIF (MX_MORS_HVALIDE(HOBJ)) THEN
         N = MX_MORS_REQNEQL(HOBJ)
      ELSEIF (MX_SKYL_HVALIDE(HOBJ)) THEN
         N = MX_SKYL_REQNEQL(HOBJ)
      ELSEIF (MX_DNSE_HVALIDE(HOBJ)) THEN
         N = MX_DNSE_REQNEQL(HOBJ)
      ELSEIF (MX_DIAG_HVALIDE(HOBJ)) THEN
         N = MX_DIAG_REQNEQL(HOBJ)
      ELSEIF (MX_DIST_HVALIDE(HOBJ)) THEN
         N = MX_DIST_REQNEQL(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      MX_MTRX_REQNEQL = N
      RETURN
      END
