C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: MX
C      H2D2 Class: MX_DIAG
C         SUBROUTINE MX_DIAG_000
C         SUBROUTINE MX_DIAG_999
C         SUBROUTINE MX_DIAG_CTR
C         SUBROUTINE MX_DIAG_DTR
C         SUBROUTINE MX_DIAG_INI
C         SUBROUTINE MX_DIAG_RST
C         SUBROUTINE MX_DIAG_REQHBASE
C         SUBROUTINE MX_DIAG_HVALIDE
C         SUBROUTINE MX_DIAG_ASMKE
C         SUBROUTINE MX_DIAG_ASMFIN
C         SUBROUTINE MX_DIAG_ASGMTX
C         SUBROUTINE MX_DIAG_DIMMAT
C         SUBROUTINE MX_DIAG_ADDMAT
C         SUBROUTINE MX_DIAG_INIMAT
C         SUBROUTINE MX_DIAG_MULVAL
C         SUBROUTINE MX_DIAG_MULVEC
C         SUBROUTINE MX_DIAG_ASMDIM
C         SUBROUTINE MX_DIAG_REQHMORS
C         SUBROUTINE MX_DIAG_REQNEQL
C         SUBROUTINE MX_DIAG_REQLKG
C         SUBROUTINE MX_DIAG_DMPMAT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_DIAG_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_DIAG_NOBJMAX,
     &                   MX_DIAG_HBASE,
     &                   'Matrice diagonale')

      MX_DIAG_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER  IERR
      EXTERNAL MX_DIAG_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_DIAG_NOBJMAX,
     &                   MX_DIAG_HBASE,
     &                   MX_DIAG_DTR)

      MX_DIAG_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_DIAG_NOBJMAX,
     &                   MX_DIAG_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_DIAG_HVALIDE(HOBJ))
         IOB = HOBJ - MX_DIAG_HBASE

         MX_DIAG_NEQL(IOB) = 0
         MX_DIAG_LKG (IOB) = 0
         MX_DIAG_HMTX(IOB) = 0
         MX_DIAG_IOPR(IOB) = 0
         MX_DIAG_OWNR(IOB) = .FALSE.
      ENDIF

      MX_DIAG_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_DIAG_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_DIAG_NOBJMAX,
     &                   MX_DIAG_HBASE)

      MX_DIAG_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MX_DIAG_INI
C
C Description:
C     La fonction MX_DIAG_INI initialise et dimensionne une matrice
C     diagonale.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IOPR        Opération de diagonalisation (lump)
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_INI(HOBJ, IOPR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPR

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_DIAG_RST(HOBJ)

C---     CONTRÔLES
      IF (IOPR .LT. SP_MORS_LUMP_PREMIERE) GOTO 9900
      IF (IOPR .GE. SP_MORS_LUMP_DERNIERE) GOTO 9900

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MX_DIAG_HBASE
         MX_DIAG_HMTX(IOB) = 0
         MX_DIAG_IOPR(IOB) = IOPR
         MX_DIAG_OWNR(IOB) = .TRUE.
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OPERATION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_DIAG_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LKG
      INTEGER HMTX
      LOGICAL OWNR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      LKG  = MX_DIAG_LKG (IOB)
      HMTX = MX_DIAG_HMTX(IOB)
      OWNR = MX_DIAG_OWNR(IOB)

C---     DETRUIS LA MATRICE MORSE
      IF (OWNR .AND. MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

C---     RECUPERE LA MEMOIRE
      IF (LKG .NE. 0) IERR = SO_ALLC_ALLRE8(0, LKG)

C---     RESET
      MX_DIAG_NEQL(IOB) = 0
      MX_DIAG_LKG (IOB) = 0
      MX_DIAG_HMTX(IOB) = 0
      MX_DIAG_IOPR(IOB) = SP_MORS_LUMP_INDEFINI
      MX_DIAG_OWNR(IOB) = .FALSE.

      MX_DIAG_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_DIAG_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdiag.fc'
C------------------------------------------------------------------------

      MX_DIAG_REQHBASE = MX_DIAG_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_DIAG_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxdiag.fc'
C------------------------------------------------------------------------

      MX_DIAG_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_DIAG_NOBJMAX,
     &                                  MX_DIAG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice Ke dans la matrice globale.
C
C Description:
C     La méthode <code>MX_DIAG_ASMKE</code> ajoute à l'objet la contribution
C     de la matrice élémentaire VKE passée en paramètre.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      HMTX = MX_DIAG_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     ASSEMBLE VIA LA MATRICE MORSE
      IERR = MX_MORS_ASMKE(HMTX, NDLE, KLOCE, VKE)

      MX_DIAG_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Signale la fin de l'assemblage.
C
C Description:
C     La méthode <code>MX_DIAG_ASMFIN</code> signale que l'assemblage est
C     terminé. Au besoin, la matrice va être lumpée à partir de sa forme
C     morse.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_ASMFIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_ASMFIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IOPR
      INTEGER HMTX
      INTEGER LDIAG
      INTEGER LIAP, LJAP, LKG
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB   = HOBJ - MX_DIAG_HBASE
      LDIAG = MX_DIAG_LKG (IOB)
      HMTX  = MX_DIAG_HMTX(IOB)
      IOPR  = MX_DIAG_IOPR(IOB)

C---     RECUPERE LES POINTEURS
      IF (ERR_GOOD()) THEN
         NEQL = MX_MORS_REQNEQL(HMTX)
         LIAP = MX_MORS_REQLIAP(HMTX)
         LJAP = MX_MORS_REQLJAP(HMTX)
         LKG  = MX_MORS_REQLKG (HMTX)
      ENDIF

C---     LUMP LA MATRICE
      IF (ERR_GOOD()) THEN
         IERR = SP_MORS_LUMP(NEQL,
     &                       KA(SO_ALLC_REQKIND(KA,LIAP)),
     &                       KA(SO_ALLC_REQKIND(KA,LJAP)),
     &                       VA(SO_ALLC_REQVIND(VA,LKG)),
     &                       VA(SO_ALLC_REQVIND(VA,LDIAG)),
     &                       IOPR)
      ENDIF

      MX_DIAG_ASMFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne une matrice externe
C
C Description:
C     La fonction MX_DIAG_ASGMTX assigne la matrice externe à utiliser.
C     Les types de matrice supportés sont les matrices morses et les
C     matrices distribuées. L'appel à MX_DIAG_ASGMTX doit être suivi d'un
C     appel à MX_DIAG_DIMMAT afin de dimensionner les tables internes.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HMXT     Handle sur la matrice externe
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_ASGMTX(HOBJ, HMXT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_ASGMTX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMXT

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX, HMRS
      LOGICAL OWNR
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
D     CALL ERR_PRE(HMXT .NE. 0)
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_DIAG_HBASE
      HMTX = MX_DIAG_HMTX(IOB)
      OWNR = MX_DIAG_OWNR(IOB)

C---     Détruis la matrice sous-jacente
      IF (OWNR .AND. MX_MORS_HVALIDE(HMTX))
     &   IERR = MX_MORS_DTR(HMTX)

C---     Gère le type de matrice
      IF     (MX_MORS_HVALIDE(HMXT)) THEN
         HMRS = HMXT
      ELSEIF (MX_DIST_HVALIDE(HMXT)) THEN
         HMRS = MX_DIST_REQHMORS(HMXT)
D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

C---     Assigne la nouvelle matrice
      IF (ERR_GOOD()) THEN
         MX_DIAG_HMTX(IOB) = HMRS
         MX_DIAG_OWNR(IOB) = .FALSE.
      ENDIF

      MX_DIAG_ASGMTX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne la matrice
C
C Description:
C     La fonction <code>MX_DIAG_DIMMAT</code> dimensionne la matrice. Elle
C     alloue la mémoire pour les tables et les initialise.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de la simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER IOPR
      INTEGER HMTX
      INTEGER LKG
      INTEGER NEQL
      LOGICAL OWNR
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)
      HMTX = MX_DIAG_HMTX(IOB)
      IOPR = MX_DIAG_IOPR(IOB)
      OWNR = MX_DIAG_OWNR(IOB)

C---     SI NEQL EST IDENTIQUE ALORS LA MATRICE EST DIMENSIONNEE
C---     ======================================================
      IF (NEQL .EQ. LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)) THEN
D        CALL ERR_ASR(LKG .NE. 0)

C---        INITIALISE LA MATRICE
         CALL DINIT(NEQL, 0.0D0, VA(SO_ALLC_REQVIND(VA, LKG)), 1)
         IF (OWNR .AND. HMTX .NE. 0) IERR = MX_MORS_DIMMAT(HMTX, HSIM)

C---     SI NON PROPRIETAIRE
C---     ===================
      ELSEIF (.NOT. OWNR) THEN
D        CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))
D        CALL ERR_ASR(MX_MORS_REQLKG(HMTX) .NE. 0) ! Matrice dimensionnée

C---        PARAMETRES DE LA SIMULATION
         NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
D        CALL ERR_ASR(NEQL .EQ. MX_MORS_REQNEQL(HMTX))

C---        DIMENSIONNE LA MATRICE
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LKG)

C---        STOKE LES ATTRIBUTS
         IF (ERR_GOOD()) THEN
            MX_DIAG_NEQL(IOB) = NEQL
            MX_DIAG_LKG (IOB) = LKG
         ENDIF

C---     MATRICE NON DIMENSIONNEE
C---     ========================
      ELSE

C---        RESET GLOBAL, ON REASSIGNE LES DIM À LA FIN
         IERR = MX_DIAG_RST(HOBJ)

C---        PARAMETRES DE LA SIMULATION
         NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)

C---        DIMENSIONNE LA MATRICE
         IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL, LKG)

C---        INITIALISE LA MATRICE MORSE AU BESOIN
         IF (HMTX .EQ. 0) THEN
            IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
            IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, 0)  ! ILU = 0
         ENDIF
C---        DIMENSIONNE LA MATRICE MORSE AU BESOIN
         IF (HMTX .NE. 0) THEN
            IF (ERR_GOOD()) IERR = MX_MORS_DIMMAT(HMTX, HSIM)
         ENDIF

C---        STOKE LES ATTRIBUTS
         IF (ERR_GOOD()) THEN
            MX_DIAG_NEQL(IOB) = NEQL
            MX_DIAG_LKG (IOB) = LKG
            MX_DIAG_HMTX(IOB) = HMTX
            MX_DIAG_IOPR(IOB) = IOPR
            MX_DIAG_OWNR(IOB) = .TRUE.
         ENDIF
      ENDIF

      MX_DIAG_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Addition de matrice
C
C Description:
C     La méthode MX_DIAG_ADDMAT additionne à l'objet courant la matrice
C     HMAT.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HMAT        Handle sur la matrice à additionner
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_ADDMAT(HOBJ, HMAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMAT

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NKGP, NEQL
      INTEGER LJDP
      INTEGER LKGx, LKGy

      REAL*8 UN
      PARAMETER (UN = 1.0D0)
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
D     CALL ERR_PRE(HOBJ .NE. HMAT)
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      NEQL = MX_DIAG_REQNEQL(HOBJ)
      LKGy = MX_DIAG_REQLKG (HOBJ)

      IF (MX_DIAG_HVALIDE(HMAT)) THEN
D        CALL ERR_ASR(NEQL .EQ. MX_DIAG_REQNEQL(HMAT))
         LKGx = MX_DIAG_REQLKG (HMAT)
         CALL DAXPY(NEQL, UN,
     &              VA(SO_ALLC_REQVIND(VA,LKGx)), 1,
     &              VA(SO_ALLC_REQVIND(VA,LKGy)), 1)

D     ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

      MX_DIAG_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_INIMAT(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_INIMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL  = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)

C---     Initialise à la valeur
      CALL DINIT(NEQL,     A, VA(SO_ALLC_REQVIND(VA, LKG)), 1)

      MX_DIAG_INIMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplie la matrice
C
C Description:
C     La fonction MX_DIAG_MULVAL multiplie la matrice par le facteur A.
C     La matrice est modifiée.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL  = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)

C---     MULTIPLIE
      CALL DSCAL(NEQL, A, VA(SO_ALLC_REQVIND(VA, LKG)), 1)

      MX_DIAG_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplie externe composante par composante
C
C Description:
C     La fonction MX_DIAG_MULVEC retourne dans le vecteur R
C     le produit composante par composante du vecteur V
C     et de la matrice
C     La matrice n'est pas modifiée.
C
C Entrée:
C     V(*)     Vecteur à multiplier
C
C Sortie:
C     R(*)     Résultat
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)
D     CALL ERR_ASR(NEQL .GT. 0)
D     CALL ERR_ASR(N .EQ. NEQL)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LKG))

      CALL DCOPY(NEQL, V, 1, R, 1)
      CALL DAXTY(NEQL, 1.0D0, VA(SO_ALLC_REQVIND(VA, LKG)), 1, R, 1)

      MX_DIAG_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplie composante par composante
C
C Description:
C     La fonction MX_DIAG_MULVECI multiplie la matrice 
C     composante par composante ave le vecteur V.
C     La matrice est modifiée.
C
C Entrée:
C     V(*)     Vecteur à multiplier
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_MULVECI(HOBJ, N, V)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_MULVECI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NEQL
      INTEGER LKG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)
D     CALL ERR_ASR(NEQL .GT. 0)
D     CALL ERR_ASR(N .EQ. NEQL)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LKG))

      CALL DAXTY(NEQL, 1.0D0, V, 1, VA(SO_ALLC_REQVIND(VA, LKG)), 1)

      MX_DIAG_MULVECI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     DIMENSIONNE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_ASMDIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_ASMDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER NEQL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MX_DIAG_HBASE

C---     PARAMETRES DE LA SIMULATION
      NEQL = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NEQL)
      IF (NEQL .LE. 0) GOTO 9900

C---     STOKE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         MX_DIAG_NEQL (IOB) = NEQL
      ENDIF

      GOTO 9999
C----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NEQ_NUL'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      MX_DIAG_ASMDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la matrice morse associée
C
C Description:
C     La fonction MX_DIAG_REQHMORS retourne le handle sur la matrice
C     morse associée.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_REQHMORS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_REQHMORS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdiag.fc'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIAG_HMTX(HOBJ-MX_DIAG_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIAG_REQHMORS = MX_DIAG_HMTX(HOBJ-MX_DIAG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdiag.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIAG_NEQL(HOBJ-MX_DIAG_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIAG_REQNEQL = MX_DIAG_NEQL(HOBJ-MX_DIAG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Pointeur à la  table VKG
C
C Description:
C     La méthode MX_MORS_REQLKG retourne le pointeur à la table VKG
C     des valeurs de la matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIAG_REQLKG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_REQLKG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'mxdiag.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIAG_LKG(HOBJ-MX_DIAG_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIAG_REQLKG = MX_DIAG_LKG(HOBJ-MX_DIAG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIAG_DMPMAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIAG_DMPMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdiag.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spmors.fi'
      INCLUDE 'mxdiag.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LKG
      INTEGER NEQL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIAG_HBASE
      NEQL  = MX_DIAG_NEQL(IOB)
      LKG  = MX_DIAG_LKG (IOB)

!      IERR = SP_DIAG_DMPMAT(NEQL,
!     &                      VA(SO_ALLC_REQVIND(VA, LKG)))

      MX_DIAG_DMPMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
CC Sommaire: Construit une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_DIAG_CTRSMX(HOBJ, HSMX)
CC$pragma aux MX_DIAG_CTRSMX export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_DIAG_CTRSMX
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HSMX
C
C      INCLUDE 'mxdiag.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'spmors.fi'
C      INCLUDE 'soallc.fi'
C      INCLUDE 'mxdiag.fc'
C
C      INTEGER IOB
C      INTEGER ISM
C      INTEGER IERR
C      INTEGER LKG
C      INTEGER NKGP
CC------------------------------------------------------------------------
CD     CALL ERR_PRE(MX_DIAG_HVALIDE(HOBJ))
CC------------------------------------------------------------------------
C
CC---     CONSTRUIS LA SOUS-MATRICE
C      IERR = MX_DIAG_CTR(HSMX)
C      IERR = MX_DIAG_INI(HSMX,
C     &                   MX_DIAG_ILU (IOB))
C
CC---     RECUPERE LES ATTRIBUTS
C      IOB = HOBJ - MX_DIAG_HBASE
C      ISM = HSMX - MX_DIAG_HBASE
C
CC---     ASSIGNE LES VALEURS DU PARENT
C      MX_DIAG_ILUM (ISM) = MX_DIAG_ILUM (IOB)
C      MX_DIAG_NDLT (ISM) = MX_DIAG_NDLT (IOB)
C      MX_DIAG_NKGP (ISM) = MX_DIAG_NKGP (IOB)
C      MX_DIAG_NKGSP(ISM) = MX_DIAG_NKGSP(IOB)
C      MX_DIAG_NCIEL(ISM) = MX_DIAG_NCIEL(IOB)
C      MX_DIAG_LIAP (ISM) = MX_DIAG_LIAP (IOB)
C      MX_DIAG_LJDP (ISM) = MX_DIAG_LJDP (IOB)
C      MX_DIAG_LJAP (ISM) = MX_DIAG_LJAP (IOB)
C      MX_DIAG_LJTR (ISM) = MX_DIAG_LJTR (IOB)
C      MX_DIAG_SMTX (ISM) = .TRUE.
C
CC---     ALLOUE LA MEMOIRE
C      NKGP = MX_DIAG_NKGP(ISM)
C      LKG  = 0
C      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGP, LKG)
C      IF (ERR_GOOD()) MX_DIAG_LKG(ISM) = LKG
C
C      MX_DIAG_CTRSMX = ERR_TYP()
C      RETURN
C      END
C
C************************************************************************
CC Sommaire: Initialise une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_DIAG_INISM(HOBJ, HPAR, DLLMIN, DLLMAX)
CC$pragma aux MX_DIAG_INISM export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_DIAG_INISM
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HPAR
C      INTEGER DLLMIN
C      INTEGER DLLMAX
C
C      INCLUDE 'mxdiag.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'mxdiag.fc'
C
CC      INTEGER IERR
CC      INTEGER IOB
CC------------------------------------------------------------------------
CD      CALL ERR_ASR(MX_DIAG_HVALIDE(HPAR))
CC------------------------------------------------------------------------
C
C      MX_DIAG_INISM = ERR_TYP()
C      RETURN
C      END
