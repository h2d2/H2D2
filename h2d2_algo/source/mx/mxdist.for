C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER MX_DIST_000
C     INTEGER MX_DIST_999
C     INTEGER MX_DIST_CTR
C     INTEGER MX_DIST_DTR
C     INTEGER MX_DIST_INI
C     INTEGER MX_DIST_RST
C     INTEGER MX_DIST_REQHBASE
C     LOGICAL MX_DIST_HVALIDE
C     INTEGER MX_DIST_ASMKE
C     INTEGER MX_DIST_DIMMAT
C     INTEGER MX_DIST_ADDMAT
C     INTEGER MX_DIST_MULVAL
C     INTEGER MX_DIST_MULVEC
C     INTEGER MX_DIST_REQNEQL
C     INTEGER MX_DIST_REQNEQP
C     INTEGER MX_DIST_REQNEQT
C     INTEGER MX_DIST_REQHMORS
C     INTEGER MX_DIST_REQLLING
C     INTEGER MX_DIST_REQLCOLG
C   Private:
C     INTEGER MX_DIST_ASMLCGL
C     INTEGER MX_DIST_KLCGL_GLOBAL
C     INTEGER MX_DIST_KLCGL_PETSC
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction <code>MX_DIST_000(...)</code> initialise les tables
C     internes de l'objet. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_DIST_NOBJMAX,
     &                   MX_DIST_HBASE,
     &                   'Matrix distributed')

      MX_DIST_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fc'

      INTEGER  IERR
      EXTERNAL MX_DIST_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_DIST_NOBJMAX,
     &                   MX_DIST_HBASE,
     &                   MX_DIST_DTR)

      MX_DIST_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_DIST_NOBJMAX,
     &                   MX_DIST_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_DIST_HVALIDE(HOBJ))
         IOB = HOBJ - MX_DIST_HBASE

         MX_DIST_HMTX (IOB) = 0
         MX_DIST_ITYP (IOB) = MX_DIST_NUM_INDEFINI
         MX_DIST_NEQP (IOB) = 0
         MX_DIST_NEQT (IOB) = 0
         MX_DIST_LLING(IOB) = 0
         MX_DIST_LCOLG(IOB) = 0
      ENDIF

      MX_DIST_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxdist.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_DIST_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_DIST_NOBJMAX,
     &                   MX_DIST_HBASE)

      MX_DIST_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_INI(HOBJ, ITYP, ADONNEES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ITYP
      LOGICAL ADONNEES

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(ITYP .EQ. MX_DIST_NUM_GLOBAL .OR.
D    &             ITYP .EQ. MX_DIST_NUM_PETSC)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_DIST_RST(HOBJ)

C---     INITIALISE LA MATRICE MORSE AU BESOIN
      HMTX = 0
      IF (ADONNEES) THEN
         IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMTX)
         IF (ERR_GOOD()) IERR = MX_MORS_INI(HMTX, 0)  ! ILU = 0
      ENDIF

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - MX_DIST_HBASE
         MX_DIST_HMTX (IOB) = HMTX
         MX_DIST_ITYP (IOB) = ITYP
         MX_DIST_NEQP (IOB) = 0
         MX_DIST_NEQT (IOB) = 0
         MX_DIST_LLING(IOB) = 0
         MX_DIST_LCOLG(IOB) = 0
      ENDIF

      MX_DIST_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
      INTEGER LLING, LCOLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - MX_DIST_HBASE

C---     RECUPERE LES ATTRIBUTS
      HMTX = MX_DIST_HMTX (IOB)
      LLING= MX_DIST_LLING(IOB)
      LCOLG= MX_DIST_LCOLG(IOB)

C---     DETRUIS LA MATRICE MORSE
      IF (MX_MORS_HVALIDE(HMTX)) IERR = MX_MORS_DTR(HMTX)

C---     RECUPERE LA MEMOIRE
      IF (LCOLG .NE. 0) IERR = SO_ALLC_ALLINT(0, LCOLG)
      IF (LLING .NE. 0) IERR = SO_ALLC_ALLINT(0, LLING)

C---     RESET LES ATTRIBUTS
      MX_DIST_HMTX (IOB) = 0
      MX_DIST_ITYP (IOB) = MX_DIST_NUM_INDEFINI
      MX_DIST_NEQP (IOB) = 0
      MX_DIST_NEQT (IOB) = 0
      MX_DIST_LLING(IOB) = 0
      MX_DIST_LCOLG(IOB) = 0

      MX_DIST_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_DIST_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C------------------------------------------------------------------------

      MX_DIST_REQHBASE = MX_DIST_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_DIST_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxdist.fc'
C------------------------------------------------------------------------

      MX_DIST_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_DIST_NOBJMAX,
     &                                  MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assemble la matrice Ke dans la matrice globale.
C
C Description:
C     La méthode <code>MX_DIST_ASMKE</code> ajoute à l'objet la contribution
C     de la matrice élémentaire VKE passée en paramètre.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX = MX_DIST_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     TRAITEMENT VIA LA MATRICE MORSE
      IERR = MX_MORS_ASMKE(HMTX, NDLE, KLOCE, VKE)

      MX_DIST_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne la matrice
C
C Description:
C     DIMENSIONNE
C     CALCUL DES POINTEURS DU STOCKAGE MORSE DU TYPE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX, HNUMR
      INTEGER LLING, LLOCN
      INTEGER NDLN, NNL, NNT, NEQ
C----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX  = MX_DIST_HMTX (IOB)
      LLING = MX_DIST_LLING(IOB)

C---     DIMENSIONNE LA MATRICE MORSE
      IF (ERR_GOOD() .AND. MX_MORS_HVALIDE(HMTX)) THEN
         IERR = MX_MORS_DIMMAT(HMTX, HSIM)
      ENDIF

C---     AU BESOIN, CREE LES TABLES DE LOCALISATION
      IF (ERR_GOOD() .AND. LLING .EQ. 0) THEN

C---        Récupère les données des DDL
         HNUMR = LM_HELE_REQHNUMC(HSIM)
         LLOCN = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_LLOCN)
         NDLN  = LM_HELE_REQPRM(HSIM, LM_HELE_PRM_NDLN)
         NNL   = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
         NNT   = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)

C---        Assemble le tableau de localisation globale
         IERR = MX_DIST_ASMLCGL(HOBJ,
     &                          HNUMR,
     &                          NDLN,
     &                          NNL,
     &                          NNT,
     &                          KA(SO_ALLC_REQKIND(KA,LLOCN)))
      ENDIF

      MX_DIST_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_ADDMAT(HOBJ, HRHS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRHS

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX = MX_DIST_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     TRAITEMENT VIA LA MATRICE MORSE
      IERR = MX_MORS_ADDMAT(HMTX, HRHS)

      MX_DIST_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX = MX_DIST_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     TRAITEMENT VIA LA MATRICE MORSE
      IERR = MX_MORS_MULVAL(HMTX, A)

      MX_DIST_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_DIST_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX = MX_DIST_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

C---     TRAITEMENT VIA LA MATRICE MORSE
      IERR = MX_MORS_MULVEC(HMTX, N, V, R)

      MX_DIST_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode MX_DIST_REQNEQL retourne le nombre d'équations local.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MX_DIST_HBASE
      HMTX = MX_DIST_HMTX(IOB)
D     CALL ERR_ASR(MX_MORS_HVALIDE(HMTX))

      MX_DIST_REQNEQL = MX_MORS_REQNEQL(HMTX)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode MX_DIST_REQNEQP retourne le nombre d'équations privées.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQNEQP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQNEQP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_DIST_REQNEQP = MX_DIST_NEQP(HOBJ-MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode MX_DIST_REQNEQT retourne le nombre d'équations total.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQNEQT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQNEQT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_DIST_REQNEQT = MX_DIST_NEQT(HOBJ-MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne la matrice morse associée
C
C Description:
C     La fonction MX_DIST_REQHMORS retourne le handle sur la matrice
C     morse associée.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQHMORS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQHMORS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIST_HMTX(HOBJ-MX_DIST_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIST_REQHMORS = MX_DIST_HMTX(HOBJ-MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQLLING(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQLLING
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIST_LLING(HOBJ-MX_DIST_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIST_REQLLING = MX_DIST_LLING(HOBJ-MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_REQLCOLG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_DIST_REQLCOLG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxdist.fi'
      INCLUDE 'mxdist.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
D     CALL ERR_PRE(MX_DIST_LCOLG(HOBJ-MX_DIST_HBASE) .NE. 0)
C------------------------------------------------------------------------

      MX_DIST_REQLCOLG = MX_DIST_LCOLG(HOBJ-MX_DIST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: MX_DIST_ASMLCGL
C
C Description:
C     La fonction privée MX_DIST_ASMLCGL assemble les tables de transfert
C     local-global pour les DDL des noeuds.
C     On cherche en premier la table de localisation globale des DDL des
C     noeuds locaux. Puis on supprime les entrées nulles.
C
C Entrée:
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Total local
C     NEQ         Nombre d'EQuations local
C     KLOCN       Table de localisation locale
C
C Sortie:
C     KCOLG       Table de transfert local-global
C
C Notes:
C************************************************************************
      FUNCTION MX_DIST_ASMLCGL(HOBJ,
     &                         HNUMR,
     &                         NDLN,
     &                         NNL,
     &                         NNT,
     &                         KLOCN)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HNUMR
      INTEGER  NDLN
      INTEGER  NNL
      INTEGER  NNT
      INTEGER  KLOCN(NDLN, NNL)

      INCLUDE 'mxdist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mxdist.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITYP
      INTEGER LLING, LCOLG
      INTEGER L_TRV
      INTEGER NEQL, NEQP, NEQT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_DIST_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - MX_DIST_HBASE
      ITYP  = MX_DIST_ITYP (IOB)
      LLING = MX_DIST_LLING(IOB)
      LCOLG = MX_DIST_LCOLG(IOB)
D     CALL ERR_ASR(LLING .EQ. 0)
      NEQL  = MX_DIST_REQNEQL(HOBJ)

C---     Dimensionne le tableau de localisation globale
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, LLING)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NEQL, LCOLG)

C---     Remplis la table KCOLG
      IF (ERR_BAD()) GOTO 9999
      L_TRV = 0
      IF (ITYP .EQ. MX_DIST_NUM_GLOBAL) THEN
         IERR = SO_ALLC_ALLINT(NDLN*NNT, L_TRV)

         IERR = MX_DIST_KLCGL_GLOBAL(HNUMR,
     &                               NDLN,
     &                               NNL,
     &                               NNT,
     &                               NEQL,
     &                               KLOCN,
     &                               NEQP,
     &                               NEQT,
     &                               KA(SO_ALLC_REQKIND(KA,L_TRV)),
     &                               KA(SO_ALLC_REQKIND(KA,LLING)),
     &                               KA(SO_ALLC_REQKIND(KA,LCOLG)))

      ELSE IF (ITYP .EQ. MX_DIST_NUM_PETSC)  THEN
         IERR = SO_ALLC_ALLINT(NDLN*NNL, L_TRV)

         IERR = MX_DIST_KLCGL_PETSC (NDLN,
     &                               NNL,
     &                               NEQL,
     &                               KLOCN,
     &                               NEQT,
     &                               KA(SO_ALLC_REQKIND(KA,L_TRV)),
     &                               KA(SO_ALLC_REQKIND(KA,LLING)))

      ENDIF

C---     RÉCUPÈRE LA MÉMOIRE
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)

C---        STOKE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         MX_DIST_NEQP (IOB) = NEQP
         MX_DIST_NEQT (IOB) = NEQT
         MX_DIST_LLING(IOB) = LLING
         MX_DIST_LCOLG(IOB) = LCOLG
      ENDIF

9999  CONTINUE
      MX_DIST_ASMLCGL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MX_DIST_KLCGL_GLOBAL
C
C Description:
C     La fonction statique privée MX_DIST_KLCGL_GLOBAL monte la table de
C     transfert local-global pour la numérotation globale initiale. Pour cela,
C     elle monte la table KLOCG de localisation globale des noeuds locaux du
C     process. C'est une numérotation par noeud, quelle que soit la numérotation
C     locale.
C     <p>
C     La table KLOCG(NDLN, NNT) contient les numéros d'équation globale de tous
C     les noeuds:
C     <pre>
C        KLOCG(ID, IN) == 0 si le ddl n'est pas assigné
C                      >  0 si le ddl est assigné; c'est le no global
C     </pre>
C     <p>
C     La table KCOLG(NEQL) contient les numéros d'équation globale des
C     ddl locaux au process.
C     <pre>
C        KCOLG(IE)  <  0 si le ddl n'est pas privé
C                   >  0 si le ddl est privé
C     </pre>
C     La table KLING(NEQL) est une compression de KCOLG qui ne comprend que les
C     numéros d'équation globale des noeuds privés au process, donc ceux tel que
C     KCOLG(IE) > 0.
C
C Entrée:
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Local
C     NNT         Nombre de Noeuds Total
C     NCOL        Nombre de COLonnes
C     KLOCN       Table de localisation locale
C
C Sortie:
C     NEQP        Nombre d'Équations privées
C     NEQT        Nombre d'Équation Global
C     KLOCG       Table de LOCalisation Globale des noeuds locaux
C     KLING       Table de DDL globaux des LIGnes
C     KCOLG       Table de DDL globaux des COLonnes
C
C Notes:
C     ieq_lcl = klocn(i)
C     ieq_glb = klcgl(ieq_lcl)
C************************************************************************
      FUNCTION MX_DIST_KLCGL_GLOBAL(HNUMR,
     &                              NDLN,
     &                              NNL,
     &                              NNT,
     &                              NEQL,
     &                              KLOCN,
     &                              NEQP,
     &                              NEQT,
     &                              KLOCG,
     &                              KLING,
     &                              KCOLG)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      INTEGER NNT
      INTEGER NEQL
      INTEGER KLOCN(NDLN, NNL)
      INTEGER NEQP
      INTEGER NEQT
      INTEGER KLOCG(NDLN, NNT)
      INTEGER KLING(NEQL)
      INTEGER KCOLG(NEQL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mxdist.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER ID, IN, IC, IL, IG
      INTEGER NN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.algo.matrix.distributed'

C---     Initialise
      CALL IINIT(NEQL, 0, KLING, 1)
      CALL IINIT(NEQL, 0, KCOLG, 1)

C---     Peuple KLOCG avec l'info locale (présence/absence)
      CALL IINIT(NDLN*NNT, 0, KLOCG, 1)
      IF (ERR_GOOD()) THEN
         DO IN=1,NNL
            NN = NM_NUMR_REQNEXT(HNUMR, IN)
            DO ID=1,NDLN
               IF (KLOCN(ID,IN) .NE. 0) KLOCG(ID,NN) = 1
            ENDDO
         ENDDO
      ENDIF

C---     Réduis pour obtenir la table globale (présence/absence)
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(MPI_IN_PLACE, KLOCG, NDLN*NNT,
     &                      MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Transforme en table KLOCG
      NEQT = 0
      IF (ERR_GOOD()) THEN
         DO IN=1,NNT
            DO ID=1,NDLN
               IF (KLOCG(ID,IN) .NE. 0) THEN
                   NEQT = NEQT + 1
                   KLOCG(ID,IN) = NEQT
               ENDIF
            ENDDO
         ENDDO
      ENDIF

C---     Monte la table des colonnes (DDL local) --> (DDL global)
      IF (ERR_GOOD()) THEN
D        NEQP = 0          ! Check sum
         DO IN=1,NNL
            NN = NM_NUMR_REQNEXT(HNUMR, IN)
            DO ID=1,NDLN
               IL = KLOCN(ID,IN)
               IF (IL .NE. 0) THEN
                  IC = ABS(IL)
                  IG = KLOCG(ID,NN)
D                 CALL ERR_ASR(IC .GE. 1 .AND. IC .LE. NEQL)
D                 CALL ERR_ASR(IG .GE. 1 .AND. IG .LE. NEQT)
                  KCOLG(IC) = SIGN(IG, IL)   ! Garde le signe
D                 NEQP = NEQP + 1
D              ELSE
D                 IG = KLOCG(ID,NN)
D                 CALL ERR_ASR(IG .EQ. 0)
               ENDIF
            ENDDO
         ENDDO
D        CALL ERR_ASR(NEQP .EQ. NEQL)
      ENDIF

C---     Monte la table des lignes KLING
      IF (ERR_GOOD()) THEN
         NEQP = 0
         DO IL=1,NEQL
            IG = KCOLG(IL)
D           CALL ERR_ASR(IG .NE. 0)
            IF (IG .GT. 0) THEN
               KLING(IL) = IG
               NEQP = NEQP + 1
            ENDIF
         ENDDO
D        CALL ERR_ASR(NEQP .LE. NEQL)
      ENDIF

C---     Écrase le signe de KCOLG
      IF (ERR_GOOD()) THEN
         DO IL=1,NEQL
            KCOLG(IL) = ABS(KCOLG(IL))
         ENDDO
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_DDL_MAP_LOCAL_GLOBAL'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         DO IN=1,NNL
            DO ID=1,NDLN
               IL = KLOCN(ID,IN)
               IG = 0
               IF (IL .NE. 0) IG = KCOLG(ABS(IL))
               WRITE(LOG_BUF,'(I9,I3,2I12)') IN, ID, IL, IG
               CALL LOG_DBG(LOG_ZNE, LOG_BUF)
            ENDDO
         ENDDO
      ENDIF

      MX_DIST_KLCGL_GLOBAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: MX_DIST_KLCGL_PETSC
C
C Description:
C     La fonction privée MX_DIST_KLCGL_PETSC monte la table de transfert
C     local-global en format pour PETSc. Pour cela, elle monte la table KLOCG
C     de localisation globale des noeuds privés du process. C'est une
C     numérotation par noeud, quelle que soit la numérotation locale.
C     <p>
C     La table KLOCG(NDLN, NNT) contient les numéros d'équation globale des
C     noeuds privés du process:
C     <pre>
C        KLOCG(ID, IN) == 0 si le ddl n'est pas assigné
C                      >  0 si le ddl est privé ; c'est alors le no global
C     </pre>
C     En numérotation globale, les ddl privés doivent former un bloc
C     sans trous et être en ordre croissant.
C
C Entrée:
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Local
C     KLOCN       Table de localisation locale
C
C Sortie:
C     KLOCG       Table de LOCalisation Globale des noeuds locaux (privés)
C     KCOLG       Table de transfert local-global
C
C Notes:
C     ieq_lcl = klocn(i)
C     ieq_glb = klcgl(ieq_lcl)
C************************************************************************
      FUNCTION MX_DIST_KLCGL_PETSC(NDLN,
     &                             NNL,
     &                             NEQ,
     &                             KLOCN,
     &                             NEQT,
     &                             KLOCG,
     &                             KCOLG)

      IMPLICIT NONE

      INTEGER NDLN
      INTEGER NNL
      INTEGER NEQ
      INTEGER KLOCN(NDLN, NNL)
      INTEGER NEQT
      INTEGER KLOCG(NDLN, NNL)
      INTEGER KCOLG(NEQ)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'mxdist.fc'

      INTEGER I_RANK, I_SIZE, I_ERROR
      INTEGER I_SIZMAX
      PARAMETER (I_SIZMAX = 64)

      INTEGER IERR
      INTEGER ID, IN, IP
      INTEGER ILOCGMIN
      INTEGER NEQL, NN
      INTEGER KSEND(I_SIZMAX), KRECV(I_SIZMAX)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.algo.matrix.distributed'

C---     Initialise
      CALL IINIT(NDLN*NNL, 0, KLOCG, 1)

C---     KLOCG en numérotation locale par noeud
      NEQL = 0
      DO IN=1,NNL
         DO ID=1,NDLN
            IF (KLOCN(ID,IN) .GT. 0) THEN
               NEQL = NEQL + 1
               KLOCG(ID,IN) = NEQL
            ENDIF
         ENDDO
      ENDDO
D     CALL ERR_ASR(NEQL .EQ. NEQ)

C---     Paramètres MPI
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      ENDIF
      CALL ERR_ASR(ERR_BAD() .OR. I_SIZE .LE. I_SIZMAX)  ! Toujours actif car dynamic

C---     Synchronise les NEQL
      IF (ERR_GOOD()) THEN
         DO IP=1,I_SIZE
            KSEND(IP) = -1
            KRECV(IP) = -1
         ENDDO
         KSEND(I_RANK+1) = NEQL          ! Assigne au process
         CALL MPI_ALLREDUCE(KSEND, KRECV, I_SIZE, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     NEQ Total
      IF (ERR_GOOD()) THEN
         NEQT = 0
         DO IP=1,I_SIZE
            NEQT = NEQT + KRECV(IP)
         ENDDO
      ENDIF

C---     DDL local min = DDL global max cumulé jusqu'au process
      IF (ERR_GOOD()) THEN
         ILOCGMIN = 0
         DO IP=1,I_RANK                      ! Les I_RANK sont en base 0
            ILOCGMIN = ILOCGMIN + KRECV(IP)  ! les KRECV en base 1
         ENDDO
      ENDIF

C---     KLOCG en numérotation globale
      IF (ERR_GOOD()) THEN
         DO IN=1,NNL
            DO ID=1,NDLN
               IF (KLOCG(ID,IN) .GT. 0) THEN
                  KLOCG(ID,IN) = KLOCG(ID,IN) + ILOCGMIN
               ENDIF
            ENDDO
         ENDDO
      ENDIF

C---     Initialise KCOLG
      CALL IINIT(NEQ, 0, KCOLG,  1)

C---     Monte la table (DDL local) --> (DDL global)
      IF (ERR_GOOD()) THEN
         DO IN=1,NNL
            DO ID=1,NDLN
               IF (KLOCN(ID,IN) .GT. 0) THEN
                  NN = KLOCN(ID,IN)
                  KCOLG(NN) = KLOCG(ID,IN)
D                 CALL ERR_ASR(KLOCG(ID,IN) .GT. 0)
D              ELSE
D                 CALL ERR_ASR(KLOCG(ID,IN) .EQ. 0)
               ENDIF
            ENDDO
         ENDDO
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_DDL_MAP_LOCAL_GLOBAL'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         DO ID=1,NEQ
            WRITE(LOG_BUF,'(2I9)') ID, KCOLG(ID)
            CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         ENDDO
      ENDIF

      MX_DIST_KLCGL_PETSC = ERR_TYP()
      RETURN
      END
