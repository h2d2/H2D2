C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>MX_CPNT_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MX_CPNT_NOBJMAX,
     &                   MX_CPNT_HBASE,
     &                   'Matrix in component form')

      MX_CPNT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER  IERR
      EXTERNAL MX_CPNT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MX_CPNT_NOBJMAX,
     &                   MX_CPNT_HBASE,
     &                   MX_CPNT_DTR)

      MX_CPNT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MX_CPNT_NOBJMAX,
     &                   MX_CPNT_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MX_CPNT_HVALIDE(HOBJ))
         IOB = HOBJ - MX_CPNT_HBASE

         MX_CPNT_HMRS (IOB) = 0
      ENDIF

      MX_CPNT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MX_CPNT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MX_CPNT_NOBJMAX,
     &                   MX_CPNT_HBASE)

      MX_CPNT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_INI(HOBJ, ILU)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ILU

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MX_CPNT_RST(HOBJ)

C---     Construit le parent
      IF (ERR_GOOD()) IERR = MX_MORS_CTR(HMRS)
      IF (ERR_GOOD()) IERR = MX_MORS_INI(HMRS, ILU)

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - MX_CPNT_HBASE
         MX_CPNT_HMRS (IOB) = HMRS
      ENDIF

      MX_CPNT_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Détruis le parent
      IF (MX_MORS_HVALIDE(HMRS)) IERR = MX_MORS_DTR(HMRS)

C---     Reset
      MX_CPNT_HMRS (IOB) = 0

      MX_CPNT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MX_CPNT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxcpnt.fc'
C------------------------------------------------------------------------

      MX_CPNT_REQHBASE = MX_CPNT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MX_CPNT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mxcpnt.fc'
C------------------------------------------------------------------------

      MX_CPNT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MX_CPNT_NOBJMAX,
     &                                  MX_CPNT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_ASMKE(HOBJ, NDLE, KLOCE, VKE)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_ASMKE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLE
      INTEGER KLOCE(*)
      REAL*8  VKE  (*)

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_ASMKE(HMRS, NDLE, KLOCE, VKE)

      MX_CPNT_ASMKE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Dimensionne et initialise la matrice.
C
C Description:
C     La méthode MX_CPNT_DIMMAT dimensionne la matrice. Elle fait les appels
C     pour assembler les dimensions et les indices. Elle initialise l'espace
C     de la matrice.
C     La méthode doit être appelée avant tout calcul sur la matrice afin de
C     l'initialiser.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_DIMMAT(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_DIMMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_DIMMAT(HMRS, HSIM)

C---     P
      CALL LOG_TODO('Manque le calcul des indices I')
      CALL ERR_ASR(.FALSE.)

      MX_CPNT_DIMMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     DIMENSIONNE
C     CALCUL DES POINTEURS DU STOCKAGE MORSE DU TYPE
C     COMPRESSION DE LIGNE
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_LISMAT(HOBJ, FNAME)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_LISMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_LISMAT(HMRS, FNAME)

      MX_CPNT_LISMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Addition de matrice
C
C Description:
C     La méthode MX_CPNT_ADDMAT additionne à l'objet courant la matrice
C     HMAT.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HMAT        Handle sur la matrice à additionner
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_ADDMAT(HOBJ, HMAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_ADDMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HMAT

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_ADDMAT(HMRS, HMAT)

      MX_CPNT_ADDMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un scalaire.
C
C Description:
C     La méthode MX_CPNT_MULVAL multiplie l'objet courant par le scalaire
C     A.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     A           Facteur multiplicatif
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_MULVAL(HOBJ, A)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_MULVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  A

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_MULVAL(HMRS, A)

      MX_CPNT_MULVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Multiplication par un vecteur
C
C Description:
C     La méthode MX_CPNT_MULVEC fait l'opération {R} = [M]{V}.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     N           Dimension des vecteurs
C     V           Vecteur à multiplier
C
C Sortie:
C     R           Vecteur résultat
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_MULVEC(HOBJ, N, V, R)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_MULVEC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N
      REAL*8  V(*)
      REAL*8  R(*)

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_MULVEC(HMRS, N, V, R)

      MX_CPNT_MULVEC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQILU(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQILU
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQILU=MX_MORS_REQILU(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQNEQL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQNEQL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQNEQL=MX_MORS_REQNEQL(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQNKGP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQNKGP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQNKGP=MX_MORS_REQNKGP(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQLIAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQLIAP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQLIAP=MX_MORS_REQLIAP(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQLJAP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQLJAP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQLJAP=MX_MORS_REQLJAP(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQLJDP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQLJDP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQLJDP=MX_MORS_REQLJDP(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire: Pointeur à la  table VKG
C
C Description:
C     La méthode MX_CPNT_REQLKG retourne le pointeur à la table VKG
C     des valeurs de la matrice.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_CPNT_REQLKG(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_REQLKG
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      MX_CPNT_REQLKG=MX_MORS_REQLKG(MX_CPNT_HMRS(HOBJ-MX_CPNT_HBASE))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MX_CPNT_DMPMAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MX_CPNT_DMPMAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mxcpnt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mxmors.fi'
      INCLUDE 'mxcpnt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMRS
C------------------------------------------------------------------------
D     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - MX_CPNT_HBASE
      HMRS = MX_CPNT_HMRS(IOB)

C---     Appel du parent
      IERR = MX_MORS_DMPMAT(HMRS)

      MX_CPNT_DMPMAT = ERR_TYP()
      RETURN
      END

C************************************************************************
CC Sommaire: Construit une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_CPNT_CTRSMX(HOBJ, HSMX)
CC$pragma aux MX_CPNT_CTRSMX export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_CPNT_CTRSMX
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HSMX
C
C      INCLUDE 'mxcpnt.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'spmors.fi'
C      INCLUDE 'soallc.fi'
C      INCLUDE 'mxcpnt.fc'
C
C      INTEGER IOB
C      INTEGER ISM
C      INTEGER IERR
C      INTEGER LKG
C      INTEGER NKGP
CC------------------------------------------------------------------------
CD     CALL ERR_PRE(MX_CPNT_HVALIDE(HOBJ))
CC------------------------------------------------------------------------
C
CC---     CONSTRUIS LA SOUS-MATRICE
C      IERR = MX_CPNT_CTR(HSMX)
C      IERR = MX_CPNT_INI(HSMX,
C     &                   MX_CPNT_ILU (IOB))
C
CC---     RECUPERE LES ATTRIBUTS
C      IOB = HOBJ - MX_CPNT_HBASE
C      ISM = HSMX - MX_CPNT_HBASE
C
CC---     ASSIGNE LES VALEURS DU PARENT
C      MX_CPNT_ILUM (ISM) = MX_CPNT_ILUM (IOB)
C      MX_CPNT_NDLT (ISM) = MX_CPNT_NDLT (IOB)
C      MX_CPNT_NKGP (ISM) = MX_CPNT_NKGP (IOB)
C      MX_CPNT_NKGSP(ISM) = MX_CPNT_NKGSP(IOB)
C      MX_CPNT_LIAP (ISM) = MX_CPNT_LIAP (IOB)
C      MX_CPNT_LJDP (ISM) = MX_CPNT_LJDP (IOB)
C      MX_CPNT_LJAP (ISM) = MX_CPNT_LJAP (IOB)
C      MX_CPNT_LJTR (ISM) = MX_CPNT_LJTR (IOB)
C      MX_CPNT_SMTX (ISM) = .TRUE.
C
CC---     ALLOUE LA MEMOIRE
C      NKGP = MX_CPNT_NKGP(ISM)
C      LKG  = 0
C      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NKGP, LKG)
C      IF (ERR_GOOD()) MX_CPNT_LKG(ISM) = LKG
C
C      MX_CPNT_CTRSMX = ERR_TYP()
C      RETURN
C      END
C
C************************************************************************
CC Sommaire: Initialise une sous-matrice
CC
CC Description:
CC
CC Entrée:
CC
CC Sortie:
CC
CC Notes:
CC
C************************************************************************
C      FUNCTION MX_CPNT_INISM(HOBJ, HPAR, DLLMIN, DLLMAX)
CC$pragma aux MX_CPNT_INISM export
CCMS$ ATTRIBUTES DLLEXPORT :: MX_CPNT_INISM
C
C      IMPLICIT NONE
C
C      INTEGER HOBJ
C      INTEGER HPAR
C      INTEGER DLLMIN
C      INTEGER DLLMAX
C
C      INCLUDE 'mxcpnt.fi'
C      INCLUDE 'err.fi'
C      INCLUDE 'mxcpnt.fc'
C
CC      INTEGER IERR
CC      INTEGER IOB
CC------------------------------------------------------------------------
CD      CALL ERR_ASR(MX_CPNT_HVALIDE(HPAR))
CC------------------------------------------------------------------------
C
C      MX_CPNT_INISM = ERR_TYP()
C      RETURN
C      END
