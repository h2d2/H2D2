C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C   Private:
C
C************************************************************************

      MODULE MX_MORS_M

      USE SO_ALLC_M
      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: MX_MORS_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: MX_MORS_SELF_T
         INTEGER ILU
         INTEGER HSIM
         INTEGER HHSH
         INTEGER NEQL
         INTEGER NKGP
         INTEGER NKGSP
         TYPE(SO_ALLC_PTR_T) :: LIAP
         TYPE(SO_ALLC_PTR_T) :: LJDP
         TYPE(SO_ALLC_PTR_T) :: LJAP
         TYPE(SO_ALLC_PTR_T) :: LKG
      END TYPE MX_MORS_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée MX_MORS_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MX_MORS_REQSELF(HOBJ)

      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE

      TYPE (MX_MORS_SELF_T), POINTER :: MX_MORS_REQSELF
      INTEGER, INTENT(IN):: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (MX_MORS_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

D     CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
D     CALL ERR_ASR(ERR_GOOD())
D     CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      MX_MORS_REQSELF => SELF
      RETURN
      END FUNCTION MX_MORS_REQSELF

      END MODULE MX_MORS_M
