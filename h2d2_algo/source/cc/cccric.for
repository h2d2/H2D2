C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère de Convergence
C Objet:   Critère de Convergence
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CC_CRIC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_CRIC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cccric.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CC_CRIC_NOBJMAX,
     &                   CC_CRIC_HBASE,
     &                   'Convergence Criteria')

      CC_CRIC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_CRIC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cccric.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER  IERR
      EXTERNAL CC_CRIC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CC_CRIC_NOBJMAX,
     &                   CC_CRIC_HBASE,
     &                   CC_CRIC_DTR)

      CC_CRIC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CC_CRIC_NOBJMAX,
     &                   CC_CRIC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CC_CRIC_HVALIDE(HOBJ))
         IOB = HOBJ - CC_CRIC_HBASE

         CC_CRIC_DNRM(1:CC_CRIC_NITERMAX, IOB) = 1.0D0
         CC_CRIC_NMIN(IOB) = -1
         CC_CRIC_ILST(IOB) = -1
         CC_CRIC_ISKP(IOB) = -1
         CC_CRIC_STTS(IOB) = CC_CRIC_STATUS_UNDEFINED
         CC_CRIC_RSON(IOB) = CC_CRIC_REASON_UNDEFINED
      ENDIF

      CC_CRIC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CC_CRIC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CC_CRIC_NOBJMAX,
     &                   CC_CRIC_HBASE)

      CC_CRIC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     NMIN     Nombre d'itérations obligatoires
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_INI(HOBJ, NMIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NMIN

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (NMIN .LT. 1) GOTO 9900

C---     Reset les données
      IERR = CC_CRIC_RST(HOBJ)

C---     Assigne les attributs
      IOB = HOBJ - CC_CRIC_HBASE
      CC_CRIC_DNRM(CC_CRIC_NITERMAX, IOB) = 1.0D0
      CC_CRIC_NMIN(IOB) = NMIN
      CC_CRIC_ILST(IOB) = -1
      CC_CRIC_ISKP(IOB) = -1
      CC_CRIC_STTS(IOB) = CC_CRIC_STATUS_UNDEFINED
      CC_CRIC_RSON(IOB) = CC_CRIC_REASON_UNDEFINED

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6,A)')
     &   'ERR_NMIN_INVALIDE',':', NMIN, ' (NMIN > 0)'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CC_CRIC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CC_CRIC_HBASE
      CC_CRIC_DNRM(1:CC_CRIC_NITERMAX, IOB) = 1.0D0
      CC_CRIC_NMIN(IOB) = -1
      CC_CRIC_ILST(IOB) = -1
      CC_CRIC_ISKP(IOB) = -1
      CC_CRIC_STTS(IOB) = CC_CRIC_STATUS_UNDEFINED
      CC_CRIC_RSON(IOB) = CC_CRIC_REASON_UNDEFINED

      CC_CRIC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CC_CRIC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_CRIC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cccric.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------

      CC_CRIC_REQHBASE = CC_CRIC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CC_CRIC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CC_CRIC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------

      CC_CRIC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CC_CRIC_NOBJMAX,
     &                                  CC_CRIC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Calcule le critère de convergence.
C
C Description:
C     La fonction <code>CC_CRIC_CALCRI(...)</code> retourne .TRUE.
C     si le critère de convergence est satisfait.
C     Le critère est la norme L2 sur l'accroissement relatif,
C     tous les DDL.
C     Une itération ITER .LE. 0 marque une notification de rejet 
C     par l'utilisateur.
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ITER        Itération actuelle
C     NITER       Nombre max d'itérations
C
C Sortie:
C
C Notes:
C     Le code de detection des oscillations n'est pas au point. Si la
C     detection est "pas si pire",  l'interaction avec la convergence
C     n'est pas au point et l'utilisation en aval non plus. Si le pas
C     a été limité par backtracking ou limiteur, il est superflu de 
C     le limiter encore (surtout sur la borne min de globalisation).
C************************************************************************
      FUNCTION CC_CRIC_CALCRI(HOBJ,
     &                        VNRM,
     &                        ITER,
     &                        NITER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VNRM
      INTEGER ITER
      INTEGER NITER

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'splsqr.fi'
      INCLUDE 'cccric.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTATUS, IREASON

      REAL*8  DLTA_5(5), D1_3, D1_4
      REAL*8  R3_5, A3_5, B3_5
      REAL*8  R4_5, A4_5, B4_5
      REAL*8  DNRM(CC_CRIC_NITERMAX), DNRM_1, DNRM_N
      REAL*8  QNEWT
      REAL*8  R2, A2, B2
      REAL*8  R3, A3, B3
      REAL*8  R4, A4, B4
      REAL*8  R5, A5, B5
      INTEGER I, ILST, ISKP, NMIN, NXNG
      LOGICAL DOOSC
      REAL*8, SAVE :: X(CC_CRIC_NITERMAX)
     &               = (/ (I, I=CC_CRIC_NITERMAX,1,-1) /)


      REAL*8, PARAMETER :: A2_SEUIL = -1.0D-2 ! Seuil de pente de divergence
      REAL*8, PARAMETER :: A2_PLIM  =  1.0D0  ! Pentes limites
      REAL*8, PARAMETER :: A3_PLIM  =  0.5D0
      REAL*8, PARAMETER :: A4_PLIM  =  0.0D0
      REAL*8, PARAMETER :: A5_PLIM  = -0.5D0
C------------------------------------------------------------------------
      LOGICAL EST_DIV
      EST_DIV(A2, A3, A4, A5) = ((A2 .GT. A2_PLIM) .OR.
     &                           (A3 .GT. A3_PLIM) .OR.
     &                           (A4 .GT. A4_PLIM) .OR.
     &                           (A5 .GT. A5_PLIM))
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.algo.conv'

C---     Récupère les attributs
      IOB = HOBJ - CC_CRIC_HBASE
      DNRM = CC_CRIC_DNRM(:,IOB)
      NMIN = CC_CRIC_NMIN(IOB)
      ISKP = CC_CRIC_ISKP(IOB)
      ILST = CC_CRIC_ILST(IOB)
      IF (ITER .EQ. 1) ISKP = NMIN
      
C---     La norme
      IF (ITER .NE. ILST)
     &   DNRM(2:CC_CRIC_NITERMAX) = DNRM(1:CC_CRIC_NITERMAX-1)
      DNRM_1  = VNRM
      DNRM(1) = LOG(DNRM_1)

C---     Calcule les regressions sur 2, 3, 4, et 5 points
      A2 = -1.0D0
      A3 = -1.0D0
      A4 = -1.0D0
      A5 = -1.0D0
      IF (ITER .GE. 2) R2 = SP_LSQR_LINREG(2, X, DNRM, A2, B2)
      IF (ITER .GE. 3) R3 = SP_LSQR_LINREG(3, X, DNRM, A3, B3)
      IF (ITER .GE. 4) R4 = SP_LSQR_LINREG(4, X, DNRM, A4, B4)
      IF (ITER .GE. 5) R5 = SP_LSQR_LINREG(5, X, DNRM, A5, B5)

C---     Détecte les oscillations
      DOOSC = .FALSE.
      NXNG  = -1
      !!! ====================
      !!!      c.f. Note
      !!! ====================
      !!! IF (ITER .GE. 5) THEN
      !!!    R3_5 = SP_LSQR_LINREG(3, X(2:4), DNRM(2:4), A3_5, B3_5)
      !!!    R4_5 = SP_LSQR_LINREG(4, X(2:5), DNRM(2:5), A4_5, B4_5)
      !!!    D1_3 = B3_5 + A3_5*X(1)
      !!!    D1_4 = B4_5 + A4_5*X(1)
      !!!    IF ((DNRM(1)-D1_3) .GT. 1.0D0) DOOSC = .TRUE.
      !!!    IF ((DNRM(1)-D1_4) .GT. 1.0D0) DOOSC = .TRUE.
      !!!    ! ---  Les écarts par rapport à la regression sur 5 points
      !!!    DLTA_5(:) = DNRM(1:5) - (B5 + A5*X(1:5))
      !!!    ! ---  Écart min pour avoir des oscillations
      !!!    IF (MINVAL(ABS(DLTA_5(:))) .GT. 0.20D0) THEN
      !!!       ! ---  Compte le nombre de croisements +-
      !!!       NXNG = COUNT(DLTA_5(1:4)*DLTA_5(2:5) .LT. 0.0D0)
      !!!       IF (NXNG .GE. 2) DOOSC = .TRUE.
      !!!    ENDIF
      !!!    IF (DOOSC) ISKP = MAX(ITER+4, NMIN)
      !!! ENDIF

C---     Le critère
      IF (ITER .LE. 0) THEN
         ISTATUS = CC_CRIC_STATUS_STEP_REJECTED
         IREASON = CC_CRIC_REASON_EXTERNAL_IMPOSED
      ELSEIF (ITER .LE. ISKP) THEN
         IF (DNRM(1) .LE. 0.0D0) THEN
            ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
            IREASON = CC_CRIC_REASON_STEP_CONVERGED
         ELSEIF (DOOSC .AND. EST_DIV(A2,A3,A4,A5)) THEN
            ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
            IREASON = CC_CRIC_REASON_OSCILLATION_DETECTED
         ELSE
            ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
            IREASON = CC_CRIC_REASON_CONVERGENCE_IN_PROGRESS
         ENDIF
      ELSEIF (ITER .EQ. NITER) THEN
         IF (A2 .LT. A2_SEUIL) THEN
            IF (DNRM(1) .LE. 0.0D0) THEN
               ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
               IREASON = CC_CRIC_REASON_STEP_CONVERGED
            ELSE
               ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
               IREASON = CC_CRIC_REASON_EOI
            ENDIF
         ELSE
            ISTATUS = CC_CRIC_STATUS_STEP_REJECTED
            IREASON = CC_CRIC_REASON_DIVERGENCE_ESTABLISHED
         ENDIF
      ELSE
         IF (DOOSC .AND. EST_DIV(A2,A3,A4,A5)) THEN
            ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
            IREASON = CC_CRIC_REASON_OSCILLATION_DETECTED
         ELSEIF (EST_DIV(A2,A3,A4,A5)) THEN
            ISTATUS = CC_CRIC_STATUS_STEP_REJECTED
            IREASON = CC_CRIC_REASON_DIVERGENCE_ESTABLISHED
         ELSEIF (A2 .LT. A2_SEUIL) THEN
            IF (DNRM(1) .LE. 0.0D0) THEN
               ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
               IREASON = CC_CRIC_REASON_STEP_CONVERGED
            ELSE
               ! ---  Projection à NITER de la norme
               DNRM_N = A2 * (NITER-1-ITER)     ! Pente * iter restantes
               QNEWT  = DNRM(1) + DNRM_N        ! Norme attendue
               IF (QNEWT .GE. 0.0D0) THEN
                  ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
                  IREASON = CC_CRIC_REASON_WEAK_CONVERGENCE
               ELSE
                  ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
                  IREASON = CC_CRIC_REASON_CONVERGENCE_IN_PROGRESS
               ENDIF
            ENDIF
         ELSE
            ISTATUS = CC_CRIC_STATUS_STEP_ACCEPTED
            IREASON = CC_CRIC_REASON_DIVERGENCE_DETECTED
         ENDIF
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_NRM_CONV_L2_RES#<35>#= ',
     &                                 DNRM_1
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IF (ITER .GE. 2) THEN
         WRITE(LOG_BUF, '(2(A,1PE14.6E3),3X,L3)')
     &      'MSG_NRM_PENTE_2#<35>#= ',A2,' <= ',A2_PLIM,(A2.LE.A2_PLIM)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (ITER .GE. 3) THEN
         WRITE(LOG_BUF, '(2(A,1PE14.6E3),3X,L3)')
     &      'MSG_NRM_PENTE_3#<35>#= ',A3,' <= ',A3_PLIM,(A3.LE.A3_PLIM)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (ITER .GE. 4) THEN
         WRITE(LOG_BUF, '(2(A,1PE14.6E3),3X,L3)')
     &      'MSG_NRM_PENTE_4#<35>#= ',A4,' <= ',A4_PLIM,(A4.LE.A4_PLIM)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF
      IF (ITER .GE. 5) THEN
         WRITE(LOG_BUF, '(2(A,1PE14.6E3),3X,L3)')
     &      'MSG_NRM_PENTE_5#<35>#= ',A5,' <= ',A5_PLIM,(A5.LE.A5_PLIM)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDIF
!!!      IF (ITER .GE. 5) THEN
!!!         WRITE(LOG_BUF, '(2(A,1PE14.6E3))') 'MSG_PROJ_3#<35>#= ',
!!!     &                       (DNRM(1)-D1_3), ' <  ', 1.00D0
!!!         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!         WRITE(LOG_BUF, '(2(A,1PE14.6E3))') 'MSG_PROJ_4#<35>#= ',
!!!     &                       (DNRM(1)-D1_4), ' <  ', 1.00D0
!!!         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!         WRITE(LOG_BUF, '(A,I6)')  'MSG_NBR_XING#<35>#= ', NXNG
!!!         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!         WRITE(LOG_BUF, '(2(A,1PE14.6E3))') 'MSG_MIN_LOG#<35>#= ',
!!!     &                       MINVAL(ABS(DLTA_5(:))), ' >  ', 0.20D0
!!!         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!!!!         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_MAX_LOG#<35>#= ',
!!!!!!     &                       MAXVAL(ABS(DLTA_5(:)))
!!!!!!         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!      ENDIF
      
      WRITE(LOG_BUF, '(2A,L3)') 'MSG_EST_DIV', '= ',EST_DIV(A2,A3,A4,A5)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
!!!      WRITE(LOG_BUF, '(2A,L3)') 'MSG_EST_OSCILLANT', '= ',DOOSC
!!!      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,I6)') 'MSG_STATUS', '= ', ISTATUS
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,I6)') 'MSG_REASON', '= ', IREASON
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CC_CRIC_DNRM(:,IOB) = DNRM(:)
      CC_CRIC_ILST(IOB) = ITER
      CC_CRIC_ISKP(IOB) = ISKP
      CC_CRIC_STTS(IOB) = ISTATUS
      CC_CRIC_RSON(IOB) = IREASON

      CC_CRIC_CALCRI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CC_CRIC_REQVAL(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_ESTRAISON(HOBJ, IR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_REQVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IR

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CC_CRIC_ESTRAISON = (CC_CRIC_RSON(HOBJ-CC_CRIC_HBASE) .EQ. IR)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CC_CRIC_ESTSTATUS(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_ESTSTATUS(HOBJ, IS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_ESTSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IS

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CC_CRIC_ESTSTATUS = (CC_CRIC_STTS(HOBJ-CC_CRIC_HBASE) .EQ. IS)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne l'état du critère.
C
C Description:
C     La fonction <code>CC_CRIC_REQSTATUS(...)</code> retourne l'état
C     du critère.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_REQVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CC_CRIC_REQSTATUS = CC_CRIC_STTS(HOBJ-CC_CRIC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne la raison de l'état.
C
C Description:
C     La fonction <code>CC_CRIC_REQRAISON(...)</code> retourne la 
C     raison de l'état du critère.
C
C Entrée:
C     HOBJ        L'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CC_CRIC_REQRAISON(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CC_CRIC_REQRAISON
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cccric.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CC_CRIC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CC_CRIC_REQRAISON = CC_CRIC_RSON(HOBJ-CC_CRIC_HBASE)
      RETURN
      END
