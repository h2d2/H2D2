C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: IC
C      H2D2 Class: IC_ALNWTN
C         INTEGER IC_ALNWTN_XEQCTR
C         INTEGER IC_ALNWTN_PRN
C         INTEGER IC_ALNWTN_XEQMTH
C         CHARACTER*(32) IC_ALNWTN_REQCLS
C         INTEGER IC_ALNWTN_REQHDL
C         SUBROUTINE IC_ALNWTN_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALNWTN_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNWTN_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icnwtn.fi'
      INCLUDE 'alnwtn.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icnwtn.fc'

      INTEGER IERR
      INTEGER I
      INTEGER HOBJ, HALG
      INTEGER HCRA, HCRC, HGLB
      INTEGER HRES
      INTEGER NITER, NSITR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALNWTN_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      HCRA = 0
      HCRC = 0
      HGLB = 0
      HRES = 0
      NITER = 0
      NSITR = 0
      I = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = I + 1
C     <comment>Handle on the stopping criterion</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HCRA)
      I = I + 1
C     <comment>Handle on the convergence criterion</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HCRC)
      I = I + 1
C     <comment>Handle on the globalization algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HGLB)
      I = I + 1
C     <comment>Handle on the matrix resolution algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HRES)
      I = I + 1
C     <comment>Number of iterations (max)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, NITER)
      IF (IERR .NE. 0) GOTO 9901
      I = I + 1
C     <comment>Number of sub-iterations (default 0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, NSITR)
      IF (IERR .NE. 0) NSITR = 0

C---     Construis et initialise l'objet concret
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_NWTN_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_NWTN_INI(HALG,
     &                                   HCRA,
     &                                   HCRC,
     &                                   HGLB,
     &                                   HRES,
     &                                   NITER,
     &                                   NSITR)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_ALNWTN_PRN(HALG)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>newton</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNWTN_AID()

9999  CONTINUE
      IC_ALNWTN_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: fonction PRiNt
C
C Description: Fonction PRN qui imprime tous les paramètres de l'objet
C
C Entrée: HOBJ
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNWTN_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnwtn.fc'
      INCLUDE 'icnwtn.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNDL
      INTEGER NITER
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_ALGORITHME_NEWTON'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_NWTN_HBASE
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                             NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         HNDL = AL_NWTN_HCRA (IOB)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HNDL)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_CRITERE_ARRET#<35>#', '= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         HNDL = AL_NWTN_HCCG (IOB)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HNDL)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_CRITERE_CONVERGENCE#<35>#','= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         HNDL = AL_NWTN_HGLB(IOB)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HNDL)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_GLOBALISATION#<35>#', '= ',
     &                             NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         HNDL = AL_NWTN_HRES(IOB)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HNDL)
         WRITE (LOG_BUF,'(2A,A)') 'MSG_RESO_MATRICIEL#<35>#', '= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         NITER = AL_NWTN_NITER(IOB)
         WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_ITERATIONS#<35>#', '= ',
     &                              NITER
         CALL LOG_ECRIS(LOG_BUF)
         NITER = AL_NWTN_NSITR(IOB)
         WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_SOUS_ITER#<35>#', '= ',
     &                              NITER
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_ALNWTN_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALNWTN_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNWTN_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icnwtn.fi'
      INCLUDE 'alnwtn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnwtn.fc'
      INCLUDE 'icnwtn.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NWTN_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the matrix resolution algorithm</comment>
         IF (PROP .EQ. 'hres') THEN
            IVAL = AL_NWTN_HRES(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'H', ',', IVAL
C        <comment>Number of iterations (max)</comment>
         ELSEIF (PROP .EQ. 'niter') THEN
            IVAL = AL_NWTN_NITER(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NWTN_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'hres') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            IF (.NOT. MR_RESO_HVALIDE(IVAL)) GOTO 9901
            AL_NWTN_HRES(IOB) = IVAL
         ELSEIF (PROP .EQ. 'niter') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_NWTN_NITER(IOB) = IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_NWTN_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_NWTN_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALNWTN_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALNWTN_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNWTN_AID()

9999  CONTINUE
      IC_ALNWTN_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNWTN_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNWTN_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icnwtn.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>newton</b> represents the resolution algorithm of a non-linear
C  system with the linearization iterative method of Newton.
C</comment>
      IC_ALNWTN_REQCLS = 'newton'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNWTN_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNWTN_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icnwtn.fi'
      INCLUDE 'alnwtn.fi'
C-------------------------------------------------------------------------

      IC_ALNWTN_REQHDL = AL_NWTN_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALNWTN_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icnwtn.hlp')

      RETURN
      END
