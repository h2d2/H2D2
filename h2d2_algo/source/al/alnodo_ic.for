C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C      FUNCTION IC_ALNODO_XEQCTR ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNODO_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNODO_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alnodo_ic.fi'
      INCLUDE 'alnodo.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnodo_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HNOD
      INTEGER HALG, HXPR, HLFT, HRHT
      INTEGER NITER
      INTEGER IRST
      LOGICAL LRST
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALNODO_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the resolution algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HALG)
C     <comment>Number of  iterations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, NITER)
C     <comment>Handle on the testing expression</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HXPR)
C     <comment>Handle on the left node executed if <code>hxpr</code> returns TRUE</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HLFT)
C     <comment>Handle on the right node executed if <code>hxpr</code> returns FALSE</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, HRHT)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>In case <code>hxpr</code> returns FALSE, indicated if the solution
C              is reset [0=no rest, 1=do reset] (default 1).</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 6, IRST)
      IF (IERR .NE. 0) IRST = 1
      LRST = (IRST .NE. 0)

C---     CONSTRUIS ET INITIALISE L'OBJET
      HNOD = 0
      IF (ERR_GOOD()) IERR = AL_NODO_CTR(HNOD)
      IF (ERR_GOOD()) IERR = AL_NODO_INI(HNOD, HALG, NITER,
     &                                   HXPR, HLFT, HRHT, LRST)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HNOD)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALNODO_PRN(HNOD)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the node</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HNOD
      ENDIF

C<comment>
C  The constructor <b>algo_node_for</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNODO_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_ALNODO_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALNODO_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNODO_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alnodo_ic.fi'
      INCLUDE 'alnodo.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnodo.fc'
      INCLUDE 'alnodo_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NODO_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the resolution algorithm</comment>
         IF (PROP .EQ. 'h_algo') THEN
            IVAL = AL_NODO_HALG(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_NODO_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALNODO_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALNODO_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNODO_AID()

9999  CONTINUE
      IC_ALNODO_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNODO_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNODO_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnodo_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>algo_node_for</b> represents a node of a resolution strategy
C  specialized with an execution loop.
C</comment>
      IC_ALNODO_REQCLS = 'algo_node_for'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNODO_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNODO_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnodo_ic.fi'
      INCLUDE 'alnodo.fi'
C-------------------------------------------------------------------------

      IC_ALNODO_REQHDL = AL_NODO_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALNODO_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alnodo_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNODO_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnodo_ic.fi'
      INCLUDE 'alnodo.fi'
      INCLUDE 'alnodo.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnodo_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IVAL
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D        CALL ERR_PRE(AL_NODO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IOB = HOBJ - AL_NODO_HBASE

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ALGO_NODE_DO'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NODO_HALG(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IVAL = AL_NODO_NITER(IOB)
      WRITE(LOG_BUF,'(2A,I12)') 'MSG_NBR_ITERATIONS#<35>#', '= ', IVAL
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NODO_HXPR(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NODO_HLFT(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NODO_HRHT(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      WRITE(LOG_BUF,'(2A,3L)')'MSG_RESET#<35>#', '= ',AL_NODO_LRST(IOB)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_ALNODO_PRN = ERR_TYP()
      RETURN
      END
