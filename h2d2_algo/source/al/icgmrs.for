C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface de Commandes
C Objet:   GMReS
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALGMRS_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALGMRS_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icgmrs.fi'
      INCLUDE 'algmrs.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icgmrs.fc'

      INTEGER IERR
      INTEGER I
      INTEGER HOBJ, HALG
      INTEGER HCRA, HCRC, HGLOB
      INTEGER HPREC
      INTEGER NITER
      INTEGER NRDEM
      INTEGER NPREC
      REAL*8  EPS0
      REAL*8  EPS11
      REAL*8  EPS12
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALGMRS_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      HCRA  = 0
      HCRC  = 0
      HGLOB = 0
      HPREC = 0
      NRDEM = 0
      NITER = 0
      I = 0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = I + 1
C     <comment>Handle on the stopping criterion</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HCRA)
      I = I + 1
!!!C     <comment>Handle on the convergence criterion</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HCRC)
!!!      I = I + 1
C     <comment>Handle on the globalization algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HGLOB)
      I = I + 1
C     <comment>Handle on the preconditioning</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HPREC)
      I = I + 1
C     <comment>Number of preconditioning</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, NPREC)
      I = I + 1
C     <comment>Number of restarts</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, NRDEM)
      I = I + 1
C     <comment>Number of iterations</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, NITER)
      I = I + 1
C     <comment>Convergence criteria:</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, EPS0)
      I = I + 1
C     <comment>   eps0 + (eps11 + eps12*eta)*|u|</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, EPS11)
      I = I + 1
C     <comment></comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, EPS12)
      IF (IERR .NE. 0) GOTO 9901


C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_GMRS_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_GMRS_INI(HALG,
     &                                   HCRA,
     &                                   HCRC,
     &                                   HGLOB,
     &                                   HPREC,
     &                                   NPREC,
     &                                   NRDEM,
     &                                   NITER,
     &                                   EPS0,
     &                                   EPS11,
     &                                   EPS12)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALGMRS_PRN(HALG)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C  <comment>
C  The constructor <b>gmres</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C  </comment>


      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALGMRS_AID()

9999  CONTINUE
      IC_ALGMRS_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALGMRS_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'algmrs.fc'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'icgmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NITER
      INTEGER NRDEM
      INTEGER NPREC
      REAL*8  EPS0
      REAL*8  EPS11
      REAL*8  EPS12
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IOB = HOBJ - AL_GMRS_HBASE

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_ALGORITHME_GMRS'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---      Impression de l'objet
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_SELF#<35>#', '= ',
     &                          NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

C---      Impression des paramètres
      NPREC = AL_GMRS_NPREC(IOB)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_PRECOND#<35>#', '= ',
     &                            NPREC
      CALL LOG_ECRIS(LOG_BUF)
      NRDEM = AL_GMRS_NREDM(IOB)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_REDEMARAGES#<35>#', '= ',
     &                            NRDEM
      CALL LOG_ECRIS(LOG_BUF)
      NITER = AL_GMRS_NITER(IOB)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_ITERATIONS#<35>#', '= ',
     &                            NITER
      CALL LOG_ECRIS(LOG_BUF)
      EPS0 = AL_GMRS_EPS(1,IOB)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON0#<35>#', '= ',
     &                            EPS0
      CALL LOG_ECRIS(LOG_BUF)
      EPS11 = AL_GMRS_EPS(2,IOB)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON11#<35>#', '= ',
     &                            EPS11
      CALL LOG_ECRIS(LOG_BUF)
      EPS12 = AL_GMRS_EPS(3,IOB)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON12#<35>#', '= ',
     &                            EPS12
      CALL LOG_ECRIS(LOG_BUF)


      CALL LOG_DECIND()

      IC_ALGMRS_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALGMRS_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALGMRS_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'icgmrs.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'algmrs.fc'
      INCLUDE 'icgmrs.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
         IOB = HOBJ - AL_GMRS_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the preconditioning</comment>
         IF (PROP .EQ. 'hprec') THEN
            IVAL = AL_GMRS_HPRE(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

C        <comment>Number of preconditioning</comment>
         ELSEIF (PROP .EQ. 'nprec') THEN
            IVAL = AL_GMRS_NPREC(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of restarts</comment>
         ELSEIF (PROP .EQ. 'nredm') THEN
            IVAL = AL_GMRS_NREDM(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of iterations</comment>
         ELSEIF (PROP .EQ. 'niter') THEN
            IVAL = AL_GMRS_NITER(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Absolute epsilon</comment>
         ELSEIF (PROP .EQ. 'eps0') THEN
            RVAL = AL_GMRS_EPS(1,IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C        <comment>Relative epsilon</comment>
         ELSEIF (PROP .EQ. 'eps11') THEN
            RVAL = AL_GMRS_EPS(2,IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL

C        <comment>Relative epsilon</comment>
         ELSEIF (PROP .EQ. 'eps12') THEN
            RVAL = AL_GMRS_EPS(3,IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
         IOB = HOBJ - AL_GMRS_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'hprec') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            IF (.NOT. PR_PREC_CTRLH(IVAL)) GOTO 9901
            AL_GMRS_HPRE(IOB) = IVAL
         ELSEIF (PROP .EQ. 'nprec') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_NPREC(IOB) = IVAL
         ELSEIF (PROP .EQ. 'nredm') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_NREDM(IOB) = IVAL
         ELSEIF (PROP .EQ. 'niter') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_NITER(IOB) = IVAL
         ELSEIF (PROP .EQ. 'eps0') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_EPS(1,IOB) = RVAL
         ELSEIF (PROP .EQ. 'eps11') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_EPS(2,IOB) = RVAL
         ELSEIF (PROP .EQ. 'eps12') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            AL_GMRS_EPS(3,IOB) = RVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQ@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_GMRS_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALGMRS_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALGMRS_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALGMRS_AID()

9999  CONTINUE
      IC_ALGMRS_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALGMRS_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALGMRS_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icgmrs.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>gmres</b> is a system resolution algorithm based on a
C  preconditioned non-linear GMRES method. The algorithm is not distributed
C  and not parallelized.
C</comment>
      IC_ALGMRS_REQCLS = 'gmres'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALGMRS_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALGMRS_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icgmrs.fi'
      INCLUDE 'algmrs.fi'
C-------------------------------------------------------------------------

      IC_ALGMRS_REQHDL = AL_GMRS_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_ALGMRS_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALGMRS_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icgmrs.hlp')

      RETURN
      END

