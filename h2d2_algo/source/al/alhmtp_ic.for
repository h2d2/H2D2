C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C      FUNCTION IC_ALHMTP_XEQCTR ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALHMTP_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALHMTP_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alhmtp_ic.fi'
      INCLUDE 'alhmtp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alhmtp_ic.fc'

      INTEGER IERR, IRET
      INTEGER HOBJ, HALG
      INTEGER HRES, HCTI, HPJC, HMTP
      REAL*8  DELT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALHMTP_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IF (SP_STRN_NTOK(IPRM, ',') .LT. 4) GOTO 9901
C     <comment>Handle on the resolution algorithm</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HRES)
C     <comment>Time step of the algorithm</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', 2, DELT)
C     <comment>Handle on the homotopy (can be 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, HMTP)
C     <comment>Handle on the increment controller</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, HCTI)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Handle on the dof projection (default 0) (Status: Experimental)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 5, HPJC)
      IF (IRET .NE. 0) HPJC = 0

C---     Construis et initialise l'objet
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_HMTP_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_HMTP_INI(HALG,HRES,DELT,HMTP,HCTI,HPJC)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = AL_HMTP_PRN(HALG)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>algo_homotopy</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALHMTP_AID()

9999  CONTINUE
      IC_ALHMTP_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALHMTP_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALHMTP_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alhmtp_ic.fi'
      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alhmtp.fc'
      INCLUDE 'alhmtp_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
         IOB = HOBJ - AL_HMTP_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the load</comment>
         IF (PROP .EQ. 'h_homo') THEN
            IVAL = AL_HMTP_HMTP(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

C        <comment>Handle on the increment controller</comment>
         ELSEIF (PROP .EQ. 'h_cinc') THEN
            IVAL = AL_HMTP_HCTI(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

C        <comment>Handle on the underlying solver</comment>
         ELSEIF (PROP .EQ. 'h_solver') THEN
            IVAL = AL_HMTP_HRES(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_HMTP_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_HMTP_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALHMTP_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALHMTP_AID()

9999  CONTINUE
      IC_ALHMTP_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALHMTP_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALHMTP_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alhmtp_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>homotopy</b> represents an homotopy algorithm that will
C  follow a load path. The load increment is controlled by an increment
C  controller (Status: Experimental).
C</comment>
      IC_ALHMTP_REQCLS = 'homotopy'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALHMTP_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALHMTP_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alhmtp_ic.fi'
      INCLUDE 'alhmtp.fi'
C-------------------------------------------------------------------------

      IC_ALHMTP_REQHDL = AL_HMTP_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALHMTP_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alhmtp_ic.hlp')

      RETURN
      END
