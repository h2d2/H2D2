C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_AL_READ_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_READ_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alread_ic.fi'
      INCLUDE 'alread.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alread_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HALG
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_AL_READ_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9900

C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_READ_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_READ_INI(HALG)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_AL_READ_PRN(HALG)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>read</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_AL_READ_AID()

9999  CONTINUE
      IC_AL_READ_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_READ_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'alread.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alread_ic.fc'

      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_SOLUTION_READER'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', ' = ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_AL_READ_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_AL_READ_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_READ_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alread_ic.fi'
      INCLUDE 'alread.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alread_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      IF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_READ_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_AL_READ_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_AL_READ_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_AL_READ_AID()

9999  CONTINUE
      IC_AL_READ_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_READ_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_READ_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alread_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>solution_reader</b> gives access to an already existing solution.
C  It acts as an algorithm advancing the solution in time by reading the
C  values from file.<p>
C  It allows to separate the computing phase from the post-processing.
C</comment>
      IC_AL_READ_REQCLS = 'solution_reader'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_AL_READ_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_AL_READ_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alread_ic.fi'
      INCLUDE 'alread.fi'
C-------------------------------------------------------------------------

      IC_AL_READ_REQHDL = AL_READ_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_AL_READ_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alread_ic.hlp')
      RETURN
      END

