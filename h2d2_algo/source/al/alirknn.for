!todo
!   newton avec matrice existante
!   solution initiale

C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   esdIRK générique
C Type:    Virtuel
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_IRKNN
C         SUBROUTINE AL_IRKNN_000
C         SUBROUTINE AL_IRKNN_999
C         SUBROUTINE AL_IRKNN_CTR
C         SUBROUTINE AL_IRKNN_DTR
C         SUBROUTINE AL_IRKNN_INI
C         SUBROUTINE AL_IRKNN_RST
C         SUBROUTINE AL_IRKNN_REQHBASE
C         SUBROUTINE AL_IRKNN_HVALIDE
C         SUBROUTINE AL_IRKNN_RESOUS
C         SUBROUTINE AL_IRKNN_RESOUS_E
C         SUBROUTINE AL_IRKNN_UN_PAS
C         SUBROUTINE AL_IRKNN_UN_STG
C         SUBROUTINE AL_IRKNN_REQSTATUS
C         SUBROUTINE AL_IRKNN_ASMMDTK
C         SUBROUTINE AL_IRKNN_ASMMDTKT
C         SUBROUTINE AL_IRKNN_ASMMDTKU
C         SUBROUTINE AL_IRKNN_ASMF_T0
C         SUBROUTINE AL_IRKNN_ASMF_T1
C         SUBROUTINE AL_IRKNN_ASMF
C         SUBROUTINE AL_IRKNN_ASMRES
C         SUBROUTINE AL_IRKNN_ASGDELT
C         SUBROUTINE AL_IRKNN_REQDELT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_IRKNN_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirknn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_IRKNN_NOBJMAX,
     &                   AL_IRKNN_HBASE,
     &                   'Generic ESDIRK Algorithm')

      AL_IRKNN_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirknn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

      INTEGER  IERR
      EXTERNAL AL_IRKNN_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_IRKNN_NOBJMAX,
     &                   AL_IRKNN_HBASE,
     &                   AL_IRKNN_DTR)

      AL_IRKNN_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_IRKNN_NOBJMAX,
     &                   AL_IRKNN_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_IRKNN_HVALIDE(HOBJ))
         IOB = HOBJ - AL_IRKNN_HBASE

         AL_IRKNN_DTAL(IOB) = 0.0D0
         AL_IRKNN_DTEF(IOB) = 0.0D0
         AL_IRKNN_DTST(IOB) = 0.0D0
         AL_IRKNN_NSTG(IOB) = 0
         AL_IRKNN_ISTG(IOB) = 0
         AL_IRKNN_HCTI(IOB) = 0
         AL_IRKNN_HRES(IOB) = 0
         AL_IRKNN_HSIM(IOB) = 0
         AL_IRKNN_HF_A(IOB) = 0
         AL_IRKNN_HF_B(IOB) = 0
         AL_IRKNN_HF_C(IOB) = 0
         AL_IRKNN_HF_D(IOB) = 0
         AL_IRKNN_HF_Z(IOB) = 0
         AL_IRKNN_NDLL(IOB) = 0
         AL_IRKNN_NEQL(IOB) = 0
         AL_IRKNN_LRES(IOB) = 0
         AL_IRKNN_LFT0(IOB) = 0
         AL_IRKNN_LFT (IOB) = 0
         AL_IRKNN_LTMP(IOB) = 0
         AL_IRKNN_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_IRKNN_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_IRKNN_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_IRKNN_NOBJMAX,
     &                   AL_IRKNN_HBASE)

      AL_IRKNN_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_INI(HOBJ,
     &                      HCTI,
     &                      HRES,
     &                      DELT,
     &                      NSTG,
     &                      HF_A,
     &                      HF_B,
     &                      HF_C,
     &                      HF_D,
     &                      HF_Z)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCTI
      INTEGER HRES
      REAL*8  DELT
      INTEGER NSTG
      INTEGER HF_A
      INTEGER HF_B
      INTEGER HF_C
      INTEGER HF_D
      INTEGER HF_Z

      INCLUDE 'alirknn.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IOB
      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (.NOT. (HCTI .EQ. 0 .OR. CI_CINC_HVALIDE(HCTI))) GOTO 9900
      IF (.NOT. AL_SLVR_HVALIDE(HRES)) GOTO 9901
      IF (DELT .LE. 0.0D0) GOTO 9902
      IF (.NOT. SO_FUNC_HVALIDE(HF_A)) GOTO 9903
      IF (.NOT. SO_FUNC_HVALIDE(HF_B)) GOTO 9903
      IF (.NOT. SO_FUNC_HVALIDE(HF_C)) GOTO 9903
      IF (.NOT. SO_FUNC_HVALIDE(HF_D)) GOTO 9903
      IF (.NOT. SO_FUNC_HVALIDE(HF_Z)) GOTO 9903

C---     RESET LES DONNEES
      IERR = AL_IRKNN_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_IRKNN_HBASE
      AL_IRKNN_DTAL(IOB) = DELT
      AL_IRKNN_DTEF(IOB) = 0.0D0
      AL_IRKNN_DTST(IOB) = 0.0D0
      AL_IRKNN_NSTG(IOB) = NSTG
      AL_IRKNN_ISTG(IOB) = 0
      AL_IRKNN_HCTI(IOB) = HCTI
      AL_IRKNN_HRES(IOB) = HRES
      AL_IRKNN_HSIM(IOB) = 0
      AL_IRKNN_HF_A(IOB) = HF_A
      AL_IRKNN_HF_B(IOB) = HF_B
      AL_IRKNN_HF_C(IOB) = HF_C
      AL_IRKNN_HF_D(IOB) = HF_D
      AL_IRKNN_HF_Z(IOB) = HF_Z
      AL_IRKNN_NDLL(IOB) = 0
      AL_IRKNN_NEQL(IOB) = 0
      AL_IRKNN_LRES(IOB) = 0
      AL_IRKNN_LFT0(IOB) = 0
      AL_IRKNN_LFT (IOB) = 0
      AL_IRKNN_LTMP(IOB) = 0
      AL_IRKNN_STTS(IOB) = AL_STTS_INDEFINI

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCTI
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_CTRL_INCREMENT')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_SOLVER')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HF_A
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_FUNCTION')
      GOTO 9999

9999  CONTINUE
      AL_IRKNN_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LFT0, LFT, LTMP, LRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_IRKNN_HBASE
      LRES = AL_IRKNN_LRES(IOB)
      LFT0 = AL_IRKNN_LFT0(IOB)
      LFT  = AL_IRKNN_LFT (IOB)
      LTMP = AL_IRKNN_LTMP(IOB)

C---     Récupère la mémoire
      IF (SO_ALLC_HEXIST(LTMP)) IERR = SO_ALLC_ALLRE8(0, LTMP)
      IF (SO_ALLC_HEXIST(LFT )) IERR = SO_ALLC_ALLRE8(0, LFT )
      IF (SO_ALLC_HEXIST(LFT0)) IERR = SO_ALLC_ALLRE8(0, LFT0)
      IF (SO_ALLC_HEXIST(LRES)) IERR = SO_ALLC_ALLRE8(0, LRES)

C---     Reset
      AL_IRKNN_DTAL(IOB) = 0.0D0
      AL_IRKNN_DTEF(IOB) = 0.0D0
      AL_IRKNN_DTST(IOB) = 0.0D0
      AL_IRKNN_NSTG(IOB) = 0
      AL_IRKNN_ISTG(IOB) = 0
      AL_IRKNN_HCTI(IOB) = 0
      AL_IRKNN_HRES(IOB) = 0
      AL_IRKNN_HSIM(IOB) = 0
      AL_IRKNN_HF_A(IOB) = 0
      AL_IRKNN_HF_B(IOB) = 0
      AL_IRKNN_HF_C(IOB) = 0
      AL_IRKNN_HF_D(IOB) = 0
      AL_IRKNN_HF_Z(IOB) = 0
      AL_IRKNN_NDLL(IOB) = 0
      AL_IRKNN_NEQL(IOB) = 0
      AL_IRKNN_LRES(IOB) = 0
      AL_IRKNN_LFT0(IOB) = 0
      AL_IRKNN_LFT (IOB) = 0
      AL_IRKNN_LTMP(IOB) = 0
      AL_IRKNN_STTS(IOB) = AL_STTS_INDEFINI

      AL_IRKNN_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_IRKNN_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alirknn.fi'
      INCLUDE 'alirknn.fc'
C------------------------------------------------------------------------

      AL_IRKNN_REQHBASE = AL_IRKNN_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_IRKNN_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alirknn.fc'
C------------------------------------------------------------------------

      AL_IRKNN_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_IRKNN_NOBJMAX,
     &                                  AL_IRKNN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_IRKNN_RESOUS intègre dans le temps par une méthode ESDIRK
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des ddl. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_IRKNN_RESOUS(HOBJ,
     &                         TSIM,
     &                         DELT,
     &                         HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR

      EXTERNAL AL_IRKNN_ASMMDTK
      EXTERNAL AL_IRKNN_ASMMDTKT
      EXTERNAL AL_IRKNN_ASMMDTKU
      EXTERNAL AL_IRKNN_ASMF
      EXTERNAL AL_IRKNN_ASMRES
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)    ! cf. note
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_IRKNN_RESOUS_E(HOBJ,
     &                            TSIM,
     &                            DELT,
     &                            HSIM,
     &                            HOBJ,
     &                            AL_IRKNN_ASMMDTK,
     &                            AL_IRKNN_ASMMDTKT,
     &                            AL_IRKNN_ASMMDTKU,
     &                            AL_IRKNN_ASMF,
     &                            AL_IRKNN_ASMRES)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_IRKNN_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_IRKNN_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme ESDIRK.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo. C'est la fonction appelante qui dois gérer le
C     chargement des données.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + a.K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + a.KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + a.K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C************************************************************************
      FUNCTION AL_IRKNN_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HALG,
     &                           F_MDTK,
     &                           F_MDTKT,
     &                           F_MDTKU,
     &                           F_F,
     &                           F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alirknn.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'alirknn.fc'

      REAL*8  DTNM, DTPRV, DTNOW, DTNXT, DTRST, TINI, TFIN
      REAL*8  TS_C, DT_C
      REAL*8  VTOL(1)

      INTEGER IERR
      INTEGER IOB
      INTEGER IPAS, ISTG, ISTC
      INTEGER HCTI
      INTEGER HNMR
      INTEGER HSVPT
      INTEGER NEQL, NDLN, NNL, NDLL
      INTEGER NSTG
      INTEGER LRES, LFT0, LFT, LTMP
      INTEGER LDLG, LSPT, LLOCN
      LOGICAL ACTI, ESTOK, ESTRES, ESTRJT, ESTXIT, PUSHT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.esdirk'

C---     Récupère les attributs
      IOB = HOBJ - AL_IRKNN_HBASE
      NSTG = AL_IRKNN_NSTG(IOB)
      HCTI = AL_IRKNN_HCTI(IOB)
      LRES = AL_IRKNN_LRES(IOB)
      LFT0 = AL_IRKNN_LFT0(IOB)
      LFT  = AL_IRKNN_LFT (IOB)
      LTMP = AL_IRKNN_LTMP(IOB)
D     CALL ERR_ASR(LRES .EQ. 0 .OR. SO_ALLC_HEXIST(LRES))
D     CALL ERR_ASR(LFT0 .EQ. 0 .OR. SO_ALLC_HEXIST(LFT0))
D     CALL ERR_ASR(LFT  .EQ. 0 .OR. SO_ALLC_HEXIST(LFT))
D     CALL ERR_ASR(LTMP .EQ. 0 .OR. SO_ALLC_HEXIST(LTMP))

C---     Récupère les paramètres de la simulation
      HNMR = LM_ELEM_REQHNUMC(HSIM)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      NEQL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NNL  = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)

C---     Présence/absence des composantes
      ACTI = CI_CINC_HVALIDE(HCTI)

C---     Calcule les paramètres
      DTNM  = AL_IRKNN_DTAL(IOB)
      IF (HSIM .EQ. AL_IRKNN_HSIM(IOB)) THEN
         DTNXT = MIN(AL_IRKNN_DTEF(IOB), DELT)
      ELSE
         DTNXT = MIN(AL_IRKNN_DTAL(IOB), DELT)
      ENDIF
      DTNOW = DTNXT
      TINI = TSIM
      TFIN = TSIM + DELT

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_ESDIRK_SOLVE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', '= ', TFIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DTNM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     (Ré)-alloue les tables de travail
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NEQL*NSTG, LRES)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LFT0) ! Utilisées en
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LFT)  ! en NEQ et en
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTMP) ! NDLL

C---     Conserve les paramètres pour la simulation
      AL_IRKNN_HSIM(IOB) = HSIM
      AL_IRKNN_NDLL(IOB) = NDLL
      AL_IRKNN_NEQL(IOB) = NEQL
      AL_IRKNN_LRES(IOB) = LRES
      AL_IRKNN_LFT0(IOB) = LFT0
      AL_IRKNN_LFT (IOB) = LFT
      AL_IRKNN_LTMP(IOB) = LTMP

C---     Crée un point de sauvegarde
      HSVPT = 0
      LSPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)
      IF (ERR_GOOD()) LSPT = SO_SVPT_GET(HSVPT)

C---     Initialise le contrôleur d'incrément
      IF (ERR_GOOD() .AND. ACTI)
     &   IERR = CI_CINC_DEB(HCTI, TINI, TFIN, DTNOW)


C        --------------------------------
C---     Boucle sur les sous-pas de temps
C        --------------------------------
      IPAS = 0
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (DTNOW .LE. 0.0D0) GOTO 199
         IF (TINI  .GE. TFIN)  GOTO 199

C---        Log
         IPAS = IPAS + 1
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6,3(A,F20.6))',ERR=109)
     &      'MSG_PAS_DE_TEMPS:', IPAS,
     &      ', MSG_TINI=', TINI,
     &      ', MSG_TFIN=', TINI+DTNOW,
     &      ', MSG_DELT=', DTNOW
109      CONTINUE
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Traceur
         IERR = TRA_ASGTALG(TINI)

C---        Sauve l'état courant
         IF (ERR_GOOD() .AND. HSVPT .NE. 0) THEN
            IERR = SO_SVPT_SAVE(HSVPT)
         ENDIF

C---        Résous
         TS_C = TINI
         DT_C = DTNOW
         IF (ERR_GOOD()) THEN
            IERR = AL_IRKNN_UN_PAS(HOBJ,
     &                             TS_C,
     &                             DT_C,
     &                             HSIM,
     &                             VA(SO_ALLC_REQVIND(VA,LSPT)),
     &                             HALG,
     &                             F_MDTK,
     &                             F_MDTKT,
     &                             F_MDTKU,
     &                             F_F,
     &                             F_RES)
         ENDIF
         IF (ERR_BAD()) GOTO 199
         CALL LOG_INFO(LOG_ZNE, ' ')

C---        Delta sur le pas de temps dans LFT0
         CALL DCOPY(NDLL,
     &              VA(SO_ALLC_REQVIND(VA,LDLG)), 1,
     &              VA(SO_ALLC_REQVIND(VA,LFT0)), 1)
         CALL DAXPY(NDLL,
     &              -1.0D0,
     &               VA(SO_ALLC_REQVIND(VA,LSPT)), 1,
     &               VA(SO_ALLC_REQVIND(VA,LFT0)), 1)
         IERR = SP_ELEM_CLIM2VAL(NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           0.0D0,
     &                           VA(SO_ALLC_REQVIND(VA, LFT0)))

C---        L'état de résolution
         ISTG = AL_IRKNN_REQSTATUS(HOBJ)

C---        Adapte le pas de temps
         DTPRV = DTNOW
         IF (ACTI) THEN
D           CALL ERR_ASR(SO_ALLC_HEXIST(LSPT))
            ESTOK = (ISTG .EQ. AL_STTS_OK)
            IERR = CI_CINC_CLCDEL(HCTI,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                            VA(SO_ALLC_REQVIND(VA,LFT0)), ! delU
     &                            ESTOK)
            IERR = CI_CINC_REQDELT(HCTI, ISTC, DTNXT, DTNOW)
            ISTG = AL_UTIL_CLRSTTS(ISTG)
            ISTG = AL_UTIL_IORSTTS(ISTG, ISTC)
         ELSE
            DTRST = TFIN - (TINI+DTPRV)
            DTNOW = MIN(DTPRV, DTRST)
            DTRST = DTRST - DTNOW
            IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTPRV)) THEN
               DTNOW = DTNOW + DTRST
            ENDIF
         ENDIF

C---        L'état de résolution
         ESTOK  = (ISTG .EQ. AL_STTS_OK)
         ESTRES = .NOT. BTEST(ISTG, AL_STTS_CRIA_BAD)
         ESTRJT = .FALSE.
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_COUPE)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)
         ESTXIT = .FALSE.
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)

C---        Logique de continuation
         PUSHT = .FALSE.
         IF (ESTRES) THEN
            PUSHT = .TRUE.
D           CALL ERR_ASR(ERR_BAD() .OR. DT_C .EQ. 0.0D0)
         ELSE
            IF (ACTI) THEN                ! !OK, Avec contrôleur d'incrément
               IERR = SO_SVPT_RBCK(HSVPT) !  rollback. On pert possiblement
               PUSHT = .FALSE.            !  une partie déjà convergée
            ELSE
               PUSHT = .TRUE.             ! !OK, Sans contrôleur d'incrément
            ENDIF                         !   continue
         ENDIF
         IF (PUSHT) TINI = TINI + DTPRV

C---        Log l'incrément sur le pas de temps
         IF (ERR_GOOD()) THEN
            IERR = AL_UTIL_LOGSOL(NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LFT0)),
     &                            HNMR)
         ENDIF

C---        Log l'état
         IF (ERR_GOOD()) THEN
            IF (ACTI) THEN
               IF (BTEST(ISTG, AL_STTS_CINC_DTMIN))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_DTMIN')
               IF (BTEST(ISTG, AL_STTS_CINC_SKIP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_SKIP')
               IF (BTEST(ISTG, AL_STTS_CINC_STOP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_STOP')
               IF (.NOT. ESTRES)
     &         CALL LOG_INFO(LOG_ZNE, 'MSG_PAS_REJETE')
            ELSE
               IF (.NOT. ESTRES) THEN
                  CALL LOG_WRN(LOG_ZNE, 'MSG_PAS_NON_CONVERGE')
                  CALL LOG_WRN(LOG_ZNE, 'MSG_CONTINUE')
               ENDIF
            ENDIF
         ENDIF

         CALL LOG_DECIND()
         IF (ESTXIT) GOTO 199
      GOTO 100 ! end while
199   CONTINUE

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         DELT = (TSIM+DELT) - TINI
         TSIM = TINI
      ENDIF

C---     Conserve la prévision suite au dernier pas de temps
      IF (ERR_GOOD()) THEN
         AL_IRKNN_DTEF(IOB) = DTNXT
      ENDIF

C---     Conserve l'état
      AL_IRKNN_STTS(IOB) = ISTG

C---     Relâche le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = SO_SVPT_DTR(HSVPT)
         CALL ERR_POP()
      ENDIF

      CALL LOG_DECIND()
      AL_IRKNN_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_IRKNN_UN_PAS intègre dans le temps par une méthode ESDIRK
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HCRA         Handle sur le critère d'arrêt
C     INTEGER HLTR         Handle sur le limiteur
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des ddl. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_IRKNN_UN_PAS(HOBJ,
     &                         TSIM,
     &                         DTEF,
     &                         HSIM,
     &                         VDLG0,
     &                         HALG,
     &                         F_MDTK,
     &                         F_MDTKT,
     &                         F_MDTKU,
     &                         F_F,
     &                         F_RES)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DTEF
      INTEGER  HSIM
      REAL*8   VDLG0(*)
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IS, IR, IOFF
      INTEGER HF_A, HF_D, HF_Z
      INTEGER LRES, LFT, LTMP, LDLG, LLOCN
      INTEGER NSTG
      INTEGER NEQL, NDLL
      REAL*8  DTST, GAMMA, D
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_IRKNN_HBASE
      NSTG = AL_IRKNN_NSTG(IOB)
      HF_A = AL_IRKNN_HF_A(IOB)
      HF_D = AL_IRKNN_HF_D(IOB)
      HF_Z = AL_IRKNN_HF_Z(IOB)
      NEQL = AL_IRKNN_NEQL(IOB)
      NDLL = AL_IRKNN_NDLL(IOB)
      LRES = AL_IRKNN_LRES(IOB)
      LFT  = AL_IRKNN_LFT (IOB)
      LTMP = AL_IRKNN_LTMP(IOB)
D     CALL ERR_ASR(LRES .EQ. 0 .OR. SO_ALLC_HEXIST(LRES))

C---     Récupère les param de la simulation
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Pas de temps du stage
      IERR = SO_FUNC_CALL3(HF_A,
     &                     SO_ALLC_CST2B(NSTG),
     &                     SO_ALLC_CST2B(NSTG),
     &                     SO_ALLC_CST2B(GAMMA))
      DTST = DTEF * GAMMA

C---     Conserve les paramètres pour la simu
      AL_IRKNN_DTEF(IOB) = DTEF
      AL_IRKNN_DTST(IOB) = DTST

C---     Charge les données et pre-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     Partie de F à T
      IF (ERR_GOOD()) IERR = AL_IRKNN_ASMF_T0(HOBJ)

C---     Résidu initial
      IERR = LM_ELEM_ASMFKU(HSIM, VA(SO_ALLC_REQVIND(VA,LRES)))

C        --------------------
C---     Itère sur les stages
C        --------------------
      DO IS=2,AL_IRKNN_NSTG(IOB)
         AL_IRKNN_ISTG(IOB) = IS

!!!C---        Solution initiale
!!!         IF (ERR_GOOD()) THEN
!!!            IR = IS - 1
!!!            IERR = SO_FUNC_CALL6(HF_Z,
!!!     &                           SO_ALLC_CST2B(DTEF),
!!!     &                           SO_ALLC_CST2B(GAMMA),
!!!     &                           SO_ALLC_CST2B(NEQ),
!!!     &                           SO_ALLC_CST2B(IR),
!!!     &                           SO_ALLC_CST2B(LRES,0_2),
!!!     &                           SO_ALLC_CST2B(LTMP,0_2))  ! dU
!!!         ENDIF
!!!         IF (ERR_GOOD()) THEN
!!!            CALL DINIT(NDLL, 0.0D0, VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
!!!            IERR = SP_ELEM_NEQ2NDLT(NEQ,
!!!     &                              NDLL,
!!!     &                              KA(SO_ALLC_REQKIND(KA, LLOCN)),
!!!     &                              VA(SO_ALLC_REQVIND(VA, LTMP)),
!!!     &                              VA(SO_ALLC_REQVIND(VA, LDLG)))
!!!         ENDIF
!!!         IF (ERR_GOOD()) THEN
!!!            CALL DAXPY(NDLL, 1.0D0,
!!!     &                 VDLG0, 1,
!!!     &                 VA(SO_ALLC_REQVIND(VA, LDLG)),  1)
!!!         ENDIF

C---        Résolution
         IF (ERR_GOOD()) THEN
            IERR = AL_IRKNN_UN_STG(HOBJ,
     &                             TSIM,
     &                             DTST,
     &                             HSIM,
     &                             HALG,
     &                             F_MDTK,
     &                             F_MDTKT,
     &                             F_MDTKU,
     &                             F_F,
     &                             F_RES)
         ENDIF

C---        Calcul du résidu
         IF (ERR_GOOD()) THEN
            IOFF=(IS-1)*NEQL
            IERR=LM_ELEM_ASMFKU(HSIM,VA(SO_ALLC_REQVIND(VA,LRES)+IOFF))
         ENDIF
      ENDDO

C---     Calcul de l'erreur dans LTMP
      CALL DINIT(NEQL, 0.0D0, VA(SO_ALLC_REQVIND(VA,LTMP)), 1)
      DO IS=1,AL_IRKNN_NSTG(IOB)
         IERR = SO_FUNC_CALL2(HF_D,
     &                        SO_ALLC_CST2B(IS),
     &                        SO_ALLC_CST2B(D))
         IOFF = (IS-1)*NEQL     ! Offset du résidu
         CALL DAXPY(NEQL,
     &              DTEF*D,
     &              VA(SO_ALLC_REQVIND(VA,LRES)+IOFF), 1,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)), 1)
      ENDDO

C---     Passe de NEQ à NDLL dans LFT
      IF (ERR_GOOD()) THEN
         CALL DINIT(NDLL, 0.0D0, VA(SO_ALLC_REQVIND(VA,LFT)), 1)
         IERR = SP_ELEM_NEQ2NDLT(NEQL,
     &                           NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           VA(SO_ALLC_REQVIND(VA, LTMP)),
     &                           VA(SO_ALLC_REQVIND(VA, LFT)))
      ENDIF

      AL_IRKNN_UN_PAS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée AL_IRKNN_UN_STG intègre un "stage".
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM        Handle sur l'élément
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C    ajustées à la dernière valeur des ddl. En fait elles sont une
C    itération en retard.
C************************************************************************
      FUNCTION AL_IRKNN_UN_STG(HOBJ,
     &                         TSIM,
     &                         DTST,
     &                         HSIM,
     &                         HALG,
     &                         F_MDTK,
     &                         F_MDTKT,
     &                         F_MDTKU,
     &                         F_F,
     &                         F_RES)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DTST
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alirknn.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISTG
      INTEGER HRES, HF_C
      REAL*8  C, DTEF, DT_C
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_IRKNN_HBASE
      DTEF = AL_IRKNN_DTEF(IOB)
      HRES = AL_IRKNN_HRES(IOB)
      HF_C = AL_IRKNN_HF_C(IOB)
      ISTG = AL_IRKNN_ISTG(IOB)

C---     Pas de temps des données
      IERR = SO_FUNC_CALL2(HF_C,
     &                     SO_ALLC_CST2B(ISTG),
     &                     SO_ALLC_CST2B(C))
      DT_C = DTEF * C

C---     Charge les données à T+DT et pre-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM+DT_C)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC(HSIM)

C---     Partie de F à T+dT
      IF (ERR_GOOD()) IERR = AL_IRKNN_ASMF_T1(HOBJ)

C---     Résous
      IF (ERR_GOOD()) THEN
         IERR = AL_SLVR_RESOUS_E(HRES,
     &                           TSIM,
     &                           DTST,
     &                           HSIM,
     &                           HALG,
     &                           F_MDTK,
     &                           F_MDTKT,
     &                           F_MDTKU,
     &                           F_F,
     &                           F_RES)
      ENDIF

C---     Conserve le résultat
      IF (ERR_GOOD()) THEN
      AL_IRKNN_STTS(IOB) = AL_SLVR_REQSTATUS(HRES)
      ENDIF

      AL_IRKNN_UN_STG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_IRKNN_REQSTATUS = AL_IRKNN_STTS(HOBJ - AL_IRKNN_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMMDTK(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'mxmtrx.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      REAL*8  DTST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_IRKNN_HBASE
      DTST = AL_IRKNN_DTST(IOB)
      HSIM = AL_IRKNN_HSIM(IOB)

C---     ASSEMBLE K
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMK  (HSIM, HMTX, F_ASM)

C---     GAMMA*DELT * K
      IF (ERR_GOOD()) IERR = MX_MTRX_MULVAL(HMTX, DTST)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMM  (HSIM, HMTX, F_ASM)

      AL_IRKNN_ASMMDTK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMMDTKT(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'mxmtrx.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      REAL*8  DTST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_IRKNN_HBASE
      DTST = AL_IRKNN_DTST(IOB)
      HSIM = AL_IRKNN_HSIM(IOB)

C---     ASSEMBLE Kt
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKT (HSIM, HMTX, F_ASM)

C---     GAMMA*DELT * Kt
      IF (ERR_GOOD()) IERR = MX_MTRX_MULVAL(HMTX, DTST)

C---     ASSEMBLE M
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMM  (HSIM, HMTX, F_ASM)

      AL_IRKNN_ASMMDTKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMMDTKU(HOBJ, VDLG, VRES)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VDLG(*)
      REAL*8  VRES(*)

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

C      INTEGER IERR
      INTEGER IOB
      INTEGER HRES
C      INTEGER HGRD
      REAL*8  DELT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - AL_IRKNN_HBASE
      HRES = AL_IRKNN_HRES(IOB)
      DELT = AL_IRKNN_DTEF(IOB)
C      HGRD = LM_ELEM_REQHGRID(HSOL)

C      IERR = LM_ELEM_ASMBL_MKU()

      WRITE(6,*) '--------------------------------------------'
      WRITE(6,*) '---  Fonction AL_IRKNN_ASMMDTKU non implantee'
      WRITE(6,*) '--------------------------------------------'

      AL_IRKNN_ASMMDTKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: AL_IRKNN_ASMF_T0
C
C Description:
C     La méthode privée AL_IRKNN_ASMF_T0 assemble la partie à T
C     du membre de droite.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMF_T0(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LFT0
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_IRKNN_HBASE
      HSIM = AL_IRKNN_HSIM(IOB)
      LFT0 = AL_IRKNN_LFT0(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LFT0))

C---     Assemble MU
      IF (ERR_GOOD())
     &   IERR = LM_ELEM_ASMMU(HSIM,
     &                        VA(SO_ALLC_REQVIND(VA,LFT0)))

      AL_IRKNN_ASMF_T0 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: AL_IRKNN_ASMF_T1
C
C Description:
C     La méthode privée AL_IRKNN_ASMF_T1 ajoute à la partie précalculée
C     par AL_IRKNN_ASMF_T0 la partie à T+DT.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMF_T1(HOBJ)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IS, ISTG, IOFF
      INTEGER HSIM, HF_A
      INTEGER LRES, LFT0, LFT
      INTEGER NEQL
      REAL*8  A, DTEF, DTST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_IRKNN_HBASE
      DTEF = AL_IRKNN_DTEF(IOB)
      DTST = AL_IRKNN_DTST(IOB)
      HSIM = AL_IRKNN_HSIM(IOB)
      HF_A = AL_IRKNN_HF_A(IOB)
      NEQL = AL_IRKNN_NEQL(IOB)
      LRES = AL_IRKNN_LRES(IOB)
      LFT0 = AL_IRKNN_LFT0(IOB)
      LFT  = AL_IRKNN_LFT (IOB)
      ISTG = AL_IRKNN_ISTG(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LFT0))

C---     Assemble DTEF*GAMMA*F(t+dt)
      IERR = LM_ELEM_ASMF(HSIM, VA(SO_ALLC_REQVIND(VA,LFT)))
      CALL DSCAL(NEQL, DTST, VA(SO_ALLC_REQVIND(VA,LFT)), 1)

C---     Assemble la somme des résidus
      DO IS=1,ISTG-1
         IERR = SO_FUNC_CALL3(HF_A,
     &                        SO_ALLC_CST2B(ISTG),
     &                        SO_ALLC_CST2B(IS),
     &                        SO_ALLC_CST2B(A))
         IOFF = (IS-1)*NEQL     ! Offset du résidu
         CALL DAXPY(NEQL,
     &              DTEF*A,
     &              VA(SO_ALLC_REQVIND(VA,LRES)+IOFF), 1,
     &              VA(SO_ALLC_REQVIND(VA,LFT)), 1)
      ENDDO

C---     La partie à T0
      CALL DAXPY(NEQL,
     &           1.0D0,
     &           VA(SO_ALLC_REQVIND(VA,LFT0)), 1,
     &           VA(SO_ALLC_REQVIND(VA,LFT)),  1)

      AL_IRKNN_ASMF_T1 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMF(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LFT
      INTEGER NEQL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
D     CALL ERR_PRE(.NOT. ADLG)
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_IRKNN_HBASE
      NEQL = AL_IRKNN_NEQL(IOB)
      LFT  = AL_IRKNN_LFT (IOB)

C---     TRANSFERT
      CALL DCOPY(NEQL, VA(SO_ALLC_REQVIND(VA,LFT)), 1, VRES, 1)

      AL_IRKNN_ASMF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_IRKNN_ASMRES(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LTMP
      INTEGER NEQL
      REAL*8  DUMMY(1)
      REAL*8  DTST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_IRKNN_HBASE
      DTST = AL_IRKNN_DTST(IOB)
      HSIM = AL_IRKNN_HSIM(IOB)
      NEQL = AL_IRKNN_NEQL(IOB)
      LTMP = AL_IRKNN_LTMP(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LTMP))

C---     Assigne VDLG à la simulation
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     F = DELT*A*F(T+dT) + DELT*(1-A)*(F(T) - K.U(T)) + M.U(T)
      IF (ERR_GOOD()) THEN
         IERR = AL_IRKNN_ASMF(HOBJ, VRES, DUMMY, .FALSE.) ! Pas de VDLG passé
      ENDIF

C---     ASSEMBLE M.U(T+dT)
      IF (ERR_GOOD()) THEN
         IERR = LM_ELEM_ASMMU(HSIM, VA(SO_ALLC_REQVIND(VA,LTMP)))
      ENDIF

C---     F - M.U(T+dT)
      IF (ERR_GOOD()) THEN
         CALL DAXPY(NEQL,
     &              -1.0D0,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)),
     &              1,
     &              VRES,
     &              1)
      ENDIF

C---     ASSEMBLE KU(T+dT)
      IF (ERR_GOOD()) THEN
         IERR = LM_ELEM_ASMKU(HSIM, VA(SO_ALLC_REQVIND(VA,LTMP)))
      ENDIF

C---     F - MU(T+dT) - DELT*GAMMA*KU(T+dT)
      IF (ERR_GOOD()) THEN
         CALL DAXPY(NEQL,
     &              -DTST,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)),
     &              1,
     &              VRES,
     &              1)
      ENDIF

C---     RÉTABLIS LA SIMULATION
      IF (ADLG) THEN
         CALL ERR_PUSH()
         IERR = LM_ELEM_POPLDLG(HSIM)
         CALL ERR_POP()
      ENDIF

      AL_IRKNN_ASMRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_ASGDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_ASGDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (DELT .LE. 0.0D0) GOTO 9900

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_IRKNN_HBASE
      AL_IRKNN_DTAL(IOB) = DELT

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_IRKNN_ASGDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_IRKNN_REQDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_IRKNN_REQDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alirknn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alirknn.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_IRKNN_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DELT = AL_IRKNN_DTAL(HOBJ-AL_IRKNN_HBASE)

      AL_IRKNN_REQDELT = ERR_TYP()
      RETURN
      END
