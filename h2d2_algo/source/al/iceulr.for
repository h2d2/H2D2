C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: IC
C      H2D2 Class: IC_ALEULR
C         INTEGER IC_ALEULR_XEQCTR
C         INTEGER IC_ALEULR_PRN
C         INTEGER IC_ALEULR_XEQMTH
C         CHARACTER*(32) IC_ALEULR_REQCLS
C         INTEGER IC_ALEULR_REQHDL
C         SUBROUTINE IC_ALEULR_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALEULR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALEULR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'iceulr.fi'
      INCLUDE 'aleulr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'iceulr.fc'

      INTEGER IERR, IRET
      INTEGER HOBJ, HALG
      INTEGER HRES, HCTI, HPJC
      REAL*8  DELT, ALFA
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALEULR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      HRES = 0
      DELT = 0.0D0
      ALFA = 0.0D0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the resolution algorithm</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HRES)
C     <comment>Time step of the algorithm</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', 2, DELT)
C     <comment>Alpha factor (a=0 explicit, a=1 implicit, a=0.5 Crank-Nicholson)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', 3, ALFA)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Handle on the increment controller (default 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, HCTI)
      IF (IRET .NE. 0) HCTI = 0
C     <comment>Handle on the projection (default 0) (Status: Experimental)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 5, HPJC)
      IF (IRET .NE. 0) HPJC = 0

C---     Construis et initialise l'objet
      HALG = 0
      IF (ERR_GOOD()) IERR = AL_EULR_CTR(HALG)
      IF (ERR_GOOD()) IERR = AL_EULR_INI(HALG,HRES,DELT,ALFA,HCTI,HPJC)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HALG)

C---     Imprime l'objet
      IF (ERR_GOOD()) THEN
         IERR = IC_ALEULR_PRN(HALG)
      ENDIF

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HALG
      ENDIF

C<comment>
C  The constructor <b>euler</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALEULR_AID()

9999  CONTINUE
      IC_ALEULR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALEULR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'aleulr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'aleulr.fc'
      INCLUDE 'iceulr.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LTXT
      REAL*8  ALFA
      REAL*8  DELT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_ALGORITHME_EULER'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - AL_EULR_HBASE

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_EULR_HRES(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_RESO_MATRICIEL#<35>#', '= ',TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      DELT = AL_EULR_DTAL(IOB)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_PAS_DE_TEMPS#<35>#= ', DELT
      CALL LOG_ECRIS(LOG_BUF)
      ALFA = AL_EULR_ALFA(IOB)
      WRITE(LOG_BUF,'(A,F4.1)') 'MSG_EULER_ALFA#<35>#= ', ALFA
      CALL LOG_ECRIS(LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_EULR_HCTI(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_CTRL_INCREMENT#<35>#', '= ',TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)
      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_EULR_HPJC(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_PROJECTION_DDL#<35>#', '= ',TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)


      CALL LOG_DECIND()

      IC_ALEULR_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALEULR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALEULR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'iceulr.fi'
      INCLUDE 'aleulr.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'aleulr.fc'
      INCLUDE 'iceulr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
         IOB = HOBJ - AL_EULR_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the increment controller</comment>
         IF (PROP .EQ. 'h_cinc') THEN
            IVAL = AL_EULR_HCTI(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

C        <comment>Handle on the underlying solver</comment>
         ELSEIF (PROP .EQ. 'h_solver') THEN
            IVAL = AL_EULR_HRES(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_EULR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_EULR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALEULR_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALEULR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALEULR_AID()

9999  CONTINUE
      IC_ALEULR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALEULR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALEULR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'iceulr.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>euler</b> represents the Euler time integration algorithm
C  (Theta scheme).
C</comment>
      IC_ALEULR_REQCLS = 'euler'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALEULR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALEULR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'iceulr.fi'
      INCLUDE 'aleulr.fi'
C-------------------------------------------------------------------------

      IC_ALEULR_REQHDL = AL_EULR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALEULR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('iceulr.hlp')
      RETURN
      END

