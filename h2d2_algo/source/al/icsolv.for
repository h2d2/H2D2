C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_SOLV_XEQ
C   Private:
C     INTEGER IC_SOLV_XEQA
C     INTEGER IC_SOLV_XEQS
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SOLV_XEQ(HOBJ, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SOLV_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'icsolv.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C---     Dispatch
      IF     (AL_SLVR_HVALIDE(HOBJ)) THEN
         IERR = IC_SOLV_XEQA(HOBJ, IPRM)
      ELSEIF (AL_SQNC_HVALIDE(HOBJ)) THEN
         IERR = IC_SOLV_XEQS(HOBJ, IPRM)
      ELSE
         GOTO 9901
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HOBJ
      IERR = OB_OBJC_ERRH(HOBJ, 'MSG_SOLUTION')
      GOTO 9999

9999  CONTINUE
      IC_SOLV_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_SOLV_XEQA exécute un algorithme. L'algorithme
C     est encapsulé dans une séquence qui est exécutée.
C
C Entrée:
C     HALG     Handle sur l'algorithme
C     IPRM     Paramètres de l'algorithme
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SOLV_XEQA(HALG, IPRM)

      IMPLICIT NONE

      INTEGER HALG
      CHARACTER*(*) IPRM

      INCLUDE 'icsolv.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER HELE
      INTEGER HPST
      INTEGER IDEB5, IFIN5
      INTEGER HSEQ, HTRG, HTMR
      CHARACTER*256 LPRM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HALG))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      HTMR = OB_OBJC_HNUL
      HELE = OB_OBJC_HNUL
      HPST = OB_OBJC_HNUL
      HTRG = OB_OBJC_HNUL
C     <comment>Handle on the timer  (can be 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HTMR)
C     <comment>Handle on the element</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 2, HELE)
C     <comment>Handle on the post-treatment during resolution (can be 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, HPST)
C     <comment>Handle on the trigger for the post-treatment (can be 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 4, HTRG)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>
C     Arguments passed directly to the sequence.
C     For a stationary simulation:
C     <pre>
C        TINI: initial time</pre>
C     And for a transient simulation:
C     <pre>
C        TINI: initial time,
C        DELT: time step,
C        NPAS: number of time steps</pre>
C     </comment>
      IERR = SP_STRN_TKS(IPRM, ',', 5, LPRM) ! uniquement pour le scan
      IF (IRET .NE. 0) GOTO 9901
      IDEB5 = SP_STRN_TOK(IPRM, ',', 5)
      IFIN5 = SP_STRN_LEN(IPRM)

C---     CONTROLE QUE LES PARAM SONT VALIDES
      IF (IDEB5 .LE. 0) GOTO 9901
      IF (IFIN5 .LT. IDEB5) GOTO 9901
      IF (.NOT. LM_HELE_HVALIDE(HELE)) GOTO 9902
      IF (HPST .NE. 0 .AND. .NOT. PS_POST_HVALIDE(HPST)) GOTO 9903
      IF (HTRG .NE. 0 .AND. .NOT. TG_TRIG_CTRLH(HTRG))   GOTO 9904

C---     CONSTRUIS LA SEQUENCE
      HSEQ = 0
      IF (ERR_GOOD()) IERR = AL_SQNC_CTR(HSEQ)
      IF (ERR_GOOD()) IERR = AL_SQNC_INI(HSEQ)
      IF (ERR_GOOD()) IERR = AL_SQNC_ADDALGO(HSEQ, HELE, HALG, 0) ! HTRG algo
      IF (ERR_GOOD()) THEN
         IF (PS_POST_HVALIDE(HPST)) IERR = AL_SQNC_ADDPOST(HSEQ, HELE,
     &                                                     HPST, HTRG)
      ENDIF
C---     EXECUTE
      IF (ERR_GOOD()) IERR = AL_SQNC_XEQ(HSEQ, HTMR, IPRM(IDEB5:IFIN5))

C---     DETRUIS LA SEQUENCE
      CALL ERR_PUSH()
      IF (AL_SQNC_HVALIDE(HSEQ)) IERR = AL_SQNC_DTR(HSEQ)
      CALL ERR_POP()

C---     NE RETOURNE RIEN
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HELE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HELE, 'MSG_SIMULATION')
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HPST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPST, 'MSG_POST_TRAITEMENT')
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HTRG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HTRG, 'MSG_TRIGGER')
      GOTO 9999

9999  CONTINUE
      IC_SOLV_XEQA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée IC_SOLV_XEQS exécute une séquence.
C
C Entrée:
C     HALG     Handle sur la séquence
C     IPRM     Paramètres de l'algorithme
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SOLV_XEQS(HALG, IPRM)

      IMPLICIT NONE

      INTEGER HALG
      CHARACTER*(*) IPRM

      INCLUDE 'icsolv.fi'
      INCLUDE 'alsqnc.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'trcdwn.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR, IRET
      INTEGER IDEB5, IFIN5
      INTEGER HTMR
      CHARACTER*256 LPRM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HALG))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      HTMR = OB_OBJC_HNUL
C     <comment>Handle on the timer  (can be 0)</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HTMR)
C     <comment>
C     Arguments passed directly to the sequence.
C     For a stationary simulation:
C     <pre>
C        TINI:    initial time</pre>
C     For a transient simulation:
C     <pre>
C        TINI:    initial time
C        DELT:    time step
C        NPAS:    number of time steps</pre>
C     </comment>
      IF (IRET .EQ. 0) IRET  = SP_STRN_TKS(IPRM, ',', 2, LPRM) ! uniquement pour le scan
      IF (IRET .NE. 0) GOTO 9901
      IDEB5 = SP_STRN_TOK(IPRM, ',', 2)
      IFIN5 = SP_STRN_LEN(IPRM)

C---     Contrôle que les paramètres sont valides
      IF (IDEB5 .LE. 0) GOTO 9901
      IF (IFIN5 .LT. IDEB5) GOTO 9901
      IF (HTMR .NE. 0 .AND. .NOT. TR_CDWN_HVALIDE(HTMR)) GOTO 9902

C---     Exécute
      IERR = AL_SQNC_XEQ(HALG, HTMR, IPRM(IDEB5:IFIN5))

C---     Ne retourne rien
      IPRM = ' '

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HTMR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HTMR, 'MSG_TIMER')
      GOTO 9999

9999  CONTINUE
      IC_SOLV_XEQS = ERR_TYP()
      RETURN
      END
