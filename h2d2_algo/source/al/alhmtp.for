C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   HoMoToPy
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_HMTP
C         SUBROUTINE AL_HMTP_000
C         SUBROUTINE AL_HMTP_999
C         SUBROUTINE AL_HMTP_CTR
C         SUBROUTINE AL_HMTP_DTR
C         SUBROUTINE AL_HMTP_INI
C         SUBROUTINE AL_HMTP_RST
C         SUBROUTINE AL_HMTP_REQHBASE
C         SUBROUTINE AL_HMTP_HVALIDE
C         SUBROUTINE AL_HMTP_PRN
C         SUBROUTINE AL_HMTP_RESOUS
C         SUBROUTINE AL_HMTP_RESOUS_E
C         SUBROUTINE AL_HMTP_UN_PAS
C         SUBROUTINE AL_HMTP_XEQK
C         SUBROUTINE AL_HMTP_XEQKT
C         SUBROUTINE AL_HMTP_XEQRES
C         SUBROUTINE AL_HMTP_REQSTATUS
C         SUBROUTINE AL_HMTP_ASMK
C         SUBROUTINE AL_HMTP_ASMKT
C         SUBROUTINE AL_HMTP_ASMRES
C         SUBROUTINE AL_HMTP_DUMMY
C         SUBROUTINE AL_HMTP_ASGDELT
C         SUBROUTINE AL_HMTP_REQDELT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_HMTP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_HMTP_NOBJMAX,
     &                   AL_HMTP_HBASE,
     &                   'Algorithm Homotopy')

      AL_HMTP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER  IERR
      EXTERNAL AL_HMTP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_HMTP_NOBJMAX,
     &                   AL_HMTP_HBASE,
     &                   AL_HMTP_DTR)

      AL_HMTP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_HMTP_NOBJMAX,
     &                   AL_HMTP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_HMTP_HVALIDE(HOBJ))
         IOB = HOBJ - AL_HMTP_HBASE

         AL_HMTP_DTAL(IOB) = 0.0D0
         AL_HMTP_DTEF(IOB) = 0.0D0
         AL_HMTP_HRES(IOB) = 0
         AL_HMTP_HSIM(IOB) = 0
         AL_HMTP_HCTI(IOB) = 0
         AL_HMTP_HPJC(IOB) = 0
         AL_HMTP_HMTP(IOB) = 0
         AL_HMTP_HTBL(IOB) = 0
         AL_HMTP_LTMP(IOB) = 0
         AL_HMTP_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_HMTP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_HMTP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_HMTP_NOBJMAX,
     &                   AL_HMTP_HBASE)

      AL_HMTP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HRES  Handle sur l'algo de résolution
C     HMTP  Handle sur le calculateur de charge
C     HCTI  Handle sur le contrôleur d'incrément
C     HPJC  Handle sur la projection
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_INI(HOBJ, HRES, DELT, HMTP, HCTI, HPJC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HRES
      REAL*8  DELT
      INTEGER HMTP
      INTEGER HCTI
      INTEGER HPJC

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HTBL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. (HCTI .EQ. 0 .OR. CI_CINC_HVALIDE(HCTI))) GOTO 9900
      IF (.NOT. (HPJC .EQ. 0 .OR. PJ_PRJC_HVALIDE(HPJC))) GOTO 9901
      IF (.NOT. AL_SLVR_HVALIDE(HRES)) GOTO 9902
      IF (DELT .LE. 0.0D0) GOTO 9903
      IF (.NOT. (HMTP .EQ. 0 .OR. HM_HMTP_HVALIDE(HMTP))) GOTO 9904

C---     Reset les données
      IERR = AL_HMTP_RST(HOBJ)

C---     Construit la table de CB
      HTBL = 0
      IF (ERR_GOOD()) IERR = SO_VTBL_CTR(HTBL)
      IF (ERR_GOOD()) IERR = SO_VTBL_INI(HTBL)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_HMTP_HBASE
         AL_HMTP_DTAL(IOB) = DELT
         AL_HMTP_DTEF(IOB) = 0.0D0
         AL_HMTP_HRES(IOB) = HRES
         AL_HMTP_HSIM(IOB) = 0
         AL_HMTP_HCTI(IOB) = HCTI
         AL_HMTP_HPJC(IOB) = HPJC
         AL_HMTP_HMTP(IOB) = HMTP
         AL_HMTP_HTBL(IOB) = HTBL
         AL_HMTP_LTMP(IOB) = 0
         AL_HMTP_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HPJC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPJC, 'MSG_PROJECTION_DDL')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCTI
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCTI, 'MSG_CTRL_INCREMENT')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HRES
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRES, 'MSG_SOLVER')
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HMTP
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HMTP, 'MSG_HOMOTOPY')
      GOTO 9999

9999  CONTINUE
      AL_HMTP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTBL, HPRJ, HRML
      INTEGER LTMP
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_HMTP_HBASE
      HTBL = AL_HMTP_HTBL(IOB)
      LTMP = AL_HMTP_LTMP(IOB)

C---     Détruis les objets
      IF (SO_VTBL_HVALIDE(HTBL)) IERR = SO_VTBL_DTR(HTBL)

C---     Récupère la mémoire
      IF (SO_ALLC_HEXIST(LTMP)) IERR = SO_ALLC_ALLRE8(0, LTMP)

C---     Reset
      AL_HMTP_DTAL(IOB) = 0.0D0
      AL_HMTP_DTEF(IOB) = 0.0D0
      AL_HMTP_HRES(IOB) = 0
      AL_HMTP_HSIM(IOB) = 0
      AL_HMTP_HCTI(IOB) = 0
      AL_HMTP_HPJC(IOB) = 0
      AL_HMTP_HMTP(IOB) = 0
      AL_HMTP_HTBL(IOB) = 0
      AL_HMTP_LTMP(IOB) = 0
      AL_HMTP_STTS(IOB) = AL_STTS_INDEFINI

      AL_HMTP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_HMTP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alhmtp.fc'
C------------------------------------------------------------------------

      AL_HMTP_REQHBASE = AL_HMTP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_HMTP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alhmtp.fc'
C------------------------------------------------------------------------

      AL_HMTP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_HMTP_NOBJMAX,
     &                                  AL_HMTP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime les paramètres dans le log.
C
C Description:
C     La méthode AL_HMTP_PRN imprime dans le log les paramètres de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK
      IOB = HOBJ - AL_HMTP_HBASE

C---     En-tête de commande
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ALGO_HOMOTOPY'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     Impression des attributs
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_HMTP_HMTP(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_HMTP_HCTI(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_HMTP_HPJC(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_HMTP_HRES(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      WRITE(LOG_BUF,'(2A,1PE14.6E3)') 'MSG_DTAL#<35>#', '= ',
     &                              AL_HMTP_DTAL(IOB)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      AL_HMTP_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_HMTP_RESOUS résout par une méthode d'homotopie
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C     En sortie, TSIM et DELT sont ajustés à la partie calculée.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des DDL. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_HMTP_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR

      EXTERNAL AL_HMTP_ASMK
      EXTERNAL AL_HMTP_ASMKT
      EXTERNAL AL_HMTP_ASMRES
      EXTERNAL AL_HMTP_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)    ! cf. note
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_HMTP_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_HMTP_ASMK,
     &                           AL_HMTP_ASMKT,
     &                           AL_HMTP_DUMMY,
     &                           AL_HMTP_DUMMY,
     &                           AL_HMTP_ASMRES)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_HMTP_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_HMTP_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme d'Homotopie.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo. C'est la fonction appelante qui dois gérer le
C     chargement des données.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_K         Fonction de calcul de [K]
C     EXTERNAL F_KT        Fonction de calcul de [KT]
C     EXTERNAL F_KU        Fonction de calcul de [K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C     En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C     ajustées à la dernière valeur des DDL. En fait elles sont une
C     itération en retard.
C************************************************************************
      FUNCTION AL_HMTP_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM
      INTEGER, INTENT(IN)     :: HALG
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'mrlmpd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'alhmtp.fc'

      REAL*8  DTNM, DTPRV, DTNOW, DTNXT, DTRST, TINI, TFIN
      REAL*8  TS_C, DT_C

      INTEGER IERR
      INTEGER IOB
      INTEGER IPAS, ISTG, ISTC
      INTEGER HCTI, HMTP, HTBL
      INTEGER HNMR
      INTEGER HSVPT
      INTEGER NDLN, NNL, NDLL
      INTEGER LTMP
      INTEGER LDLG, LSPT, LLOCN
      LOGICAL ACTI, AMTP, ESTOK, ESTRES, ESTRJT, ESTXIT, PUSHT

      EXTERNAL AL_HMTP_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.homotopy'

C---     Récupère les attributs
      IOB = HOBJ - AL_HMTP_HBASE
      HCTI = AL_HMTP_HCTI(IOB)
      HMTP = AL_HMTP_HMTP(IOB)
      HTBL = AL_HMTP_HTBL(IOB)
      LTMP = AL_HMTP_LTMP(IOB)
D     CALL ERR_ASR(SO_VTBL_HVALIDE(HTBL))
D     CALL ERR_ASR(LTMP .EQ. 0 .OR. SO_ALLC_HEXIST(LTMP))

C---     Récupère les paramètres de la simulation
      HNMR = LM_ELEM_REQHNUMC(HSIM)
      LLOCN= LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)
      LDLG = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      NDLL = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NDLN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NNL  = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)

C---     Présence/absence des composantes
      ACTI = CI_CINC_HVALIDE(HCTI)
      AMTP = HM_HMTP_HVALIDE(HMTP)

C---     Calcule les paramètres
      DTNM  = AL_HMTP_DTAL(IOB)
      IF (HSIM .EQ. AL_HMTP_HSIM(IOB)) THEN
         DTNXT = MIN(AL_HMTP_DTEF(IOB), DELT)
      ELSE
         DTNXT = MIN(AL_HMTP_DTAL(IOB), DELT)
      ENDIF
      DTNOW = DTNXT
      TINI = TSIM
      TFIN = TSIM + DELT

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_HOMOTOPY_SOLVE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', '= ', TFIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT_NOMINAL#<35>#', '= ', DTNM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     (Ré)-alloue les tables de travail
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, LTMP) ! NEQ et NDLL

C---     Conserve les paramètres pour la simulation
      AL_HMTP_HSIM(IOB) = HSIM
      AL_HMTP_LTMP(IOB) = LTMP

C---     Monte la table virtuelle
      IERR = SO_VTBL_AJTMTH(HTBL, AL_HMTP_FNC_K,  HALG, F_K)
      IERR = SO_VTBL_AJTMTH(HTBL, AL_HMTP_FNC_KT, HALG, F_KT)
      IERR = SO_VTBL_AJTMTH(HTBL, AL_HMTP_FNC_RES,HALG, F_RES)

C---     Crée un point de sauvegarde
      HSVPT = 0
      LSPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)
      IF (ERR_GOOD()) LSPT = SO_SVPT_GET(HSVPT)

C---     Initialise le contrôleur d'incrément
      IF (ERR_GOOD() .AND. ACTI)
     &   IERR = CI_CINC_DEB(HCTI, TINI, TFIN, DTNOW)


C        --------------------------------
C---     Boucle sur les sous-pas de temps
C        --------------------------------
      IPAS = 0
100   CONTINUE
         IF (ERR_BAD()) GOTO 199
         IF (DTNOW .LE. 0.0D0) GOTO 199
         IF (TINI  .GE. TFIN)  GOTO 199

C---        Log
         IPAS = IPAS + 1
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6,3(A,F20.6))',ERR=109)
     &      'MSG_PAS_DE_TEMPS:', IPAS,
     &      ', MSG_TINI=', TINI,
     &      ', MSG_TFIN=', TINI+DTNOW,
     &      ', MSG_DELT=', DTNOW
109      CONTINUE
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Traceur
         IERR = TRA_ASGTALG(TINI)

C---        Sauve l'état courant
         IF (ERR_GOOD() .AND. HSVPT .NE. 0) THEN
            IERR = SO_SVPT_SAVE(HSVPT)
         ENDIF

C---        Résous
         TS_C = TINI
         DT_C = DTNOW
         IF (ERR_GOOD()) THEN
            IERR = AL_HMTP_UN_PAS(HOBJ,
     &                            TS_C,
     &                            DT_C,
     &                            HSIM,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)))
         ENDIF
         IF (ERR_BAD()) GOTO 199
         CALL LOG_INFO(LOG_ZNE, ' ')

C---        Delta sur le pas dans LTMP
         CALL DCOPY(NDLL,
     &              VA(SO_ALLC_REQVIND(VA,LDLG)), 1,
     &              VA(SO_ALLC_REQVIND(VA,LTMP)), 1)
         CALL DAXPY(NDLL,
     &              -1.0D0,
     &               VA(SO_ALLC_REQVIND(VA,LSPT)), 1,
     &               VA(SO_ALLC_REQVIND(VA,LTMP)), 1)
         IERR = SP_ELEM_CLIM2VAL(NDLL,
     &                           KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                           0.0D0,
     &                           VA(SO_ALLC_REQVIND(VA, LTMP)))

C---        L'état de résolution
         ISTG = AL_HMTP_REQSTATUS(HOBJ)

C---        Adapte le pas
         DTPRV = DTNOW
         IF (ACTI) THEN
D           CALL ERR_ASR(SO_ALLC_HEXIST(LSPT))
            ESTOK = (ISTG .EQ. AL_STTS_OK)
            IERR = CI_CINC_CLCDEL(HCTI,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                            VA(SO_ALLC_REQVIND(VA,LTMP)), ! delU
     &                            ESTOK)
            IERR = CI_CINC_REQDELT(HCTI, ISTC, DTNXT, DTNOW)
            ISTG = AL_UTIL_CLRSTTS(ISTG)
            ISTG = AL_UTIL_IORSTTS(ISTG, ISTC)
         ELSE
            DTRST = TFIN - (TINI+DTPRV)
            DTNOW = MIN(DTPRV, DTRST)
            DTRST = DTRST - DTNOW
            IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTPRV)) THEN
               DTNOW = DTNOW + DTRST
            ENDIF
         ENDIF

C---        L'état de résolution
         ESTOK  = (ISTG .EQ. AL_STTS_OK)
         ESTRES = .NOT. BTEST(ISTG, AL_STTS_CRIA_BAD)
         ESTRJT = .FALSE.
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_COUPE)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTRJT = ESTRJT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)
         ESTXIT = .FALSE.
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_DTMIN)
         ESTXIT = ESTXIT .OR. BTEST(ISTG, AL_STTS_CINC_STOP)

C---        Logique de continuation
         PUSHT = .FALSE.
         IF (ESTRES) THEN
            PUSHT = .TRUE.
D           CALL ERR_ASR(ERR_BAD() .OR. DT_C .EQ. 0.0D0)
         ELSE
            IF (ACTI) THEN                ! !OK, Avec contrôleur d'incrément
               IERR = SO_SVPT_RBCK(HSVPT) !  rollback. On pert possiblement
               PUSHT = .FALSE.            !  une partie déjà convergée
            ELSE
               PUSHT = .TRUE.             ! !OK, Sans contrôleur d'incrément
            ENDIF                         !   continue
         ENDIF
         IF (PUSHT) TINI = TINI + DTPRV

C---        Log l'incrément sur le pas de temps
         IF (ERR_GOOD()) THEN
            IERR = AL_UTIL_LOGSOL(NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LTMP)),
     &                            HNMR)
         ENDIF

C---        Log l'état
         IF (ERR_GOOD()) THEN
            IF (ACTI) THEN
               IF (BTEST(ISTG, AL_STTS_CINC_DTMIN))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_DTMIN')
               IF (BTEST(ISTG, AL_STTS_CINC_SKIP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_SKIP')
               IF (BTEST(ISTG, AL_STTS_CINC_STOP))
     &            CALL LOG_WRN(LOG_ZNE, 'MSG_STOP')
               IF (.NOT. ESTRES)
     &            CALL LOG_INFO(LOG_ZNE, 'MSG_PAS_REJETE')
            ELSE
               IF (.NOT. ESTRES) THEN
                  CALL LOG_WRN(LOG_ZNE, 'MSG_PAS_NON_CONVERGE')
                  CALL LOG_WRN(LOG_ZNE, 'MSG_CONTINUE')
               ENDIF
            ENDIF
         ENDIF

         CALL LOG_DECIND()
         IF (ESTXIT) GOTO 199
      GOTO 100 ! end while
199   CONTINUE

C---     Repositionne l'homotopie au dernier pas convergé
      IF (ERR_GOOD() .AND. AMTP) IERR = HM_HMTP_XEQ(HMTP, TINI)

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         DELT = (TSIM+DELT) - TINI
         TSIM = TINI
      ENDIF

C---     Conserve la prévision suite au dernier pas de temps
      IF (ERR_GOOD()) THEN
         AL_HMTP_DTEF(IOB) = DTNXT
      ENDIF

C---     Conserve l'état
      AL_HMTP_STTS(IOB) = ISTG

C---     Relâche le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = SO_SVPT_DTR(HSVPT)
         CALL ERR_POP()
      ENDIF

      CALL LOG_DECIND()
      AL_HMTP_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_HMTP_UN_PAS intègre un pas d'homotopie.
C     Elle amène HSIM de ASIM à ASIM+DELA.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C     REAL*8  VDLG0        Table des DDL (valeurs initiales)
C     REAL*8  VDLG         Table des DDL de HSIM
C
C Sortie:
C     REAL*8  VDLG         Table des DDL de HSIM mis à jour
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les DLL sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des DDL. En fait elles sont une
C       itération en retard.
C    3) le calcul du résidu dans le projection est FAUX, car VDLG est
C       modifié par la projection mais les PROP ne sont pas mise à jour.
C************************************************************************
      FUNCTION AL_HMTP_UN_PAS(HOBJ,
     &                        TSIM,
     &                        DTEF,
     &                        HSIM,
     &                        VDLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DTEF
      INTEGER  HSIM
      REAL*8   VDLG (*)

      INCLUDE 'alhmtp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'hmhmtp.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'pjprjc.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTBL, HMTP, HRES, HPJC, HFRES
      LOGICAL AMTP

      INCLUDE 'ioutil.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psresi.fi'
      INTEGER HRES_D
      REAL*8 TSIM_D
      CHARACTER*(256) FNAME

      EXTERNAL AL_HMTP_XEQK
      EXTERNAL AL_HMTP_XEQKT
      EXTERNAL AL_HMTP_XEQRES
      EXTERNAL AL_HMTP_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - AL_HMTP_HBASE
      HRES = AL_HMTP_HRES(IOB)
      HPJC = AL_HMTP_HPJC(IOB)
      HMTP = AL_HMTP_HMTP(IOB)
      HTBL = AL_HMTP_HTBL(IOB)
D     CALL ERR_ASR(AL_SLVR_HVALIDE(HRES))
D     CALL ERR_ASR(HPJC .EQ. 0 .OR. PJ_PRJC_HVALIDE(HPJC))

C---     Présence/absence des composantes
      AMTP = HM_HMTP_HVALIDE(HMTP)

C---     Conserve les paramètres pour la simulation
      AL_HMTP_DTEF(IOB) = DTEF

C---     Ajoute la solution initiale à la projection
      IF (HPJC .NE. 0) THEN
         IF (ERR_GOOD()) IERR = PJ_PRJC_AJT(HPJC, HSIM, TSIM, VDLG)
      ENDIF

C---     Charge les données
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM+DTEF)
      IF (ERR_GOOD() .AND. AMTP) IERR = HM_HMTP_XEQ(HMTP, TSIM+DTEF)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASMAJ(HSIM)

C---     Projette la solution initiale (c.f.note)
      IF (HPJC .NE. 0) THEN
         IF (ERR_GOOD()) IERR = SO_VTBL_REQMTH(HTBL,
     &                                         AL_HMTP_FNC_RES, HFRES)
         IF (ERR_GOOD()) IERR = PJ_PRJC_PRJ   (HPJC, HSIM, HFRES,
     &                                         TSIM+DTEF, VDLG)
      ENDIF

C---     Pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC   (HSIM)

C---     Résous
      IF (ERR_GOOD()) THEN
         IERR = AL_SLVR_RESOUS_E(HRES,
     &                           TSIM,
     &                           DTEF,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_HMTP_XEQK,
     &                           AL_HMTP_XEQKT,
     &                           AL_HMTP_DUMMY,
     &                           AL_HMTP_DUMMY,
     &                           AL_HMTP_XEQRES)
      ENDIF

C---     Conserve le résultat
      IF (ERR_GOOD()) THEN
      AL_HMTP_STTS(IOB) = AL_SLVR_REQSTATUS(HRES)
      ENDIF

      AL_HMTP_UN_PAS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée AL_HMTP_XEQK appelle la fonction enregistrée
C     pour K.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_XEQK(HOBJ, HMTX, F_ASM)

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTBL, HMTH
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_HMTP_HBASE
      HTBL = AL_HMTP_HTBL(IOB)

C---     Récupère le méthode
      IF (ERR_GOOD()) IERR = SO_VTBL_REQMTH(HTBL, AL_HMTP_FNC_K, HMTH)

C---     Appel la fonction enregistrée
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL2(HMTH,
     &                        SO_ALLC_CST2B(HMTX),
     &                        SO_ALLC_CST2B(F_ASM,0_1))

      AL_HMTP_XEQK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée AL_HMTP_XEQKT appelle la fonction enregistrée
C     pour Kt.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_XEQKT(HOBJ, HMTX, F_ASM)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTBL, HMTH
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_HMTP_HBASE
      HTBL = AL_HMTP_HTBL(IOB)

C---     Récupère le fonction
      IF (ERR_GOOD()) IERR = SO_VTBL_REQMTH(HTBL, AL_HMTP_FNC_KT, HMTH)

C---     Appel la fonction enregistrée
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL2(HMTH,
     &                        SO_ALLC_CST2B(HMTX),
     &                        SO_ALLC_CST2B(F_ASM,0_1))

      AL_HMTP_XEQKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée AL_HMTP_XEQRES organise le calcul du résidu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_XEQRES(HOBJ, VRES, VDLG, ADLG)

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sovtbl.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HTBL, HMTH
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_HMTP_HBASE
      HTBL = AL_HMTP_HTBL(IOB)

C---     Récupère le fonction
      IF (ERR_GOOD()) IERR = SO_VTBL_REQMTH(HTBL, AL_HMTP_FNC_RES, HMTH)

C---     R dans VRES
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL3(HMTH,
     &                        SO_ALLC_CST2B(VRES(1)),
     &                        SO_ALLC_CST2B(VDLG(1)),
     &                        SO_ALLC_CST2B(ADLG))

      AL_HMTP_XEQRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_HMTP_REQSTATUS = AL_HMTP_STTS(HOBJ - AL_HMTP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_ASMK(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_HMTP_HBASE
      HSIM = AL_HMTP_HSIM(IOB)

C---     ASSEMBLE K
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMK (HSIM, HMTX, F_ASM)

      AL_HMTP_ASMK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_ASMKT(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_HMTP_HBASE
      HSIM = AL_HMTP_HSIM(IOB)

C---     ASSEMBLE Kt
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKT (HSIM, HMTX, F_ASM)

      AL_HMTP_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_ASMRES(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_HMTP_HBASE
      HSIM = AL_HMTP_HSIM(IOB)

C---     ASSIGNE VDLG À LA SIMULATION
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     ASSEMBLE (F-KU) DANS VRES
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMFKU(HSIM, VRES)

C---     RÉTABLIS LA SIMULATION
      IF (ADLG) THEN
         CALL ERR_PUSH()
         IERR = LM_ELEM_POPLDLG(HSIM)
         CALL ERR_POP()
      ENDIF

      AL_HMTP_ASMRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_HMTP_DUMMY(HOBJ, VDLG, VRES)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VDLG(*)
      REAL*8  VRES(*)

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'AL_HMTP_DUMMY shall never be called')
      CALL ERR_ASR(.FALSE.)

      AL_HMTP_DUMMY = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_ASGDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_ASGDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IF (DELT .LE. 0.0D0) GOTO 9900

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_HMTP_HBASE
      AL_HMTP_DTAL(IOB) = DELT

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_HMTP_ASGDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_HMTP_REQDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_HMTP_REQDELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alhmtp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alhmtp.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_HMTP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DELT = AL_HMTP_DTAL(HOBJ-AL_HMTP_HBASE)

      AL_HMTP_REQDELT = ERR_TYP()
      RETURN
      END
