C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   strategy NODE
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_SLVR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_SLVR_NOBJMAX,
     &                   AL_SLVR_HBASE,
     &                   'Algorithm Solver')

      AL_SLVR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
      EXTERNAL AL_SLVR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_SLVR_NOBJMAX,
     &                   AL_SLVR_HBASE,
     &                   AL_SLVR_DTR)

      AL_SLVR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SLVR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_SLVR_NOBJMAX,
     &                   AL_SLVR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_SLVR_HVALIDE_SELF(HOBJ))
         IOB = HOBJ - AL_SLVR_HBASE

         AL_SLVR_HOMG(IOB) = 0
      ENDIF

      AL_SLVR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SLVR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alslvr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE_SELF(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_SLVR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_SLVR_NOBJMAX,
     &                   AL_SLVR_HBASE)

      AL_SLVR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HOMG        Handle de l'Objet ManaGé
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_INI(HOBJ, HOMG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HOMG

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL
      INTEGER  HFNC
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE_SELF(HOBJ))
D     CALL ERR_PRE(HOMG .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_SLVR_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - AL_SLVR_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HOMG, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HOMG)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         AL_SLVR_HOMG(IOB) = HOMG
         AL_SLVR_HMDL(IOB) = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HOMG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HOMG, 'MSG_ALGORITHME')
      GOTO 9999

9999  CONTINUE
      AL_SLVR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SLVR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HMDL, HFNC, HOMG
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE_SELF(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_SLVR_HBASE
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      HVALIDE = .FALSE.
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     FAIT L'APPEL POUR DÉTRUIRE L'OBJET ASSOCIÉ
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOMG))

C---     RESET LES ATTRIBUTS
1000  CONTINUE
      AL_SLVR_HOMG(IOB) = 0
      AL_SLVR_HMDL(IOB) = 0

      AL_SLVR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_SLVR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alslvr.fi'
      INCLUDE 'alslvr.fc'
C------------------------------------------------------------------------

      AL_SLVR_REQHBASE = AL_SLVR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_SLVR_HVALIDE_SELF permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_HVALIDE_SELF(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'obobjc.fi'
      INCLUDE 'alslvr.fc'
C------------------------------------------------------------------------

      AL_SLVR_HVALIDE_SELF = OB_OBJC_HVALIDE(HOBJ,
     &                                       AL_SLVR_NOBJMAX,
     &                                       AL_SLVR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction AL_SLVR_HVALIDE permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SLVR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_HVALIDE
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG, HMDL, HFNC
      LOGICAL HVALIDE
C------------------------------------------------------------------------
      
      HVALIDE = AL_SLVR_HVALIDE_SELF(HOBJ)
      IF (HVALIDE) GOTO 9999
         
C---     Cherche si le handle est connu
      HVALIDE = .FALSE.
      IOB  = AL_SLVR_REQIOB(HOBJ)
      IF (IOB .LE. 0) GOTO 9999
      HMDL = AL_SLVR_HMDL(IOB)
      IF (HMDL .NE. 0) HVALIDE = SO_MDUL_HVALIDE(HMDL)
      IF (.NOT. HVALIDE) GOTO 9999

C---     Contrôle le handle
      HVALIDE = .FALSE.
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HOBJ))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      HVALIDE = (ERR_GOOD() .AND. HVALIDE)

9999  CONTINUE
      AL_SLVR_HVALIDE = HVALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM        Handle sur l'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_RESOUS
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cherche le solver
      IOB  = AL_SLVR_REQIOB(HOBJ)
D     CALL ERR_ASR(IOB .GT. 0)
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'RESOUS', HFNC)

C---     Configure l'outil de trace
      IF (ERR_GOOD()) IERR = TRA_ASGINOD(HOMG)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(TSIM),
     &                        SO_ALLC_CST2B(DELT),
     &                        SO_ALLC_CST2B(HSIM))

C---     Reset l'outil de trace
      IF (ERR_GOOD()) IERR = TRA_ASGINOD(0)

      AL_SLVR_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_RESOUS_E
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER, INTENT(IN)     :: HOBJ
      REAL*8,  INTENT(INOUT)  :: TSIM
      REAL*8,  INTENT(INOUT)  :: DELT
      INTEGER, INTENT(IN)     :: HSIM
      INTEGER, INTENT(IN)     :: HALG
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER  IERR
      INTEGER  IOB
      INTEGER  HOMG, HMDL, HFNC
      INTEGER  H(3)
      INTEGER  AL_SLVR_RESOUS_E_CB
      EXTERNAL AL_SLVR_RESOUS_E_CB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cherche le solver
      IOB  = AL_SLVR_REQIOB(HOBJ)
D     CALL ERR_ASR(IOB .GT. 0)
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Compacte les handles dans une seule table
      H(1) = HOMG
      H(2) = HSIM
      H(3) = HALG

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'RESOUS_E', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CBFNC8(HFNC,
     &                         AL_SLVR_RESOUS_E_CB,
     &                         SO_ALLC_CST2B(TSIM),
     &                         SO_ALLC_CST2B(DELT),
     &                         SO_ALLC_CST2B(H),
     &                         SO_ALLC_CST2B(F_K,  0_1),
     &                         SO_ALLC_CST2B(F_KT, 0_1),
     &                         SO_ALLC_CST2B(F_KU, 0_1),
     &                         SO_ALLC_CST2B(F_F,  0_1),
     &                         SO_ALLC_CST2B(F_RES,0_1))

      AL_SLVR_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Call-back privé.
C
C Description:
C     La fonction privée AL_SLVR_RESOUS_E_CB est le call-back pour
C     la fonction AL_SLVR_RESOUS_E.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SLVR_RESOUS_E_CB(F,
     &                             TSIM,
     &                             DELT,
     &                             H,
     &                             F_K,
     &                             F_KT,
     &                             F_KU,
     &                             F_F,
     &                             F_RES)

      IMPLICIT NONE

      INTEGER AL_SLVR_RESOUS_E_CB
      INTEGER  F
      EXTERNAL F

      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  H(3)
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = F(H(1),
     &         TSIM,
     &         DELT,
     &         H(2),
     &         H(3),
     &         F_K,
     &         F_KT,
     &         F_KU,
     &         F_F,
     &         F_RES)

      AL_SLVR_RESOUS_E_CB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_REQSTATUS
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HOMG, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cherche le solver
      IOB  = AL_SLVR_REQIOB(HOBJ)
D     CALL ERR_ASR(IOB .GT. 0)
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQSTATUS', HFNC)

C---     Fait l'appel
      IRET = -1
      IF (ERR_GOOD()) IRET = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HOMG))

      AL_SLVR_REQSTATUS = IRET
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_ASGDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_ASGDELT
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cherche le solver
      IOB  = AL_SLVR_REQIOB(HOBJ)
D     CALL ERR_ASR(IOB .GT. 0)
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASGDELT', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL2(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(DELT))

      AL_SLVR_ASGDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SLVR_REQDELT(HOBJ, DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SLVR_REQDELT
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   DELT

      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HOMG, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SLVR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Cherche le solver
      IOB  = AL_SLVR_REQIOB(HOBJ)
D     CALL ERR_ASR(IOB .GT. 0)
      HOMG = AL_SLVR_HOMG(IOB)
      HMDL = AL_SLVR_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQDELT', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL2(HFNC,
     &                        SO_ALLC_CST2B(HOMG),
     &                        SO_ALLC_CST2B(DELT))

      AL_SLVR_REQDELT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée AL_SLVR_REQIOB retourne l'indice de l'objet
C     qui correspond à l'objet managé HOBJ.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     On accepte en entrée tout handle sur une méthode de résolution 
C     managée par AL_SLVR.
C************************************************************************
      FUNCTION AL_SLVR_REQIOB(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'alslvr.fc'

      INTEGER IOB
C------------------------------------------------------------------------

      DO IOB=1,AL_SLVR_NOBJMAX
         IF (AL_SLVR_HOMG(IOB) .EQ. HOBJ) THEN
            EXIT
         ENDIF
      ENDDO
      IF (IOB .GT. AL_SLVR_NOBJMAX) IOB = -1

      AL_SLVR_REQIOB = IOB
      RETURN
      END
      