C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************
C$NOREFERENCE

C---     FONCTIONS PRIVEES

C------------------------------------------------------------------------

C---     ATTRIBUTS DE GESTION DES OBJETS
      INTEGER AL_MRQD_NOBJMAX
      INTEGER AL_MRQD_HBASE
      PARAMETER (AL_MRQD_NOBJMAX = 8)

C---     COMMON DE GESTION DES OBJETS
      COMMON /AL_MRQD_CGO/ AL_MRQD_HBASE

C------------------------------------------------------------------------

C---     ATTRIBUTS PRIVES
C     <comment>Handle sur la table virtuelle</comment>
      INTEGER AL_MRQD_HTBL
C     <comment>Handle sur la matrice diagonale lumpée</comment>
      INTEGER AL_MRQD_HMLP
C     <comment>Handle sur l'algorithme d'amortissement</comment>
      INTEGER AL_MRQD_HDMP
C     <comment>Handle sur l'algo, paramètre des call-backs</comment>
      INTEGER AL_MRQD_HALG
C     <comment>Handle sur le critère d'arrêt</comment>
      INTEGER AL_MRQD_HCRA
C     <comment>Handle sur le critère de convergence global</comment>
      INTEGER AL_MRQD_HCCG
C     <comment>Handle sur le critère de convergence des sous-itérations</comment>
      INTEGER AL_MRQD_HCCS
C     <comment>Handle sur l'algorithme de globalisation</comment>
      INTEGER AL_MRQD_HGLB
C     <comment>Handle sur l'algorithme de calcul de résidu</comment>
      INTEGER AL_MRQD_HRDU
C     <comment>Handle sur l'algorithme de résolution matricielle</comment>
      INTEGER AL_MRQD_HRES
C     <comment>Handle sur l'élément</comment>
      INTEGER AL_MRQD_HSIM
C     <comment>Table de travail pour les incréments</comment>
      INTEGER AL_MRQD_LDLU
C     <comment>Table de travail pour la solution</comment>
      INTEGER AL_MRQD_LSOL
C     <comment>Nombre d'itérations</comment>
      INTEGER AL_MRQD_NITER
C     <comment>Nombre de sous-itérations</comment>
      INTEGER AL_MRQD_NSITR
C     <comment>Compteurs du nombre d'itérations</comment>
      INTEGER AL_MRQD_CNTRS
C     <comment>Status de la résolution</comment>
      INTEGER AL_MRQD_STTS

C---     COMMON DES DONNEES
      COMMON /AL_MRQD_CDT/ AL_MRQD_HTBL (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HMLP (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HDMP (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HALG (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HCRA (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HCCG (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HCCS (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HGLB (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HRDU (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HRES (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_HSIM (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_LDLU (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_LSOL (AL_MRQD_NOBJMAX),
     &                     AL_MRQD_NITER(AL_MRQD_NOBJMAX),
     &                     AL_MRQD_NSITR(AL_MRQD_NOBJMAX),
     &                     AL_MRQD_CNTRS(2, AL_MRQD_NOBJMAX),
     &                     AL_MRQD_STTS (AL_MRQD_NOBJMAX)


C------------------------------------------------------------------------

      INTEGER AL_MRQD_FNC_KT
      INTEGER AL_MRQD_FNC_RES
      PARAMETER (AL_MRQD_FNC_KT  = 1)
      PARAMETER (AL_MRQD_FNC_RES = 2)

C$REFERENCE

