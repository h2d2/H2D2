C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: IC
C      H2D2 Class: IC_ALNOLR
C         SUBROUTINE IC_ALNOLR_XEQCTR
C         SUBROUTINE IC_ALNOLR_XEQMTH
C         SUBROUTINE IC_ALNOLR_REQCLS
C         SUBROUTINE IC_ALNOLR_REQHDL
C         SUBROUTINE IC_ALNOLR_AID
C         SUBROUTINE IC_ALNOLR_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNOLR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNOLR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alnolr_ic.fi'
      INCLUDE 'alnolr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnolr_ic.fc'

      INTEGER IERR
      INTEGER HOBJ, HNOD
      INTEGER HALG, HXPR, HLFT, HRHT
      INTEGER IRST
      LOGICAL LRST
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALNOLR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the resolution algorithm</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HALG)
C     <comment>Handle on the testing expression</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HXPR)
C     <comment>Handle on the left node executed if <code>hxpr</code> returns TRUE</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HLFT)
C     <comment>Handle on the right node executed if <code>hxpr</code> returns FALSE</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HRHT)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>In case <code>hxpr</code> returns FALSE, indicated if the solution
C              is reset [0=no rest, 1=do reset] (default 1).</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 5, IRST)
      IF (IERR .NE. 0) IRST = 1
      LRST = (IRST .NE. 0)

C---     CONSTRUIS ET INITIALISE L'OBJET
      HNOD = 0
      IF (ERR_GOOD()) IERR = AL_NOLR_CTR(HNOD)
      IF (ERR_GOOD()) IERR = AL_NOLR_INI(HNOD,
     &                                   HALG, HXPR,
     &                                   HLFT, HRHT, LRST)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SLVR_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = AL_SLVR_INI(HOBJ, HNOD)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALNOLR_PRN(HNOD)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the node</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HNOD
      ENDIF

C<comment>
C  The constructor <b>algo_node</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNOLR_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_ALNOLR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALNOLR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNOLR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alnolr_ic.fi'
      INCLUDE 'alnolr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icsolv.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnolr.fc'
      INCLUDE 'alnolr_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NOLR_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the resolution algorithm</comment>
         IF (PROP .EQ. 'h_algo') THEN
            IVAL = AL_NOLR_HALG(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the testing expression</comment>
         ELSEIF (PROP .EQ. 'h_expression') THEN
            IVAL = AL_NOLR_HXPR(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the left node executed if the expression returns TRUE</comment>
         ELSEIF (PROP .EQ. 'h_left') THEN
            IVAL = AL_NOLR_HLFT(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
C        <comment>Handle on the right node executed if the expression returns FALSE</comment>
         ELSEIF (PROP .EQ. 'h_right') THEN
            IVAL = AL_NOLR_HRHT(IOB)
            WRITE(IPRM, '(2A,I12)') 'H', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>solve</b> executes the algorithm.</comment>
      ELSEIF (IMTH .EQ. 'solve') THEN
D        CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C        <include>IC_SOLV_XEQA@icsolv.for</include>
         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = AL_NOLR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_ALNOLR_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_ALNOLR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALNOLR_AID()

9999  CONTINUE
      IC_ALNOLR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNOLR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNOLR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnolr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>algo_node</b> represents the node of a resolution strategy. Nodes
C  can be connected to form a decision tree. The strategy resolution will
C  start from a head node and walk its path down the tree depending on the
C  results of the expression evaluation. Note: As of now (June 2018),
C  expressions are not yet in use and are replaced by the convergence
C  criterion.
C</comment>
      IC_ALNOLR_REQCLS = 'algo_node'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNOLR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALNOLR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnolr_ic.fi'
      INCLUDE 'alnolr.fi'
C-------------------------------------------------------------------------

      IC_ALNOLR_REQHDL = AL_NOLR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALNOLR_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alnolr_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALNOLR_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnolr_ic.fi'
      INCLUDE 'alnolr.fi'
      INCLUDE 'alnolr.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alnolr_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IVAL
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
D        CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IOB = HOBJ - AL_NOLR_HBASE

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ALGO_NODE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NOLR_HALG(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NOLR_HXPR(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NOLR_HLFT(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(TXT, AL_NOLR_HRHT(IOB))
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      WRITE(LOG_BUF,'(2A,3L)')'MSG_RESET#<35>#', '= ',AL_NOLR_LRST(IOB)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_ALNOLR_PRN = ERR_TYP()
      RETURN
      END
