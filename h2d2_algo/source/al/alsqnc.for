C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: AL_SQNC
C     INTEGER AL_SQNC_000
C     INTEGER AL_SQNC_999
C     INTEGER AL_SQNC_CTR
C     INTEGER AL_SQNC_DTR
C     INTEGER AL_SQNC_INI
C     INTEGER AL_SQNC_RST
C     INTEGER AL_SQNC_REQHBASE
C     LOGICAL AL_SQNC_HVALIDE
C     INTEGER AL_SQNC_ADDITEM
C     INTEGER AL_SQNC_REQDIM
C     INTEGER AL_SQNC_REQHITM
C     INTEGER AL_SQNC_XEQ
C     INTEGER AL_SQNC_XEQST
C     INTEGER AL_SQNC_XEQTI
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_SQNC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQNC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqnc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_SQNC_NOBJMAX,
     &                   AL_SQNC_HBASE,
     &                   'Algorithm Sequence')

      AL_SQNC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQNC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqnc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER  IERR
      EXTERNAL AL_SQNC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_SQNC_NOBJMAX,
     &                   AL_SQNC_HBASE,
     &                   AL_SQNC_DTR)

      AL_SQNC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_SQNC_NOBJMAX,
     &                   AL_SQNC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_SQNC_HVALIDE(HOBJ))
         IOB = HOBJ - AL_SQNC_HBASE

         AL_SQNC_HLIST(IOB) = 0
      ENDIF

      AL_SQNC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_SQNC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_SQNC_NOBJMAX,
     &                   AL_SQNC_HBASE)

      AL_SQNC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_SQNC_RST(HOBJ)

C---     CONSTRUIS LA LISTE
      HLIST = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLIST)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HLIST)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_SQNC_HBASE
         AL_SQNC_HLIST(IOB) = HLIST
      ENDIF

C---     NETTOIE
      IF (ERR_BAD() .AND. DS_LIST_HVALIDE(HLIST))
     &   IERR = DS_LIST_DTR(HLIST)

      AL_SQNC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ISEQ, NSEQ
      INTEGER HLIST, HITEM
      LOGICAL EST_PROPRIO
      CHARACTER*(32) VAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - AL_SQNC_HBASE
      HLIST = AL_SQNC_HLIST(IOB)

C---     Détruis les items de la séquence
      IF (DS_LIST_HVALIDE(HLIST)) THEN
         NSEQ = DS_LIST_REQDIM(HLIST)
         DO ISEQ=1,NSEQ
            IERR = DS_LIST_REQVAL(HLIST, ISEQ, VAL)
D           CALL ERR_ASR(ERR_GOOD())
            READ(VAL, *) HITEM, EST_PROPRIO
            IF (EST_PROPRIO) IERR = AL_SQIT_DTR(HITEM)
         ENDDO
      ENDIF

C---     Détruis la liste
      IF (DS_LIST_HVALIDE(HLIST)) IERR = DS_LIST_DTR(HLIST)
      AL_SQNC_HLIST(IOB) = 0

      AL_SQNC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_SQNC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQNC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqnc.fi'
      INCLUDE 'alsqnc.fc'
C------------------------------------------------------------------------

      AL_SQNC_REQHBASE = AL_SQNC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_SQNC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQNC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alsqnc.fc'
C------------------------------------------------------------------------

      AL_SQNC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_SQNC_NOBJMAX,
     &                                  AL_SQNC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un algorithme
C
C Description:
C     La méthode AL_SQNC_ADDALGO permet d'ajouter un algorithme à la
C     séquence. Elle construit un item de séquence qui est ajouté à la 
C     liste des items de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_ADDALGO(HOBJ,
     &                         HSIMD,
     &                         HALGO,
     &                         HTRIG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_ADDALGO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIMD
      INTEGER HALGO
      INTEGER HTRIG

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST, HITEM
      LOGICAL EST_PROPRIO
      CHARACTER*(32) VAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - AL_SQNC_HBASE
      HLIST = AL_SQNC_HLIST(IOB)

C---     CRÉE L'ITEM
      HITEM = 0
      IF (ERR_GOOD()) IERR = AL_SQIT_CTR  (HITEM)
      IF (ERR_GOOD()) IERR = AL_SQIT_INIAL(HITEM,
     &                                     HSIMD,
     &                                     HALGO,
     &                                     HTRIG)

C---     AJOUTE L'ITEM
      IF (ERR_GOOD()) THEN
         EST_PROPRIO = .TRUE.
         WRITE(VAL,*) HITEM, ', ', EST_PROPRIO
         IERR = DS_LIST_AJTVAL(HLIST, VAL)
      ENDIF

C---     NETTOIE
      IF (ERR_BAD() .AND. AL_SQIT_HVALIDE(HITEM))
     &   IERR = AL_SQIT_DTR(HITEM)

      AL_SQNC_ADDALGO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un post-traitement
C
C Description:
C     La méthode AL_SQNC_ADDALGO permet d'ajouter un post-traitement à la
C     séquence. Elle construit un item de séquence qui est ajouté à la 
C     liste des items de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_ADDPOST(HOBJ,
     &                         HSIMD,
     &                         HPOST,
     &                         HTRIG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_ADDPOST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIMD
      INTEGER HPOST
      INTEGER HTRIG

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST, HITEM
      LOGICAL EST_PROPRIO
      CHARACTER*(32) VAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - AL_SQNC_HBASE
      HLIST = AL_SQNC_HLIST(IOB)

C---     Crée l'item
      HITEM = 0
      IF (ERR_GOOD()) IERR = AL_SQIT_CTR  (HITEM)
      IF (ERR_GOOD()) IERR = AL_SQIT_INIPS(HITEM,
     &                                     HSIMD,
     &                                     HPOST,
     &                                     HTRIG)

C---     Ajoute l'item
      IF (ERR_GOOD()) THEN
         EST_PROPRIO = .TRUE.
         WRITE(VAL,*) HITEM, ', ', EST_PROPRIO
         IERR = DS_LIST_AJTVAL(HLIST, VAL)
      ENDIF

C---     NETTOIE
      IF (ERR_BAD() .AND. AL_SQIT_HVALIDE(HITEM))
     &   IERR = AL_SQIT_DTR(HITEM)

      AL_SQNC_ADDPOST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute un item
C
C Description:
C     La méthode AL_SQNC_ADDITEM permet d'ajouter un item à la
C     séquence. Elle ajoute l'item à la liste des items de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_ADDITEM(HOBJ,
     &                         HITEM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_ADDITEM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HITEM

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
      LOGICAL EST_PROPRIO
      CHARACTER*(32) VAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - AL_SQNC_HBASE
      HLIST = AL_SQNC_HLIST(IOB)

C---     Contrôle
      IF (.NOT. AL_SQIT_HVALIDE(HITEM)) GOTO 9900

C---     AJOUTE L'ITEM
      IF (ERR_GOOD()) THEN
         EST_PROPRIO = .FALSE.
         WRITE(VAL,*) HITEM, ', ', EST_PROPRIO
         IERR = DS_LIST_AJTVAL(HLIST, VAL)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HITEM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_SQNC_ADDITEM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_REQDIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_REQDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER HLIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HLIST = AL_SQNC_HLIST(HOBJ - AL_SQNC_HBASE)
      AL_SQNC_REQDIM = DS_LIST_REQDIM(HLIST)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_REQHITM(HOBJ, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_REQHITM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IND

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER HLIST, HITEM
      CHARACTER*(32) VAL
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(IND .GT. 0)
D     CALL ERR_PRE(IND .LE. AL_SQNC_REQDIM(HOBJ))
C------------------------------------------------------------------------

      HLIST = AL_SQNC_HLIST(HOBJ - AL_SQNC_HBASE)
      IERR = DS_LIST_REQVAL(HLIST, IND, VAL)
      IF (ERR_GOOD()) THEN
         READ(VAL, *) HITEM
      ELSE
         HITEM = 0
      ENDIF

      AL_SQNC_REQHITM = HITEM
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_SQNC_XEQ(...) exécute la séquence. Chaque item
C     est appelé tour à tour et résolu par son algorithme propre.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     CHARACTER*(*) IPRM   Paramètres particuliers à l'algo
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_XEQ(HOBJ, HTMR, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_SQNC_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HTMR
      CHARACTER*(*) IPRM

      INCLUDE 'alsqnc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'trcdwn.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER NTOK
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(HTMR .EQ. 0 .OR. TR_CDWN_HVALIDE(HTMR))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C---     Dispatch
      NTOK = SP_STRN_NTOK(IPRM, ',')
      IF     (NTOK .EQ. 1) THEN
         IERR = AL_SQNC_XEQST(HOBJ, HTMR, IPRM)
      ELSEIF (NTOK .EQ. 3) THEN
         IERR = AL_SQNC_XEQTI(HOBJ, HTMR, IPRM)
      ELSE
         GOTO 9901
      ENDIF

C---     Gère les timer 
      IF (ERR_ESTMSG('ERR_TEMPS_ECOULE')) THEN
         CALL ERR_AJT('MSG_STOP_SEQUENCE')
         CALL ERR_AJT('MSG_CONTINUE')
         CALL LOG_MSG_ERR()
         CALL ERR_RESET()
      ENDIF
      
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_SQNC_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée AL_SQNC_XEQST(...) exécute l'algorithme stationnaire
C     pour résoudre la séquence de simulations. Chaque item de la
C     séquence est appelé tour à tour et résolu par son algorithme
C     propre.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     INTEGER HSEQ         Handle sur la séquence de simulations
C     CHARACTER*(*) IPRM   Paramètres particuliers à l'algo
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_SQNC_XEQST(HOBJ, HTMR, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HTMR
      CHARACTER*(*) IPRM

      INCLUDE 'alsqnc.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER HITM
      INTEGER HSIM
      INTEGER ISEQ, NSEQ
      REAL*8  TINI, DELT
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso')

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.sequence'

C---     Décode les paramètres
      DELT = 0.0D0
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, TINI)
      IF (IERR .NE. 0) GOTO 9900
      IF (TINI .LT. 0.0D0) GOTO 9901

C---     Contrôles
      NSEQ = AL_SQNC_REQDIM(HOBJ)
      DO ISEQ=1,NSEQ
         HITM = AL_SQNC_REQHITM(HOBJ, ISEQ)
         HSIM = AL_SQIT_REQHSIM(HITM)
         IF (.NOT. LM_HELE_ESTINI(HSIM)) GOTO 9903
      ENDDO

C---     ECRIS L'ENTETE
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_RESOLUTION_STATIONNAIRE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TINI
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Le traceur
      IERR = TRA_ASGTSIM(TINI)

C---     Itère sur les items de la séquence
      NSEQ = AL_SQNC_REQDIM(HOBJ)
      DO ISEQ=1,NSEQ
         IF (NSEQ .GT. 1) THEN
            CALL LOG_INFO(LOG_ZNE, ' ')
            WRITE(LOG_BUF, '(A,I6,A,I6)') 'MSG_SEQ_ITEM: ',
     &                                        ISEQ,'/',NSEQ
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         ENDIF

         IERR = TRA_ASGISEQ(ISEQ)            ! Traceur

         HITM = AL_SQNC_REQHITM(HOBJ, ISEQ)
         IERR = AL_SQIT_XEQ(HITM, TINI, DELT)

         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

      CALL LOG_DECIND()
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_TEMPS_INVALIDE',': ', TINI
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_SIMD_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12)') 'MSG_HANDLE', ': ', HSIM
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.reso')
      AL_SQNC_XEQST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Séquence non stationnaire
C
C Description:
C     La méthode privée AL_SQNC_XEQTI
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_SQNC_XEQTI(HOBJ, HTMR, IPRM)

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HTMR
      CHARACTER*(*) IPRM

      INCLUDE 'alsqnc.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tra.fi'
      INCLUDE 'trcdwn.fi'
      INCLUDE 'alsqnc.fc'

      INTEGER IERR
      INTEGER HITM
      INTEGER HSIM
      INTEGER ISEQ, NSEQ
      INTEGER IPAS, NPAS
      REAL*8  DELT, DPAS, TINI, TSIM
      REAL*8  DT_C, TS_C
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_SQNC_HVALIDE(HOBJ))
D     CALL ERR_PRE(HTMR .EQ. 0 .OR. TR_CDWN_HVALIDE(HTMR))
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.reso')

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.sequence'

C---     Décode les paramètres
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, TINI)
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, DELT)
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, DPAS)
      IF (IERR .NE. 0) GOTO 9900
      IF (DELT .LE. 0) GOTO 9901
      NPAS = NINT(DPAS)
      IF (NPAS .LE. 0) GOTO 9902
      IF (ABS(DPAS-DBLE(NPAS)) .GT. 1.0D-8) THEN
         CALL LOG_WRN(LOG_ZNE, 'MSG_NPAS_VALEUR_REELLE_TRONQUE')
      ENDIF

C---     Contrôles
      NSEQ = AL_SQNC_REQDIM(HOBJ)
      DO ISEQ=1,NSEQ
         HITM = AL_SQNC_REQHITM(HOBJ, ISEQ)
         HSIM = AL_SQIT_REQHSIM(HITM)
         IF (.NOT. LM_HELE_ESTINI(HSIM)) GOTO 9903
      ENDDO

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_RESOLUTION_NON_STATIONNAIRE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      TSIM = TINI + NPAS*DELT
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TINI
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DELT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,I12)') 'MSG_NBR_PAS#<35>#', '= ', NPAS
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C        ---------------------------
C---     BOUCLE SUR LES PAS DE TEMPS
C        ---------------------------
      DO IPAS=1, NPAS
         
C---        Contrôle le timer
         IF (HTMR .NE. 0) THEN
            IF (TR_CDWN_ESTFINI(HTMR)) GOTO 9904
         ENDIF

C---        Log l'entête         
         TSIM = TINI + DELT*(IPAS-1)
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6,A,I6,2(A,F25.6))')
     &      'MSG_PAS_DE_TEMPS: ', IPAS, '/', NPAS,
     &      ', MSG_TINI=', TSIM,
     &      ', MSG_TFIN=', TSIM+DELT
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

         IERR = TRA_ASGTSIM(TSIM)                ! Traceur

C---        Itère sur les items de la séquence
         NSEQ = AL_SQNC_REQDIM(HOBJ)
         DO ISEQ=1,NSEQ
            IF (NSEQ .GT. 1) THEN
               CALL LOG_INFO(LOG_ZNE, ' ')
               WRITE(LOG_BUF, '(A,I6,A,I6)') 'MSG_SEQ_ITEM: ',
     &                                        ISEQ,'/',NSEQ
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            ENDIF

            DT_C = DELT
            TS_C = TSIM
            HITM = AL_SQNC_REQHITM(HOBJ, ISEQ)
            IF (ERR_GOOD()) IERR = TRA_ASGISEQ(ISEQ)    ! Traceur
            IF (ERR_GOOD()) IERR = AL_SQIT_XEQ(HITM, TS_C, DT_C)
            CALL ERR_ASR(ERR_BAD() .OR.
     &                   ABS((TS_C+DT_C)-(TSIM+DELT)) .LT. 1.0D-12)
            IF (ERR_BAD()) EXIT
         ENDDO

         CALL LOG_DECIND()
         IF (ERR_BAD()) EXIT
      ENDDO    ! Boucle sur les pas de temps

      CALL LOG_DECIND()
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_DELT_INVALIDE',': ',DELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NPAS_INVALIDE',': ', NPAS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_SIMD_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12)') 'MSG_HANDLE', ': ', HSIM
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_TEMPS_ECOULE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.reso')
      AL_SQNC_XEQTI = ERR_TYP()
      RETURN
      END
