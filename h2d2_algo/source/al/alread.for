C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   READ
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_READ_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_READ_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alread.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_READ_NOBJMAX,
     &                   AL_READ_HBASE,
     &                   'Algorithm Read')

      AL_READ_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_READ_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alread.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER  IERR
      EXTERNAL AL_READ_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_READ_NOBJMAX,
     &                   AL_READ_HBASE,
     &                   AL_READ_DTR)

      AL_READ_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_READ_NOBJMAX,
     &                   AL_READ_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_READ_HVALIDE(HOBJ))
         IOB = HOBJ - AL_READ_HBASE

         AL_READ_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_READ_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_READ_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_READ_NOBJMAX,
     &                   AL_READ_HBASE)

      AL_READ_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_READ_RST(HOBJ)

C---     ASSIGNE LES ATTRIBUTS
      IOB = HOBJ - AL_READ_HBASE
      AL_READ_STTS(IOB) = AL_STTS_INDEFINI

      AL_READ_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - AL_READ_HBASE

C---     RESET
      AL_READ_STTS(IOB) = AL_STTS_INDEFINI

      AL_READ_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_READ_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_READ_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alread.fi'
      INCLUDE 'alread.fc'
C------------------------------------------------------------------------

      AL_READ_REQHBASE = AL_READ_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_READ_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_READ_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alread.fc'
C------------------------------------------------------------------------

      AL_READ_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_READ_NOBJMAX,
     &                                  AL_READ_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_READ_RESOUS intègre dans le temps par une méthode d'Euler
C     la simulation HSIM. Globalement, elle amène HSIM de TSIM à TSIM+DELT.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur la simulation
C
C Sortie:
C
C Notes:
C    1) Les dimensions comme NEQ sont assignées dans PRCPROP
C    2) En sortie, les dll sont à t+dt, mais les prop ne sont pas toutes
C       ajustées à la dernière valeur des ddl. En fait elles sont une
C       itération en retard.
C************************************************************************
      FUNCTION AL_READ_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alread.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alread.fc'

      INTEGER IERR

      EXTERNAL AL_READ_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM)    ! cf. note
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_READ_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_READ_DUMMY,
     &                           AL_READ_DUMMY,
     &                           AL_READ_DUMMY,
     &                           AL_READ_DUMMY,
     &                           AL_READ_DUMMY)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_READ_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_READ_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur la simulation
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + a.K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + a.KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + a.K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C     En sortie, HSIM est à t+dt
C************************************************************************
      FUNCTION AL_READ_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_MDTK,
     &                          F_MDTKT,
     &                          F_MDTKU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_MDTK
      INTEGER  F_MDTKT
      INTEGER  F_MDTKU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_MDTK
      EXTERNAL F_MDTKT
      EXTERNAL F_MDTKU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alread.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'alread.fc'

      REAL*8  TFIN

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.read'

C---     Récupère les attributs
      IOB = HOBJ - AL_READ_HBASE

C---     Calcule les paramètres
      TFIN = TSIM + DELT

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_READ_SOLUTION'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TFIN#<35>#', ' = ', TFIN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TFIN)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLC   (HSIM)

C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD()) THEN
         DELT = 0.0D0
         TSIM = TFIN
      ENDIF

C---     Conserve le résultat
      IF (ERR_GOOD()) AL_READ_STTS(IOB) = AL_STTS_OK

      CALL LOG_DECIND()
      AL_READ_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_READ_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_READ_REQSTATUS = AL_READ_STTS(HOBJ - AL_READ_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_READ_DUMMY(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alread.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alread.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_READ_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'AL_READ_DUMMY shall never be called')
      CALL ERR_ASR(.TRUE.)

      AL_READ_DUMMY = ERR_TYP()
      RETURN
      END

