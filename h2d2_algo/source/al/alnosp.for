C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   NOde Save Point
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_NOSP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOSP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnosp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_NOSP_NOBJMAX,
     &                   AL_NOSP_HBASE,
     &                   'Algorithm Node Save-Point')

      AL_NOSP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOSP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnosp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'

      INTEGER  IERR
      EXTERNAL AL_NOSP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_NOSP_NOBJMAX,
     &                   AL_NOSP_HBASE,
     &                   AL_NOSP_DTR)

      AL_NOSP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOSP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnosp.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_NOSP_NOBJMAX,
     &                   AL_NOSP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_NOSP_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NOSP_HBASE

         AL_NOSP_HNXT(IOB) = 0
         AL_NOSP_HSET(IOB) = 0
         AL_NOSP_HSIM(IOB) = 0
         AL_NOSP_IDSP(IOB) = 0
         AL_NOSP_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_NOSP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOSP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnosp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_NOSP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_NOSP_NOBJMAX,
     &                   AL_NOSP_HBASE)

      AL_NOSP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     La méthode AL_NOSP_INI initialise et dimensionne l'objet. Celui-ci a
C     deux configurations:
C        - Si HSET est nul, alors l'objet va activer un SavePoint
C        - si HSET est non nul, alors l'objet va restaurer l'état à
C          celui maintenu par HSET
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HNXT        Handle sur le prochain noeud
C     HSET        Handle sur l'objet des données du SavePoint
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOSP_INI(HOBJ, HNXT, HSET)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSET
      INTEGER HNXT

      INCLUDE 'alnosp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnosp.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_NOSP_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - AL_NOSP_HBASE

C---     CONTROLE LES DONNÉES
      IF (.NOT. (HNXT .EQ. 0 .OR. AL_SLVR_HVALIDE(HNXT))) GOTO 9900
      IF (.NOT. (HSET. EQ. 0 .OR. AL_NOSP_HVALIDE(HSET))) GOTO 9901

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         AL_NOSP_HNXT(IOB) = HNXT
         AL_NOSP_HSET(IOB) = HSET
         AL_NOSP_HSIM(IOB) = 0
         AL_NOSP_IDSP(IOB) = 0
         AL_NOSP_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HNXT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNXT, 'MSG_NOEUD_SUIVANT')
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HSET
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HSET, 'MSG_SAVE_POINT')
      GOTO 9999

9999  CONTINUE
      AL_NOSP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOSP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnosp.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NOSP_HBASE

C---     RESET LES ATTRIBUTS
      AL_NOSP_HNXT(IOB) = 0
      AL_NOSP_HSET(IOB) = 0
      AL_NOSP_HSIM(IOB) = 0
      AL_NOSP_IDSP(IOB) = 0
      AL_NOSP_STTS(IOB) = AL_STTS_INDEFINI

      AL_NOSP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_NOSP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOSP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnosp.fi'
      INCLUDE 'alnosp.fc'
C------------------------------------------------------------------------

      AL_NOSP_REQHBASE = AL_NOSP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_NOSP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOSP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnosp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnosp.fc'
C------------------------------------------------------------------------

      AL_NOSP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_NOSP_NOBJMAX,
     &                                  AL_NOSP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C     Le SP est libéré après l'exécution du fils, pour assurer qu'il est
C     disponible
C************************************************************************
      FUNCTION AL_NOSP_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alnosp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnosp.fc'

      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER HSET, HNOD
      INTEGER ISTTS
      INTEGER LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOSP_HBASE
      HSET = AL_NOSP_HSET(IOB)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Set ou Restaure
      IF (HSET .EQ. 0) THEN
         HSVPT = 0
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
         IF (ERR_GOOD()) THEN
            AL_NOSP_IDSP(IOB) = HSVPT
            AL_NOSP_HSIM(IOB) = HSIM
         ENDIF

      ELSE
         HSVPT = AL_NOSP_IDSP (HSET-AL_NOSP_HBASE)
D        CALL ERR_ASR(HSVPT .NE. 0)
D        CALL ERR_ASR(HSIM .EQ. AL_NOSP_IDSP(HSET-AL_NOSP_HBASE))
         IERR = SO_SVPT_RBCK(HSVPT)
      ENDIF

C---     Résous le fils
      HNOD = AL_NOSP_HNXT(IOB)
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS(HNOD, TSIM, DELT, HSIM)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         IERR = SO_SVPT_DTR(HSVPT)
         AL_NOSP_IDSP(IOB) = HSVPT
         AL_NOSP_HSIM(IOB) = HSIM
      ENDIF

C---     Conserve le résultat
      AL_NOSP_STTS(IOB) = ISTTS

      AL_NOSP_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_NOSP_RESOUS_E(...) est la méthode de bas niveau pour
C     la résolution.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOSP_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          H_F,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  H_F
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alnosp.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnosp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER HSET, HNOD
      INTEGER ISTTS
      INTEGER LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOSP_HBASE
      HSET = AL_NOSP_HSET(IOB)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Set ou Restaure
      IF (HSET .EQ. 0) THEN
         HSVPT = 0
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
         IF (ERR_GOOD()) THEN
            AL_NOSP_IDSP(IOB) = HSVPT
            AL_NOSP_HSIM(IOB) = HSIM
         ENDIF

      ELSE
         HSVPT = AL_NOSP_IDSP (HSET-AL_NOSP_HBASE)
D        CALL ERR_ASR(HSVPT .NE. 0)
D        CALL ERR_ASR(HSIM .EQ. AL_NOSP_IDSP(HSET-AL_NOSP_HBASE))
         IERR = SO_SVPT_RBCK(HSVPT)
      ENDIF

C---     Résous le fils
      HNOD = AL_NOSP_HNXT(IOB)
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS_E(HNOD,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           H_F,
     &                           F_K,
     &                           F_KT,
     &                           F_KU,
     &                           F_F,
     &                           F_RES)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) THEN
         IERR = SO_SVPT_DTR(HSVPT)
         AL_NOSP_IDSP(IOB) = HSVPT
         AL_NOSP_HSIM(IOB) = HSIM
      ENDIF

C---     Conserve le résultat
      AL_NOSP_STTS(IOB) = ISTTS

      AL_NOSP_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOSP_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOSP_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alnosp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnosp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOSP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NOSP_REQSTATUS = AL_NOSP_STTS(HOBJ - AL_NOSP_HBASE)
      RETURN
      END
