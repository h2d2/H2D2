C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_ALSQIP_CMD
C     CHARACTER*(32) IC_ALSQIP_REQCMD
C   Private:
C     SUBROUTINE IC_ALSQIP_AID
C     INTEGER IC_ALSQIP_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIP_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIP_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alsqip_ic.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqip_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HELE, HPST, HTRG
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALSQIP_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---        LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C     <comment>Handle on the post-treatment</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HPST)
C     <comment>Handle on the post-treatment trigger (default 0)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HTRG)
      IF (IERR .NE. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = AL_SQIT_CTR  (HOBJ)
      IF (ERR_GOOD()) IERR = AL_SQIT_INIPS(HOBJ,
     &                                     HELE,
     &                                     HPST,
     &                                     HTRG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_ALSQIP_PRN(HOBJ)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the sequence item</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>algo_sequence_post</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_AUCUN_PARAMETRE_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALSQIP_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_ALSQIP_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIP_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALSQIP_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alsqip_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>algo_sequence_post</b> represents an post-treatment item
C  in a sequence of simulations. Items are to be solved one after the other.
C</comment>
      IC_ALSQIP_REQCMD = 'algo_sequence_post'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALSQIP_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alsqip_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALSQIP_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alsqip_ic.fi'
      INCLUDE 'alsqit.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alsqip_ic.fc'

      INTEGER IERR
      INTEGER HNDL
      INTEGER LTXT
      CHARACTER*(256) TXT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_ITEM_SEQUENCE_POST_TREATMENT'
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHSIM(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHPST(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      TXT = "---"
      HNDL = AL_SQIT_REQHTRG(HOBJ)
      IF (HNDL .NE. 0) IERR = OB_OBJC_REQNOMCMPL(TXT, HNDL)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_HANDLE#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_ALSQIP_PRN = ERR_TYP()
      RETURN
      END
