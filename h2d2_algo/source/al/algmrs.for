C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   GMReS
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_GMRS
C         SUBROUTINE AL_GMRS_000
C         SUBROUTINE AL_GMRS_999
C         SUBROUTINE AL_GMRS_CTR
C         SUBROUTINE AL_GMRS_DTR
C         SUBROUTINE AL_GMRS_INI
C         SUBROUTINE AL_GMRS_RST
C         SUBROUTINE AL_GMRS_REQHBASE
C         SUBROUTINE AL_GMRS_HVALIDE
C         SUBROUTINE AL_GMRS_RESOUS
C         SUBROUTINE AL_GMRS_RESOUS_E
C         SUBROUTINE AL_GMRS_REQSTATUS
C         SUBROUTINE AL_GMRS_ASMKT
C         SUBROUTINE AL_GMRS_ASMKU
C         SUBROUTINE AL_GMRS_ASMRES
C         SUBROUTINE AL_GMRS_DUMMY
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_GMRS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_GMRS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_GMRS_NOBJMAX,
     &                   AL_GMRS_HBASE,
     &                   'Algorithm GMRes')

      AL_GMRS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_GMRS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'algmrs.fc'

      INTEGER  IERR
      EXTERNAL AL_GMRS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_GMRS_NOBJMAX,
     &                   AL_GMRS_HBASE,
     &                   AL_GMRS_DTR)

      AL_GMRS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_GMRS_NOBJMAX,
     &                   AL_GMRS_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_GMRS_HVALIDE(HOBJ))
         IOB = HOBJ - AL_GMRS_HBASE

         AL_GMRS_EPS(1,IOB) =  0.0D0
         AL_GMRS_EPS(2,IOB) =  0.0D0
         AL_GMRS_EPS(3,IOB) =  0.0D0
         AL_GMRS_EPS(4,IOB) = -1.0D0
         AL_GMRS_EPS(5,IOB) = -1.0D0
         AL_GMRS_EPS(6,IOB) = -1.0D0
         AL_GMRS_HCRA (IOB) = 0
         AL_GMRS_HCRC (IOB) = 0
         AL_GMRS_HGLB (IOB) = 0
         AL_GMRS_HPRE (IOB) = 0
         AL_GMRS_HSIM (IOB) = 0
         AL_GMRS_NPREC(IOB) = 0
         AL_GMRS_NREDM(IOB) = 0
         AL_GMRS_NITER(IOB) = 0
         AL_GMRS_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_GMRS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'algmrs.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_GMRS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_GMRS_NOBJMAX,
     &                   AL_GMRS_HBASE)

      AL_GMRS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_INI(HOBJ,
     &                     HCRA,
     &                     HCRC,
     &                     HGLB,
     &                     HPRE,
     &                     NPREC,
     &                     NREDM,
     &                     NITER,
     &                     EPS0,
     &                     EPS11,
     &                     EPS12)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCRA
      INTEGER HCRC
      INTEGER HGLB
      INTEGER HPRE
      INTEGER NPREC
      INTEGER NREDM
      INTEGER NITER
      REAL*8  EPS0
      REAL*8  EPS11
      REAL*8  EPS12

      INCLUDE 'algmrs.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.solver.gmres'

C---     Contrôle des paramètres
      IF (HCRA .NE. 0 .AND. .NOT. CA_CRIA_HVALIDE(HCRA)) GOTO 9900
      IF (HCRC .NE. 0 .AND. .NOT. CC_CRIC_HVALIDE(HCRC)) GOTO 9901
      IF (HGLB .NE. 0 .AND. .NOT. GL_GLBL_HVALIDE(HGLB)) GOTO 9902
      IF (.NOT. PR_PREC_CTRLH(HPRE)) GOTO 9903
      IF (NPREC .LT. 0) GOTO 9904
      IF (NREDM .LE. 0) GOTO 9905
      IF (NITER .LE. 0) GOTO 9906
      IF (EPS0  .LE. 0.0D0) GOTO 9907
      IF (EPS11 .LT. 0.0D0) GOTO 9908
      IF (EPS12 .LT. 0.0D0) GOTO 9909

C---     Contrôles
      IF (ERR_GOOD()) THEN
         IF (MP_UTIL_ESTMPROC()) THEN
            CALL ERR_ASG(ERR_ERR, 'ERR_METHODE_RESO_INVALIDE_EN_MPROC')
         ENDIF
      ENDIF

C---     Reset les données
      IERR = AL_GMRS_RST(HOBJ)

C---     Enregistre les données
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - AL_GMRS_HBASE
         AL_GMRS_EPS(1,IOB) = EPS0
         AL_GMRS_EPS(2,IOB) = EPS11
         AL_GMRS_EPS(3,IOB) = EPS12
         AL_GMRS_HCRA (IOB) = HCRA
         AL_GMRS_HCRC (IOB) = HCRC
         AL_GMRS_HGLB (IOB) = HGLB
         AL_GMRS_HPRE (IOB) = HPRE
         AL_GMRS_NPREC(IOB) = NPREC
         AL_GMRS_NREDM(IOB) = NREDM
         AL_GMRS_NITER(IOB) = NITER
         AL_GMRS_STTS (IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCRA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCRA, 'MSG_CRITERE_ARRET')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HCRC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCRC, 'MSG_CRITERE_CONVERGENCE')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HGLB
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HGLB, 'MSG_GLOBALISATION')
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ',HPRE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HPRE, 'MSG_PRECONDITIONNEMENT')
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_PRECOND_INVALIDE',': ',NPREC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_REDEM_INVALIDE',': ',NREDM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_ITERATION_INVALIDE',': ',NITER
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9907  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_EPS0_INVALIDE',': ',EPS0
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9908  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_EPS11_INVALIDE',': ',EPS11
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9909  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_EPS12_INVALIDE',': ',EPS12
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_GMRS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les attributs
      IOB = HOBJ - AL_GMRS_HBASE
      AL_GMRS_EPS(6,IOB) = -1.0D0
      AL_GMRS_HCRA (IOB) = 0
      AL_GMRS_HCRC (IOB) = 0
      AL_GMRS_HGLB (IOB) = 0
      AL_GMRS_HPRE (IOB) = 0
      AL_GMRS_HSIM (IOB) = 0
      AL_GMRS_NPREC(IOB) = 0
      AL_GMRS_NREDM(IOB) = 0
      AL_GMRS_NITER(IOB) = 0
      AL_GMRS_STTS (IOB) = AL_STTS_INDEFINI

      AL_GMRS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_GMRS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_GMRS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'algmrs.fi'
      INCLUDE 'algmrs.fc'
C------------------------------------------------------------------------

      AL_GMRS_REQHBASE = AL_GMRS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_GMRS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_GMRS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'algmrs.fc'
C------------------------------------------------------------------------

      AL_GMRS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_GMRS_NOBJMAX,
     &                                  AL_GMRS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Résolution par un algorithme de GMRes.
C
C Description:
C     La méthode AL_GMRS_RESOUS(...) résout la simulation par un algorithme
C     itératif de GMRes non linéaire.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM         Handle sur l'élément
C     INTEGER HCRA         Handle sur le critère d'arrêt
C     INTEGER HLTR         Handle sur le limiteur
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_GMRS_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB

      EXTERNAL AL_GMRS_ASMKT
      EXTERNAL AL_GMRS_ASMKU
      EXTERNAL AL_GMRS_ASMRES
      EXTERNAL AL_GMRS_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Conserve les paramètres pour la simulation
      IOB  = HOBJ - AL_GMRS_HBASE
      AL_GMRS_HSIM(IOB) = HSIM

C---     Charge les données et pré-traitement avant calcul
      IF (ERR_GOOD()) IERR = LM_ELEM_PASDEB(HSIM, TSIM+DELT)
      IF (ERR_GOOD()) IERR = LM_ELEM_CLCPRE(HSIM)

C---     Résous
      IF (ERR_GOOD())
     &   IERR = AL_GMRS_RESOUS_E(HOBJ,
     &                           TSIM,
     &                           DELT,
     &                           HSIM,
     &                           HOBJ,
     &                           AL_GMRS_DUMMY, ! K
     &                           AL_GMRS_ASMKT,
     &                           AL_GMRS_ASMKU,
     &                           AL_GMRS_DUMMY, ! F
     &                           AL_GMRS_ASMRES)

C---     Post-calcul des données (TSIM mis à jour)
      IF (ERR_GOOD()) IERR = LM_ELEM_PASFIN(HSIM, TSIM)

      AL_GMRS_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_GMRS_RESOUS_E(...) est la méthode de bas niveau pour
C     résoudre une simulation par un algorithme itératif de GMRes non
C     linéaire..
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_K         Fonction de calcul de [K]
C     EXTERNAL F_KT        Fonction de calcul de [KT]
C     EXTERNAL F_KU        Fonction de calcul de [K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C     Les propriétés ne sont pas réactualisées sur les itérations
C************************************************************************
      FUNCTION AL_GMRS_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          HALG,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'algmrs.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'alutil.fi'
      INCLUDE 'c_dh.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'cccric.fi'
      INCLUDE 'glglbl.fi'
      INCLUDE 'glstts.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'prprec.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spadft.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'spgmrs.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IP, IR, I
      INTEGER ISTTS
      INTEGER HNUMR, HCRA, HCRC, HGLB, HPRE
      INTEGER KTRV(8)
      INTEGER LDLG, LLOCN
      INTEGER L_SOL, L_DLU, L_DLG0
      INTEGER NDLN, NNL, NDLL, NEQL
      INTEGER NPREC, NREDM, NITER
      REAL*8  EPSGMR
      REAL*8  RSTAT(7)           ! Stats sur les redem
      REAL*8  PSTAT(7)           ! Stats sur les precond
      REAL*8  GSTAT(7)           ! Stats globales
      REAL*8  DELTEMPS
D     REAL*8  DTMP
      LOGICAL ESTCNVGR
      LOGICAL ESTRSLUR, ESTFINR, ESTMODIFR
      LOGICAL ESTRSLUP, ESTFINP  !, ESTMODIFP
      LOGICAL ACRC, ACRA, AGLB
      CHARACTER*(256) NOM

      EXTERNAL PR_PREC_PRC
C------------------------------------------------------------------------
      INTEGER II
      LOGICAL IS_GL
      IS_GL(II) = BTEST(GL_GLBL_REQSTATUS(HGLB), II)
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.algo.solver.gmres'

C---     Récupère les attributs
      IOB = HOBJ - AL_GMRS_HBASE
      HCRA  = AL_GMRS_HCRA (IOB)
      HCRC  = AL_GMRS_HCRC (IOB)
      HGLB  = AL_GMRS_HGLB (IOB)
      HPRE  = AL_GMRS_HPRE (IOB)
      NPREC = AL_GMRS_NPREC(IOB)
      NREDM = AL_GMRS_NREDM(IOB)
      NITER = AL_GMRS_NITER(IOB)

C---     Récupère les param de la simulation
      NNL   = LM_ELEM_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NDLN  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NDLL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NDLL)
      NEQL  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)
      LLOCN = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LLOCN)

C---     Présence/absence des composantes
      ACRC = CC_CRIC_HVALIDE(HCRC)
      ACRA = CA_CRIA_HVALIDE(HCRA)
      AGLB = GL_GLBL_HVALIDE(HGLB)

C---     Alloue l'espace pour la solution
      L_DLU  = 0
      L_DLG0 = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, L_DLU)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NDLL, L_DLG0)

C---     Alloue l'espace de travail pour GMRES
      IF (ERR_GOOD()) IERR = SP_GMRS_ALLOC(NEQL, NITER, KTRV)

C---     Initialise les stats globales
      ESTCNVGR = .FALSE.
      ESTRSLUR = .FALSE.
      ESTFINR  = .FALSE.
      ESTRSLUP = .FALSE.
      ESTFINP  = .FALSE.
      DO I=1,6
         GSTAT(I)= 0.0D0
      ENDDO
      GSTAT(1)= NPREC*NREDM         ! Nb redem à faire
      GSTAT(3)= NPREC*NREDM*NITER   ! Nb iter à faire
      IERR = C_DH_TMRSTART(GSTAT(7))

C---     Écris l'entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_RESOLUTION_GMRES'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Écris les attributs pertinents
      IERR = OB_OBJC_REQNOMCMPL(NOM, HPRE)
      WRITE (LOG_BUF,'(2A,A)') 'MSG_HANDLE_PRECOND#<35>#', '= ',
     &                          NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_PRECOND#<35>#', '= ',
     &                           NPREC
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_REDEMARAGES#<35>#', '= ',
     &                            NREDM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_ITERATIONS#<35>#', '= ',
     &                           NITER
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON0#<35>#', '= ',
     &                           AL_GMRS_EPS(1,IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON11#<35>#', '= ',
     &                           AL_GMRS_EPS(2,IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE14.6E3)')'MSG_EPSILON12#<35>#', '= ',
     &                           AL_GMRS_EPS(3,IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C------------------------------------------
C---     BOUCLE SUR LES PRECONDITIONNEMENTS
C------------------------------------------
      RSTAT(6)= -1.0D-0
      DO IP=1, NPREC
         CALL LOG_INFO(LOG_ZNE, ' ')
         WRITE(LOG_BUF, '(A,I6)') 'GMRES PRECOND: ', IP
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         CALL LOG_INCIND()

C---        Assemble le préconditionnement
         IF (ERR_GOOD()) IERR=LM_ELEM_CLC(HSIM)
         IF (ERR_GOOD())
     &      IERR = PR_PREC_ASM(HPRE,
     &                         HSIM,
     &                         HALG,
     &                         F_KT,
     &                         F_KU)

C---        Adaptative forcing term
         AL_GMRS_EPS(6,IOB) = RSTAT(6)                     !RÉSIDU GMRES
         IF (ERR_GOOD())
     &      EPSGMR = SP_ADFT_EPS(AL_GMRS_EPS(1,IOB),
     &                           NDLL,
     &                           NEQL,
     &                           VA(SO_ALLC_REQVIND(VA,LDLG)),   ! VDLG
     &                           VA(SO_ALLC_REQVIND(VA,L_DLU)),  ! VDELU
     &                           HPRE,
     &                           PR_PREC_PRC,
     &                           HALG,
     &                           F_RES)

C---        Prend copie des DDL
         IF (ERR_GOOD())
     &      CALL DCOPY(NDLL,
     &                 VA(SO_ALLC_REQVIND(VA,LDLG)), 1,     ! VDLG
     &                 VA(SO_ALLC_REQVIND(VA,L_DLG0)), 1)   ! VDLG0

C---        Initialise les stats de préconditionnement
         DO I=1,6
            PSTAT(I) = 0.0D0
         ENDDO
         PSTAT(1) = NREDM              ! Nb redem à faire
         PSTAT(3) = NREDM*NITER        ! Nb iter à faire
         PSTAT(5) = AL_GMRS_EPS(5,IOB) ! Résidu initial
         IF (IP .EQ. 1) GSTAT(5) = PSTAT(5)

         IF (ERR_BAD()) GOTO 299

C---        Imprime le target
         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_RESIDU_INITIAL#<35>#: ',
     &                                  PSTAT(5)
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
         WRITE(LOG_BUF, '(A,1PE14.6E3)') 'MSG_EPSGMR#<35>#: ',
     &                                  EPSGMR
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---        BOUCLE SUR LES REDEMARRAGES
C---        ===========================
         DO IR=1, NREDM

            WRITE(LOG_BUF, '(A,I6)') 'RESTART: ', IR
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            CALL LOG_INCIND()

C---           Résous pour un redémarrage
            IF (ERR_GOOD()) IERR=LM_ELEM_CLC(HSIM)
            IF (ERR_GOOD()) THEN
               IERR = SP_GMRS_REDM(NITER,
     &                             NDLL,
     &                             NEQL,
     &                             KA(SO_ALLC_REQKIND(KA,LLOCN)),  !KLOCN
     &                             VA(SO_ALLC_REQVIND(VA,LDLG)),   !VDLG
     &                             VA(SO_ALLC_REQVIND(VA,L_DLU)),  !VDELU
     &                             EPSGMR,
     &                             RSTAT,
     &                             KTRV,
     &                             HPRE,
     &                             PR_PREC_PRC,
     &                             HALG,
     &                             F_RES)
            ENDIF

C---           Passe de NEQ a NDLL
C           GMRES retourne L_DLU directement en NDLN.
C           L'appel est donc superflu.

C---           Synchronise les process
            HNUMR = LM_ELEM_REQHNUMC(HSIM)
            IF (ERR_GOOD() .AND. NM_NUMR_CTRLH(HNUMR)) THEN
               IERR = NM_NUMR_DSYNC(HNUMR,
     &                              NDLN,
     &                              NNL,
     &                              VA(SO_ALLC_REQVIND(VA,L_DLU)))
            ENDIF

C---           Critère d'arrêt - avant limiteur
            ESTRSLUR = .FALSE.
            IF (ERR_GOOD() .AND. ACRA) THEN
               IERR = CA_CRIA_CALCRI(HCRA,
     &                               NDLN,
     &                               NNL,
     &                               VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                               VA(SO_ALLC_REQVIND(VA,L_DLU)))
               ESTRSLUR = CA_CRIA_ESTCONVERGE(HCRA)
            ENDIF

C---           Globalisation si non convergence
            ESTMODIFR = .FALSE.
            IF (ERR_GOOD() .AND. AGLB .AND. .NOT. ESTRSLUR) THEN
               IERR = GL_GLBL_XEQ(HGLB,
     &                            HSIM,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,LDLG)),
     &                            VA(SO_ALLC_REQVIND(VA,L_DLU)),
     &                            AL_GMRS_EPS(4,IOB), ! ETA_K
     &                            HALG,
     &                            F_RES)
               ESTMODIFR = IS_GL(GL_STTS_MODIF)
            ENDIF

C---           Critère de convergence numérique
            ESTCNVGR = .FALSE.
            IF (NINT(RSTAT(7)) .EQ. 1) ESTCNVGR = .TRUE.

C---           Critère global du redémarrage
            ESTFINR = (ESTCNVGR .OR. ESTRSLUR)

C---           Log l'incrément après limiteur
            IF (ERR_GOOD()) THEN
               IERR = AL_UTIL_LOGSOL(NDLN,
     &                               NNL,
     &                               VA(SO_ALLC_REQVIND(VA,L_DLU)),
     &                               HNUMR)
            ENDIF

C---           Mise à jour de la solution
            IF (ERR_GOOD()) THEN
               CALL DAXPY(NDLL,
     &                    1.0D0,
     &                    VA(SO_ALLC_REQVIND(VA,L_DLU)), 1,
     &                    VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
            ENDIF

C---           MAJ les stats de préconditionnement
            PSTAT(2)= PSTAT(2) + RSTAT(2)    ! Nb redem fait
            PSTAT(4)= PSTAT(4) + RSTAT(4)    ! Nb iter faites
            PSTAT(6)= RSTAT(6)               ! Résidu final
            PSTAT(7)= RSTAT(7)               ! Echec-Réussite

            CALL LOG_DECIND()
            IF (ERR_BAD()) GOTO 299
            IF (ESTFINR) GOTO 299
         ENDDO    ! Fin de la boucle sur les redémarrages
299      CONTINUE

C---        Calcule DELU du préconditionnement dans VDELU
         IF (ERR_GOOD()) THEN
            CALL DCOPY(NDLL,
     &                 VA(SO_ALLC_REQVIND(VA,LDLG)), 1,     ! VDLG
     &                 VA(SO_ALLC_REQVIND(VA,L_DLU)), 1)    ! VDELU
            CALL DAXPY(NDLL,
     &                 -1.0D0,
     &                 VA(SO_ALLC_REQVIND(VA,L_DLG0)),
     &                 1,
     &                 VA(SO_ALLC_REQVIND(VA,L_DLU)),
     &                 1)
            IERR = SP_ELEM_CLIM2VAL(NDLL,
     &                              KA(SO_ALLC_REQKIND(KA, LLOCN)),
     &                              0.0D0,
     &                              VA(SO_ALLC_REQVIND(VA, L_DLU)))
         ENDIF

C---        Critère d'arrêt - utilisateur
         ESTRSLUP = .FALSE.
         IF (ERR_GOOD() .AND. ACRA) THEN
            IERR = CA_CRIA_CALCRI(HCRA,
     &                            NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,L_DLG0)),
     &                            VA(SO_ALLC_REQVIND(VA,L_DLU)))
            ESTRSLUP = CA_CRIA_ESTCONVERGE(HCRA)
         ENDIF

!!C---        GLOBALISATION
!!         IF (ERR_GOOD() .AND. AGLB .AND. .NOT. ESTRSLUP) THEN
!!            IERR = GL_GLBL_XEQ(HGLB,
!!     &                         HSIM,
!!     &                         NDLN,
!!     &                         NNL,
!!     &                         VA(SO_ALLC_REQVIND(VA,L_DLG0)),
!!     &                         VA(SO_ALLC_REQVIND(VA,L_DLU)),
!!     &                         AL_GMRS_EPS(4,IOB), ! ETA_K
!!     &                         HALG,
!!     &                         F_RES)
!!            ESTMODIFP = IS_GL(GL_STTS_MODIF)
!!         ENDIF

C---        Critère global du préconditionnement
         ESTFINP = .FALSE.
         IF (ERR_GOOD())
     &      ESTFINP = (ESTFINR .AND. ESTRSLUP)

C---        Log l'incrément après globalisation
         IF (ERR_GOOD()) THEN
            CALL LOG_INFO(LOG_ZNE, ' ')
            IERR = AL_UTIL_LOGSOL(NDLN,
     &                            NNL,
     &                            VA(SO_ALLC_REQVIND(VA,L_DLU)),
     &                            HNUMR)
         ENDIF

C---        MAJ de la solution dans VDLG0 - Copie dans VDLG
         IF (ERR_GOOD()) THEN
            CALL DAXPY(NDLL,
     &                 1.0D0,
     &                 VA(SO_ALLC_REQVIND(VA,L_DLU)), 1,
     &                 VA(SO_ALLC_REQVIND(VA,L_DLG0)), 1)
            CALL DCOPY(NDLL,
     &                 VA(SO_ALLC_REQVIND(VA,L_DLG0)), 1,
     &                 VA(SO_ALLC_REQVIND(VA,LDLG)), 1)
         ENDIF

C---        Traitement post-resolution sur les DDL actualisés
         IF (ERR_GOOD())
     &      IERR = LM_ELEM_CLCPST(HSIM)

C---        STATS GLOBALES
         GSTAT(2) = GSTAT(2) + PSTAT(2)    ! Nb redem fait
         GSTAT(4) = GSTAT(4) + PSTAT(4)    ! Nb iter faites
         GSTAT(6) = PSTAT(6)               ! Résidu GMRes final

         CALL LOG_DECIND()
         IF (ERR_BAD()) GOTO 199
         IF (ESTFINP)   GOTO 199
      ENDDO
      IP = IP - 1    ! Fin de boucle ==> 1 trop loin
199   CONTINUE

C---     Imprime les stats
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'GMRES STATISTIQUES: '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(A,I6,A,I6,A,I3,A)')'PRECONDITIONNEMENTS#<35>#: ',
     &   IP, '/', NPREC,
     &   ' (', NINT(100.0D0*IP/NPREC), '%)'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I6,A,I6,A,I3,A)') 'REDEMARRAGES#<35>#: ',
     &   NINT(GSTAT(2)), '/', NINT(GSTAT(1)),
     &   ' (', NINT(100.0D0*GSTAT(2)/GSTAT(1)), '%)'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,I6,A,I6,A,I3,A)') 'ITERATIONS#<35>#: ',
     &   NINT(GSTAT(4)), '/', NINT(GSTAT(3)),
     &   ' (', NINT(100.0D0*GSTAT(4)/GSTAT(3)), '%)'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'RESIDU_INITIAL#<35>#: ',
     &                         GSTAT(5)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,1PE14.6E3)') 'RESIDU_FINAL#<35>#: ',
     &                         GSTAT(6)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,L3)') 'CRI_CONV#<35>#: ', ESTCNVGR
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(A,L3)') 'CRI_ARRET#<35>#: ', ESTRSLUR
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IERR = C_DH_TMRDELT(DELTEMPS, GSTAT(7))
      IERR = C_DH_TMRSTR (DELTEMPS, LOG_BUF)
      LOG_BUF = 'TEMPS_RESOLUTION#<35>#: ' //
     &          LOG_BUF(1:SP_STRN_LEN(LOG_BUF))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

C---        L'état de résolution
      ISTTS = AL_STTS_OK
      IF (ERR_GOOD()) THEN
         IF (.NOT. ESTRSLUP) ISTTS = IBSET(ISTTS, AL_STTS_CRIA_BAD)
         IF (ESTMODIFR)      ISTTS = IBSET(ISTTS, AL_STTS_GLOB_LIM)
!!!         IF (ESTRJT)       ISTTS = IBSET(ISTTS, AL_STTS_CRIC_RJT)
!!!         IF (ESTOSC)       ISTTS = IBSET(ISTTS, AL_STTS_CRIC_OSC)
      ENDIF
      
C---     Ajuste TSIM et DELT à la partie calculée
      IF (ERR_GOOD() .AND. ISTTS .EQ. AL_STTS_OK) THEN
         TSIM = TSIM + DELT
         DELT = 0.0D0
      ENDIF

C---     Conserve l'état
      AL_GMRS_STTS(IOB) = ISTTS

C---     Récupère l'espace de la solution
      IERR = SO_ALLC_ALLRE8(0, L_DLG0)
      IERR = SO_ALLC_ALLRE8(0, L_DLU)

C---     Récupère l'espace de GMRES
      CALL ERR_PUSH()
      IERR = SP_GMRS_DEALLOC(KTRV)
      CALL ERR_POP()

      CALL LOG_DECIND()
      AL_GMRS_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_GMRS_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'algmrs.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_GMRS_REQSTATUS = AL_GMRS_STTS(HOBJ - AL_GMRS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Utilisé pour le préconditionnement
C************************************************************************
      FUNCTION AL_GMRS_ASMKT(HOBJ, HMTX, F_ASM)

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HMTX
      INTEGER  F_ASM
      EXTERNAL F_ASM

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_GMRS_HBASE
      HSIM = AL_GMRS_HSIM(IOB)

C---     ASSEMBLE Kt
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKT(HSIM, HMTX, F_ASM)

      AL_GMRS_ASMKT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_ASMKU(HOBJ, VDLG, VRES)

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  VDLG(*)
      REAL*8  VRES(*)

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_GMRS_HBASE
      HSIM = AL_GMRS_HSIM(IOB)

C---     ASSIGNE VDLG À LA SIMULATION
      IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))

C---     ASSEMBLE (F-KU) DANS VRES
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMKU(HSIM, VRES)

C---     RÉTABLIS LA SIMULATION
      IERR = LM_ELEM_POPLDLG(HSIM)

      AL_GMRS_ASMKU = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_ASMRES(HOBJ, VRES, VDLG, ADLG)

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   VRES(*)
      REAL*8   VDLG(*)
      LOGICAL  ADLG

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'algmrs.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HSIM
      INTEGER LDLG
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_GMRS_HBASE
      HSIM = AL_GMRS_HSIM(IOB)

C---     ASSIGNE VDLG À LA SIMULATION
      IF (ADLG) THEN
         IERR = LM_ELEM_PUSHLDLG(HSIM, SO_ALLC_REQVHND(VDLG))
      ENDIF

C---     ASSEMBLE (F-KU) DANS VRES
      IF (ERR_GOOD()) IERR = LM_ELEM_ASMFKU(HSIM, VRES)
!!!      IF (ERR_GOOD()) IERR = LM_ELEM_ASMRES(HSIM, VRES)

C---     RÉTABLIS LA SIMULATION
      IF (ERR_GOOD() .AND. ADLG) THEN
         IERR = LM_ELEM_POPLDLG(HSIM)
      ENDIF

      AL_GMRS_ASMRES = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Fonction vide, tenant lieu d'une autre.
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_GMRS_DUMMY(HOBJ)

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'algmrs.fi'
      INCLUDE 'err.fi'
      INCLUDE 'algmrs.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_GMRS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

D     CALL ERR_ASR(.FALSE.)
      CALL ERR_ASG(ERR_FTL, 'ERR_APPEL_FNCT_INVALIDE')

      AL_GMRS_DUMMY = ERR_TYP()
      RETURN
      END
