C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALALGO_REQMDL(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALALGO_REQMDL
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'alalgo_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_ALALGO_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les param
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
         HOBJ = IC_ALALGO_REQHDL()
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  <b>algo</b> returns the handle on the module.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALALGO_AID()

9999  CONTINUE
      IC_ALALGO_REQMDL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_ALALGO_OPBDOT(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALALGO_OPBDOT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'alalgo_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alsqnc_ic.fi'
      INCLUDE 'alsqit_ic.fi'
      INCLUDE 'alsqip_ic.fi'
      INCLUDE 'alnolr_ic.fi'
      INCLUDE 'alnobi_ic.fi'
      INCLUDE 'alnodo_ic.fi'
      INCLUDE 'alnosp_ic.fi'
      INCLUDE 'icpcrd.fi'
      INCLUDE 'icnwtn.fi'
      INCLUDE 'icgmrs.fi'
      INCLUDE 'iceulr.fi'
      INCLUDE 'icrk4l.fi'
      INCLUDE 'almrqd_ic.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'alalgo_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_ASR((HOBJ/1000)*1000 .EQ. IC_ALALGO_REQHDL())
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <include>IC_ALSQNC_REQCLS@alsqnc_ic.for</include>
      IF (IMTH .EQ. 'algo_sequence') THEN
D        CALL ERR_ASR(IC_ALSQNC_REQCLS() .EQ. 'algo_sequence')
C        <include>IC_ALSQNC_XEQCTR@alsqnc_ic.for</include>
         IERR = IC_ALSQNC_XEQCTR(IPRM)

C     <include>IC_ALSQIT_REQCLS@alsqit_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_sequence_algo') THEN
D        CALL ERR_ASR(IC_ALSQIT_REQCLS() .EQ. 'algo_sequence_algo')
C        <include>IC_ALSQIT_XEQCTR@alsqit_ic.for</include>
         IERR = IC_ALSQIT_XEQCTR(IPRM)

C     <include>IC_ALSQIP_REQCMD@alsqip_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_sequence_post') THEN
D        CALL ERR_ASR(IC_ALSQIP_REQCMD() .EQ. 'algo_sequence_post')
C        <include>IC_ALSQIP_CMD@alsqip_ic.for</include>
         IERR = IC_ALSQIP_CMD(IPRM)

C     <include>IC_ALNOLR_REQCLS@alnolr_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_node') THEN
D        CALL ERR_ASR(IC_ALNOLR_REQCLS() .EQ. 'algo_node')
C        <include>IC_ALNOLR_XEQCTR@alnolr_ic.for</include>
         IERR = IC_ALNOLR_XEQCTR(IPRM)

C     <include>IC_ALNOBI_REQCLS@alnobi_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_node_bi') THEN
D        CALL ERR_ASR(IC_ALNOBI_REQCLS() .EQ. 'algo_node_bi')
C        <include>IC_ALNOBI_XEQCTR@alnobi_ic.for</include>
         IERR = IC_ALNOBI_XEQCTR(IPRM)

C     <include>IC_ALNODO_REQCLS@alnodo_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_node_for') THEN
D        CALL ERR_ASR(IC_ALNODO_REQCLS() .EQ. 'algo_node_for')
C        <include>IC_ALNODO_XEQCTR@alnodo_ic.for</include>
         IERR = IC_ALNODO_XEQCTR(IPRM)

C     <include>IC_ALNOSP_REQCLS@alnosp_ic.for</include>
      ELSEIF (IMTH .EQ. 'algo_node_save_point') THEN
D        CALL ERR_ASR(IC_ALNOSP_REQCLS() .EQ. 'algo_node_save_point')
C        <include>IC_ALNOSP_XEQCTR@alnosp_ic.for</include>
         IERR = IC_ALNOSP_XEQCTR(IPRM)

C     <include>IC_ALPCRD_REQCLS@icpcrd.for</include>
      ELSEIF (IMTH .EQ. 'picard') THEN
D        CALL ERR_ASR(IC_ALPCRD_REQCLS() .EQ. 'picard')
C        <include>IC_ALPCRD_XEQCTR@icpcrd.for</include>
         IERR = IC_ALPCRD_XEQCTR(IPRM)

C     <include>IC_ALNWTN_REQCLS@icnwtn.for</include>
      ELSEIF (IMTH .EQ. 'newton') THEN
D        CALL ERR_ASR(IC_ALNWTN_REQCLS() .EQ. 'newton')
C        <include>IC_ALNWTN_XEQCTR@icnwtn.for</include>
         IERR = IC_ALNWTN_XEQCTR(IPRM)

C     <include>IC_ALGMRS_REQCLS@icgmrs.for</include>
      ELSEIF (IMTH .EQ. 'gmres') THEN
D        CALL ERR_ASR(IC_ALGMRS_REQCLS() .EQ. 'gmres')
C        <include>IC_ALGMRS_XEQCTR@icgmrs.for</include>
         IERR = IC_ALGMRS_XEQCTR(IPRM)

C     <include>IC_ALEULR_REQCLS@iceulr.for</include>
      ELSEIF (IMTH .EQ. 'euler') THEN
D        CALL ERR_ASR(IC_ALEULR_REQCLS() .EQ. 'euler')
C        <include>IC_ALEULR_XEQCTR@iceulr.for</include>
         IERR = IC_ALEULR_XEQCTR(IPRM)

C     <include>IC_ALRK4L_REQCLS@icrk4l.for</include>
      ELSEIF (IMTH .EQ. 'runge_kutta_4l') THEN
D        CALL ERR_ASR(IC_ALRK4L_REQCLS() .EQ. 'runge_kutta_4l')
C        <include>IC_ALRK4L_XEQCTR@icrk4l.for</include>
         IERR = IC_ALRK4L_XEQCTR(IPRM)

C     <include>IC_ALMRQD_REQCLS@almrqd_ic.for</include>
      ELSEIF (IMTH .EQ. 'newton_damped') THEN
D        CALL ERR_ASR(IC_ALMRQD_REQCLS() .EQ. 'newton_damped')
C        <include>IC_ALMRQD_XEQCTR@almrqd_ic.for</include>
         IERR = IC_ALMRQD_XEQCTR(IPRM)

!C     <include>IC_ALLMTR_REQCLS@iclmtr.for</include>
!      ELSEIF (IMTH .EQ. 'limiter') THEN
!D        CALL ERR_ASR(IC_ALLMTR_REQCLS() .EQ. 'limiter')
!C        <include>IC_ALLMTR_XEQCTR@iclmtr.for</include>
!         IERR = IC_ALLMTR_XEQCTR(IPRM)

      ELSE
         GOTO 9903
      ENDIF


      GOTO 9999
C------------------------------------------------------------------------
9903  WRITE(ERR_BUF, '(3A)') 'ERR_CLASSE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_ALALGO_AID()

9999  CONTINUE
      IC_ALALGO_OPBDOT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALALGO_REQNOM()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALALGO_REQNOM
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alalgo_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The module <b>algo</b> regroups the algorithms.
C</comment>
      IC_ALALGO_REQNOM = 'algo'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_ALALGO_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_ALALGO_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alalgo_ic.fi'
C-------------------------------------------------------------------------

      IC_ALALGO_REQHDL = 1999031000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_ALALGO_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('alalgo_ic.hlp')

      RETURN
      END
