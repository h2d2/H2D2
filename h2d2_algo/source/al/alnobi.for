C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  ALgorithme
C Objet:   NOde time-step BIsection
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_NOBI_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnobi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_NOBI_NOBJMAX,
     &                   AL_NOBI_HBASE,
     &                   'Algorithm Node Time-Step Bisection')

      AL_NOBI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnobi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'

      INTEGER  IERR
      EXTERNAL AL_NOBI_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_NOBI_NOBJMAX,
     &                   AL_NOBI_HBASE,
     &                   AL_NOBI_DTR)

      AL_NOBI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOBI_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnobi.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_NOBI_NOBJMAX,
     &                   AL_NOBI_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_NOBI_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NOBI_HBASE

         AL_NOBI_HALG(IOB) = 0
         AL_NOBI_NLVL(IOB) = 0
         AL_NOBI_HXPR(IOB) = 0
         AL_NOBI_HLFT(IOB) = 0
         AL_NOBI_HRHT(IOB) = 0
         AL_NOBI_LRST(IOB) = .TRUE.
         AL_NOBI_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_NOBI_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOBI_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnobi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_NOBI_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_NOBI_NOBJMAX,
     &                   AL_NOBI_HBASE)

      AL_NOBI_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_INI(HOBJ, HALG, NLVL, HXPR, HLFT, HRHT, LRST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HALG
      INTEGER NLVL
      INTEGER HXPR
      INTEGER HLFT
      INTEGER HRHT
      LOGICAL LRST

      INCLUDE 'alnobi.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnobi.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_NOBI_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - AL_NOBI_HBASE

C---     CONTROLE LES DONNÉES
      IF (.NOT. AL_SLVR_HVALIDE(HALG)) GOTO 9900
      IF (.NOT. (HXPR .EQ. 0 .OR. IC_EXPR_HVALIDE(HXPR))) GOTO 9901
      IF (.NOT. (HLFT .EQ. 0 .OR. AL_SLVR_HVALIDE(HLFT))) GOTO 9902
      IF (.NOT. (HRHT .EQ. 0 .OR. AL_SLVR_HVALIDE(HRHT))) GOTO 9903
      IF (NLVL .LE. 0 .OR. NLVL .GT. 16) GOTO 9904

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         AL_NOBI_HALG(IOB) = HALG
         AL_NOBI_NLVL(IOB) = NLVL
         AL_NOBI_HXPR(IOB) = HXPR
         AL_NOBI_HLFT(IOB) = HLFT
         AL_NOBI_HRHT(IOB) = HRHT
         AL_NOBI_LRST(IOB) = LRST
         AL_NOBI_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HALG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HALG, 'MSG_SOLVER')
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HXPR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HXPR, 'MSG_EXPRESSION')
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HLFT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HLFT, 'MSG_NOEUD_GAUCHE')
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HRHT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRHT, 'MSG_NOEUD_DROIT')
      GOTO 9999
9904  WRITE(ERR_BUF,'(2A,I12)')'ERR_NBR_NIVEAU_INVALIDE',': ',NLVL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      AL_NOBI_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOBI_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnobi.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NOBI_HBASE

C---     RESET LES ATTRIBUTS
      AL_NOBI_HALG(IOB) = 0
      AL_NOBI_NLVL(IOB) = 0
      AL_NOBI_STTS(IOB) = AL_STTS_INDEFINI

      AL_NOBI_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_NOBI_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnobi.fi'
      INCLUDE 'alnobi.fc'
C------------------------------------------------------------------------

      AL_NOBI_REQHBASE = AL_NOBI_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_NOBI_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnobi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnobi.fc'
C------------------------------------------------------------------------

      AL_NOBI_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_NOBI_NOBJMAX,
     &                                  AL_NOBI_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alnobi.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnobi.fc'

      REAL*8  DTBACK, DTACTU
      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER ISTTS
      INTEGER HSVPT, ILVL, NLVL
      INTEGER HNOD, HALG, HXPR
      INTEGER LDLG
      LOGICAL LRES, LRST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOBI_HBASE
      HALG = AL_NOBI_HALG(IOB)
      NLVL = AL_NOBI_NLVL(IOB)
      HXPR = AL_NOBI_HXPR(IOB)
      HNOD = AL_NOBI_HRHT(IOB)      ! le noeud négatif
      LRST = AL_NOBI_LRST(IOB)

C---     Récupère le pas de temps de l'algo
      IERR = AL_SLVR_REQDELT(HALG, DTBACK)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Crée un point de sauvegarde
      HSVPT = 0
      IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT)
      IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT, LDLG)

C---     Boucle sur les niveaux
      TS_C = TSIM
      DT_C = DELT
      ISTTS = AL_STTS_INDEFINI
      LRES = .TRUE.
      DTACTU = DTBACK
      DO ILVL=1,NLVL

C---        Assigne le pas de temps à l'algo
         IF (ERR_GOOD()) IERR = AL_SLVR_ASGDELT(HALG, DTACTU)

C---        Execute l'algo
         IF (ILVL .NE. 1)
     &      IERR = SO_SVPT_RBCK(HSVPT)
         IF (ERR_GOOD())
     &      IERR = AL_SLVR_RESOUS(HALG, TS_C, DT_C, HSIM)

C---        Évalue l'expression
         ISTTS = AL_STTS_INDEFINI
         LRES = .TRUE.
         IF (ERR_GOOD()) THEN
            IF (IC_EXPR_HVALIDE(HXPR)) THEN
               IERR = IC_EXPR_XEQ_L(HXPR, LRES)
            ELSE
               ISTTS = AL_SLVR_REQSTATUS(HALG)
               LRES = (ISTTS .EQ. AL_STTS_OK)
            ENDIF
         ENDIF

C---        Coupe le pas en deux
         DTACTU = DTACTU * 0.5D0

         IF (LRES) EXIT
         IF (ERR_BAD()) EXIT
      ENDDO

C---     Restaure le pas de temps de l'algo
      IERR = AL_SLVR_ASGDELT(HALG, DTBACK)

C---     Choisi le fils
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NOBI_HLFT(IOB)
         ELSE
            HNOD = AL_NOBI_HRHT(IOB)
            IF (HSVPT .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT)
            ENDIF
         ENDIF
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) IERR = SO_SVPT_DTR(HSVPT)

C---     Résous le fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS(HNOD, TS_C, DT_C, HSIM)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Conserve le résultat
      AL_NOBI_STTS(IOB) = ISTTS

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

      AL_NOBI_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode AL_NOLR_RESOUS_E(...) est la méthode de bas niveau pour
C     la résolution.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOBI_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          H_F,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  H_F
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alnobi.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnobi.fc'

      REAL*8  DTA_0, DTA_A, DTN_0, DTN_A
      REAL*8  TINI, TFIN
      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT_A, HSVPT_N, ILVL, NLVL, NPOK
      INTEGER HNOD, HALG, HXPR
      INTEGER ISTTS
      INTEGER LDLG
      LOGICAL LRES, LRST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOBI_HBASE
      HALG = AL_NOBI_HALG(IOB)
      NLVL = AL_NOBI_NLVL(IOB)
      HXPR = AL_NOBI_HXPR(IOB)
      HNOD = AL_NOBI_HRHT(IOB)      ! le noeud négatif
      LRST = AL_NOBI_LRST(IOB)

C---     Récupère le pas de temps de l'algo
      IERR = AL_SLVR_REQDELT(HALG, DTA_0)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Initialise les param
      DTN_0 = DELT      ! DT du Noeud original
      DTN_A = DTN_0     ! DT du Noeud actuel
      DTA_A = DTA_0     ! DT de l'Algo actuel
      NPOK  = 0         ! Nombre de pas OK
      ILVL  = 1         ! Niveau de raffinement
      LRES  = .TRUE.    ! État de la résolution

C---     Crée un point de sauvegarde
      HSVPT_N = 0         ! Save-Point du noeud
      HSVPT_A = 0         ! Save-Point de l'algo
      IF (LRST) THEN
         IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT_N)
         IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT_N, LDLG)
         IF (ERR_GOOD()) IERR = SO_SVPT_CTR(HSVPT_A)
         IF (ERR_GOOD()) IERR = SO_SVPT_INI(HSVPT_A, LDLG)
      ENDIF

C---     ====================================
C---     Boucle d'intégration avec bi-section
C---     ====================================
      TS_C = TSIM
      DT_C = DELT
      TINI  = TSIM
      TFIN  = TINI + DTN_A
100   CONTINUE
         IF (TINI .GE. TFIN) GOTO 199

C---        Assigne le pas de temps à l'algo
         IF (ERR_GOOD()) IERR = AL_SLVR_ASGDELT(HALG, DTA_A)

C---        Execute l'algo
         DT_C = DTN_A
         IF (ERR_GOOD())
     &      IERR = AL_SLVR_RESOUS_E(HALG,
     &                              TS_C,
     &                              DT_C,
     &                              HSIM,
     &                              H_F,
     &                              F_K,
     &                              F_KT,
     &                              F_KU,
     &                              F_F,
     &                              F_RES)

C---        Évalue l'expression
         ISTTS = AL_STTS_INDEFINI
         LRES = .TRUE.
         IF (ERR_GOOD()) THEN
            IF (IC_EXPR_HVALIDE(HXPR)) THEN
               IERR = IC_EXPR_XEQ_L(HXPR, LRES)
            ELSE
               ISTTS = AL_SLVR_REQSTATUS(HALG)
               LRES = (ISTTS .EQ. AL_STTS_OK)
            ENDIF
         ENDIF

C---        Adapte le pas de temps
         IF (ERR_GOOD()) THEN
            IF (LRES) THEN
               TINI = TFIN
               TFIN = TINI + DTN_A
               NPOK = NPOK + 1
               IF (NPOK .EQ. 2) THEN
                  DTA_A = MIN(DTA_A*2, DTA_0)
                  DTN_A = MIN(DTN_A*2, DTN_0)
                  DTN_A = MIN(DTN_A, (TSIM+DELT)-TINI)
                  NPOK = 1
                  ILVL = ILVL - 1
               ENDIF
            ELSE
               DTA_A = DTA_A / 2
               DTN_A = DTN_A / 2
               NPOK = 0
               ILVL = ILVL + 1
            ENDIF
         ENDIF

C---        Gère le pt de sauvegarde
         IF (ERR_GOOD()) THEN
D           CALL LOG_TODO('HSVPT_A apparaît dans les deux branches???')
            IF (LRES) THEN
               IERR = SO_SVPT_SAVE(HSVPT_A)
            ELSE
               IERR = SO_SVPT_SAVE(HSVPT_A)
            ENDIF
         ENDIF

         IF (ILVL .GT. NLVL) GOTO 199
         IF (ERR_BAD())  GOTO 199
      GOTO 100
199   CONTINUE

C---     Restaure le pas de temps de l'algo
      IERR = AL_SLVR_ASGDELT(HALG, DTA_0)

C---     Choisi le fils
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NOBI_HLFT(IOB)
         ELSE
            HNOD = AL_NOBI_HRHT(IOB)
            IF (LRST .AND. HNOD .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT_N)
            ENDIF
         ENDIF
      ENDIF

C---     Libère les points de sauvegarde
      CALL ERR_PUSH()
      IF (HSVPT_A .NE. 0) IERR = SO_SVPT_DTR(HSVPT_A)
      IF (HSVPT_N .NE. 0) IERR = SO_SVPT_DTR(HSVPT_N)
      CALL ERR_POP()

C---     Résous le fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS_E(HNOD,
     &                           TS_C,
     &                           DT_C,
     &                           HSIM,
     &                           H_F,
     &                           F_K,
     &                           F_KT,
     &                           F_KU,
     &                           F_F,
     &                           F_RES)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

C---     Conserve le résultat
      AL_NOBI_STTS(IOB) = ISTTS

      AL_NOBI_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOBI_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOBI_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alnobi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnobi.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOBI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NOBI_REQSTATUS = AL_NOBI_STTS(HOBJ - AL_NOBI_HBASE)
      RETURN
      END
