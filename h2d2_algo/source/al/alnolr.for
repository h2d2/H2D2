C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  ALgorithme
C Objet:   strategy NODE
C Type:    Concret
C
C Interface:
C   H2D2 Module: AL
C      H2D2 Class: AL_NOLR
C         SUBROUTINE AL_NOLR_000
C         SUBROUTINE AL_NOLR_999
C         SUBROUTINE AL_NOLR_CTR
C         SUBROUTINE AL_NOLR_DTR
C         SUBROUTINE AL_NOLR_INI
C         SUBROUTINE AL_NOLR_RST
C         SUBROUTINE AL_NOLR_REQHBASE
C         SUBROUTINE AL_NOLR_HVALIDE
C         SUBROUTINE AL_NOLR_RESOUS
C         SUBROUTINE AL_NOLR_RESOUS_E
C         SUBROUTINE AL_NOLR_REQSTATUS
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>AL_NOLR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnolr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(AL_NOLR_NOBJMAX,
     &                   AL_NOLR_HBASE,
     &                   'Algorithm Node Left-Right')

      AL_NOLR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnolr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'

      INTEGER  IERR
      EXTERNAL AL_NOLR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(AL_NOLR_NOBJMAX,
     &                   AL_NOLR_HBASE,
     &                   AL_NOLR_DTR)

      AL_NOLR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOLR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnolr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   AL_NOLR_NOBJMAX,
     &                   AL_NOLR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(AL_NOLR_HVALIDE(HOBJ))
         IOB = HOBJ - AL_NOLR_HBASE

         AL_NOLR_HALG(IOB) = 0
         AL_NOLR_HXPR(IOB) = 0
         AL_NOLR_HLFT(IOB) = 0
         AL_NOLR_HRHT(IOB) = 0
         AL_NOLR_LRST(IOB) = .TRUE.
         AL_NOLR_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      AL_NOLR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOLR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnolr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = AL_NOLR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   AL_NOLR_NOBJMAX,
     &                   AL_NOLR_HBASE)

      AL_NOLR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise et dimensionne
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_INI(HOBJ, HALG, HXPR, HLFT, HRHT, LRST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HALG
      INTEGER HXPR
      INTEGER HLFT
      INTEGER HRHT
      LOGICAL LRST

      INCLUDE 'alnolr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnolr.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = AL_NOLR_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - AL_NOLR_HBASE

C---     CONTROLE LES DONNÉES
      IF (.NOT. AL_SLVR_HVALIDE(HALG)) GOTO 9900
      IF (.NOT. (HXPR .EQ. 0 .OR. IC_EXPR_HVALIDE(HXPR))) GOTO 9901
      IF (.NOT. (HLFT .EQ. 0 .OR. AL_SLVR_HVALIDE(HLFT))) GOTO 9902
      IF (.NOT. (HRHT .EQ. 0 .OR. AL_SLVR_HVALIDE(HRHT))) GOTO 9903

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         AL_NOLR_HALG(IOB) = HALG
         AL_NOLR_HXPR(IOB) = HXPR
         AL_NOLR_HLFT(IOB) = HLFT
         AL_NOLR_HRHT(IOB) = HRHT
         AL_NOLR_LRST(IOB) = LRST
         AL_NOLR_STTS(IOB) = AL_STTS_INDEFINI
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HALG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HALG, 'MSG_SOLVER')
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HXPR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HXPR, 'MSG_EXPRESSION')
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HLFT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HLFT, 'MSG_NOEUD_GAUCHE')
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HRHT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HRHT, 'MSG_NOEUD_DROIT')
      GOTO 9999

9999  CONTINUE
      AL_NOLR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOLR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnolr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - AL_NOLR_HBASE

C---     RESET LES ATTRIBUTS
      AL_NOLR_HALG(IOB) = 0
      AL_NOLR_HXPR(IOB) = 0
      AL_NOLR_HLFT(IOB) = 0
      AL_NOLR_HRHT(IOB) = 0
      AL_NOLR_LRST(IOB) = .TRUE.
      AL_NOLR_STTS(IOB) = AL_STTS_INDEFINI

      AL_NOLR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction AL_NOLR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'alnolr.fi'
      INCLUDE 'alnolr.fc'
C------------------------------------------------------------------------

      AL_NOLR_REQHBASE = AL_NOLR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction AL_NOLR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'alnolr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'alnolr.fc'
C------------------------------------------------------------------------

      AL_NOLR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  AL_NOLR_NOBJMAX,
     &                                  AL_NOLR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps (non utilisé en stationnaire)
C     INTEGER HSIM         Handle sur l'élément
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION AL_NOLR_RESOUS(HOBJ,
     &                        TSIM,
     &                        DELT,
     &                        HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_RESOUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM

      INCLUDE 'alnolr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnolr.fc'

      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER HNOD, HALG, HXPR
      INTEGER ISTTS
      INTEGER LDLG
      LOGICAL LRES, LSTP, LRST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOLR_HBASE
      HALG = AL_NOLR_HALG(IOB)
      HXPR = AL_NOLR_HXPR(IOB)
      HNOD = AL_NOLR_HRHT(IOB)      ! le noeud négatif
      LRST = AL_NOLR_LRST(IOB)

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Crée un point de sauvegarde
      HSVPT = 0
      IF (LRST .AND. HNOD .NE. 0) THEN
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
      ENDIF

C---     Exécute l'algorithme
      TS_C = TSIM
      DT_C = DELT
      ISTTS = AL_STTS_INDEFINI
      IF (ERR_GOOD()) THEN
         IERR = AL_SLVR_RESOUS(HALG, TS_C, DT_C, HSIM)
         ISTTS = AL_SLVR_REQSTATUS(HALG)
      ENDIF

C---     Évalue l'expression
      LRES = .FALSE.    ! Est résolu
      LSTP = .FALSE.    ! Stop
      IF (ERR_GOOD()) THEN
         IF (IC_EXPR_HVALIDE(HXPR)) THEN
            IERR = IC_EXPR_XEQ_L(HXPR, LRES)
         ELSE
            LRES = (ISTTS .EQ. AL_STTS_OK)
            LSTP = BTEST(ISTTS, AL_STTS_CINC_STOP)
         ENDIF
      ENDIF

C---     Choisi le fils
      HNOD = 0
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NOLR_HLFT(IOB)
         ELSE IF (.NOT. LSTP) THEN
            HNOD = AL_NOLR_HRHT(IOB)
            IF (HSVPT .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT)

               CALL LOG_INCIND()
               WRITE(LOG_BUF,'(3A)') 'MSG_ROLLBACK'
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TS_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DT_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               CALL LOG_DECIND()

            ENDIF
         ENDIF
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) IERR = SO_SVPT_DTR(HSVPT)

C---     Résous le fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS(HNOD, TS_C, DT_C, HSIM)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

C---     Conserve le résultat
      AL_NOLR_STTS(IOB) = ISTTS

      AL_NOLR_RESOUS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Résolution de bas niveau
C
C Description:
C     La méthode AL_NOLR_RESOUS_E(...) est la méthode de bas niveau pour
C     la résolution.
C     Elle prend en paramètre les fonctions de call-back pour l'assemblage
C     des matrices et du résidu, ce qui permet de l'utiliser comme élément
C     d'un autre algo.
C
C Entrée:
C     INTEGER HOBJ         Handle sur l'objet courant
C     REAL*8  TSIM         Temps de la simulation
C     REAL*8  DELT         Pas de temps
C     INTEGER HSIM        Handle sur l'élément
C     INTEGER HALG         Paramètre pour les fonction call-back
C     EXTERNAL F_MDTK      Fonction de calcul de [M.dt + K]
C     EXTERNAL F_MDTKT     Fonction de calcul de [M.dt + KT]
C     EXTERNAL F_MDTKU     Fonction de calcul de [M.dt + K].{U}
C     EXTERNAL F_F         Fonction de calcul de {F}
C     EXTERNAL F_RES       Fonction de calcul du résidu
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOLR_RESOUS_E(HOBJ,
     &                          TSIM,
     &                          DELT,
     &                          HSIM,
     &                          H_F,
     &                          F_K,
     &                          F_KT,
     &                          F_KU,
     &                          F_F,
     &                          F_RES)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_RESOUS_E
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      REAL*8   TSIM
      REAL*8   DELT
      INTEGER  HSIM
      INTEGER  H_F
      INTEGER  F_K
      INTEGER  F_KT
      INTEGER  F_KU
      INTEGER  F_F
      INTEGER  F_RES
      EXTERNAL F_K
      EXTERNAL F_KT
      EXTERNAL F_KU
      EXTERNAL F_F
      EXTERNAL F_RES

      INCLUDE 'alnolr.fi'
      INCLUDE 'alslvr.fi'
      INCLUDE 'alstts.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'icexpr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'sosvpt.fi'
      INCLUDE 'alnolr.fc'

      REAL*8  TS_C, DT_C
      INTEGER IERR
      INTEGER IOB
      INTEGER HSVPT
      INTEGER HNOD, HALG, HXPR
      INTEGER ISTTS
      INTEGER LDLG
      LOGICAL LRES, LSTP, LRST
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_ELEM_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.node.nodelr'

C---     Récupère les attributs
      IOB  = HOBJ - AL_NOLR_HBASE
      HALG = AL_NOLR_HALG(IOB)
      HXPR = AL_NOLR_HXPR(IOB)
      HNOD = AL_NOLR_HRHT(IOB)      ! le noeud négatif
      LRST = AL_NOLR_LRST(IOB)      ! do reset

C---     Récupère les paramètres de la simulation
      LDLG  = LM_ELEM_REQPRM(HSIM, LM_ELEM_PRM_LDLG)

C---     Crée un point de sauvegarde
      HSVPT = 0
      IF (LRST .AND. HNOD .NE. 0) THEN
         IERR = SO_SVPT_CTR(HSVPT)
         IERR = SO_SVPT_INI(HSVPT, LDLG)
      ENDIF

C---     Exécute l'algorithme
      TS_C = TSIM
      DT_C = DELT
      ISTTS = AL_STTS_INDEFINI
      IF (ERR_GOOD()) THEN
         IERR = AL_SLVR_RESOUS_E(HALG,
     &                           TS_C,
     &                           DT_C,
     &                           HSIM,
     &                           H_F,
     &                           F_K,
     &                           F_KT,
     &                           F_KU,
     &                           F_F,
     &                           F_RES)
         ISTTS = AL_SLVR_REQSTATUS(HALG)
      ENDIF

C---     Évalue l'expression
      LRES = .FALSE.    ! Est résolu
      LSTP = .FALSE.    ! Stop
      IF (ERR_GOOD()) THEN
         IF (IC_EXPR_HVALIDE(HXPR)) THEN
            IERR = IC_EXPR_XEQ_L(HXPR, LRES)
         ELSE
            LRES = (ISTTS .EQ. AL_STTS_OK)
            LSTP = BTEST(ISTTS, AL_STTS_CINC_STOP)
         ENDIF
      ENDIF

C---     Choisi le fils
      HNOD = 0
      IF (ERR_GOOD()) THEN
         IF (LRES) THEN
            HNOD = AL_NOLR_HLFT(IOB)
         ELSE IF (.NOT. LSTP) THEN
            HNOD = AL_NOLR_HRHT(IOB)
            IF (HSVPT .NE. 0) THEN
               TS_C = TSIM
               DT_C = DELT
               IERR = SO_SVPT_RBCK(HSVPT)

               CALL LOG_INCIND()
               WRITE(LOG_BUF,'(3A)') 'MSG_ROLLBACK'
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TINI#<35>#', '= ', TS_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_DELT#<35>#', '= ', DT_C
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
               CALL LOG_DECIND()

            ENDIF
         ENDIF
      ENDIF

C---     Libère le point de sauvegarde
      IF (HSVPT .NE. 0) IERR = SO_SVPT_DTR(HSVPT)

C---     Appelle un des fils
      IF (ERR_GOOD() .AND. HNOD .NE. 0) THEN
         IERR = AL_SLVR_RESOUS_E(HNOD,
     &                           TS_C,
     &                           DT_C,
     &                           HSIM,
     &                           H_F,
     &                           F_K,
     &                           F_KT,
     &                           F_KU,
     &                           F_F,
     &                           F_RES)
         ISTTS = AL_SLVR_REQSTATUS(HNOD)
      ENDIF

C---     Ajuste TSIM et DELT
      TSIM = TS_C
      DELT = DT_C

C---     Conserve le résultat
      AL_NOLR_STTS(IOB) = ISTTS

      AL_NOLR_RESOUS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION AL_NOLR_REQSTATUS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: AL_NOLR_REQSTATUS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'alnolr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'alnolr.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(AL_NOLR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      AL_NOLR_REQSTATUS = AL_NOLR_STTS(HOBJ - AL_NOLR_HBASE)
      RETURN
      END
