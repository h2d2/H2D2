C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Critère d'Arret
C Objet:   No-OP
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_NOOP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NOOP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_NOOP_NOBJMAX,
     &                   CA_NOOP_HBASE,
     &                   'No-OP Stopping Criteria')

      CA_NOOP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NOOP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER  IERR
      EXTERNAL CA_NOOP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_NOOP_NOBJMAX,
     &                   CA_NOOP_HBASE,
     &                   CA_NOOP_DTR)

      CA_NOOP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NOOP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_NOOP_NOBJMAX,
     &                   CA_NOOP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_NOOP_HVALIDE(HOBJ))
         IOB = HOBJ - CA_NOOP_HBASE
      ENDIF

      CA_NOOP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NOOP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_NOOP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_NOOP_NOBJMAX,
     &                   CA_NOOP_HBASE)

      CA_NOOP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NOOP_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = CA_NOOP_RST(HOBJ)

      CA_NOOP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_NOOP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CA_NOOP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_NOOP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NOOP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'canoop.fi'
      INCLUDE 'canoop.fc'
C------------------------------------------------------------------------

      CA_NOOP_REQHBASE = CA_NOOP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_NOOP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NOOP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'canoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'canoop.fc'
C------------------------------------------------------------------------

      CA_NOOP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_NOOP_NOBJMAX,
     &                                  CA_NOOP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Calcule le critère d'arrêt.
C
C Description:
C     La fonction <code>CA_N2GR_CALCRI(...)</code> calcule le
C     critère d'arrêt.
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_NOOP_CALCRI(HOBJ, HNUMR, NDLN, NNL, VDLG, VDEL, CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_NOOP_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN*NNL)
      REAL*8  VDEL(NDLN*NNL)
      REAL*8  CRIA

      INCLUDE 'canoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'canoop.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CRIA = 0.0D0
      CA_NOOP_CALCRI = ERR_TYP()
      RETURN
      END
