C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CA_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_COMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CA_COMP_NOBJMAX,
     &                   CA_COMP_HBASE,
     &                   'Critere d''arret - Compose')

      CA_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_COMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER  IERR
      EXTERNAL CA_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CA_COMP_NOBJMAX,
     &                   CA_COMP_HBASE,
     &                   CA_COMP_DTR)

      CA_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_COMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CA_COMP_NOBJMAX,
     &                   CA_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CA_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - CA_COMP_HBASE

         CA_COMP_IOPB (IOB) = CA_COMP_OPB_INDEFINI
         CA_COMP_NCRIA(IOB) = 0
         CA_COMP_LCRIA(IOB) = 0
      ENDIF

      CA_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_COMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CA_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CA_COMP_NOBJMAX,
     &                   CA_COMP_HBASE)

      CA_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_COMP_INI(HOBJ, IOPB, NCRIA, KCRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IOPB
      INTEGER NCRIA
      INTEGER KCRIA(*)

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCRIA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (IOPB .NE. CA_COMP_OPB_AND .AND.
     &    IOPB .NE. CA_COMP_OPB_OR) GOTO 9900
      IF (NCRIA .LT. 0) GOTO 9901
      DO I=1, NCRIA
         IF (.NOT. CA_CRIA_HVALIDE(KCRIA(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = CA_COMP_RST(HOBJ)

C---     ALLOUE LA MÉMOIRE POUR LA TABLE
      LCRIA = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NCRIA, LCRIA)

C---     REMPLIS LA TABLE
      IF (ERR_GOOD()) CALL ICOPY (NCRIA,
     &                            KCRIA, 1,
     &                            KA(SO_ALLC_REQKIND(KA,LCRIA)), 1)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CA_COMP_HBASE
         CA_COMP_IOPB (IOB) = IOPB
         CA_COMP_NCRIA(IOB) = NCRIA
         CA_COMP_LCRIA(IOB) = LCRIA
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)')'ERR_OPERATION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NCRIA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KCRIA(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CA_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CA_COMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LCRIA
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CA_COMP_HBASE
      LCRIA = CA_COMP_LCRIA(IOB)

      IF (LCRIA .NE. 0) IERR = SO_ALLC_ALLINT(0, LCRIA)

      CA_COMP_IOPB (IOB) = CA_COMP_OPB_INDEFINI
      CA_COMP_NCRIA(IOB) = 0
      CA_COMP_LCRIA(IOB) = 0

      CA_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CA_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_COMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacomp.fi'
      INCLUDE 'cacomp.fc'
C------------------------------------------------------------------------

      CA_COMP_REQHBASE = CA_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CA_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_COMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cacomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cacomp.fc'
C------------------------------------------------------------------------

      CA_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CA_COMP_NOBJMAX,
     &                                  CA_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Retourne .TRUE. si le critère d'arrêt est satisfait.
C
C Description:
C     La fonction <code>CA_COMP_CALCRI(...)</code> retourne .TRUE.
C     si le critère d'arrêt est satisfait.
C     On compose les contributions des composantes.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table de Degré de Liberté Globaux
C     VDEL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CA_COMP_CALCRI(HOBJ, NDLN, NNL, VDLG, VDEL, CRIA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CA_COMP_CALCRI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  CRIA

      INCLUDE 'cacomp.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cacomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOPB
      INTEGER IC
      INTEGER NCRIA, LCRIA, HCRIA
      LOGICAL RESG, RESL
      REAL*8  VALG, VALL
C------------------------------------------------------------------------
D     CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPÈRE LES ATTRIBUTS
      IOB = HOBJ - CA_COMP_HBASE
      IOPB  = CA_COMP_IOPB (IOB)
      NCRIA = CA_COMP_NCRIA(IOB)
      LCRIA = CA_COMP_LCRIA(IOB)
D     CALL ERR_ASR(NCRIA .GT. 0)
D     CALL ERR_ASR(LCRIA .NE. 0)

C---     INITIALISE LE RÉSULTAT GLOBAL
      IF     (IOPB .EQ. CA_COMP_OPB_AND) THEN
         RESG = .TRUE.
         VALG = 0.0D0
         WRITE(LOG_BUF, '(A)') 'MSG_NRM_COMP#<20>#: MSG_AND'
      ELSEIF (IOPB .EQ. CA_COMP_OPB_OR) THEN
         RESG = .FALSE.
         VALG = 1.0D99
         WRITE(LOG_BUF, '(A)') 'MSG_NRM_COMP#<20>#: MSG_OR'
      ENDIF
      CALL LOG_ECRIS(LOG_BUF)

C---     BOUCLE SUR LES CRITERES
      CALL LOG_INCIND()
      DO IC = 1, NCRIA
         HCRIA = SO_ALLC_REQIN4(LCRIA, IC)
         IERR = CA_CRIA_CALCRI(HCRIA,
     &                         NDLN,
     &                         NNL,
     &                         VDLG,
     &                         VDEL)
         VALL = CA_CRIA_REQVAL(HCRIA)
         RESL = CA_CRIA_ESTCONVERGE(HCRIA)

         IF (IOPB .EQ. CA_COMP_OPB_AND) THEN
            RESG = RESG .AND. RESL
            VALG = MAX(VALG, VALL)
            IF (.NOT. RESG) GOTO 199      ! COURT-CIRCUIT
         ELSEIF (IOPB .EQ. CA_COMP_OPB_OR) THEN
            RESG = RESG .OR. RESL
            VALG = MIN(VALG, VALL)
            IF (RESG) GOTO 199            ! COURT-CIRCUIT
D        ELSE
D           CALL ERR_ASR(.FALSE.)
         ENDIF

      ENDDO
199   CONTINUE
      CALL LOG_DECIND()

      WRITE(LOG_BUF, '(2A,L3)') 'MSG_NRM_COMP#<20>#: ', 'MSG_OK =', RESG
      CALL LOG_ECRIS(LOG_BUF)

      CRIA = VALG
      CA_COMP_CALCRI = ERR_TYP()
      RETURN
      END
