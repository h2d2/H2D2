C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CAN2AR_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2AR_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'can2ar_ic.fi'
      INCLUDE 'can2ar.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IDDL
      INTEGER HOBJ, HELE, HCRA
      REAL*8  EPSR, EPSA
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CAN2AR_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CRITERE_ARRET' //
     &                       '_L2_ACCROISSEMENT_RELATIF_UN_DDL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      HELE = 0
      EPSR = 0.0D0
      EPSA = 0.0D0
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HELE)
C     <comment>Index of the degree of freedom to control</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, IDDL)
C     <comment>Relative epsilon</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 3, EPSR)
C     <comment>Absolute epsilon</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 4, EPSA)
      IF (IERR .NE. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET CONCRET
      HCRA = 0
      IF (ERR_GOOD()) IERR = CA_N2AR_CTR(HCRA)
      IF (ERR_GOOD()) IERR = CA_N2AR_INI(HCRA, HELE, IDDL, EPSR, EPSA)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CA_CRIA_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CA_CRIA_INI(HOBJ, HCRA)

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HCRA)
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         IERR = OB_OBJC_REQNOMCMPL(NOM, HELE)
         WRITE (LOG_BUF,'(A,A)') 'MSG_SIMULATION#<35>#= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_DDL#<35>#= ',
     &                               IDDL
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,1PE14.6E3)')'MSG_PRECISION_REL#<35>#= ',
     &                               EPSR
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,1PE14.6E3)')'MSG_PRECISION_ABS#<35>#= ',
     &                               EPSA
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the stopping criterion</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cria_l2_1ddlrel</b> constructs an object, with the
C  given arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CAN2AR_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_CAN2AR_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CAN2AR_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2AR_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'can2ar_ic.fi'
      INCLUDE 'can2ar.fi'
      INCLUDE 'can2ar.fc'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
         IOB = HOBJ - CA_N2AR_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Index of the degree of freedom to control</comment>
         IF (PROP .EQ. 'ddl') THEN
            IVAL = CA_N2AR_IDDL(IOB)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Relative epsilon</comment>
         ELSEIF (PROP .EQ. 'eps_rel') THEN
            RVAL = CA_N2AR_EPSR(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
C        <comment>Absolute epsilon</comment>
         ELSEIF (PROP .EQ. 'eps_abs') THEN
            RVAL = CA_N2AR_EPSA(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
         IOB = HOBJ - CA_N2AR_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Index of the degree of freedom to control</comment>
         IF (PROP .EQ. 'ddl') THEN
            IERR = SP_STRN_TKI(IPRM, ',', 2, IVAL)
            IF (IERR .NE. 0) GOTO 9901
            CA_N2AR_IDDL(IOB) = IVAL
C        <comment>Relative epsilon</comment>
         ELSEIF (PROP .EQ. 'eps_rel') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            CA_N2AR_EPSR(IOB) = RVAL
C        <comment>Absolute epsilon</comment>
         ELSEIF (PROP .EQ. 'eps_abs') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            CA_N2AR_EPSA(IOB) = RVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CA_N2AR_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CA_N2AR_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = CA_N2AR_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test CA_N2AR_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CAN2AR_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CAN2AR_AID()

9999  CONTINUE
      IC_CAN2AR_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CAN2AR_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2AR_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2ar_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cria_l2_1ddlrel</b> represents the stopping criterion in L2
C  norm, for one degree of freedom, for a relative increase.
C  The condition on the solution increment is:
C  <pre>
C  || dU / (eps_r*|u| + eps_a) || < 1.0 </pre>
C  On a dof, it can be expressed as:
C  <pre>
C   dU < eps_r*|u| + eps_a </pre>
C</comment>
      IC_CAN2AR_REQCLS = 'cria_l2_1ddlrel'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CAN2AR_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CAN2AR_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'can2ar_ic.fi'
      INCLUDE 'can2ar.fi'
C-------------------------------------------------------------------------

      IC_CAN2AR_REQHDL = CA_N2AR_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_CAN2AR_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CAN2AR_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('can2ar_ic.hlp')

      RETURN
      END
