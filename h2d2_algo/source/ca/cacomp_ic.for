C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CACOMP_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACOMP_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cacomp_ic.fi'
      INCLUDE 'cacomp.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ, HCRA
      INTEGER I
      INTEGER IOPB
      INTEGER NCRIA
      INTEGER NCRIAMAX
      PARAMETER (NCRIAMAX = 20)
      INTEGER KCRIA(NCRIAMAX)
      CHARACTER*(64)  SOPB
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CACOMP_AID()
            GOTO 9999
         ENDIF
      ENDIF

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CRIA_COMPOSE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Composition operation ['and', 'or']</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, SOPB)
      IF (IERR .NE. 0) GOTO 9902
      I = 0
100   CONTINUE
         IF (I .EQ. NCRIAMAX) GOTO 9901
         I = I + 1
C        <comment>List of the stopping criteria to compose, comma separated</comment>
         IERR = SP_STRN_TKI(IPRM, ',', I+1, KCRIA(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9902
      GOTO 100
199   CONTINUE
      NCRIA = I-1

C---     VALIDE
      CALL SP_STRN_UCS(SOPB)
      IOPB = CA_COMP_OPB_INDEFINI
      IF (SOPB .EQ. 'AND') IOPB = CA_COMP_OPB_AND
      IF (SOPB .EQ.  'OR') IOPB = CA_COMP_OPB_OR
      IF (IOPB .EQ. CA_COMP_OPB_INDEFINI) GOTO 9903
      IF (NCRIA .LE. 0) GOTO 9904

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HCRA)
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A, A)') 'MSG_OPB_COMPOSE#<35>#',
     &                             '= ', SOPB(1:SP_STRN_LEN(SOPB))
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(2A,I6)') 'MSG_NBR_OBJ_COMPOSE#<35>#',
     &                             '= ', NCRIA
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     CONSTRUIS ET INITIALISE L'OBJET
      HCRA = 0
      IF (ERR_GOOD()) IERR = CA_COMP_CTR(HCRA)
      IF (ERR_GOOD()) IERR = CA_COMP_INI(HCRA, IOPB, NCRIA, KCRIA)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CA_CRIA_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CA_CRIA_INI(HOBJ, HCRA)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the stopping criterion</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cria_compose</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF,'(3A)') 'ERR_OPERATION_INVALIDE',': ',
     &                      SOPB(1:SP_STRN_LEN(SOPB))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF,'(2A,I6)') 'ERR_NBR_CRIA_INVALIDE',': ', NCRIA
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CACOMP_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_CACOMP_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CACOMP_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACOMP_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cacomp_ic.fi'
      INCLUDE 'cacomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CA_COMP_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CA_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = CA_COMP_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test CA_COMP_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CACOMP_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CACOMP_AID()

9999  CONTINUE
      IC_CACOMP_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CACOMP_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACOMP_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacomp_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cria_compose</b> represents the composition of two or more
C  stopping criteria with a logical operation.
C</comment>
      IC_CACOMP_REQCLS = 'cria_compose'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CACOMP_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACOMP_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacomp_ic.fi'
      INCLUDE 'cacomp.fi'
C-------------------------------------------------------------------------

      IC_CACOMP_REQHDL = CA_COMP_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C     La fonction IC_CACOMP_AID fait afficher le contenu du fichier d'aide.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CACOMP_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('cacomp_ic.hlp')
      RETURN
      END
