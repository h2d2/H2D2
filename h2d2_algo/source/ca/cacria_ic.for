C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes: Critère d'Arret
C Objet:   Parent et proxy
C Type:    ---
C************************************************************************

C************************************************************************
C Sommaire: Fonction vide qui ne devrait jamais être appelée.
C
C Description:
C     La fonction IC_CACRIA_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un proxy via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CACRIA_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACRIA_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'cacria_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_CACRIA_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_CACRIA_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type AL_PRXY.
C     Le proxy résout directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CACRIA_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACRIA_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'cacria_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cacria.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'cacria.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
      REAL*8  RVAL
      LOGICAL LVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(CA_CRIA_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Value of the criteria</comment>
         IF (PROP .EQ. 'value') THEN
            RVAL = CA_CRIA_REQVAL(HOBJ)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
C        <comment>True (<> 0) if the criterion is satisfied</comment>
         ELSEIF (PROP .EQ. 'is_satified') THEN
            LVAL = CA_CRIA_ESTCONVERGE(HOBJ)
            WRITE(IPRM, '(2A,L3)') 'L', ',', LVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>Class destructor</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CA_CRIA_DTR(HOBJ)

      ELSE
         IOB = HOBJ - CA_CRIA_HBASE
         HVAL = CA_CRIA_HOMG(IOB)
         WRITE(IPRM, '(2A,I12)') 'X', ',', HVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_CACRIA_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CACRIA_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACRIA_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacria_ic.fi'
C-------------------------------------------------------------------------

      IC_CACRIA_REQCLS = '#__dummy_placeholder__#__CA_CRIA__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CACRIA_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CACRIA_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cacria_ic.fi'
      INCLUDE 'cacria.fi'
C-------------------------------------------------------------------------

      IC_CACRIA_REQHDL = CA_CRIA_REQHBASE()
      RETURN
      END
