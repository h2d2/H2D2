C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id: cicomp.for,v 1.14 2008/10/08 20:10:42 secretyv Exp $
C
C Functions:
C   Public:
C     SUBROUTINE CI_COMP_000
C     SUBROUTINE CI_COMP_999
C     SUBROUTINE CI_COMP_CTR
C     SUBROUTINE CI_COMP_DTR
C     SUBROUTINE CI_COMP_INI
C     SUBROUTINE CI_COMP_RST
C     SUBROUTINE CI_COMP_REQHBASE
C     SUBROUTINE CI_COMP_HVALIDE
C     SUBROUTINE CI_COMP_CLCDEL
C     SUBROUTINE CI_COMP_CLCERR
C     SUBROUTINE CI_COMP_DEB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_COMP_000()
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_000

      IMPLICIT NONE

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_COMP_NOBJMAX,
     &                   CI_COMP_HBASE,
     &                   'Increment Controller - Compose')

      CI_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_COMP_999()
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_999

      IMPLICIT NONE

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER  IERR
      EXTERNAL CI_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_COMP_NOBJMAX,
     &                   CI_COMP_HBASE,
     &                   CI_COMP_DTR)

      CI_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_COMP_NOBJMAX,
     &                   CI_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - CI_COMP_HBASE

         CI_COMP_NCINC(IOB) = 0
         CI_COMP_LCINC(IOB) = 0
      ENDIF

      CI_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_COMP_NOBJMAX,
     &                   CI_COMP_HBASE)

      CI_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_INI(HOBJ, NCINC, KCINC)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_INI

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCINC
      INTEGER KCINC(*)

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
      INTEGER LCINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (NCINC .LT. 0) GOTO 9901
      DO I=1, NCINC
         IF (.NOT. CI_CINC_HVALIDE(KCINC(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = CI_COMP_RST(HOBJ)

C---     ALLOUE LA MÉMOIRE POUR LA TABLE
      LCINC = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLIN4(NCINC, LCINC)

C---     REMPLIS LA TABLE
      IF (ERR_GOOD()) CALL ICOPY (NCINC,
     &                            KCINC, 1,
     &                            KA(SO_ALLC_REQKIND(KA,LCINC)), 1)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CI_COMP_HBASE
         CI_COMP_NCINC(IOB) = NCINC
         CI_COMP_LCINC(IOB) = LCINC
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NCINC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KCINC(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LCINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_COMP_HBASE
      LCINC = CI_COMP_LCINC(IOB)

      IF (LCINC .NE. 0) IERR = SO_ALLC_ALLIN4(0, LCINC)

      CI_COMP_NCINC(IOB) = 0
      CI_COMP_LCINC(IOB) = 0

      CI_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_COMP_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_REQHBASE

      IMPLICIT NONE

      INCLUDE 'cicomp.fi'
      INCLUDE 'cicomp.fc'
C------------------------------------------------------------------------

      CI_COMP_REQHBASE = CI_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_COMP_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicomp.fc'
C------------------------------------------------------------------------

      CI_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_COMP_NOBJMAX,
     &                                  CI_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_COMP_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C************************************************************************
      FUNCTION CI_COMP_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_CLCDEL

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK
      REAL*8,  INTENT(OUT):: DELT(3)

      INCLUDE 'cicomp.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER ISTT
      INTEGER NCINC
      INTEGER LCINC
      INTEGER HCINC
      REAL*8  DTCI, DTEF
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_COMP_HBASE
      NCINC = CI_COMP_NCINC(IOB)
      LCINC = CI_COMP_LCINC(IOB)
D     CALL ERR_ASR(NCINC .GT. 0)
D     CALL ERR_ASR(LCINC .NE. 0)

C---     Initialise le retour
      DELT(1) = 1.0D99
      DELT(2) = 1.0D99
      DELT(3) = CI_CINC_STATUS_UNDEFINED

C---     Calcule et réduis DTEF
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) THEN
            IERR = CI_CINC_CLCDEL(HCINC,
     &                            NDLN,
     &                            NNL,
     &                            VDLG,
     &                            VDEL,
     &                            ESTOK)
         ENDIF
         IF (ERR_GOOD()) THEN
             IERR = CI_CINC_REQDELT(HCINC, ISTT, DTCI, DTEF)
         ENDIF
         IF (ERR_GOOD()) THEN
             DELT(1) = MIN(DELT(1), DTCI)
             DELT(2) = MIN(DELT(2), DTEF)
             DELT(3) = MAX(NINT(DELT(3)), ISTT)
         ENDIF
      ENDDO
      
C---     Assigne le DTEF réduis
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) THEN
            IERR = CI_CINC_ASGDTEF(HCINC, DELT(2))
         ENDIF
      ENDDO

      CI_COMP_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_COMP_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_COMP_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_CLCERR

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VERR(NDLN, NNL)
      LOGICAL, INTENT(IN) :: ESTOK
      REAL*8,  INTENT(OUT):: DELT(3)

      INCLUDE 'cicomp.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER ISTT
      INTEGER NCINC
      INTEGER LCINC
      INTEGER HCINC
      REAL*8  DTCI, DTEF
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_COMP_HBASE
      NCINC = CI_COMP_NCINC(IOB)
      LCINC = CI_COMP_LCINC(IOB)
D     CALL ERR_ASR(NCINC .GT. 0)
D     CALL ERR_ASR(LCINC .NE. 0)

C---     Initialise le retour
      DELT(1) = 0.0D0
      DELT(2) = 0.0D0
      DELT(3) = CI_CINC_STATUS_UNDEFINED

C---     Calcule et réduis DTEF
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) THEN
            IERR = CI_CINC_CLCERR(HCINC,
     &                            NDLN,
     &                            NNL,
     &                            VDLG,
     &                            VDEL,
     &                            VERR,
     &                            ESTOK)
         ENDIF
         IF (ERR_GOOD()) THEN
             IERR = CI_CINC_REQDELT(HCINC, ISTT, DTCI, DTEF)
         ENDIF
         IF (ERR_GOOD()) THEN
             DELT(1) = MIN(DELT(1), DTCI)
             DELT(2) = MIN(DELT(2), DTEF)
             DELT(3) = MAX(NINT(DELT(3)), ISTT)
         ENDIF
      ENDDO

C---     Assigne le DTEF réduis
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) THEN
            IERR = CI_CINC_ASGDTEF(HCINC, DELT(2))
         ENDIF
      ENDDO

      CI_COMP_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_COMP_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_ASGDTEF(HOBJ, DTEF)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_ASGDTEF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cicomp.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER NCINC
      INTEGER LCINC
      INTEGER HCINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_COMP_HBASE
      NCINC = CI_COMP_NCINC(IOB)
      LCINC = CI_COMP_LCINC(IOB)
D     CALL ERR_ASR(NCINC .GT. 0)
D     CALL ERR_ASR(LCINC .NE. 0)

C---     Boucle sur les contrôleurs
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) IERR = CI_CINC_ASGDTEF(HCINC, DTEF)
      ENDDO

      CI_COMP_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_COMP_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_COMP_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ATTRIBUTES DLLEXPORT :: CI_COMP_DEB

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cicomp.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IC
      INTEGER NCINC
      INTEGER LCINC
      INTEGER HCINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_COMP_HBASE
      NCINC = CI_COMP_NCINC(IOB)
      LCINC = CI_COMP_LCINC(IOB)
D     CALL ERR_ASR(NCINC .GT. 0)
D     CALL ERR_ASR(LCINC .NE. 0)

C---     Boucle sur les contrôleurs
      DO IC = 1, NCINC
         HCINC = SO_ALLC_REQIN4(LCINC, IC)
         IF (ERR_GOOD()) IERR = CI_CINC_DEB(HCINC, TINI, TFIN, DT0)
      ENDDO

      CI_COMP_DEB = ERR_TYP()
      RETURN
      END

