C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   PReDictive Control
C Type:    Concret
C************************************************************************
C$NOREFERENCE

C------------------------------------------------------------------------

C---     ATTRIBUTS DE GESTION DES OBJETS
      INTEGER CI_PRDC_NOBJMAX
      INTEGER CI_PRDC_HBASE
      PARAMETER (CI_PRDC_NOBJMAX = 4)

C---     COMMON DE GESTION DES OBJETS
      COMMON /CI_PRDC_CGO/ CI_PRDC_HBASE

C------------------------------------------------------------------------

C---     ATTRIBUTS PRIVES
      INTEGER CI_PRDC_NEPSMAX
      PARAMETER (CI_PRDC_NEPSMAX = 8)

C---     ATTRIBUTS PRIVES
      REAL*8  CI_PRDC_XPR0       ! Exposant du terme en ETGT/R
      REAL*8  CI_PRDC_XPR1       ! Exposant du terme en ETGT/R_M1
      REAL*8  CI_PRDC_EPSA       ! Epsilon absolu
      REAL*8  CI_PRDC_EPSR       ! Epsilon relatif
      REAL*8  CI_PRDC_ETGT       ! Epsilon cible
      REAL*8  CI_PRDC_EMAX       ! Epsilon max
      REAL*8  CI_PRDC_R_M1       ! Norme de l'erreur à l'itération n-1
      REAL*8  CI_PRDC_F_M1       ! Facteur multiplication l'itération n-1
      REAL*8  CI_PRDC_DTMI       ! Delta T min
      REAL*8  CI_PRDC_TACT
      REAL*8  CI_PRDC_TFIN
      REAL*8  CI_PRDC_DTCI
      REAL*8  CI_PRDC_DTEF
      INTEGER CI_PRDC_HELE       ! Handle sur l'élément
      INTEGER CI_PRDC_NEPS       ! Nombre d'epsilon
      INTEGER CI_PRDC_INRM       ! Norme

C---     COMMON DES DONNEES
      COMMON /CI_PRDC_CDT/ CI_PRDC_XPR0(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_XPR1(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_EPSA(CI_PRDC_NEPSMAX,
     &                                  CI_PRDC_NOBJMAX),
     &                     CI_PRDC_EPSR(CI_PRDC_NEPSMAX,
     &                                  CI_PRDC_NOBJMAX),
     &                     CI_PRDC_ETGT(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_EMAX(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_R_M1(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_F_M1(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_DTMI(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_TACT(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_TFIN(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_DTCI(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_DTEF(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_HELE(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_NEPS(CI_PRDC_NOBJMAX),
     &                     CI_PRDC_INRM(CI_PRDC_NOBJMAX)

C$REFERENCE
