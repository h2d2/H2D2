C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   NO-OPeration
C Type:    Concret
C
C Functions:
C   Public:
C     INTEGER CI_NOOP_000
C     INTEGER CI_NOOP_999
C     INTEGER CI_NOOP_CTR
C     INTEGER CI_NOOP_DTR
C     INTEGER CI_NOOP_INI
C     INTEGER CI_NOOP_RST
C     INTEGER CI_NOOP_REQHBASE
C     LOGICAL CI_NOOP_HVALIDE
C     INTEGER CI_NOOP_CLCDEL
C     INTEGER CI_NOOP_CLCERR
C     INTEGER CI_NOOP_DEB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_NOOP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cinoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_NOOP_NOBJMAX,
     &                   CI_NOOP_HBASE,
     &                   'Increment Controller - NoOp')

      CI_NOOP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_NOOP_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cinoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER  IERR
      EXTERNAL CI_NOOP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_NOOP_NOBJMAX,
     &                   CI_NOOP_HBASE,
     &                   CI_NOOP_DTR)

      CI_NOOP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_NOOP_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_NOOP_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_NOOP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cinoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_NOOP_NOBJMAX,
     &                   CI_NOOP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_NOOP_HVALIDE(HOBJ))
         IOB = HOBJ - CI_NOOP_HBASE

         CI_NOOP_DTCI  (IOB) = -1.0D0
      ENDIF

      CI_NOOP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_NOOP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cinoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_NOOP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_NOOP_NOBJMAX,
     &                   CI_NOOP_HBASE)

      CI_NOOP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_NOOP_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     HSIM     Handle sur l'élément
C     NEPS     Nombre d'epsilon
C     EPSA     Epsilon absolue
C     EPSR     Epsilon relatif
C     EPS      Target error
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_NOOP_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL DOSTOP

      INCLUDE 'cinoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = CI_NOOP_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CI_NOOP_HBASE
         CI_NOOP_DTCI(IOB) = -1.0D0
      ENDIF

      CI_NOOP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_NOOP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cinoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_NOOP_HBASE
      CI_NOOP_DTCI  (IOB) = -1.0D0

      CI_NOOP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_NOOP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cinoop.fi'
      INCLUDE 'cinoop.fc'
C------------------------------------------------------------------------

      CI_NOOP_REQHBASE = CI_NOOP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_NOOP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cinoop.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cinoop.fc'
C------------------------------------------------------------------------

      CI_NOOP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_NOOP_NOBJMAX,
     &                                  CI_NOOP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_NOOP_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cinoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CI_NOOP_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_NOOP_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cinoop.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_NOOP_HBASE

C---     Le nouvel état
      IF (ESTOK) THEN      ! passthrough
         DELT(1) = CI_NOOP_DTCI(IOB)
         DELT(2) = DELT(1)
         DELT(3) = CI_CINC_STATUS_OK
      ELSE
         DELT(1) = CI_NOOP_DTCI(IOB)
         DELT(2) = -1.0D0
         DELT(3) = CI_CINC_STATUS_SKIP
      ENDIF

      CI_NOOP_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_NOOP_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_NOOP_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cinoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_NOOP_CLCDEL(HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_NOOP_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_NOOP_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_NOOP_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_NOOP_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cinoop.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cinoop.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_NOOP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_NOOP_HBASE
      CI_NOOP_DTCI(IOB) = DT0

      CI_NOOP_DEB = ERR_TYP()
      RETURN
      END
