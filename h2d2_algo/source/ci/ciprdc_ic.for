C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_CI_PRDC_XEQCTR
C     INTEGER IC_CI_PRDC_XEQMTH
C     CHARACTER*(32) IC_CI_PRDC_REQCLS
C     INTEGER IC_CI_PRDC_REQHDL
C   Private:
C     INTEGER IC_CI_PRDC_PRN
C     SUBROUTINE IC_CI_PRDC_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_PRDC_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_PRDC_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'ciprdc_ic.fi'
      INCLUDE 'ciprdc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ciprdc_ic.fc'

      INTEGER NVALMAX
      PARAMETER (NVALMAX = 10)

      INTEGER IERR
      INTEGER I, IV
      INTEGER IKND
      INTEGER HOBJ, HALG
      INTEGER HELE
      INTEGER NTOK, NVAL
      REAL*8  ETGT, EMAX
      REAL*8  DTMIN
      REAL*8  VEPSA(NVALMAX), VEPSR(NVALMAX)
      CHARACTER*(8) LKND
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_CI_PRDC_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Les dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      NVAL = (NTOK-4) / 2
      IF (NVAL .GT. NVALMAX) GOTO 9902

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 1
C     <comment>Minimal time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, DTMIN)
      I = I + 1
C     <comment>Handle on the element</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', I, HELE)
      I = I + 1
C     <comment>Global target tolerance</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, ETGT)
      I = I + 1
C     <comment>Max tolerance for step rejection</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, EMAX)
      IF (IERR .NE. 0) GOTO 9901
      DO IV = 1,NVAL
C        <comment>
C        List of tolerances, comma separated. For each degree of freedom,
C        2 values for relative and absolute tolerance.
C        </comment>
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSR(IV))
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSA(IV))
         IF (IERR .NE.  0) GOTO 9901
      ENDDO
      I = I + 1
C     <comment>Kind of norm ['l2', 'max'] (default 'l2')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', I, LKND)
      IF (IERR .NE. 0) LKND = 'l2'

C---     Contrôle
      IF (NVAL .LE. 0) GOTO 9904
      IKND = CI_CINC_NRM_INDEFINI
      IF (LKND .EQ. 'l2')  IKND = CI_CINC_NRM_L2
      IF (LKND .EQ. 'max') IKND = CI_CINC_NRM_MAX
      IF (IKND .EQ. CI_CINC_NRM_INDEFINI) GOTO 9905

C---     CONSTRUIS ET INITIALISE L'OBJET
      HALG = 0
      IF (ERR_GOOD()) IERR = CI_PRDC_CTR(HALG)
      IF (ERR_GOOD()) IERR = CI_PRDC_INI(HALG,
     &                                   DTMIN,
     &                                   HELE,
     &                                   NVAL,
     &                                   VEPSR,
     &                                   VEPSA,
     &                                   ETGT,
     &                                   EMAX,
     &                                   IKND)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = CI_CINC_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = CI_CINC_INI(HOBJ, HALG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_CI_PRDC_PRN(HALG)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the algorithm</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>cinc_predictive</b> constructs an object, with the given arguments,
C  and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_NORME_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_PRDC_AID()

9999  CONTINUE
      IC_CI_PRDC_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_PRDC_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ciprdc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ciprdc.fc'
      INCLUDE 'ciprdc_ic.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER LTXT
      CHARACTER*(256) TXT
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CI_PREDICTIVE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - CI_PRDC_HBASE

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(TXT, HOBJ)
      LTXT = SP_STRN_LEN(TXT)
      WRITE(LOG_BUF,'(3A)') 'MSG_SELF#<35>#', '= ', TXT(1:LTXT)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_CI_PRDC_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_CI_PRDC_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_PRDC_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'ciprdc_ic.fi'
      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'ciprdc_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = CI_PRDC_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = IC_CI_PRDC_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_CI_PRDC_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_CI_PRDC_AID()

9999  CONTINUE
      IC_CI_PRDC_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_PRDC_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_PRDC_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ciprdc_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>cinc_predictive</b> represents a predictive increment controller.
C  Based on the relative L2 norm of the increment and a target L2 norm, it will
C  evaluate the new increment.
C
C</comment>
      IC_CI_PRDC_REQCLS = 'cinc_predictive'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_CI_PRDC_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_CI_PRDC_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ciprdc_ic.fi'
      INCLUDE 'ciprdc.fi'
C-------------------------------------------------------------------------

      IC_CI_PRDC_REQHDL = CI_PRDC_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_CI_PRDC_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('ciprdc_ic.hlp')
      RETURN
      END

