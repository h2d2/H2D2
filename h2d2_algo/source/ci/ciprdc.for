C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Contrôle d'Incrément
C Objet:   PReDictive Control
C Type:    Concret
C Note:
C  1) Völcker C., J. B. Jørgensen, P. G. Thomsen, E. H. Stenby (2010). 
C     Adaptive Stepsize Control in Implicit Runge-Kutta Methods
C     for Reservoir Simulation.
C     Proceedings of the 9th International Symposium on 
C     Dynamics and Control of Process Systems (DYCOPS 2010),
C     Leuven, Belgium, July 5-7, 2010
C  2) Morten Rode Kristense, John Bagterp Jørgensen, Per Grove Thomsen,
C     Sten Bay Jørgense.
C     An ESDIRK method with sensitivity analysis capabilities
C     Computers and Chemical Engineering 28 (2004) 2695–2707
C  3) P.M. Burrage, R.Herdiana, K.Burrage.
C     Adaptive stepsize based on control theory for stochastic differential equations
C     Journal of Computational and Applied Mathematics 170 (2004) 317 – 336
C     La formule utilisée est
C     </pre>
C     FMUL = F_M1
C    &     * (ETGT / R) ** (XPR0 / 2.0D0) ! Integral     part
C    &     * (R_M1 / R) ** (XPR1 / 2.0D0) ! Proportional part
C     </pre>
C     avec les valeurs XPR0, XPR1 = 1.0 (paramètres k1, k2 de Söderlind).
C     2.0 est la valeur de  k = p+1, avec p l'ordre de la méthode
C     de résolution, ici fixé à 1.
C     La formule se laisse récrire sous la forme :
C     </pre>
C     XPR0b = XPR0 + XPR1 = 2.0
C     XPR1b = -XPR0      = -1.0
C     FMUL = F_M1
C    &     * (ETGT / R   ) ** (XPR0b / 2.0D0)
C    &     * (ETGT / R_M1) ** (XPR1b / 2.0D0)
C     </pre>
C
C Functions:
C   Public:
C     INTEGER CI_PRDC_000
C     INTEGER CI_PRDC_999
C     INTEGER CI_PRDC_CTR
C     INTEGER CI_PRDC_DTR
C     INTEGER CI_PRDC_INI
C     INTEGER CI_PRDC_RST
C     INTEGER CI_PRDC_REQHBASE
C     LOGICAL CI_PRDC_HVALIDE
C     INTEGER CI_PRDC_ASGXPO
C     INTEGER CI_PRDC_CLCDEL
C     INTEGER CI_PRDC_CLCERR
C     INTEGER CI_PRDC_DEB
C     INTEGER CI_PRDC_REQXPO
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_PRDC_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ciprdc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_PRDC_NOBJMAX,
     &                   CI_PRDC_HBASE,
     &                   'Increment Controller - Predictive Control')

      CI_PRDC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_PRDC_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ciprdc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER  IERR
      EXTERNAL CI_PRDC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_PRDC_NOBJMAX,
     &                   CI_PRDC_HBASE,
     &                   CI_PRDC_DTR)

      CI_PRDC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_PRDC_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_PRDC_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ciprdc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_PRDC_NOBJMAX,
     &                   CI_PRDC_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_PRDC_HVALIDE(HOBJ))
         IOB = HOBJ - CI_PRDC_HBASE

         CI_PRDC_XPR0(IOB) =  2.0D0
         CI_PRDC_XPR1(IOB) = -1.0D0
         DO ID=1, CI_PRDC_NEPSMAX
            CI_PRDC_EPSA(ID, IOB) = 1.0D-4
            CI_PRDC_EPSR(ID, IOB) = 1.0D-4
         ENDDO
         CI_PRDC_ETGT(IOB) =  1.0D0
         CI_PRDC_EMAX(IOB) = 10.0D0
         CI_PRDC_R_M1(IOB) = -1.0D0
         CI_PRDC_F_M1(IOB) =  1.0D0
         CI_PRDC_NEPS(IOB) = 0
         CI_PRDC_INRM(IOB) = CI_CINC_NRM_INDEFINI
      ENDIF

      CI_PRDC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ciprdc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_PRDC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_PRDC_NOBJMAX,
     &                   CI_PRDC_HBASE)

      CI_PRDC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_PRDC_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     DTMIN    DeltaT MINimal
C     HELE     Handle sur l'élément
C     NEPS     Nombre d'epsilon
C     VEPSR    Epsilon relatif
C     VEPSA    Epsilon absolue
C     ETGT     Target error
C     EMAX     Max error over a step
C     INRM     Type of norm
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_INI(HOBJ,
     &                     DTMIN,
     &                     HELE,
     &                     NEPS,
     &                     VEPSR,
     &                     VEPSA,
     &                     ETGT,
     &                     EMAX,
     &                     INRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTMIN
      INTEGER HELE
      INTEGER NEPS
      REAL*8  VEPSR(NEPS)
      REAL*8  VEPSA(NEPS)
      REAL*8  ETGT
      REAL*8  EMAX
      INTEGER INRM

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ID, IV
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (DTMIN .LE. 0.0D0) GOTO 9900
      IF (.NOT. LM_HELE_HVALIDE(HELE)) GOTO 9901
      NDLN = LM_HELE_REQPRM(HELE, LM_HELE_PRM_NDLN)
      IF (NEPS .NE. 1 .AND. NEPS .NE. NDLN) GOTO 9902
      DO ID=1,NEPS
         IF (VEPSA(ID) .LE. 1.0D-16) GOTO 9903
         IF (VEPSA(ID) .GT. 1.0D+08) GOTO 9903
      ENDDO
      IF (EMAX .LE. ETGT) GOTO 9904

C---     Reset les données
      IERR = CI_PRDC_RST(HOBJ)

C---     Assigne les attributs
      IOB = HOBJ - CI_PRDC_HBASE
      CI_PRDC_XPR0(IOB) =  2.0D0
      CI_PRDC_XPR1(IOB) = -1.0D0
      DO ID=1,NDLN
         IV = MIN(ID, NEPS)
         CI_PRDC_EPSA(ID, IOB) = VEPSA(IV)
         CI_PRDC_EPSR(ID, IOB) = VEPSR(IV)
      ENDDO
      CI_PRDC_ETGT(IOB) = ETGT
      CI_PRDC_EMAX(IOB) = EMAX
      CI_PRDC_R_M1(IOB) = -1.0D0
      CI_PRDC_F_M1(IOB) =  1.0D0
      CI_PRDC_DTMI(IOB) = DTMIN
      CI_PRDC_HELE(IOB) = HELE
      CI_PRDC_NEPS(IOB) = NDLN
      CI_PRDC_INRM(IOB) = INRM

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_HANDLE_INVALIDE',':',
     &                           HELE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HELE, 'MSG_SIMULATION')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NOMBRE_VALEUR_INVALIDE',':',
     &                           NEPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)')
     &   'ERR_VALEUR_INVALIDE',':', VEPSA(ID)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,1PE14.6E3))')
     &   'ERR_BORNES_INVALIDE (ETGT > EMAX)', ':', ETGT, ' > ', EMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_PRDC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ciprdc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_PRDC_HBASE
      CI_PRDC_XPR0(IOB) =  2.0D0
      CI_PRDC_XPR1(IOB) = -1.0D0
      DO ID=1, CI_PRDC_NEPSMAX
         CI_PRDC_EPSA(ID, IOB) = 1.0D-4
         CI_PRDC_EPSR(ID, IOB) = 1.0D-4
      ENDDO
      CI_PRDC_ETGT(IOB) =  1.0D0
      CI_PRDC_ETGT(IOB) = 10.0D0
      CI_PRDC_R_M1(IOB) = -1.0D0
      CI_PRDC_F_M1(IOB) =  1.0D0
      CI_PRDC_DTMI(IOB) = -1.0D0
      CI_PRDC_HELE(IOB) = 0
      CI_PRDC_NEPS(IOB) = 0
      CI_PRDC_INRM(IOB) = CI_CINC_NRM_INDEFINI

      CI_PRDC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_PRDC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'ciprdc.fi'
      INCLUDE 'ciprdc.fc'
C------------------------------------------------------------------------

      CI_PRDC_REQHBASE = CI_PRDC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_PRDC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'ciprdc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'ciprdc.fc'
C------------------------------------------------------------------------

      CI_PRDC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_PRDC_NOBJMAX,
     &                                  CI_PRDC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les exposants
C
C Description:
C     La méthode CI_PRDC_ASGXPO permet d'assigner les valeurs des
C     exposants utilisés dans le calcul du facteur multiplicatif.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_ASGXPO(HOBJ, XPR0, XPR1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_ASGXPO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  XPR0
      REAL*8  XPR1

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IOB
      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Assigne les attributs
      IOB = HOBJ - CI_PRDC_HBASE
      CI_PRDC_XPR0(IOB) = XPR0
      CI_PRDC_XPR1(IOB) = XPR1

      CI_PRDC_ASGXPO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_PRDC_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
D     CALL ERR_PRE(DTEF .LE. CI_PRDC_DTEF(HOBJ-CI_PRDC_HBASE))
C------------------------------------------------------------------------

      CI_PRDC_DTEF(HOBJ-CI_PRDC_HBASE) = DTEF

      CI_PRDC_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_PRDC_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_PRDC_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_PRDC_CLCERR(HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_PRDC_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_PRDC_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C     DELT(3)     .TRUE. si le pas est valide (cast en REAL*8)
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C************************************************************************
      FUNCTION CI_PRDC_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'ciprdc.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IERR
      INTEGER IOB

      REAL*8  R, R_D, R_M1, FMUL, F_M1
      REAL*8  XPR0, XPR1, EPSA, EPSR, ETGT, EMAX
      REAL*8  TACT, TFIN
      REAL*8  DTMI, DTCI, DTEF, DTRST
      INTEGER ID, INRM, ISTTS
      INTEGER HELE, HNUMR
      LOGICAL DOCLC, DOMUL, DOLMT, DOINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.cinc.predictif'

C---     Récupère les attributs
      IOB = HOBJ - CI_PRDC_HBASE
      XPR0 = CI_PRDC_XPR0(IOB)
      XPR1 = CI_PRDC_XPR1(IOB)
      ETGT = CI_PRDC_ETGT(IOB)
      EMAX = CI_PRDC_EMAX(IOB)
      R_M1 = CI_PRDC_R_M1(IOB)
      F_M1 = CI_PRDC_F_M1(IOB)
      DTMI = CI_PRDC_DTMI(IOB)
      TACT = CI_PRDC_TACT(IOB)
      TFIN = CI_PRDC_TFIN(IOB)
      DTCI = CI_PRDC_DTCI(IOB)
      DTEF = CI_PRDC_DTEF(IOB)
      HELE = CI_PRDC_HELE(IOB)
      INRM = CI_PRDC_INRM(IOB)
D     CALL ERR_ASR(CI_PRDC_NEPS(IOB) .EQ. NDLN)

C---     Switch sur le type de pas
      IF (.NOT. ESTOK) THEN               ! Pas non convergé
         FMUL =  0.5D0                    !  Coupe dT
         R    = -1.0D0                    !  Réinitialise
         DOCLC = .FALSE.                  !  Calcule la norme
         DOMUL = .TRUE.                   !  Applique FMUL
         DOLMT = .FALSE.                  !  Limite le pas
         DOINC = .FALSE.                  !  Incrémente
         DTCI  = DTEF                     !  cf. note 1
      ELSEIF (TACT+DTEF .LT. TFIN) THEN   ! Pas intermédiaire
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .TRUE.
         DOINC = .TRUE.
      ELSEIF (DTEF .GE. DTCI) THEN        ! Pas final normal
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ELSE                                ! Pas final tronqué
         FMUL = F_M1                      !  Rétablis au pas calculé
         R    = R_M1
         DOCLC = .FALSE.
         DOMUL = .FALSE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ENDIF

C---     Calcule le max des normes relative
      IF (DOCLC) THEN
         HNUMR = LM_HELE_REQHNUMC(HELE)
         R = -1.0D99
         DO ID=1,NDLN
            EPSA = CI_PRDC_EPSA(ID, IOB)
            EPSR = CI_PRDC_EPSR(ID, IOB)
            IF    (INRM .EQ. CI_CINC_NRM_L2) THEN
               R_D = NR_UTIL_N2SR(HNUMR,NDLN,NNL,VERR,ID,VDLG,EPSA,EPSR)
            ELSEIF (INRM .EQ. CI_CINC_NRM_MAX) THEN
               R_D = NR_UTIL_NISR(HNUMR,NDLN,NNL,VERR,ID,VDLG,EPSA,EPSR)
D           ELSE
D              CALL ERR_ASR(INRM .NE. CI_CINC_NRM_INDEFINI)
            ENDIF
            R = MAX(R, R_D)
         ENDDO
      ENDIF

C---     Calcule le facteur multiplicatif
      IF (DOCLC) THEN
         IF (R .GE. EMAX) THEN               ! Erreur trop grande
            WRITE(LOG_BUF, '(A,2(A,1PE14.6E3))') 'MSG_EMAX_DEPASSE',
     &               ': ', R, ' / ', EMAX
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            FMUL =  0.5D0                    !  Coupe dT
            R    = -1.0D0                    !  Réinitialise
            DOCLC = .FALSE.                  !  Calcule la norme
            DOLMT = .FALSE.                  !  Limite le pas
            DOINC = .FALSE.
         ELSEIF (R_M1 .LE. 0.0D0) THEN
            FMUL = F_M1
     &           * (ETGT / R) ** (XPR0 / 2.0D0)
         ELSE
            FMUL = F_M1 !! ?? DTM1 / DTCI
     &           * (ETGT / R) ** (XPR0 / 2.0D0)
     &           * (R_M1 / R) ** (XPR1 / 2.0D0)
         ENDIF
      ENDIF

C---     Applique FMUL
      IF (DOMUL) DTCI = DTCI*FMUL

C---     Status
      ISTTS = CI_CINC_STATUS_UNDEFINED
      IF (DOINC)           ISTTS = CI_CINC_STATUS_OK
      IF (.NOT. DOINC)     ISTTS = CI_CINC_STATUS_COUPE
      IF (DTCI .LT. DTMI)  ISTTS = CI_CINC_STATUS_DTMIN

C---     Limite au pas min
      IF (DOMUL) THEN
         DTCI = MAX(DTCI, DTMI)
      ENDIF

C---     Incrémente le pas
      IF (DOINC) THEN
         TACT = TACT + DTEF
      ENDIF

C---     Limite le pas
      IF (DOLMT) THEN
         DTRST = TFIN - (TACT + DTCI)
         IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTCI)) THEN
            DTEF = TFIN-TACT
            FMUL = FMUL*(DTEF/DTCI)
         ELSE
            DTEF = MIN(DTCI, TFIN-TACT)
         ENDIF
      ELSE
         DTEF = DTCI
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_CRITERE#<35>#', '= ', R
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_CRI_REL#<35>#', '= ', R/ETGT
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_FACMUL#<35>#', '= ', FMUL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_CI#<35>#', '= ', DTCI
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_EFF#<35>#', '= ', DTEF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CI_PRDC_R_M1(IOB) = R
      CI_PRDC_F_M1(IOB) = FMUL
      CI_PRDC_TACT(IOB) = TACT
      CI_PRDC_DTCI(IOB) = DTCI
      CI_PRDC_DTEF(IOB) = DTEF

C---     Valeurs retournées
      DELT(1) = DTCI
      DELT(2) = DTEF
      DELT(3) = ISTTS

      CI_PRDC_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_PRDC_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      REAL*8  DTMI
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Indice de l'objet
      IOB = HOBJ - CI_PRDC_HBASE

C---     Contrôles
      DTMI = CI_PRDC_DTMI(IOB)
      IF (DT0 .LT. DTMI) GOTO 9900

C---     Assigne les valeurs
      CI_PRDC_TACT(IOB) = TINI
      CI_PRDC_TFIN(IOB) = TFIN
      IF (CI_PRDC_DTCI(IOB) .NE. DT0) THEN
         CI_PRDC_R_M1(IOB) = -1.0D0
         CI_PRDC_F_M1(IOB) =  1.0D0
      ENDIF
      CI_PRDC_DTCI(IOB) = DT0
      CI_PRDC_DTEF(IOB) = DT0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_INITIAL', ': ', DT0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMI
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_PRDC_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les exposants
C
C Description:
C     La méthode CI_PRDC_REQXPO retourne les valeurs des
C     exposants utilisés dans le calcul du facteur multiplicatif.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_PRDC_REQXPO(HOBJ, XPR0, XPR1)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_PRDC_REQXPO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  XPR0
      REAL*8  XPR1

      INCLUDE 'ciprdc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ciprdc.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_PRDC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_PRDC_HBASE
      XPR0 = CI_PRDC_XPR0(IOB)
      XPR1 = CI_PRDC_XPR1(IOB)

      CI_PRDC_REQXPO = ERR_TYP()
      RETURN
      END
