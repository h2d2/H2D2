C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Contrôle d'Incrément
C Objet:   LiMiTeR
C Type:    Concret
C Functions:
C   Public:
C     INTEGER CI_LMTR_000
C     INTEGER CI_LMTR_999
C     INTEGER CI_LMTR_CTR
C     INTEGER CI_LMTR_DTR
C     INTEGER CI_LMTR_INI
C     INTEGER CI_LMTR_RST
C     INTEGER CI_LMTR_REQHBASE
C     LOGICAL CI_LMTR_HVALIDE
C     INTEGER CI_LMTR_CLCDEL
C     INTEGER CI_LMTR_CLCERR
C     INTEGER CI_LMTR_DEB
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_LMTR_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cilmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_LMTR_NOBJMAX,
     &                   CI_LMTR_HBASE,
     &                   'Increment Controller - Limiter')

      CI_LMTR_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_LMTR_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cilmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER  IERR
      EXTERNAL CI_LMTR_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_LMTR_NOBJMAX,
     &                   CI_LMTR_HBASE,
     &                   CI_LMTR_DTR)

      CI_LMTR_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_LMTR_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_LMTR_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_LMTR_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cilmtr.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_LMTR_NOBJMAX,
     &                   CI_LMTR_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_LMTR_HVALIDE(HOBJ))
         IOB = HOBJ - CI_LMTR_HBASE

         DO ID=1,CI_LMTR_NVALMAX
            CI_LMTR_VVAL(ID, IOB) = 1.0D-1
         ENDDO
         CI_LMTR_DTMI(IOB) = -1.0D0
         CI_LMTR_TACT(IOB) = 0.0D0
         CI_LMTR_TFIN(IOB) = 0.0D0
         CI_LMTR_DTCI(IOB) = 0.0D0
         CI_LMTR_DTEF(IOB) = 0.0D0
         CI_LMTR_NVAL(IOB) = 0
      ENDIF

      CI_LMTR_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_LMTR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cilmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_LMTR_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_LMTR_NOBJMAX,
     &                   CI_LMTR_HBASE)

      CI_LMTR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_LMTR_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     DTMIN    DeltaT MINimal
C     HELE     Handle sur l'élément
C     NVAL     Nombre d'epsilon
C     VVAL     Epsilon absolue
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_LMTR_INI(HOBJ,
     &                     DTMIN,
     &                     HELE,
     &                     NVAL,
     &                     VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTMIN
      INTEGER HELE
      INTEGER NVAL
      REAL*8  VVAL(NVAL)

      INCLUDE 'cilmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ID, IV
      INTEGER NDLN
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (DTMIN .LE. 0.0D0) GOTO 9900
      IF (.NOT. LM_HELE_HVALIDE(HELE)) GOTO 9901
      NDLN = LM_HELE_REQPRM(HELE, LM_HELE_PRM_NDLN)
      IF (NVAL .NE. 1 .AND. NVAL .NE. NDLN) GOTO 9902
      DO ID=1,NVAL
         IF (VVAL(ID) .LE. 1.0D-16) GOTO 9903
         IF (VVAL(ID) .GT. 1.0D+08) GOTO 9903
      ENDDO

C---     Reset les données
      IERR = CI_LMTR_RST(HOBJ)

C---     Assigne les attributs
      IOB = HOBJ - CI_LMTR_HBASE
      DO ID=1,NDLN
         IV = MIN(ID, NVAL)
         CI_LMTR_VVAL(ID, IOB) = VVAL(IV)
      ENDDO
      CI_LMTR_DTMI(IOB) = DTMIN
      CI_LMTR_TACT(IOB) = 0.0D0
      CI_LMTR_TFIN(IOB) = 0.0D0
      CI_LMTR_DTCI(IOB) = 0.0D0
      CI_LMTR_DTEF(IOB) = 0.0D0
      CI_LMTR_DTMI(IOB) = DTMIN
      CI_LMTR_HELE(IOB) = HELE
      CI_LMTR_NVAL(IOB) = NDLN

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_HANDLE_INVALIDE',':',
     &                           HELE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HELE, 'MSG_SIMULATION')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NOMBRE_VALEUR_INVALIDE',':',
     &                           NVAL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)')
     &   'ERR_VALEUR_INVALIDE',':', VVAL(ID)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_LMTR_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_LMTR_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cilmtr.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_LMTR_HBASE
      DO ID=1, CI_LMTR_NVALMAX
         CI_LMTR_VVAL(ID, IOB) = 1.0D-1
      ENDDO
      CI_LMTR_DTMI(IOB) = -1.0D0
      CI_LMTR_TACT(IOB) = 0.0D0
      CI_LMTR_TFIN(IOB) = 0.0D0
      CI_LMTR_DTCI(IOB) = 0.0D0
      CI_LMTR_DTEF(IOB) = 0.0D0
      CI_LMTR_HELE(IOB) = 0
      CI_LMTR_NVAL(IOB) = 0

      CI_LMTR_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_LMTR_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cilmtr.fi'
      INCLUDE 'cilmtr.fc'
C------------------------------------------------------------------------

      CI_LMTR_REQHBASE = CI_LMTR_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_LMTR_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cilmtr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cilmtr.fc'
C------------------------------------------------------------------------

      CI_LMTR_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  CI_LMTR_NOBJMAX,
     &                                  CI_LMTR_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_LMTR_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cilmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
D     CALL ERR_PRE(DTEF .LE. CI_LMTR_DTEF(HOBJ-CI_LMTR_HBASE))
C------------------------------------------------------------------------

      CI_LMTR_DTEF(HOBJ-CI_LMTR_HBASE) = DTEF

      CI_LMTR_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_LMTR_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C************************************************************************
      FUNCTION CI_LMTR_CLCDEL(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cilmtr.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'nrutil.fi'
      INCLUDE 'cilmtr.fc'

      REAL*8  FMUL
      REAL*8  TACT, TFIN
      REAL*8  DTMI, DTCI, DTEF, DTRST
      REAL*8  R_D
      INTEGER IERR
      INTEGER IOB
      INTEGER ID, ISTTS
      INTEGER HELE, HNUMR
D     INTEGER NVAL
      LOGICAL DOCLC, DOMUL, DOLMT, DOINC
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.cinc.limiter'

C---     Récupère les attributs
      IOB = HOBJ - CI_LMTR_HBASE
      DTMI = CI_LMTR_DTMI(IOB)
      TACT = CI_LMTR_TACT(IOB)
      TFIN = CI_LMTR_TFIN(IOB)
      DTCI = CI_LMTR_DTCI(IOB)
      DTEF = CI_LMTR_DTEF(IOB)
      HELE = CI_LMTR_HELE(IOB)
D     NVAL = CI_LMTR_NVAL(IOB)
D     CALL ERR_ASR(NVAL .EQ. NDLN)

C---     Switch sur le type de pas
      FMUL = 1.0D0
      IF (.NOT. ESTOK) THEN               ! Pas non convergé
         FMUL = 0.5D0                     !  Coupe dT
         DOCLC = .FALSE.                  !  Calcule la norme
         DOMUL = .TRUE.                   !  Applique FMUL
         DOLMT = .FALSE.                  !  Limite le pas
         DOINC = .FALSE.                  !  Incrémente
         DTCI  = DTEF                     !  cf. note 1
      ELSEIF (TACT+DTEF .LT. TFIN) THEN   ! Pas intermédiaire
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .TRUE.
         DOINC = .TRUE.
      ELSEIF (DTEF .GE. DTCI) THEN        ! Pas final normal
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ELSE                                ! Pas final tronqué
         DOCLC = .FALSE.                  !  Rétablis au pas calculé
         DOMUL = .FALSE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ENDIF

C---     Calcule le facteur multiplicatif
      IF (DOCLC) THEN
         HNUMR = LM_HELE_REQHNUMC(HELE)
         FMUL = 1.0D99
         DO ID=1, NDLN
            R_D  = NR_UTIL_NISD(HNUMR, NDLN, NNL, VDEL, ID)
            R_D  = MAX(R_D, 1.0D-8)
            R_D  = CI_LMTR_VVAL(ID, IOB)/ R_D
!!            R_D  = MAX( MIN(R_D, 2.0D0), 0.5D0)
            FMUL = MIN(FMUL, R_D)
         ENDDO
      ENDIF

C---     Applique FMUL
      IF (DOMUL) DTCI = DTCI*FMUL

C---     Status
      ISTTS = CI_CINC_STATUS_UNDEFINED
      IF (DOINC)           ISTTS = CI_CINC_STATUS_OK
      IF (.NOT. DOINC)     ISTTS = CI_CINC_STATUS_COUPE
      IF (DTCI .LT. DTMI)  ISTTS = CI_CINC_STATUS_DTMIN

C---     Limite au pas min
      IF (DOMUL) THEN
         DTCI = MAX(DTCI, DTMI)
      ENDIF

C---     Incrémente la pas
      IF (DOINC) THEN
         TACT = TACT + DTEF
      ENDIF

C---     Limite le pas
      IF (DOLMT) THEN
         DTRST = TFIN - (TACT + DTCI)
         IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTCI)) THEN
            DTEF = TFIN-TACT
            FMUL = FMUL*(DTEF/DTCI)
         ELSE
            DTEF = MIN(DTCI, TFIN-TACT)
         ENDIF
      ELSE
         DTEF = DTCI
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_FACMUL#<35>#', '= ', FMUL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_CI#<35>#', '= ', DTCI
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_EFF#<35>#', '= ', DTEF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CI_LMTR_TACT(IOB) = TACT
      CI_LMTR_DTCI(IOB) = DTCI
      CI_LMTR_DTEF(IOB) = DTEF

C---     Valeurs retournées
      DELT(1) = DTCI
      DELT(2) = DTEF
      DELT(3) = ISTTS

      CI_LMTR_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_LMTR_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_LMTR_CLCERR(HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cilmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_LMTR_CLCDEL(HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_LMTR_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_LMTR_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_LMTR_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_LMTR_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cilmtr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cilmtr.fc'

      REAL*8  DTMI
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_LMTR_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Indice de l'objet
      IOB = HOBJ - CI_LMTR_HBASE

C---     Contrôles
      DTMI = CI_LMTR_DTMI(IOB)
      IF (DT0 .LT. DTMI) GOTO 9900

C---     Assigne les valeurs
      CI_LMTR_TACT(IOB) = TINI
      CI_LMTR_TFIN(IOB) = TFIN
      CI_LMTR_DTCI(IOB) = DT0
      CI_LMTR_DTEF(IOB) = DT0

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_INITIAL', ': ', DT0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMI
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_LMTR_DEB = ERR_TYP()
      RETURN
      END

