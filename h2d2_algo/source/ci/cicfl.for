C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Contrôle d'Incrément
C Objet:   CFL
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>CI_CFL_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(CI_CFL_NOBJMAX,
     &                   CI_CFL_HBASE,
     &                   'Increment Controller - CFL')

      CI_CFL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset la classe.
C
C Description:
C     La fonction <code>CI_CFL_999(...)</code> désalloue tous les objets
C     non encore désalloués. Elle doit être appelée après toute utilisation
C     des fonctionnalités des objets pour nettoyer les tables.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER  IERR
      EXTERNAL CI_CFL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(CI_CFL_NOBJMAX,
     &                   CI_CFL_HBASE,
     &                   CI_CFL_DTR)

      CI_CFL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe.
C
C Description:
C     Le constructeur <code>CI_CFL_CTR(...)</code> construit un objet
C     et retourne son handle.
C     <p>
C     L'objet est initialisé aux valeurs par défaut. C'est l'appel à
C     <code>CI_CFL_INI(...)</code> qui l'initialise avec des valeurs
C     utilisateurs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CFL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicfl.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   CI_CFL_NOBJMAX,
     &                   CI_CFL_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(CI_CFL_HVALIDE(HOBJ))
         IOB = HOBJ - CI_CFL_HBASE

         CI_CFL_DTMI(IOB) =-1.0D0
         CI_CFL_TACT(IOB) = 0.0D0
         CI_CFL_TFIN(IOB) = 0.0D0
         CI_CFL_DTCI(IOB) =-1.0D0
         CI_CFL_DTEF(IOB) =-1.0D0
         CI_CFL_CTGT(IOB) = 1.0D0
         CI_CFL_HCFL(IOB) = 0
         CI_CFL_ICFL(IOB) = 0
         CI_CFL_NPOK(IOB) = -1
      ENDIF

      CI_CFL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     Le destructeur <code>CI_PICD_DTR(...)</code> détruis l'objet et
C     tous ses attributs. En sortie, le handle n'est plus valide et
C     son utilisation n'est plus permise.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CFL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_CFL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   CI_CFL_NOBJMAX,
     &                   CI_CFL_HBASE)

      CI_CFL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet.
C
C Description:
C     La méthode <code>CI_CFL_INI(...)</code> initialise l'objet avec les
C     valeurs passées en arguments.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     HCFL     Handle sur le VNO de CFL
C     ICFL     Indice de colonne dans le VNO de CFL
C     CTGT     Target CFL
C
C Sortie:
C
C Notes:
C     HCFL est un VNO qui fournit le CFL actuel. Il peut être remis
C     à jour comme item de séquence (comme le lien sv2d-cd2d).
C************************************************************************
      FUNCTION CI_CFL_INI (HOBJ,
     &                     DTMIN,
     &                     HCFL,
     &                     ICFL,
     &                     CTGT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_INI
CDEC$ ENDIF

      IMPLICIT NONE

      REAL*8  DTMIN
      INTEGER HOBJ
      INTEGER HCFL
      INTEGER ICFL
      REAL*8  CTGT

      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ID, IV
      INTEGER NVN, NNL
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (DTMIN .LE. 0.0D0) GOTO 9900
      IF (.NOT. DT_VNOD_HVALIDE(HCFL)) GOTO 9901
      NVN = DT_VNOD_REQNVNO(HCFL)
      NNL = DT_VNOD_REQNNL (HCFL)
      IF (ICFL .LE. 0) GOTO 9902
      IF (ICFL .GT. NVN .AND. NNL .NE. DT_VNOD_DIM_DIFFERE) GOTO 9902
      IF (CTGT .LE. 0.0D0 .OR. CTGT .GT. 1.0D3) GOTO 9903

C---     Reset les données
      IERR = CI_CFL_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - CI_CFL_HBASE
         CI_CFL_DTMI(IOB) = DTMIN
         CI_CFL_TACT(IOB) = 0.0D0
         CI_CFL_TFIN(IOB) = 0.0D0
         CI_CFL_DTCI(IOB) =-1.0D0
         CI_CFL_DTEF(IOB) =-1.0D0
         CI_CFL_CTGT(IOB) = CTGT
         CI_CFL_HCFL(IOB) = HCFL
         CI_CFL_ICFL(IOB) = ICFL
         CI_CFL_NPOK(IOB) = -1
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMIN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',':', HCFL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCFL, 'MSG_CFL')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A)') 'ERR_NBRCOL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,A)') 'ERR_CFL_INVALIDE (0 < CFL < 1000)'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_CFL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CFL_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicfl.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IOB
      INTEGER ID
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - CI_CFL_HBASE
      CI_CFL_DTMI(IOB) =-1.0D0
      CI_CFL_TACT(IOB) = 0.0D0
      CI_CFL_TFIN(IOB) = 0.0D0
      CI_CFL_DTCI(IOB) =-1.0D0
      CI_CFL_DTEF(IOB) =-1.0D0
      CI_CFL_CTGT(IOB) = 1.0D0
      CI_CFL_HCFL(IOB) = 0
      CI_CFL_ICFL(IOB) = 0
      CI_CFL_NPOK(IOB) = -1

      CI_CFL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction CI_CFL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'cicfl.fi'
      INCLUDE 'cicfl.fc'
C------------------------------------------------------------------------

      CI_CFL_REQHBASE = CI_CFL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction CI_CFL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'cicfl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicfl.fc'
C------------------------------------------------------------------------

      CI_CFL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                 CI_CFL_NOBJMAX,
     &                                 CI_CFL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Assigne les exposants
C
C Description:
C     La méthode CI_CFL_ASGHCFL permet d'assigner les valeurs des
C     exposants utilisés dans le calcul du facteur multiplicatif.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Duplication complète du code de INI!!!
C************************************************************************
      FUNCTION CI_CFL_ASGHCFL(HOBJ, HCFL, ICFL, CTGT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_ASGCFL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCFL
      INTEGER ICFL
      REAL*8  CTGT

      INCLUDE 'cicfl.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER NVN, NNL
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle des données
      IF (.NOT. DT_VNOD_HVALIDE(HCFL)) GOTO 9901
      NVN = DT_VNOD_REQNVNO(HCFL)
      NNL = DT_VNOD_REQNNL (HCFL)
      IF (ICFL .LE. 0) GOTO 9902
      IF (ICFL .GT. NVN .AND. NNL .NE. DT_VNOD_DIM_DIFFERE) GOTO 9902
      IF (CTGT .LE. 0.0D0 .OR. CTGT .GT. 1.0D3) GOTO 9903

C---     Assigne les attributs
      IOB = HOBJ - CI_CFL_HBASE
      CI_CFL_HCFL(IOB) = HCFL
      CI_CFL_ICFL(IOB) = ICFL
      CI_CFL_CTGT(IOB) = CTGT

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',':', HCFL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HCFL, 'MSG_CFL')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A)') 'ERR_NBRCOL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,A)') 'ERR_CFL_INVALIDE (0 < CFL < 1000)'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_CFL_ASGHCFL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le CFL max.
C
C Description:
C     La fonction privée <code>CI_CFL_REQCMAX(...)</code> retourne le
C     CFL max de VCFL. Elle permet de résoudre la table VCFL.
C
C Entrée:
C     VCLF     Table de dimension (NCFL, NNL)
C     ICFL     Indice de la colonne de VCFL qui contient le CFL
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_REQCMAX(NCFL, NNL, VCFL, ICFL)

      IMPLICIT NONE

      REAL*8  CI_CFL_REQCMAX
      INTEGER NCFL
      INTEGER NNL
      REAL*8  VCFL(NCFL, NNL)
      INTEGER ICFL

      REAL*8  CMAX
      INTEGER IMAX

      INTEGER IDAMAX
C------------------------------------------------------------------------

      IMAX = IDAMAX(NNL, VCFL(ICFL,1), NCFL)
      CMAX = VCFL(ICFL, IMAX)

      CI_CFL_REQCMAX = CMAX
      RETURN
      END

C************************************************************************
C Sommaire:    Assigne l'incrément effectif.
C
C Description:
C     La fonction <code>CI_CFL_ASGDTEF(...)</code> permet d'assigner
C     par l'extérieur la pas de temps effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     DTEF        Incrément effectif
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_ASGDTEF(HOBJ, DTEF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_ASGDTEF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DTEF

      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
D     CALL ERR_PRE(DTEF .LE. CI_CFL_DTEF(HOBJ-CI_CFL_HBASE))
C------------------------------------------------------------------------

      CI_CFL_DTEF(HOBJ-CI_CFL_HBASE) = DTEF

      CI_CFL_ASGDTEF = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_CFL_CLCDEL(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C     1) Un pas tronqué peut générer un !OK. FMUL s'applique sur DTEF
C************************************************************************
      FUNCTION CI_CFL_CLCDEL(HOBJ,
     &                       NDLN,
     &                       NNL,
     &                       VDLG,
     &                       VDEL,
     &                       ESTOK,
     &                       DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_CLCDEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cicfl.fi'
      INCLUDE 'cicinc.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'cicfl.fc'

      INTEGER I_ERROR

      INTEGER IERR
      INTEGER IOB

      REAL*8  FMUL
      REAL*8  CTGT, CMAX, CMAX_L
      REAL*8  TACT, TFIN
      REAL*8  DTMI, DTCI, DTEF, DTRST, DTNW
      INTEGER IMAX, ISTTS
      INTEGER HCFL, NCFL, ICFL
      INTEGER NNL_L, LVNO
      INTEGER NPOK
      LOGICAL DOCLC, DOMUL, DOLMT, DOINC

      REAL*8  CI_CFL_REQCMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.algo.cinc.cfl'

C---     Récupère les attributs
      IOB = HOBJ - CI_CFL_HBASE
      DTMI = CI_CFL_DTMI(IOB)
      TACT = CI_CFL_TACT(IOB)
      TFIN = CI_CFL_TFIN(IOB)
      DTCI = CI_CFL_DTCI(IOB)
      DTEF = CI_CFL_DTEF(IOB)
      CTGT = CI_CFL_CTGT(IOB)
      HCFL = CI_CFL_HCFL(IOB)
      ICFL = CI_CFL_ICFL(IOB)
      NPOK = CI_CFL_NPOK(IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HCFL))

C---     Switch sur le type de pas
      IF (.NOT. ESTOK) THEN               ! Pas non convergé
         FMUL = 0.5D0                     !  Coupe dT
         NPOK = 0                         !  Initialise le compteur
         DOCLC = .FALSE.                  !  Calcule la norme
         DOMUL = .TRUE.                   !  Applique FMUL
         DOLMT = .FALSE.                  !  Limite le pas
         DOINC = .FALSE.                  !  Incrémente
         DTCI  = DTEF                     !  cf. note 1
      ELSEIF (TACT+DTEF .LT. TFIN) THEN   ! Pas intermédiaire
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .TRUE.
         DOINC = .TRUE.
      ELSEIF (DTEF .GE. DTCI) THEN        ! Pas final normal
         DOCLC = .TRUE.
         DOMUL = .TRUE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ELSE                                ! Pas final tronqué
         FMUL = 1.0D0                     !  Rétablis au pas calculé
         DOCLC = .FALSE.
         DOMUL = .FALSE.
         DOLMT = .FALSE.
         DOINC = .TRUE.
      ENDIF

C---     Calcule le CFL max
      IF (DOCLC) THEN
         NCFL  = DT_VNOD_REQNVNO(HCFL)
D        NNL_L = DT_VNOD_REQNNL (HCFL)
D        CALL ERR_ASR(ICFL  .LE. NCFL)
D        CALL ERR_ASR(NNL_L .EQ. NNL)

         LVNO = DT_VNOD_REQLVNO(HCFL)
         CMAX_L = CI_CFL_REQCMAX(NCFL,
     &                           NNL,
     &                           VA(SO_ALLC_REQVIND(VA,LVNO)),
     &                           ICFL)

         CALL MPI_ALLREDUCE(CMAX_L, CMAX, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Calcule le facteur multiplicatif
      IF (DOCLC) THEN
         IF (NPOK .LT. 0) THEN
            DTNW = CTGT / CMAX
            FMUL = DTNW / DTCI
            FMUL = MIN(1.0D+1, FMUL)
            FMUL = MAX(1.0D-2, FMUL)
         ELSE
            NPOK = NPOK + 1      ! Nombre de pas OK
            IF (MOD(NPOK, 2) .EQ. 0) FMUL = MIN(FMUL, SQRT(2.0D0))
            IF (NPOK .EQ. 4) NPOK = -1
         ENDIF
      ENDIF

C---     Applique FMUL
      IF (DOMUL) DTCI = DTCI*FMUL

C---     Status
      ISTTS = CI_CINC_STATUS_UNDEFINED
      IF (DOINC)           ISTTS = CI_CINC_STATUS_OK
      IF (.NOT. DOINC)     ISTTS = CI_CINC_STATUS_COUPE
      IF (DTCI .LT. DTMI)  ISTTS = CI_CINC_STATUS_DTMIN

C---     Limitant au pas min
      IF (DOMUL) THEN
         DTCI = MAX(DTCI, DTMI)
      ENDIF

C---     Incrémente la pas
      IF (DOINC) THEN
         TACT = TACT + DTEF
      ENDIF

C---     Limite le pas
      IF (DOLMT) THEN
         DTRST = TFIN - (TACT + DTCI)
         IF (DTRST .GT. 0.0D0 .AND. DTRST < (0.05D0*DTCI)) THEN
            DTEF = TFIN-TACT
            FMUL = FMUL*(DTEF/DTCI)
         ELSE
            DTEF = MIN(DTCI, TFIN-TACT)
         ENDIF
      ELSE
         DTEF = DTCI
      ENDIF

C---     Log
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_FACMUL#<35>#', '= ', FMUL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_CI#<35>#', '= ', DTCI
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_EFF#<35>#', '= ', DTEF
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     Conserve les valeurs
      CI_CFL_TACT(IOB) = TACT
      CI_CFL_DTCI(IOB) = DTCI
      CI_CFL_DTEF(IOB) = DTEF
      CI_CFL_NPOK(IOB) = NPOK

C---     Valeurs retournées
      DELT(1) = DTCI
      DELT(2) = DTEF
      DELT(3) = ISTTS

      CI_CFL_CLCDEL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule les nouveaux incréments.
C
C Description:
C     La méthode <code>CI_CFL_CLCERR(...)</code> calcule les nouveaux
C     incréments, l'incrément calculé et l'incrément effectif.
C
C Entrée:
C     HOBJ        L'objet courant
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VDLG        Table des Degrés de Liberté Globaux
C     VDEL        Table DELta des accroissements
C     VERR        Table des ERReurs
C     ESTOK       .TRUE. si le pas présent est convergé
C
C Sortie:
C     DELT(1)     Incrément calculé
C     DELT(2)     Incrément effectif
C
C Notes:
C************************************************************************
      FUNCTION CI_CFL_CLCERR (HOBJ,
     &                        NDLN,
     &                        NNL,
     &                        VDLG,
     &                        VDEL,
     &                        VERR,
     &                        ESTOK,
     &                        DELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_CLCERR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)
      REAL*8  VDEL(NDLN, NNL)
      REAL*8  VERR(NDLN, NNL)
      LOGICAL ESTOK
      REAL*8  DELT(3)

      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = CI_CFL_CLCDEL (HOBJ,
     &                      NDLN,
     &                      NNL,
     &                      VDLG,
     &                      VDEL,
     &                      ESTOK,
     &                      DELT)

      CI_CFL_CLCERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode <code>CI_CFL_DEB(...)</code> marque le début d'une
C     séquence de pas qui vont être adaptés.
C
C Entrée:
C     TINI     Temps initial de la séquence
C     TFIN     Temps de fin de la séquence
C     DT0      Pas initial
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CFL_DEB(HOBJ, TINI, TFIN, DT0)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_DEB
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TINI
      REAL*8  TFIN
      REAL*8  DT0

      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      REAL*8  DTMI
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Indice de l'objet
      IOB = HOBJ - CI_CFL_HBASE

C---     Contrôles
      DTMI = CI_CFL_DTMI(IOB)
      IF (DT0 .LT. DTMI) GOTO 9900

C---     Assigne les valeurs
      CI_CFL_TACT(IOB) = TINI
      CI_CFL_TFIN(IOB) = TFIN
      CI_CFL_DTCI(IOB) = DT0
      CI_CFL_DTEF(IOB) = DT0

C---     Test pour une continuation
      IF (CI_CFL_DTCI(IOB) .NE. DT0) THEN
         CI_CFL_NPOK(IOB) = -1
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_DELT_INVALIDE', ': '
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_INITIAL', ': ', DT0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'MSG_DELT_MINIMAL', ': ', DTMI
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CI_CFL_DEB = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les paramètres d'adaptation
C
C Description:
C     La méthode CI_CFL_REQHCFL retourne le handle sur le VNO
C     des CFL nodaux et le CFL cible.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION CI_CFL_REQHCFL(HOBJ, HCFL, CTGT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CI_CFL_REQHCFL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCFL
      REAL*8  CTGT

      INCLUDE 'cicfl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'cicfl.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(CI_CFL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - CI_CFL_HBASE
      HCFL = CI_CFL_HCFL(IOB)
      CTGT = CI_CFL_CTGT(IOB)

      CI_CFL_REQHCFL = ERR_TYP()
      RETURN
      END
