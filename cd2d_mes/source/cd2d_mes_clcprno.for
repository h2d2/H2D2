C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MES_CLCPRNO
C
C Description:
C     Calcul des propriétés nodales dépendantes de VDLG
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C      REAL*8    VDLG       Table des Degrés de Liberté nodaux Globaux
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MES_CLCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             VDLG,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MES_CLCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8  VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSE_CLCPRNO(VCORG,
     &                      KNGV,
     &                      VDJV,
     &                      VPRGL,
     &                      VPRNO,
     &                      VDLG,
     &                      IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_MES_CLCTPS (VPRGL,
     &                      VPRNO,
     &                      VDLG)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_MES_CLCTPS
C
C Description:
C    Calcul des composantes des termes puits-source
C
C Entrée:
C   VPRGL
C   VDLG
C   VPRNO
C
C Sortie: VPRNO
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D_MES_CLCTPS(VPRGL, VPRNO, VDLG)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      REAL*8 VDLG (LM_CMMN_NDLN,  EG_CMMN_NNL)

      INCLUDE 'cd2d_bse.fi'

      INTEGER IN
      INTEGER ICSOL
      REAL*8  GRA
      REAL*8  RHOW
      REAL*8  VISC
      REAL*8  WS
      REAL*8  TAUD
      REAL*8  TAUE
      REAL*8  DMT
      REAL*8  RHOS
      REAL*8  CKS
      REAL*8  CE

      REAL*8  WSED, EXPN
      REAL*8  CSOL
      REAL*8  PD
      INTEGER IPG, IPA

      REAL*8 QTIER
      PARAMETER (QTIER=1.333333333333333333333333333D0)
C-----------------------------------------------------------------------

C---     EXTRACTION DES PARAMETRES DU MILIEUX
      CALL XTR_PRGL_MES(VPRGL,
     &   GRA, RHOW, VISC, ICSOL, WS, TAUD, TAUE, DMT, RHOS, CKS, CE)
      IF (WS .GT. PETIT) RETURN  ! WS constante : calcul déjà complet

C---     PROPRIETES GLOBALES
      IPG = LM_CMMN_NPRGLL + (CD2D_BSE_NPRGL-CD2D_BSE_NPRGLL)
      WSED = VPRGL(IPG+1)
      EXPN = VPRGL(IPG+2)

C---     INDICES DANS VPRNO
      IPA = LM_CMMN_NPRNOL + 6

!$omp  parallel
!$omp& default(shared)
!$omp& private(IN, CSOL, WS, PD)

C---     BOUCLE SUR SUR LES NOEUDS
!$omp do
      DO IN =1,EG_CMMN_NNL

C---        CONCENTRATION DE SOLIDES EN SUPENSION
         CSOL = MAX(ZERO, VDLG(1, IN))

C---        VITESSE DE SEDIMENTATION
         IF     (ICSOL .EQ. 1) THEN          ! solides cohésifs
            WS = CKS*(CSOL**QTIER)
         ELSEIF (ICSOL .EQ. 2) THEN          ! solides non cohésifs
            WS = WSED*(UN - CSOL/RHOS)**EXPN
         ENDIF

C---        PROBABILITÉ DE DEPOT
         PD = VPRNO(IPA,IN)                  ! Probabilité de déposition

C---        PROPRIETES NODALES
         VPRNO(IPA,IN) = PD*WS               ! WS corrigée
      ENDDO
!$omp end do
!$omp end parallel

      RETURN
      END
