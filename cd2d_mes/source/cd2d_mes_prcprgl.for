C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MES_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MES_PRCPRGL (VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MES_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IGLL, IGC, IGW, IGE, ICSOL
      REAL*8  GRA, RHOW, VISC, WS
      REAL*8  TAUD, TAUE, DMT, RHOS, CKS, CE
      REAL*8  TAUCRE
      REAL*8  RD, D, WSED, ARGU, RE, EXPN
C-----------------------------------------------------------------------

C---     APPEL DU PARENT
      CALL CD2D_BSE_PRCPRGL (VPRGL, IERR)

C---     ECRIS L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_CALCULEES')
      CALL LOG_INCIND()

C---     INDICE DANS VPRGL
      IGC  = CD2D_BSE_NPRGLL +  7
      IGLL = LM_CMMN_NPRGLL + (CD2D_BSE_NPRGL-CD2D_BSE_NPRGLL)
      IGW  = IGLL + 1
      IGE  = IGLL + 2

C---     EXTRACTION DES PARAMETRES DU MILIEUX
      CALL XTR_PRGL_MES(VPRGL,
     &   GRA, RHOW, VISC, ICSOL, WS, TAUD, TAUE, DMT, RHOS, CKS, CE)

C---     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION SI ITAUE=1
      IF (VPRGL(IGC) .LE. PETIT) THEN
         CALL TCSHIELDS(GRA,RHOW,VISC,DMT,RHOS,TAUCRE)
         VPRGL(IGC) = TAUCRE
         IERR=CD2D_BSE_PRN1PRGL(IGC,'MSG_SIGMA_CRITIQUE_ERO',VPRGL(IGC))
      ENDIF


C---     VARIABLES DÉRIVÉES POUR LES SOLIDES NON COHÉSIFS
      IF (ICSOL .EQ. 2) THEN
         RD    = RHOS / RHOW       ! rapport des densités

C---        VITESSE DE SEDIMENTATION DISCRETE
         D = DMT*1.0D+06           ! diamètre moyen (micron)
         IF (D .LE. 100.0D0) THEN
            WSED=UN_18*(RD - UN)*GRA*DMT*DMT/VISC
         ELSEIF (D .LE. 1000.0D0) THEN
            ARGU=UN + 0.01D0*(RD - UN)*GRA*DMT*DMT*DMT/(VISC*VISC)
            WSED=10.0D0*VISC/DMT*(SQRT(ARGU) - UN)
         ELSE
            WSED=1.1D0*SQRT((RD - UN)*GRA*DMT)
         ENDIF

C---        CORRECTION DE LA VITESSE DE CHUTE EN FONCTION DE LA
C---        CONCENTRATION TOTALE DE SOLIDES EN SUSPENSION
         RE = DMT*WSED/VISC        ! nombre de reynolds particulaire
         IF (RE .LE. UN) THEN
            EXPN = 4.65D0
         ELSEIF (RE .LT. 1000.0D0) THEN
            EXPN = 4.D0
         ELSE
            EXPN = 2.31D0
         ENDIF

         VPRGL(IGW) = WSED
         VPRGL(IGE) = EXPN

         IERR=CD2D_BSE_PRN1PRGL(IGW,'MSG_W_SED_NON_COESIF',VPRGL(IGW))
         IERR=CD2D_BSE_PRN1PRGL(IGW,'MSG_EXPO_CORRECTION',VPRGL(IGE))
      ENDIF

      CALL LOG_DECIND()

      IERR = ERR_TYP()
      RETURN
      END

C**************************************************************
C Sommaire :  TCSHIELDS
C
C Description:
C     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION SELON LE DIAGRAMME DE SHIELDS
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      SUBROUTINE TCSHIELDS(GRA,RHOW,VISC,DMT,RHOS,TAUCRE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'

      REAL*8 GRA
      REAL*8 RHOW
      REAL*8 VISC
      REAL*8 DMT
      REAL*8 RHOS
      REAL*8 TAUCRE

      INCLUDE 'log.fi'

      REAL*8 RD, RD_UN
      REAL*8 DSED
      REAL*8 SHIELDS
      REAL*8 UETCRE
C-----------------------------------------------------------------------

C---     EXTRACTION DES PROPRIÉTÉS GLOBALES DE BASE (PROPRIÉTÉS DU MILIEU)
      RD    = RHOS / RHOW                        ! rapport des densités
      RD_UN = RD - UN
      RD_UN = MAX(ZERO,RD_UN)
      DSED  = DMT*((GRA*RD_UN/(VISC*VISC))**UN_3)! diamètre sédimentologique
      DSED  = MAX(PETIT,DSED)

C---     CALCUL DE LA VALEUR CRITIQUE DU PARAMÈTRE ADIMENSIONNEL DE SHIELDS
      IF     (                      DSED .LE.  4.D0) THEN
         SHIELDS = 0.24D0/DSED
      ELSEIF(DSED .GT.  4.D0 .AND. DSED .LE. 10.D0) THEN
         SHIELDS = 0.14D0*(DSED**(-0.64D0))
      ELSEIF(DSED .GT. 10.D0 .AND. DSED .LE. 20.D0) THEN
         SHIELDS = 0.04D0*(DSED**(-0.10D0))
      ELSEIF(DSED .GT. 20.D0 .AND. DSED .LE.150.D0) THEN
         SHIELDS = 0.013D0*(DSED**(0.29D0))
      ELSEIF(DSED .GT.150.D0                      ) THEN
         SHIELDS = 0.055D0
      ENDIF

C---     VITESSE DE CISAILLEMENT CRITIQUE D'ÉROSION
      UETCRE = SQRT(GRA*RD_UN*SHIELDS*DMT)

C---     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION
      TAUCRE = RHOW*UETCRE*UETCRE

C---     PLACER CONTRAINTE CRITIQUE EN PROP. GLOB.
      IF (TAUCRE .LT .PETIT) TAUCRE = PETIT

C---     IMPRESSION DE L'INFO
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,1001)'CALCUL DE LA CONTRAINTE D''EROSION' //
     &                      'D''APRES LE DIAG. DE SHIELDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()

         WRITE(LOG_BUF,1001)'DIAMETRE SEDIMENTOLOGIQUE#<38>#=',
     &                       DSED
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)'VAL. CRIT. PARAM. ADIM. DE SHIELDS#<38>#=',
     &                       SHIELDS
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)'VIT. DE CISAILLEMENT CRIT. EROSION#<38>#=',
     &                       UETCRE
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_DECIND()
      ENDIF
C---------------------------------------------------------------
1001  FORMAT(A,1PE14.6E3)
C---------------------------------------------------------------
      RETURN
      END

