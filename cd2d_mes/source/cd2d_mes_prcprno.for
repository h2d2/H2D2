C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MES_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MES_PRCPRNO (VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MES_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSEN_PRCPRNO(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_MES_PRCTPS (VPRGL,
     &                      VPRNO)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_MES_PRCTPS
C
C Description:
C     PRE-CALCUL DU TERME PUITS-SOURCE DE FLUX DE CHALEUR
C
C Entrée: VPRGL,VDLG,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C     le calcul (13) (16) n'est correct que pour ws constant.
C     pour ws non constant, ils sont recalculées dans CLCPRNO
C************************************************************************
      SUBROUTINE CD2D_MES_PRCTPS(VPRGL, VPRNO)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)

      INTEGER IN
      INTEGER ICSOL
      REAL*8  GRA
      REAL*8  RHOW
      REAL*8  VISC
      REAL*8  WS
      REAL*8  TAUD
      REAL*8  TAUE
      REAL*8  DMT
      REAL*8  RHOS
      REAL*8  CKS
      REAL*8  CE

      REAL*8  TAU
      REAL*8  PD, PE
      INTEGER IPTAU, IPA, IPB
C-----------------------------------------------------------------------

C---     EXTRACTION DES PARAMETRES DU MILIEUX
      CALL XTR_PRGL_MES(VPRGL,
     &   GRA, RHOW, VISC, ICSOL, WS, TAUD, TAUE, DMT, RHOS, CKS, CE)

C---     MET WS=1 POUR LE CAS WS
      IF (WS .LE. PETIT) WS = UN

C---     INDICES DANS VPRNO
      IPTAU = 6
      IPA   = LM_CMMN_NPRNOL + 6
      IPB   = LM_CMMN_NPRNOL + 7

C---     BOUCLE SUR SUR LES NOEUDS
      DO IN =1,EG_CMMN_NNL

C---        CONTRAINTE DE CISAILLEMENT AU FOND (TAU)
         TAU = VPRNO(IPTAU,IN)

C---        COMPARAISON DU CISAILLEMENT AU CISAILLEMENT CRITIQUE / PROBABILITÉ DE DEPOT
         IF (TAU .LT. TAUD) THEN
            PD = (UN - TAU/TAUD)
            PE = ZERO
         ELSEIF (TAU .LE. TAUE) THEN
            PD = ZERO
            PE = ZERO
         ELSE
            PD = ZERO
            PE = (TAU/TAUE - UN)
         ENDIF

C---        PROPRIETES NODALES
         VPRNO(IPA, IN) = PD*WS             ! Terme linéaire: WS corrigée
         VPRNO(IPB, IN) = PE*CE             ! Terme constant
      ENDDO

      RETURN
      END

C************************************************************************
C Sommaire :  XTR_PRGL_MES
C
C Description:
C     EXTRAIT DE VPRGL LES PARAMETRES DU MILIEUX
C
C Entrée:
C     VPRGL
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE XTR_PRGL_MES(VPRGL,
     &                        GRA,
     &                        RHOW,
     &                        VISC,
     &                        ICSOL,
     &                        WS,
     &                        TAUD,
     &                        TAUE,
     &                        DMT,
     &                        RHOS,
     &                        CKS,
     &                        CE)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  GRA
      REAL*8  RHOW
      REAL*8  VISC
      INTEGER ICSOL
      REAL*8  WS
      REAL*8  TAUD
      REAL*8  TAUE
      REAL*8  DMT
      REAL*8  RHOS
      REAL*8  CKS
      REAL*8  CE

      INCLUDE 'cd2d_bse.fi'

      INTEGER IP
C----------------------------------------------------------------

C---     EXTRACTION DES PROPRIÉTÉS GLOBALES DE BASE (PROPRIÉTÉS DU MILIEU)
      IP = CD2D_BSE_NPRGLL
      GRA   = VPRGL(IP+ 1)       ! gravité
      RHOW  = VPRGL(IP+ 2)       ! kg/m3
      VISC  = VPRGL(IP+ 3)       ! viscosité cinématique
      ICSOL = NINT(VPRGL(IP+4))  ! code pour solides cohésif (1) ou non (2)
      WS    = VPRGL(IP+ 5)       ! vitesse de chute, si connue...
      TAUD  = VPRGL(IP+ 6)       ! cisaillement critique de déposition
      TAUE  = VPRGL(IP+ 7)       ! cisaillement critique d'érosion
      DMT   = VPRGL(IP+ 8)       ! diamètre moyen (m)
      RHOS  = VPRGL(IP+ 9)       ! masse volumique moyenne (kg/m3)
      CKS   = VPRGL(IP+10)       ! cnst de sédimentation solides cohésifs
      CE    = VPRGL(IP+11)       ! coefficient d'érosion (kg/m2/s)

      RETURN
      END
