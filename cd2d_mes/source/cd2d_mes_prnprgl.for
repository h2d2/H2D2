C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES)
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MES_PRNPRGL
C
C Description:
C     IMPRESSION DES PROPRIETES GLOBALES
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MES_PRNPRGL(NPRGL,
     &                            VPRGL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MES_PRNPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      INTEGER NPRGL
      REAL*8  VPRGL(LM_CMMN_NPRGL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IERR
      INTEGER I, IPL, IPC
C-----------------------------------------------------------------------

C---     APPEL LE PARENT
      CALL CD2D_BSE_PRNPRGL(LM_CMMN_NPRGL, VPRGL)

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_LUES:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      I = CD2D_BSE_NPRGLL
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_GRAVITE',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_RHO_EAU',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_VISCO_CINE_EAU',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_MES_COHESIFS_OU_NON',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_W_SED_CONSTANTE',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_SIGMA_CRITIQUE_DEPO',VPRGL(I))
      I = I + 1
      IF (VPRGL(I) .GT. PETIT) THEN
         IERR=CD2D_BSE_PRN1PRGL(I,'MSG_SIGMA_CRITIQUE_ERO',VPRGL(I))
      ELSE
         IERR=CD2D_BSE_PRN1PRGL(I,'---',VPRGL(I))
      ENDIF
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_D_MOYEN_SEDIMENTS',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_RHO_MOYEN_SEDIMENTS',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_CNST_SEDIMENTATION',VPRGL(I))
      I = I + 1
      IERR=CD2D_BSE_PRN1PRGL(I,'MSG_COEF_ERO',VPRGL(I))
      CALL LOG_DECIND()

C---     IMPRESSION DU TITRE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_CALCULEES:')

C---     IMPRESSION DE L'INFO
      CALL LOG_INCIND()
      IPL = CD2D_BSE_NPRGLL + 7
      IPC = LM_CMMN_NPRGLL
      IF (VPRGL(IPL) .LE. PETIT) THEN
         IERR=CD2D_BSE_PRN1PRGL(IPL,'MSG_SIGMA_CRITIQUE_ERO',VPRGL(IPL))
      ENDIF
      IERR=CD2D_BSE_PRN1PRGL(IPC+1,'MSG_W_SED_NON_COHESIF',VPRGL(IPC+1))
      IERR=CD2D_BSE_PRN1PRGL(IPC+2,'MSG_EXPO_CORRECTION',VPRGL(IPC+2))
      CALL LOG_DECIND()

      RETURN
      END

