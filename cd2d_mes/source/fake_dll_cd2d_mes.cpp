//************************************************************************
// H2D2 - External declaration of public symbols
// Module: cd2d_mes
// Entry point: extern "C" void fake_dll_cd2d_mes()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:46:56.265847
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class CD2D_MES
F_PROT(CD2D_MES_000, cd2d_mes_000);
F_PROT(CD2D_MES_REQHBASE, cd2d_mes_reqhbase);
F_PROT(CD2D_MES_ASGCMN, cd2d_mes_asgcmn);
F_PROT(CD2D_MES_CLCPRNO, cd2d_mes_clcprno);
F_PROT(CD2D_MES_HLPPRGL, cd2d_mes_hlpprgl);
F_PROT(CD2D_MES_HLPPRNO, cd2d_mes_hlpprno);
F_PROT(CD2D_MES_PRCPRGL, cd2d_mes_prcprgl);
F_PROT(CD2D_MES_PRCPRNO, cd2d_mes_prcprno);
F_PROT(CD2D_MES_PRNPRGL, cd2d_mes_prnprgl);
F_PROT(CD2D_MES_PRNPRNO, cd2d_mes_prnprno);
F_PROT(CD2D_MES_PSLPRGL, cd2d_mes_pslprgl);
F_PROT(CD2D_MES_REQNFN, cd2d_mes_reqnfn);
F_PROT(CD2D_MES_REQPRM, cd2d_mes_reqprm);
 
// ---  class IC_CD2D
F_PROT(IC_CD2D_MES_XEQCTR, ic_cd2d_mes_xeqctr);
F_PROT(IC_CD2D_MES_XEQMTH, ic_cd2d_mes_xeqmth);
F_PROT(IC_CD2D_MES_OPBDOT, ic_cd2d_mes_opbdot);
F_PROT(IC_CD2D_MES_REQCLS, ic_cd2d_mes_reqcls);
F_PROT(IC_CD2D_MES_REQHDL, ic_cd2d_mes_reqhdl);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_cd2d_mes()
{
   static char libname[] = "cd2d_mes";
 
   // ---  class CD2D_MES
   F_RGST(CD2D_MES_000, cd2d_mes_000, libname);
   F_RGST(CD2D_MES_REQHBASE, cd2d_mes_reqhbase, libname);
   F_RGST(CD2D_MES_ASGCMN, cd2d_mes_asgcmn, libname);
   F_RGST(CD2D_MES_CLCPRNO, cd2d_mes_clcprno, libname);
   F_RGST(CD2D_MES_HLPPRGL, cd2d_mes_hlpprgl, libname);
   F_RGST(CD2D_MES_HLPPRNO, cd2d_mes_hlpprno, libname);
   F_RGST(CD2D_MES_PRCPRGL, cd2d_mes_prcprgl, libname);
   F_RGST(CD2D_MES_PRCPRNO, cd2d_mes_prcprno, libname);
   F_RGST(CD2D_MES_PRNPRGL, cd2d_mes_prnprgl, libname);
   F_RGST(CD2D_MES_PRNPRNO, cd2d_mes_prnprno, libname);
   F_RGST(CD2D_MES_PSLPRGL, cd2d_mes_pslprgl, libname);
   F_RGST(CD2D_MES_REQNFN, cd2d_mes_reqnfn, libname);
   F_RGST(CD2D_MES_REQPRM, cd2d_mes_reqprm, libname);
 
   // ---  class IC_CD2D
   F_RGST(IC_CD2D_MES_XEQCTR, ic_cd2d_mes_xeqctr, libname);
   F_RGST(IC_CD2D_MES_XEQMTH, ic_cd2d_mes_xeqmth, libname);
   F_RGST(IC_CD2D_MES_OPBDOT, ic_cd2d_mes_opbdot, libname);
   F_RGST(IC_CD2D_MES_REQCLS, ic_cd2d_mes_reqcls, libname);
   F_RGST(IC_CD2D_MES_REQHDL, ic_cd2d_mes_reqhdl, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
