//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Pont avec la DLL SuperLU
//
//************************************************************************
#ifndef C_SLUM_H_DEJA_INCLU
#define C_SLUM_H_DEJA_INCLU

#ifndef MODULE_SUPERLU
#  define MODULE_SUPERLU 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_SLUM_ASGMSG C_SLUM_ASGMSG
#  define C_SLUM_INIT   C_SLUM_INIT
#  define C_SLUM_RESET  C_SLUM_RESET
#  define C_SLUM_FACT   C_SLUM_FACT
#  define C_SLUM_SOLVE  C_SLUM_SOLVE
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_SLUM_ASGMSG c_slum_asgmsg_
#  define C_SLUM_INIT   c_slum_init_
#  define C_SLUM_RESET  c_slum_reset_
#  define C_SLUM_FACT   c_slum_fact_
#  define C_SLUM_SOLVE  c_slum_solve_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_SLUM_ASGMSG c_slum_asgmsg__
#  define C_SLUM_INIT   c_slum_init__
#  define C_SLUM_RESET  c_slum_reset__
#  define C_SLUM_FACT   c_slum_fact__
#  define C_SLUM_SOLVE  c_slum_solve__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_SUPERLU)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_ASGMSG(F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_INIT  (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_RESET (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_FACT  (hndl_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUM_SOLVE (hndl_t*, fint_t*, fint_t*, double*);


#ifdef __cplusplus
}
#endif

#endif   // C_SLUM_H_DEJA_INCLU
