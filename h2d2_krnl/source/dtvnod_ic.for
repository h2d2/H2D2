C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_VNOD_XEQCTR
C     INTEGER IC_DT_VNOD_XEQMTH
C     CHARACTER*(32) IC_DT_VNOD_REQCLS
C     INTEGER IC_DT_VNOD_REQHDL
C   Private:
C     SUBROUTINE IC_DT_VNOD_XEQCTRFIC
C     SUBROUTINE IC_DT_VNOD_XEQCTRDIR
C     INTEGER IC_DT_VNOD_XEQCTRLST
C     INTEGER IC_DT_VNOD_XEQCTRVAL
C     INTEGER IC_DT_VNOD_REQMOD
C     SUBROUTINE IC_DT_VNOD_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VNOD_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER IERR
      INTEGER IMOD
      INTEGER HOBJ
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_VNOD_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Extrait le mode
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IERR = IC_DT_VNOD_REQMOD(IPRM, IMOD)

C---     Appel le constructeur spécialisé
      IF (IMOD .EQ. IC_DT_VNOD_MOD_FIC) THEN
C        <include>IC_DT_VNOD_XEQCTRFIC@dtvnod_ic.for</include>
         IERR = IC_DT_VNOD_XEQCTRFIC(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_VNOD_MOD_DIR) THEN
C        <include>IC_DT_VNOD_XEQCTRDIR@dtvnod_ic.for</include>
         IERR = IC_DT_VNOD_XEQCTRDIR(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_VNOD_MOD_LST) THEN
C        <include>IC_DT_VNOD_XEQCTRLST@dtvnod_ic.for</include>
         IERR = IC_DT_VNOD_XEQCTRLST(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_VNOD_MOD_VAL) THEN
C        <include>IC_DT_VNOD_XEQCTRVAL@dtvnod_ic.for</include>
         IERR = IC_DT_VNOD_XEQCTRVAL(IPRM, HOBJ)
      ENDIF

C---     Imprime l'objet
      IF (ERR_GOOD()) IERR = DT_VNOD_PRN(HOBJ)

      IF (ERR_BAD()) GOTO 9988
      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_VNOD_AID()

9999  CONTINUE
      IC_DT_VNOD_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQCTRFIC(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER         IERR, IRET
      INTEGER         ISTAT
      INTEGER         HLST
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
C     <comment>Name of the nodal value file</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IRET .NE. 0) MODE = 'r'
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9903

C---     Construis et initialise l'objet
      HOBJ = 0
      HLST = 0
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_VNOD_INIFIC(HOBJ, HLST, NOMFIC, ISTAT)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the nodal value</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>vnod</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_VNOD_XEQCTRFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQCTRDIR(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'c_os.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'f_lc.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER IERR, IRET
      INTEGER IRCR
      INTEGER ISTAT
      INTEGER MR, L
      INTEGER HLST
      CHARACTER*(256) NOMDIR, SPATRN, NOMTMP
      CHARACTER*(  8) MODE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      IRET = 0
      HLST = 0
      NOMTMP = ' '

C---     Lis les paramètres
C     <comment>Name of the root directory</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMDIR)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Search pattern (as in "*.vno")</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, SPATRN)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Do recursive search [0, 1] (default 1 == True) </comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 3, IRCR)
      IF (IRET .NE. 0) IRCR = 1
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 4, MODE)
      IF (IRET .NE. 0) MODE = 'r'
      CALL SP_STRN_TRM(NOMDIR)
      CALL SP_STRN_TRM(SPATRN)

C---     Valide
      !!IF (SP_STRN_LEN(NOMDIR) .LE. 0) GOTO 9902
      IF (SP_STRN_LEN(SPATRN) .LE. 0) GOTO 9903
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904

C---     Nom de fichier temporaire
      IF (ERR_GOOD()) THEN
         IRET = C_OS_FICTMP(NOMTMP)
         IF (IRET .NE. 0) GOTO 9905
      ENDIF

C---     Charge le fichier temporaire avec les noms de fichier
      IF (ERR_GOOD()) THEN
         IRCR = F_LC_L2I( F_LC_I2L(IRCR) )   ! Normalise
         IRET = C_OS_NORMPATH(NOMDIR)
         IRET = C_OS_DIR(NOMDIR(1:SP_STRN_LEN(NOMDIR)),
     &                   SPATRN(1:SP_STRN_LEN(SPATRN)),
     &                   NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &                   IRCR)
         IF (IRET .NE. 0) GOTO 9906
      ENDIF

C---     Transfert le fichier en liste
      IF (ERR_GOOD()) THEN
         IERR = DS_LIST_CTR(HLST)
         IERR = DS_LIST_INI(HLST)
      ENDIF

C---     Transfert le fichier en liste
      IF (ERR_GOOD()) THEN
         MR = IO_UTIL_FREEUNIT()
         OPEN(UNIT  = MR,
     &        FILE  = NOMTMP(1:SP_STRN_LEN(NOMTMP)),
     &        STATUS= 'OLD',
     &        ERR   = 9907)
         DO WHILE(ERR_GOOD())      ! Squat NOMDIR
            READ(MR,'(A)', ERR = 9908, END = 199) NOMDIR
            IERR = DS_LIST_AJTVAL(HLST, NOMDIR)
         ENDDO
199      CONTINUE
         CLOSE(MR)
      ENDIF

C---     Construis et initialise l'objet
      HOBJ = 0
      NOMDIR = ' '      ! Squat NOMDIR
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_VNOD_INIFIC(HOBJ, HLST, NOMDIR, ISTAT)

C---     Nettoie
      CALL ERR_PUSH() 
      IF (SP_STRN_LEN(NOMTMP) .GT. 0)
     &   IRET = C_OS_DELFIC(NOMTMP(1:SP_STRN_LEN(NOMTMP)))
      IF (DS_LIST_HVALIDE(HLST))
     &   IERR = DS_LIST_DTR(HLST)
      CALL ERR_POP()

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the nodal value</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>vnod</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_DIR_INVALIDE',': ',
     &                        NOMDIR(1:SP_STRN_LEN(NOMDIR))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_PATTERN_INVALIDE',': ',
     &                        SPATRN(1:SP_STRN_LEN(SPATRN))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(A)') 'ERR_CREE_FICTMP'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(A)') 'ERR_OS_DIR'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT('MSG_ROOT_DIR: ' // NOMDIR(1:SP_STRN_LEN(NOMDIR)))
      CALL ERR_AJT('MSG_PATTERN:  ' // SPATRN(1:SP_STRN_LEN(SPATRN)))
      GOTO 9999
9907  WRITE(ERR_BUF, '(3A)') 'ERR_OUVERTURE_FICHIER',': ',
     &                     NOMTMP(1:SP_STRN_LEN(NOMTMP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9908  WRITE(ERR_BUF, '(3A)') 'ERR_LECTURE_FICHIER',': ',
     &                     NOMTMP(1:SP_STRN_LEN(NOMTMP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
C---     Nettoie
      CALL ERR_PUSH()
      IF (DS_LIST_HVALIDE(HLST)) IERR = DS_LIST_CTR(HLST)
      L = SP_STRN_LEN(NOMTMP)
      IF (L .GT. 0) IRET = C_OS_DELFIC(NOMTMP(1:L))
      CALL ERR_POP()

      IC_DT_VNOD_XEQCTRDIR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQCTRLST(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER IERR, IRET
      INTEGER ISTAT
      INTEGER HLST
      CHARACTER*(8) NOMFIC
      CHARACTER*(8) MODE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      HLST = 0
      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
C     <comment>Handle on a list of nodal value file names</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HLST)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IRET .NE. 0) MODE = 'r'

C---     Valide
      IF (.NOT. DS_LIST_HVALIDE(HLST)) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9903

C---     Construis et initialise l'objet
      HOBJ = 0
      NOMFIC = ' '
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_VNOD_INIFIC(HOBJ, HLST, NOMFIC, ISTAT)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the nodal value</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>vnod</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE',': ', HLST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HLST, 'MSG_LISTE')
      GOTO 9999
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_VNOD_XEQCTRLST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQCTRVAL(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       HOBJ

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER IERR, IRET
      INTEGER I, IV
      INTEGER HGRD
      INTEGER NVAL, NNL, NNT
      INTEGER, PARAMETER :: NVALMAX = 24
      REAL*8  VVAL(NVALMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
      I = 1
C     <comment>Handle on the grid</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', I, HGRD)
      IF (IRET .NE. 0) GOTO 9901
100   CONTINUE
         IF (I.EQ.NVALMAX) GOTO 9902
         I = I + 1
C        <comment>List of values, comma separated.</comment>
         IRET = SP_STRN_TKR(IPRM, ',', I, VVAL(I-1))
C ---       Contrôles
         IF (IRET .EQ. -1) GOTO 199
         IF (IRET .NE.  0) GOTO 9900
         IRET = SP_STRN_TKI(IPRM, ',', I, IV)
         IF (IRET .EQ. 0 .AND. OB_OBJN_HEXIST(IV)) GOTO 9901
      GOTO 100
199   CONTINUE
      NVAL = I-2

C---     Valide
      IF (.NOT. DT_GRID_HVALIDE(HGRD)) GOTO 9903
      IF (NVAL .LE. 0) GOTO 9904

C---     Construis et initialise l'objet
      HOBJ = 0
      NNL = DT_GRID_REQNNL(HGRD)
      NNT = DT_GRID_REQNNT(HGRD)
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_VNOD_INIDIM(HOBJ,
     &                                      NVAL,
     &                                      NNL,
     &                                      NNT,
     &                                      .TRUE., ! do init
     &                                      VVAL)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the nodal value</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>vnod</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_HANDLE_EN_PARAMETRE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I2)') 'ERR_HANDLE_INVALIDE',': ', HGRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HGRD, 'MSG_MAILLAGE')
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_VNOD_XEQCTRVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VNOD_REQMOD(IPRM, IMOD)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       IMOD

      INCLUDE 'dslist.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER IERR, IRET
      INTEGER HNDL
      INTEGER ISTAT
      INTEGER NTOK
      CHARACTER*(256) STR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IMOD = IC_DT_VNOD_MOD_INDEFINI

C---     Lis le 1er paramètre
      IRET = 0
      IF (IRET .EQ. 0) NTOK = SP_STRN_NTOK(IPRM, ',')
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS (IPRM, ',', 1, STR)
      IF (IRET .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(STR)

C---     Valide
      IF (NTOK .LT. 1 .OR. NTOK .GT. 4) GOTO 9901

C---     Détecte le type
      IMOD = IC_DT_VNOD_MOD_INDEFINI
      HNDL = 0
      IF (SP_STRN_UINT(STR)) THEN
         IRET = SP_STRN_TKI(STR, ',', 1, HNDL)
         IF (IRET .NE. 0) GOTO 9901
         IF (DS_LIST_HVALIDE(HNDL)) THEN
            IMOD = IC_DT_VNOD_MOD_LST
         ELSEIF (DT_GRID_HVALIDE(HNDL)) THEN
            IMOD = IC_DT_VNOD_MOD_VAL
         ENDIF
      ENDIF
      IF (IMOD .EQ. IC_DT_VNOD_MOD_INDEFINI) THEN
         IF (NTOK .EQ. 1) THEN
            IMOD = IC_DT_VNOD_MOD_FIC
         ELSEIF (NTOK .GE. 3) THEN
            IMOD = IC_DT_VNOD_MOD_DIR
         ELSE
            IRET = SP_STRN_TKS (IPRM, ',', 2, STR)
            IF (IRET .NE. 0) GOTO 9901
            ISTAT = IO_UTIL_STR2MOD(STR(1:SP_STRN_LEN(STR)))
            IF (ISTAT .EQ. IO_MODE_INDEFINI) THEN
               IMOD = IC_DT_VNOD_MOD_DIR
            ELSE
               IMOD = IC_DT_VNOD_MOD_FIC
            ENDIF
         ENDIF
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_VNOD_REQMOD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_VNOD_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VNOD_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod_ic.fc'

      INTEGER         IERR
      INTEGER         IVAL
      REAL*8          RVAL
      CHARACTER*(64)  PROP
      CHARACTER*(256) NOMFIC
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Number of nodes local to the process</comment>
         IF (PROP .EQ. 'nodes_local') THEN
            IVAL = DT_VNOD_REQNNL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Total number of nodes</comment>
         ELSEIF (PROP .EQ. 'nodes_total') THEN
            IVAL = DT_VNOD_REQNNT(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Number of nodal values</comment>
         ELSEIF (PROP .EQ. 'nodal_values') THEN
            IVAL = DT_VNOD_REQNVNO(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL
C        <comment>Time</comment>
         ELSEIF (PROP .EQ. 'time') THEN
            RVAL = DT_VNOD_REQTEMPS(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'R', ',', IVAL
         ELSE
            GOTO 9902
         ENDIF

!!!C     <comment>The method <b>save</b> saves the nodal values to the specified file.</comment>
!!!      ELSEIF (IMTH .EQ. 'save') THEN
!!!D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
!!!C        <include>IC_DT_VNOD_XEQMTH_SAVE@dtvnod_ic.for</include>
!!!         IERR = IC_DT_VNOD_XEQMTH_SAVE(HOBJ, IPRM)

!!!C     <comment>The method <b>load</b> loads the nodal values from file.</comment>
!!!      ELSEIF (IMTH .EQ. 'load') THEN
!!!D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
!!!C        <include>IC_DT_VNOD_XEQMTH_LOAD@dtvnod_ic.for</include>
!!!         IERR = IC_DT_VNOD_XEQMTH_LOAD(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_VNOD_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_VNOD_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_VNOD_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_VNOD_AID()

9999  CONTINUE
      IC_DT_VNOD_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VNOD_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VNOD_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvnod_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>vnod</b> represents the nodal values, varying in time. The nodal
C  values are all the values related to the nodes.
C</comment>
      IC_DT_VNOD_REQCLS = 'vnod'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VNOD_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VNOD_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvnod_ic.fi'
      INCLUDE 'dtvnod.fi'
C-------------------------------------------------------------------------

      IC_DT_VNOD_REQHDL = DT_VNOD_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_VNOD_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_VNOD_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtvnod_ic.hlp')

      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C     La fonction privée IC_DT_VNOD_XEQMTH_LOAD exécute la méthode LOAD
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C
!!!C************************************************************************
!!!      FUNCTION IC_DT_VNOD_XEQMTH_LOAD(HOBJ, IPRM)
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER       HOBJ
!!!      CHARACTER*(*) IPRM
!!!
!!!      INCLUDE 'ioutil.fi'
!!!      INCLUDE 'dtvnod.fi'
!!!      INCLUDE 'hssimd.fi'
!!!      INCLUDE 'lmelnw.fi'  ! Pour le cast
!!!      INCLUDE 'spstrn.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'log.fi'
!!!      INCLUDE 'dtvnod_ic.fc'
!!!
!!!      REAL*8  TEMPS
!!!      INTEGER IERR
!!!      INTEGER HSIM
!!!      LOGICAL MODIF
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      IERR = ERR_OK
!!!
!!!C---     EN-TETE DE COMMANDE
!!!      LOG_BUF = ' '
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!      WRITE (LOG_BUF, '(A)') 'MSG_CHARGE_VNO'
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!      CALL LOG_INCIND()
!!!
!!!C---     LECTURE DES PARAM
!!!      HSIM = 0
!!!      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!C     <comment>Handle on the simulation data</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSIM)
!!!      IF (IERR .NE. 0) GOTO 9901
!!!C     <comment>Time to load</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, TEMPS)
!!!      IF (IERR .NE. 0) GOTO 9901
!!!
!!!C---     VALIDE
!!!      HSIM = LM_ELNW_HELE2HSIM(HSIM)
!!!      IF (.NOT. HS_SIMD_HVALIDE(HSIM)) GOTO 9902
!!!      IF (TEMPS .LT. 0.0D0) GOTO 9903
!!!
!!!C---     EXECUTION
!!!      IF (ERR_GOOD())
!!!     &   IERR = DT_VNOD_CHARGE(HOBJ,
!!!     &                         LM_ELEM_REQHNUMC(HSIM),
!!!     &                         TEMPS,
!!!     &                         MODIF)
!!!
!!!C---     RETOURNE RIEN
!!!      IPRM = ' '
!!!
!!!      GOTO 9999
!!!C------------------------------------------------------------------------
!!!9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
!!!     &                       IPRM(1:SP_STRN_LEN(IPRM))
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9902  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_ATTENDU'
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)') 'ERR_TEMPS_INVALIDE',': ', TEMPS
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!
!!!9988  CONTINUE
!!!      CALL IC_DT_VNOD_AID()
!!!
!!!9999  CONTINUE
!!!      CALL LOG_DECIND()
!!!      IC_DT_VNOD_XEQMTH_LOAD = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C     La fonction privée IC_DT_VNOD_XEQMTH_SAVE exécute la méthode SAVE
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C
!!!C************************************************************************
!!!      FUNCTION IC_DT_VNOD_XEQMTH_SAVE(HOBJ, IPRM)
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER       HOBJ
!!!      CHARACTER*(*) IPRM
!!!
!!!      INCLUDE 'ioutil.fi'
!!!      INCLUDE 'dtvnod.fi'
!!!      INCLUDE 'lmelem.fi'
!!!      INCLUDE 'spstrn.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'log.fi'
!!!      INCLUDE 'dtvnod_ic.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER ISTAT
!!!      INTEGER HSIM
!!!      CHARACTER*(256) NOMFIC
!!!      CHARACTER*(256) NOMTMP
!!!      CHARACTER*(  8) MODE
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!      IERR = ERR_OK
!!!
!!!C---     EN-TETE DE COMMANDE
!!!      LOG_BUF = ' '
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!      WRITE (LOG_BUF, '(A)') 'MSG_ECRIS_VNO'
!!!      CALL LOG_ECRIS(LOG_BUF)
!!!      CALL LOG_INCIND()
!!!
!!!C---     LECTURE DES PARAM
!!!      HSIM = 0
!!!      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
!!!C     <comment>Handle on the simulation data</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HSIM)
!!!      IF (IERR .NE. 0) GOTO 9901
!!!C     <comment>Name of the saving file</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, NOMFIC)
!!!      IF (IERR .NE. 0) GOTO 9901
!!!C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 3, MODE)
!!!      IF (IERR .NE. 0) MODE = 'a'
!!!      CALL SP_STRN_TRM(NOMFIC)
!!!
!!!C---     VALIDE
!!!      IF (.NOT. LM_ELEM_HVALIDE(HSIM)) GOTO 9902
!!!      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9903
!!!      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
!!!      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904
!!!
!!!C---     IMPRESSION DU NOM DE FICHIER
!!!      IF (ERR_GOOD()) THEN
!!!         NOMTMP = NOMFIC
!!!         CALL SP_STRN_CLP(NOMTMP, 50)
!!!         WRITE (LOG_BUF,'(3A)') 'MSG_FICHIER_DONNEES#<35>#', '= ',
!!!     &                           NOMTMP(1:SP_STRN_LEN(NOMTMP))
!!!         CALL LOG_ECRIS(LOG_BUF)
!!!      ENDIF
!!!
!!!C---     EXECUTION
!!!      IF (ERR_GOOD())
!!!     &   IERR = DT_VNOD_ASGFICECR(HOBJ,
!!!     &                            NOMFIC(1:SP_STRN_LEN(NOMFIC)),
!!!     &                            ISTAT,
!!!     &                            0)   ! no HFVNO
!!!      IF (ERR_GOOD())
!!!     &   IERR = DT_VNOD_SAUVE(HOBJ,
!!!     &                        LM_ELEM_REQHNUMC(HSIM))
!!!
!!!C---     RETOURNE RIEN
!!!      IPRM = ' '
!!!
!!!      GOTO 9999
!!!C------------------------------------------------------------------------
!!!9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
!!!     &                       IPRM(1:SP_STRN_LEN(IPRM))
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9902  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_ATTENDU'
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9903  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
!!!     &                       MODE(1:2)
!!!      CALL ERR_ASG(ERR_ERR, ERR_BUF)
!!!      GOTO 9988
!!!
!!!9988  CONTINUE
!!!      CALL IC_DT_VNOD_AID()
!!!
!!!9999  CONTINUE
!!!      CALL LOG_DECIND()
!!!      IC_DT_VNOD_XEQMTH_SAVE = ERR_TYP()
!!!      RETURN
!!!      END
