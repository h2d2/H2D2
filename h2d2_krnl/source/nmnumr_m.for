C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   NUMeRotation
C Type:    Virtuel
C
C Class: NM_NUMR
C     INTEGER NM_NUMR_DTR
C     LOGICAL NM_NUMR_CTRLH
C     INTEGER NM_NUMR_CHARGE
C     INTEGER NM_NUMR_DSYNC
C     INTEGER NM_NUMR_ISYNC
C     INTEGER NM_NUMR_GENISYNC
C     INTEGER NM_NUMR_GENDSYNC
C     INTEGER NM_NUMR_REQNPRT
C     INTEGER NM_NUMR_REQNNL
C     INTEGER NM_NUMR_REQNNP
C     INTEGER NM_NUMR_REQNNT
C     LOGICAL NM_NUMR_ESTNOPP
C     INTEGER NM_NUMR_REQNEXT
C     INTEGER NM_NUMR_REQNINT
C
C************************************************************************

      MODULE NM_NUMR_M

      IMPLICIT NONE

      ! ---     Structure virtuelle
      TYPE, ABSTRACT :: NM_NUMR_T
      CONTAINS
         PROCEDURE(DTR_GEN  ),     DEFERRED :: DTR
         PROCEDURE(CHARGE_GEN  ),  DEFERRED :: CHARGE
         PROCEDURE(DSYNC_GEN   ),  DEFERRED :: DSYNC
         PROCEDURE(ISYNC_GEN   ),  DEFERRED :: ISYNC
         PROCEDURE(GENDSYNC_GEN),  DEFERRED :: GENDSYNC
         PROCEDURE(GENISYNC_GEN),  DEFERRED :: GENISYNC
         PROCEDURE(REQNPRT_GEN ),  DEFERRED :: REQNPRT
         PROCEDURE(REQNNL_GEN  ),  DEFERRED :: REQNNL
         PROCEDURE(REQNNP_GEN  ),  DEFERRED :: REQNNP
         PROCEDURE(REQNNT_GEN  ),  DEFERRED :: REQNNT
         PROCEDURE(ESTNOPP_GEN ),  DEFERRED :: ESTNOPP
         PROCEDURE(REQNEXT_GEN ),  DEFERRED :: REQNEXT
         PROCEDURE(REQNINT_GEN ),  DEFERRED :: REQNINT
      END TYPE NM_NUMR_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: NM_NUMR_DEL
      END INTERFACE DEL

      !========================================================================
      ! ---  Interface abstraite
      !========================================================================
      ABSTRACT INTERFACE
         INTEGER FUNCTION DTR_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION DTR_GEN

         INTEGER FUNCTION CHARGE_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(INOUT) :: SELF
         END FUNCTION CHARGE_GEN

         INTEGER FUNCTION DSYNC_GEN(SELF, NVAL, NNL, VVAL)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: NVAL
            INTEGER, INTENT(IN) :: NNL
            REAL*8,  INTENT(IN) :: VVAL(:,:)
         END FUNCTION DSYNC_GEN

         INTEGER FUNCTION ISYNC_GEN(SELF, NVAL, NNL, KVAL)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: NVAL
            INTEGER, INTENT(IN) :: NNL
            INTEGER, INTENT(IN) :: KVAL(:,:)
         END FUNCTION ISYNC_GEN

         INTEGER FUNCTION GENDSYNC_GEN(SELF, NVAL, NNL, HSNC)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: NVAL
            INTEGER, INTENT(IN) :: NNL
            INTEGER, INTENT(IN) :: HSNC
         END FUNCTION GENDSYNC_GEN

         INTEGER FUNCTION GENISYNC_GEN(SELF, NVAL, NNL, HSNC)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: NVAL
            INTEGER, INTENT(IN) :: NNL
            INTEGER, INTENT(IN) :: HSNC
         END FUNCTION GENISYNC_GEN

         INTEGER FUNCTION REQNPRT_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
         END FUNCTION REQNPRT_GEN

         INTEGER FUNCTION REQNNL_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
         END FUNCTION REQNNL_GEN

         INTEGER FUNCTION REQNNP_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
         END FUNCTION REQNNP_GEN

         INTEGER FUNCTION REQNNT_GEN(SELF)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
         END FUNCTION REQNNT_GEN

         LOGICAL FUNCTION ESTNOPP_GEN(SELF, INLOC)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: INLOC
         END FUNCTION ESTNOPP_GEN

         INTEGER FUNCTION REQNEXT_GEN(SELF, INLOC)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: INLOC
         END FUNCTION REQNEXT_GEN

         INTEGER FUNCTION REQNINT_GEN(SELF, INGLB)
            IMPORT :: NM_NUMR_T
            CLASS(NM_NUMR_T), INTENT(IN) :: SELF
            INTEGER, INTENT(IN) :: INGLB
         END FUNCTION REQNINT_GEN

      END INTERFACE

      CONTAINS

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     SELF est passé par pointeur pour permettre la précondition
C************************************************************************
      INTEGER FUNCTION NM_NUMR_DEL(SELF)

      CLASS(NM_NUMR_T), INTENT(INOUT), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(ASSOCIATED(SELF))
C------------------------------------------------------------------------

      NM_NUMR_DEL = SELF%DTR()
      RETURN
      END FUNCTION NM_NUMR_DEL

      END MODULE NM_NUMR_M

