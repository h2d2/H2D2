C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_COND_000
C     INTEGER DT_COND_999
C     INTEGER DT_COND_PKL
C     INTEGER DT_COND_UPK
C     INTEGER DT_COND_CTR
C     INTEGER DT_COND_DTR
C     INTEGER DT_COND_INI
C     INTEGER DT_COND_RST
C     INTEGER DT_COND_REQHBASE
C     LOGICAL DT_COND_HVALIDE
C     INTEGER DT_COND_PRN
C     INTEGER DT_COND_CHARGE
C     INTEGER DT_COND_REQNCLCND
C     INTEGER DT_COND_REQNCLCNV
C     INTEGER DT_COND_REQLCLCND
C     INTEGER DT_COND_REQLCLCNV
C     CHARACTER*256 DT_COND_REQNOMF
C   Private:
C     INTEGER DT_COND_RAZ
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>DT_COND_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_COND_NOBJMAX,
     &                   DT_COND_HBASE,
     &                   'Condition for Boundary Condition')

      DT_COND_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fc'

      INTEGER  IERR
      EXTERNAL DT_COND_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_COND_NOBJMAX,
     &                   DT_COND_HBASE,
     &                   DT_COND_DTR)

      DT_COND_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_COND_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_COND_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_COND_HFCND (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_COND_NCLCND(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_COND_NCLCNV(IOB))

C---     La table KCLCND
      IF (ERR_GOOD()) THEN
         N = 4 *        DT_COND_NCLCND(IOB)
         NX= 4 * MAX(1, DT_COND_NCLCND(IOB))
         L = DT_COND_LCLCND(IOB)
         IERR = IO_XML_WI_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table VCLCNV
      IF (ERR_GOOD()) THEN
         N =        DT_COND_NCLCNV(IOB)
         NX= MAX(1, DT_COND_NCLCNV(IOB))
         L = DT_COND_LCLCNV(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

      DT_COND_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_COND_HBASE

C---     Initialise l'objet
      IF (LNEW) IERR = DT_COND_RAZ(HOBJ)
      IERR = DT_COND_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_COND_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_COND_HFCND (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_COND_NCLCND(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_COND_NCLCNV(IOB))

C---     La table KCLCND
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. 4*MAX(1,DT_COND_NCLCND(IOB)))
      DT_COND_LCLCND(IOB) = L

C---     La table VNOD
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. MAX(1,DT_COND_NCLCNV(IOB)))
      DT_COND_LCLCNV(IOB) = L

      DT_COND_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   DT_COND_NOBJMAX,
     &                                   DT_COND_HBASE)
      IF (ERR_GOOD()) IERR = DT_COND_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_COND_HVALIDE(HOBJ))

      DT_COND_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_COND_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_COND_NOBJMAX,
     &                   DT_COND_HBASE)

      DT_COND_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode DT_COND_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IOB = HOBJ - DT_COND_HBASE

      DT_COND_TEMPS (IOB) = -1.0D0
      DT_COND_HFCND (IOB) = 0
      DT_COND_NCLCND(IOB) = 0
      DT_COND_LCLCND(IOB) = 0
      DT_COND_NCLCNV(IOB) = 0
      DT_COND_LCLCNV(IOB) = 0

      DT_COND_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_INI(HOBJ, HFLST, FICCND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HFLST
      CHARACTER*(*) FICCND

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFCND
      INTEGER NCLCND
      INTEGER NCLCNV
      INTEGER LCLCND
      INTEGER LCLCNV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles des paramètres
      IF (.NOT. DS_LIST_HVALIDE(HFLST)) THEN
         IF (SP_STRN_LEN(FICCND) .LE. 0) GOTO 9900
      ELSE
         IF (DS_LIST_REQDIM(HFLST) .LE. 0) GOTO 9901
      ENDIF

C---     Reset les données
      IERR = DT_COND_RST(HOBJ)

C---     Construis le fichier de données
      IF (ERR_GOOD()) IERR = FD_COND_CTR(HFCND)
      IF (ERR_GOOD()) IERR = FD_COND_INI(HFCND, HFLST, FICCND)

C---     Demande les dimensions
      NCLCND = 0
      NCLCNV = 0
      IF (ERR_GOOD()) NCLCND = FD_COND_REQNCLCND(HFCND)
      IF (ERR_GOOD()) NCLCNV = FD_COND_REQNCLCNV(HFCND)

C---     Alloue l'espace
      LCLCND = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(4*MAX(1,NCLCND), LCLCND)
      LCLCNV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(  MAX(1,NCLCNV), LCLCNV)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_COND_HBASE
         DT_COND_HFCND (IOB) = HFCND
         DT_COND_NCLCND(IOB) = NCLCND
         DT_COND_LCLCND(IOB) = LCLCND
         DT_COND_NCLCNV(IOB) = NCLCNV
         DT_COND_LCLCNV(IOB) = LCLCNV
         DT_COND_TEMPS (IOB) = -1.0D0
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LISTE_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_COND_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFCND
      INTEGER LCLCND
      INTEGER LCLCNV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB  = HOBJ - DT_COND_HBASE

C---     Récupère les données
      HFCND  = DT_COND_HFCND(IOB)
      LCLCND = DT_COND_LCLCND(IOB)
      LCLCNV = DT_COND_LCLCNV(IOB)

C---     Désalloue la mémoire
      IF (LCLCND .NE. 0) IERR = SO_ALLC_ALLINT(0, LCLCND)
      IF (LCLCNV .NE. 0) IERR = SO_ALLC_ALLRE8(0, LCLCNV)

C---     Détruis le fichier
      IF (FD_COND_HVALIDE(HFCND)) IERR = FD_COND_DTR(HFCND)

C---     Reset les paramètres
      IERR = DT_COND_RAZ(HOBJ)

      DT_COND_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_COND_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtcond.fi'
      INCLUDE 'dtcond.fc'
C------------------------------------------------------------------------

      DT_COND_REQHBASE = DT_COND_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_COND_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtcond.fc'
C------------------------------------------------------------------------

      DT_COND_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_COND_NOBJMAX,
     &                                  DT_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DT_COND_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C************************************************************************
      FUNCTION DT_COND_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtcond.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFCND
      INTEGER LVNOD
      INTEGER I, NFIC
      CHARACTER*256 NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - DT_COND_HBASE
      HFCND = DT_COND_HFCND(IOB)

C---     Zone de log
      LOG_ZNE = 'h2d2.data.condition'

C---     En-tête
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CONDITION_DE_LIMITE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     Nom des fichiers
      IF (FD_COND_HVALIDE(HFCND)) THEN
         NFIC = DT_COND_REQNFIC(HOBJ)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NOMBRE_FICHIERS#<35>#= ',
     &                            NFIC
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
         IF (NFIC .GT. 0) THEN
            NOM = DT_COND_REQNOMF(HOBJ, 1)
            CALL SP_STRN_CLP(NOM, 50)
            WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                               NOM(1:SP_STRN_LEN(NOM))
            CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
            DO I=2,NFIC
               NOM = DT_COND_REQNOMF(HOBJ, I)
               CALL SP_STRN_CLP(NOM, 50)
               WRITE (LOG_BUF,'(A,A)') '#<35>#   ',
     &                               NOM(1:SP_STRN_LEN(NOM))
               CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
            ENDDO
         ENDIF
      ENDIF
      
C---     Nombre de conditions
      WRITE (LOG_BUF,'(A,I6)') 'MSG_NBR_CONDITIONS#<35>#= ',
     &                          DT_COND_REQNCLCND(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)

C---     Les sections
      IF (FD_COND_HVALIDE(HFCND)) THEN
         WRITE (LOG_BUF,'(A,I12)')   'MSG_NBR_PAS_DE_TEMPS#<35>#= ',
     &                                FD_COND_REQNSCT(HFCND)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TMIN#<35>#= ',
     &                              FD_COND_REQTMIN(HFCND)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TMAX#<35>#= ',
     &                                FD_COND_REQTMAX(HFCND)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

C---     Temps actuel
      WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ',
     &                             DT_COND_TEMPS(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()

      DT_COND_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_COND_CHARGE
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_CHARGE (HOBJ, HNUMR, TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  TNEW

      INCLUDE 'dtcond.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtcond.fc'

      REAL*8  TOLD
      INTEGER IERR
      INTEGER IOB
      INTEGER HFCND
      INTEGER NCLCND
      INTEGER NCLCNV
      INTEGER LCLCND
      INTEGER LCLCNV
      LOGICAL MODIF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_COND_HBASE

C---     RECUPERE LES DONNEES
      HFCND  = DT_COND_HFCND (IOB)
      NCLCND = DT_COND_NCLCND(IOB)
      LCLCND = DT_COND_LCLCND(IOB)
      NCLCNV = DT_COND_NCLCNV(IOB)
      LCLCNV = DT_COND_LCLCNV(IOB)
      TOLD   = DT_COND_TEMPS (IOB)

C---     LIS LES DONNEES
      IERR = FD_COND_LIS(HFCND,
     &                   NCLCND,
     &                   KA(SO_ALLC_REQKIND(KA, LCLCND)),
     &                   NCLCNV,
     &                   VA(SO_ALLC_REQVIND(VA, LCLCNV)),
     &                   TOLD,
     &                   TNEW,
     &                   MODIF)

C---     CONSERVE LE TEMPS
      IF (ERR_GOOD()) THEN
         DT_COND_TEMPS(IOB) = TNEW
      ENDIF

      DT_COND_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQNCLCND(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQNCLCND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'dtcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_COND_REQNCLCND = DT_COND_NCLCND(HOBJ-DT_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQNCLCNV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQNCLCNV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'dtcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_COND_REQNCLCNV = DT_COND_NCLCNV(HOBJ-DT_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQLCLCND(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQLCLCND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'dtcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_COND_LCLCND(HOBJ-DT_COND_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_COND_REQLCLCND = DT_COND_LCLCND(HOBJ-DT_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQLCLCNV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQLCLCNV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'dtcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_COND_LCLCNV(HOBJ-DT_COND_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_COND_REQLCLCNV = DT_COND_LCLCNV(HOBJ-DT_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de fichier de lecture.
C
C Description:
C     La fonction DT_COND_REQNFIC retourne le nombre de fichiers de lecture
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQNFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQNFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtcond.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'dtcond.fc'

      INTEGER HFCND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFCND = DT_COND_HFCND(HOBJ-DT_COND_HBASE)
D     CALL ERR_ASR(FD_COND_HVALIDE(HFCND))

      DT_COND_REQNFIC = FD_COND_REQNFIC(HFCND)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du Fichier.
C
C Description:
C     La fonction DT_COND_REQNOMF retourne le nom de fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_COND_REQNOMF(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_COND_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'dtcond.fi'
      INCLUDE 'fdcond.fi'
      INCLUDE 'dtcond.fc'

      INTEGER HFCND
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFCND = DT_COND_HFCND(HOBJ-DT_COND_HBASE)
D     CALL ERR_ASR(FD_COND_HVALIDE(HFCND))

      DT_COND_REQNOMF = FD_COND_REQNOMF(HFCND, I)
      RETURN
      END
