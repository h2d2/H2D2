C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_GRID_000
C     INTEGER DT_GRID_999
C     INTEGER DT_GRID_PKL
C     INTEGER DT_GRID_UPK
C     INTEGER DT_GRID_CTR
C     INTEGER DT_GRID_DTR
C     INTEGER DT_GRID_INI
C     INTEGER DT_GRID_RST
C     INTEGER DT_GRID_REQHBASE
C     LOGICAL DT_GRID_HVALIDE
C     INTEGER DT_GRID_PRN
C     INTEGER DT_GRID_CHARGE
C     INTEGER DT_GRID_SAUVE
C     INTEGER DT_GRID_SPLIT
C     INTEGER DT_GRID_REQHCOOR
C     INTEGER DT_GRID_REQHELEV
C     INTEGER DT_GRID_REQHNUMC
C     INTEGER DT_GRID_REQHNUME
C     INTEGER DT_GRID_REQNDIM
C     INTEGER DT_GRID_REQNNL
C     INTEGER DT_GRID_REQNNP
C     INTEGER DT_GRID_REQNNT
C     INTEGER DT_GRID_REQNNEL
C     INTEGER DT_GRID_REQNELL
C     INTEGER DT_GRID_REQNELT
C   Private:
C     INTEGER DT_GRID_RAZ
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_GRID_NOBJMAX,
     &                   DT_GRID_HBASE,
     &                   'Finite Element Grid')

      DT_GRID_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER  IERR
      EXTERNAL DT_GRID_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_GRID_NOBJMAX,
     &                   DT_GRID_HBASE,
     &                   DT_GRID_DTR)

      DT_GRID_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER N, L
      INTEGER NELV
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_GRID_HBASE

C---     Récupère les paramètres
      NELV = DT_ELEM_REQNELL(DT_GRID_HELEV(IOB))

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_GRID_HNUMC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_GRID_HNUME(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_GRID_HCOOR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_GRID_HELEV(IOB))

      DT_GRID_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Pas d'assertion: On ne peut pas faire d'appels de fonctions
C     tant que tous les objets n'ont pas été reconstruits.
C************************************************************************
      FUNCTION DT_GRID_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, NX, N
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_GRID_HBASE

C---     Efface l'objet
      IF (ERR_GOOD()) IERR = DT_GRID_RAZ(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_GRID_HNUMC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_GRID_HNUME(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_GRID_HCOOR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_GRID_HELEV(IOB))

      DT_GRID_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   DT_GRID_NOBJMAX,
     &                                   DT_GRID_HBASE)
      IF (ERR_GOOD()) IERR = DT_GRID_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_GRID_HVALIDE(HOBJ))

      DT_GRID_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_GRID_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_GRID_NOBJMAX,
     &                   DT_GRID_HBASE)

      DT_GRID_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - DT_GRID_HBASE

      DT_GRID_HNUMC(IOB) = 0
      DT_GRID_HNUME(IOB) = 0
      DT_GRID_HCOOR(IOB) = 0
      DT_GRID_HELEV(IOB) = 0
      DT_GRID_DOOWN(IOB) = 0

      DT_GRID_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DT_GRID_INI initialise l'objet à l'aide des valeurs passées
C     en paramètre. L'objet est réinitialisé et l'état antérieur est effacé.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HCOOR       Handle sur les coordonnées
C     LCOOR       .TRUE. si la propriété de HCOOR est transférée à HOBJ
C     HELEV       Handle sur les connectivités
C     LELEV       .TRUE. si la propriété de HELEV est transférée à HOBJ
C     HNUMC       Handle sur la renumérotation des noeuds
C     LNUMC       .TRUE. si la propriété de HNUMC est transférée à HOBJ
C     HNUME       Handle sur la renumérotation des éléments
C     LNUME       .TRUE. si la propriété de HNUME est transférée à HOBJ
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_INI(HOBJ,
     &                     HCOOR, LCOOR,
     &                     HELEV, LELEV,
     &                     HNUMC, LNUMC,
     &                     HNUME, LNUME)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HCOOR
      LOGICAL LCOOR
      INTEGER HELEV
      LOGICAL LELEV
      INTEGER HNUMC
      LOGICAL LNUMC
      INTEGER HNUME
      LOGICAL LNUME

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER DOOWN
      INTEGER ITPES
      INTEGER ITPEV
      INTEGER H_NUMC, H_NUME
      INTEGER NNT, NELT
      LOGICAL L_NUMC, L_NUME
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C--      CONTROLES DES PARAMETRES
      IF (.NOT. DT_VNOD_HVALIDE(HCOOR)) GOTO 9900
      IF (.NOT. DT_ELEM_HVALIDE(HELEV)) GOTO 9901
      IF (HNUMC .NE. 0 .AND. .NOT. NM_NUMR_CTRLH(HNUMC)) GOTO 9902
!!!      IF (HNUMC .EQ. 0 .AND. MP_UTIL_ESTMPROC()) GOTO 9903
      IF (HNUME .NE. 0 .AND. .NOT. NM_NUMR_CTRLH(HNUME)) GOTO 9902

C---     RESET LES DONNEES
      IERR = DT_GRID_RST(HOBJ)

C---     AU BESOIN CREE LES RENUMEROTATIONS IDENTITE
      IF (ERR_GOOD()) THEN
         IF (HNUMC .EQ. 0) THEN
            NNT = DT_VNOD_REQNNT(HCOOR)
            IF (ERR_GOOD()) IERR = NM_IDEN_CTR(H_NUMC)
            IF (ERR_GOOD()) IERR = NM_IDEN_INI(H_NUMC, NNT)
            L_NUMC = .TRUE.
         ELSE
            H_NUMC = HNUMC
            L_NUMC = LNUMC
         ENDIF
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (HNUME .EQ. 0) THEN
            NELT = DT_ELEM_REQNELT(HELEV)
            IF (ERR_GOOD()) IERR = NM_IDEN_CTR(H_NUME)
            IF (ERR_GOOD()) IERR = NM_IDEN_INI(H_NUME, NELT)
            L_NUME = .TRUE.
         ELSE
            H_NUME = HNUME
            L_NUME = LNUME
         ENDIF
      ENDIF

C---     Bit-Set de possession des données
      DOOWN = 0
      IF (LCOOR)  DOOWN = IBSET(DOOWN, DT_GRID_ID_HCOOR)
      IF (LELEV)  DOOWN = IBSET(DOOWN, DT_GRID_ID_HELEV)
      IF (L_NUMC) DOOWN = IBSET(DOOWN, DT_GRID_ID_HNUMC)
      IF (L_NUME) DOOWN = IBSET(DOOWN, DT_GRID_ID_HNUME)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - DT_GRID_HBASE
         DT_GRID_HNUMC(IOB) = H_NUMC
         DT_GRID_HNUME(IOB) = H_NUME
         DT_GRID_HCOOR(IOB) = HCOOR
         DT_GRID_HELEV(IOB) = HELEV
         DT_GRID_DOOWN(IOB) = DOOWN
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_COORD_INVALIDE',': ',HCOOR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_ELEM_INVALIDE',': ',HELEV
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_RENUM_INVALIDE',': ',HNUMC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)')'ERR_RENUM_NULL_INVALIDE_EN_MPROC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_GRID_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER DOOWN
      INTEGER HDATA
      INTEGER LDJV, LDJS
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_GRID_HBASE
      DOOWN = DT_GRID_DOOWN(IOB)

C---     DETRUIS LES COORDONNEES
      IF (BTEST(DOOWN, DT_GRID_ID_HCOOR)) THEN
         HDATA = DT_GRID_HCOOR(IOB)
         IF (DT_VNOD_HVALIDE(HDATA)) THEN
            IERR = DT_VNOD_DTR(HDATA)
         ENDIF
      ENDIF

C---     DETRUIS LES CONNECTIVITÉS
      IF (BTEST(DOOWN, DT_GRID_ID_HELEV)) THEN
         HDATA = DT_GRID_HELEV(IOB)
         IF (DT_ELEM_HVALIDE(HDATA)) THEN
            IERR = DT_ELEM_DTR(HDATA)
         ENDIF
      ENDIF

C---     DETRUIS LA NUMÉROTATION DES NOEUDS
      IF (BTEST(DOOWN, DT_GRID_ID_HNUMC)) THEN
         HDATA = DT_GRID_HNUMC(IOB)
         IF (NM_NUMR_CTRLH(HDATA)) THEN
            IERR = NM_NUMR_DTR(HDATA)
         ENDIF
      ENDIF

C---     DETRUIS LA NUMÉROTATION DES ELEMENTS
      IF (BTEST(DOOWN, DT_GRID_ID_HNUME)) THEN
         HDATA = DT_GRID_HNUME(IOB)
         IF (NM_NUMR_CTRLH(HDATA)) THEN
            IERR = NM_NUMR_DTR(HDATA)
         ENDIF
      ENDIF

C---     RESET LES DONNEES
      IERR = DT_GRID_RAZ(HOBJ)

      DT_GRID_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_GRID_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtgrid.fc'
C------------------------------------------------------------------------

      DT_GRID_REQHBASE = DT_GRID_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_GRID_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtgrid.fc'
C------------------------------------------------------------------------

      DT_GRID_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_GRID_NOBJMAX,
     &                                  DT_GRID_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DT_GRID_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER       IERR
      CHARACTER*256 NOM
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.grid'

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_MAILLAGE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(NOM, DT_GRID_REQHCOOR(HOBJ))
      WRITE (LOG_BUF,'(A,A)') 'MSG_COORDONNEES#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      IERR = OB_OBJC_REQNOMCMPL(NOM, DT_GRID_REQHELEV(HOBJ))
      WRITE (LOG_BUF,'(A,A)') 'MSG_ELEMENTS#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS#<35>#= ',
     &                                 DT_GRID_REQNNT(HOBJ)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_DIM#<35>#= ',
     &                                 DT_GRID_REQNDIM(HOBJ)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM#<35>#= ',
     &                                 DT_GRID_REQNELT(HOBJ)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_OF_NOD_PER_ELEM#<35>#= ',
     &                                 DT_GRID_REQNNEL(HOBJ)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

 !     WRITE (LOG_BUF,'(A,I12)') 'MSG_TYPE_ELEM#<35>#= ',
 !    &                                 DT_ELEM_REQITPE(HELEM)
 !     CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      DT_GRID_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION DT_GRID_CHARGE(HOBJ, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  TEMPS

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITYPE
      INTEGER HCOOR
      INTEGER HELEV
      INTEGER HNUMC, HNUME
      INTEGER NELV,  NELS
      LOGICAL MDFCOR, MDFELV
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.grid')

C---     RECUPERE LES HANDLES
      IOB   = HOBJ - DT_GRID_HBASE
      HNUMC = DT_GRID_HNUMC(IOB)
      HNUME = DT_GRID_HNUME(IOB)
      HCOOR = DT_GRID_HCOOR(IOB)
      HELEV = DT_GRID_REQHELEV(HOBJ)
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUME))

C---     CHARGE LES DONNEES
      MDFCOR = .FALSE.
      MDFELV = .FALSE.
      IF (ERR_GOOD())
     &   IERR = DT_VNOD_CHARGE(HCOOR, HNUMC, TEMPS, MDFCOR)
      IF (ERR_GOOD())
     &   IERR = DT_ELEM_CHARGE(HELEV, HNUMC, HNUME, TEMPS, MDFELV)

C---     STOPPE LE CHRONO
9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid')

      DT_GRID_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_SAUVE
C
C Description:
C     La fonction <code>DT_GRID_SAUVE(...)<code> écris les données de l'objet
C     dans le fichier maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_SAUVE(HOBJ, FICCOR, FICELE, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FICCOR
      CHARACTER*(*) FICELE
      INTEGER       ISTAT

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HNUMC, HNUME, HCOOR, HELEV
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - DT_GRID_HBASE
      HNUMC = DT_GRID_HNUMC(IOB)
      HNUME = DT_GRID_HNUME(IOB)
      HCOOR = DT_GRID_HCOOR(IOB)
      HELEV = DT_GRID_REQHELEV(HOBJ)

C---     Sauve les coordonnées
      IF (ERR_GOOD()) IERR = DT_VNOD_ASGFICECR(HCOOR,
     &                                         FICCOR,
     &                                         ISTAT,
     &                                         0)  ! pas de HFVNO
      IF (ERR_GOOD()) IERR = DT_VNOD_SAUVE    (HCOOR,
     &                                         HNUMC)

C---     Sauve les connectivités
      IF (ERR_GOOD()) IERR = DT_ELEM_SAUVE(HELEV,
     &                                     HNUMC,
     &                                     HNUME,
     &                                     FICELE,
     &                                     ISTAT)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FICHIER_NON_DEFINI'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_GRID_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_SPLIT
C
C Description:
C     La méthode DT_GRID_SPLIT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_SPLIT(HOBJ, HSPLT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_SPLIT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSPLT

      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'dtgrid.fc'

      INTEGER IERR
      INTEGER HCOOR, HELEV
      INTEGER HNUMC, HNUME
      INTEGER HESP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid')

C---     Récupère les attributs
      !IOB   = HOBJ - DT_GRID_HBASE
      HCOOR = DT_GRID_REQHCOOR(HOBJ)
      HELEV = DT_GRID_REQHELEV(HOBJ)
      HNUMC = DT_GRID_REQHNUMC(HOBJ)
      HNUME = DT_GRID_REQHNUME(HOBJ)
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUMC))
D     CALL ERR_ASR(NM_NUMR_CTRLH(HNUME))

C---     Initialise les éléments comme split
      IERR = DT_ELEM_CTR   (HESP)
      IERR = DT_ELEM_INISPL(HESP, HELEV)

C---     Construis le maillage
      IERR = DT_GRID_CTR(HSPLT)
      IERR = DT_GRID_INI(HSPLT,
     &                   HCOOR, .FALSE.,   ! Don't trf ownership
     &                   HESP,  .TRUE.,    ! Do trf ownership
     &                   HNUMC, .FALSE.,   ! Don't trf ownership
     &                   HNUME, .FALSE.)   ! Don't trf ownership

C---     Stoppe le chrono
      CALL TR_CHRN_STOP('h2d2.grid')

      DT_GRID_SPLIT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQHCOOR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQHCOOR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQHCOOR = DT_GRID_HCOOR(HOBJ-DT_GRID_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQHELEV(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQHELEV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQHELEV = DT_GRID_HELEV(HOBJ-DT_GRID_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la numérotation des noeuds.
C
C Description:
C     La fonction DT_GRID_REQHNUMC retourne le handle sur
C     la renumérotation des noeuds associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQHNUMC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQHNUMC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_GRID_HNUMC(HOBJ-DT_GRID_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_GRID_REQHNUMC = DT_GRID_HNUMC(HOBJ-DT_GRID_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la renumérotation des éléments.
C
C Description:
C     La fonction DT_GRID_REQHNUME retourne le handle sur
C     la renumérotation des éléments associée à l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQHNUME(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQHNUME
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_GRID_HNUME(HOBJ-DT_GRID_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_GRID_REQHNUME = DT_GRID_HNUME(HOBJ-DT_GRID_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNDIM
C
C Description:
C     La fonction DT_GRID_REQNDIM retourne le nombre de dimension.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNDIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNDIM = DT_VNOD_REQNVNO(DT_GRID_REQHCOOR(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNNL
C
C Description:
C     La fonction DT_GRID_REQNNL retourne le nombre de noeuds local.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNNL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNNL = NM_NUMR_REQNNL(DT_GRID_REQHNUMC(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNNP
C
C Description:
C     La fonction DT_GRID_REQNNP retourne le nombre de noeuds privés.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNNP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNNP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNNP = NM_NUMR_REQNNP(DT_GRID_REQHNUMC(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNNT
C
C Description:
C     La fonction DT_GRID_REQNNT retourne le nombre de noeuds total (global).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNNT = DT_VNOD_REQNNT(DT_GRID_REQHCOOR(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNNEL
C
C Description:
C     La fonction DT_GRID_REQNNEL retourne le nombre de noeuds par élément.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNNEL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNNEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNNEL = DT_ELEM_REQNNEL(DT_GRID_REQHELEV(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: DT_GRID_REQNELL
C
C Description:
C     La fonction DT_GRID_REQNELL retourne le nombre local d'éléments
C     de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNELL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNELL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNELL = DT_ELEM_REQNELL(DT_GRID_REQHELEV(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DT_GRID_REQNELT retourne le nombre total (global) d'éléments
C     de volume.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_GRID_REQNELT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_GRID_REQNELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtgrid.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'dtgrid.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_GRID_REQNELT = DT_ELEM_REQNELT(DT_GRID_REQHELEV(HOBJ))
      RETURN
      END
