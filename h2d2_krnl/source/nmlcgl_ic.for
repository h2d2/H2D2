C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Interface Commandes: NUMeRotation
C Objet:   LoCal-GLobal
C Type:    Virtuel
C************************************************************************

C************************************************************************
C Sommaire: Fonction vide qui ne devrait jamais être appelée.
C
C Description:
C     La fonction IC_NM_LCGL_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un objet virtuel via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_LCGL_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_LCGL_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'nmlcgl_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_NM_LCGL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_LCGL_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type NM_LCGL.
C     La fonction résout directement les appels aux méthodes virtuelles.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_LCGL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_LCGL_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'nmlcgl_ic.fi'
!!      INCLUDE 'icsolv.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
C------------------------------------------------------------------------
      CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

!!C     <comment>Reads and loads a previously computed renumbering</comment>
      IF (IMTH .EQ. 'load') THEN
         CALL LOG_TODO('IC_NM_LCGL_XEQMTH: ajoute les méthodes virt.')
!!C        <include>IC_SOLV_XEQ@icsolv.for</include>
!!         IERR = IC_SOLV_XEQ(HOBJ, IPRM)

C     <comment>Class destructor</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = NM_LCGL_DTR(HOBJ)

C     <comment>Print statement</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_NM_LCGL_AID()

C     <comment>Help statement</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_NM_LCGL_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NM_LCGL_AID()


9999  CONTINUE
      IC_NM_LCGL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_LCGL_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_LCGL_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmlcgl_ic.fi'
C-------------------------------------------------------------------------

      IC_NM_LCGL_REQCLS = '#__dummy_placeholder__#__NM_LCGL__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_LCGL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NM_LCGL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmlcgl_ic.fi'
      INCLUDE 'nmlcgl.fi'
C-------------------------------------------------------------------------

      IC_NM_LCGL_REQHDL = NM_LCGL_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_NM_LCGL_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NM_LCGL_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('nmlcgl_ic.hlp')

      RETURN
      END
