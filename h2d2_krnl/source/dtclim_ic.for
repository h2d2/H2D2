C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_CLIM_XEQCTR
C     INTEGER IC_DT_CLIM_XEQMTH
C     CHARACTER*(32) IC_DT_CLIM_REQCLS
C     INTEGER IC_DT_CLIM_REQHDL
C   Private:
C     SUBROUTINE IC_DT_CLIM_PRN
C     SUBROUTINE IC_DT_CLIM_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_CLIM_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_CLIM_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtclim_ic.fi'
      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtclim_ic.fc'

      INTEGER NCNDMAX
      PARAMETER (NCNDMAX = 20)

      INTEGER I
      INTEGER IERR
      INTEGER HOBJ
      INTEGER HLMT
      INTEGER NHCND
      INTEGER KHCND(NCNDMAX)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_CLIM_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the boundaries</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HLMT)
      IF (IERR .NE. 0) GOTO 9901
      I = 1
100   CONTINUE
         IF (I .EQ. NCNDMAX) GOTO 9902
         I = I + 1
C        <comment>Non-empty list of the handles on conditions, comma separated</comment>
         IERR = SP_STRN_TKI(IPRM, ',', I, KHCND(I-1))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9901
      GOTO 100
199   CONTINUE
      NHCND = I-2

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_CLIM_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_CLIM_INI(HOBJ, HLMT, NHCND, KHCND)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) IERR = IC_DT_CLIM_PRN(HOBJ)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the boundary conditions</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C The constructor <b>boundary_condition</b> constructs an object, with the given
C arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_CLIM_AID()

9999  CONTINUE
      IC_DT_CLIM_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_CLIM_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_CLIM_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtclim.fc'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtclim_ic.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NHCND
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_CONDITIONS_LIMITES'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DE L'OBJET
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      IOB = HOBJ - DT_CLIM_HBASE
      NHCND = DT_CLIM_NHCND(IOB)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_CONDITIONS#<35>#', '= ',
     &                              NHCND
      CALL LOG_ECRIS(LOG_BUF)

C---      IMPRESSION DES PARAMETRES DU BLOC
C      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_DDL#<35>#', '= ',
C     &                              GR_GRID_REQNDLT(HGRD)
C         CALL LOG_ECRIS(LOG_BUF)
C      ENDIF


      CALL LOG_DECIND()

      IC_DT_CLIM_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_CLIM_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_CLIM_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtclim_ic.fi'
      INCLUDE 'dtclim.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_CLIM_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_CLIM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = DT_CLIM_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test DT_CLIM_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_CLIM_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_CLIM_AID()

9999  CONTINUE
      IC_DT_CLIM_XEQMTH = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_CLIM_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_CLIM_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtclim_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C The class <b>boundary_condition</b> represents the boundary conditions and it
C links conditions to boundaries. The boundary conditions are conditions
C that must be satisfied by the degrees of freedom on the boundary of the
C simulation domain.
C</comment>
      IC_DT_CLIM_REQCLS = 'boundary_condition'
      RETURN
      END
C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_CLIM_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_CLIM_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtclim_ic.fi'
      INCLUDE 'dtclim.fi'
C-------------------------------------------------------------------------

      IC_DT_CLIM_REQHDL = DT_CLIM_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_CLIM_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_CLIM_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtclim_ic.hlp')

      RETURN
      END

