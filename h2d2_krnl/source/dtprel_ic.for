C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_PREL_CMD
C     CHARACTER*(32) IC_DT_PREL_REQCMD
C   Private:
C     INTEGER IC_DT_PREL_PRN
C     SUBROUTINE IC_DT_PREL_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PREL_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PREL_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtprel_ic.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprel_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER I
      INTEGER NHVEL
      INTEGER NVELMAX
      PARAMETER (NVELMAX = 20)
      INTEGER KVELE(NVELMAX)
      INTEGER IDUM(1)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_PREL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 0
100   CONTINUE
         IF (I .EQ. NVELMAX) GOTO 9901
         I = I + 1
C        <comment>List of the handles on elemental values, comma separated</comment>
         IERR = SP_STRN_TKI(IPRM, ',', I, KVELE(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9902
      GOTO 100
199   CONTINUE
      NHVEL = I-1


C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_PRNO_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_PRNO_INI(HOBJ)
      DO I=1, NHVEL
         IF (ERR_GOOD()) IERR = DT_PRNO_ADDVNO(HOBJ, KVELE(I), -1, IDUM)
      ENDDO

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_DT_PREL_PRN(HOBJ)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the elemental properties</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PREL_AID()

9999  CONTINUE
      IC_DT_PREL_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_PREL_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PREL_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
      INCLUDE 'dtprel_ic.fc'

      INTEGER IOB
      INTEGER NHVNO
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_PROPRIETES_ELEMENTAIRES'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IOB = HOBJ - DT_PRNO_HBASE
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_ELEMENTS#<35>#', '= ',
     &                            DT_PRNO_REQNNL(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_PROP_ELEM#<35>#','= ',
     &                            DT_PRNO_REQNPRN(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)

!      NHVNO = DT_PRNO_NHVNO(IOB)
!      WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_OBJ_PROP_ELEM#<35>#',
!     &                           '= ', NHVNO
!      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      IC_DT_PREL_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PREL_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PREL_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprel_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>prel</b> constructs the elemental properties as an
C  agglomeration of elemental values.
C</comment>
      IC_DT_PREL_REQCMD = 'prel'
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_PREL_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_PREL_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtprel_ic.hlp')

      RETURN
      END

