C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numerotation
C Objet:   Algo de partitionnement 'a mano'
C Type:    Concret
C
C Sousroutines:
C      INTEGER NM_AMNO_000
C      INTEGER NM_AMNO_999
C      INTEGER NM_AMNO_CTR
C      INTEGER NM_AMNO_DTR
C      INTEGER NM_AMNO_INI
C      INTEGER NM_AMNO_RST
C      INTEGER NM_AMNO_REQHBASE
C      LOGICAL NM_AMNO_HVALIDE
C      INTEGER NM_AMNO_ASG1ROW
C      INTEGER NM_AMNO_ASGROW
C      INTEGER NM_AMNO_GENNUM
C      INTEGER NM_AMNO_SAUVE
C      INTEGER NM_AMNO_DUMMY
C
C Note:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_AMNO_NOBJMAX,
     &                   NM_AMNO_HBASE,
     &                   'Renumerotation à mitaine')

      NM_AMNO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmamno.fc'

      INTEGER  IERR
      EXTERNAL NM_AMNO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_AMNO_NOBJMAX,
     &                   NM_AMNO_HBASE,
     &                   NM_AMNO_DTR)

      NM_AMNO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_AMNO_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_AMNO_NOBJMAX,
     &                   NM_AMNO_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_AMNO_HVALIDE(HOBJ))
         IOB = HOBJ - NM_AMNO_HBASE

         NM_AMNO_HPRNT(IOB) = 0
         NM_AMNO_HFDUM(IOB) = 0
         NM_AMNO_LDSTR(IOB) = 0
         NM_AMNO_LDSTR(IOB) = 0
      ENDIF

      NM_AMNO_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_AMNO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmamno.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_AMNO_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_AMNO_NOBJMAX,
     &                   NM_AMNO_HBASE)

      NM_AMNO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_AMNO_INI(HOBJ, NNT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NNT

      INCLUDE 'nmamno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFDUM
      INTEGER LDSTR
      EXTERNAL NM_AMNO_DUMMY
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_AMNO_RST(HOBJ)

C---     CONSTRUIS LA FONCTION DUMMY DEMANDEE PAR LE PARENT
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFDUM)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFDUM, NM_AMNO_DUMMY)

C---     CONSTRUIS LE PARENT
      IF (ERR_GOOD()) IERR = NM_NBSE_CTR(HPRNT)
      IF (ERR_GOOD()) IERR = NM_NBSE_INI(HPRNT, HFDUM, HFDUM)

C---     ALLOUE LA TABLE DE DISTRIBUTION
      LDSTR = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(2*NNT, LDSTR)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - NM_AMNO_HBASE
         NM_AMNO_HPRNT(IOB) = HPRNT
         NM_AMNO_HFDUM(IOB) = HFDUM
         NM_AMNO_LDSTR(IOB) = LDSTR
         NM_AMNO_NNT  (IOB) = NNT
      ENDIF

      NM_AMNO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_AMNO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmamno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFDUM
      INTEGER LDSTR
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_AMNO_HBASE
      HPRNT = NM_AMNO_HPRNT(IOB)
      HFDUM = NM_AMNO_HFDUM(IOB)
      LDSTR = NM_AMNO_LDSTR(IOB)

C---     DETRUIS LES ATTRIBUTS
      IF (SO_FUNC_HVALIDE(HFDUM)) IERR = SO_FUNC_DTR(HFDUM)
      IF (NM_NBSE_HVALIDE(HPRNT)) IERR = NM_NBSE_DTR(HPRNT)
      IF (LDSTR .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSTR)

C---     RESET
      NM_AMNO_HPRNT(IOB) = 0
      NM_AMNO_HFDUM(IOB) = 0
      NM_AMNO_LDSTR(IOB) = 0
      NM_AMNO_NNT  (IOB) = 0

      NM_AMNO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_AMNO_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmamno.fi'
      INCLUDE 'nmamno.fc'
C------------------------------------------------------------------------

      NM_AMNO_REQHBASE = NM_AMNO_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_AMNO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmamno.fc'
C------------------------------------------------------------------------

      NM_AMNO_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_AMNO_NOBJMAX,
     &                                  NM_AMNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_AMNO_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_TVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmamno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmamno.fc'
C------------------------------------------------------------------------

      NM_AMNO_TVALIDE = OB_OBJC_TVALIDE(HOBJ,
     &                                  NM_AMNO_NOBJMAX,
     &                                  NM_AMNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée NM_AMNO_ASG1ROW assigne à la table de distribution
C     les données d'une ligne. Elle permet de résoudre les appels à la
C     table.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_ASG1ROW(KDSTR, NINT, NEXT, IPP)

      IMPLICIT NONE

      INTEGER KDSTR(2, *)
      INTEGER NINT
      INTEGER NEXT
      INTEGER IPP

      INCLUDE 'nmamno.fc'

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      KDSTR(1,NINT) = NEXT
      KDSTR(2,NINT) = IPP

      NM_AMNO_ASG1ROW = 0
      RETURN
      END

C************************************************************************
C Sommaire: Assigne une ligne à la table de distribution.
C
C Description:
C     La fonction NM_AMNO_ADDROW ajoute une ligne à la table de
C     redistribution. IPP < 0 indique un noeuds qui n'appartient
C     pas au process.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NINT        Numéro interne
C     NEXT        Numéro externe
C     IPP         Process propriétaire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_ASGROW(HOBJ, NINT, NEXT, IPP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_ASGROW
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NINT
      INTEGER NEXT
      INTEGER IPP

      INCLUDE 'nmamno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
      INTEGER LDSTR, NNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NINT .GT. 0)
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_AMNO_HBASE
      LDSTR = NM_AMNO_LDSTR(IOB)
      NNT   = NM_AMNO_NNT  (IOB)
D     CALL ERR_ASR(LDSTR .NE. 0)
D     CALL ERR_ASR(NINT  .LE. NNT)

C---     Assigne les valeurs
      IERR = NM_AMNO_ASG1ROW(KA(SO_ALLC_REQKIND(KA,LDSTR)),
     &                       NINT, NEXT, IPP)

      NM_AMNO_ASGROW = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis une renumérotation.
C
C Description:
C     La fonction NM_AMNO_GENNUM retourne un renumérotation globale
C     nouvellement créée à partir des données de l'objet. Il est de
C     la responsabilité de l'utilisateur de disposer de HNUM.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HNUM        Handle sur la renumérotation
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_GENNUM (HOBJ, HNUM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_GENNUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmamno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_SIZE, I_PROC, I_ERROR
      INTEGER HPRNT
      INTEGER LDSTR, NNT
      INTEGER NPRTG, NPRTL, IPRTL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - NM_AMNO_HBASE
      HPRNT = NM_AMNO_HPRNT(IOB)
      LDSTR = NM_AMNO_LDSTR(IOB)
      NNT   = NM_AMNO_NNT  (IOB)

C---     Paramètres MPI
      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     Assigne la table de redistribution au parent
      NPRTG = I_SIZE
      NPRTL = 1
      IPRTL = I_PROC
      IF (ERR_GOOD()) IERR = NM_NBSE_ASGDSTR(HPRNT,
     &                                       NPRTG, NPRTL, IPRTL,
     &                                       NNT, LDSTR)
      IF (ERR_GOOD()) NM_AMNO_LDSTR(IOB) = 0

C---     Appel le parent
      IF (ERR_GOOD()) IERR  = NM_NBSE_GENNUM(HPRNT, HNUM)

      NM_AMNO_GENNUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris la table de redistribution.
C
C Description:
C     La fonction NM_AMNO_SAUVE écris la table de redistribution dans le
C     fichier NOMFIC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_SAUVE (HOBJ, NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_AMNO_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'nmamno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmamno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_AMNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_AMNO_HBASE
      HPRNT = NM_AMNO_HPRNT(IOB)

C---     APPEL LE PARENT
      IERR  = NM_NBSE_SAUVE(HPRNT, NOMFIC)

      NM_AMNO_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote un maillage.
C
C Description:
C     La fonction privée NM_AMNO_ADD1ROW ajoute à la table de distribution
C     les données d'une ligne. Elle permet de résoudre les appels à la
C     table
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_AMNO_DUMMY()

      IMPLICIT NONE

      INCLUDE 'err.fi'
      INCLUDE 'nmamno.fc'
C-----------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'NM_AMNO_DUMMY shall never be called')
      CALL ERR_ASR(.FALSE.)

      NM_AMNO_DUMMY = ERR_TYP()
      RETURN
      END

