C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_PRGL_XEQCTR
C     CHARACTER*(32) IC_DT_PRGL_REQCLS
C     INTEGER IC_DT_PRGL_REQHDL
C     INTEGER IC_DT_PRGL_XEQMTH
C   Private:
C     INTEGER IC_DT_PRGL_XEQCTRFIC
C     INTEGER IC_DT_PRGL_XEQCTRVAL
C     SUBROUTINE IC_DT_PRGL_AID
C     INTEGER IC_DT_PRGL_REQMOD
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRGL_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRGL_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtprgl_ic.fi'
      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl_ic.fc'

      INTEGER IERR
      INTEGER IMOD
      INTEGER HOBJ
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_PRGL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Extrait le mode
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IERR = IC_DT_PRGL_REQMOD(IPRM, IMOD)

C---     Appel le constructeur spécialisé
      IF (IMOD .EQ. IC_DT_PRGL_MOD_FIC) THEN
C        <include>IC_DT_PRGL_XEQCTRFIC@dtprgl_ic.for</include>
         IERR = IC_DT_PRGL_XEQCTRFIC(IPRM, HOBJ)
      ELSEIF (IMOD .EQ. IC_DT_PRGL_MOD_VAL) THEN
C        <include>IC_DT_PRGL_XEQCTRVAL@dtprgl_ic.for</include>
         IERR = IC_DT_PRGL_XEQCTRVAL(IPRM, HOBJ)
      ENDIF

C---     Imprime l'objet
      IF (ERR_GOOD()) IERR = DT_PRGL_PRN(HOBJ)

      IF (ERR_BAD()) GOTO 9988
      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRGL_AID()

9999  CONTINUE
      IC_DT_PRGL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_PRGL_XEQCTRFIC(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl_ic.fc'

      INTEGER IERR, IRET
      INTEGER ISTAT
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Lecture fichier & mode
C     <comment>Name of the global properties file</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IRET .NE. 0) GOTO 9900
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IRET .NE. 0) MODE = 'r'

C---     Valide
      CALL SP_STRN_TRM(NOMFIC)
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9901
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9902

C---     Construit un objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_PRGL_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_PRGL_INIFIC(HOBJ, NOMFIC, ISTAT)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on global properties</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C     <comment>
C     The constructor <b>prgl</b> constructs an object, with the given arguments, and
C     returns a handle on this object.
C     </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRGL_AID()

9999  CONTINUE
      IC_DT_PRGL_XEQCTRFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     HOBJ est retourné 2 fois, en argument et dans IPRM.
C     Il faut IPRM pour que la commande soit extraite, et le retour de HOBJ
C     est pour éviter à l'appelant de l'extraire.
C************************************************************************
      FUNCTION IC_DT_PRGL_XEQCTRVAL(IPRM, HOBJ)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER HOBJ

      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl_ic.fc'

      INTEGER, PARAMETER :: NPRGLMAX = 50

      REAL*8  VPRGL(NPRGLMAX)
      INTEGER IERR, IRET
      INTEGER I
      INTEGER NTOK, NPRGL
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle des dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      IF (NTOK .LT. 1) GOTO 9901
      IF (NTOK .GT. NPRGLMAX) GOTO 9901

C---     Lecture des propriétés
      DO I=1,NTOK
C        <comment>List of global properties, comma separated</comment>
         IRET = SP_STRN_TKR(IPRM, ',', I, VPRGL(I))
         IF (IRET .NE.  0) GOTO 9902
      ENDDO
      NPRGL = NTOK

C---     Construit un objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_PRGL_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_PRGL_INIVAL(HOBJ, NPRGL, VPRGL)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on global properties</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C     <comment>
C     The constructor <b>prgl</b> constructs an object, with the given arguments, and
C     returns a handle on this object.
C     </comment>

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)') 'ERR_NBR_VALEURS_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,2(I2,A))') 'MSG_DOMAINE_VALIDE',
     &               '[',1, ',', NPRGLMAX, ']'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(2A, I6)') 'ERR_LECTURE_PARAMETRE',': ', I
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRGL_AID()

9999  CONTINUE
      IC_DT_PRGL_XEQCTRVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:    Nom de la classe
C
C Description:
C     La fonction <code>IC_DT_PRGL_REQCLS()</code> retourne le nom de la
C     classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRGL_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRGL_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprgl_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>prgl</b> represents the global properties, properties that
C  are invariant in space but can vary in time.
C</comment>
      IC_DT_PRGL_REQCLS = 'prgl'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRGL_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRGL_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprgl_ic.fi'
      INCLUDE 'dtprgl.fi'
C-------------------------------------------------------------------------

      IC_DT_PRGL_REQHDL = DT_PRGL_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_PRGL_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_PRGL_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtprgl_ic.hlp')

      RETURN
      END



C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRGL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_PRGL_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtprgl_ic.fi'
      INCLUDE 'dtprgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl_ic.fc'

      INTEGER IERR
      INTEGER IVAL
      REAL*8  RVAL
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Dispatch les méthodes, propriétés et opérateurs
C     <comment>Get a property with its index (rvalue), as in g=h_prgl[2]</comment>
      IF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_ASR(DT_PRGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index of the property</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) IERR = DT_PRGL_REQPRGL(HOBJ, IVAL, RVAL)
         IF (ERR_GOOD()) WRITE(IPRM, '(2A, 1PE25.17E3)') 'R', ',', RVAL

C     <comment>Set a property with its index (lvalue), as in h_prgl[2]=1.</comment>
      ELSEIF (IMTH .EQ. '##opb_[]_set##') THEN
D        CALL ERR_ASR(DT_PRGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index of the property</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
C        <comment>Value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) IERR = DT_PRGL_ASGPRGL(HOBJ, IVAL, RVAL)

C<comment>
C     The method <b>del</b> deletes the object. The handle shall not
C     be used anymore to reference the object.
C</comment>

      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_PRGL_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_PRGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_PRGL_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_PRGL_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_PRGL_AID()

9999  CONTINUE
      IC_DT_PRGL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_PRGL_REQMOD(IPRM, IMOD)

      IMPLICIT NONE

      CHARACTER*(*) IPRM
      INTEGER       IMOD

      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtprgl_ic.fc'

      INTEGER IERR, IRET
      INTEGER NTOK
      REAL*8  VDUM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(SP_STRN_LEN(IPRM) .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IMOD = IC_DT_PRGL_MOD_INDEFINI

C---     Contrôle les dimensions
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      NTOK = SP_STRN_NTOK(IPRM, ',')
      IF (NTOK .LT. 1) GOTO 9901

C---     Détecte un fichier ou une liste de valeurs
      IRET = SP_STRN_TKR(IPRM, ',', 1, VDUM)
      IF (NTOK .LE. 2 .AND. IRET .NE. 0) THEN
         IMOD = IC_DT_PRGL_MOD_FIC
      ELSE
         IMOD = IC_DT_PRGL_MOD_VAL
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_DT_PRGL_REQMOD = ERR_TYP()
      RETURN
      END


