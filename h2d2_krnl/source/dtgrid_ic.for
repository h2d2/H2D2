C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_GRID_XEQCTR
C     INTEGER IC_DT_GRID_XEQMTH
C     CHARACTER*(32) IC_DT_GRID_REQCLS
C     INTEGER IC_DT_GRID_REQHDL
C   Private:
C     SUBROUTINE IC_DT_GRID_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_GRID_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_GRID_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtgrid_ic.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtgrid_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HCOOR, HELEM, HNUMC, HNUME
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_GRID_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LECTURE DES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Handle on the coordinates (nodes) of the mesh</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, HCOOR)
C     <comment>Handle on the connectivities (elements) of the mesh.</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 2, HELEM)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Handle on the node renumbering. By default, the value will be set to 0 (no renumbering).</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 3, HNUMC)
      IF (IERR .NE. 0) HNUMC = 0
C     <comment>Handle on the elements renumbering. By default, the value will be set to 0 (no renumbering).</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 4, HNUME)
      IF (IERR .NE. 0) HNUME = 0

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_GRID_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = DT_GRID_INI(HOBJ,
     &                                   HCOOR, .FALSE., ! Don't trf ownership HCOOR
     &                                   HELEM, .FALSE., ! Don't trf ownership HELEM
     &                                   HNUMC, .FALSE., ! Don't trf ownership HNUMC
     &                                   HNUME, .FALSE.) ! Don't trf ownership HNUME

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) IERR = DT_GRID_PRN(HOBJ)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the mesh</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>grid</b> constructs an object, with the given arguments, and
C  returns a handle on this object.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_GRID_AID()

9999  CONTINUE
      IC_DT_GRID_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_GRID_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_GRID_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtgrid_ic.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtgrid_ic.fc'     ! Pour IC_DT_GRID_PRN

      INTEGER        IERR, IRET
      INTEGER        HVAL
      INTEGER        IVAL
      REAL*8         RVAL
      CHARACTER*(64) PROP
      CHARACTER*(256) NOMCOR, NOMELE
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Number of dimensions</comment>
         IF (PROP .EQ. 'dim') THEN
            IVAL = DT_GRID_REQNDIM(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Total number of nodes</comment>
         ELSEIF (PROP .EQ. 'nodes_total') THEN
            IVAL = DT_GRID_REQNNT(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of nodes local to the process</comment>
         ELSEIF (PROP .EQ. 'nodes_local') THEN
            IVAL = DT_GRID_REQNNL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of nodes per element</comment>
         ELSEIF (PROP .EQ. 'nodes_per_element') THEN
            IVAL = DT_GRID_REQNNEL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Total number of elements</comment>
         ELSEIF (PROP .EQ. 'elements_total') THEN
            IVAL = DT_GRID_REQNELT(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Number of elements local to the process</comment>
         ELSEIF (PROP .EQ. 'elements_local') THEN
            IVAL = DT_GRID_REQNELL(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'I', ',', IVAL

C        <comment>Handle on the coordinates</comment>
         ELSEIF (PROP .EQ. 'hcoor') THEN
            HVAL = DT_GRID_REQHCOOR(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

C        <comment>Handle on the connectivities</comment>
         ELSEIF (PROP .EQ. 'helem') THEN
            HVAL = DT_GRID_REQHELEV(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

C        <comment>Handle on the node numbering</comment>
         ELSEIF (PROP .EQ. 'hnumc') THEN
            HVAL = DT_GRID_REQHNUMC(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

C        <comment>Handle on the element numbering</comment>
         ELSEIF (PROP .EQ. 'hnume') THEN
            HVAL = DT_GRID_REQHNUME(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>split</b> creates a new grid with the T6 or T6L elements splitted into T3.</comment>
      ELSEIF (IMTH .EQ. 'split') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901

         HVAL = 0
         IF (ERR_GOOD()) IERR = DT_GRID_SPLIT (HOBJ, HVAL)
         IF (ERR_GOOD()) THEN
C           <comment>Handle on the new grid.</comment>
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL
         ELSE
            IPRM = ' '
         ENDIF

C     <comment>The method <b>load</b> loads the grid from files.</comment>
      ELSEIF (IMTH .EQ. 'load') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900
C        <comment>Time</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, RVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) IERR = DT_GRID_CHARGE(HOBJ, RVAL)

C     <comment>
C     The method <b>save</b> saves the grid to files. The grid must be
C     loaded, either explicitly with a call to the method <i>load</i> or
C     implicitly through an algorithm
C     </comment>
      ELSEIF (IMTH .EQ. 'save') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900

C        <comment>Coordinates file name</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMCOR)
C        <comment>Connectivities file name</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, NOMELE)
         IF (IERR .NE. 0) GOTO 9901

         IF (ERR_GOOD())
     &       IERR = DT_GRID_SAUVE(HOBJ,
     &                            NOMCOR,
     &                            NOMELE,
     &                            IO_MODE_ECRITURE + IO_MODE_ASCII)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_GRID_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_GRID_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_GRID_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_GRID_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_GRID_AID()

9999  CONTINUE
      IC_DT_GRID_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_GRID_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_GRID_REQCLS
CDEC$ ENDIF

      INCLUDE 'dtgrid_ic.fi'
C------------------------------------------------------------------------

C<comment>
C   The class <b>grid</b> represents a finite element mesh. A finite element mesh
C   is a homogeneous assembly of identical full geometric entities of one,
C   two or three dimensions, made of a finite number of nodes placed inside
C   or on the contour of the domain, discretizing this domain of
C   calculation.
C</comment>

      IC_DT_GRID_REQCLS = 'grid'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_GRID_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_GRID_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtgrid_ic.fi'
      INCLUDE 'dtgrid.fi'
C-------------------------------------------------------------------------

      IC_DT_GRID_REQHDL = DT_GRID_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_GRID_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_GRID_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtgrid_ic.hlp')

      RETURN
      END

