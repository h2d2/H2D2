C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_ELEM_000
C     INTEGER DT_ELEM_999
C     INTEGER DT_ELEM_PKL
C     INTEGER DT_ELEM_UPK
C     INTEGER DT_ELEM_CTR
C     INTEGER DT_ELEM_DTR
C     INTEGER DT_ELEM_INIDIM
C     INTEGER DT_ELEM_INIFIC
C     INTEGER DT_ELEM_INISPL
C     INTEGER DT_ELEM_RST
C     INTEGER DT_ELEM_REQHBASE
C     LOGICAL DT_ELEM_HVALIDE
C     INTEGER DT_ELEM_PRN
C     INTEGER DT_ELEM_CHARGE
C     INTEGER DT_ELEM_MAJVAL
C     INTEGER DT_ELEM_SAUVE
C     LOGICAL DT_ELEM_ESTINIFIC
C     LOGICAL DT_ELEM_ESTINIDIM
C     LOGICAL DT_ELEM_ESTINISPL
C     INTEGER DT_ELEM_REQISTAT
C     INTEGER DT_ELEM_REQITPE
C     INTEGER DT_ELEM_REQLELE
C     INTEGER DT_ELEM_REQNCEL
C     INTEGER DT_ELEM_REQNNEL
C     INTEGER DT_ELEM_REQNELT
C     INTEGER DT_ELEM_REQNELL
C     INTEGER DT_ELEM_REQTEMPS
C     CHARACTER*256 DT_ELEM_REQNOMF
C   Private:
C     INTEGER DT_ELEM_RAZ
C     INTEGER DT_ELEM_PRNELE
C     INTEGER DT_ELEM_CHRGSPL
C     INTEGER DT_ELEM_CHRGFIC
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction DT_ELEM_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_ELEM_NOBJMAX,
     &                   DT_ELEM_HBASE,
     &                   'Connectivities')

      DT_ELEM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction DT_ELEM_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fc'

      INTEGER  IERR
      EXTERNAL DT_ELEM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_ELEM_NOBJMAX,
     &                   DT_ELEM_HBASE,
     &                   DT_ELEM_DTR)

      DT_ELEM_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER N, L
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_ELEM_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_ELEM_HFELE(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_ELEM_HEPAR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_ELEM_HNUMC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_ELEM_HNUME(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_ELEM_NELT (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_ELEM_TEMPS(IOB))

C---     La table
      IF (ERR_GOOD()) THEN
         N = DT_ELEM_REQNNEL(HOBJ) * DT_ELEM_NELL(IOB)
         L = DT_ELEM_LELEM(IOB)
         IERR = IO_XML_WI_V(HXML, N, N, L, 1)
      ENDIF

      DT_ELEM_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     1) Pas d'assertion: On ne peut pas faire d'appels de fonctions
C     tant que tous les objets n'ont pas été reconstruits.
C************************************************************************
      FUNCTION DT_ELEM_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_ELEM_HBASE

C---     Reset l'objet
      IF (LNEW) IERR = DT_ELEM_RAZ(HOBJ)
      IF (ERR_GOOD()) IERR = DT_ELEM_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_ELEM_HFELE(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_ELEM_HEPAR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_ELEM_HNUMC(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_ELEM_HNUME(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_ELEM_NELT (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_ELEM_NELL (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_ELEM_TEMPS(IOB))

C---     La table
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
      DT_ELEM_LELEM(IOB) = L  ! c.f. note

      DT_ELEM_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction DT_ELEM_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   DT_ELEM_NOBJMAX,
     &                   DT_ELEM_HBASE)
      IF (ERR_GOOD()) IERR = DT_ELEM_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_ELEM_HVALIDE(HOBJ))

      DT_ELEM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction DT_ELEM_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_ELEM_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_ELEM_NOBJMAX,
     &                   DT_ELEM_HBASE)

      DT_ELEM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode DT_ELEM_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - DT_ELEM_HBASE

      DT_ELEM_TEMPS (IOB) = -1.0D0
      DT_ELEM_HFELE (IOB) = 0
      DT_ELEM_HEPAR (IOB) = 0
      DT_ELEM_HNUMC (IOB) = 0
      DT_ELEM_HNUME (IOB) = 0
      DT_ELEM_LELEM (IOB) = 0
      DT_ELEM_ITPE  (IOB) = 0
      DT_ELEM_NCEL  (IOB) = 0
      DT_ELEM_NNEL  (IOB) = 0
      DT_ELEM_NELT  (IOB) = 0
      DT_ELEM_NELL  (IOB) = 0

      DT_ELEM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec des dimensions.
C
C Description:
C     La fonction <code>DT_ELEM_INIDIM(...)<code> initialise l'objet avec
C     les dimensions en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     ITPE     Type d'élément
C     NNEL     Nombre de Noeuds par ELement
C     NELL     Nombre d'ELements Locaux
C     NELT     Nombre d'ELements Totaux
C
C Sortie:
C
C Notes:
C     le cas (NELL .NE. NELT) n'est pas supporté. En fait il faudrait passer
C     la renum correspondant.
C************************************************************************
      FUNCTION DT_ELEM_INIDIM(HOBJ, ITPE, NCEL, NNEL, NELL, NELT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_INIDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER ITPE
      INTEGER NNEL
      INTEGER NCEL
      INTEGER NELL
      INTEGER NELT

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LELEM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(ITPE .GT. 0)
D     CALL ERR_PRE(NCEL .GT. 0)
D     CALL ERR_PRE(NNEL .GT. 0)
D     CALL ERR_PRE(NCEL .GE. NNEL)
D     CALL ERR_PRE(NELL .GE. 0)
D     CALL ERR_PRE(NELT .GT. 0)
D     CALL ERR_PRE(NELT .GE. NELL)
D     CALL ERR_PRE(NELT .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (NCEL .LE. 0) GOTO 9900
      IF (NNEL .LE. 0) GOTO 9900
      IF (NELL .LT. 0) GOTO 9901
      IF (NELT .LE. 0) GOTO 9902
      IF (NELL .NE. NELT) GOTO 9902    ! c.f. note
      IF (ITPE .LE. 0) GOTO 9903

C---     Reset les données
      IERR = DT_ELEM_RST(HOBJ)

C---     Alloue l'espace (min de 1 pour qu'il existe)
      LELEM = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(MAX(1,NELL*NCEL), LELEM)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_ELEM_HBASE
         DT_ELEM_TEMPS (IOB) = -1.0D0
         DT_ELEM_HFELE (IOB) = 0
         DT_ELEM_HEPAR (IOB) = 0
         DT_ELEM_HNUMC (IOB) = 0
         DT_ELEM_HNUME (IOB) = 0
         DT_ELEM_LELEM (IOB) = LELEM
         DT_ELEM_ITPE  (IOB) = ITPE
         DT_ELEM_NCEL  (IOB) = NCEL
         DT_ELEM_NNEL  (IOB) = NNEL
         DT_ELEM_NELT  (IOB) = NELT
         DT_ELEM_NELL  (IOB) = NELL
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NNEL_INVALIDE',':', NNEL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NELL_INVALIDE',':', NELL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NELT_INVALIDE',':', NELT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12)') 'ERR_TYPE_ELEMENT_INVALIDE', ITPE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_ELEM_INIDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec un fichier de données.
C
C Description:
C     La fonction <code>DT_ELEM_INIFIC(...)<code> initialise l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     FICELE      Nom du fichier d'éléments
C     ISTAT       IO_LECTURE, mais pas IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_INIFIC(HOBJ, FICELE, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_INIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FICELE
      INTEGER       ISTAT

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITPE
      INTEGER HFELE, HFRGO
      INTEGER NELT, NNEL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (SP_STRN_LEN(FICELE) .LE. 0) GOTO 9900

C---     Reset les données
      IERR = DT_ELEM_RST(HOBJ)

C---     Construis le fichier de données
      IF (ERR_GOOD())IERR = FD_ELEM_CTR(HFELE)
      IF (ERR_GOOD())IERR = FD_ELEM_INI(HFELE,
     &                                  FICELE(1:SP_STRN_LEN(FICELE)),
     &                                  ISTAT)

C---     Demande les dimensions
      NELT = 0
      NNEL = 0
      ITPE = 0
      IF (ERR_GOOD()) NELT = FD_ELEM_REQNELT(HFELE)
      IF (ERR_GOOD()) NNEL = FD_ELEM_REQNNEL(HFELE)
      IF (ERR_GOOD()) ITPE = FD_ELEM_REQITPE(HFELE)
D     CALL ERR_ASR(ERR_BAD() .OR. NELT .GT. 0)
D     CALL ERR_ASR(ERR_BAD() .OR. NNEL .GT. 0)
D     CALL ERR_ASR(ERR_BAD() .OR. ITPE .GT. 0)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_ELEM_HBASE
         DT_ELEM_TEMPS (IOB) = -1.0D0
         DT_ELEM_HFELE (IOB) = HFELE
         DT_ELEM_HEPAR (IOB) = 0
         DT_ELEM_HNUMC (IOB) = 0
         DT_ELEM_HNUME (IOB) = 0
         DT_ELEM_LELEM (IOB) = 0
         DT_ELEM_ITPE  (IOB) = ITPE
         DT_ELEM_NCEL  (IOB) = NNEL
         DT_ELEM_NNEL  (IOB) = NNEL
         DT_ELEM_NELT  (IOB) = NELT
         DT_ELEM_NELL  (IOB) = 0        ! Connu à la lecture
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_ELEM_INIFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise comme split
C
C Description:
C     La fonction DT_ELEM_INISPL initialise l'objet comme maillage
C     split dépendant d'un parent.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HEPAR    Handle sur le parent
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_INISPL(HOBJ, HEPAR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_INISPL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HEPAR

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'spsplit.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITPEV, NNELV, NCELV, NELTV
      INTEGER ITPEZ, NNELZ, NCELZ, NELTZ
      INTEGER KNGV(1,1), KNGZ(1,1)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HEPAR))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RESET LES DONNEES
      IERR = DT_ELEM_RST(HOBJ)

C---     Récupère les dimensions
      ITPEV = DT_ELEM_REQITPE(HEPAR)
      NNELV = DT_ELEM_REQNNEL(HEPAR)
      NCELV = DT_ELEM_REQNCEL(HEPAR)
      NELTV = DT_ELEM_REQNELT(HEPAR)

      ITPEZ = EG_TPGEO_INDEFINI
      IERR = SP_SPLIT_SPLIT(ITPEV,
     &                      NNELV,
     &                      NCELV,
     &                      NELTV,
     &                      KNGV,
     &                      ITPEZ,
     &                      NNELZ,
     &                      NCELZ,
     &                      NELTZ,
     &                      KNGZ)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_ELEM_HBASE
         DT_ELEM_TEMPS (IOB) = -1.0D0
         DT_ELEM_HFELE (IOB) = 0
         DT_ELEM_HEPAR (IOB) = HEPAR
         DT_ELEM_HNUMC (IOB) = 0
         DT_ELEM_HNUME (IOB) = 0
         DT_ELEM_LELEM (IOB) = 0
         DT_ELEM_ITPE  (IOB) = ITPEZ
         DT_ELEM_NCEL  (IOB) = NCELZ
         DT_ELEM_NNEL  (IOB) = NCELZ
         DT_ELEM_NELT  (IOB) = NELTZ
         DT_ELEM_NELL  (IOB) = 0        ! Connu à la lecture
      ENDIF

      DT_ELEM_INISPL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFELE, HFRGO
      INTEGER LELEM, LELCO
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB = HOBJ - DT_ELEM_HBASE

C---     Désalloue la mémoire
      LELEM = DT_ELEM_LELEM(IOB)
      IF (LELEM .NE. 0) IERR = SO_ALLC_ALLINT(0, LELEM)

C---     Détruis le fichier
      HFELE = DT_ELEM_HFELE(IOB)
      IF (FD_ELEM_HVALIDE(HFELE)) IERR = FD_ELEM_DTR(HFELE)

C---     Efface les attributs
      IERR = DT_ELEM_RAZ(HOBJ)

      DT_ELEM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_ELEM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C------------------------------------------------------------------------

      DT_ELEM_REQHBASE = DT_ELEM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_ELEM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtelem.fc'
C------------------------------------------------------------------------

      DT_ELEM_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_ELEM_NOBJMAX,
     &                                  DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DT_ELEM_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C     Prototype
C************************************************************************
      FUNCTION DT_ELEM_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFELE
      INTEGER LELEM
      CHARACTER*256 NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - DT_ELEM_HBASE
      HFELE = DT_ELEM_HFELE(IOB)

C---     EN-TETE
      LOG_ZNE = 'h2d2.data.elem'
      WRITE (LOG_BUF, '(A)') 'MSG_ELEMENTS'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOM DES FICHIERS
      IF (FD_ELEM_HVALIDE(HFELE)) THEN
         NOM = DT_ELEM_REQNOMF(HOBJ)
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

C---     NOMBRE DE DIMENSIONS
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_OF_NOD_PER_ELEM#<35>#= ',
     &                           DT_ELEM_NNEL(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOMBRE DE NOEUDS TOTAL
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_TOTAL#<35>#= ',
     &                           DT_ELEM_NELT(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOMBRE DE NOEUDS LOCAL
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_LOCAL#<35>#= ',
     &                           DT_ELEM_NELL(IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     TYPE
      WRITE (LOG_BUF,'(A,I12)') 'MSG_TYPE_ELEMENT#<35>#= ',
     &                             DT_ELEM_ITPE(IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     TEMPS ACTUEL
      WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ',
     &                             DT_ELEM_TEMPS(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     CHARGÉ?
      WRITE (LOG_BUF,'(A,L12)') 'MSG_CHARGE#<35>#= ',
     &                           (DT_ELEM_LELEM(IOB) .NE. 0)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     POINTEUR AUX DONNÉES
      WRITE (LOG_BUF,'(A,Z12)') 'MSG_LVNOD#<35>#= ',
     &                           DT_ELEM_LELEM(IOB)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     LES VALEURS
      IF (LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_DEBUG)) THEN
         LELEM = DT_ELEM_LELEM(IOB)
         IF (SO_ALLC_HEXIST(LELEM))
     &      IERR = DT_ELEM_PRNELE(DT_ELEM_NELL(IOB),
     &                            DT_ELEM_NNEL(IOB),
     &                            KA(SO_ALLC_REQKIND(KA, LELEM)))
      ENDIF

      CALL LOG_DECIND()

      DT_ELEM_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime les valeurs nodales.
C
C Description:
C     La fonction privée DT_ELEM_PRNELE imprime les valeurs. Elle permet
C     de résoudre l'utilisation de la table KNG.
C
C Entrée:
C     NELL         Nombre d'éléments locaux
C     NNEL         Nombre de noeuds par élément
C     KNG          Table des connectivités
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_PRNELE(NELL, NNEL, KNG)

      IMPLICIT NONE

      INTEGER DT_ELEM_PRNELE
      INTEGER NELL
      INTEGER NNEL
      INTEGER KNG(NNEL, *)

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IE, I
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NELL .GT. 0)
D     CALL ERR_PRE(NNEL .GT. 0)
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.data.elem'

      WRITE(LOG_BUF,'(A)') 'MSG_VALEURS_LOCALES:'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      DO IE = 1, NELL
         WRITE(LOG_BUF, '(I6,A,12(1X,I6))')
     &         IE, ': ', (KNG(I,IE),I=1,NNEL)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

      DT_ELEM_PRNELE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_ELEM_CHRGSPL
C
C Description:
C     La fonction <code>DT_ELEM_CHRGSPL(...)<code> charge l'objet avec les
C     données au temps TNEW. Les données sont prises du fichier maintenu par
C     l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     HNUME       Handle sur la renumérotation
C     TNEW        Temps pour lequel les données doivent être chargées
C
C Sortie:
C     MODIF       VRAI s'il y a eu modification
C
C Notes:
C     Le temps n'est pas supporté pour les éléments
C     A la lecture on doit avoir NNEL=NCEL
C     La fonction n'est pas récursive, et on ne peut pas avoir de
C     parent de parent (grand-parents)
C     Si les renumérotations changent, il faudrait forcer la relecture
C************************************************************************
      FUNCTION DT_ELEM_CHRGSPL(HOBJ, HNUMC, HNUME, TNEW, MODIF)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC, HNUME
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtpgeo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spsplit.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITPE_L, ITPE_P
      INTEGER HEPAR_L
      INTEGER LELEM_L, LELEM_P
      INTEGER NCEL_L, NCEL_P
      INTEGER NNEL_L, NNEL_P
      INTEGER NELL_L, NELL_P
      INTEGER KNGV_L(1,1), KNGV_P(1,1)
      LOGICAL ESTINI
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_ELEM_HBASE
      ITPE_L = DT_ELEM_ITPE (IOB)
      HEPAR_L= DT_ELEM_HEPAR(IOB)
      LELEM_L= DT_ELEM_LELEM(IOB)
      NCEL_L = DT_ELEM_NCEL (IOB)
      NNEL_L = DT_ELEM_NNEL (IOB)
      NELL_L = DT_ELEM_NELL (IOB)
      ESTINI = (NELL_L .GT. 0 .AND. SO_ALLC_HEXIST(LELEM_L))

C---     Contrôles
      IF (.NOT. DT_ELEM_HVALIDE(HEPAR_L)) GOTO 9903

C---     Charge le parent
      IF (ERR_GOOD()) THEN
         MODIF = .FALSE.
         IERR = DT_ELEM_CHARGE(HEPAR_L, HNUMC, HNUME, TNEW, MODIF)
         IF (.NOT. MODIF .AND. ESTINI) GOTO 9999
      ENDIF

C---     Récupère les données du parent
      ITPE_P = DT_ELEM_REQITPE(HEPAR_L)
      NCEL_P = DT_ELEM_REQNCEL(HEPAR_L)
      NNEL_P = DT_ELEM_REQNNEL(HEPAR_L)
      NELL_P = DT_ELEM_REQNELT(HEPAR_L)
      LELEM_P= DT_ELEM_REQLELE(HEPAR_L)

C---    Demande les dim locales
      IF (ERR_GOOD()) THEN
         ITPE_L = EG_TPGEO_INDEFINI
         IERR = SP_SPLIT_SPLIT(ITPE_P,
     &                         NNEL_P,
     &                         NCEL_P,
     &                         NELL_P,
     &                         KNGV_P,
     &                         ITPE_L,
     &                         NNEL_L,
     &                         NCEL_L,
     &                         NELL_L,
     &                         KNGV_L)
         !! Tout plein d'assertions possibles
      ENDIF

C---     (Re)-Alloue l'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELL_L*NNEL_L, LELEM_L)
      IF (ERR_GOOD()) DT_ELEM_LELEM(IOB) = LELEM_L

C---     Split
      IF (ERR_GOOD()) THEN
         IERR = SP_SPLIT_SPLIT(ITPE_P,
     &                         NNEL_P,
     &                         NCEL_P,
     &                         NELL_P,
     &                         KA(SO_ALLC_REQKIND(KA, LELEM_P)),
     &                         ITPE_L,
     &                         NNEL_L,
     &                         NCEL_L,
     &                         NELL_L,
     &                         KA(SO_ALLC_REQKIND(KA, LELEM_L)))
      ENDIF

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_ELEM_TEMPS(IOB) = TNEW
         DT_ELEM_HNUMC(IOB) = HNUMC
         DT_ELEM_HNUME(IOB) = HNUME
         DT_ELEM_NELL (IOB) = NELL_L
         MODIF = .TRUE.
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9903  WRITE(ERR_BUF,'(A)') 'ERR_MAILLAGE_PARENT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HEPAR_L, 'MSG_ELEM')
      GOTO 9999

9999  CONTINUE
      DT_ELEM_CHRGSPL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_ELEM_CHRGFIC
C
C Description:
C     La fonction <code>DT_ELEM_CHRGFIC(...)<code> charge l'objet
C     à partir d'un fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     HNUME       Handle sur la renumérotation
C     TNEW        Temps pour lequel les données doivent être chargées
C
C Sortie:
C     MODIF       VRAI s'il y a eu modification
C
C Notes:
C     Le temps n'est pas supporté pour les éléments
C     A la lecture on doit avoir NNEL=NCEL
C************************************************************************
      FUNCTION DT_ELEM_CHRGFIC(HOBJ, HNUMC, HNUME, TNEW, MODIF)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC, HNUME
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFELE
      INTEGER LELEM
      INTEGER HNUMC_L
      INTEGER HNUME_L
      INTEGER NNEL
      INTEGER NELL_L, NELL_R
      INTEGER NELT_L, NELT_R
      REAL*8  TOLD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_ELEM_HBASE
      HNUMC_L= DT_ELEM_HNUMC(IOB)
      HNUME_L= DT_ELEM_HNUME(IOB)
      HFELE  = DT_ELEM_HFELE(IOB)
      LELEM  = DT_ELEM_LELEM(IOB)
      NNEL   = DT_ELEM_NNEL (IOB)
      NELT_L = DT_ELEM_NELT (IOB)
      NELL_L = DT_ELEM_NELL (IOB)
      TOLD   = DT_ELEM_TEMPS(IOB)
D     CALL ERR_ASR(HFELE .EQ. 0 .OR. FD_ELEM_HVALIDE (HFELE))

C---     Récupère les données
      NELT_R = NM_NUMR_REQNNT(HNUME)
      NELL_R = NM_NUMR_REQNNL(HNUME)
D     CALL ERR_ASR(NELT_R .GT. 0)
D     CALL ERR_ASR(NELL_R .GE. 0)

C---     Contrôles
      IF (NELL_L .NE. 0 .AND. NELL_L .NE. NELL_R) GOTO 9902
      IF (NELT_L .NE. NELT_R) GOTO 9903

C---     (Re)-Alloue l'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NELL_R*NNEL, LELEM)
      IF (ERR_GOOD()) DT_ELEM_LELEM(IOB) = LELEM

C---     Lis les données
      MODIF = .FALSE.
      IF (HNUMC_L .NE. HNUMC) TOLD = -1.0D0    ! Force une relecture
      IF (HNUME_L .NE. HNUME) TOLD = -1.0D0    ! Force une relecture
      IERR = FD_ELEM_LIS(HFELE,
     &                   HNUMC,
     &                   HNUME,
     &                   NELL_R,
     &                   NNEL,
     &                   KA(SO_ALLC_REQKIND(KA, LELEM)),
     &                   TOLD,
     &                   TNEW,
     &                   MODIF)

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_ELEM_TEMPS(IOB) = TNEW
         DT_ELEM_HNUMC(IOB) = HNUMC
         DT_ELEM_HNUME(IOB) = HNUME
         DT_ELEM_NELL (IOB) = NELL_R
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9902  WRITE(ERR_BUF,'(A)') 'ERR_NELL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_LOCAL#<35>#:', NELL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_LOCAL#<35>#:', NELL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(A)') 'ERR_NELT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_TOTAL#<35>#:', NELT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NBR_OF_ELEM_TOTAL#<35>#:', NELT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_ELEM_CHRGFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_ELEM_CHARGE
C
C Description:
C     La fonction <code>DT_ELEM_CHARGE(...)<code> charge l'objet avec les
C     données au temps TNEW. Les données sont prises du fichier maintenu par
C     l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     HNUME       Handle sur la renumérotation
C     TNEW        Temps pour lequel les données doivent être chargées
C
C Sortie:
C     MODIF       VRAI s'il y a eu modification
C
C Notes:
C     Le temps n'est pas supporté pour les éléments
C     A la lecture on doit avoir NNEL=NCEL
C     La fonction n'est pas récursive, et on ne peut pas avoir de
C     parent de parent (grand-parents)
C     Si les renumérotations changent, il faudrait forcer la relecture
C************************************************************************
      FUNCTION DT_ELEM_CHARGE(HOBJ, HNUMC, HNUME, TNEW, MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMC, HNUME
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFELE
      INTEGER HEPAR
      INTEGER LELEM
      INTEGER HNUMC_L
      INTEGER HNUME_L
      INTEGER NCEL, NNEL
      INTEGER NELL_L
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      LOG_ZNE = 'h2d2.grid.elem'

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.elem')

C---     Récupère les attributs
      IOB  = HOBJ - DT_ELEM_HBASE
      HNUMC_L= DT_ELEM_HNUMC(IOB)
      HNUME_L= DT_ELEM_HNUME(IOB)
      HFELE  = DT_ELEM_HFELE(IOB)
      HEPAR  = DT_ELEM_HEPAR(IOB)
      NCEL   = DT_ELEM_NCEL (IOB)
      NNEL   = DT_ELEM_NNEL (IOB)
      NELL_L = DT_ELEM_NELL (IOB)
D     CALL ERR_ASR(HFELE .EQ. 0 .OR. FD_ELEM_HVALIDE (HFELE))

C---     Contrôles génériques
      IF (.NOT. FD_ELEM_HVALIDE(HFELE)) THEN
         IF (HNUMC_L .NE. 0 .AND. HNUMC_L .NE. HNUMC) GOTO 9900
         IF (HNUME_L .NE. 0 .AND. HNUME_L .NE. HNUME) GOTO 9901
      ENDIF

C---     Charge spécialisés
      MODIF = .FALSE.
      IF     (DT_ELEM_ESTINISPL(HOBJ)) THEN
         IERR = DT_ELEM_CHRGSPL(HOBJ, HNUMC, HNUME, TNEW, MODIF)
      ELSEIF (DT_ELEM_ESTINIFIC(HOBJ)) THEN
         IERR = DT_ELEM_CHRGFIC(HOBJ, HNUMC, HNUME, TNEW, MODIF)
      ELSEIF (DT_ELEM_ESTINIDIM(HOBJ)) THEN
D        CALL LOG_TODO('DT_ELEM_CHARGE invalide pour DT_ELEM_ESTINIDIM.'
D    &              // ' Il manque un DT_ELEM_CHRGDIM')
D        CALL ERR_ASR(.FALSE.)
         IF (HNUMC_L .NE. HNUMC .OR. HNUME_L .NE. HNUME) MODIF = .TRUE.
      ELSE
D        CALL ERR_ASR(.FALSE.)
      ENDIF

C---     Contrôle les éléments
      LELEM = DT_ELEM_LELEM(IOB)    ! Possiblement modifié par charge
      NELL_L = DT_ELEM_NELL (IOB)
      IF (ERR_GOOD() .AND. MODIF) THEN
         IERR = SP_ELEM_CTRCON(NNEL,
     &                         NCEL,
     &                         NELL_L,
     &                         KA(SO_ALLC_REQKIND(KA, LELEM)),
     &                        .TRUE.)  ! Main grid
      ENDIF

C---     Imprime les éléments
      IF (ERR_GOOD() .AND. MODIF .AND.
     &    LOG_ESTZONEACTIVE(LOG_ZNE, LOG_LVL_DEBUG)) THEN
         IERR = DT_ELEM_PRNELE(NELL_L,
     &                         NCEL,
     &                         KA(SO_ALLC_REQKIND(KA, LELEM)))
      ENDIF

C---     Conserve le temps et la renumérotation
      IF (ERR_GOOD()) THEN
         DT_ELEM_TEMPS(IOB) = TNEW
         DT_ELEM_HNUMC(IOB) = HNUMC
         DT_ELEM_HNUME(IOB) = HNUME
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMC_L, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMC,   'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUME_L, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUME,   'MSG_RENUM')
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.elem')    ! STOPPE LE CHRONO
      DT_ELEM_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Met à jour les valeurs.
C
C Description:
C     La fonction DT_ELEM_MAJVAL assigne à l'objet les valeurs passées
C     en argument. La table K doit avoir les mêmes dimensions que l'objet
C     et ses valeurs vont donc remplacer toutes celles de l'objet.
C     en argument.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NCEL
C     NELL
C     K           Tables des valeurs
C     TEMPS       Temps associé aux valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_MAJVAL(HOBJ, NCEL, NELL, K, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_MAJVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCEL
      INTEGER NELL
      INTEGER K(NCEL, NELL)
      REAL*8  TEMPS

      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LELE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_ELEM_HBASE
      LELE = DT_ELEM_LELEM(IOB)
D     CALL ERR_ASR(NCEL .EQ. DT_ELEM_REQNCEL(HOBJ))
D     CALL ERR_ASR(NELL .GE. DT_ELEM_REQNELL(HOBJ))
D     CALL ERR_ASR(SO_ALLC_HEXIST(LELE))

C---     Transfert
      CALL ICOPY(NCEL*NELL,
     &           K, 1,
     &           KA(SO_ALLC_REQKIND(KA, LELE)), 1)

C---     Conserve le temps
      IF (ERR_GOOD()) THEN
         DT_ELEM_TEMPS(IOB) = TEMPS
      ENDIF

      DT_ELEM_MAJVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_ELEM_SAUVE
C
C Description:
C     La fonction <code>DT_ELEM_SAUVE(...)<code> écris les données de l'objet
C     dans le fichier maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_SAUVE(HOBJ, HNUMC, HNUME, FICELE, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HNUMC
      INTEGER       HNUME
      CHARACTER*(*) FICELE
      INTEGER       ISTAT

      INCLUDE 'dtelem.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtelem.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ITPE
      INTEGER HFELE
      INTEGER LELE
      INTEGER NELL, NELT
      INTEGER NNEL
      REAL*8  TSIM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_ELEM_HBASE

C---     RECUPERE LES DONNEES
      TSIM = DT_ELEM_TEMPS(IOB)
      LELE = DT_ELEM_LELEM(IOB)
      ITPE = DT_ELEM_ITPE (IOB)
      NNEL = DT_ELEM_NNEL (IOB)
      NELL = DT_ELEM_NELL (IOB)
      NELT = DT_ELEM_NELT (IOB)
D     CALL ERR_ASR(NELT .GE. NELL)
D     CALL ERR_ASR(NELL .GE. 0)
D     CALL ERR_ASR(LELE .NE. 0)

C---     Construis le fichier
      IF (ERR_GOOD()) IERR = FD_ELEM_CTR(HFELE)
      IF (ERR_GOOD()) IERR = FD_ELEM_INI(HFELE,
     &                                   FICELE(1:SP_STRN_LEN(FICELE)),
     &                                   ISTAT)

C---     Écris
      IF (ERR_GOOD()) IERR = FD_ELEM_ECRIS(HFELE,
     &                                     HNUMC,
     &                                     HNUME,
     &                                     ITPE,
     &                                     NELT,
     &                                     NELL,
     &                                     NNEL,
     &                                     KA(SO_ALLC_REQKIND(KA,LELE)),
     &                                     TSIM)

C---     Détruis le fichier
      IF (FD_ELEM_HVALIDE(HFELE)) THEN
         CALL ERR_PUSH()
         IERR = FD_ELEM_DTR(HFELE)
         CALL ERR_POP()
      ENDIF

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_ELEM_HNUMC(IOB) = HNUMC
         DT_ELEM_HNUME(IOB) = HNUME
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FICHIER_NON_DEFINI'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_ELEM_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé avec un fichier.
C
C Description:
C     La fonction DT_ELEM_ESTINIFIC retourne .TRUE. si l'objet a été
C     initialisé avec un fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_ESTINIFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_ESTINIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'dtelem.fc'

      INTEGER HFELE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFELE = DT_ELEM_HFELE(HOBJ-DT_ELEM_HBASE)
      DT_ELEM_ESTINIFIC = FD_ELEM_HVALIDE(HFELE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé avec des données.
C
C Description:
C     La fonction DT_ELEM_ESTINIDIM retourne .TRUE. si l'objet a été
C     initialisé avec des données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_ESTINIDIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_ESTINIDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_ESTINIDIM = (.NOT. DT_ELEM_ESTINIFIC(HOBJ)) .AND.
     &                    (.NOT. DT_ELEM_ESTINISPL(HOBJ))
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé comme maillage split.
C
C Description:
C     La fonction DT_ELEM_ESTINISPL retourne .TRUE. si l'objet a été
C     initialisé comme un maillage split.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_ESTINISPL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_ESTINISPL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'

      INTEGER HEPAR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HEPAR = DT_ELEM_HEPAR(HOBJ-DT_ELEM_HBASE)
      DT_ELEM_ESTINISPL = DT_ELEM_HVALIDE(HEPAR)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQISTAT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQISTAT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'dtelem.fc'

      INTEGER HFELE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFELE = DT_ELEM_HFELE(HOBJ-DT_ELEM_HBASE)
D     CALL ERR_ASR(FD_ELEM_HVALIDE(HFELE))

      DT_ELEM_REQISTAT = FD_ELEM_REQISTAT(HFELE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQITPE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQITPE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQITPE = DT_ELEM_ITPE(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le pointeur aux données
C
C Description:
C     La fonction DT_ELEM_REQLELE retourne le pointeur à la table des
C     connectivités maintenues par l'objet. La table est de dimensions
C     (NNEL, NELL). Le pointeur peut être nul.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQLELE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQLELE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQLELE = DT_ELEM_LELEM(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de connectivités par élément.
C
C Description:
C     La fonction DT_ELEM_REQNCEL retourne le nombre de connectivités par élément
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQNCEL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQNCEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQNCEL = DT_ELEM_NCEL(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de noeuds par élément.
C
C Description:
C     La fonction DT_ELEM_REQNNEL retourne le nombre de noeuds par élément
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQNNEL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQNNEL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQNNEL = DT_ELEM_NNEL(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQNELT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQNELT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQNELT = DT_ELEM_NELT(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQNELL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQNELL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQNELL = DT_ELEM_NELL(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps des données.
C
C Description:
C     La fonction DT_ELEM_REQTEMPS retourne le temps associé aux données
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQTEMPS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQTEMPS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'dtelem.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_ELEM_REQTEMPS = DT_ELEM_TEMPS(HOBJ-DT_ELEM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du fichier de lecture.
C
C Description:
C     La fonction DT_ELEM_REQNOMF retourne le nom du fichier de lecture
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_ELEM_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_ELEM_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtelem.fi'
      INCLUDE 'fdelem.fi'
      INCLUDE 'dtelem.fc'

      INTEGER HFELE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFELE = DT_ELEM_HFELE(HOBJ-DT_ELEM_HBASE)
D     CALL ERR_ASR(FD_ELEM_HVALIDE(HFELE))

      DT_ELEM_REQNOMF = FD_ELEM_REQNOMF(HFELE)
      RETURN
      END
