C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   Base des algo renum/partition par noeuds
C Type:    Concret
C
C Class: NM_NBSE
C     INTEGER NM_NBSE_000
C     INTEGER NM_NBSE_999
C     INTEGER NM_NBSE_CTR
C     INTEGER NM_NBSE_DTR
C     INTEGER NM_NBSE_INI
C     INTEGER NM_NBSE_RST
C     INTEGER NM_NBSE_REQHBASE
C     LOGICAL NM_NBSE_HVALIDE
C     INTEGER NM_NBSE_PART
C     INTEGER NM_NBSE_RENUM
C     INTEGER NM_NBSE_SAUVE
C     INTEGER NM_NBSE_GENNUM
C     INTEGER NM_NBSE_ASGDSTR
C     INTEGER NM_NBSE_PA_XEQ
C     INTEGER NM_NBSE_PA_RNM
C     INTEGER NM_NBSE_PA_NNL
C     INTEGER NM_NBSE_PA_C2R
C     INTEGER NM_NBSE_RN_XEQ
C
C Note:
C     Structure de la table de redistribution locale, une colonne pour
C     chaque sous-domaine assigné au process: KDISTR(NPRTL+1, NNT)
C        NNO_PRL1 NNO_PRL2 ...  NNO_PRLn IPROC   ! .LE. 0 si absent
C************************************************************************

      MODULE NM_NBSE_M

      USE SO_ALLC_M
      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: NM_NBSE_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: NM_NBSE_SELF_T
C        <comment>Function de partitionnement</comment>
         INTEGER HFPRT
C        <comment>Function de renumérotation</comment>
         INTEGER HFRNM
C        <comment>Table de distribution</comment>
         INTEGER LDSTR
C        <comment>Nombre de noeuds total</comment>
         INTEGER NNT
C        <comment>Nombre de sous-domaines global du partitionnement</comment>
         INTEGER NPRTG
C        <comment>Nombre de sous-domaines local du partitionnement</comment>
         INTEGER NPRTL
C        <comment>Indice global du premier sous-domaines local</comment>
         INTEGER IPRTL
      END TYPE NM_NBSE_SELF_T

      CONTAINS
      
C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée NM_NBSE_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (NM_NBSE_SELF_T), POINTER :: NM_NBSE_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      NM_NBSE_REQSELF => SELF
      RETURN
      END FUNCTION NM_NBSE_REQSELF

      END MODULE NM_NBSE_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction NM_NBSE_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_000
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(NM_NBSE_HBASE,
     &                   'Base de renumérotation par noeuds')

      NM_NBSE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction NM_NBSE_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_999
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL NM_NBSE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(NM_NBSE_HBASE,
     &                   NM_NBSE_DTR)

      NM_NBSE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_NBSE_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_CTR
CDEC$ ENDIF

      USE NM_NBSE_M
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR, IRET
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   NM_NBSE_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = NM_NBSE_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. NM_NBSE_HVALIDE(HOBJ))
      
      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      NM_NBSE_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_NBSE_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_DTR
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = NM_NBSE_RST(HOBJ)

C---     Dé-alloue
      SELF => NM_NBSE_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, NM_NBSE_HBASE)

      NM_NBSE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NBSE_INI(HOBJ, HFPRT, HFRNM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_INI
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HFPRT
      INTEGER HFRNM

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'sofunc.fi'

      INTEGER IERR
      INTEGER I_ERROR
      LOGICAL I_INIT
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFPRT))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFRNM))
C------------------------------------------------------------------------

C---     Contrôle les paramètres
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (.NOT. I_INIT) GOTO 9900

C---     Reset les données
      IERR = NM_NBSE_RST(HOBJ)

C---     Assigne les functions call_back
      SELF => NM_NBSE_REQSELF(HOBJ)
      SELF%HFPRT = HFPRT
      SELF%HFRNM = HFRNM

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_MPI_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_NBSE_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NBSE_RAZ(HOBJ)

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fc'

      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)

C---     Reset
      SELF%HFPRT = 0
      SELF%HFRNM = 0
      SELF%LDSTR = 0
      SELF%NNT   = 0
      SELF%NPRTG = 0
      SELF%NPRTL = 0
      SELF%IPRTL = 0

      NM_NBSE_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NBSE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_RST
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER LDSTR
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      LDSTR = SELF%LDSTR

C---     Désalloue la mémoire
      IF (LDSTR .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSTR)

C---     Remise à  zéro
      IERR = NM_NBSE_RAZ(HOBJ)

      NM_NBSE_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_NBSE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_REQHBASE
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmnbse.fc'
C------------------------------------------------------------------------

      NM_NBSE_REQHBASE = NM_NBSE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_NBSE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_HVALIDE
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_NBSE_HVALIDE = OB_OBJN_HVALIDE(HOBJ, NM_NBSE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_NBSE_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_TVALIDE
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnbse.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_NBSE_TVALIDE = OB_OBJN_TVALIDE(HOBJ, NM_NBSE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Partitionne un maillage.
C
C Description:
C     La fonction NM_NBSE_PART partitionne le maillage donné par HGRID en NPROC
C     sous-domaines. Si NPROC .LE. 0, alors on partitionne pour le nombre
C     de process dans le groupe MPI (cluster).
C     <p>
C     La partition n'est pas utilisable immédiatement car il n'y a pas
C     redistribution des données entre les process. La redistribution a lieu au
C     chargement des données. La partition doit donc être sauvée puis chargée.
C     Le maillage devra également être relu.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur les connectivités du maillage à partitionner
C     NPART       Nombre de sous-domaines demandés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_PART (HOBJ, HELEM, NPART)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_PART
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER NPART

      INCLUDE 'nmnbse.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER I_ERROR, I_SIZE
      LOGICAL I_INIT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.part')

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.part'

C---     Contrôle les paramètres
      IF (.NOT. DT_ELEM_HVALIDE(HELEM)) GOTO 9900
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (.NOT. I_INIT)     GOTO 9901

C---     Nombre de sous_domaines
      IF (NPART .LE. 0) THEN
         CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
         IF (ERR_GOOD()) NPART = I_SIZE
      ENDIF
      
C---     Log
      WRITE(LOG_BUF, '(2A,I6)') 'MSG_NBR_SOUS_DOMAINES#<35>#', ': ',
     &                           NPART
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Exécute le partitionnement
      IF (ERR_GOOD()) IERR = NM_NBSE_PA_XEQ(HOBJ, HELEM, NPART)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_ELEM_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HELEM, 'MSG_ELEM')
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_MPI_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.part')
      NM_NBSE_PART = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote un maillage.
C
C Description:
C     La fonction NM_NBSE_RENUM renumérote le maillage HELEM afin d'en
C     réduire la largeur de bande.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur les connectivités du maillage à renuméroter
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_RENUM (HOBJ, HELEM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_RENUM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER I_ERROR
      LOGICAL I_INIT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.grid.renum')

C---     Contrôle les paramètres
      IF (.NOT. DT_ELEM_HVALIDE(HELEM)) GOTO 9900
      CALL MPI_INITIALIZED(I_INIT, I_ERROR)
      IF (.NOT. ERR_GOOD()) GOTO 9999
      IF (.NOT. I_INIT)     GOTO 9901

C---     Renumérote
      IF (ERR_GOOD()) IERR = NM_NBSE_RN_XEQ(HOBJ, HELEM)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CONNEC_MAILLAGE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_MPI_NON_INITIALISE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.grid.renum')
      NM_NBSE_RENUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Écris la table de redistribution.
C
C Description:
C     La fonction NM_NBSE_SAUVE écris la table de redistribution dans le
C     fichier NOMFIC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_SAUVE(HOBJ, NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_SAUVE
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER HFIC
      INTEGER LDSTR
      INTEGER NNT, NPRTG, NPRTL, IPRTL
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      LDSTR = SELF%LDSTR
      NNT   = SELF%NNT  
      NPRTG = SELF%NPRTG
      NPRTL = SELF%NPRTL
      IPRTL = SELF%IPRTL

C---     Contrôles
      IF (LDSTR .EQ. 0) GOTO 9900
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9901

C---     Crée et initialise l'objet fichier
      HFIC = 0
      IF (ERR_GOOD()) IERR = FD_NUMR_CTR(HFIC)
      IF (ERR_GOOD())
     &   IERR = FD_NUMR_INI(HFIC,
     &                      NOMFIC,
     &                      IO_MODE_ECRITURE + IO_MODE_ASCII)

C---     Écris
      IF (ERR_GOOD())
     &   IERR = FD_NUMR_ECRIS(HFIC,
     &                        NNT,
     &                        NPRTG,
     &                        NPRTL,
     &                        IPRTL,
     &                        KA(SO_ALLC_REQKIND(KA,LDSTR)))

C---     Détruis l'objet fichier
      IF (FD_NUMR_HVALIDE(HFIC)) IERR = FD_NUMR_DTR(HFIC)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_OBJET_NON_CHARGE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_NBSE_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis une renumérotation.
C
C Description:
C     La fonction NM_NBSE_GENNUM retourne un renumérotation globale
C     nouvellement créée à partir des données de l'objet. Il est de
C     la responsabilité de l'utilisateur de disposer de HNUM.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HNUM        Handle sur la renumérotation crée
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_GENNUM(HOBJ, HNUM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_GENNUM
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'nmlcgl.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER I_COMM, I_RANK, I_SIZE, I_ERROR
      INTEGER NNA, NNL, NNT, NPRTG, NPRTL, IPRTL
      INTEGER LDSTR, L_ROW
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      LDSTR = SELF%LDSTR
      NNT   = SELF%NNT  
      NPRTG = SELF%NPRTG
      NPRTL = SELF%NPRTL
      IPRTL = SELF%IPRTL

C---     Paramètres MPI
      CALL MPI_COMM_SIZE(MP_UTIL_REQCOMM(), I_SIZE, I_ERROR)
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     Contrôles
      IF (NM_NUMR_CTRLH(HNUM)) GOTO 9900
      IF (NPRTG .NE. I_SIZE)   GOTO 9901
      IF (NPRTL .NE. 1)        GOTO 9902
      IF (IPRTL .NE. I_RANK+1) GOTO 9903

C---     Calcul le nombre de noeuds locaux
      IF (ERR_GOOD())
     &   IERR = NM_NBSE_PA_NNL(NNL,
     &                         NNT,
     &                         NPRTL,
     &                         KA(SO_ALLC_REQKIND(KA,LDSTR)))

C---     Alloue la mémoire
      L_ROW = 0
      NNA = MAX(1,NNL)     ! Pour faire exister la table
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNA*(NPRTG+2), L_ROW)

C---     Remplis la table en format ligne
      IF (ERR_GOOD())
     &   IERR = NM_NBSE_PA_C2R(NNL,
     &                         NNT,
     &                         NPRTG,
     &                         NPRTL,
     &                         IPRTL,
     &                         KA(SO_ALLC_REQKIND(KA,LDSTR)),
     &                         KA(SO_ALLC_REQKIND(KA,L_ROW)))

C---     Crée et initialise la renumérotation
      HNUM = 0
      IF (ERR_GOOD()) IERR = NM_LCGL_CTR(HNUM)
      IF (ERR_GOOD()) IERR = NM_LCGL_INI(HNUM,
     &                                   ' ',
     &                                   NNL,
     &                                   NNT,
     &                                   NPRTG,
     &                                   L_ROW)

C---     Écris
      IF (ERR_GOOD())
     &   IERR = NM_LCGL_CHARGE(HNUM)

C---     Récupère la mémoire
      IF (L_ROW .NE. 0) IERR = SO_ALLC_ALLINT(0, L_ROW)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_NULL_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A,I3,A,I3)') 'ERR_PART_CLUSTER_INCOHERENTS: ',
     &      NPRTG, '/', I_SIZE
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,I3)') 'ERR_NBR_SDOM_INVALIDE: ', NPRTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      CALL ERR_AJT('ERR_UN_SEUL_SOUSDOM_PAR_PROCESS_ATTENDU')
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,I3)') 'ERR_: ', IPRTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_NBSE_GENNUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne la table de distribution.
C
C Description:
C     La fonction NM_NBSE_ASGDSTR permet d'assigner directement une
C     table de distribution. L'objet courant prend la propriété de la table
C     et s'occupera de désallouer la mémoire.
C     La table doit être aux dimensions (NPRTL+1, NNT), donc en format
C     colonne.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NPRTG       Nombre global de sous-domaines
C     NPRTL       Nombre local  de sous-domaines
C     IPRTL       Process local
C     NNT         Nombre de noeuds
C     LDSTR       Pointeur à la table
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_ASGDSTR(HOBJ, NPRTG, NPRTL, IPRTL, NNT, LDSTR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NBSE_ASGDSTR
CDEC$ ENDIF

      USE NM_NBSE_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRTG
      INTEGER NPRTL
      INTEGER IPRTL
      INTEGER NNT
      INTEGER LDSTR

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fc'

      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPRTG .GT. 0)
D     CALL ERR_PRE(NPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL .LE. NPRTG)
D     CALL ERR_PRE(NNT   .GT. 0)
D     CALL ERR_PRE(LDSTR .NE. 0)
C-----------------------------------------------------------------------

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => NM_NBSE_REQSELF(HOBJ)
         SELF%LDSTR = LDSTR
         SELF%NNT   = NNT
         SELF%NPRTG = NPRTG
         SELF%NPRTL = NPRTL
         SELF%IPRTL = IPRTL
      ENDIF

      NM_NBSE_ASGDSTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute le partitionnement.
C
C Description:
C     La méthode privée NM_NBSE_PA_XEQ exécute le partitionnement du maillage
C     HGRID en NPRTG sous-domaines. Elle partitionne puis renumérote les noeuds
C     de chaque sous-domaine. En sortie, la table de distribution est montée
C     pour les sous-domaines assignés au process courant.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HELEM       Handle sur les connectivités du maillage
C     NPRTG       Nombre global de sous-domaines
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_PA_XEQ(HOBJ, HELEM, NPRTG)

      USE NM_NBSE_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER NPRTG

      INCLUDE 'nmnbse.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER IERR, IRET
      INTEGER IP, IPL
      INTEGER II, JJ, IDEB, IFIN
      INTEGER I_RANK, I_SIZE, I_ERROR, I_PROC, I_COMM
      INTEGER HNUMC, HNUME, HFPRT, H_ELE
      INTEGER LELE, LDSTR, L_TRV
      INTEGER NNEL, NELT, NNT, NPRTL, NDUM
      INTEGER*8 IND
      REAL*8  TNEW
      LOGICAL MODIF
      INTEGER, ALLOCATABLE :: KLOAD(:)

      INTEGER IIAMAX
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_NBSE_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HELEM))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Zone de log
      LOG_ZNE = 'h2d2.grid.part'

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      I_PROC = I_RANK + 1

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      HFPRT = SELF%HFPRT
      LDSTR = SELF%LDSTR

C---     Contrôles
      IF (SELF%NPRTG .GT. 0) GOTO 9900

C---     Construis des connectivités globales
      H_ELE = 0
      IF (ERR_GOOD()) THEN
         IERR = DT_ELEM_CTR   (H_ELE)
         IERR = DT_ELEM_INIFIC(H_ELE,
     &                         DT_ELEM_REQNOMF(HELEM),
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)
      ENDIF

C---     Récupère les dimensions du maillage
      IF (ERR_GOOD()) THEN
         NNEL = DT_ELEM_REQNNEL(H_ELE)
         NELT = DT_ELEM_REQNELT(H_ELE)
      ENDIF

C---     Lis les connectivités avec des num. identité
      HNUMC = 0
      HNUME = 0
      IF (ERR_GOOD()) THEN
         TNEW = 0.0D0
         NDUM = NELT*NNEL   ! Surdimensionné, devrait être NNT
         IERR = NM_IDEN_CTR   (HNUMC)
         IERR = NM_IDEN_INI   (HNUMC, NDUM)
         IERR = NM_IDEN_CTR   (HNUME)
         IERR = NM_IDEN_INI   (HNUME, NELT)
         IERR = DT_ELEM_CHARGE(H_ELE, HNUMC, HNUME, TNEW, MODIF)
         IERR = NM_IDEN_DTR   (HNUME)
         IERR = NM_IDEN_DTR   (HNUMC)
      ENDIF

C---     Récupère la table des connectivités
      LELE = 0
      IF (ERR_GOOD()) THEN
         LELE = DT_ELEM_REQLELE(H_ELE)
      ENDIF

C---     Calcul le nombre de noeuds total
      IF (ERR_GOOD()) THEN
         NNT = IIAMAX(NNEL*NELT, KA(SO_ALLC_REQKIND(KA,LELE)), 1)
         NNT = SO_ALLC_REQIN4(LELE, NNT)
      ENDIF

C---     Dimensionne la table de travail
      L_TRV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNT, L_TRV)

C---     Partitionne - distribue les noeuds
      IF (ERR_GOOD()) THEN
         IRET = SO_FUNC_CALL7(HFPRT,
     &                        SO_ALLC_CST2B(I_COMM),
     &                        SO_ALLC_CST2B(NPRTG),
     &                        SO_ALLC_CST2B(NNT),
     &                        SO_ALLC_CST2B(L_TRV,0_2),
     &                        SO_ALLC_CST2B(NNEL),
     &                        SO_ALLC_CST2B(NELT),
     &                        SO_ALLC_CST2B(LELE,0_2))
         IF (IRET .NE. 0) THEN
            WRITE(ERR_BUF, '(A)') 'ERR_PARTIONNEMENT'
            CALL ERR_ASG(ERR_ERR, ERR_BUF)
         ENDIF
         IERR = MP_UTIL_SYNCERR()
      ENDIF

C---     Distribue les sous-domaines sur les process
      II = NPRTG / I_SIZE
      JJ = MOD(NPRTG, I_SIZE)
      IDEB = II*(I_PROC-1) + MIN(I_PROC-1, JJ) + 1
      IFIN = II*(I_PROC+0) + MIN(I_PROC-1, JJ)
      IF (I_PROC .LE. JJ) THEN
         IFIN = IFIN + 1
      ENDIF
      NPRTL = IFIN - IDEB + 1

C---     Log
      IF (ERR_GOOD()) THEN
         ALLOCATE(KLOAD(NPRTG), STAT=IERR)
         KLOAD(:) = 0
         DO IP=IDEB, IFIN
            IND = SO_ALLC_REQKIND(KA, L_TRV)
            KLOAD(IP) = COUNT(KA(IND:IND+NNT) .EQ. IP)
         ENDDO

         IF (I_RANK .EQ. I_MASTER) THEN
            CALL MPI_REDUCE(MPI_IN_PLACE, KLOAD, NPRTG,
     &                      MP_TYPE_INT(),
     &                      MPI_MAX, I_MASTER, I_COMM, I_ERROR)
         ELSE
            CALL MPI_REDUCE(KLOAD, NDUM, NPRTG,
     &                      MP_TYPE_INT(),
     &                      MPI_MAX, I_MASTER, I_COMM, I_ERROR)
         ENDIF

         IF (I_RANK .EQ. I_MASTER) THEN
            WRITE(LOG_BUF,'(2A,I12)') 'MSG_NNT#<35>#', ': ', NNT
            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            DO IP=1, NPRTG
               NDUM = KLOAD(IP)
               WRITE(LOG_BUF,'(2A,I6,I12,A,F6.2,A)')
     &            'MSG_SOUS_DOMAINE_NBR_NOEUD#<35>#', ': ', 
     &            IP, NDUM, ' (', 100*DBLE(NDUM)/DBLE(NNT), '%)'
               CALL LOG_INFO(LOG_ZNE, LOG_BUF)
            ENDDO
         ENDIF

         IF (ALLOCATED(KLOAD)) DEALLOCATE(KLOAD, STAT=IRET)
      ENDIF

C---     Dimensionne la table de distribution
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT((NPRTL+1)*NNT, LDSTR)

C---     Copie la distribution des noeuds
      IF (ERR_GOOD())
     &   CALL ICOPY(NNT, KA(SO_ALLC_REQKIND(KA,L_TRV)), 1,
     &                   KA(SO_ALLC_REQKIND(KA,LDSTR)+NPRTL), NPRTL+1)

C---     Noeuds locaux - renumérotation
      DO IP=IDEB, IFIN
         IF (ERR_GOOD()) THEN
            IERR = NM_NBSE_PA_RNM(HOBJ,
     &                            NNT,
     &                            NNEL,
     &                            NELT,
     &                            KA(SO_ALLC_REQKIND(KA, LELE)),
     &                            KA(SO_ALLC_REQKIND(KA, L_TRV)),
     &                            IP,
     &                            IP-IDEB+1,
     &                            NPRTL,
     &                            KA(SO_ALLC_REQKIND(KA, LDSTR)))
         ENDIF
      ENDDO

C---     Récupère la mémoire
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)

C---     Détruis le maillage
      IF (DT_ELEM_HVALIDE(H_ELE)) THEN
         IERR = DT_ELEM_DTR(H_ELE)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%LDSTR = LDSTR
         SELF%NNT   = NNT
         SELF%NPRTG = NPRTG
         SELF%NPRTL = NPRTL
         SELF%IPRTL = IDEB
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALGO_DEJA_UTILISE')
      CALL ERR_AJT('MSG_REFUSE_LACHEMENT_DE_PROCEDER')
      GOTO 9999

9999  CONTINUE
      NM_NBSE_PA_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Monte la table de renumérotation locale pour un sous-domaine.
C
C Description:
C     La méthode privée NM_NBSE_PA_RNM monte la table de renumérotation
C     locale. En entrée, la table KDIST contient dans la colonne (NPRTL+1)
C     pour chaque noeud le numéro du sous-domaine auquel il appartient.
C     Pour le sous-domaine de numéro global IPRTG et de numéro local IPRTL,
C     elle renumérote les noeuds du sous-domaine, ce qui comprend les noeuds
C     des éléments touchés. En sortie, KDIST(IPRTL,*) contient pour chaque
C     noeud son numéro local:
C     <p>
C           = 0   noeud absent du process
C           > 0   noeud du process
C
C Entrée:
C     INTEGER HOBJ            HANDLE SUR l'OBJET COURANT
C     INTEGER NNT             NOMBRE DE NOEUDS TOTAL
C     INTEGER NNEL            NOMBRE DE NOEUDS PAR ELEMENT
C     INTEGER NELT            NOMBRE D'ELEMENTS TOTAL
C     INTEGER KNG(NNEL, NELT) TABLE DES CONNECTIVITES
C     INTEGER KTRV(NNT)       TABLE DE TRAVAIL
C     INTEGER IPRTG           INDICE GLOBAL DU SOUS-DOMAINE
C     INTEGER IPRTL           INDICE LOCAL DU SOUS-DOMAINE
C     INTEGER NPRTL           NOMBRE DE SOUS-DOMAINE LOCAUX
C
C Sortie:
C     INTEGER KDIST           TABLE DE DISTRIBUTION DES SOUS-DOMAINES LOCAUX
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_PA_RNM(HOBJ,
     &                        NNT,
     &                        NNEL,
     &                        NELT,
     &                        KNG,
     &                        KTRV,
     &                        IPRTG,
     &                        IPRTL,
     &                        NPRTL,
     &                        KDIST)

      USE NM_NBSE_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NNT
      INTEGER NNEL
      INTEGER NELT
      INTEGER KNG  (NNEL, NELT)
      INTEGER KTRV (NNT)
      INTEGER IPRTG
      INTEGER IPRTL
      INTEGER NPRTL
      INTEGER KDIST(NPRTL+1, NNT)

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER IE, ID, IN
      INTEGER IDSTP
      INTEGER I_COMM
      INTEGER HFRNM
      INTEGER L_RNM
      INTEGER NNL
      LOGICAL ESTLOCAL
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT  .GT. 0)
D     CALL ERR_PRE(NNEL .GT. 0)
D     CALL ERR_PRE(NELT .GT. 0)
D     CALL ERR_PRE(IPRTL .LE. IPRTG)
D     CALL ERR_PRE(IPRTL .LE. NPRTL)
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      HFRNM = SELF%HFRNM

C---     Initialise les tables
      CALL IINIT(NNT, 0, KDIST(IPRTL,1), NPRTL+1)
      CALL IINIT(NNT, 0, KTRV, 1)

C---     Flag tous les noeuds des éléments touchés
      IDSTP = NPRTL+1      ! Indice du numéro de sous-domaine
      DO IE=1, NELT
         ESTLOCAL = .FALSE.
         DO ID=1, NNEL
            IN = KNG(ID,IE)
            IF (KDIST(IDSTP, IN) .EQ. IPRTG) THEN
               ESTLOCAL = .TRUE.
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE

         IF (ESTLOCAL) THEN
            DO ID=1, NNEL
               IN = KNG(ID,IE)
               KDIST(IPRTL, IN) = 1
            ENDDO
         ENDIF
      ENDDO

C---     Numérotation locale naturelle
      NNL = 0
      DO IN=1,NNT
         IF (KDIST(IPRTL, IN) .NE. 0) THEN
            NNL = NNL + 1
            KDIST(IPRTL, IN) = NNL
         ENDIF
      ENDDO

C---     KNG des éléments locaux en numérotation locale
      DO IE=1,NELT
         ESTLOCAL = .TRUE.
         DO ID=1,NNEL
            IN = KNG(ID,IE)
            IF (KDIST(IPRTL, IN) .EQ. 0) THEN
               ESTLOCAL = .FALSE.
               GOTO 119
            ENDIF
         ENDDO
119      CONTINUE

         IF (ESTLOCAL) THEN
            DO ID=1, NNEL     ! ELEM LOCAL --> KNG POSITIVES
               IN = KNG(ID,IE)
D              CALL ERR_ASR(KDIST(IPRTL, IN) .GE. 1)
D              CALL ERR_ASR(KDIST(IPRTL, IN) .LE. NNL)
               KNG(ID,IE) = KDIST(IPRTL, IN)
               KTRV(KDIST(IPRTL, IN)) = IN
            ENDDO
         ELSE
            DO ID=1, NNEL     ! ELEM NON LOCAL --> KNG NEGATIVES
               IN = KNG(ID,IE)
D              CALL ERR_ASR(IN .GE. 1)
D              CALL ERR_ASR(IN .LE. NNT)
               KNG(ID,IE) = -IN
            ENDDO
         ENDIF
      ENDDO

C---     Alloue la mémoire pour la table de renumérotation
      L_RNM = 0
      IERR = SO_ALLC_ALLINT(NNL, L_RNM)

C---     Renumérote
      IF (ERR_GOOD()) THEN
         I_COMM = MP_UTIL_REQCOMM()
         IERR = SO_FUNC_CALL6(HFRNM,
     &                        SO_ALLC_CST2B(I_COMM),
     &                        SO_ALLC_CST2B(NNL),
     &                        SO_ALLC_CST2B(L_RNM,0_2),
     &                        SO_ALLC_CST2B(NNEL),
     &                        SO_ALLC_CST2B(NELT),
     &                        SO_ALLC_CST2B(KNG(:,1)))
      ENDIF

C---     Re-balance KNG en numérotation globale
      IF (ERR_GOOD()) THEN
         DO IE=1,NELT
            DO ID=1,NNEL
               IN = KNG(ID,IE)
               IF (IN .LE. 0) THEN
                  KNG(ID,IE) = -IN
               ELSE
D                 CALL ERR_ASR(KTRV(IN) .GT. 0)
D                 CALL ERR_ASR(KTRV(IN) .LE. NNT)
                  KNG(ID,IE) = KTRV(IN)
               ENDIF
            ENDDO
         ENDDO
      ENDIF

C---     Combine les deux tables
      IF (ERR_GOOD()) THEN
         NNL = 0
         DO IN=1,NNT
            IF (KDIST(IPRTL, IN) .GT. 0) THEN
               NNL = NNL + 1
               KDIST(IPRTL, IN) = SO_ALLC_REQIN4(L_RNM, NNL)
            ENDIF
         ENDDO
      ENDIF

C---     Récupère la mémoire
      IF (L_RNM .NE. 0) IERR = SO_ALLC_ALLINT(0, L_RNM)

      NM_NBSE_PA_RNM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NM_NBSE_PA_NNL
C
C Description:
C     La fonction privée NM_NBSE_PA_NNL retourne le nombre de noeuds locaux
C     pour le process courant. Elle n'est valable que dans le cas ou le nombre
C     de sous-domaines égale le nombre de process.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_PA_NNL(NNL,
     &                        NNT,
     &                        NPRTL,
     &                        KDIST)

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NNT
      INTEGER NPRTL
      INTEGER KDIST(NPRTL+1, NNT)

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fc'

      INTEGER IERR
      INTEGER IN, IP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GT. 0)
D     CALL ERR_PRE(NPRTL .EQ. 1)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     COMPTE LES NNL
      NNL = 0
      DO IN=1,NNT
         IF (KDIST(1, IN) .GT. 0) NNL = NNL + 1
      ENDDO

      NM_NBSE_PA_NNL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NM_NBSE_PA_C2R
C
C Description:
C     La fonction NM_NBSE_PA_C2R passe d'une table de renumérotation
C     par colonne en une table des lignes pour un process.
C
C Entrée:
C     NNL         Nombre de noeuds local
C     NNT         Nombre de noeuds total
C     NPRTG       Nombre de partitions global
C     NPRTL       Nombre de partitions local
C     IPRTL       Partition locale à traiter
C     KCOL        Table de renumérotation par colonne
C
C Sortie:
C     KROW        Table de renumérotation des lignes du process IPRTL
C
C Notes:
C     La localisation des points dans un maillage peut comporter moins de
C     points détectés dans le maillage que de points soumis.
C************************************************************************
      FUNCTION NM_NBSE_PA_C2R(NNL,
     &                        NNT,
     &                        NPRTG,
     &                        NPRTL,
     &                        IPRTL,
     &                        KCOL,
     &                        KROW)

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NNT
      INTEGER NPRTG
      INTEGER NPRTL
      INTEGER IPRTL
      INTEGER KCOL(NPRTL+1, NNT)
      INTEGER KROW(NPRTG+2, NNL)

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnbse.fc'

D     INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER IERR
      INTEGER IDUM
      INTEGER I_PROC, I_RANK, I_SIZE, I_ERROR, I_COMM
      INTEGER IN, ID, IL, I
      INTEGER IGLB, IPRC
      INTEGER NPROC_MAX
      PARAMETER (NPROC_MAX = 256)
      INTEGER KTMP(NPROC_MAX)
      INTEGER KRECV(NPROC_MAX), KDSPL(NPROC_MAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNT   .GT. 0)
D     CALL ERR_PRE(NPRTG .GT. 0)
D     CALL ERR_PRE(NPRTL .GT. 0)
D     CALL ERR_PRE(IPRTL .GT. 0)
D     CALL ERR_PRE(NPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL .LE. NPRTG)
D     CALL ERR_PRE(IPRTL+NPRTL-1 .LE. NPRTG)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôle
      IF (NPRTG+2 .GT. NPROC_MAX) GOTO 9900

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
      I_PROC = I_RANK + 1

C---     Si on a un seul process
      IF (I_SIZE .GT. 1) GOTO 1000
D        CALL ERR_ASR(NNL   .LE. NNT)           ! c.f. Note
D        CALL ERR_ASR(NPRTL .EQ. NPRTG)
         IL = 0
         DO IN=1,NNT
            IF (KCOL(1,IN) .GT. 0) THEN
               IL = IL + 1
               KROW(1,IL) = KCOL(1,IN)
               KROW(2,IL) = IN
            ENDIF
         ENDDO
         CALL IINIT(NNL, 1, KROW(3,1), NPRTG+2) ! IPROC
      GOTO 1999

C---     En multi-process
1000  CONTINUE

C---     Indices
      IGLB = NPRTG + 1     ! NNO global
      IPRC = NPRTG + 2     ! No du process propriétaire

C---     Broadcast NNT comme contrôle que tous ont le même
D     IF (ERR_GOOD()) THEN
D        IF (I_RANK .EQ. I_MASTER) IDUM = NNT
D        CALL MPI_BCAST(IDUM, 1, MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
D        CALL ERR_ASR(IDUM .EQ. NNT)
D     ENDIF

C---     Structures pour le GATHERV
      IF (ERR_GOOD()) CALL MPI_ALLGATHER(NPRTL, 1, MP_TYPE_INT(),
     &                                   KRECV, 1, MP_TYPE_INT(),
     &                                   I_COMM, I_ERROR)
      IF (ERR_GOOD()) CALL MPI_ALLGATHER(IPRTL-1, 1, MP_TYPE_INT(),
     &                                   KDSPL, 1, MP_TYPE_INT(),
     &                                   I_COMM, I_ERROR)

C---     Boucle sur les noeuds globaux
      IL = 0
      DO IN=1,NNT
         IF (ERR_BAD()) GOTO 399

C---        Gather une ligne
         CALL MPI_ALLGATHERV(KCOL(1,IN), NPRTL, MP_TYPE_INT(),
     &                       KTMP, KRECV, KDSPL, MP_TYPE_INT(),
     &                       I_COMM, I_ERROR)

C---        Un noeud local
         IF (KTMP(I_PROC) .GT. 0) THEN
            IL = IL + 1
D           CALL ERR_ASR(IL .LE. NNL)
            CALL ICOPY(NPRTG, KTMP, 1, KROW(1,IL), 1)
            KROW(IGLB,IL) = IN
            KROW(IPRC,IL) = KCOL(NPRTL+1, IN)
         ENDIF

      ENDDO
399   CONTINUE

1999  CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_NBSE_PA_C2R = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Exécute la renumérotation des noeuds d'un maillage.
C
C Description:
C     La méthode privée NM_NBSE_RN_XEQ monte la table de renumérotation
C     des noeuds du maillage HGRID. En sortie, KRNO contient pour
C     chaque noeud son nouveau numéro.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HELEM       Handle sur les connectivités du maillage
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NBSE_RN_XEQ(HOBJ, HELEM)

      USE NM_NBSE_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM

      INCLUDE 'nmnbse.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtelem.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'nmnbse.fc'

      REAL*8  TNEW
      INTEGER IERR
      INTEGER I_COMM
      INTEGER HNUMC, HNUME, HFRNM, H_ELE
      INTEGER LELE, LDSTR, L_TRV
      INTEGER NNEL, NELT, NDUM, NNT
      LOGICAL MODIF

      INTEGER IIAMAX
      LOGICAL ESTINIFIC
      TYPE (NM_NBSE_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_ELEM_HVALIDE(HELEM))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => NM_NBSE_REQSELF(HOBJ)
      HFRNM = SELF%HFRNM
      LDSTR = SELF%LDSTR

C---     Contrôles
      IF (SELF%NPRTG .GT. 0) GOTO 9900

C---     Détecte si le maillage de base est uniquement en mémoire
      H_ELE = HELEM
      ESTINIFIC = DT_ELEM_ESTINIFIC(HELEM)
      IF (.NOT. ESTINIFIC) GOTO 2000

C---------------------------------
1000  CONTINUE
C---------------------------------

C---     Construis un nouveau maillage global de travail
      H_ELE = 0
      IF (ERR_GOOD()) THEN
         IERR = DT_ELEM_CTR   (H_ELE)
         IERR = DT_ELEM_INIFIC(H_ELE,
     &                         DT_ELEM_REQNOMF(HELEM),
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)
      ENDIF

C---     Récupère les dimensions du maillage
      IF (ERR_GOOD()) THEN
         NNEL = DT_ELEM_REQNNEL(H_ELE)
         NELT = DT_ELEM_REQNELT(H_ELE)
      ENDIF

C---     Lis tout le maillage avec une num. identité
      HNUMC = 0
      HNUME = 0
      TNEW  = 0.0D0
      NDUM = NELT*NNEL   ! Surdimensionné, devrait être NNT
      IF (ERR_GOOD()) IERR=NM_IDEN_CTR   (HNUMC)
      IF (ERR_GOOD()) IERR=NM_IDEN_INI   (HNUMC, NDUM)
      IF (ERR_GOOD()) IERR=NM_IDEN_CTR   (HNUME)
      IF (ERR_GOOD()) IERR=NM_IDEN_INI   (HNUME, NELT)
      IF (ERR_GOOD()) IERR=DT_ELEM_CHARGE(H_ELE,HNUMC,HNUME,TNEW,MODIF)
      IF (NM_IDEN_HVALIDE(HNUME)) IERR = NM_IDEN_DTR(HNUME)
      IF (NM_IDEN_HVALIDE(HNUMC)) IERR = NM_IDEN_DTR(HNUMC)

C---------------------------------
2000  CONTINUE
C---------------------------------

C---     Récupère les données du maillage
      IF (ERR_GOOD()) THEN
         NNEL = DT_ELEM_REQNNEL(H_ELE)
         NELT = DT_ELEM_REQNELT(H_ELE)
         LELE = DT_ELEM_REQLELE(H_ELE)
      ENDIF

C---     Calcul le nombre de noeuds
      IF (ERR_GOOD()) THEN
         NNT = IIAMAX(NNEL*NELT, KA(SO_ALLC_REQKIND(KA,LELE)), 1)
         NNT = SO_ALLC_REQIN4(LELE, NNT)
      ENDIF

C---     Dimensionne la table de travail
      L_TRV = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNT, L_TRV)

C---     Renumérote les noeuds
      IF (ERR_GOOD()) THEN
         I_COMM = MP_UTIL_REQCOMM()
         IERR = SO_FUNC_CALL6(HFRNM,
     &                        SO_ALLC_CST2B(I_COMM),
     &                        SO_ALLC_CST2B(NNT),
     &                        SO_ALLC_CST2B(L_TRV,0_2),
     &                        SO_ALLC_CST2B(NNEL),
     &                        SO_ALLC_CST2B(NELT),
     &                        SO_ALLC_CST2B(LELE,0_2))
      ENDIF

C---     Dimensionne la table de distribution
      IF (ERR_GOOD()) THEN
         IERR = SO_ALLC_ALLINT(2*NNT, LDSTR)
         CALL ICOPY(NNT, KA(SO_ALLC_REQKIND(KA,L_TRV)), 1,
     &                   KA(SO_ALLC_REQKIND(KA,LDSTR)), 2)
      ENDIF

C---     Récupère la mémoire
      IF (L_TRV .NE. 0) IERR = SO_ALLC_ALLINT(0, L_TRV)

C---     Au besoin, détruis le maillage de travail
      IF (ESTINIFIC .AND. DT_ELEM_HVALIDE(H_ELE)) THEN
         IERR = DT_ELEM_DTR(H_ELE)
      ENDIF

C---     Stoke les attributs
      IF (ERR_GOOD()) THEN
         SELF%LDSTR = LDSTR
         SELF%NNT   = NNT
         SELF%NPRTG = 1
         SELF%NPRTL = 1
         SELF%IPRTL = 1
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALGO_DEJA_UTILISE')
      CALL ERR_AJT('MSG_REFUSE_LACHEMENT_DE_PROCEDER')
      GOTO 9999

9999  CONTINUE
      NM_NBSE_RN_XEQ = ERR_TYP()
      RETURN
      END
