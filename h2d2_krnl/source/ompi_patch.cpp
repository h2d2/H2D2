//************************************************************************
// --- Copyright (c) INRS 2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Redéfinition des fonctions:
//           MPI_FILE_GET_POSITION
//           MPI_FILE_GET_SIZE
//           MPI_FILE_GET_VIEw
//           MPI_TYPE_EXTENT
//
// Description:
//      La fonction MPI_FILE_GET_SIZE de Open-MPI ne fournit pas
//      un résultat correct pour des fichiers dont la dimension est
//      supérieure à 2GB. Elle cast le résultat en MPI_Fint qui par défaut
//      est 32 bits.
//      http://www.open-mpi.org/community/lists/devel/2010/12/8805.php
//
// Notes:
//      Il manque:
//          mpi_address
//          mpi_file_get_position_shared
//       Les fonctions sont redéfinies pour toutes les librairies MPI
//       par simplicité. L'alternative est de refaire appel au Fortran
//       dont on ne dispose pas des prototypes ...
//************************************************************************

#include "cconfig.h"

#include "mpi.h"
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define MP_UTIL_FILE_GET_POSITION  MP_UTIL_FILE_GET_POSITION
#  define MP_UTIL_FILE_GET_SIZE      MP_UTIL_FILE_GET_SIZE
#  define MP_UTIL_FILE_GET_VIEW      MP_UTIL_FILE_GET_VIEW
#  define MP_UTIL_TYPE_GET_EXTENT    MP_UTIL_TYPE_GET_EXTENT
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define MP_UTIL_FILE_GET_POSITION  mp_util_file_get_position_
#  define MP_UTIL_FILE_GET_SIZE      mp_util_file_get_size_
#  define MP_UTIL_FILE_GET_VIEW      mp_util_file_get_view_
#  define MP_UTIL_TYPE_GET_EXTENT    mp_util_type_get_extent_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define MP_UTIL_FILE_GET_POSITION  mp_util_file_get_position__
#  define MP_UTIL_FILE_GET_SIZE      mp_util_file_get_size__
#  define MP_UTIL_FILE_GET_VIEW      mp_util_file_get_view__
#  define MP_UTIL_TYPE_GET_EXTENT    mp_util_type_get_extent__
#endif


F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL MP_UTIL_FILE_GET_POSITION (fint_t*, int64_t*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL MP_UTIL_FILE_GET_SIZE     (fint_t*, int64_t*, fint_t*);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL MP_UTIL_FILE_GET_VIEW     (fint_t*, int64_t*, fint_t*, fint_t*, F2C_CONF_STRING, fint_t* F2C_CONF_SUP_INT);
F2C_CONF_DLL_EXPORT void F2C_CONF_CNV_APPEL MP_UTIL_TYPE_GET_EXTENT   (fint_t*, int64_t*, int64_t*, fint_t*);


#ifdef __cplusplus
}
#endif


F2C_CONF_DLL_EXPORT void MP_UTIL_FILE_GET_POSITION(fint_t* i_file, int64_t* i_size, fint_t* i_err)
{
    MPI_File   c_file = MPI_File_f2c(*i_file);
    MPI_Offset c_size;

    int c_err = MPI_File_get_position(c_file, &c_size);

    if (MPI_SUCCESS == c_err)
    {
        *i_size = (int64_t) c_size;
    }
    *i_err = (fint_t) c_err;
}

F2C_CONF_DLL_EXPORT void MP_UTIL_FILE_GET_SIZE(fint_t* i_file, int64_t* i_size, fint_t* i_err)
{
    MPI_File   c_file = MPI_File_f2c(*i_file);
    MPI_Offset c_size;

    int c_err = MPI_File_get_size(c_file, &c_size);

    if (MPI_SUCCESS == c_err)
    {
        *i_size = (int64_t) c_size;
    }
    *i_err = (fint_t) c_err;
}

#if defined(F2C_CONF_A_SUP_INT)
F2C_CONF_DLL_EXPORT void MP_UTIL_FILE_GET_VIEW(fint_t*  i_file,
                                               int64_t* i_disp,
                                               fint_t*  i_etyp,
                                               fint_t*  i_ftyp,
                                               F2C_CONF_STRING i_drep,
                                               fint_t*  i_err
                                               F2C_CONF_SUP_INT len)
#else
F2C_CONF_DLL_EXPORT void MP_UTIL_FILE_GET_VIEW(fint_t*  i_file,
                                               int64_t* i_disp,
                                               fint_t*  i_etyp,
                                               fint_t*  i_ftyp,
                                               F2C_CONF_STRING i_drep,
                                               fint_t*  i_err)
#endif
{
#if   defined(F2C_CONF_A_SUP_INT)
   char* dP = i_drep;
   int   dl = len;
#else
   char* dP = i_drep->strP;
   int   dl = i_drep->len;
#endif

    MPI_File     c_file = MPI_File_f2c(*i_file);
    MPI_Datatype c_etyp, c_ftyp;
    MPI_Offset   c_disp;
    char         c_drep[MPI_MAX_DATAREP_STRING];

    int c_err = MPI_File_get_view(c_file, &c_disp, &c_etyp, &c_ftyp, c_drep);

    if (MPI_SUCCESS == c_err)
    {
        *i_disp = (int64_t) c_disp;
        *i_etyp = MPI_Type_c2f(c_etyp);
        *i_ftyp = MPI_Type_c2f(c_ftyp);
        memset(dP, ' ', dl);
        memcpy(dP, c_drep, (dl < MPI_MAX_DATAREP_STRING) ? dl : MPI_MAX_DATAREP_STRING);
    }
    *i_err = (fint_t) c_err;
}


F2C_CONF_DLL_EXPORT void MP_UTIL_TYPE_GET_EXTENT(fint_t* i_type, int64_t* i_lb, int64_t* i_extent, fint_t* i_err)
{
    MPI_Datatype c_type = MPI_Type_f2c(*i_type);
    MPI_Aint     c_lb;
    MPI_Aint     c_sz;

    int c_err = MPI_Type_get_extent(c_type, &c_lb, &c_sz);

    if (MPI_SUCCESS == c_err)
    {
       *i_lb     = (int64_t) c_lb;
       *i_extent = (int64_t) c_sz;
    }
    *i_err = (fint_t) c_err;
}
