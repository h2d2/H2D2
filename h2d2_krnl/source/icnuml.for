C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C      FUNCTION IC_NUML_CMD ()
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NUML_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NUML_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icnuml.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER         IERR
      INTEGER         HOBJ
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NOMTMP
      INTEGER         LNOM
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_NUML_AID()
            GOTO 9999
         ENDIF
      ENDIF

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_NUMEROTATION'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the renumbering file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         NOMTMP = NOMFIC
         CALL SP_STRN_CLP(NOMTMP, 50)
         LNOM = SP_STRN_LEN(NOMTMP)
         WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                           NOMTMP(1:LNOM)
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = NM_LCLC_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = NM_LCLC_INI   (HOBJ, NOMFIC, 0, 0)
      IF (ERR_GOOD()) IERR = NM_LCLC_CHARGE(HOBJ)

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS#<35>#= ',
C     &                             NM_NUML_REQNNT(HOBJ)
C         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the renumbering</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NUML_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_NUML_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NUML_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NUML_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icnuml.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>local_numbering</b> constructs the
C  node renumbering of the mesh local to a process.
C</comment>
      IC_NUML_REQCMD = 'local_numbering'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NUML_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icnuml.hlp')

      RETURN
      END

