C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_VELE_CMD
C     CHARACTER*(32) IC_DT_VELE_REQCMD
C   Private:
C     INTEGER IC_DT_VELE_PRN
C     SUBROUTINE IC_DT_VELE_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VELE_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VELE_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtvele_ic.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvele_ic.fc'

      INTEGER         IERR, IRET
      INTEGER         ISTAT
      INTEGER         HOBJ
      INTEGER         HLST
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_VELE_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the elemental value file</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IRET .NE. 0) MODE = 'r'
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9903

C---     Détecte une liste de fichiers
      HLST = 0
      IF (SP_STRN_UINT(NOMFIC)) THEN
         IRET = SP_STRN_TKI(NOMFIC, ',', 1, HLST)
         IF (IRET .NE. 0) GOTO 9901
         IF (.NOT. DS_LIST_HVALIDE(HLST)) GOTO 9904
         NOMFIC = ' '
      ENDIF

C---     Construis et initialise l'objet
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_VNOD_INIFIC(HOBJ, HLST, NOMFIC, ISTAT)

C---     Imprime l'objet
      IF (ERR_GOOD()) IERR = IC_DT_VELE_PRN(HOBJ)

C---     Retourne la handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the elemental values</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ',
     &                        MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A,I12)') 'ERR_HANDLE_INVALIDE',': ',
     &                        HLST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_VELE_AID()

9999  CONTINUE
      IC_DT_VELE_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_VELE_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VELE_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvele_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tete
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_VALEURS_ELEMENTAIRES'
      CALL LOG_ECRIS(LOG_BUF)

C---     Corps
      CALL LOG_INCIND()
      IERR = DT_VNOD_PRN(HOBJ)
      CALL LOG_DECIND()

      IC_DT_VELE_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_VELE_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_VELE_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvele_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>vele</b> constructs the elemental values, varying in time. The
C  elemental values are all the values related to the elements and
C  discontinuous across element boundaries.
C</comment>
      IC_DT_VELE_REQCMD = 'vele'
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_VELE_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_VELE_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtvele_ic.hlp')

      RETURN
      END
