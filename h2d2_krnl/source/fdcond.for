C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER FD_COND_000
C     INTEGER FD_COND_999
C     INTEGER FD_COND_PKL
C     INTEGER FD_COND_UPK
C     INTEGER FD_COND_CTR
C     INTEGER FD_COND_DTR
C     INTEGER FD_COND_INI
C     INTEGER FD_COND_RST
C     INTEGER FD_COND_REQHBASE
C     LOGICAL FD_COND_HVALIDE
C     INTEGER FD_COND_REQNCLCND
C     INTEGER FD_COND_REQNCLCNV
C     INTEGER FD_COND_REQNSCT
C     REAL*8 FD_COND_REQTMIN
C     REAL*8 FD_COND_REQTMAX
C     CHARACTER*256 FD_COND_REQNOMF
C   Private:
C     INTEGER FD_COND_RAZ
C     INTEGER FD_COND_LISVAL
C     INTEGER FD_COND_LISSCT
C     INTEGER FD_COND_OPEN
C     INTEGER FD_COND_CLOSE
C     INTEGER FD_COND_BHDR
C     INTEGER FD_COND_INI1FIC
C     INTEGER FD_COND_INISTR
C     INTEGER FD_COND_ASGSCT
C     INTEGER FD_COND_ASGVALSCT
C     INTEGER FD_COND_TRISCT
C     INTEGER FD_COND_REQSCT
C     INTEGER FD_COND_REQVALSCT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction <code>FD_COND_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_COND_NOBJMAX,
     &                   FD_COND_HBASE,
     &                   '(Boundary) Condition file')

      FD_COND_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_COND_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER  IERR
      EXTERNAL FD_COND_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_COND_NOBJMAX,
     &                   FD_COND_HBASE,
     &                   FD_COND_DTR)

      FD_COND_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_COND_HBASE

C---     Pickle
      IERR = IO_XML_WH_A(HXML, FD_COND_HLIST (IOB))
      IERR = IO_XML_WI_1(HXML, FD_COND_NCLCND(IOB))
      IERR = IO_XML_WI_1(HXML, FD_COND_NSCT  (IOB))
      IERR = IO_XML_WI_1(HXML, FD_COND_NSMAX (IOB))

C---     La table des temps
      IF (ERR_GOOD()) THEN
         N = FD_COND_NSCT (IOB)
         NX= FD_COND_NSMAX(IOB)
         L = FD_COND_LTEMP(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table des positions
      IF (ERR_GOOD()) THEN
         N = FD_COND_NSCT (IOB)
         NX= FD_COND_NSMAX(IOB)
         L = FD_COND_LFPOS(IOB)
         IERR = IO_XML_WX_V(HXML, NX, N, L, 1)
      ENDIF

      FD_COND_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, NX, N
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_COND_HBASE

C---     Efface les attributs
      IERR = FD_COND_RAZ(HOBJ)

C---     Un-pickle
      IERR = IO_XML_RH_A(HXML, FD_COND_HLIST (IOB))
      IERR = IO_XML_RI_1(HXML, FD_COND_NCLCND(IOB))
      IERR = IO_XML_RI_1(HXML, FD_COND_NSCT  (IOB))
      IERR = IO_XML_RI_1(HXML, FD_COND_NSMAX (IOB))

C---     La table des temps
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_COND_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_COND_NSCT (IOB))
      FD_COND_LTEMP(IOB) = L

C---     La table des positions
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RX_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. FD_COND_NSMAX(IOB))
D     CALL ERR_ASR(ERR_BAD() .OR. N  .EQ. FD_COND_NSCT (IOB))
      FD_COND_LFPOS(IOB) = L

      FD_COND_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_COND_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   FD_COND_NOBJMAX,
     &                                   FD_COND_HBASE)
      IF (ERR_GOOD()) IERR = FD_COND_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. FD_COND_HVALIDE(HOBJ))

      FD_COND_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_COND_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_COND_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_COND_NOBJMAX,
     &                   FD_COND_HBASE)

      FD_COND_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode FD_COND_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - FD_COND_HBASE

      FD_COND_HLIST (IOB) = 0
      FD_COND_NCLCND(IOB) = 0
      FD_COND_NCLCNV(IOB) = 0
      FD_COND_NSCT  (IOB) = 0
      FD_COND_NSMAX (IOB) = 0
      FD_COND_LIFIC (IOB) = 0
      FD_COND_LTEMP (IOB) = 0
      FD_COND_LFPOS (IOB) = 0

      FD_COND_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction FD_COND_INI initialise l'objet à l'aide des
C     paramètres. Le nom de fichier pour être égal à la valeur
C     retournée par IO_UTIL_FICDUM(), auquel cas le fichier est
C     amorphe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLST        Handle sur une liste de fichiers
C     NOMFIC      Nom du fichier de condition
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_INI(HOBJ, HFLST, NOMFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HFLST
      CHARACTER*(*) NOMFIC

      INCLUDE 'fdcond.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I, K, L
      INTEGER HLIST
      CHARACTER*(256) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.cond')

C---     Contrôle les paramètres
      IF (.NOT. DS_LIST_HVALIDE(HFLST)) THEN
         IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      ELSE
         IF (DS_LIST_REQDIM(HFLST) .LE. 0) GOTO 9901
      ENDIF

C---     Reset les données
      IERR = FD_COND_RST(HOBJ)

C---     Construis la liste des fichiers
      HLIST = 0
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLIST)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HLIST)
      IF (ERR_GOOD()) THEN
         IF (DS_LIST_HVALIDE(HFLST)) THEN
            DO I=1, DS_LIST_REQDIM(HFLST)
               IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HFLST, I, BUF)
               L = SP_STRN_LEN(BUF)
               IF (ERR_GOOD()) IERR = DS_LIST_TRVVAL(HLIST,K,BUF(1:L))
               IF (ERR_GOOD()) THEN      ! Fichier existe déjà
                  GOTO 9902
               ELSEIF (ERR_ESTMSG('ERR_LISTE_VALEUR_ABSENTE')) THEN
                  CALL ERR_RESET()
               ENDIF
               IF (ERR_GOOD()) IERR = DS_LIST_AJTVAL(HLIST, BUF(1:L))
            ENDDO
         ELSE
            IERR = DS_LIST_AJTVAL(HLIST, NOMFIC(1:SP_STRN_LEN(NOMFIC)))
         ENDIF
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_COND_HBASE
         FD_COND_HLIST(IOB) = HLIST
      ENDIF

C---     Initialise la structure interne
      !!! FNAML = SP_STRN_LEN(NOMFIC)
      !!! FDUM  = IO_UTIL_FICDUM()
      !!! FDUML = SP_STRN_LEN(FDUM)
      IF (ERR_GOOD()) IERR = FD_COND_INISTR(HOBJ, HLIST)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LISTE_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  CALL SP_STRN_CLP(BUF, 80)
      L = SP_STRN_LEN(BUF)
      WRITE(ERR_BUF,'(3A)') 'ERR_NOM_FICHIER_EXISTANT',': ', BUF(1:L)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.cond')
      FD_COND_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_COND_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LIFIC
      INTEGER LTEMP
      INTEGER LFPOS
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_COND_HBASE
      LIFIC = FD_COND_LIFIC(IOB)
      LTEMP = FD_COND_LTEMP(IOB)
      LFPOS = FD_COND_LFPOS(IOB)

C---     Désalloue la mémoire
      IF (LIFIC .NE. 0) IERR = SO_ALLC_ALLINT(0, LIFIC)
      IF (LTEMP .NE. 0) IERR = SO_ALLC_ALLRE8(0, LTEMP)
      IF (LFPOS .NE. 0) IERR = SO_ALLC_ALLIN8(0, LFPOS)

C---     Efface les attributs
      IERR = FD_COND_RAZ(HOBJ)

      FD_COND_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_COND_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'
C------------------------------------------------------------------------

      FD_COND_REQHBASE = FD_COND_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_COND_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdcond.fc'
C------------------------------------------------------------------------

      FD_COND_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_COND_NOBJMAX,
     &                                  FD_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_COND_LIS
C
C Description:
C     La fonction FD_COND_LIS est la fonction principale de lecture
C     d'un fichier de type CONDition.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     TOLD        Temps
C     TNEW        Temps nouveau demandé
C
C Sortie:
C     NCLCND      Nombre de conditions
C     KCLCND      Vecteur des conditions
C     NCLCNV      Nombre de valeurs des conditions
C     VCLCNV      Vecteur des valeurs
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_LIS(HOBJ,
     &                     NCLCND,
     &                     KCLCND,
     &                     NCLCNV,
     &                     VCLCNV,
     &                     TOLD,
     &                     TNEW,
     &                     MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_LIS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCLCND
      INTEGER KCLCND(4, NCLCND)
      INTEGER NCLCNV
      REAL*8  VCLCNV(NCLCNV)
      REAL*8  TOLD
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'fdcond.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER LIFIC, LTEMP, LFPOS
D     INTEGER NCLCND_L, NCLCNV_L      
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(TNEW   .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.cond.read')

C---     Récupère les attributs
      IOB     = HOBJ - FD_COND_HBASE
      NSCT    = FD_COND_NSCT  (IOB)
      LIFIC   = FD_COND_LIFIC (IOB)
      LTEMP   = FD_COND_LTEMP (IOB)
      LFPOS   = FD_COND_LFPOS (IOB)
D     NCLCND_L= FD_COND_NCLCND(IOB)
D     NCLCNV_L= FD_COND_NCLCNV(IOB)
D     CALL ERR_ASR(NCLCND_L .GT. 0 .AND. NCLCND_L .GE. NCLCND)
D     CALL ERR_ASR(NCLCNV_L .GT. 0 .AND. NCLCNV_L .EQ. NCLCNV)

C---     Lis la section
      IERR = FD_COND_LIS_E(HOBJ,
     &                     NCLCND,
     &                     KCLCND,
     &                     NCLCNV,
     &                     VCLCNV,
     &                     TOLD,
     &                     TNEW,
     &                     NSCT,
     &                     KA(SO_ALLC_REQKIND(KA,LIFIC)),
     &                     VA(SO_ALLC_REQVIND(VA,LTEMP)),
     &                     XA(SO_ALLC_REQXIND(XA,LFPOS)),
     &                     MODIF)

      CALL TR_CHRN_STOP('h2d2.io.cond.read')
      FD_COND_LIS = ERR_TYP()
      RETURN
      END

C************************************************************************
C     Adapted from Numerical Recipes
C
C     Given an array XX(1:N), and a value X, returns an index J,
C     J in [1,N+1], such that if X is inserted at XX(J), XX is kept sorted.
C     The case X = XX(J) is possible.
C************************************************************************
      FUNCTION FD_COND_FIND_I(XX, X) RESULT(JU)

      IMPLICIT NONE
      REAL*8, INTENT(IN) :: XX(:)
      REAL*8, INTENT(IN) :: X

      INTEGER :: JL,JM,JU
C-----------------------------------------------------------------------

      JL = 0                     ! Initialize lower
      JU = SIZE(XX) + 1          ! and upper limits.
      DO WHILE(JU-JL > 1)        ! Repeat until this condition is satisfied.
         JM = (JU+JL)/2          ! Compute a midpoint,
         IF (X > XX(JM)) THEN
            JL=JM                ! and replace either the lower limit
         ELSE
            JU=JM                ! or the upper limit, as appropriate.
         END IF
      END DO

      END FUNCTION FD_COND_FIND_I
      
C************************************************************************
C Sommaire: FD_COND_LIS_E
C
C Description:
C     La fonction privée FD_COND_LIS_E est la fonction principale
C     de lecture d'un fichier de type CONDition. Elle détermine le
C     pas de temps demandé, lis des données et au besoin interpole
C     linéairement.
C
C Entrée:
C     TOLD        Temps
C     TNEW        Temps nouveau demandé
C     NOMFIC      Nom du fichier
C     NSCT        Nombre de sections du fichier
C     VTEMPS      Table des temps des sections
C     KPOS        Table des positions fichier des sections
C
C Sortie:
C     NCLCND      Nombre de conditions
C     KCLCND      Table des conditions
C     NCLCNV      Nombre de valeurs des conditions
C     VCLCNV      Table des valeurs
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_LIS_E(HOBJ,
     &                       NCLCND,
     &                       KCLCND,
     &                       NCLCNV,
     &                       VCLCNV,
     &                       TOLD,
     &                       TNEW,
     &                       NSCT,
     &                       KIFIC,
     &                       VTEMPS,
     &                       KFPOS,
     &                       MODIF)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T, SO_ALLC_KPTR2,
     &                     SO_ALLC_VPTR1_T, SO_ALLC_VPTR1 
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   NCLCND
      INTEGER   KCLCND(4, NCLCND)
      INTEGER   NCLCNV
      REAL*8    VCLCNV(NCLCNV)
      REAL*8    TOLD
      REAL*8    TNEW
      INTEGER   NSCT
      INTEGER   KIFIC (NSCT)
      REAL*8    VTEMPS(NSCT)
      INTEGER*8 KFPOS (NSCT)
      LOGICAL   MODIF
      
      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'fdcond.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      REAL*8  VN1, VN2
      INTEGER IERR, IRET
      INTEGER ICL, IS, ISCT(2)
      INTEGER I_RANK, I_ERROR, I_COMM
      TYPE(SO_ALLC_KPTR2_T) :: KCLCND0
      TYPE(SO_ALLC_VPTR1_T) :: VCLCNV0
      LOGICAL SAUT
      
!!!      INTEGER FD_COND_FIND_I
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(TNEW .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK
      MODIF = .TRUE.
      KCLCND0 = SO_ALLC_KPTR2()
      VCLCNV0 = SO_ALLC_VPTR1()

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

      IF (I_RANK .NE. I_MASTER) GOTO 200

C---     Contrôles primaires
      SAUT = .TRUE.
      IF (TOLD .GE. 0) THEN
         IF (TNEW.EQ.TOLD) GOTO 119
         IF (TOLD.GE.VTEMPS(NSCT) .AND. TNEW.GE.VTEMPS(NSCT)) GOTO 119
         IF (TOLD.LE.VTEMPS(   1) .AND. TNEW.LE.VTEMPS(   1)) GOTO 119
      ENDIF
      SAUT = .FALSE.
119   CONTINUE

C---     Recherche les sections
      IS = 1
      IF (.NOT. SAUT) THEN
         !!! TODO - Factoriser tous les FIND_I
         !!! IS = FD_COND_FIND_I(VTEMPS, TNEW)
         !!! FD_COND_FIND_I ne donne pas le même résultat que la boucle.
         !!! La boucle retourne l'indice strictement supérieur.
         DO IS=1,NSCT
            IF (VTEMPS(IS) .GT. TNEW) GOTO 120
         ENDDO
120      CONTINUE
      ENDIF
      ISCT(2) = IS
      ISCT(1) = ISCT(2) - 1

C---     Broadcast pour que tous aient les mêmes valeurs
200   CONTINUE
      CALL MPI_BCAST(SAUT, 1,MPI_LOGICAL,  I_MASTER,I_COMM,I_ERROR)
      IF (SAUT) GOTO 9999
      CALL MPI_BCAST(ISCT, 2,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)

C---     TNEW < TMIN
      IF (ERR_GOOD() .AND. ISCT(2) .EQ. 1) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         NCLCND,
     &                         KCLCND,
     &                         NCLCNV,
     &                         VCLCNV)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW > TMAX
      IF (ERR_GOOD() .AND. ISCT(1) .EQ. NSCT) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         NCLCND,
     &                         KCLCND,
     &                         NCLCNV,
     &                         VCLCNV)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TINF
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(ISCT(1))) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         NCLCND,
     &                         KCLCND,
     &                         NCLCNV,
     &                         VCLCNV)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TNEW = TSUP
      IF (ERR_GOOD() .AND. TNEW .EQ. VTEMPS(ISCT(2))) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         NCLCND,
     &                         KCLCND,
     &                         NCLCNV,
     &                         VCLCNV)
         MODIF = .TRUE.
         GOTO 8000
      ENDIF

C---     TINF > TNEW > TSUP

C---     Alloue la mémoire
      IF (ERR_GOOD()) KCLCND0 = SO_ALLC_KPTR2(4, NCLCND)
      IF (ERR_GOOD()) VCLCNV0 = SO_ALLC_VPTR1(NCLCNV)

C---     Lis la valeur inférieur
      IF (ERR_GOOD()) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(1)),
     &                         KFPOS (ISCT(1)),
     &                         VTEMPS(ISCT(1)),
     &                         NCLCND,
     &                         KCLCND,
     &                         NCLCNV,
     &                         VCLCNV)
      ENDIF

C---     Lis la valeur supérieur
      IF (ERR_GOOD()) THEN
         IERR = FD_COND_LISSCT(HOBJ,
     &                         KIFIC (ISCT(2)),
     &                         KFPOS (ISCT(2)),
     &                         VTEMPS(ISCT(2)),
     &                         NCLCND,
     &                         KCLCND0%KPTR,
     &                         NCLCNV,
     &                         VCLCNV0%VPTR)
      ENDIF

C---     Contrôles
      IF (ERR_GOOD()) THEN
         DO ICL=1, NCLCND
            IF (KCLCND(1,ICL) .NE. KCLCND0%KPTR(1,ICL)) GOTO 9901
            IF (KCLCND(2,ICL) .NE. KCLCND0%KPTR(2,ICL)) GOTO 9902
         ENDDO
      ENDIF

C---     Interpole
      IF (ERR_GOOD()) THEN
         VN2 = (            TNEW - VTEMPS(ISCT(1))) /
     &          (VTEMPS(ISCT(2)) - VTEMPS(ISCT(1)))
         VN1 = 1.0D0 - VN2
         VCLCNV(:) = VN1*VCLCNV(:) + VN2*VCLCNV0%VPTR(:)
         MODIF = .TRUE.
      ENDIF

8000  CONTINUE

C---     Nettoie
      KCLCND0 = SO_ALLC_KPTR2()
      VCLCNV0 = SO_ALLC_VPTR1()

      GOTO 9999
C-------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(3A)') 'ERR_CL_NOM_DIFFERENTS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_CL_CODE_DIFFERENTS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,2(A,I6))') 'MSG_CODE_CL', ': ', KCLCND (2,ICL),
     &                                       '/ ', KCLCND0%KPTR(2,ICL)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988
      
9988  CONTINUE
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_LIMITE', ': ', ICL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,F25.6)') 'MSG_TEMPS', ': ', VTEMPS(ISCT(1))
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,F25.6)') 'MSG_TEMPS', ': ', VTEMPS(ISCT(2))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_COND_LIS_E = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_COND_LISSCT
C
C Description:
C     La fonction privée FD_COND_LISSCT lis une section d'un fichier
C     non-stationnaire de type CONDition.
C
C Entrée:
C     XFIC        Handle eXterne sur le FIChier à lire
C     XPOS        Offset dans le fichier
C     TEMPS       Le temps demandé
C
C Sortie:
C     NCLCND      Nombre de conditions
C     KCLCND      Table des conditions
C     NCLCNV      Nombre de valeurs des conditions
C     VCLCNV      Table des valeurs
C
C Notes:
C
C************************************************************************
      FUNCTION FD_COND_LISSCT(HOBJ,
     &                        IFIC,
     &                        XPOS,
     &                        TEMPS,
     &                        NCLCND,
     &                        KCLCND,
     &                        NCLCNV,
     &                        VCLCNV)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   IFIC      ! Indice du fichier
      INTEGER*8 XPOS      ! Offset dans le fichier
      REAL*8    TEMPS     ! Pour contrôles
      INTEGER   NCLCND
      INTEGER   KCLCND(4, NCLCND)
      INTEGER   NCLCNV
      REAL*8    VCLCNV(NCLCNV)

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER, PARAMETER :: I_MASTER = 0

      REAL*8           V
      INTEGER*8        XFIC
      INTEGER          IERR, IRET
      INTEGER          I, IC, JC
      INTEGER          INOM, IV
      INTEGER          ITYP
      INTEGER          IVDEB, IVFIN
      INTEGER          I_RANK, I_ERROR, I_COMM
      INTEGER          LB
      INTEGER          NBLK
      LOGICAL          LXTND
      CHARACTER*(1024) BUF
      CHARACTER*(256)  TOK, FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Initialise
      BUF = ' '
      IVFIN = 0
      FNAME = FD_COND_REQNOMF(HOBJ, IFIC)

C---     Ouvre le fichier
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_COND_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()
      IF (.NOT. ERR_GOOD()) GOTO 9988

C---     Lis l'entête de section
      NBLK = -1
      IF (I_RANK .EQ. I_MASTER) THEN
         IRET = C_FA_LISLN(XFIC, BUF)
         IF (IRET .NE. 0) GOTO 108
         LB = SP_STRN_LEN(BUF)
         IF (LB .LE. 0) GOTO 108
         READ(BUF(1:LB), *, ERR=108, END=109) NBLK, V
         IF (NBLK .LT. NCLCND) GOTO 110
         IF (V .NE. TEMPS) GOTO 111
         IRET = 0
         GOTO 119
108      CONTINUE ! ERR
         IRET = -1
         GOTO 119
109      CONTINUE ! EOR
         IRET = -2
         GOTO 119
110      CONTINUE ! ERR NCLCND
         IRET = -3
         GOTO 119
111      CONTINUE ! ERR TEMPS
         IRET = -4
         GOTO 119
119      CONTINUE
      ENDIF

C---     Broadcast the error code
      CALL MPI_BCAST(IRET, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IRET .EQ. -1) GOTO 9901
      IF (IRET .EQ. -2) GOTO 9902
      IF (IRET .EQ. -3) GOTO 9903
      IF (IRET .EQ. -4) GOTO 9904
D     CALL ERR_ASR(IRET .EQ. 0)

C---     Broadcast the number of lines to process
      CALL MPI_BCAST(NBLK, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)

C---     Boucle sur les conditions
      IC = 0
      DO JC=1,NBLK
         IF (ERR_BAD()) GOTO 299

C---        Lis la ligne
         BUF = ' '
         IF (I_RANK .EQ. I_MASTER) THEN
            IRET = C_FA_LISLN(XFIC, BUF)
            IF (IRET .NE. 0) GOTO 208
            IF (SP_STRN_LEN(BUF) .GT. 0) THEN
               CALL SP_STRN_TRM(BUF)
               CALL SP_STRN_SBC(BUF, CHAR_TAB, ' ')
               CALL SP_STRN_DMC(BUF, ' ')
            ENDIF
            IF (SP_STRN_LEN(BUF) .EQ. 0) GOTO 209
            GOTO 219

208         CONTINUE
            IRET = -1    ! ERR
            GOTO 219
209         CONTINUE
            IRET = -2    ! END
            GOTO 219
219         CONTINUE
         ENDIF

C---        Broadcast le code d'erreur
         CALL MPI_BCAST(IRET, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IRET .EQ. -1) GOTO 9901
         IF (IRET .EQ. -2) GOTO 9902
D        CALL ERR_ASR(IRET .EQ. 0)

C---        Broadcast les données
         LB = SP_STRN_LEN(BUF)
         CALL MPI_BCAST(LB, 1, MP_TYPE_INT(),
     &                  I_MASTER, I_COMM, I_ERROR)
         IF (LB .LE. 0) GOTO 9901
         CALL MPI_BCAST(BUF(1:LB), LB, MPI_CHARACTER,
     &                  I_MASTER, I_COMM, I_ERROR)

C---        Extrais le nom de la condition
         IF (ERR_GOOD()) THEN
            IRET = SP_STRN_TKS(BUF(1:LB), ' ', 1, TOK)
            IF (IRET .NE. 0) GOTO 9901
            CALL SP_STRN_LCS(TOK)
            IRET = C_ST_CRC32(TOK(1:SP_STRN_LEN(TOK)), INOM)
            IF (IRET .NE. 0) GOTO 9901
         ENDIF
         
C---        Nouveau ou extension?
         LXTND = (IC .GE. 1 .AND. KCLCND(1,IC) .EQ. INOM)

C---        Extrais le type de condition
         IF (ERR_GOOD() .AND. .NOT. LXTND) THEN
            IRET = SP_STRN_TKS(BUF(1:LB), ' ', 2, TOK)
            IF (IRET .EQ. -1) GOTO 9901
            IF (SP_STRN_INT(TOK(1:SP_STRN_LEN(TOK)))) THEN
               READ(TOK, *, ERR=9901) ITYP
            ELSE
               IRET = C_ST_CRC32(TOK(1:SP_STRN_LEN(TOK)), ITYP)
               IF (IRET .NE. 0) GOTO 9901
            ENDIF
         ENDIF

C---        Extrais les valeurs
         IF (ERR_GOOD()) THEN
            IVDEB = IVFIN + 1
            I = 1                      ! SAUTE LE NOM
            IF (.NOT. LXTND) I = I+1   ! SAUTE LE TYPE
220         CONTINUE
               I = I + 1
               IRET = SP_STRN_TKS(BUF(1:LB), ' ', I, TOK)
               IF (IRET .EQ. -1) GOTO 229
               IF (SP_STRN_DBL(TOK(1:SP_STRN_LEN(TOK)))) THEN
                  READ(TOK, *, ERR=9901) V
               ELSE
                  IRET = C_ST_CRC32(TOK(1:SP_STRN_LEN(TOK)), IV)
                  IF (IRET .NE. 0) GOTO 9901
                  V = IV
               ENDIF
               IVFIN = IVFIN + 1
               VCLCNV(IVFIN) = V
            GOTO 220
229         CONTINUE
         ENDIF

C---        Stoke les données
         IF (ERR_GOOD()) THEN
            IF (LXTND) THEN
               KCLCND(4,IC) = IVFIN
            ELSE
               DO I=1,IC
                  IF (KCLCND(1,I) .EQ. INOM) GOTO 9905
               ENDDO
               IC = IC + 1
               KCLCND(1,IC) = INOM
               KCLCND(2,IC) = ITYP
               KCLCND(3,IC) = IVDEB
               KCLCND(4,IC) = IVFIN
            ENDIF
         ENDIF

      ENDDO

299   CONTINUE
      IF (IVFIN .NE. NCLCNV) GOTO 9906

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF,'(A,2(A,I6))') 'ERR_NCLCND_INCOHERENT', ': ',
     &                       NBLK, ' / ', NCLCND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF,'(A,2(A,1PE14.6E3))') 'ERR_TEMPS_INCOHERENT', ': ',
     &                       V, ' / ', TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_REUTILISATION_NOM_LIMITE',': ',
     &                       TOK(1:SP_STRN_LEN(TOK))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NBR_VALEURS_LUES',': ',
     &      IVFIN, ' / ', NCLCNV
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF,'(2A,F25.6)') 'MSG_TEMPS', ': ', TEMPS
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I25)') 'MSG_OFFSET', ': ', XPOS
      CALL ERR_AJT(ERR_BUF)
      IF (SP_STRN_LEN(BUF) .GT. 0) THEN
         WRITE(ERR_BUF,'(2A,A)') 'MSG_BUFFER', ': ',
     &      BUF(1:MAX(60,SP_STRN_LEN(BUF)))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_COND_CLOSE(HOBJ, XFIC)
      IERR = MP_UTIL_SYNCERR()
      FD_COND_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute une section.
C
C Description:
C     La fonction privée FD_COND_ASGSCT ajoute une section à la
C     structure.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     TEMPS       Temps associé à la section
C     XPOS        Offset du début de la section
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_ASGSCT (HOBJ, IFIC, TEMPS, XPOS)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   IFIC
      REAL*8    TEMPS
      INTEGER*8 XPOS

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER NSMAX
      INTEGER LIFIC, LTEMP, LFPOS
      TYPE(SO_ALLC_PTR_T) :: PIFIC
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      INTEGER,   POINTER :: KIFIC(:)
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_COND_HBASE
      NSMAX = FD_COND_NSMAX(IOB)
      NSCT  = FD_COND_NSCT (IOB)
      LIFIC = FD_COND_LIFIC(IOB)
      LTEMP = FD_COND_LTEMP(IOB)
      LFPOS = FD_COND_LFPOS(IOB)

C---     (Re)-dimensionne les tables
      IF (NSCT .GE. NSMAX) THEN
         NSMAX = NSMAX + NSMAX
         IF (NSMAX .EQ. 0) NSMAX = 128
         IERR = SO_ALLC_ALLINT(NSMAX, LIFIC)
         IERR = SO_ALLC_ALLRE8(NSMAX, LTEMP)
         IERR = SO_ALLC_ALLIN8(NSMAX, LFPOS)
         FD_COND_NSMAX(IOB) = NSMAX
         FD_COND_LIFIC(IOB) = LIFIC
         FD_COND_LTEMP(IOB) = LTEMP
         FD_COND_LFPOS(IOB) = LFPOS
      ENDIF

C---     Les tables
      PIFIC = SO_ALLC_PTR(LIFIC)
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      KIFIC => PIFIC%CST2K()
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Assigne les valeurs
      NSCT = NSCT + 1
      KIFIC(NSCT) = IFIC
      VTMPS(NSCT) = TEMPS
      KPOS (NSCT) = XPOS

C---     Conserve les attributs
      FD_COND_NSCT (IOB) = NSCT

      FD_COND_ASGSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_COND_OPEN
C
C Description:
C     La fonction privée FD_COND_OPEN ouvre le fichier à la position
C     XPOS.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAME    Nom du fichier
C     ISTA     Status du fichier
C     XPOS     Offset dans le fichier
C
C Sortie:
C     XFIC     Handle sur le fichier
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_OPEN(HOBJ,
     &                      FNAME,
     &                      ISTA,
     &                      XPOS,
     &                      XFIC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER       ISTA
      INTEGER*8     XPOS      ! Offset dans le fichier
      INTEGER*8     XFIC

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0
      XFIC = 0

C---     Ouvre le fichier
      XFIC = C_FA_OUVRE(FNAME(1:SP_STRN_LEN(FNAME)), ISTA)
      IF (XFIC .EQ. 0) GOTO 9900

C---     Positionne dans le fichier
      IF (XPOS .GT. 0) THEN
         IRET = C_FA_ASGPOS(XFIC, XPOS)
         IF (IRET .NE. 0) GOTO 9901
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_COND_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_COND_CLOSE
C
C Description:
C     La fonction privée FD_COND_CLOSE ferme le fichier.
C
C Entrée:
C     XFIC     Handle sur le fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_CLOSE(HOBJ, XFIC)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0

      IRET = C_FA_FERME(XFIC)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FERMETURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_COND_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis une entête
C
C Description:
C     La fonction privée FD_PRNA_RHDR lis une entête.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     XFIC           Handle sur le fichier
C
C Sortie:
C     NNT
C     NPRN
C     TMPS
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_RHDR(HOBJ,
     &                      XFIC,
     &                      ILIGN,
     &                      NCLCND,
     &                      NCLCNV,
     &                      TMPS)

      IMPLICIT NONE

      INTEGER,   INTENT(IN)    :: HOBJ
      INTEGER*8, INTENT(IN)    :: XFIC
      INTEGER,   INTENT(INOUT) :: ILIGN
      INTEGER,   INTENT(OUT)   :: NCLCND
      INTEGER,   INTENT(OUT)   :: NCLCNV
      REAL*8,    INTENT(OUT)   :: TMPS

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_st.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR, IRET
      INTEGER IC
      INTEGER ITMP, INOM
      INTEGER LB
      INTEGER NBLK, NVAL

      INTEGER, PARAMETER :: LBUF = 1024
      CHARACTER*(LBUF) BUF
      CHARACTER*(256)  TOK
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise
      BUF  = ' '
      INOM = 0
      NCLCND = 0
      NCLCNV = 0

C---     Lis l'entête
      ILIGN = ILIGN + 1
      IRET = C_FA_LISLN (XFIC, BUF)
      IF (IRET .NE. 0) GOTO 9901
      LB = SP_STRN_LEN(BUF)
      IF (LB .LE. 0) GOTO 9901
      READ(BUF(1:LB), *, ERR=9902, END=9902) NBLK, TMPS

C---        Compte le nombre de valeurs
      IERR = 0
      DO IC=1, NBLK
C---        Lis la ligne
         ILIGN = ILIGN + 1
         IRET = C_FA_LISLN(XFIC, BUF)
         IF (IRET .NE. 0) GOTO 9902
         IF (SP_STRN_LEN(BUF) .GT. 0) THEN
            CALL SP_STRN_SBC(BUF, CHAR_TAB, ' ')
            CALL SP_STRN_DMC(BUF, ' ')
            CALL SP_STRN_TRM(BUF)
         ENDIF
         IF (SP_STRN_LEN(BUF) .EQ. 0) GOTO 9902

C---        Extrais le nom de la limite
         IF (ERR_GOOD()) THEN
            IRET = SP_STRN_TKS(BUF, ' ', 1, TOK)
            IF (IRET .NE. 0) GOTO 9902
         ENDIF
C---        Compte le nombre de limites différentes
         IF (ERR_GOOD()) THEN
            ITMP = 0
            IRET = C_ST_CRC32(TOK(1:SP_STRN_LEN(TOK)), ITMP)
            IF (ITMP .NE. INOM) THEN
               NCLCND = NCLCND + 1
               INOM = ITMP
            ENDIF
         ENDIF
C---        Compte le nombre total de tokens
         NVAL = SP_STRN_NTOK(BUF, ' ')
         IF (NVAL .LT. 1) GOTO 9903
         NCLCNV = NCLCNV + NVAL
      ENDDO

C---     Nombre de valeurs: Saute le nom et le type
      IF (ERR_GOOD()) THEN
         NCLCNV = NCLCNV - NBLK - NCLCND
      ENDIF
      
      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_NVAL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12)') 'MSG_NCLCNV:', NVAL
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF, '(A,I12)') 'MSG_LIGNE:', ILIGN
      CALL ERR_AJT(ERR_BUF)
      IF (SP_STRN_LEN(BUF) .GT. 0) THEN
         CALL SP_STRN_CLP(BUF, 80)
         CALL ERR_AJT(BUF(1:SP_STRN_LEN(BUF)))
      ENDIF
      GOTO 9999
      
9999  CONTINUE
      FD_COND_RHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un groupe d'entêtes
C
C Description:
C     La fonction privée FD_COND_BHDR lis un maximum de NBMAX entêtes.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ              Handle sur l'objet
C     XFIC              Handle eXterne sur le fichier
C     NNT               Nombre de Noeuds Total
C     NPRN              Nombre de Propriétés Nodales
C     TMAX              Temps max du fichier
C     NBMAX             Nombre max de bloc à lire
C
C Sortie:
C     TMAX              Temps max du fichier
C     IBMAX             Nombre de blocs lus
C     KPOS              Table des positions dans le fichier
C     VTMP              Table des temps
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_BHDR(HOBJ,
     &                      XFIC,
     &                      ILIGN,
     &                      NCLCND,
     &                      NCLCNV,
     &                      TMAX,
     &                      IBMAX,
     &                      NBMAX,
     &                      KPOS,
     &                      VTMP)

      IMPLICIT NONE

      INTEGER,  INTENT(IN)   :: HOBJ
      INTEGER*8,INTENT(IN)   :: XFIC
      INTEGER,  INTENT(INOUT):: ILIGN
      INTEGER,  INTENT(IN)   :: NCLCND
      INTEGER,  INTENT(IN)   :: NCLCNV
      REAL*8,   INTENT(INOUT):: TMAX
      INTEGER,  INTENT(OUT)  :: IBMAX
      INTEGER,  INTENT(IN)   :: NBMAX
      INTEGER*8,INTENT(OUT)  :: KPOS(NBMAX)
      REAL*8,   INTENT(OUT)  :: VTMP(NBMAX)

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      REAL*8    TMPS_L, V
      INTEGER*8 XPOS
      INTEGER   IB, IC
      INTEGER   IERR, IRET
      INTEGER   LB
      INTEGER   NCLCND_L, NCLCNV_L
      INTEGER   NVAL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(NCLCND .GT. 0)
D     CALL ERR_PRE(NCLCNV .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Boucle sur les blocs
      DO IB=1,NBMAX
         IRET = C_FA_REQPOS(XFIC, XPOS)
         IERR = FD_COND_RHDR(HOBJ, XFIC, ILIGN,
     &                       NCLCND_L, NCLCNV_L, TMPS_L)
         IF (ERR_ESTMSG('ERR_FIN_FICHIER')) THEN
            CALL ERR_RESET()
            EXIT
         ENDIF
     
C---        Contrôles
         IF (NCLCND_L .NE. NCLCND)  GOTO 9900
         IF (NCLCNV_L .NE. NCLCNV)  GOTO 9901
         IF (TMPS_L   .LE. TMAX)    GOTO 9902

C---        Assigne les paramètres de section
         KPOS(IB) = XPOS
         VTMP(IB) = TMPS_L

         TMAX  = TMPS_L
         IBMAX = IB
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NCLCND_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,I12))') 'MSG_NCLCND:', NCLCND_L, '/', NCLCND
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(A)') 'ERR_NCLCNV_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,I12))') 'MSG_NCLCNV:', NCLCNV_L, '/', NCLCNV
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_TEMPS_DECROISSANT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,F25.6))') 'MSG_TEMPS:', TMPS_L, ' <=', TMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF, '(A,F25.6)') 'MSG_TEMPS:', TMPS_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,I12)') 'MSG_LIGNE: <=', ILIGN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
      GOTO 9999
      
9999  CONTINUE
      FD_COND_BHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure pour 1 fichier
C
C Description:
C     La fonction privée FD_COND_INI1FIC initialise les structures
C     internes (table des débuts des sections) à partir du fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C     On ne gère que partiellement les erreurs MPI de transmission
C************************************************************************
      FUNCTION FD_COND_INI1FIC(HOBJ, NCLCND, NCLCNV, TMAX, IFIC, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCLCND
      INTEGER NCLCNV
      REAL*8  TMAX
      INTEGER IFIC
      CHARACTER*(*) FNAME

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER I_RANK, I_ERROR, I_COMM
      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER, PARAMETER :: NBMAX = 128

      REAL*8    VTMP(NBMAX)
      REAL*8    TEMPS
      INTEGER*8 KPOS(NBMAX)
      INTEGER*8 XPOS
      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IB, IBMAX
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   ILIGN
      INTEGER   NBTOT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_COND_HBASE

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Ouvre le fichier
      XFIC = 0
      XPOS = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_COND_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     WHILE (ERR_GOOD())
      NBTOT = 0
      ILIGN = 0
      DO WHILE(ERR_GOOD())
         IBMAX = 0
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_COND_BHDR(HOBJ,
     &                          XFIC,
     &                          ILIGN,
     &                          NCLCND,
     &                          NCLCNV,
     &                          TMAX,
     &                          IBMAX,
     &                          NBMAX,
     &                          KPOS,
     &                          VTMP)
         ENDIF
         IERR = MP_UTIL_SYNCERR()

C---        Broadcast les données
         IF (ERR_GOOD()) THEN
            CALL MPI_BCAST(IBMAX, 1, MP_TYPE_INT(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF
         IF (ERR_GOOD() .AND. IBMAX .GT. 0) THEN
            CALL MPI_BCAST(KPOS, IBMAX, MP_TYPE_IN8(),
     &                     I_MASTER, I_COMM, I_ERROR)
            CALL MPI_BCAST(VTMP, IBMAX, MP_TYPE_RE8(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF

C---        Assigne les paramètres de section
         IF (ERR_GOOD()) THEN
!$omp critical(OMP_CS_COND_ASGSCT)
            DO IB=1,IBMAX
               IERR = FD_COND_ASGSCT(HOBJ, IFIC, VTMP(IB), KPOS(IB))
            ENDDO
            NBTOT = NBTOT + IBMAX
!$omp end critical(OMP_CS_COND_ASGSCT)
         ENDIF

         IF (IBMAX .LT. NBMAX) EXIT
      ENDDO

      IF (ERR_GOOD() .AND. FD_COND_NSCT(IOB) .LE. 0) GOTO 9900
      IF (.NOT. ERR_GOOD()) GOTO 9989

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_STRUCT_FICHIER_INVALIDE',': ', FNAME
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989

9989  CONTINUE
      WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_COND_CLOSE(HOBJ, XFIC)
      FD_COND_INI1FIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_COND_INISTR initialise la structure interne à
C     partir de la liste des fichiers passée en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLIST       Handle sur la liste des fichiers
C
C Sortie:
C
C Notes:
C     Le fichier est lu avec les fonctions C C_FA qui permettent de
C     repositionner.
C************************************************************************
      FUNCTION FD_COND_INISTR(HOBJ, HLIST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLIST

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      REAL*8  TMAX
      REAL*8  TEMPS_DUMMY
      INTEGER*8 XPOS_DUMMY
      INTEGER IERR
      INTEGER IOB
      INTEGER I
      INTEGER I_COMM, I_SIZE, I_ERROR
      INTEGER NFIC
      INTEGER NCLCND, NCLCND_L
      INTEGER NCLCNV, NCLCNV_L
      LOGICAL DO_OMP
      CHARACTER*256 FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_COND_HBASE
      NCLCND_L = FD_COND_NCLCND(IOB)
      NCLCNV_L = FD_COND_NCLCNV(IOB)

C---     Lis les dimensions du premier fichier
      NCLCND = 0
      NCLCNV = 0
      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, 1, FNAME)
      IF (ERR_GOOD()) IERR = FD_COND_REQDIM(HOBJ, FNAME, NCLCND, NCLCNV)
      IF (ERR_GOOD() .AND. NCLCND .LE. 0) GOTO 9900
      IF (ERR_GOOD() .AND. NCLCNV .LE. 0) GOTO 9901
D     IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NCLCND_L .EQ. 0 .OR. NCLCND_L .EQ. NCLCND)
D        CALL ERR_ASR(NCLCNV_L .EQ. 0 .OR. NCLCNV_L .EQ. NCLCNV)
D     ENDIF
      IF (NCLCND_L .EQ. 0) NCLCND_L = NCLCND
      IF (NCLCNV_L .EQ. 0) NCLCNV_L = NCLCNV
      
C---     Boucle sur les fichiers
      TMAX = -1.0D99
      NFIC = DS_LIST_REQDIM(HLIST)
!$    I_COMM = MP_UTIL_REQCOMM()
!$    CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
!$    DO_OMP = I_SIZE .LE. 1
!$omp parallel
!$omp& if(DO_OMP)
!$omp& private(I, IERR, FNAME)
!$omp& firstprivate(TMAX)
!$omp& default(shared)
!$omp do
      DO I=1,NFIC
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, I, FNAME)
         IF (ERR_GOOD())
     &      IERR = FD_COND_INI1FIC(HOBJ,
     &                             NCLCND,
     &                             NCLCNV,
     &                             TMAX,
     &                             I,
     &                             FNAME(1:SP_STRN_LEN(FNAME)))
      ENDDO
!$omp end do
      IERR = ERR_OMP_RDC()
!$omp end parallel

C---     Trie les données des sections
      IF (ERR_GOOD() .AND. NFIC .GT. 1) THEN
         IERR = FD_COND_TRISCT(HOBJ)
      ENDIF

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_COND_NCLCND(IOB) = NCLCND
         FD_COND_NCLCNV(IOB) = NCLCNV
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NBR_COND_INVALIDE', ' : ', NCLCND
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NCLCNV_INVALIDE', ' : ', NCLCNV
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_FICHIER_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,I12))') 'MSG_NCLCND', NCLCND, '/', NCLCND_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2(A,I12))') 'MSG_NCLCNV', NCLCNV, '/', NCLCNV_L
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE      
      FD_COND_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Trie les paramètres de section
C
C Description:
C     La méthode protégée FD_COND_TRISCT trie les paramètres de section
C     par ordre croissant de temps.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_TRISCT(HOBJ)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NSCT
      INTEGER NSMAX
      INTEGER LIFIC, LTEMP, LFPOS
      TYPE(SO_ALLC_PTR_T) :: PIFIC
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      INTEGER,   POINTER :: KIFIC(:)
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
      INTEGER, ALLOCATABLE :: KIDX(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_COND_HBASE
!      NSMAX = FD_COND_NSMAX(IOB)
      NSCT  = FD_COND_NSCT (IOB)
      LIFIC = FD_COND_LIFIC(IOB)
      LTEMP = FD_COND_LTEMP(IOB)
      LFPOS = FD_COND_LFPOS(IOB)

C---     Les tables
      PIFIC = SO_ALLC_PTR(LIFIC)
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      KIFIC => PIFIC%CST2K()
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Alloue la table d'indexation
      ALLOCATE(KIDX(NSCT))

C---     Génère la table d'index pour le tri
      CALL DINDEX(NSCT, VTMPS, KIDX)

C---     Trie les tables
      CALL ISWAPF(1, NSCT, KIFIC, KIDX)
      CALL DSWAPF(1, NSCT, VTMPS, KIDX)
      CALL XSWAPF(1, NSCT, KPOS,  KIDX)

C---     Désalloue la table d'indexation
      IF (ALLOCATED(KIDX)) DEALLOCATE(KIDX)

      FD_COND_TRISCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les paramètres d'une section.
C
C Description:
C     La fonction privée FD_COND_REQSCT retourne les paramètres
C     d'une section de la structure.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     TEMPS       Temps associé à la section
C     XPOS        Offset du début de la section
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQSCT (HOBJ, ISCT, TEMPS, XPOS)

      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER   ISCT
      REAL*8    TEMPS
      INTEGER*8 XPOS

      INCLUDE 'fdcond.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LFPOS
      INTEGER LTEMP
D     INTEGER NSCT
      TYPE(SO_ALLC_PTR_T) :: PTMPS
      TYPE(SO_ALLC_PTR_T) :: PPOS
      REAL*8,    POINTER :: VTMPS(:)
      INTEGER*8, POINTER :: KPOS (:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(ISCT .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - FD_COND_HBASE
D     NSCT  = FD_COND_NSCT (IOB)
      LTEMP = FD_COND_LTEMP(IOB)
      LFPOS = FD_COND_LFPOS(IOB)
D     CALL ERR_ASR(ISCT .GT. 0)

C---     Les tables
      PTMPS = SO_ALLC_PTR(LTEMP)
      PPOS  = SO_ALLC_PTR(LFPOS)
      VTMPS => PTMPS%CST2V()
      KPOS  => PPOS %CST2X()

C---     Les valeurs
      TEMPS = VTMPS(ISCT)
      XPOS  = KPOS (ISCT)

      FD_COND_REQSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les dimensions du fichier.
C
C Description:
C     La fonction privée FD_COND_REQDIM lis l'entête d'un fichier
C     et en retourne les dimensions.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NOMFIC         Nom du fichier
C
C Sortie:
C     NCLCND
C     NCLCNV
C
C Notes:
C
C************************************************************************
      FUNCTION FD_COND_REQDIM(HOBJ, FNAME, NCLCND, NCLCNV)

      IMPLICIT NONE

      INTEGER,       INTENT(IN)  :: HOBJ
      CHARACTER*(*), INTENT(IN)  :: FNAME
      INTEGER,       INTENT(OUT) :: NCLCND
      INTEGER,       INTENT(OUT) :: NCLCNV

      INCLUDE 'fdcond.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER   I_COMM, I_RANK, I_ERROR
      INTEGER, PARAMETER :: I_MASTER = 0

      REAL*8    TEMPS
      REAL*8    TMP(3)
      INTEGER*8 XFIC, XPOS
      INTEGER   IERR
      INTEGER   ILIGN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Lis l'entête de section
      XFIC = 0
      XPOS = 0
      ILIGN  = 0
      NCLCND = 0
      NCLCNV = 0
      TEMPS  = 0.0D0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD())
     &      IERR = FD_COND_OPEN(HOBJ,
     &                          FNAME(1:SP_STRN_LEN(FNAME)),
     &                          C_FA_LECTURE,
     &                          XPOS,
     &                          XFIC)
         IF (ERR_GOOD())
     &      IERR = FD_COND_RHDR(HOBJ,
     &                          XFIC,
     &                          ILIGN,
     &                          NCLCND,
     &                          NCLCNV,
     &                          TEMPS)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast tout
      IF (ERR_GOOD()) THEN
         TMP(1) = NCLCND
         TMP(2) = NCLCNV
         TMP(3) = TEMPS
         CALL MPI_BCAST(TMP, 3, MP_TYPE_RE8(),
     &                  I_MASTER, I_COMM, I_ERROR)
         NCLCND = NINT(TMP(1))
         NCLCNV = NINT(TMP(2))
         TEMPS  = TMP(3)
      ENDIF

C---     Ferme le fichier
      IF (XFIC .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = FD_COND_CLOSE(HOBJ, XFIC)
         CALL ERR_POP()
      ENDIF

      FD_COND_REQDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de CoNDitions limites
C
C Description:
C     La fonction FD_COND_REQNCLCND retourne le nombre de conditions
C     limites de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQNCLCND (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQNCLCND
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_COND_REQNCLCND = FD_COND_NCLCND(HOBJ-FD_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Valeurs de CoNDitions limites
C
C Description:
C     La fonction FD_COND_REQNCLCNV retourne le nombre de valeurs
C     de conditions limites de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQNCLCNV (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQNCLCNV
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_COND_REQNCLCNV = FD_COND_NCLCNV(HOBJ-FD_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de sections
C
C Description:
C     La fonction FD_COND_REQNSCT retourne le nombre de sections
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQNSCT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQNSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      FD_COND_REQNSCT = FD_COND_NSCT(HOBJ-FD_COND_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps min des sections
C
C Description:
C     La fonction FD_COND_REQTMIN retourne le temps associé à la
C     première section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQTMIN (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQTMIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'

      REAL*8    TEMPS
      INTEGER*8 XPOS
      INTEGER   ISCT
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISCT = 1
      IERR = FD_COND_REQSCT(HOBJ, ISCT, TEMPS, XPOS)

      FD_COND_REQTMIN = TEMPS
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps max des sections
C
C Description:
C     La fonction FD_COND_REQTMIN retourne le temps associé à la
C     dernière section.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQTMAX (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQTMAX
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'fdcond.fc'

      REAL*8    TEMPS
      INTEGER*8 XPOS
      INTEGER   ISCT
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      ISCT = FD_COND_REQNSCT(HOBJ)
      IERR = FD_COND_REQSCT (HOBJ, ISCT, TEMPS, XPOS)

      FD_COND_REQTMAX = TEMPS
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom du fichier.
C
C Description:
C     La fonction FD_COND_REQNOMF retourne le nom du fichier
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQNFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQNFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdcond.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER HLIST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HLIST = FD_COND_HLIST(HOBJ-FD_COND_HBASE)

      FD_COND_REQNFIC = DS_LIST_REQDIM(HLIST)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nom du fichier.
C
C Description:
C     La fonction FD_COND_REQNOMF retourne le nom du fichier
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_COND_REQNOMF(HOBJ, IFIC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_COND_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IFIC

      INCLUDE 'fdcond.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdcond.fc'

      INTEGER IERR
      INTEGER HLIST
      INTEGER L
      CHARACTER*256 BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_COND_HVALIDE(HOBJ))
D     CALL ERR_PRE(IFIC .GE. 1)
C------------------------------------------------------------------------

      HLIST = FD_COND_HLIST(HOBJ-FD_COND_HBASE)
D     CALL ERR_ASR(DS_LIST_HVALIDE(HLIST))
D     CALL ERR_ASR(IFIC .LE. DS_LIST_REQDIM(HLIST))
      IERR = DS_LIST_REQVAL(HLIST, IFIC, BUF)
      IF (ERR_GOOD()) THEN
         L = MIN(SP_STRN_LEN(BUF), 256)
         IF (L .EQ. 0) THEN
            FD_COND_REQNOMF  = ' '
         ELSE
            FD_COND_REQNOMF  = BUF(1:L)
         ENDIF
      ELSE
         FD_COND_REQNOMF  = ' '
      ENDIF

      RETURN
      END
