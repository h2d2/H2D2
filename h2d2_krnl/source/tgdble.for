C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_DBLE_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgdble.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(TG_DBLE_NOBJMAX,
     &                   TG_DBLE_HBASE,
     &                   'Trigger sur double')

      TG_DBLE_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_DBLE_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgdble.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER  IERR
      EXTERNAL TG_DBLE_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(TG_DBLE_NOBJMAX,
     &                   TG_DBLE_HBASE,
     &                   TG_DBLE_DTR)

      TG_DBLE_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_DBLE_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgdble.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   TG_DBLE_NOBJMAX,
     &                   TG_DBLE_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(TG_DBLE_HVALIDE(HOBJ))
         IOB = HOBJ - TG_DBLE_HBASE

         TG_DBLE_DTRG(IOB) = -1.0D0
         TG_DBLE_DINC(IOB) =  0.0D0
         TG_DBLE_DACC(IOB) =  0.0D0
      ENDIF

      TG_DBLE_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_DBLE_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgdble.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TG_DBLE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = TG_DBLE_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   TG_DBLE_NOBJMAX,
     &                   TG_DBLE_HBASE)

      TG_DBLE_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_DBLE_INI(HOBJ, DINC, DTRG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      REAL*8  DINC
      REAL*8  DTRG

      INCLUDE 'tgdble.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_DBLE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTRÔLES
      IF (DINC .LE. 0.0D0) GOTO 9900
      IF (DTRG .LE. 0.0D0) GOTO 9901

C---     RESET LES DONNEES
      IERR = TG_DBLE_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - TG_DBLE_HBASE

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         TG_DBLE_DTRG(IOB) = DTRG
         TG_DBLE_DINC(IOB) = DINC
         TG_DBLE_DACC(IOB) = 0.0d0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,1PE14.6E3)')'ERR_INCREMENT_INVALIDE',': ',DINC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,1PE14.6E3)')'ERR_SEUIL_INVALIDE',': ',DTRG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      TG_DBLE_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_DBLE_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgdble.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_DBLE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - TG_DBLE_HBASE

C---     RESET LES ATTRIBUTS
      TG_DBLE_DTRG(IOB) = -1.0D0
      TG_DBLE_DINC(IOB) =  0.0D0
      TG_DBLE_DACC(IOB) =  0.0D0

      TG_DBLE_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction TG_DBLE_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_DBLE_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgdble.fi'
      INCLUDE 'tgdble.fc'
C------------------------------------------------------------------------

      TG_DBLE_REQHBASE = TG_DBLE_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction TG_DBLE_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_DBLE_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgdble.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tgdble.fc'
C------------------------------------------------------------------------

      TG_DBLE_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  TG_DBLE_NOBJMAX,
     &                                  TG_DBLE_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction TG_DBLE_INC exécute le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_DBLE_INC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_DBLE_INC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgdble.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fc'

      INTEGER IOB
      REAL*8  REST
      LOGICAL LRET
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_DBLE_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - TG_DBLE_HBASE

      TG_DBLE_DACC(IOB) = TG_DBLE_DACC(IOB) + TG_DBLE_DINC(IOB)
D     CALL ERR_ASR(TG_DBLE_DACC(IOB) .GT. 0.0D0)

      LRET = .FALSE.
      IF (TG_DBLE_DTRG(IOB) .GT. 0) THEN
         IF (TG_DBLE_DACC(IOB) .GE. TG_DBLE_DTRG(IOB)) THEN
            REST = DMOD(TG_DBLE_DTRG(IOB), TG_DBLE_DACC(IOB))
            TG_DBLE_DACC(IOB) = REST
            LRET = .TRUE.
         ENDIF
      ENDIF

      TG_DBLE_INC = LRET
      RETURN
      END
