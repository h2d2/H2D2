C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numérotation
C Objet:   NUMeRotation
C Type:    Virtuel
C
C Class: NM_PRXY
C     INTEGER NM_PRXY_000
C     INTEGER NM_PRXY_999
C     INTEGER NM_PRXY_CTR
C     INTEGER NM_PRXY_DTR
C     INTEGER NM_PRXY_INI
C     INTEGER NM_PRXY_RST
C     INTEGER NM_PRXY_REQHBASE
C     LOGICAL NM_PRXY_HVALIDE
C     INTEGER NM_PRXY_CHARGE
C     INTEGER NM_PRXY_ISYNC
C     INTEGER NM_PRXY_DSYNC
C     INTEGER NM_PRXY_GENDSYNC
C     INTEGER NM_PRXY_GENISYNC
C     INTEGER NM_PRXY_REQNPRT
C     INTEGER NM_PRXY_REQNNL
C     INTEGER NM_PRXY_REQNNP
C     INTEGER NM_PRXY_REQNNT
C     LOGICAL NM_PRXY_ESTNOPP
C     INTEGER NM_PRXY_REQNEXT
C     INTEGER NM_PRXY_REQNINT
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les COMMON
C
C Description:
C     Le block data initialise les COMMON propres à NM_PRXY_DATA
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA NM_PRXY_DATA

      IMPLICIT NONE

      INCLUDE 'obobjc.fi'
      INCLUDE 'nmprxy.fc'

      DATA NM_PRXY_HBASE  / OB_OBJC_HBSP_TAG /

      END

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>NM_PRXY_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_PRXY_NOBJMAX,
     &                   NM_PRXY_HBASE,
     &                   'Proxy: Renumbering')

      NM_PRXY_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER  IERR
      EXTERNAL NM_PRXY_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_PRXY_NOBJMAX,
     &                   NM_PRXY_HBASE,
     &                   NM_PRXY_DTR)

      NM_PRXY_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Retourne un handle sur l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_PRXY_NOBJMAX,
     &                   NM_PRXY_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_PRXY_HVALIDE(HOBJ))
         IOB = HOBJ - NM_PRXY_HBASE

         NM_PRXY_HNUM  (IOB) = 0
         NM_PRXY_HMDL  (IOB) = 0
         NM_PRXY_HFNNT (IOB) = 0
         NM_PRXY_HFNNL (IOB) = 0
         NM_PRXY_HFNOPP(IOB) = 0
         NM_PRXY_HFNINT(IOB) = 0
         NM_PRXY_HFNEXT(IOB) = 0
      ENDIF

      NM_PRXY_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_PRXY_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_PRXY_NOBJMAX,
     &                   NM_PRXY_HBASE)

      NM_PRXY_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_INI(HOBJ, HNUM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_INI
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER  IOB
      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HFNNL, HFNNP, HFNNT, HFNOPP, HFNINT, HFNEXT
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
D     CALL ERR_PRE(HNUM .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_PRXY_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     CONNECTE AU MODULE
      IERR = SO_UTIL_REQHMDLHBASE(HNUM, HMDL)
D     CALL ERR_ASR(.NOT. ERR_GOOD() .OR. SO_MDUL_HVALIDE(HMDL))

C---     CONTROLE LE HANDLE
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC,
     &                                     SO_ALLC_CST2B(HNUM))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     CHARGE LES FONCTIONS
      HFNNL = 0
      HFNNP = 0
      HFNNT = 0
      HFNOPP= 0
      HFNINT= 0
      HFNEXT= 0
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNNL', HFNNL)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNNP', HFNNP)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNNT', HFNNT)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'ESTNOPP',HFNOPP)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNINT',HFNINT)
      IF (ERR_GOOD()) IERR = SO_MDUL_REQFNC(HMDL, 'REQNEXT',HFNEXT)

C---     AJOUTE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HNUM)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         NM_PRXY_HNUM  (IOB) = HNUM
         NM_PRXY_HMDL  (IOB) = HMDL
         NM_PRXY_HFNNL (IOB) = HFNNL
         NM_PRXY_HFNNP (IOB) = HFNNP
         NM_PRXY_HFNNT (IOB) = HFNNT
         NM_PRXY_HFNOPP(IOB) = HFNOPP
         NM_PRXY_HFNINT(IOB) = HFNINT
         NM_PRXY_HFNEXT(IOB) = HFNEXT
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HNUM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_PRXY_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_RST
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER  IOB
      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  HNUM
      INTEGER  HMDL
      INTEGER  HFNNL, HFNNP, HFNNT, HFNOPP, HFNINT, HFNEXT
      LOGICAL  HVALIDE
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     RECUPERE LES DONNEES
      HNUM   = NM_PRXY_HNUM  (IOB)
      HMDL   = NM_PRXY_HMDL  (IOB)
      HFNNL  = NM_PRXY_HFNNL (IOB)
      HFNNP  = NM_PRXY_HFNNP (IOB)
      HFNNT  = NM_PRXY_HFNNT (IOB)
      HFNOPP = NM_PRXY_HFNOPP(IOB)
      HFNINT = NM_PRXY_HFNINT(IOB)
      HFNEXT = NM_PRXY_HFNEXT(IOB)
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     ENLEVE LE LIEN
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     CONTROLE LE HANDLE MANAGÉ
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1(HFNC,
     &                                     SO_ALLC_CST2B(HNUM))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     LIBERE LES FONCTIONS
      IF (HFNNL  .NE. 0) IERR = SO_MDUL_DTR(HFNNL)
      IF (HFNNP  .NE. 0) IERR = SO_MDUL_DTR(HFNNP)
      IF (HFNNT  .NE. 0) IERR = SO_MDUL_DTR(HFNNT)
      IF (HFNOPP .NE. 0) IERR = SO_MDUL_DTR(HFNOPP)
      IF (HFNINT .NE. 0) IERR = SO_MDUL_DTR(HFNINT)
      IF (HFNEXT .NE. 0) IERR = SO_MDUL_DTR(HFNEXT)

C---     RESET
1000  CONTINUE
      NM_PRXY_HNUM  (IOB) = 0
      NM_PRXY_HMDL  (IOB) = 0
      NM_PRXY_HFNNL (IOB) = 0
      NM_PRXY_HFNNP (IOB) = 0
      NM_PRXY_HFNNT (IOB) = 0
      NM_PRXY_HFNOPP(IOB) = 0
      NM_PRXY_HFNINT(IOB) = 0
      NM_PRXY_HFNEXT(IOB) = 0

      NM_PRXY_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_PRXY_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmprxy.fc'
C------------------------------------------------------------------------

      NM_PRXY_REQHBASE = NM_PRXY_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_PRXY_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmprxy.fc'
C------------------------------------------------------------------------

      NM_PRXY_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_PRXY_NOBJMAX,
     &                                  NM_PRXY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_PRXY_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_TVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmprxy.fc'
C------------------------------------------------------------------------

      NM_PRXY_TVALIDE = OB_OBJC_TVALIDE(HOBJ,
     &                                  NM_PRXY_NOBJMAX,
     &                                  NM_PRXY_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_CHARGE (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL LOG_TODO('Fonction NM_PRXY_CHARGE non implantée')
      CALL ERR_ASR(.FALSE.)

      NM_PRXY_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_ISYNC (HOBJ, NVAL, NNL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_ISYNC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL(NVAL, NNL)

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNUM, HMDL, HFNC
      INTEGER NM_PRXY_ISYNC_CB
      EXTERNAL NM_PRXY_ISYNC_CB
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     RECUPERE LES DONNEES
      HNUM = NM_PRXY_HNUM(IOB)
      HMDL = NM_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ISYNC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC,
     &                                     SO_ALLC_CST2B(HNUM),
     &                                     SO_ALLC_CST2B(NVAL),
     &                                     SO_ALLC_CST2B(NNL),
     &                                     SO_ALLC_CST2B(KVAL(:,1)))

      NM_PRXY_ISYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_DSYNC (HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_DSYNC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      REAL*8  VVAL(NVAL, NNL)

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNUM, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     RECUPERE LES DONNEES
      HNUM = NM_PRXY_HNUM(IOB)
      HMDL = NM_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DSYNC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD())
     &   IERR = SO_FUNC_CALL4(HFNC,
     &                        SO_ALLC_CST2B(HNUM),
     &                        SO_ALLC_CST2B(NVAL),
     &                        SO_ALLC_CST2B(NNL),
     &                        SO_ALLC_CST2B(VVAL(:,1)))

      NM_PRXY_DSYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_PRXY_DSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_GENDSYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_GENDSYNC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER HSNC

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNUM, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     RECUPERE LES DONNEES
      HNUM = NM_PRXY_HNUM(IOB)
      HMDL = NM_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'GENDSYNC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC,
     &                                     SO_ALLC_CST2B(HNUM),
     &                                     SO_ALLC_CST2B(NVAL),
     &                                     SO_ALLC_CST2B(NNL),
     &                                     SO_ALLC_CST2B(HSNC))

      NM_PRXY_GENDSYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_PRXY_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_PRXY_GENISYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_GENISYNC
CDEC$ ENDIF

      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER HSNC

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNUM, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - NM_PRXY_HBASE

C---     RECUPERE LES DONNEES
      HNUM = NM_PRXY_HNUM(IOB)
      HMDL = NM_PRXY_HMDL(IOB)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     CHARGE LA FONCTION
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'GENISYNC', HFNC)

C---     FAIT L'APPEL
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL4(HFNC,
     &                                     SO_ALLC_CST2B(HNUM),
     &                                     SO_ALLC_CST2B(NVAL),
     &                                     SO_ALLC_CST2B(NNL),
     &                                     SO_ALLC_CST2B(HSNC))

      NM_PRXY_GENISYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: NM_PRXY_REQNPRT
C
C Description:
C     La fonction NM_PRXY_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNPRT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNPRT
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HNUM, HMDL, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM(IOB)
      HMDL = NM_PRXY_HMDL(IOB)
      IERR = SO_MDUL_CCHFNC(HMDL, 'REQNPRT', HFNC)

      NM_PRXY_REQNPRT = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HNUM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNNL (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNNL
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM (IOB)
      HFNC = NM_PRXY_HFNNL(IOB)

      NM_PRXY_REQNNL = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HNUM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNNP (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNNP
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM (IOB)
      HFNC = NM_PRXY_HFNNP(IOB)

      NM_PRXY_REQNNP = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HNUM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNNT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNNT
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM (IOB)
      HFNC = NM_PRXY_HFNNT(IOB)

      NM_PRXY_REQNNT = SO_FUNC_CALL1(HFNC, SO_ALLC_CST2B(HNUM))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_ESTNOPP(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_ESTNOPP
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER INLOC

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
      INTEGER IRET
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM  (IOB)
      HFNC = NM_PRXY_HFNOPP(IOB)

      NM_PRXY_ESTNOPP =
     &   (SO_FUNC_CALL2(HFNC,
     &                  SO_ALLC_CST2B(HNUM),
     &                  SO_ALLC_CST2B(INLOC)) .NE. 0)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNEXT(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNEXT
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER INLOC

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM  (IOB)
      HFNC = NM_PRXY_HFNEXT(IOB)

      NM_PRXY_REQNEXT = SO_FUNC_CALL2(HFNC,
     &                                SO_ALLC_CST2B(HNUM),
     &                                SO_ALLC_CST2B(INLOC))
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_PRXY_REQNINT(HOBJ, INGLB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_PRXY_REQNINT
CDEC$ ENDIF

      USE SO_ALLC_M
      USE SO_FUNC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER INGLB

      INCLUDE 'nmprxy.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmprxy.fc'

      INTEGER IOB
      INTEGER HNUM, HFNC
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_PRXY_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - NM_PRXY_HBASE
      HNUM = NM_PRXY_HNUM  (IOB)
      HFNC = NM_PRXY_HFNINT(IOB)

      NM_PRXY_REQNINT = SO_FUNC_CALL2(HFNC,
     &                                SO_ALLC_CST2B(HNUM),
     &                                SO_ALLC_CST2B(INGLB))
      RETURN
      END
