C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-GLobal
C Type:
C
C Class: NM_LCGL
C     INTEGER NM_LCGL_000
C     INTEGER NM_LCGL_999
C     INTEGER NM_LCGL_CTR
C     INTEGER NM_LCGL_DTR
C     INTEGER NM_LCGL_INI
C     INTEGER NM_LCGL_RST
C     INTEGER NM_LCGL_REQHBASE
C     INTEGER NM_LCGL_REQHBASE
C     LOGICAL NM_LCGL_HVALIDE
C     INTEGER NM_LCGL_CHARGE
C     INTEGER NM_LCGL_LCLGLB
C     INTEGER NM_LCGL_PRCECR
C     INTEGER NM_LCGL_DIMTBL
C     INTEGER NM_LCGL_TBLECR
C     INTEGER NM_LCGL_PRNDST
C     LOGICAL NM_LCGL_ESTNOPP
C     INTEGER NM_LCGL_REQNPRT
C     INTEGER NM_LCGL_REQNNL
C     INTEGER NM_LCGL_REQNNP
C     INTEGER NM_LCGL_REQNNT
C     INTEGER NM_LCGL_REQNEXT
C     INTEGER NM_LCGL_REQNINT
C     INTEGER NM_LCGL_REQGLB
C     INTEGER NM_LCGL_REQLCL
C     INTEGER NM_LCGL_DSYNC
C     INTEGER NM_LCGL_ISYNC
C     INTEGER NM_LCGL_GENDSYNC
C     INTEGER NM_LCGL_GENISYNC
C     INTEGER NM_LCGL_GENSYNC
C
C************************************************************************

      !SUBMODULE (NM_LCGL_M) NM_LCGL_M
      MODULE NM_LCGL_HOBJ_M

      USE NM_LCGL_M, ONLY : NM_LCGL_T
      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: NM_LCGL_HBASE = 0

      CONTAINS
      
C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée NM_LCGL_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQSELF(HOBJ)
         
      USE ISO_C_BINDING

      TYPE (NM_LCGL_T), POINTER :: NM_LCGL_REQSELF
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

D     CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
D     CALL ERR_ASR(ERR_GOOD())
D     CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      NM_LCGL_REQSELF => SELF
      RETURN
      END FUNCTION NM_LCGL_REQSELF

      END MODULE NM_LCGL_HOBJ_M
      
C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_LCGL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_CTR
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      USE NM_LCGL_M, ONLY : NM_LCGL_T, NM_LCGL_CTR_SELF
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER, INTENT(OUT) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NM_LCGL_CTR_SELF()

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   NM_LCGL_HBASE,
     &                                   C_LOC(SELF))

      NM_LCGL_CTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_CTR

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_LCGL_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_DTR
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      USE NM_LCGL_M, ONLY: DEL
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_LCGL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      IERR = DEL(SELF)

      IERR = OB_OBJN_DTR(HOBJ, NM_LCGL_HBASE)

      NM_LCGL_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_DTR

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La méthode NM_LCGL_INI initialise l'objet à partir soit du nom de
C     fichier, soit des dimensions et du pointeur à la table. Pour initialiser
C     l'objet avec le nom de fichier, il faut mettre les autres
C     paramètres à 0. Sinon, il faut donner une chaîne vide pour le nom
C     de fichier.
C
C Entrée:
C     NOMFIC      Nom du fichier contenant la table
C     NNL_P       Le nombre d'items locaux
C     NNT_P       Le nombre d'items globaux
C     NPROC_P     Le nombre de sous-domaines
C     LDIST_P     Le pointeur à la table de distribution des items
C
C Sortie:
C     HOBJ        Le handle de l'objet initialisé
C
C Notes:
C     La table est passée par pointeur pour permettre un pointeur nul.
C************************************************************************
      FUNCTION NM_LCGL_INI(HOBJ,
     &                    NOMFIC,
     &                    NNL_P,
     &                    NNT_P,
     &                    NPROC_P,
     &                    LDIST_P)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_INI
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      CHARACTER*(*), INTENT(IN) :: NOMFIC
      INTEGER, INTENT(IN) :: NNL_P
      INTEGER, INTENT(IN) :: NNT_P
      INTEGER, INTENT(IN) :: NPROC_P
      INTEGER, INTENT(IN) :: LDIST_P

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_INI = SELF%INI(NOMFIC, NNL_P, NNT_P, NPROC_P, LDIST_P)
      RETURN
      END FUNCTION NM_LCGL_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_LCGL_RST(HOBJ)

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_RST = SELF%RST()
      RETURN
      END FUNCTION NM_LCGL_RST

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_LCGL_CHARGE charge les tables internes des
C     paramètres de configuration.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NPROC          Nombre de process
C     NNL            Nombre de noeuds locaux
C     KDISTR         Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_CHARGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_CHARGE
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_CHARGE = SELF%CHARGE()
      RETURN
      END FUNCTION NM_LCGL_CHARGE
      
C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_LCGL_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_DSYNC(HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_DSYNC
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(NVAL, NNL)

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_DSYNC = SELF%DSYNC(NVAL, NNL, VVAL)
      RETURN
      END FUNCTION NM_LCGL_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_LCGL_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_ISYNC(HOBJ, NVAL, NNL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_ISYNC
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(NVAL, NNL)

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_ISYNC = SELF%ISYNC(NVAL, NNL, KVAL)
      RETURN
      END FUNCTION NM_LCGL_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCGL_GENDSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_GENDSYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_GENDSYNC
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_LCGL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_GENDSYNC = SELF%GENDSYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_LCGL_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCGL_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_GENISYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_GENISYNC
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------
      
      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_GENISYNC = SELF%GENISYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_LCGL_GENISYNC
      
C************************************************************************
C Sommaire: NM_LCGL_ESTNOPP
C
C Description:
C     La fonction NM_LCGL_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_ESTNOPP(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_ESTNOPP
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_ESTNOPP = SELF%ESTNOPP(INLOC)
      RETURN
      END FUNCTION NM_LCGL_ESTNOPP

C************************************************************************
C Sommaire: NM_LCGL_REQNPRT
C
C Description:
C     La fonction NM_LCGL_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNPRT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNPRT
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNPRT = SELF%REQNPRT()
      RETURN
      END FUNCTION NM_LCGL_REQNPRT

C************************************************************************
C Sommaire: NM_LCGL_REQNNL
C
C Description:
C     La fonction NM_LCGL_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNNL
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNNL = SELF%REQNNL()
      RETURN
      END FUNCTION NM_LCGL_REQNNL

C************************************************************************
C Sommaire: NM_LCGL_REQNNP
C
C Description:
C     La fonction NM_LCGL_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNNP
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNNP = SELF%REQNNP()
      RETURN
      END FUNCTION NM_LCGL_REQNNP

C************************************************************************
C Sommaire: NM_LCGL_REQNNT
C
C Description:
C     La fonction NM_LCGL_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNNT
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNNT = SELF%REQNNT()
      RETURN
      END FUNCTION NM_LCGL_REQNNT

C************************************************************************
C Sommaire: NM_LCGL_REQNEXT
C
C Description:
C     La fonction NM_LCGL_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNEXT(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNEXT
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNEXT = SELF%REQNEXT(INLOC)
      RETURN 
      END FUNCTION NM_LCGL_REQNEXT

C************************************************************************
C Sommaire: NM_LCGL_REQNINT
C
C Description:
C     La fonction NM_LCGL_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C     En fait, pour accélérer, il faudrait monter
C     la table inverse.
C************************************************************************
      FUNCTION NM_LCGL_REQNINT(HOBJ, INGLB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQNINT
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INGLB

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'err.fi'
      
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCGL_REQSELF(HOBJ)
      NM_LCGL_REQNINT = SELF%REQNINT(INGLB)
      RETURN
      END FUNCTION NM_LCGL_REQNINT

     
C************************************************************************
C************************************************************************
C************************************************************************
C************************************************************************
      
      
C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction NM_LCGL_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_000
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(NM_LCGL_HBASE,
     &                   'Renumbering - Local to Global')

      NM_LCGL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction NM_LCGL_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_999
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL NM_LCGL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(NM_LCGL_HBASE,
     &                   NM_LCGL_DTR)

      NM_LCGL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_LCGL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_REQHBASE
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlcgl.fi'
C------------------------------------------------------------------------

      NM_LCGL_REQHBASE = NM_LCGL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_LCGL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_HVALIDE
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_LCGL_HVALIDE = OB_OBJN_HVALIDE(HOBJ, NM_LCGL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_LCGL_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCGL_TVALIDE
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmlcgl.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_LCGL_TVALIDE = OB_OBJN_TVALIDE(HOBJ, NM_LCGL_HBASE)
      RETURN
      END
