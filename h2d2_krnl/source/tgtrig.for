C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: TG_TRIG_DTR
C
C Description:
C     La fonction TG_TRIG_DTR agit comme destructeur virtuel de la
C     structure de classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_TRIG_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_TRIG_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgtrig.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'tgdble.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (TG_INTG_HVALIDE(HOBJ)) THEN
         IERR = TG_INTG_DTR(HOBJ)
      ELSEIF (TG_DBLE_HVALIDE(HOBJ)) THEN
         IERR = TG_DBLE_DTR(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      TG_TRIG_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction TG_TRIG_CTRLH permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_TRIG_CTRLH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_TRIG_CTRLH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgtrig.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'tgdble.fi'

      LOGICAL VALIDE
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      VALIDE = .FALSE.
      IF     (TG_INTG_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (TG_DBLE_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ENDIF

      TG_TRIG_CTRLH = VALIDE
      RETURN
      END

C************************************************************************
C Sommaire: TG_TRIG_INC
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_TRIG_INC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_TRIG_INC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgtrig.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fi'
      INCLUDE 'tgdble.fi'

      LOGICAL LRET
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF     (TG_INTG_HVALIDE(HOBJ)) THEN
         LRET = TG_INTG_INC(HOBJ)
      ELSEIF (TG_DBLE_HVALIDE(HOBJ)) THEN
         LRET = TG_DBLE_INC(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      TG_TRIG_INC = LRET
      RETURN
      END
