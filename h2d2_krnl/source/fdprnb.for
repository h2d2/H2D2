C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Le fichier peut s'employer pour toute donnée réelle, stockée par
C     bloc pour chaque pas de temps, avec renumérotation; donc autant
C     pour des valeurs nodales que pour des valeurs élémentaires.
C     <p>
C     nlines ncols time
C     v_1 v_2 v_3 ... v_ncols
C
C Functions:
C   Public:
C     INTEGER FD_PRNB_000
C     INTEGER FD_PRNB_999
C     INTEGER FD_PRNB_PKL
C     INTEGER FD_PRNB_UPK
C     INTEGER FD_PRNB_CTR
C     INTEGER FD_PRNB_DTR
C     INTEGER FD_PRNB_INI
C     INTEGER FD_PRNB_RST
C     INTEGER FD_PRNB_REQHBASE
C     LOGICAL FD_PRNB_HVALIDE
C     INTEGER FD_PRNB_ECRSCT
C     INTEGER FD_PRNB_LISSCT
C   Private:
C     INTEGER FD_PRNB_INISTR
C     INTEGER FD_PRNB_INI1FIC
C     INTEGER FD_PRNB_REQDIM
C     INTEGER FD_PRNB_INITYP
C     INTEGER FD_PRNB_RSTTYP
C     INTEGER FD_PRNB_INIIDX
C     INTEGER FD_PRNB_RSTIDX
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_PRNB_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprnb.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_PRNB_NOBJMAX,
     &                   FD_PRNB_HBASE,
     &                   'Binary Nodal Properties File')

      FD_PRNB_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_PRNB_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprnb.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER  IERR
      EXTERNAL FD_PRNB_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_PRNB_NOBJMAX,
     &                   FD_PRNB_HBASE,
     &                   FD_PRNB_DTR)

      FD_PRNB_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - FD_PRNB_HBASE
      HPRNT = FD_PRNB_HPRNT(IOB)

C---     ECRIS LES DONNÉES
      IERR = IO_XML_WH_A(HXML, HPRNT)

      FD_PRNB_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_PRNB_HBASE

C---     Les données
      IERR = IO_XML_RH_A(HXML, FD_PRNB_HPRNT(IOB))

      FD_PRNB_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_PRNB_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprnb.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   FD_PRNB_NOBJMAX,
     &                   FD_PRNB_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(FD_PRNB_HVALIDE(HOBJ))
         IOB = HOBJ - FD_PRNB_HBASE

         FD_PRNB_HPRNT(IOB) = 0
         FD_PRNB_HNUMR(IOB) = 0
         FD_PRNB_LINDX(IOB) = 0
         FD_PRNB_LDISP(IOB) = 0
         FD_PRNB_TPHDR(IOB) = 0
         FD_PRNB_TPROW(IOB) = 0
         FD_PRNB_TPDTA(IOB) = 0
      ENDIF

      FD_PRNB_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_PRNB_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprnb.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_PRNB_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_PRNB_NOBJMAX,
     &                   FD_PRNB_HBASE)

      FD_PRNB_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_PRNB_INI initialise l'objet à l'aide des
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HPRNT       Handle sur le parent
C     NOMFIC      Nom du fichier de propriétés nodales
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_INI(HOBJ, HPRNT, HLIST, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRNT
      INTEGER HLIST
      INTEGER ISTAT

      INCLUDE 'fdprnb.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I_HDLR, I_ERROR
      
      EXTERNAL MP_UTIL_ERRHNDLR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HPRNT))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.binary')

C---     Contrôle les paramètres
      IF (DS_LIST_REQDIM(HLIST) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901
      IF (.NOT. IO_UTIL_MODACTIF(ISTAT, IO_MODE_BINAIRE)) GOTO 9901

C---     Reset les données
      IERR = FD_PRNB_RST(HOBJ)

C---     MPI default file error handler
      I_HDLR = 0
      CALL MPI_FILE_CREATE_ERRHANDLER(MP_UTIL_ERRHNDLR, I_HDLR, I_ERROR)
      CALL MPI_FILE_SET_ERRHANDLER(MPI_FILE_NULL, I_HDLR, I_ERROR)
      
C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_PRNB_HBASE
         FD_PRNB_HPRNT(IOB) = HPRNT
         FD_PRNB_EHDLR(IOB) = I_HDLR
      ENDIF

C---     Initialise la structure interne en lecture
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNB_INISTR(HOBJ, HLIST)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I3)') 'ERR_PARAMETRE_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.binary')
      FD_PRNB_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_PRNB_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGEr I_HDLR, I_ERROR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_PRNB_HBASE

C---     Reset les types
      IERR = FD_PRNB_RSTTYP(HOBJ)
      IERR = FD_PRNB_RSTIDX(HOBJ)

C---     Reset MPI file error handler 
      I_HDLR = FD_PRNB_EHDLR(IOB)
      IF (I_HDLR .NE. 0) CALL MPI_ERRHANDLER_FREE(I_HDLR, I_ERROR)
      
C---     Reset les attributs
      FD_PRNB_HPRNT(IOB) = 0
      FD_PRNB_HNUMR(IOB) = 0
      FD_PRNB_EHDLR(IOB) = 0
      FD_PRNB_LINDX(IOB) = 0
      FD_PRNB_LDISP(IOB) = 0
      FD_PRNB_TPHDR(IOB) = 0
      FD_PRNB_TPROW(IOB) = 0
      FD_PRNB_TPDTA(IOB) = 0

      FD_PRNB_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_PRNB_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprnb.fi'
      INCLUDE 'fdprnb.fc'
C------------------------------------------------------------------------

      FD_PRNB_REQHBASE = FD_PRNB_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_PRNB_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprnb.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdprnb.fc'
C------------------------------------------------------------------------

      FD_PRNB_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_PRNB_NOBJMAX,
     &                                  FD_PRNB_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNB_ECRIS
C
C Description:
C     La fonction FD_PRNB_ECRIS est la fonction principale d'écriture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNT         Nombre de noeuds total
C     NNL         Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C     Il y a une fragilité avec la numérotation. On prend pour acquis
C     que la numérotation HNUMR est la même si les dimensions NNT, NPRN
C     ne sont pas modifiées
C************************************************************************
      FUNCTION FD_PRNB_ECRSCT(HOBJ,
     &                        FNAME,
     &                        ISTAT,
     &                        HNUMR,
     &                        NNT,
     &                        NNL,
     &                        NPRN,
     &                        VPRNO,
     &                        TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_ECRSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT
      INTEGER       HNUMR
      INTEGER       NNT
      INTEGER       NNL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNL)
      REAL*8        TSIM

      INCLUDE 'fdprnb.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER*8 I_OFF, I_DISP
      INTEGER   I_ERROR
      INTEGER   I_COMM, I_FILE, I_MODE
      INTEGER   I_STATUS(MPI_STATUS_SIZE)

      INTEGER IERR
      INTEGER IOB
      INTEGER IFEH, IHDR, IROW, IDTA
      INTEGER LDSP, LIDX
      REAL*8  HDR(3)
      LOGICAL SWAP_DONE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NNT  .GE. NNL)
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.binary.write')

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      IFEH = FD_PRNB_EHDLR(IOB)

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()

C---     Le mode d'écriture
      I_MODE = 0
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
         I_MODE = MPI_MODE_WRONLY + MPI_MODE_CREATE
      ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT)) THEN
         I_MODE = MPI_MODE_WRONLY + MPI_MODE_CREATE + MPI_MODE_APPEND
      ENDIF

C---     Assigne le contexte
      IERR = MP_UTIL_ASGERRCTX('Writing '//FNAME(1:SP_STRN_LEN(FNAME)))
      
C---     Ouvre le fichier
      I_FILE = 0
      CALL MPI_FILE_OPEN(I_COMM,
     &                   FNAME(1:SP_STRN_LEN(FNAME)),
     &                   I_MODE,
     &                   MPI_INFO_NULL,  !! Pourrait être creusé
     &                   I_FILE,
     &                   I_ERROR)

C---     Tronque le fichier en mode IO_MODE_ECRITURE
      IF (ERR_GOOD()) THEN
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
            I_OFF = 0
            CALL MPI_FILE_SET_SIZE(I_FILE, I_OFF, I_ERROR)
         ENDIF
      ENDIF

C---     Au besoin, génère les types et index
      IF (HNUMR .NE. FD_PRNB_HNUMR(IOB)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNB_RSTTYP(HOBJ)
         IF (ERR_GOOD()) IERR = FD_PRNB_RSTIDX(HOBJ)
         IF (ERR_GOOD()) IERR = FD_PRNB_INIIDX(HOBJ, HNUMR)
         IF (ERR_GOOD()) IERR = FD_PRNB_INITYP(HOBJ, NPRN, NNL)
      ENDIF

C---     Récupère les types et tables
      IF (ERR_GOOD()) THEN
         IHDR = FD_PRNB_TPHDR(IOB)
         IROW = FD_PRNB_TPROW(IOB)
         IDTA = FD_PRNB_TPDTA(IOB)
         LDSP = FD_PRNB_LDISP(IOB)
         LIDX = FD_PRNB_LINDX(IOB)
D        CALL ERR_ASR(IHDR.NE.IROW)
D        CALL ERR_ASR(IHDR.NE.IDTA)
D        CALL ERR_ASR(IROW.NE.IDTA)
      ENDIF

C---     Swap VPRNO selon la table d'index
      SWAP_DONE = .FALSE.
      IF (ERR_GOOD()) THEN
         SWAP_DONE = .TRUE.
         CALL DSWAPF(NPRN,
     &               NNL,
     &               VPRNO,
     &               KA(SO_ALLC_REQKIND(KA, LIDX)))
      ENDIF

C---     Récupère la position initiale
      I_DISP = 0
      IF (ERR_GOOD()) CALL MP_UTIL_FILE_GET_POSITION(I_FILE,
     &                                               I_OFF,     ! Offset
     &                                               I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_GET_BYTE_OFFSET (I_FILE,
     &                                               I_OFF,     ! Offset
     &                                               I_DISP,    ! Displacement
     &                                               I_ERROR)

C---     Écris l'entête
      HDR(1) = NNT
      HDR(2) = NPRN
      HDR(3) = TSIM
      IF (ERR_GOOD()) CALL MPI_FILE_SET_VIEW (I_FILE,
     &                                        I_DISP,          ! Displacement
     &                                        IHDR,
     &                                        IHDR,
     &                                        'native',
     &                                         MPI_INFO_NULL,  ! Pourrait être creusé
     &                                        I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_WRITE_ALL(I_FILE,
     &                                        HDR,
     &                                        1,               ! 1 element of type IHDR
     &                                        IHDR,
     &                                        I_STATUS,
     &                                        I_ERROR)

C---     Récupère la position après l'entête
      IF (ERR_GOOD()) CALL MP_UTIL_FILE_GET_POSITION(I_FILE,
     &                                               I_OFF,     ! Offset header
     &                                               I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_GET_BYTE_OFFSET (I_FILE,
     &                                               I_OFF,     ! Offset header
     &                                               I_DISP,    ! Displacement
     &                                               I_ERROR)

C---     Écris les données
      IF (ERR_GOOD()) CALL MPI_FILE_SET_VIEW (I_FILE,
     &                                        I_DISP,          ! Offset
     &                                        IROW,
     &                                        IDTA,
     &                                        'native',
     &                                        MPI_INFO_NULL,   ! Pourrait être creusé
     &                                        I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_WRITE_ALL(I_FILE,
     &                                        VPRNO,
     &                                        NNL,
     &                                        IROW,
     &                                        I_STATUS,
     &                                        I_ERROR)

C---     Rétablis l'ordre original
      IF (SWAP_DONE) THEN
         CALL DSWAPB(NPRN,
     &               NNL,
     &               VPRNO,
     &               KA(SO_ALLC_REQKIND(KA, LIDX)))
      ENDIF

C---     Ferme le fichier
      IF (I_FILE .NE. 0) THEN
         CALL ERR_PUSH()
         CALL MPI_FILE_CLOSE(I_FILE, I_ERROR)
         CALL ERR_POP()
      ENDIF
      
C---     Reset le contexte
      IERR = MP_UTIL_ASGERRCTX(' ')

      CALL TR_CHRN_STOP('h2d2.io.prno.binary.write')
      FD_PRNB_ECRSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNB_LISSCT
C
C Description:
C     La fonction privée FD_PRNB_LISSCT lis une section d'un fichier
C     de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     IFIC        Indice du fichier
C     I_DTOP      Position dans le fichier (Data TOP)
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de Noeuds Total Local au process
C     NPRN        Nombre de propriétés
C
C Sortie:
C     VPRNO       Table des valeurs lues
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_LISSCT(HOBJ,
     &                        FNAME,
     &                        XPOS,
     &                        TEMPS,
     &                        HNUMR,
     &                        NNTL,
     &                        NPRN,
     &                        VPRNO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNB_LISSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER*8     XPOS      ! Offset dans le fichier
      REAL*8        TEMPS     ! Pour contrôles
      INTEGER       HNUMR
      INTEGER       NNTL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNTL)

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER*8 I_OFF, I_DISP
      INTEGER I_COMM, I_FILE, I_ERROR
      INTEGER I_STATUS(MPI_STATUS_SIZE)

      REAL*8  HDR(3)
      INTEGER IERR
      INTEGER IOB
      INTEGER IFEH, IHDR, IROW, IDTA
      INTEGER HPRNT
      INTEGER LDSP, LIDX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNTL .GT. 1)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.binary.read')

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      IFEH = FD_PRNB_EHDLR(IOB)

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()

C---     Au besoin, génère les types et index
      IF (HNUMR .NE. FD_PRNB_HNUMR(IOB)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNB_RSTTYP(HOBJ)
         IF (ERR_GOOD()) IERR = FD_PRNB_RSTIDX(HOBJ)
         IF (ERR_GOOD()) IERR = FD_PRNB_INIIDX(HOBJ, HNUMR)
         IF (ERR_GOOD()) IERR = FD_PRNB_INITYP(HOBJ, NPRN, NNTL)
      ENDIF

C---     Récupère les types et tables
      IF (ERR_GOOD()) THEN
         IHDR = FD_PRNB_TPHDR(IOB)
         IROW = FD_PRNB_TPROW(IOB)
         IDTA = FD_PRNB_TPDTA(IOB)
         LDSP = FD_PRNB_LDISP(IOB)
         LIDX = FD_PRNB_LINDX(IOB)
D        CALL ERR_ASR(IHDR.NE.IROW)
D        CALL ERR_ASR(IHDR.NE.IDTA)
D        CALL ERR_ASR(IROW.NE.IDTA)
      ENDIF

C---     Assigne le contexte
      IERR = MP_UTIL_ASGERRCTX('Reading '//FNAME(1:SP_STRN_LEN(FNAME)))
      
C---     Ouvre le fichier
      I_FILE = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_OPEN(I_COMM,
     &                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                      MPI_MODE_RDONLY,
     &                      MPI_INFO_NULL,  !! Pourrait être creusé
     &                      I_FILE,
     &                      I_ERROR)
      ENDIF

C---     Positionne et lis
      I_DISP = XPOS
      IF (ERR_GOOD()) CALL MPI_FILE_SET_VIEW(I_FILE,
     &                                       I_DISP,           ! Displacement
     &                                       IHDR,
     &                                       IHDR,
     &                                       'native',
     &                                       MPI_INFO_NULL,    ! Pourrait être creusé
     &                                       I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_READ_ALL(I_FILE,
     &                                       HDR,
     &                                       1,                ! 1 element of type IHDR
     &                                       IHDR,
     &                                       I_STATUS,
     &                                       I_ERROR)

      IF (ERR_GOOD()) CALL MP_UTIL_FILE_GET_POSITION(I_FILE,
     &                                               I_OFF,    ! Offset header
     &                                               I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_GET_BYTE_OFFSET (I_FILE,
     &                                               I_OFF,    ! Offset header
     &                                               I_DISP,   ! Displacement
     &                                               I_ERROR)

      IF (ERR_GOOD()) CALL MPI_FILE_SET_VIEW(I_FILE,
     &                                       I_DISP,           ! Displacement
     &                                       IROW,
     &                                       IDTA,
     &                                       'native',
     &                                       MPI_INFO_NULL,    ! Pourrait être creusé
     &                                       I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_READ_ALL(I_FILE,
     &                                       VPRNO,
     &                                       NNTL,
     &                                       IROW,
     &                                       I_STATUS,
     &                                       I_ERROR)

C---     Rétablis l'ordre original
      IF (ERR_GOOD()) THEN
         CALL DSWAPB(NPRN,
     &               NNTL,
     &               VPRNO,
     &               KA(SO_ALLC_REQKIND(KA, LIDX)))
      ENDIF

C---     Nettoie
      IF (I_FILE .NE. 0) THEN
         CALL ERR_PUSH()
         CALL MPI_FILE_CLOSE(I_FILE, I_ERROR)
         CALL ERR_POP()
      ENDIF

C---     Reset le contexte
      IERR = MP_UTIL_ASGERRCTX(' ')

      CALL TR_CHRN_STOP('h2d2.io.prno.binary.read')
      FD_PRNB_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_PRNB_INISTR initialise la structure interne à
C     partir de la liste des fichiers passée en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLIST       Handle sur la liste des fichiers
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_INISTR(HOBJ, HLIST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLIST

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB, IOP
      INTEGER I
      INTEGER HPRNT, HNUMR
      INTEGER NPRN, NNT
      INTEGER NPRN_L, NNT_L
      CHARACTER*256 FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      HPRNT = FD_PRNB_HPRNT(IOB)

C---     Récupère les attributs du parent
      IOP = HPRNT - FD_PRNO_HBASE
      NNT_L  = FD_PRNO_NNT  (IOP)
      NPRN_L = FD_PRNO_NPRN (IOP)

C---     Crée une numérotation bidon à un seul noeud
      HNUMR = 0
      IF (ERR_GOOD()) IERR = NM_IDEN_CTR(HNUMR)
      IF (ERR_GOOD()) IERR = NM_IDEN_INI(HNUMR, 1)

C---     Initialise des types temporaires (1 row, 1 col)
      IF (ERR_GOOD()) IERR = FD_PRNB_RSTTYP(HOBJ)
      IF (ERR_GOOD()) IERR = FD_PRNB_RSTIDX(HOBJ)
      IF (ERR_GOOD()) IERR = FD_PRNB_INIIDX(HOBJ, HNUMR)
      IF (ERR_GOOD()) IERR = FD_PRNB_INITYP(HOBJ, 1, 1)

C---     Lis les dimensions du premier fichier
      NPRN = 0
      NNT  = 0
      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, 1, FNAME)
      IF (ERR_GOOD()) IERR = FD_PRNB_REQDIM(HOBJ, NPRN, NNT, FNAME)
      IF (ERR_GOOD() .AND. NPRN .LE. 0) GOTO 9900
      IF (ERR_GOOD() .AND. NNT  .LE. 0) GOTO 9901
D     IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NNT_L  .EQ. 0 .OR. NNT_L  .EQ. NNT)
D        CALL ERR_ASR(NPRN_L .EQ. 0 .OR. NPRN_L .EQ. NPRN)
D     ENDIF
      IF (NNT_L  .EQ. 0) NNT_L  = NNT
      IF (NPRN_L .EQ. 0) NPRN_L = NPRN

C---     Initialise des types temporaires (1 row, N col)
      IF (ERR_GOOD()) IERR = FD_PRNB_RSTTYP(HOBJ)
      IF (ERR_GOOD()) IERR = FD_PRNB_INITYP(HOBJ, NPRN, 1)

C---     Boucle sur les fichiers
      DO I=1,DS_LIST_REQDIM(HLIST)
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, I, FNAME)
         IF (ERR_GOOD())
     &      IERR = FD_PRNB_INI1FIC(HOBJ,
     &                             NPRN,
     &                             NNT,
     &                             I,
     &                             FNAME(1:SP_STRN_LEN(FNAME)))
         IF (ERR_BAD()) EXIT
      ENDDO

C---     Nettoie
      CALL ERR_PUSH()
      IERR = FD_PRNB_RSTTYP(HOBJ)
      IERR = FD_PRNB_RSTIDX(HOBJ)
      IF (NM_IDEN_HVALIDE(HNUMR)) IERR = NM_IDEN_DTR(HNUMR)
      CALL ERR_POP()

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_PRNO_NNT  (IOP) = NNT      ! Parent
         FD_PRNO_NPRN (IOP) = NPRN
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NBR_PRNO_INVALIDE', ' : ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNT_INVALIDE', ' : ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNB_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure pour un fichier
C
C Description:
C     La fonction privée FD_PRNB_INI1FIC initialise la structure interne
C     pour un seule fichier, dont l'indice et le nom sont passée en
C     paramètres.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ                    Handle sur l'objet
C     NPRN                    Nombre de PRopriétés Nodales
C     NNT                     Nombre de Noeuds Total
C     IFIC                    Indice du fichier dans la liste
C     FNAME                   Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_INI1FIC(HOBJ, NPRN, NNT, IFIC, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRN
      INTEGER NNT
      INTEGER IFIC
      CHARACTER*(*) FNAME

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprnb.fc'

      INTEGER*8 I_FSIZE, I_HSIZE, I_DSIZE, I_BSIZE
      INTEGER*8 I_DSP
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      INTEGER I_COMM, I_ERROR
      INTEGER I_FILE

      INTEGER IERR
      INTEGER IOB
      INTEGER IFEH, IHDR, IROW, IDTA
      INTEGER IB
      INTEGER HPRNT
      INTEGER NBLK
      INTEGER NPRN_L, NNT_L
      REAL*8  HDR( 3)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      HPRNT = FD_PRNB_HPRNT(IOB)

C---     Récupère les types et handler MPI
      IFEH = FD_PRNB_EHDLR(IOB)
      IHDR = FD_PRNB_TPHDR(IOB)
      IROW = FD_PRNB_TPROW(IOB)
D     IDTA = FD_PRNB_TPDTA(IOB)
D     CALL ERR_ASR((IHDR.EQ.0 .AND. IROW.EQ.0 .AND. IDTA.EQ.0) .OR.
D    &             (IHDR.NE.IROW .AND. IHDR.NE.IDTA .AND. IROW.NE.IDTA))

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()

C---     Assigne le contexte
      IERR = MP_UTIL_ASGERRCTX('Scanning '//FNAME(1:SP_STRN_LEN(FNAME)))
      
C---     Ouvre le fichier
      I_FILE = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_OPEN(I_COMM,
     &                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                      MPI_MODE_RDONLY,
     &                      MPI_INFO_NULL,  !! Pourrait être creusé
     &                      I_FILE,
     &                      I_ERROR)
      ENDIF

C---     Les dimensions (en bytes)
      I_FSIZE = 0
      I_HSIZE = 0
      I_DSIZE = 0
      I_BSIZE = -1   ! Pour éviter un div par zéro
      IF (ERR_GOOD()) THEN
         CALL MP_UTIL_FILE_GET_SIZE(I_FILE,I_FSIZE,I_ERROR)
         CALL MPI_FILE_GET_TYPE_EXTENT(I_FILE, IHDR, I_HSIZE, I_ERROR)
         CALL MPI_FILE_GET_TYPE_EXTENT(I_FILE, IROW, I_DSIZE, I_ERROR)
         I_BSIZE = I_DSIZE*NNT + I_HSIZE
      ENDIF

C---     Boucle sur les blocs
      I_DSP = 0
      NBLK = 0
      IF (ERR_GOOD()) NBLK = INT(I_FSIZE / I_BSIZE)
      DO IB=1,NBLK
         IF (ERR_BAD()) EXIT

         CALL MPI_FILE_SET_VIEW(I_FILE,
     &                          I_DSP,           ! Displacement
     &                          IHDR,
     &                          IHDR,
     &                          'native',
     &                          MPI_INFO_NULL,   ! Pourrait être creusé
     &                          I_ERROR)
         CALL MPI_FILE_READ_ALL(I_FILE,
     &                          HDR,
     &                          1,               ! 1 element of type IHDR
     &                          IHDR,
     &                          I_STATUS,
     &                          I_ERROR)

C---        Contrôle les dimensions
         IF (ERR_GOOD()) THEN
            NNT_L  = NINT(HDR(1))
            NPRN_L = NINT(HDR(2))
            IF (NNT  .NE. NNT_L)  GOTO 9900
            IF (NPRN .NE. NPRN_L) GOTO 9900
         ENDIF
         
C---        Assigne les param de section
         IF (ERR_GOOD()) IERR = FD_PRNO_ASGSCT(HPRNT,IFIC,HDR(3),I_DSP)

         I_DSP = I_DSP + I_BSIZE
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_FICHIER_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NNT', NNT, '/', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NPRN', NPRN, '/', NPRN_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_BLOC', IB, '/', NBLK
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
      
9999  CONTINUE
C---     Nettoie
      IF (I_FILE .NE. 0) THEN
         CALL ERR_PUSH()
         CALL MPI_FILE_CLOSE(I_FILE, I_ERROR)
         CALL ERR_POP()
      ENDIF

C---     Reset le contexte
      IERR = MP_UTIL_ASGERRCTX(' ')

      FD_PRNB_INI1FIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les dimensions du fichier.
C
C Description:
C     La fonction privée FD_PRNB_REQDIM lis l'entête d'un fichier
C     et en retourne les dimensions.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NOMFIC         Nom du fichier
C
C Sortie:
C     NVAL
C     NNT
C
C Notes:
C
C************************************************************************
      FUNCTION FD_PRNB_REQDIM(HOBJ, NVAL, NNT, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNT
      CHARACTER*(*) FNAME

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER   I_STATUS(MPI_STATUS_SIZE)
      INTEGER   I_COMM, I_ERROR
      INTEGER   I_FILE
      INTEGER*8 I_DSP

      INTEGER IERR
      INTEGER IOB
      INTEGER IFEH, IHDR
      REAL*8  HDR(3)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      
C---     Récupère les types et handler MPI
      IFEH = FD_PRNB_EHDLR(IOB)
      IHDR = FD_PRNB_TPHDR(IOB)
D     CALL ERR_ASR(IHDR.NE.0)

C---     Assigne le contexte
      IERR = MP_UTIL_ASGERRCTX('Reading '//FNAME(1:SP_STRN_LEN(FNAME)))
      
C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()

C---     Ouvre le fichier
      I_FILE = 0
      IF (ERR_GOOD()) THEN
         CALL MPI_FILE_OPEN(I_COMM,
     &                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                      MPI_MODE_RDONLY,
     &                      MPI_INFO_NULL,  !! Pourrait être creusé
     &                      I_FILE,
     &                      I_ERROR)
      ENDIF

C---     Lis l'entête
      I_DSP = 0
      CALL DINIT(3, 0.0D0, HDR, 1)
      IF (ERR_GOOD()) CALL MPI_FILE_SET_VIEW(I_FILE,
     &                                       I_DSP,          ! Displacement in bytes
     &                                       IHDR,
     &                                       IHDR,
     &                                       'native',
     &                                       MPI_INFO_NULL,  ! Pourrait être creusé
     &                                       I_ERROR)
      IF (ERR_GOOD()) CALL MPI_FILE_READ_ALL(I_FILE,
     &                                       HDR,
     &                                       1,              ! 1 element of type IHDR
     &                                       IHDR,
     &                                       I_STATUS,
     &                                       I_ERROR)

C---     Les valeurs de retour
      IF (ERR_GOOD()) THEN
         NNT  = NINT(HDR(1))
         NVAL = NINT(HDR(2))
      ENDIF

C---     Nettoie
      IF (I_FILE .NE. 0) THEN
         CALL ERR_PUSH()
         CALL MPI_FILE_CLOSE(I_FILE, I_ERROR)
         CALL ERR_POP()
      ENDIF

C---     Reset le contexte
      IERR = MP_UTIL_ASGERRCTX(' ')

      FD_PRNB_REQDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise les types MPI de l'objet
C
C Description:
C     La fonction statique FD_PRNB_INITYP initialise les types MPI
C     utilisés pour les opérations d'IO. Les types sont enregistrés
C     auprès de MPI (MPI_TYPE_COMMIT). Il est de la responsabilité
C     de l'appelant de disposer des types.
C
C Entrée:
C     NPRN       Nombre de colonnes
C     NNL        Nombre de lignes
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_INITYP(HOBJ, NPRN, NNL)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRN
      INTEGER NNL

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER I_ERROR
      INTEGER IHDR, IROW, IDTA
      INTEGER LDSP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NNL .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      LDSP = FD_PRNB_LDISP(IOB)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LDSP))

C---     Le type pour l'entête
      IHDR = 0
      IF (ERR_GOOD()) CALL MPI_TYPE_CONTIGUOUS(3,
     &                                         MP_TYPE_RE8(),
     &                                         IHDR,
     &                                         I_ERROR)
      IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT    (IHDR,
     &                                         I_ERROR)

C---     Le type pour une ligne de données
      IROW = 0
      IF (ERR_GOOD()) CALL MPI_TYPE_CONTIGUOUS(NPRN,
     &                                         MP_TYPE_RE8(),
     &                                         IROW,
     &                                         I_ERROR)
      IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT    (IROW,
     &                                         I_ERROR)

C---     Le type pour les données
      IDTA = 0
      IF (ERR_GOOD())
     &   CALL MPI_TYPE_CREATE_INDEXED_BLOCK(NNL,     ! IN count length of array of displacements (non-negative integer)
     &                                      1,       ! IN blocklength size of block (non-negative integer)
     &                                    KA(SO_ALLC_REQKIND(KA,LDSP)),    ! IN array_of_displacements array of displacements (array of integer)
     &                                      IROW,    ! IN oldtype old datatype (handle)
     &                                      IDTA,    ! OUT newtype new datatype (handle)
     &                                      I_ERROR)
      IF (ERR_GOOD()) CALL MPI_TYPE_COMMIT (IDTA,
     &                                      I_ERROR)


C---     Conserve les données
      IF (ERR_GOOD()) THEN
         FD_PRNB_TPHDR(IOB) = IHDR
         FD_PRNB_TPROW(IOB) = IROW
         FD_PRNB_TPDTA(IOB) = IDTA
      ENDIF

      FD_PRNB_INITYP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset les types MPI de l'objet
C
C Description:
C     La fonction statique FD_PRNB_RSTTYP reset les types MPI
C     utilisés pour les opérations d'IO.
C
C Entrée:
C     IHDR        Type pour l'entête
C     IROW        Type pour une ligne
C     IDTA        Type pour toutes les lignes
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_RSTTYP(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER I_ERROR
      INTEGER IOB
      INTEGER IHDR, IROW, IDTA
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      I_ERROR = MPI_SUCCESS

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
      IHDR  = FD_PRNB_TPHDR(IOB)
      IROW  = FD_PRNB_TPROW(IOB)
      IDTA  = FD_PRNB_TPDTA(IOB)

      IF (IDTA .NE. 0) CALL MPI_TYPE_FREE(IDTA,I_ERROR)
      IF (IROW .NE. 0) CALL MPI_TYPE_FREE(IROW,I_ERROR)
      IF (IHDR .NE. 0) CALL MPI_TYPE_FREE(IHDR,I_ERROR)

      FD_PRNB_TPHDR(IOB) = 0
      FD_PRNB_TPROW(IOB) = 0
      FD_PRNB_TPDTA(IOB) = 0

      FD_PRNB_RSTTYP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction FD_PRNB_INIIDX
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_INIIDX(HOBJ, HNUMR)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER ILCL, IGLB, LDSP, LIDX
      INTEGER NNL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNB_HBASE
D     LIDX = FD_PRNB_LINDX(IOB)
D     CALL ERR_ASR(LIDX .EQ. 0)

C---     Les dimensions
      NNL  = NM_NUMR_REQNNL(HNUMR)

C---     Alloue la mémoire
      LDSP = 0
      LIDX = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNL, LDSP)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNL, LIDX)

C---     Monte la table de déplacements
      IF (ERR_GOOD()) THEN
         DO ILCL=1,NNL
            IGLB = NM_NUMR_REQNEXT(HNUMR, ILCL)
            IGLB = IGLB-1     ! Passe d'indice à déplacement
            IERR = SO_ALLC_ASGIN4(LDSP, ILCL, IGLB)
         ENDDO
      ENDIF

C---     Génère la table d'index pour le tri
      IF (ERR_GOOD()) THEN
         CALL IINDEX(NNL,
     &               KA(SO_ALLC_REQKIND(KA,LDSP)),
     &               KA(SO_ALLC_REQKIND(KA,LIDX)))
      ENDIF

C---     Trie la table de distribution
      IF (ERR_GOOD()) THEN
         CALL ISWAPF(1, NNL,
     &               KA(SO_ALLC_REQKIND(KA,LDSP)),
     &               KA(SO_ALLC_REQKIND(KA,LIDX)))
      ENDIF

C---     Conserve les attributs ou nettoie
      IF (ERR_GOOD()) THEN
         FD_PRNB_HNUMR(IOB) = HNUMR
         FD_PRNB_LINDX(IOB) = LIDX
         FD_PRNB_LDISP(IOB) = LDSP
      ELSE
         IF (LIDX .NE. 0) IERR = SO_ALLC_ALLINT(0, LIDX)
         IF (LDSP .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSP)
      ENDIF

      FD_PRNB_INIIDX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction FD_PRNB_RSTIDX
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNB_RSTIDX(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprnb.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'fdprnb.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LDSP, LIDX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

      IOB = HOBJ - FD_PRNB_HBASE

      LIDX = FD_PRNB_LINDX(IOB)
      LDSP = FD_PRNB_LDISP(IOB)

      IF (LIDX .NE. 0) IERR = SO_ALLC_ALLINT(0, LIDX)
      IF (LDSP .NE. 0) IERR = SO_ALLC_ALLINT(0, LDSP)

      FD_PRNB_LINDX(IOB) = 0
      FD_PRNB_LDISP(IOB) = 0
      FD_PRNB_HNUMR(IOB) = 0

      FD_PRNB_RSTIDX = ERR_TYP()
      RETURN
      END
