C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes : TriGger
C Objet:   trigger DouBLE
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TG_DBLE_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_DBLE_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'tgdble_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgdble_ic.fc'

      INTEGER IERR
      INTEGER HOBJ
      REAL*8  DINC, DTRG
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_TG_DBLE_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Increment</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 1, DINC)
C     <comment>Trigger time step</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', 2, DTRG)
      IF (IERR .NE. 0) GOTO 9901

C---     VALIDE
      IF (DINC .LE. 0.0D0) GOTO 9902
      IF (DTRG .LE. 0.0D0) GOTO 9903

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = TG_DBLE_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = TG_DBLE_INI(HOBJ, DINC, DTRG)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_TG_DBLE_PRN(HOBJ)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the trigger</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>trigger_double</b> constructs an object, with the
C  given arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,1PE14.6E3)')'ERR_INCREMENT_INVALIDE',': ',DINC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,1PE14.6E3)')'ERR_INTERVAL_INVALIDE',': ',DTRG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TG_DBLE_AID()

9999  CONTINUE
      IC_TG_DBLE_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_TG_DBLE_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_DBLE_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'tgdble_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgdble.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgdble.fc'
      INCLUDE 'tgdble_ic.fc'

      INTEGER      IERR
      INTEGER      IOB
      INTEGER      IVAL
      REAL*8       RVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_ASR(TG_DBLE_HVALIDE(HOBJ))
         IOB = HOBJ - TG_DBLE_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Trigger increment</comment>
         IF      (PROP .EQ. 'increment') THEN
            RVAL = TG_DBLE_DINC(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
C        <comment>Trigger threshold</comment>
         ELSE IF (PROP .EQ. 'threshold') THEN
            RVAL = TG_DBLE_DTRG(IOB)
            WRITE(IPRM, '(2A,1PE25.17E3)') 'R', ',', RVAL
         ELSE
            GOTO 9902
         ENDIF

C---     SET
      ELSEIF (IMTH .EQ. '##property_set##') THEN
D        CALL ERR_ASR(TG_DBLE_HVALIDE(HOBJ))
         IOB = HOBJ - TG_DBLE_HBASE

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

         IF (PROP .EQ. 'increment') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            TG_DBLE_DINC(IOB) = RVAL
         ELSE IF (PROP .EQ. 'threshold') THEN
            IERR = SP_STRN_TKR(IPRM, ',', 2, RVAL)
            IF (IERR .NE. 0) GOTO 9901
            TG_DBLE_DTRG(IOB) = RVAL
         ELSE
            GOTO 9902
         ENDIF

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(TG_DBLE_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = TG_DBLE_PRN(HOBJ)
         IERR = IC_TG_DBLE_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_TG_DBLE_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_TG_DBLE_AID()

9999  CONTINUE
      IC_TG_DBLE_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_DBLE_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_DBLE_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgdble_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>trigger_double</b> represents the trigger based on a real value.
C  The trigger is incremented at each time step and will be activated when
C  the accumulated value is greater than a given threshold, this threshold
C  value being then subtracted from the accumulated value. For example,
C  with a step of 3 and a threshold of 10, the accumulated value will be 3,
C  6, 9, 2, 5, 8, 1, ... and the trigger will be activated on call 4, 7, ...
C</comment>
      IC_TG_DBLE_REQCLS = 'trigger_double'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_DBLE_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_TG_DBLE_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgdble_ic.fi'
      INCLUDE 'tgdble.fi'
C-------------------------------------------------------------------------

      IC_TG_DBLE_REQHDL = TG_DBLE_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_TG_DBLE_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_TG_DBLE_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('tgdble_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_TG_DBLE_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tgdble.fi'
      INCLUDE 'tgdble.fc'
      INCLUDE 'spstrn.fi'
      INCLUDE 'tgdble_ic.fc'

      INTEGER IERR
      INTEGER IOB
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_TRIGGER_DOUBLE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      IOB = HOBJ - TG_DBLE_HBASE

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      WRITE (LOG_BUF,'(2A,1PE25.17E3)')  'MSG_INCREMENT#<35>#', '= ',
     &                                   TG_DBLE_DINC(IOB)
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(2A,1PE25.17E3)')  'MSG_SEUIL#<35>#', '= ',
     &                                   TG_DBLE_DTRG(IOB)
      CALL LOG_ECRIS(LOG_BUF)


      CALL LOG_DECIND()

      IC_TG_DBLE_PRN = ERR_TYP()
      RETURN
      END
