C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER IC_NUMG_CMD
C     CHARACTER*(32) IC_NUMG_REQCMD
C   Private:
C     SUBROUTINE IC_NUMG_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NUMG_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NUMG_CMD
CDEC$ ENDIF

      USE NM_LCGL_HOBJ_M
      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icnumg.fi'
!!      INCLUDE 'nmlcgl.fi'
      INTEGER NM_LCGL_CTR
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER         IERR
      INTEGER         HOBJ
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NOMTMP
      INTEGER         LNOM
      CLASS (NM_LCGL_T), POINTER :: PNUM
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_NUMG_AID()
            GOTO 9999
         ENDIF
      ENDIF

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_DISTRIBUTION'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the renumbering file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902

C---     CONSTRUIS ET INITIALISE L'OBJET, LE CHARGE
      HOBJ = 0
      IF (ERR_GOOD()) IERR = NM_LCGL_CTR(HOBJ)
      IF (ERR_GOOD()) PNUM => NM_LCGL_REQSELF(HOBJ)
      IF (ERR_GOOD()) IERR = PNUM%INI(NOMFIC, 0, 0, 0, 0)
      IF (ERR_GOOD()) IERR = PNUM%CHARGE()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOMTMP, HOBJ)
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                            NOMTMP(1:SP_STRN_LEN(NOMTMP))
         CALL LOG_ECRIS(LOG_BUF)
         NOMTMP = NOMFIC
         CALL SP_STRN_CLP(NOMTMP, 50)
         LNOM = SP_STRN_LEN(NOMTMP)
         WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                           NOMTMP(1:LNOM)
         CALL LOG_ECRIS(LOG_BUF)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS_GLOBAL#<35>#= ',
     &                             PNUM%REQNNT()
         CALL LOG_ECRIS(LOG_BUF)
         IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
            WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS_LOCAL#<35>#= ',
     &                                PNUM%REQNNL()
            CALL LOG_ECRIS(LOG_BUF)
         ENDIF
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the renumbering</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NUMG_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_NUMG_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NUMG_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NUMG_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icnumg.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>global_numbering</b> creates a renumbering between the global
C  mesh and the sub-meshes local to a process. It describes the
C  partitioning of the nodes of the mesh between the different sub-domains
C  and the associated node renumbering.
C</comment>
      IC_NUMG_REQCMD = 'global_numbering'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NUMG_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icnumg.hlp')

      RETURN
      END

