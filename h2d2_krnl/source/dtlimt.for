C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_LIMT_000
C     INTEGER DT_LIMT_999
C     INTEGER DT_LIMT_PKL
C     INTEGER DT_LIMT_UPK
C     INTEGER DT_LIMT_CTR
C     INTEGER DT_LIMT_DTR
C     INTEGER DT_LIMT_INIDIM
C     INTEGER DT_LIMT_INIFIC
C     INTEGER DT_LIMT_RST
C     INTEGER DT_LIMT_REQHBASE
C     LOGICAL DT_LIMT_HVALIDE
C     INTEGER DT_LIMT_PRN
C     INTEGER DT_LIMT_CHARGE
C     INTEGER DT_LIMT_MAJVAL
C     INTEGER DT_LIMT_SAUVE
C     INTEGER DT_LIMT_REQNCLLIM
C     INTEGER DT_LIMT_REQNCLNOD
C     INTEGER DT_LIMT_REQNCLELE
C     INTEGER DT_LIMT_REQLCLLIM
C     INTEGER DT_LIMT_REQLCLNOD
C     INTEGER DT_LIMT_REQLCLELE
C     INTEGER DT_LIMT_REQLCLDST
C     CHARACTER*256 DT_LIMT_REQNOMF
C   Private:
C     INTEGER DT_LIMT_RAZ
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>DT_LIMT_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_LIMT_NOBJMAX,
     &                   DT_LIMT_HBASE,
     &                   'Boundary for Boundary Condition')

      DT_LIMT_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER  IERR
      EXTERNAL DT_LIMT_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_LIMT_NOBJMAX,
     &                   DT_LIMT_HBASE,
     &                   DT_LIMT_DTR)

      DT_LIMT_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_LIMT_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_LIMT_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_LIMT_HFLIM (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_LIMT_NCLLIM(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_LIMT_NCLNOD(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_LIMT_NCLELE(IOB))

C---     La table KCLLIM
      IF (ERR_GOOD()) THEN
         N = 7 *        DT_LIMT_NCLLIM(IOB)
         NX= 7 * MAX(1, DT_LIMT_NCLLIM(IOB))
         L = DT_LIMT_LCLLIM(IOB)
         IERR = IO_XML_WI_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table KCLNOD
      IF (ERR_GOOD()) THEN
         N =        DT_LIMT_NCLNOD(IOB)
         NX= MAX(1, DT_LIMT_NCLNOD(IOB))
         L = DT_LIMT_LCLNOD(IOB)
         IERR = IO_XML_WI_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table KCLELE
      IF (ERR_GOOD()) THEN
         N =        DT_LIMT_NCLELE(IOB)
         NX= MAX(1, DT_LIMT_NCLELE(IOB))
         L = DT_LIMT_LCLELE(IOB)
         IERR = IO_XML_WI_V(HXML, NX, N, L, 1)
      ENDIF

C---     La table VCLDST
      IF (ERR_GOOD()) THEN
         N =        DT_LIMT_NCLNOD(IOB)
         NX= MAX(1, DT_LIMT_NCLNOD(IOB))
         L = DT_LIMT_LCLDST(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

      DT_LIMT_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_LIMT_HBASE

C---     Initialise l'objet
      IF (LNEW) IERR = DT_LIMT_RAZ(HOBJ)
      IERR = DT_LIMT_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_LIMT_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_LIMT_HFLIM (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_LIMT_NCLLIM(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_LIMT_NCLNOD(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_LIMT_NCLELE(IOB))

C---     La table KCLLIM
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. 7*MAX(1,DT_LIMT_NCLLIM(IOB)))
      DT_LIMT_LCLLIM(IOB) = L

C---     La table KCLNOD
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. MAX(1,DT_LIMT_NCLNOD(IOB)))
      DT_LIMT_LCLNOD(IOB) = L

C---     La table KCLELE
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RI_V(HXML, N, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. MAX(1, DT_LIMT_NCLELE(IOB)))
      DT_LIMT_LCLELE(IOB) = L

C---     La table VCLDST
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, N, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR. NX .EQ. MAX(1, DT_LIMT_NCLNOD(IOB)))
      DT_LIMT_LCLDST(IOB) = L

      DT_LIMT_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   DT_LIMT_NOBJMAX,
     &                                   DT_LIMT_HBASE)
      IF (ERR_GOOD()) IERR = DT_LIMT_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_LIMT_HVALIDE(HOBJ))

      DT_LIMT_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_LIMT_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_LIMT_NOBJMAX,
     &                   DT_LIMT_HBASE)

      DT_LIMT_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IOB = HOBJ - DT_LIMT_HBASE

      DT_LIMT_TEMPS (IOB) = -1.0D0
      DT_LIMT_HFLIM (IOB) = 0
      DT_LIMT_NCLLIM(IOB) = 0
      DT_LIMT_NCLNOD(IOB) = 0
      DT_LIMT_NCLELE(IOB) = 0
      DT_LIMT_LCLLIM(IOB) = 0
      DT_LIMT_LCLNOD(IOB) = 0
      DT_LIMT_LCLELE(IOB) = 0
      DT_LIMT_LCLDST(IOB) = 0

      DT_LIMT_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec des dimensions.
C
C Description:
C     La fonction <code>DT_LIMT_INIDIM(...)<code> initialise l'objet avec
C     les dimensions en argument. L'objet est dimensionné et initialisé aux
C     valeurs de VINI si DOINIT est .TRUE..
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NCLLIM      Nombre de limites
C     NCLNOD      Nombre de noeuds
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_INIDIM(HOBJ, NCLLIM, NCLNOD)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_INIDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCLLIM
      INTEGER NCLNOD

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LCLLIM, LCLNOD, LCLELE, LCLDST
      INTEGER NCLELE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(NCLLIM .GT. 0)
D     CALL ERR_PRE(NCLNOD .GT. 0)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     CONTROLES DES PARAMETRES
      IF (NCLLIM .LE. 0) GOTO 9900
      IF (NCLNOD .LE. 0) GOTO 9901

C---     RESET LES DONNEES
      IERR = DT_LIMT_RST(HOBJ)
      NCLELE = 0

C---     ALLOUE L'ESPACE
      LCLLIM = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(7*MAX(1,NCLLIM), LCLLIM)
      LCLNOD = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(  MAX(1,NCLNOD), LCLNOD)
      LCLELE = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(  MAX(1,NCLELE), LCLELE)
      LCLDST = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(  MAX(1,NCLNOD), LCLDST)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_LIMT_HBASE
         DT_LIMT_TEMPS (IOB) = -1.0D0
         DT_LIMT_HFLIM (IOB) = 0
         DT_LIMT_NCLLIM(IOB) = NCLLIM
         DT_LIMT_NCLNOD(IOB) = NCLNOD
         DT_LIMT_NCLELE(IOB) = NCLELE
         DT_LIMT_LCLLIM(IOB) = LCLLIM
         DT_LIMT_LCLNOD(IOB) = LCLNOD
         DT_LIMT_LCLELE(IOB) = LCLELE
         DT_LIMT_LCLDST(IOB) = LCLDST
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NCLLIM_INVALIDE',':', NCLLIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NCLNOD_INVALIDE',':', NCLNOD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_LIMT_INIDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec un fichier de données.
C
C Description:
C     La fonction <code>DT_LIMT_INIFIC(...)<code> initialise l'objet.
C     avec un fichier source des données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FICVNO      Nom du fichier de LIMite
C     ISTAT       IO_LECTURE, mais pas IO_ECRITURE ou IO_AJOUT
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_INIFIC(HOBJ, FICLIM, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_INIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FICLIM
      INTEGER       ISTAT

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLIM
      INTEGER NCLLIM, NCLNOD, NCLELE
      INTEGER LCLLIM, LCLNOD, LCLELE, LCLDST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLES DES PARAMETRE
      IF (SP_STRN_LEN(FICLIM) .LE. 0) GOTO 9900

C---     RESET LES DONNEES
      IERR = DT_LIMT_RST(HOBJ)

C---     CONSTRUIS LE FICHIER DE DONNEES
      IF (ERR_GOOD())IERR = FD_LIMT_CTR(HFLIM)
      IF (ERR_GOOD())IERR = FD_LIMT_INI(HFLIM,
     &                                  FICLIM(1:SP_STRN_LEN(FICLIM)),
     &                                  ISTAT)

C---     DEMANDE LE DIMENSIONS
      NCLLIM = 0
      NCLNOD = 0
      IF (ERR_GOOD()) NCLLIM = FD_LIMT_REQNCLLIM(HFLIM)
      IF (ERR_GOOD()) NCLNOD = FD_LIMT_REQNCLNOD(HFLIM)
      NCLELE = NCLNOD

C---     ALLOUE L'ESPACE
      LCLLIM = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(7*MAX(1,NCLLIM), LCLLIM)
      LCLNOD = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(  MAX(1,NCLNOD), LCLNOD)
      LCLELE = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(  MAX(1,NCLELE), LCLELE)
      LCLDST = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(  MAX(1,NCLNOD), LCLDST)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_LIMT_HBASE
         DT_LIMT_HFLIM (IOB) = HFLIM
         DT_LIMT_NCLLIM(IOB) = NCLLIM
         DT_LIMT_NCLNOD(IOB) = NCLNOD
         DT_LIMT_NCLELE(IOB) = NCLELE
         DT_LIMT_LCLLIM(IOB) = LCLLIM
         DT_LIMT_LCLNOD(IOB) = LCLNOD
         DT_LIMT_LCLELE(IOB) = LCLELE
         DT_LIMT_LCLDST(IOB) = LCLDST
         DT_LIMT_TEMPS (IOB) = -1.0D0
      ENDIF
      GOTO 9999
C---------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_LIMT_INIFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLIM
      INTEGER LCLLIM, LCLNOD, LCLELE, LCLDST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_LIMT_HBASE

C---     RECUPERE LES DONNEES
      HFLIM  = DT_LIMT_HFLIM(IOB)
      LCLLIM = DT_LIMT_LCLLIM(IOB)
      LCLNOD = DT_LIMT_LCLNOD(IOB)
      LCLELE = DT_LIMT_LCLELE(IOB)
      LCLDST = DT_LIMT_LCLDST(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LCLDST .NE. 0) IERR = SO_ALLC_ALLRE8(0, LCLDST)
      IF (LCLELE .NE. 0) IERR = SO_ALLC_ALLINT(0, LCLELE)
      IF (LCLNOD .NE. 0) IERR = SO_ALLC_ALLINT(0, LCLNOD)
      IF (LCLLIM .NE. 0) IERR = SO_ALLC_ALLINT(0, LCLLIM)

C---     DETRUIS LE FICHIER
      IF (FD_LIMT_HVALIDE(HFLIM)) IERR = FD_LIMT_DTR(HFLIM)

C---     RESET LES PARAMETRES
      IERR = DT_LIMT_RAZ(HOBJ)

      DT_LIMT_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_LIMT_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C------------------------------------------------------------------------

      DT_LIMT_REQHBASE = DT_LIMT_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_LIMT_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtlimt.fc'
C------------------------------------------------------------------------

      DT_LIMT_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_LIMT_NOBJMAX,
     &                                  DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DT_LIMT_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C************************************************************************
      FUNCTION DT_LIMT_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFLIM
      CHARACTER*256 NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_LIMT_HBASE
      HFLIM = DT_LIMT_HFLIM(IOB)

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.data.limite'

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_LIMITE'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOM DU FICHIER
      HFLIM = DT_LIMT_HFLIM(IOB)
      IF (FD_LIMT_HVALIDE(HFLIM)) THEN
         NOM = DT_LIMT_REQNOMF(HOBJ)
         CALL SP_STRN_CLP(NOM, 50)
      ELSE
         NOM = '---'
      ENDIF
      WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                         NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOMBRE DE CONDITIONS
      WRITE (LOG_BUF,'(A,I6)') 'MSG_NBR_LIMITES#<35>#= ',
     &                          DT_LIMT_REQNCLLIM(HOBJ)
      CALL LOG_ECRIS(LOG_BUF)

      CALL LOG_DECIND()

      DT_LIMT_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_LIMT_CHARGE
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_CHARGE (HOBJ, HNUMR, TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  TNEW

      INCLUDE 'dtlimt.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      REAL*8  TOLD
      INTEGER IERR
      INTEGER IOB
      INTEGER HFLIM
      INTEGER NCLLIM
      INTEGER NCLNOD
      INTEGER NCLELE
      INTEGER LCLLIM
      INTEGER LCLNOD
      INTEGER LCLELE
      LOGICAL MODIF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB  = HOBJ - DT_LIMT_HBASE

C---     Récupère les données
      HFLIM  = DT_LIMT_HFLIM (IOB)
      NCLLIM = DT_LIMT_NCLLIM(IOB)
      LCLLIM = DT_LIMT_LCLLIM(IOB)
      NCLNOD = DT_LIMT_NCLNOD(IOB)
      LCLNOD = DT_LIMT_LCLNOD(IOB)
      NCLELE = DT_LIMT_NCLELE(IOB)
      LCLELE = DT_LIMT_LCLELE(IOB)
      TOLD   = DT_LIMT_TEMPS (IOB)

C---     Lis les données
      IF (FD_LIMT_HVALIDE(HFLIM)) THEN
         IERR = FD_LIMT_LIS(HFLIM,
     &                      HNUMR,
     &                      NCLLIM,
     &                      KA(SO_ALLC_REQKIND(KA, LCLLIM)),
     &                      NCLNOD,
     &                      KA(SO_ALLC_REQKIND(KA, LCLNOD)),
     &                      TOLD,
     &                      TNEW,
     &                      MODIF)
      ENDIF

C---     Initialise les numéros d'éléments
      IF (ERR_GOOD() .AND. MODIF) THEN
         CALL IINIT(NCLELE, 0, KA(SO_ALLC_REQKIND(KA, LCLELE)), 1)
      ENDIF

C---     Conserve le temps
      IF (ERR_GOOD()) THEN
         DT_LIMT_TEMPS(IOB) = TNEW
      ENDIF

      DT_LIMT_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Met à jour les valeurs.
C
C Description:
C     La fonction DT_LIMT_MAJVAL assigne à l'objet les valeurs passées
C     en argument. Les tables KCLLIM et KCLNOD doivent avoir les mêmes
C     dimensions que l'objet et leurs valeurs vont donc remplacer toutes
C     celles de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NCLLIM      Nombre de conditions limites
C     KCLLIM      Table des conditions limites
C     TEMPS       Temps associé aux valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_MAJVAL(HOBJ,
     &                        NCLLIM, KCLLIM,
     &                        NCLNOD, KCLNOD,
     &                        TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_MAJVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NCLLIM
      INTEGER KCLLIM(7, NCLLIM)
      INTEGER NCLNOD
      INTEGER KCLNOD(NCLNOD)
      REAL*8  TEMPS

      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER LLIM_L, LNOD_L
      INTEGER NLIM_L, NNOD_L
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_LIMT_HBASE
      NLIM_L = DT_LIMT_NCLLIM(IOB)
      NNOD_L = DT_LIMT_NCLNOD(IOB)
      LLIM_L = DT_LIMT_LCLLIM(IOB)
      LNOD_L = DT_LIMT_LCLNOD(IOB)
D     CALL ERR_ASR(NLIM_L .GE. 0)
D     CALL ERR_ASR(NNOD_L .GT. 0)
D     CALL ERR_ASR(LLIM_L .NE. 0)
D     CALL ERR_ASR(LNOD_L .NE. 0)

C---     Contrôles
      IF (NLIM_L .NE. 0 .AND. NLIM_L .NE. NCLLIM) GOTO 9900
      IF (NNOD_L .NE. 0 .AND. NNOD_L .NE. NCLNOD) GOTO 9901

C---     Transfert
      CALL ICOPY(7*NCLLIM,KCLLIM, 1, KA(SO_ALLC_REQKIND(KA,LLIM_L)), 1)
      CALL ICOPY(NCLNOD,  KCLNOD, 1, KA(SO_ALLC_REQKIND(KA,LNOD_L)), 1)

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_LIMT_TEMPS(IOB) = TEMPS
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NCLLIM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NCLLIM#<35>#:', NLIM_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NCLLIM#<35>#:', NCLLIM
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NCLNOD_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NCLNOD#<35>#:', NNOD_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NCLNOD#<35>#:', NCLNOD
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_LIMT_MAJVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_LIMT_SAUVE
C
C Description:
C     La fonction <code>DT_LIMT_SAUVE(...)<code> écris les données de l'objet
C     dans le fichier maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation des noeuds
C     FICLMT      Nom du fichier de limite
C     ISTAT       Status du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_SAUVE(HOBJ, HNUMR, FICLMT, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HNUMR
      CHARACTER*(*) FICLMT
      INTEGER       ISTAT

      INCLUDE 'dtlimt.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLMT
      INTEGER LCLLIM, LCLNOD
      INTEGER NCLLIM,NCLNOD
      REAL*8  TSIM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_LIMT_HBASE
      NCLLIM = DT_LIMT_NCLLIM(IOB)
      LCLLIM = DT_LIMT_LCLLIM(IOB)
      NCLNOD = DT_LIMT_NCLNOD(IOB)
      LCLNOD = DT_LIMT_LCLNOD(IOB)
      TSIM   = DT_LIMT_TEMPS (IOB)
D     CALL ERR_ASR(NCLLIM .GT. 0)
D     CALL ERR_ASR(NCLNOD .GT. 0)
D     CALL ERR_ASR(SO_ALLC_HEXIST(LCLLIM))
D     CALL ERR_ASR(SO_ALLC_HEXIST(LCLNOD))

C---     Construis le fichier
      IF (ERR_GOOD()) IERR = FD_LIMT_CTR(HFLMT)
      IF (ERR_GOOD()) IERR = FD_LIMT_INI(HFLMT,
     &                                   FICLMT(1:SP_STRN_LEN(FICLMT)),
     &                                   ISTAT)

C---     Ecris
      IF (ERR_GOOD())
     &   IERR = FD_LIMT_ECRIS(HFLMT,
     &                        HNUMR,
     &                        NCLLIM,
     &                        KA(SO_ALLC_REQKIND(KA,LCLLIM)),
     &                        NCLNOD,
     &                        KA(SO_ALLC_REQKIND(KA,LCLNOD)),
     &                        TSIM)

C---     Détruis le fichier
      IF (FD_LIMT_HVALIDE(HFLMT)) THEN
         CALL ERR_PUSH()
         IERR = FD_LIMT_DTR(HFLMT)
         CALL ERR_POP()
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FICHIER_NON_DEFINI'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_LIMT_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQNCLLIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQNCLLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_LIMT_REQNCLLIM = DT_LIMT_NCLLIM(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQNCLNOD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQNCLNOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_LIMT_REQNCLNOD = DT_LIMT_NCLNOD(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQNCLELE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQNCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_LIMT_REQNCLELE = DT_LIMT_NCLELE(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQLCLLIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQLCLLIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_LIMT_LCLLIM(HOBJ-DT_LIMT_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_LIMT_REQLCLLIM = DT_LIMT_LCLLIM(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQLCLNOD(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQLCLNOD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_LIMT_LCLNOD(HOBJ-DT_LIMT_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_LIMT_REQLCLNOD = DT_LIMT_LCLNOD(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQLCLELE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQLCLELE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_LIMT_LCLELE(HOBJ-DT_LIMT_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_LIMT_REQLCLELE = DT_LIMT_LCLELE(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQLCLDST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQLCLDST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'dtlimt.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
D     CALL ERR_PRE(DT_LIMT_LCLDST(HOBJ-DT_LIMT_HBASE) .NE. 0)
C------------------------------------------------------------------------

      DT_LIMT_REQLCLDST = DT_LIMT_LCLDST(HOBJ-DT_LIMT_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du Fichier.
C
C Description:
C     La fonction DT_LIMT_REQNOMF retourne le nom de fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_LIMT_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_LIMT_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtlimt.fi'
      INCLUDE 'fdlimt.fi'
      INCLUDE 'dtlimt.fc'

      INTEGER HFLIM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLIM = DT_LIMT_HFLIM(HOBJ-DT_LIMT_HBASE)
D     CALL ERR_ASR(FD_LIMT_HVALIDE(HFLIM))

      DT_LIMT_REQNOMF = FD_LIMT_REQNOMF(HFLIM)
      RETURN
      END
