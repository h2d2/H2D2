C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NR_NORM_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NR_NORM_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'nrnomr_ic.fi'
      INCLUDE 'nrnomr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER, PARAMETER :: NVALMAX = 10

      INTEGER IERR
      INTEGER HOBJ
      INTEGER I, IV
!!      INTEGER IKND
!!      INTEGER HOBJ, HALG
!!      INTEGER HELE
!!      INTEGER NTOK, NVAL
!!      REAL*8  VEPSA(NVALMAX), VEPSR(NVALMAX)
!!      CHARACTER*(8) LKND
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Traitement spécial pour afficher l'aide
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_NR_NORM_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Contrôle les param
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     Les dimensions
      NTOK = SP_STRN_NTOK(IPRM, ',')
      NVAL = (NTOK-2) / 2
      IF (NVAL .GE. NVALMAX) GOTO 9902

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 1
C     <comment>Kind of norm ['l2', 'max']</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', I, LKND)
      I = I + 1
C     <comment>DOF to apply the norm on (-1 for all DOF)</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, IDDL)
      IF (IERR .NE. 0) GOTO 9901
      DO IV=1,NVAL
C        <comment>
C        List of tolerances, comma separated. For each degree of freedom,
C        2 values for relative and absolute tolerance.
C        </comment>
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSR(IV))
         I = I + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKR(IPRM, ',', I, VEPSA(IV))
         IF (IERR .NE. 0) GOTO 9901
      ENDDO

C---     Contrôle
      IF (IDDL .EQ. 0 .OR. IDDL .GT. NVAL) GOTO 9904
      IF (IDDL .EQ. -1 .OR. IDDL .LE. NVAL) GOTO 9904
      IKND = NR_NORM_KIND_INDEFINI
      IF (LKND .EQ. 'l2') THEN
         IF (IDDL .EQ. -1 .AND. NVAL .EQ. 0) IKND = NR_NORM_LINK_N2GD
         IF (IDDL .EQ. -1 .AND. NVAL .EQ. 1) IKND = NR_NORM_LINK_N2GR
         IF (IDDL .GE.  1 .AND. NVAL .EQ. 0) IKND = NR_NORM_LINK_N2SD
         IF (IDDL .GE.  1 .AND. NVAL .GT. 0) IKND = NR_NORM_LINK_N2SR
      ELSE IF (LKND .EQ. 'max') THEN
         IF (IDDL .EQ. -1 .AND. NVAL .EQ. 0) IKND = NR_NORM_LINK_NIGD
         IF (IDDL .EQ. -1 .AND. NVAL .EQ. 1) IKND = NR_NORM_LINK_NIGR
         IF (IDDL .GE.  1 .AND. NVAL .EQ. 0) IKND = NR_NORM_LINK_NISD
         IF (IDDL .GE.  1 .AND. NVAL .GT. 0) IKND = NR_NORM_LINK_NISR
      ENDIF
      IF (IKND .EQ. NR_NORM_NRM_INDEFINI) GOTO 9905

C---     Construis l'objet
      IERR = NR_NORM_CTR(HOBJ)
      IERR = NR_NORM_INI(HOBJ, IKND, IDDL, NVAL, VVAL)

C---     Retourne le handle
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the dictionary</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>norm</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOMBRE_VALEUR_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(A)') 'ERR_TYPE_NORME_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NR_NORM_AID()

9999  CONTINUE
      IC_NR_NORM_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NR_NORM_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NR_NORM_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'nrnomr_ic.fi'
      INCLUDE 'nrnomr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER      IERR
      CHARACTER*64 KVAL, VVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     DISPATCH LES MÉTHODES, PROPRIÉTÉS ET OPERATEURS
C     <comment>Get a value with its key.</comment>
      IF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_ASR(NR_NORM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Key</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, KVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NR_NORM_REQVAL(HOBJ, KVAL(1:SP_STRN_LEN(KVAL)), VVAL)
         IF (IERR .NE. 0) GOTO 9902
         WRITE(IPRM, '(3A)') 'S', ',', VVAL

C     <comment>Set a value with its key. If the value does not exist, it is created. If it does, it is replaced.</comment>
      ELSEIF (IMTH .EQ. '##opb_[]_set##') THEN
D        CALL ERR_ASR(NR_NORM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Key</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, KVAL)
C        <comment>Value</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, VVAL)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NR_NORM_ASGVAL(HOBJ,
     &                        KVAL(1:SP_STRN_LEN(KVAL)),
     &                        VVAL(1:SP_STRN_LEN(VVAL)))
         IF (IERR .NE. 0) GOTO 9902

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_ASR(NR_NORM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = NR_NORM_DTR(HOBJ)
         IF (IERR .NE. 0) GOTO 9902

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(NR_NORM_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = LOG_STS(HOBJ)
         CALL LOG_ECRIS('<!-- Test NR_NORM_MAP___(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_NR_NORM_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_DICO_AVEC_CLEF',': ', KVAL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NR_NORM_AID()

9999  CONTINUE
      IC_NR_NORM_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NR_NORM_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NR_NORM_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nrnomr_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C     The class <b>map</b> represents a dictionary data structure, with keys and
C     values associated. Keys and values are strings.
C</comment>
      IC_NR_NORM_REQCLS = 'map'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NR_NORM_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_NR_NORM_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'nrnomr_ic.fi'
      INCLUDE 'nrnomr.fi'
C-------------------------------------------------------------------------

      IC_NR_NORM_REQHDL = NR_NORM_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NR_NORM_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('nrnomr_ic.hlp')

      RETURN
      END
