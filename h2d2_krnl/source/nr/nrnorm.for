C************************************************************************
C --- Copyright (c) INRS 2018
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  NR
C Objet:   NORMe
C Type:    Concret
C
C Interface:
C   H2D2 Module: NR
C      H2D2 Class: NR_NORM
C         SUBROUTINE NR_NORM_000
C         SUBROUTINE NR_NORM_999
C         SUBROUTINE NR_NORM_CTR
C         SUBROUTINE NR_NORM_DTR
C         SUBROUTINE NR_NORM_RAZ
C         SUBROUTINE NR_NORM_INI
C         SUBROUTINE NR_NORM_RST
C         SUBROUTINE NR_NORM_REQHBASE
C         SUBROUTINE NR_NORM_HVALIDE
C         SUBROUTINE NR_NORM_PRN
C         SUBROUTINE NR_NORM_CLC
C         FTN (Sub)Module: NR_NORM_M
C            Public:
C            Private:
C               TYPE (NR_NORM_SELF_T), POINTER NR_NORM_REQSELF
C            
C            FTN Type: NR_NORM_SELF_T
C               Public:
C               Private:
C
C************************************************************************

      MODULE NR_NORM_M

      USE SO_ALLC_M
      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: NR_NORM_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: NR_NORM_SELF_T
         INTEGER :: IKND
         INTEGER :: IDDL
         INTEGER :: NVAL
         TYPE(SO_ALLC_VPTR1_T) :: VEPSR
         TYPE(SO_ALLC_VPTR1_T) :: VEPSA
      END TYPE NR_NORM_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée NR_NORM_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_REQSELF(HOBJ) !, SELF)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (NR_NORM_SELF_T), POINTER :: NR_NORM_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (NR_NORM_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      NR_NORM_REQSELF => SELF ! ERR_TYP()
      RETURN
      END FUNCTION NR_NORM_REQSELF

      END MODULE NR_NORM_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_000
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(NR_NORM_HBASE, 'Norm')

      NR_NORM_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_999
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INCLUDE 'nrnorm.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL NR_NORM_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(NR_NORM_HBASE,
     &                   NR_NORM_DTR)

      NR_NORM_999 = ERR_TYP()
      RETURN
      END

!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C
!!!C Entrée:
!!!C
!!!C Sortie:
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION NR_NORM_PKL(HOBJ, HXML)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_PKL
!!!CDEC$ ENDIF
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!      INTEGER HXML
!!!
!!!      INCLUDE 'nrnorm.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'ioxml.fi'
!!!      INCLUDE 'spstrn.fi'
!!!
!!!      INTEGER IERR, IK
!!!      CHARACTER*(256) SKEY, SVAL
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!C---     Les clefs/valeurs
!!!      IERR = IO_XML_WI_1(HXML, NR_NORM_REQDIM(HOBJ))
!!!      DO IK=1,NR_NORM_REQDIM(HOBJ)
!!!         IERR = NR_NORM_REQCLF(HOBJ, IK, SKEY)
!!!         IERR = NR_NORM_REQVAL(HOBJ, SKEY(1:SP_STRN_LEN(SKEY)), SVAL)
!!!
!!!         IERR = IO_XML_WS(HXML, SKEY(1:SP_STRN_LEN(SKEY)))
!!!         IERR = IO_XML_WS(HXML, SVAL(1:SP_STRN_LEN(SVAL)))
!!!         IF (ERR_BAD()) GOTO 199
!!!      ENDDO
!!!199   CONTINUE
!!!
!!!      NR_NORM_PKL = ERR_TYP()
!!!      RETURN
!!!      END
!!!
!!!C************************************************************************
!!!C Sommaire:
!!!C
!!!C Description:
!!!C     La fonction NR_NORM_UPK restaure l'objet à partir d'une sauvegarde sous
!!!C     format XML. Le handle passé en argument n'est pas construit, et c'est
!!!C     le travail de la fonction de le charger.
!!!C
!!!C Entrée:
!!!C     HOBJ     Handle sur l'objet non construit
!!!C     LNEW     .TRUE. si l'objet nouveau
!!!C     HXML     Handle sur le reader XML
!!!C
!!!C Sortie:
!!!C     HOBJ     Handle sur l'objet construit
!!!C
!!!C Notes:
!!!C************************************************************************
!!!      FUNCTION NR_NORM_UPK(HOBJ, LNEW, HXML)
!!!CDEC$ IF DEFINED(MODE_DYNAMIC)
!!!CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_UPK
!!!CDEC$ ENDIF
!!!
!!!      IMPLICIT NONE
!!!
!!!      INTEGER HOBJ
!!!      LOGICAL LNEW
!!!      INTEGER HXML
!!!
!!!      INCLUDE 'nrnorm.fi'
!!!      INCLUDE 'err.fi'
!!!      INCLUDE 'ioxml.fi'
!!!      INCLUDE 'spstrn.fi'
!!!      INCLUDE 'nrnorm.fc'
!!!
!!!      INTEGER IERR
!!!      INTEGER IK, N
!!!      CHARACTER*(256) SKEY, SVAL
!!!C------------------------------------------------------------------------
!!!D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
!!!C------------------------------------------------------------------------
!!!
!!!C---     Initialise l'objet
!!!      IF (LNEW) IERR = NR_NORM_RAZ(HOBJ)
!!!      IERR = NR_NORM_INI(HOBJ)
!!!
!!!C---     Les clef/valeurs
!!!      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, N)
!!!      DO IK=1,N
!!!         IF (ERR_GOOD()) IERR = IO_XML_RS(HXML, SKEY)
!!!         IF (ERR_GOOD()) IERR = IO_XML_RS(HXML, SVAL)
!!!
!!!         IF (ERR_GOOD()) IERR = NR_NORM_ASGVAL(HOBJ,
!!!     &                                        SKEY(1:SP_STRN_LEN(SKEY)),
!!!     &                                        SVAL(1:SP_STRN_LEN(SVAL)))
!!!      ENDDO
!!!D     CALL ERR_ASR(ERR_BAD() .OR. NR_NORM_REQDIM(HOBJ) .EQ. N)
!!!
!!!      NR_NORM_UPK = ERR_TYP()
!!!      RETURN
!!!      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_CTR
CDEC$ ENDIF

      USE NR_NORM_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nrnorm.fc'

      INTEGER IERR, IRET
      TYPE (NR_NORM_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   NR_NORM_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = NR_NORM_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. NR_NORM_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      NR_NORM_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_DTR
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NR_NORM_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = NR_NORM_RST(HOBJ)

C---     Dé-alloue
      SELF => NR_NORM_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, NR_NORM_HBASE)

      NR_NORM_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée NR_NORM_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_RAZ(HOBJ)

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nrnorm.fc'

      TYPE (NR_NORM_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NR_NORM_REQSELF(HOBJ)
      SELF%IKND  = NR_NOMR_KIND_INDEFINI
      SELF%IDDL  = 0
      SELF%NVAL  = 0
      SELF%VEPSR = SO_ALLC_VPTR1()
      SELF%VEPSA = SO_ALLC_VPTR1()

      NR_NORM_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_INI(HOBJ, IKND, IDDL, NVAL, VEPSR, VEPSA)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_INI
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: IKND
      INTEGER, INTENT(IN) :: IDDL
      INTEGER, INTENT(IN) :: NVAL
      REAL*8,  INTENT(IN) :: VEPSR(:)
      REAL*8,  INTENT(IN) :: VEPSA(:)

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      TYPE (NR_NORM_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôles
      IF (IKND .LE. NR_NOMR_KIND_INDEFINI) GOTO 9900
      IF (IKND .GE. NR_NOMR_KIND_LAST)     GOTO 9900
!!!      IF (IDDL .LE. 0 .AND. NVAL .GT. 0)   GOTO 9901
      IF (IDDL .GT. 0 .AND. NVAL .GT. 1) GOTO 9901
      IF (NVAL .GT. 0 .AND. NVAL .NE. SIZE(VEPSR)) GOTO 9902
      IF (NVAL .GT. 0 .AND. NVAL .NE. SIZE(VEPSA)) GOTO 9902

      SELECT CASE(IKND)
         CASE(NR_NOMR_KIND_N2SD)
            IF (IDDL .LE. 0) GOTO 9901
            IF (NVAL .GE. 0) GOTO 9901
         CASE(NR_NOMR_KIND_N2GD)
            IF (IDDL .GT. 0) GOTO 9901
            IF (NVAL .GE. 0) GOTO 9901
         CASE(NR_NOMR_KIND_N2SR)
            IF (IDDL .LE. 0) GOTO 9901
            IF (NVAL .NE. 1) GOTO 9901
         CASE(NR_NOMR_KIND_N2GR)
            IF (IDDL .GT. 0) GOTO 9901
            IF (NVAL .LE. 0) GOTO 9901
         CASE(NR_NOMR_KIND_NISD)
            IF (IDDL .LE. 0) GOTO 9901
            IF (NVAL .GE. 0) GOTO 9901
         CASE(NR_NOMR_KIND_NIGD)
            IF (IDDL .GT. 0) GOTO 9901
            IF (NVAL .GE. 0) GOTO 9901
         CASE(NR_NOMR_KIND_NISR)
            IF (IDDL .LE. 0) GOTO 9901
            IF (NVAL .NE. 1) GOTO 9901
         CASE(NR_NOMR_KIND_NIGR)
            IF (IDDL .GT. 0) GOTO 9901
            IF (NVAL .LE. 0) GOTO 9901
         CASE DEFAULT
            GOTO 9901
      END SELECT

C---     Reset les données
      IERR = NR_NORM_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => NR_NORM_REQSELF(HOBJ)
         SELF%IKND = IKND
         SELF%IDDL = IDDL
         SELF%NVAL = NVAL
         IF (NVAL .GT. 0) THEN
            SELF%VEPSR = SO_ALLC_VPTR1(SIZE(VEPSR))
            SELF%VEPSA = SO_ALLC_VPTR1(SIZE(VEPSA))
            SELF%VEPSR%VPTR(:) = VEPSR(:)
            SELF%VEPSA%VPTR(:) = VEPSA(:)
         ENDIF
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NORME_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_INDICE_DDL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_NVAL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NR_NORM_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est récupérée automatiquement dans RAZ
C************************************************************************
      FUNCTION NR_NORM_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_RST
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nrnorm.fc'

      INTEGER IERR
      TYPE (NR_NORM_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Reset les attributs
      IF (ERR_GOOD()) IERR = NR_NORM_RAZ(HOBJ)

      NR_NORM_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NR_NORM_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_REQHBASE
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INCLUDE 'nrnorm.fi'
C------------------------------------------------------------------------

      NR_NORM_REQHBASE = NR_NORM_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NR_NORM_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_HVALIDE
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NR_NORM_HVALIDE = OB_OBJN_HVALIDE(HOBJ,
     &                                 NR_NORM_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction NR_NORM_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C     Prototype
C************************************************************************
      FUNCTION NR_NORM_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IK, NVAL
      CHARACTER*256 BUF, VAR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     En-tête
      LOG_ZNE = 'h2d2.data.map'
      WRITE (LOG_BUF, '(A)') 'MSG_NORME'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Impression du handle
      IF (ERR_GOOD()) IERR = OB_OBJC_REQNOMCMPL(BUF, HOBJ)
      IF (ERR_GOOD()) THEN
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        BUF(1:SP_STRN_LEN(BUF))
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      ENDIF

!!C---     Le nombre de valeurs
!!      NVAL = 0
!!      IF (ERR_GOOD()) THEN
!!         NVAL = NR_NORM_REQDIM(HOBJ)
!!         WRITE (LOG_BUF,'(A,I12)') 'MSG_NOMBRE_VALEURS#<35>#= ', NVAL
!!         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
!!      ENDIF
!!
!!C---     Les valeurs
!!      IF (ERR_GOOD()) THEN
!!         WRITE (LOG_BUF,'(A)') 'MSG_VALEURS:'
!!         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
!!      ENDIF
!!      CALL LOG_INCIND()
!!      DO IK=1,NVAL
!!         IF (ERR_GOOD()) IERR = NR_NORM_REQCLF(HOBJ, IK, VAR)
!!         IF (ERR_GOOD()) IERR = NR_NORM_REQVAL(HOBJ,VAR, BUF)
!!         IF (ERR_GOOD()) THEN
!!            CALL SP_STRN_CLP(VAR, 30)
!!            CALL SP_STRN_CLP(BUF, 50)
!!            WRITE (LOG_BUF,'(3A)') VAR(1:SP_STRN_LEN(VAR)),
!!     &                             '#<30># : ', BUF(1:SP_STRN_LEN(BUF))
!!            CALL LOG_INFO(LOG_ZNE, LOG_BUF)
!!         ENDIF
!!      ENDDO
!!      CALL LOG_DECIND()

C---     Reset l'indentation
      CALL LOG_DECIND()

      NR_NORM_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme
C
C Description:
C     La fonction NR_NORM_CLC calcule la norme.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NR_NORM_CLC(HOBJ,
     &                     NDLN,
     &                     NNL,
     &                     VDLG,
     &                     VDEL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_NORM_ASGVAL
CDEC$ ENDIF

      USE NR_NORM_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NDLN
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VDLG(NDLN, NNL)
      REAL*8,  INTENT(IN) :: VDEL(NDLN, NNL)

      INCLUDE 'nrnorm.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nrutil.fi'

      REAL*8  R_D
      INTEGER IERR
      INTEGER IDDL
      INTEGER HNUMR
      TYPE (NR_NORM_SELF_T), POINTER :: SELF
      REAL*8, POINTER :: EPSR(:)
      REAL*8, POINTER :: EPSA(:)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NR_NORM_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

C---     Récupère les attributs
      SELF => NR_NORM_REQSELF(HOBJ)
      IDDL = SELF%IDDL
      EPSR => SELF%VEPSR%VPTR
      EPSA => SELF%VEPSA%VPTR

      SELECT CASE(SELF%IKND)
         CASE(NR_NOMR_KIND_N2SD)
            R_D = NR_UTIL_N2SD(HNUMR, NDLN, NNL, VDEL, IDDL)
         CASE(NR_NOMR_KIND_N2GD)
            R_D = NR_UTIL_N2GD(HNUMR, NDLN, NNL, VDEL)
         CASE(NR_NOMR_KIND_N2SR)
            R_D = NR_UTIL_N2SR(HNUMR, NDLN, NNL, VDEL, IDDL,
     &                         VDLG, EPSA(1), EPSR(1))
         CASE(NR_NOMR_KIND_N2GR)
            R_D = NR_UTIL_N2GR(HNUMR, NDLN, NNL, VDEL, VDLG, EPSA, EPSR)
         CASE(NR_NOMR_KIND_NISD)
            R_D = NR_UTIL_NISD(HNUMR, NDLN, NNL, VDEL, IDDL)
         CASE(NR_NOMR_KIND_NIGD)
            R_D = NR_UTIL_NIGD(HNUMR, NDLN, NNL, VDEL)
         CASE(NR_NOMR_KIND_NISR)
            R_D = NR_UTIL_NISR(HNUMR, NDLN, NNL, VDEL, IDDL,
     &                         VDLG, EPSA(1), EPSR(1))
         CASE(NR_NOMR_KIND_NIGR)
            R_D = NR_UTIL_NIGR(HNUMR, NDLN, NNL, VDEL, VDLG, EPSA, EPSR)
         CASE DEFAULT
      END SELECT

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_CLEF_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NR_NORM_CLC = R_D
      RETURN
      END
