C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-LoCal
C Type:
C
C Class: NM_LCGL
C     INTEGER NM_LCLC_000
C     INTEGER NM_LCLC_999
C     INTEGER NM_LCLC_CTR
C     INTEGER NM_LCLC_DTR
C     INTEGER NM_LCLC_INI
C     INTEGER NM_LCLC_RST
C     INTEGER NM_LCLC_REQHBASE
C     INTEGER NM_LCLC_REQHBASE
C     LOGICAL NM_LCLC_HVALIDE
C     INTEGER NM_LCLC_CHARGE
C     INTEGER NM_LCLC_LCLGLB
C     INTEGER NM_LCLC_PRCECR
C     INTEGER NM_LCLC_DIMTBL
C     INTEGER NM_LCLC_TBLECR
C     INTEGER NM_LCLC_PRNDST
C     LOGICAL NM_LCLC_ESTNOPP
C     INTEGER NM_LCLC_REQNPRT
C     INTEGER NM_LCLC_REQNNL
C     INTEGER NM_LCLC_REQNNP
C     INTEGER NM_LCLC_REQNNT
C     INTEGER NM_LCLC_REQNEXT
C     INTEGER NM_LCLC_REQNINT
C     INTEGER NM_LCLC_REQGLB
C     INTEGER NM_LCLC_REQLCL
C     INTEGER NM_LCLC_DSYNC
C     INTEGER NM_LCLC_ISYNC
C     INTEGER NM_LCLC_GENDSYNC
C     INTEGER NM_LCLC_GENISYNC
C     INTEGER NM_LCLC_GENSYNC
C
C************************************************************************

      MODULE NM_LCLC_M

      USE NM_NUMR_M, ONLY: NM_NUMR_T
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T, SO_ALLC_KPTR2_T
      IMPLICIT NONE
      PUBLIC

C========================================================================
C========================================================================
C---     Type de donnée associé à la classe
      TYPE, EXTENDS (NM_NUMR_T) :: NM_LCLC_T
         PRIVATE
         TYPE(SO_ALLC_KPTR2_T) :: KDIST
         TYPE(SO_ALLC_KPTR1_T) :: KCOR
         TYPE(SO_ALLC_KPTR1_T) :: KCORINV
         INTEGER :: NNT
      CONTAINS
         ! ---  Méthodes de la classe
         !!!PROCEDURE, PUBLIC :: NM_LCLC_CTR
         PROCEDURE, PUBLIC :: DTR      => NM_LCLC_DTR
         PROCEDURE, PUBLIC :: INI      => NM_LCLC_INI
         PROCEDURE, PUBLIC :: RST      => NM_LCLC_RST

         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: CHARGE   => NM_LCLC_CHARGE
         PROCEDURE, PUBLIC :: DSYNC    => NM_LCLC_DSYNC
         PROCEDURE, PUBLIC :: ISYNC    => NM_LCLC_ISYNC
         PROCEDURE, PUBLIC :: GENDSYNC => NM_LCLC_GENDSYNC
         PROCEDURE, PUBLIC :: GENISYNC => NM_LCLC_GENISYNC

         PROCEDURE, PUBLIC :: REQNPRT  => NM_LCLC_REQNPRT
         PROCEDURE, PUBLIC :: REQNNL   => NM_LCLC_REQNNL
         PROCEDURE, PUBLIC :: REQNNP   => NM_LCLC_REQNNP
         PROCEDURE, PUBLIC :: REQNNT   => NM_LCLC_REQNNT

         PROCEDURE, PUBLIC :: ESTNOPP  => NM_LCLC_ESTNOPP
         PROCEDURE, PUBLIC :: REQNEXT  => NM_LCLC_REQNEXT
         PROCEDURE, PUBLIC :: REQNINT  => NM_LCLC_REQNINT

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: RAZ     => NM_LCLC_RAZ
         PROCEDURE, PRIVATE :: CLCORI  => NM_LCLC_CLCORI
      END TYPE NM_LCLC_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: NM_LCLC_CTR_SELF
      INTERFACE DEL
         PROCEDURE NM_LCLC_DTR
      END INTERFACE DEL
      
      CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que par Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      INCLUDE 'nmlclc.for'

      END MODULE NM_LCLC_M
