C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_PRNO_000
C     INTEGER DT_PRNO_999
C     INTEGER DT_PRNO_PKL
C     INTEGER DT_PRNO_UPK
C     INTEGER DT_PRNO_CTR
C     INTEGER DT_PRNO_DTR
C     INTEGER DT_PRNO_INI
C     INTEGER DT_PRNO_RST
C     INTEGER DT_PRNO_REQHBASE
C     LOGICAL DT_PRNO_HVALIDE
C     INTEGER DT_PRNO_ADDVNO
C     INTEGER DT_PRNO_CHARGE
C     INTEGER DT_PRNO_GATHER
C     INTEGER DT_PRNO_SCATTER
C     INTEGER DT_PRNO_REQNNT
C     INTEGER DT_PRNO_REQNNL
C     INTEGER DT_PRNO_REQNPRN
C     REAL*8 DT_PRNO_REQTEMPS
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>DT_PRNO_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_PRNO_NOBJMAX,
     &                   DT_PRNO_HBASE,
     &                   'Nodal Properties')

      DT_PRNO_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprno.fc'

      INTEGER  IERR
      EXTERNAL DT_PRNO_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_PRNO_NOBJMAX,
     &                   DT_PRNO_HBASE,
     &                   DT_PRNO_DTR)

      DT_PRNO_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_PRNO_HBASE

C---     Les données
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_PRNO_TEMPS(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_PRNO_HLIST(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_PRNO_NNL  (IOB))

      DT_PRNO_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_PRNO_HBASE

C---     Les données
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_PRNO_TEMPS(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_PRNO_HLIST(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_PRNO_NNL  (IOB))

      DT_PRNO_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   DT_PRNO_NOBJMAX,
     &                   DT_PRNO_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(DT_PRNO_HVALIDE(HOBJ))
         IOB = HOBJ - DT_PRNO_HBASE

         DT_PRNO_TEMPS (IOB) = -1.0D0
         DT_PRNO_HLIST (IOB) = 0
         DT_PRNO_HNUMR (IOB) = 0
         DT_PRNO_NNT   (IOB) = DT_VNOD_DIM_DIFFERE
         DT_PRNO_NNL   (IOB) = DT_VNOD_DIM_DIFFERE
         DT_PRNO_NPRN  (IOB) = 0
      ENDIF

      DT_PRNO_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprno.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_PRNO_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_PRNO_NOBJMAX,
     &                   DT_PRNO_HBASE)

      DT_PRNO_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = DT_PRNO_RST(HOBJ)

C---     Construis la liste
      IF (ERR_GOOD()) IERR = DS_LIST_CTR(HLIST)
      IF (ERR_GOOD()) IERR = DS_LIST_INI(HLIST)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_PRNO_HBASE
         DT_PRNO_TEMPS (IOB) = -1.0D0
         DT_PRNO_HLIST (IOB) = HLIST
         DT_PRNO_HNUMR (IOB) = 0
         DT_PRNO_NNT   (IOB) = DT_VNOD_DIM_DIFFERE
         DT_PRNO_NNL   (IOB) = DT_VNOD_DIM_DIFFERE
         DT_PRNO_NPRN  (IOB) = 0
      ENDIF

      DT_PRNO_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB = HOBJ - DT_PRNO_HBASE

C---     Détruis la liste
      HLIST = DT_PRNO_HLIST(IOB)
      IF (DS_LIST_HVALIDE(HLIST)) IERR = DS_LIST_DTR(HLIST)

C---     Reset les paramètres
      DT_PRNO_TEMPS (IOB) = -1.0D0
      DT_PRNO_HLIST (IOB) = 0
      DT_PRNO_HNUMR (IOB) = 0
      DT_PRNO_NNT   (IOB) = DT_VNOD_DIM_DIFFERE
      DT_PRNO_NNL   (IOB) = DT_VNOD_DIM_DIFFERE
      DT_PRNO_NPRN  (IOB) = 0

      DT_PRNO_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_PRNO_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
C------------------------------------------------------------------------

      DT_PRNO_REQHBASE = DT_PRNO_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_PRNO_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtprno.fc'
C------------------------------------------------------------------------

      DT_PRNO_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_PRNO_NOBJMAX,
     &                                  DT_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction DT_PRNO_ADDVNO ajoute des valeurs nodales à l'objet.
C     Les valeurs nodales doivent être de même dimensions ou de
C     dimensionnement différé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HVNO        Handle sur l'objet de valeurs nodales
C     NIDX        Nombre d'indices à ajouter
C     KIDX        Table des indices à ajouter
C
C Sortie:
C
C Notes:
C     Les boucles d'assignation des valeurs pourraient être fusionnées.
C     Il manque un contrôle d'erreur dans ces boucles.
C************************************************************************
      FUNCTION DT_PRNO_ADDVNO(HOBJ, HVNO, NIDX, KIDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_ADDVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HVNO
      INTEGER NIDX
      INTEGER KIDX(*)

      INCLUDE 'dtprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IX, II, ID
      INTEGER HLIST
      INTEGER NVNO, NNT_L, NNT_S, NNL_L, NNL_S
      LOGICAL DOCTRL, TROUVE
      CHARACTER*(128) BUF

      REAL*8, PARAMETER :: VSHFT = 0.0D0
      REAL*8, PARAMETER :: VSCAL = 1.0D0
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NIDX .EQ. -1 .OR. NIDX .GT. 0)
C------------------------------------------------------------------------

C---     Contrôle des paramètres
      IF (.NOT. DT_VNOD_HVALIDE(HVNO)) GOTO 9901
      NVNO  = DT_VNOD_REQNVNO(HVNO)
      NNT_S = DT_VNOD_REQNNT (HVNO)
      NNL_S = DT_VNOD_REQNNL (HVNO)
D     CALL ERR_ASR((NVNO .GT. 0) .OR.
D    &             (NVNO .EQ. 0 .AND.
D    &              NNL_S .EQ. DT_VNOD_DIM_DIFFERE .AND.
D    &              NNT_S .EQ. DT_VNOD_DIM_DIFFERE))
      DOCTRL = .TRUE.
      IF (NIDX .EQ. -1 .AND. NVNO .EQ. 0) GOTO 9902
      IF (NIDX .EQ. -1 .AND. NVNO .GT. 0) DOCTRL = .TRUE.
      IF (NIDX .GT.  0 .AND. NVNO .EQ. 0) DOCTRL = .FALSE.
      IF (NIDX .GT.  0 .AND. NVNO .GT. 0) DOCTRL = .TRUE.
      IF (DOCTRL) THEN
         IF (NIDX .GT. NVNO) GOTO 9903
         DO IX=1,NIDX
            IF (KIDX(IX) .LE. 0) GOTO 9904
            IF (KIDX(IX) .GT. NVNO) GOTO 9904
            DO II=1,IX-1
               IF (KIDX(II) .EQ. KIDX(IX)) GOTO 9905
            ENDDO
         ENDDO
      ENDIF

C---     Récupère les attributs
      IOB   = HOBJ - DT_PRNO_HBASE
      NNT_L = DT_PRNO_NNT  (IOB)
      NNL_L = DT_PRNO_NNL  (IOB)
      HLIST = DT_PRNO_HLIST(IOB)
D     CALL ERR_ASR(DS_LIST_HVALIDE(HLIST))

C---     Assigne NNL & NNT
      IF (NNT_L .EQ. DT_VNOD_DIM_DIFFERE) THEN
         IF (NNT_S .NE. DT_VNOD_DIM_DIFFERE) NNT_L = NNT_S
      ENDIF
      IF (NNL_L .EQ. DT_VNOD_DIM_DIFFERE) THEN
         IF (NNL_S .NE. DT_VNOD_DIM_DIFFERE) NNL_L = NNL_S
      ENDIF

C---     Contrôle NNL & NNT
      IF(NNT_S .NE. DT_VNOD_DIM_DIFFERE .AND. NNT_S .NE. NNT_L)GOTO 9906
      IF(NNL_S .NE. DT_VNOD_DIM_DIFFERE .AND. NNL_S .NE. NNL_L)GOTO 9907

C---     Assigne les valeurs
      IF (NIDX .EQ. -1) THEN
D        CALL ERR_ASR(NVNO .GT. 0)
         DO IX=1,NVNO
            ID = IX
            WRITE(BUF, *) HVNO, ID, VSHFT, VSCAL

            IERR = DS_LIST_TRVVAL(HLIST, II, BUF)
            IF (ERR_GOOD()) GOTO 9908
            CALL ERR_RESET()

            IERR = DS_LIST_AJTVAL(HLIST, BUF)
         ENDDO
      ELSE
         DO IX=1,NIDX
            ID = KIDX(IX)
            WRITE(BUF, *) HVNO, ID, VSHFT, VSCAL

            IERR = DS_LIST_TRVVAL(HLIST, II, BUF)
            IF (ERR_GOOD()) GOTO 9908
            CALL ERR_RESET()

            IERR = DS_LIST_AJTVAL(HLIST, BUF)
         ENDDO
      ENDIF
      DT_PRNO_TEMPS(IOB) = -1.0D0
      DT_PRNO_NNT  (IOB) = NNT_L
      DT_PRNO_NNL  (IOB) = NNL_L
      DT_PRNO_NPRN (IOB) = DS_LIST_REQDIM(HLIST)

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',HVNO
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_NON_DIMENSIONNE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',HVNO
      CALL ERR_AJT(ERR_BUF)
      CALL ERR_AJT('MSG_SPECIFIER_EXPLICITEMENT_LES_COLONNES')
      GOTO 9999
9903  WRITE(ERR_BUF,'(A)')'ERR_NBR_INDICE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_MIN', ':', 0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_MAX', ':', DT_VNOD_REQNVNO(HVNO)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF,'(A)')'ERR_INDICE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':', KIDX(IX)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_MIN', ':', 0
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_MAX', ':', DT_VNOD_REQNVNO(HVNO)
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF,'(2A,I12)')'ERR_INDICE_REPETE',':',KIDX(IX)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF,'(2A,I12,A,I12)')'ERR_NNT_INCOHERENT',':',
     &                        NNT_S, '/', NNT_L
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9907  WRITE(ERR_BUF,'(2A,I12,A,I12)')'ERR_NNL_INCOHERENT',':',
     &                        NNL_S, '/', NNL_L
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9908  WRITE(ERR_BUF,'(A)')'ERR_VNO_EN_DOUBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',HVNO
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':', ID
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRNO_ADDVNO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_PRNO_CHARGE
C
C Description:
C     La méthode DT_PRNO_CHARGE charge tous les DT_VNOD de l'objet au
C     temps TNEW. Après chargement, un appel à DT_PRNO_GATHER permet
C     de récupérer les données dans une table fournie.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HNUMR       Handle sur la numérotation des noeuds
C     TNEW        Temps demandé
C
C Sortie:
C
C Notes:
C     Avant la 1er appel à charge, les NNL sont à DT_VNOD_DIM_DIFFERE. Après,
C     ils sont non nuls et identiques. Si HNUM change, NNL sera différent en sortie.
C     Si un VNO est utilisé dans plusieurs PRNO et que la renumérotation change,
C     alors il y aura une erreur détecté sur NNL non compatible (false positive)
C************************************************************************
      FUNCTION DT_PRNO_CHARGE(HOBJ, HNUMR, TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'dtprno.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
      INTEGER HVNO, HLAST, HTMP
      INTEGER IP, II
      INTEGER NPRN, NNT, NNL, NNT_S, NNL_S
      LOGICAL MODIF, DOITLIRE
      CHARACTER*(128) BUF
      INTEGER KLIRE(24), NLIRE
!$    INTEGER I_COMM, I_SIZE, I_ERROR      
!$    INTEGER NB_OMP
!$    LOGICAL DO_OMP
!$    INTEGER omp_get_max_threads
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - DT_PRNO_HBASE
      HLIST = DT_PRNO_HLIST(IOB)
      NNL   = DT_PRNO_NNL  (IOB)
      NNT   = DT_PRNO_NNT  (IOB)
      NPRN  = DT_PRNO_NPRN (IOB)
D     CALL ERR_ASR(NPRN .EQ. DS_LIST_REQDIM(HLIST))

C---     Valeurs nodales à lire
      NLIRE = 0
      DO IP=1,NPRN
         IERR = DS_LIST_REQVAL(HLIST, IP, BUF)
         READ(BUF, *, END=9901, ERR=9901) HVNO

C---        Recherche s'il a déjà été lu
         DO II=1,NLIRE
            IF (KLIRE(II) .EQ. HVNO) EXIT
         ENDDO
         DOITLIRE = (II .GT. NLIRE)

C---        Contrôle la cohérence des NNL et lis
         IF (DOITLIRE) THEN
            NNL_S = DT_VNOD_REQNNL(HVNO)
            NNT_S = DT_VNOD_REQNNT(HVNO)
            IF (NNL .EQ. DT_VNOD_DIM_DIFFERE) NNL = NNL_S
            IF (NNT .EQ. DT_VNOD_DIM_DIFFERE) NNT = NNT_S
            IF (NNL .NE. NNL_S .AND.
     &          NNL_S .NE. DT_VNOD_DIM_DIFFERE) GOTO 9902
            IF (NNT .NE. NNT_S) GOTO 9903
            NLIRE = NLIRE + 1
            KLIRE(NLIRE) = HVNO
         ENDIF

         IF (ERR_BAD()) GOTO 199
      ENDDO
199   CONTINUE

C---     Lis les valeurs nodales
!$    I_COMM = MP_UTIL_REQCOMM()
!$    CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
!$    DO_OMP = I_SIZE .LE. 1
!$    NB_OMP = omp_get_max_threads() - omp_get_max_threads()/2 + 1
!$    NB_OMP = min(NB_OMP, 4)
!$    NB_OMP = max(NB_OMP, 1)
!$omp parallel
!$omp& num_threads(NB_OMP) if (DO_OMP)
!$omp& private(IP, IERR, MODIF, HVNO, BUF)
!$omp& default(shared)
!$omp do
!$omp& schedule(static, 1)
      DO II=1,NLIRE
         HVNO = KLIRE(II)
         IF (ERR_GOOD()) IERR = DT_VNOD_CHARGE(HVNO, HNUMR, TNEW, MODIF)
      ENDDO
!$omp end do
      IERR = ERR_OMP_RDC()
!$omp end parallel

C---     Conserve les valeurs
      IF (ERR_GOOD()) THEN
         DT_PRNO_TEMPS(IOB) = TNEW
         DT_PRNO_HNUMR(IOB) = HNUMR
         DT_PRNO_NNT  (IOB) = NNT
         DT_PRNO_NNL  (IOB) = NM_NUMR_REQNNL(HNUMR) ! c.f. note
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)')'ERR_PRNO_NON_DIMENSIONNE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_BUFFER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12,A,I12)')'ERR_NNL_INCOHERENT',':',
     &                        NNL, '/', NNL_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(2A,I12,A,I12)')'ERR_NNT_INCOHERENT',':',
     &                        NNT, '/', NNT_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRNO_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_PRNO_GATHER
C
C Description:
C     La méthode DT_PRNO_GATHER rassemble les données des DT_VNOD de l'objet
C     dans la table VPRNO.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HNUMR       Handle sur la numérotation des noeuds
C     NPRN        Dimensions de la table VPRNO
C     NNL
C     DOGTHR      Table: .TRUE. s'il faut charger la colonne
C
C Sortie:
C     VPRNO       Table mise à jour
C     TNEW        Temps
C
C Notes:
C     Le temps retourné est mis au temps de l'objet, qui est assigné dans
C     CHARGE. Entre temps, le temps peut ne plus être cohérent entre
C     les VNO.
C************************************************************************
      FUNCTION DT_PRNO_GATHER (HOBJ,
     &                         HNUMR,
     &                         NPRN,
     &                         NNL,
     &                         VPRNO,
     &                         DOGTHR,
     &                         TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_GATHER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPRN
      INTEGER NNL
      REAL*8  VPRNO(NPRN, NNL)
      LOGICAL DOGTHR(NPRN)
      REAL*8  TEMPS

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtprno.fc'

      REAL*8  VSHFT, VSCAL
      INTEGER IERR
      INTEGER IOB
      INTEGER HLIST
      INTEGER HVNO, IVNO
      INTEGER NVNO, LVNO
      INTEGER NPRNL
      INTEGER IP
      CHARACTER*(128) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNL  .LE. DT_PRNO_REQNNL (HOBJ))
D     CALL ERR_PRE(NPRN .GE. DT_PRNO_REQNPRN(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - DT_PRNO_HBASE
      HLIST = DT_PRNO_HLIST(IOB)
      NPRNL = DT_PRNO_NPRN (IOB)
D     CALL ERR_ASR(NPRNL .EQ. DS_LIST_REQDIM(HLIST))

C---     Rassemble les valeurs nodales
      DO IP = 1, NPRNL
         IF (.NOT. DOGTHR(IP)) CYCLE

         IERR = DS_LIST_REQVAL(HLIST, IP, BUF)
         READ(BUF, *, END=9900, ERR=9900) HVNO, IVNO, VSHFT, VSCAL

         IERR = DT_VNOD_REQ1VAL(HVNO,
     &                          HNUMR,
     &                          IP,
     &                          NPRN,
     &                          VPRNO,
     &                          IVNO,
     &                          TEMPS)

!!!         VPRNO(IP,:) = (VPRNO(IP,:) + VSHFT) * VSCAL
      ENDDO
      TEMPS = DT_PRNO_TEMPS(IOB)

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_BUFFER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRNO_GATHER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_PRNO_SCATTER
C
C     La méthode DT_PRNO_SCATTER distribue les données de la table VPRNO
C     vers les DT_VNOD de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C     HNUMR       Handle sur la numérotation des noeuds
C     NPRN        Dimension de la table VPRNO
C     NNL
C     VPRNO       Table mise à jour
C     DOSCTR      Table
C     TNEW        Temps
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_SCATTER(HOBJ,
     &                         HNUMR,
     &                         NPRN,
     &                         NNL,
     &                         VPRNO,
     &                         DOSCTR,
     &                         TNEW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_SCATTER
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NPRN
      INTEGER NNL
      REAL*8  VPRNO (NPRN, NNL)
      LOGICAL DOSCTR(NPRN)
      REAL*8  TNEW

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtprno.fc'

      REAL*8  VSHFT, VSCAL
      INTEGER IERR
      INTEGER IOB
      INTEGER IP
      INTEGER HLIST
      INTEGER HVNO, IVNO
      INTEGER NPRNL
      CHARACTER*(128) BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNL  .LE. DT_PRNO_REQNNT (HOBJ))
D     CALL ERR_PRE(NPRN .GE. DT_PRNO_REQNPRN(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - DT_PRNO_HBASE
      HLIST = DT_PRNO_HLIST(IOB)
      NNL   = DT_PRNO_NNL  (IOB)
      NPRNL = DT_PRNO_NPRN (IOB)
D     CALL ERR_ASR(NPRNL .EQ. DS_LIST_REQDIM(HLIST))

C---     Distribue les propriétés nodales
      DO IP = 1, NPRNL
         IF (.NOT. DOSCTR(IP)) CYCLE

         IERR = DS_LIST_REQVAL(HLIST, IP, BUF)
         READ(BUF, *, END=9900, ERR=9900) HVNO, IVNO, VSHFT, VSCAL

!!!         VPRNO(IP,:) = VPRNO(IP,:) / VSCAL - VSHFT
         IERR = DT_VNOD_ASG1VAL(HVNO,
     &                          HNUMR,
     &                          IP,
     &                          NPRN,
     &                          VPRNO,
     &                          IVNO,
     &                          TNEW)
!!!         VPRNO(IP,:) = (VPRNO(IP,:) + VSHFT) * VSCAL
      ENDDO

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_BUFFER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_PRNO_SCATTER = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_PRNO_REQNNT = DT_PRNO_NNT(HOBJ-DT_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_REQNNL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_PRNO_REQNNL = DT_PRNO_NNL(HOBJ-DT_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_REQNPRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_REQNPRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_PRNO_REQNPRN = DT_PRNO_NPRN(HOBJ-DT_PRNO_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps des données.
C
C Description:
C     La fonction DT_VNOD_REQTEMPS retourne le temps associé aux données
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_PRNO_REQTEMPS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_PRNO_REQTEMPS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtprno.fi'
      INCLUDE 'dtprno.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_PRNO_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_PRNO_REQTEMPS = DT_PRNO_TEMPS(HOBJ-DT_PRNO_HBASE)
      RETURN
      END
