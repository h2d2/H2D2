C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Note:
C     Le fichier peut s'employer pour toute donnée réelle, stockée par
C     bloc pour chaque pas de temps, avec renumérotation; donc autant
C     pour des valeurs nodales que pour des valeurs élémentaires.
C     <p>
C     nlines ncols time
C     v_1 v_2 v_3 ... v_ncols
C
C Functions:
C   Public:
C     INTEGER FD_PRNA_000
C     INTEGER FD_PRNA_999
C     INTEGER FD_PRNA_PKL
C     INTEGER FD_PRNA_UPK
C     INTEGER FD_PRNA_CTR
C     INTEGER FD_PRNA_DTR
C     INTEGER FD_PRNA_INI
C     INTEGER FD_PRNA_RST
C     INTEGER FD_PRNA_REQHBASE
C     LOGICAL FD_PRNA_HVALIDE
C     INTEGER FD_PRNA_ECRSCT
C     INTEGER FD_PRNA_LISSCT
C   Private:
C     INTEGER FD_PRNA_OPEN
C     INTEGER FD_PRNA_CLOSE
C     INTEGER FD_PRNA_RHDR
C     INTEGER FD_PRNA_BHDR
C     INTEGER FD_PRNA_BDTA
C     INTEGER FD_PRNA_INI1FIC
C     INTEGER FD_PRNA_INISTR
C     INTEGER FD_PRNA_REQDIM
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_PRNA_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprna.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_PRNA_NOBJMAX,
     &                   FD_PRNA_HBASE,
     &                   'Nodal Properties File')

      FD_PRNA_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_PRNA_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprna.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER  IERR
      EXTERNAL FD_PRNA_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_PRNA_NOBJMAX,
     &                   FD_PRNA_HBASE,
     &                   FD_PRNA_DTR)

      FD_PRNA_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdprna.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB = HOBJ - FD_PRNA_HBASE
      HPRNT = FD_PRNA_HPRNT(IOB)

C---     ECRIS LES DONNÉES
      IERR = IO_XML_WH_A(HXML, HPRNT)

      FD_PRNA_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdprna.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_PRNA_HBASE

C---     Les données
      IERR = IO_XML_RH_A(HXML, FD_PRNA_HPRNT(IOB))

      FD_PRNA_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_PRNA_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprna.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   FD_PRNA_NOBJMAX,
     &                   FD_PRNA_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(FD_PRNA_HVALIDE(HOBJ))
         IOB = HOBJ - FD_PRNA_HBASE

         FD_PRNA_HPRNT (IOB) = 0
      ENDIF

      FD_PRNA_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_PRNA_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprna.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_PRNA_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_PRNA_NOBJMAX,
     &                   FD_PRNA_HBASE)

      FD_PRNA_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_PRNA_INI initialise l'objet à l'aide des
C     paramètres. Elle complète l'initialisation du parent.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HPRNT       Handle sur le parent
C     HLIST       Handle sur la liste des noms de fichier
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_INI(HOBJ, HPRNT, HLIST, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRNT
      INTEGER HLIST
      INTEGER ISTAT

      INCLUDE 'fdprna.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HPRNT))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C------------------------------------------------------------------------

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.ascii')

C---     Contrôle les paramètres
      IF (DS_LIST_REQDIM(HLIST) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901
      IF (.NOT. IO_UTIL_MODACTIF(ISTAT, IO_MODE_ASCII)) GOTO 9901

C---     Reset les données
      IERR = FD_PRNA_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_PRNA_HBASE
         FD_PRNA_HPRNT(IOB) = HPRNT
      ENDIF

C---     En lecture, initialise la structure interne
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         IF (ERR_GOOD()) IERR = FD_PRNA_INISTR(HOBJ, HLIST)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I3)') 'ERR_MODE_ACCES_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.ascii')
      FD_PRNA_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_PRNA_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprna.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_PRNA_HBASE

C---     RESET LES ATTRIBUTS
      FD_PRNA_HPRNT (IOB) = 0

      FD_PRNA_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_PRNA_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprna.fc'
C------------------------------------------------------------------------

      FD_PRNA_REQHBASE = FD_PRNA_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_PRNA_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprna.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdprna.fc'
C------------------------------------------------------------------------

      FD_PRNA_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_PRNA_NOBJMAX,
     &                                  FD_PRNA_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNA_ECRSCT
C
C Description:
C     La fonction FD_PRNA_ECRSCT est la fonction principale d'écriture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C     Il est plus simple d'écrire directement en FTN plutôt que de passer
C     par les fonction C de C_FA.
C************************************************************************
      FUNCTION FD_PRNA_ECRSCT(HOBJ,
     &                        FNAME,
     &                        ISTAT,
     &                        HNUMR,
     &                        NNT,
     &                        NNL,
     &                        NPRN,
     &                        VPRNO,
     &                        TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_ECRSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT
      INTEGER       HNUMR
      INTEGER       NNT
      INTEGER       NNL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNL)
      REAL*8        TSIM

      INCLUDE 'fdprna.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprna.fc'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER I_RANK, I_SIZE, I_ERROR, I_TAG, I_COMM
      INTEGER M1
      INTEGER IN, INL, ID, IR, IRSND
      REAL*8  VTMP(FD_PRNA_MAXCOL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NNT  .GE. NNL)
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0)
D     CALL ERR_PRE(NPRN .LE. FD_PRNA_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.prno.ascii.write')

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNA_HBASE

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         M1 = IO_UTIL_FREEUNIT()
         IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_ECRITURE)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'REPLACE')
         ELSEIF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_AJOUT)) THEN
            OPEN (UNIT  = M1,
     &            ERR   = 109,
     &            FILE  = FNAME(1:SP_STRN_LEN(FNAME)),
     &            FORM  = 'FORMATTED',
     &            STATUS= 'UNKNOWN',
     &            ACCESS='APPEND')     ! Non standard
C     &            POSITION='APPEND')  ! Pas implanté par g77
         ELSE
            IERR = -2
         ENDIF
         GOTO 110
109      CONTINUE
         IERR = -1
110      CONTINUE
      ENDIF
C---     BROADCAST LE CODE D'ERREUR
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9900
      IF (IERR .EQ. -2) GOTO 9901
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Écris l'entête
      IERR = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         WRITE (M1,'(I12,1X,I12,1X,1PE25.17E3)',ERR=209) NNT,NPRN,TSIM
         GOTO 210
209      CONTINUE
         IERR = -1
210      CONTINUE
      ENDIF
C---     Broadcast le code d'erreur à cause du write
      CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      IF (IERR .EQ. -1) GOTO 9902
D     CALL ERR_ASR(IERR .EQ. 0)

C---     Si on a un seul process
      IF (I_SIZE .GT. 1) GOTO 1000
      DO IN=1,NNT
         INL = NM_NUMR_REQNINT(HNUMR, IN)
         WRITE (M1,'(25(1PE26.17E3))',ERR=9902)(VPRNO(ID,INL),ID=1,NPRN)
      ENDDO
      GOTO 1999

C---     Écris la séquence des propriétés nodales
1000  CONTINUE
      I_TAG = 12
      DO IN=1,NNT
         INL = NM_NUMR_REQNINT(HNUMR, IN)
         IR  = -1
         IF (INL .GT. 0) THEN
            IF (NM_NUMR_ESTNOPP(HNUMR, INL)) IR = I_RANK
         ENDIF
         CALL MPI_ALLREDUCE(IR, IRSND, 1, MP_TYPE_INT(), MPI_MAX,
     &                      I_COMM, I_ERROR)
         IF (ERR_BAD()) GOTO 1999

         IERR = 0
         IF (I_RANK .EQ. I_MASTER) THEN
            IF (IRSND .LT. 0) THEN
               CALL DINIT(NPRN, 1.0D99, VTMP, 1)
            ELSEIF (IRSND .EQ. I_RANK) THEN
D              CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
               CALL DCOPY(NPRN, VPRNO(1,INL), 1, VTMP, 1)
            ELSE
               CALL MPI_RECV(VTMP, NPRN, MP_TYPE_RE8(),
     &                       IRSND,
     &                       I_TAG, I_COMM, I_STATUS,I_ERROR)
               IF (ERR_BAD()) GOTO 1999
            ENDIF
            WRITE (M1,'(25(1PE26.17E3))',ERR=1009) (VTMP(ID),ID=1,NPRN)
            GOTO 1010
1009        CONTINUE
            IERR = -1
1010        CONTINUE

         ELSEIF (IRSND .EQ. I_RANK) THEN
D           CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
            CALL MPI_SEND(VPRNO(1,INL), NPRN, MP_TYPE_RE8(),
     &                    I_MASTER,
     &                    I_TAG, I_COMM, I_ERROR)
            IF (ERR_BAD()) GOTO 1999
         ENDIF

C---        Broadcast le code d'erreur à cause du write
         CALL MPI_BCAST(IERR, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (IERR .EQ. -1) GOTO 9902
D        CALL ERR_ASR(IERR .EQ. 0)

      ENDDO

C---     FERME LE FICHIER
1999  CONTINUE
      IF (I_RANK .EQ. I_MASTER) CLOSE(M1)

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_MODE_ACCES_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_ECRITURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.ascii.write')
      FD_PRNA_ECRSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNA_LISSCT
C
C Description:
C     La fonction privée FD_PRNA_LISSCT lis une section d'un fichier
C     de type ProprietesNodales.
C
C Entrée:
C     XFIC     Handle sur le fichier
C     HNUMR    Handle sur la renumérotation
C     NNTL     Nombre de Noeuds Total Local au process
C     NPRN     Nombre de propriétés
C
C Sortie:
C     VPRNO    Table des valeurs lues
C
C Notes:
C     Le fichier est lu avec les fonctions C C_FA qui permettent de
C     repositionner.
C************************************************************************
      FUNCTION FD_PRNA_LISSCT(HOBJ,
     &                        FNAME,
     &                        XPOS,
     &                        TEMPS,
     &                        HNUMR,
     &                        NNTL,
     &                        NPRN,
     &                        VPRNO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRNA_LISSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER*8     XPOS      ! Offset dans le fichier
      REAL*8        TEMPS     ! Pour contrôles
      INTEGER       HNUMR
      INTEGER       NNTL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNTL)

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprna.fc'

      INTEGER I_COMM, I_RANK, I_SIZE, I_ERROR
      INTEGER, PARAMETER :: I_MASTER = 0

      INTEGER*8 XFIC
      INTEGER IERR
      INTEGER IB, ID, IN, INL
      INTEGER NNB, NNL
      INTEGER NNTG , NNTL_SUM
      INTEGER NPRN_L
      REAL*8  TMPS_L

      INTEGER, PARAMETER :: NBMAX = 32
      REAL*8  VTMP(FD_PRNA_MAXCOL*NBMAX)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNTL .GT. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NPRN .LE. FD_PRNA_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.ascii.read')

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      XFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_PRNA_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Lis l'entête
      NNTG = -1
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_PRNA_RHDR(HOBJ, XFIC, NNTG, NPRN_L, TMPS_L)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast the data
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NNTG,  1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(NPRN_L,1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(TMPS_L,1,MP_TYPE_RE8(),I_MASTER,I_COMM,I_ERROR)
      ENDIF

C---     Contrôle la cohérence des dimensions locales et globales
      IF (ERR_GOOD()) THEN
         IF (I_SIZE .LE. 1) THEN       ! 1 PROC ==> NNTL = NNTG
            IF (NNTG .NE. NNTL) GOTO 9906
         ELSE                          ! n PROC ==> SUM(NNTL) > NNTG
            CALL MPI_ALLREDUCE(NNTL, NNTL_SUM, 1, MP_TYPE_INT(),
     &                         MPI_SUM, I_COMM, I_ERROR)
            IF (ERR_GOOD() .AND. NNTL_SUM .LE. NNTG) GOTO 9907
         ENDIF
      ENDIF
      IF (ERR_BAD()) GOTO 9988

C---     Lis les lignes par blocs
      NNL = 0
      DO IB=1, NNTG, NBMAX
         NNB = MIN(NNTG-IB+1, NBMAX)
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_PRNA_BDTA(HOBJ, XFIC, NNB, NPRN, VTMP)
         ENDIF
         IERR = MP_UTIL_SYNCERR()

C---        Broadcast the data
         IF (ERR_GOOD()) THEN    ! devrait être asynch.
            CALL MPI_BCAST(VTMP, NPRN*NNB, MP_TYPE_RE8(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF

C---        Conserve si concerné
         IF (ERR_GOOD()) THEN
            ID = 1
            DO IN=IB,IB+NNB-1
               INL = NM_NUMR_REQNINT(HNUMR, IN)
               IF (INL .GT. 0) THEN
                  CALL DCOPY(NPRN, VTMP(ID), 1, VPRNO(1,INL), 1)
                  NNL = NNL + 1
               ENDIF
               ID = ID + NPRN
            ENDDO
         ENDIF

      ENDDO

      IF (NNL .NE. NNTL) GOTO 9908

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9904  WRITE(ERR_BUF,'(A,2(A,I6))') 'ERR_NPRN_INCOHERENT', ': ',
     &                       NPRN_L, ' / ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF,'(A,2(A,1PE14.6E3))') 'ERR_TEMPS_INCOHERENT', ': ',
     &                       TMPS_L, ' / ', TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NNL_GE_NNT',': ',
     &      NNTL, ' / ', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_SUM(NNL)_LE_NNT',': ',
     &      NNTL_SUM, ' / ', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9908  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NBR_VALEURS_LUES',': ',
     &      NNL, ' / ', NNTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF,'(2A,F25.6)') 'MSG_TEMPS', ': ', TEMPS
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I25)') 'MSG_OFFSET', ': ', XPOS
      CALL ERR_AJT(ERR_BUF)

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_PRNA_CLOSE(HOBJ, XFIC)
      IERR = MP_UTIL_SYNCERR()
      CALL TR_CHRN_STOP('h2d2.io.prno.ascii.read')
      FD_PRNA_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNA_OPEN
C
C Description:
C     La fonction privée FD_PRNA_OPEN ouvre le fichier à la position
C     XPOS.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     FNAME    Nom du fichier
C     ISTA     Status du fichier
C     XPOS     Offset dans le fichier
C
C Sortie:
C     XFIC     Handle sur le fichier
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_OPEN(HOBJ,
     &                      FNAME,
     &                      ISTA,
     &                      XPOS,
     &                      XFIC)

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER       ISTA
      INTEGER*8     XPOS      ! Offset dans le fichier
      INTEGER*8     XFIC

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0
      XFIC = 0

C---     Ouvre le fichier
      XFIC = C_FA_OUVRE(FNAME(1:SP_STRN_LEN(FNAME)), ISTA)
      IF (XFIC .EQ. 0) GOTO 9900

C---     Positionne dans le fichier
      IF (XPOS .GT. 0) THEN
         IRET = C_FA_ASGPOS(XFIC, XPOS)
         IF (IRET .NE. 0) GOTO 9901
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNA_OPEN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNA_CLOSE
C
C Description:
C     La fonction privée FD_PRNA_CLOSE ferme le fichier.
C
C Entrée:
C     XFIC     Handle sur le fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_CLOSE(HOBJ, XFIC)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR, IRET
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = 0

      IRET = C_FA_FERME(XFIC)
      IF (IRET .NE. 0) GOTO 9900

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FERMETURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNA_CLOSE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis une entête
C
C Description:
C     La fonction privée FD_PRNA_RHDR lis une entête.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     XFIC           Handle sur le fichier
C
C Sortie:
C     NNT
C     NPRN
C     TMPS
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_RHDR(HOBJ,
     &                      XFIC,
     &                      NNT,
     &                      NPRN,
     &                      TMPS)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC
      INTEGER   NNT
      INTEGER   NPRN
      REAL*8    TMPS

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprna.fc'

      INTEGER   IERR, IRET
      INTEGER   LB

      INTEGER    LBUF
      PARAMETER (LBUF = 1024)
      CHARACTER*(LBUF)  BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise
      BUF  = ' '
      NNT  = 0
      NPRN = 0

C---     Lis l'entête
      IRET = C_FA_LISLN(XFIC, BUF)
      IF (IRET .NE. 0) GOTO 9901
      LB = SP_STRN_LEN(BUF)
      IF (LB .LE. 0) GOTO 9901
      READ(BUF(1:LB), *, ERR=9902, END=9902) NNT, NPRN, TMPS

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNA_RHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Lis un groupe d'entêtes
C
C Description:
C     La fonction privée FD_PRNA_BHDR lis un maximum de NBMAX entêtes.
C     Elle ne doit être appelée que par le process maître.
C
C Entrée:
C     HOBJ              Handle sur l'objet
C     XFIC              Handle eXterne sur le fichier
C     NNT               Nombre de Noeuds Total
C     NPRN              Nombre de Propriétés Nodales
C     TMAX              Temps max du fichier
C     NBMAX             Nombre max de bloc à lire
C
C Sortie:
C     TMAX              Temps max du fichier
C     IBMAX             Nombre de blocs lus
C     KPOS              Table des positions dans le fichier
C     VTMP              Table des temps
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_BHDR(HOBJ,
     &                      XFIC,
     &                      NNT,
     &                      NPRN,
     &                      TMAX,
     &                      IBMAX,
     &                      NBMAX,
     &                      KPOS,
     &                      VTMP)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC
      INTEGER   NNT
      INTEGER   NPRN
      REAL*8    TMAX
      INTEGER   IBMAX
      INTEGER   NBMAX
      INTEGER*8 KPOS(NBMAX)
      REAL*8    VTMP(NBMAX)

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprna.fc'

      REAL*8    TMPS_L
      INTEGER*8 XPOS
      INTEGER   IB
      INTEGER   IERR, IRET
      INTEGER   LB
      INTEGER   NNT_L
      INTEGER   NPRN_L

      INTEGER    LBUF
      PARAMETER (LBUF = 1024)
      CHARACTER*(LBUF)  BUF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NPRN .LE. FD_PRNA_MAXCOL)
D     CALL ERR_PRE(NNT  .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Initialise
      BUF  = ' '

C---     Boucle sur les blocs
      DO IB=1,NBMAX
         IRET = C_FA_REQPOS(XFIC, XPOS)
         IRET = C_FA_LISLN (XFIC, BUF)
         IF (IRET .NE. 0) GOTO 199
         LB = SP_STRN_LEN(BUF)
         IF (LB .LE. 0) GOTO 199
         READ(BUF(1:LB), *, ERR=9902, END=9902) NNT_L, NPRN_L, TMPS_L

C---        Contrôles
         IF (NNT_L  .NE. NNT)  GOTO 9903
         IF (NPRN_L .NE. NPRN) GOTO 9904
         IF (TMPS_L .LE. TMAX) GOTO 9905

C---        Assigne les paramètres de section
         KPOS(IB) = XPOS
         VTMP(IB) = TMPS_L

C---        Saute le bloc de données
         IRET = C_FA_SKIPN(XFIC, NNT)
         IF (IRET .NE. 0) GOTO 9901

         TMAX  = TMPS_L
         IBMAX = IB
      ENDDO
199   CONTINUE

      GOTO 9999
C-----------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(A)') 'ERR_NNT_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NNT:', NNT_L, '/', NNT
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A)') 'ERR_NPRN_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NPRN:', NPRN_L, '/', NPRN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9905  WRITE(ERR_BUF, '(A)') 'ERR_TEMPS_DECROISSANT'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,1PE25.17E3))') 'MSG_TEMPS:',TMPS_L,' <=',TMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNA_BHDR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRNA_BDTA
C
C Description:
C     La fonction privée FD_PRNA_BDTA lis NNTL lignes de données d'un fichier
C     de type ProprietesNodales.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     XFIC     Handle sur le fichier
C     NNTL     Nombre de Noeuds à lire
C     NPRN     Nombre de propriétés
C
C Sortie:
C     VPRNO    Table des valeurs lues
C
C Notes:
C************************************************************************
      FUNCTION FD_PRNA_BDTA(HOBJ,
     &                      XFIC,
     &                      NNTL,
     &                      NPRN,
     &                      VPRNO)

      IMPLICIT NONE

      INTEGER   HOBJ
      INTEGER*8 XFIC      ! Offset dans le fichier
      INTEGER   NNTL
      INTEGER   NPRN
      REAL*8    VPRNO(NPRN, NNTL)

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprna.fc'

      INTEGER IERR, IRET
      INTEGER IN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPRN .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

      DO IN=1,NNTL
         IRET = C_FA_LISDBL(XFIC, VPRNO(1,IN), NPRN)
         IF (IRET .NE. 0) GOTO 9900
      ENDDO

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      FD_PRNA_BDTA = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure pour un fichier
C
C Description:
C     La fonction privée FD_PRNA_INI1FIC initialise la structure interne à
C     partir du fichier dont le nom est passé en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ                    Handle sur l'objet
C     NPRN
C     NNT
C     TFIN
C     IFIC                    Indice du fichier
C     FNAME                   Nom du fichier
C
C Sortie:
C
C Notes:
C     Le fichier est lu avec les fonctions C C_FA qui permettent de
C     repositionner.
C************************************************************************
      FUNCTION FD_PRNA_INI1FIC(HOBJ, NPRN, NNT, TMAX, IFIC, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRN
      INTEGER NNT
      REAL*8  TMAX
      INTEGER IFIC
      CHARACTER*(*) FNAME

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprna.fc'

      INTEGER I_RANK, I_ERROR, I_COMM
      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER   NBMAX, IDUM_
      PARAMETER (NBMAX = 128)

      REAL*8    VTMP(NBMAX)
      REAL*8    TEMPS
      INTEGER*8 KPOS(NBMAX)
      INTEGER*8 XPOS
      INTEGER*8 XFIC       ! handle eXterne sur le fichier
      INTEGER   IB, IBMAX
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   HPRNT
      INTEGER   NBTOT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - FD_PRNA_HBASE
      HPRNT = FD_PRNA_HPRNT(IOB)

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Ouvre le fichier
      XFIC = 0
      XPOS = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = FD_PRNA_OPEN(HOBJ,
     &                       FNAME(1:SP_STRN_LEN(FNAME)),
     &                       C_FA_LECTURE,
     &                       XPOS,
     &                       XFIC)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     WHILE (ERR_GOOD())
      NBTOT = 0
      DO WHILE(ERR_GOOD())
         IBMAX = 0
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = FD_PRNA_BHDR(HOBJ,
     &                          XFIC,
     &                          NNT,
     &                          NPRN,
     &                          TMAX,
     &                          IBMAX,
     &                          NBMAX,
     &                          KPOS,
     &                          VTMP)
         ENDIF
         IERR = MP_UTIL_SYNCERR()

C---        Broadcast les données
         IF (ERR_GOOD()) THEN
            CALL MPI_BCAST(IBMAX, 1, MP_TYPE_INT(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF
         IF (ERR_GOOD() .AND. IBMAX .GT. 0) THEN
            CALL MPI_BCAST(KPOS, IBMAX, MP_TYPE_IN8(),
     &                     I_MASTER, I_COMM, I_ERROR)
            CALL MPI_BCAST(VTMP, IBMAX, MP_TYPE_RE8(),
     &                     I_MASTER, I_COMM, I_ERROR)
         ENDIF

C---        Assigne les paramètres de section
         IF (ERR_GOOD()) THEN
!$omp critical(OMP_CS_PRNO_ASGSCT)
            DO IB=1,IBMAX
               IERR = FD_PRNO_ASGSCT(HPRNT, IFIC, VTMP(IB), KPOS(IB))
            ENDDO
            NBTOT = NBTOT + IBMAX
!$omp end critical(OMP_CS_PRNO_ASGSCT)
         ENDIF

         IF (IBMAX .LT. NBMAX) EXIT
      ENDDO

C---     Contrôle les fichiers vides
      IF (ERR_GOOD() .AND. NBTOT .LE. 0) GOTO 9900
      IF (.NOT. ERR_GOOD()) GOTO 9989

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_STRUCT_FICHIER_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989

9989  CONTINUE
      WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (XFIC .NE. 0) IERR = FD_PRNA_CLOSE(HOBJ, XFIC)
      FD_PRNA_INI1FIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_PRNA_INISTR initialise la structure interne à
C     partir de la liste des fichiers passée en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLIST       Handle sur la liste des fichiers
C
C Sortie:
C
C Notes:
C     Le fichier est lu avec les fonctions C C_FA qui permettent de
C     repositionner.
C************************************************************************
      FUNCTION FD_PRNA_INISTR(HOBJ, HLIST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLIST

      INCLUDE 'fdprna.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprna.fc'

      REAL*8  TMAX
      REAL*8    TEMPS_DUMMY
      INTEGER*8 XPOS_DUMMY
      INTEGER IERR
      INTEGER IOB, IOP
      INTEGER I
      INTEGER I_COMM, I_SIZE, I_ERROR
      INTEGER HPRNT
      INTEGER NFIC
      INTEGER NPRN, NNT
      INTEGER NPRN_L, NNT_L
      LOGICAL DO_OMP
      CHARACTER*256 FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRNA_HBASE
      HPRNT = FD_PRNA_HPRNT(IOB)

C---     Récupère les attributs du parent
      IOP = HPRNT - FD_PRNO_HBASE
      NNT_L  = FD_PRNO_NNT (IOP)
      NPRN_L = FD_PRNO_NPRN(IOP)

C---     Lis les dimensions du premier fichier
      NPRN = 0
      NNT  = 0
      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, 1, FNAME)
      IF (ERR_GOOD()) IERR = FD_PRNA_REQDIM(HOBJ, NPRN, NNT, FNAME)
      IF (ERR_GOOD() .AND. NPRN .LE. 0) GOTO 9900
      IF (ERR_GOOD() .AND. NNT  .LE. 0) GOTO 9901
D     IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NNT_L  .EQ. 0 .OR. NNT_L  .EQ. NNT)
D        CALL ERR_ASR(NPRN_L .EQ. 0 .OR. NPRN_L .EQ. NPRN)
D     ENDIF
      IF (NNT_L  .EQ. 0) NNT_L  = NNT
      IF (NPRN_L .EQ. 0) NPRN_L = NPRN

C---     Boucle sur les fichiers
      TMAX = -1.0D99
      NFIC = DS_LIST_REQDIM(HLIST)
!$    I_COMM = MP_UTIL_REQCOMM()
!$    CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)
!$    DO_OMP = I_SIZE .LE. 1
!$omp parallel
!$omp& if(DO_OMP)
!$omp& private(I, IERR, FNAME)
!$omp& firstprivate(NPRN, NNT, TMAX)
!$omp& default(shared)
!$omp do
      DO I=1,NFIC
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, I, FNAME)
         IF (ERR_GOOD())
     &      IERR = FD_PRNA_INI1FIC(HOBJ,
     &                             NPRN,
     &                             NNT,
     &                             TMAX,
     &                             I,
     &                             FNAME(1:SP_STRN_LEN(FNAME)))
      ENDDO
!$omp end do
      IERR = ERR_OMP_RDC()
!$omp end parallel

C---     Trie les données des sections
      IF (ERR_GOOD() .AND. NFIC .GT. 1) THEN
         IERR = FD_PRNO_TRISCT(HPRNT)
      ENDIF

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_PRNO_NNT (IOP) = NNT      ! Parent
         FD_PRNO_NPRN(IOP) = NPRN
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NBR_PRNO_INVALIDE', ' : ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNT_INVALIDE', ' : ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9989

C---     Complète le message d'erreur
9989  CONTINUE
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

9999  CONTINUE      
      FD_PRNA_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les dimensions du fichier.
C
C Description:
C     La fonction privée FD_PRNA_REQDIM lis l'entête d'un fichier
C     et en retourne les dimensions.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NOMFIC         Nom du fichier
C
C Sortie:
C     NVAL
C     NNT
C
C Notes:
C
C************************************************************************
      FUNCTION FD_PRNA_REQDIM(HOBJ, NVAL, NNT, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNT
      CHARACTER*(*) FNAME

      INCLUDE 'fdprna.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprna.fc'

      INTEGER   I_COMM, I_RANK, I_ERROR
      INTEGER   I_MASTER
      PARAMETER (I_MASTER = 0)

      REAL*8    TEMPS
      REAL*8    TMP(3)
      INTEGER*8 XFIC, XPOS
      INTEGER   IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRNA_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Lis l'entête de section
      XFIC = 0
      XPOS = 0
      NNT  = 0
      NVAL = 0
      TEMPS= 0.0D0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD())
     &      IERR = FD_PRNA_OPEN(HOBJ,
     &                          FNAME(1:SP_STRN_LEN(FNAME)),
     &                          C_FA_LECTURE,
     &                          XPOS,
     &                          XFIC)
         IF (ERR_GOOD())
     &      IERR = FD_PRNA_RHDR(HOBJ,
     &                          XFIC,
     &                          NNT,
     &                          NVAL,
     &                          TEMPS)
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C---     Broadcast tout
      IF (ERR_GOOD()) THEN
         TMP(1) = NNT
         TMP(2) = NVAL
         TMP(3) = TEMPS
         CALL MPI_BCAST(TMP, 3, MP_TYPE_RE8(),
     &                  I_MASTER, I_COMM, I_ERROR)
         NNT   = NINT(TMP(1))
         NVAL  = NINT(TMP(2))
         TEMPS = TMP(3)
      ENDIF

C---     Ferme le fichier
      IF (XFIC .NE. 0) THEN
         CALL ERR_PUSH()
         IERR = FD_PRNA_CLOSE(HOBJ, XFIC)
         CALL ERR_POP()
      ENDIF

      FD_PRNA_REQDIM = ERR_TYP()
      RETURN
      END
