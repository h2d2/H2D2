C************************************************************************
C --- Copyright (c) INRS 2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-LoCal
C Type:
C Interface:
C   H2D2 Module: NM
C      H2D2 Class: NM_LCLC
C         INTEGER NM_LCLC_CTR
C         INTEGER NM_LCLC_DTR
C         INTEGER NM_LCLC_INI
C         INTEGER NM_LCLC_RST
C         INTEGER NM_LCLC_CHARGE
C         INTEGER NM_LCLC_DSYNC
C         INTEGER NM_LCLC_ISYNC
C         INTEGER NM_LCLC_GENDSYNC
C         INTEGER NM_LCLC_GENISYNC
C         LOGICAL NM_LCLC_ESTNOPP
C         INTEGER NM_LCLC_REQNPRT
C         INTEGER NM_LCLC_REQNNL
C         INTEGER NM_LCLC_REQNNP
C         INTEGER NM_LCLC_REQNNT
C         INTEGER NM_LCLC_REQNEXT
C         INTEGER NM_LCLC_REQNINT
C         INTEGER NM_LCLC_000
C         INTEGER NM_LCLC_999
C         INTEGER NM_LCLC_REQHBASE
C         LOGICAL NM_LCLC_HVALIDE
C         LOGICAL NM_LCLC_TVALIDE
C         FTN (Sub)Module: NM_LCLC_HOBJ_M
C            Public:
C            Private:
C               TYPE (NM_LCLC_T), POINTER NM_LCLC_REQSELF
C
C************************************************************************

      MODULE NM_LCLC_HOBJ_M

      USE NM_LCLC_M, ONLY : NM_LCLC_T
      IMPLICIT NONE

C---     Attributs statiques
      INTEGER, SAVE :: NM_LCLC_HBASE = 0

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée NM_LCLC_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQSELF(HOBJ)

      USE ISO_C_BINDING

      TYPE (NM_LCLC_T), POINTER :: NM_LCLC_REQSELF
      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

D     CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
D     CALL ERR_ASR(ERR_GOOD())
D     CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      NM_LCLC_REQSELF => SELF
      RETURN
      END FUNCTION NM_LCLC_REQSELF

      END MODULE NM_LCLC_HOBJ_M

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_LCLC_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_CTR
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      USE NM_LCLC_M, ONLY : NM_LCLC_T, NM_LCLC_CTR_SELF
      USE ISO_C_BINDING, ONLY : C_LOC
      IMPLICIT NONE

      INTEGER, INTENT(OUT) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NM_LCLC_CTR_SELF()

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   NM_LCLC_HBASE,
     &                                   C_LOC(SELF))

      NM_LCLC_CTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_CTR

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_LCLC_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_DTR
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      USE NM_LCLC_M, ONLY: DEL
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_LCLC_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      IERR = DEL(SELF)

      IERR = OB_OBJN_DTR(HOBJ, NM_LCLC_HBASE)

      NM_LCLC_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCLC_DTR

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La méthode NM_LCLC_INI initialise l'objet à partir soit du nom de
C     fichier, soit des dimensions et du pointeur à la table. Pour initialiser
C     l'objet avec le nom de fichier, il faut mettre les autres
C     paramètres à 0. Sinon, il faut donner une chaîne vide pour le nom
C     de fichier.
C
C Entrée:
C     NOMFIC      Nom du fichier contenant la table
C     NNL_P       Le nombre d'items locaux
C     NNT_P       Le nombre d'items globaux
C     NPROC_P     Le nombre de sous-domaines
C     LDIST_P     Le pointeur à la table de distribution des items
C
C Sortie:
C     HOBJ        Le handle de l'objet initialisé
C
C Notes:
C     La table est passée par pointeur pour permettre un pointeur nul.
C************************************************************************
      FUNCTION NM_LCLC_INI(HOBJ,
     &                     NOMFIC,
     &                     NNT_P,
     &                     LDIST_P)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_INI
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      CHARACTER*(*), INTENT(IN) :: NOMFIC
      INTEGER, INTENT(IN) :: NNT_P
      INTEGER, INTENT(IN) :: LDIST_P

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_INI = SELF%INI(NOMFIC, NNT_P, LDIST_P)
      RETURN
      END FUNCTION NM_LCLC_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_LCLC_RST(HOBJ)

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_RST = SELF%RST()
      RETURN
      END FUNCTION NM_LCLC_RST

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_LCLC_CHARGE charge les tables internes des
C     paramètres de configuration.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NPROC          Nombre de process
C     NNL            Nombre de noeuds locaux
C     KDISTR         Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_CHARGE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_CHARGE
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_CHARGE = SELF%CHARGE()
      RETURN
      END FUNCTION NM_LCLC_CHARGE

C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_LCLC_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_DSYNC(HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_DSYNC
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(NVAL, NNL)

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_DSYNC = SELF%DSYNC(NVAL, NNL, VVAL)
      RETURN
      END FUNCTION NM_LCLC_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_LCLC_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_ISYNC(HOBJ, NVAL, NNL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_ISYNC
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(NVAL, NNL)

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_ISYNC = SELF%ISYNC(NVAL, NNL, KVAL)
      RETURN
      END FUNCTION NM_LCLC_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCLC_GENDSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_GENDSYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_GENDSYNC
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (NM_LCLC_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_GENDSYNC = SELF%GENDSYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_LCLC_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCLC_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_GENISYNC(HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_GENISYNC
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_GENISYNC = SELF%GENISYNC(NVAL, NNL, HSNC)
      RETURN
      END FUNCTION NM_LCLC_GENISYNC

C************************************************************************
C Sommaire: NM_LCLC_ESTNOPP
C
C Description:
C     La fonction NM_LCLC_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_ESTNOPP(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_ESTNOPP
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_ESTNOPP = SELF%ESTNOPP(INLOC)
      RETURN
      END FUNCTION NM_LCLC_ESTNOPP

C************************************************************************
C Sommaire: NM_LCLC_REQNPRT
C
C Description:
C     La fonction NM_LCLC_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNPRT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNPRT
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNPRT = SELF%REQNPRT()
      RETURN
      END FUNCTION NM_LCLC_REQNPRT

C************************************************************************
C Sommaire: NM_LCLC_REQNNL
C
C Description:
C     La fonction NM_LCLC_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNNL
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNNL = SELF%REQNNL()
      RETURN
      END FUNCTION NM_LCLC_REQNNL

C************************************************************************
C Sommaire: NM_LCLC_REQNNP
C
C Description:
C     La fonction NM_LCLC_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNP(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNNP
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNNP = SELF%REQNNP()
      RETURN
      END FUNCTION NM_LCLC_REQNNP

C************************************************************************
C Sommaire: NM_LCLC_REQNNT
C
C Description:
C     La fonction NM_LCLC_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNNT
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNNT = SELF%REQNNT()
      RETURN
      END FUNCTION NM_LCLC_REQNNT

C************************************************************************
C Sommaire: NM_LCLC_REQNEXT
C
C Description:
C     La fonction NM_LCLC_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQNEXT(HOBJ, INLOC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNEXT
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNEXT = SELF%REQNEXT(INLOC)
      RETURN
      END FUNCTION NM_LCLC_REQNEXT

C************************************************************************
C Sommaire: NM_LCLC_REQNINT
C
C Description:
C     La fonction NM_LCLC_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C     En fait, pour accélérer, il faudrait monter
C     la table inverse.
C************************************************************************
      FUNCTION NM_LCLC_REQNINT(HOBJ, INGLB)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQNINT
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: INGLB

      INCLUDE 'nmlclc.fi'
      INCLUDE 'err.fi'

      TYPE (NM_LCLC_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_LCLC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => NM_LCLC_REQSELF(HOBJ)
      NM_LCLC_REQNINT = SELF%REQNINT(INGLB)
      RETURN
      END FUNCTION NM_LCLC_REQNINT


C************************************************************************
C************************************************************************
C************************************************************************
C************************************************************************


C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction NM_LCLC_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_000
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(NM_LCLC_HBASE,
     &                   'Renumbering - Local to Global')

      NM_LCLC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction NM_LCLC_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_999
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL NM_LCLC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(NM_LCLC_HBASE,
     &                   NM_LCLC_DTR)

      NM_LCLC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_LCLC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_REQHBASE
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INCLUDE 'nmlclc.fi'
C------------------------------------------------------------------------

      NM_LCLC_REQHBASE = NM_LCLC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_LCLC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_HVALIDE
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_LCLC_HVALIDE = OB_OBJN_HVALIDE(HOBJ, NM_LCLC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si le type de l'objet est celui de la classe
C
C Description:
C     La fonction NM_LCLC_TVALIDE permet de valider le type d'un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé à le même type que
C     la classe. C'est un test plus léger mais moins poussé que HVALIDE.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCLC_TVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_LCLC_TVALIDE
CDEC$ ENDIF

      USE NM_LCLC_HOBJ_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmlclc.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      NM_LCLC_TVALIDE = OB_OBJN_TVALIDE(HOBJ, NM_LCLC_HBASE)
      RETURN
      END
