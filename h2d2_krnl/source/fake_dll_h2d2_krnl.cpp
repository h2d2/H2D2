//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_krnl
// Entry point: extern "C" void fake_dll_h2d2_krnl()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:04.402657
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class DT_CLIM
F_PROT(DT_CLIM_000, dt_clim_000);
F_PROT(DT_CLIM_999, dt_clim_999);
F_PROT(DT_CLIM_PKL, dt_clim_pkl);
F_PROT(DT_CLIM_UPK, dt_clim_upk);
F_PROT(DT_CLIM_CTR, dt_clim_ctr);
F_PROT(DT_CLIM_DTR, dt_clim_dtr);
F_PROT(DT_CLIM_INI, dt_clim_ini);
F_PROT(DT_CLIM_RST, dt_clim_rst);
F_PROT(DT_CLIM_REQHBASE, dt_clim_reqhbase);
F_PROT(DT_CLIM_HVALIDE, dt_clim_hvalide);
F_PROT(DT_CLIM_CHARGE, dt_clim_charge);
F_PROT(DT_CLIM_REQHLIMT, dt_clim_reqhlimt);
F_PROT(DT_CLIM_REQNCLCND, dt_clim_reqnclcnd);
F_PROT(DT_CLIM_REQLCLCND, dt_clim_reqlclcnd);
F_PROT(DT_CLIM_REQNCLCNV, dt_clim_reqnclcnv);
F_PROT(DT_CLIM_REQLCLCNV, dt_clim_reqlclcnv);
F_PROT(DT_CLIM_REQNCLLIM, dt_clim_reqncllim);
F_PROT(DT_CLIM_REQNCLNOD, dt_clim_reqnclnod);
F_PROT(DT_CLIM_REQNCLELE, dt_clim_reqnclele);
F_PROT(DT_CLIM_REQLCLLIM, dt_clim_reqlcllim);
F_PROT(DT_CLIM_REQLCLNOD, dt_clim_reqlclnod);
F_PROT(DT_CLIM_REQLCLELE, dt_clim_reqlclele);
F_PROT(DT_CLIM_REQLCLDST, dt_clim_reqlcldst);
 
// ---  class DT_COND
F_PROT(DT_COND_000, dt_cond_000);
F_PROT(DT_COND_999, dt_cond_999);
F_PROT(DT_COND_PKL, dt_cond_pkl);
F_PROT(DT_COND_UPK, dt_cond_upk);
F_PROT(DT_COND_CTR, dt_cond_ctr);
F_PROT(DT_COND_DTR, dt_cond_dtr);
F_PROT(DT_COND_INI, dt_cond_ini);
F_PROT(DT_COND_RST, dt_cond_rst);
F_PROT(DT_COND_REQHBASE, dt_cond_reqhbase);
F_PROT(DT_COND_HVALIDE, dt_cond_hvalide);
F_PROT(DT_COND_PRN, dt_cond_prn);
F_PROT(DT_COND_CHARGE, dt_cond_charge);
F_PROT(DT_COND_REQNCLCND, dt_cond_reqnclcnd);
F_PROT(DT_COND_REQNCLCNV, dt_cond_reqnclcnv);
F_PROT(DT_COND_REQLCLCND, dt_cond_reqlclcnd);
F_PROT(DT_COND_REQLCLCNV, dt_cond_reqlclcnv);
F_PROT(DT_COND_REQNFIC, dt_cond_reqnfic);
F_PROT(DT_COND_REQNOMF, dt_cond_reqnomf);
 
// ---  class DT_ELEM
F_PROT(DT_ELEM_000, dt_elem_000);
F_PROT(DT_ELEM_999, dt_elem_999);
F_PROT(DT_ELEM_PKL, dt_elem_pkl);
F_PROT(DT_ELEM_UPK, dt_elem_upk);
F_PROT(DT_ELEM_CTR, dt_elem_ctr);
F_PROT(DT_ELEM_DTR, dt_elem_dtr);
F_PROT(DT_ELEM_INIDIM, dt_elem_inidim);
F_PROT(DT_ELEM_INIFIC, dt_elem_inific);
F_PROT(DT_ELEM_INISPL, dt_elem_inispl);
F_PROT(DT_ELEM_RST, dt_elem_rst);
F_PROT(DT_ELEM_REQHBASE, dt_elem_reqhbase);
F_PROT(DT_ELEM_HVALIDE, dt_elem_hvalide);
F_PROT(DT_ELEM_PRN, dt_elem_prn);
F_PROT(DT_ELEM_CHARGE, dt_elem_charge);
F_PROT(DT_ELEM_MAJVAL, dt_elem_majval);
F_PROT(DT_ELEM_SAUVE, dt_elem_sauve);
F_PROT(DT_ELEM_ESTINIFIC, dt_elem_estinific);
F_PROT(DT_ELEM_ESTINIDIM, dt_elem_estinidim);
F_PROT(DT_ELEM_ESTINISPL, dt_elem_estinispl);
F_PROT(DT_ELEM_REQISTAT, dt_elem_reqistat);
F_PROT(DT_ELEM_REQITPE, dt_elem_reqitpe);
F_PROT(DT_ELEM_REQLELE, dt_elem_reqlele);
F_PROT(DT_ELEM_REQNCEL, dt_elem_reqncel);
F_PROT(DT_ELEM_REQNNEL, dt_elem_reqnnel);
F_PROT(DT_ELEM_REQNELT, dt_elem_reqnelt);
F_PROT(DT_ELEM_REQNELL, dt_elem_reqnell);
F_PROT(DT_ELEM_REQTEMPS, dt_elem_reqtemps);
F_PROT(DT_ELEM_REQNOMF, dt_elem_reqnomf);
 
// ---  class DT_GRID
F_PROT(DT_GRID_000, dt_grid_000);
F_PROT(DT_GRID_999, dt_grid_999);
F_PROT(DT_GRID_PKL, dt_grid_pkl);
F_PROT(DT_GRID_UPK, dt_grid_upk);
F_PROT(DT_GRID_CTR, dt_grid_ctr);
F_PROT(DT_GRID_DTR, dt_grid_dtr);
F_PROT(DT_GRID_INI, dt_grid_ini);
F_PROT(DT_GRID_RST, dt_grid_rst);
F_PROT(DT_GRID_REQHBASE, dt_grid_reqhbase);
F_PROT(DT_GRID_HVALIDE, dt_grid_hvalide);
F_PROT(DT_GRID_PRN, dt_grid_prn);
F_PROT(DT_GRID_CHARGE, dt_grid_charge);
F_PROT(DT_GRID_SAUVE, dt_grid_sauve);
F_PROT(DT_GRID_SPLIT, dt_grid_split);
F_PROT(DT_GRID_REQHCOOR, dt_grid_reqhcoor);
F_PROT(DT_GRID_REQHELEV, dt_grid_reqhelev);
F_PROT(DT_GRID_REQHNUMC, dt_grid_reqhnumc);
F_PROT(DT_GRID_REQHNUME, dt_grid_reqhnume);
F_PROT(DT_GRID_REQNDIM, dt_grid_reqndim);
F_PROT(DT_GRID_REQNNL, dt_grid_reqnnl);
F_PROT(DT_GRID_REQNNP, dt_grid_reqnnp);
F_PROT(DT_GRID_REQNNT, dt_grid_reqnnt);
F_PROT(DT_GRID_REQNNEL, dt_grid_reqnnel);
F_PROT(DT_GRID_REQNELL, dt_grid_reqnell);
F_PROT(DT_GRID_REQNELT, dt_grid_reqnelt);
 
// ---  class DT_LIMT
F_PROT(DT_LIMT_000, dt_limt_000);
F_PROT(DT_LIMT_999, dt_limt_999);
F_PROT(DT_LIMT_PKL, dt_limt_pkl);
F_PROT(DT_LIMT_UPK, dt_limt_upk);
F_PROT(DT_LIMT_CTR, dt_limt_ctr);
F_PROT(DT_LIMT_DTR, dt_limt_dtr);
F_PROT(DT_LIMT_INIDIM, dt_limt_inidim);
F_PROT(DT_LIMT_INIFIC, dt_limt_inific);
F_PROT(DT_LIMT_RST, dt_limt_rst);
F_PROT(DT_LIMT_REQHBASE, dt_limt_reqhbase);
F_PROT(DT_LIMT_HVALIDE, dt_limt_hvalide);
F_PROT(DT_LIMT_PRN, dt_limt_prn);
F_PROT(DT_LIMT_CHARGE, dt_limt_charge);
F_PROT(DT_LIMT_MAJVAL, dt_limt_majval);
F_PROT(DT_LIMT_SAUVE, dt_limt_sauve);
F_PROT(DT_LIMT_REQNCLLIM, dt_limt_reqncllim);
F_PROT(DT_LIMT_REQNCLNOD, dt_limt_reqnclnod);
F_PROT(DT_LIMT_REQNCLELE, dt_limt_reqnclele);
F_PROT(DT_LIMT_REQLCLLIM, dt_limt_reqlcllim);
F_PROT(DT_LIMT_REQLCLNOD, dt_limt_reqlclnod);
F_PROT(DT_LIMT_REQLCLELE, dt_limt_reqlclele);
F_PROT(DT_LIMT_REQLCLDST, dt_limt_reqlcldst);
F_PROT(DT_LIMT_REQNOMF, dt_limt_reqnomf);
 
// ---  class DT_PRGL
F_PROT(DT_PRGL_000, dt_prgl_000);
F_PROT(DT_PRGL_999, dt_prgl_999);
F_PROT(DT_PRGL_PKL, dt_prgl_pkl);
F_PROT(DT_PRGL_UPK, dt_prgl_upk);
F_PROT(DT_PRGL_CTR, dt_prgl_ctr);
F_PROT(DT_PRGL_DTR, dt_prgl_dtr);
F_PROT(DT_PRGL_INIVAL, dt_prgl_inival);
F_PROT(DT_PRGL_INIFIC, dt_prgl_inific);
F_PROT(DT_PRGL_RST, dt_prgl_rst);
F_PROT(DT_PRGL_REQHBASE, dt_prgl_reqhbase);
F_PROT(DT_PRGL_HVALIDE, dt_prgl_hvalide);
F_PROT(DT_PRGL_CHARGE, dt_prgl_charge);
F_PROT(DT_PRGL_GATHER, dt_prgl_gather);
F_PROT(DT_PRGL_ASGPRGL, dt_prgl_asgprgl);
F_PROT(DT_PRGL_REQPRGL, dt_prgl_reqprgl);
F_PROT(DT_PRGL_REQNPRGL, dt_prgl_reqnprgl);
F_PROT(DT_PRGL_REQNOMF, dt_prgl_reqnomf);
 
// ---  class DT_PRNO
F_PROT(DT_PRNO_000, dt_prno_000);
F_PROT(DT_PRNO_999, dt_prno_999);
F_PROT(DT_PRNO_PKL, dt_prno_pkl);
F_PROT(DT_PRNO_UPK, dt_prno_upk);
F_PROT(DT_PRNO_CTR, dt_prno_ctr);
F_PROT(DT_PRNO_DTR, dt_prno_dtr);
F_PROT(DT_PRNO_INI, dt_prno_ini);
F_PROT(DT_PRNO_RST, dt_prno_rst);
F_PROT(DT_PRNO_REQHBASE, dt_prno_reqhbase);
F_PROT(DT_PRNO_HVALIDE, dt_prno_hvalide);
F_PROT(DT_PRNO_ADDVNO, dt_prno_addvno);
F_PROT(DT_PRNO_CHARGE, dt_prno_charge);
F_PROT(DT_PRNO_GATHER, dt_prno_gather);
F_PROT(DT_PRNO_SCATTER, dt_prno_scatter);
F_PROT(DT_PRNO_REQNNT, dt_prno_reqnnt);
F_PROT(DT_PRNO_REQNNL, dt_prno_reqnnl);
F_PROT(DT_PRNO_REQNPRN, dt_prno_reqnprn);
F_PROT(DT_PRNO_REQTEMPS, dt_prno_reqtemps);
 
// ---  class DT_VNOD
F_PROT(DT_VNOD_000, dt_vnod_000);
F_PROT(DT_VNOD_999, dt_vnod_999);
F_PROT(DT_VNOD_PKL, dt_vnod_pkl);
F_PROT(DT_VNOD_UPK, dt_vnod_upk);
F_PROT(DT_VNOD_CTR, dt_vnod_ctr);
F_PROT(DT_VNOD_DTR, dt_vnod_dtr);
F_PROT(DT_VNOD_INIDIM, dt_vnod_inidim);
F_PROT(DT_VNOD_INIFIC, dt_vnod_inific);
F_PROT(DT_VNOD_RST, dt_vnod_rst);
F_PROT(DT_VNOD_REQHBASE, dt_vnod_reqhbase);
F_PROT(DT_VNOD_HVALIDE, dt_vnod_hvalide);
F_PROT(DT_VNOD_PRN, dt_vnod_prn);
F_PROT(DT_VNOD_ASGFICECR, dt_vnod_asgficecr);
F_PROT(DT_VNOD_CHARGE, dt_vnod_charge);
F_PROT(DT_VNOD_ASG1VAL, dt_vnod_asg1val);
F_PROT(DT_VNOD_ASGVALS, dt_vnod_asgvals);
F_PROT(DT_VNOD_REQ1VAL, dt_vnod_req1val);
F_PROT(DT_VNOD_REQVALS, dt_vnod_reqvals);
F_PROT(DT_VNOD_SAUVE, dt_vnod_sauve);
F_PROT(DT_VNOD_ESTINI, dt_vnod_estini);
F_PROT(DT_VNOD_ESTINIVAL, dt_vnod_estinival);
F_PROT(DT_VNOD_ESTINIFIC, dt_vnod_estinific);
F_PROT(DT_VNOD_REQHNUMR, dt_vnod_reqhnumr);
F_PROT(DT_VNOD_REQLVNO, dt_vnod_reqlvno);
F_PROT(DT_VNOD_REQLVINI, dt_vnod_reqlvini);
F_PROT(DT_VNOD_REQNNL, dt_vnod_reqnnl);
F_PROT(DT_VNOD_REQNNT, dt_vnod_reqnnt);
F_PROT(DT_VNOD_REQNVNO, dt_vnod_reqnvno);
F_PROT(DT_VNOD_REQTEMPS, dt_vnod_reqtemps);
F_PROT(DT_VNOD_REQNFIC, dt_vnod_reqnfic);
F_PROT(DT_VNOD_REQNOMF, dt_vnod_reqnomf);
 
// ---  class FD_COND
F_PROT(FD_COND_000, fd_cond_000);
F_PROT(FD_COND_999, fd_cond_999);
F_PROT(FD_COND_PKL, fd_cond_pkl);
F_PROT(FD_COND_UPK, fd_cond_upk);
F_PROT(FD_COND_CTR, fd_cond_ctr);
F_PROT(FD_COND_DTR, fd_cond_dtr);
F_PROT(FD_COND_INI, fd_cond_ini);
F_PROT(FD_COND_RST, fd_cond_rst);
F_PROT(FD_COND_REQHBASE, fd_cond_reqhbase);
F_PROT(FD_COND_HVALIDE, fd_cond_hvalide);
F_PROT(FD_COND_LIS, fd_cond_lis);
F_PROT(FD_COND_REQNCLCND, fd_cond_reqnclcnd);
F_PROT(FD_COND_REQNCLCNV, fd_cond_reqnclcnv);
F_PROT(FD_COND_REQNSCT, fd_cond_reqnsct);
F_PROT(FD_COND_REQTMIN, fd_cond_reqtmin);
F_PROT(FD_COND_REQTMAX, fd_cond_reqtmax);
F_PROT(FD_COND_REQNFIC, fd_cond_reqnfic);
F_PROT(FD_COND_REQNOMF, fd_cond_reqnomf);
 
// ---  class FD_ELEM
F_PROT(FD_ELEM_000, fd_elem_000);
F_PROT(FD_ELEM_999, fd_elem_999);
F_PROT(FD_ELEM_PKL, fd_elem_pkl);
F_PROT(FD_ELEM_UPK, fd_elem_upk);
F_PROT(FD_ELEM_CTR, fd_elem_ctr);
F_PROT(FD_ELEM_DTR, fd_elem_dtr);
F_PROT(FD_ELEM_INI, fd_elem_ini);
F_PROT(FD_ELEM_RST, fd_elem_rst);
F_PROT(FD_ELEM_REQHBASE, fd_elem_reqhbase);
F_PROT(FD_ELEM_HVALIDE, fd_elem_hvalide);
F_PROT(FD_ELEM_ECRIS, fd_elem_ecris);
F_PROT(FD_ELEM_LIS, fd_elem_lis);
F_PROT(FD_ELEM_REQNELL, fd_elem_reqnell);
F_PROT(FD_ELEM_REQISTAT, fd_elem_reqistat);
F_PROT(FD_ELEM_REQITPE, fd_elem_reqitpe);
F_PROT(FD_ELEM_REQNELT, fd_elem_reqnelt);
F_PROT(FD_ELEM_REQNNEL, fd_elem_reqnnel);
F_PROT(FD_ELEM_REQNSCT, fd_elem_reqnsct);
F_PROT(FD_ELEM_REQNOMF, fd_elem_reqnomf);
 
// ---  class FD_LIMT
F_PROT(FD_LIMT_000, fd_limt_000);
F_PROT(FD_LIMT_999, fd_limt_999);
F_PROT(FD_LIMT_PKL, fd_limt_pkl);
F_PROT(FD_LIMT_UPK, fd_limt_upk);
F_PROT(FD_LIMT_CTR, fd_limt_ctr);
F_PROT(FD_LIMT_DTR, fd_limt_dtr);
F_PROT(FD_LIMT_INI, fd_limt_ini);
F_PROT(FD_LIMT_RST, fd_limt_rst);
F_PROT(FD_LIMT_REQHBASE, fd_limt_reqhbase);
F_PROT(FD_LIMT_HVALIDE, fd_limt_hvalide);
F_PROT(FD_LIMT_ECRIS, fd_limt_ecris);
F_PROT(FD_LIMT_LIS, fd_limt_lis);
F_PROT(FD_LIMT_REQNCLLIM, fd_limt_reqncllim);
F_PROT(FD_LIMT_REQNCLNOD, fd_limt_reqnclnod);
F_PROT(FD_LIMT_REQNOMF, fd_limt_reqnomf);
 
// ---  class FD_NUMR
F_PROT(FD_NUMR_000, fd_numr_000);
F_PROT(FD_NUMR_999, fd_numr_999);
F_PROT(FD_NUMR_CTR, fd_numr_ctr);
F_PROT(FD_NUMR_DTR, fd_numr_dtr);
F_PROT(FD_NUMR_INI, fd_numr_ini);
F_PROT(FD_NUMR_RST, fd_numr_rst);
F_PROT(FD_NUMR_REQHBASE, fd_numr_reqhbase);
F_PROT(FD_NUMR_HVALIDE, fd_numr_hvalide);
F_PROT(FD_NUMR_ECRIS, fd_numr_ecris);
F_PROT(FD_NUMR_LIS, fd_numr_lis);
F_PROT(FD_NUMR_REQNNTG, fd_numr_reqnntg);
F_PROT(FD_NUMR_REQNNTL, fd_numr_reqnntl);
F_PROT(FD_NUMR_REQNPROC, fd_numr_reqnproc);
 
// ---  class FD_PRN8
F_PROT(FD_PRN8_000, fd_prn8_000);
F_PROT(FD_PRN8_999, fd_prn8_999);
F_PROT(FD_PRN8_PKL, fd_prn8_pkl);
F_PROT(FD_PRN8_UPK, fd_prn8_upk);
F_PROT(FD_PRN8_CTR, fd_prn8_ctr);
F_PROT(FD_PRN8_DTR, fd_prn8_dtr);
F_PROT(FD_PRN8_INI, fd_prn8_ini);
F_PROT(FD_PRN8_RST, fd_prn8_rst);
F_PROT(FD_PRN8_REQHBASE, fd_prn8_reqhbase);
F_PROT(FD_PRN8_HVALIDE, fd_prn8_hvalide);
F_PROT(FD_PRN8_ECRSCT, fd_prn8_ecrsct);
F_PROT(FD_PRN8_LISSCT, fd_prn8_lissct);
 
// ---  class FD_PRNA
F_PROT(FD_PRNA_000, fd_prna_000);
F_PROT(FD_PRNA_999, fd_prna_999);
F_PROT(FD_PRNA_PKL, fd_prna_pkl);
F_PROT(FD_PRNA_UPK, fd_prna_upk);
F_PROT(FD_PRNA_CTR, fd_prna_ctr);
F_PROT(FD_PRNA_DTR, fd_prna_dtr);
F_PROT(FD_PRNA_INI, fd_prna_ini);
F_PROT(FD_PRNA_RST, fd_prna_rst);
F_PROT(FD_PRNA_REQHBASE, fd_prna_reqhbase);
F_PROT(FD_PRNA_HVALIDE, fd_prna_hvalide);
F_PROT(FD_PRNA_ECRSCT, fd_prna_ecrsct);
F_PROT(FD_PRNA_LISSCT, fd_prna_lissct);
 
// ---  class FD_PRNB
F_PROT(FD_PRNB_000, fd_prnb_000);
F_PROT(FD_PRNB_999, fd_prnb_999);
F_PROT(FD_PRNB_PKL, fd_prnb_pkl);
F_PROT(FD_PRNB_UPK, fd_prnb_upk);
F_PROT(FD_PRNB_CTR, fd_prnb_ctr);
F_PROT(FD_PRNB_DTR, fd_prnb_dtr);
F_PROT(FD_PRNB_INI, fd_prnb_ini);
F_PROT(FD_PRNB_RST, fd_prnb_rst);
F_PROT(FD_PRNB_REQHBASE, fd_prnb_reqhbase);
F_PROT(FD_PRNB_HVALIDE, fd_prnb_hvalide);
F_PROT(FD_PRNB_ECRSCT, fd_prnb_ecrsct);
F_PROT(FD_PRNB_LISSCT, fd_prnb_lissct);
 
// ---  class FD_PRNO
F_PROT(FD_PRNO_000, fd_prno_000);
F_PROT(FD_PRNO_999, fd_prno_999);
F_PROT(FD_PRNO_PKL, fd_prno_pkl);
F_PROT(FD_PRNO_UPK, fd_prno_upk);
F_PROT(FD_PRNO_CTR, fd_prno_ctr);
F_PROT(FD_PRNO_DTR, fd_prno_dtr);
F_PROT(FD_PRNO_INI, fd_prno_ini);
F_PROT(FD_PRNO_RST, fd_prno_rst);
F_PROT(FD_PRNO_REQHBASE, fd_prno_reqhbase);
F_PROT(FD_PRNO_HVALIDE, fd_prno_hvalide);
F_PROT(FD_PRNO_ECRIS, fd_prno_ecris);
F_PROT(FD_PRNO_LIS, fd_prno_lis);
F_PROT(FD_PRNO_REQISTAT, fd_prno_reqistat);
F_PROT(FD_PRNO_REQNNT, fd_prno_reqnnt);
F_PROT(FD_PRNO_REQNPRN, fd_prno_reqnprn);
F_PROT(FD_PRNO_REQNSCT, fd_prno_reqnsct);
F_PROT(FD_PRNO_REQTMIN, fd_prno_reqtmin);
F_PROT(FD_PRNO_REQTMAX, fd_prno_reqtmax);
F_PROT(FD_PRNO_REQNFIC, fd_prno_reqnfic);
F_PROT(FD_PRNO_REQNOMF, fd_prno_reqnomf);
 
// ---  class IC_NUMG
F_PROT(IC_NUMG_CMD, ic_numg_cmd);
F_PROT(IC_NUMG_REQCMD, ic_numg_reqcmd);
 
// ---  class IC_NUML
F_PROT(IC_NUML_CMD, ic_numl_cmd);
F_PROT(IC_NUML_REQCMD, ic_numl_reqcmd);
 
// ---  class IC_DT_CLIM
F_PROT(IC_DT_CLIM_XEQCTR, ic_dt_clim_xeqctr);
F_PROT(IC_DT_CLIM_XEQMTH, ic_dt_clim_xeqmth);
F_PROT(IC_DT_CLIM_REQCLS, ic_dt_clim_reqcls);
F_PROT(IC_DT_CLIM_REQHDL, ic_dt_clim_reqhdl);
 
// ---  class IC_DT_COND
F_PROT(IC_DT_COND_XEQCTR, ic_dt_cond_xeqctr);
F_PROT(IC_DT_COND_XEQMTH, ic_dt_cond_xeqmth);
F_PROT(IC_DT_COND_REQCLS, ic_dt_cond_reqcls);
F_PROT(IC_DT_COND_REQHDL, ic_dt_cond_reqhdl);
 
// ---  class IC_DT_COOR
F_PROT(IC_DT_COOR_CMD, ic_dt_coor_cmd);
F_PROT(IC_DT_COOR_REQCMD, ic_dt_coor_reqcmd);
 
// ---  class IC_DT_DLIB
F_PROT(IC_DT_DLIB_XEQCTR, ic_dt_dlib_xeqctr);
F_PROT(IC_DT_DLIB_XEQMTH, ic_dt_dlib_xeqmth);
F_PROT(IC_DT_DLIB_REQHDL, ic_dt_dlib_reqhdl);
F_PROT(IC_DT_DLIB_REQCLS, ic_dt_dlib_reqcls);
 
// ---  class IC_DT_ELEM
F_PROT(IC_DT_ELEM_XEQCTR, ic_dt_elem_xeqctr);
F_PROT(IC_DT_ELEM_XEQMTH, ic_dt_elem_xeqmth);
F_PROT(IC_DT_ELEM_REQCLS, ic_dt_elem_reqcls);
F_PROT(IC_DT_ELEM_REQHDL, ic_dt_elem_reqhdl);
 
// ---  class IC_DT_GRID
F_PROT(IC_DT_GRID_XEQCTR, ic_dt_grid_xeqctr);
F_PROT(IC_DT_GRID_XEQMTH, ic_dt_grid_xeqmth);
F_PROT(IC_DT_GRID_REQCLS, ic_dt_grid_reqcls);
F_PROT(IC_DT_GRID_REQHDL, ic_dt_grid_reqhdl);
 
// ---  class IC_DT_LIMT
F_PROT(IC_DT_LIMT_XEQCTR, ic_dt_limt_xeqctr);
F_PROT(IC_DT_LIMT_XEQMTH, ic_dt_limt_xeqmth);
F_PROT(IC_DT_LIMT_REQCLS, ic_dt_limt_reqcls);
F_PROT(IC_DT_LIMT_REQHDL, ic_dt_limt_reqhdl);
 
// ---  class IC_DT_PREL
F_PROT(IC_DT_PREL_CMD, ic_dt_prel_cmd);
F_PROT(IC_DT_PREL_REQCMD, ic_dt_prel_reqcmd);
 
// ---  class IC_DT_PRGL
F_PROT(IC_DT_PRGL_XEQCTR, ic_dt_prgl_xeqctr);
F_PROT(IC_DT_PRGL_REQCLS, ic_dt_prgl_reqcls);
F_PROT(IC_DT_PRGL_REQHDL, ic_dt_prgl_reqhdl);
F_PROT(IC_DT_PRGL_XEQMTH, ic_dt_prgl_xeqmth);
 
// ---  class IC_DT_PRNO
F_PROT(IC_DT_PRNO_XEQCTR, ic_dt_prno_xeqctr);
F_PROT(IC_DT_PRNO_XEQMTH, ic_dt_prno_xeqmth);
F_PROT(IC_DT_PRNO_REQCLS, ic_dt_prno_reqcls);
F_PROT(IC_DT_PRNO_REQHDL, ic_dt_prno_reqhdl);
 
// ---  class IC_DT_SOLC
F_PROT(IC_DT_SOLC_CMD, ic_dt_solc_cmd);
F_PROT(IC_DT_SOLC_REQCMD, ic_dt_solc_reqcmd);
 
// ---  class IC_DT_SOLR
F_PROT(IC_DT_SOLR_CMD, ic_dt_solr_cmd);
F_PROT(IC_DT_SOLR_REQCMD, ic_dt_solr_reqcmd);
 
// ---  class IC_DT_VELE
F_PROT(IC_DT_VELE_CMD, ic_dt_vele_cmd);
F_PROT(IC_DT_VELE_REQCMD, ic_dt_vele_reqcmd);
 
// ---  class IC_DT_VNOD
F_PROT(IC_DT_VNOD_XEQCTR, ic_dt_vnod_xeqctr);
F_PROT(IC_DT_VNOD_XEQMTH, ic_dt_vnod_xeqmth);
F_PROT(IC_DT_VNOD_REQCLS, ic_dt_vnod_reqcls);
F_PROT(IC_DT_VNOD_REQHDL, ic_dt_vnod_reqhdl);
 
// ---  class IC_NM_LCGL
F_PROT(IC_NM_LCGL_XEQCTR, ic_nm_lcgl_xeqctr);
F_PROT(IC_NM_LCGL_XEQMTH, ic_nm_lcgl_xeqmth);
F_PROT(IC_NM_LCGL_REQCLS, ic_nm_lcgl_reqcls);
F_PROT(IC_NM_LCGL_REQHDL, ic_nm_lcgl_reqhdl);
 
// ---  class IC_NM_PRXY
F_PROT(IC_NM_PRXY_XEQCTR, ic_nm_prxy_xeqctr);
F_PROT(IC_NM_PRXY_XEQMTH, ic_nm_prxy_xeqmth);
F_PROT(IC_NM_PRXY_REQCLS, ic_nm_prxy_reqcls);
F_PROT(IC_NM_PRXY_REQHDL, ic_nm_prxy_reqhdl);
 
// ---  class IC_TG_DBLE
F_PROT(IC_TG_DBLE_XEQCTR, ic_tg_dble_xeqctr);
F_PROT(IC_TG_DBLE_XEQMTH, ic_tg_dble_xeqmth);
F_PROT(IC_TG_DBLE_REQCLS, ic_tg_dble_reqcls);
F_PROT(IC_TG_DBLE_REQHDL, ic_tg_dble_reqhdl);
 
// ---  class IC_TG_INTG
F_PROT(IC_TG_INTG_XEQCTR, ic_tg_intg_xeqctr);
F_PROT(IC_TG_INTG_XEQMTH, ic_tg_intg_xeqmth);
F_PROT(IC_TG_INTG_REQCLS, ic_tg_intg_reqcls);
F_PROT(IC_TG_INTG_REQHDL, ic_tg_intg_reqhdl);
 
// ---  class NM_AMNO
F_PROT(NM_AMNO_000, nm_amno_000);
F_PROT(NM_AMNO_999, nm_amno_999);
F_PROT(NM_AMNO_CTR, nm_amno_ctr);
F_PROT(NM_AMNO_DTR, nm_amno_dtr);
F_PROT(NM_AMNO_INI, nm_amno_ini);
F_PROT(NM_AMNO_RST, nm_amno_rst);
F_PROT(NM_AMNO_REQHBASE, nm_amno_reqhbase);
F_PROT(NM_AMNO_HVALIDE, nm_amno_hvalide);
F_PROT(NM_AMNO_TVALIDE, nm_amno_tvalide);
F_PROT(NM_AMNO_ASGROW, nm_amno_asgrow);
F_PROT(NM_AMNO_GENNUM, nm_amno_gennum);
F_PROT(NM_AMNO_SAUVE, nm_amno_sauve);
 
// ---  class NM_COMP
F_PROT(NM_COMP_000, nm_comp_000);
F_PROT(NM_COMP_999, nm_comp_999);
F_PROT(NM_COMP_CTR, nm_comp_ctr);
F_PROT(NM_COMP_DTR, nm_comp_dtr);
F_PROT(NM_COMP_INI, nm_comp_ini);
F_PROT(NM_COMP_RST, nm_comp_rst);
F_PROT(NM_COMP_REQHBASE, nm_comp_reqhbase);
F_PROT(NM_COMP_HVALIDE, nm_comp_hvalide);
F_PROT(NM_COMP_TVALIDE, nm_comp_tvalide);
F_PROT(NM_COMP_CHARGE, nm_comp_charge);
F_PROT(NM_COMP_REQNPRT, nm_comp_reqnprt);
F_PROT(NM_COMP_REQNNL, nm_comp_reqnnl);
F_PROT(NM_COMP_REQNNP, nm_comp_reqnnp);
F_PROT(NM_COMP_REQNNT, nm_comp_reqnnt);
F_PROT(NM_COMP_REQNEXT, nm_comp_reqnext);
F_PROT(NM_COMP_REQNINT, nm_comp_reqnint);
F_PROT(NM_COMP_DSYNC, nm_comp_dsync);
F_PROT(NM_COMP_ISYNC, nm_comp_isync);
 
// ---  class NM_IDEN
F_PROT(NM_IDEN_CTR, nm_iden_ctr);
F_PROT(NM_IDEN_DTR, nm_iden_dtr);
F_PROT(NM_IDEN_INI, nm_iden_ini);
F_PROT(NM_IDEN_RST, nm_iden_rst);
F_PROT(NM_IDEN_CHARGE, nm_iden_charge);
F_PROT(NM_IDEN_DSYNC, nm_iden_dsync);
F_PROT(NM_IDEN_ISYNC, nm_iden_isync);
F_PROT(NM_IDEN_GENDSYNC, nm_iden_gendsync);
F_PROT(NM_IDEN_GENISYNC, nm_iden_genisync);
F_PROT(NM_IDEN_ESTNOPP, nm_iden_estnopp);
F_PROT(NM_IDEN_REQNPRT, nm_iden_reqnprt);
F_PROT(NM_IDEN_REQNNL, nm_iden_reqnnl);
F_PROT(NM_IDEN_REQNNP, nm_iden_reqnnp);
F_PROT(NM_IDEN_REQNNT, nm_iden_reqnnt);
F_PROT(NM_IDEN_REQNEXT, nm_iden_reqnext);
F_PROT(NM_IDEN_REQNINT, nm_iden_reqnint);
F_PROT(NM_IDEN_000, nm_iden_000);
F_PROT(NM_IDEN_999, nm_iden_999);
F_PROT(NM_IDEN_REQHBASE, nm_iden_reqhbase);
F_PROT(NM_IDEN_HVALIDE, nm_iden_hvalide);
F_PROT(NM_IDEN_TVALIDE, nm_iden_tvalide);
 
// ---  class NM_LCGL
F_PROT(NM_LCGL_CTR, nm_lcgl_ctr);
F_PROT(NM_LCGL_DTR, nm_lcgl_dtr);
F_PROT(NM_LCGL_INI, nm_lcgl_ini);
F_PROT(NM_LCGL_CHARGE, nm_lcgl_charge);
F_PROT(NM_LCGL_DSYNC, nm_lcgl_dsync);
F_PROT(NM_LCGL_ISYNC, nm_lcgl_isync);
F_PROT(NM_LCGL_GENDSYNC, nm_lcgl_gendsync);
F_PROT(NM_LCGL_GENISYNC, nm_lcgl_genisync);
F_PROT(NM_LCGL_ESTNOPP, nm_lcgl_estnopp);
F_PROT(NM_LCGL_REQNPRT, nm_lcgl_reqnprt);
F_PROT(NM_LCGL_REQNNL, nm_lcgl_reqnnl);
F_PROT(NM_LCGL_REQNNP, nm_lcgl_reqnnp);
F_PROT(NM_LCGL_REQNNT, nm_lcgl_reqnnt);
F_PROT(NM_LCGL_REQNEXT, nm_lcgl_reqnext);
F_PROT(NM_LCGL_REQNINT, nm_lcgl_reqnint);
F_PROT(NM_LCGL_000, nm_lcgl_000);
F_PROT(NM_LCGL_999, nm_lcgl_999);
F_PROT(NM_LCGL_REQHBASE, nm_lcgl_reqhbase);
F_PROT(NM_LCGL_HVALIDE, nm_lcgl_hvalide);
F_PROT(NM_LCGL_TVALIDE, nm_lcgl_tvalide);
 
// ---  class NM_LCLC
F_PROT(NM_LCLC_CTR, nm_lclc_ctr);
F_PROT(NM_LCLC_DTR, nm_lclc_dtr);
F_PROT(NM_LCLC_INI, nm_lclc_ini);
F_PROT(NM_LCLC_CHARGE, nm_lclc_charge);
F_PROT(NM_LCLC_DSYNC, nm_lclc_dsync);
F_PROT(NM_LCLC_ISYNC, nm_lclc_isync);
F_PROT(NM_LCLC_GENDSYNC, nm_lclc_gendsync);
F_PROT(NM_LCLC_GENISYNC, nm_lclc_genisync);
F_PROT(NM_LCLC_ESTNOPP, nm_lclc_estnopp);
F_PROT(NM_LCLC_REQNPRT, nm_lclc_reqnprt);
F_PROT(NM_LCLC_REQNNL, nm_lclc_reqnnl);
F_PROT(NM_LCLC_REQNNP, nm_lclc_reqnnp);
F_PROT(NM_LCLC_REQNNT, nm_lclc_reqnnt);
F_PROT(NM_LCLC_REQNEXT, nm_lclc_reqnext);
F_PROT(NM_LCLC_REQNINT, nm_lclc_reqnint);
F_PROT(NM_LCLC_000, nm_lclc_000);
F_PROT(NM_LCLC_999, nm_lclc_999);
F_PROT(NM_LCLC_REQHBASE, nm_lclc_reqhbase);
F_PROT(NM_LCLC_HVALIDE, nm_lclc_hvalide);
F_PROT(NM_LCLC_TVALIDE, nm_lclc_tvalide);
 
// ---  class NM_NBSE
F_PROT(NM_NBSE_000, nm_nbse_000);
F_PROT(NM_NBSE_999, nm_nbse_999);
F_PROT(NM_NBSE_CTR, nm_nbse_ctr);
F_PROT(NM_NBSE_DTR, nm_nbse_dtr);
F_PROT(NM_NBSE_INI, nm_nbse_ini);
F_PROT(NM_NBSE_RST, nm_nbse_rst);
F_PROT(NM_NBSE_REQHBASE, nm_nbse_reqhbase);
F_PROT(NM_NBSE_HVALIDE, nm_nbse_hvalide);
F_PROT(NM_NBSE_TVALIDE, nm_nbse_tvalide);
F_PROT(NM_NBSE_PART, nm_nbse_part);
F_PROT(NM_NBSE_RENUM, nm_nbse_renum);
F_PROT(NM_NBSE_SAUVE, nm_nbse_sauve);
F_PROT(NM_NBSE_GENNUM, nm_nbse_gennum);
F_PROT(NM_NBSE_ASGDSTR, nm_nbse_asgdstr);
 
// ---  class NM_NUMR
F_PROT(NM_NUMR_DTR, nm_numr_dtr);
F_PROT(NM_NUMR_CTRLH, nm_numr_ctrlh);
F_PROT(NM_NUMR_CHARGE, nm_numr_charge);
F_PROT(NM_NUMR_DSYNC, nm_numr_dsync);
F_PROT(NM_NUMR_ISYNC, nm_numr_isync);
F_PROT(NM_NUMR_GENISYNC, nm_numr_genisync);
F_PROT(NM_NUMR_GENDSYNC, nm_numr_gendsync);
F_PROT(NM_NUMR_REQNPRT, nm_numr_reqnprt);
F_PROT(NM_NUMR_REQNNL, nm_numr_reqnnl);
F_PROT(NM_NUMR_REQNNP, nm_numr_reqnnp);
F_PROT(NM_NUMR_REQNNT, nm_numr_reqnnt);
F_PROT(NM_NUMR_ESTNOPP, nm_numr_estnopp);
F_PROT(NM_NUMR_REQNEXT, nm_numr_reqnext);
F_PROT(NM_NUMR_REQNINT, nm_numr_reqnint);
 
// ---  class NM_PRXY
F_PROT(NM_PRXY_000, nm_prxy_000);
F_PROT(NM_PRXY_999, nm_prxy_999);
F_PROT(NM_PRXY_CTR, nm_prxy_ctr);
F_PROT(NM_PRXY_DTR, nm_prxy_dtr);
F_PROT(NM_PRXY_INI, nm_prxy_ini);
F_PROT(NM_PRXY_RST, nm_prxy_rst);
F_PROT(NM_PRXY_REQHBASE, nm_prxy_reqhbase);
F_PROT(NM_PRXY_HVALIDE, nm_prxy_hvalide);
F_PROT(NM_PRXY_TVALIDE, nm_prxy_tvalide);
F_PROT(NM_PRXY_CHARGE, nm_prxy_charge);
F_PROT(NM_PRXY_ISYNC, nm_prxy_isync);
F_PROT(NM_PRXY_DSYNC, nm_prxy_dsync);
F_PROT(NM_PRXY_GENDSYNC, nm_prxy_gendsync);
F_PROT(NM_PRXY_GENISYNC, nm_prxy_genisync);
F_PROT(NM_PRXY_REQNPRT, nm_prxy_reqnprt);
F_PROT(NM_PRXY_REQNNL, nm_prxy_reqnnl);
F_PROT(NM_PRXY_REQNNP, nm_prxy_reqnnp);
F_PROT(NM_PRXY_REQNNT, nm_prxy_reqnnt);
F_PROT(NM_PRXY_ESTNOPP, nm_prxy_estnopp);
F_PROT(NM_PRXY_REQNEXT, nm_prxy_reqnext);
F_PROT(NM_PRXY_REQNINT, nm_prxy_reqnint);
 
// ---  class NR_NORM
F_PROT(NR_NORM_000, nr_norm_000);
F_PROT(NR_NORM_999, nr_norm_999);
F_PROT(NR_NORM_CTR, nr_norm_ctr);
F_PROT(NR_NORM_DTR, nr_norm_dtr);
F_PROT(NR_NORM_INI, nr_norm_ini);
F_PROT(NR_NORM_RST, nr_norm_rst);
F_PROT(NR_NORM_REQHBASE, nr_norm_reqhbase);
F_PROT(NR_NORM_HVALIDE, nr_norm_hvalide);
F_PROT(NR_NORM_PRN, nr_norm_prn);
F_PROT(NR_NORM_CLC, nr_norm_clc);
 
// ---  class NR_UTIL
F_PROT(NR_UTIL_N2SD, nr_util_n2sd);
F_PROT(NR_UTIL_N2SR, nr_util_n2sr);
F_PROT(NR_UTIL_N2GD, nr_util_n2gd);
F_PROT(NR_UTIL_N2GR, nr_util_n2gr);
F_PROT(NR_UTIL_NISD, nr_util_nisd);
F_PROT(NR_UTIL_NISR, nr_util_nisr);
F_PROT(NR_UTIL_NIGD, nr_util_nigd);
F_PROT(NR_UTIL_NIGR, nr_util_nigr);
 
// ---  class TG_DBLE
F_PROT(TG_DBLE_000, tg_dble_000);
F_PROT(TG_DBLE_999, tg_dble_999);
F_PROT(TG_DBLE_CTR, tg_dble_ctr);
F_PROT(TG_DBLE_DTR, tg_dble_dtr);
F_PROT(TG_DBLE_INI, tg_dble_ini);
F_PROT(TG_DBLE_RST, tg_dble_rst);
F_PROT(TG_DBLE_REQHBASE, tg_dble_reqhbase);
F_PROT(TG_DBLE_HVALIDE, tg_dble_hvalide);
F_PROT(TG_DBLE_INC, tg_dble_inc);
 
// ---  class TG_INTG
F_PROT(TG_INTG_000, tg_intg_000);
F_PROT(TG_INTG_999, tg_intg_999);
F_PROT(TG_INTG_CTR, tg_intg_ctr);
F_PROT(TG_INTG_DTR, tg_intg_dtr);
F_PROT(TG_INTG_INI, tg_intg_ini);
F_PROT(TG_INTG_RST, tg_intg_rst);
F_PROT(TG_INTG_REQHBASE, tg_intg_reqhbase);
F_PROT(TG_INTG_HVALIDE, tg_intg_hvalide);
F_PROT(TG_INTG_INC, tg_intg_inc);
 
// ---  class TG_TRIG
F_PROT(TG_TRIG_DTR, tg_trig_dtr);
F_PROT(TG_TRIG_CTRLH, tg_trig_ctrlh);
F_PROT(TG_TRIG_INC, tg_trig_inc);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_krnl()
{
   static char libname[] = "h2d2_krnl";
 
   // ---  class DT_CLIM
   F_RGST(DT_CLIM_000, dt_clim_000, libname);
   F_RGST(DT_CLIM_999, dt_clim_999, libname);
   F_RGST(DT_CLIM_PKL, dt_clim_pkl, libname);
   F_RGST(DT_CLIM_UPK, dt_clim_upk, libname);
   F_RGST(DT_CLIM_CTR, dt_clim_ctr, libname);
   F_RGST(DT_CLIM_DTR, dt_clim_dtr, libname);
   F_RGST(DT_CLIM_INI, dt_clim_ini, libname);
   F_RGST(DT_CLIM_RST, dt_clim_rst, libname);
   F_RGST(DT_CLIM_REQHBASE, dt_clim_reqhbase, libname);
   F_RGST(DT_CLIM_HVALIDE, dt_clim_hvalide, libname);
   F_RGST(DT_CLIM_CHARGE, dt_clim_charge, libname);
   F_RGST(DT_CLIM_REQHLIMT, dt_clim_reqhlimt, libname);
   F_RGST(DT_CLIM_REQNCLCND, dt_clim_reqnclcnd, libname);
   F_RGST(DT_CLIM_REQLCLCND, dt_clim_reqlclcnd, libname);
   F_RGST(DT_CLIM_REQNCLCNV, dt_clim_reqnclcnv, libname);
   F_RGST(DT_CLIM_REQLCLCNV, dt_clim_reqlclcnv, libname);
   F_RGST(DT_CLIM_REQNCLLIM, dt_clim_reqncllim, libname);
   F_RGST(DT_CLIM_REQNCLNOD, dt_clim_reqnclnod, libname);
   F_RGST(DT_CLIM_REQNCLELE, dt_clim_reqnclele, libname);
   F_RGST(DT_CLIM_REQLCLLIM, dt_clim_reqlcllim, libname);
   F_RGST(DT_CLIM_REQLCLNOD, dt_clim_reqlclnod, libname);
   F_RGST(DT_CLIM_REQLCLELE, dt_clim_reqlclele, libname);
   F_RGST(DT_CLIM_REQLCLDST, dt_clim_reqlcldst, libname);
 
   // ---  class DT_COND
   F_RGST(DT_COND_000, dt_cond_000, libname);
   F_RGST(DT_COND_999, dt_cond_999, libname);
   F_RGST(DT_COND_PKL, dt_cond_pkl, libname);
   F_RGST(DT_COND_UPK, dt_cond_upk, libname);
   F_RGST(DT_COND_CTR, dt_cond_ctr, libname);
   F_RGST(DT_COND_DTR, dt_cond_dtr, libname);
   F_RGST(DT_COND_INI, dt_cond_ini, libname);
   F_RGST(DT_COND_RST, dt_cond_rst, libname);
   F_RGST(DT_COND_REQHBASE, dt_cond_reqhbase, libname);
   F_RGST(DT_COND_HVALIDE, dt_cond_hvalide, libname);
   F_RGST(DT_COND_PRN, dt_cond_prn, libname);
   F_RGST(DT_COND_CHARGE, dt_cond_charge, libname);
   F_RGST(DT_COND_REQNCLCND, dt_cond_reqnclcnd, libname);
   F_RGST(DT_COND_REQNCLCNV, dt_cond_reqnclcnv, libname);
   F_RGST(DT_COND_REQLCLCND, dt_cond_reqlclcnd, libname);
   F_RGST(DT_COND_REQLCLCNV, dt_cond_reqlclcnv, libname);
   F_RGST(DT_COND_REQNFIC, dt_cond_reqnfic, libname);
   F_RGST(DT_COND_REQNOMF, dt_cond_reqnomf, libname);
 
   // ---  class DT_ELEM
   F_RGST(DT_ELEM_000, dt_elem_000, libname);
   F_RGST(DT_ELEM_999, dt_elem_999, libname);
   F_RGST(DT_ELEM_PKL, dt_elem_pkl, libname);
   F_RGST(DT_ELEM_UPK, dt_elem_upk, libname);
   F_RGST(DT_ELEM_CTR, dt_elem_ctr, libname);
   F_RGST(DT_ELEM_DTR, dt_elem_dtr, libname);
   F_RGST(DT_ELEM_INIDIM, dt_elem_inidim, libname);
   F_RGST(DT_ELEM_INIFIC, dt_elem_inific, libname);
   F_RGST(DT_ELEM_INISPL, dt_elem_inispl, libname);
   F_RGST(DT_ELEM_RST, dt_elem_rst, libname);
   F_RGST(DT_ELEM_REQHBASE, dt_elem_reqhbase, libname);
   F_RGST(DT_ELEM_HVALIDE, dt_elem_hvalide, libname);
   F_RGST(DT_ELEM_PRN, dt_elem_prn, libname);
   F_RGST(DT_ELEM_CHARGE, dt_elem_charge, libname);
   F_RGST(DT_ELEM_MAJVAL, dt_elem_majval, libname);
   F_RGST(DT_ELEM_SAUVE, dt_elem_sauve, libname);
   F_RGST(DT_ELEM_ESTINIFIC, dt_elem_estinific, libname);
   F_RGST(DT_ELEM_ESTINIDIM, dt_elem_estinidim, libname);
   F_RGST(DT_ELEM_ESTINISPL, dt_elem_estinispl, libname);
   F_RGST(DT_ELEM_REQISTAT, dt_elem_reqistat, libname);
   F_RGST(DT_ELEM_REQITPE, dt_elem_reqitpe, libname);
   F_RGST(DT_ELEM_REQLELE, dt_elem_reqlele, libname);
   F_RGST(DT_ELEM_REQNCEL, dt_elem_reqncel, libname);
   F_RGST(DT_ELEM_REQNNEL, dt_elem_reqnnel, libname);
   F_RGST(DT_ELEM_REQNELT, dt_elem_reqnelt, libname);
   F_RGST(DT_ELEM_REQNELL, dt_elem_reqnell, libname);
   F_RGST(DT_ELEM_REQTEMPS, dt_elem_reqtemps, libname);
   F_RGST(DT_ELEM_REQNOMF, dt_elem_reqnomf, libname);
 
   // ---  class DT_GRID
   F_RGST(DT_GRID_000, dt_grid_000, libname);
   F_RGST(DT_GRID_999, dt_grid_999, libname);
   F_RGST(DT_GRID_PKL, dt_grid_pkl, libname);
   F_RGST(DT_GRID_UPK, dt_grid_upk, libname);
   F_RGST(DT_GRID_CTR, dt_grid_ctr, libname);
   F_RGST(DT_GRID_DTR, dt_grid_dtr, libname);
   F_RGST(DT_GRID_INI, dt_grid_ini, libname);
   F_RGST(DT_GRID_RST, dt_grid_rst, libname);
   F_RGST(DT_GRID_REQHBASE, dt_grid_reqhbase, libname);
   F_RGST(DT_GRID_HVALIDE, dt_grid_hvalide, libname);
   F_RGST(DT_GRID_PRN, dt_grid_prn, libname);
   F_RGST(DT_GRID_CHARGE, dt_grid_charge, libname);
   F_RGST(DT_GRID_SAUVE, dt_grid_sauve, libname);
   F_RGST(DT_GRID_SPLIT, dt_grid_split, libname);
   F_RGST(DT_GRID_REQHCOOR, dt_grid_reqhcoor, libname);
   F_RGST(DT_GRID_REQHELEV, dt_grid_reqhelev, libname);
   F_RGST(DT_GRID_REQHNUMC, dt_grid_reqhnumc, libname);
   F_RGST(DT_GRID_REQHNUME, dt_grid_reqhnume, libname);
   F_RGST(DT_GRID_REQNDIM, dt_grid_reqndim, libname);
   F_RGST(DT_GRID_REQNNL, dt_grid_reqnnl, libname);
   F_RGST(DT_GRID_REQNNP, dt_grid_reqnnp, libname);
   F_RGST(DT_GRID_REQNNT, dt_grid_reqnnt, libname);
   F_RGST(DT_GRID_REQNNEL, dt_grid_reqnnel, libname);
   F_RGST(DT_GRID_REQNELL, dt_grid_reqnell, libname);
   F_RGST(DT_GRID_REQNELT, dt_grid_reqnelt, libname);
 
   // ---  class DT_LIMT
   F_RGST(DT_LIMT_000, dt_limt_000, libname);
   F_RGST(DT_LIMT_999, dt_limt_999, libname);
   F_RGST(DT_LIMT_PKL, dt_limt_pkl, libname);
   F_RGST(DT_LIMT_UPK, dt_limt_upk, libname);
   F_RGST(DT_LIMT_CTR, dt_limt_ctr, libname);
   F_RGST(DT_LIMT_DTR, dt_limt_dtr, libname);
   F_RGST(DT_LIMT_INIDIM, dt_limt_inidim, libname);
   F_RGST(DT_LIMT_INIFIC, dt_limt_inific, libname);
   F_RGST(DT_LIMT_RST, dt_limt_rst, libname);
   F_RGST(DT_LIMT_REQHBASE, dt_limt_reqhbase, libname);
   F_RGST(DT_LIMT_HVALIDE, dt_limt_hvalide, libname);
   F_RGST(DT_LIMT_PRN, dt_limt_prn, libname);
   F_RGST(DT_LIMT_CHARGE, dt_limt_charge, libname);
   F_RGST(DT_LIMT_MAJVAL, dt_limt_majval, libname);
   F_RGST(DT_LIMT_SAUVE, dt_limt_sauve, libname);
   F_RGST(DT_LIMT_REQNCLLIM, dt_limt_reqncllim, libname);
   F_RGST(DT_LIMT_REQNCLNOD, dt_limt_reqnclnod, libname);
   F_RGST(DT_LIMT_REQNCLELE, dt_limt_reqnclele, libname);
   F_RGST(DT_LIMT_REQLCLLIM, dt_limt_reqlcllim, libname);
   F_RGST(DT_LIMT_REQLCLNOD, dt_limt_reqlclnod, libname);
   F_RGST(DT_LIMT_REQLCLELE, dt_limt_reqlclele, libname);
   F_RGST(DT_LIMT_REQLCLDST, dt_limt_reqlcldst, libname);
   F_RGST(DT_LIMT_REQNOMF, dt_limt_reqnomf, libname);
 
   // ---  class DT_PRGL
   F_RGST(DT_PRGL_000, dt_prgl_000, libname);
   F_RGST(DT_PRGL_999, dt_prgl_999, libname);
   F_RGST(DT_PRGL_PKL, dt_prgl_pkl, libname);
   F_RGST(DT_PRGL_UPK, dt_prgl_upk, libname);
   F_RGST(DT_PRGL_CTR, dt_prgl_ctr, libname);
   F_RGST(DT_PRGL_DTR, dt_prgl_dtr, libname);
   F_RGST(DT_PRGL_INIVAL, dt_prgl_inival, libname);
   F_RGST(DT_PRGL_INIFIC, dt_prgl_inific, libname);
   F_RGST(DT_PRGL_RST, dt_prgl_rst, libname);
   F_RGST(DT_PRGL_REQHBASE, dt_prgl_reqhbase, libname);
   F_RGST(DT_PRGL_HVALIDE, dt_prgl_hvalide, libname);
   F_RGST(DT_PRGL_CHARGE, dt_prgl_charge, libname);
   F_RGST(DT_PRGL_GATHER, dt_prgl_gather, libname);
   F_RGST(DT_PRGL_ASGPRGL, dt_prgl_asgprgl, libname);
   F_RGST(DT_PRGL_REQPRGL, dt_prgl_reqprgl, libname);
   F_RGST(DT_PRGL_REQNPRGL, dt_prgl_reqnprgl, libname);
   F_RGST(DT_PRGL_REQNOMF, dt_prgl_reqnomf, libname);
 
   // ---  class DT_PRNO
   F_RGST(DT_PRNO_000, dt_prno_000, libname);
   F_RGST(DT_PRNO_999, dt_prno_999, libname);
   F_RGST(DT_PRNO_PKL, dt_prno_pkl, libname);
   F_RGST(DT_PRNO_UPK, dt_prno_upk, libname);
   F_RGST(DT_PRNO_CTR, dt_prno_ctr, libname);
   F_RGST(DT_PRNO_DTR, dt_prno_dtr, libname);
   F_RGST(DT_PRNO_INI, dt_prno_ini, libname);
   F_RGST(DT_PRNO_RST, dt_prno_rst, libname);
   F_RGST(DT_PRNO_REQHBASE, dt_prno_reqhbase, libname);
   F_RGST(DT_PRNO_HVALIDE, dt_prno_hvalide, libname);
   F_RGST(DT_PRNO_ADDVNO, dt_prno_addvno, libname);
   F_RGST(DT_PRNO_CHARGE, dt_prno_charge, libname);
   F_RGST(DT_PRNO_GATHER, dt_prno_gather, libname);
   F_RGST(DT_PRNO_SCATTER, dt_prno_scatter, libname);
   F_RGST(DT_PRNO_REQNNT, dt_prno_reqnnt, libname);
   F_RGST(DT_PRNO_REQNNL, dt_prno_reqnnl, libname);
   F_RGST(DT_PRNO_REQNPRN, dt_prno_reqnprn, libname);
   F_RGST(DT_PRNO_REQTEMPS, dt_prno_reqtemps, libname);
 
   // ---  class DT_VNOD
   F_RGST(DT_VNOD_000, dt_vnod_000, libname);
   F_RGST(DT_VNOD_999, dt_vnod_999, libname);
   F_RGST(DT_VNOD_PKL, dt_vnod_pkl, libname);
   F_RGST(DT_VNOD_UPK, dt_vnod_upk, libname);
   F_RGST(DT_VNOD_CTR, dt_vnod_ctr, libname);
   F_RGST(DT_VNOD_DTR, dt_vnod_dtr, libname);
   F_RGST(DT_VNOD_INIDIM, dt_vnod_inidim, libname);
   F_RGST(DT_VNOD_INIFIC, dt_vnod_inific, libname);
   F_RGST(DT_VNOD_RST, dt_vnod_rst, libname);
   F_RGST(DT_VNOD_REQHBASE, dt_vnod_reqhbase, libname);
   F_RGST(DT_VNOD_HVALIDE, dt_vnod_hvalide, libname);
   F_RGST(DT_VNOD_PRN, dt_vnod_prn, libname);
   F_RGST(DT_VNOD_ASGFICECR, dt_vnod_asgficecr, libname);
   F_RGST(DT_VNOD_CHARGE, dt_vnod_charge, libname);
   F_RGST(DT_VNOD_ASG1VAL, dt_vnod_asg1val, libname);
   F_RGST(DT_VNOD_ASGVALS, dt_vnod_asgvals, libname);
   F_RGST(DT_VNOD_REQ1VAL, dt_vnod_req1val, libname);
   F_RGST(DT_VNOD_REQVALS, dt_vnod_reqvals, libname);
   F_RGST(DT_VNOD_SAUVE, dt_vnod_sauve, libname);
   F_RGST(DT_VNOD_ESTINI, dt_vnod_estini, libname);
   F_RGST(DT_VNOD_ESTINIVAL, dt_vnod_estinival, libname);
   F_RGST(DT_VNOD_ESTINIFIC, dt_vnod_estinific, libname);
   F_RGST(DT_VNOD_REQHNUMR, dt_vnod_reqhnumr, libname);
   F_RGST(DT_VNOD_REQLVNO, dt_vnod_reqlvno, libname);
   F_RGST(DT_VNOD_REQLVINI, dt_vnod_reqlvini, libname);
   F_RGST(DT_VNOD_REQNNL, dt_vnod_reqnnl, libname);
   F_RGST(DT_VNOD_REQNNT, dt_vnod_reqnnt, libname);
   F_RGST(DT_VNOD_REQNVNO, dt_vnod_reqnvno, libname);
   F_RGST(DT_VNOD_REQTEMPS, dt_vnod_reqtemps, libname);
   F_RGST(DT_VNOD_REQNFIC, dt_vnod_reqnfic, libname);
   F_RGST(DT_VNOD_REQNOMF, dt_vnod_reqnomf, libname);
 
   // ---  class FD_COND
   F_RGST(FD_COND_000, fd_cond_000, libname);
   F_RGST(FD_COND_999, fd_cond_999, libname);
   F_RGST(FD_COND_PKL, fd_cond_pkl, libname);
   F_RGST(FD_COND_UPK, fd_cond_upk, libname);
   F_RGST(FD_COND_CTR, fd_cond_ctr, libname);
   F_RGST(FD_COND_DTR, fd_cond_dtr, libname);
   F_RGST(FD_COND_INI, fd_cond_ini, libname);
   F_RGST(FD_COND_RST, fd_cond_rst, libname);
   F_RGST(FD_COND_REQHBASE, fd_cond_reqhbase, libname);
   F_RGST(FD_COND_HVALIDE, fd_cond_hvalide, libname);
   F_RGST(FD_COND_LIS, fd_cond_lis, libname);
   F_RGST(FD_COND_REQNCLCND, fd_cond_reqnclcnd, libname);
   F_RGST(FD_COND_REQNCLCNV, fd_cond_reqnclcnv, libname);
   F_RGST(FD_COND_REQNSCT, fd_cond_reqnsct, libname);
   F_RGST(FD_COND_REQTMIN, fd_cond_reqtmin, libname);
   F_RGST(FD_COND_REQTMAX, fd_cond_reqtmax, libname);
   F_RGST(FD_COND_REQNFIC, fd_cond_reqnfic, libname);
   F_RGST(FD_COND_REQNOMF, fd_cond_reqnomf, libname);
 
   // ---  class FD_ELEM
   F_RGST(FD_ELEM_000, fd_elem_000, libname);
   F_RGST(FD_ELEM_999, fd_elem_999, libname);
   F_RGST(FD_ELEM_PKL, fd_elem_pkl, libname);
   F_RGST(FD_ELEM_UPK, fd_elem_upk, libname);
   F_RGST(FD_ELEM_CTR, fd_elem_ctr, libname);
   F_RGST(FD_ELEM_DTR, fd_elem_dtr, libname);
   F_RGST(FD_ELEM_INI, fd_elem_ini, libname);
   F_RGST(FD_ELEM_RST, fd_elem_rst, libname);
   F_RGST(FD_ELEM_REQHBASE, fd_elem_reqhbase, libname);
   F_RGST(FD_ELEM_HVALIDE, fd_elem_hvalide, libname);
   F_RGST(FD_ELEM_ECRIS, fd_elem_ecris, libname);
   F_RGST(FD_ELEM_LIS, fd_elem_lis, libname);
   F_RGST(FD_ELEM_REQNELL, fd_elem_reqnell, libname);
   F_RGST(FD_ELEM_REQISTAT, fd_elem_reqistat, libname);
   F_RGST(FD_ELEM_REQITPE, fd_elem_reqitpe, libname);
   F_RGST(FD_ELEM_REQNELT, fd_elem_reqnelt, libname);
   F_RGST(FD_ELEM_REQNNEL, fd_elem_reqnnel, libname);
   F_RGST(FD_ELEM_REQNSCT, fd_elem_reqnsct, libname);
   F_RGST(FD_ELEM_REQNOMF, fd_elem_reqnomf, libname);
 
   // ---  class FD_LIMT
   F_RGST(FD_LIMT_000, fd_limt_000, libname);
   F_RGST(FD_LIMT_999, fd_limt_999, libname);
   F_RGST(FD_LIMT_PKL, fd_limt_pkl, libname);
   F_RGST(FD_LIMT_UPK, fd_limt_upk, libname);
   F_RGST(FD_LIMT_CTR, fd_limt_ctr, libname);
   F_RGST(FD_LIMT_DTR, fd_limt_dtr, libname);
   F_RGST(FD_LIMT_INI, fd_limt_ini, libname);
   F_RGST(FD_LIMT_RST, fd_limt_rst, libname);
   F_RGST(FD_LIMT_REQHBASE, fd_limt_reqhbase, libname);
   F_RGST(FD_LIMT_HVALIDE, fd_limt_hvalide, libname);
   F_RGST(FD_LIMT_ECRIS, fd_limt_ecris, libname);
   F_RGST(FD_LIMT_LIS, fd_limt_lis, libname);
   F_RGST(FD_LIMT_REQNCLLIM, fd_limt_reqncllim, libname);
   F_RGST(FD_LIMT_REQNCLNOD, fd_limt_reqnclnod, libname);
   F_RGST(FD_LIMT_REQNOMF, fd_limt_reqnomf, libname);
 
   // ---  class FD_NUMR
   F_RGST(FD_NUMR_000, fd_numr_000, libname);
   F_RGST(FD_NUMR_999, fd_numr_999, libname);
   F_RGST(FD_NUMR_CTR, fd_numr_ctr, libname);
   F_RGST(FD_NUMR_DTR, fd_numr_dtr, libname);
   F_RGST(FD_NUMR_INI, fd_numr_ini, libname);
   F_RGST(FD_NUMR_RST, fd_numr_rst, libname);
   F_RGST(FD_NUMR_REQHBASE, fd_numr_reqhbase, libname);
   F_RGST(FD_NUMR_HVALIDE, fd_numr_hvalide, libname);
   F_RGST(FD_NUMR_ECRIS, fd_numr_ecris, libname);
   F_RGST(FD_NUMR_LIS, fd_numr_lis, libname);
   F_RGST(FD_NUMR_REQNNTG, fd_numr_reqnntg, libname);
   F_RGST(FD_NUMR_REQNNTL, fd_numr_reqnntl, libname);
   F_RGST(FD_NUMR_REQNPROC, fd_numr_reqnproc, libname);
 
   // ---  class FD_PRN8
   F_RGST(FD_PRN8_000, fd_prn8_000, libname);
   F_RGST(FD_PRN8_999, fd_prn8_999, libname);
   F_RGST(FD_PRN8_PKL, fd_prn8_pkl, libname);
   F_RGST(FD_PRN8_UPK, fd_prn8_upk, libname);
   F_RGST(FD_PRN8_CTR, fd_prn8_ctr, libname);
   F_RGST(FD_PRN8_DTR, fd_prn8_dtr, libname);
   F_RGST(FD_PRN8_INI, fd_prn8_ini, libname);
   F_RGST(FD_PRN8_RST, fd_prn8_rst, libname);
   F_RGST(FD_PRN8_REQHBASE, fd_prn8_reqhbase, libname);
   F_RGST(FD_PRN8_HVALIDE, fd_prn8_hvalide, libname);
   F_RGST(FD_PRN8_ECRSCT, fd_prn8_ecrsct, libname);
   F_RGST(FD_PRN8_LISSCT, fd_prn8_lissct, libname);
 
   // ---  class FD_PRNA
   F_RGST(FD_PRNA_000, fd_prna_000, libname);
   F_RGST(FD_PRNA_999, fd_prna_999, libname);
   F_RGST(FD_PRNA_PKL, fd_prna_pkl, libname);
   F_RGST(FD_PRNA_UPK, fd_prna_upk, libname);
   F_RGST(FD_PRNA_CTR, fd_prna_ctr, libname);
   F_RGST(FD_PRNA_DTR, fd_prna_dtr, libname);
   F_RGST(FD_PRNA_INI, fd_prna_ini, libname);
   F_RGST(FD_PRNA_RST, fd_prna_rst, libname);
   F_RGST(FD_PRNA_REQHBASE, fd_prna_reqhbase, libname);
   F_RGST(FD_PRNA_HVALIDE, fd_prna_hvalide, libname);
   F_RGST(FD_PRNA_ECRSCT, fd_prna_ecrsct, libname);
   F_RGST(FD_PRNA_LISSCT, fd_prna_lissct, libname);
 
   // ---  class FD_PRNB
   F_RGST(FD_PRNB_000, fd_prnb_000, libname);
   F_RGST(FD_PRNB_999, fd_prnb_999, libname);
   F_RGST(FD_PRNB_PKL, fd_prnb_pkl, libname);
   F_RGST(FD_PRNB_UPK, fd_prnb_upk, libname);
   F_RGST(FD_PRNB_CTR, fd_prnb_ctr, libname);
   F_RGST(FD_PRNB_DTR, fd_prnb_dtr, libname);
   F_RGST(FD_PRNB_INI, fd_prnb_ini, libname);
   F_RGST(FD_PRNB_RST, fd_prnb_rst, libname);
   F_RGST(FD_PRNB_REQHBASE, fd_prnb_reqhbase, libname);
   F_RGST(FD_PRNB_HVALIDE, fd_prnb_hvalide, libname);
   F_RGST(FD_PRNB_ECRSCT, fd_prnb_ecrsct, libname);
   F_RGST(FD_PRNB_LISSCT, fd_prnb_lissct, libname);
 
   // ---  class FD_PRNO
   F_RGST(FD_PRNO_000, fd_prno_000, libname);
   F_RGST(FD_PRNO_999, fd_prno_999, libname);
   F_RGST(FD_PRNO_PKL, fd_prno_pkl, libname);
   F_RGST(FD_PRNO_UPK, fd_prno_upk, libname);
   F_RGST(FD_PRNO_CTR, fd_prno_ctr, libname);
   F_RGST(FD_PRNO_DTR, fd_prno_dtr, libname);
   F_RGST(FD_PRNO_INI, fd_prno_ini, libname);
   F_RGST(FD_PRNO_RST, fd_prno_rst, libname);
   F_RGST(FD_PRNO_REQHBASE, fd_prno_reqhbase, libname);
   F_RGST(FD_PRNO_HVALIDE, fd_prno_hvalide, libname);
   F_RGST(FD_PRNO_ECRIS, fd_prno_ecris, libname);
   F_RGST(FD_PRNO_LIS, fd_prno_lis, libname);
   F_RGST(FD_PRNO_REQISTAT, fd_prno_reqistat, libname);
   F_RGST(FD_PRNO_REQNNT, fd_prno_reqnnt, libname);
   F_RGST(FD_PRNO_REQNPRN, fd_prno_reqnprn, libname);
   F_RGST(FD_PRNO_REQNSCT, fd_prno_reqnsct, libname);
   F_RGST(FD_PRNO_REQTMIN, fd_prno_reqtmin, libname);
   F_RGST(FD_PRNO_REQTMAX, fd_prno_reqtmax, libname);
   F_RGST(FD_PRNO_REQNFIC, fd_prno_reqnfic, libname);
   F_RGST(FD_PRNO_REQNOMF, fd_prno_reqnomf, libname);
 
   // ---  class IC_NUMG
   F_RGST(IC_NUMG_CMD, ic_numg_cmd, libname);
   F_RGST(IC_NUMG_REQCMD, ic_numg_reqcmd, libname);
 
   // ---  class IC_NUML
   F_RGST(IC_NUML_CMD, ic_numl_cmd, libname);
   F_RGST(IC_NUML_REQCMD, ic_numl_reqcmd, libname);
 
   // ---  class IC_DT_CLIM
   F_RGST(IC_DT_CLIM_XEQCTR, ic_dt_clim_xeqctr, libname);
   F_RGST(IC_DT_CLIM_XEQMTH, ic_dt_clim_xeqmth, libname);
   F_RGST(IC_DT_CLIM_REQCLS, ic_dt_clim_reqcls, libname);
   F_RGST(IC_DT_CLIM_REQHDL, ic_dt_clim_reqhdl, libname);
 
   // ---  class IC_DT_COND
   F_RGST(IC_DT_COND_XEQCTR, ic_dt_cond_xeqctr, libname);
   F_RGST(IC_DT_COND_XEQMTH, ic_dt_cond_xeqmth, libname);
   F_RGST(IC_DT_COND_REQCLS, ic_dt_cond_reqcls, libname);
   F_RGST(IC_DT_COND_REQHDL, ic_dt_cond_reqhdl, libname);
 
   // ---  class IC_DT_COOR
   F_RGST(IC_DT_COOR_CMD, ic_dt_coor_cmd, libname);
   F_RGST(IC_DT_COOR_REQCMD, ic_dt_coor_reqcmd, libname);
 
   // ---  class IC_DT_DLIB
   F_RGST(IC_DT_DLIB_XEQCTR, ic_dt_dlib_xeqctr, libname);
   F_RGST(IC_DT_DLIB_XEQMTH, ic_dt_dlib_xeqmth, libname);
   F_RGST(IC_DT_DLIB_REQHDL, ic_dt_dlib_reqhdl, libname);
   F_RGST(IC_DT_DLIB_REQCLS, ic_dt_dlib_reqcls, libname);
 
   // ---  class IC_DT_ELEM
   F_RGST(IC_DT_ELEM_XEQCTR, ic_dt_elem_xeqctr, libname);
   F_RGST(IC_DT_ELEM_XEQMTH, ic_dt_elem_xeqmth, libname);
   F_RGST(IC_DT_ELEM_REQCLS, ic_dt_elem_reqcls, libname);
   F_RGST(IC_DT_ELEM_REQHDL, ic_dt_elem_reqhdl, libname);
 
   // ---  class IC_DT_GRID
   F_RGST(IC_DT_GRID_XEQCTR, ic_dt_grid_xeqctr, libname);
   F_RGST(IC_DT_GRID_XEQMTH, ic_dt_grid_xeqmth, libname);
   F_RGST(IC_DT_GRID_REQCLS, ic_dt_grid_reqcls, libname);
   F_RGST(IC_DT_GRID_REQHDL, ic_dt_grid_reqhdl, libname);
 
   // ---  class IC_DT_LIMT
   F_RGST(IC_DT_LIMT_XEQCTR, ic_dt_limt_xeqctr, libname);
   F_RGST(IC_DT_LIMT_XEQMTH, ic_dt_limt_xeqmth, libname);
   F_RGST(IC_DT_LIMT_REQCLS, ic_dt_limt_reqcls, libname);
   F_RGST(IC_DT_LIMT_REQHDL, ic_dt_limt_reqhdl, libname);
 
   // ---  class IC_DT_PREL
   F_RGST(IC_DT_PREL_CMD, ic_dt_prel_cmd, libname);
   F_RGST(IC_DT_PREL_REQCMD, ic_dt_prel_reqcmd, libname);
 
   // ---  class IC_DT_PRGL
   F_RGST(IC_DT_PRGL_XEQCTR, ic_dt_prgl_xeqctr, libname);
   F_RGST(IC_DT_PRGL_REQCLS, ic_dt_prgl_reqcls, libname);
   F_RGST(IC_DT_PRGL_REQHDL, ic_dt_prgl_reqhdl, libname);
   F_RGST(IC_DT_PRGL_XEQMTH, ic_dt_prgl_xeqmth, libname);
 
   // ---  class IC_DT_PRNO
   F_RGST(IC_DT_PRNO_XEQCTR, ic_dt_prno_xeqctr, libname);
   F_RGST(IC_DT_PRNO_XEQMTH, ic_dt_prno_xeqmth, libname);
   F_RGST(IC_DT_PRNO_REQCLS, ic_dt_prno_reqcls, libname);
   F_RGST(IC_DT_PRNO_REQHDL, ic_dt_prno_reqhdl, libname);
 
   // ---  class IC_DT_SOLC
   F_RGST(IC_DT_SOLC_CMD, ic_dt_solc_cmd, libname);
   F_RGST(IC_DT_SOLC_REQCMD, ic_dt_solc_reqcmd, libname);
 
   // ---  class IC_DT_SOLR
   F_RGST(IC_DT_SOLR_CMD, ic_dt_solr_cmd, libname);
   F_RGST(IC_DT_SOLR_REQCMD, ic_dt_solr_reqcmd, libname);
 
   // ---  class IC_DT_VELE
   F_RGST(IC_DT_VELE_CMD, ic_dt_vele_cmd, libname);
   F_RGST(IC_DT_VELE_REQCMD, ic_dt_vele_reqcmd, libname);
 
   // ---  class IC_DT_VNOD
   F_RGST(IC_DT_VNOD_XEQCTR, ic_dt_vnod_xeqctr, libname);
   F_RGST(IC_DT_VNOD_XEQMTH, ic_dt_vnod_xeqmth, libname);
   F_RGST(IC_DT_VNOD_REQCLS, ic_dt_vnod_reqcls, libname);
   F_RGST(IC_DT_VNOD_REQHDL, ic_dt_vnod_reqhdl, libname);
 
   // ---  class IC_NM_LCGL
   F_RGST(IC_NM_LCGL_XEQCTR, ic_nm_lcgl_xeqctr, libname);
   F_RGST(IC_NM_LCGL_XEQMTH, ic_nm_lcgl_xeqmth, libname);
   F_RGST(IC_NM_LCGL_REQCLS, ic_nm_lcgl_reqcls, libname);
   F_RGST(IC_NM_LCGL_REQHDL, ic_nm_lcgl_reqhdl, libname);
 
   // ---  class IC_NM_PRXY
   F_RGST(IC_NM_PRXY_XEQCTR, ic_nm_prxy_xeqctr, libname);
   F_RGST(IC_NM_PRXY_XEQMTH, ic_nm_prxy_xeqmth, libname);
   F_RGST(IC_NM_PRXY_REQCLS, ic_nm_prxy_reqcls, libname);
   F_RGST(IC_NM_PRXY_REQHDL, ic_nm_prxy_reqhdl, libname);
 
   // ---  class IC_TG_DBLE
   F_RGST(IC_TG_DBLE_XEQCTR, ic_tg_dble_xeqctr, libname);
   F_RGST(IC_TG_DBLE_XEQMTH, ic_tg_dble_xeqmth, libname);
   F_RGST(IC_TG_DBLE_REQCLS, ic_tg_dble_reqcls, libname);
   F_RGST(IC_TG_DBLE_REQHDL, ic_tg_dble_reqhdl, libname);
 
   // ---  class IC_TG_INTG
   F_RGST(IC_TG_INTG_XEQCTR, ic_tg_intg_xeqctr, libname);
   F_RGST(IC_TG_INTG_XEQMTH, ic_tg_intg_xeqmth, libname);
   F_RGST(IC_TG_INTG_REQCLS, ic_tg_intg_reqcls, libname);
   F_RGST(IC_TG_INTG_REQHDL, ic_tg_intg_reqhdl, libname);
 
   // ---  class NM_AMNO
   F_RGST(NM_AMNO_000, nm_amno_000, libname);
   F_RGST(NM_AMNO_999, nm_amno_999, libname);
   F_RGST(NM_AMNO_CTR, nm_amno_ctr, libname);
   F_RGST(NM_AMNO_DTR, nm_amno_dtr, libname);
   F_RGST(NM_AMNO_INI, nm_amno_ini, libname);
   F_RGST(NM_AMNO_RST, nm_amno_rst, libname);
   F_RGST(NM_AMNO_REQHBASE, nm_amno_reqhbase, libname);
   F_RGST(NM_AMNO_HVALIDE, nm_amno_hvalide, libname);
   F_RGST(NM_AMNO_TVALIDE, nm_amno_tvalide, libname);
   F_RGST(NM_AMNO_ASGROW, nm_amno_asgrow, libname);
   F_RGST(NM_AMNO_GENNUM, nm_amno_gennum, libname);
   F_RGST(NM_AMNO_SAUVE, nm_amno_sauve, libname);
 
   // ---  class NM_COMP
   F_RGST(NM_COMP_000, nm_comp_000, libname);
   F_RGST(NM_COMP_999, nm_comp_999, libname);
   F_RGST(NM_COMP_CTR, nm_comp_ctr, libname);
   F_RGST(NM_COMP_DTR, nm_comp_dtr, libname);
   F_RGST(NM_COMP_INI, nm_comp_ini, libname);
   F_RGST(NM_COMP_RST, nm_comp_rst, libname);
   F_RGST(NM_COMP_REQHBASE, nm_comp_reqhbase, libname);
   F_RGST(NM_COMP_HVALIDE, nm_comp_hvalide, libname);
   F_RGST(NM_COMP_TVALIDE, nm_comp_tvalide, libname);
   F_RGST(NM_COMP_CHARGE, nm_comp_charge, libname);
   F_RGST(NM_COMP_REQNPRT, nm_comp_reqnprt, libname);
   F_RGST(NM_COMP_REQNNL, nm_comp_reqnnl, libname);
   F_RGST(NM_COMP_REQNNP, nm_comp_reqnnp, libname);
   F_RGST(NM_COMP_REQNNT, nm_comp_reqnnt, libname);
   F_RGST(NM_COMP_REQNEXT, nm_comp_reqnext, libname);
   F_RGST(NM_COMP_REQNINT, nm_comp_reqnint, libname);
   F_RGST(NM_COMP_DSYNC, nm_comp_dsync, libname);
   F_RGST(NM_COMP_ISYNC, nm_comp_isync, libname);
 
   // ---  class NM_IDEN
   F_RGST(NM_IDEN_CTR, nm_iden_ctr, libname);
   F_RGST(NM_IDEN_DTR, nm_iden_dtr, libname);
   F_RGST(NM_IDEN_INI, nm_iden_ini, libname);
   F_RGST(NM_IDEN_RST, nm_iden_rst, libname);
   F_RGST(NM_IDEN_CHARGE, nm_iden_charge, libname);
   F_RGST(NM_IDEN_DSYNC, nm_iden_dsync, libname);
   F_RGST(NM_IDEN_ISYNC, nm_iden_isync, libname);
   F_RGST(NM_IDEN_GENDSYNC, nm_iden_gendsync, libname);
   F_RGST(NM_IDEN_GENISYNC, nm_iden_genisync, libname);
   F_RGST(NM_IDEN_ESTNOPP, nm_iden_estnopp, libname);
   F_RGST(NM_IDEN_REQNPRT, nm_iden_reqnprt, libname);
   F_RGST(NM_IDEN_REQNNL, nm_iden_reqnnl, libname);
   F_RGST(NM_IDEN_REQNNP, nm_iden_reqnnp, libname);
   F_RGST(NM_IDEN_REQNNT, nm_iden_reqnnt, libname);
   F_RGST(NM_IDEN_REQNEXT, nm_iden_reqnext, libname);
   F_RGST(NM_IDEN_REQNINT, nm_iden_reqnint, libname);
   F_RGST(NM_IDEN_000, nm_iden_000, libname);
   F_RGST(NM_IDEN_999, nm_iden_999, libname);
   F_RGST(NM_IDEN_REQHBASE, nm_iden_reqhbase, libname);
   F_RGST(NM_IDEN_HVALIDE, nm_iden_hvalide, libname);
   F_RGST(NM_IDEN_TVALIDE, nm_iden_tvalide, libname);
 
   // ---  class NM_LCGL
   F_RGST(NM_LCGL_CTR, nm_lcgl_ctr, libname);
   F_RGST(NM_LCGL_DTR, nm_lcgl_dtr, libname);
   F_RGST(NM_LCGL_INI, nm_lcgl_ini, libname);
   F_RGST(NM_LCGL_CHARGE, nm_lcgl_charge, libname);
   F_RGST(NM_LCGL_DSYNC, nm_lcgl_dsync, libname);
   F_RGST(NM_LCGL_ISYNC, nm_lcgl_isync, libname);
   F_RGST(NM_LCGL_GENDSYNC, nm_lcgl_gendsync, libname);
   F_RGST(NM_LCGL_GENISYNC, nm_lcgl_genisync, libname);
   F_RGST(NM_LCGL_ESTNOPP, nm_lcgl_estnopp, libname);
   F_RGST(NM_LCGL_REQNPRT, nm_lcgl_reqnprt, libname);
   F_RGST(NM_LCGL_REQNNL, nm_lcgl_reqnnl, libname);
   F_RGST(NM_LCGL_REQNNP, nm_lcgl_reqnnp, libname);
   F_RGST(NM_LCGL_REQNNT, nm_lcgl_reqnnt, libname);
   F_RGST(NM_LCGL_REQNEXT, nm_lcgl_reqnext, libname);
   F_RGST(NM_LCGL_REQNINT, nm_lcgl_reqnint, libname);
   F_RGST(NM_LCGL_000, nm_lcgl_000, libname);
   F_RGST(NM_LCGL_999, nm_lcgl_999, libname);
   F_RGST(NM_LCGL_REQHBASE, nm_lcgl_reqhbase, libname);
   F_RGST(NM_LCGL_HVALIDE, nm_lcgl_hvalide, libname);
   F_RGST(NM_LCGL_TVALIDE, nm_lcgl_tvalide, libname);
 
   // ---  class NM_LCLC
   F_RGST(NM_LCLC_CTR, nm_lclc_ctr, libname);
   F_RGST(NM_LCLC_DTR, nm_lclc_dtr, libname);
   F_RGST(NM_LCLC_INI, nm_lclc_ini, libname);
   F_RGST(NM_LCLC_CHARGE, nm_lclc_charge, libname);
   F_RGST(NM_LCLC_DSYNC, nm_lclc_dsync, libname);
   F_RGST(NM_LCLC_ISYNC, nm_lclc_isync, libname);
   F_RGST(NM_LCLC_GENDSYNC, nm_lclc_gendsync, libname);
   F_RGST(NM_LCLC_GENISYNC, nm_lclc_genisync, libname);
   F_RGST(NM_LCLC_ESTNOPP, nm_lclc_estnopp, libname);
   F_RGST(NM_LCLC_REQNPRT, nm_lclc_reqnprt, libname);
   F_RGST(NM_LCLC_REQNNL, nm_lclc_reqnnl, libname);
   F_RGST(NM_LCLC_REQNNP, nm_lclc_reqnnp, libname);
   F_RGST(NM_LCLC_REQNNT, nm_lclc_reqnnt, libname);
   F_RGST(NM_LCLC_REQNEXT, nm_lclc_reqnext, libname);
   F_RGST(NM_LCLC_REQNINT, nm_lclc_reqnint, libname);
   F_RGST(NM_LCLC_000, nm_lclc_000, libname);
   F_RGST(NM_LCLC_999, nm_lclc_999, libname);
   F_RGST(NM_LCLC_REQHBASE, nm_lclc_reqhbase, libname);
   F_RGST(NM_LCLC_HVALIDE, nm_lclc_hvalide, libname);
   F_RGST(NM_LCLC_TVALIDE, nm_lclc_tvalide, libname);
 
   // ---  class NM_NBSE
   F_RGST(NM_NBSE_000, nm_nbse_000, libname);
   F_RGST(NM_NBSE_999, nm_nbse_999, libname);
   F_RGST(NM_NBSE_CTR, nm_nbse_ctr, libname);
   F_RGST(NM_NBSE_DTR, nm_nbse_dtr, libname);
   F_RGST(NM_NBSE_INI, nm_nbse_ini, libname);
   F_RGST(NM_NBSE_RST, nm_nbse_rst, libname);
   F_RGST(NM_NBSE_REQHBASE, nm_nbse_reqhbase, libname);
   F_RGST(NM_NBSE_HVALIDE, nm_nbse_hvalide, libname);
   F_RGST(NM_NBSE_TVALIDE, nm_nbse_tvalide, libname);
   F_RGST(NM_NBSE_PART, nm_nbse_part, libname);
   F_RGST(NM_NBSE_RENUM, nm_nbse_renum, libname);
   F_RGST(NM_NBSE_SAUVE, nm_nbse_sauve, libname);
   F_RGST(NM_NBSE_GENNUM, nm_nbse_gennum, libname);
   F_RGST(NM_NBSE_ASGDSTR, nm_nbse_asgdstr, libname);
 
   // ---  class NM_NUMR
   F_RGST(NM_NUMR_DTR, nm_numr_dtr, libname);
   F_RGST(NM_NUMR_CTRLH, nm_numr_ctrlh, libname);
   F_RGST(NM_NUMR_CHARGE, nm_numr_charge, libname);
   F_RGST(NM_NUMR_DSYNC, nm_numr_dsync, libname);
   F_RGST(NM_NUMR_ISYNC, nm_numr_isync, libname);
   F_RGST(NM_NUMR_GENISYNC, nm_numr_genisync, libname);
   F_RGST(NM_NUMR_GENDSYNC, nm_numr_gendsync, libname);
   F_RGST(NM_NUMR_REQNPRT, nm_numr_reqnprt, libname);
   F_RGST(NM_NUMR_REQNNL, nm_numr_reqnnl, libname);
   F_RGST(NM_NUMR_REQNNP, nm_numr_reqnnp, libname);
   F_RGST(NM_NUMR_REQNNT, nm_numr_reqnnt, libname);
   F_RGST(NM_NUMR_ESTNOPP, nm_numr_estnopp, libname);
   F_RGST(NM_NUMR_REQNEXT, nm_numr_reqnext, libname);
   F_RGST(NM_NUMR_REQNINT, nm_numr_reqnint, libname);
 
   // ---  class NM_PRXY
   F_RGST(NM_PRXY_000, nm_prxy_000, libname);
   F_RGST(NM_PRXY_999, nm_prxy_999, libname);
   F_RGST(NM_PRXY_CTR, nm_prxy_ctr, libname);
   F_RGST(NM_PRXY_DTR, nm_prxy_dtr, libname);
   F_RGST(NM_PRXY_INI, nm_prxy_ini, libname);
   F_RGST(NM_PRXY_RST, nm_prxy_rst, libname);
   F_RGST(NM_PRXY_REQHBASE, nm_prxy_reqhbase, libname);
   F_RGST(NM_PRXY_HVALIDE, nm_prxy_hvalide, libname);
   F_RGST(NM_PRXY_TVALIDE, nm_prxy_tvalide, libname);
   F_RGST(NM_PRXY_CHARGE, nm_prxy_charge, libname);
   F_RGST(NM_PRXY_ISYNC, nm_prxy_isync, libname);
   F_RGST(NM_PRXY_DSYNC, nm_prxy_dsync, libname);
   F_RGST(NM_PRXY_GENDSYNC, nm_prxy_gendsync, libname);
   F_RGST(NM_PRXY_GENISYNC, nm_prxy_genisync, libname);
   F_RGST(NM_PRXY_REQNPRT, nm_prxy_reqnprt, libname);
   F_RGST(NM_PRXY_REQNNL, nm_prxy_reqnnl, libname);
   F_RGST(NM_PRXY_REQNNP, nm_prxy_reqnnp, libname);
   F_RGST(NM_PRXY_REQNNT, nm_prxy_reqnnt, libname);
   F_RGST(NM_PRXY_ESTNOPP, nm_prxy_estnopp, libname);
   F_RGST(NM_PRXY_REQNEXT, nm_prxy_reqnext, libname);
   F_RGST(NM_PRXY_REQNINT, nm_prxy_reqnint, libname);
 
   // ---  class NR_NORM
   F_RGST(NR_NORM_000, nr_norm_000, libname);
   F_RGST(NR_NORM_999, nr_norm_999, libname);
   F_RGST(NR_NORM_CTR, nr_norm_ctr, libname);
   F_RGST(NR_NORM_DTR, nr_norm_dtr, libname);
   F_RGST(NR_NORM_INI, nr_norm_ini, libname);
   F_RGST(NR_NORM_RST, nr_norm_rst, libname);
   F_RGST(NR_NORM_REQHBASE, nr_norm_reqhbase, libname);
   F_RGST(NR_NORM_HVALIDE, nr_norm_hvalide, libname);
   F_RGST(NR_NORM_PRN, nr_norm_prn, libname);
   F_RGST(NR_NORM_CLC, nr_norm_clc, libname);
 
   // ---  class NR_UTIL
   F_RGST(NR_UTIL_N2SD, nr_util_n2sd, libname);
   F_RGST(NR_UTIL_N2SR, nr_util_n2sr, libname);
   F_RGST(NR_UTIL_N2GD, nr_util_n2gd, libname);
   F_RGST(NR_UTIL_N2GR, nr_util_n2gr, libname);
   F_RGST(NR_UTIL_NISD, nr_util_nisd, libname);
   F_RGST(NR_UTIL_NISR, nr_util_nisr, libname);
   F_RGST(NR_UTIL_NIGD, nr_util_nigd, libname);
   F_RGST(NR_UTIL_NIGR, nr_util_nigr, libname);
 
   // ---  class TG_DBLE
   F_RGST(TG_DBLE_000, tg_dble_000, libname);
   F_RGST(TG_DBLE_999, tg_dble_999, libname);
   F_RGST(TG_DBLE_CTR, tg_dble_ctr, libname);
   F_RGST(TG_DBLE_DTR, tg_dble_dtr, libname);
   F_RGST(TG_DBLE_INI, tg_dble_ini, libname);
   F_RGST(TG_DBLE_RST, tg_dble_rst, libname);
   F_RGST(TG_DBLE_REQHBASE, tg_dble_reqhbase, libname);
   F_RGST(TG_DBLE_HVALIDE, tg_dble_hvalide, libname);
   F_RGST(TG_DBLE_INC, tg_dble_inc, libname);
 
   // ---  class TG_INTG
   F_RGST(TG_INTG_000, tg_intg_000, libname);
   F_RGST(TG_INTG_999, tg_intg_999, libname);
   F_RGST(TG_INTG_CTR, tg_intg_ctr, libname);
   F_RGST(TG_INTG_DTR, tg_intg_dtr, libname);
   F_RGST(TG_INTG_INI, tg_intg_ini, libname);
   F_RGST(TG_INTG_RST, tg_intg_rst, libname);
   F_RGST(TG_INTG_REQHBASE, tg_intg_reqhbase, libname);
   F_RGST(TG_INTG_HVALIDE, tg_intg_hvalide, libname);
   F_RGST(TG_INTG_INC, tg_intg_inc, libname);
 
   // ---  class TG_TRIG
   F_RGST(TG_TRIG_DTR, tg_trig_dtr, libname);
   F_RGST(TG_TRIG_CTRLH, tg_trig_ctrlh, libname);
   F_RGST(TG_TRIG_INC, tg_trig_inc, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
