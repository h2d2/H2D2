C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER DT_VNOD_000
C     INTEGER DT_VNOD_999
C     INTEGER DT_VNOD_PKL
C     INTEGER DT_VNOD_UPK
C     INTEGER DT_VNOD_CTR
C     INTEGER DT_VNOD_DTR
C     INTEGER DT_VNOD_INIDIM
C     INTEGER DT_VNOD_INIFIC
C     INTEGER DT_VNOD_RST
C     INTEGER DT_VNOD_REQHBASE
C     LOGICAL DT_VNOD_HVALIDE
C     INTEGER DT_VNOD_PRN
C     INTEGER DT_VNOD_ASGFICECR
C     INTEGER DT_VNOD_CHARGE
C     INTEGER DT_VNOD_ASG1VAL
C     INTEGER DT_VNOD_ASGVALS
C     INTEGER DT_VNOD_REQ1VAL
C     INTEGER DT_VNOD_REQVALS
C     INTEGER DT_VNOD_SAUVE
C     LOGICAL DT_VNOD_ESTINI
C     LOGICAL DT_VNOD_ESTINIVAL
C     LOGICAL DT_VNOD_ESTINIFIC
C     INTEGER DT_VNOD_REQLVNO
C     INTEGER DT_VNOD_REQLVINI
C     INTEGER DT_VNOD_REQNNL
C     INTEGER DT_VNOD_REQNNT
C     INTEGER DT_VNOD_REQNVNO
C     REAL*8 DT_VNOD_REQTEMPS
C     INTEGER DT_VNOD_REQNFIC
C     CHARACTER*256 DT_VNOD_REQNOMF
C   Private:
C     INTEGER DT_VNOD_RAZ
C     INTEGER DT_VNOD_PRNVNO
C     INTEGER DT_VNOD_ASGVAL_TRV
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction DT_VNOD_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(DT_VNOD_NOBJMAX,
     &                   DT_VNOD_HBASE,
     &                   'Nodal Values')

      DT_VNOD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction DT_VNOD_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER  IERR
      EXTERNAL DT_VNOD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(DT_VNOD_NOBJMAX,
     &                   DT_VNOD_HBASE,
     &                   DT_VNOD_DTR)

      DT_VNOD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_VNOD_HBASE

C---     Pickle
      IF (ERR_GOOD()) IERR = IO_XML_WD_1(HXML, DT_VNOD_TEMPS (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_R(HXML, DT_VNOD_HNUMR (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_VNOD_HFLCT (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WH_A(HXML, DT_VNOD_HFECR (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_VNOD_NNT   (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_VNOD_NNL   (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_WI_1(HXML, DT_VNOD_NVNO  (IOB))

C---     La table VINI
      IF (ERR_GOOD()) THEN
         N = DT_VNOD_NVNO (IOB)
         L = DT_VNOD_LVINI(IOB)
         IERR = IO_XML_WD_V(HXML, N, N, L, 1)
      ENDIF

C---     La table VNOD
      IF (ERR_GOOD()) THEN
         N = DT_VNOD_NNL(IOB) * DT_VNOD_NVNO(IOB)
         NX = MAX(1, N)
         L = DT_VNOD_LVNOD(IOB)
         IERR = IO_XML_WD_V(HXML, NX, N, L, 1)
      ENDIF

      DT_VNOD_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER L, N, NX
      LOGICAL LEXIST
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB = HOBJ - DT_VNOD_HBASE

C---     Initialise l'objet
      IF (LNEW) IERR = DT_VNOD_RAZ(HOBJ)
      IERR = DT_VNOD_RST(HOBJ)

C---     Un-pickle
      IF (ERR_GOOD()) IERR = IO_XML_RD_1(HXML, DT_VNOD_TEMPS(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_R(HXML, DT_VNOD_HNUMR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_VNOD_HFLCT(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RH_A(HXML, DT_VNOD_HFECR(IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_VNOD_NNT  (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_VNOD_NNL  (IOB))
      IF (ERR_GOOD()) IERR = IO_XML_RI_1(HXML, DT_VNOD_NVNO (IOB))

C---     La table VINI
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR.
D    &             NX .EQ. DT_VNOD_NVNO(IOB))
      DT_VNOD_LVINI(IOB) = L

C---     La table VNOD
      L = 0
      IF (ERR_GOOD()) IERR = IO_XML_RD_V(HXML, NX, N, L, 1)
D     CALL ERR_ASR(ERR_BAD() .OR.
D    &             NX .EQ. MAX(1, DT_VNOD_NNL(IOB)*DT_VNOD_NVNO(IOB)))
      DT_VNOD_LVNOD(IOB) = L

      DT_VNOD_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction DT_VNOD_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IF (ERR_GOOD()) IERR = OB_OBJC_CTR(HOBJ,
     &                                   DT_VNOD_NOBJMAX,
     &                                   DT_VNOD_HBASE)
      IF (ERR_GOOD()) IERR = DT_VNOD_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. DT_VNOD_HVALIDE(HOBJ))

      DT_VNOD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction DT_VNOD_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = DT_VNOD_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   DT_VNOD_NOBJMAX,
     &                   DT_VNOD_HBASE)

      DT_VNOD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Effaceur de la classe.
C
C Description:
C     La méthode DT_VNOD_RAZ efface les attributs en leur assignant
C     une valeur nulle (ou l'équivalent).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_RAZ(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - DT_VNOD_HBASE

      DT_VNOD_TEMPS (IOB) = -1.0D0
      DT_VNOD_HNUMR (IOB) = 0
      DT_VNOD_HFLCT (IOB) = 0
      DT_VNOD_HFECR (IOB) = 0
      DT_VNOD_LVINI (IOB) = 0
      DT_VNOD_LVNOD (IOB) = 0
      DT_VNOD_NNT   (IOB) = DT_VNOD_DIM_DIFFERE
      DT_VNOD_NNL   (IOB) = DT_VNOD_DIM_DIFFERE
      DT_VNOD_NVNO  (IOB) = 0

      DT_VNOD_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec des dimensions.
C
C Description:
C     La fonction <code>DT_VNOD_INIDIM(...)<code> initialise l'objet avec
C     les dimensions en argument. L'objet est dimensionné et initialisé aux
C     valeurs de VINI si DOINIT est .TRUE..
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVNO        Nombre de valeurs
C     NNL         Nombre de Noeuds Locaux
C     NNT         Nombre de Noeuds Total
C     DOINIT      .TRUE. pour une initialisation avec VINI
C     VINI        Valeurs d'initialisation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_INIDIM(HOBJ, NVNO, NNL, NNT, DOINIT, VINI)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_INIDIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVNO
      INTEGER NNL
      INTEGER NNT
      LOGICAL DOINIT
      REAL*8  VINI(*)

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER NNX, NVX
      INTEGER LVNOD, LVINI
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     CALL ERR_PRE(NVNO .GE. 0)
D     CALL ERR_PRE(NNL  .GE. 0 .OR. NNT .EQ. DT_VNOD_DIM_DIFFERE)
D     CALL ERR_PRE(NNT  .GT. 0 .OR. NNT .EQ. DT_VNOD_DIM_DIFFERE)
D     CALL ERR_PRE(NNT  .GE. NNL)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles
      IF (NVNO .LE. 0 .AND. NNT .NE. DT_VNOD_DIM_DIFFERE) GOTO 9900
!!!      IF (NVNO .NE. 0 .AND. NNT .EQ. DT_VNOD_DIM_DIFFERE) GOTO 9900
      IF (NNL  .LT. 0 .AND. NNT .NE. DT_VNOD_DIM_DIFFERE) GOTO 9901
      IF (NNT  .LE. 0 .AND. NNT .NE. DT_VNOD_DIM_DIFFERE) GOTO 9902
      IF (NNT  .LT. NNL) GOTO 9902

C---     Reset les données
      IERR = DT_VNOD_RST(HOBJ)

C---     Alloue l'espace (min de 1 pour qu'il existe)
      NNX = MAX(1, NNL)       ! NNL  peut être <= 0
      NVX = MAX(1, NNX*NVNO)  ! NVNO peut être = 0
      LVNOD = 0
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NVX, LVNOD)

C---     Initialise les données
      LVINI = 0
      IF (ERR_GOOD() .AND. DOINIT) THEN
D        CALL ERR_ASR(NVNO .GT. 0)
D        CALL ERR_ASR(NNX  .GT. 0)
         IERR = SO_ALLC_ALLRE8(NVNO, LVINI)
         CALL DCOPY(NVNO, VINI, 1, VA(SO_ALLC_REQVIND(VA, LVINI)), 1)
         IERR = DT_VNOD_ASGVAL_TRV(NVNO,
     &                             NNX,
     &                             VA(SO_ALLC_REQVIND(VA, LVINI)),
     &                             VA(SO_ALLC_REQVIND(VA, LVNOD)))
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - DT_VNOD_HBASE
         DT_VNOD_LVINI(IOB) = LVINI
         DT_VNOD_LVNOD(IOB) = LVNOD
         DT_VNOD_NNT  (IOB) = NNT
         DT_VNOD_NNL  (IOB) = NNL
         DT_VNOD_NVNO (IOB) = NVNO
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NVNO_INVALIDE',':', NVNO
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NNL_INVALIDE',':', NNL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A,I12)') 'ERR_NNT_INVALIDE',':', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_INIDIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise avec un fichier de données.
C
C Description:
C     La fonction <code>DT_VNOD_INIFIC(...)<code> initialise l'objet.
C     Elle supporte deux modes: avec une liste de fichiers ou
C     avec un seul fichier source des données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLIST       Liste de nom de fichiers de valeurs nodales
C     FICVNO      Nom du fichier de Valeurs NOdales
C     ISTAT       IO_LECTURE, mais pas IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_INIFIC(HOBJ, HLIST, FICVNO, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_INIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HLIST
      CHARACTER*(*) FICVNO
      INTEGER       ISTAT

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLCT
      INTEGER NNT, NNL, NVNO
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Contrôles des paramètres
      IF (.NOT. DS_LIST_HVALIDE(HLIST)) THEN
         IF (SP_STRN_LEN(FICVNO) .LE. 0) GOTO 9900
      ELSE
         IF (DS_LIST_REQDIM(HLIST) .LE. 0) GOTO 9901
      ENDIF

C---     RESET LES DONNEES
      IERR = DT_VNOD_RST(HOBJ)

C---     CONSTRUIS LE NOUVEAU FICHIER DE DONNEES
      HFLCT = 0
      IF (ERR_GOOD()) IERR = FD_PRNO_CTR(HFLCT)
      IF (ERR_GOOD()) IERR = FD_PRNO_INI(HFLCT,
     &                                   HLIST,
     &                                   FICVNO,
     &                                   ISTAT)

C---     DEMANDE LES NOUVELLES DIMENSIONS
      NNL = DT_VNOD_DIM_DIFFERE
      IF (ERR_GOOD()) NNT  = FD_PRNO_REQNNT (HFLCT)
      IF (ERR_GOOD()) NVNO = FD_PRNO_REQNPRN(HFLCT)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - DT_VNOD_HBASE
         DT_VNOD_HFLCT(IOB) = HFLCT
         DT_VNOD_NNT  (IOB) = NNT
         DT_VNOD_NNL  (IOB) = NNL
         DT_VNOD_NVNO (IOB) = NVNO
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_LISTE_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_INIFIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFLCT, HFECR
      INTEGER LVINI, LVNOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_VNOD_HBASE

C---     RECUPERE LES DONNEES
      HFLCT  = DT_VNOD_HFLCT(IOB)
      HFECR  = DT_VNOD_HFECR(IOB)
      LVINI  = DT_VNOD_LVINI(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)

C---     DESALLOUE LA MEMOIRE
      IF (LVINI .NE. 0) IERR = SO_ALLC_ALLRE8(0, LVINI)
      IF (LVNOD .NE. 0) IERR = SO_ALLC_ALLRE8(0, LVNOD)

C---     DETRUIS LES FICHIERS
      IF (FD_PRNO_HVALIDE(HFLCT)) IERR = FD_PRNO_DTR(HFLCT)
      IF (FD_PRNO_HVALIDE(HFECR)) IERR = FD_PRNO_DTR(HFECR)

C---     Efface les attributs
      IERR = DT_VNOD_RAZ(HOBJ)

      DT_VNOD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction DT_VNOD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C------------------------------------------------------------------------

      DT_VNOD_REQHBASE = DT_VNOD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction DT_VNOD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'dtvnod.fc'
C------------------------------------------------------------------------

      DT_VNOD_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  DT_VNOD_NOBJMAX,
     &                                  DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Imprime l'objet.
C
C Description:
C     La fonction DT_VNOD_PRN imprime l'objet dans le log.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C     Utilise les log par zones
C     Prototype
C************************************************************************
      FUNCTION DT_VNOD_PRN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_PRN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFLCT
      INTEGER LVNOD
      INTEGER I, NFIC
      CHARACTER*256 NOM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_VNOD_HBASE
      HFLCT = DT_VNOD_HFLCT(IOB)

C---     EN-TETE
      LOG_ZNE = 'h2d2.data.vnod'
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE (LOG_BUF, '(A)') 'MSG_VALEURS_NODALES'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DU HANDLE
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<35>#= ',
     &                        NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOM DES FICHIERS
      IF (FD_PRNO_HVALIDE(HFLCT)) THEN
         NFIC = DT_VNOD_REQNFIC(HOBJ)
         WRITE (LOG_BUF,'(A,I12)') 'MSG_NOMBRE_FICHIERS#<35>#= ',
     &                            NFIC
         CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
         IF (NFIC .GT. 0) THEN
            NOM = DT_VNOD_REQNOMF(HOBJ, 1)
            CALL SP_STRN_CLP(NOM, 50)
            WRITE (LOG_BUF,'(A,A)') 'MSG_FICHIER_DONNEES#<35>#= ',
     &                               NOM(1:SP_STRN_LEN(NOM))
            CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
            DO I=2,NFIC
               NOM = DT_VNOD_REQNOMF(HOBJ, I)
               CALL SP_STRN_CLP(NOM, 50)
               WRITE (LOG_BUF,'(A,A)') '#<35>#   ',
     &                               NOM(1:SP_STRN_LEN(NOM))
               CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
            ENDDO
         ENDIF
      ENDIF

C---     NOMBRE DE DIMENSIONS
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_VAL#<35>#= ',
     &                           DT_VNOD_NVNO(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOMBRE DE NOEUDS TOTAL
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS_TOTAL#<35>#= ',
     &                           DT_VNOD_NNT(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     NOMBRE DE NOEUDS LOCAL
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NBR_NOEUDS_LOCAL#<35>#= ',
     &                           DT_VNOD_NNL(IOB)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     LES SECTIONS
      IF (FD_PRNO_HVALIDE(HFLCT)) THEN
         WRITE (LOG_BUF,'(A,I12)')   'MSG_NBR_PAS_DE_TEMPS#<35>#= ',
     &                                FD_PRNO_REQNSCT(HFLCT)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TMIN#<35>#= ',
     &                              FD_PRNO_REQTMIN(HFLCT)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
         WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TMAX#<35>#= ',
     &                                FD_PRNO_REQTMAX(HFLCT)
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDIF

C---     TEMPS ACTUEL
      WRITE (LOG_BUF,'(A,F25.6)') 'MSG_TEMPS#<35>#= ',
     &                             DT_VNOD_TEMPS(IOB)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     CHARGÉ?
      WRITE (LOG_BUF,'(A,L12)') 'MSG_CHARGE#<35>#= ',
     &                           (DT_VNOD_LVNOD(IOB) .NE. 0)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)

C---     POINTEUR AUX DONNÉES
      WRITE (LOG_BUF,'(A,Z12)') 'MSG_LVNOD#<35>#= ',
     &                           DT_VNOD_LVNOD(IOB)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---     LES VALEURS
      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         LVNOD = DT_VNOD_LVNOD(IOB)
         IF (SO_ALLC_HEXIST(LVNOD))
     &      IERR = DT_VNOD_PRNVNO(DT_VNOD_NNL (IOB),
     &                            DT_VNOD_NVNO(IOB),
     &                            VA(SO_ALLC_REQVIND(VA, LVNOD)))
      ENDIF

      CALL LOG_DECIND()

      DT_VNOD_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Imprime les valeurs nodales.
C
C Description:
C     La fonction privée DT_VNOD_PRNVNO imprime les valeurs. Elle permet
C     de résoudre l'utilisation de la table VVNOD.
C
C Entrée:
C     NNL         Nombre de noeuds local
C     NVNO        Nombre de valeurs nodales
C     VVNOD       Table des valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_PRNVNO(NNL, NVNO, VVNOD)

      IMPLICIT NONE

      INTEGER DT_VNOD_PRNVNO
      INTEGER NNL
      INTEGER NVNO
      REAL*8  VVNOD(NVNO, NNL)

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IN, I
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL  .GT. 0  .OR. NNL .EQ. DT_VNOD_DIM_DIFFERE)
D     CALL ERR_PRE(NVNO .GT. 0  .OR. NNL .EQ. DT_VNOD_DIM_DIFFERE)
C------------------------------------------------------------------------

      WRITE(LOG_BUF,'(A)') 'MSG_VALEURS_LOCALES:'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      DO IN = 1,NNL
         WRITE(LOG_BUF, '(I12,1X,10(1PE14.6E3))')
     &         IN, (VVNOD(I,IN),I=1,NVNO)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

      DT_VNOD_PRNVNO = ERR_TYP()
      RETURN
      END


C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée <code>DF_VNOD_ASGVAL_TRV(...)<code> initialise
C     la table VVNO avec les valeurs de VVAL. Elle permet de
C     résoudre la table VVNO.
C
C Entrée:
C     NVNO        Nombre de valeurs
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Tableau des valeurs
C     VVNO        Tableau à initialiser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ASGVAL_TRV(NVNO, NNL, VVAL, VVNO)

      IMPLICIT NONE

      INTEGER NVNO
      INTEGER NNL
      REAL*8  VVAL(NVNO)
      REAL*8  VVNO(NVNO, NNL)

      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER I
!$    LOGICAL OMP_IN_PARALLEL
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NVNO .GT. 0)
D     CALL ERR_PRE(NNL  .GT. 0)
C------------------------------------------------------------------------

C---     Initialise les données
!$omp  parallel if(.NOT. OMP_IN_PARALLEL())
!$omp& default(shared)

!$omp  do
!$omp& private(I)
      DO I=1,NNL
         VVNO(1:NVNO, I) = VVAL(:)
      ENDDO
!$omp end do

!$omp end parallel

      DT_VNOD_ASGVAL_TRV = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Assigne un nouveau fichier de données.
C
C Description:
C     La fonction <code>DT_VNOD_ASGFICECR(...)<code> assigne à l'objet
C     le fichier d'écriture des données. On désalloue l'ancien fichier
C     s'il y en avait un.
C     La fonction supporte deux modes: le premier avec un nom de fichier,
C     auquel cas HFVNO doit être nul; le second avec un handle valide,
C     auquel cas FICVNO doit être vide. Si HFVNO est non nul, l'objet en
C     prend le contrôle.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     FICVNO      Nom du fichier de Valeurs NOdales
C     ISTAT       IO_ECRITURE ou IO_AJOUT
C     HFVNO       Handle sur le fichier d'écriture
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ASGFICECR(HOBJ, FICVNO, ISTAT, HFVNO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ASGFICECR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FICVNO
      INTEGER       ISTAT
      INTEGER       HFVNO

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFECR
D     INTEGER LVNO
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     LVNO = SP_STRN_LEN(FICVNO)
D     CALL ERR_ASR((LVNO .GT. 0 .AND. HFVNO .EQ. 0) .OR.
D    &             (LVNO .EQ. 0 .AND. FD_PRNO_HVALIDE(HFVNO)))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_VNOD_HBASE
      HFECR = DT_VNOD_HFECR(IOB)

C---     Détruis le fichier si il existe
      IF (FD_PRNO_HVALIDE(HFECR)) IERR = FD_PRNO_DTR(HFECR)
      HFECR = 0

C---     Avec handle
      IF (FD_PRNO_HVALIDE(HFVNO)) THEN
         IF (ERR_GOOD()) HFECR = HFVNO

C---     Construis le nouveau fichier de données
      ELSE
         IF (ERR_GOOD()) IERR=FD_PRNO_CTR(HFECR)
         IF (ERR_GOOD()) IERR=FD_PRNO_INI(HFECR,
     &                                    0,  ! HLIST
     &                                    FICVNO(1:SP_STRN_LEN(FICVNO)),
     &                                    ISTAT)
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         DT_VNOD_HFECR(IOB) = HFECR
      ENDIF

      DT_VNOD_ASGFICECR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_VNOD_CHARGE
C
C Description:
C     La fonction <code>DT_VNOD_CHARGE(...)<code> charge l'objet avec les
C     données au temps TNEW. Les données sont prises du fichier maintenu par
C     l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     TNEW        Temps pour lequel les données doivent être chargées
C
C Sortie:
C     MODIF       VRAI s'il y a eu modification
C
C Notes:
C     Si le vno est partagé, il y a un ping-pong entre les hnumr
C     des maillages porteurs ce qui provoque une relecture - inutile
C     mais sans effet pour des vno, mais destructive s'il s'agit de ddl.
C
C     Je peux ne pas avoir le même nombre de NNL avec des renum différentes
C     pour le même fichier.
C
C     Si la dim est assignée, alors elle doit être la même que celle de la
C     NUMR car il n'y a pas de redistribution
C
C   cas de figure:
C      même VNO: différentes maillages?
C
C      vno-ddl
C         z_sv2d   (vno) charge ==> asg hnumr sv2d
C         z_sed2d  (ddl) charge
C      vno-vno
C         vno1 charge
C
C      Pourquoi forcer la relecture?
C
C************************************************************************
      FUNCTION DT_VNOD_CHARGE (HOBJ, HNUMR, TNEW, MODIF)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  TNEW
      LOGICAL MODIF

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HNUMO, HFLCT
      INTEGER LVNOD, LVINI
      INTEGER NNL_L, NNT_L
      INTEGER NNL_R, NNT_R
      INTEGER NVNO
      REAL*8  TOLD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB  = HOBJ - DT_VNOD_HBASE
      HNUMO  = DT_VNOD_HNUMR(IOB)
      HFLCT  = DT_VNOD_HFLCT(IOB)
      LVINI  = DT_VNOD_LVINI(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
      NNT_L  = DT_VNOD_NNT  (IOB)
      NNL_L  = DT_VNOD_NNL  (IOB)
      TOLD   = DT_VNOD_TEMPS(IOB)
D     CALL ERR_ASR(NVNO  .GT. 0)
D     CALL ERR_ASR(NNL_L .GE. 0 .OR. NNL_L .EQ. DT_VNOD_DIM_DIFFERE)
D     CALL ERR_ASR(NNT_L .GT. 0)

C---     Récupère les données
      NNT_R = NM_NUMR_REQNNT(HNUMR)
      NNL_R = NM_NUMR_REQNNL(HNUMR)
D     CALL ERR_ASR(NNT_R .GT. 0)
D     CALL ERR_ASR(NNL_R .GE. 0)

C---     Contrôles
      IF (.NOT. FD_PRNO_HVALIDE(HFLCT)) THEN
         IF (HNUMO .NE. 0 .AND. HNUMO .NE. HNUMR) GOTO 9900
      ENDIF
      IF (NNL_L.NE.DT_VNOD_DIM_DIFFERE .AND. NNL_L.NE.NNL_R) GOTO 9901
      IF (NNT_L.NE.NNT_R) GOTO 9902

C---     Alloue l'espace
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLRE8(NNL_R*NVNO, LVNOD)
      IF (ERR_GOOD()) DT_VNOD_LVNOD(IOB) = LVNOD

C---     Lis les données
      MODIF = .FALSE.
      IF (FD_PRNO_HVALIDE(HFLCT)) THEN
         IF (HNUMO .NE. HNUMR) TOLD = -1.0D0    ! Force une relecture
         IERR = FD_PRNO_LIS(HFLCT,
     &                      HNUMR,
     &                      NNL_R,
     &                      NVNO,
     &                      VA(SO_ALLC_REQVIND(VA, LVNOD)),
     &                      TOLD,
     &                      TNEW,
     &                      MODIF)
      ELSE
         IF (LVINI .NE. 0 .AND. HNUMO .NE. HNUMR) THEN
            IERR = DT_VNOD_ASGVAL_TRV(NVNO,
     &                                NNL_R,
     &                                VA(SO_ALLC_REQVIND(VA, LVINI)),
     &                                VA(SO_ALLC_REQVIND(VA, LVNOD)))
            MODIF = .TRUE.
         ENDIF
      ENDIF

C---     Conserve le temps et la renumérotation
      IF (ERR_GOOD()) THEN
         DT_VNOD_TEMPS(IOB) = TNEW
         DT_VNOD_HNUMR(IOB) = HNUMR
         DT_VNOD_NNL  (IOB) = NNL_R
      ENDIF
      
      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMO, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMR, 'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NNL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_NNT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Met à jour une valeur nodale.
C
C Description:
C     La fonction DT_VNOD_ASG1VAL assigne à l'objet les valeurs passées
C     en argument. La table V doit avoir les mêmes dimensions que l'objet
C     et ses valeurs vont donc remplacer toutes celles de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     IC          Colonne source
C     NC          Première dimension de V
C     V(NC,*)     Tables des valeurs
C     IV          Colonne destination
C     TEMPS       Temps associé aux valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ASG1VAL(HOBJ, HNUMR, IC, NC, V, IV, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ASG1VAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER IC
      INTEGER NC
      REAL*8  V(NC, *)
      INTEGER IV
      REAL*8  TEMPS

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IVNO
      INTEGER HNUMO
      INTEGER NNL_L, NNT_L, NVNO
      INTEGER NNL_R, NNT_R
      INTEGER LVNOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC .GE. 1 .AND. IC .LE. NC)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_VNOD_HBASE
      HNUMO  = DT_VNOD_HNUMR(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NNL_L  = DT_VNOD_NNL  (IOB)
      NNT_L  = DT_VNOD_NNT  (IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
D     CALL ERR_ASR(NNL_L .GE. 0)
D     CALL ERR_ASR(NNT_L .GT. 0)
D     CALL ERR_ASR(NVNO  .NE. 0)
D     CALL ERR_ASR(LVNOD .NE. 0)

C---     Récupère les données
      NNL_R = NM_NUMR_REQNNL(HNUMR)
      NNT_R = NM_NUMR_REQNNT(HNUMR)
D     CALL ERR_ASR(NNL_R .GE. 0)
D     CALL ERR_ASR(NNT_R .GT. 0)

C---     Contrôles - Doit avoir été dimensionné
      IF (HNUMO .NE. HNUMR) GOTO 9900
      IF (NNL_L .NE. NNL_R) GOTO 9901
      IF (NNT_L .NE. NNT_R) GOTO 9902
      IF (IV .LT. 1 .OR. IV .GT. NVNO) GOTO 9903

C---     Transfert
      IVNO = IV - 1   ! comme offset
      CALL DCOPY(NNL_L,
     &           V(IC, 1), NC,
     &           VA(SO_ALLC_REQVIND(VA, LVNOD)+IVNO), NVNO)

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_VNOD_HNUMR(IOB) = HNUMR
         DT_VNOD_TEMPS(IOB) = TEMPS
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMO, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMR, 'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NNL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_NNT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(A)') 'ERR_INDICE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_INDICE#<35>#:', IC
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NVNO#<35>#:', NVNO
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_ASG1VAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Met à jour les valeurs.
C
C Description:
C     La fonction DT_VNOD_ASGVALS assigne à l'objet les valeurs passées
C     en argument. La table V doit avoir les mêmes dimensions que l'objet
C     et ses valeurs vont donc remplacer toutes celles de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     V           Tables des valeurs
C     TEMPS       Temps associé aux valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ASGVALS(HOBJ, HNUMR, V, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ASGVALS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      REAL*8  V(*)
      REAL*8  TEMPS

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HNUMO
      INTEGER NNL_L, NNT_L, NVNO
      INTEGER NNL_R, NNT_R
      INTEGER LVNOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_VNOD_HBASE
      HNUMO  = DT_VNOD_HNUMR(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NNL_L  = DT_VNOD_NNL  (IOB)
      NNT_L  = DT_VNOD_NNT  (IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
D     CALL ERR_ASR(NNL_L .GE. 0)
D     CALL ERR_ASR(NNT_L .GT. 0)
D     CALL ERR_ASR(NVNO  .NE. 0)
D     CALL ERR_ASR(LVNOD .NE. 0)

C---     Récupère les données
      NNL_R = NM_NUMR_REQNNL(HNUMR)
      NNT_R = NM_NUMR_REQNNT(HNUMR)
D     CALL ERR_ASR(NNL_R .GE. 0)
D     CALL ERR_ASR(NNT_R .GT. 0)

C---     Contrôles
      IF (HNUMO .NE. 0 .AND. HNUMO .NE. HNUMR) GOTO 9900
      IF (NNL_L .NE. 0 .AND. NNL_L .NE. NNL_R) GOTO 9901
      IF (NNT_L .NE. NNT_R) GOTO 9902

C---     Transfert
      CALL DCOPY(NNL_L*NVNO,
     &           V, 1,
     &           VA(SO_ALLC_REQVIND(VA, LVNOD)), 1)

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         DT_VNOD_HNUMR(IOB) = HNUMR
         DT_VNOD_TEMPS(IOB) = TEMPS
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMO, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMR, 'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NNL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_NNT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_ASGVALS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne une valeur nodale.
C
C Description:
C     La fonction DT_VNOD_REQ1VAL copie les valeurs de la colonne IV de
C     l'objet vers la colonne IC de la table V.
C     Elle retourne également le temps associé aux données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     IC          Colonne destination
C     NC          Première dimension de V
C     IV          Colonne source
C
C Sortie:
C     TEMPS       Temps associé aux valeurs
C     V(NC,*)     Tables des valeurs
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQ1VAL(HOBJ, HNUMR, IC, NC, V, IV, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQ1VAL
CDEC$ ENDIF

      IMPLICIT NONE 

      INTEGER, INTENT(IN)  :: HOBJ
      INTEGER, INTENT(IN)  :: HNUMR
      INTEGER, INTENT(IN)  :: IC
      INTEGER, INTENT(IN)  :: NC
      REAL*8,  INTENT(OUT) :: V(NC, *)
      INTEGER, INTENT(IN)  :: IV
      REAL*8,  INTENT(OUT) :: TEMPS

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IVNO
      INTEGER HNUMO
      INTEGER NNL_L, NNT_L, NVNO
      INTEGER NNL_R, NNT_R
      INTEGER LVNOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     CALL ERR_PRE(IC .GE. 1 .AND. IC .LE. NC)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_VNOD_HBASE
      HNUMO  = DT_VNOD_HNUMR(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NNL_L  = DT_VNOD_NNL  (IOB)
      NNT_L  = DT_VNOD_NNT  (IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
D     CALL ERR_ASR(NNL_L .GE. 0)
D     CALL ERR_ASR(NNT_L .GT. 0)
D     CALL ERR_ASR(NVNO  .NE. 0)
D     CALL ERR_ASR(LVNOD .NE. 0)

C---     Récupère les données
      NNL_R = NM_NUMR_REQNNL(HNUMR)
      NNT_R = NM_NUMR_REQNNT(HNUMR)
D     CALL ERR_ASR(NNL_R .GE. 0)
D     CALL ERR_ASR(NNT_R .GT. 0)

C---     Contrôles
      IF (HNUMO .NE. HNUMR) GOTO 9900
      IF (NNL_L .NE. NNL_R) GOTO 9901
      IF (NNT_L .NE. NNT_R) GOTO 9902
      IF (IV .LT. 1 .OR. IV .GT. NVNO) GOTO 9903

C---     Transfert
      IVNO = IV - 1   ! comme offset
      CALL DCOPY(NNL_L,
     &           VA(SO_ALLC_REQVIND(VA, LVNOD)+IVNO), NVNO,
     &           V(IC, 1), NC)

C---     Le temps
      IF (ERR_GOOD()) TEMPS = DT_VNOD_TEMPS(IOB)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMO, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMR, 'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NNL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_NNT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF,'(A)') 'ERR_INDICE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_INDICE#<35>#:', IC
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NVNO#<35>#:', NVNO
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_REQ1VAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne toutes les valeurs nodales.
C
C Description:
C     La fonction DT_VNOD_REQVALS copie toutes les valeurs de l'objet
C     vers la table V.
C     Elle retourne également le temps associé aux données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     V           Tables des valeurs
C     TEMPS       Temps associé aux valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQVALS(HOBJ, HNUMR, V, TEMPS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQVALS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER, INTENT(IN)  :: HOBJ
      INTEGER, INTENT(IN)  :: HNUMR
      REAL*8,  INTENT(OUT) :: V(*)
      REAL*8,  INTENT(OUT) :: TEMPS

      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HNUMO
      INTEGER NNL_L, NNT_L, NVNO
      INTEGER NNL_R, NNT_R
      INTEGER LVNOD
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - DT_VNOD_HBASE
      HNUMO  = DT_VNOD_HNUMR(IOB)
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NNL_L  = DT_VNOD_NNL  (IOB)
      NNT_L  = DT_VNOD_NNT  (IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
D     CALL ERR_ASR(NNL_L .GE. 0)
D     CALL ERR_ASR(NNT_L .GT. 0)
D     CALL ERR_ASR(NVNO  .NE. 0)
D     CALL ERR_ASR(LVNOD .NE. 0)

C---     Récupère les données
      NNL_R = NM_NUMR_REQNNL(HNUMR)
      NNT_R = NM_NUMR_REQNNT(HNUMR)
D     CALL ERR_ASR(NNL_R .GE. 0)
D     CALL ERR_ASR(NNT_R .GT. 0)

C---     Contrôles
      IF (HNUMO .NE. HNUMR) GOTO 9900
      IF (NNL_L .NE. NNL_R) GOTO 9901
      IF (NNT_L .NE. NNT_R) GOTO 9902

C---     Transfert
      CALL DCOPY(NNL_L*NVNO,
     &           VA(SO_ALLC_REQVIND(VA, LVNOD)), 1,
     &           V, 1)

C---     Le temps
      IF (ERR_GOOD()) TEMPS = DT_VNOD_TEMPS(IOB)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_RENUM_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HNUMO, 'MSG_RENUM')
      IERR = OB_OBJC_ERRH(HNUMR, 'MSG_RENUM')
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_NNL_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNL#<35>#:', NNL_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)') 'ERR_NNT_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(A,I12)') 'MSG_NNT#<35>#:', NNT_R
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_REQVALS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: DT_VNOD_SAUVE
C
C Description:
C     La fonction <code>DT_VNOD_SAUVE(...)<code> écris les données de l'objet
C     dans le fichier maintenu par l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_SAUVE(HOBJ, HNUMR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_SAUVE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HFECR
      INTEGER LVNOD
      INTEGER NNL, NNT
      INTEGER NVNO
      REAL*8  TSIM
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - DT_VNOD_HBASE

C---     RECUPERE LES DONNEES
      HFECR  = DT_VNOD_HFECR(IOB)
      IF (.NOT. FD_PRNO_HVALIDE(HFECR)) GOTO 9900
      LVNOD  = DT_VNOD_LVNOD(IOB)
      NNT    = DT_VNOD_NNT  (IOB)
      NNL    = DT_VNOD_NNL  (IOB)
      NVNO   = DT_VNOD_NVNO (IOB)
      TSIM   = DT_VNOD_TEMPS(IOB)
D     CALL ERR_ASR(NNT   .GE. NNL)
D     CALL ERR_ASR(NNL   .GE. 0)
D     CALL ERR_ASR(NVNO  .NE. 0)
      IF (LVNOD .EQ. 0) GOTO 9901

C---     ECRIS LES DONNEES
      IERR = FD_PRNO_ECRIS(HFECR,
     &                     HNUMR,
     &                     NNT,
     &                     NNL,
     &                     NVNO,
     &                     VA(SO_ALLC_REQVIND(VA, LVNOD)),
     &                     TSIM)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_FICHIER_SORTIE_NON_DEFINI'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(A)') 'ERR_DONNEES_NON_CHARGEES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      DT_VNOD_SAUVE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé.
C
C Description:
C     La fonction DT_VNOD_ESTINI retourne .TRUE. si l'objet a été
C     initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ESTINI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ESTINI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'

C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_ESTINI = DT_VNOD_ESTINIVAL(HOBJ) .OR.
     &                 DT_VNOD_ESTINIFIC(HOBJ)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé avec des données.
C
C Description:
C     La fonction DT_VNOD_ESTINIVAL retourne .TRUE. si l'objet a été
C     initialisé avec des données.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ESTINIVAL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ESTINIVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER HFLCT
      INTEGER LVINI
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLCT = DT_VNOD_HFLCT(HOBJ-DT_VNOD_HBASE)
      LVINI = DT_VNOD_LVINI(HOBJ-DT_VNOD_HBASE)
      DT_VNOD_ESTINIVAL = (.NOT. FD_PRNO_HVALIDE(HFLCT)) .AND.
     &                    (SO_ALLC_HEXIST(LVINI))
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si initialisé avec un fichier.
C
C Description:
C     La fonction DT_VNOD_ESTINIFIC retourne .TRUE. si l'objet a été
C     initialisé avec un fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_ESTINIFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_ESTINIFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER HFLCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLCT = DT_VNOD_HFLCT(HOBJ-DT_VNOD_HBASE)
      DT_VNOD_ESTINIFIC = FD_PRNO_HVALIDE(HFLCT)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le handle sur la renumérotation.
C
C Description:
C     La fonction DT_VNOD_REQHNUMR retourne le handle sur la renumérotation
C     de l'objet. Le handle peut être nul.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQHNUMR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQHNUMR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQHNUMR = DT_VNOD_HNUMR(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le pointeur aux données
C
C Description:
C     La fonction DT_VNOD_REQLVNO retourne le pointeur à la table des
C     valeurs nodales maintenues par l'objet. La table est de dimensions
C     (NVNO, NNL). Le pointeur peut être nul.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQLVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQLVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQLVNO = DT_VNOD_LVNOD(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le pointeur aux données d'initialisation
C
C Description:
C     La fonction DT_VNOD_REQLVINI retourne le pointeur à la table des
C     valeurs d'initialisation. La pointeur est nul si l'objet est initialisé
C     par fichier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQLVINI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQLVINI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQLVINI = DT_VNOD_LVINI(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeud Local
C
C Description:
C     La fonction DT_VNOD_REQNNL retourne le nombre de noeuds local
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQNNL(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQNNL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQNNL = DT_VNOD_NNL(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le Nombre de Noeud Total
C
C Description:
C     La fonction DT_VNOD_REQNNT retourne le nombre de noeuds total
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQNNT(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQNNT = DT_VNOD_NNT(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de valeurs nodales
C
C Description:
C     La fonction DT_VNOD_REQNVNO retourne le nombre de valeurs nodales
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQNVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQNVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQNVNO = DT_VNOD_NVNO(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le temps des données.
C
C Description:
C     La fonction DT_VNOD_REQTEMPS retourne le temps associé aux données
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQTEMPS(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQTEMPS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtvnod.fc'
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      DT_VNOD_REQTEMPS = DT_VNOD_TEMPS(HOBJ-DT_VNOD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le nombre de fichier de lecture.
C
C Description:
C     La fonction DT_VNOD_REQNFIC retourne le nombre de fichiers de lecture
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQNFIC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQNFIC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER HFLCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLCT = DT_VNOD_HFLCT(HOBJ-DT_VNOD_HBASE)
D     CALL ERR_ASR(FD_PRNO_HVALIDE(HFLCT))

      DT_VNOD_REQNFIC = FD_PRNO_REQNFIC(HFLCT)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne le NOM du fichier de lecture.
C
C Description:
C     La fonction DT_VNOD_REQNOMF retourne le nom du fichier de lecture
C     de l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION DT_VNOD_REQNOMF(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: DT_VNOD_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'dtvnod.fc'

      INTEGER HFLCT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HFLCT = DT_VNOD_HFLCT(HOBJ-DT_VNOD_HBASE)
D     CALL ERR_ASR(FD_PRNO_HVALIDE(HFLCT))

      DT_VNOD_REQNOMF = FD_PRNO_REQNOMF(HFLCT, I)
      RETURN
      END
