C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: FD_PRNA
C     INTEGER FD_PRN8_000
C     INTEGER FD_PRN8_999
C     INTEGER FD_PRN8_PKL
C     INTEGER FD_PRN8_UPK
C     INTEGER FD_PRN8_CTR
C     INTEGER FD_PRN8_DTR
C     INTEGER FD_PRN8_INI
C     INTEGER FD_PRN8_RST
C     INTEGER FD_PRN8_REQHBASE
C     LOGICAL FD_PRN8_HVALIDE
C     INTEGER FD_PRN8_ECRIS
C     INTEGER FD_PRN8_LIS
C     INTEGER FD_PRN8_LISVAL
C     INTEGER FD_PRN8_LISSCT
C     INTEGER FD_PRN8_INISTR
C
C Note:
C     Le fichier peut s'employer pour toute donnée réelle, stockée par
C     bloc pour chaque pas de temps, avec renumérotation; donc autant
C     pour des valeurs nodales que pour des valeurs élémentaires.
C     <p>
C     nlines ncols time
C     v_1 v_2 v_3 ... v_ncols
C************************************************************************

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction FD_PRN8_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprn8.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(FD_PRN8_NOBJMAX,
     &                   FD_PRN8_HBASE,
     &                   'Nodal Properties File')

      FD_PRN8_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction FD_PRN8_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprn8.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER  IERR
      EXTERNAL FD_PRN8_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(FD_PRN8_NOBJMAX,
     &                   FD_PRN8_HBASE,
     &                   FD_PRN8_DTR)

      FD_PRN8_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_PKL(HOBJ, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_PKL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HXML

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_PRN8_HBASE
      HPRNT = FD_PRN8_HPRNT(IOB)

C---     ECRIS LES DONNÉES
      IERR = IO_XML_WH_A(HXML, HPRNT)

      FD_PRN8_PKL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_UPK(HOBJ, LNEW, HXML)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_UPK
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      LOGICAL LNEW
      INTEGER HXML

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioxml.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère l'indice
      IOB  = HOBJ - FD_PRN8_HBASE

C---     Les données
      IERR = IO_XML_RH_A(HXML, FD_PRN8_HPRNT(IOB))

      FD_PRN8_UPK = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction FD_PRN8_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprn8.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   FD_PRN8_NOBJMAX,
     &                   FD_PRN8_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(FD_PRN8_HVALIDE(HOBJ))
         IOB = HOBJ - FD_PRN8_HBASE

         FD_PRN8_HPRNT (IOB) = 0
      ENDIF

      FD_PRN8_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction FD_PRN8_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprn8.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = FD_PRN8_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   FD_PRN8_NOBJMAX,
     &                   FD_PRN8_HBASE)

      FD_PRN8_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction FD_PRN8_INI initialise l'objet à l'aide des
C     paramètres. Elle complète l'initialisation du parent.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HPRNT       Handle sur le parent
C     HLIST       Handle sur la liste des noms de fichier
C     ISTAT       IO_LECTURE, IO_ECRITURE ou IO_AJOUT
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_INI(HOBJ, HPRNT, HLIST, ISTAT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPRNT
      INTEGER HLIST
      INTEGER ISTAT

      INCLUDE 'fdprn8.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
D     CALL ERR_PRE(FD_PRNO_HVALIDE(HPRNT))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C------------------------------------------------------------------------

C---     DEMARRE LE CHRONO
      CALL TR_CHRN_START('h2d2.io.prno.prec8')

C---     Contrôle les paramètres
      IF (DS_LIST_REQDIM(HLIST) .LE. 0) GOTO 9900
      IF (.NOT. IO_UTIL_MODVALIDE(ISTAT)) GOTO 9901
      IF (.NOT. IO_UTIL_MODACTIF(ISTAT, IO_MODE_PREC_08)) GOTO 9901

C---     Reset les données
      IERR = FD_PRN8_RST(HOBJ)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - FD_PRN8_HBASE
         FD_PRN8_HPRNT(IOB) = HPRNT
      ENDIF

C---     En lecture, initialise la structure interne
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) THEN
         IF (ERR_GOOD()) IERR = FD_PRN8_INISTR(HOBJ, HLIST)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I3)') 'ERR_MODE_ACCES_INVALIDE',': ', ISTAT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.prec8')
      FD_PRN8_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C     La fonction FD_PRN8_RST reset l'objet, donc le remet dans
C     un état valide non initialisé.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE L'INDICE
      IOB  = HOBJ - FD_PRN8_HBASE

C---     RESET LES ATTRIBUTS
      FD_PRN8_HPRNT (IOB) = 0

      FD_PRN8_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction FD_PRN8_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'fdprn8.fi'
      INCLUDE 'fdprn8.fc'
C------------------------------------------------------------------------

      FD_PRN8_REQHBASE = FD_PRN8_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction FD_PRN8_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'fdprn8.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'fdprn8.fc'
C------------------------------------------------------------------------

      FD_PRN8_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  FD_PRN8_NOBJMAX,
     &                                  FD_PRN8_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRN8_GETMIN
C
C Description:
C     La fonction statique prive FD_PRN8_GETMIN est
C
C Entrée:
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C
C Sortie:
C     VMIN
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_GETMIN(NNL, NPRN, VPRN, VMIN)

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NPRN
      REAL*8  VPRN(NPRN, NNL)
      REAL*8  VMIN(NPRN)

      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER ID, I

      INTEGER IDMIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NPRN .LE. FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------

      DO ID=1, NPRN
         I = IDMIN(NNL, VPRN(ID,1), NPRN)
         VMIN(ID) = VPRN(ID, I)
      ENDDO

      FD_PRN8_GETMIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRN8_GETMAX
C
C Description:
C     La fonction statique prive FD_PRN8_GETMAX est
C
C Entrée:
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C
C Sortie:
C     VMAX
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_GETMAX(NNL, NPRN, VPRN, VMAX)

      IMPLICIT NONE

      INTEGER NNL
      INTEGER NPRN
      REAL*8  VPRN(NPRN, NNL)
      REAL*8  VMAX(NPRN)

      INCLUDE 'err.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER ID, I

      INTEGER IDMAX
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NPRN .LE. FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------

      DO ID=1, NPRN
         I = IDMAX(NNL, VPRN(ID,1), NPRN)
         VMAX(ID) = VPRN(ID, I)
      ENDDO

      FD_PRN8_GETMAX = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRN8_ECRSCT
C
C Description:
C     La fonction FD_PRN8_ECRSCT est la fonction principale d'écriture
C     d'un fichier de type ProprietesNodales.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HNUMR       Handle sur la renumérotation
C     NNTL        Nombre de noeuds local
C     NPRN        Nombre de propriétés nodales
C     VPRNO       Vecteur des propriétés nodales
C     TSIM        Temps des valeurs
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_ECRSCT(HOBJ,
     &                        FNAME,
     &                        ISTAT,
     &                        HNUMR,
     &                        NNT,
     &                        NNL,
     &                        NPRN,
     &                        VPRNO,
     &                        TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_ECRSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME
      INTEGER       ISTAT
      INTEGER       HNUMR
      INTEGER       NNT
      INTEGER       NNL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNL)
      REAL*8        TSIM

      INCLUDE 'fdprn8.fi'
      INCLUDE 'ioi08.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprn8.fc'

      INTEGER I_RANK, I_SIZE, I_ERROR, I_TAG, I_COMM
      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER HFIC
      INTEGER IN, INL, ID, IR, IRSND

      REAL*8  VMIN (FD_PRN8_MAXCOL)
      REAL*8  VMAX (FD_PRN8_MAXCOL)
      REAL*8  VMING(FD_PRN8_MAXCOL)
      REAL*8  VMAXG(FD_PRN8_MAXCOL)
      REAL*8  VTMP (FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NNT  .GE. NNL)
D     CALL ERR_PRE(NNL  .GE. 0)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(TSIM .GE. 0)
D     CALL ERR_PRE(NPRN .LE. FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.prec8.write')

C---     Récupère les attributs
      IOB = HOBJ - FD_PRN8_HBASE

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Param du fichier
      IERR = FD_PRN8_GETMIN(NNL, NPRN, VPRNO, VMIN)
      IERR = FD_PRN8_GETMAX(NNL, NPRN, VPRNO, VMAX)
      CALL MPI_REDUCE(VMIN, VMING, NPRN, MP_TYPE_RE8(),
     &                MPI_MIN, I_MASTER, I_COMM, I_ERROR)
      CALL MPI_REDUCE(VMAX, VMAXG, NPRN, MP_TYPE_RE8(),
     &                MPI_MAX, I_MASTER, I_COMM, I_ERROR)

C---     Ouvre le fichier
      IERR = 0
      HFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_CTR (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_INI (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_OPEN(HFIC,
     &                                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                                      ISTAT)
      ENDIF

C---     Écris l'entête
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_WHDR(HFIC, NNT, NPRN, TSIM,
     &                                      VMING, VMAXG)
      ENDIF

C---     Sync. le code d'erreur
      IERR = MP_UTIL_SYNCERR()
      IF (ERR_BAD()) GOTO 9999

C---     Si on a un seul process
      IF (I_SIZE .GT. 1) GOTO 1000
      DO IN=1,NNT
         INL = NM_NUMR_REQNINT(HNUMR, IN)
         IERR = IO_I08_WD_N(HFIC, NPRN, VPRNO(1,INL), 1)
         IERR = IO_I08_ENDL(HFIC)
      ENDDO
      GOTO 1999

C---     Ecris la séquence des propriétés nodales
1000  CONTINUE
      I_TAG = 12
!!      do ib=1,nnt/1024
!!         nmin = (ib-1)*1024 + 1
!!         nmax = min(nnt, ib*1024)
!!
!!C---        Cherche les process propriétaires
!!         call iinit(1024, -1, kir)
!!         do in-nmin, nmax
!!            inl = nm_numr_reqnint(hnumr, in)
!!            if (inl .gt. 0) then
!!               if (nm_numr_estnopp(hnumr, inl)) kir(in) = i_rank
!!            endif
!!         enddo
!!         call mpi_allreduce(kir, kirsnd, 1024, mpi_integer, mpi_max,
!!     &                      i_comm, i_error)
!!         if (err_bad()) goto 1999

      DO IN=1,NNT
         INL = NM_NUMR_REQNINT(HNUMR, IN)
         IR  = -1
         IF (INL .GT. 0) THEN
            IF (NM_NUMR_ESTNOPP(HNUMR, INL)) IR = I_RANK
         ENDIF
         CALL MPI_ALLREDUCE(IR, IRSND, 1, MP_TYPE_INT(), MPI_MAX,
     &                      I_COMM, I_ERROR)
         IF (ERR_BAD()) GOTO 1999

         IF (I_RANK .EQ. I_MASTER) THEN
            IF (IRSND .EQ. I_RANK) THEN
D              CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
               CALL DCOPY(NPRN, VPRNO(1,INL), 1, VTMP, 1)
            ELSE
               CALL MPI_RECV(VTMP, NPRN, MP_TYPE_RE8(),
     &                       IRSND,
     &                       I_TAG, I_COMM, I_STATUS,I_ERROR)
            ENDIF
            IF (ERR_GOOD()) IERR = IO_I08_WD_N(HFIC, NPRN, VTMP, 1)
         ELSEIF (IRSND .EQ. I_RANK) THEN
D           CALL ERR_ASR(INL .GT. 0 .AND. INL .LE. NNL)
            CALL MPI_SEND(VPRNO(1,INL), NPRN, MP_TYPE_RE8(),
     &                    I_MASTER,
     &                    I_TAG, I_COMM, I_ERROR)
         ENDIF

C---        BROADCAST LE CODE D'ERREUR A CAUSE DU WRITE
         IERR = MP_UTIL_SYNCERR()
         IF (ERR_BAD()) GOTO 1999
      ENDDO

C---     FERME LE FICHIER
1999  CONTINUE
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (IO_I08_HVALIDE(HFIC)) THEN
            IERR = IO_I08_CLOSE(HFIC)
            IERR = IO_I08_DTR  (HFIC)
         ENDIF
      ENDIF

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.prec8.write')

      FD_PRN8_ECRSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: FD_PRN8_LISSCT
C
C Description:
C     La fonction privée FD_PRN8_LISSCT lis une section d'un fichier
C     de type ProprietesNodales.
C
C Entrée:
C     XFIC     Handle sur le fichier
C     HNUMR    Handle sur la renumérotation
C     NNTL     Nombre de Noeuds Total Local au process
C     NPRN     Nombre de propriétés
C
C Sortie:
C     VPRNO    Table des valeurs lues
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_LISSCT(HOBJ,
     &                        FNAME,
     &                        XPOS,
     &                        TEMPS,
     &                        HNUMR,
     &                        NNTL,
     &                        NPRN,
     &                        VPRNO)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: FD_PRN8_LISSCT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FNAME     ! Nom du fichier
      INTEGER*8     XPOS      ! Offset dans le fichier
      REAL*8        TEMPS     ! Pour contrôles
      INTEGER       HNUMR
      INTEGER       NNTL
      INTEGER       NPRN
      REAL*8        VPRNO(NPRN, NNTL)

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER I_COMM, I_RANK, I_SIZE, I_ERROR
      INTEGER I_MASTER
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IN, ID, INL, NNL
      INTEGER HFIC
      INTEGER NNTG, NNTL_SUM
      INTEGER NPRN_TMP
      CHARACTER*(1024) BUF
!      REAL*8  VTMP(FD_PRN8_MAXCOL)
      REAL*8  VMIN (FD_PRN8_MAXCOL)
      REAL*8  VMAX (FD_PRN8_MAXCOL)
      REAL*8  VDEL (FD_PRN8_MAXCOL)
      REAL*8  T
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
D     CALL ERR_PRE(NNTL .GT. 1)
D     CALL ERR_PRE(NPRN .GT. 0)
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NPRN .LE. FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Démarre le chrono
      CALL TR_CHRN_START('h2d2.io.prno.prec8.read')

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      CALL MPI_COMM_SIZE(I_COMM, I_SIZE, I_ERROR)

C---     Ouvre le fichier
      IERR = 0
      HFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_CTR (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_INI (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_OPEN(HFIC,
     &                                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                                      IO_MODE_LECTURE)
      ENDIF

C---     Positionne dans le fichier
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_SETPOS(HFIC, XPOS)
      ENDIF

C---     Lis l'entête
      NNTG = -1
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_RHDR(HFIC, NNTG, NPRN_TMP, T,
     &                                      VMIN, VMAX, VDEL)
      ENDIF

C---     Sync. le code d'erreur
      IERR = MP_UTIL_SYNCERR()
      IF (ERR_BAD()) GOTO 9999

C---     BROADCAST THE DATA
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NNTG, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
      ENDIF

C---     CONTROLE LA COHERANCE DES DIMENSIONS LOCALES ET GLOBALES
      IF (ERR_GOOD()) THEN
         IF (I_SIZE .LE. 1) THEN       ! 1 PROC ==> NNTL = NNTG
            IF (NNTG .NE. NNTL) GOTO 9906
         ELSE                          ! n PROC ==> SUM(NNTL) > NNTG
            CALL MPI_ALLREDUCE(NNTL, NNTL_SUM, 1, MP_TYPE_INT(),
     &                         MPI_SUM, I_COMM, I_ERROR)
            IF (ERR_GOOD() .AND. NNTL_SUM .LE. NNTG) GOTO 9907
         ENDIF
      ENDIF
      IF (ERR_BAD()) GOTO 9988

!C---     LIS LES LIGNES
!      NNL = 0
!      DO IN=1,NNTG
!         IF (ERR_BAD()) GOTO 299
!
!         IF (I_RANK .EQ. I_MASTER) THEN
!            IERR = C_FA_LISLN(XFIC, BUF)
!            IF (IERR .NE. 0) GOTO 208
!            READ(BUF,*, ERR=208, END=209) (VTMP(ID),ID=1,NPRN)
!            IERR = 0
!            GOTO 219
!208         CONTINUE ! ERR
!            IERR = -1
!            GOTO 219
!209         CONTINUE ! END
!            IERR = -2
!            GOTO 219
!219         CONTINUE
!         ENDIF
!
!C---        BROADCAST THE ERROR CODE
!         CALL MPI_BCAST(IERR, 1, MP_TYPE_INT(), I_MASTER, I_COMM, I_ERROR)
!         IF (IERR .EQ. -1) GOTO 9902
!         IF (IERR .EQ. -2) GOTO 9903
!D        CALL ERR_ASR(IERR .EQ. 0)
!
!C---        BROADCAST THE DATA
!         CALL MPI_BCAST(VTMP, NPRN, MP_TYPE_RE8(),
!     &                  I_MASTER, I_COMM, I_ERROR)
!
!C---        CONSERVE SI CONCERNÉ
!         IF (ERR_GOOD()) THEN
!            INL = NM_NUMR_REQNINT(HNUMR, IN)
!            IF (INL .GT. 0) THEN
!               CALL DCOPY(NPRN, VTMP, 1, VPRNO(1,INL), 1)
!               NNL = NNL + 1
!            ENDIF
!         ENDIF
!
!      ENDDO
!
!299   CONTINUE
!      IF (NNL .NE. NNTL) GOTO 9908

C---     FERME LE FICHIER
      IF (IO_I08_HVALIDE(HFIC)) THEN
         IERR = IO_I08_CLOSE(HFIC)
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(A)') 'ERR_OUVERTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_POSITION_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A)') 'ERR_LECTURE_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_FIN_FICHIER'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9904  WRITE(ERR_BUF,'(A,2(A,I6))') 'ERR_NPRN_INCOHERENT', ': ',
     &                       NPRN_TMP, ' / ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF,'(A,2(A,1PE14.6E3))') 'ERR_TEMPS_INCOHERENT', ': ',
     &                       T, ' / ', TEMPS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NNL_GE_NNT',': ',
     &      NNTL, ' / ', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_SUM(NNL)_LE_NNT',': ',
     &      NNTL_SUM, ' / ', NNTG
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9908  WRITE(ERR_BUF,'(2A,I12,A,I12)') 'ERR_NBR_VALEURS_LUES',': ',
     &      NNL, ' / ', NNTL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      WRITE(ERR_BUF,'(2A,F25.6)') 'MSG_TEMPS', ': ', TEMPS
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I25)') 'MSG_OFFSET', ': ', XPOS
      CALL ERR_AJT(ERR_BUF)
      CALL SP_STRN_CLP(BUF, 80)
      WRITE(ERR_BUF,'(2A,A)') 'MSG_BUFFER', ': ',
     &      BUF(1:SP_STRN_LEN(BUF))
      CALL ERR_AJT(ERR_BUF)

9999  CONTINUE
      CALL TR_CHRN_STOP('h2d2.io.prno.prec8.read')
      IERR = MP_UTIL_SYNCERR()
      FD_PRN8_LISSCT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_PRN8_INISTR initialise la structure interne à
C     partir de la liste des fichiers passée en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HLIST       Handle sur la liste des fichiers
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION FD_PRN8_INISTR(HOBJ, HLIST)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLIST

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprn8.fc'

      INTEGER IERR
      INTEGER IOB, IOP
      INTEGER I
      INTEGER HPRNT
      INTEGER NPRN, NNT
      INTEGER NPRN_L, NNT_L
      REAL*8  TMAX
      CHARACTER*256 FNAME
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
D     CALL ERR_PRE(DS_LIST_HVALIDE(HLIST))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRN8_HBASE
      HPRNT = FD_PRN8_HPRNT(IOB)

C---     Récupère les attributs du parent
      IOP = HPRNT - FD_PRNO_HBASE
      NNT_L  = FD_PRNO_NNT  (IOP)
      NPRN_L = FD_PRNO_NPRN (IOP)

C---     Lis les dimensions du premier fichier
      NPRN = 0
      NNT  = 0
      IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, 1, FNAME)
      IF (ERR_GOOD()) IERR = FD_PRN8_REQDIM(HOBJ, NPRN, NNT, FNAME)
      IF (ERR_GOOD() .AND. NPRN .LE. 0) GOTO 9900
      IF (ERR_GOOD() .AND. NNT  .LE. 0) GOTO 9901
D     IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NNT_L  .EQ. 0 .OR. NNT_L  .EQ. NNT)
D        CALL ERR_ASR(NPRN_L .EQ. 0 .OR. NPRN_L .EQ. NPRN)
D     ENDIF
      IF (NNT_L  .EQ. 0) NNT_L  = NNT
      IF (NPRN_L .EQ. 0) NPRN_L = NPRN

C---     Boucle sur les fichiers
      TMAX = -1.0D99
      DO I=1,DS_LIST_REQDIM(HLIST)
         NPRN = 0
         NNT  = 0
         IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLIST, I, FNAME)
         IF (ERR_GOOD()) IERR = FD_PRN8_REQDIM(HOBJ, NPRN, NNT, FNAME)
         IF (ERR_GOOD() .AND. NNT  .NE. NNT_L)  GOTO 9902
         IF (ERR_GOOD() .AND. NPRN .NE. NPRN_L) GOTO 9902
         IF (ERR_GOOD())
     &      IERR = FD_PRN8_INI1FIC(HOBJ,
     &                             NPRN,
     &                             NNT,
     &                             TMAX,
     &                             I,
     &                             FNAME(1:SP_STRN_LEN(FNAME)))
      ENDDO

C---     Conserve les attributs
      IF (ERR_GOOD()) THEN
         FD_PRNO_NNT (IOP) = NNT      ! Parent
         FD_PRNO_NPRN(IOP) = NPRN
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NBR_PRNO_INVALIDE', ' : ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNT_INVALIDE', ' : ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_FICHIER_INCOMPATIBLE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NNT', NNT, '/', NNT_L
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,I12,A,I12)') 'MSG_NPRN', NPRN, '/', NPRN_L
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

C---     Complète le message d'erreur
9999  CONTINUE
      IF (ERR_BAD()) THEN
         WRITE(ERR_BUF, '(A)') FNAME(1:SP_STRN_LEN(FNAME))
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      FD_PRN8_INISTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise la structure
C
C Description:
C     La fonction privée FD_PRN8_INISTR initialise la structure interne à
C     partir du fichier dont le nom est passé en argument.
C     Cette structure est passée à tous les process enfants même si ceux-ci
C     n'en n'ont pas a-priori besoin.
C
C Entrée:
C     HOBJ                    Handle sur l'objet
C     FNAME                  Nom du fichier
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION FD_PRN8_INI1FIC(HOBJ,
     &                         NPRN,
     &                         NNT,
     &                         TMAX,
     &                         IFIC,
     &                         FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRN
      INTEGER NNT
      REAL*8  TMAX
      INTEGER IFIC
      CHARACTER*(*) FNAME

      INCLUDE 'fdprn8.fi'
      INCLUDE 'err.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioi08.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprno.fc'
      INCLUDE 'fdprn8.fc'

      INTEGER   I_RANK, I_ERROR, I_COMM
      INTEGER   I_MASTER
      PARAMETER (I_MASTER = 0)

      REAL*8    T_L
      REAL*8    VMIN (FD_PRN8_MAXCOL)
      REAL*8    VMAX (FD_PRN8_MAXCOL)
      REAL*8    VDEL (FD_PRN8_MAXCOL)
      INTEGER*8 XPOS
      INTEGER   HPRNT, HFIC
      INTEGER   I
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   ILGN
      INTEGER   LB
      INTEGER   NBLCS
      INTEGER   NNT_L, NPRN_L
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
D     CALL ERR_PRE(NPRN .LE. FD_PRN8_MAXCOL)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      IOB = HOBJ - FD_PRN8_HBASE
      HPRNT = FD_PRN8_HPRNT(IOB)

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Ouvre le fichier
      ILGN = 0
      HFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IF (ERR_GOOD()) IERR = IO_I08_CTR (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_INI (HFIC)
         IF (ERR_GOOD()) IERR = IO_I08_OPEN(HFIC,
     &                                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                                      IO_MODE_LECTURE)
      ENDIF
C---     Sync les erreurs
      IERR = MP_UTIL_SYNCERR()
      IF (ERR_BAD()) GOTO 9999

C---     WHILE (.NOT. ERR_BAD() .OR. IERR)
      NBLCS = 0
100   CONTINUE
         IF (ERR_BAD()) GOTO 199

C---        Lis l'entête de section
         IF (I_RANK .EQ. I_MASTER) THEN
            IF (ERR_GOOD()) IERR = IO_I08_GETPOS(HFIC, XPOS)
            IF (ERR_GOOD()) IERR = IO_I08_RHDR(HFIC, NNT_L, NPRN_L, T_L,
     &                                         VMIN, VMAX, VDEL)
         ENDIF

C---        Sync les erreurs
         IERR = MP_UTIL_SYNCERR()
         IF (ERR_ESTMSG('ERR_FIN_FICHIER')) THEN
            CALL ERR_RESET()
            GOTO 199
         ENDIF
         IF (ERR_BAD()) GOTO 9999

C---        Broadcast the data
         CALL MPI_BCAST(NNT_L, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(NPRN_L,1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(XPOS,  1,MP_TYPE_IN8(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(T_L,   1,MP_TYPE_RE8(),I_MASTER,I_COMM,I_ERROR)
         IF (ERR_BAD()) GOTO 9999

C---        Contrôles
         IF (NNT_L  .NE. NNT)  GOTO 9906
         IF (NPRN_L .NE. NPRN) GOTO 9906
         IF (T_L    .LE. TMAX) GOTO 9905
         ILGN = ILGN + NPRN+1

C---        Assigne les param de section
         IERR = FD_PRNO_ASGSCT(HPRNT, IFIC, T_L, XPOS)

C---        Saute le bloc
         IF (I_RANK .EQ. I_MASTER) THEN
            IERR = IO_I08_SKIP(HFIC, NNT)
            ILGN = ILGN + NNT
         ENDIF
         IERR = MP_UTIL_SYNCERR()
         IF (ERR_BAD()) GOTO 9999

         NBLCS = NBLCS + 1
      GOTO 100
199   CONTINUE

      IF (ERR_GOOD() .AND. NBLCS .LE. 0) GOTO 9906

      GOTO 9989
C-----------------------------------------------------------------------
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNT_INVALIDE', ': ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_PRNO_INVALIDE', ': ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(A)') 'ERR_TEMPS_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2(A,1PE14.6E3))') 'MSG_TEMPS:',T_L,'<=',TMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9906  WRITE(ERR_BUF, '(A)') 'ERR_ENTETE_BLOC_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF, '(3A)') 'ERR_STRUCT_FICHIER_INVALIDE',': ',
     &                        FNAME(1:SP_STRN_LEN(FNAME))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      IF (ILGN .GT. 0) THEN
         WRITE(ERR_BUF, '(2A,I6,A)') FNAME(1:SP_STRN_LEN(FNAME)),
     &                               '(', ILGN, ')'
         CALL SP_STRN_DLC(ERR_BUF, ' ')
         CALL ERR_AJT(ERR_BUF)
      ENDIF
      GOTO 9989

9989  CONTINUE
      IF (IO_I08_HVALIDE(HFIC)) IERR = IO_I08_CLOSE(HFIC)
      GOTO 9999

9999  CONTINUE
      FD_PRN8_INI1FIC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne les dimensions du fichier.
C
C Description:
C     La fonction privée FD_PRN8_REQDIM lis l'entête d'un fichier
C     et en retourne les dimensions.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     FNAME         Nom du fichier
C
C Sortie:
C     NPRN
C     NNT
C
C Notes:
C
C************************************************************************
      FUNCTION FD_PRN8_REQDIM(HOBJ, NPRN, NNT, FNAME)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPRN
      INTEGER NNT
      CHARACTER*(*) FNAME

      INCLUDE 'fdprn8.fi'
      INCLUDE 'c_fa.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioi08.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'fdprn8.fc'

      INTEGER   I_RANK, I_ERROR, I_COMM
      INTEGER   I_MASTER
      PARAMETER (I_MASTER = 0)

      REAL*8    TEMPS
      REAL*8    VMIN (FD_PRN8_MAXCOL)
      REAL*8    VMAX (FD_PRN8_MAXCOL)
      REAL*8    VDEL (FD_PRN8_MAXCOL)
      INTEGER   IERR
      INTEGER   IOB
      INTEGER   HFIC
      INTEGER   NNT_REF, NPRN_REF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(FD_PRN8_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère l'indice
      IOB = HOBJ - FD_PRN8_HBASE

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)

C---     Initialise
      NNT  = 0
      NPRN = 0

C---     Ouvre le fichier
      HFIC = 0
      IF (I_RANK .EQ. I_MASTER) THEN
         IERR = IO_I08_CTR (HFIC)
         IERR = IO_I08_INI (HFIC)
         IERR = IO_I08_OPEN(HFIC,
     &                      FNAME(1:SP_STRN_LEN(FNAME)),
     &                      IO_MODE_LECTURE)
      ENDIF
C---     Sync les erreurs
      IERR = MP_UTIL_SYNCERR()

C---     Lis l'entête de section
      IF (ERR_GOOD() .AND. I_RANK .EQ. I_MASTER) THEN
         IERR = IO_I08_RHDR(HFIC, NNT, NPRN, TEMPS,
     &                      VMIN, VMAX, VDEL)
      ENDIF

C---     Sync les erreurs
      IERR = MP_UTIL_SYNCERR()

C---        Broadcast the data
      IF (ERR_GOOD()) THEN
         CALL MPI_BCAST(NNT,  1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         CALL MPI_BCAST(NPRN, 1,MP_TYPE_INT(),I_MASTER,I_COMM,I_ERROR)
         IF (ERR_BAD()) GOTO 9999
      ENDIF

C---     Contrôles
      IF (ERR_GOOD()) THEN
         IF (NNT  .LE. 0) GOTO 9900
         IF (NPRN .LE. 0) GOTO 9901
         IF (NPRN .GT. FD_PRN8_MAXCOL) GOTO 9901
      ENDIF

      GOTO 9999
C-----------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NNT_INVALIDE', ': ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_PRNO_INVALIDE', ': ', NPRN
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IF (IO_I08_HVALIDE(HFIC)) THEN
         CALL ERR_PUSH()
         IERR = IO_I08_CLOSE(HFIC)
         CALL ERR_POP()
      ENDIF

      FD_PRN8_REQDIM = ERR_TYP()
      RETURN
      END
