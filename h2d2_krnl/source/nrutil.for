C************************************************************************
C --- Copyright (c) INRS 2011-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Norme
C Objet:   ----
C Type:    Fonctions utilitaires
C
C Nomenclature:
C     N        Norme
C     [2,I]    L2 ou Infinie
C     [G,S]    Global ou Single DDL
C     [R,D]    Relative ou Directe
C************************************************************************

C************************************************************************
C Sommaire: Calcule la norme L2, Single, Directe.
C
C Description:
C     La fonction <code>NR_UTIL_N2SD(...)</code> calcule
C     la Norme L2 Single (1 DDL) Directe.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table des VALeurs
C     IDDL        Indice de DDL pour le calcul
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_N2SD(HNUMR, NDLN, NNL, VVAL, IDDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_N2SD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN,NNL)
      INTEGER IDDL

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN
      REAL*8  T_L(2), T_G(2)
      REAL*8  DU
      REAL*8  VNRM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      T_L(1) = 0.0D0
      T_L(2) = 0.0D0
      DO IN=1,NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DU = VVAL(IDDL,IN)
            T_L(1) = T_L(1) + DU*DU
            T_L(2) = T_L(2) + 1.0D0
         ENDIF
      ENDDO

C---     Synchronise
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 2, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Calcule la norme
      VNRM = SQRT(T_G(1) / NINT(T_G(2)))

      NR_UTIL_N2SD = VNRM
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme L2, Single, Relative.
C
C Description:
C     La fonction <code>NR_UTIL_N2SR(...)</code> calcule
C     la Norme L2 Single (1 DDL) Relative.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table DELta des accroissements
C     IDDL        Indice de DDL pour le calcul
C     VREL        Table de Degré de Liberté Globaux
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_N2SR(HNUMR, NDLN, NNL, VVAL,
     &                      IDDL, VREL, EPSA, EPSR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_N2SR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN, NNL)
      INTEGER IDDL
      REAL*8  VREL(NDLN, NNL)
      REAL*8  EPSA
      REAL*8  EPSR

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN, NNT, NNPP
      REAL*8  DU, U, DR
      REAL*8  T_L(2), T_G(2)
      REAL*8  VNRM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      T_L(1) = 0.0D0
      T_L(2) = 0.0D0
      DO IN=1,NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DU = VVAL(IDDL, IN)
            U  = VREL(IDDL, IN)
            DR = DU / (ABS(U)*EPSR + EPSA)
            T_L(1) = T_L(1) + DR*DR
            T_L(2) = T_L(2) + 1.0D0
         ENDIF
      ENDDO

C---     SYNCHRONISE
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 2, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Calcule la norme
      VNRM = SQRT(T_G(1) / NINT(T_G(2)))

      NR_UTIL_N2SR = VNRM
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme L2, Global, Directe.
C
C Description:
C     La fonction <code>NR_UTIL_N2GD(...)</code> calcule
C     la Norme L2 Globale (tous les DDL) Directe.
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VREL        Table de Degré de Liberté Globaux
C     VVAL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_N2GD(HNUMR, NDLN, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_N2GD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN,NNL)

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN, ID
      REAL*8  T_L(2), T_G(2)
      REAL*8  DU
      REAL*8  VNRM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      T_L(1) = 0.0D0
      T_L(2) = 0.0D0
      DO IN=1,NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DO ID=1,NDLN
               DU = VVAL(ID,IN)
               T_L(1) = T_L(1) + DU*DU
               T_L(2) = T_L(2) + 1.0D0
            ENDDO
         ENDIF
      ENDDO

C---     Synchronise
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 2, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Calcule la norme
      VNRM = SQRT(T_G(1) / NINT(T_G(2)))

      NR_UTIL_N2GD = VNRM
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme L2, Globale, Relative.
C
C Description:
C     La fonction <code>NR_UTIL_N2GR(...)</code> calcule
C     la Norme L2 Globale (tous les DDL) Relative.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table DELta des accroissements
C     VREL        Table de Degré de Liberté Globaux
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_N2GR(HNUMR, NDLN, NNL, VVAL, VREL, EPSA, EPSR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_N2GR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VREL(NDLN, NNL)
      REAL*8  VVAL(NDLN, NNL)
      REAL*8  EPSA(NDLN)
      REAL*8  EPSR(NDLN)

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN, ID, NNT, NNPP
      REAL*8  DU, U, DR, DRM
      REAL*8  T_L(2), T_G(2)
      REAL*8  VNRM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      T_L(1) = 0.0D0
      T_L(2) = 0.0D0
      DO IN=1,NNL
         IF (NM_NUMR_ESTNOPP(HNUMR, IN)) THEN
            DO ID=1,NDLN
               DU = VVAL(ID, IN)
               U  = VREL(ID, IN)
               DR = DU / (ABS(U)*EPSR(ID) + EPSA(ID))
               T_L(1) = T_L(1) + DR*DR
               T_L(2) = T_L(2) + 1.0D0
            ENDDO
         ENDIF
      ENDDO

C---     Synchronise
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 2, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Calcule la norme
      VNRM = SQRT(T_G(1) / NINT(T_G(2)))

      NR_UTIL_N2GR = VNRM
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme MAX, Single, Directe.
C
C Description:
C     La fonction <code>NR_UTIL_NISD(...)</code> calcule
C     la Norme MAX Single (1 DDL) Directe.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table des VALeurs
C     IDDL        Indice de DDL pour le calcul
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_NISD(HNUMR, NDLN, NNL, VVAL, IDDL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_NISD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN,NNL)
      INTEGER IDDL

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER I_L
      REAL*8  T_L, T_G

      INTEGER IDAMAX          ! Fonctions BLAS
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      I_L = IDAMAX(NNL, VVAL(IDDL, 1), NDLN)
      T_L = ABS(VVAL(IDDL, I_L))

C---     Synchronise
      T_G = 0.0D0
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

      NR_UTIL_NISD = T_G
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme MAX, Single, Relative.
C
C Description:
C     La fonction <code>NR_UTIL_NISR(...)</code> calcule
C     la Norme MAX Single (1 DDL) Relative.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table DELta des accroissements
C     IDDL        Indice de DDL pour le calcul
C     VREL        Table de Degré de Liberté Globaux
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_NISR(HNUMR, NDLN, NNL, VVAL,
     &                      IDDL, VREL, EPSA, EPSR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_NISR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN, NNL)
      INTEGER IDDL
      REAL*8  VREL(NDLN, NNL)
      REAL*8  EPSA
      REAL*8  EPSR

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN
      REAL*8  DU, U, DR, DRM, DRT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule le critère
      DRM = 0.0D0
      DO IN=1, NNL
         DU = VVAL(IDDL, IN)
         U  = VREL(IDDL, IN)
         DR = DU / (ABS(U)*EPSR + EPSA)
         DRM = MAX(ABS(DR), DRM)
      ENDDO

C---     Synchronise
      DRT = 0.0D0
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(DRM, DRT, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

      NR_UTIL_NISR = DRT
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme MAX, Global, Directe.
C
C Description:
C     La fonction <code>NR_UTIL_NIGD(...)</code> calcule
C     la Norme MAX Globale (tous les DDL) Directe.
C
C Entrée:
C     HOBJ        L'objet courant
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VREL        Table de Degré de Liberté Globaux
C     VVAL        Table DELta des accroissements
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_NIGD(HNUMR, NDLN, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_NIGD
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VVAL(NDLN*NNL)

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER I_L
      REAL*8  T_L, T_G

      INTEGER IDAMAX          ! Fonctions BLAS
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule la partie locale
      I_L = IDAMAX(NDLN*NNL, VVAL, 1)
      T_L = ABS(VVAL(I_L))

C---     Synchronise
      T_G = 0.0D0
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(T_L, T_G, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

      NR_UTIL_NIGD = T_G
      RETURN
      END

C************************************************************************
C Sommaire: Calcule la norme MAX, Globale, Relative.
C
C Description:
C     La fonction <code>NR_UTIL_NIGR(...)</code> calcule
C     la Norme MAX Globale (tous les DDL) Relative.
C
C Entrée:
C     HNUMR       La numérotation des noeuds
C     NDLN        Nombre de Degrés de Liberté par Noeud
C     NNL         Nombre de Noeuds Locaux
C     VVAL        Table DELta des accroissements
C     VREL        Table de Degré de Liberté Globaux
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NR_UTIL_NIGR(HNUMR, NDLN, NNL, VVAL, VREL, EPSA, EPSR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NR_UTIL_NIGR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VREL(NDLN, NNL)
      REAL*8  VVAL(NDLN, NNL)
      REAL*8  EPSA(NDLN)
      REAL*8  EPSR(NDLN)

      INCLUDE 'nrutil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'

      INTEGER I_ERROR

      INTEGER IN, ID
      REAL*8  DU, U, DR, DRM, DRT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

C---     Calcule le critère
      DRM = 0.0D0
      DO IN=1, NNL
         DO ID=1, NDLN
            DU = VVAL(ID, IN)
            U  = VREL(ID, IN)
            DR = DU / (ABS(U)*EPSR(ID) + EPSA(ID))
            DRM = MAX(ABS(DR), DRM)
         ENDDO
      ENDDO

C---     Synchronise
      DRT = 0.0D0
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(DRM, DRT, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

      NR_UTIL_NIGR = DRT
      RETURN
      END
