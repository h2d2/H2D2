C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>TG_INTG_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_INTG_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgintg.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(TG_INTG_NOBJMAX,
     &                   TG_INTG_HBASE,
     &                   'Trigger sur integer')

      TG_INTG_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_INTG_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgintg.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER  IERR
      EXTERNAL TG_INTG_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(TG_INTG_NOBJMAX,
     &                   TG_INTG_HBASE,
     &                   TG_INTG_DTR)

      TG_INTG_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_INTG_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgintg.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   TG_INTG_NOBJMAX,
     &                   TG_INTG_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(TG_INTG_HVALIDE(HOBJ))
         IOB = HOBJ - TG_INTG_HBASE

         TG_INTG_ITRG(IOB) = -1
         TG_INTG_IACC(IOB) =  0
      ENDIF

      TG_INTG_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_INTG_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgintg.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TG_INTG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = TG_INTG_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   TG_INTG_NOBJMAX,
     &                   TG_INTG_HBASE)

      TG_INTG_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_INTG_INI(HOBJ, N)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER N

      INCLUDE 'tgintg.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_INTG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTRÔLES
      IF (N .LE. 0) GOTO 9900

C---     RESET LES DONNEES
      IERR = TG_INTG_RST(HOBJ)

C---     RECUPERE L'INDICE
      IOB  = HOBJ - TG_INTG_HBASE

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         TG_INTG_ITRG(IOB) = N
         TG_INTG_IACC(IOB) = 0
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_VALEUR_INVALIDE',': ',N
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      TG_INTG_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TG_INTG_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgintg.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER  IERR
      INTEGER  IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_INTG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - TG_INTG_HBASE

C---     RESET LES ATTRIBUTS
      TG_INTG_ITRG(IOB) = -1
      TG_INTG_IACC(IOB) =  0

      TG_INTG_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction TG_INTG_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_INTG_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tgintg.fi'
      INCLUDE 'tgintg.fc'
C------------------------------------------------------------------------

      TG_INTG_REQHBASE = TG_INTG_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction TG_INTG_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_INTG_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tgintg.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tgintg.fc'
C------------------------------------------------------------------------

      TG_INTG_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  TG_INTG_NOBJMAX,
     &                                  TG_INTG_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction TG_INTG_INC exécute le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TG_INTG_INC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TG_INTG_INC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'tgintg.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tgintg.fc'

      INTEGER IOB
      LOGICAL LRET
C------------------------------------------------------------------------
D     CALL ERR_PRE(TG_INTG_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - TG_INTG_HBASE

      TG_INTG_IACC(IOB) = TG_INTG_IACC(IOB) + 1
D     CALL ERR_ASR(TG_INTG_IACC(IOB) .GT. 0)

      LRET = .FALSE.
      IF (TG_INTG_ITRG(IOB) .GT. 0) THEN
         IF (MOD(TG_INTG_IACC(IOB), TG_INTG_ITRG(IOB)) .EQ. 0)
     &      LRET = .TRUE.
      ENDIF

      TG_INTG_INC = LRET
      RETURN
      END
