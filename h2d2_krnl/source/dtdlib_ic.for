C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_DLIB_XEQCTR
C     INTEGER IC_DT_DLIB_XEQMTH
C     INTEGER IC_DT_DLIB_REQHDL
C     CHARACTER*(32) IC_DT_DLIB_REQCLS
C   Private:
C     INTEGER IC_DT_DLIB_PRN
C     SUBROUTINE IC_DT_DLIB_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_DLIB_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_DLIB_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtdlib_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtdlib_ic.fc'

      INTEGER NVALMAX
      PARAMETER (NVALMAX = 20)

      REAL*8  VVAL(NVALMAX)
      INTEGER IERR, IRET
      INTEGER I
      INTEGER HOBJ
      INTEGER NTOK, NVAL
      CHARACTER*(256) NOMFIC
      LOGICAL DOINIT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_DLIB_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     VALIDE
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9901

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR(HOBJ)
      IF (ERR_GOOD()) THEN
         NVAL = 0          !! 1
         DOINIT = .FALSE.
         IERR = DT_VNOD_INIDIM(HOBJ,
     &                         NVAL,
     &                         DT_VNOD_DIM_DIFFERE,
     &                         DT_VNOD_DIM_DIFFERE,
     &                         DOINIT,
     &                         VVAL)
      ENDIF

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_DT_DLIB_PRN(HOBJ)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the degrees of freedom</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C  <comment>
C  The constructor <b>dof</b> constructs an object and returns a handle on this object.
C  The object <b>dof</b> is created empty and will be linked to a problem with <b>simd</b>.
C  </comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I3)') 'MSG_NVAL_MAX',': ',NVALMAX
      CALL ERR_AJT(ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(A)') 'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NBR_VALEURS_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_DLIB_AID()

9999  CONTINUE
      IC_DT_DLIB_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_DT_DLIB_PRN permet d'imprimer tous les paramètres
C     de l'objet.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_DLIB_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtdlib_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_DLIB'
      CALL LOG_ECRIS(LOG_BUF)

C---     Imprime le corps
      CALL LOG_INCIND()
      IERR = DT_VNOD_PRN(HOBJ)
      CALL LOG_DECIND()

      IC_DT_DLIB_PRN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_DLIB_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_DLIB_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtdlib_ic.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      IF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_VNOD_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_VNOD_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_VNOD_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_DLIB_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_DLIB_AID()

9999  CONTINUE
      IC_DT_DLIB_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Base de handle particulière, car sinon on retourne celle de dtvnod
C************************************************************************
      FUNCTION IC_DT_DLIB_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_DLIB_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtdlib_ic.fi'
      INCLUDE 'dtvnod.fi'
C-------------------------------------------------------------------------

      IC_DT_DLIB_REQHDL = 1999011000
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_DLIB_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_DLIB_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtdlib_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>dof</b> represent the degrees of freedom, therefore a solution to
C  a simulation. The degrees of freedom (unknowns) are the arguments
C  defining the state of a physical system.
C</comment>
      IC_DT_DLIB_REQCLS = 'dof'
      RETURN
      END


C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_DLIB_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_DLIB_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtdlib_ic.hlp')

      RETURN
      END

