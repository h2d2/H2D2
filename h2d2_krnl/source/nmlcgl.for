C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   LoCal-GLobal
C Type:
C
C Class: NM_LCGL
C     INTEGER NM_LCGL_000
C     INTEGER NM_LCGL_999
C     INTEGER NM_LCGL_CTR
C     INTEGER NM_LCGL_DTR
C     INTEGER NM_LCGL_INI
C     INTEGER NM_LCGL_RST
C     INTEGER NM_LCGL_REQHBASE
C     INTEGER NM_LCGL_REQHBASE
C     LOGICAL NM_LCGL_HVALIDE
C     INTEGER NM_LCGL_CHARGE
C     INTEGER NM_LCGL_LCLGLB
C     INTEGER NM_LCGL_PRCECR
C     INTEGER NM_LCGL_DIMTBL
C     INTEGER NM_LCGL_TBLECR
C     INTEGER NM_LCGL_PRNDST
C     LOGICAL NM_LCGL_ESTNOPP
C     INTEGER NM_LCGL_REQNPRT
C     INTEGER NM_LCGL_REQNNL
C     INTEGER NM_LCGL_REQNNP
C     INTEGER NM_LCGL_REQNNT
C     INTEGER NM_LCGL_REQNEXT
C     INTEGER NM_LCGL_REQNINT
C     INTEGER NM_LCGL_REQGLB
C     INTEGER NM_LCGL_REQLCL
C     INTEGER NM_LCGL_DSYNC
C     INTEGER NM_LCGL_ISYNC
C     INTEGER NM_LCGL_GENDSYNC
C     INTEGER NM_LCGL_GENISYNC
C     INTEGER NM_LCGL_GENSYNC
C
C************************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Note: 2018-01-02
!!!
!!! Les SUBMODULE(s) ne sont supportés que par Intel >= 16.4 et GCC >= 6.0
!!! Pour rester compatible avec les versions Intel antérieures (CMC) et
!!! pour pouvoir compiler Sun, le choix est fait de ne pas utiliser de
!!! SUBMODULE. Pour simuler/conserver la séparation déclaration/définition,
!!! le SUBMODULE est transformé en fichier inclus dans le section
!!! CONTAINS du module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !!!SUBMODULE(NM_LCGL_M) NM_LCGL_M_SRC1

      !!!CONTAINS

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction NM_LCGL_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C     HOBJ        Handle sur l'objet
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_CTR_SELF()

      TYPE(NM_LCGL_T), POINTER :: NM_LCGL_CTR_SELF

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      TYPE (NM_LCGL_T), POINTER :: SELF
C------------------------------------------------------------------------

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Initialise
      IERR = SELF%NM_LCGL_RAZ()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      NM_LCGL_CTR_SELF => SELF
      RETURN
      END FUNCTION NM_LCGL_CTR_SELF

C************************************************************************
C Sommaire: Destructeur de la classe
C
C Description:
C     La fonction NM_LCGL_DTR agit comme destructeur de la classe.
C     Elle désalloue l'objet.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_DTR(SELF)

      INTEGER :: NM_LCGL_DTR
      CLASS(NM_LCGL_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SELF%RST()
!!!      DEALLOCATE(SELF)

      NM_LCGL_DTR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_DTR


C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La méthode NM_LCGL_INI initialise l'objet à partir soit du nom de
C     fichier, soit des dimensions et du pointeur à la table. Pour initialiser
C     l'objet avec le nom de fichier, il faut mettre les autres
C     paramètres à 0. Sinon, il faut donner une chaîne vide pour le nom
C     de fichier.
C
C Entrée:
C     NOMFIC      Nom du fichier contenant la table
C     NNL_P       Le nombre d'items locaux
C     NNT_P       Le nombre d'items globaux
C     NPROC_P     Le nombre de sous-domaines
C     LDIST_P     Le pointeur à la table de distribution des items
C
C Sortie:
C     HOBJ        Le handle de l'objet initialisé
C
C Notes:
C     La table est passée par pointeur pour permettre un pointeur nul.
C************************************************************************
      FUNCTION NM_LCGL_INI(SELF,
     &                     NOMFIC,
     &                     NNL_P,
     &                     NNT_P,
     &                     NPROC_P,
     &                     LDIST_P)

      USE SO_ALLC_M

      INTEGER :: NM_LCGL_INI
      CLASS(NM_LCGL_T), INTENT(INOUT) :: SELF
      CHARACTER*(*), INTENT(IN) :: NOMFIC
      INTEGER, INTENT(IN) :: NNL_P
      INTEGER, INTENT(IN) :: NNT_P
      INTEGER, INTENT(IN) :: NPROC_P
      INTEGER, INTENT(IN) :: LDIST_P

      INCLUDE 'err.fi'
      INCLUDE 'fdnumr.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'

      TYPE(SO_ALLC_KPTR2_T) :: KDIST
      INTEGER IERR
      INTEGER HFNUM
      INTEGER NNA, NNL, NNT, NPROC
      LOGICAL MODFIC
C------------------------------------------------------------------------

C--      Contrôles des paramètres
      MODFIC = SP_STRN_LEN(NOMFIC) .GT. 0
      IF (MODFIC) THEN
         IF (NNL_P   .NE. 0) GOTO 9900
         IF (NNT_P   .NE. 0) GOTO 9900
         IF (NPROC_P .NE. 0) GOTO 9900
         IF (LDIST_P .NE. 0) GOTO 9900
      ELSE
         IF (NNL_P   .LT. 0) GOTO 9900
         IF (NNT_P   .LE. 0) GOTO 9900
         IF (NPROC_P .LE. 0) GOTO 9900
         IF (LDIST_P .EQ. 0) GOTO 9900
      ENDIF

C---     Reset les données
      IERR = SELF%RST()

C---     Charge la table à partir du fichier si nécessaire
      KDIST = SO_ALLC_KPTR2()
      IF (MODFIC) THEN

C---        Construis le fichier de données
         HFNUM = 0
         IF (ERR_GOOD()) IERR = FD_NUMR_CTR(HFNUM)
         IF (ERR_GOOD()) THEN
            IERR = FD_NUMR_INI(HFNUM,
     &                         NOMFIC(1:SP_STRN_LEN(NOMFIC)),
     &                         IO_MODE_LECTURE + IO_MODE_ASCII)
         ENDIF

C---       Demande les dimensions
         NPROC = -1
         NNT   = -1
         NNL   = -1
         IF (ERR_GOOD()) NNL   = FD_NUMR_REQNNTL (HFNUM)
         IF (ERR_GOOD()) NNT   = FD_NUMR_REQNNTG (HFNUM)
         IF (ERR_GOOD()) NPROC = FD_NUMR_REQNPROC(HFNUM)
         IF (ERR_GOOD() .AND. NPROC .LE. 0)   GOTO 9901
         IF (ERR_GOOD() .AND. NNT   .LE. 0)   GOTO 9902
         IF (ERR_GOOD() .AND. NNT   .LT. NNL) GOTO 9903

C---        Alloue l'espace de travail et la table globale des process
         NNA = MAX(1,NNL)     ! Pour faire exister la table
         IF (ERR_GOOD()) KDIST = SO_ALLC_KPTR2(NPROC+2, NNA)

C---        Lis la table de distribution
         IF (ERR_GOOD())
     &      IERR = FD_NUMR_LIS (HFNUM,
     &                          NPROC,
     &                          NPROC+2,
     &                          NNL,
     &                          KDIST%KPTR)

C---        Détruis le fichier de données
         IF (FD_NUMR_HVALIDE(HFNUM)) THEN
            CALL ERR_PUSH()
            IERR = FD_NUMR_DTR(HFNUM)
            CALL ERR_POP()
         ENDIF
      ELSE

C---        Les dimensions
         NNL   = NNL_P
         NNT   = NNT_P
         NPROC = NPROC_P

C---        Alloue l'espace de travail et la table globale des process
         NNA = MAX(1,NNL)     ! Pour faire exister la table
         IF (ERR_GOOD()) KDIST = SO_ALLC_KPTR2(NPROC+2, NNA)

C---        Copie la table passée comme paramètres
         IF (ERR_GOOD())
     &      CALL ICOPY((NPROC+2)*NNL,
     &                 KA(SO_ALLC_REQKIND(KA, LDIST_P)),
     &                 1,
     &                 KDIST%KPTR,
     &                 1)
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%KDIST = KDIST
         SELF%NPROC = NPROC
         SELF%NNT   = NNT
         SELF%NNL   = NNL
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NBR_PROCESS_INVALIDE', ': ', NPROC
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNT_INVALIDE', ': ', NNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNL_INVALIDE', ': ', NNL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_LCGL_INI = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_INI

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     La mémoire est désallouée automatiquement dans RAZ
C************************************************************************
      FUNCTION NM_LCGL_RST(SELF)

      INTEGER :: NM_LCGL_RST
      CLASS(NM_LCGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

C---     Remise à  zéro
      IERR = SELF%NM_LCGL_RAZ()

      NM_LCGL_RST = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_RST

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_LCGL_RAZ(SELF)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1, SO_ALLC_KPTR2

      INTEGER :: NM_LCGL_RAZ
      CLASS(NM_LCGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'err.fi'
C------------------------------------------------------------------------

C---     Reset les attributs
      SELF%KDIST     = SO_ALLC_KPTR2()
      SELF%NLOCGLB_P = SO_ALLC_KPTR1()
      SELF%NGLBLOC_P = SO_ALLC_KPTR2()
      SELF%PRECR_P   = SO_ALLC_KPTR1()
      SELF%ECRD_P    = SO_ALLC_KPTR1()
      SELF%ECR_P     = SO_ALLC_KPTR1()
      SELF%LISD_P    = SO_ALLC_KPTR1()
      SELF%LIS_P     = SO_ALLC_KPTR1()
      SELF%NPROC    = 0
      SELF%NECR     = 0
      SELF%NLIS     = 0
      SELF%NNT      = 0
      SELF%NNL      = 0
      SELF%NNP      = 0

      NM_LCGL_RAZ = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_RAZ

C************************************************************************
C Sommaire: Charge les données
C
C Description:
C     La fonction NM_LCGL_CHARGE charge les tables internes des
C     paramètres de configuration.
C
C Entrée:
C     HOBJ           Handle sur l'objet
C     NPROC          Nombre de process
C     NNL            Nombre de noeuds locaux
C     KDISTR         Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_CHARGE(SELF)

      USE SO_ALLC_M

      INTEGER :: NM_LCGL_CHARGE
      CLASS(NM_LCGL_T), INTENT(INOUT) :: SELF

      INCLUDE 'fdnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'

      INTEGER IERR, IRET
      INTEGER NNL, NNP, NNX, NNT, NNA, NPROC
      INTEGER NECR, NLIS
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.renum.local_global'

C---     Récupère les attributs
      NNL   = SELF%NNL
      NNT   = SELF%NNT
      NPROC = SELF%NPROC

C---     Alloue l'espace pour la table de renumérotation locale-globale
      NNA = MAX(1,NNL)     ! Pour faire exister les tables
      IF (ERR_GOOD()) SELF%NLOCGLB_P = SO_ALLC_KPTR1(NNA)
      IF (ERR_GOOD()) SELF%NGLBLOC_P = SO_ALLC_KPTR2(NNA, 2)
      IRET = MP_UTIL_SYNCERR()

C---     Extrais la table de renumérotation locale-globale
      IF (ERR_GOOD())
     &      IERR = NM_LCGL_LCLGLB(NPROC,
     &                            NNL,
     &                            SELF%KDIST%KPTR,
     &                            NNP,
     &                            NNX,
     &                            SELF%NLOCGLB_P%KPTR,
     &                            SELF%NGLBLOC_P%KPTR)
      IF (ERR_GOOD()) SELF%NNP = NNP

C---     Alloue l'espace pour la table des process à écrire
      IF (ERR_GOOD()) SELF%PRECR_P = SO_ALLC_KPTR1(MAX(NPROC-1,1))
      IRET = MP_UTIL_SYNCERR()

C---     Crée la table des process à écrire
      IF (ERR_GOOD())
     &   IERR = NM_LCGL_PRCECR(NPROC, SELF%PRECR_P%KPTR)

C---     Alloue l'espace des tables des pointeurs
      IF (ERR_GOOD()) SELF%ECRD_P = SO_ALLC_KPTR1(NPROC+1)
      IF (ERR_GOOD()) SELF%LISD_P = SO_ALLC_KPTR1(NPROC+1)
      IRET = MP_UTIL_SYNCERR()

C---     Dimension des tables de noeuds à écrire/lire
      IF (ERR_GOOD())
     &   IERR = NM_LCGL_DIMTBL(NPROC,
     &                         NNL,
     &                         SELF%KDIST%KPTR,
     &                         SELF%PRECR_P%KPTR,
     &                         NECR,
     &                         SELF%ECRD_P%KPTR,
     &                         NLIS,
     &                         SELF%LISD_P%KPTR)

C---     Alloue l'espace des tables de noeuds
      IF (ERR_GOOD()) SELF%ECR_P = SO_ALLC_KPTR1(MAX(NECR,1))
      IF (ERR_GOOD()) SELF%NECR  = NECR
      IF (ERR_GOOD()) SELF%LIS_P = SO_ALLC_KPTR1(MAX(NLIS,1))
      IF (ERR_GOOD()) SELF%NLIS  = NLIS
      IRET = MP_UTIL_SYNCERR()

C---     Table des noeuds à écrire et a lire
      IF (ERR_GOOD())
     &   IERR = NM_LCGL_TBLECR(NPROC,
     &                         NNL,
     &                         SELF%KDIST%KPTR,
     &                         SELF%PRECR_P%KPTR,
     &                         SELF%NECR,
     &                         SELF%ECRD_P%KPTR,
     &                         SELF%ECR_P%KPTR,
     &                         SELF%NLIS,
     &                         SELF%LISD_P%KPTR,
     &                         SELF%LIS_P%KPTR)

C---     Libère la mémoire de la table de distribution
      SELF%KDIST = SO_ALLC_KPTR2()

C---     Imprime les noeuds
      IF (ERR_GOOD()) THEN
         WRITE(LOG_BUF,'(A)') 'MSG_RENUM_LOCAL_GLOBAL:'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         IERR = NM_LCGL_PRNDST(NNL, SELF%NLOCGLB_P%KPTR)
      ENDIF

      NM_LCGL_CHARGE = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_CHARGE

C************************************************************************
C Sommaire: Synchronise les données REAL*8
C
C Description:
C     La fonction NM_LCGL_DSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_DSYNC(SELF, NVAL, NNL, VVAL)

      INTEGER :: NM_LCGL_DSYNC
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      REAL*8,  INTENT(IN) :: VVAL(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Synchronise
      IERR = MP_UTIL_DSYNC(SELF%NPROC,
     &                     SELF%PRECR_P%KPTR,
     &                     SELF%NECR,
     &                     SELF%ECRD_P%KPTR,
     &                     SELF%ECR_P%KPTR,
     &                     SELF%NLIS,
     &                     SELF%LISD_P%KPTR,
     &                     SELF%LIS_P%KPTR,
     &                     NVAL,
     &                     NNL,
     &                     VVAL)

      NM_LCGL_DSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_DSYNC

C************************************************************************
C Sommaire: Synchronise les données INTEGER
C
C Description:
C     La fonction NM_LCGL_ISYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table KVAL
C     NNL         Dimension 2 de la table KVAL
C     KVAL        Table INTEGER à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_ISYNC(SELF, NVAL, NNL, KVAL)

      INTEGER :: NM_LCGL_ISYNC
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: KVAL(:,:)

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     SYNCHRONISE
      IERR = MP_UTIL_ISYNC(SELF%NPROC,
     &                     SELF%PRECR_P%KPTR,
     &                     SELF%NECR,
     &                     SELF%ECRD_P%KPTR,
     &                     SELF%ECR_P%KPTR,
     &                     SELF%NLIS,
     &                     SELF%LISD_P%KPTR,
     &                     SELF%LIS_P%KPTR,
     &                     NVAL,
     &                     NNL,
     &                     KVAL)

      NM_LCGL_ISYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_ISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCGL_GENDSYNC génère un objet de synchronisation pour
C     une table REAL*8 de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_GENDSYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_LCGL_GENDSYNC
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SELF%NM_LCGL_GENSYNC(MP_TYPE_RE8(), NVAL ,NNL, HSNC)

      NM_LCGL_GENDSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_GENDSYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCGL_GENISYNC génère un objet de synchronisation pour
C     une table INTEGER de dimensions (NVAL, NNL).
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C     HSNC        Handle sur l'objet de synchronisation
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_GENISYNC(SELF, NVAL, NNL, HSNC)

      INTEGER :: NM_LCGL_GENISYNC
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mputil.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = SELF%NM_LCGL_GENSYNC(MP_TYPE_INT(), NVAL, NNL, HSNC)

      NM_LCGL_GENISYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_GENISYNC

C************************************************************************
C Sommaire: Génère un objet de synchronisation
C
C Description:
C     La fonction NM_LCGL_GENSYNC
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table à synchroniser
C     NNL         Dimension 2 de la table à synchroniser
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_GENSYNC(SELF, ITYP, NVAL, NNL, HSNC)

      INTEGER :: NM_LCGL_GENSYNC
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: ITYP
      INTEGER, INTENT(IN) :: NVAL
      INTEGER, INTENT(IN) :: NNL
      INTEGER, INTENT(IN) :: HSNC

      INCLUDE 'err.fi'
      INCLUDE 'mpsync.fi'

      INTEGER IERR
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     Synchronise
      IERR = MP_SYNC_CTR   (HSNC)
      IERR = MP_SYNC_INIMPI(HSNC,
     &                      ITYP,
     &                      SELF%NPROC,
     &                      SELF%PRECR_P%KPTR,
     &                      SELF%NECR,
     &                      SELF%ECRD_P%KPTR,
     &                      SELF%ECR_P%KPTR,
     &                      SELF%NLIS,
     &                      SELF%LISD_P%KPTR,
     &                      SELF%LIS_P%KPTR,
     &                      NVAL)

      NM_LCGL_GENSYNC = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_GENSYNC

C************************************************************************
C Sommaire: NM_LCGL_REQNPRT
C
C Description:
C     La fonction NM_LCGL_REQNPRT retourne le nombre de sous-domaines de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNPRT(SELF)

      INTEGER :: NM_LCGL_REQNPRT
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCGL_REQNPRT = SELF%NPROC
      RETURN
      END FUNCTION NM_LCGL_REQNPRT

C************************************************************************
C Sommaire: NM_LCGL_REQNNL
C
C Description:
C     La fonction NM_LCGL_REQNNL retourne le nombre de noeuds local de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNL(SELF)

      INTEGER :: NM_LCGL_REQNNL
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCGL_REQNNL = SELF%NNL
      RETURN
      END FUNCTION NM_LCGL_REQNNL

C************************************************************************
C Sommaire: NM_LCGL_REQNNP
C
C Description:
C     La fonction NM_LCGL_REQNNP retourne le nombre de noeuds privés de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNP(SELF)

      INTEGER :: NM_LCGL_REQNNP
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCGL_REQNNP = SELF%NNP
      RETURN
      END FUNCTION NM_LCGL_REQNNP

C************************************************************************
C Sommaire: NM_LCGL_REQNNT
C
C Description:
C     La fonction NM_LCGL_REQNNT retourne le nombre de noeuds total de la
C     renumérotation.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNNT(SELF)

      INTEGER :: NM_LCGL_REQNNT
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
C------------------------------------------------------------------------

      NM_LCGL_REQNNT = SELF%NNT
      RETURN
      END FUNCTION NM_LCGL_REQNNT

C************************************************************************
C Sommaire: NM_LCGL_ESTNOPP
C
C Description:
C     La fonction NM_LCGL_ESTNOPP retourne .TRUE. si le noeud passé en
C     argument est privé au process.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_ESTNOPP(SELF, INLOC)

      LOGICAL :: NM_LCGL_ESTNOPP
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'err.fi'

      INTEGER IG
C------------------------------------------------------------------------
D     CALL ERR_PRE(INLOC .GT. 0)
D     CALL ERR_PRE(INLOC .LE. SELF%NNL)
C------------------------------------------------------------------------

      IG  = SELF%NLOCGLB_P%KPTR(INLOC)
      NM_LCGL_ESTNOPP = (IG .GT. 0)
      RETURN
      END FUNCTION NM_LCGL_ESTNOPP

C************************************************************************
C Sommaire: NM_LCGL_REQNEXT
C
C Description:
C     La fonction NM_LCGL_REQNEXT retourne le numéro de noeud global
C     correspondant au numéro local passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INLOC    Numéro de noeud local à traduire en numéro global
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_REQNEXT(SELF, INLOC)

      INTEGER :: NM_LCGL_REQNEXT
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INLOC

      INCLUDE 'err.fi'

      INTEGER IG
C------------------------------------------------------------------------
D     CALL ERR_PRE(INLOC .GT. 0)
D     CALL ERR_PRE(INLOC .LE. SELF%NNL)
C------------------------------------------------------------------------

      IG = SELF%NLOCGLB_P%KPTR(INLOC)
      NM_LCGL_REQNEXT = ABS(IG)
      RETURN
      END FUNCTION NM_LCGL_REQNEXT

C************************************************************************
C Sommaire: NM_LCGL_REQNINT
C
C Description:
C     La fonction NM_LCGL_REQNINT retourne le numéro de noeud local
C     correspondant au numéro global passé en argument.
C
C Entrée:
C     HOBJ     Handle sur l'objet
C     INGLB    Numéro de noeud global à traduire en numéro local
C
C Sortie:
C
C Notes:
C     En fait, pour accélérer, il faudrait monter
C     la table inverse.
C************************************************************************
      FUNCTION NM_LCGL_REQNINT(SELF, INGLB)

      INTEGER :: NM_LCGL_REQNINT
      CLASS(NM_LCGL_T), INTENT(IN) :: SELF
      INTEGER, INTENT(IN) :: INGLB

      INCLUDE 'err.fi'

      INTEGER I1, I2, IM, IN
      INTEGER IRET
      INTEGER, POINTER :: KNGLBLOC(:,:)
C------------------------------------------------------------------------
D     CALL ERR_PRE(INGLB .GT. 0)
C------------------------------------------------------------------------

      IRET = 0

C---     Récupère les attributs
      KNGLBLOC => SELF%NGLBLOC_P%KPTR
      IF (SELF%NNL .LE. 0) GOTO 9999

C---     CHERCHE PAR BI-SECTION
      I1 = 1
      I2 = SELF%NNL
100   CONTINUE
         IM = (I1+I2)/2
         IN = KNGLBLOC(IM, 1)
         IF (IN .EQ. INGLB) GOTO 199
         IF (IM .EQ. I2)    GOTO 199
         IF (IN .LT. INGLB) THEN
            I1 = IM + 1
         ELSE
            I2 = IM
         ENDIF
      GOTO 100
199   CONTINUE
      IF (IN .EQ. INGLB)
     &   IRET = KNGLBLOC(IM, 2)

9999  CONTINUE
      NM_LCGL_REQNINT = IRET
      RETURN
      END FUNCTION NM_LCGL_REQNINT

C************************************************************************
C Sommaire: NM_LCGL_LCLGLB
C
C Description:
C     La fonction privée NM_LCGL_LCLGLB extrait de la table de
C     distribution la renumérotation locale-globale.
C
C Entrée:
C     NPROC          Nombre de process
C     NNL            Nombre de noeuds locaux
C     KDISTR         Table de distribution
C
C Sortie:
C     KNLOCGLB       Table de traduction local-global
C     KNGLBLOC       Table de traduction global-local
C     NNP            Nombre de noeuds privés
C     NNX            Nombre de noeuds externes
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_LCLGLB(NPROC,
     &                        NNL,
     &                        KDISTR,
     &                        NNP,
     &                        NNX,
     &                        KNLOCGLB,
     &                        KNGLBLOC)

      INTEGER NM_LCGL_LCLGLB
      INTEGER NPROC
      INTEGER NNL
      INTEGER KDISTR(NPROC+2, NNL)
      INTEGER NNP
      INTEGER NNX
      INTEGER KNLOCGLB(NNL)
      INTEGER KNGLBLOC(NNL,2)

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
      INTEGER IG, IL, IN, IP, IPN
      INTEGER I_RANK, I_ERROR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL   .GE. 0)
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     PARAMETRES MPI
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     INITIALISE
      NNP = 0
      NNX = 0
      CALL IINIT(NNL, 0, KNLOCGLB, 1)

C---     TRANVASE
      IP = I_RANK + 1
      IG = NPROC + 1
      DO IN=1,NNL
         IL = KDISTR(IP,IN)
         IF (IL .LT.   1) GOTO 9900
         IF (IL .GT. NNL) GOTO 9900
         KNLOCGLB(IL) = KDISTR(IG,IN)
      ENDDO

C---     CONTROLE
      DO IN=1,NNL
         IF (KNLOCGLB(IN) .LE. 0) GOTO 9901
      ENDDO

C---     MONTE LA TABLE INVERSE
      DO IN=1,NNL
         KNGLBLOC(IN,1) = KNLOCGLB(IN)
         KNGLBLOC(IN,2) = IN
      ENDDO
      IF (NNL .GT. 0) CALL ISORT(KNGLBLOC(1,1),KNGLBLOC(1,2),NNL,2)

C---     MET LES NOEUDS DES AUTRES PROCESS EN NÉGATIF
      IP = I_RANK + 1
      IG = NPROC + 2
      NNX = 0
      DO IN=1,NNL
         IPN = KDISTR(IG,IN)        ! PROCESS DU NOEUD
         IF (IPN .NE. IP) THEN
            IL = KDISTR(IP,IN)
            KNLOCGLB(IL) = -KNLOCGLB(IL)
            NNX = NNX + 1
         ENDIF
      ENDDO
      NNP = NNL - NNX

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(3A)') 'ERR_PARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(A,3(A,I6),A)') 'MSG_NOEUD_LOCAL_INVALIDE',
     &   ': IL(', IN, ') = ', IL, ' (1 <=  IL  <= ', NNL, ')'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(3A)') 'ERR_PARTITION_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I6)') 'MSG_NOEUD_LOCAL_ABSENT', ': I = ', IN
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      NM_LCGL_LCLGLB = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_LCLGLB

C************************************************************************
C Sommaire: NM_LCGL_PRCECR
C
C Description:
C     La fonction privée NM_LCGL_PRCECR monte la table des process
C     auxquels il faut écrire des informations.
C
C Entrée:
C     NPROC          Nombre de process
C
C Sortie:
C     KPRECR         Table des process à écrire
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_PRCECR(NPROC,
     &                        KPRECR)

      INTEGER NM_LCGL_PRCECR
      INTEGER NPROC
      INTEGER KPRECR(NPROC-1)

      INCLUDE 'err.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER IERR
      INTEGER I_RANK, I_ERROR
      INTEGER II, IR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     PARAMETRES MPI
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)

C---     TABLES DES PROCESS A ECRIRE(SEND)
      II = 0
      DO IR=(I_RANK+1),(NPROC-1)
         II = II + 1
         KPRECR(II) = IR+1
      ENDDO
      DO IR=0,(I_RANK-1)
         II = II + 1
         KPRECR(II) = IR+1
      ENDDO
D     CALL ERR_ASR(II .EQ. (NPROC-1))

      NM_LCGL_PRCECR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_PRCECR

C************************************************************************
C Sommaire: NM_LCGL_DIMTBL
C
C Description:
C     La fonction privée NM_LCGL_DIMTBL calcule les dimensions des tables
C     des noeuds à écrire et à lire.
C
C Entrée:
C     NPROC          Nombre de process
C     NNL            Nombre de valeurs à lire pour le process
C     KDISTR         Table de distribution
C     KPRECR         Table des process à écrire
C
C Sortie:
C     NECR           Nombre de noeuds total à écrire
C     KECRD          Table du nombre de noeuds à écrire par process
C     NLIS           Nombre de noeuds total à lire
C     KLISD          Table du nombre de noeuds à lire par process
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_DIMTBL(NPROC,
     &                        NNL,
     &                        KDISTR,
     &                        KPRECR,
     &                        NECR,
     &                        KECRD,
     &                        NLIS,
     &                        KLISD)

      INTEGER NM_LCGL_DIMTBL
      INTEGER NPROC
      INTEGER NNL
      INTEGER KDISTR(NPROC+2, NNL)
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER I_RANK, I_ERROR, I_TAG, I_PROC, I_COMM
      INTEGER IN, IP, IS
      INTEGER IPPNO
      INTEGER IPRNK
      INTEGER NNE, NTMP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL   .GE. 0)
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     ZONE DE LOG
      LOG_ZNE = 'h2d2.renum.local_global'

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      I_PROC = I_RANK + 1
      I_TAG = 1223

C---     COMPTE LE NOMBRE DE NOEUDS A ENVOYER PAR PROCESS
      IPPNO = NPROC + 2       ! Process propriétaire du noeud
      DO IP=1,(NPROC-1)
         IS = KPRECR(IP)
         NNE = 0
         DO IN=1,NNL
            IF (KDISTR(IPPNO,IN) .EQ. I_PROC) THEN ! noeud de i_rank
               IF (KDISTR(IS,IN) .GT. 0) THEN      ! noeud présent sur ip
                  NNE = NNE + 1
               ENDIF
            ENDIF
         ENDDO
         KECRD(IP) = NNE
      ENDDO

C---     DISPATCH LE NOMBRE DE NOEUDS A LIRE PAR LES AUTRES PROCESS
      CALL LOG_INCIND()
      WRITE(LOG_BUF,'(A)') '         From   To  N.nod'
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      DO IP=1,(NPROC-1)

C---        ENVOIE LE NOMBRE DE NOEUDS
         IPRNK = KPRECR(IP)-1
         CALL MPI_SEND(KECRD(IP), 1, MP_TYPE_INT(),
     &                 IPRNK,
     &                 I_TAG, I_COMM, I_ERROR)
         IF (.NOT. ERR_GOOD()) GOTO 199
         WRITE(LOG_BUF,*) 'SEND: ',I_RANK,IPRNK,KECRD(IP)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

C---        RECOIS LE NOMBRE DE NOEUDS
         IPRNK = KPRECR(NPROC-IP)-1
         CALL MPI_RECV(KLISD(IP), 1, MP_TYPE_INT(),
     &                 IPRNK,
     &                 I_TAG, I_COMM, I_STATUS, I_ERROR)
         IF (.NOT. ERR_GOOD()) GOTO 199
         WRITE(LOG_BUF,*) 'RECV: ',I_RANK,IPRNK,KLISD(IP)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)

      ENDDO
199   CONTINUE
      CALL LOG_DECIND()

C---     TRANSFORME EN TABLES CUMULATIVES
      NNE = 1
      DO IP=2,NPROC
         NTMP = KECRD(IP)
         KECRD(IP) = KECRD(IP-1) + NNE
         NNE = NTMP
      ENDDO
      KECRD(1) = 1
      NNE = 1
      DO IP=2,NPROC
         NTMP = KLISD(IP)
         KLISD(IP) = KLISD(IP-1) + NNE
         NNE = NTMP
      ENDDO
      KLISD(1) = 1

C---     NOMBRES DE NOEUDS TOTAL
      NECR = KECRD(NPROC)-1
      NLIS = KLISD(NPROC)-1

      NM_LCGL_DIMTBL = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_DIMTBL

C************************************************************************
C Sommaire: NM_LCGL_TBLECR
C
C Description:
C     La fonction privée NM_LCGL_TBLECR
C
C Entrée:
C     NPROC          Nombre de process
C     NVAL           Nombre de valeurs à lire pour le process
C     KDISTR         Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_TBLECR(NPROC,
     &                        NNL,
     &                        KDISTR,
     &                        KPRECR,
     &                        NECR,
     &                        KECRD,
     &                        KECR,
     &                        NLIS,
     &                        KLISD,
     &                        KLIS)

      INTEGER NM_LCGL_TBLECR
      INTEGER NPROC
      INTEGER NNL
      INTEGER KDISTR(NPROC+2, NNL)
      INTEGER KPRECR(NPROC-1)
      INTEGER NECR
      INTEGER KECRD(NPROC+1)
      INTEGER KECR (NECR)
      INTEGER NLIS
      INTEGER KLISD(NPROC+1)
      INTEGER KLIS (NLIS)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'

      INTEGER I_MASTER
      INTEGER I_STATUS(MPI_STATUS_SIZE)
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER I_RANK, I_ERROR, I_TAG, I_COMM
      INTEGER IN, IP, IS
      INTEGER IPRNK, IPROC, IPPNO
      INTEGER NNE
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NNL   .GE. 0)
D     CALL ERR_PRE(NPROC .GT. 0)
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     PARAMETRES MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      I_TAG = 1223
      IPROC = I_RANK + 1

C---     ASSEMBLE LA LISTE DES NOEUDS A ENVOYER (TABLE DE TRAVAIL)
      IPPNO = NPROC + 2       ! Process propriétaire du noeud
      NNE = 0
      DO IP=1,(NPROC-1)
         IS = KPRECR(IP)
         DO IN=1,NNL
            IF (KDISTR(IPPNO,IN) .EQ. IPROC) THEN ! noeud de i_rank
               IF (KDISTR(IS,IN) .GT. 0) THEN     ! noeud présent sur is
                  NNE = NNE + 1
                  KECR(NNE) = KDISTR(IS,IN)       ! dans la num. de is
               ENDIF
            ENDIF
         ENDDO
      ENDDO
D     CALL ERR_ASR(NNE .EQ. NECR)

      IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
         WRITE(LOG_BUF,*) 'TABLES DE LECTURE-ECRITURE:'
      ENDIF

C---     DISPATCH LES NOEUDS
      DO IP=1,(NPROC-1)

C---        ENVOIE LES NOEUDS
         NNE = KECRD(IP+1)-KECRD(IP)
         IF (NNE .GT. 0) THEN
            IPRNK = KPRECR(IP)-1
            CALL MPI_SEND(KECR(KECRD(IP)), NNE, MP_TYPE_INT(),
     &                    IPRNK,
     &                    I_TAG, I_COMM, I_ERROR)
            IF (.NOT. ERR_GOOD()) GOTO 199

            IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
               WRITE(LOG_BUF,*) 'NOEUDS DEMANDES',
     &                          ' PAR:', I_RANK, ' A:', IPRNK
               CALL LOG_ECRIS(LOG_BUF)
               DO IN=1,NNE
                  WRITE(LOG_BUF,*) '   ',IN, KECR(KECRD(IP)+IN-1)
                  CALL LOG_ECRIS(LOG_BUF)
               ENDDO
            ENDIF

         ENDIF

C---        RECOIS LES NOEUDS
         NNE = KLISD(IP+1)-KLISD(IP)
         IF (NNE .GT. 0) THEN
            IPRNK = KPRECR(NPROC-IP)-1
            CALL MPI_RECV(KLIS(KLISD(IP)), NNE, MP_TYPE_INT(),
     &                    IPRNK,
     &                    I_TAG, I_COMM, I_STATUS, I_ERROR)
            IF (.NOT. ERR_GOOD()) GOTO 199

            IF (LOG_REQNIV() .GE. LOG_LVL_DEBUG) THEN
               WRITE(LOG_BUF,*) 'NOEUDS A LIRE',
     &                        ' DE', IPRNK, ' PAR:', I_RANK
               CALL LOG_ECRIS(LOG_BUF)
               DO IN=1,NNE
                  WRITE(LOG_BUF,*) '   ',IN, KLIS(KLISD(IP)+IN-1)
                  CALL LOG_ECRIS(LOG_BUF)
               ENDDO
            ENDIF

         ENDIF

      ENDDO
199   CONTINUE

C---     ASSEMBLE LA LISTE DES NOEUDS A ENVOYER
      IPROC = I_RANK + 1
      IPPNO = NPROC + 2       ! Process propriétaire du noeud
      NNE = 0
      DO IP=1,(NPROC-1)
         IS = KPRECR(IP)
         DO IN=1,NNL
            IF (KDISTR(IPPNO,IN) .EQ. IPROC) THEN ! noeud de i_rank
               IF (KDISTR(IS,IN) .GT. 0) THEN     ! noeud présent sur is
                  NNE = NNE + 1
                  KECR(NNE) = KDISTR(IPROC,IN)    ! dans la num. de i_rank
               ENDIF
            ENDIF
         ENDDO
      ENDDO
D     CALL ERR_ASR(NNE .EQ. NECR)

      GOTO 9999
C-------------------------------------------------------------------------

9999  CONTINUE
      NM_LCGL_TBLECR = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_TBLECR

C************************************************************************
C Sommaire: Log la distribution
C
C Description:
C     La fonction privée NM_LCGL_PRNDST imprime la distribution
C
C Entrée:
C     INTEGER NNL          Nombre de Noeuds Locaux
C     INTEGER KDIST(NNL)   Table de distribution
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_LCGL_PRNDST(NNL, KDIST)

      INTEGER NM_LCGL_PRNDST
      INTEGER NNL
      INTEGER KDIST(NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER IN
C-----------------------------------------------------------------------

      LOG_ZNE = 'h2d2.renum.local_global'

      DO IN = 1,NNL
         WRITE(LOG_BUF, '(I12,1X,I12)') IN, KDIST(IN)
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      ENDDO

      NM_LCGL_PRNDST = ERR_TYP()
      RETURN
      END FUNCTION NM_LCGL_PRNDST

      !!!END SUBMODULE NM_LCGL_M_SRC1
      