C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_DT_LIMT_XEQCTR
C     INTEGER IC_DT_LIMT_XEQMTH
C     CHARACTER*(32) IC_DT_LIMT_REQCLS
C     INTEGER IC_DT_LIMT_REQHDL
C   Private:
C     SUBROUTINE IC_DT_LIMT_AID
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_LIMT_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_LIMT_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'dtlimt_ic.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'dtlimt_ic.fc'

      INTEGER         IERR
      INTEGER         ISTAT
      INTEGER         HOBJ
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_DT_LIMT_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the boundary file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
!!!C     <comment>Opening mode ['r', 'rb'] (default 'r')</comment>
!!!      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, MODE)
!!!      IF (IERR .NE. 0) MODE = 'r'
!!!  Le mode binaire n'est pas supporté par le fichier
      IF (IERR .EQ. 0) MODE = 'r'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9903

C---     CONSTRUIS ET INITIALISE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = DT_LIMT_CTR   (HOBJ)
      IF (ERR_GOOD()) IERR = DT_LIMT_INIFIC(HOBJ, NOMFIC, ISTAT)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) IERR = DT_LIMT_PRN(HOBJ)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the boundary</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>boundary</b> constructs an object with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_NOM_FICHIER_INVALIDE',': ',
     &                        IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_ACCES_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_LIMT_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_DT_LIMT_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_DT_LIMT_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_LIMT_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'dtlimt_ic.fi'
      INCLUDE 'dtlimt.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IRET
      INTEGER HVAL
      REAL*8  RVAL
      CHARACTER*(256) NOMFIC
C------------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C     <comment>The method <b>load</b> loads the limit from file.</comment>
      IF (IMTH .EQ. 'load') THEN
D        CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900

C        <comment>Handle on the node numbering</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HVAL)
C        <comment>Time</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', 2, RVAL)
         IF (IRET .NE. 0) GOTO 9901
         IF (ERR_GOOD()) IERR = DT_LIMT_CHARGE(HOBJ, HVAL, RVAL)

C     <comment>
C     The method <b>save</b> saves the limit to file. The limit must be
C     loaded, either explicitly with a call to the method <i>load</i> or
C     implicitly through an algorithm.
C     </comment>
      ELSEIF (IMTH .EQ. 'save') THEN
D        CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .EQ. 0) GOTO 9900

C        <comment>Handle on the node numbering</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', 1, HVAL)
C        <comment>Limit file name</comment>
         IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', 2, NOMFIC)
         IF (IERR .NE. 0) GOTO 9901

         IF (ERR_GOOD())
     &       IERR = DT_LIMT_SAUVE(HOBJ,
     &                            HVAL,
     &                            NOMFIC,
     &                            IO_MODE_ECRITURE + IO_MODE_ASCII)

C     <comment>
C     The method <b>del</b> deletes the object.
C     The handle shall not be used anymore to reference the object.
C     </comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_LIMT_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(DT_LIMT_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = DT_LIMT_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_DT_LIMT_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_DT_LIMT_AID()

9999  CONTINUE
      IC_DT_LIMT_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_LIMT_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_LIMT_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtlimt_ic.fi'
C-------------------------------------------------------------------------


C<comment>
C  The class <b>boundary</b> represents the boundaries, i.e. the
C  geometrical part of the boundary conditions. The boundary conditions are
C  conditions that must be satisfied by the degrees of freedom on the
C  boundary of the simulation domain. They are invariant in time.
C</comment>

      IC_DT_LIMT_REQCLS = 'boundary'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_DT_LIMT_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_DT_LIMT_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'dtlimt_ic.fi'
      INCLUDE 'dtlimt.fi'
C-------------------------------------------------------------------------

      IC_DT_LIMT_REQHDL = DT_LIMT_REQHBASE()
      RETURN
      END


C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_DT_LIMT_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_DT_LIMT_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('dtlimt_ic.hlp')

      RETURN
      END

