C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Groupe:  Numérotation
C Objet:   NUMeRotation
C Type:    Virtuel
C
C Class: NM_NUMR
C     INTEGER NM_NUMR_DTR
C     LOGICAL NM_NUMR_CTRLH
C     INTEGER NM_NUMR_CHARGE
C     INTEGER NM_NUMR_DSYNC
C     INTEGER NM_NUMR_ISYNC
C     INTEGER NM_NUMR_GENISYNC
C     INTEGER NM_NUMR_GENDSYNC
C     INTEGER NM_NUMR_REQNPRT
C     INTEGER NM_NUMR_REQNNL
C     INTEGER NM_NUMR_REQNNP
C     INTEGER NM_NUMR_REQNNT
C     LOGICAL NM_NUMR_ESTNOPP
C     INTEGER NM_NUMR_REQNEXT
C     INTEGER NM_NUMR_REQNINT
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER  IERR
C-----------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_HVALIDE(HOBJ)) THEN
         IERR = NM_COMP_DTR(HOBJ)
      ELSEIF (NM_PRXY_HVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_DTR(HOBJ)
      ELSEIF (NM_IDEN_HVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_DTR(HOBJ)
      ELSEIF (NM_LCLC_HVALIDE(HOBJ)) THEN
         IERR = NM_LCLC_DTR(HOBJ)
      ELSEIF (NM_LCGL_HVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_DTR(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction NM_NUMR_CTRLH permet de valider un objet de la
C     hiérarchie virtuelle. Elle retourne .TRUE. si le handle qui
C     lui est passé est valide comme handle de l'objet ou d'un
C     héritier.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_NUMR_CTRLH(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_CTRLH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      LOGICAL VALIDE
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      VALIDE = .FALSE.
      IF     (NM_COMP_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (NM_PRXY_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (NM_IDEN_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (NM_LCLC_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ELSEIF (NM_LCGL_HVALIDE(HOBJ)) THEN
         VALIDE = .TRUE.
      ENDIF

      NM_NUMR_CTRLH = VALIDE
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_CHARGE (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_CHARGE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_HVALIDE(HOBJ)) THEN
         IERR = NM_COMP_CHARGE(HOBJ)
      ELSEIF (NM_PRXY_HVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_CHARGE(HOBJ)
      ELSEIF (NM_IDEN_HVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_CHARGE(HOBJ)
      ELSEIF (NM_LCLC_HVALIDE(HOBJ)) THEN
         IERR = NM_LCLC_CHARGE(HOBJ)
      ELSEIF (NM_LCGL_HVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_CHARGE(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_CHARGE = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NVAL        Dimension 1 de la table VVAL
C     NNL         Dimension 2 de la table VVAL
C     VVAL        Table (NVAL, NNL) REAL*8 à synchroniser
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_DSYNC (HOBJ, NVAL, NNL, VVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_DSYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      REAL*8  VVAL(NVAL, NNL)

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         IERR = NM_COMP_DSYNC(HOBJ, NVAL, NNL, VVAL)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_DSYNC(HOBJ, NVAL, NNL, VVAL)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_DSYNC(HOBJ, NVAL, NNL, VVAL)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         IERR = NM_LCLC_DSYNC(HOBJ, NVAL, NNL, VVAL)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_DSYNC(HOBJ, NVAL, NNL, VVAL)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_DSYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_ISYNC (HOBJ, NVAL, NNL, KVAL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_ISYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER KVAL(NVAL, NNL)

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
C         IERR = NM_COMP_ISYNC(HOBJ, NVAL, NNL, KVAL)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_ISYNC(HOBJ, NVAL, NNL, KVAL)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_ISYNC(HOBJ, NVAL, NNL, KVAL)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
C         IERR = NM_LCLC_ISYNC(HOBJ, NVAL, NNL, KVAL)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_ISYNC(HOBJ, NVAL, NNL, KVAL)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_ISYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_GENISYNC (HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_GENISYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER HSNC

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
C         IERR = NM_COMP_GENISYNC (HOBJ, HSNC)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_GENISYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_GENISYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         IERR = NM_LCLC_GENISYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_GENISYNC (HOBJ, NVAL, NNL, HSNC)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_GENISYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_GENDSYNC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      INTEGER NNL
      INTEGER HSNC

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
C         IERR = NM_COMP_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         IERR = NM_PRXY_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         IERR = NM_IDEN_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         IERR = NM_LCLC_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         IERR = NM_LCGL_GENDSYNC (HOBJ, NVAL, NNL, HSNC)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_GENDSYNC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNPRT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNPRT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NPRT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NPRT = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         NPRT = NM_COMP_REQNPRT(HOBJ)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NPRT = NM_PRXY_REQNPRT(HOBJ)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NPRT = NM_IDEN_REQNPRT(HOBJ)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NPRT = NM_LCLC_REQNPRT(HOBJ)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NPRT = NM_LCGL_REQNPRT(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNPRT = NPRT
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNNL (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNNL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NNL
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NNL = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         NNL = NM_COMP_REQNNL(HOBJ)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NNL = NM_PRXY_REQNNL(HOBJ)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NNL = NM_IDEN_REQNNL(HOBJ)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NNL = NM_LCLC_REQNNL(HOBJ)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NNL = NM_LCGL_REQNNL(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNNL = NNL
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNNP (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNNP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NNP
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NNP = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         NNP = NM_COMP_REQNNP(HOBJ)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NNP = NM_PRXY_REQNNP(HOBJ)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NNP = NM_IDEN_REQNNP(HOBJ)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NNP = NM_LCLC_REQNNP(HOBJ)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NNP = NM_LCGL_REQNNP(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNNP = NNP
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNNT (HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNNT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NNT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NNT = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         NNT = NM_COMP_REQNNT(HOBJ)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NNT = NM_PRXY_REQNNT(HOBJ)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NNT = NM_IDEN_REQNNT(HOBJ)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NNT = NM_LCLC_REQNNT(HOBJ)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NNT = NM_LCGL_REQNNT(HOBJ)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNNT = NNT
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_ESTNOPP(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_ESTNOPP
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      LOGICAL NOPP
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NOPP = .FALSE.
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
C         NOPP = NM_COMP_ESTNOPP(HOBJ, I)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NOPP = NM_PRXY_ESTNOPP(HOBJ, I)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NOPP = NM_IDEN_ESTNOPP(HOBJ, I)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
C         NOPP = NM_LCLC_ESTNOPP(HOBJ, I)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NOPP = NM_LCGL_ESTNOPP(HOBJ, I)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_ESTNOPP = NOPP
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNEXT(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNEXT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NEXT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NEXT = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
C         NEXT = NM_COMP_REQNEXT(HOBJ, I)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NEXT = NM_PRXY_REQNEXT(HOBJ, I)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NEXT = NM_IDEN_REQNEXT(HOBJ, I)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NEXT = NM_LCLC_REQNEXT(HOBJ, I)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NEXT = NM_LCGL_REQNEXT(HOBJ, I)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNEXT = NEXT
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_NUMR_REQNINT(HOBJ, I)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: NM_NUMR_REQNINT
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER I

      INCLUDE 'nmnumr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmcomp.fi'
      INCLUDE 'nmprxy.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'nmlclc.fi'
      INCLUDE 'nmlcgl.fi'

      INTEGER NINT
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      NINT = -1
      IF     (NM_COMP_TVALIDE(HOBJ)) THEN
         NINT = NM_COMP_REQNINT(HOBJ, I)
      ELSEIF (NM_PRXY_TVALIDE(HOBJ)) THEN
         NINT = NM_PRXY_REQNINT(HOBJ, I)
      ELSEIF (NM_IDEN_TVALIDE(HOBJ)) THEN
         NINT = NM_IDEN_REQNINT(HOBJ, I)
      ELSEIF (NM_LCLC_TVALIDE(HOBJ)) THEN
         NINT = NM_LCLC_REQNINT(HOBJ, I)
      ELSEIF (NM_LCGL_TVALIDE(HOBJ)) THEN
         NINT = NM_LCGL_REQNINT(HOBJ, I)
      ELSE
         CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')
      ENDIF

      NM_NUMR_REQNINT = NINT
      RETURN
      END
