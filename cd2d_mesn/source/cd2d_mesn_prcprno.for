C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES) pour N classes
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MESN_PRCPRNO
C
C Description:
C     Pré-traitement au calcul des propriétés nodales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C      REAL*8    VCORG      Table des COoRdonnées Globales
C      INTEGER   KNGV       Table des coNectivités Globales de Volume
C      REAL*8    VDJV       Table des métriques (Determinant, Jacobien) de Volume
C      REAL*8    VPRGL      Table des PRopriétés GLobales
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Sortie:
C      REAL*8    VPRNO      Table des Propriétés NOdales
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MESN_PRCPRNO(VCORG,
     &                             KNGV,
     &                             VDJV,
     &                             VPRGL,
     &                             VPRNO,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MESN_PRCPRNO
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG(EG_CMMN_NDIM,  EG_CMMN_NNL)
      INTEGER KNGV (EG_CMMN_NCELV, EG_CMMN_NELV)
      REAL*8  VDJV (EG_CMMN_NDJV,  EG_CMMN_NELV)
      REAL*8  VPRGL(LM_CMMN_NPRGL)
      REAL*8  VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

C---     APPELLE LE PARENT
      CALL CD2D_BSEN_PRCPRNO(VCORG,
     &                       KNGV,
     &                       VDJV,
     &                       VPRGL,
     &                       VPRNO,
     &                       IERR)

C---     CALCUL DES TERMES PUITS-SOURCES
      CALL CD2D_MESN_PRCTPS(VPRGL,
     &                      VPRNO)

      RETURN
      END

C************************************************************************
C Sommaire :  CD2D_MESN_PRCTPS
C
C Description:
C     PRE-CALCUL DU TERME PUITS-SOURCE DE FLUX DE CHALEUR
C
C Entrée: VPRGL,VDLG,VPRNO
C
C Sortie: VPRNO
C
C Notes:
C     le calcul (13) (16) n'est correct que pour ws constant.
C     pour ws non constant, ils sont recalculées dans CLCPRNO
C************************************************************************
      SUBROUTINE CD2D_MESN_PRCTPS(VPRGL, VPRNO)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8 VPRGL(LM_CMMN_NPRGL)
      REAL*8 VPRNO(LM_CMMN_NPRNO, EG_CMMN_NNL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_mesn.fc'

      INTEGER IN, ID
      INTEGER IPTAU, IPA, IPB
      REAL*8  TAU, TAUD, TAUE
      REAL*8  PD, PE
      REAL*8  WS, CE
C-----------------------------------------------------------------------

      CALL LOG_TODO('CD2D_MESN_PRCTPS: Variable CE non assignee')
      CALL ERR_ASR(.FALSE.)

C---     INDICES DANS VPRNO
      IPTAU = 6

C---     BOUCLE SUR SUR LES NOEUDS
      DO IN =1,EG_CMMN_NNL

C---        CONTRAINTE DE CISAILLEMENT AU FOND (TAU)
         TAU = VPRNO(IPTAU,IN)

C---        INDICES DANS VPRNO
         IPA = LM_CMMN_NPRNOL + 6
         IPB = LM_CMMN_NPRNOL + 7

C---        BOUCLE SUR SUR LES DDL
         DO ID=1, LM_CMMN_NDLN

C---           COMPARAISON DU CISAILLEMENT AU CISAILLEMENT CRITIQUE / PROBABILITÉ DE DEPOT
            TAUD = CD2D_MESN_TAUD(ID)
            TAUE = CD2D_MESN_TAUE(ID)
            IF (TAU .LT. TAUD) THEN
               PD = (UN - TAU/TAUD)
               PE = ZERO
            ELSEIF (TAU .LE. TAUE) THEN
               PD = ZERO
               PE = ZERO
            ELSE
               PD = ZERO
               PE = (TAU/TAUE - UN)
            ENDIF

C---           MET WS=1 POUR LE CAS WS PETIT
            WS = CD2D_MESN_WS(ID)
            IF (WS .LE. PETIT) WS = UN

C---           PROPRIETES NODALES
            VPRNO(IPA, IN) = PD*WS      ! Terme linéaire: WS corrigée
            VPRNO(IPB, IN) = PE*CE      ! Terme constant

            IPA = IPA + 2
            IPB = IPB + 2
         ENDDO

      ENDDO

      RETURN
      END
