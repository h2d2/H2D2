C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES) pour N classes
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MESN_PRCPRGL
C
C Description:
C     Pré-traitement au calcul des propriétés globales.
C     Fait tout le traitement qui ne dépend pas de VDLG.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MESN_PRCPRGL(VPRGL,
     &                             IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MESN_PRCPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_mesn.fc'

      INTEGER ID, IPL, IPC
      REAL*8  TAUCRE
      REAL*8  WSED, EXPN
C-----------------------------------------------------------------------

C---     APPEL DU PARENT
      CALL CD2D_BSE_PRCPRGL (VPRGL, IERR)

C---     EXTRAIT LES PARAMETRES DU MILIEUX
      CALL XTR_PRGL(VPRGL)

C---     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION SI ITAUE=1
      IPL = 11             ! Indice de prop. lue
      IPC = LM_CMMN_NPRGLL ! Indice de prop. calc.
      DO ID=1, LM_CMMN_NDLN
         IF (CD2D_MESN_TAUE(ID) .LE. PETIT) THEN
            TAUCRE = TC_SHIELDS(CD2D_MESN_GRAVITE,
     &                          CD2D_MESN_RHO_EAU,
     &                          CD2D_MESN_VISCO_EAU,
     &                          CD2D_MESN_DMT (ID),
     &                          CD2D_MESN_RHOS(ID))
            VPRGL(IPL) = TAUCRE
         ENDIF

C---        VARIABLES DÉRIVÉES POUR LES SOLIDES NON COHÉSIFS
         IF (CD2D_MESN_ICSOL(ID) .EQ. 2) THEN
            WSED = WS_NONCOHESIF(CD2D_MESN_GRAVITE,
     &                           CD2D_MESN_RHO_EAU,
     &                           CD2D_MESN_VISCO_EAU,
     &                           CD2D_MESN_DMT (ID),
     &                           CD2D_MESN_RHOS(ID))
            EXPN = XP_NONCOHESIF(CD2D_MESN_VISCO_EAU,
     &                           CD2D_MESN_DMT (ID),
     &                           WSED)
            VPRGL(IPC+1) = WSED
            VPRGL(IPC+2) = EXPN
         ENDIF

         IPL = IPL + 8
         IPC = IPC + 2
      ENDDO

C---     EXTRAIT À NOUVEAU LES PARAMETRES SUITE AU MODIFS
      CALL XTR_PRGL(VPRGL)

C---     ECRIS L'ENTETE
      CALL LOG_ECRIS(' ')
      CALL LOG_ECRIS('MSG_PRGL_CINETIQUE_CALCULEES')
      CALL LOG_INCIND()

C---     ECRIS LES PROPRIÉTÉS
      IPL = 11             ! Indice de prop. lue
      IPC = LM_CMMN_NPRGLL ! Indice de prop. calc.
      DO ID=1, LM_CMMN_NDLN
         IERR=CD2D_BSE_PRN1PRGL(IPL,'MSG_SIGMA_CRITIQUE_ERO',VPRGL(IPL))
         IF (CD2D_MESN_ICSOL(ID) .EQ. 2) THEN
           IERR = CD2D_BSE_PRN1PRGL(IPC+1,
     &                              'MSG_W_SED_NON_COESIF',
     &                              VPRGL(IPC+1))
           IERR = CD2D_BSE_PRN1PRGL(IPC+1,
     &                              'MSG_EXPO_CORRECTION',
     &                              VPRGL(IPC+2))
         ENDIF
         IPL = IPL + 8
         IPC = IPC + 2
      ENDDO
      CALL LOG_DECIND()

      IERR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire :  XTR_PRGL
C
C Description:
C     EXTRAIT DE VPRGL LES PARAMETRES DU MILIEUX
C
C Entrée:
C     VPRGL
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE XTR_PRGL(VPRGL)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)

      INCLUDE 'cd2d_bse.fi'
      INCLUDE 'cd2d_mesn.fc'

      INTEGER I, ID
C----------------------------------------------------------------

      I = 4
      I = I + 1
      CD2D_MESN_GRAVITE    = VPRGL(I)
      I = I + 1
      CD2D_MESN_RHO_EAU    = VPRGL(I)
      I = I + 1
      CD2D_MESN_VISCO_EAU  = VPRGL(I)

      DO ID=1,LM_CMMN_NDLN
         I = I + 1
         CD2D_MESN_ICSOL(ID)= NINT(VPRGL(I))
         I = I + 1
         CD2D_MESN_WS(ID)   = VPRGL(I)
         I = I + 1
         CD2D_MESN_TAUD(ID) = VPRGL(I)
         I = I + 1
         CD2D_MESN_TAUE(ID) = VPRGL(I)
         I = I + 1
         CD2D_MESN_DMT(ID)  = VPRGL(I)
         I = I + 1
         CD2D_MESN_RHOS(ID) = VPRGL(I)
         I = I + 1
         CD2D_MESN_CKS(ID)  = VPRGL(I)
         I = I + 1
         CD2D_MESN_CE(ID)   = VPRGL(I)
      ENDDO
D     CALL ERR_ASR(I .LE. LM_CMMN_NPRGLL)
      DO ID=1,LM_CMMN_NDLN
         I = I + 1
         CD2D_MESN_WSED(ID) = VPRGL(I)
         I = I + 1
         CD2D_MESN_EXPN(ID) = VPRGL(I)
      ENDDO

      RETURN
      END

C**************************************************************
C Sommaire :  TCSHIELDS
C
C Description:
C     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION SELON LE DIAGRAMME DE SHIELDS
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION TC_SHIELDS(GRA,RHOW,VISC,DMT,RHOS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'

      REAL*8 TC_SHIELDS
      REAL*8 GRA
      REAL*8 RHOW
      REAL*8 VISC
      REAL*8 DMT
      REAL*8 RHOS

      INCLUDE 'log.fi'

      REAL*8 RD, RD_UN
      REAL*8 DSED
      REAL*8 SHIELDS
      real*8 UETCRE
      REAL*8 TAUCRE
C-----------------------------------------------------------------------

C---     EXTRACTION DES PROPRIÉTÉS GLOBALES DE BASE (PROPRIÉTÉS DU MILIEU)
      RD    = RHOS / RHOW                        ! rapport des densités
      RD_UN = RD - UN
      RD_UN = MAX(ZERO,RD_UN)
      DSED  = DMT*((GRA*RD_UN/(VISC*VISC))**UN_3)! diamètre sédimentologique
      DSED  = MAX(PETIT,DSED)

C---     CALCUL DE LA VALEUR CRITIQUE DU PARAMÈTRE ADIMENSIONNEL DE SHIELDS
      IF     (                      DSED .LE.  4.D0) THEN
         SHIELDS = 0.24D0/DSED
      ELSEIF(DSED .GT.  4.D0 .AND. DSED .LE. 10.D0) THEN
         SHIELDS = 0.14D0*(DSED**(-0.64D0))
      ELSEIF(DSED .GT. 10.D0 .AND. DSED .LE. 20.D0) THEN
         SHIELDS = 0.04D0*(DSED**(-0.10D0))
      ELSEIF(DSED .GT. 20.D0 .AND. DSED .LE.150.D0) THEN
         SHIELDS = 0.013D0*(DSED**(0.29D0))
      ELSEIF(DSED .GT.150.D0                      ) THEN
         SHIELDS = 0.055D0
      ENDIF

C---     VITESSE DE CISAILLEMENT CRITIQUE D'ÉROSION
      UETCRE = SQRT(GRA*RD_UN*SHIELDS*DMT)

C---     CALCUL DE LA CONTRAINTE CRITIQUE D'ÉROSION
      TAUCRE = RHOW*UETCRE*UETCRE

C---     PLACER CONTRAINTE CRITIQUE EN PROP. GLOB.
      IF (TAUCRE .LT .PETIT) TAUCRE = PETIT

C---     IMPRESSION DE L'INFO
      IF (LOG_REQNIV() .GE. LOG_LVL_VERBOSE) THEN
         CALL LOG_ECRIS(' ')
         WRITE(LOG_BUF,1001)'CALCUL DE LA CONTRAINTE D''EROSION' //
     &                       'D''APRES LE DIAG. DE SHIELDS'
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_INCIND()

         WRITE(LOG_BUF,1001)'DIAMETRE SEDIMENTOLOGIQUE#<38>#=',
     &                       DSED
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)'VAL. CRIT. PARAM. ADIM. DE SHIELDS#<38>#=',
     &                       SHIELDS
         CALL LOG_ECRIS(LOG_BUF)
         WRITE(LOG_BUF,1001)'VIT. DE CISAILLEMENT CRIT. EROSION#<38>#=',
     &                       UETCRE
         CALL LOG_ECRIS(LOG_BUF)
         CALL LOG_DECIND()
      ENDIF
C---------------------------------------------------------------
1001  FORMAT(A,1PE14.6E3)
C---------------------------------------------------------------

      TC_SHIELDS = TAUCRE
      RETURN
      END

C**************************************************************
C Sommaire:  WS_COHESIF
C
C Description:
C     La fonction WS_NONCOHESIF calcule la vitesse de sédimentation
C     pour des solides non cohésifs
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION WS_NONCOHESIF(GRA, RHOW, VISC, DMT, RHOS)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'

      REAL*8 WS_NONCOHESIF
      REAL*8 GRA
      REAL*8 RHOW
      REAL*8 VISC
      REAL*8 DMT
      REAL*8 RHOS

      INCLUDE 'log.fi'

      REAL*8 RD, D, ARGU
      REAL*8 WS
C-----------------------------------------------------------------------

      RD    = RHOS / RHOW       ! rapport des densités

C---     VITESSE DE SEDIMENTATION DISCRETE
      D = DMT*1.0D+06           ! diamètre moyen (micron)
      IF (D .LE. 100.0D0) THEN
         WS = UN_18*(RD - UN)*GRA*DMT*DMT/VISC
      ELSEIF (D .LE. 1000.0D0) THEN
         ARGU = UN + 0.01D0*(RD - UN)*GRA*DMT*DMT*DMT/(VISC*VISC)
         WS = 10.0D0*VISC/DMT*(SQRT(ARGU) - UN)
      ELSE
         WS = 1.1D0*SQRT((RD - UN)*GRA*DMT)
      ENDIF

      WS_NONCOHESIF = WS
      RETURN
      END

C**************************************************************
C Sommaire:  CE_COHESIF
C
C Description:
C     La fonction CS_COHESIF calcule le coefficient d'érosion
C     pour des solides cohésifs.
C        CORRECTION DE LA VITESSE DE CHUTE EN FONCTION DE LA
C        CONCENTRATION TOTALE DE SOLIDES EN SUSPENSION
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C****************************************************************
      FUNCTION XP_NONCOHESIF(VISC, DMT, WSED)

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'

      REAL*8 XP_NONCOHESIF
      REAL*8 VISC
      REAL*8 DMT
      REAL*8 WSED

      INCLUDE 'log.fi'

      REAL*8 RE
      REAL*8 EXPN
C-----------------------------------------------------------------------

      RE = DMT*WSED/VISC        ! nombre de reynolds particulaire
      IF (RE .LE. UN) THEN
         EXPN = 4.65D0
      ELSEIF (RE .LT. 1000.0D0) THEN
         EXPN = 4.D0
      ELSE
         EXPN = 2.31D0
      ENDIF

      XP_NONCOHESIF = EXPN
      RETURN
      END

