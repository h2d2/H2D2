C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES) pour N classes
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MESN_REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MESN_REQPRM(TGELV,
     &                            NPRGL,
     &                            NPRGLL,
     &                            NPRNO,
     &                            NPRNOL,
     &                            NPREV,
     &                            NPREVL,
     &                            NPRES,
     &                            NSOLC,
     &                            NSOLCL,
     &                            NSOLR,
     &                            NSOLRL,
     &                            NDLN,
     &                            NDLEV,
     &                            NDLES,
     &                            ASURFACE,
     &                            ESTLIN)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MESN_REQPRM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER TGELV
      INTEGER NPRGL
      INTEGER NPRGLL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NSOLC
      INTEGER NSOLCL
      INTEGER NSOLR
      INTEGER NSOLRL
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------

C---     INITIALISE LES PARAMETRES DE L'ÉLÉMENT PARENT
      CALL CD2D_BNL_REQPRM(TGELV,
     &                     NPRGL,
     &                     NPRGLL,
     &                     NPRNO,
     &                     NPRNOL,
     &                     NPREV,
     &                     NPREVL,
     &                     NPRES,
     &                     NSOLC,
     &                     NSOLCL,
     &                     NSOLR,
     &                     NSOLRL,
     &                     NDLN,
     &                     NDLEV,
     &                     NDLES,
     &                     ASURFACE,
     &                     ESTLIN)

C---     PARAMETRES DEVANT ETRE DÉFINI PAR LES HERITIERS
C     NDLN  = 0                      ! NB DE DDL/NOEUD
C     NDLEV = NDLN * NNELV           ! NB DE DDL PAR ELEMENT DE VOLUME
C     NDLES = NDLN * NNELS           ! NB DE DDL PAR ELEMENT DE SURFACE
C     NPRGL = NPRGL + 3 + 10*NDLN    ! NB DE PROPRIÉTÉS GLOBALES
C     NPRGLL= NPRGLL+ 3 +  8*NDLN    ! NB DE PROPRIÉTÉS GLOBALES LUES
C     NPRNO = NPRNO + 2*NDLN + 1     ! NB DE PROPRIÉTÉS NODALES
C     NPRNOL= NPRNOL+ 1              ! NB DE PROPRIÉTÉS NODALES LUES
C     NSOLC = NDLN                   ! NB DE SOLLICITATIONS CONCENTRÉES
C     NSOLCL= NDLN                   ! NB DE SOLLICITATIONS CONCENTRÉES LUES
C     NSOLR = NDLN                   ! NB DE SOLLICITATIONS RÉPARTIES
C     NSOLRL= NDLN                   ! NB DE SOLLICITATIONS RÉPARTIES LUES

      RETURN
      END

