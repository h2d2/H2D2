C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Élément:
C     Convection-diffusion eulérienne 2-D
C     Matières en suspension (MES) pour N classes
C     Formulation non-conservative pour (C).
C     Élément T3 - linéaire
C
C Notes:
C************************************************************************

C************************************************************************
C Sommaire : CD2D_MESN_PSLPRGL
C
C Description:
C     Traitement post-lecture des propriétés globales
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D_MESN_PSLPRGL (VPRGL, IERR)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: CD2D_MESN_PSLPRGL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacnst.fi'
      INCLUDE 'eacmmn.fc'

      REAL*8  VPRGL(LM_CMMN_NPRGL)
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER I, ID
      INTEGER ICSOL
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     APPEL L'ELEMENT DE BASE
      IF (ERR_GOOD()) THEN
         CALL CD2D_BSE_PSLPRGL (VPRGL, IERR)
      ENDIF

C---     CONTROLE DE POSITIVITE
      IF (ERR_GOOD()) THEN
         DO I=5,LM_CMMN_NPRGL
            IF (VPRGL(I) .LT. ZERO) THEN
               WRITE(LOG_BUF,*) 'ERR_PROP_GLOB_NEGATIVE:',
     &                          'V(', I, ')=',VPRGL(I)
               CALL LOG_ECRIS(LOG_BUF)
               CALL ERR_ASG(ERR_ERR, 'ERR_PROP_GLOB_NEGATIVE')
            ENDIF
         ENDDO
      ENDIF

C---     CONTROLE TYPE DE SEDIMENT COHESIF OU NONCOHESIF
      IF (ERR_GOOD()) THEN
         I = 8
         DO ID=1,LM_CMMN_NDLN
            ICSOL = NINT(VPRGL(I))
            IF (ICSOL .NE. 1 .AND. ICSOL .NE. 2) THEN
               WRITE(ERR_BUF,*) 'ERR_PROP_GLOB_INVALIDE',':',
     &                          'V(', I, ')=',VPRGL(I)
               CALL ERR_ASG(ERR_ERR, ERR_BUF)
            ENDIF
            I = I + 8
         ENDDO
      ENDIF

C---     PROPRIÉTÉS EGALES A PETIT SI ZERO
      IF (ERR_GOOD()) THEN
         DO I=5,LM_CMMN_NPRGLL
            VPRGL(I) = MAX(VPRGL(I), PETIT)
         ENDDO
      ENDIF

      IERR = ERR_TYP()
      RETURN
      END
