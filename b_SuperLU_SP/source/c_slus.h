//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire: Pont avec la DLL SuperLU
//
//************************************************************************
#ifndef C_SLUS_H_DEJA_INCLU
#define C_SLUS_H_DEJA_INCLU

#ifndef MODULE_SUPERLU
#  define MODULE_SUPERLU 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_SLUS_ASGMSG C_SLUS_ASGMSG
#  define C_SLUS_INIT   C_SLUS_INIT
#  define C_SLUS_RESET  C_SLUS_RESET
#  define C_SLUS_FACT   C_SLUS_FACT
#  define C_SLUS_SOLVE  C_SLUS_SOLVE
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_SLUS_ASGMSG c_slus_asgmsg_
#  define C_SLUS_INIT   c_slus_init_
#  define C_SLUS_RESET  c_slus_reset_
#  define C_SLUS_FACT   c_slus_fact_
#  define C_SLUS_SOLVE  c_slus_solve_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_SLUS_ASGMSG c_slus_asgmsg__
#  define C_SLUS_INIT   c_slus_init__
#  define C_SLUS_RESET  c_slus_reset__
#  define C_SLUS_FACT   c_slus_fact__
#  define C_SLUS_SOLVE  c_slus_solve__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_SUPERLU)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUS_ASGMSG(F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUS_INIT  (hndl_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUS_RESET (hndl_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUS_FACT  (hndl_t*, fint_t*, fint_t*, double*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_SLUS_SOLVE (hndl_t*, fint_t*, fint_t*, double*);


#ifdef __cplusplus
}
#endif

#endif   // C_SLUS_H_DEJA_INCLU
