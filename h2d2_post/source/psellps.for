C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER PS_ELLPS_000
C     INTEGER PS_ELLPS_999
C     INTEGER PS_ELLPS_CTR
C     INTEGER PS_ELLPS_DTR
C     INTEGER PS_ELLPS_INI
C     INTEGER PS_ELLPS_RST
C     INTEGER PS_ELLPS_REQHBASE
C     LOGICAL PS_ELLPS_HVALIDE
C     INTEGER PS_ELLPS_ACC
C     INTEGER PS_ELLPS_FIN
C     INTEGER PS_ELLPS_XEQ
C     INTEGER PS_ELLPS_ASGHSIM
C     INTEGER PS_ELLPS_REQHVNO
C     CHARACTER*256 PS_ELLPS_REQNOMF
C   Private:
C     INTEGER PS_ELLPS_LOG
C     SUBROUTINE PS_ELLPS_COPY
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PS_ELLPS_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_ELLPS_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psellps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_ELLPS_NOBJMAX,
     &                   PS_ELLPS_HBASE,
     &                   'Post_Treatment Error Ellipses')

      PS_ELLPS_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_ELLPS_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psellps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER  IERR
      EXTERNAL PS_ELLPS_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_ELLPS_NOBJMAX,
     &                   PS_ELLPS_HBASE,
     &                   PS_ELLPS_DTR)

      PS_ELLPS_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction PS_ELLPS_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_ELLPS_NOBJMAX,
     &                   PS_ELLPS_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_ELLPS_HVALIDE(HOBJ))
         IOB = HOBJ - PS_ELLPS_HBASE

         PS_ELLPS_HPRNT(IOB) = 0
         PS_ELLPS_HVNO (IOB) = 0
         PS_ELLPS_IVNO (IOB) = 0
         PS_ELLPS_ICLC (IOB) = 0
      ENDIF

      PS_ELLPS_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_ELLPS_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_ELLPS_NOBJMAX,
     &                   PS_ELLPS_HBASE)

      PS_ELLPS_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier de données
C     HVNO        Handle sur les vno à traiter
C     IVNO        Indice (colonne) à traiter
C     ICLC        Type de calcul
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_INI(HOBJ,
     &                      NOMFIC,
     &                      ISTAT,
     &                      IOPR,
     &                      IOPW,
     &                      HVNO,
     &                      IVNO,
     &                      ICLC)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       HVNO
      INTEGER       IVNO
      INTEGER       ICLC

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psellps.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLES DES PARAMETRES
      IF (.NOT. DT_VNOD_HVALIDE(HVNO)) GOTO 9900
      IF    (BTEST(ICLC, EG_TPERR_HESSIEN)) THEN
         IF (IOPR .NE. PS_OP_NOOP) GOTO 9901
      ELSEIF (BTEST(ICLC, EG_TPERR_ELLIPSE)) THEN
         IF (IOPR .NE. PS_OP_NOOP) GOTO 9901
      ELSEIF (BTEST(ICLC, EG_TPERR_NRMHESS)) THEN
!        PASS
      ELSE
         GOTO 9902
      ENDIF

C---     RESET LES DONNEES
      IERR = PS_ELLPS_RST(HOBJ)

C---     CONSTRUIT ET INITIALISE LE PARENT
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_ELLPS_HBASE
         PS_ELLPS_HPRNT(IOB) = HPRNT
         PS_ELLPS_HVNO (IOB) = HVNO
         PS_ELLPS_IVNO (IOB) = IVNO
         PS_ELLPS_ICLC (IOB) = ICLC
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A, I12)') 'ERR_HANDLE_INVALIDE', ':', HVNO
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A, I12)') 'ERR_OPRDUC_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(2A, I12)') 'ERR_TYPE_CALCUL_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_ELLPS_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psellps.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_ELLPS_HBASE

C---     DETRUIS LES DONNÉES
      HPRNT = PS_ELLPS_HPRNT(IOB)
      IF (PS_PSTD_HVALIDE(HPRNT)) IERR = PS_PSTD_DTR(HPRNT)

C---     RESET
      IF (ERR_GOOD()) THEN
         PS_ELLPS_HPRNT(IOB) = 0
         PS_ELLPS_HVNO (IOB) = 0
         PS_ELLPS_IVNO (IOB) = 0
         PS_ELLPS_ICLC (IOB) = 0
      ENDIF

      PS_ELLPS_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_ELLPS_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_ELLPS_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psellps.fi'
      INCLUDE 'psellps.fc'
C------------------------------------------------------------------------

      PS_ELLPS_REQHBASE = PS_ELLPS_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_ELLPS_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_ELLPS_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psellps.fc'
C------------------------------------------------------------------------

      PS_ELLPS_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_ELLPS_NOBJMAX,
     &                                  PS_ELLPS_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_ACC
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1_T, SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'psellps.fc'

      REAL*8  TSIM
      INTEGER IERR
      INTEGER IOB, IOP
      INTEGER ICLC
      INTEGER HSIM, HELPS
      INTEGER HPRNT
      INTEGER HSRCE, ISRCE
      INTEGER HDLIB, HFRML, HNUMR
      INTEGER NNT, NNL, NPST
      INTEGER NNL_S, NVNO_S, LVNO_S
      INTEGER NNL_D, NPST_D, LVNO_D
      TYPE(SO_ALLC_VPTR1_T) :: P_SRC
      TYPE(SO_ALLC_VPTR2_T) :: P_HSS
      TYPE(SO_ALLC_VPTR1_T) :: P_NRM
      INTEGER L_PST
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.hessian'

C---     Récupère les attributs
      IOB = HOBJ - PS_ELLPS_HBASE
      HPRNT = PS_ELLPS_HPRNT(IOB)
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs
      HSRCE = PS_ELLPS_HVNO (IOB)
      ISRCE = PS_ELLPS_IVNO (IOB)
      ICLC  = PS_ELLPS_ICLC (IOB)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HSRCE))
      IF (BTEST(ICLC, EG_TPERR_NRMHESS)) THEN
         NPST = 1
      ELSE
         NPST = 3
      ENDIF

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
      HELPS = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HELPS))

C---     Récupère les données de simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)

C---     Récupère les données des DDL
      NNL  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NNT  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)
      TSIM = LM_HELE_REQTEMPS(HSIM)

C---     Récupère les données du VNO src
      NNL_S   = DT_VNOD_REQNNL (HSRCE)
      NVNO_S  = DT_VNOD_REQNVNO(HSRCE)
      LVNO_S  = DT_VNOD_REQLVNO(HSRCE)

C---     Récupère les données du VNO dest
      NNL_D  = DT_VNOD_REQNNL (HELPS)
      NPST_D = DT_VNOD_REQNVNO(HELPS)
      LVNO_D = DT_VNOD_REQLVNO(HELPS)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Contrôles
      IF (ISRCE .LE. 0) GOTO 9900
      IF (ISRCE .GT. DT_VNOD_REQNVNO(HSRCE)) GOTO 9900
      IF (NNL_D  .NE. NNL)  GOTO 9901
      IF (NPST_D .NE. NPST) GOTO 9902
      IF (NNL_S  .NE. NNL)  GOTO 9903
      IF (NVNO_S .LE. 0)    GOTO 9904
D     CALL ERR_ASR(LVNO_S .NE.  0)

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_ELLIPSE',
     &                                      TSIM)

C---     Copie les données source
      IF (ERR_GOOD()) P_SRC = SO_ALLC_VPTR1(NNL)
      IF (ERR_GOOD())
     &   CALL PS_ELLPS_COPY(NNL,
     &                      NVNO_S,
     &                      ISRCE,
     &                      VA(SO_ALLC_REQVIND(VA,LVNO_S)),
     &                      1,
     &                      1,
     &                      P_SRC%VPTR)

C---     Alloue la table de travail
      IF (ERR_GOOD()) P_HSS = SO_ALLC_VPTR2(3, NNL)
      IF (ERR_GOOD()) P_NRM = SO_ALLC_VPTR1(NNL)
      IF (BTEST(ICLC, EG_TPERR_NRMHESS)) THEN
         L_PST = P_NRM%REQHNDL()  ! Pointeur vers le résultat
      ELSE
         L_PST = P_HSS%REQHNDL()  ! Pointeur vers le résultat
      ENDIF

C---     Calcul du post-traitement
      IF (ERR_GOOD()) THEN
         IERR = LM_HGEO_CLCERR(HSIM,
     &                         P_SRC%VPTR,
     &                         P_HSS%VPTR,
     &                         P_NRM%VPTR,
     &                         ICLC)
      ENDIF

C---     Opération de réduction dans L_DLU
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       NPST_D*NNL_D,
     &                       PVNO_D%VPTR, 1,
     &                       VA(SO_ALLC_REQVIND(VA,L_PST)), 1)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HELPS,
     &                         HNUMR,
     &                         VA(SO_ALLC_REQVIND(VA, L_PST)),
     &                         TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HELPS, HNUMR)
      ENDIF

C---     Récupère la mémoire
      P_NRM = SO_ALLC_VPTR1()
      P_HSS = SO_ALLC_VPTR2()
      P_SRC = SO_ALLC_VPTR1()

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3,A,I3,A)') 'ERR_INDICE_INVALIDE', ':', ISRCE,
     &                      ', (1 <= I <=', DT_VNOD_REQNVNO(HSRCE), ')'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               NNL, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INCOMPATIBLES', ': ',
     &                               NPST, '/', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INCOMPATIBLES', ': ',
     &                               NNL, '/', NNL_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NVNO_INVALIDE', ': ', NVNO_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_ELLPS_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C
C************************************************************************
      FUNCTION PS_ELLPS_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_FIN
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'psellps.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNT_D, NNL_D, NPST_D, LVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PS_ELLPS_HBASE
      HPRNT = PS_ELLPS_HPRNT(IOB)
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NNT_D  = DT_VNOD_REQNNT (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère les données de la simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)

C---     Contrôles
      IF (NNL_D  .LE. 0) GOTO 9901
      IF (NPST_D .LE. 0) GOTO 9902

!!!C---     Opération de réduction
!!!      IF (ERR_GOOD()) THEN
!!!         IERR = PS_PSTD_RDUCFIN(HPRNT,
!!!     &                          NNL_D,
!!!     &                          PVNO_D%VPTR, 1)
!!!      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = PS_ELLPS_LOG(HOBJ,
     &                       HNUMR,
     &                       NNT_D,
     &                       NPST_D,
     &                       NNL_D,
     &                       PVNO_D%VPTR)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_ELLPS_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'psellps.fc'

      REAL*8  TSIM
      INTEGER IERR
      INTEGER IOB
      INTEGER HSRCE, HNUMR
      LOGICAL MODIF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - PS_ELLPS_HBASE
      HSRCE = PS_ELLPS_HVNO (IOB)

C---     Récupère les données de simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC   (HSIM)

C---     Charge les vno source
      IF (ERR_GOOD()) IERR = DT_VNOD_CHARGE(HSRCE,
     &                                      HNUMR,
     &                                      TSIM,
     &                                      MODIF)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_ELLPS_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_ELLPS_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_ELLPS_FIN(HOBJ)

      PS_ELLPS_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode privée PS_ELLPS_LOG
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_ELLPS_LOG(HOBJ, HNUMR, NNT, NDLN, NNL, VELPS)

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUMR
      INTEGER NNT
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VELPS(NDLN, NNL)

      INCLUDE 'psellps.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'psellps.fc'

      REAL*8  DNRM2     ! Fonctions BLAS
      INTEGER IDAMAX

      REAL*8  RL2_L, RL2_G
      REAL*8  RMX_L, RMX_G

      INTEGER IERR
      INTEGER IOB
      INTEGER ICLC
      INTEGER NMX_L, NMX_G
      INTEGER NDLL, NEQT

      INTEGER I_ERROR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NDLN .GT. 0)
D     CALL ERR_PRE(NNL  .GT. 0)
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.post.error'

C---     Récupère les attributs
      IOB = HOBJ - PS_ELLPS_HBASE
      ICLC = PS_ELLPS_ICLC (IOB)

C---     Entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_HESSIEN_ELLIPSE:'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Compteurs
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NDLN#<40>#= ', NDLN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NNL#<40>#= ', NNL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)') 'MSG_NNT#<40>#= ', NNT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Type de calcul
      IF    (BTEST(ICLC, EG_TPERR_HESSIEN)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_RESULTAT#<40>#= ','hessian'
      ELSEIF (BTEST(ICLC, EG_TPERR_ELLIPSE)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_RESULTAT#<40>#= ','ellipse'
      ELSEIF (BTEST(ICLC, EG_TPERR_NRMHESS)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_RESULTAT#<40>#= ',
     &                          'hessian norm'
      ENDIF
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IF     (BTEST(ICLC, EG_TPERR_HESSGREEN)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_CALCUL#<40>#= ','green'
      ELSEIF (BTEST(ICLC, EG_TPERR_HESSDXDX)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_CALCUL#<40>#= ','dxdx'
      ELSEIF (BTEST(ICLC, EG_TPERR_HESSRECOV)) THEN
         WRITE (LOG_BUF,'(2A)') 'MSG_TYPE_CALCUL#<40>#= ','recovery'
      ENDIF
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Normes
      RL2_L = DNRM2 (NNL, VELPS(1,1), NDLN)
      RL2_L = RL2_L*RL2_L
      NMX_L = IDAMAX(NNL, VELPS(1,1), NDLN)
      RMX_L = VELPS(1,NMX_L)
      NMX_L = NM_NUMR_REQNEXT(HNUMR, NMX_L)     ! En num. globale

      CALL MPI_ALLREDUCE(RL2_L, RL2_G, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      CALL MPI_ALLREDUCE(RMX_L, RMX_G, 1, MP_TYPE_RE8(),
     &                   MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      IF (RMX_G .EQ. RMX_L) NMX_L = NMX_L + NNT
      CALL MPI_ALLREDUCE(NMX_L, NMX_G, 1, MP_TYPE_INT(),
     &                   MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      NMX_L = NMX_L - NNT
      NMX_G = NMX_G - NNT

      WRITE (LOG_BUF,'(A,1PE14.6E3,A,I7,A)')
     &      'MSG_NORME_MAX#<40>#= ', RMX_G,' (', NMX_G, ')'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)')
     &      'MSG_NORME_L2#<40>#= ', SQRT(RL2_G/NNT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

      CALL LOG_DECIND()
      PS_ELLPS_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_ELLPS_ASGHSIM
C
C Description:
C     La fonction PS_ELLPS_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psellps.fi'
      INCLUDE 'err.fi'
      INCLUDE 'egtperr.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psellps.fc'

      INTEGER IOB, ICLC
      INTEGER HPRNT
      INTEGER NPST, NNL, NNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - PS_ELLPS_HBASE
      ICLC = PS_ELLPS_ICLC(IOB)

C---     Le nombre de variables de post-traitement
      NNL  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NNT  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)
      IF (BTEST(ICLC, EG_TPERR_NRMHESS)) THEN
         NPST = 1
      ELSE
         NPST = 3
      ENDIF

      HPRNT = PS_ELLPS_HPRNT(IOB)
      PS_ELLPS_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NNL, NNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_ELLPS_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_ELLPS_HPRNT(HOBJ-PS_ELLPS_HBASE)
      PS_ELLPS_REQHVNO = PS_PSTD_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_ELLPS_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_ELLPS_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_ELLPS_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psellps.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psellps.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_ELLPS_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_ELLPS_HPRNT(HOBJ-PS_ELLPS_HBASE)
      PS_ELLPS_REQNOMF = PS_PSTD_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: Copie des tables.
C
C Description:
C     La subroutine privée PS_ELLPS_COPY copie les données entre deux tables.
C     Par rapport à DCOPY, elle permet de spécifier la colonne en paramètre.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE PS_ELLPS_COPY(N,
     &                         NSRC, ISRC, VSRC,
     &                         NDST, IDST, VDST)
      IMPLICIT NONE
      INTEGER N
      INTEGER NSRC, ISRC
      REAL*8 VSRC(NSRC, *)
      INTEGER NDST, IDST
      REAL*8 VDST(NDST, *)
C------------------------------------------------------------------------
C------------------------------------------------------------------------
      CALL DCOPY(N, VSRC(ISRC,1), NSRC, VDST(IDST,1), NDST)

      RETURN
      END
