C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id:
C
C Functions:
C   Public:
C     INTEGER PS_SIMU_000
C     INTEGER PS_SIMU_999
C     INTEGER PS_SIMU_CTR
C     INTEGER PS_SIMU_DTR
C     INTEGER PS_SIMU_INI
C     INTEGER PS_SIMU_RST
C     INTEGER PS_SIMU_REQHBASE
C     LOGICAL PS_SIMU_HVALIDE
C     INTEGER PS_SIMU_ACC
C     INTEGER PS_SIMU_FIN
C     INTEGER PS_SIMU_XEQ
C     INTEGER PS_SIMU_ASGHSIM
C     INTEGER PS_SIMU_REQHVNO
C     CHARACTER*256 PS_SIMU_REQNOMF
C   Private:
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SIMU_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssimu.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_SIMU_NOBJMAX,
     &                   PS_SIMU_HBASE,
     &                   'Post-Treatment - Simulation')

      PS_SIMU_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SIMU_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssimu.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER  IERR
      EXTERNAL PS_SIMU_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_SIMU_NOBJMAX,
     &                   PS_SIMU_HBASE,
     &                   PS_SIMU_DTR)

      PS_SIMU_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_SIMU_NOBJMAX,
     &                   PS_SIMU_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_SIMU_HVALIDE(HOBJ))
         IOB = HOBJ - PS_SIMU_HBASE

         PS_SIMU_HPRNT(IOB) = 0
         PS_SIMU_HFCLC(IOB) = 0
         PS_SIMU_HFLOG(IOB) = 0
      ENDIF

      PS_SIMU_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_SIMU_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_SIMU_NOBJMAX,
     &                   PS_SIMU_HBASE)

      PS_SIMU_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C     L'objet prend le contrôle des call-back et se charge de leur
C     destruction.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_INI(HOBJ,
     &                     HFCLC,
     &                     HFLOG,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      INTEGER       HFCLC
      INTEGER       HFLOG
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'pssimu.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFCLC))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFLOG))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = PS_SIMU_RST(HOBJ)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_SIMU_HBASE
         PS_SIMU_HPRNT(IOB) = HPRNT
         PS_SIMU_HFCLC(IOB) = HFCLC
         PS_SIMU_HFLOG(IOB) = HFLOG
      ENDIF

      PS_SIMU_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT, HFCLC, HFLOG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB  = HOBJ - PS_SIMU_HBASE
      HPRNT = PS_SIMU_HPRNT(IOB)
      HFCLC = PS_SIMU_HFCLC(IOB)
      HFLOG = PS_SIMU_HFLOG(IOB)

C---     Détruis les données
      IF (SO_FUNC_HVALIDE(HFCLC)) IERR = SO_FUNC_DTR(HFCLC)
      IF (SO_FUNC_HVALIDE(HFLOG)) IERR = SO_FUNC_DTR(HFLOG)
      IF (PS_PSTD_HVALIDE(HPRNT)) IERR = PS_PSTD_DTR(HPRNT)

C---     Reset
      PS_SIMU_HPRNT(IOB) = 0
      PS_SIMU_HFCLC(IOB) = 0
      PS_SIMU_HFLOG(IOB) = 0

      PS_SIMU_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SIMU_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SIMU_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssimu.fi'
      INCLUDE 'pssimu.fc'
C------------------------------------------------------------------------

      PS_SIMU_REQHBASE = PS_SIMU_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SIMU_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SIMU_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssimu.fc'
C------------------------------------------------------------------------

      PS_SIMU_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_SIMU_NOBJMAX,
     &                                  PS_SIMU_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_ACC
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER HPRNT, HFCLC
      INTEGER HSIM, HVNO
      INTEGER HNUMR
      INTEGER NNL_L
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      REAL*8  TSIM
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: P_PST
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.simulation'

C---     Récupère les attributs
      IOB = HOBJ - PS_SIMU_HBASE
      HPRNT = PS_SIMU_HPRNT(IOB)
      HFCLC = PS_SIMU_HFCLC(IOB)
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)
      NNL_L = LM_HELE_REQPRM  (HSIM, LM_GEOM_PRM_NNL)
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Contrôles
      IF (NNL_D  .NE. NNL_L)  GOTO 9900

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_SIMU',
     &                                      TSIM)

C---     Alloue l'espace pour la solution
      IF (ERR_GOOD()) P_PST = SO_ALLC_VPTR2(NPST_D, NNL_D)

C---     Calcul du post-traitement par fonction enregistrée
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL(HFCLC,
     &                       SO_ALLC_CST2B(HSIM),
     &                       SO_ALLC_CST2B(NPST_D),
     &                       SO_ALLC_CST2B(NNL_D),
     &                       SO_ALLC_CST2B(P_PST%VPTR(:,1)))
      ENDIF

C---     Opération de réduction dans L_PST
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       NPST_D*NNL_D,
     &                       PVNO_D%VPTR,1,
     &                       P_PST%VPTR, 1)
      ENDIF

C---     Met à jour les données (LVNOD_D)
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                         HNUMR,
     &                         P_PST%VPTR,
     &                         TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

C---     Récupère l'espace
      P_PST = SO_ALLC_VPTR2()

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               NNL_L, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SIMU_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C************************************************************************
      FUNCTION PS_SIMU_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_FIN
CDEC$ ENDIF

      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFLOG
      INTEGER HSIM, HVNO, HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      INTEGER PS_SIMU_LOG
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - PS_SIMU_HBASE
      HPRNT = PS_SIMU_HPRNT(IOB)
      HFLOG = PS_SIMU_HFLOG(IOB)
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère la numérotation
      HNUMR  = LM_HELE_REQHNUMC(HSIM)

C---     Contrôles
      IF (NNL_D  .LE. 0) GOTO 9901
      IF (NPST_D .LE. 0) GOTO 9902

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL(HFLOG,
     &                       SO_ALLC_CST2B(HNUMR),
     &                       SO_ALLC_CST2B(NPST_D),
     &                       SO_ALLC_CST2B(NNL_D),
     &                       PVNO_D%CST2B())
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SIMU_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_XEQ(HOBJ, HSIM, NPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NPOST

      INCLUDE 'pssimu.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC   (HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_SIMU_ASGHSIM(HOBJ, HSIM, NPOST)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_SIMU_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_SIMU_FIN(HOBJ)

      PS_SIMU_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_SIMU_ASGHSIM
C
C Description:
C     La fonction PS_SIMU_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_ASGHSIM(HOBJ, HSIM, NPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NPOST

      INCLUDE 'pssimu.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER IOB
      INTEGER HPRNT
      INTEGER NNL, NNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ-PS_SIMU_HBASE
      HPRNT = PS_SIMU_HPRNT(IOB)

C---     Les dimensions
      NNL = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NNT = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)

      PS_SIMU_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPOST, NNL, NNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SIMU_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_SIMU_HPRNT(HOBJ-PS_SIMU_HBASE)
      PS_SIMU_REQHVNO = PS_PSTD_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SIMU_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SIMU_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SIMU_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssimu.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssimu.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SIMU_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_SIMU_HPRNT(HOBJ-PS_SIMU_HBASE)
      PS_SIMU_REQNOMF = PS_PSTD_REQNOMF(HPRNT)
      RETURN
      END
