C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Groupe:  Interface Commandes: PoSt-traitement
C Objet:   PoSt-traitement
C Type:    Concret
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PS_POST_XEQCTR(IPRM) est une fonction vide car il
C     n'est pas prévu de construire un proxy via une commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_POST_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_POST_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pspost_ic.fi'
      INCLUDE 'err.fi'

C------------------------------------------------------------------------
      CALL ERR_PRE(.FALSE.)
C------------------------------------------------------------------------

      IPRM = ' '

      IC_PS_POST_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_PS_POST_XEQMTH(...) exécute les méthodes valides
C     sur un objet de type PS_PRXY.
C     Le proxy résous directement les appels aux méthodes virtuelles.
C     Pour toutes les autres méthodes, on retourne le handle de l'objet
C     géré.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_POST_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_POST_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pspost_ic.fi'
      INCLUDE 'psxeq_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER IOB
      INTEGER HVAL
      CHARACTER*64 PROP
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     GET
      IF (IMTH .EQ. '##property_get##') THEN
D        CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, PROP)
         IF (IERR .NE. 0) GOTO 9901

C        <comment>Handle on the underlying nodal values</comment>
         IF (PROP .EQ. 'hvno') THEN
            HVAL = PS_POST_REQHVNO(HOBJ)
            WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

         ELSE
            GOTO 9902
         ENDIF

C     <comment>
C     The method <b>accumulate</b> add the present post-treatment values.
C     Values are reduced according to the reduction operation, and written to file
C     according to the writing operation. 
C     Simulation data must be loaded.
C     </comment>
      ELSEIF (IMTH .EQ. 'accumulate') THEN
C        <include>IC_PS_XEQ_ACC@psxeq_ic.for</include>
         IERR = IC_PS_XEQ_ACC(HOBJ, IPRM)

C     <comment>
C     The method <b>finalize</b> finalizes the post-treatment process. It
C     shall be used to write to file the results when the option 'nowrite' is
C     used.
C     </comment>
      ELSEIF (IMTH .EQ. 'finalize') THEN
C        <include>IC_PS_XEQ_FIN@psxeq_ic.for</include>
         IERR = IC_PS_XEQ_FIN(HOBJ, IPRM)

C     <comment>
C     The method <b>xeq</b> executes the post-treatment process. It is a combination of
C     a call to <b>accumulate</b> followed by a call to <b>finalize</b>.
C     </comment>
      ELSEIF (IMTH .EQ. 'xeq') THEN
C        <include>IC_PS_XEQ_XEQ@psxeq_ic.for</include>
         IERR = IC_PS_XEQ_XEQ(HOBJ, IPRM)

C     <comment>The method <b>del</b> deletes the object. The handle shall
C     not be used anymore to reference the object.
C     </comment>
      ELSEIF (IMTH .EQ. 'del') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = PS_POST_DTR(HOBJ)

      ELSE
         HVAL = PS_POST_REQHPST(HOBJ)
         WRITE(IPRM, '(2A,I12)') 'X', ',', HVAL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(3A)') 'ERR_PROPERTY_INVALIDE', ': ',
     &                       PROP(1:SP_STRN_LEN(PROP))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IC_PS_POST_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_POST_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_POST_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pspost_ic.fi'
C-------------------------------------------------------------------------

      IC_PS_POST_REQCLS = '#__dummy_placeholder__#__PS_POST__'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_POST_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_POST_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pspost_ic.fi'
      INCLUDE 'pspost.fi'
C-------------------------------------------------------------------------

      IC_PS_POST_REQHDL = PS_POST_REQHBASE()
      RETURN
      END
