C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: IC_PS
C      H2D2 Class: IC_PS_SN1V
C         INTEGER IC_PS_SN1V_XEQCTR
C         INTEGER IC_PS_SN1V_XEQMTH
C         CHARACTER*(32) IC_PS_SN1V_REQCLS
C         INTEGER IC_PS_SN1V_REQHDL
C         SUBROUTINE IC_PS_SN1V_AID
C         INTEGER IC_PS_SN1V_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN1V_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN1V_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pssn1v_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'pssn1v.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn1v_ic.fc'

      REAL*8  XY1(2), XY2(2)
      INTEGER IERR, IRET
      INTEGER IK, ISTAT
      INTEGER HPST, HOBJ
      INTEGER IOPR, IOPW
      INTEGER NDIM, NPNT
      INTEGER HVNO(2), ICOL(2)
      CHARACTER*(256) NOMFIC
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
      PARAMETER (NDIM = 2)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_SN1V_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IK = 0
C     <comment>Handle on the first nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, HVNO(1))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Index of column of first nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, ICOL(1))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Handle on the second nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, HVNO(2))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Index of column of second nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, ICOL(2))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Number of computation points on the line</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, NPNT)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Start point of line to probe (x,y)</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY1(1))
      IF (IRET .NE. 0) GOTO 9901
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY1(2))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>End point of line to probe (x,y)</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY2(1))
      IF (IRET .NE. 0) GOTO 9901
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY2(2))
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Name of the post-treatment file</comment>
      IK = IK + 1
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', IK, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IK = IK + 1
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', IK, MODE)
      IF (IERR .NE. 0) MODE = 'a'
C     <comment>Reduction operation (default 'noop')</comment>
      IK = IK + 1
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', IK, OPRD)
      IF (IERR .NE. 0) OPRD = 'noop'
C     <comment>Writing operation (default 'nowrite')</comment>
      IK = IK + 1
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', IK, OPWR)
      IF (IERR .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      IF (ICOL(1) .LE. 0) GOTO 9902
      IF (ICOL(2) .LE. 0) GOTO 9902
      IF (NPNT    .LE. 0) GOTO 9903
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9904
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9906
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9907
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9908

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR=PS_SN1V_CTR(HPST)
      IF (ERR_GOOD()) IERR=PS_SN1V_INI(HPST,
     &                                 NOMFIC,ISTAT,IOPR,IOPW,
     &                                 NDIM,
     &                                 NPNT,
     &                                 XY1,
     &                                 XY2,
     &                                 HVNO, ICOL)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_SN1V_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>probe_vector_on_line</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_INDICE_INVALIDE', ': ', ICOL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NBR_POINTS_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9908  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN1V_AID()

9999  CONTINUE
      IC_PS_SN1V_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN1V_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN1V_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pssn1v_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1v.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn1v_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>print</b> prints information about the object.</comment>
      IF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_SN1V_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_SN1V_PRN(HOBJ)
         IERR = IC_PS_SN1V_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_SN1V_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN1V_AID()

9999  CONTINUE
      IC_PS_SN1V_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN1V_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN1V_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1v_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>probe_vector_on_line</b> represents the post-treatment that
C   implements a numerical probe applied on a line, integrating the projection
C   of a vector value on the normal to the line.
C</comment>
      IC_PS_SN1V_REQCLS = 'probe_vector_on_line'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN1V_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN1V_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1v_ic.fi'
      INCLUDE 'pssn1v.fi'
C-------------------------------------------------------------------------

      IC_PS_SN1V_REQHDL = PS_SN1V_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_SN1V_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_SN1V_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pssn1v_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN1V_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn1v.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn1v_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_PROBE_1D_V'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_SN1V_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PS_SN1V_PRN = ERR_TYP()
      RETURN
      END
