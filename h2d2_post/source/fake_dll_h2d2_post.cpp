//************************************************************************
// H2D2 - External declaration of public symbols
// Module: h2d2_post
// Entry point: extern "C" void fake_dll_h2d2_post()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:05.408623
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_PS_COMP
F_PROT(IC_PS_COMP_XEQCTR, ic_ps_comp_xeqctr);
F_PROT(IC_PS_COMP_XEQMTH, ic_ps_comp_xeqmth);
F_PROT(IC_PS_COMP_REQCLS, ic_ps_comp_reqcls);
F_PROT(IC_PS_COMP_REQHDL, ic_ps_comp_reqhdl);
 
// ---  class IC_PS_DLIB
F_PROT(IC_PS_DLIB_XEQCTR, ic_ps_dlib_xeqctr);
F_PROT(IC_PS_DLIB_XEQMTH, ic_ps_dlib_xeqmth);
F_PROT(IC_PS_DLIB_REQCLS, ic_ps_dlib_reqcls);
F_PROT(IC_PS_DLIB_REQHDL, ic_ps_dlib_reqhdl);
 
// ---  class IC_PS_ELLPS
F_PROT(IC_PS_ELLPS_XEQCTR, ic_ps_ellps_xeqctr);
F_PROT(IC_PS_ELLPS_XEQMTH, ic_ps_ellps_xeqmth);
F_PROT(IC_PS_ELLPS_REQCLS, ic_ps_ellps_reqcls);
F_PROT(IC_PS_ELLPS_REQHDL, ic_ps_ellps_reqhdl);
 
// ---  class IC_PS_HESS
F_PROT(IC_PS_HESS_XEQCTR, ic_ps_hess_xeqctr);
F_PROT(IC_PS_HESS_XEQMTH, ic_ps_hess_xeqmth);
F_PROT(IC_PS_HESS_REQCLS, ic_ps_hess_reqcls);
F_PROT(IC_PS_HESS_REQHDL, ic_ps_hess_reqhdl);
 
// ---  class IC_PS_HNRM
F_PROT(IC_PS_HNRM_XEQCTR, ic_ps_hnrm_xeqctr);
F_PROT(IC_PS_HNRM_XEQMTH, ic_ps_hnrm_xeqmth);
F_PROT(IC_PS_HNRM_REQCLS, ic_ps_hnrm_reqcls);
F_PROT(IC_PS_HNRM_REQHDL, ic_ps_hnrm_reqhdl);
 
// ---  class IC_PS_POST
F_PROT(IC_PS_POST_XEQCTR, ic_ps_post_xeqctr);
F_PROT(IC_PS_POST_XEQMTH, ic_ps_post_xeqmth);
F_PROT(IC_PS_POST_REQCLS, ic_ps_post_reqcls);
F_PROT(IC_PS_POST_REQHDL, ic_ps_post_reqhdl);
 
// ---  class IC_PS_RESI
F_PROT(IC_PS_RESI_XEQCTR, ic_ps_resi_xeqctr);
F_PROT(IC_PS_RESI_XEQMTH, ic_ps_resi_xeqmth);
F_PROT(IC_PS_RESI_REQCLS, ic_ps_resi_reqcls);
F_PROT(IC_PS_RESI_REQHDL, ic_ps_resi_reqhdl);
 
// ---  class IC_PS_SIMU
F_PROT(IC_PS_SIMU_XEQCTR, ic_ps_simu_xeqctr);
F_PROT(IC_PS_SIMU_XEQMTH, ic_ps_simu_xeqmth);
F_PROT(IC_PS_SIMU_REQCLS, ic_ps_simu_reqcls);
F_PROT(IC_PS_SIMU_REQHDL, ic_ps_simu_reqhdl);
 
// ---  class IC_PS_SN0N
F_PROT(IC_PS_SN0N_XEQCTR, ic_ps_sn0n_xeqctr);
F_PROT(IC_PS_SN0N_XEQMTH, ic_ps_sn0n_xeqmth);
F_PROT(IC_PS_SN0N_REQCLS, ic_ps_sn0n_reqcls);
F_PROT(IC_PS_SN0N_REQHDL, ic_ps_sn0n_reqhdl);
 
// ---  class IC_PS_SN0P
F_PROT(IC_PS_SN0P_XEQCTR, ic_ps_sn0p_xeqctr);
F_PROT(IC_PS_SN0P_XEQMTH, ic_ps_sn0p_xeqmth);
F_PROT(IC_PS_SN0P_REQCLS, ic_ps_sn0p_reqcls);
F_PROT(IC_PS_SN0P_REQHDL, ic_ps_sn0p_reqhdl);
 
// ---  class IC_PS_SN1S
F_PROT(IC_PS_SN1S_XEQCTR, ic_ps_sn1s_xeqctr);
F_PROT(IC_PS_SN1S_XEQMTH, ic_ps_sn1s_xeqmth);
F_PROT(IC_PS_SN1S_REQCLS, ic_ps_sn1s_reqcls);
F_PROT(IC_PS_SN1S_REQHDL, ic_ps_sn1s_reqhdl);
 
// ---  class IC_PS_SN1V
F_PROT(IC_PS_SN1V_XEQCTR, ic_ps_sn1v_xeqctr);
F_PROT(IC_PS_SN1V_XEQMTH, ic_ps_sn1v_xeqmth);
F_PROT(IC_PS_SN1V_REQCLS, ic_ps_sn1v_reqcls);
F_PROT(IC_PS_SN1V_REQHDL, ic_ps_sn1v_reqhdl);
 
// ---  class IC_PS_XEQ
F_PROT(IC_PS_XEQ_ACC, ic_ps_xeq_acc);
F_PROT(IC_PS_XEQ_FIN, ic_ps_xeq_fin);
F_PROT(IC_PS_XEQ_XEQ, ic_ps_xeq_xeq);
 
// ---  class PS_COMP
F_PROT(PS_COMP_000, ps_comp_000);
F_PROT(PS_COMP_999, ps_comp_999);
F_PROT(PS_COMP_CTR, ps_comp_ctr);
F_PROT(PS_COMP_DTR, ps_comp_dtr);
F_PROT(PS_COMP_INI, ps_comp_ini);
F_PROT(PS_COMP_RST, ps_comp_rst);
F_PROT(PS_COMP_REQHBASE, ps_comp_reqhbase);
F_PROT(PS_COMP_HVALIDE, ps_comp_hvalide);
F_PROT(PS_COMP_ACC, ps_comp_acc);
F_PROT(PS_COMP_FIN, ps_comp_fin);
F_PROT(PS_COMP_XEQ, ps_comp_xeq);
F_PROT(PS_COMP_ASGHSIM, ps_comp_asghsim);
F_PROT(PS_COMP_REQHITM, ps_comp_reqhitm);
F_PROT(PS_COMP_REQHVNO, ps_comp_reqhvno);
 
// ---  class PS_DLIB
F_PROT(PS_DLIB_000, ps_dlib_000);
F_PROT(PS_DLIB_999, ps_dlib_999);
F_PROT(PS_DLIB_CTR, ps_dlib_ctr);
F_PROT(PS_DLIB_DTR, ps_dlib_dtr);
F_PROT(PS_DLIB_INI, ps_dlib_ini);
F_PROT(PS_DLIB_RST, ps_dlib_rst);
F_PROT(PS_DLIB_REQHBASE, ps_dlib_reqhbase);
F_PROT(PS_DLIB_HVALIDE, ps_dlib_hvalide);
F_PROT(PS_DLIB_ACC, ps_dlib_acc);
F_PROT(PS_DLIB_FIN, ps_dlib_fin);
F_PROT(PS_DLIB_XEQ, ps_dlib_xeq);
F_PROT(PS_DLIB_ASGHSIM, ps_dlib_asghsim);
F_PROT(PS_DLIB_REQHVNO, ps_dlib_reqhvno);
F_PROT(PS_DLIB_REQNOMF, ps_dlib_reqnomf);
 
// ---  class PS_ELLPS
F_PROT(PS_ELLPS_000, ps_ellps_000);
F_PROT(PS_ELLPS_999, ps_ellps_999);
F_PROT(PS_ELLPS_CTR, ps_ellps_ctr);
F_PROT(PS_ELLPS_DTR, ps_ellps_dtr);
F_PROT(PS_ELLPS_INI, ps_ellps_ini);
F_PROT(PS_ELLPS_RST, ps_ellps_rst);
F_PROT(PS_ELLPS_REQHBASE, ps_ellps_reqhbase);
F_PROT(PS_ELLPS_HVALIDE, ps_ellps_hvalide);
F_PROT(PS_ELLPS_ACC, ps_ellps_acc);
F_PROT(PS_ELLPS_FIN, ps_ellps_fin);
F_PROT(PS_ELLPS_XEQ, ps_ellps_xeq);
F_PROT(PS_ELLPS_ASGHSIM, ps_ellps_asghsim);
F_PROT(PS_ELLPS_REQHVNO, ps_ellps_reqhvno);
F_PROT(PS_ELLPS_REQNOMF, ps_ellps_reqnomf);
 
// ---  class PS_POST
F_PROT(PS_POST_000, ps_post_000);
F_PROT(PS_POST_999, ps_post_999);
F_PROT(PS_POST_CTR, ps_post_ctr);
F_PROT(PS_POST_DTR, ps_post_dtr);
F_PROT(PS_POST_INI, ps_post_ini);
F_PROT(PS_POST_RST, ps_post_rst);
F_PROT(PS_POST_REQHBASE, ps_post_reqhbase);
F_PROT(PS_POST_HVALIDE, ps_post_hvalide);
F_PROT(PS_POST_REQHPST, ps_post_reqhpst);
F_PROT(PS_POST_ACC, ps_post_acc);
F_PROT(PS_POST_FIN, ps_post_fin);
F_PROT(PS_POST_XEQ, ps_post_xeq);
F_PROT(PS_POST_ASGHSIM, ps_post_asghsim);
F_PROT(PS_POST_REQHVNO, ps_post_reqhvno);
 
// ---  class PS_PSTD
F_PROT(PS_PSTD_000, ps_pstd_000);
F_PROT(PS_PSTD_999, ps_pstd_999);
F_PROT(PS_PSTD_CTR, ps_pstd_ctr);
F_PROT(PS_PSTD_DTR, ps_pstd_dtr);
F_PROT(PS_PSTD_RAZ, ps_pstd_raz);
F_PROT(PS_PSTD_INI, ps_pstd_ini);
F_PROT(PS_PSTD_RST, ps_pstd_rst);
F_PROT(PS_PSTD_REQHBASE, ps_pstd_reqhbase);
F_PROT(PS_PSTD_HVALIDE, ps_pstd_hvalide);
F_PROT(PS_PSTD_LOGACC, ps_pstd_logacc);
F_PROT(PS_PSTD_XEQ, ps_pstd_xeq);
F_PROT(PS_PSTD_ASGHSIM, ps_pstd_asghsim);
F_PROT(PS_PSTD_REQHSIM, ps_pstd_reqhsim);
F_PROT(PS_PSTD_REQHVNO, ps_pstd_reqhvno);
F_PROT(PS_PSTD_REQNOMF, ps_pstd_reqnomf);
F_PROT(PS_PSTD_REQOPR, ps_pstd_reqopr);
F_PROT(PS_PSTD_REQOPW, ps_pstd_reqopw);
F_PROT(PS_PSTD_LOGABS, ps_pstd_logabs);
F_PROT(PS_PSTD_LOGVAL, ps_pstd_logval);
F_PROT(PS_PSTD_RDUC, ps_pstd_rduc);
F_PROT(PS_PSTD_RDUCFIN, ps_pstd_rducfin);
F_PROT(PS_PSTD_STR2OP, ps_pstd_str2op);
F_PROT(PS_PSTD_OP2STR, ps_pstd_op2str);
 
// ---  class PS_RESI
F_PROT(PS_RESI_000, ps_resi_000);
F_PROT(PS_RESI_999, ps_resi_999);
F_PROT(PS_RESI_CTR, ps_resi_ctr);
F_PROT(PS_RESI_DTR, ps_resi_dtr);
F_PROT(PS_RESI_INI, ps_resi_ini);
F_PROT(PS_RESI_RST, ps_resi_rst);
F_PROT(PS_RESI_REQHBASE, ps_resi_reqhbase);
F_PROT(PS_RESI_HVALIDE, ps_resi_hvalide);
F_PROT(PS_RESI_ACC, ps_resi_acc);
F_PROT(PS_RESI_FIN, ps_resi_fin);
F_PROT(PS_RESI_XEQ, ps_resi_xeq);
F_PROT(PS_RESI_ASGHSIM, ps_resi_asghsim);
F_PROT(PS_RESI_REQHVNO, ps_resi_reqhvno);
F_PROT(PS_RESI_REQNOMF, ps_resi_reqnomf);
 
// ---  class PS_SIMU
F_PROT(PS_SIMU_000, ps_simu_000);
F_PROT(PS_SIMU_999, ps_simu_999);
F_PROT(PS_SIMU_CTR, ps_simu_ctr);
F_PROT(PS_SIMU_DTR, ps_simu_dtr);
F_PROT(PS_SIMU_INI, ps_simu_ini);
F_PROT(PS_SIMU_RST, ps_simu_rst);
F_PROT(PS_SIMU_REQHBASE, ps_simu_reqhbase);
F_PROT(PS_SIMU_HVALIDE, ps_simu_hvalide);
F_PROT(PS_SIMU_ACC, ps_simu_acc);
F_PROT(PS_SIMU_FIN, ps_simu_fin);
F_PROT(PS_SIMU_XEQ, ps_simu_xeq);
F_PROT(PS_SIMU_ASGHSIM, ps_simu_asghsim);
F_PROT(PS_SIMU_REQHVNO, ps_simu_reqhvno);
F_PROT(PS_SIMU_REQNOMF, ps_simu_reqnomf);
 
// ---  class PS_SN0N
F_PROT(PS_SN0N_000, ps_sn0n_000);
F_PROT(PS_SN0N_999, ps_sn0n_999);
F_PROT(PS_SN0N_CTR, ps_sn0n_ctr);
F_PROT(PS_SN0N_DTR, ps_sn0n_dtr);
F_PROT(PS_SN0N_INI, ps_sn0n_ini);
F_PROT(PS_SN0N_RST, ps_sn0n_rst);
F_PROT(PS_SN0N_REQHBASE, ps_sn0n_reqhbase);
F_PROT(PS_SN0N_HVALIDE, ps_sn0n_hvalide);
F_PROT(PS_SN0N_AJTNOD, ps_sn0n_ajtnod);
F_PROT(PS_SN0N_ACC, ps_sn0n_acc);
F_PROT(PS_SN0N_FIN, ps_sn0n_fin);
F_PROT(PS_SN0N_XEQ, ps_sn0n_xeq);
F_PROT(PS_SN0N_ASGHSIM, ps_sn0n_asghsim);
F_PROT(PS_SN0N_REQHVNO, ps_sn0n_reqhvno);
F_PROT(PS_SN0N_REQNOMF, ps_sn0n_reqnomf);
 
// ---  class PS_SN0P
F_PROT(PS_SN0P_000, ps_sn0p_000);
F_PROT(PS_SN0P_999, ps_sn0p_999);
F_PROT(PS_SN0P_CTR, ps_sn0p_ctr);
F_PROT(PS_SN0P_DTR, ps_sn0p_dtr);
F_PROT(PS_SN0P_INI, ps_sn0p_ini);
F_PROT(PS_SN0P_RST, ps_sn0p_rst);
F_PROT(PS_SN0P_REQHBASE, ps_sn0p_reqhbase);
F_PROT(PS_SN0P_HVALIDE, ps_sn0p_hvalide);
F_PROT(PS_SN0P_AJTPNT, ps_sn0p_ajtpnt);
F_PROT(PS_SN0P_ACC, ps_sn0p_acc);
F_PROT(PS_SN0P_FIN, ps_sn0p_fin);
F_PROT(PS_SN0P_XEQ, ps_sn0p_xeq);
F_PROT(PS_SN0P_ASGHSIM, ps_sn0p_asghsim);
F_PROT(PS_SN0P_REQHVNO, ps_sn0p_reqhvno);
F_PROT(PS_SN0P_REQNOMF, ps_sn0p_reqnomf);
 
// ---  class PS_SN1D
F_PROT(PS_SN1D_000, ps_sn1d_000);
F_PROT(PS_SN1D_999, ps_sn1d_999);
F_PROT(PS_SN1D_CTR, ps_sn1d_ctr);
F_PROT(PS_SN1D_DTR, ps_sn1d_dtr);
F_PROT(PS_SN1D_INI, ps_sn1d_ini);
F_PROT(PS_SN1D_RST, ps_sn1d_rst);
F_PROT(PS_SN1D_REQHBASE, ps_sn1d_reqhbase);
F_PROT(PS_SN1D_HVALIDE, ps_sn1d_hvalide);
F_PROT(PS_SN1D_ACC, ps_sn1d_acc);
F_PROT(PS_SN1D_FIN, ps_sn1d_fin);
F_PROT(PS_SN1D_XEQ, ps_sn1d_xeq);
F_PROT(PS_SN1D_ASGHSIM, ps_sn1d_asghsim);
F_PROT(PS_SN1D_REQNOMF, ps_sn1d_reqnomf);
 
// ---  class PS_SN1S
F_PROT(PS_SN1S_000, ps_sn1s_000);
F_PROT(PS_SN1S_999, ps_sn1s_999);
F_PROT(PS_SN1S_CTR, ps_sn1s_ctr);
F_PROT(PS_SN1S_DTR, ps_sn1s_dtr);
F_PROT(PS_SN1S_INI, ps_sn1s_ini);
F_PROT(PS_SN1S_RST, ps_sn1s_rst);
F_PROT(PS_SN1S_REQHBASE, ps_sn1s_reqhbase);
F_PROT(PS_SN1S_HVALIDE, ps_sn1s_hvalide);
F_PROT(PS_SN1S_ACC, ps_sn1s_acc);
F_PROT(PS_SN1S_FIN, ps_sn1s_fin);
F_PROT(PS_SN1S_XEQ, ps_sn1s_xeq);
F_PROT(PS_SN1S_ASGHSIM, ps_sn1s_asghsim);
F_PROT(PS_SN1S_REQNOMF, ps_sn1s_reqnomf);
 
// ---  class PS_SN1V
F_PROT(PS_SN1V_000, ps_sn1v_000);
F_PROT(PS_SN1V_999, ps_sn1v_999);
F_PROT(PS_SN1V_CTR, ps_sn1v_ctr);
F_PROT(PS_SN1V_DTR, ps_sn1v_dtr);
F_PROT(PS_SN1V_INI, ps_sn1v_ini);
F_PROT(PS_SN1V_RST, ps_sn1v_rst);
F_PROT(PS_SN1V_REQHBASE, ps_sn1v_reqhbase);
F_PROT(PS_SN1V_HVALIDE, ps_sn1v_hvalide);
F_PROT(PS_SN1V_ACC, ps_sn1v_acc);
F_PROT(PS_SN1V_FIN, ps_sn1v_fin);
F_PROT(PS_SN1V_XEQ, ps_sn1v_xeq);
F_PROT(PS_SN1V_ASGHSIM, ps_sn1v_asghsim);
F_PROT(PS_SN1V_REQNOMF, ps_sn1v_reqnomf);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_h2d2_post()
{
   static char libname[] = "h2d2_post";
 
   // ---  class IC_PS_COMP
   F_RGST(IC_PS_COMP_XEQCTR, ic_ps_comp_xeqctr, libname);
   F_RGST(IC_PS_COMP_XEQMTH, ic_ps_comp_xeqmth, libname);
   F_RGST(IC_PS_COMP_REQCLS, ic_ps_comp_reqcls, libname);
   F_RGST(IC_PS_COMP_REQHDL, ic_ps_comp_reqhdl, libname);
 
   // ---  class IC_PS_DLIB
   F_RGST(IC_PS_DLIB_XEQCTR, ic_ps_dlib_xeqctr, libname);
   F_RGST(IC_PS_DLIB_XEQMTH, ic_ps_dlib_xeqmth, libname);
   F_RGST(IC_PS_DLIB_REQCLS, ic_ps_dlib_reqcls, libname);
   F_RGST(IC_PS_DLIB_REQHDL, ic_ps_dlib_reqhdl, libname);
 
   // ---  class IC_PS_ELLPS
   F_RGST(IC_PS_ELLPS_XEQCTR, ic_ps_ellps_xeqctr, libname);
   F_RGST(IC_PS_ELLPS_XEQMTH, ic_ps_ellps_xeqmth, libname);
   F_RGST(IC_PS_ELLPS_REQCLS, ic_ps_ellps_reqcls, libname);
   F_RGST(IC_PS_ELLPS_REQHDL, ic_ps_ellps_reqhdl, libname);
 
   // ---  class IC_PS_HESS
   F_RGST(IC_PS_HESS_XEQCTR, ic_ps_hess_xeqctr, libname);
   F_RGST(IC_PS_HESS_XEQMTH, ic_ps_hess_xeqmth, libname);
   F_RGST(IC_PS_HESS_REQCLS, ic_ps_hess_reqcls, libname);
   F_RGST(IC_PS_HESS_REQHDL, ic_ps_hess_reqhdl, libname);
 
   // ---  class IC_PS_HNRM
   F_RGST(IC_PS_HNRM_XEQCTR, ic_ps_hnrm_xeqctr, libname);
   F_RGST(IC_PS_HNRM_XEQMTH, ic_ps_hnrm_xeqmth, libname);
   F_RGST(IC_PS_HNRM_REQCLS, ic_ps_hnrm_reqcls, libname);
   F_RGST(IC_PS_HNRM_REQHDL, ic_ps_hnrm_reqhdl, libname);
 
   // ---  class IC_PS_POST
   F_RGST(IC_PS_POST_XEQCTR, ic_ps_post_xeqctr, libname);
   F_RGST(IC_PS_POST_XEQMTH, ic_ps_post_xeqmth, libname);
   F_RGST(IC_PS_POST_REQCLS, ic_ps_post_reqcls, libname);
   F_RGST(IC_PS_POST_REQHDL, ic_ps_post_reqhdl, libname);
 
   // ---  class IC_PS_RESI
   F_RGST(IC_PS_RESI_XEQCTR, ic_ps_resi_xeqctr, libname);
   F_RGST(IC_PS_RESI_XEQMTH, ic_ps_resi_xeqmth, libname);
   F_RGST(IC_PS_RESI_REQCLS, ic_ps_resi_reqcls, libname);
   F_RGST(IC_PS_RESI_REQHDL, ic_ps_resi_reqhdl, libname);
 
   // ---  class IC_PS_SIMU
   F_RGST(IC_PS_SIMU_XEQCTR, ic_ps_simu_xeqctr, libname);
   F_RGST(IC_PS_SIMU_XEQMTH, ic_ps_simu_xeqmth, libname);
   F_RGST(IC_PS_SIMU_REQCLS, ic_ps_simu_reqcls, libname);
   F_RGST(IC_PS_SIMU_REQHDL, ic_ps_simu_reqhdl, libname);
 
   // ---  class IC_PS_SN0N
   F_RGST(IC_PS_SN0N_XEQCTR, ic_ps_sn0n_xeqctr, libname);
   F_RGST(IC_PS_SN0N_XEQMTH, ic_ps_sn0n_xeqmth, libname);
   F_RGST(IC_PS_SN0N_REQCLS, ic_ps_sn0n_reqcls, libname);
   F_RGST(IC_PS_SN0N_REQHDL, ic_ps_sn0n_reqhdl, libname);
 
   // ---  class IC_PS_SN0P
   F_RGST(IC_PS_SN0P_XEQCTR, ic_ps_sn0p_xeqctr, libname);
   F_RGST(IC_PS_SN0P_XEQMTH, ic_ps_sn0p_xeqmth, libname);
   F_RGST(IC_PS_SN0P_REQCLS, ic_ps_sn0p_reqcls, libname);
   F_RGST(IC_PS_SN0P_REQHDL, ic_ps_sn0p_reqhdl, libname);
 
   // ---  class IC_PS_SN1S
   F_RGST(IC_PS_SN1S_XEQCTR, ic_ps_sn1s_xeqctr, libname);
   F_RGST(IC_PS_SN1S_XEQMTH, ic_ps_sn1s_xeqmth, libname);
   F_RGST(IC_PS_SN1S_REQCLS, ic_ps_sn1s_reqcls, libname);
   F_RGST(IC_PS_SN1S_REQHDL, ic_ps_sn1s_reqhdl, libname);
 
   // ---  class IC_PS_SN1V
   F_RGST(IC_PS_SN1V_XEQCTR, ic_ps_sn1v_xeqctr, libname);
   F_RGST(IC_PS_SN1V_XEQMTH, ic_ps_sn1v_xeqmth, libname);
   F_RGST(IC_PS_SN1V_REQCLS, ic_ps_sn1v_reqcls, libname);
   F_RGST(IC_PS_SN1V_REQHDL, ic_ps_sn1v_reqhdl, libname);
 
   // ---  class IC_PS_XEQ
   F_RGST(IC_PS_XEQ_ACC, ic_ps_xeq_acc, libname);
   F_RGST(IC_PS_XEQ_FIN, ic_ps_xeq_fin, libname);
   F_RGST(IC_PS_XEQ_XEQ, ic_ps_xeq_xeq, libname);
 
   // ---  class PS_COMP
   F_RGST(PS_COMP_000, ps_comp_000, libname);
   F_RGST(PS_COMP_999, ps_comp_999, libname);
   F_RGST(PS_COMP_CTR, ps_comp_ctr, libname);
   F_RGST(PS_COMP_DTR, ps_comp_dtr, libname);
   F_RGST(PS_COMP_INI, ps_comp_ini, libname);
   F_RGST(PS_COMP_RST, ps_comp_rst, libname);
   F_RGST(PS_COMP_REQHBASE, ps_comp_reqhbase, libname);
   F_RGST(PS_COMP_HVALIDE, ps_comp_hvalide, libname);
   F_RGST(PS_COMP_ACC, ps_comp_acc, libname);
   F_RGST(PS_COMP_FIN, ps_comp_fin, libname);
   F_RGST(PS_COMP_XEQ, ps_comp_xeq, libname);
   F_RGST(PS_COMP_ASGHSIM, ps_comp_asghsim, libname);
   F_RGST(PS_COMP_REQHITM, ps_comp_reqhitm, libname);
   F_RGST(PS_COMP_REQHVNO, ps_comp_reqhvno, libname);
 
   // ---  class PS_DLIB
   F_RGST(PS_DLIB_000, ps_dlib_000, libname);
   F_RGST(PS_DLIB_999, ps_dlib_999, libname);
   F_RGST(PS_DLIB_CTR, ps_dlib_ctr, libname);
   F_RGST(PS_DLIB_DTR, ps_dlib_dtr, libname);
   F_RGST(PS_DLIB_INI, ps_dlib_ini, libname);
   F_RGST(PS_DLIB_RST, ps_dlib_rst, libname);
   F_RGST(PS_DLIB_REQHBASE, ps_dlib_reqhbase, libname);
   F_RGST(PS_DLIB_HVALIDE, ps_dlib_hvalide, libname);
   F_RGST(PS_DLIB_ACC, ps_dlib_acc, libname);
   F_RGST(PS_DLIB_FIN, ps_dlib_fin, libname);
   F_RGST(PS_DLIB_XEQ, ps_dlib_xeq, libname);
   F_RGST(PS_DLIB_ASGHSIM, ps_dlib_asghsim, libname);
   F_RGST(PS_DLIB_REQHVNO, ps_dlib_reqhvno, libname);
   F_RGST(PS_DLIB_REQNOMF, ps_dlib_reqnomf, libname);
 
   // ---  class PS_ELLPS
   F_RGST(PS_ELLPS_000, ps_ellps_000, libname);
   F_RGST(PS_ELLPS_999, ps_ellps_999, libname);
   F_RGST(PS_ELLPS_CTR, ps_ellps_ctr, libname);
   F_RGST(PS_ELLPS_DTR, ps_ellps_dtr, libname);
   F_RGST(PS_ELLPS_INI, ps_ellps_ini, libname);
   F_RGST(PS_ELLPS_RST, ps_ellps_rst, libname);
   F_RGST(PS_ELLPS_REQHBASE, ps_ellps_reqhbase, libname);
   F_RGST(PS_ELLPS_HVALIDE, ps_ellps_hvalide, libname);
   F_RGST(PS_ELLPS_ACC, ps_ellps_acc, libname);
   F_RGST(PS_ELLPS_FIN, ps_ellps_fin, libname);
   F_RGST(PS_ELLPS_XEQ, ps_ellps_xeq, libname);
   F_RGST(PS_ELLPS_ASGHSIM, ps_ellps_asghsim, libname);
   F_RGST(PS_ELLPS_REQHVNO, ps_ellps_reqhvno, libname);
   F_RGST(PS_ELLPS_REQNOMF, ps_ellps_reqnomf, libname);
 
   // ---  class PS_POST
   F_RGST(PS_POST_000, ps_post_000, libname);
   F_RGST(PS_POST_999, ps_post_999, libname);
   F_RGST(PS_POST_CTR, ps_post_ctr, libname);
   F_RGST(PS_POST_DTR, ps_post_dtr, libname);
   F_RGST(PS_POST_INI, ps_post_ini, libname);
   F_RGST(PS_POST_RST, ps_post_rst, libname);
   F_RGST(PS_POST_REQHBASE, ps_post_reqhbase, libname);
   F_RGST(PS_POST_HVALIDE, ps_post_hvalide, libname);
   F_RGST(PS_POST_REQHPST, ps_post_reqhpst, libname);
   F_RGST(PS_POST_ACC, ps_post_acc, libname);
   F_RGST(PS_POST_FIN, ps_post_fin, libname);
   F_RGST(PS_POST_XEQ, ps_post_xeq, libname);
   F_RGST(PS_POST_ASGHSIM, ps_post_asghsim, libname);
   F_RGST(PS_POST_REQHVNO, ps_post_reqhvno, libname);
 
   // ---  class PS_PSTD
   F_RGST(PS_PSTD_000, ps_pstd_000, libname);
   F_RGST(PS_PSTD_999, ps_pstd_999, libname);
   F_RGST(PS_PSTD_CTR, ps_pstd_ctr, libname);
   F_RGST(PS_PSTD_DTR, ps_pstd_dtr, libname);
   F_RGST(PS_PSTD_RAZ, ps_pstd_raz, libname);
   F_RGST(PS_PSTD_INI, ps_pstd_ini, libname);
   F_RGST(PS_PSTD_RST, ps_pstd_rst, libname);
   F_RGST(PS_PSTD_REQHBASE, ps_pstd_reqhbase, libname);
   F_RGST(PS_PSTD_HVALIDE, ps_pstd_hvalide, libname);
   F_RGST(PS_PSTD_LOGACC, ps_pstd_logacc, libname);
   F_RGST(PS_PSTD_XEQ, ps_pstd_xeq, libname);
   F_RGST(PS_PSTD_ASGHSIM, ps_pstd_asghsim, libname);
   F_RGST(PS_PSTD_REQHSIM, ps_pstd_reqhsim, libname);
   F_RGST(PS_PSTD_REQHVNO, ps_pstd_reqhvno, libname);
   F_RGST(PS_PSTD_REQNOMF, ps_pstd_reqnomf, libname);
   F_RGST(PS_PSTD_REQOPR, ps_pstd_reqopr, libname);
   F_RGST(PS_PSTD_REQOPW, ps_pstd_reqopw, libname);
   F_RGST(PS_PSTD_LOGABS, ps_pstd_logabs, libname);
   F_RGST(PS_PSTD_LOGVAL, ps_pstd_logval, libname);
   F_RGST(PS_PSTD_RDUC, ps_pstd_rduc, libname);
   F_RGST(PS_PSTD_RDUCFIN, ps_pstd_rducfin, libname);
   F_RGST(PS_PSTD_STR2OP, ps_pstd_str2op, libname);
   F_RGST(PS_PSTD_OP2STR, ps_pstd_op2str, libname);
 
   // ---  class PS_RESI
   F_RGST(PS_RESI_000, ps_resi_000, libname);
   F_RGST(PS_RESI_999, ps_resi_999, libname);
   F_RGST(PS_RESI_CTR, ps_resi_ctr, libname);
   F_RGST(PS_RESI_DTR, ps_resi_dtr, libname);
   F_RGST(PS_RESI_INI, ps_resi_ini, libname);
   F_RGST(PS_RESI_RST, ps_resi_rst, libname);
   F_RGST(PS_RESI_REQHBASE, ps_resi_reqhbase, libname);
   F_RGST(PS_RESI_HVALIDE, ps_resi_hvalide, libname);
   F_RGST(PS_RESI_ACC, ps_resi_acc, libname);
   F_RGST(PS_RESI_FIN, ps_resi_fin, libname);
   F_RGST(PS_RESI_XEQ, ps_resi_xeq, libname);
   F_RGST(PS_RESI_ASGHSIM, ps_resi_asghsim, libname);
   F_RGST(PS_RESI_REQHVNO, ps_resi_reqhvno, libname);
   F_RGST(PS_RESI_REQNOMF, ps_resi_reqnomf, libname);
 
   // ---  class PS_SIMU
   F_RGST(PS_SIMU_000, ps_simu_000, libname);
   F_RGST(PS_SIMU_999, ps_simu_999, libname);
   F_RGST(PS_SIMU_CTR, ps_simu_ctr, libname);
   F_RGST(PS_SIMU_DTR, ps_simu_dtr, libname);
   F_RGST(PS_SIMU_INI, ps_simu_ini, libname);
   F_RGST(PS_SIMU_RST, ps_simu_rst, libname);
   F_RGST(PS_SIMU_REQHBASE, ps_simu_reqhbase, libname);
   F_RGST(PS_SIMU_HVALIDE, ps_simu_hvalide, libname);
   F_RGST(PS_SIMU_ACC, ps_simu_acc, libname);
   F_RGST(PS_SIMU_FIN, ps_simu_fin, libname);
   F_RGST(PS_SIMU_XEQ, ps_simu_xeq, libname);
   F_RGST(PS_SIMU_ASGHSIM, ps_simu_asghsim, libname);
   F_RGST(PS_SIMU_REQHVNO, ps_simu_reqhvno, libname);
   F_RGST(PS_SIMU_REQNOMF, ps_simu_reqnomf, libname);
 
   // ---  class PS_SN0N
   F_RGST(PS_SN0N_000, ps_sn0n_000, libname);
   F_RGST(PS_SN0N_999, ps_sn0n_999, libname);
   F_RGST(PS_SN0N_CTR, ps_sn0n_ctr, libname);
   F_RGST(PS_SN0N_DTR, ps_sn0n_dtr, libname);
   F_RGST(PS_SN0N_INI, ps_sn0n_ini, libname);
   F_RGST(PS_SN0N_RST, ps_sn0n_rst, libname);
   F_RGST(PS_SN0N_REQHBASE, ps_sn0n_reqhbase, libname);
   F_RGST(PS_SN0N_HVALIDE, ps_sn0n_hvalide, libname);
   F_RGST(PS_SN0N_AJTNOD, ps_sn0n_ajtnod, libname);
   F_RGST(PS_SN0N_ACC, ps_sn0n_acc, libname);
   F_RGST(PS_SN0N_FIN, ps_sn0n_fin, libname);
   F_RGST(PS_SN0N_XEQ, ps_sn0n_xeq, libname);
   F_RGST(PS_SN0N_ASGHSIM, ps_sn0n_asghsim, libname);
   F_RGST(PS_SN0N_REQHVNO, ps_sn0n_reqhvno, libname);
   F_RGST(PS_SN0N_REQNOMF, ps_sn0n_reqnomf, libname);
 
   // ---  class PS_SN0P
   F_RGST(PS_SN0P_000, ps_sn0p_000, libname);
   F_RGST(PS_SN0P_999, ps_sn0p_999, libname);
   F_RGST(PS_SN0P_CTR, ps_sn0p_ctr, libname);
   F_RGST(PS_SN0P_DTR, ps_sn0p_dtr, libname);
   F_RGST(PS_SN0P_INI, ps_sn0p_ini, libname);
   F_RGST(PS_SN0P_RST, ps_sn0p_rst, libname);
   F_RGST(PS_SN0P_REQHBASE, ps_sn0p_reqhbase, libname);
   F_RGST(PS_SN0P_HVALIDE, ps_sn0p_hvalide, libname);
   F_RGST(PS_SN0P_AJTPNT, ps_sn0p_ajtpnt, libname);
   F_RGST(PS_SN0P_ACC, ps_sn0p_acc, libname);
   F_RGST(PS_SN0P_FIN, ps_sn0p_fin, libname);
   F_RGST(PS_SN0P_XEQ, ps_sn0p_xeq, libname);
   F_RGST(PS_SN0P_ASGHSIM, ps_sn0p_asghsim, libname);
   F_RGST(PS_SN0P_REQHVNO, ps_sn0p_reqhvno, libname);
   F_RGST(PS_SN0P_REQNOMF, ps_sn0p_reqnomf, libname);
 
   // ---  class PS_SN1D
   F_RGST(PS_SN1D_000, ps_sn1d_000, libname);
   F_RGST(PS_SN1D_999, ps_sn1d_999, libname);
   F_RGST(PS_SN1D_CTR, ps_sn1d_ctr, libname);
   F_RGST(PS_SN1D_DTR, ps_sn1d_dtr, libname);
   F_RGST(PS_SN1D_INI, ps_sn1d_ini, libname);
   F_RGST(PS_SN1D_RST, ps_sn1d_rst, libname);
   F_RGST(PS_SN1D_REQHBASE, ps_sn1d_reqhbase, libname);
   F_RGST(PS_SN1D_HVALIDE, ps_sn1d_hvalide, libname);
   F_RGST(PS_SN1D_ACC, ps_sn1d_acc, libname);
   F_RGST(PS_SN1D_FIN, ps_sn1d_fin, libname);
   F_RGST(PS_SN1D_XEQ, ps_sn1d_xeq, libname);
   F_RGST(PS_SN1D_ASGHSIM, ps_sn1d_asghsim, libname);
   F_RGST(PS_SN1D_REQNOMF, ps_sn1d_reqnomf, libname);
 
   // ---  class PS_SN1S
   F_RGST(PS_SN1S_000, ps_sn1s_000, libname);
   F_RGST(PS_SN1S_999, ps_sn1s_999, libname);
   F_RGST(PS_SN1S_CTR, ps_sn1s_ctr, libname);
   F_RGST(PS_SN1S_DTR, ps_sn1s_dtr, libname);
   F_RGST(PS_SN1S_INI, ps_sn1s_ini, libname);
   F_RGST(PS_SN1S_RST, ps_sn1s_rst, libname);
   F_RGST(PS_SN1S_REQHBASE, ps_sn1s_reqhbase, libname);
   F_RGST(PS_SN1S_HVALIDE, ps_sn1s_hvalide, libname);
   F_RGST(PS_SN1S_ACC, ps_sn1s_acc, libname);
   F_RGST(PS_SN1S_FIN, ps_sn1s_fin, libname);
   F_RGST(PS_SN1S_XEQ, ps_sn1s_xeq, libname);
   F_RGST(PS_SN1S_ASGHSIM, ps_sn1s_asghsim, libname);
   F_RGST(PS_SN1S_REQNOMF, ps_sn1s_reqnomf, libname);
 
   // ---  class PS_SN1V
   F_RGST(PS_SN1V_000, ps_sn1v_000, libname);
   F_RGST(PS_SN1V_999, ps_sn1v_999, libname);
   F_RGST(PS_SN1V_CTR, ps_sn1v_ctr, libname);
   F_RGST(PS_SN1V_DTR, ps_sn1v_dtr, libname);
   F_RGST(PS_SN1V_INI, ps_sn1v_ini, libname);
   F_RGST(PS_SN1V_RST, ps_sn1v_rst, libname);
   F_RGST(PS_SN1V_REQHBASE, ps_sn1v_reqhbase, libname);
   F_RGST(PS_SN1V_HVALIDE, ps_sn1v_hvalide, libname);
   F_RGST(PS_SN1V_ACC, ps_sn1v_acc, libname);
   F_RGST(PS_SN1V_FIN, ps_sn1v_fin, libname);
   F_RGST(PS_SN1V_XEQ, ps_sn1v_xeq, libname);
   F_RGST(PS_SN1V_ASGHSIM, ps_sn1v_asghsim, libname);
   F_RGST(PS_SN1V_REQNOMF, ps_sn1v_reqnomf, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
