C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER IC_PS_COMP_XEQCTR
C     INTEGER IC_PS_COMP_XEQMTH
C     CHARACTER*(32) IC_PS_COMP_REQCLS
C     INTEGER IC_PS_COMP_REQHDL
C   Private:
C     SUBROUTINE IC_PS_COMP_AID
C     INTEGER IC_PS_COMP_PRN
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_COMP_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_COMP_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pscomp_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pscomp_ic.fc'

      INTEGER IERR
      INTEGER HPST, HOBJ
      INTEGER I
      INTEGER NPOST
      INTEGER NPOSTMAX
      PARAMETER (NPOSTMAX = 20)
      INTEGER KPOST(NPOSTMAX)

      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_COMP_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      I = 0
100   CONTINUE
         IF (I .EQ. NPOSTMAX) GOTO 9901
         I = I + 1
         IERR = SP_STRN_TKI(IPRM, ',', I, KPOST(I))
         IF (IERR .EQ. -1) GOTO 199
         IF (IERR .NE.  0) GOTO 9902
      GOTO 100
199   CONTINUE
      NPOST = I-1

C---     VALIDE
      IF (NPOST .LE. 0) GOTO 9900

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR = PS_COMP_CTR(HPST)
      IF (ERR_GOOD()) IERR = PS_COMP_INI(HPST, NPOST, KPOST)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_COMP_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>post_compose</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF,'(A)') 'ERR_DEBORDEMENT_TAMPON'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF,'(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                      IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)

9988  CONTINUE
      CALL IC_PS_COMP_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_PS_COMP_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_COMP_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_COMP_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pscomp_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pscomp_ic.fc'

      INTEGER IERR
      INTEGER IVAL, HVAL
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>Get an item with its index</comment>
      IF (IMTH .EQ. '##opb_[]_get##') THEN
D        CALL ERR_ASR(PS_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900

C        <comment>Index of the item</comment>
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', 1, IVAL)
         IF (IERR .NE. 0) GOTO 9901
         IF (ERR_GOOD()) HVAL = PS_COMP_REQHITM(HOBJ, IVAL)
C        <comment>The sequence item</comment>
         IF (ERR_GOOD()) WRITE(IPRM, '(2A,I12)') 'H', ',', HVAL

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_COMP_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_COMP_PRN(HOBJ)
         IERR = IC_PS_COMP_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_COMP_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_COMP_AID()

9999  CONTINUE
      IC_PS_COMP_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_COMP_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_COMP_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pscomp_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C  The class <b>post_compose</b> composes the post-treatments.
C</comment>
      IC_PS_COMP_REQCLS = 'post_compose'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_COMP_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_COMP_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pscomp_ic.fi'
      INCLUDE 'pscomp.fi'
C-------------------------------------------------------------------------

      IC_PS_COMP_REQHDL = PS_COMP_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_COMP_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_COMP_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pscomp_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_COMP_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pscomp.fc'
      INCLUDE 'pscomp_ic.fc'

      INTEGER IERR
      INTEGER IOB, I
      INTEGER NPOST

      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_CMD_POST_COMPOSE'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PS_COMP_HBASE
      NPOST = PS_COMP_NALGO(IOB)

C---     IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
         IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
         WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                            NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)

         WRITE (LOG_BUF,'(2A,I12)') 'MSG_NBR_OBJ_POST_COMPOSE#<25>#',
     &                              '= ', NPOST
         CALL LOG_ECRIS(LOG_BUF)

         IERR = OB_OBJC_REQNOMCMPL(NOM, PS_COMP_HALGO(IOB,1))
         WRITE (LOG_BUF,'(3A)') 'MSG_HANDLE#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
         DO I=2,NPOST
            IERR = OB_OBJC_REQNOMCMPL(NOM, PS_COMP_HALGO(IOB,I))
            WRITE (LOG_BUF,'(3A)') '#<25>#', '  ',
     &                              NOM(1:SP_STRN_LEN(NOM))
            CALL LOG_ECRIS(LOG_BUF)
         ENDDO
      ENDIF

      CALL LOG_DECIND()

      IC_PS_COMP_PRN = ERR_TYP()
      RETURN
      END
