C************************************************************************
C --- Copyright (c) INRS 2005-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0P_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0P_XEQCTR
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pssn0p_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dslist.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'pssn0p.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0p_ic.fc'

      INTEGER, PARAMETER :: NDIM = 2

      REAL*8  XY(NDIM)
      INTEGER IERR, IRET
      INTEGER IK, IN, IC, ISTAT
      INTEGER HPST, HOBJ
      INTEGER IOPR, IOPW
      INTEGER HVNO, ICOL
      INTEGER HLST, NPNT
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NTMP
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
      TYPE (SO_ALLC_VPTR2_T) :: PCOR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_SN0P_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IK = 0
C     <comment>Handle on the nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, HVNO)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Index of column of nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, ICOL)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Handle on points list, or number of points to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, NPNT)
      IF (IRET .NE. 0) GOTO 9901

C---     Valide
      HLST = 0
      IF (ICOL .LE. 0) GOTO 9902
      IF (DS_LIST_HVALIDE(NPNT)) THEN
         HLST = NPNT
         NPNT = DS_LIST_REQDIM(HLST) / 2
      ENDIF
      IF (HLST .EQ. 0 .AND. NPNT .LE. 0) GOTO 9903

C---     Dimensionne les tables
      IF (ERR_GOOD()) PCOR = SO_ALLC_VPTR2(NDIM, NPNT)

C---     Lis les coordonnées des points
      IF (DS_LIST_HVALIDE(HLST)) THEN
         IC = 0
         DO IN=1,NPNT
            IC = IC + 1
            IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLST, IC, NTMP)
            IF (ERR_GOOD()) IERR = SP_STRN_TKR(NTMP, ',', 1, XY(1))
            IC = IC + 1
            IF (ERR_GOOD()) IERR = DS_LIST_REQVAL(HLST, IC, NTMP)
            IF (ERR_GOOD()) IERR = SP_STRN_TKR(NTMP, ',', 1, XY(2))
            PCOR%VPTR(:,IN) = XY(:)
         ENDDO
      ELSE
         DO IN=1,NPNT
C           <comment>x, y of points to probe, comma separated</comment>
            IK = IK + 1
            IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY(1))     ! Utilise XY(I) pour qu'à l'extraction
            IF (IRET .NE. 0) GOTO 9901                                    ! de la commande on voit une liste
            IK = IK + 1
            IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY(2))     ! Utilise XY(I) pour qu'à l'extraction
            IF (IRET .NE. 0) GOTO 9901                                    ! de la commande on voit une liste
            PCOR%VPTR(:,IN) = XY(:)
         ENDDO
      ENDIF

C---     Lis la suite des paramètres
C     <comment>Name of the post-treatment file</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, MODE)
      IF (IRET .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, OPRD)
      IF (IRET .NE. 0) OPRD = 'noop'
C     <comment>Writing operation (default 'nowrite')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, OPWR)
      IF (IRET .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9904
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9906
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9907
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9908

C---     Construis et initialise l'objet
      HPST = 0
      IF (ERR_GOOD()) IERR=PS_SN0P_CTR(HPST)
      IF (ERR_GOOD()) IERR=PS_SN0P_INI(HPST,
     &                                 NOMFIC,ISTAT,IOPR,IOPW,
     &                                 NDIM,
     &                                 NPNT,
     &                                 PCOR%VPTR,
     &                                 HVNO,
     &                                 ICOL)

C---     Construis et initialise le proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_SN0P_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>probe_on_points</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C  <p>
C  The constructor can accept:
C  <ul>
C     <li>either a handle on a list of points coordinates,</li>
C     <li>or the enumeration of the coordinates</li>
C  </ul>
C  as in:
C  <ul>
C     <li>probe_on_points(hvno, icol, hlst, ....)</li>
C     <li>probe_on_points(hvno, icol, nptn, x1, y1, x2, y2, ....)</li>
C  </ul>
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_INDICE_INVALIDE', ': ', ICOL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9908  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0P_AID()

9999  CONTINUE
      CALL ERR_PUSH()
      PCOR = SO_ALLC_VPTR2()
      CALL ERR_POP()
      IC_PS_SN0P_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0P_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0P_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pssn0p_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0p.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0p_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>add</b> add points to the list of points to probe.</comment>
      IF (IMTH .EQ. 'add') THEN
D        CALL ERR_ASR(PS_SN0P_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9901
C        <include>IC_PS_SN0P_XEQMTH_ADD@pssn0p_ic.for</include>
         IERR = IC_PS_SN0P_XEQMTH_ADD(HOBJ, IPRM)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_SN0P_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_SN0P_PRN(HOBJ)
         IERR = IC_PS_SN0P_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_SN0P_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0P_AID()

9999  CONTINUE
      IC_PS_SN0P_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0P_XEQMTH_ADD(HOBJ, IPRM)

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'pssn0p_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0p.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0p_ic.fc'

      INTEGER, PARAMETER :: NDIM = 2
      
      REAL*8  XY(NDIM)
      INTEGER IERR, IRET
      INTEGER IK, IN
      INTEGER HVNO, ICOL
      INTEGER NPNT
      TYPE (SO_ALLC_VPTR2_T) :: PCOR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IK = 0
C     <comment>Handle on the nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, HVNO)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Index of column of nodal values to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, ICOL)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Number of points to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, NPNT)
      IF (IRET .NE. 0) GOTO 9901

C---     Valide
      IF (ICOL .LE. 0) GOTO 9902
      IF (NPNT .LE. 0) GOTO 9903

C---     Dimensionne les tables
      IF (ERR_GOOD()) PCOR = SO_ALLC_VPTR2(NDIM, NPNT)

C---     Lis la suite des paramètres
      DO IN=1,NPNT
C        <comment>x, y of points to probe, comma separated</comment>
         IK = IK + 1
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY(1))     ! Utilise XY(I) pour qu'à l'extraction
         IF (IRET .NE. 0) GOTO 9901                                    ! de la commande on voit une liste
         IK = IK + 1
         IF (IRET .EQ. 0) IRET = SP_STRN_TKR(IPRM, ',', IK, XY(2))     ! Utilise XY(I) pour qu'à l'extraction
         IF (IRET .NE. 0) GOTO 9901                                    ! de la commande on voit une liste
         PCOR%VPTR(:,IN) = XY(:)
      ENDDO

C---     Valide
      IF (ICOL .LE. 0) GOTO 9903

C---     Construis et initialise l'objet
      IF (ERR_GOOD()) IERR=PS_SN0P_AJTPNT(HOBJ,
     &                                    NDIM,
     &                                    NPNT,
     &                                    PCOR%VPTR)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:MIN(80,SP_STRN_LEN(IPRM)))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_INDICE_INVALIDE', ': ', ICOL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0P_AID()

9999  CONTINUE
      CALL ERR_PUSH()
      PCOR = SO_ALLC_VPTR2()
      CALL ERR_POP()
      IC_PS_SN0P_XEQMTH_ADD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN0P_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0P_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn0p_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>probe_on_points</b> represents the post-treatment that
C   implements a numerical probe applied on points (as opposed to nodes).
C</comment>
      IC_PS_SN0P_REQCLS = 'probe_on_points'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN0P_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0P_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn0p_ic.fi'
      INCLUDE 'pssn0p.fi'
C-------------------------------------------------------------------------

      IC_PS_SN0P_REQHDL = PS_SN0P_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_SN0P_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_SN0P_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pssn0p_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0P_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn0p.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0p_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_PROBE_ON_POINTS'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_SN0P_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PS_SN0P_PRN = ERR_TYP()
      RETURN
      END
