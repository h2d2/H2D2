C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: PS_RESI
C     INTEGER PS_RESI_000
C     INTEGER PS_RESI_999
C     INTEGER PS_RESI_CTR
C     INTEGER PS_RESI_DTR
C     INTEGER PS_RESI_INI
C     INTEGER PS_RESI_RST
C     INTEGER PS_RESI_REQHBASE
C     LOGICAL PS_RESI_HVALIDE
C     INTEGER PS_RESI_ACC
C     INTEGER PS_RESI_FIN
C     INTEGER PS_RESI_XEQ
C     INTEGER PS_RESI_LOG
C     INTEGER PS_RESI_ASGHSIM
C     INTEGER PS_RESI_REQHVNO
C     CHARACTER*256 PS_RESI_REQNOMF
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_RESI_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_RESI_NOBJMAX,
     &                   PS_RESI_HBASE,
     &                   'Post-Treatment Residuum')

      PS_RESI_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_RESI_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER  IERR
      EXTERNAL PS_RESI_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_RESI_NOBJMAX,
     &                   PS_RESI_HBASE,
     &                   PS_RESI_DTR)

      PS_RESI_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_RESI_NOBJMAX,
     &                   PS_RESI_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_RESI_HVALIDE(HOBJ))
         IOB = HOBJ - PS_RESI_HBASE

         PS_RESI_HPRNT(IOB) = 0
      ENDIF

      PS_RESI_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_RESI_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_RESI_NOBJMAX,
     &                   PS_RESI_HBASE)

      PS_RESI_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'psresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psresi.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PS_RESI_RST(HOBJ)

C---     CONSTRUIT ET INITIALISE LE PARENT
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_RESI_HBASE
         PS_RESI_HPRNT(IOB) = HPRNT
      ENDIF

      PS_RESI_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_RESI_HBASE

C---     DETRUIS LES DONNÉES
      HPRNT = PS_RESI_HPRNT(IOB)
      IF (PS_PSTD_HVALIDE(HPRNT)) IERR = PS_PSTD_DTR(HPRNT)

C---     RESET
      IF (ERR_GOOD()) PS_RESI_HPRNT(IOB) = 0

      PS_RESI_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_RESI_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_RESI_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psresi.fi'
      INCLUDE 'psresi.fc'
C------------------------------------------------------------------------

      PS_RESI_REQHBASE = PS_RESI_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_RESI_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_RESI_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psresi.fc'
C------------------------------------------------------------------------

      PS_RESI_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_RESI_NOBJMAX,
     &                                  PS_RESI_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_ACC
CDEC$ ENDIF

      USE LM_HELE_M, ONLY: LM_HELE_REQEDTA, LM_HELE_REQGDTA
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1_T, SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spelem.fi'
      INCLUDE 'tgtrig.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER HSIM, HVNO
      INTEGER HPRNT
      INTEGER HNUMR
      INTEGER NNL_D, NPST_D, LVNO_D
      REAL*8  TSIM
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (SO_ALLC_VPTR1_T) :: P_DLU
      TYPE (SO_ALLC_VPTR1_T) :: P_SOL
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.residuum'

C---     Récupère les attributs
      IOB = HOBJ - PS_RESI_HBASE
      HPRNT = PS_RESI_HPRNT(IOB)
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)

C---     Récupère les données de DDL.
      GDTA => LM_HELE_REQGDTA(HSIM)
      EDTA => LM_HELE_REQEDTA(HSIM)
      TSIM  = LM_HELE_REQTEMPS(HSIM)
D     CALL ERR_ASR(EDTA%NEQL .GT. 0)

C---     Récupère les données du VNO dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Contrôles
      IF (NNL_D  .NE. GDTA%NNL)  GOTO 9900
      IF (NPST_D .NE. EDTA%NDLN) GOTO 9901

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_RESIDU',
     &                                      TSIM)

C---     Alloue l'espace pour la solution
      IF (ERR_GOOD()) P_DLU = SO_ALLC_VPTR1(EDTA%NDLL)
      IF (ERR_GOOD()) P_SOL = SO_ALLC_VPTR1(EDTA%NEQL)

C---     Calcul du résidu
      IF (ERR_GOOD()) THEN
         IERR = LM_HELE_ASMFKU(HSIM, P_SOL%VPTR)
!!!         CALL LOG_TODO('Essai avec LM_HELE_ASMRES pour le GPU')
!!!         IERR = LM_HELE_ASMRES(HSIM, VA(SO_ALLC_REQVIND(VA,L_SOL)))
      ENDIF

C---     Passe de NEQ a NDLL
      IF (ERR_GOOD()) THEN
         P_DLU%VPTR(:) = 0.0D0
         IERR = SP_ELEM_NEQ2NDLT(EDTA%NEQL,
     &                           EDTA%NDLL,
     &                           EDTA%KLOCN,
     &                           P_SOL%VPTR,
     &                           P_DLU%VPTR)
      ENDIF

!!C---     SYNCHRONISE LES PROCESS
!!      IF (ERR_GOOD() .AND. NM_NUMR_CTRLH(HNUMR)) THEN
!!         IERR = NM_NUMR_DSYNC(HNUMR,
!!     &                        NDLN,
!!     &                        NNL,
!!     &                        KA(SO_ALLC_REQKIND(KA,L_DLU)))
!!      ENDIF

!!C---     TRAITEMENT POST-RESO
!!      IF (ERR_GOOD()) THEN
!!         IERR = LM_HELE_CLCPST(HSIM,
!!     &                         VA(SO_ALLC_REQVIND(VA,L_DLU)))
!!      ENDIF

C---     OPERATION DE RÉDUCTION DANS L_DLU
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       EDTA%NDLL,
     &                       PVNO_D%VPTR, 1,
     &                       P_DLU%VPTR, 1)
      ENDIF

C---     MET À JOUR LES DONNÉES
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                          HNUMR,
     &                          P_DLU%VPTR,
     &                          TSIM)
      ENDIF

C---     ECRIS LES DONNÉES
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

C---     RECUPERE L'ESPACE
      IF (ERR_GOOD()) P_SOL = SO_ALLC_VPTR1()
      IF (ERR_GOOD()) P_DLU = SO_ALLC_VPTR1()

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               GDTA%NNL, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INCOMPATIBLES', ': ',
     &                               EDTA%NDLN, '/', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_RESI_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C
C************************************************************************
      FUNCTION PS_RESI_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_FIN
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNT_D, NNL_D, NPST_D, LVNO_D
      INTEGER NEQL, NEQP
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - PS_RESI_HBASE
      HPRNT = PS_RESI_HPRNT(IOB)
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du VNO destination
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NNT_D  = DT_VNOD_REQNNT (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère les données de la simulation
      HNUMR = LM_HELE_REQHNUMC(HSIM)
      NEQL  = LM_HELE_REQPRM(HSIM, LM_ELEM_PRM_NEQL)
      NEQP  = LM_HELE_REQPRM(HSIM, LM_ELEM_PRM_NEQP)

C---     CONTROLES
      IF (NNL_D  .LE. 0) GOTO 9901
      IF (NPST_D .LE. 0) GOTO 9902

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     LOG
      IF (ERR_GOOD()) THEN
         IERR = PS_RESI_LOG(HNUMR,
     &                      NEQL,
     &                      NEQP,
     &                      NNT_D,
     &                      NPST_D,
     &                      NNL_D,
     &                      PVNO_D%VPTR)
      ENDIF

C---     ECRIS LES DONNÉES
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_RESI_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psresi.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'psresi.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère le handle des DDL et le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_RESI_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_RESI_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_RESI_FIN(HOBJ)

      PS_RESI_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     VRES est en NDLN*NNL, mais seul NEQP entrées sont non nulles
C************************************************************************
      FUNCTION PS_RESI_LOG(HNUMR, NEQL, NEQP, NNT, NDLN, NNL, VRES)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER NEQL
      INTEGER NEQP
      INTEGER NNT
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VRES(NDLN, NNL)

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'psresi.fc'

      REAL*8  DNRM2     ! Fonctions BLAS
      INTEGER IDAMAX

      REAL*8  RL2_L, RL2_G
      REAL*8  RMX_L, RMX_G

      INTEGER IERR
      INTEGER ID
      INTEGER NMX_L, NMX_G
      INTEGER NDLL, NEQT

      INTEGER I_ERROR
      INTEGER I_OPSIGNEDMAX
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_NUMR_CTRLH(HNUMR))
D     CALL ERR_PRE(NDLN .GT. 0)
D     CALL ERR_PRE(NNL  .GT. 0)
C------------------------------------------------------------------------

      LOG_ZNE = 'h2d2.post.residuum'

C---     Entête
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') 'MSG_POST_RESIDU:'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()

C---     Demande l'OP de reduction
      I_OPSIGNEDMAX = MP_UTIL_GETOP_SIGNEDMAX()

C---     Normes tous DDL et synchro
      NDLL = NDLN*NNL
      RL2_L = DNRM2(NDLL, VRES, 1)     ! c.f. note
      RL2_L = RL2_L*RL2_L
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(RL2_L, RL2_G, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
         CALL MPI_ALLREDUCE(NEQP, NEQT, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF

C---     Valeurs tous DDL
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NDLN#<40>#= ', NDLN
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NNL#<40>#= ', NNL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NNT#<40>#= ', NNT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NEQP#<40>#= ', NEQP
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NEQL#<40>#= ', NEQL
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')    'MSG_NEQT#<40>#= ', NEQT
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      WRITE(LOG_BUF,'(A,1PE14.6E3)') 'MSG_NORME_L2_DU_PROCESS#<40>#= ',
     &                              SQRT(RL2_L / NEQP)
      CALL LOG_DBG(LOG_ZNE, LOG_BUF)
      WRITE (LOG_BUF,'(A,1PE14.6E3)')'MSG_NORME_L2#<40>#= ',
     &                              SQRT(RL2_G / NEQT)
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)

C---     Normes pour chaque DDL
      WRITE (LOG_BUF,'(A)') 'MSG_NORMES_PAR_DDL:'
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      DO ID=1, NDLN
         RL2_L  = DNRM2 (NNL, VRES(ID,1), NDLN)
         RL2_L  = RL2_L*RL2_L
         NMX_L  = IDAMAX(NNL, VRES(ID,1), NDLN)
         RMX_L  = VRES(ID,NMX_L)
         NMX_L = NM_NUMR_REQNEXT(HNUMR, NMX_L)     ! En num. globale

         CALL MPI_ALLREDUCE(RL2_L, RL2_G, 1, MP_TYPE_RE8(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
         CALL MPI_ALLREDUCE(RMX_L, RMX_G, 1, MP_TYPE_RE8(),
     &                      I_OPSIGNEDMAX, MP_UTIL_REQCOMM(), I_ERROR)
         IF (RMX_G .EQ. RMX_L) NMX_L = NMX_L + NNT
         CALL MPI_ALLREDUCE(NMX_L, NMX_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NMX_L = NMX_L - NNT
         NMX_G = NMX_G - NNT


         WRITE (LOG_BUF,'(A,1PE14.6E3,A,I2,A,I7,A,A,1PE14.6E3,A)')
     &      '(MAX)= ', RMX_L,' ; MSG_DLIB: (', ID, ', ', NMX_L, ')',
     &      '  (L2)= ', SQRT(RL2_L/NNL), ' (/nnl) local'
         CALL LOG_DBG(LOG_ZNE, LOG_BUF)
         WRITE (LOG_BUF,'(A,1PE14.6E3,A,I2,A,I7,A,A,1PE14.6E3,A)')
     &      '(MAX)= ', RMX_G,' ; MSG_DLIB: (', ID, ', ', NMX_G, ')',
     &      '  (L2)= ', SQRT(RL2_G/NNT), ' (/nnt)'
         CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      PS_RESI_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_RESI_ASGHSIM
C
C Description:
C     La fonction PS_RESI_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psresi.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER HPRNT
      INTEGER NPST, NNL, NNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Le nombre de variables de post-traitement
      NPST = LM_HELE_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NNL  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NNT  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)

      HPRNT = PS_RESI_HPRNT(HOBJ-PS_RESI_HBASE)
      PS_RESI_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NNL, NNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_RESI_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_RESI_HPRNT(HOBJ-PS_RESI_HBASE)
      PS_RESI_REQHVNO = PS_PSTD_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_RESI_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_RESI_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_RESI_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psresi.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psresi.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_RESI_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_RESI_HPRNT(HOBJ-PS_RESI_HBASE)
      PS_RESI_REQNOMF = PS_PSTD_REQNOMF(HPRNT)
      RETURN
      END
