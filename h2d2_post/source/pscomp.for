C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: PS_COMP
C     INTEGER PS_COMP_000
C     INTEGER PS_COMP_999
C     INTEGER PS_COMP_CTR
C     INTEGER PS_COMP_DTR
C     INTEGER PS_COMP_INI
C     INTEGER PS_COMP_RST
C     INTEGER PS_COMP_REQHBASE
C     LOGICAL PS_COMP_HVALIDE
C     INTEGER PS_COMP_ACC
C     INTEGER PS_COMP_FIN
C     INTEGER PS_COMP_XEQ
C     INTEGER PS_COMP_REQHVNO
C
C************************************************************************

C************************************************************************
C Sommaire: Initialise la classe
C
C Description:
C     La fonction <code>PS_COMP_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_COMP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_COMP_NOBJMAX,
     &                   PS_COMP_HBASE,
     &                   'Post-Treatment Compose')

      PS_COMP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_COMP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fc'

      INTEGER  IERR
      EXTERNAL PS_COMP_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_COMP_NOBJMAX,
     &                   PS_COMP_HBASE,
     &                   PS_COMP_DTR)

      PS_COMP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_COMP_NOBJMAX,
     &                   PS_COMP_HBASE)

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_COMP_HVALIDE(HOBJ))
         IOB = HOBJ - PS_COMP_HBASE

         PS_COMP_NALGO(IOB) = 0
      ENDIF

      PS_COMP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fc'

      INTEGER  IERR
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_COMP_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_COMP_NOBJMAX,
     &                   PS_COMP_HBASE)

      PS_COMP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_INI(HOBJ, NPOST, KPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NPOST
      INTEGER KPOST(*)

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLE DES PARAMETRES
      IF (NPOST .LT. 0) GOTO 9900
      IF (NPOST .GT. PS_COMP_NPOSTMAX) GOTO 9901
      DO I=1, NPOST
         IF (.NOT. PS_POST_HVALIDE(KPOST(I))) GOTO 9902
      ENDDO

C---     RESET LES DONNEES
      IERR = PS_COMP_RST(HOBJ)

C---     ASSIGNE LES VALEURS
      IOB = HOBJ - PS_COMP_HBASE
      DO I = 1, NPOST
         PS_COMP_HALGO(IOB, I) = KPOST(I)
      ENDDO
      PS_COMP_NALGO(IOB) = NPOST

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I3)')'ERR_NBR_HANDLE_INVALIDE',':',NPOST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF,'(2A,I3,A,I3)')'ERR_NBR_HANDLE_TROP_GRAND',':',
     &                           NPOST, '/', PS_COMP_NPOSTMAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF,'(A)')'ERR_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_HANDLE',':',KPOST(I)
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF,'(2A,I12)')'MSG_INDICE',':',I
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_COMP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IOB
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB = HOBJ - PS_COMP_HBASE
      PS_COMP_NALGO(IOB) = 0

      PS_COMP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_COMP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_COMP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pscomp.fi'
      INCLUDE 'pscomp.fc'
C------------------------------------------------------------------------

      PS_COMP_REQHBASE = PS_COMP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_COMP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_COMP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pscomp.fc'
C------------------------------------------------------------------------

      PS_COMP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_COMP_NOBJMAX,
     &                                  PS_COMP_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_COMP_HBASE

C---     BOUCLE SUR LES POST-TRAITEMENTS
      DO IALG = 1, PS_COMP_NALGO(IOB)
         IF (ERR_GOOD())
     &      IERR = PS_POST_ACC(PS_COMP_HALGO(IOB, IALG))
      ENDDO

      PS_COMP_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_COMP_HBASE

C---     BOUCLE SUR LES POST-TRAITEMENTS
      DO IALG = 1, PS_COMP_NALGO(IOB)
         IF (ERR_GOOD())
     &      IERR = PS_POST_FIN(PS_COMP_HALGO(IOB, IALG))
      ENDDO

      PS_COMP_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_XEQ(HOBJ, HSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSOL

      INCLUDE 'pscomp.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_COMP_HBASE

C---     BOUCLE SUR LES POST-TRAITEMENTS
      DO IALG = 1, PS_COMP_NALGO(IOB)
         IF (ERR_GOOD())
     &      IERR = PS_POST_XEQ(PS_COMP_HALGO(IOB, IALG), HSOL)
      ENDDO

      PS_COMP_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_COMP_REQHVNO
C
C Description:
C     La fonction PS_COMP_ASGHSIM assigne l'élément du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM       Handle sur l'élément
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pscomp.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IALG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_COMP_HBASE

C---     BOUCLE SUR LES POST-TRAITEMENTS
      DO IALG = 1, PS_COMP_NALGO(IOB)
         IF (ERR_GOOD())
     &      IERR = PS_POST_ASGHSIM(PS_COMP_HALGO(IOB, IALG), HSIM)
      ENDDO

      PS_COMP_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_COMP_REQHITM
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_REQHITM(HOBJ, IND)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_REQHITM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER IND

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'

      INTEGER IOB
      INTEGER HITM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_COMP_HBASE
      IF (IND .GE. 0 .AND. IND .LE. PS_COMP_NALGO(IOB)) THEN
         HITM = PS_COMP_HALGO(IOB, IND)
      ELSE
         HITM = 0
      ENDIF

      PS_COMP_REQHITM = HITM
      RETURN
      END

C************************************************************************
C Sommaire: PS_COMP_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_COMP_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_COMP_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pscomp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pscomp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_COMP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_ERR, 'ERR_REQ_INVALIDE')
      PS_COMP_REQHVNO = 0
      RETURN
      END
