C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0N_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0N_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'pssn0n_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'pssn0n.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n_ic.fc'

      INTEGER IERR, IRET
      INTEGER I, IK, IN, INOD, ISTAT
      INTEGER HPST, HOBJ
      INTEGER HGRD, IOPR, IOPW
      INTEGER NNOD, IDDL
      INTEGER LNOD, LDDL
      INTEGER KNOD(1)
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NTMP
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

      LNOD = 0
      LDDL = 0

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_SN0N_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     Lis les paramètres
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IK = 0
C     <comment>Handle on the mesh</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, HGRD)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Degrees of freedom to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, IDDL)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Number of nodes to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, NNOD)
      IF (IRET .NE. 0) GOTO 9901

C---     Valide
      IF (IDDL .LE. 0) GOTO 9902
      IF (NNOD .LE. 0) GOTO 9902

C---     Dimensionne les tables
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOD, LNOD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOD, LDDL)

C---     Lis la suite des paramètres
      I = 1
      DO IN=1,NNOD
C        <comment>List of nodes to probe, comma separated</comment>
         IK = IK + 1
         IF (IRET .EQ. 0) IRET = SP_STRN_TKI(IPRM, ',', IK, KNOD(I))     ! Utilise KNOD(I) pour qu'à l'extraction
         IF (IRET .NE. 0) GOTO 9901                                      ! de la commande on voit une liste
         IERR = SO_ALLC_ASGIN4(LNOD, IN, KNOD(I))
         IERR = SO_ALLC_ASGIN4(LDDL, IN, IDDL)
      ENDDO
C     <comment>Name of the post-treatment file</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, NOMFIC)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, MODE)
      IF (IRET .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, OPRD)
      IF (IRET .NE. 0) OPRD = 'noop'
C     <comment>Writing operation ['write', 'nowrite'] (default 'nowrite')</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IRET = SP_STRN_TKS(IPRM, ',', IK, OPWR)
      IF (IRET .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     Valide
      DO IN=1,NNOD
         INOD = SO_ALLC_REQIN4(LNOD, IN)
         IF (INOD .LE. 0) GOTO 9902
         IDDL = SO_ALLC_REQIN4(LDDL, IN)
         IF (IDDL .LE. 0) GOTO 9903
      ENDDO
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9904
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9906
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9907
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9908

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR=PS_SN0N_CTR(HPST)
      IF (ERR_GOOD()) IERR=PS_SN0N_INI(HPST,
     &                                 NOMFIC,ISTAT,IOPR,IOPW,
     &                                 HGRD,
     &                                 NNOD,
     &                                 KA(SO_ALLC_REQKIND(KA,LNOD)),
     &                                 KA(SO_ALLC_REQKIND(KA,LDDL)))

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_SN0N_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>probe_on_nodes</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNO_INVALIDE', ': ', INOD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_DDL_INVALIDE', ': ', IDDL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9907  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9908  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0N_AID()

9999  CONTINUE
      CALL ERR_PUSH()
      IF (LNOD .NE. 0) IERR = SO_ALLC_ALLINT(0, LNOD)
      IF (LDDL .NE. 0) IERR = SO_ALLC_ALLINT(0, LDDL)
      CALL ERR_POP()
      IC_PS_SN0N_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0N_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0N_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'pssn0n_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0n.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>add</b> add nodes to the list of nodes to probe.</comment>
      IF (IMTH .EQ. 'add') THEN
D        CALL ERR_ASR(PS_SN0N_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9901
C        <include>IC_PS_SN0N_XEQMTH_ADD@pssn0n_ic.for</include>
         IERR = IC_PS_SN0N_XEQMTH_ADD(HOBJ, IPRM)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_SN0N_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_SN0N_PRN(HOBJ)
         IERR = IC_PS_SN0N_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_SN0N_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0N_AID()

9999  CONTINUE
      IC_PS_SN0N_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0N_XEQMTH_ADD(HOBJ, IPRM)

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IPRM

      INCLUDE 'pssn0n_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0n.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n_ic.fc'

      INTEGER IERR, IRET
      INTEGER I, IK, IN
      INTEGER HGRD
      INTEGER NNOD, INOD, IDDL
      INTEGER LNOD, LDDL
      INTEGER KNOD(1)
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0N_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK
      IRET = 0

      LNOD = 0
      LDDL = 0

C---     Lis les param
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
      IK = 0
C     <comment>Handle on the mesh</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', IK, HGRD)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Degrees of freedom to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', IK, IDDL)
      IF (IRET .NE. 0) GOTO 9901
C     <comment>Number of nodes to probe</comment>
      IK = IK + 1
      IF (IRET .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', IK, NNOD)
      IF (IRET .NE. 0) GOTO 9901

C---     Valide
      IF (IDDL .LE. 0) GOTO 9902
      IF (NNOD .LE. 0) GOTO 9902

C---     Dimensionne les tables
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOD, LNOD)
      IF (ERR_GOOD()) IERR = SO_ALLC_ALLINT(NNOD, LDDL)

C---     Lis la suite des paramètres
      I = 1
      DO IN=1,NNOD
C        <comment>List of nodes to probe, comma separated</comment>
         IK = IK + 1
         IF (IERR .EQ. 0) IERR = SP_STRN_TKI(IPRM, ',', IK, KNOD(I))     ! Utilise KNOD(I) pour qu'à l'extraction
         IF (IERR .NE. 0) GOTO 9901                                      ! de la commande on voit une liste
         IERR = SO_ALLC_ASGIN4(LNOD, IN, KNOD(I))
         IERR = SO_ALLC_ASGIN4(LDDL, IN, IDDL)
      ENDDO

C---     Valide
      DO IN=1,NNOD
         INOD = SO_ALLC_REQIN4(LNOD, IN)
         IF (INOD .LE. 0) GOTO 9902
         IDDL = SO_ALLC_REQIN4(LDDL, IN)
         IF (IDDL .LE. 0) GOTO 9903
      ENDDO

C---     CONSTRUIS ET INITIALISE L'OBJET
      IF (ERR_GOOD()) IERR=PS_SN0N_AJTNOD(HOBJ,
     &                                    HGRD,
     &                                    NNOD,
     &                                    KA(SO_ALLC_REQKIND(KA,LNOD)),
     &                                    KA(SO_ALLC_REQKIND(KA,LDDL)))

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(2A,I9)') 'ERR_NNO_INVALIDE', ': ', INOD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(2A,I9)') 'ERR_DDL_INVALIDE', ': ', IDDL
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_SN0N_AID()

9999  CONTINUE
      CALL ERR_PUSH()
      IF (LNOD .NE. 0) IERR = SO_ALLC_ALLINT(0, LNOD)
      IF (LDDL .NE. 0) IERR = SO_ALLC_ALLINT(0, LDDL)
      CALL ERR_POP()
      IC_PS_SN0N_XEQMTH_ADD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN0N_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0N_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn0n_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>probe_on_nodes</b> represents the post-treatment that
C   implements a numerical probe applied on nodes.
C</comment>
      IC_PS_SN0N_REQCLS = 'probe_on_nodes'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_SN0N_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_SN0N_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn0n_ic.fi'
      INCLUDE 'pssn0n.fi'
C-------------------------------------------------------------------------

      IC_PS_SN0N_REQHDL = PS_SN0N_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_SN0N_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_SN0N_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('pssn0n_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_SN0N_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn0n.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0n_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_PROBE_ON_NODES'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_SN0N_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PS_SN0N_PRN = ERR_TYP()
      RETURN
      END
