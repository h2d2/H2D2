C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Class: PS_DLIB
C     INTEGER PS_DLIB_000
C     INTEGER PS_DLIB_999
C     INTEGER PS_DLIB_CTR
C     INTEGER PS_DLIB_DTR
C     INTEGER PS_DLIB_INI
C     INTEGER PS_DLIB_RST
C     INTEGER PS_DLIB_REQHBASE
C     LOGICAL PS_DLIB_HVALIDE
C     INTEGER PS_DLIB_ACC
C     INTEGER PS_DLIB_FIN
C     INTEGER PS_DLIB_XEQ
C     INTEGER PS_DLIB_REQHVNO
C     CHARACTER*256 PS_DLIB_REQNOMF
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_DLIB_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psdlib.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_DLIB_NOBJMAX,
     &                   PS_DLIB_HBASE,
     &                   'Post-Treatment Degree of Freedom')

      PS_DLIB_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_DLIB_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psdlib.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER  IERR
      EXTERNAL PS_DLIB_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_DLIB_NOBJMAX,
     &                   PS_DLIB_HBASE,
     &                   PS_DLIB_DTR)

      PS_DLIB_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_DLIB_NOBJMAX,
     &                   PS_DLIB_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_DLIB_HVALIDE(HOBJ))
         IOB = HOBJ - PS_DLIB_HBASE

         PS_DLIB_HPRNT(IOB) = 0
      ENDIF

      PS_DLIB_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_DLIB_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_DLIB_NOBJMAX,
     &                   PS_DLIB_HBASE)

      PS_DLIB_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'psdlib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = PS_DLIB_RST(HOBJ)

C---     CONSTRUIT ET INITIALISE LE PARENT
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     ASSIGNE LES VALEURS
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_DLIB_HBASE
         PS_DLIB_HPRNT(IOB) = HPRNT
      ENDIF

      PS_DLIB_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_DLIB_HBASE

C---     DETRUIS LES DONNÉES
      HPRNT = PS_DLIB_HPRNT(IOB)
      IF (PS_PSTD_HVALIDE(HPRNT)) IERR = PS_PSTD_DTR(HPRNT)

C---     RESET
      IF (ERR_GOOD()) PS_DLIB_HPRNT(IOB) = 0

      PS_DLIB_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_DLIB_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_DLIB_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psdlib.fi'
      INCLUDE 'psdlib.fc'
C------------------------------------------------------------------------

      PS_DLIB_REQHBASE = PS_DLIB_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_DLIB_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_DLIB_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psdlib.fc'
C------------------------------------------------------------------------

      PS_DLIB_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_DLIB_NOBJMAX,
     &                                  PS_DLIB_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C
C************************************************************************
      FUNCTION PS_DLIB_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_ACC
CDEC$ ENDIF

      USE LM_HELE_M, ONLY: LM_HELE_REQEDTA, LM_HELE_REQGDTA
      USE LM_GDTA_M, ONLY: LM_GDTA_T
      USE LM_EDTA_M, ONLY: LM_EDTA_T
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR1_T, SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2_T, SO_ALLC_VPTR2 
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER HSIM, HVNO
      INTEGER HPRNT
      INTEGER HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      REAL*8  TSIM
      TYPE (LM_GDTA_T), POINTER :: GDTA
      TYPE (LM_EDTA_T), POINTER :: EDTA
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (SO_ALLC_VPTR1_T) :: P_DLB
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.dof'

C---     Récupère les attributs
      IOB = HOBJ - PS_DLIB_HBASE
      HPRNT = PS_DLIB_HPRNT(IOB)
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de la simulation
      GDTA => LM_HELE_REQGDTA(HSIM)
      EDTA => LM_HELE_REQEDTA(HSIM)
      HNUMR = LM_HELE_REQHNUMC(HSIM)
      TSIM  = LM_HELE_REQTEMPS(HSIM)
D     CALL ERR_ASR(EDTA%NEQL  .GT. 0)
D     CALL ERR_ASR(ASSOCIATED(EDTA%VDLG))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Contrôles
      IF (NNL_D  .NE. GDTA%NNL)  GOTO 9900
      IF (NPST_D .NE. EDTA%NDLN) GOTO 9901

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_DLIB',
     &                                      TSIM)

C---     Alloue l'espace pour la solution
      IF (ERR_GOOD()) P_DLB = SO_ALLC_VPTR1(EDTA%NDLL)

C---     Copie des DDL
      IF (ERR_GOOD()) THEN
         CALL DCOPY(EDTA%NDLL,
     &              EDTA%VDLG, 1,
     &              P_DLB%VPTR, 1)
      ENDIF

C---     Opération de réduction dans L_DLB
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       EDTA%NDLL,
     &                       PVNO_D%VPTR, 1,
     &                       P_DLB%VPTR,  1)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                         HNUMR,
     &                         P_DLB%VPTR,
     &                         TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

C---     Récupère l'espace
      P_DLB = SO_ALLC_VPTR1()

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNL_INCOMPATIBLES', ': ',
     &                               GDTA%NNL, '/', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INCOMPATIBLES', ': ',
     &                               EDTA%NDLN, '/', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_DLIB_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C************************************************************************
      FUNCTION PS_DLIB_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_FIN
CDEC$ ENDIF

      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2_T, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      IOB = HOBJ - PS_DLIB_HBASE
      HPRNT = PS_DLIB_HPRNT(IOB)
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère la numérotation
      HNUMR  = LM_HELE_REQHNUMC(HSIM)

C---     Contrôles
      IF (NNL_D  .LE. 0) GOTO 9901
      IF (NPST_D .LE. 0) GOTO 9902

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = PS_DLIB_LOG(NPST_D,
     &                      NNL_D,
     &                      PVNO_D%VPTR)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_DLIB_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psdlib.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'psdlib.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des DDL et le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_DLIB_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_DLIB_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_DLIB_FIN(HOBJ)

      PS_DLIB_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_DLIB_ASGHSIM
C
C Description:
C     La fonction PS_DLIB_ASGHSIM assigne la simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur l'élément
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'psdlib.fi'
      INCLUDE 'lmelem.fi'
      INCLUDE 'lmgeom.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER HPRNT
      INTEGER NPST, NNL, NNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Le nombre de variables de post-traitement
      NPST = LM_HELE_REQPRM(HSIM, LM_ELEM_PRM_NDLN)
      NNL  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNL)
      NNT  = LM_HELE_REQPRM(HSIM, LM_GEOM_PRM_NNT)

      HPRNT = PS_DLIB_HPRNT(HOBJ-PS_DLIB_HBASE)
      PS_DLIB_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NNL, NNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_DLIB_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_REQHVNO
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_DLIB_HPRNT(HOBJ-PS_DLIB_HBASE)
      PS_DLIB_REQHVNO = PS_PSTD_REQHVNO(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_DLIB_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_DLIB_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'psdlib.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fc'

      INTEGER HPRNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_DLIB_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HPRNT = PS_DLIB_HPRNT(HOBJ-PS_DLIB_HBASE)
      PS_DLIB_REQNOMF = PS_PSTD_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_DLIB_LOG(NDLN, NNL, VDLG)

      IMPLICIT NONE

      INTEGER PS_DLIB_LOG
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ID, IRMAX, IRMIN
      INTEGER IDAMAX, IDAMIN
      REAL*8  RMX, RMN
C------------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_POST_DLIB:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE (LOG_BUF,'(A,I12)')  'MSG_NDLN#<25>#= ', NDLN
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')  'MSG_NNL#<25>#= ', NNL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)')      'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

C---     Normes pour chaque DDL
      CALL LOG_INCIND()
      IRMIN = IDAMIN(NNL, VDLG(1,1), NDLN)
      IRMAX = IDAMAX(NNL, VDLG(1,1), NDLN)
      RMN = VDLG(1, IRMIN)
      RMX = VDLG(1, IRMAX)
      WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                    '  (max)= ', RMX, IRMAX
      CALL LOG_ECRIS(LOG_BUF)

      DO ID=2, NDLN
         IRMIN = IDAMIN(NNL, VDLG(ID,1), NDLN)
         IRMAX = IDAMAX(NNL, VDLG(ID,1), NDLN)
         RMN = VDLG(ID, IRMIN)
         RMX = VDLG(ID, IRMAX)
         WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                       '  (max)= ', RMX, IRMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      PS_DLIB_LOG = ERR_TYP()
      RETURN
      END
