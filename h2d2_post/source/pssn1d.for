C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  PoSt-traitement
C Objet:   SoNde 1D
C Type:    Virtuel
C
C Functions:
C   Public:
C     INTEGER PS_SN1D_000
C     INTEGER PS_SN1D_999
C     INTEGER PS_SN1D_CTR
C     INTEGER PS_SN1D_DTR
C     INTEGER PS_SN1D_INI
C     INTEGER PS_SN1D_RST
C     INTEGER PS_SN1D_REQHBASE
C     LOGICAL PS_SN1D_HVALIDE
C     INTEGER PS_SN1D_ACC
C     INTEGER PS_SN1D_FIN
C     INTEGER PS_SN1D_XEQ
C     INTEGER PS_SN1D_ASGHSIM
C     CHARACTER*256 PS_SN1D_REQNOMF
C   Private:
C     INTEGER PS_SN1D_LOG
C     INTEGER PS_SN1D_ITRP
C     INTEGER PS_SN1D_NINT
C     INTEGER ASGKNG
C     INTEGER ASGPNT
C
C************************************************************************

      MODULE PS_SN1D_M

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T,
     &                     SO_ALLC_KPTR2_T, 
     &                     SO_ALLC_VPTR1_T, 
     &                     SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC

!      INCLUDE 'obobjc.fi'

C---     Attributs statiques
      INTEGER, SAVE :: PS_SN1D_HBASE = 0

      INTEGER, PARAMETER :: PS_SN1D_NVNOMAX = 3

C---     Type de donnée associé à la classe
      TYPE :: PS_SN1D_SELF_T
C        <comment>Handle on parent</comment>
         INTEGER HPRNT
C        <comment>Handle on integration function</comment>
         INTEGER HFITG
C        <comment>Number of source nodal values</comment>
         INTEGER NVNOS
C        <comment>Index of nodal value to monitor</comment>
         INTEGER INDXS(PS_SN1D_NVNOMAX)
C        <comment>Handle on nodal values to monitor</comment>
         INTEGER HVNOS(PS_SN1D_NVNOMAX)
!!   C     <comment>Handle on L2 grid</comment>
!!         INTEGER HGRID
C        <comment>Handle on the numbering</comment>
         INTEGER HNUMR
C        <comment>Number of dimensions</comment>
         INTEGER NDIM
C        <comment>Local number of nodes to monitor</comment>
         INTEGER NPNT
C        <comment>Local number of nodes to monitor</comment>
         INTEGER NPNL
C        <comment>Coordinates of points to monitor</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PCORG
C        <comment>Elements of points</comment>
         TYPE(SO_ALLC_KPTR1_T) :: PELEM
C        <comment>Coord. on ref. element of points</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PCORE
C        <comment>Number of nodes per L2 elements of the line</comment>
         INTEGER NNEL
C        <comment>Number of L2 elements of the line</comment>
         INTEGER NELL
C        <comment>Connectivities of the L2 elements of the line</comment>
         TYPE(SO_ALLC_KPTR2_T) :: PKNGL
C        <comment>Work table for the values</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PVAL
      END TYPE PS_SN1D_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PS_SN1D_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_REQSELF(HOBJ) RESULT(SELF)

      USE ISO_C_BINDING, ONLY: C_PTR, C_F_POINTER
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
      TYPE (C_PTR) :: CELF
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      RETURN
      END FUNCTION PS_SN1D_REQSELF

      END MODULE PS_SN1D_M

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction <code>PS_SN1D_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_000
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INCLUDE 'pssn1d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PS_SN1D_HBASE,
     &                   'Post-Treatment Probe 1D')

      PS_SN1D_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_999
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INCLUDE 'pssn1d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PS_SN1D_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PS_SN1D_HBASE,
     &                   PS_SN1D_DTR)

      PS_SN1D_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_SN1D_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_CTR
CDEC$ ENDIF

      USE PS_SN1D_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER IERR, IRET
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PS_SN1D_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PS_SN1D_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PS_SN1D_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PS_SN1D_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_DTR
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PS_SN1D_RST(HOBJ)

C---     Dé-alloue
      SELF => PS_SN1D_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PS_SN1D_HBASE)

      PS_SN1D_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée PS_SN1D_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_RAZ(HOBJ)

      USE PS_SN1D_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_KPTR2, 
     &                     SO_ALLC_VPTR1, 
     &                     SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER ID
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN1D_REQSELF(HOBJ)
      SELF%HPRNT = 0
      SELF%NVNOS = 0
      SELF%INDXS(:) = 0
      SELF%HVNOS(:) = 0
      SELF%HNUMR = 0
      SELF%NDIM  = 0
      SELF%NPNT  = 0
      SELF%NPNL  = 0
      SELF%PCORG = SO_ALLC_VPTR2()
      SELF%PELEM = SO_ALLC_KPTR1()
      SELF%PCORE = SO_ALLC_VPTR2()
      SELF%NNEL  = 0
      SELF%NELL  = 0
      SELF%PKNGL = SO_ALLC_KPTR2()
      SELF%PVAL  = SO_ALLC_VPTR2()

      PS_SN1D_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux. Elles seront
C     compactées plus tard.
C     Les données ne sont pas forcément chargées. Le contrôle de dimension
C     sur la borne max (<= NDLN) est renvoyé dans ACC
C************************************************************************
      FUNCTION PS_SN1D_INI(HOBJ,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW,
     &                     NDIM,
     &                     NPNT,
     &                     X1,
     &                     X2,
     &                     NVNO,
     &                     HVNO,
     &                     INDX,
     &                     HFITG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_INI
CDEC$ ENDIF

      USE PS_SN1D_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       NDIM
      INTEGER       NPNT
      REAL*8        X1(NDIM)
      REAL*8        X2(NDIM)
      INTEGER       NVNO
      INTEGER       HVNO(NVNO)
      INTEGER       INDX(NVNO)
      INTEGER       HFITG

      INCLUDE 'pssn1d.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER, PARAMETER :: NDIM_MAX = 3

      INTEGER IERR
      INTEGER ID, IX
      INTEGER HPRNT
      TYPE(SO_ALLC_VPTR2_T) :: PCORG
      TYPE(PS_SN1D_SELF_T), POINTER :: SELF
      REAL*8  X(NDIM_MAX), DX(NDIM_MAX)

      INTEGER ASGPNT
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
D     CALL ERR_PRE(SO_FUNC_HVALIDE(HFITG))
C------------------------------------------------------------------------

C---     CONTROLE LES DONNEES
      IF (NPNT .LT. 2) GOTO 9900
      IF (NDIM .GT. NDIM_MAX) GOTO 9901
      IF (NVNO .LT. 1) GOTO 9902
      IF (NVNO .GT. NDIM) GOTO 9902
      DO ID=1,NVNO
         IF (.NOT. DT_VNOD_HVALIDE(HVNO(ID))) GOTO 9903
         IF (INDX(ID) .LE. 0) GOTO 9904    ! cf note
      ENDDO

C---     Reset les données
      IERR = PS_SN1D_RST(HOBJ)

C---     Construit et initialise le parent
      IF (ERR_GOOD()) IERR = PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD()) IERR = PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     Alloue la mémoire pour tous les points
      IF (ERR_GOOD()) PCORG = SO_ALLC_VPTR2(NDIM, NPNT)

C---     Génère les points
      DX(1:NDIM) = (X2(:)-X1(:)) / (NPNT-1)
      X(1:NDIM)  = X1(:)
      DO IX=1,NPNT-1
         PCORG%VPTR(1:NDIM, IX) = X(:)
         X(1:NDIM) = X(:) + DX(:)
      ENDDO
      PCORG%VPTR(:,IX) = X2(1:NDIM)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%HPRNT = HPRNT
         SELF%HFITG = HFITG
         SELF%NVNOS = NVNO
         SELF%INDXS(1:NVNO) = INDX(:)
         SELF%HVNOS(1:NVNO) = HVNO(:)
         SELF%NDIM  = NDIM
         SELF%NPNT  = NPNT
         SELF%NPNL  = NPNT     ! NPNL
         SELF%PCORG = PCORG
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A,2(A,I6))') 'ERR_NDIM_INCOMPATIBLES', ': ',
     &                               NDIM, ' / ', NDIM_MAX
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(A,2(A,I6))') 'ERR_NVNO_INCOMPATIBLES', ': ',
     &                              NVNO, ' / ', NDIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HVNO(ID)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HVNO(ID), 'MSG_VALEURS_NODALES')
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_INDICE_INVALIDE', ': ',
     &            INDX(ID), ' /', DT_VNOD_REQNVNO(HVNO(ID))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN1D_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Réinitialise
C
C Description:
C     La fonction PS_SN1D_RST réinitialise l'objet. La mémoire est
C     désallouée, les objets attributs détruis et tous les attributs
C     réinitialisés.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_RST
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER IERR
      INTEGER HPRNT, HNUMR
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN1D_REQSELF(HOBJ)

C---     Détruis le parent
      HPRNT = SELF%HPRNT
      IF (PS_PSTD_HVALIDE(HPRNT)) THEN
         IERR = PS_PSTD_DTR(HPRNT)
      ENDIF

C---     Détruis la numérotation
      HNUMR = SELF%HNUMR
      IF (NM_IDEN_HVALIDE(HNUMR)) THEN
         IERR = NM_IDEN_DTR(HNUMR)
      ENDIF

C---     Récupère la mémoire
      ! pass   !La mémoire est récupérée automatiquement dans RAZ

C---     RESET
      IF (ERR_GOOD()) IERR = PS_SN1D_RAZ(HOBJ)

      PS_SN1D_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SN1D_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_REQHBASE
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INCLUDE 'pssn1d.fi'
C------------------------------------------------------------------------

      PS_SN1D_REQHBASE = PS_SN1D_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SN1D_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_HVALIDE
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn1d.fc'
C------------------------------------------------------------------------

      PS_SN1D_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PS_SN1D_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C************************************************************************
      FUNCTION PS_SN1D_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_ACC
CDEC$ ENDIF

      USE PS_SN1D_M
      USE SO_FUNC_M, ONLY: SO_FUNC_CALL
      USE SO_ALLC_M, ONLY: SO_ALLC_CST2B, SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER I_COMM, I_MASTER, I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR
      INTEGER IOB
      INTEGER IOP
      INTEGER ID
      INTEGER HPRNT, HFITG, HVNO, HNUMR
      INTEGER HSIM, HLMGO
      INTEGER HVNO_S, INDX_S
      INTEGER NPNL_D, NPST_D      ! Destination
      INTEGER NPNL_L, NDLN_L      ! Local
      INTEGER NNL_S,  NDLN_S      ! Source
      INTEGER NDIM, NVNO, NPNL, NNEL, NELL
      INTEGER LVNO_S, LVNO_D
      REAL*8  VINT(1), VSUM(1)
      REAL*8  TSIM_S, TSIM
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_S, PVNO_D
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.probe.1d'

C---     Récupère les attributs
      SELF => PS_SN1D_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de simulation
      TSIM  = LM_HELE_REQTEMPS(HSIM)
      HLMGO = LM_HELE_REQHLMGO(HSIM)
D     CALL ERR_ASR(LM_HGEO_HVALIDE(HLMGO))

C---     Interpole les points
      IF (SELF%HNUMR .EQ. 0) IERR = PS_SN1D_ITRP(HOBJ, HLMGO)

C---     Récupère les attributs
      NVNO = 0
      IF (ERR_GOOD()) THEN
         HFITG = SELF%HFITG
         NVNO  = SELF%NVNOS
         HNUMR = SELF%HNUMR
         NDIM  = SELF%NDIM 
         NPNL  = SELF%NPNL 
         NNEL  = SELF%NNEL 
         NELL  = SELF%NELL 
D        CALL ERR_ASR(NM_NUMR_CTRLH(HNUMR))
      ENDIF

C---     Nos dimensions locales
      IF (ERR_GOOD()) THEN
         NPNL_L = NPNL
         NDLN_L = NVNO
      ENDIF

C---     Récupère les données du vno dest.
      IF (ERR_GOOD()) THEN
         NPNL_D = DT_VNOD_REQNNL (HVNO)
         NPST_D = DT_VNOD_REQNVNO(HVNO)
         LVNO_D = DT_VNOD_REQLVNO(HVNO)
         PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NPNL_D)
      ENDIF

C---     Contrôles
      IF (ERR_GOOD()) THEN
         IF (NPNL_D .NE. 1) GOTO 9900
         IF (NPST_D .NE. 1) GOTO 9901
      ENDIF

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT, 
     &                                      LOG_ZNE,
     &                                      'MSG_POST_PROBE_1D',
     &                                      TSIM)

C---     Interpole les valeurs dans VAL
      DO ID=1,NVNO

C---        Récupère les données sources
         INDX_S = SELF%INDXS(ID)
         HVNO_S = SELF%HVNOS(ID)
D        CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO_S))

         NNL_S  = DT_VNOD_REQNNL  (HVNO_S)
         NDLN_S = DT_VNOD_REQNVNO (HVNO_S)
         LVNO_S = DT_VNOD_REQLVNO (HVNO_S)
         TSIM_S = DT_VNOD_REQTEMPS(HVNO_S)
         PVNO_S = SO_ALLC_VPTR2(LVNO_S, NDLN_S, NNL_S)

C---        Contrôles
         IF (NNL_S  .LT. 0 .OR. NDLN_S .LE. 0) GOTO 9902
         IF (INDX_S .LT. 0 .OR. INDX_S .GT. NDLN_S) GOTO 9903
         IF (TSIM_S .LT. TSIM)   GOTO 9904

C---        Interpole
         IERR = LM_HGEO_INTRP(HLMGO,
     &                        SELF%PELEM%KPTR,
     &                        SELF%PCORE%VPTR,
     &                        INDX_S,
     &                        PVNO_S%VPTR,
     &                        ID,
     &                        SELF%PVAL%VPTR)

         IF (.NOT. ERR_GOOD()) EXIT
      ENDDO

C---     Intègre sur la ligne
      VINT(1) = 0.0D0
      IF (ERR_GOOD()) THEN
         IERR = SO_FUNC_CALL(HFITG,
     &                       SO_ALLC_CST2B(NDIM),
     &                       SO_ALLC_CST2B(NPNL_L),
     &                       SELF%PCORG%CST2B(),
     &                       SO_ALLC_CST2B(NNEL),
     &                       SO_ALLC_CST2B(NELL),
     &                       SELF%PKNGL%CST2B(),
     &                       SO_ALLC_CST2B(NVNO),
     &                       SELF%PVAL%CST2B(),
     &                       SO_ALLC_CST2B(VINT))
      ENDIF

C---     Réduis entre les proc vers le master
      VSUM(1) = 0.0D0
      IF (ERR_GOOD()) THEN
         I_COMM = MP_UTIL_REQCOMM()
         CALL MPI_REDUCE(VINT, VSUM, 1, MP_TYPE_RE8(),
     &                   MPI_SUM, I_MASTER, I_COMM, I_ERROR)
      ENDIF

C---     Operation de réduction dans VAL
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       NPST_D*NPNL_D,
     &                       PVNO_D%VPTR, 1,
     &                       VSUM, 1)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                          HNUMR,
     &                          VSUM,
     &                          TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPNT_INCOMPATIBLES', ': ',
     &                               1, '/', NPNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_VNO_SOURCE_NON_CHARGE', HVNO_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HVNO_S, 'MSG_VNO')
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_INDICE_INVALIDE', ': ',
     &            INDX_S, ' /', NDLN_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_TEMPS_INCOHERENTS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_TEMPS_SIMU#<35>#', TSIM
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_TEMPS_SOURCE#<35>#', TSIM_S
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN1D_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C
C************************************************************************
      FUNCTION PS_SN1D_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_FIN
CDEC$ ENDIF

      USE PS_SN1D_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER IERR
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN1D_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      IF (NNL_D  .LE. 0) GOTO 9901
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      IF (NPST_D .LE. 0) GOTO 9902
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère la numérotation
      HNUMR  = SELF%HNUMR

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = PS_SN1D_LOG(NPST_D,
     &                      NNL_D,
     &                      PVNO_D%VPTR)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN1D_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des DDL et le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_SN1D_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_SN1D_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_SN1D_FIN(HOBJ)

      PS_SN1D_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1D_ASGHSIM
C
C Description:
C     La fonction PS_SN1D_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_ASGHSIM
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1d.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER, PARAMETER :: NNL  = 1
      INTEGER, PARAMETER :: NNT  = 1
      INTEGER, PARAMETER :: NPST = 1
      INTEGER HPRNT
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

      SELF => PS_SN1D_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      PS_SN1D_ASGHSIM = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NNL, NNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1D_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1D_REQNOMF
CDEC$ ENDIF

      USE PS_SN1D_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1d.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER HPRNT
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN1D_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      PS_SN1D_REQNOMF = PS_PSTD_REQNOMF(HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: Log les résultats
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1D_LOG(NDLN, NNL, VDLG)

      IMPLICIT NONE

      INTEGER PS_SN1D_LOG
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ID, IRMAX, IRMIN
      INTEGER IDAMAX, IDAMIN
      REAL*8  RMX, RMN
C------------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_POST_PROBE_1D:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE (LOG_BUF,'(A,I12)')  'MSG_NDLN#<25>#= ', NDLN
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')  'MSG_NNL#<25>#= ', NNL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)')      'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

C---     Normes pour chaque DDL
      CALL LOG_INCIND()
      IRMIN = IDAMIN(NNL, VDLG(1,1), NDLN)
      IRMAX = IDAMAX(NNL, VDLG(1,1), NDLN)
      RMN = VDLG(1, IRMIN)
      RMX = VDLG(1, IRMAX)
      WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                  '  (max)= ', RMX, IRMAX
      CALL LOG_ECRIS(LOG_BUF)

      DO ID=2, NDLN
         IRMIN = IDAMIN(NNL, VDLG(ID,1), NDLN)
         IRMAX = IDAMAX(NNL, VDLG(ID,1), NDLN)
         RMN = VDLG(1, IRMIN)
         RMX = VDLG(1, IRMAX)
         WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                     '  (max)= ', RMX, IRMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      PS_SN1D_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée PS_SN1D_ITRP traduis les numéros de noeuds de la
C     numérotation globale à la numérotation locale. Seuls les noeuds
C     privés au process sont considérés. Les tables sont compactées et la
C     fonction retourne le nombre de noeuds locaux.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1D_ITRP(HOBJ, HLMGO)

      USE PS_SN1D_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_KPTR2, 
     &                     SO_ALLC_VPTR1, 
     &                     SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLMGO

      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'nmiden.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn1d.fc'

      INTEGER IERR
      INTEGER HNUMR
      INTEGER LNGL
      INTEGER NPNT, NDIM, NVNO
      INTEGER NPNL, NELL, NPMIN, NEMIN
      TYPE (PS_SN1D_SELF_T), POINTER :: SELF

      REAL*8, PARAMETER :: TOL = 1.0D-6  ! Extrapolation tolerance
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1D_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HLMGO))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => PS_SN1D_REQSELF(HOBJ)
      NVNO  = SELF%NVNOS
      HNUMR = SELF%HNUMR
      NPNT  = SELF%NPNT 
      NDIM  = SELF%NDIM 
      CALL ERR_ASR(SELF%PCORG%ISVALID())

C---     Alloue la mémoire
      IF (ERR_GOOD()) SELF%PCORE = SO_ALLC_VPTR2(NDIM, NPNT)
      IF (ERR_GOOD()) SELF%PELEM = SO_ALLC_KPTR1(NPNT)

C---     Recherche les éléments des points
      IF (ERR_GOOD()) THEN
         IERR = LM_HGEO_LCLELV(HLMGO,
     &                         TOL,
     &                         SELF%PCORG%VPTR,
     &                         SELF%PELEM%KPTR,
     &                         SELF%PCORE%VPTR)
      ENDIF

C---     Génère les connec et compacte les tables
      NPNL = 0
      NELL = 0
      LNGL = 0
      IF (ERR_GOOD())
     &   IERR = PS_SN1D_NINT(HLMGO,
     &                       NDIM,
     &                       NPNT,
     &                       SELF%PCORG%VPTR,
     &                       SELF%PELEM%KPTR,
     &                       SELF%PCORE%VPTR,
     &                       NPNL,
     &                       NELL,
     &                       SELF%PKNGL,
     &                       HNUMR)

C---     Compacte mais fait exister
      NPMIN = MAX(1, NPNL)
      NEMIN = MAX(1, NELL)
      IF (ERR_GOOD()) IERR = SELF%PCORE%REDIM(NDIM, NPMIN)
      IF (ERR_GOOD()) IERR = SELF%PELEM%REDIM(NEMIN)

C---     Alloue la mémoire
      IF (ERR_GOOD()) SELF%PVAL = SO_ALLC_VPTR2(NVNO, NPMIN)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%HNUMR = HNUMR    ! la renum du seul point
         SELF%NPNL  = NPNL
         SELF%NNEL  = 2
         SELF%NELL  = NELL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HLMGO
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NDIM_INCOMPATIBLES', ': ', NDIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IERR = MP_UTIL_SYNCERR()
      PS_SN1D_ITRP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Numéro internes des noeuds
C
C Description:
C     La fonction statique privée PS_SN1D_NINT détermine les noeuds
C     locaux, génère les connectivités, compacte les tables et crée
C     la renumérotation.
C
C Entrée:
C     HGRID    Handle sur le maillage des noeuds
C     NDIM     Nombre de dimensions
C     NNT      Nombre de points en numérotation globale
C     VCORG    Table des coordonnées des points
C     KELE     Table des éléments sous les points
C     VCORG    Table des coordonnées élémentaires des points
C     KTRV     Table de travail
C
C Sortie:
C     NNL      Le nombre de noeuds locaux
C     NEL      Le nombre d'éléments locaux
C     LNG      Pointeur à la table des connectivités
C     HNUMR    Handle sur la renumérotation
C
C Notes:
C     La renum ne contient qu'un seul point pour la valeur intégrée
C************************************************************************
      FUNCTION PS_SN1D_NINT(HGRID,
     &                      NDIM,
     &                      NNT,
     &                      VCORG,
     &                      KELE,
     &                      VCORE,
     &                      NNL,
     &                      NEL,
     &                      PKNG,
     &                      HNUMR)

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR2_T, SO_ALLC_KPTR2
      IMPLICIT NONE

      INTEGER PS_SN1D_NINT
      INTEGER, INTENT(IN) ::  HGRID
      INTEGER, INTENT(IN) ::  NDIM
      INTEGER, INTENT(IN) ::  NNT
      REAL*8,  INTENT(IN) ::  VCORG(NDIM, NNT)
      INTEGER, INTENT(INOUT) ::  KELE (NNT)
      REAL*8,  INTENT(IN) ::  VCORE(NDIM, NNT)
      INTEGER, INTENT(OUT)::  NNL
      INTEGER, INTENT(OUT)::  NEL
      TYPE(SO_ALLC_KPTR2_T), INTENT(OUT) :: PKNG
      INTEGER, INTENT(OUT)::  HNUMR

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'nmamno.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'soallc.fi'

      INTEGER I_MASTER, I_COMM, I_PROC, I_RANK, I_ERROR
      PARAMETER (I_MASTER = 0)

      INTEGER IERR, IDUM
      INTEGER IE, IG, IL, IP, IE1, IE2
      INTEGER IG_NL, IL_NL, IP_NL
      INTEGER HALGN
      INTEGER NO1, NO2
      LOGICAL ADDNO1

      INTEGER, ALLOCATABLE :: KTRV(:)
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      I_COMM = MP_UTIL_REQCOMM()
      CALL MPI_COMM_RANK(I_COMM, I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     Flag les éléments locaux
      ALLOCATE(KTRV(NNT))
      DO IE=1,NNT-1
         IE1 = KELE(IE)    ! Elem du point 1
         IE2 = KELE(IE+1)  ! Elem du point 2

         ! 2 points à l'intérieur
         IF (IE1 .GT. 0 .AND. IE2 .GT. 0) THEN
            KTRV(IE) = I_PROC
         ELSE
            KTRV(IE) = 0
         ENDIF
      ENDDO
      KTRV(NNT) = 0

C---     Réduis pour déterminer les PP
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, KTRV, NNT, MP_TYPE_INT(),
     &                   MPI_MAX, I_COMM, I_ERROR)

C---     Contrôle que chaque ele L2 a son proc
      NEL = 0
      IF (ERR_GOOD()) THEN
         DO IE=1,NNT-1
            IF (KTRV(IE) .LE. 0) GOTO 9900
            IF (KTRV(IE) .EQ. I_PROC) NEL = NEL + 1
         ENDDO
      ENDIF

C---     Dimensionne la table des connec L2
      IF (ERR_GOOD()) PKNG = SO_ALLC_KPTR2(2, MAX(NEL,1))

C---     Scanne les noeuds, compacte KNOD
      IE  = 0
      NNL = 0
      NO2 = 1
      ADDNO1 = .TRUE.
      DO IG=1,NNT-1
         IF (KTRV(IG) .NE. I_PROC) THEN
            ADDNO1 = .TRUE.
         ELSE
            NO1 = NO2
            NO2 = NO1 + 1

            IF (ADDNO1) THEN
               NNL = NNL + 1
               KELE(NNL) = KELE(NO1)
               CALL DCOPY(NDIM, VCORG(1,NO1), 1, VCORG(1,NNL), 1)
               CALL DCOPY(NDIM, VCORE(1,NO1), 1, VCORE(1,NNL), 1)
            ENDIF

            NNL = NNL + 1
            KELE(NNL) = KELE(NO2)
            CALL DCOPY(NDIM, VCORG(1,NO2), 1, VCORG(1,NNL), 1)
            CALL DCOPY(NDIM, VCORE(1,NO2), 1, VCORE(1,NNL), 1)

            IE = IE + 1
            PKNG%KPTR(:, IE) = [NNL-1, NNL]

            ADDNO1 = .FALSE.
         ENDIF
      ENDDO
D     CALL ERR_ASR(IE .EQ. NEL)

C---     Numéro locaux pour la renum
      IG = 1
      IL =  0
      IP = -1
      IF (I_RANK .EQ. I_MASTER) THEN
         IL = 1
         IP = I_PROC
      ENDIF

C---     Synchronise l'erreur avant la renum
      IERR = MP_UTIL_SYNCERR()

C---     Construit et initialise l'algo de renum
      HALGN = 0
      HNUMR = 0
      IF (ERR_GOOD()) IERR = NM_AMNO_CTR(HALGN)
      IF (ERR_GOOD()) IERR = NM_AMNO_INI(HALGN, 1)
      IF (ERR_GOOD()) IERR = NM_AMNO_ASGROW(HALGN, IG, IL, IP)
      IF (ERR_GOOD()) IERR = NM_AMNO_GENNUM(HALGN, HNUMR)
      IF (NM_AMNO_HVALIDE(HALGN)) IERR = NM_AMNO_DTR(HALGN)

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_ELEM_L2_SANS_PROCESS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,2I12)') 'MSG_NOEUDS', ':', IE, IE+1
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A)') 'MSG_POINTS_POSSIBLEMENT_HORS_DOMAINE'
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      IERR = MP_UTIL_SYNCERR()
      PS_SN1D_NINT = ERR_TYP()
      RETURN
      END
            