C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  PoSt-traitement
C Objet:   SoNde 0d sur Points
C Type:    Concret
C Interface:
C   H2D2 Module: PS
C      H2D2 Class: PS_SN0P
C         INTEGER PS_SN0P_000
C         INTEGER PS_SN0P_999
C         INTEGER PS_SN0P_CTR
C         INTEGER PS_SN0P_DTR
C         INTEGER PS_SN0P_RAZ
C         INTEGER PS_SN0P_INI
C         INTEGER PS_SN0P_RST
C         INTEGER PS_SN0P_REQHBASE
C         LOGICAL PS_SN0P_HVALIDE
C         INTEGER PS_SN0P_AJTPNT
C         INTEGER PS_SN0P_ACC
C         INTEGER PS_SN0P_FIN
C         INTEGER PS_SN0P_XEQ
C         INTEGER PS_SN0P_ASGHSIM
C         INTEGER PS_SN0P_REQHVNO
C         CHARACTER*256 PS_SN0P_REQNOMF
C         INTEGER PS_SN0P_LOG
C         INTEGER PS_SN0P_ITRP
C         INTEGER PS_SN0P_NINT
C         INTEGER PS_SN0P_CTRL
C         FTN (Sub)Module: PS_SN0P_M
C            Public:
C            Private:
C               TYPE (PS_SN0P_SELF_T), POINTER PS_SN0P_REQSELF
C            
C            FTN Type: PS_SN0P_SELF_T
C               Public:
C               Private:
C
C************************************************************************

      MODULE PS_SN0P_M

      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1_T,
     &                     SO_ALLC_VPTR1_T,
     &                     SO_ALLC_VPTR2_T
      IMPLICIT NONE
      PUBLIC

!      INCLUDE 'obobjc.fi'

C---     Attributs statiques
      INTEGER, SAVE :: PS_SN0P_HBASE = 0

C---     Type de donnée associé à la classe
      TYPE :: PS_SN0P_SELF_T
C        <comment>Handle on parent</comment>
         INTEGER HPRNT
C        <comment>Handle on nodal value to monitor</comment>
         INTEGER HVNOS
C        <comment>Index of nodal value to monitor</comment>
         INTEGER INDXS
C        <comment>Number of points total</comment>
         INTEGER NPNT
C        <comment>Number of points local</comment>
         INTEGER NPNL
C        <comment>Number of dimensions</comment>
         INTEGER NDIM
C        <comment>Coordinates of points to monitor</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PCORG
C        <comment>Handle on the numbering</comment>
         INTEGER HNUMR
C        <comment>Elements of points</comment>
         TYPE(SO_ALLC_KPTR1_T) :: PELEM
C        <comment>Coord. on ref. element of points</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PCORE
C        <comment>Work table for the values</comment>
         TYPE(SO_ALLC_VPTR2_T) :: PVAL
      END TYPE PS_SN0P_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PS_SN0P_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PS_SN0P_SELF_T), POINTER :: PS_SN0P_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PS_SN0P_REQSELF => SELF
      RETURN
      END FUNCTION PS_SN0P_REQSELF

      END MODULE PS_SN0P_M

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction <code>PS_SN0P_000(...)</code> initialise les tables
C     internes de la classe. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_000
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INCLUDE 'pssn0p.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PS_SN0P_HBASE,
     &                   'Post-Treatment Probe 0D on points')

      PS_SN0P_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_999
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INCLUDE 'pssn0p.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PS_SN0P_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PS_SN0P_HBASE,
     &                   PS_SN0P_DTR)

      PS_SN0P_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_SN0P_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_CTR
CDEC$ ENDIF

      USE PS_SN0P_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR, IRET
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PS_SN0P_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PS_SN0P_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PS_SN0P_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PS_SN0P_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_DTR
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PS_SN0P_RST(HOBJ)

C---     Dé-alloue
      SELF => PS_SN0P_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PS_SN0P_HBASE)

      PS_SN0P_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée PS_SN0P_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_RAZ(HOBJ)

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0p.fc'

      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0P_REQSELF(HOBJ)
      SELF%HPRNT = 0
      SELF%HVNOS = 0
      SELF%INDXS = 0
      SELF%NPNT  = 0
      SELF%NPNL  = 0
      SELF%NDIM  = 0
      SELF%PCORG = SO_ALLC_VPTR2()
      SELF%HNUMR = 0
      SELF%PELEM = SO_ALLC_KPTR1()
      SELF%PCORE = SO_ALLC_VPTR2()
      SELF%PVAL  = SO_ALLC_VPTR2()

      PS_SN0P_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Le contrôle de l'indice ne peut pas être fait ici car le VNO
C     n'est peut-être pas dimensionné.
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C     Chaque process a tous les points. Il faut une op de réduction à
C     la fin.
C************************************************************************
      FUNCTION PS_SN0P_INI(HOBJ,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW,
     &                     NDIM,
     &                     NPNT,
     &                     VCORP,
     &                     HVNOS,
     &                     INDXS)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_INI
CDEC$ ENDIF

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       NDIM
      INTEGER       NPNT
      REAL*8        VCORP(NDIM, NPNT)
      INTEGER       HVNOS
      INTEGER       INDXS

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HPRNT, HNUMR
      TYPE(SO_ALLC_VPTR2_T) :: PCORG
      TYPE(PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Contrôle les données
      IF (NPNT  .LE. 0) GOTO 9900
      IF (.NOT. DT_VNOD_HVALIDE(HVNOS)) GOTO 9901
      IF (INDXS .LE. 0) GOTO 9903
!!      IF (INDXS .GT. DT_VNOD_REQNVNO(HVNOS)) GOTO 9903 ! cf note

C---     Reset les données
      IERR = PS_SN0P_RST(HOBJ)

C---     Construit et initialise le parent
      IF (ERR_GOOD())IERR=PS_PSTD_CTR(HPRNT)
      IF (ERR_GOOD())IERR=PS_PSTD_INI(HPRNT,NOMFIC,ISTAT,IOPR,IOPW)

C---     Alloue la mémoire
      IF (ERR_GOOD()) PCORG = SO_ALLC_VPTR2(NDIM, NPNT)

C---     Copie les points
      IF (ERR_GOOD()) PCORG%VPTR(:, :) = VCORP(:, :)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => PS_SN0P_REQSELF(HOBJ)
         SELF%HPRNT = HPRNT
         SELF%HVNOS = HVNOS
         SELF%INDXS = INDXS
         SELF%NPNT  = NPNT
         SELF%NPNL  = 0
         SELF%NDIM  = NDIM
         SELF%PCORG = PCORG
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I12)') 'ERR_HANDLE_INVALIDE', ': ', HVNOS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HVNOS, 'MSG_VALEURS_NODALES')
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_NDIM_INCOMPATIBLES', ': ', NDIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9903  WRITE(ERR_BUF, '(2A,I12)') 'ERR_INDICE_INVALIDE', ': ', INDXS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0P_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Réinitialise
C
C Description:
C     La fonction PS_SN0P_RST réinitialise l'objet. La mémoire est
C     désallouée, les objets attributs détruis et tous les attributs
C     réinitialisés.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_RST
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HPRNT, HNUMR
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0P_REQSELF(HOBJ)

C---     Détruis le parent
      HPRNT = SELF%HPRNT
      IF (PS_PSTD_HVALIDE(HPRNT)) THEN
         IERR = PS_PSTD_DTR(HPRNT)
      ENDIF

C---     Détruis la numérotation
      HNUMR = SELF%HNUMR
      IF (NM_NUMR_CTRLH(HNUMR)) THEN
         IERR = NM_NUMR_DTR(HNUMR)
      ENDIF

C---     Récupère la mémoire
      ! pass   !La mémoire est récupérée automatiquement dans RAZ

C---     RESET
      IF (ERR_GOOD()) IERR = PS_SN0P_RAZ(HOBJ)

      PS_SN0P_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SN0P_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_REQHBASE
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INCLUDE 'pssn0p.fi'
C------------------------------------------------------------------------

      PS_SN0P_REQHBASE = PS_SN0P_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SN0P_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_HVALIDE
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PS_SN0P_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PS_SN0P_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Ajoute des points.
C
C Description:
C     La méthode PS_SN0P_AJTPNT permet d'ajouter des points de sondage.
C     C'est une complément à la méthode PS_SN0P_INI et doit être appelée
C     avant l'utilisation de l'objet pour des calculs.
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C     Chaque process a tous les points. Il faut une op de réduction à
C     la fin.
C************************************************************************
      FUNCTION PS_SN0P_AJTPNT(HOBJ,
     &                        NDIM,
     &                        NADD,
     &                        VCORP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_AJTPNT
CDEC$ ENDIF

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDIM
      INTEGER NADD
      REAL*8  VCORP(NDIM, NADD)

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HNUMR
      INTEGER NPNT, NVAL
      TYPE(SO_ALLC_VPTR2_T) :: PCORG
      TYPE(PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0P_REQSELF(HOBJ)
      NPNT  = SELF%NPNT
      HNUMR = SELF%HNUMR
      PCORG = SELF%PCORG

C---     Contrôle les données
      IF (HNUMR.NE. 0) GOTO 9900
      IF (NPNT .LE. 0) GOTO 9901
      IF (NDIM .NE. SELF%NDIM) GOTO 9902

C---     (Ré)-alloue la mémoire
      IF (ERR_GOOD()) IERR = PCORG%REDIM(NDIM, NPNT+NADD)

C---     Copie les points
      IF (ERR_GOOD()) THEN
         PCORG%VPTR(:,NPNT:) = VCORP(:,:)
         NPNT = NPNT + NADD
      ENDIF

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF%NPNT  = NPNT
         SELF%PCORG = PCORG
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6)')
      CALL ERR_ASG(ERR_ERR, 'ERR_AJOUT_POINTS_INTERDIT')
      CALL ERR_AJT('MSG_AJOUT_TROP_TARDIF')
      CALL ERR_AJT('MSG_AJOUT_VALIDE_AVANT_CALCUL')
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NPNT_INVALIDE', ': ', NPNT
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6)') 'ERR_NDIM_INCOHERENT', ': ', NDIM
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0P_AJTPNT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C************************************************************************
      FUNCTION PS_SN0P_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_ACC
CDEC$ ENDIF

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'dtgrid.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER IOP
      INTEGER HPRNT, HNUMR, HSIM
      INTEGER HVNO, HVNOS
      INTEGER INDXS
      INTEGER NPNL_D, NDLN_D      ! Destination
      INTEGER NPNL_L, NDLN_L      ! Local
      INTEGER NNL_S,  NDLN_S      ! Source
      INTEGER NPNT, NPNL, NDIM
      !INTEGER LCORG, LCORE, LELE, LNOD, LVAL
      INTEGER LVNO_S, LVNO_D
      REAL*8  TSIM_S, TSIM
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_S, PVNO_D
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Zone de log
      LOG_ZNE = 'h2d2.post.probe.0d.points'

C---     Récupère les attributs
      SELF => PS_SN0P_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))

C---     Récupère les attributs
      HNUMR = SELF%HNUMR
      HVNOS = SELF%HVNOS
      INDXS = SELF%INDXS
      NPNT  = SELF%NPNT
      NPNL  = SELF%NPNL
      NDIM  = SELF%NDIM

C---     Récupère les attributs du parent
      HSIM = PS_PSTD_REQHSIM(HPRNT)
D     CALL ERR_ASR(LM_HELE_HVALIDE(HSIM))
      HVNO = PS_PSTD_REQHVNO(HPRNT)
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données de simulation
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Récupère les données sources
      NNL_S  = DT_VNOD_REQNNL (HVNOS)
      NDLN_S = DT_VNOD_REQNVNO(HVNOS)
      LVNO_S = DT_VNOD_REQLVNO(HVNOS)
D     CALL ERR_ASR(LVNO_S .NE. 0)
      PVNO_S = SO_ALLC_VPTR2(LVNO_S, NDLN_S, NNL_S)
      TSIM_S = DT_VNOD_REQTEMPS(HVNOS)

C---     Nos dimensions locales
      NPNL_L = NPNL
      NDLN_L = 1

C---     Récupère les données du vno dest.
      NPNL_D = DT_VNOD_REQNNL (HVNO)
      NDLN_D = DT_VNOD_REQNVNO(HVNO)
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NDLN_D, NPNL_D)

C---     Contrôles
      IF (ERR_GOOD()) THEN
         IF (NPNL_D .GT. NPNL_L) GOTO 9900   ! .GT. pour des points
         IF (NDLN_D .NE. NDLN_L) GOTO 9901   ! hors maillage
         IF (NNL_S  .LT. 0) GOTO 9902        ! VNO pas chargé
         IF (NDLN_S .LE. 0) GOTO 9902        ! VNO pas chargé
         IF (INDXS  .GT. NDLN_S) GOTO 9903   ! Index invalide
         IF (TSIM_S .LT. TSIM)   GOTO 9904
      ENDIF

C---     Log
      IF (ERR_GOOD()) IERR = PS_PSTD_LOGACC(HPRNT,
     &                                      LOG_ZNE,
     &                                      'MSG_POST_PROBE_0D',
     &                                      TSIM)

C---     Interpole les valeurs dans VAL
      IF (ERR_GOOD()) THEN
         IERR = LM_HGEO_INTRP(HSIM,
     &                        SELF%PELEM%KPTR,
     &                        SELF%PCORE%VPTR,
     &                        INDXS,
     &                        PVNO_S%VPTR,
     &                        1,       ! IDDL_D,
     &                        SELF%PVAL%VPTR)
      ENDIF

C---     Operation de réduction dans VAL
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUC(HPRNT,
     &                       NDLN_D*NPNL_D,
     &                       PVNO_D%VPTR, 1,
     &                       SELF%PVAL%VPTR, 1)
      ENDIF

C---     Met à jour les données
      IF (ERR_GOOD()) THEN
         IERR = DT_VNOD_ASGVALS(HVNO,
     &                          HNUMR,
     &                          SELF%PVAL%VPTR,
     &                          TSIM)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD()) THEN
         IOP = PS_PSTD_REQOPW(HPRNT)
         IF (IOP .EQ. PS_OP_WRITE) IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPNT_INCOMPATIBLES', ': ',
     &                               NPNL_L, '/', NPNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INCOMPATIBLES', ': ',
     &                               NDLN_L, '/', NDLN_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I12)') 'ERR_VNO_SOURCE_NON_CHARGE', HVNOS
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      IERR = OB_OBJC_ERRH(HVNOS, 'MSG_VNO')
      GOTO 9999
9903  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_INDICE_INVALIDE', ': ',
     &            INDXS, ' /', NDLN_S
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9904  WRITE(ERR_BUF, '(A,2(A,I12))') 'ERR_TEMPS_INCOHERENTS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_TEMPS_SIMU#<35>#', TSIM
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(A,1PE14.6E3)') 'MSG_TEMPS_SOURCE#<35>#', TSIM_S
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0P_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Suivant les post-traitements, HNUMR peut être nul. Il est souvent
C     calculé dans ACC, et ACC peut ne pas être appelé si le trigger
C     n'est pas déclenché, résultant en FIN appelé sans ACC.
C************************************************************************
      FUNCTION PS_SN0P_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_FIN
CDEC$ ENDIF

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HPRNT, HSIM, HVNO, HNUMR
      INTEGER NNL_D, NPST_D
      INTEGER LVNO_D
      TYPE (SO_ALLC_VPTR2_T) :: PVNO_D
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0P_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      HSIM = PS_PSTD_REQHSIM(HPRNT)
      HVNO = PS_PSTD_REQHVNO(HPRNT)
      IF (HSIM .EQ. 0) GOTO 9900
D     CALL ERR_ASR(PS_PSTD_HVALIDE(HPRNT))
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Récupère les données du vno dest.
      NNL_D  = DT_VNOD_REQNNL (HVNO)
      IF (NNL_D  .LE. 0) GOTO 9901
      NPST_D = DT_VNOD_REQNVNO(HVNO)
      IF (NPST_D .LE. 0) GOTO 9902
      LVNO_D = DT_VNOD_REQLVNO(HVNO)
D     CALL ERR_ASR(LVNO_D .NE. 0)
      PVNO_D = SO_ALLC_VPTR2(LVNO_D, NPST_D, NNL_D)

C---     Récupère la numérotation
      HNUMR  = SELF%HNUMR

C---     Opération de réduction
      IF (ERR_GOOD()) THEN
         IERR = PS_PSTD_RDUCFIN(HPRNT,
     &                          NNL_D,
     &                          PVNO_D%VPTR, 1)
      ENDIF

C---     Log
      IF (ERR_GOOD()) THEN
         IERR = PS_SN0P_LOG(NPST_D,
     &                      NNL_D,
     &                      PVNO_D%VPTR)
      ENDIF

C---     Écris les données
      IF (ERR_GOOD() .AND. HNUMR .NE. 0) THEN
         IERR = DT_VNOD_SAUVE(HVNO, HNUMR)
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_APPEL_FINALIZE_SANS_ACC'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(2A,I9,A,I9)') 'ERR_NNT_INVALIDE', ': ', NNL_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9902  WRITE(ERR_BUF, '(2A,I6,A,I6)') 'ERR_NPOST_INVALIDE', ': ', NPST_D
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_SN0P_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      REAL*8  TSIM
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère le handle des DDL et le temps
      TSIM  = LM_HELE_REQTEMPS(HSIM)

C---     Charge les données de la simulation
      IF (ERR_GOOD()) IERR = LM_HELE_PASDEB(HSIM, TSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLCPRE(HSIM)
      IF (ERR_GOOD()) IERR = LM_HELE_CLC(HSIM)

C---     Conserve la simulation (dimensionne)
      IF (ERR_GOOD()) IERR = PS_SN0P_ASGHSIM(HOBJ, HSIM)

C---     Accumule
      IF (ERR_GOOD()) IERR = PS_SN0P_ACC(HOBJ)

C---     Finalise
      IF (ERR_GOOD()) IERR = PS_SN0P_FIN(HOBJ)

      PS_SN0P_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0P_ASGHSIM
C
C Description:
C     La fonction PS_SN0P_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_ASGHSIM
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn0p.fc'

      REAL*8  TSIM
      INTEGER IERR
      INTEGER HPRNT, HNUMR
      INTEGER NPNL, NPNT
      INTEGER NPST
      PARAMETER (NPST = 1)
      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_SN0P_REQSELF(HOBJ)
      HPRNT = SELF%HPRNT
      HNUMR = SELF%HNUMR

C---     Monte la renumérotation locale
      IF (HNUMR .EQ. 0) THEN
         TSIM  = LM_HELE_REQTEMPS(HSIM)
         TSIM  = MAX(TSIM, 0.0D0)
         IF (ERR_GOOD()) IERR = LM_HGEO_PASDEB(HSIM, TSIM)
         IF (ERR_GOOD()) IERR = PS_SN0P_ITRP(HOBJ, HSIM)
      ENDIF

C---     Récupère le nombre de points
      IF (ERR_GOOD()) THEN
         NPNL = SELF%NPNL
         NPNT = SELF%NPNT
         IERR = PS_PSTD_ASGHSIM(HPRNT, HSIM, NPST, NPNL, NPNT)
      ENDIF

      PS_SN0P_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0P_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_REQHVNO
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0P_REQSELF(HOBJ)
      PS_SN0P_REQHVNO = PS_PSTD_REQHVNO(SELF%HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN0P_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN0P_REQNOMF
CDEC$ ENDIF

      USE PS_SN0P_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn0p.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_SN0P_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_SN0P_REQSELF(HOBJ)
      PS_SN0P_REQNOMF = PS_PSTD_REQNOMF(SELF%HPRNT)
      RETURN
      END

C************************************************************************
C Sommaire: Log les résultats
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN0P_LOG(NDLN, NNL, VDLG)

      IMPLICIT NONE

      INTEGER PS_SN0P_LOG
      INTEGER NDLN
      INTEGER NNL
      REAL*8  VDLG(NDLN, NNL)

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'

      INTEGER ID, IRMAX, IRMIN
      INTEGER IDAMAX, IDAMIN
      REAL*8  RMX, RMN
C------------------------------------------------------------------------

      CALL LOG_ECRIS(' ')
      WRITE(LOG_BUF, '(A)') 'MSG_POST_PROBE_ON_POINTS:'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

      WRITE (LOG_BUF,'(A,I12)')  'MSG_NDLN#<25>#= ', NDLN
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A,I12)')  'MSG_NNL#<25>#= ', NNL
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)')      'MSG_VAL#<15>#:'
      CALL LOG_ECRIS(LOG_BUF)

C---     Normes pour chaque DDL
      CALL LOG_INCIND()
      IRMIN = IDAMIN(NNL, VDLG(1,1), NDLN)
      IRMAX = IDAMAX(NNL, VDLG(1,1), NDLN)
      RMN = VDLG(1, IRMIN)
      RMX = VDLG(1, IRMAX)
      WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                  '  (max)= ', RMX, IRMAX
      CALL LOG_ECRIS(LOG_BUF)

      DO ID=2, NDLN
         IRMIN = IDAMIN(NNL, VDLG(ID,1), NDLN)
         IRMAX = IDAMAX(NNL, VDLG(ID,1), NDLN)
         RMN = VDLG(1, IRMIN)
         RMX = VDLG(1, IRMAX)
         WRITE (LOG_BUF,'(2(A,1PE14.6E3,I7))') '(min)= ', RMN, IRMIN,
     &                                     '  (max)= ', RMX, IRMAX
         CALL LOG_ECRIS(LOG_BUF)
      ENDDO
      CALL LOG_DECIND()

      CALL LOG_DECIND()
      PS_SN0P_LOG = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction privée PS_SN0P_ITRP localise les points dans le
C     maillage HGRID.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_ITRP(HOBJ, HLMGO)

      USE PS_SN0P_M
      USE SO_ALLC_M, ONLY: SO_ALLC_KPTR1,
     &                     SO_ALLC_VPTR1,
     &                     SO_ALLC_VPTR2
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HLMGO

      INCLUDE 'pssn0p.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HNUMR
      INTEGER NPNT, NDIM
      TYPE(PS_SN0P_SELF_T), POINTER :: SELF

      REAL*8, PARAMETER :: TOL = 1.0D-6      ! Extrapolation tolerance
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN0P_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HLMGO))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => PS_SN0P_REQSELF(HOBJ)
      HNUMR = SELF%HNUMR
      NPNT  = SELF%NPNT
      NDIM  = SELF%NDIM

C---     Alloue la mémoire
      IF (ERR_GOOD()) SELF%PCORE = SO_ALLC_VPTR2(NDIM, NPNT)
      IF (ERR_GOOD()) SELF%PELEM = SO_ALLC_KPTR1(NPNT)
      IF (ERR_GOOD()) SELF%PVAL  = SO_ALLC_VPTR2(1, NPNT)

C---     Recherche les éléments des points
      IF (ERR_GOOD()) THEN
         IERR = LM_HGEO_LCLELV(HLMGO,
     &                         TOL,
     &                         SELF%PCORG%VPTR,
     &                         SELF%PELEM%KPTR,
     &                         SELF%PCORE%VPTR)
      ENDIF

C---     Crée la renum
      HNUMR = 0
      IF (ERR_GOOD())
     &   IERR = PS_SN0P_NINT(HNUMR,
     &                       HLMGO,
     &                       NDIM,
     &                       NPNT,
     &                       SELF%PCORG%VPTR,
     &                       SELF%PELEM%KPTR,
     &                       SELF%PCORE%VPTR)

C---     Contrôles
      IF (ERR_GOOD())
     &   IERR = PS_SN0P_CTRL(HOBJ,
     &                       HNUMR,
     &                       HLMGO,
     &                       NDIM,
     &                       NPNT,
     &                       SELF%PCORG%VPTR,
     &                       SELF%PELEM%KPTR)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => PS_SN0P_REQSELF(HOBJ)
         SELF%HNUMR = HNUMR    ! la renum des points
         SELF%NPNL  = NM_NUMR_REQNNL(HNUMR)
      ENDIF

      PS_SN0P_ITRP = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Numéros internes des noeuds
C
C Description:
C     La fonction privée PS_SN0P_NINT traduis les numéros de noeuds de la
C     numérotation globale à la numérotation locale. Seuls les noeuds
C     privés au process sont considérés. Les tables sont compactées et la
C     fonction retourne le nombre de noeuds locaux.
C
C Entrée:
C     HGRID    Handle sur le maillage des noeuds
C     NDIM     Nombre de  DIMensions
C     NNG      Nombre de points Globaux
C     VCORG    Table de CooRdonnées des points Globaux
C
C Sortie:
C     HNUMR    Handle sur la numérotation du post-traitement
C     KELE     Table des noeuds en numérotation locale
C     VCORE    Table de CooRdonnées des points locaux
C
C Notes:
C     Quitte à dédoubler, on prend tout les points qui sont
C     tombés dans le maillage local. Ne pas le faire élimine
C     les points de la zone de recoupement parce que ces
C     éléments ne sont pas privés à un process.
C************************************************************************
      FUNCTION PS_SN0P_NINT(HNUMR,
     &                      HLMGO,
     &                      NDIM,
     &                      NNG,
     &                      VCORG,
     &                      KELE,
     &                      VCORE)

      IMPLICIT NONE

      INTEGER HNUMR
      INTEGER HLMGO
      INTEGER NDIM
      INTEGER NNG
      REAL*8  VCORG(NDIM, NNG)
      INTEGER KELE (NNG)
      REAL*8  VCORE(NDIM, NNG)
      INTEGER NNL

      INCLUDE 'lmhgeo.fi'
      INCLUDE 'nmamno.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER IE
      INTEGER IG_NL, IL_NL, IP_NL
      INTEGER I_PROC, I_RANK, I_ERROR
      INTEGER HNUME, HALGN
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HLMGO))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Paramètres MPI
      CALL MPI_COMM_RANK(MP_UTIL_REQCOMM(), I_RANK, I_ERROR)
      I_PROC = I_RANK + 1

C---     Construit et initialise l'algo de renum
      HALGN = 0
      IF (ERR_GOOD()) IERR = NM_AMNO_CTR(HALGN)
      IF (ERR_GOOD()) IERR = NM_AMNO_INI(HALGN, NNG)

C---     Scanne les noeuds, compacte KNOD et monte HNUMA
      HNUME = LM_HGEO_REQHNUME(HLMGO)
      NNL = 0
      DO IG_NL=1,NNG
         IF (ERR_BAD()) GOTO 199

         IL_NL = 0                              ! Dans la renum interne
         IP_NL = -1                             !     i local, i process

         IE = KELE(IG_NL)
         IF (IE .GT. 0) THEN
            NNL = NNL + 1     ! cf. note
            IL_NL = NNL
            IP_NL = I_PROC

C---           Compacte
            KELE (IL_NL)   = KELE (IG_NL)
            VCORG(:,IL_NL) = VCORG(:,IG_NL)
            VCORE(:,IL_NL) = VCORE(:,IG_NL)
         ENDIF

         IERR = NM_AMNO_ASGROW(HALGN, IG_NL, IL_NL, IP_NL)
      ENDDO
199   CONTINUE

C---     Génère la renumérotation
      HNUMR = 0
      IF (ERR_GOOD()) IERR = NM_AMNO_GENNUM(HALGN, HNUMR)

C---     Détruis l'algo de renum
      IF (NM_AMNO_HVALIDE(HALGN)) IERR = NM_AMNO_DTR(HALGN)

      PS_SN0P_NINT = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Contrôle pour des points hors domaine
C
C Description:
C     La fonction privée PS_SN0P_CTRL
C
C Entrée:
C     HNUMR    Handle sur la numérotation du post-traitement
C     HGRID    Handle sur le maillage
C     NDIM     Nombre de  DIMensions
C     NNG      Nombre de points Globaux
C     VCORG    Table de CooRdonnées des points Globaux
C     KELE     Table des éléments des points
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN0P_CTRL(HOBJ,
     &                      HNUMR,
     &                      HLMGO,
     &                      NDIM,
     &                      NNG,
     &                      VCORG,
     &                      KELE)

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: HOBJ
      INTEGER, INTENT(IN) :: HNUMR
      INTEGER, INTENT(IN) :: HLMGO
      INTEGER, INTENT(IN) :: NDIM
      INTEGER, INTENT(IN) :: NNG
      REAL*8 , INTENT(IN) :: VCORG(NDIM, NNG)
      INTEGER, INTENT(IN) :: KELE (NNG)

      INCLUDE 'err.fi'
      INCLUDE 'lmhgeo.fi'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'mputil.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'pssn0p.fc'

      INTEGER IERR
      INTEGER HNUME
      INTEGER IPL, IPG, IEL, IEG
      INTEGER NNL
      INTEGER NERR
      INTEGER I_ERROR
      CHARACTER*(256) NOM
      INTEGER, ALLOCATABLE :: KTRV (:)
C------------------------------------------------------------------------
D     CALL ERR_PRE(LM_HGEO_HVALIDE(HLMGO))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les info
      HNUME= LM_HGEO_REQHNUME(HLMGO)
      NNL  = NM_NUMR_REQNNL  (HNUMR)

C---     Alloue la table de travail
      ALLOCATE(KTRV(NNG))

C---     Monte la table globale
      KTRV(:) = 0
      DO IPL=1,NNL
         IEL = KELE(IPL)
D        CALL ERR_ASR(IEL .GT. 0)
         IPG = NM_NUMR_REQNEXT(HNUMR, IPL)
D        CALL ERR_ASR(IPG .GE. 1 .AND. IPG .LE. NNG)
         IEG = NM_NUMR_REQNEXT(HNUME, IEL)
         KTRV(IPG) = IEG
      ENDDO

C---     Réduis
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, KTRV, NNG,
     &                   MP_TYPE_INT(), MPI_MAX,
     &                   MP_UTIL_REQCOMM(), I_ERROR)

C---     Contrôle les éléments négatifs/nulls
      IF (ERR_GOOD()) THEN
         NERR = 0
         DO IPL=1,NNG
            IF (KTRV(IPL) .LE. 0) NERR = NERR + 1
         ENDDO
         IF (NERR .GT. 0) THEN
            IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
            CALL ERR_ASG(ERR_ERR, 'ERR_POINTS_HORS_DOMAINE')
            DO IPL=1,NNG
               IF (KTRV(IPL) .LE. 0) THEN
                  WRITE(ERR_BUF, '(2A,I6)') 'MSG_POINT', ': ', IPL
                  CALL ERR_AJT(ERR_BUF)
               ENDIF
            ENDDO
            WRITE(ERR_BUF,'(3A)')'MSG_SELF',': ',NOM(1:SP_STRN_LEN(NOM))
            CALL ERR_AJT(ERR_BUF)
         ENDIF
      ENDIF
      IERR = MP_UTIL_SYNCERR()

C----    Récupère la mémoire
      DEALLOCATE(KTRV)

      PS_SN0P_CTRL = ERR_TYP()
      RETURN
      END
