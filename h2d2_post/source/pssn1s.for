C************************************************************************
C --- Copyright (c) INRS 2010-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  PoSt-traitement
C Objet:   SoNde 1d sur Scalaire
C Type:    Concret
C
C Functions:
C   Public:
C     INTEGER PS_SN1S_000
C     INTEGER PS_SN1S_999
C     INTEGER PS_SN1S_CTR
C     INTEGER PS_SN1S_DTR
C     INTEGER PS_SN1S_INI
C     INTEGER PS_SN1S_RST
C     INTEGER PS_SN1S_REQHBASE
C     LOGICAL PS_SN1S_HVALIDE
C     INTEGER PS_SN1S_ACC
C     INTEGER PS_SN1S_FIN
C     INTEGER PS_SN1S_XEQ
C     INTEGER PS_SN1S_ASGHSIM
C     CHARACTER*256 PS_SN1S_REQNOMF
C   Private:
C     INTEGER PS_SN1S_INTG
C
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1S_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1s.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(PS_SN1S_NOBJMAX,
     &                   PS_SN1S_HBASE,
     &                   'Post-Treatment Probe 1D on scalar')

      PS_SN1S_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1S_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1s.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER  IERR
      EXTERNAL PS_SN1S_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(PS_SN1S_NOBJMAX,
     &                   PS_SN1S_HBASE,
     &                   PS_SN1S_DTR)

      PS_SN1S_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   PS_SN1S_NOBJMAX,
     &                   PS_SN1S_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(PS_SN1S_HVALIDE(HOBJ))
         IOB = HOBJ - PS_SN1S_HBASE

         PS_SN1S_HSN1D(IOB) = 0
         PS_SN1S_HFITG(IOB) = 0
      ENDIF

      PS_SN1S_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = PS_SN1S_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   PS_SN1S_NOBJMAX,
     &                   PS_SN1S_HBASE)
      HOBJ = 0

      PS_SN1S_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Les contrôles sont faits par PS_SN1D_INI.
C     Les tables sont dimensionnées au nombre de noeuds globaux alors
C     qu'elles pourraient l'être au nombre de noeuds locaux.
C     Chaque process a tous les points. Il faut une op de réduction à
C     la fin.
C************************************************************************
      FUNCTION PS_SN1S_INI(HOBJ,
     &                     NOMFIC,
     &                     ISTAT,
     &                     IOPR,
     &                     IOPW,
     &                     NDIM,
     &                     NPNT,
     &                     X1,
     &                     X2,
     &                     HVNO,
     &                     INDX)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW
      INTEGER       NDIM
      INTEGER       NPNT
      REAL*8        X1(NDIM)
      REAL*8        X2(NDIM)
      INTEGER       HVNO
      INTEGER       INDX

      INCLUDE 'pssn1s.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER ITMP(1)         ! == INDX (Contrôle d'interface)
      INTEGER HFITG, HSN1D

      EXTERNAL PS_SN1S_INTG
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les données
      IERR = PS_SN1S_RST(HOBJ)

C---     Construit la fonction d'interpolation
      HFITG = 0
      IF (ERR_GOOD())IERR = SO_FUNC_CTR   (HFITG)
      IF (ERR_GOOD())IERR = SO_FUNC_INIFNC(HFITG, PS_SN1S_INTG)

C---     Construit et initialise le parent
      HSN1D = 0
      ITMP(1) = INDX
      IF (ERR_GOOD())IERR = PS_SN1D_CTR(HSN1D)
      IF (ERR_GOOD())IERR = PS_SN1D_INI(HSN1D,
     &                                  NOMFIC,
     &                                  ISTAT,
     &                                  IOPR,
     &                                  IOPW,
     &                                  NDIM,
     &                                  NPNT,
     &                                  X1,
     &                                  X2,
     &                                  1,      ! NVNO
     &                                  HVNO,
     &                                  ITMP,
     &                                  HFITG)

C---     Détruis la fonction en cas d'erreur
      IF (ERR_BAD() .AND. SO_FUNC_HVALIDE(HFITG))
     &   IERR = SO_FUNC_DTR (HFITG)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         IOB  = HOBJ - PS_SN1S_HBASE
         PS_SN1S_HSN1D(IOB) = HSN1D
         PS_SN1S_HFITG(IOB) = HFITG
      ENDIF

      PS_SN1S_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Réinitialise
C
C Description:
C     La fonction PS_SN1S_RST réinitialise l'objet. La mémoire est
C     désallouée, les objets attributs détruis et tous les attributs
C     réinitialisés.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1S_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HFITG, HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IOB  = HOBJ - PS_SN1S_HBASE

C---     Détruis le parent
      HSN1D = PS_SN1S_HSN1D(IOB)
      IF (PS_SN1D_HVALIDE(HSN1D)) IERR = PS_SN1D_DTR(HSN1D)

C---     Détruis la fonction d'intégration
      HFITG = PS_SN1S_HFITG(IOB)
      IF (SO_FUNC_HVALIDE(HFITG)) IERR = SO_FUNC_DTR(HFITG)

C---     RESET
      IF (ERR_GOOD()) THEN
         PS_SN1S_HSN1D(IOB) = 0
         PS_SN1S_HFITG(IOB) = 0
      ENDIF

      PS_SN1S_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_SN1S_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1S_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'pssn1s.fi'
      INCLUDE 'pssn1s.fc'
C------------------------------------------------------------------------

      PS_SN1S_REQHBASE = PS_SN1S_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_SN1S_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_SN1S_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'pssn1s.fc'
C------------------------------------------------------------------------

      PS_SN1S_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  PS_SN1S_NOBJMAX,
     &                                  PS_SN1S_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     En principe, les données ont déjà été chargées
C************************************************************************
      FUNCTION PS_SN1S_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_ACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1S_HSN1D(HOBJ-PS_SN1S_HBASE)
      PS_SN1S_ACC = PS_SN1D_ACC(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_FIN
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1S_HSN1D(HOBJ-PS_SN1S_HBASE)
      PS_SN1S_FIN = PS_SN1D_FIN(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_XEQ(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1s.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1S_HSN1D(HOBJ-PS_SN1S_HBASE)
      PS_SN1S_XEQ = PS_SN1D_XEQ(HSN1D, HSIM)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1S_ASGHSIM
C
C Description:
C     La fonction PS_SN1S_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_ASGHSIM
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM

      INCLUDE 'pssn1s.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1S_HSN1D(HOBJ-PS_SN1S_HBASE)
      PS_SN1S_ASGHSIM = PS_SN1D_ASGHSIM(HSN1D, HSIM)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1S_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_SN1S_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_SN1S_REQNOMF
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pssn1s.fi'
      INCLUDE 'pssn1d.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      INTEGER HSN1D
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_SN1S_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      HSN1D = PS_SN1S_HSN1D(HOBJ-PS_SN1S_HBASE)
      PS_SN1S_REQNOMF = PS_SN1D_REQNOMF(HSN1D)
      RETURN
      END

C************************************************************************
C Sommaire: PS_SN1S_INTG
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Interpolation linéaire
C
C************************************************************************
      FUNCTION PS_SN1S_INTG(NDIM,
     &                      NNL,
     &                      VCORG,
     &                      NNEL,
     &                      NELL,
     &                      KNGV,
     &                      NVNO,
     &                      VNOD,
     &                      VINT)

      IMPLICIT NONE

      INTEGER NDIM
      INTEGER NNL
      REAL*8  VCORG(NDIM, NNL)
      INTEGER NNEL
      INTEGER NELL
      INTEGER KNGV(NNEL, NELL)
      INTEGER NVNO
      REAL*8  VNOD(NVNO, NNL)
      REAL*8  VINT

      INCLUDE 'err.fi'
      INCLUDE 'pssn1s.fc'

      REAL*8  X1, Y1, X2, Y2, Dx, DY, DS
      REAL*8  V1, V2
      REAL*8  DEMI
      INTEGER IE
      INTEGER NO1, NO2

      PARAMETER (DEMI =0.5000000000000000D0)
C------------------------------------------------------------------------
C     CALL ERR_PRE(NDIM .EQ. 2)
C     CALL ERR_PRE(NVNO .EQ. 1)
C------------------------------------------------------------------------

      VINT = 0.0D0
      DO IE=1, NELL
         NO1 = KNGV(1, IE)
         NO2 = KNGV(2, IE)

         X1 = VCORG(1, NO1)
         Y1 = VCORG(2, NO1)
         X2 = VCORG(1, NO2)
         Y2 = VCORG(2, NO2)
         DX = X2 - X1
         DY = Y2 - Y1
         DS = HYPOT(DX, DY)

         V1 = VNOD(1, NO1)
         V2 = VNOD(1, NO2)

         VINT = VINT + DEMI*(V1+V2)*DS
      ENDDO

      PS_SN1S_INTG = ERR_TYP()
      RETURN
      END
