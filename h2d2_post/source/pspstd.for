C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER PS_PSTD_000
C     INTEGER PS_PSTD_999
C     INTEGER PS_PSTD_CTR
C     INTEGER PS_PSTD_DTR
C     INTEGER PS_PSTD_RAZ
C     INTEGER PS_PSTD_INI
C     INTEGER PS_PSTD_RST
C     INTEGER PS_PSTD_REQHBASE
C     LOGICAL PS_PSTD_HVALIDE
C     INTEGER PS_PSTD_LOGACC
C     INTEGER PS_PSTD_XEQ
C     INTEGER PS_PSTD_ASGHSIM
C     INTEGER PS_PSTD_REQHSIM
C     INTEGER PS_PSTD_REQHVNO
C     CHARACTER*256 PS_PSTD_REQNOMF
C     INTEGER PS_PSTD_REQOPR
C     INTEGER PS_PSTD_REQOPW
C     INTEGER PS_PSTD_LOGABS
C     INTEGER PS_PSTD_LOGVAL
C     INTEGER PS_PSTD_RDUC
C     INTEGER PS_PSTD_RDUCFIN
C     INTEGER PS_PSTD_STR2OP
C     CHARACTER*8 PS_PSTD_OP2STR
C   Private:
C     INTEGER PS_PSTD_DIMHVNO
C     INTEGER PS_PSTD_RDUCV0
C
C************************************************************************

      MODULE PS_PSTD_M

      IMPLICIT NONE
      PUBLIC

C---     Attributs statiques
      INTEGER, SAVE :: PS_PSTD_HBASE = -1

C---     Type de donnée associé à la classe
      TYPE :: PS_PSTD_SELF_T
C        <comment>Handle sur l'élément</comment>
         INTEGER HSIM
C        <comment>Handle sur le fichier</comment>
         INTEGER HFIC
C        <comment>Handle sur les données</comment>
         INTEGER HVNO
C        <comment>Opération de réduction</comment>
         INTEGER IOPR
C        <comment>Opération d'écriture</comment>
         INTEGER IOPW
C        <comment>Compteur</comment>
         INTEGER CNTR
      END TYPE PS_PSTD_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PS_PSTD_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PS_PSTD_SELF_T), POINTER :: PS_PSTD_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PS_PSTD_REQSELF => SELF
      RETURN
      END FUNCTION PS_PSTD_REQSELF

      END MODULE PS_PSTD_M

C************************************************************************
C Sommaire: Initialise les tables de la classe
C
C Description:
C     La fonction PS_PSTD_000 initialise les tables de la classe.
C     Elle doit obligatoirement être appelée avant toute utilisation
C     des autres fonctionnalités.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_000
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INCLUDE 'pspstd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PS_PSTD_HBASE,
     &                   'Post-Treatment with Data')

      PS_PSTD_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset des tables de la classe.
C
C Description:
C     La fonction PS_PSTD_999 reset les tables de la classe. C'est
C     la dernière fonction à appeler pour nettoyer. Elle va appeler
C     les destructeurs sur les objets non désalloués.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_999
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INCLUDE 'pspstd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PS_PSTD_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PS_PSTD_HBASE,
     &                   PS_PSTD_DTR)

      PS_PSTD_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Constructeur de la classe
C
C Description:
C     La fonction PS_PSTD_CTR agit comme constructeur de la classe.
C     Elle retourne un handle sur un nouvel objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_CTR
CDEC$ ENDIF

      USE PS_PSTD_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fc'

      INTEGER IERR, IRET
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PS_PSTD_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PS_PSTD_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PS_PSTD_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PS_PSTD_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_DTR
C
C Description:
C     La fonction PS_PSTD_DTR agit comme destructeur virtuel de la
C     structure de classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_DTR
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = PS_PSTD_RST(HOBJ)
      IERR = OB_OBJN_DTR(HOBJ, PS_PSTD_HBASE)

      PS_PSTD_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée PS_PSTD_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_RAZ(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_RAZ
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fc'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset les attributs
      SELF => PS_PSTD_REQSELF(HOBJ)
      SELF%HSIM = 0
      SELF%HFIC = 0
      SELF%HVNO = 0
      SELF%IOPR = PS_OP_INDEFINI
      SELF%IOPW = PS_OP_INDEFINI
      SELF%CNTR = 0

      PS_PSTD_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction PS_PSTD_INI initialise l'objet à l'aide des
C     paramètres.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier de données
C     ISTAT       Statut à l'ouverture du fichier
C     IOPR        Opération de réduction
C     IOPW        Opération d'écriture
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_INI(HOBJ, NOMFIC, ISTAT, IOPR, IOPW)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_INI
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC
      INTEGER       ISTAT
      INTEGER       IOPR
      INTEGER       IOPW

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HFIC, HVNO
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     CONTROLES DES PARAMETRES
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9900
      IF (IO_UTIL_MODACTIF(ISTAT, IO_MODE_LECTURE)) GOTO 9901

C---     RESET LES DONNEES
      IERR = PS_PSTD_RST(HOBJ)

C---     Construit et initialise le fichier de données
      IF (ERR_GOOD()) IERR = FD_PRNO_CTR(HFIC)
      IF (ERR_GOOD()) IERR = FD_PRNO_INI(HFIC,
     &                                   0,  ! HLIST
     &                                   NOMFIC,
     &                                   ISTAT)

C---     Construis le VNO (CTR uniquement)
      IF (ERR_GOOD()) IERR = DT_VNOD_CTR(HVNO)

C---     Assigne les valeurs
      IF (ERR_GOOD()) THEN
         SELF => PS_PSTD_REQSELF(HOBJ)
         SELF%HSIM = 0
         SELF%HFIC = HFIC
         SELF%HVNO = HVNO
         SELF%IOPR = IOPR
         SELF%IOPW = IOPW
         SELF%CNTR = 0
      ENDIF

      GOTO 9999
C-------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_VIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999
9901  WRITE(ERR_BUF, '(A)') 'ERR_FICHIER_EN_LECTURE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_PSTD_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Reset l'objet
C
C Description:
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_RST
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'pspstd.fc'

      INTEGER IERR
      INTEGER HFIC, HVNO
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_PSTD_REQSELF(HOBJ)
      HFIC = SELF%HFIC
      HVNO = SELF%HVNO

C---     Détruis les données
      IF (FD_PRNO_HVALIDE(HFIC)) IERR = FD_PRNO_DTR(HFIC)
      IF (DT_VNOD_HVALIDE(HVNO)) IERR = DT_VNOD_DTR(HVNO)

C---     Reset
      IERR = PS_PSTD_RAZ(HOBJ)

      PS_PSTD_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_PSTD_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQHBASE
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INCLUDE 'pspstd.fi'
C------------------------------------------------------------------------

      PS_PSTD_REQHBASE = PS_PSTD_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_PSTD_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_HVALIDE
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PS_PSTD_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PS_PSTD_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire:  PS_PSTD_LOGACC
C
C Description:
C     La fonction protégée PS_PSTD_LOGACC est une fonction auxiliaire
C     qui écris dans le log une variable de post-traitement.
C     On log les min et les max en valeur absolue de la variable.
C
C Entrée:
C     HNUMR    Handle sur la renumérotation
C     IPOST    Indice de la variable à logger
C     NPOST    Nombre de variables; dimension de la table VPOST
C     NNL      Nombre de noeuds locaux
C     VPOST    Table des valeurs VPOST(NPOST, NNL)
C     LPOST    Chaîne descriptive
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_LOGACC(HOBJ, LZN, MSG, TSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_LOGACC
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) LZN
      CHARACTER*(*) MSG
      REAL*8        TSIM

      INCLUDE 'pspstd.fi'
      INCLUDE 'log.fi'
      INCLUDE 'err.fi'

      INTEGER IOP
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      LOG_ZNE = LZN

C---     Log
      CALL LOG_INFO(LOG_ZNE, ' ')
      WRITE(LOG_BUF, '(A)') MSG
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      CALL LOG_INCIND()
      WRITE(LOG_BUF, '(2A,F25.6)') 'MSG_TEMPS_SIMU#<35>#', '= ', TSIM
      CALL LOG_INFO(LOG_ZNE, LOG_BUF)
      IOP = PS_PSTD_REQOPR(HOBJ)
      WRITE(LOG_BUF, '(2A,A)') 'MSG_OP#<35>#', '= ', PS_PSTD_OP2STR(IOP)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      IOP = PS_PSTD_REQOPW(HOBJ)
      WRITE(LOG_BUF, '(2A,A)') 'MSG_OW#<35>#', '= ', PS_PSTD_OP2STR(IOP)
      CALL LOG_VRBS(LOG_ZNE, LOG_BUF)
      CALL LOG_DECIND()

      PS_PSTD_LOGACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_XEQ
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_XEQ (HOBJ, HSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSOL

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'ERR_HANDLE_INVALIDE')

      PS_PSTD_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_DIMHVNO
C
C Description:
C     La méthode privée PS_PSTD_DIMHVNO dimensionne les données sous-jacentes
C     avec les dimensions passées en argument. La méthode peut être
C     appelée de manière répétitive avec les mêmes arguments sans effets.
C     Par contre, l'appel avec des arguments différents est une erreur.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_DIMHVNO(HOBJ, NDLN, NNL, NNT)

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NDLN
      INTEGER NNL
      INTEGER NNT

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'
      INCLUDE 'pspstd.fc'

      INTEGER IERR
      INTEGER HFIC, HVNO
      INTEGER NNT_L, NNL_L
      REAL*8, ALLOCATABLE :: VDUM(:)
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Récupère les attributs
      SELF => PS_PSTD_REQSELF(HOBJ)
      HFIC = SELF%HFIC
      HVNO = SELF%HVNO
D     CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))

C---     Contrôles
      IF (HFIC .EQ. 0) THEN
         IF (DT_VNOD_REQNVNO(HVNO) .NE. NDLN) GOTO 9900
         IF (DT_VNOD_REQNNL (HVNO) .NE. NNL)  GOTO 9900
         IF (DT_VNOD_REQNNT (HVNO) .NE. NNT)  GOTO 9900
      ENDIF

C---     Initialise les données et transfert le fichier
      IF (FD_PRNO_HVALIDE(HFIC)) THEN
D        CALL ERR_ASR(DT_VNOD_HVALIDE(HVNO))
         NNL_L = MERGE(DT_VNOD_DIM_DIFFERE, NNL, NNL .LT. 0)
         NNT_L = MERGE(DT_VNOD_DIM_DIFFERE, NNT, NNT .LT. 0)
         ALLOCATE(VDUM(NDLN))
         VDUM(:) = PS_PSTD_RDUCV0(HOBJ)
         IF (ERR_GOOD()) IERR = DT_VNOD_INIDIM   (HVNO,
     &                                            NDLN,
     &                                            NNL_L,
     &                                            NNT_L,
     &                                            .TRUE.,  ! ini
     &                                            VDUM)
         IF (ERR_GOOD()) IERR = DT_VNOD_ASGFICECR(HVNO, ' ', 0, HFIC)
         IF (ERR_GOOD()) HFIC = 0
         IF (ALLOCATED(VDUM)) DEALLOCATE(VDUM)
D     ELSE
D        CALL ERR_ASR(HFIC .EQ. 0)
      ENDIF

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF%HFIC = HFIC
         SELF%HVNO = HVNO
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_PARAMETRES_INVALIDES'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12,A,I12)')
     &   'NVNO', ' = ', DT_VNOD_REQNVNO(HVNO), ' / ', NDLN
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12,A,I12)')
     &   'NNL', ' = ', DT_VNOD_REQNNL (HVNO), ' / ', NNL
      CALL ERR_AJT(ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12,A,I12)')
     &   'NNT', ' = ', DT_VNOD_REQNNT (HVNO), ' / ', NNT
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_PSTD_DIMHVNO = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_ASGHSIM
C
C Description:
C     La fonction PS_PSTD_ASGHSIM assigne l'élément du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM       Handle sur l'élément
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_ASGHSIM(HOBJ, HSIM, NPST, NNL, NNT)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_ASGHSIM
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HSIM
      INTEGER NPST
      INTEGER NNL
      INTEGER NNT

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'
      INCLUDE 'pspstd.fc'

      INTEGER IERR
      INTEGER HVNO
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
D     CALL ERR_PRE(LM_HELE_HVALIDE(HSIM))
D     CALL ERR_PRE(NPST .GT. 0)
C------------------------------------------------------------------------

C---     Contrôles
      SELF => PS_PSTD_REQSELF(HOBJ)
      IF (SELF%HSIM .NE. 0 .AND. SELF%HSIM .NE. HSIM) GOTO 9900

C---     Assigne les dim au VNO associé
      IF (ERR_GOOD()) IERR = PS_PSTD_DIMHVNO(HOBJ, NPST, NNL, NNT)

C---     Assigne les attributs
      IF (ERR_GOOD()) SELF%HSIM = HSIM

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(2A)') 'ERR_REDEFINITION_HANDLE_INVALIDE'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      WRITE(ERR_BUF, '(2A,I12,A,I12)')
     &   'MSG_HANDLE', ' = ', SELF%HSIM, ' / ', HSIM
      CALL ERR_AJT(ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_PSTD_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_REQHSIM
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C   Sun: En release, SIGSEGV si on n'accède pas self. l'appel à
C        err_asr force cet accès, tout en étant correct.
C************************************************************************
      FUNCTION PS_PSTD_REQHSIM(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQHSIM
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'lmhele.fi'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      CALL ERR_ASR(SELF%HSIM .EQ. 0 .OR. LM_HELE_HVALIDE(SELF%HSIM)) ! cf note
      PS_PSTD_REQHSIM = SELF%HSIM
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_REQHVNO
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQHVNO
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
D     CALL ERR_ASR(SELF%HVNO .NE. 0)
      PS_PSTD_REQHVNO = SELF%HVNO
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_REQNOMF
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_REQNOMF(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQNOMF
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'dtvnod.fi'
      INCLUDE 'fdprno.fi'

      INTEGER IOB
      INTEGER HFIC
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      HFIC = SELF%HFIC
      IF (FD_PRNO_HVALIDE(HFIC)) THEN
         PS_PSTD_REQNOMF = FD_PRNO_REQNOMF(HFIC, 1)
      ELSE
         PS_PSTD_REQNOMF = DT_VNOD_REQNOMF(SELF%HVNO, 1)
      ENDIF
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_REQOPR
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_REQOPR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQOPR
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      PS_PSTD_REQOPR = SELF%IOPR
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_REQOPW
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_REQOPW(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_REQOPW
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      PS_PSTD_REQOPW = SELF%IOPW
      RETURN
      END

C************************************************************************
C Sommaire:  PS_PSTD_LOGABS
C
C Description:
C     La fonction protégée PS_PSTD_LOGABS est une fonction auxiliaire
C     qui écris dans le log une variable de post-traitement.
C     On log les min et les max en valeur absolue de la variable.
C
C Entrée:
C     HNUMR    Handle sur la renumérotation
C     IPOST    Indice de la variable à logger
C     NPOST    Nombre de variables; dimension de la table VPOST
C     NNL      Nombre de noeuds locaux
C     VPOST    Table des valeurs VPOST(NPOST, NNL)
C     LPOST    Chaîne descriptive
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_LOGABS(HNUMR,
     &                        IPOST,
     &                        NPOST,
     &                        NNL,
     &                        VPOST,
     &                        LPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_LOGABS
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HNUMR
      INTEGER       IPOST
      INTEGER       NPOST
      INTEGER       NNL
      REAL*8        VPOST(NPOST, NNL)
      CHARACTER*(*) LPOST

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'

      REAL*8  VMIN_L, VMAX_L
      REAL*8  VMIN_G, VMAX_G
      INTEGER NMIN_L, NMAX_L
      INTEGER NMIN_G, NMAX_G
      INTEGER NNT
      INTEGER IERR

      INTEGER IDAMIN, IDAMAX           ! Fonctions BLAS

      INTEGER I_ERROR                  ! MPI
      INTEGER I_OPSIGNEDMAX
      INTEGER I_OPSIGNEDMIN
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IPOST .LE. NPOST)
D     CALL ERR_PRE(HNUMR .EQ. 0 .OR. NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

C---     Info locales
      NMIN_L = IDAMIN(NNL, VPOST(IPOST,1), NPOST)
      NMAX_L = IDAMAX(NNL, VPOST(IPOST,1), NPOST)
      VMIN_L = VPOST(IPOST, NMIN_L)
      VMAX_L = VPOST(IPOST, NMAX_L)

C---     Noeuds en num globale
      IF (NM_NUMR_CTRLH(HNUMR)) THEN
         NMIN_L = NM_NUMR_REQNEXT(HNUMR, NMIN_L)
         NMAX_L = NM_NUMR_REQNEXT(HNUMR, NMAX_L)
      ENDIF

C---     Demande les op de reduction
      I_OPSIGNEDMAX = MP_UTIL_GETOP_SIGNEDMAX()
      I_OPSIGNEDMIN = MP_UTIL_GETOP_SIGNEDMIN()

C---     Synchronise
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(NNL, NNT, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(VMIN_L, VMIN_G, 1, MP_TYPE_RE8(),
     &                      I_OPSIGNEDMIN, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (VMIN_G .EQ. VMIN_L) NMIN_L = NMIN_L + NNT
         CALL MPI_ALLREDUCE(NMIN_L, NMIN_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NMIN_G = NMIN_G - NNT
      ENDIF

      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(VMAX_L, VMAX_G, 1, MP_TYPE_RE8(),
     &                      I_OPSIGNEDMAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (VMAX_G .EQ. VMAX_L) NMAX_L = NMAX_L + NNT
         CALL MPI_ALLREDUCE(NMAX_L, NMAX_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NMAX_G = NMAX_G - NNT
      ENDIF

C---     Log les infos globales
      WRITE(LOG_BUF, '(A,2(A,1PE14.6E3,I9))')
     &   LPOST(1:SP_STRN_LEN(LPOST)), '#<15>#(amin)=', VMIN_G, NMIN_G,
     &                                    '  (amax)=', VMAX_G, NMAX_G
      CALL LOG_ECRIS(LOG_BUF)

      PS_PSTD_LOGABS = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:  PS_PSTD_LOGVAL
C
C Description:
C     La fonction protégée PS_PSTD_LOGVAL est une fonction auxiliaire
C     qui écris dans le log une variable de post-traitement.
C     On log les min et les max de la variable.
C
C Entrée:
C     HNUMR    Handle sur la renumérotation
C     IPOST    Indice de la variable à logger
C     NPOST    Nombre de variables; dimension de la table VPOST
C     NNL      Nombre de noeuds locaux
C     VPOST    Table des valeurs VPOST(NPOST, NNL)
C     LPOST    Chaîne descriptive
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_LOGVAL(HNUMR,
     &                        IPOST,
     &                        NPOST,
     &                        NNL,
     &                        VPOST,
     &                        LPOST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_LOGVAL
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HNUMR
      INTEGER       IPOST
      INTEGER       NPOST
      INTEGER       NNL
      REAL*8        VPOST(NPOST, NNL)
      CHARACTER*(*) LPOST

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'mpif.h'
      INCLUDE 'nmnumr.fi'
      INCLUDE 'mputil.fi'
      INCLUDE 'spstrn.fi'

      REAL*8  VMIN_L, VMAX_L
      REAL*8  VMIN_G, VMAX_G
      INTEGER NMIN_L, NMAX_L
      INTEGER NMIN_G, NMAX_G
      INTEGER NNT
      INTEGER IERR

      INTEGER IDMIN, IDMAX             ! Fonctions BLAS

      INTEGER I_ERROR                  ! MPI
C-----------------------------------------------------------------------
D     CALL ERR_PRE(IPOST .LE. NPOST)
D     CALL ERR_PRE(HNUMR .EQ. 0 .OR. NM_NUMR_CTRLH(HNUMR))
C-----------------------------------------------------------------------

C---     Info locales
      NMIN_L = IDMIN(NNL, VPOST(IPOST,1), NPOST)
      NMAX_L = IDMAX(NNL, VPOST(IPOST,1), NPOST)
      VMIN_L = VPOST(IPOST, NMIN_L)
      VMAX_L = VPOST(IPOST, NMAX_L)

C---     Noeuds en num globale
      IF (NM_NUMR_CTRLH(HNUMR)) THEN
         NMIN_L = NM_NUMR_REQNEXT(HNUMR, NMIN_L)
         NMAX_L = NM_NUMR_REQNEXT(HNUMR, NMAX_L)
      ENDIF

C---     Synchronise
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(NNL, NNT, 1, MP_TYPE_INT(),
     &                      MPI_SUM, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(VMIN_L, VMIN_G, 1, MP_TYPE_RE8(),
     &                      MPI_MIN, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (VMIN_G .EQ. VMIN_L) NMIN_L = NMIN_L + NNT
         CALL MPI_ALLREDUCE(NMIN_L, NMIN_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NMIN_G = NMIN_G - NNT
      ENDIF

      IF (ERR_GOOD()) THEN
         CALL MPI_ALLREDUCE(VMAX_L, VMAX_G, 1, MP_TYPE_RE8(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
      ENDIF
      IF (ERR_GOOD()) THEN
         IF (VMAX_G .EQ. VMAX_L) NMAX_L = NMAX_L + NNT
         CALL MPI_ALLREDUCE(NMAX_L, NMAX_G, 1, MP_TYPE_INT(),
     &                      MPI_MAX, MP_UTIL_REQCOMM(), I_ERROR)
         NMAX_G = NMAX_G - NNT
      ENDIF

C---     Log les infos globales
      WRITE(LOG_BUF, '(A,2(A,1PE14.6E3,I9))')
     &   LPOST(1:SP_STRN_LEN(LPOST)), '#<15>#(vmin)=', VMIN_G, NMIN_G,
     &                                  '  (vmax)=', VMAX_G, NMAX_G
      CALL LOG_ECRIS(LOG_BUF)

      PS_PSTD_LOGVAL = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_RDUC
C
C Description:
C     La méthode protégée PS_PSTD_RDUC fait les opérations de réduction
C     entre deux tables réelles, lors du post-traitement. Sémantiquement,
C     l'opération revient à:
C        Y = OP(X,Y)
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NVAL     Nombre d'opération à effectuer
C     X        Table
C     IX       Incrément dans la table X
C     Y        Table résultat
C     IY       Incrément dans la table Y
C
C Sortie:
C     Y        Table résultat modifiée
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_RDUC(HOBJ, NVAL, X, INCX, Y, INCY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_RDUC
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      REAL*8  X(*)
      INTEGER INCX
      REAL*8  Y(*)
      INTEGER INCY

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      INTEGER IOP
      INTEGER I, IX, IY
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      IOP = SELF%IOPR

      IX = INCX
      IY = INCY
      IF      (IOP .EQ. PS_OP_NOOP) THEN
      ELSE IF (IOP .EQ. PS_OP_COPY) THEN
         DO I=1,NVAL
            Y(IY) = X(IX)
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_SUM) THEN
         DO I=1,NVAL
            Y(IY) = Y(IY) + X(IX)
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_SUMABS) THEN
         DO I=1,NVAL
            Y(IY) = ABS(Y(IY)) + ABS(X(IX))
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_MIN) THEN
         DO I=1,NVAL
            Y(IY) = MIN(Y(IY), X(IX))
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_MINABS) THEN
         DO I=1,NVAL
            Y(IY) = MIN(ABS(Y(IY)), ABS(X(IX)))
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_MAX) THEN
         DO I=1,NVAL
            Y(IY) = MAX(Y(IY), X(IX))
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_MAXABS) THEN
         DO I=1,NVAL
            Y(IY) = MAX(ABS(Y(IY)), ABS(X(IX)))
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE IF (IOP .EQ. PS_OP_MEAN) THEN
         DO I=1,NVAL
            Y(IY) = Y(IY) + X(IX)
            IX = IX + INCX
            IY = IY + INCY
         ENDDO
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_OPRDUC_INVALIDE')
         CALL ERR_AJT(PS_PSTD_OP2STR(IOP))
      ENDIF

      SELF%CNTR = SELF%CNTR + 1

      PS_PSTD_RDUC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_RDUCV0
C
C Description:
C     La méthode protégée PS_PSTD_RDUCV0 retourne la valeur d'initialisation
C     des tables pour l'opération de réduction.
C
C Entrée:
C
C Sortie:
C     V     Valeur d'initialisation des tables
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_RDUCV0(HOBJ)

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGEr HOBJ

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspstd.fc'

      REAL*8, PARAMETER :: TGV = 1.0D+99
      REAL*8  V
      INTEGER IOP
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      IOP = SELF%IOPR

      IF      (IOP .EQ. PS_OP_NOOP) THEN
         V = 0.0D0
      ELSE IF (IOP .EQ. PS_OP_COPY) THEN
         V = 0.0D0
      ELSE IF (IOP .EQ. PS_OP_SUM) THEN
         V = 0.0D0
      ELSE IF (IOP .EQ. PS_OP_SUMABS) THEN
         V = 0.0D0
      ELSE IF (IOP .EQ. PS_OP_MIN) THEN
         V = TGV
      ELSE IF (IOP .EQ. PS_OP_MINABS) THEN
         V = TGV
      ELSE IF (IOP .EQ. PS_OP_MAX) THEN
         V = -TGV
      ELSE IF (IOP .EQ. PS_OP_MAXABS) THEN
         V = 0.0D0
      ELSE IF (IOP .EQ. PS_OP_MEAN) THEN
         V = 0.0D0
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_OPRDUC_INVALIDE')
         CALL ERR_AJT(PS_PSTD_OP2STR(IOP))
      ENDIF

      PS_PSTD_RDUCV0 = V
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_RDUCFIN
C
C Description:
C     La méthode protégée PS_PSTD_RDUCFIN finalise l'opération de réduction
C     pour les opérations le nécessitant.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     NVAL     Nombre d'opération à effectuer
C     Y        Table
C     IY       Incrément dans la table Y
C
C Sortie:
C     Y        Table résultat modifiée
C
C Notes:
C
C************************************************************************
      FUNCTION PS_PSTD_RDUCFIN(HOBJ, NVAL, Y, INCY)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_RDUCFIN
CDEC$ ENDIF

      USE PS_PSTD_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER NVAL
      REAL*8  Y(*)
      INTEGER INCY

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      INTEGER IOP, CNT
      INTEGER I, IY
      REAL*8  V
      TYPE (PS_PSTD_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_PSTD_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_PSTD_REQSELF(HOBJ)
      IOP = SELF%IOPR
      CNT = SELF%CNTR

      IY = INCY
      IF      (IOP .EQ. PS_OP_NOOP) THEN
      ELSE IF (IOP .EQ. PS_OP_COPY) THEN
      ELSE IF (IOP .EQ. PS_OP_SUM) THEN
      ELSE IF (IOP .EQ. PS_OP_SUMABS) THEN
      ELSE IF (IOP .EQ. PS_OP_MIN) THEN
      ELSE IF (IOP .EQ. PS_OP_MINABS) THEN
      ELSE IF (IOP .EQ. PS_OP_MAX) THEN
      ELSE IF (IOP .EQ. PS_OP_MAXABS) THEN
      ELSE IF (IOP .EQ. PS_OP_MEAN) THEN
         IF (CNT .GT. 0) THEN
            V = 1.0D0 / CNT
            DO I=1,NVAL
               Y(IY) = Y(IY)*V
               IY = IY + INCY
            ENDDO
         ENDIF
      ELSE
         CALL ERR_ASG(ERR_ERR, 'ERR_OPRDUC_INVALIDE')
         CALL ERR_AJT(PS_PSTD_OP2STR(IOP))
      ENDIF

      PS_PSTD_RDUCFIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_STR2OP
C
C Description:
C     La fonction protégée PS_PSTD_STR2OP traduis une opération de sa forme
C     chaîne de caractères à sa forme d'énumération INTEGER. En cas d'erreur
C     on retourne PS_OP_INDEFINI.
C
C Entrée:
C     OPER      La chaîne à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_STR2OP(OPER)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_STR2OP
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER(*) OPER

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      INTEGER IOP
C------------------------------------------------------------------------

      IF      (OPER .EQ. 'noop') THEN
         IOP = PS_OP_NOOP
      ELSE IF (OPER .EQ. 'copy') THEN
         IOP = PS_OP_COPY
      ELSE IF (OPER .EQ. 'sum') THEN
         IOP = PS_OP_SUM
      ELSE IF (OPER .EQ. 'sumabs') THEN
         IOP = PS_OP_SUMABS
      ELSE IF (OPER .EQ. 'min') THEN
         IOP = PS_OP_MIN
      ELSE IF (OPER .EQ. 'minabs') THEN
         IOP = PS_OP_MINABS
      ELSE IF (OPER .EQ. 'max') THEN
         IOP = PS_OP_MAX
      ELSE IF (OPER .EQ. 'maxabs') THEN
         IOP = PS_OP_MAXABS
      ELSE IF (OPER .EQ. 'mean') THEN
         IOP = PS_OP_MEAN
      ELSE IF (OPER .EQ. 'nowrite') THEN
         IOP = PS_OP_NOWRITE
      ELSE IF (OPER .EQ. 'write') THEN
         IOP = PS_OP_WRITE
      ELSE
         IOP = PS_OP_INDEFINI
      ENDIF

      PS_PSTD_STR2OP = IOP
      RETURN
      END

C************************************************************************
C Sommaire: PS_PSTD_OP2STR
C
C Description:
C     La fonction protégée PS_PSTD_OP2STR traduis une opération de sa forme
C     d'énumération INTEGER à sa forme chaîne de caractères.
C
C Entrée:
C     IOP      L'opération à traduire
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_PSTD_OP2STR(IOP)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_PSTD_OP2STR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER IOP

      INCLUDE 'pspstd.fi'
      INCLUDE 'err.fi'

      CHARACTER*(8) OPER
C------------------------------------------------------------------------

      IF      (IOP .EQ. PS_OP_NOOP) THEN
         OPER = 'noop'
      ELSE IF (IOP .EQ. PS_OP_COPY) THEN
         OPER = 'copy'
      ELSE IF (IOP .EQ. PS_OP_SUM) THEN
         OPER = 'sum'
      ELSE IF (IOP .EQ. PS_OP_SUMABS) THEN
         OPER = 'sumabs'
      ELSE IF (IOP .EQ. PS_OP_MIN) THEN
         OPER = 'min'
      ELSE IF (IOP .EQ. PS_OP_MINABS) THEN
         OPER = 'minabs'
      ELSE IF (IOP .EQ. PS_OP_MAX) THEN
         OPER = 'max'
      ELSE IF (IOP .EQ. PS_OP_MAXABS) THEN
         OPER = 'maxabs'
      ELSE IF (IOP .EQ. PS_OP_MEAN) THEN
         OPER = 'mean'
      ELSE IF (IOP .EQ. PS_OP_NOWRITE) THEN
         OPER = 'nowrite'
      ELSE IF (IOP .EQ. PS_OP_WRITE) THEN
         OPER = 'write'
      ELSE
         OPER = 'invalid'
         CALL ERR_ASG(ERR_ERR, 'ERR_OP_INVALIDE')
      ENDIF

      PS_PSTD_OP2STR = OPER
      RETURN
      END
