C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_DLIB_XEQCTR(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_DLIB_XEQCTR
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'psdlib_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'ioutil.fi'
      INCLUDE 'psdlib.fi'
      INCLUDE 'pspost.fi'
      INCLUDE 'pspstd.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psdlib_ic.fc'

      INTEGER IERR
      INTEGER ISTAT
      INTEGER HPST, HOBJ
      INTEGER IOPR, IOPW
      CHARACTER*(256) NOMFIC
      CHARACTER*(256) NTMP
      CHARACTER*(  8) MODE
      CHARACTER*(  8) OPRD, OPWR
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_PS_DLIB_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     LIS LES PARAM
      IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
C     <comment>Name of the post-treatment file</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
      IF (IERR .NE. 0) GOTO 9901
C     <comment>Opening mode ['w', 'a'] (default 'a')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 2, MODE)
      IF (IERR .NE. 0) MODE = 'a'
C     <comment>Reduction operation ['sum', 'sumabs', 'min', 'minabs', 'max', 'maxabs', 'mean'] (default 'noop')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 3, OPRD)
      IF (IERR .NE. 0) OPRD = 'noop'
C     <comment>Writing operation ['write', 'nowrite'] (default 'nowrite')</comment>
      IF (IERR .EQ. 0) IERR = SP_STRN_TKS(IPRM, ',', 4, OPWR)
      IF (IERR .NE. 0) OPWR = 'nowrite'
      CALL SP_STRN_TRM(NOMFIC)

C---     VALIDE
      IF (SP_STRN_LEN(NOMFIC) .LE. 0) GOTO 9902
      ISTAT = IO_UTIL_STR2MOD(MODE(1:SP_STRN_LEN(MODE)))
      IF (ISTAT .EQ. IO_MODE_INDEFINI) GOTO 9904
      IOPR = PS_PSTD_STR2OP(OPRD(1:SP_STRN_LEN(OPRD)))
      IF (IOPR .EQ. PS_OP_INDEFINI .OR.
     &    IOPR .EQ. PS_OP_NOWRITE  .OR.
     &    IOPR .EQ. PS_OP_WRITE) GOTO 9905
      IOPW = PS_PSTD_STR2OP(OPWR(1:SP_STRN_LEN(OPWR)))
      IF (IOPW .NE. PS_OP_NOWRITE .AND.
     &    IOPW .NE. PS_OP_WRITE) GOTO 9906

C---     CONSTRUIS ET INITIALISE L'OBJET
      HPST = 0
      IF (ERR_GOOD()) IERR=PS_DLIB_CTR(HPST)
      IF (ERR_GOOD()) IERR=PS_DLIB_INI(HPST,NOMFIC,ISTAT,IOPR,IOPW)

C---     CONSTRUIS ET INITIALISE LE PROXY
      HOBJ = 0
      IF (ERR_GOOD()) IERR = PS_POST_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = PS_POST_INI(HOBJ, HPST)

C---     IMPRIME L'OBJET
      IF (ERR_GOOD()) THEN
         IERR = IC_PS_DLIB_PRN(HPST)
      END IF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the post-treatment</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>post_dlib</b> constructs an object, with the given
C  arguments, and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(A)') 'ERR_NOM_FICHIER_ATTENDU'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9904  WRITE(ERR_BUF, '(3A)') 'ERR_MODE_INVALIDE',': ', MODE(1:2)
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9905  WRITE(ERR_BUF, '(3A)') 'ERR_OPRDUC_INVALIDE',': ', OPRD
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9906  WRITE(ERR_BUF, '(3A)') 'ERR_OPWRITE_INVALIDE',': ', OPWR
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_DLIB_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_PS_DLIB_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_DLIB_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_DLIB_XEQMTH
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'psdlib_ic.fi'
      INCLUDE 'err.fi'
      INCLUDE 'psdlib.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psdlib_ic.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Les méthodes virtuelles sont gérées par le proxy
D     IF (IMTH .EQ. 'xeq') CALL ERR_ASR(.FALSE.)
D     IF (IMTH .EQ. 'del') CALL ERR_ASR(.FALSE.)
C     <include>IC_PS_POST_XEQMTH@pspost_ic.for</include>

C     <comment>The method <b>print</b> prints information about the object.</comment>
      IF (IMTH .EQ. 'print') THEN
D        CALL ERR_ASR(PS_DLIB_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = PS_DLIB_PRN(HOBJ)
         IERR = IC_PS_DLIB_PRN(HOBJ)

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_PS_DLIB_AID()

      ELSE
         GOTO 9902
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9902  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_PS_DLIB_AID()

9999  CONTINUE
      IC_PS_DLIB_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_DLIB_REQCLS()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_DLIB_REQCLS
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psdlib_ic.fi'
C-------------------------------------------------------------------------

C<comment>
C   The class <b>post_dlib</b> represents the post-treatment on the degrees of freedom.
C</comment>
      IC_PS_DLIB_REQCLS = 'post_dlib'
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_PS_DLIB_REQHDL()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_PS_DLIB_REQHDL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'psdlib_ic.fi'
      INCLUDE 'psdlib.fi'
C-------------------------------------------------------------------------

      IC_PS_DLIB_REQHDL = PS_DLIB_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire: Aide
C
C Description:
C        La fonction IC_PS_DLIB_AID qui permet d'écrire dans un fichier d'aide
C        pour l'utilisateur
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_PS_DLIB_AID()

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('psdlib_ic.hlp')

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_PS_DLIB_PRN(HOBJ)

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'psdlib.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'psdlib_ic.fc'

      INTEGER IERR
      CHARACTER*(256) NOM
C------------------------------------------------------------------------
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     EN-TETE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF, '(A)') 'MSG_POST_DLIB'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     IMPRESSION DES PARAMETRES DU BLOC
      IERR = OB_OBJC_REQNOMCMPL(NOM, HOBJ)
      WRITE (LOG_BUF,'(A,A)') 'MSG_SELF#<25>#= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
      CALL LOG_ECRIS(LOG_BUF)

      NOM = PS_DLIB_REQNOMF(HOBJ)
      IF (SP_STRN_LEN(NOM) .NE. 0) THEN
         CALL SP_STRN_CLP(NOM, 50)
         WRITE (LOG_BUF,'(3A)')  'MSG_NOM_FICHIER#<25>#', '= ',
     &                           NOM(1:SP_STRN_LEN(NOM))
         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

      CALL LOG_DECIND()

      IC_PS_DLIB_PRN = ERR_TYP()
      RETURN
      END
      