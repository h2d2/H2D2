C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Functions:
C   Public:
C     INTEGER PS_POST_000
C     INTEGER PS_POST_999
C     INTEGER PS_POST_CTR
C     INTEGER PS_POST_DTR
C     INTEGER PS_POST_INI
C     INTEGER PS_POST_RST
C     INTEGER PS_POST_REQHBASE
C     LOGICAL PS_POST_HVALIDE
C     INTEGER PS_POST_REQHPST
C     INTEGER PS_POST_ACC
C     INTEGER PS_POST_FIN
C     INTEGER PS_POST_XEQ
C     INTEGER PS_POST_ASGHSIM
C     INTEGER PS_POST_REQHVNO
C   Private:
C     SUBROUTINE PS_POST_REQSELF
C     INTEGER PS_POST_RAZ
C
C************************************************************************

      MODULE PS_POST_M

      IMPLICIT NONE
      PUBLIC

!      INCLUDE 'obobjc.fi'

C---     Attributs statiques
      INTEGER, SAVE :: PS_POST_HBASE = -1 !! OB_OBJC_HBSP_TAG

C---     Type de donnée associé à la classe
      TYPE :: PS_POST_SELF_T
C        <comment>Handle sur l'objet managé</comment>
         INTEGER HPST
C        <comment>Handle sur le module de l'objet managé</comment>
         INTEGER HMDL
      END TYPE PS_POST_SELF_T

      CONTAINS

C************************************************************************
C Sommaire:    Retourne le pointeur aux attributs.
C
C Description:
C     La fonction privée PS_POST_REQSELF(...) retourne le pointeur à
C     la structure de données de l'objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_REQSELF(HOBJ)

      USE ISO_C_BINDING
      IMPLICIT NONE

      TYPE (PS_POST_SELF_T), POINTER :: PS_POST_REQSELF
      INTEGER HOBJ

      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'

      TYPE (PS_POST_SELF_T), POINTER :: SELF
      TYPE (C_PTR) :: CELF

      INTEGER IERR
C-----------------------------------------------------------------------

      CALL ERR_PUSH()
      IERR = OB_OBJN_REQDTA(HOBJ, CELF)
      CALL ERR_ASR(ERR_GOOD())
      CALL ERR_POP()
      CALL C_F_POINTER(CELF, SELF)

      PS_POST_REQSELF => SELF
      RETURN
      END FUNCTION PS_POST_REQSELF

      END MODULE PS_POST_M

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_000
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INCLUDE 'pspost.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJN_000(PS_POST_HBASE,
     &                   'ABC: Post-Treatment')

      PS_POST_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_999
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INCLUDE 'pspost.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      EXTERNAL PS_POST_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJN_999(PS_POST_HBASE,
     &                   PS_POST_DTR)

      PS_POST_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_POST_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_CTR
CDEC$ ENDIF

      USE PS_POST_M
      USE ISO_C_BINDING
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fc'

      INTEGER IERR, IRET
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
      ALLOCATE (SELF, STAT=IRET)
      IF (IRET .NE. 0) GOTO 9900

C---     Enregistre
      IF (ERR_GOOD()) IERR = OB_OBJN_CTR(HOBJ,
     &                                   PS_POST_HBASE,
     &                                   C_LOC(SELF))

C---     Initialise
      IF (ERR_GOOD()) IERR = PS_POST_RAZ(HOBJ)
D     CALL ERR_ASR(ERR_BAD() .OR. PS_POST_HVALIDE(HOBJ))

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      PS_POST_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_POST_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_DTR
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'

      INTEGER  IERR
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C-----------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Reset
      IERR = PS_POST_RST(HOBJ)

C---     Dé-alloue
      SELF => PS_POST_REQSELF(HOBJ)
D     CALL ERR_ASR(ASSOCIATED(SELF))
      DEALLOCATE(SELF)

C---     Dé-enregistre
      IERR = OB_OBJN_DTR(HOBJ, PS_POST_HBASE)

      PS_POST_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Remise à zéro des attributs (eRAZe)
C
C Description:
C     La méthode privée PS_POST_RAZ (re)met les attributs à zéro
C     ou à leur valeur par défaut. C'est une initialisation de bas niveau de
C     l'objet. Elle efface. Il n'y a pas de destruction ou de désallocation,
C     ceci est fait par _RST.
C
C Entrée:
C     HOBJ        Handle sur l'objet courant
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_RAZ(HOBJ)

      USE PS_POST_M

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'pspost.fc'

      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)

      SELF%HPST = 0
      SELF%HMDL = 0

      PS_POST_RAZ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_POST_INI(HOBJ, HPST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_INI
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HPST

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'soutil.fi'

      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  HMDL
      LOGICAL  HVALIDE
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
D     CALL ERR_PRE(HPST .GT. 0)
C------------------------------------------------------------------------

C---     Reset les données
      IERR = PS_POST_RST(HOBJ)

C---     Connecte au module
      HMDL = 0
      IERR = SO_UTIL_REQHMDLHBASE(HPST, HMDL)
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Contrôle le handle
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 9900

C---     Ajoute le lien
      IF (ERR_GOOD()) IERR = OB_OBJC_ASGLNK(HOBJ, HPST)

C---     Assigne les attributs
      IF (ERR_GOOD()) THEN
         SELF => PS_POST_REQSELF(HOBJ)
         SELF%HPST = HPST
         SELF%HMDL = HMDL
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF,'(2A,I12)')'ERR_HANDLE_INVALIDE',': ',HPST
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9999

9999  CONTINUE
      PS_POST_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION PS_POST_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_RST
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'somdul.fi'
      INCLUDE 'pspost.fc'

      INTEGER  IERR
      INTEGER  HFNC
      INTEGER  HMDL
      INTEGER  HPST
      LOGICAL  HVALIDE
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
      IF (.NOT. SO_MDUL_HVALIDE(HMDL)) GOTO 1000

C---     Enlève le lien
      IF (ERR_GOOD()) IERR = OB_OBJC_EFFLNK(HOBJ)

C---     Contrôle le handle managé
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'HVALIDE', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))
      IF (ERR_GOOD()) HVALIDE = (IERR .NE. 0)
      IF (ERR_GOOD() .AND. .NOT. HVALIDE) GOTO 1000

C---     Fait l'appel pour détruire l'objet associé
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'DTR', HFNC)
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))

C---     Reset les attributs
1000  CONTINUE
      IERR = PS_POST_RAZ(HOBJ)

      PS_POST_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction PS_POST_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_REQHBASE
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INCLUDE 'pspost.fi'
C------------------------------------------------------------------------

      PS_POST_REQHBASE = PS_POST_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction PS_POST_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_HVALIDE
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'obobjc.fi'
C------------------------------------------------------------------------

      PS_POST_HVALIDE = OB_OBJN_HVALIDE(HOBJ, PS_POST_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: PS_POST_REQHPST
C
C Description:
C     La fonction PS_POST_REQHPST retourne le handle au post-traitement
C     géré par l'objet virtuel.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_REQHPST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_REQHPST
CDEC$ ENDIF

      USE PS_POST_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'

      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      SELF => PS_POST_REQSELF(HOBJ)
      PS_POST_REQHPST = SELF%HPST
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_POST_ACC exécute le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_ACC(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_ACC
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HPST
      INTEGER HMDL, HFNC
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ACC', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))

      PS_POST_ACC = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_POST_FIN exécute le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_FIN(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_FIN
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HPST
      INTEGER HMDL, HFNC
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'FIN', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))

      PS_POST_FIN = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction PS_POST_XEQ exécute le post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_XEQ(HOBJ, HSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_XEQ
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSOL

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HPST
      INTEGER HMDL, HFNC
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'XEQ', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2 (HFNC,
     &                                      SO_ALLC_CST2B(HPST),
     &                                      SO_ALLC_CST2B(HSOL))

      PS_POST_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_POST_ASGHSIM
C
C Description:
C     La fonction PS_POST_ASGHSIM assigne les données de simulation du
C     post-traitement.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HSIM        Handle sur les données de simulation
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_ASGHSIM(HOBJ, HSIM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_ASGHSIM
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HPST
      INTEGER HMDL, HFNC
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'ASGHSIM', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) IERR = SO_FUNC_CALL2 (HFNC,
     &                                      SO_ALLC_CST2B(HPST),
     &                                      SO_ALLC_CST2B(HSIM))

      PS_POST_ASGHSIM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: PS_POST_REQHVNO
C
C Description:
C     La fonction PS_POST_REQHVNO retourne le handle au DT_VNOD associé
C     au post-traitement.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION PS_POST_REQHVNO(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: PS_POST_REQHVNO
CDEC$ ENDIF

      USE PS_POST_M
      USE SO_FUNC_M
      USE SO_ALLC_M
      IMPLICIT NONE

      INTEGER  HOBJ

      INCLUDE 'pspost.fi'
      INCLUDE 'err.fi'
      INCLUDE 'somdul.fi'

      INTEGER IERR
      INTEGER HPST, HVNO
      INTEGER HMDL, HFNC
      TYPE (PS_POST_SELF_T), POINTER :: SELF
C------------------------------------------------------------------------
D     CALL ERR_PRE(PS_POST_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     Récupère les attributs
      SELF => PS_POST_REQSELF(HOBJ)
      HPST = SELF%HPST
      HMDL = SELF%HMDL
D     CALL ERR_ASR(SO_MDUL_HVALIDE(HMDL))

C---     Charge la fonction
      HFNC = 0
      IF (ERR_GOOD()) IERR = SO_MDUL_CCHFNC(HMDL, 'REQHVNO', HFNC)

C---     Fait l'appel
      IF (ERR_GOOD()) HVNO = SO_FUNC_CALL1 (HFNC,
     &                                      SO_ALLC_CST2B(HPST))

      PS_POST_REQHVNO = HVNO
      RETURN
      END
