class probe_on_points
=====================
 
   The class **probe_on_points** represents the post-treatment that implements
   a numerical probe applied on points (as opposed to nodes).
    
   constructor handle probe_on_points(hvno, icol, npnt, xy(1), xy(2), nomfic,
                                    mode, oprd, opwr)
      The constructor **probe_on_points** constructs an object, with the given
      arguments, and returns a handle on this object. 
      The constructor can accept: 
        * either a handle on a list of points coordinates,
        * or the enumeration of the coordinates
       
      as in: 
        * probe_on_points(hvno, icol, hlst, ....)
        * probe_on_points(hvno, icol, nptn, x1, y1, x2, y2, ....)
         handle   HOBJ        Return value: Handle on the post-treatment
         handle   hvno        Handle on the nodal values to probe
         integer  icol        Index of column of nodal values to probe
         integer  npnt        Handle on points list, or number of points to
                           probe
         double   xy(1)       x, y of points to probe, comma separated
         string   nomfic      Name of the post-treatment file
         string   mode        Opening mode ['w', 'a'] (default 'a')
         string   oprd        Reduction operation ['sum', 'sumabs', 'min',
                           'minabs', 'max', 'maxabs', 'mean'] (default 'noop')
         string   opwr        Writing operation (default 'nowrite')
    
   getter hvno
      Handle on the underlying nodal values
    
   method accumulate(hsim)
      The method **accumulate** add the present post-treatment values. Values
      are reduced according to the reduction operation, and written to file
      according to the writing operation. Simulation data must be loaded.
         handle   hsim        Handle on the simulation
    
   method finalize()
      The method **finalize** finalizes the post-treatment process. It shall
      be used to write to file the results when the option 'nowrite' is used.
    
   method xeq(hsim)
      The method **xeq** executes the post-treatment process. It is a
      combination of a call to **accumulate** followed by a call to
      **finalize**.
         handle   hsim        Handle on the simulation
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method add(hvno, icol, npnt, xy(1), xy(2))
      The method **add** add points to the list of points to probe.
         handle   hvno        Handle on the nodal values to probe
         integer  icol        Index of column of nodal values to probe
         integer  npnt        Number of points to probe
         double   xy(1)       x, y of points to probe, comma separated
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class.
    
