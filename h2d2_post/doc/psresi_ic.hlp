class post_residu
=================
 
   The class **post_residu** represents the post-treatment that computes the
   residuum.
    
   constructor handle post_residu(nomfic, mode, oprd, opwr)
      The constructor **post_residu** constructs an object, with the given
      arguments, and returns a handle on this object.
         handle   HOBJ        Return value: Handle on the post-treatment
         string   nomfic      Name of the post-treatment file
         string   mode        Opening mode ['w', 'a'] (default 'a')
         string   oprd        Reduction operation ['sum', 'sumabs', 'min',
                           'minabs', 'max', 'maxabs', 'mean'] (default 'noop')
         string   opwr        Writing operation ['write', 'nowrite'] (default
                           'nowrite')
    
   getter hvno
      Handle on the underlying nodal values
    
   method accumulate(hsim)
      The method **accumulate** add the present post-treatment values. Values
      are reduced according to the reduction operation, and written to file
      according to the writing operation. Simulation data must be loaded.
         handle   hsim        Handle on the simulation
    
   method finalize()
      The method **finalize** finalizes the post-treatment process. It shall
      be used to write to file the results when the option 'nowrite' is used.
    
   method xeq(hsim)
      The method **xeq** executes the post-treatment process. It is a
      combination of a call to **accumulate** followed by a call to
      **finalize**.
         handle   hsim        Handle on the simulation
    
   method del()
      The method **del** deletes the object. The handle shall not be used
      anymore to reference the object.
    
   method print()
      The method **print** prints information about the object.
    
   method help()
      The method **help** displays the help content for the class
    
