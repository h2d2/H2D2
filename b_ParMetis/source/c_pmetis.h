//************************************************************************
// --- Copyright (c) INRS 2003-2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire:
//
// Description:
//
// Notes:
//
//************************************************************************
#ifndef C_PMETIS_H_DEJA_INCLU
#define C_PMETIS_H_DEJA_INCLU

#ifndef MODULE_PARMETIS
#  define MODULE_PARMETIS 1
#endif

#include "cconfig.h"

#ifdef __cplusplus
extern "C"
{
#endif


#if   defined (F2C_CONF_NOM_FONCTION_MAJ )
#  define C_PMETIS_ERRMSG C_PMETIS_ERRMSG
#  define C_PMETIS_PART   C_PMETIS_PART
#  define C_PMETIS_RENUM  C_PMETIS_RENUM
#elif defined (F2C_CONF_NOM_FONCTION_MIN_)
#  define C_PMETIS_ERRMSG c_pmetis_errmsg_
#  define C_PMETIS_PART   c_pmetis_part_
#  define C_PMETIS_RENUM  c_pmetis_renum_
#elif defined (F2C_CONF_NOM_FONCTION_MIN__)
#  define C_PMETIS_ERRMSG c_pmetis_errmsg__
#  define C_PMETIS_PART   c_pmetis_part__
#  define C_PMETIS_RENUM  c_pmetis_renum__
#endif

#define F2C_CONF_DLL_IMPEXP F2C_CONF_DLL_DECLSPEC(MODULE_PARMETIS)

F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PMETIS_ERRMSG  (F2C_CONF_STRING F2C_CONF_SUP_INT);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PMETIS_PART    (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);
F2C_CONF_DLL_IMPEXP fint_t F2C_CONF_CNV_APPEL C_PMETIS_RENUM   (fint_t*, fint_t*, fint_t*, fint_t*, fint_t*, fint_t*);


#ifdef __cplusplus
}
#endif

#endif   // C_PMETIS_H_DEJA_INCLU
