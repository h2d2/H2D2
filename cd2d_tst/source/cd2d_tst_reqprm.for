C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C     Élément de convection-diffusion sans cinétiques à 5 ddls par noeuds
C     Élément réservé aux tests de contrôle de programmation
C
C************************************************************************

C************************************************************************
C Sommaire : CD2D$TST$REQPRM
C
C Description:
C     PARAMETRES DE L'ELEMENT
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$TST$REQPRM(TGELV,
     &                           TGELS,
     &                           NDIM,
     &                           NNELV,
     &                           NNELS,
     &                           NCELV,
     &                           NCELS,
     &                           NDJV,
     &                           NDJS,
     &                           NPRGL,
     &                           NPRNO,
     &                           NPRNOL,
     &                           NPREV,
     &                           NPREVL,
     &                           NPRES,
     &                           NDLN,
     &                           NDLEV,
     &                           NDLES,
     &                           NTINI,
     &                           NPOST,
     &                           ASURFACE,
     &                           ESTLIN)

      INTEGER TGELV
      INTEGER TGELS
      INTEGER NDIM
      INTEGER NNELV
      INTEGER NNELS
      INTEGER NCELV
      INTEGER NCELS
      INTEGER NDJV
      INTEGER NDJS
      INTEGER NPRGL
      INTEGER NPRNO
      INTEGER NPRNOL
      INTEGER NPREV
      INTEGER NPREVL
      INTEGER NPRES
      INTEGER NDLN
      INTEGER NDLEV
      INTEGER NDLES
      INTEGER NTINI
      INTEGER NPOST
      LOGICAL ASURFACE
      LOGICAL ESTLIN
C-----------------------------------------------------------------------

C------ INITIALISATION DES PARAMETRES DE L'ÉLÉMENT PARENT VIRTUEL
      CALL CD2D$BSE$REQPRM(TGELV,
     &                     TGELS,
     &                     NDIM,
     &                     NNELV,
     &                     NNELS,
     &                     NCELV,
     &                     NCELS,
     &                     NDJV,
     &                     NDJS,
     &                     NPRGL,
     &                     NPRNO,
     &                     NPRNOL,
     &                     NPREV,
     &                     NPREVL,
     &                     NPRES,
     &                     NDLN,
     &                     NDLEV,
     &                     NDLES,
     &                     NTINI,
     &                     NPOST,
     &                     ASURFACE,
     &                     ESTLIN)

C------ INITIALISATION DES PARAMETRES DE L'HERITIER
      NDLN  = 5                                ! NB DE DDL PAR NOEUD
      NDLEV = NDLN * NNELV                     ! NB DE DDL PAR ELEMENT DE VOLUME
      NDLES = NDLN * NNELS                     ! NB DE DDL PAR ELEMENT DE SURFACE
      NPRNO = NPRNO + NDLN                     ! NB DE PROPRIÉTÉS NODALES
      NTINI = NDLN                             ! NB DE TERMES D'INITIALISATION
      NPOST = NDLN                             ! NB DE VARIABLES NODALES POST-TRAITÉES

      RETURN
      END

