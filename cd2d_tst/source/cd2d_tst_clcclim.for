C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description:
C     ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                EULERIENNE 2-D DE BASE SANS CINÉTIQUES.
C                FORMULATION NON-CONSERVATIVE POUR (C).
C     ÉLÉMENT  : T3 - LINÉAIRE
C
C Notes:
C     Élément de convection-diffusion sans cinétiques à 5 ddls par noeuds
C     Élément réservé aux tests de contrôle de programmation
C
C************************************************************************

C************************************************************************
C Sommaire : CD2D$TST$CLCCLIM
C
C Description:
C     Calcul des conditions limites DANS LE VECTEUR
C     DES D.D.L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      SUBROUTINE CD2D$TST$CLCCLIM(KNGV,KNGS,VDJV,VDJS,
     &                            VPRGL,VPRNO,VPREV,VPRES,
     &                            KDIMP,VDIMP,KEIMP,VDLG)

      INTEGER KNGV (*)
      INTEGER KNGS (*)
      REAL*8  VDJV (*)
      REAL*8  VDJS (*)
      REAL*8  VPRGL(*)
      REAL*8  VPRNO(*)
      REAL*8  VPREV(*)
      REAL*8  VPRES(*)
      INTEGER KDIMP(*)
      REAL*8  VDIMP(*)
      INTEGER KEIMP(*)
      REAL*8  VDLG (*)
C-----------------------------------------------------------------------

      CALL CD2D$BSE$CLCCLIM(KNGV,KNGS,VDJV,VDJS,
     &                      VPRGL,VPRNO,VPREV,VPRES,
     &                      KDIMP,VDIMP,KEIMP,VDLG)

      RETURN
      END

