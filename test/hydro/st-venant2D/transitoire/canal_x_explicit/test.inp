#-----------------------------------
#  Test du terme transitoire dqx/dt
#     u = 1.0
#     v = 0
#     h = 2.5
#  Les termes de gravité, convection, vent, Manning et de viscosité sont activés
#  Temps de 5 secondes et on a une erreur assez élevée. Méthode rapidement instable.
#-----------------------------------

n_fic  = 'test'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_vno  = vnod(n_fic+'.pnt')      # terrain
h_prn  = prno(h_vno)             # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0   # prel()            # regroupement des champs en prel

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(1.0,          #  1 gravite
              0.0,          #  2 latitude
              1.0,          #  3 fonction du vent
              1.0e-00,      #  4 laminar viscosity
              0.0,          #  5 mixing length coef
              0.0,          #  6 Smagorinsky coef
              1.0e-99,      #  7 viscosity limiter: inf
              1.0e+99,      #  8 viscosity limiter: sup
              1.0e-03,      #  9 H treshold
              9.9e-04,      # 10 dry/wet: min depth
              0.1,          # 11 dry/wet: manning
              9.9e+001,     # 12 dry/wet: |u| max
              1.0,          # 13 dry/wet: porosity
              0.0e-000,     # 14 dry/wet: damping
              0.0,          # 15 dry/wet: coef. convection
              0.0,          # 16 dry/wet: coef. gravity
              0.0e-00,      # 17 dry/wet: nu diffusion   !!1/Hmin!!
              0.0e-00,      # 18 dry/wet: nu darcy
              1.0e+99,      # 19 peclet
              0.0e-000,     # 20 damping coef.
              0.0e-00,      # 21 free surface smoothing (Darcy)
              0.0e-00,      # 22 free surface smoothing (Lapidus)
              1.0,          # 23 coef. convection
              1.0,          # 24 coef. gravity
              1.0,          # 25 coef. manning
              1.0,          # 26 coef. wind
              0.0,          # 27 coef. boundary integral
              1.0e-18,      # 28 coef. penality
              1.0e-007,     # 29 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
              1.0e-007,     # 30 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
              1.0,          # 31 coef. Kt relaxation
              1,            # 32 flag prel perturbation (0 = false, !=0 = true)
              1)            # 33 flag prno perturbation (0 = false, !=0 = true)

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_slr = solr(vnod(n_fic+'.slr'))
h_slc = 0

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_sol  = sv2d_y4(h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_resmat  = ldu_memory()
h_btrk = 0
h_delt = 1 #seconde
h_alfa = 0 # 0=Explicite 0.5=Crank-Nicholson 1=Implicite
h_lmtr = glob_limiter(h_sol, 10.0, 10.0, 10.0)
h_cria = cria_l2_allrel(h_sol, 0.0, 1.0e-15)
h_cric = 0
h_newton = newton(h_cria, h_cric, h_lmtr, h_resmat, 50, 0)
h_euler  = euler (h_newton, h_delt, h_alfa)

#-----------------------------------
#  Résolution
#-----------------------------------
h_post  = 0
h_trig  = 0
h_tini  = 0.0
n_tstep = 5
h_sol.init_f(0.0,n_fic+'.ini')
h_euler.solve(h_sol,h_post,h_trig, h_tini,h_delt, n_tstep)
h_sol.save(n_fic+'.fin', 'w')

stop()


