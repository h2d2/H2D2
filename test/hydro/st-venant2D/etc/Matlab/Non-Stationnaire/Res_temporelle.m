clear
syms x; syms t;
H=1;
nu = 1*10^(-6);
rho = 1000;

d2qdx2 = -exp(t)-0*x-1.225;          %On pose l'accélération de qx
dqdx   = int(d2qdx2,x);              
q      = int(dqdx,x)+1
dq2dx  = diff(q^2,x);
dqdt   = diff(q,t);

f = dqdt +1/H*dq2dx -2*nu/rho*d2qdx2;  %Conservation des moments
rangetx = [0 30 0 10];
g = inline(f,'t','x');
%implot2(g,rangetx);

subplot(2,1,1)
qx = subs(q,{x,t},{sym('x'),0});        %distribution de qx le long de
                                        %la limite y=0 au temps 0.
ezplot(qx,[0 1])

subplot(2,1,2)
q0t = subs(q,{x,t},{0.5,'t'});          %qx en fonction du temps à x=0.5
ezplot(q0t,[0 1])
