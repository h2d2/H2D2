clc
clear
syms x y SOLR1 SOLR2 SOLR3
u     = exp(x);     dudx = diff(u,x);  dudy = diff(u,y); 
v     = exp(y);     dvdy = diff(v,y);  dvdx = diff(v,x);
H     = exp(-x-y);  dHdx = diff(H,x);  dHdy = diff(H,y);
K     = 1.0;        lm   = K*H;        g = 1.0;
nuT   = (lm^2)*(2*(dudx)^2+2*(dvdy)^2+(dudy+dvdx)^2)^(1/2)
tauxx = nuT*2*H*dudx;
tauxy = nuT*H*(dudy+dvdx);
tauyy = nuT*2*H*dvdy;

E1 = SOLR2-g*H*dHdx+diff(tauxx,x)+diff(tauxy,y);
E2 = SOLR3-g*H*dHdy+diff(tauxy,x)+diff(tauyy,y);

SOLR2 = solve(E1,SOLR2)
SOLR3 = solve(E2,SOLR3)
SOLR1 = diff(u*H,x)+diff(v*H,y)
%% Test ey_0_f(x)
clc
clear
syms x y SOLRqx SOLRqy SOLRh
%Paramètres du test
u = exp(y);             v  = 0;           H = 2-0.00075*x;
K = 1.0;                g  = 1.0;

%Calculs des dérivées et autres termes
lm     = K*H;
dudx   = diff(u,x);    dudy = diff(u,y); 
dvdy   = diff(v,y);    dvdx = diff(v,x);
dHdx   = diff(H,x);    dHdy = diff(H,y);
nuT    = (lm^2)*(2*(dudx)^2+2*(dvdy)^2+(dudy+dvdx)^2)^(1/2)           

tauxx = nuT*2*H*dudx;
tauxy = nuT*H*(dudy+dvdx);
tauyy = nuT*2*H*dvdy;

E1 = SOLRqx-g*H*dHdx+diff(tauxx,x)+diff(tauxy,y);
E2 = SOLRqy-g*H*dHdy+diff(tauxy,x)+diff(tauyy,y);

SOLRqx = solve(E1,SOLRqx)
SOLRqy = solve(E2,SOLRqy)
SOLRh  = diff(u*H,x)+diff(v*H,y)
%% Test u(y)_0_1_LM
%Ce test donne une sollicitation de -1.0 en x et c'est tout.
clc
clear
syms x y SOLRqx SOLRqy SOLRh
%Paramètres du test
u = 2/3*y^(3/2);        v  = 0;           H = 1.0;
K = 1.0;                g  = 1.0;

%Calculs des dérivées et autres termes
lm     = K*H;
dudx   = diff(u,x);    dudy = diff(u,y); 
dvdy   = diff(v,y);    dvdx = diff(v,x);
dHdx   = diff(H,x);    dHdy = diff(H,y);
nuT    = (lm^2)*(2*(dudx)^2+2*(dvdy)^2+(dudy+dvdx)^2)^(1/2)           

tauxx = nuT*2*H*dudx;
tauxy = nuT*H*(dudy+dvdx);
tauyy = nuT*2*H*dvdy;

E1 = SOLRqx-g*H*dHdx+diff(tauxx,x)+diff(tauxy,y);
E2 = SOLRqy-g*H*dHdy+diff(tauxy,x)+diff(tauyy,y);

SOLRqx = solve(E1,SOLRqx)
SOLRqy = solve(E2,SOLRqy)
SOLRh  = diff(u*H,x)+diff(v*H,y)
































