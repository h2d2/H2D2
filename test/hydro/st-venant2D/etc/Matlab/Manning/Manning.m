clear
syms x; syms n;
u     = 1;
dhdx  = -0.00075;
h     = 2.5+dhdx*x;
zf    = 1+dhdx*x;
H     = h-zf;
g     = 9.806;

qx = u*H;
q  = double([qx 0]);
a  = -g*H*dhdx-g*n^2*norm(q)*qx/(H^(7/3));

n  = solve(a,n);
format long e
n  = vpa(n)
n  = double(n(2))
%%
clc
clear
syms x y SOLR1 SOLR2
u = y; v = x; dhdx = -0.00075; dhdy = 0.0;
zf = 1+dhdx*x;
h = 3+dhdx*x;
H = h-zf;
g = 1.0; n = 3/100;

e1 = -g*H*dhdx + SOLR1 -n^2*g*sqrt((u*H)^2+(v*H)^2)*u*H/H^(7/3);
e2 = -g*H*dhdy + SOLR2 -n^2*g*sqrt((u*H)^2+(v*H)^2)*v*H/H^(7/3);

SOLR1 = Solve(e1, SOLR1)
SOLR2 = Solve(e2, SOLR2)
%% Conditions limites 123-124 sur le Manning
clc 
clear
%Propriétés du test
H1 = 1.5; H3 = 1.5;   H5 = 1.5;          QTOT = -1.5;
n1 = 1E-16;       n3 = 0.005;      n5   = 0.01;


QS = 0;
Q1 = H1^(5/3)/n1;
Q3 = H3^(5/3)/n3;
Q5 = H3^(5/3)/n5;
C1  = -1/3*QTOT/(QS+Q1+Q3);
C2  = -1/3*QTOT/(QS+Q3+Q5);
S1 = C1*(Q1+Q1+Q3)
S31 = C1*(Q3+Q3+Q1)
S32 = C2*(Q3+Q3+Q5)
S5 = C2*(Q5+Q5+Q3)

