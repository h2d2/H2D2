clear
clc

syms h x y du2Hdx duvHdy duvHdx dv2dy 
syms dhdx dhdy v ddx ddy

g = 9.806; rho = 1000; nu = 1; 
w = .7292*10^(-4); phi = pi/2; 

%Propriétés du test
u = y; v = 0; zf = 0;
tauwx = 0; tauwy = 0; n = 0;

%Termes à désactiver
CNV = 0;            % Convection
G   = 0;            % Gravité
M   = 0;            % Manning
C   = 1;            % Coriolis
W   = 0;            % Vent

H     = h - zf;
convx = CNV*(du2Hdx + duvHdy);
convy = CNV*(duvHdx + dv2dy);
gx    = G*(g*H*dhdx);
gy    = G*(g*H*dhdy);
manx  = M*(n^2*(u*H)^2/H^(7/3));
many  = M*(n^2*(v*H)^2/H^(7/3));
viscx = 1/rho*H*nu*ddx*(2*diff(u,x)) + 1/rho*nu*H*ddy*(diff(v,x)+diff(u,y));
viscy = 1/rho*H*nu*ddx(diff(v,x)+diff(u,y)) + 1/rho*nu*H*ddy*(2*diff(v,y));
wx    = tauwx
wy    = tauwy
corix = C*(2*w*sin(phi)*v*H);
coriy = C*(2*w*sin(phi)*u*H);


eqnx = -convx - gx - manx + viscx + corix
eqny = -convy - gy - many + viscy + coriy
