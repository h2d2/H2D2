C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, KDIMP)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*256 NOMCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I3,', NDLN, '(1X,1PE25.17E3),A)'

      NCND = 0
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) NCND = NCND + 1
      ENDDO
      WRITE(10, '(I9,1X,1PE25.17E3)') NCND, 0.0


100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)

               WRITE(NOMCL,FMT) '$',IC/1000,(VDLG(ID,IN),ID=1,NDLN),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               WRITE(10,'(A,I3,6(1X,1PE25.17E3))')
     &                                 NOMCL(1:LENSTR(NOMCL)),
     &                                 MOD(IC,1000),
     &                                 (VDLG(ID,IN),ID=1,NDLN)
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END
