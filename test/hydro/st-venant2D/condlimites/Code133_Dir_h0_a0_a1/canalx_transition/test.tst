0.0               XORI
0.0               YORI
1.0               DIMX
1.0               DIMY
6                 NNEL
'>'              ITYPEL
'z0=0.0, dzdx=0.0, dzdy=0.0, n=0.0, ng=0.0, eg=0.0'      MNT
'canalx'    	   ECOUL
'101_0_133_0'		   CL

Faire un canal de 4 limites et 3 parties: |  1  | 2 |  3  |
où h1=1.5, pas de points dans la partie 2, h3=1.0
Mettre la limite #3 code 133 avec la limite #2 (et faire diminuer la hauteur h=1.5*2-2 = 1.0 dans partie 3)
À faire lorsque le code 143 va être fait car la même technique est appliquée dans ce code et prime sur celle-ci.

