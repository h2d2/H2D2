#
#-----------------------------------------------------------------
#        EQUATION DE TRANSPORT-DIFFUSION NON-CONSERVATIVE
#
#                 ELEMENT T3 A 2 DIMENSIONS
#
#        CONTROLE DE PROGRAMMATION
#        TEST DE BASE
#        TEST DE CONFORMITE DE L'ELEMENT
#        CONCENTRATION C= X + CONSTANTE
#        CHAMP DE VITESSE NUL
#        PROFONDEUR CONSTANTE
#        SANS SOURCES ET PUITS
#        C.L. DE DIRICHLET SUR TOUS LES NOEUDS DU CONTOUR
#        SOLLICITATIONS REPARTIES NULLES
#        SOLLICITATIONS CONCENTREES NULLES
#        MAILLAGE A 8 ELEMENTS ET 8 NOEUDS DONT 2 INTERNES
#
#-----------------------------------------------------------------
#
h_frm = form('cd2d_coliformesfecaux')

n_fic  = 'test'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_vno  = vnod(n_fic+'.pnh')
h_prn  = prno(h_vno)           # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0   # prel()            # regroupement des champs en prel

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(  0,                    # 1 Diffusivité moléculaire
                1e-50,                # 2 Coefficient de diffusion verticale
                0.0,                  # 3 Coefficient de diffusion horizontale
                1e+46,                # 4 Coefficient de diffusion longitudinal
                1.0e+99,              # 5 Stabilisation: Nombre de Peclet
                0.0,                  # 6 Stabilisation: Coefficient de Lapidus
                0.02,                 # 7 Couvrant-découvrant: H tresh
                0.0,                  # 8 Couvrant-découvrant: Hmin
                0,                    # 9 Couvrant-découvrant: Manning
                0,                    #10 Couvrant-découvrant: Conductivité
                0,                    #11 Couvrant-découvrant: Porosité
                0,                    #12 Couvrant-découvrant: Diff. Horizontale
                0,                    #13 Couvrant-découvrant: Diff. Longitudinale
                1.0e-4,               #14 Coefficient de dégradation des coliformes fécaux
                1e-99,                #15 Couvrant-découvrant: D10
                0)                    #16 Couvrant-découvrant: VSed

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_sl0  = vnod(n_fic+'.slr')
h_slr  = solr(h_sl0)
h_slc  = 0

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#------------------------------------
#  Critère d'arrêt, limiteur
#------------------------------------
h_limiter = glob_limiter(h_sol, 10.0)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0e-15)
h_criconv = 0

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_resmat = ldu_memory()
h_ressys = newton(h_criarret, h_criconv, h_limiter, h_resmat, 25, 0)
t_del  = 10
h_algo = euler(h_ressys, t_del, 0.5, 0)             #Méthode de Crank-Nicolson-Galerkin

#_IMPR  =3
#_DPAS  =10.0     OK
#_NPAS  =10       OK
#_ALFA  =0.5      OK
#_EPSDL =1.0E-15  OK
#_NPREC =1
#_NITER =25       OK
#_NRDEM =3
#_OMEGA =1.D0

#-----------------------------------
#  Résolution
#-----------------------------------
h_post = 0
t_npas = 10
h_trg = 0
t_ini = 0.0
h_sol.init_f(t_ini, n_fic+'.ini')
h_algo.solve(h_sol, h_post, h_trg, t_ini, t_del, t_npas)
h_sol.save(n_fic+'.fin')

stop()

