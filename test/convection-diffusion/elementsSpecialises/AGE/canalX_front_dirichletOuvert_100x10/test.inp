#
#-----------------------------------------------------------------
#        EQUATION DE TRANSPORT-DIFFUSION NON-CONSERVATIVE
#
#                 ELEMENT T3 A 2 DIMENSIONS
#
#        CONTROLE DE PROGRAMMATION
#        TEST DE BASE
#        TEST DE CONFORMITE DE L'ELEMENT
#        CONCENTRATION C= X + CONSTANTE
#        CHAMP DE VITESSE NUL (Non, on a choisi CanalX)
#        PROFONDEUR CONSTANTE
#        SANS SOURCES ET PUITS
#        C.L. DE DIRICHLET SUR TOUS LES NOEUDS DU CONTOUR (Non, on a choisi D_O_O_O)
#        SOLLICITATIONS REPARTIES NULLES
#        SOLLICITATIONS CONCENTREES NULLES
#        MAILLAGE A 8 ELEMENTS ET 8 NOEUDS DONT 2 INTERNES
#        Nombre de propriétés globales est de 7 au lieu de 5 ou 6
#
#       u = 1
#       v = 0
#       H = 1.5
#       C = x + 1
#       C.L. Dirichlet à gauche et Ouvert partout ailleurs
#
#       Ce test évalue l'âge des particules du système (à gauche, l'âge est de 1, à droite, l'âge est de 6.
#       DeltaS=KH=1.5 m/s^-2
#
#-----------------------------------------------------------------
#

h_num = 0
h_frm = form('cd2d_age')

n_fic  = 'test'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_vno  = vnod(n_fic+'.pnh')
h_prn  = prno(h_vno)             # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0   # prel()            # regroupement des champs en prel

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(  1.0e-16,              # 1 Diffusivité moléculaire
                0.0,                  # 2 Coefficient de diffusion verticale
                0.0,                  # 3 Coefficient de diffusion horizontale
                0.0,                  # 4 Coefficient de diffusion longitudinal
                1.0e+99,              # 5 Stabilisation: Nombre de Peclet
                0.0,                  # 6 Stabilisation: Coefficient de Lapidus
                0.01,                 # 7 Couvrant-découvrant: H tresh
                0.0,                  # 8 Couvrant-découvrant: Hmin
                0,                    # 9 Couvrant-découvrant: Manning
                0,                    #10 Couvrant-découvrant: Conductivité
                0,                    #11 Couvrant-découvrant: Porosité
                0,                    #12 Couvrant-découvrant: Diff. Horizontale
                0,                    #13 Couvrant-découvrant: Diff. Longitudinale
                1)                    #14 Facteur de vieillissement

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_sl0  = vnod(n_fic+'.slr')
h_slr  = solr(h_sl0)
h_slc  = 0

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#------------------------------------
#  Critère d'arrêt, limiteur
#------------------------------------
h_limiter = glob_limiter(h_sol, 10.0)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0e-15)
h_criconv = 0

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_glob = 0
h_resmat = ldu_memory()
h_newton = newton (h_criarret, h_criconv, h_limiter, h_resmat, 25, 0)

#-----------------------------------
#  Résolution
#-----------------------------------
h_post = 0
h_sol.init_f(0.0, n_fic+'.ini')
h_newton.solve(h_sol, h_post, 0, 0.0)
h_sol.save(n_fic+'.fin')

stop()
