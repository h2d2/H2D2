# -*- coding: UTF-8 -*-

class section:
    def __init__(self):
        self.nom = ''
        self.valeurs = []

    def lis(self, nom, f):
        self.nom = nom

        line = f.readline()[:-1]
        while (line != '' and line.strip() != ''):
            line = line.strip()
            while line != line.replace('  ', ' '):
                line = line.replace('  ', ' ')
            toks = line.split(' ')
            self.valeurs.append(toks)
            line = f.readline()[:-1]

    def __eq__(self, other):
        if (self.nom != other.nom): return False
        if (len(self.valeurs) != len(other.valeurs)): return False
        for svs, ovs in zip(self.valeurs, other.valeurs):
            if (len(svs) != len(ovs)): return False
            for sv, ov in zip(svs, ovs):
                if (sv == 'NaN'): return False
                if (ov == 'NaN'): return False
                sf = float(sv)
                of = float(ov)
                d = abs(sf-of)
                tol = 1.0e-17
                if (d > tol): return False
        return True

def lis_sections(nomf):
    sections = []
    f = open(nomf, 'r')

    line = ' '
    while (line != ''):
        while (line != '' and line.strip() == ''):
            line = f.readline()
        if (line != ''):
            line = line[:-1]
            s = section()
            s.lis(line.strip(), f)
            sections.append(s)
            line = ' '

    return sections

def main():
    fin = lis_sections('test.fin')
    tgt = lis_sections('test.tgt')

    # ---  Compare les sections de même nom
    estOK = True
    for stgt in tgt:
        for sfin in fin:
            if (stgt.nom == sfin.nom):
                estOK = estOK and (stgt == sfin)

    # ---  Imprime le résultat
    if (estOK):
        print 'test=ok'
    else:
        print 'test=erreur'

if __name__ == '__main__':
    main()
