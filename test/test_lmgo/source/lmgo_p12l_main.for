C************************************************************************
C Fichier: $Id: lmgo_p12l_main.for,v 1.2 2006/05/28
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: PROGRAM LMGO_P12L_MAIN
C
C Description: Le PROGRAM LMGO_P12L_MAIN est un test de la fonction
C              CLCJELV du module LMGO_P12L.
C
C Entrée:
C
C
C Sortie:
C
C Notes:
C     Appels à répétitions de la fonction TEST_LMGO en lui passant
C     les bons répertoires où insérer ses fichiers.
C     Serait à transformer pour correspondre à la structure de test
C     et éliminer ce main d'appels consécutifs.
C
C************************************************************************

      PROGRAM LMGO_P12L_MAIN

      INCLUDE 'lmgo_p12l_main.fc'

      CHARACTER*(64) TYPEELEMENT
      CHARACTER*(64) INDICEPT

C--   Élément réel = élément de référence
      TYPEELEMENT = 'ref\'
      INDICEPT = '1\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '2\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '3\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '4\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '5\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)

C--   Le fond et la surface ne sont ni parrallèle entre eux, ni
C      perpendiculaires aux côtés

      TYPEELEMENT = 'quelconque\'
      INDICEPT = '1\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '2\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '3\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '4\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '5\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)

C--   Le fond est perpendiculaire aux côtés. La surface n'est pas
C     perpendiculaire aux côtés
      TYPEELEMENT = 'trapezedroit\'
      INDICEPT = '1\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '2\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '3\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '4\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '5\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)

C--   Le fond et la surface ne sont pas perpendiculaires aux côtés
C     mais sont parrallèles entre eux.
      TYPEELEMENT = 'parrallelogramme\'
      INDICEPT = '1\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '2\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '3\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '4\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)
      INDICEPT = '5\'
      CALL TEST_LMGO(TYPEELEMENT, INDICEPT)

      END


C************************************************************************
C Sommaire: SUBROUTINE TEST_LMGO(TYPEELEMENT, INDICEPT)
C
C Description: La routine TEST_LMGO reçoit un type d'élément à tester
C              ainsi que le numéro d'un point de test. Elle appelle
C              la fonction lmgo_p12l et imprime les réponses
C
C Entrée:      TYPEELEMENT  Caractère correspondant au type d'élément
C                           à tester (et donc à son nom dans la structure
C                           de répertoires)
C
C              INDICEPT     Caractère correspondant au point à tester
C                           et donc au nom du point dans la structure de
C                           répertoires
C
C
C Sortie: Impression des résultats dans les fichiers
C         VJACOP12L.txt, VJACOP6.txt, DETJP6.txt, DETJP12L.txt, IP6.txt
C         se trouvant dans le répertoire resultat associé au test
C
C
C Notes:
C     Une application externe pourrait appeler à plusieurs reprise
C     cette fonction si elle était un exécutable et ainsi éliminer le
C     main. La structure de répertoire hardcodée pourrait être
C     remplacée vers une réception des fichiers d'entrées
C     et de sortie.
C
C************************************************************************

      SUBROUTINE TEST_LMGO(TYPEELEMENT, INDICEPT)

      CHARACTER*(64) TYPEELEMENT
      CHARACTER*(64) INDICEPT

      INCLUDE 'spstrn.fi'  !SP_STRN_LEN
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      include 'eacnst.fi'

      INTEGER NDJV
      INTEGER NDIM
      INTEGER NNELV
      INTEGER NCELV
      INTEGER NELV
      INTEGER NDIMELV
      INTEGER NNT

      PARAMETER (NDJV=5)
      PARAMETER (NDIM=2)
      PARAMETER (NNELV=6)
      PARAMETER (NCELV=6)
      PARAMETER (NELV=1)
      PARAMETER (NDIMELV=3)
      PARAMETER (NNT = 6)

      REAL*8 VJP12L(NDIMELV, NDIMELV)
      REAL*8 VJP6(NDIMELV, NDIMELV)
      REAL*8 DJP12L
      REAL*8 DJP6

      REAL*8   VCOPT(NDIMELV)
      REAL*8   VCORE(NDIM, NNELV)
      REAL*8   VZCORE(2, NNELV)
      REAL*8   VDJE(NDJV)
      INTEGER  IP6

      INTEGER KNGV(NCELV, NELV)

      INTEGER IERR

C--   VARIABLES POUR LA STRUCTURE DE RÉPERTOIRE
      CHARACTER*(64) REP
      CHARACTER*(64) P12L
      CHARACTER*(64) PTEVALUATION
      CHARACTER*(128) FICHIERP12L
      CHARACTER*(128) FICHIERPTEVALUATION
      CHARACTER*(128) REPERTOIRESORTIE
      CHARACTER*(128) FICHIERVJP12L
      CHARACTER*(128) FICHIERVJP6
      CHARACTER*(128) FICHIERDJP12L
      CHARACTER*(128) FICHIERDJP6
      CHARACTER*(128) FICHIERIP6


C ---------------------------------------------------------------------
C ---------------------------------------------------------------------
C---  RÉPERTOIRE DE LECTUERE ET D'ÉCRITURE
      REP = 'E:\dev\H2D2\test\test_lmgo\fichiersinput\'
      P12L ='p12l.cor'
      PTEVALUATION = 'ptEvaluation.txt'

      FICHIERP12L = REP(1:SP_STRN_LEN(REP)) //
     &                  TYPEELEMENT(1:SP_STRN_LEN(TYPEELEMENT)) //
     &                  P12L(1:SP_STRN_LEN(P12L))

      FICHIERPTEVALUATION = REP(1:SP_STRN_LEN(REP))//
     &                      TYPEELEMENT(1:SP_STRN_LEN(TYPEELEMENT)) //
     &                      INDICEPT(1:SP_STRN_LEN(INDICEPT)) //
     &                      PTEVALUATION(1:SP_STRN_LEN(PTEVALUATION))

      REPERTOIRESORTIE = REP(1:SP_STRN_LEN(REP))//
     &                   TYPEELEMENT(1:SP_STRN_LEN(TYPEELEMENT)) //
     &                   INDICEPT(1:SP_STRN_LEN(INDICEPT)) //
     &                   'reponse\'
      FICHIERVJP12L = REPERTOIRESORTIE(1:SP_STRN_LEN(REPERTOIRESORTIE))
     &                   // 'VJACOP12L.txt'
      FICHIERVJP6 = REPERTOIRESORTIE(1:SP_STRN_LEN(REPERTOIRESORTIE))
     &                   // 'VJACOP6.txt'
      FICHIERDJP12L =REPERTOIRESORTIE(1:SP_STRN_LEN(REPERTOIRESORTIE))
     &                   // 'DETJP12L.txt'
      FICHIERDJP6 =REPERTOIRESORTIE(1:SP_STRN_LEN(REPERTOIRESORTIE))
     &                   // 'DETJP6.txt'

      FICHIERIP6 =REPERTOIRESORTIE(1:SP_STRN_LEN(REPERTOIRESORTIE))
     &                   // 'IP6.txt'

C--   CONNECTIVITÉS
      KNGV(1,1) = 1
      KNGV(2,1) = 2
      KNGV(3,1) = 3
      KNGV(4,1) = 4
      KNGV(5,1) = 5
      KNGV(6,1) = 6

C---  Lecture
      OPEN(7,file = FICHIERPTEVALUATION)
      DO I=1, NDIMELV
            READ(7,*) VCOPT(I)
      ENDDO

      OPEN(2, file=FICHIERP12L)

      DO I=1,2
            READ(2,*)   VCORE(I,1), VCORE(I,2), VCORE(I,3),
     &                  VCORE(I,4), VCORE(I,5), VCORE(I,6)
      ENDDO

      DO I=1,2
         READ(2,*)   VZCORE(I,1), VZCORE(I,2), VZCORE(I,3),
     &               VZCORE(I,4), VZCORE(I,5), VZCORE(I,6)

C---     S'assure de la linéarité (test si le test est valide !)
D        CALL ERR_ASR(  VZCORE(I,2) .EQ.
D    &                ( VZCORE(I,1) / DEUX + VZCORE(I,3) / DEUX ) )
D        CALL ERR_ASR(  VZCORE(I,4) .EQ.
D    &                ( VZCORE(I,3) / DEUX + VZCORE(I,5) / DEUX ) )
D        CALL ERR_ASR(  VZCORE(I,6) .EQ.
D    &                ( VZCORE(I,5) / DEUX + VZCORE(I,1) / DEUX ) )
      ENDDO

C---  S'assure que profondeur positive
D     CALL ERR_ASR(VZCORE(2,1) .GT. VZCORE(1,1))
D     CALL ERR_ASR(VZCORE(2,3) .GT. VZCORE(1,3))
D     CALL ERR_ASR(VZCORE(2,5) .GT. VZCORE(1,5))


C---  APPELS
      IERR = LMGO_P12L_CLCJELV(  NDIM,
     &                           NNT,
     &                           NCELV,
     &                           NELV,
     &                           NDJV,
     &                           VCORE,
     &                           KNGV,
     &                           VDJE)


      IERR = LMGO_P12L_EVAJELV   (  NDIM,
     &                              NDIMELV,
     &                              NNELV,
     &                              NDJV,
     &                              VCORE,
     &                              VZCORE,
     &                              VDJE,
     &                              VCOPT,
     &                              VJP12L,
     &                              DJP12L,
     &                              VJP6,
     &                              DJP6,
     &                              IP6 )

C--   ÉCRITURE DES RÉSULTATS
      OPEN(9, file=FICHIERVJP12L)
      WRITE(9,*) VJP12L

      OPEN(10, file=FICHIERVJP6)
      WRITE(10,*) VJP6

      OPEN(11, file=FICHIERDJP12L)
      WRITE(11,*) DJP12L

      OPEN(12, file=FICHIERDJP6)
      WRITE(12,*) DJP6

      OPEN(13, file=FICHIERIP6)
      WRITE(13,*) IP6

C---  FERMETURE DES FICHIERS
      CLOSE(7)
      CLOSE(2)
      CLOSE(9)
      CLOSE(10)
      CLOSE(11)
      CLOSE(12)
      CLOSE(13)




      RETURN
      END