h_frm = form('SV2D_Conservatif_CDYS_NN')

n_fic  = 'test'

#-----------------------------------
#  Maillage
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')

h_renum = bgl()
h_renum.renum(h_ele)
h_num = h_renum.gen_num()
h_nume = 0
h_grid = grid(h_cor, h_ele, h_num, h_nume)

#-----------------------------------
#  Propriétés nodales
#-----------------------------------
h_vno  = vnod(n_fic+'.pnt')      # terrain
h_prn  = prno(h_vno)             # regroupement des champs en prno

#-----------------------------------
#  Propriétés élémentaires
#-----------------------------------
h_pre  = 0   # prel()            # regroupement des champs en prel

#-----------------------------------
#  Propriétés globales
#-----------------------------------
h_pgl  = prgl(1.0,          #  1 gravite
              0.0,          #  2 latitude
              1.0e-00,      #  3 laminar viscosity
              0.0,          #  4 mixing length coef
              0.0,          #  5 mixing length coef related to grid size
              1.0e-99,      #  6 viscosity limiter: inf
              1.0e+99,      #  7 viscosity limiter: sup
              1.0e-03,      #  8 dry/wet: min depth
              1.0e-03,      #  9
              0.1,          #  10 dry/wet: manning
              1.0e+003,     # 11 dry/wet: |u| max
              1.0,          # 12 dry/wet: porosity
              0.0e-000,     # 13 dry/wet: damping
              0.0,          # 14 dry/wet: coef. convection
              0.0,          # 15 dry/wet: coef. gravity
              0.0e-00,      # 16 dry/wet: nu diffusion   !!1/Hmin!!
              0.0e-00,      # 17 dry/wet: nu darcy
              1.0e+99,      # 18 peclet
              0.0e-000,     # 19 damping coef.
              0.0e-00,      # 20 free surface smoothing (Darcy)
              0.0e-00,      # 21 free surface smoothing (Lapidus)
              0.0,          # 22 coef. convection
              1.0,          # 23 coef. gravity
              0.0,          # 24 coef. manning
              0.0,          # 25 coef. wind
              0.0,          # 26 coef. boundary integral
              1.0e-18,      # 27 coef. penality
              1.0e-007,     # 28 coef. Kt perturbation (~ 0.1*sqrt(eps_mach); eps_mach ~ 2.2e-16)
              1.0,          # 29 coef. Kt relaxation
              1,            # 30 flag prel perturbation (0 = false, !=0 = true)
              1)            # 31 flag prno perturbation (0 = false, !=0 = true)

#-----------------------------------
#  Sollicitations
#-----------------------------------
h_slr  = 0
h_slc  = 0

#-----------------------------------
#  Conditions limites
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Degrés de liberté
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Données globales de simulation
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#-----------------------------------
#  Définition des algo de résolution
#-----------------------------------
h_resmat  = ldu_memory()
h_btrk = 0
h_newton  = newton (h_btrk,h_resmat, 2)

#------------------------------------
#  Critère d'arrêt, limiteur
#------------------------------------
h_limiter  = limiter(h_sol, 1.0, 1.0, 0.2)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0e-15)

#-----------------------------------
#  Résolution
#-----------------------------------
h_post = 0
h_sol.init_f(n_fic+'.ini', 0)
h_newton.solve(h_sol, h_criarret, h_limiter, h_post, 1)
h_sol.save(n_fic+'.fin')

stop()


