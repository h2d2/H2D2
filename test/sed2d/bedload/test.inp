#-----------------------------------
#  Formulation
#-----------------------------------
h_frm = form('sed2d_gs1')

#-----------------------------------
#  File name
#-----------------------------------
n_fic  = 'test'

#-----------------------------------
#  Mesh
#-----------------------------------
h_cor  = coor(n_fic+'.cor')
h_ele  = elem(n_fic+'.ele')
h_grid = grid(h_cor, h_ele)

#-----------------------------------
#  Nodal properties
#-----------------------------------
h_vno_hydro  = vnod(n_fic+'.pnh')
h_vno_zpre   = vnod(n_fic+'.zpr')
h_vno_tl2    = vnod(n_fic+'.tl2')
h_vno_fl1    = vnod(n_fic+'.fl1')
h_vno_fl2    = vnod(n_fic+'.fl2')
h_vno_fl3    = vnod(n_fic+'.fl3')
h_vno_fl4    = vnod(n_fic+'.fl4')
h_vno_fl5    = vnod(n_fic+'.fl5')
h_prn        = prno(h_vno_hydro, h_vno_zpre, h_vno_tl2,
                    h_vno_fl1, h_vno_fl2, h_vno_fl3, h_vno_fl4, h_vno_fl5)           # regroupement des champs en prno

#-----------------------------------
#  Elemental properties
#-----------------------------------
h_pre  = 0                      # No elemental properties

#-----------------------------------
#  Global properties
#-----------------------------------
h_pgl  = prgl(9.81e+00,     #  1 gravite
              1.0e+03,      #  2 water density
              1.0e-06,      #  3 water viscosity
              2.65e+03,     #  4 sediment density
              1.0e+00,      #  5 bed material porosity
              1.0e+00,      #  6 active layer thickness
              1.0e+00,      #  7 artificial viscosity
              1.0e+00,      #  8 exchange parameter
              0.0e+01,      #  9 bed shear direction correction for helical flow, beta (0 = for no correction)
              0.0e-00,      # 10 bed shear direction correction for bed slope, 1/fs (0 = for no correction)
              1,            # 11 total shear stress formula (1 - MANNING LAW, 2 - MODIFIED MANNING LAW)
              1,            # 12 effective shear stress formula (1 - NO SHEAR PARTITION (EFFECTIVE STRESS = TOTAL STRESS),
                            #                                    2 - USING ENGELUND AND HANSEN, 1967 FORMULA,
                            #                                    3 - USING WU AND WANG, 1999 FORMULA)
              0.05e-00,     # 13 dimensionless critical shear stress: varies between 0.023 to 0.06 (used by bedload equation using Wu et al, 200)
              1,            # 14 bed load equation type:
                            #    1)Ackers and White (1973)
                            #    2)WU et al. (2000)
                            #    3)For model testing
                            #    4)For testing sed2d-sv2d connection
              1.0e+00)      #  11 upper size of class 1 sediment in phi units

#-----------------------------------
#  Loads
#-----------------------------------
h_slr  = 0
h_slc  = 0

#-----------------------------------
#  Boundary conditions
#-----------------------------------
h_cnd = condition(n_fic+'.cnd')
h_lmt = boundary (n_fic+'.bnd')
h_bc  = boundary_condition(h_lmt, h_cnd)

#-----------------------------------
#  Unknowns
#-----------------------------------
h_ddl  = dof()

#-----------------------------------
#  Simulation data
#-----------------------------------
h_sol  = simd(h_frm, h_grid, h_ddl, h_bc, h_slc, h_slr, h_pgl, h_prn, h_pre)

#------------------------------------
#  Stopping criteria, limiter
#------------------------------------
h_limiter = limiter(h_sol, 1, 1)
h_criarret = cria_l2_allrel(h_sol, 0.0, 1.0e-15)

#-----------------------------------
#  DÃ©finition des algo de rÃ©solution
#-----------------------------------
h_resmat = mkl_pardiso()
h_btrk = 0
h_newton = newton(h_btrk, h_resmat, 3)

#-----------------------------------
#  RÃ©solution
#-----------------------------------
h_post = 0

h_sol.init_f(n_fic+'.ini', 0)
h_newton.solve(h_sol, h_criarret, h_limiter, h_post, 1)

#-----------------------------------
#  Final results
#-----------------------------------
h_sol.save(n_fic+'.fin')

#-----------------------------------
#  End of calculation
#-----------------------------------
stop()

