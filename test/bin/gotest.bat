
rem --- Change de repertoire
set HOME_DIR=\dispersim
v:
cd %HOME_DIR%

rem --- Efface les vieux fichiers
del /s *.err
del *.htm

rem --- Make
set PATH=c:\apps\cygwin;%PATH%;
set GENTEST_BINDIR=v:/dispersim/bin
set DISPERSIM=t:/hydrosim/bin/win32/dispersim.exe
gmake --unix -f makefile.mak

rem --- Compile les résultats
bin\cmpltest

