#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import csv
import logging
import math
import numpy as np
import random
import sys

logger = logging.getLogger("INRS.IEHSS.Test")

# Dans tout ce fichier, il serait bien d'utiliser une hiérarchie d'exception

class DistNod:
    """Représentation d'une table de distribution de noeuds (table de partition)
    """
# Note: Attention, l'utilisation du mot "noeud" est erroné lorsqu'on a une
# renumérotation d'éléments
    def __init__(self, tabDist, appNod, nbrNodDom, nbrNodTot, nbrDom, option):
        """Initialise un objet DistNod

option vaut 1 s'il s'agit d'une partition, 2 s'il s'agit d'une renumérotation
        """

        self.__tabDist = tabDist
        self.__appNod = appNod
        self.__nbrNodDom = nbrNodDom
        self.__nbrNodTot = nbrNodTot
        self.__nbrDom = nbrDom
        self.__option = option

        self.__verifieIntegrite()

    def __verifieIntegrite(self):
        """Vérifie que la table de distribution est bien valide"""

        # vérification que chaque noeud a un sous-domaine valide
        for iNod in range(self.__nbrNodTot):
            domNodI = self.__appNod[iNod]                  # domaine auquel appartient le noeud i
            if ((domNodI < 0 or domNodI >= self.__nbrDom) and self.__option != 2):
                erreur = "ERREUR: Le sous-domaine d'au moins un noeud n'est pas valide"
                logger.error(erreur)
                logger.error("    Noeud %d" % iNod)
                raise Exception(erreur)
            numLocNodI = self.__tabDist[iNod,domNodI]      # numéro local du noeud i
            if (numLocNodI == -1):
                erreur = "ERREUR: Un noeud n'a pas de numéro local dans le domaine auquel il appartient"
                logger.error(erreur)
                logger.error('    Noeud %d, domaine %d' % (iNod, domNodI))
                raise Exception(erreur)

        # vérification de la numérotation locale de chaque sous-domaine
        for iDom in range(self.__nbrDom):
            ensemble = set(range(self.__nbrNodDom[iDom]))
            for iNod in range(self.__nbrNodTot):     # on vérifie que les élément non-nuls sont une permutation de {1,...,__nbrNodDom[j]}
                if self.__tabDist[iNod,iDom] != -1:
                    try:
                        ensemble.remove(self.__tabDist[iNod,iDom])
                    except:
                        if (self.__tabDist[iNod,iDom] < 1 \
                            or self.__tabDist[iNod,iDom] > self.__nbrNodDom[iDom]):
                            erreur = "ERREUR: Un noeud est mal numéroté dans la table de " \
                                     + "distribution ou le nombre de noeuds du domaine est invalide"
                        else:
                            erreur = "ERREUR: Un numéro de noeud revient deux" \
                                     + " fois dans la table de distribution"
                        logger.error(erreur)
                        logger.error("    Domaine: %d Noeud: %d" % (iDom+1, iNod+1))
                        logger.error("    Numérotation locale: %d" % self.__tabDist[iNod,iDom])
                        raise Exception(erreur)
            if len(ensemble) != 0:
                logger.error("ERREUR: Le domaine %d n'a pas %d noeuds comme" \
                             + " indiqué dans le fichier .part" % (iDom+1,self.__nbrNodDom[iDom]))
                raise Exception("ERREUR: Un domaine n'a pas le même nombre de noeuds qu'indiqué dans le fichier .part")

    @classmethod
    def charge(cls, fichier, option):
        """Initialise un objet DistNod à partir d'un fichier

charge(fichier,option)
fichier est un handle sur le fichier .part
option vaut 1 s'il s'agit d'une partition, 2 s'il s'agit d'une renumérotation
retourne un objet DistNod
"""
        if option != 1 and option != 2:
            raise Exception("ERREUR: Mauvais appel la méthode DistNod.charge()," \
                            + " option doit être 1 ou 2")

        # première ligne de l'entête
        entete = fichier.readline()
        listeEntete = entete.split()
        nbrNodTot = int(listeEntete[0])
        nbrDom = int(listeEntete[1])
        if option == 2 and nbrDom != 1:
            raise Exception("ERREUR: La renumérotation contient plus d'un domaine")

        # deuxième ligne de l'entête
        entete = fichier.readline()
        listeEntete = entete.split()
        nbrNodDom = map(int, listeEntete) # indique combien de noeud les domaines ont
        if len(nbrNodDom) != nbrDom:
            raise Exception("ERREUR: Format de fichier .part invalide")

        tabDist = np.zeros([nbrNodTot, nbrDom], 'i')
        appNod  = np.zeros([nbrNodTot],'i') # indique à quel domaine appartient le noeud
        listeStringKDist = fichier.readlines()
        if len(listeStringKDist) != nbrNodTot:
            raise Exception("ERREUR: Le nombre de lignes du fichier .part ne concorde pas avec le nombre de noeuds")
        for iNod in range(nbrNodTot):
            liste = listeStringKDist[iNod].split()
            try:
                appNod[iNod] = int(liste[nbrDom]) - 1 # le -1 fait passer en numérotation C plutôt que fortran
            except:
                raise Exception("ERREUR: Le nombre de colonnes de la ligne %s du fichier .ele est invalide" % str(i+3))
            for iDom in range(nbrDom):
                tabDist[iNod,iDom] = int(liste[iDom]) - 1

        return cls(tabDist, appNod, nbrNodDom, nbrNodTot, nbrDom, option)

    @classmethod
    def genNumAleat(cls, nbrNod):
        """Créé une numérotation aléatoire de nbrNod noeuds"""

        tabDist = np.zeros([nbrNod,1], 'i')
        listeNum = range(nbrNod)
        random.shuffle(listeNum)
        tabDist[:,0] = np.array(listeNum)
        appNod = np.zeros([nbrNod], 'i') - 1
        nbrNodDom = [nbrNod]
        nbrDom = 1

        return cls(tabDist, appNod, nbrNodDom, nbrNod, nbrDom, 2)

    @classmethod
    def genNumIden(cls, nbrNod):
        """Créé une numérotation identité de nbrNod noeuds"""

        tabDist = np.zeros([nbrNod,1], 'i')
        tabDist[:,0] = np.array(range(nbrNod))
        appNod = np.zeros([nbrNod], 'i') - 1
        nbrNodDom = [nbrNod]
        nbrDom = 1

        return cls(tabDist, appNod, nbrNodDom, nbrNod, nbrDom, 2)

    def inverseNum(self):
        """Inverse la numérotation (on trouve l'inverse additif des noeuds)"""

        for iNod in range(self.__nbrNodTot):
            for iDom in range(self.__nbrDom):
                self.__tabDist[iNod,iDom] = self.__nbrNodDom[iDom] - self.__tabDist[iNod,iDom] - 1

    def inversePerm(self):
        """Inverse la numérotation (on trouve la permutation inverse)"""

        if self.__option != 2:
            raise Exception("ERREUR: cette méthode ne peut pas être utilisée sur un partitionnement")

        tabDistInv = np.zeros([self.__nbrNodTot, self.__nbrDom], 'i')

        for iNod in range(self.__nbrNodTot):
            tabDistInv[self.__tabDist[iNod,0],0] = iNod

        self.__tabDist = tabDistInv

    def reqAppNod(self):
        return self.__appNod

    def reqNbrDom(self):
        return self.__nbrDom

    def reqNbrNodDom(self):
        return self.__nbrNodDom

    def reqNbrNodTot(self):
        return self.__nbrNodTot

    def reqTabDist(self):
        return self.__tabDist

    def sauve(self, fichier):
        """Sauvegarde la numérotation dans fichier, qui doit être ouvert en écriture"""

        # écriture de l'entête
        ligne = str(self.__nbrNodTot).rjust(9) + str(self.__nbrDom).rjust(9) + '\n'
        fichier.write(ligne)
        ligne = ""
        for nbrNodLoc in self.__nbrNodDom:
            ligne = ligne + str(nbrNodLoc).rjust(9)
        ligne = ligne + '\n'
        fichier.write(ligne)

        # écriture de la table de distribution
        for iNod in range(self.__nbrNodTot):
            ligne = ""
            for iDom in range(self.__nbrDom):
                ligne = ligne + str(self.__tabDist[iNod,iDom]+1).rjust(9)
            ligne = ligne + str(self.__appNod[iNod]+1).rjust(9) + '\n'
            fichier.write(ligne)
# fin classe DistNod


class GrilleElem:
    """Représentation d'une grille d'éléments"""
    def __init__(self, kng, nbrNodEl, nbrElTot, nbrNodTot):
        """Initialise une grille d'éléments
        """

        self.__kng = kng
        self.__nbrNodEl = nbrNodEl
        self.__nbrElTot = nbrElTot
        self.__nbrNodTot = nbrNodTot
        self.__verifieIntegrite()
        self.__construitTabConnInv()

    def __construitTabConnInv(self):
        """Construit la table des connexités inverses"""
        self.__idxElem=np.zeros([self.__nbrNodTot + 1], 'i')
        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                noNod = self.__kng[iEl,iNod]
                self.__idxElem[noNod] = self.__idxElem[noNod] + 1

        # cumule la table
        for iNod in range(self.__nbrNodTot + 1):
            self.__idxElem[iNod] = self.__idxElem[iNod] + self.__idxElem[iNod-1];
        for iNod in range(self.__nbrNodTot,0,-1):
            self.__idxElem[iNod] = self.__idxElem[iNod - 1];
        self.__idxElem[0] = 0

        # table auxiliaire: indice des éléments pour chaque noeud
        aux = np.array(self.__idxElem);

        # monte la table des connectivités inverses
        self.__connecInv = np.zeros([self.__idxElem[self.__nbrNodTot]],'i')
        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                noNod = self.__kng[iEl,iNod]
                self.__connecInv[aux[noNod]] = iEl
                aux[noNod] = aux[noNod] + 1

    def __verifieIntegrite(self):
        if len(self.__kng) != self.__nbrElTot:
            raise Exception("ERREUR: La taille de la table de connectivités" \
                            + " ne correspond pas au nombre d'éléments total")
        if np.size(self.__kng) / self.__nbrElTot != self.__nbrNodEl:
            raise Exception("ERREUR: La taille de la table de connectivités" \
                            + " ne correspond pas au nombre de noeuds par élément")
        nodMin = 1
        nodMax = 0
        for iEl in range(self.__nbrElTot):
            ensembleNodEl = set()
            for iNod in range(self.__nbrNodEl):
                noNod = self.__kng[iEl,iNod]
                if noNod < nodMin:
                    nodMin = noNod
                elif noNod > nodMax:
                    nodMax = noNod
                ensembleNodEl.add(noNod)
            if len(ensembleNodEl) != self.__nbrNodEl:
                raise Exception("ERREUR: Un noeud revient plusieurs fois dans le même élément")
        if nodMin != 0:
            raise Exception("ERREUR: Le noeud minimal n'est pas 1")
        if nodMax != self.__nbrNodTot - 1:
            raise Exception("ERREUR: Le noeud maximal n'est pas égal au nombre de noeuds total")

    def appliqueNum(self, tabNumNod):
        # lis le tableau de connectivités avec la numérotation de noeuds
        kngNouvNum = np.zeros([self.__nbrElTot,self.__nbrNodEl],'i')
        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                kngNouvNum[iEl,iNod] = tabNumNod[self.__kng[iEl,iNod]]

        self.__kng = kngNouvNum
        self.__verifieIntegrite()
        self.__construitTabConnInv()
        return

    @classmethod
    def charge(cls, fichier):
        """Initialise une grille à partir d'un fichier .ele

fichier est un handle sur le fichier .ele
La méthode retourne un objet de type GrilleElem
"""
        entete = fichier.readline()
        listeEntete = entete.split()
        nbrElTot = int(listeEntete[0])
        nbrNodEl = int(listeEntete[1])
        if ((nbrNodEl == 3 and listeEntete[2] != "201") or \
           (nbrNodEl == 6 and listeEntete[2] != "203") or \
           (nbrNodEl != 3 and nbrNodEl != 6)):
            raise Exception("ERREUR: Format de fichier .elem invalide")

        # construit la table de connectivités
        kng = np.zeros([nbrElTot,nbrNodEl], 'i')
        listeStringKNG = fichier.readlines()
        for iEl in range(nbrElTot):
            liste = listeStringKNG[iEl].split()

            for iNod in range(nbrNodEl):
                kng[iEl,iNod] = int(liste[iNod])-1 # le -1 fait passer en numérotation C plutôt que fortran

        # compte le nombre d'éléments total
        ensembleNod = set([])
        for iEl in range(nbrElTot):
            for iNod in range(nbrNodEl):
                ensembleNod.add(kng[iEl,iNod])     # ensemble des noeuds du maillage
        nbrNodTot = len(ensembleNod)
        if (min(ensembleNod) != 0 or \
            max(ensembleNod) != nbrNodTot - 1):
            raise Exception("ERREUR: la numérotation du fichier .ele est invalide")

        return cls(kng, nbrNodEl, nbrElTot, nbrNodTot)

    def cmpElem(self, elem1, elem2):
        retour = 0
        elem1 = np.sort(elem1)
        elem2 = np.sort(elem2)
        i = 0
        while i < range(len(elem1)) and retour == 0:
            if elem1[i] < elem2[i]:
                retour = - 1
            elif elem1[i] > elem2[i]:
                retour = 1
            i += 1
        return retour

    def ctrNumEl(self, tabNumNod):
        # lis le tableau de connectivités avec la numérotation de noeuds
        kngNouvNum = np.zeros([self.__nbrElTot,self.__nbrNodEl],'i')
        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                kngNouvNum[iEl,iNod] = tabNumNod[self.__kng[iEl,iNod],0]

        # trie les éléments
        listeElem = range(self.__nbrElTot)
        cle = lambda iEl: self.__kng[iEl,:]
        listeElem.sort(cmp = self.cmpElem, key = cle)
        tabNumEl = np.zeros([self.__nbrElTot, 1],'i')
        for iEl in range(self.__nbrElTot):
            tabNumEl[iEl,0] = listeElem[iEl]
        appNod = np.zeros([self.__nbrNodTot],'i')
        numEl = DistNod(tabNumEl, appNod, [self.__nbrElTot], self.__nbrElTot, 1, 2)
        return numEl

    def inverseNum(self):
        """Inverse la numérotation des noeuds"""

        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                self.__kng[iEl,iNod] = self.__nbrNodTot - self.__kng[iEl,iNod] - 1

        # on actualise la table des connexités inverses
        self.__construitTabConnInv()

    def reqIdxElemConnInv(self):
        return self.__idxElem

    def reqKNG(self):
        return self.__kng

    def reqNbrElTot(self):
        return self.__nbrElTot

    def reqNbrNodEl(self):
        return self.__nbrNodEl

    def reqNbrNodTot(self):
        return self.__nbrNodTot

    def reqTabConnInv(self):
        return self.__connecInv

    def sauve(self, fichier):
        # écriture de l'entête
        fichier.write(str(self.__nbrElTot) + '   ' + str(self.__nbrNodEl) + '   ')
        if self.__nbrNodEl == 3:
            fichier.write(str(201) + '   ')
        else:
            fichier.write(str(203) + '   ')
        fichier.write(str(0.0) + '\n') # temps ?

        # écriture des lignes
        for iEl in range(self.__nbrElTot):
            for iNod in range(self.__nbrNodEl):
                fichier.write(str(self.__kng[iEl,iNod]+1).rjust(9))
            fichier.write('\n')

        fichier.close()
# fin classe GrilleElem


def logStats(tab):
    minTab = min(tab)
    maxTab = max(tab)
    tabDouble = np.array(tab,'d')
    moyTab = np.average(tabDouble)
    moyTabCarre = np.average(tabDouble*tabDouble)
    ecartType = math.sqrt(moyTabCarre - moyTab*moyTab)

    moyTabStr = "%.2f" % moyTab
    moyTabStr = moyTabStr.rjust(10)
    minTabStr = str(minTab).rjust(10)
    maxTabStr = str(maxTab).rjust(10)
    ecartTypeStr = "%.2f" % ecartType
    ecartTypeStr = ecartTypeStr.rjust(10)

    logger.info("INFO:         Moyenne:   %s" % moyTabStr)
    logger.info("INFO:         Min:       %s" % minTabStr)
    logger.info("INFO:         Max:       %s" % maxTabStr)
    logger.info("INFO:         Écart type:%s" % ecartTypeStr)

def calcLargBande(gElem, tabNum):
    """Calcule la largeur de bande de la matrice associée
au maillage gElem et à la numérotation tabNum"""

    nbrNodEl = gElem.reqNbrNodEl()
    nbrNodTot = gElem.reqNbrNodTot()
    kng = gElem.reqKNG()
    tabConnInv = gElem.reqTabConnInv()
    idxElem = gElem.reqIdxElemConnInv()
    tabLargBande = np.zeros([nbrNodTot],'i')
    for iNod in range(nbrNodTot):
        iNodMax = tabNum[iNod]
        for iEl in tabConnInv[idxElem[iNod]:idxElem[iNod+1]]: # pour tous les éléments dont iNod fait parti
            for iNodEl in range(nbrNodEl):                    # pour chaque noeud de cet élément
                iNodTmp = kng[iEl,iNodEl]
                iNodTmpNouvNum = tabNum[iNodTmp]
                if iNodTmpNouvNum > iNodMax:
                    iNodMax = iNodTmpNouvNum
        tabLargBande[iNod] = iNodMax - tabNum[iNod]
    return tabLargBande

def calcWavefront(gElem, tabNum):
    """Calcule le wavefront de la matrice associée
au maillage gElem et à la numérotation tabNum

Définition du wavefront prise à partir de l'algo de Boost:
http://www.boost.org/doc/libs/1_35_0/boost/graph/wavefront.hpp
L'algo a été modifié pour améliorer la performance."""

    nbrNodTot = gElem.reqNbrNodTot()
    nbrNodEl = gElem.reqNbrNodEl()
    kng = gElem.reqKNG()
    tabConnInv = gElem.reqTabConnInv()
    idxElem = gElem.reqIdxElemConnInv()

    # Calcul du tableau de la permutation inverse
    tabNumInv = np.zeros([nbrNodTot],'i')
    for i in range(nbrNodTot):
        tabNumInv[tabNum[i]] = i

    # Calcul du wavefront
    tabWavefront = np.zeros([nbrNodTot+1], 'i') # un élément de plus pour éviter de vérifier jNod < nbrNodTot
    flags = np.zeros([nbrNodTot],'i') - 1 # on initialise les flags à -1
    for iNod in range(nbrNodTot):
        iNodAncNum = tabNumInv[iNod]
        for iEl in tabConnInv[idxElem[iNodAncNum]:idxElem[iNodAncNum+1]]:
            for jNodEl in range(nbrNodEl):
                jNodAncNum = kng[iEl,jNodEl]
                jNod = tabNum[jNodAncNum]
                if jNod >= iNod:
                    fin = flags[jNod]
                    if fin >= iNod:
                        debut = fin
                    else:
                        debut = iNod
                    flags[jNod] = jNod + 1
                    if jNod >= debut: # on doit additionner 1 au wavefront des noeuds de debut à jNod
                        tabWavefront[debut] += 1
                        tabWavefront[jNod+1] -= 1

    # on cumule le wavefront
    wavefront = 0
    for iNod in range(nbrNodTot):
        wavefront += tabWavefront[iNod]
        tabWavefront[iNod] = wavefront
    return tabWavefront[0:nbrNodTot] # on tronque le dernier élément

def finProgramme(racineFichier, erreur):
    """Vérifie si on a bien rencontré l'erreur attendue et termine le programme
    """
    # chargement du fichier de configuration pour trouver l'erreur attendue
    try:
        fichierConfig = open(racineFichier + '.tpn')
    except Exception:
        logger.error("test=ERREUR: Impossible d'ouvrir le fichier de configuration %s" % racineFichier + '.tpn')
        sys.exit(1)
    lignesConfig = fichierConfig.readlines()

    try:
        erreurAttendue = trouveValeur(lignesConfig, "erreurAttendue", str)
    except Exception, strErreur:
        logger.error("test=" + strErreur)
        sys.exit(1)

    erreurSansEspace = "".join(erreur.split())
    logger.info("INFO: Erreur attendue: " + erreurAttendue)
    logger.info("INFO: Erreur obtenue: " + erreurSansEspace)

    erreurOK = False
    # si H2D2 n'aurait pas du s'exécuter normalement
    if erreurAttendue[0:5] == "H2D2:":
        erreurAttendue = erreurAttendue[5:len(erreurAttendue)]
        try:
            fichierOut = open(racineFichier + ".out")
        except Exception:
            loggeer.error("test=ERREUR: Impossible d'ouvrir le fichier %s.out" % racineFichier)
            sys.exit(1)
        lignes = fichierOut.readlines()
        i = 0
        while i < len(lignes) and not erreurOK:    # on cherche l'erreur attendue dans le fichier .out
            ligneSansEspaces = "".join(lignes[i].split())
            ligneSansEspacesMin = ligneSansEspaces.lower()
            if ligneSansEspacesMin.find(erreurAttendue.lower()) != -1:
                erreurOK = True
            i = i + 1
        if not erreurOK:
            erreur = "ERREUR: H2D2 n'a pas laissé le message d'erreur attendu"
    else: # si H2D2 devait s'exécuter normalement
        if erreurSansEspace.lower() == erreurAttendue.lower():
            erreurOK = True

    if erreurOK:
        logger.error("test=ok")
    else:
        logger.error("test=" + erreur)
    sys.exit(0)

def main():
    # message qui explique l'appel du programme
    msgCommande = """Utilisation: Vous devez faire un appel de la facon suivante:
    test_part_num.py racineFichier
        Les fichier .ele, .num, .tpn et .out sont nécessaires.
    test_part_num.py -h          pour afficher ce message"""

    if (len(sys.argv) != 2):
        print msgCommande
        sys.exit(1)
    if (sys.argv[1] == "-h"):
        print msgCommande
        sys.exit(0)

    racineFichier = sys.argv[1]

    # assignation du logger
    fileHandler = logging.FileHandler(racineFichier + '.err', 'w')
    logger.addHandler(fileHandler)
    logger.setLevel(logging.INFO)

    # charger le maillage
    try:
        fichierElem = open(racineFichier + ".ele")
    except Exception:
        erreur = "ERREUR: Impossible d'ouvrir le fichier %s" \
                 % racineFichier + ".ele"
        finProgramme(racineFichier, erreur)
    try:
        gElem = GrilleElem.charge(fichierElem)
    except Exception, exceptErreur:
        finProgramme(racineFichier, exceptErreur.__str__())
    logger.info("OK: Maillage chargé")
    fichierElem.close()

    # vérification que l'exécution de h2d2 s'est bien déroulée
    try:
        fichierOut = open(racineFichier + ".out")
    except:
        erreur = "ERREUR: Impossible d'ouvrir le fichier %s" \
                        % racineFichier + ".out"
        finProgramme(racineFichier, erreur)

    lignesOut = fichierOut.readlines()
    fichierOut.close()
    if len(lignesOut) != 0:
        derniereLigne = lignesOut[-1]
    else:
        derniereLigne = ""
    if derniereLigne[0:11] != "Fin normale":
        erreur = "ERREUR: H2D2 ne s'est pas exécuté normalement"
        finProgramme(racineFichier, erreur)

    # charger le fichier de configuration et appel du bon test
    try:
        fichierConfig = open(racineFichier + '.tpn')
    except Exception:
        erreur = "ERREUR: Impossible d'ouvrir le fichier de configuration %s" \
                 % racineFichier + '.tpn'
        finProgramme(racineFichier, erreur)
    lignesConfig = fichierConfig.readlines()

    try:
        typeTest = trouveValeur(lignesConfig, "typeTest", str)
        if typeTest == "part":
            nbrDom = trouveValeur(lignesConfig, "nbrDom", int)
            fichierConfig.close()
            erreur = testPart(racineFichier, gElem, nbrDom)
        elif typeTest == "num":
            fichierConfig.close()
            erreur = testNumNod(racineFichier, gElem)
        elif typeTest == "num_el":
            fichierConfig.close()
            erreur = testNumEl(racineFichier, gElem)
        else:
            erreur = "ERREUR: typeTest doit être \"part\" ou \"num\""
    except Exception, exceptErreur:
        finProgramme(racineFichier, exceptErreur.__str__())
    finProgramme(racineFichier,erreur)

def testNumNod(racineFichier, gElem):
    try:
        fichierRenum = open(racineFichier + ".num")
    except:
        raise Exception("ERREUR: Impossible d'ouvrir le fichier %s" % racineFichier + ".num")
    renum = DistNod.charge(fichierRenum, 2)
    logger.info("OK: Init. de la table de renumérotation réussie: la table et la numérotation sont valides")
    fichierRenum.close()

    # vérification que le nombre de noeud total est le même pour le fichier .ele et le fichier .num
    nbrNodTot = gElem.reqNbrNodTot()
    if (nbrNodTot != renum.reqNbrNodTot()):
        raise Exception("ERREUR: Le nombre de noeuds total est incorrect (.num et .elem donnent un nombre différent)")
    logger.info("OK: On a bien le bon nombre de noeuds total")

    # calcul des largeurs de bandes et du wavefront avec la numérotation originale
    nbrNodTot = gElem.reqNbrNodTot()
    listeNum = range(nbrNodTot)      # on ne change pas la numérotation
    tabNum = np.array(listeNum)
    logger.info("INFO: Avec la numérotation originiale:")
    logger.info("INFO:     Statistiques sur la largeur de bande")
    tabLargBandeOri = calcLargBande(gElem, tabNum)
    logStats(tabLargBandeOri)
    #if nbrNodTot <= 500:
    logger.info("INFO:     Statistiques sur le wavefront")
    tabWavefront = calcWavefront(gElem, tabNum)
    logStats(tabWavefront)

    ## calcul des largeurs de bandes avec une numérotation aléatoire
    #nbrNodTot = gElem.reqNbrNodTot()
    #random.shuffle(listeNum)
    #tabNum = np.array(listeNum)
    #logger.info("INFO: Avec la numérotation \"aléatoire\":")
    #logger.info("INFO:     Statistiques sur la largeur de bande")
    #tabLargBandeAleat = calcLargBande(gElem, tabNum)
    #logStats(tabLargBandeAleat)
    #if nbrNodTot <= 500:
        #logger.info("INFO:     Statistiques sur le wavefront")
        #tabWavefront = calcWavefront(gElem, tabNum)
        #logStats(tabWavefront)

    # calcul des largeurs de bandes avec la nouvelle numérotation
    tabNum = renum.reqTabDist()
    logger.info("INFO: Avec la nouvelle numérotation:")
    logger.info("INFO:     Statistiques sur la largeur de bande")
    tabLargBandeNouv = calcLargBande(gElem, tabNum)
    logStats(tabLargBandeNouv)
    #if nbrNodTot <= 500:
    logger.info("INFO:     Statistiques sur le wavefront")
    tabWavefront = calcWavefront(gElem, tabNum)
    logStats(tabWavefront)

    #écriture des largeurs de bande en format csv pour lire avec excel
    fichierCsv = open(racineFichier + ".csv",'wb')
    writerCsv = csv.writer(fichierCsv)
    for i in range(nbrNodTot):
        writerCsv.writerow([tabLargBandeOri[i],tabLargBandeNouv[i]])
    fichierCsv.close()
    return "aucune"

def testNumEl(racineFichier, gElem):
    try:
        fichierRenum = open(racineFichier + ".num")
    except Exception:
        raise Exception("ERREUR: Impossible d'ouvrir le fichier %s .num" % racineFichier)
    renum = DistNod.charge(fichierRenum, 2)
    nbrElTot = gElem.reqNbrElTot()
    if nbrElTot != renum.reqNbrNodTot():
        raise Exception("ERREUR: Nombre d'éléments différent dans les fichier .ele et .num")
    numIden = DistNod.genNumIden(gElem.reqNbrNodTot())
    tabIden = numIden.reqTabDist()
    numEl = gElem.ctrNumEl(tabIden)
    tabDistEl = numEl.reqTabDist()
    tabDistElCharge = renum.reqTabDist()
    for iEl in range(nbrElTot):
        if tabDistEl[iEl] != tabDistElCharge[iEl]:
            raise Exception("ERREUR: Numérotation frontale différente de ce qui est attendu")
    return "aucune"

def testPart(racineFichier, gElem, nbrDom):
    erreur = "aucune"
    try:
        fichierPart = open(racineFichier + ".num")
    except Exception:
        raise Exception("ERREUR: Impossible d'ouvrir le fichier .num")
    partition = DistNod.charge(fichierPart, 1)
    fichierPart.close
    logger.info("OK: Init. de la table de distribution réussie: la table et la numérotation sont valides")

    # vérification que le nombre de noeud total est le même pour le fichier .ele et le fichier .num
    nbrNodTot = gElem.reqNbrNodTot()
    if (nbrNodTot != partition.reqNbrNodTot()):
        raise Exception("ERREUR: Le nombre de noeuds total est incorrect (.num et .elem donnent un nombre différent)")
    logger.info("OK: On a bien le bon nombre de noeuds total")

    # vérification qu'on a bien le nombre de sous-domaines demandé
    if (nbrDom != partition.reqNbrDom()):
        raise Exception("ERREUR: On n'a pas le nombre de sous-domaines demandé")
    logger.info("OK: On a bien le bon nombre de sous-domaines")

    # vérification que les domaines sont connexes
    tabDist = partition.reqTabDist()
    nbrNodDom = partition.reqNbrNodDom()
    tabConnInv = gElem.reqTabConnInv()
    idxElem = gElem.reqIdxElemConnInv()
    nbrNodEl = gElem.reqNbrNodEl()
    kng = gElem.reqKNG()

    connexe = True
    for iDom in range(nbrDom):
        iNod = 0
        while tabDist[iNod,iDom] == -1:             # trouve un noeud du domaine
            iNod = iNod + 1
        nodNonVisites = set(range(nbrNodDom[iDom])) # ensemble des noeuds non visités
        pileNod = [iNod]                            # pile pour parcourir le graphe
        while len(pileNod) != 0:
            iNod = pileNod.pop()
            for iEl in tabConnInv[idxElem[iNod]:idxElem[iNod+1]]: # pour tous les éléments dont iNod fait parti
                for iNodEl in range(nbrNodEl):
                    iNodGlob = kng[iEl,iNodEl]
                    iNodLoc = tabDist[iNodGlob,iDom]
                    if iNodLoc != -1: # si le noeud de l'élément est dans le domaine courant
                        if iNodLoc in nodNonVisites:
                            nodNonVisites.remove(iNodLoc)
                            pileNod.append(iNodGlob)
        if len(nodNonVisites) != 0:
            logger.error("ERREUR: Le domaine %s n'est pas connexe" % str(iDom + 1))
            #raise Exception("ERREUR: Un domaine n'est pas connexe")
            erreur = "ERREUR: Un domaine n'est pas connexe"
            connexe = False
    if connexe:
        logger.info("OK: Tous les domaines sont connexes")

    # vérification du balancement avec une tolérance de
    # 1.05 pour parmetis et 0.005/poidsDesire pour scotch
    try:
        fichierConfig = open(racineFichier + ".tpn")
    except Exception:
        raise Exception("ERREUR: Impossible d'ouvrir le fichier %s.tpn" % racineFichier)
    lignes = fichierConfig.readlines()
    #malBalanceOK = False
    #try:
        #malBalanceOK = trouveValeur(lignes, "malBalanceOK", bool)
    #except:
        #pass
    libUtilisee = ""
    libUtilisee = trouveValeur(lignes, "libUtilisee", str)
    fichierConfig.close()

    poidsDesire = 1.0/float(nbrDom)
    Tol = 0
    if (libUtilisee.lower() == "parmetis"):
        Tol = 0.05
    elif (libUtilisee.lower() == "scotch"):
        Tol = 0.005/poidsDesire
    else:
        raise Exception("ERREUR: La valeur libUtilisee lue dans %s.tpn est invalide" % racineFichier)

    Tol = max(4.0/(float(nbrNodTot)*poidsDesire), Tol) # on permet un débalancement de 4 noeuds
                                                       # ou la tolérence de la libraire (le max des deux)

    appNod = partition.reqAppNod()
    nbrNodAppDom = np.zeros([nbrDom],'i')
    for iNod in range(nbrNodTot): # calcul du nombre de noeuds appartenant à chaque domaine
        dom = appNod[iNod]
        nbrNodAppDom[dom] = nbrNodAppDom[dom] + 1
    bienBalance = True
    for iDom in range(nbrDom):
        poids = float(nbrNodAppDom[iDom])/float(nbrNodTot)
        if (abs(poids-poidsDesire) > Tol*poidsDesire):
            logger.error("ERREUR: Le domaine %s n'est pas bien balancé" % str(iDom + 1))
            logger.error("    Fraction du poids désirée: %f Fraction du poids réelle: %f Tolérance: %.3f" \
                         %(poidsDesire, poids, Tol))
            #if not malBalanceOK:
            erreur = "ERREUR: Un domaine est mal balancé"
            bienBalance = False
    if bienBalance:
        logger.info("OK: Les domaines sont bien balancés")
    #elif (not bienBalance and malBalanceOK):
    else:
        logger.info("OK: Les domaines ne sont pas bien balancés, mais le fichier .tpn dit que c'est correct")

    # calcul de la taille de l'intersection
    tailleInter = 0
    for iNod in range(nbrNodTot):
        nbrDomTmp = 0               # nombre de domaines auxquels le noeud appartient
        for iDom in range(nbrDom):
            if tabDist[iNod,iDom] != -1:
                nbrDomTmp = nbrDomTmp + 1
        if nbrDomTmp != 1:
            tailleInter = tailleInter + 1
    pourcentageInter = float(tailleInter)/float(nbrNodTot)*100
    logger.info("INFO: L'intersection a %d noeuds sur un total de %d, soit %.2f %%" \
                %(tailleInter, nbrNodTot, pourcentageInter))
    return erreur

def trouveValeur(lignes, nom, typeValeur):
    i = 0
    trouve = False
    while ((not trouve) and i < len(lignes)):
        mots = lignes[i].split()
        ligne = "".join(mots)   # ligne n'a plus de whitespace
        if ligne[0:len(nom)+1] == nom + '=':
            try:
                valeur = typeValeur(ligne[len(nom)+1:len(ligne)])  # transforme ce qui est à droite du "=" en type typeValeur
            except Exception:
                raise Exception("ERREUR: Fichier .tpn invalide, impossible de trouver la valeur de %s" % nom)
            trouve = True
        i = i + 1
    if not trouve:
        raise Exception("ERREUR: Fichier .tpn invalide, impossible de trouver la valeur de %s" % nom)
    return valeur

if __name__ == '__main__':
    main()
