#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import time

(TEST_INVALIDE, TEST_OK, TEST_ERREUR) = range(3)
nbrEcr = 0
indTest = 0
testOK = []

def boucleRecursifSurFichiers(dossier, fichier, fonction):
    # exécution de la fonction
    fonction(dossier, fichier)
    # boucle récursive sur les sous-répertoires
    for f in os.listdir(dossier):
        fullPath = os.path.join(dossier, f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            boucleRecursifSurFichiers(fullPath, fichier, fonction)

def compileStatut(dossier, fichier):
    etat = TEST_INVALIDE
    fichierOuvert = True
    try:
        fichierErr = open(os.path.join(dossier,"test.err"))
    except:
        fichierOuvert = False

    if fichierOuvert:
        lignes = fichierErr.readlines()
        i = 0
        termine = False
        while i < len(lignes) and not termine:
            ligneSansEspaces = "".join(lignes[i].split())    # enlève les espaces de la ligne
            if ligneSansEspaces[0:5] == "test=":
                if ligneSansEspaces[5:7] == "ok":
                    etat = TEST_OK
                else:
                    etat = TEST_ERREUR
                termine = True
            i = i + 1
    global testOK
    testOK.append(etat)

def ecrisGlobal(dossier, fichier):
    global indTest
    global nbrEcr
    etatTest = testOK[indTest]
    indTest = indTest + 1
    if etatTest != TEST_INVALIDE:
        if nbrEcr % 50 == 0:
            fichier.write("<tr>")
        if etatTest == TEST_OK:
            fichier.write("<td bgcolor=\"#00FF00\" width=\"15\" valign=center align=center>\n")
            fichier.write("<a href=#S_%s>&nbsp;&nbsp;&nbsp;</a></font>\n" % dossier)
        elif etatTest == TEST_ERREUR:
            fichier.write("<td bgcolor=\"#FF0000\" width=\"15\" valign=center align=center>\n")
            fichier.write("<a href=#S_%s>&nbsp;&nbsp;&nbsp;</a></td>\n" % dossier)
        nbrEcr = nbrEcr + 1
    if nbrEcr % 50 == 0 or indTest >= len(testOK):
        fichier.write("</tr>")

def ecrisSommaire(dossier, fichier):
    global indTest
    etatTest = testOK[indTest]
    indTest = indTest + 1
    if etatTest != TEST_INVALIDE:
        fichier.write("<tr>")
        if etatTest == TEST_OK:
            fichier.write("<td bgcolor=\"#00FF00\" width=\"5%%\" valign=center align=center>\n")
            fichier.write("<a name=\"S_%s\"><a href=#T_%s>\n" %(dossier, dossier))
            fichier.write("<strong>ok</strong></a></td>\n")
        elif etatTest == TEST_ERREUR:
            fichier.write("<td bgcolor=\"#FF0000\" width=\"5%\" valign=center align=center>\n")
            fichier.write("<a name=\"S_%s\"><a href=#T_%s>\n" %(dossier, dossier))
            fichier.write("<strong>err</strong></a></td>\n")

        statInfo = os.stat(os.path.join(dossier,"test.err"))
        temps = time.ctime(statInfo.st_mtime)
        fichier.write("<td width=\"20%%\">%s</td>\n" % temps)
        fichier.write("<td><strong>\n")
        fichier.write("<a href=%s>%s\n" % (dossier, dossier))
        #fichier.write("<a href=/resultats_bruts_test/dispersim/%s>%s\n" % (dossier, dossier))
        fichier.write("</a></strong></td></tr>\n")

def ecrisTests(dossier, fichier):
    global indTest
    etatTest = testOK[indTest]
    indTest = indTest + 1
    if etatTest != TEST_INVALIDE:
        fichierErr = open(os.path.join(dossier, 'test.err'))

        fichier.write("<hr>")
        fichier.write("<h4><a name=\"T_%s\">\n" % dossier)

        if etatTest == TEST_OK:
            fichier.write("<font color=\"#00FF00\">%s</font>\n" % dossier)
        if etatTest == TEST_ERREUR:
            fichier.write("<font color=\"#FF0000\">%s</font>\n" % dossier)
        fichier.write("</a></h4>\n")

        fichier.write("<pre>\n")
        lignesErr = fichierErr.readlines()
        for ligne in lignesErr:
            ligneMin = ligne.lower()
            if ligneMin[0:3] == "ok:":
                fichier.write("<font color=\"#00FF00\">OK:</font>" + ligneMin[3:len(ligneMin)])
            elif ligneMin[0:7] == "erreur:":
                fichier.write("<font color=\"#FF0000\">ERREUR:</font>" + ligneMin[7:len(ligneMin)])
            elif ligneMin[0:5] == "info:":
                fichier.write("<font color=\"#0000FF\">INFO:</font>" + ligneMin[5:len(ligneMin)])
            elif ligneMin[0:5] == "test=":
                if etatTest == TEST_OK:
                    fichier.write("<font color=\"#00FF00\">" + ligneMin[0:-1] + "</font>\n")
                else:
                    fichier.write("<font color=\"#FF0000\">" + ligneMin[0:-1] + "</font>\n")
            else:
                fichier.write(ligneMin)
        fichier.write("</pre>\n")

def main():
    fichier = open("test_dispersim.htm", 'w')
    dossier = os.getcwd()
    boucleRecursifSurFichiers(dossier, fichier, compileStatut);

    fichier.write("<html>\n")
    fichier.write("<!-- Ce fichier est généré automatiquement -->\n")
    fichier.write("<!-- Tout changement sera donc perdu       -->\n")
    fichier.write("<head>\n")
    fichier.write("<title>Compilation des tests</title>\n")
    fichier.write("</head>\n")
    fichier.write("<body>\n")

    fichier.write("<h3>Dispersim - Compilation des tests<br>\n")
    temps = time.time()
    fichier.write(time.ctime(temps) + "</h3>\n")
    fichier.write("<hr>\n")

    global indTest
    indTest = 0
    fichier.write("<table cellspacing=2>\n")
    boucleRecursifSurFichiers(dossier, fichier, ecrisGlobal)
    fichier.write("</table>\n")
    fichier.write("<hr>\n")

    indTest = 0;
    fichier.write("<table cellspacing=4 width=\"99%%\">\n")
    boucleRecursifSurFichiers(dossier, fichier, ecrisSommaire)
    fichier.write("</table>\n")

    indTest = 0;
    boucleRecursifSurFichiers(dossier, fichier, ecrisTests)

    fichier.write("</body>\n")
    fichier.write("</html>\n")

if __name__ == '__main__':
    main()
