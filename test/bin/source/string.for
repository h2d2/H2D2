C=============================================================================
C
C=============================================================================
      SUBROUTINE TRIMSTR(STR)
      CHARACTER*(*) STR
C-----------------------------------------------------------------------------

C------- LENGTH
      ILLIN=LENSTR(STR)

C-------  REMOVE LEADING BLANKS
      I = 0
100   CONTINUE
         I = I + 1
      IF ((STR(I:I) .EQ. ' ') .AND. (I .LT. ILLIN)) GOTO 100
      STR = STR(I:ILLIN)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      FUNCTION INSTR(STR, C)
      CHARACTER*(*) STR
      CHARACTER C
C-----------------------------------------------------------------------------

      INSTR = -1
      DO I=1,LEN(STR)
         IF (STR(I:I) .EQ. C) THEN
            INSTR = I
            GOTO 100
         ENDIF
      ENDDO
100   CONTINUE

      RETURN
      END

C=============================================================================
C
C=============================================================================
      FUNCTION LENSTR(STR)
      CHARACTER*(*) STR
C-----------------------------------------------------------------------------

      DO I=LEN(STR),1,-1
         IF (STR(I:I) .NE. ' ') GOTO 100
      ENDDO
100   LENSTR = I

      RETURN
      END

C=============================================================================
C     ALL UPPERCASE EXCEPT BETWEEN '
C=============================================================================
      SUBROUTINE UCASESTR(STR)
      CHARACTER*(*) STR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      ITRUP = .TRUE.
      DO I=1,LENSTR(STR)
         IF (STR(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STR(I:I) .GE. 'a') .AND. (STR(I:I) .LE. 'z')) THEN
            IF (ITRUP) STR(I:I) = CHAR(ICHAR(STR(I:I))-32)
         ENDIF
      ENDDO

      RETURN
      END

C=============================================================================
C     ALL LOWERCASE EXCEPT BETWEEN '
C=============================================================================
      SUBROUTINE LCASESTR(STR)
      CHARACTER*(*) STR
      LOGICAL ITRUP
C-----------------------------------------------------------------------

      ITRUP = .TRUE.
      DO I=1,LENSTR(STR)
         IF (STR(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STR(I:I) .GE. 'A') .AND. (STR(I:I) .LE. 'Z')) THEN
            IF (ITRUP) STR(I:I) = CHAR(ICHAR(STR(I:I))+32)
         ENDIF
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE SBSTSTR(STR, C1, C2)
      CHARACTER*(*) STR
      CHARACTER     C1
      CHARACTER     C2
      LOGICAL ITRUP
C-----------------------------------------------------------------------------

C-------  REMOVE LEADING BLANKS
      ITRUP = .TRUE.
      DO I=1,LENSTR(STR)
         IF (STR(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF (STR(I:I) .EQ. C1) THEN
            IF (ITRUP) STR(I:I) = C2
         ENDIF
      ENDDO

      RETURN
      END
