C=============================================================================
C
C=============================================================================
      SUBROUTINE GENCL_ML(CONDLIM, NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)

      CHARACTER*(*) CONDLIM
C-----------------------------------------------------------------------------

      IF (CONDLIM .EQ. 'D_D_D_D') THEN
         CALL CL_D_D_D_D_ML(NNX, NNY, NNT, NDLN, KDIMP)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:CL=', CONDLIM
         STOP
      ENDIF

C---    METS TOUS LES DDL EGAUX
      DO IN=1,NNT
         DO ID=2,NDLN
            KDIMP(ID,IN) = KDIMP(1,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       D
C     D   D
C       D
C=============================================================================
      SUBROUTINE CL_D_D_D_D_ML(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      ICOUNT = 0
      DO IY=1,NNY
         IN = IY
         ICOUNT = ICOUNT + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = ICOUNT
         ENDDO
      ENDDO
      DO IX=2,(NNX-1)
         IN = (IX-1)*NNY + 1
         ICOUNT = ICOUNT + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = ICOUNT
         ENDDO
         IN = (IX-1)*NNY + NNY
         ICOUNT = ICOUNT + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = ICOUNT
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         ICOUNT = ICOUNT + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = ICOUNT
         ENDDO
      ENDDO

      RETURN
      END