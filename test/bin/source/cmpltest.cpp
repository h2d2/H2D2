#include <io.h>
#include <dos.h>
#include <direct.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <vector>

enum TTEtatTest {TEST_INVALIDE = 0, TEST_OK, TEST_ERREUR};
std::vector<TTEtatTest> testOK;
int                     indTest = 0;

//************************************************************************
//
//************************************************************************

void boucleRecusifSurFichiers(std::ostream& os, void (* fnct)(std::ostream&))
{
   struct _finddata_t ffblk;
   intptr_t hndl = _findfirst("*.*", &ffblk);
   int done = (hndl == -1);
   while (!done)
   {
      if (ffblk.name[0] != '.' && _chdir(ffblk.name) == 0)
      {
         fnct(os);
         boucleRecusifSurFichiers(os, fnct);
         _chdir("..");
      }
      done = _findnext(hndl, &ffblk);
   }
  // return(0);
}

//************************************************************************
//
//************************************************************************
void compileStatut(std::ostream&)
{
   std::ifstream is("test.err");

   TTEtatTest etat = TEST_INVALIDE;

   char ligne[1024];
   bool termine = false;
   while (is && ! termine)
   {
      is.getline(ligne, 1024);

      char *c1P = ligne, *c2P = ligne;
      while (*c1P)
      {
         if (*c1P != ' ') *c2P++ = *c1P;
         c1P++;
      }
      *c2P = 0x0;
      strlwr(ligne);

      if (strncmp(ligne, "test=", 5) == 0)
      {
         if ((strncmp(ligne+5, "ok", 3) == 0))
            etat = TEST_OK;
         else
            etat = TEST_ERREUR;
         termine = true;
      }
   }

   testOK.push_back(etat);

   return;
}

//************************************************************************
//
//************************************************************************
void ecrisGlobal(std::ostream& os)
{
   static int nbrEcr = 0;

   TTEtatTest etatTest = testOK[indTest++];
   if (etatTest != TEST_INVALIDE)
   {
      char* repCourantP;
      char buf[_MAX_PATH];
      char * test;
      test =_getdcwd(0, buf, _MAX_PATH);
      if (test==NULL) etatTest = TEST_INVALIDE;

      repCourantP = strstr(buf, "\\");
//      repCourantP = strstr(repCourantP+1, "\\");
      repCourantP++;

      if (nbrEcr % 50 == 0)
         os << "<tr>" << std::endl;

      if (etatTest == TEST_OK)
      {
         os << "<td bgcolor=\"#00FF00\" width=\"15\" valign=center align=center>"
            << "<a href=#S_" << repCourantP << ">"
            << "&nbsp;&nbsp;&nbsp;"
            << "</a>"
            << "</font>";
      }
      else if (etatTest == TEST_ERREUR)
      {
         os << "<td bgcolor=\"#FF0000\" width=\"15\" valign=center align=center>"
            << "<a href=#S_" << repCourantP << ">"
            << "&nbsp;&nbsp;&nbsp;"
            << "</a>"
            << "</td>";
      }
      os << std::endl;
      ++nbrEcr;
   }
   if (nbrEcr % 50 == 0 || indTest >= testOK.size())
      os << "</tr>" << std::endl;

   return;
}

//************************************************************************
//
//************************************************************************
void ecrisSommaire(std::ostream& os)
{
   char* repCourantP;

   TTEtatTest etatTest = testOK[indTest++];
   if (etatTest == TEST_INVALIDE) return;
   char * test;
   char buf[_MAX_PATH];
   test =_getdcwd(0, buf, _MAX_PATH);
   if (test==NULL) etatTest = TEST_INVALIDE;
   repCourantP = strstr(buf, "\\");
//   repCourantP = strstr(repCourantP+1, "\\");
   repCourantP++;

   os << "<tr>";
   if (etatTest == TEST_OK)
   {
      os << "<td bgcolor=\"#00FF00\" width=\"5%\" valign=center align=center>"
         << "<a name=\"S_" << repCourantP << "\">"
         << "<a href=#T_"  << repCourantP << ">"
         << "<strong>ok</strong>"
         << "</a>"
         << "</td>";
   }
   else if (etatTest == TEST_ERREUR)
   {
      os << "<td bgcolor=\"#FF0000\" width=\"5%\" valign=center align=center>"
         << "<a name=\"S_" << repCourantP << "\">"
         << "<a href=#T_"  << repCourantP << ">"
         << "<strong>err</strong>"
         << "</a>"
         << "</td>";
   }

   struct stat statbuf;
   stat("test.err", &statbuf);
   char ligne[1024];
   strncpy(ligne, ctime(&statbuf.st_mtime), 24);
   ligne[24] = 0x0;
   os << "<td width=\"20%\">" << ligne << "</td>";

   os << "<td><strong>";
   os << "<a href=/" << repCourantP << ">";
   os << repCourantP;
   os << "</a>";
   os << "</strong></td>";
   os << "</tr>" << std::endl;

   return;
}

//************************************************************************
//
//************************************************************************
void ecrisTests(std::ostream& os)
{
   TTEtatTest etatTest = testOK[indTest++];
   if (etatTest == TEST_INVALIDE) return;

   std::ifstream is("test.err");
   if (!is) return;

   char buf[_MAX_PATH];
   char* repCourantP;
   char * test;
   test =_getdcwd(0, buf, _MAX_PATH);
   if (test==NULL) etatTest = TEST_INVALIDE;
   repCourantP = strstr(buf, "\\");
//   repCourantP = strstr(repCourantP+1, "\\");
   repCourantP++;

   os << "<hr>" << std::endl;
   os << "<h4><a name=\"T_" << repCourantP << "\">";
   if (etatTest == TEST_OK)
   {
      os << "<font color=\"#00FF00\">" << repCourantP << "</font>";
   }
   else if (etatTest == TEST_ERREUR)
   {
      os << "<font color=\"#FF0000\">" << repCourantP << "</font>";
   }
   os << "</a></h4>";

   char ligne[1024];
   os << "<pre>"  << std::endl;
   while (is)
   {
      is.getline(ligne, 1024);
      strlwr(ligne);
      os << ligne << std::endl;
   }
   os << "</pre>" << std::endl;

}

//************************************************************************
//
//************************************************************************
int main()
{

   std::ofstream os("test_h2d2.htm");
   if (os)
   {
      boucleRecusifSurFichiers(os, compileStatut);

      os << "<html>" << std::endl;
      os << "<!-- Ce fichier est généré automatiquement -->" << std::endl;
      os << "<!-- Tout changement sera donc perdu       -->" << std::endl;
      os << "<head>" << std::endl;
      os << "<title>Compilation des tests</title>" << std::endl;
      os << "</head>" << std::endl;
      os << "<body>" << std::endl;

      time_t timet = time(NULL);
      os << "<h3>H2D2 - Compilation des tests<br>";
      os << ctime(&timet) << "</h3>";
      os << "<hr>" << std::endl;

      indTest = 0;
      os << "<table cellspacing=2>" << std::endl;
      boucleRecusifSurFichiers(os, ecrisGlobal);
      os << "</table>" << std::endl;
      os << "<hr>" << std::endl;

      indTest = 0;
      os << "<table cellspacing=4 width=\"99%\">" << std::endl;
      boucleRecusifSurFichiers(os, ecrisSommaire);
      os << "</table>" << std::endl;

      indTest = 0;
      boucleRecusifSurFichiers(os, ecrisTests);

      os << "</body>" << std::endl;
      os << "</html>" << std::endl;
   }

   return 0;
}
