C=============================================================================
C     Génération des Conditions limites pour SV2D
C=============================================================================
      SUBROUTINE GENCL_SV2D(CONDLIM, NNX, NNY, NNT, NDLN,
     &                               VDLG, KDIMP, NVCL, VCL)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
      DIMENSION VDLG (NDLN, 1)
      DIMENSION VCL  (4, 1)
      DIMENSION NVCL (143)
      CHARACTER*(*) CONDLIM
      INTEGER KCOD(4)
      CHARACTER*256 BUF
C-----------------------------------------------------------------------------

      BUF = CONDLIM(1:LENSTR(CONDLIM))
      CALL TRIMSTR(BUF)
      CALL SBSTSTR(BUF, '_', ' ')

      READ(BUF,*,ERR=999) (KCOD(IC),IC=1,4)                                     ! Les codes code1_code2_code3_code4 sont assignés dans KCOD(IC)
      NTC = 4

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0                                                    ! La matrice des conditions limites et des codes est mise à zéro
         ENDDO
      ENDDO

      DO ID=1,NTC
         IC = ID
         CALL CL_CODE(NNX, NNY, NNT, NDLN, KDIMP, IC, KCOD)                     ! La matrice KDIMP est remplie avec le # de limite et le COD
      ENDDO

C---    METS TOUS LES DDL EGAUX
      DO IN=1,NNT
         DO ID=2,NDLN
            KDIMP(ID,IN) = KDIMP(1,IN)
         ENDDO
      ENDDO

      CALL VALCL(NNT, NDLN, KDIMP, VDLG, NVCL, VCL)                                 ! Crée les valeurs de condition aux limites
      GOTO 10

999   WRITE(*,*) 'ERREUR: CHAQUE CODE DE CONDITION DOIT ETRE ',
     &           'UN NOMBRE ENTIER:', BUF
      STOP

10    CONTINUE
      RETURN
      END

C=============================================================================
C
C       CL_CODE: Assigne un code et le numéro de la limite sur tous les
C                noeuds de la limite actuelle (IC)
C
C          4
C        1   3     Convention de numéro des limites
C          2
C
C=============================================================================
      SUBROUTINE CL_CODE(NNX, NNY, NNT, NDLN, KDIMP, IC, KCOD)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
      INTEGER KCOD(4)
C-----------------------------------------------------------------------------

      IF (IC .EQ. 1) THEN
         DO IY=1,NNY
            IN = IY
            KDIMP(1, IN) = 1000+KCOD(IC)
         ENDDO
      ELSEIF (IC .EQ. 2) THEN
         DO IX=2,(NNX-1)
            IN = (IX-1)*NNY + 1
            KDIMP(1, IN) = 2000+KCOD(IC)
         ENDDO
      ELSEIF (IC .EQ. 4) THEN
         DO IX=2,(NNX-1)
            IN = (IX-1)*NNY + NNY
            KDIMP(1, IN) = 4000+KCOD(IC)
         ENDDO
      ELSEIF (IC .EQ. 3) THEN
         DO IY=1,NNY
            IN = (NNX-1)*NNY + IY
            KDIMP(1, IN) = 3000+KCOD(IC)
         ENDDO
      ELSE
         WRITE(*,*) 'ERREUR SUR LA LIMITE:', KCOD(IC)
         STOP
      ENDIF
      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE VALCL(NNT, NDLN, KDIMP, VDLG, NVCL, VCL)

      INCLUDE 'gentest.fi'

      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)
      DIMENSION VCL (4,1)
      DIMENSION NVCL(143)
      DIMENSION Q(4)

C-----------------------------------------------------------------------------
      CODEMAX = 143
      DO IN=1,CODEMAX
         NVCL(IN) = 0
      ENDDO

      DO IN=1,NNT                        ! Chaque noeud a ses valeurs de condition
         DO ID=1,4                       ! Il y a un maximum de 4 valeurs de condition (Ex: Code 142: q0, z0, a, b)
            VCL(ID,IN) = 0.0D0           ! Rend la matrice des valeurs de condition nulle
         ENDDO
      ENDDO

100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
              CODE = MOD(IC,1000)
              KDIMP(1, IN) = -KDIMP(1,IN)

               IF (CODE .EQ. 0) THEN
                  CONTINUE
               ELSEIF (CODE .EQ. 1) THEN
                  VCL(1,IN)   = VDLG(1,IN)            ! qx
                  VCL(2,IN)   = VDLG(2,IN)            ! qy
                  VCL(3,IN)   = VDLG(3,IN)            ! h
                  NVCL(CODE)  = 3                     ! Nombre de Valeurs de C.L. total
               ELSEIF (CODE .EQ. 2) THEN
                  VCL(1,IN)   = VDLG(3,IN)            ! h
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 3) THEN
                  VCL(1,IN)   = VDLG(1,IN)            ! qx
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 4) THEN
                  VCL(1,IN)   = VDLG(2,IN)            ! qy
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 10) THEN
                  Q(1)        = -1.5D0                ! Q (m3/s)
                  Q(2)        = 1.5D0                 ! h fixe (m)
                  VCL(1,IN)   = Q(1)
                  VCL(2,IN)   = Q(2)
                  NVCL(CODE)  = 2
               ELSEIF (CODE .EQ. 11) THEN
                  Q(1)        = -1.5D0
                  VCL(1,IN)   = Q(1)                  ! Q (m3/s)
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 12) THEN
                  Q(1)        = 1.5D0               ! Integrale de Psi.q.n dGamma
                  VCL(1,IN)   = Q(1)
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 101) THEN
                  VCL(1,IN)   = VDLG(1,IN)            ! qx
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 102) THEN
                  VCL(1,IN) = VDLG(2,IN)              ! qy
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 103) THEN
                  VCL(1,IN) = VDLG(3,IN)              ! h
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 104) THEN
                  VCL(1,IN) = VDLG(1,IN)              ! qx
                  VCL(2,IN) = VDLG(2,IN)              ! qy
                  VCL(3,IN) = VDLG(3,IN)              ! h
                  NVCL(CODE)  = 3
               ELSEIF (CODE .EQ. 111) THEN
                  VCL(1,IN) = VDLG(1,IN)              ! qx
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 112) THEN
                  VCL(1,IN) = VDLG(2,IN)              ! qy
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 113) THEN
                  VCL(1,IN) = VDLG(3,IN)              ! h
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 121) THEN
                  Q(1) = 1.5D0                        ! Integrale de Psi.q.n dGamma
                  VCL(1,IN) = Q(1)
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 122) THEN
                  Q(1) = -1.5D0                       ! Q (m3/s)
                  Q(2) = 1.5D0                        ! h fixe (m)
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  NVCL(CODE)  = 2
               ELSEIF (CODE .EQ. 123) THEN
                  Q(1) = -1.5D0                        ! Q (m3/s)
                  Q(2) = 1.5D0                         ! h fixe (m)
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  NVCL(CODE)  = 2
               ELSEIF (CODE .EQ. 124) THEN
                  Q(1) = -1.5D0                        ! Q (m3/s)
                  Q(2) = 1.5D0                         ! h fixe (m)
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  NVCL(CODE)  = 2
                  ELSEIF (CODE .EQ. 125) THEN
                  Q(1) = -1.5D0
                  VCL(1,IN) = Q(1)                    ! Q (m3/s)
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 133) THEN
                  Q(1) = 9999999999999                ! Nom de la limite source
                  Q(2) = -3.0D0                       ! a0 (m)
                  Q(3) = 3.0D0                        ! a1
                  VCL(2,IN) = Q(2)
                  VCL(3,IN) = Q(3)
                  NVCL(CODE)  = 3
               ELSEIF (CODE .EQ. 138) THEN
                  Q(1) = 9999999999999                ! Nom de la limite source
                  Q(2) = -7.0D0                       ! a0 (m3/s)
                  Q(3) = 2.0D0                        ! a1
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  VCL(3,IN) = Q(3)
                  NVCL(CODE)  = 3
               ELSEIF (CODE .EQ. 141) THEN
                  Q(1) = 0.0                          ! Aucune entrée nécessaire
                  VCL(1,IN) = Q(1)
                  NVCL(CODE)  = 1
               ELSEIF (CODE .EQ. 142) THEN
                  Q(1) = -0.5D0                                      ! q0 (m2/s)
                  Q(2) = 0.5D0                                       ! z0 (m)
                  Q(3) = -1.0D0                                      ! a (s^(-1)*m^(-(b-2))
                  Q(4) = 1.0D0                                       ! b
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  VCL(3,IN) = Q(3)
                  VCL(4,IN) = Q(4)
                  NVCL(CODE)  = 4
               ELSEIF (CODE .EQ. 143) THEN
                  Q(99) = 9.99D3                                     ! Limite source ($Source$)
                  Q(1) = 1.0D0                                       ! Aire innondee ponceau Ap (m^2)
                  Q(2) = 1.0D0                                       ! Rayon hydraulique Rh (m)
                  Q(3) = 1.0D0                                       ! Coefficient de perte à l'entrée Ke
                  Q(4) = 3.0D-2                                      ! Manning n (s/m^(1/3))
                  Q(5) = 1.0D0                                       ! Longueur ponceau Lp (m)
                  VCL(1,IN) = Q(1)
                  VCL(2,IN) = Q(2)
                  VCL(3,IN) = Q(3)
                  VCL(4,IN) = Q(4)
                  VCL(5,IN) = Q(5)
                  NVCL(CODE)  = 5
               ELSE
                  WRITE(*,*) 'CODE DE CONDITION INCONNUE:', CODE
                  STOP
               ENDIF
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE GENCL_CD2D(CONDLIM, NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
      CHARACTER*(*) CONDLIM
C-----------------------------------------------------------------------------

      IF (CONDLIM .EQ. 'D_D_D_D') THEN
         CALL CL_D_D_D_D(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'D_N_D_N') THEN
         CALL CL_D_N_D_N(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'D_N_O_N') THEN
         CALL CL_D_N_O_N(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'N_D_N_D') THEN
         CALL CL_N_D_N_D(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'D_O_O_O') THEN
         CALL CL_D_O_O_O(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'C_O_D_O') THEN
         CALL CL_C_O_D_O(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'D_D_D_D_D') THEN
         CALL CL_D_D_D_D_D(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'NC_NC_NC_NC') THEN
         CALL CL_NC_NC_NC_NC(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'NC_NC_NC_NC_D') THEN
         CALL CL_NC_NC_NC_NC_D(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'OC_OC_OC_OC') THEN
         CALL CL_OC_OC_OC_OC(NNX, NNY, NNT, NDLN, KDIMP)
      ELSEIF (CONDLIM .EQ. 'OC_OC_OC_OC_D') THEN
         CALL CL_OC_OC_OC_OC_D(NNX, NNY, NNT, NDLN, KDIMP)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:CL=', CONDLIM
         STOP
      ENDIF

C---    METS TOUS LES DDL EGAUX
      DO IN=1,NNT
         DO ID=2,NDLN
            KDIMP(ID,IN) = KDIMP(1,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       D
C     D   D
C       D
C=============================================================================
      SUBROUTINE CL_D_D_D_D(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1001
         ENDDO
      ENDDO
      DO IX=2,(NNX-1)
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2001
         ENDDO
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4001
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       N
C     D   D
C       N
C=============================================================================
      SUBROUTINE CL_D_N_D_N(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1001
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       N
C     D   O
C       N
C=============================================================================
      SUBROUTINE CL_D_N_O_N(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1001
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3003
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       D
C     N   N
C       D
C=============================================================================
      SUBROUTINE CL_N_D_N_D(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IX=1,NNX
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2001
         ENDDO
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       O
C     D   O
C       O
C=============================================================================
      SUBROUTINE CL_D_O_O_O(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1001
         ENDDO
      ENDDO
      DO IX=2,(NNX-1)
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2003
         ENDDO
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4003
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3003
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       O
C     C   D
C       O
C=============================================================================
      SUBROUTINE CL_C_O_D_O(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1002
         ENDDO
      ENDDO
      DO IX=2,(NNX-1)
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2003
         ENDDO
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4003
         ENDDO
      ENDDO
      DO IY=1,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C       D
C
C    D     D
C       D
C       D
C=============================================================================
      SUBROUTINE CL_D_D_D_D_D(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      CALL CL_D_D_D_D(NNX, NNY, NNT, NDLN, KDIMP)

      NNX2 = (NNX+1)/2
      NNY2 = (NNY+1)/2

      DO IY=1,NNY2
         IN = (NNX2-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 5001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C         N-C
C     N-C     N-C
C         N-C
C=============================================================================
      SUBROUTINE CL_NC_NC_NC_NC(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO

      NNX2 = (NNX+1)/2
      NNY2 = (NNY+1)/2

      DO IY=1,NNY2
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1002
         ENDDO
      ENDDO
      DO IX=1,NNX2
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2002
         ENDDO
      ENDDO
      DO IX=NNX2, NNX
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4002
         ENDDO
      ENDDO
      DO IY=NNY2,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3002
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C         N-C
C     N-C     N-C
C          D
C         N-C
C=============================================================================
      SUBROUTINE CL_NC_NC_NC_NC_D(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      CALL CL_NC_NC_NC_NC(NNX, NNY, NNT, NDLN, KDIMP)

      NNX2 = (NNX+1)/2
      NNY2 = (NNY+1)/2

      DO IY=2,NNY2
         IN = (NNX2-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 5001
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C         O-C
C     O-C     O-C
C         O-C
C=============================================================================
      SUBROUTINE CL_OC_OC_OC_OC(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         DO ID=1,NDLN
            KDIMP(ID,IN) = 0
         ENDDO
      ENDDO

      NNX2 = (NNX+1)/2
      NNY2 = (NNY+1)/2

      DO IY=1,NNY2
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 1002
         ENDDO
      ENDDO
      DO IX=1,NNX2
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 2002
         ENDDO
      ENDDO
      DO IX=NNX2, NNX
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 4002
         ENDDO
      ENDDO
      DO IY=NNY2,NNY
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 3002
         ENDDO
      ENDDO

      DO IY=(NNY2+1),(NNY-1)
         IN = IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 11003
         ENDDO
      ENDDO
      DO IX=2,(NNX2-1)
         IN = (IX-1)*NNY + 1
         DO ID=1,NDLN
            KDIMP(ID, IN) = 12003
         ENDDO
      ENDDO
      DO IX=(NNX2+1),(NNX-1)
         IN = (IX-1)*NNY + NNY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 14003
         ENDDO
      ENDDO
      DO IY=2,(NNY2-1)
         IN = (NNX-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 13003
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C         O-C
C     O-C     O-C
C          D
C         O-C
C=============================================================================
      SUBROUTINE CL_OC_OC_OC_OC_D(NNX, NNY, NNT, NDLN, KDIMP)

      INCLUDE 'gentest.fi'

      DIMENSION KDIMP(NDLN, 1)
C-----------------------------------------------------------------------------

      CALL CL_OC_OC_OC_OC(NNX, NNY, NNT, NDLN, KDIMP)

      NNX2 = (NNX+1)/2
      NNY2 = (NNY+1)/2

      DO IY=2,NNY2
         IN = (NNX2-1)*NNY + IY
         DO ID=1,NDLN
            KDIMP(ID, IN) = 5001
         ENDDO
      ENDDO

      RETURN
      END
