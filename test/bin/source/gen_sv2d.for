C=============================================================================
C     h2d2 St-Venant
C=============================================================================
      PROGRAM GEN_SV2D

      INCLUDE 'gentest.fi'

      PARAMETER (NDIM =2)
      PARAMETER (NPRNT=6)
      PARAMETER (NPRNH=5)
      PARAMETER (NDLMAX=5)
      PARAMETER (NDIMV=150*150)
      PARAMETER (NCLMAX=4)

C      DIMENSION     VCORG(NDIM*NDIMV)
C      DIMENSION     VPRNT(NPRNT*NDIMV)
C      DIMENSION     VPRNH(NPRNH*NDIMV)
C      DIMENSION     VSOLR(NDLMAX*NDIMV)
C      DIMENSION     VDLG (NDLMAX*NDIMV)
C      DIMENSION     KDIMP(NDLMAX*NDIMV)
C      DIMENSION     KNG  (6*NDIMV)
      !! On alloue plutôt dynamiquement
      ALLOCATABLE :: VCORG(:)
      ALLOCATABLE :: VPRNT(:)
      ALLOCATABLE :: VPRNH(:)
      ALLOCATABLE :: VSOLR(:)
      ALLOCATABLE :: VDLG (:)
      ALLOCATABLE :: KDIMP(:)
      ALLOCATABLE :: NVCL (:)
      ALLOCATABLE :: VCL  (:)
      ALLOCATABLE :: KNG  (:)
      CHARACTER*256 NOMBASE
      CHARACTER*256 MNT
      CHARACTER*256 ECOUL
      CHARACTER*256 CONDLIM
      CHARACTER*4   TYPEL
      LOGICAL       LNODRND
      INTEGER       I

      !! Les variables sont placées par défaut sur le stack
      !! De trop grosses tables font sauter le stack
      !! On crée donc un common bidon pour contourner le pblm.
C      COMMON /DUMMY/ VCORG, VPRNT, VPRNH, VSOLR, VDLG, KDIMP, KNG
C-----------------------------------------------------------------------------

C-------  LIS LES DONNEES
      READ(5,*) NOMBASE
      READ(5,*) NELX
      READ(5,*) NELY
      READ(5,*) XORI
      READ(5,*) YORI
      READ(5,*) DIMX
      READ(5,*) DIMY
      READ(5,*) NNEL
      READ(5,*) TYPEL
      READ(5,*) MNT
      READ(5,*) ECOUL
      READ(5,*) CONDLIM


C-------  PRE-TRAITE
      LNODRND = .FALSE.
      CALL TRIMSTR(TYPEL)
      CALL UCASESTR(TYPEL)
      IF (TYPEL(1:1) .EQ. '~') THEN
         LNODRND = .TRUE.
         TYPEL(1:1) = ' '
         CALL TRIMSTR(TYPEL)
      ENDIF
      IF (TYPEL(1:1) .EQ. '/') THEN
         ITYPEL = IEL$SLASH
      ELSEIF (TYPEL(1:1) .EQ. '\') THEN
         ITYPEL = IEL$SLINV
      ELSEIF (TYPEL(1:1) .EQ. 'v') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. 'V') THEN
         ITYPEL = IEL$V
      ELSEIF (TYPEL(1:1) .EQ. '^') THEN
         ITYPEL = IEL$VINV
      ELSEIF (TYPEL(1:1) .EQ. '>') THEN
         ITYPEL = IEL$GT
      ELSEIF (TYPEL(1:1) .EQ. '<') THEN
         ITYPEL = IEL$LT
      ELSEIF (TYPEL(1:1) .EQ. 'X') THEN
         ITYPEL = IEL$X
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:TYPEL=', TYPEL
         STOP
      ENDIF
      IF (NNEL .EQ. 3) THEN
         NNX = NELX + 1
         NNY = NELY + 1
      ELSEIF(NNEL .EQ. 6) THEN
         NNX = 2*NELX + 1
         NNY = 2*NELY + 1
      ELSE
         WRITE(*,*) 'NOMBRE DE NOEUDS PAR ELEMENT INVALIDE:', NNEL
         STOP
      ENDIF
      NNT = NNX*NNY
      CALL TRIMSTR(NOMBASE)
      CALL LCASESTR(NOMBASE)
      CALL TRIMSTR(MNT)
      CALL UCASESTR(MNT)
      CALL TRIMSTR(ECOUL)
      CALL UCASESTR(ECOUL)

C-------  ALLOUE LES TABLES
      ALLOCATE(VCORG(NDIM*NNT))
      ALLOCATE(VPRNT(NPRNT*NNT))
      ALLOCATE(VPRNH(NPRNH*NNT))
      ALLOCATE(VSOLR(NDLMAX*NNT))
      ALLOCATE(VDLG (NDLMAX*NNT))
      ALLOCATE(KDIMP(NDLMAX*NNT))
      ALLOCATE(NVCL(143))
      ALLOCATE(VCL (4*NNT))
      ALLOCATE(KNG  (6*NNT))

C-------  GENERE LE MAILLAGE
      CALL GENCOOR(LNODRND, XORI, YORI, DIMX, DIMY, NNX, NNY, VCORG)
      CALL GENELEM(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

C-------  GENERE LE MODELE NUMERIQUE DE TERRAIN
      CALL GENMNT  (MNT(1:LENSTR(MNT)),
     &              NDIM, NNX, NNY, NNT, VCORG,
     &              NNEL, NELT, KNG,
     &              NPRNT, VPRNT)

C-------  GENERE L'HYDRODYNAMIQUE
      NDLN = 3
      CALL GENHYDRO(ECOUL(1:LENSTR(ECOUL)),
     &              NDIM, NNX, NNY, NNT, VCORG,
     &              NNEL, NELT, KNG,
     &              NPRNH, VPRNH,
     &              NDLN,  VSOLR, VPRNT)

C-------  GENERE LA SOLUTION
      CALL GENSOL_SV2D  (NNT,
     &              NPRNT, VPRNT,
     &              NPRNH, VPRNH,
     &              NDLN,  VDLG)

C-------  GENERE LES CONDITIONS LIMITES
      CALL GENCL_SV2D   (CONDLIM(1:LENSTR(CONDLIM)),
     &                  NNX, NNY, NNT, NDLN, VDLG,
     &                  KDIMP, NVCL, VCL)

C-------  ECRIS LES RESULTATS
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.cor',
     &              NNT,
     &              NDIM,
     &              VCORG)
      CALL ECRISELE(NOMBASE(1:LENSTR(NOMBASE))//'.ele',
     &              NELT,
     &              NNEL,
     &              KNG)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.pnt',
     &              NNT,
     &              NPRNT,
     &              VPRNT)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.slr',
     &              NNT,
     &              NDLN,
     &              VSOLR)
      CALL ECRISPRN(NOMBASE(1:LENSTR(NOMBASE))//'.ini',
     &              NNT,
     &              NDLN,
     &              VDLG)
      CALL ECRISCND(NOMBASE(1:LENSTR(NOMBASE))//'.cnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP, NVCL, VCL)
      CALL ECRISBND(NOMBASE(1:LENSTR(NOMBASE))//'.bnd',
     &              NNT,
     &              NDLN,
     &              VDLG, KDIMP, VCL)

C-------  DESALLOUE LES TABLES
      DEALLOCATE(VCORG)
      DEALLOCATE(VPRNT)
      DEALLOCATE(VPRNH)
      DEALLOCATE(VSOLR)
      DEALLOCATE(VDLG)
      DEALLOCATE(KDIMP)
      DEALLOCATE(NVCL)
      DEALLOCATE(VCL)
      DEALLOCATE(KNG)
      WRITE(*,*) 'Fin du GENTEST.exe'

      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE GENSOL_SV2D(NNT,NPRNT,VPRNT,NPRNH,VPRNH,NDLN,VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VPRNT(NPRNT, 1)
      DIMENSION VPRNH(NPRNH, 1)
      DIMENSION VDLG(NDLN,1)
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         VDLG(1, IN) = VPRNH(3, IN)*VPRNH(1, IN)   ! qx
         VDLG(2, IN) = VPRNH(3, IN)*VPRNH(2, IN)   ! qy
         VDLG(3, IN) = VPRNH(3, IN)+VPRNT(1, IN)   ! h
      ENDDO

      RETURN
      END


C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISCND(NOMFIC, NNT, NDLN, VDLG, KDIMP, NVCL, VCL)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)
      DIMENSION VCL  (4, 1)
      DIMENSION NVCL (143)

      CHARACTER*256 NOMCL, TMPCL
      CHARACTER*32  FMT
      LOGICAL       WRITE_DONE

C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I9,',4, '(1X,1PE25.17E3), A)'

C-------- Compte le nombre de conditions

      NCND = 0

100   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               CODE = MOD(IC,1000)
               IF (CODE .NE. 0) THEN
                  WRITE(TMPCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$'
                  CALL TRIMSTR(TMPCL)
                  CALL SBSTSTR(TMPCL, ' ','_')
                  GOTO 109
               ELSE
                  KDIMP(1,IN) = -KDIMP(1,IN)
                  GOTO 100
               ENDIF
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199
         IF ((CODE .EQ. 3) .OR. (CODE .EQ. 4)
     & .OR. (CODE .EQ. 101) .OR. (CODE .EQ. 102)
     & .OR. (CODE .EQ. 111) .OR. (CODE .EQ. 112)
     & .OR. (CODE .EQ. 113) .OR. (CODE .EQ. 133)) THEN
            NCND = NCND+1                                 !ajout des codes doublés pour la condition 141
         ENDIF
C---        Flag tous les noeuds de cette condition
         NCND = NCND + 1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               IF (CODE .NE. 0) THEN
                  WRITE(NOMCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$'
                  CALL TRIMSTR(NOMCL)
                  CALL SBSTSTR(NOMCL, ' ','_')
               ELSE
               ENDIF
               IF (NOMCL .EQ. TMPCL) THEN
                  KDIMP(1,IN) = -KDIMP(1,IN)
               ENDIF
            ENDIF
         ENDDO
      GOTO 100
199   CONTINUE

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

C-------- Ecris l'entete
      WRITE(10, '(I9,1X,1PE25.17E3)') NCND, 0.0

200   CONTINUE
C---        Cherche la prochaine condition non traitée
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               CODE = MOD(IC,1000)
               IF (CODE .NE. 0) THEN
                  WRITE(TMPCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$'
                  CALL TRIMSTR(TMPCL)
                  CALL SBSTSTR(TMPCL, ' ','_')
                  GOTO 209
               ELSE
                  KDIMP(1,IN) = -KDIMP(1,IN)
                  GOTO 200
               ENDIF
            ENDIF
         ENDDO
209      CONTINUE
         IF (IC .LT. 0) GOTO 299

C---        Flag tous les noeuds de cette condition
C---        Ecris la condition
         WRITE_DONE = .FALSE.
         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               CODE = MOD(IC,1000)
               WRITE(NOMCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$'
               CALL TRIMSTR(NOMCL)
               CALL SBSTSTR(NOMCL, ' ','_')

               IF (NOMCL .EQ. TMPCL) THEN
                    KDIMP(1,IN) = -KDIMP(1,IN)
                  IF (.NOT. WRITE_DONE) THEN

                     IF (CODE .NE. 0) THEN
                        IF (CODE .EQ. 133) THEN
                           WRITE(10, '(A,I5,1X,A, 3(1X,1PE25.17E3))')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000),
     & '$________1__1.50000000000000000E+000__0.00000000000000000E+000
     $__0.00000000000000000E+000__0.00000000000000000E+000$',
     &                               (VCL(ID,IN),ID=2,NVCL(CODE))
                           WRITE(10, '(A,A,I5)')
     &                               NOMCL(1:LENSTR(NOMCL)),'B',
     &                               141
                                     WRITE_DONE = .TRUE.

                        ELSEIF (CODE .EQ. 138) THEN
                           WRITE(10, '(A,I5,1X,A)')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000),
     & '$________3__1.50000000000000000E+000__0.00000000000000000E+000
     &__1.50000000000000000E+000__0.00000000000000000E+000$'
                                     WRITE_DONE = .TRUE.
                         ELSEIF (CODE .EQ. 141) THEN
                            WRITE(10, '(A,I5)')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000)
                                     WRITE_DONE = .TRUE.
                         ELSEIF (CODE .EQ. 143) THEN
                           WRITE(10, '(A,I5,1X,A,5(1X,1PE25.17E3))')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000),
     &                               '$LIMSOURCE$',
     &                               (VCL(ID,IN),ID=1,NVCL(CODE))
                                     WRITE_DONE = .TRUE.
                         ELSEIF ((CODE .EQ. 3) .OR. (CODE .EQ. 4)
     &.OR. (CODE .EQ. 101) .OR. (CODE .EQ. 102) .OR. (CODE .EQ. 111)
     &.OR. (CODE .EQ. 112) .OR. (CODE .EQ. 113)) THEN
                            WRITE(10, '(A,A,I5)')
     &                               NOMCL(1:LENSTR(NOMCL)),'B',
     &                               141
                            WRITE(10, '(A,I5,4(1X,1PE25.17E3))')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000),
     &                               (VCL(ID,IN),ID=1,NVCL(CODE))
                                     WRITE_DONE = .TRUE.
                         ELSE
                           WRITE(10, '(A,I5,4(1X,1PE25.17E3))')
     &                               NOMCL(1:LENSTR(NOMCL)),
     &                               MOD(IC,1000),
     &                               (VCL(ID,IN),ID=1,NVCL(CODE))
                                     WRITE_DONE = .TRUE.

                        ENDIF

                     ENDIF
                  ENDIF
               ENDIF
            ENDIF
         ENDDO

      GOTO 200
299   CONTINUE

      CLOSE(10)

C---     Reset les codes
      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISBND(NOMFIC, NNT, NDLN, VDLG, KDIMP, VCL)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VDLG (NDLN, 1)
      DIMENSION KDIMP(NDLN, 1)
      DIMENSION VCL  (4, 1)

      CHARACTER*256 NOMCL, TMPCL
      CHARACTER*32  FMT
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(FMT,'(A,I3,A)')  '(A,I9,',4, '(1X,1PE25.17E3), A)'

      NCL = 0
      IC = -1
      DO IN=1,NNT
         IF (KDIMP(1, IN) .GT. 0) THEN
            IC = KDIMP(1,IN)
            CODE = MOD(IC,1000)
            IF (CODE .NE. 0) THEN
               NCL = NCL + 1
            ENDIF
            IF ((CODE .EQ. 3) .OR. (CODE .EQ. 4)
     & .OR. (CODE .EQ. 101) .OR. (CODE .EQ. 102)
     & .OR. (CODE .EQ. 111) .OR. (CODE .EQ. 112)
     & .OR. (CODE .EQ. 113) .OR. (CODE .EQ. 133)) THEN
               NCL = NCL + 1
            ENDIF
         ENDIF
      ENDDO
      WRITE(10, '(I9)') NCL

100   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               CODE = MOD(IC,1000)
               GOTO 109
            ENDIF
         ENDDO
109      CONTINUE
         IF (IC .LT. 0) GOTO 199

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)
               IF (CODE .NE. 0) THEN
                  WRITE(NOMCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$'
                  CALL TRIMSTR(NOMCL)
                  CALL SBSTSTR(NOMCL, ' ','_')

                  WRITE(10, '(A,I9)') NOMCL(1:LENSTR(NOMCL)), IN
               ENDIF
            ENDIF
         ENDDO

      GOTO 100
199   CONTINUE


      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO
C------------------------------------------------------------------------------------
C On recommence pour les limites qui vont etre doubles a cause de l'ajout du code 141
C------------------------------------------------------------------------------------
300   CONTINUE
         IC = -1
         DO IN=1,NNT
            IF (KDIMP(1, IN) .GT. 0) THEN
               IC = KDIMP(1,IN)
               CODE = MOD(IC,1000)
               GOTO 309
            ENDIF
         ENDDO
309      CONTINUE
         IF (IC .LT. 0) GOTO 399

         DO IN=1,NNT
            IF (KDIMP(1, IN) .EQ. IC) THEN
               KDIMP(1,IN) = -KDIMP(1,IN)
               IF ((CODE .EQ. 3) .OR. (CODE .EQ. 4)
     & .OR. (CODE .EQ. 101) .OR. (CODE .EQ. 102)
     & .OR. (CODE .EQ. 111) .OR. (CODE .EQ. 112)
     & .OR. (CODE .EQ. 113) .OR. (CODE .EQ. 133)) THEN
                  WRITE(NOMCL,FMT) '$',IC/1000,(VCL(ID,IN),ID=1,4),'$B'
                  CALL TRIMSTR(NOMCL)
                  CALL SBSTSTR(NOMCL, ' ','_')

                  WRITE(10, '(A,I9)') NOMCL(1:LENSTR(NOMCL)), IN
               ENDIF
            ENDIF
         ENDDO

      GOTO 300
399   CONTINUE

      CLOSE(10)

      DO IN=1,NNT
         KDIMP(1,IN) = ABS(KDIMP(1,IN))
      ENDDO


      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISELE(NOMFIC, NELT, NNEL, KNG)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION KNG(NNEL, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      IF (NNEL .EQ. 3) THEN
         WRITE(10, '(3I6,1X,1PE25.17E3)') NELT, NNEL, 201, 0.0
      ELSE
         WRITE(10, '(3I6,1X,1PE25.17E3)') NELT, NNEL, 203, 0.0
      ENDIF
      DO IE=1,NELT
         WRITE(10, '(6I9)') (KNG(I, IE), I=1,NNEL)
      ENDDO
      CLOSE(10)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE ECRISPRN(NOMFIC, NNT, NPRN, VPRN)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) NOMFIC
      DIMENSION VPRN(NPRN, 1)
C-----------------------------------------------------------------------------

      OPEN(UNIT=10, FILE=NOMFIC, STATUS='UNKNOWN')
      WRITE(10, '(2I6,1X,1PE25.17E3)') NNT, NPRN, 0.0
      DO IN=1,NNT
         WRITE(10, '(25(1X,1PE25.17E3))') (VPRN(I, IN), I=1,NPRN)
      ENDDO
      CLOSE(10)

      RETURN
      END

  