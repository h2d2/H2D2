C=============================================================================
C     Genère le Modèle Numérique de Terrain
C        1)  BATHYMETRIE
C        2)  MANNING NODALE
C        3)  EPAISSEUR DE LA GLACE
C		 4)  MANNING DE LA GLACE (-)
C        5)  COMPOSANTE X DU VENT
C        6)  COMPOSANTE Y DU VENT
C
C=============================================================================
      SUBROUTINE GENMNT(MNT,
     &                  NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) MNT
      DIMENSION     VCORG(*)
      DIMENSION     KNG(*)
      DIMENSION     VPRN(*)
C-----------------------------------------------------------------------------

      Z0    = 1.0D0
      DZDX  = 0.0D0
      DZDY  = 0.0D0
      VMAN  = 0.0D0
      VMANG = 0.0D0
      VEG   = 0.0D0
      DMDX  = 0.0D0
      DMDY  = 0.0D0
      WX    = 0.0D0
      WY    = 0.0D0

      IF (MNT(1:3) .EQ. 'Z0=') THEN
         MNT = MNT(4:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) Z0
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:5) .EQ. 'DZDX=') THEN
         MNT = MNT(6:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) DZDX
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:5) .EQ. 'DZDY=') THEN
         MNT = MNT(6:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) DZDY
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:2) .EQ. 'N=') THEN
         MNT = MNT(3:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) VMAN
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF
      IF (MNT(1:3) .EQ. 'NG=') THEN
         MNT = MNT(4:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) VMANG
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF
      IF (MNT(1:3) .EQ. 'EG=') THEN
         MNT = MNT(4:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) VEG
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:5) .EQ. 'DMDX=') THEN
         MNT = MNT(6:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) DMDX
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:5) .EQ. 'DMDY=') THEN
         MNT = MNT(6:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) DMDY
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:3) .EQ. 'WX=') THEN
         MNT = MNT(4:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) WX
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF

      IF (MNT(1:3) .EQ. 'WY=') THEN
         MNT = MNT(4:LENSTR(MNT))
         CALL TRIMSTR(MNT)
         READ(MNT, *) WY
         IF (INSTR(MNT,',').GE.1) MNT=MNT(INSTR(MNT,',')+1:LENSTR(MNT))
         CALL TRIMSTR(MNT)
      ENDIF


      CALL PENTE  (NDIM, NNX, NNY, NNT, VCORG,
     &             NNEL, NELT, KNG,
     &             NPRN, VPRN,
     &             Z0, DZDX, DZDY, VMAN, VMANG, VEG, WX, WY)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE PENTE(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 Z0, DZDX, DZDY, VMAN, VMANG, VEG, WX, WY)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
C-----------------------------------------------------------------------------

      X0 = VCORG(1,1)
      Y0 = VCORG(2,1)
      F  = VMAN
      ET = VEG
      EF = VMANG
      DO IN=1,NNT
         X = VCORG(1,IN)
         Y = VCORG(2,IN)
         VPRN(1,IN) = Z0 + DZDX*(X-X0) + DZDY*(Y-Y0)   ! Topographie
         VPRN(2,IN) = F                                ! Manning fond
         VPRN(3,IN) = ET                               ! Épaisseur glace
         VPRN(4,IN) = EF                               ! Manning glace
         VPRN(5,IN) = WX                               ! Vitesse vent x
         VPRN(6,IN) = WY                               ! Vitesse vent y
      ENDDO
      WRITE(*,*) 'NG=',ET,'EG=', EF
      RETURN
      END

