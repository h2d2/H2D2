      SUBROUTINE RK4 (X0, Y0, X1, H0, Y1, DERIV)

      IMPLICIT REAL*8 (A-H,O-Z)
      EXTERNAL DERIV

      DELX = X1 - X0
      NPAS = NINT(DELX/H0)
      H = DELX / DBLE(NPAS)

      HH = H*0.5
      H6 = H/6.0

      X = X0
      Y = Y0
      DO IP=1, NPAS
         CALL DERIV(X,Y,DYDX)

         XH = X + HH
         YT = Y + HH*DYDX
         CALL DERIV(XH,YT,DYT)

         YT = Y + HH*DYT
         CALL DERIV(XH,YT,DYM)

         YT  = Y + H*DYM
         DYM = DYM + DYT
         CALL DERIV(X+H,YT,DYT)

         Y = Y + H6*(DYDX+DYT+2.0*DYM)
      ENDDO
      Y1 = Y

      RETURN
      END

