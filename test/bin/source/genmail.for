C=============================================================================
C
C=============================================================================
      SUBROUTINE GENCOOR(LNODRND,
     &                   XORI, YORI, DIMX, DIMY,
     &                   NNX, NNY, VCORG)

      INCLUDE 'gentest.fi'

      LOGICAL   LNODRND
      DIMENSION VCORG(2,*)

      REAL  URAND
      PARAMETER (UN_2 = 0.5D0)
C-----------------------------------------------------------------------------

      ISEED = 75347
      DELX  = DIMX / DBLE(NNX-1)
      DELY  = DIMY / DBLE(NNY-1)

      IN = 1
      X = XORI
      Y = YORI
      DO IY=1,NNY
         IF (IY .EQ. NNY) Y = YORI + DIMY
         VCORG(1, IN) = X
         VCORG(2, IN) = Y
         Y = Y + DELY
         IN = IN + 1
      ENDDO

      DO IX=2,(NNX-1)
         X = X + DELX
         Y = YORI
         VCORG(1, IN) = X
         VCORG(2, IN) = Y
         Y = Y + DELY
         IN = IN + 1
         DO IY=2,(NNY-1)
            IF (LNODRND) THEN
               VCORG(1, IN) = X + (URAND(ISEED)-UN_2)*DELX*UN_2
               VCORG(2, IN) = Y + (URAND(ISEED)-UN_2)*DELY*UN_2
            ELSE
               VCORG(1, IN) = X
               VCORG(2, IN) = Y
            ENDIF
            Y = Y + DELY
            IN = IN + 1
         ENDDO
         Y = YORI + DIMY
         VCORG(1, IN) = X
         VCORG(2, IN) = Y
         IN = IN + 1
      ENDDO

      X = XORI + DIMX
      Y = YORI
      DO IY=1,NNY
         IF (IY .EQ. NNY) Y = YORI + DIMY
         VCORG(1, IN) = X
         VCORG(2, IN) = Y
         Y = Y + DELY
         IN = IN + 1
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE GENELEM(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(2,1)
      DIMENSION KNG(NNEL, 1)
C-----------------------------------------------------------------------------
      IF (NNEL .EQ. 3) THEN
         CALL GENELET3(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)
      ELSEIF (NNEL .EQ. 6) THEN
         CALL GENELET6(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE GENELET3(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(2,1)
      DIMENSION KNG(NNEL, 1)

      LOGICAL LSLASH
C-----------------------------------------------------------------------------

      IE = 0
      DO IX=1,(NNX-1)
         NO1 = (IX-1)*NNY
         NO2 = NO1 + NNY
         IF (ITYPEL .EQ. IEL$SLASH                      .OR.
     &       ITYPEL .EQ. IEL$V    .AND. IX .GT. (NNX/2) .OR.
     &       ITYPEL .EQ. IEL$VINV .AND. IX .LE. (NNX/2) .OR.
     &       ITYPEL .EQ. IEL$GT                         .OR.
     &       ITYPEL .EQ. IEL$X    .AND. IX .LE. (NNX/2)) THEN
            LSLASH = .TRUE.
         ELSE
            LSLASH = .FALSE.
         ENDIF
         DO IY=1,(NNY-1)
            NO1 = NO1 + 1
            NO2 = NO2 + 1
            NO3 = NO2 + 1
            NO4 = NO1 + 1
            IF (IY .EQ. (NNY/2+1)) THEN
               IF (ITYPEL .EQ. IEL$GT .OR.
     &             ITYPEL .EQ. IEL$LT .OR.
     &             ITYPEL .EQ. IEL$X ) LSLASH = .NOT. LSLASH
            ENDIF

            IF (LSLASH) THEN
               IE = IE + 1
               KNG(1,IE) = NO2
               KNG(2,IE) = NO3
               KNG(3,IE) = NO1
               IE = IE + 1
               KNG(1,IE) = NO4
               KNG(2,IE) = NO1
               KNG(3,IE) = NO3
            ELSE
               IE = IE + 1
               KNG(1,IE) = NO1
               KNG(2,IE) = NO2
               KNG(3,IE) = NO4
               IE = IE + 1
               KNG(1,IE) = NO3
               KNG(2,IE) = NO4
               KNG(3,IE) = NO2
            ENDIF
         ENDDO
      ENDDO
      NELT = IE

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE GENELET6(ITYPEL, NNX, NNY, VCORG, NNEL, NELT, KNG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(2,1)
      DIMENSION KNG(NNEL, 1)

      LOGICAL LSLASH
C-----------------------------------------------------------------------------

      IE = 0
      DO IX=1,(NNX-1),2
         NO1 = (IX-1)*NNY - 1
         NO2 = (IX  )*NNY - 1
         NO3 = (IX+1)*NNY - 1
         IF (ITYPEL .EQ. IEL$SLASH                      .OR.
     &       ITYPEL .EQ. IEL$V    .AND. IX .GT. (NNX/2) .OR.
     &       ITYPEL .EQ. IEL$VINV .AND. IX .LE. (NNX/2) .OR.
     &       ITYPEL .EQ. IEL$GT                         .OR.
     &       ITYPEL .EQ. IEL$X    .AND. IX .LE. (NNX/2)) THEN
            LSLASH = .TRUE.
         ELSE
            LSLASH = .FALSE.
         ENDIF
         DO IY=1,(NNY-1),2
            NO1 = NO1 + 2
            NO8 = NO1 + 1
            NO7 = NO8 + 1
            NO2 = NO2 + 2
            NO9 = NO2 + 1
            NO6 = NO9 + 1
            NO3 = NO3 + 2
            NO4 = NO3 + 1
            NO5 = NO4 + 1
            IF (IY .EQ. (NNY/2+1)) THEN
               IF (ITYPEL .EQ. IEL$GT .OR.
     &             ITYPEL .EQ. IEL$LT .OR.
     &             ITYPEL .EQ. IEL$X ) LSLASH = .NOT. LSLASH
            ENDIF

            IF (LSLASH) THEN
               IE = IE + 1
               KNG(1,IE) = NO3
               KNG(2,IE) = NO4
               KNG(3,IE) = NO5
               KNG(4,IE) = NO9
               KNG(5,IE) = NO1
               KNG(6,IE) = NO2
               IE = IE + 1
               KNG(1,IE) = NO7
               KNG(2,IE) = NO8
               KNG(3,IE) = NO1
               KNG(4,IE) = NO9
               KNG(5,IE) = NO5
               KNG(6,IE) = NO6
            ELSE
               IE = IE + 1
               KNG(1,IE) = NO1
               KNG(2,IE) = NO2
               KNG(3,IE) = NO3
               KNG(4,IE) = NO9
               KNG(5,IE) = NO7
               KNG(6,IE) = NO8
               IE = IE + 1
               KNG(1,IE) = NO5
               KNG(2,IE) = NO6
               KNG(3,IE) = NO7
               KNG(4,IE) = NO9
               KNG(5,IE) = NO3
               KNG(6,IE) = NO4
            ENDIF

            VCORG(1,NO2) = 0.5D0*(VCORG(1,NO1)+VCORG(1,NO3))
            VCORG(2,NO2) = 0.5D0*(VCORG(2,NO1)+VCORG(2,NO3))
            VCORG(1,NO4) = 0.5D0*(VCORG(1,NO3)+VCORG(1,NO5))
            VCORG(2,NO4) = 0.5D0*(VCORG(2,NO3)+VCORG(2,NO5))
            VCORG(1,NO6) = 0.5D0*(VCORG(1,NO5)+VCORG(1,NO7))
            VCORG(2,NO6) = 0.5D0*(VCORG(2,NO5)+VCORG(2,NO7))
            VCORG(1,NO8) = 0.5D0*(VCORG(1,NO7)+VCORG(1,NO1))
            VCORG(2,NO8) = 0.5D0*(VCORG(2,NO7)+VCORG(2,NO1))
            IF (LSLASH) THEN
               VCORG(1,NO9) = 0.5D0*(VCORG(1,NO1)+VCORG(1,NO5))
               VCORG(2,NO9) = 0.5D0*(VCORG(2,NO1)+VCORG(2,NO5))
            ELSE
               VCORG(1,NO9) = 0.5D0*(VCORG(1,NO3)+VCORG(1,NO7))
               VCORG(2,NO9) = 0.5D0*(VCORG(2,NO3)+VCORG(2,NO7))
            ENDIF

         ENDDO
      ENDDO
      NELT = IE

      RETURN
      END


C=============================================================================
C Sommaire : URAND
C
C Description:
C     Retourne un nombre aléatoire entre 0 et 1
C
C Entrée:
C
C Sortie: iy
C
C Notes: Fonction copiée sur internet :
C  urand is a uniform random number generator based  on  theory  and
C  suggestions  given  in  d.e. knuth (1969),  vol  2.   the integer  iy
c  should be initialized to an arbitrary integer prior to the first call
c  to urand.  the calling program should  not  alter  the  value  of  iy
c  between  subsequent calls to urand.  values of urand will be returned
c  in the interval (0,1).
C
C=============================================================================

      real function urand(iy)
      integer  iy

C
c
      integer  ia,ic,itwo,m2,m,mic
      double precision  halfm
      real  s
      double precision  datan,dsqrt
      data m2/0/,itwo/2/

      save ia,ic,itwo,m2,m,mic
      save halfm
      save s

      if (m2 .ne. 0) go to 20
c
c  if first entry, compute machine integer word length
c
      m = 1
   10 m2 = m
      m = itwo*m2
      if (m .gt. m2) go to 10
      halfm = m2
c
c  compute multiplier and increment for linear congruential method
c
      ia = 8*idint(halfm*datan(1.d0)/8.d0) + 5
      ic = 2*idint(halfm*(0.5d0-dsqrt(3.d0)/6.d0)) + 1
      mic = (m2 - ic) + m2
c
c  s is the scale factor for converting to floating point
c
      s = 0.5/halfm
c
c  compute next random number
c
   20 iy = iy*ia
c
c  the following statement is for computers which do not allow
c  integer overflow on addition
c
      if (iy .gt. mic) iy = (iy - m2) - m2
c
      iy = iy + ic
c
c  the following statement is for computers where the
c  word length for addition is greater than for multiplication
c
      if (iy/2 .gt. m2) iy = (iy - m2) - m2
c
c  the following statement is for computers where integer
c  overflow affects the sign bit
c
      if (iy .lt. 0) iy = (iy + m2) + m2
      urand = float(iy)*s
      return
      end
