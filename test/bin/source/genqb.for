C=============================================================================
C     SORTIE VPRN (u,v,h,n), VSOL(solution at t = DT)
C=============================================================================
      SUBROUTINE GENQB(ECOULEMENT,
     &                 NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL, VDLG, DT, UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)

      CHARACTER*(*) ECOULEMENT
C-----------------------------------------------------------------------------

      IF (ECOULEMENT .EQ. 'QBCXY') THEN
         CALL QBC_XY  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'QBCIXY') THEN
         CALL QBC_IXY  (NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NPRD, VPRD,
     &                  NDLN, VSOL, VDLG)
      ELSEIF (ECOULEMENT .EQ. 'QB1X') THEN
         CALL QB1_X   (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QB1BX') THEN
         CALL QB1B_X   (NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NPRD, VPRD,
     &                  NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QBH') THEN
         CALL QBH   (NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN,
     &               NPRD, VPRD,
     &               NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QB1XY') THEN
         CALL QB1_XY  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QB1IXY') THEN
         CALL QB1_IXY  (NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NPRD, VPRD,
     &                  NDLN, VSOL, VDLG, DT, UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QBEXY') THEN
         CALL QBE_XY  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QBEIXY') THEN
         CALL QBE_IXY  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QBEXYT') THEN
         CALL QBE_XY_T  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSEIF (ECOULEMENT .EQ. 'QBEIXYT') THEN
         CALL QBE_IXY_T  (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VPRN,
     &                 NPRD, VPRD,
     &                 NDLN, VSOL,VDLG, DT,UN_LMDA)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:ECOUL=', ECOULEMENT
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBC_XY(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VPRN,
     &                  NPRD, VPRD,
     &                  NDLN, VSOL, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      U     = 1.0D0
      V     = 1.0D0
      H     = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H + VDLG(IDZ,IN)
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE QBC_IXY(NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VPRN,
     &                   NPRD, VPRD,
     &                   NDLN, VSOL, VDLG)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      U     = -1.0D0
      V     = -1.0D0
      H     = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QB1_X(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------
      U     = 1.0D0
      V     = 0.0D0
      H0    = 2.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H0 + VCORG(1, IN) - VCORG(1, 1)
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)-DT/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QB1_XY(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------
      U     = 1.0D0
      V     = 1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN) - VCORG(1, 1)
         Y          = VCORG(2, IN) - VCORG(2, 1)
         VPRN(3,IN) = H0 + X + Y
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)-2.0D0/SQRT(2.0D0)*DT/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END
C=============================================================================
C
C=============================================================================
      SUBROUTINE QB1_IXY(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------
      U     = -1.0D0
      V     = -1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN) - VCORG(1, 1)
         Y          = VCORG(2, IN) - VCORG(2, 1)
         VPRN(3,IN) = H0 + X + Y
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)+2.0D0/SQRT(2.0D0)*DT/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBE_XY(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      U     = 1.0D0
      V     = 1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
         VPRN(3,IN) = H0 + (SQRT(2.0D0)+1.0D0)*EXP(X+Y)
         VPRN(4,IN) = FN
C-------  SOLUTION
         VSOL (NDLN, IN) = VDLG(NDLN,IN)
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBE_IXY(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      U     = -1.0D0
      V     = -1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
         VPRN(3,IN) = H0 + (SQRT(2.0D0)-1.0D0)*EXP(X+Y)
         VPRN(4,IN) = FN
C-------  SOLUTION
         VSOL (NDLN, IN) = VDLG(NDLN,IN)
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBE_XY_T(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT, UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      U     = 1.0D0
      V     = 1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
         VPRN(3,IN) = H0 + EXP(X+Y)*SQRT(2.0D0)/2.0D0 + VDLG(IDZ,IN)
         VPRN(4,IN) = FN

C-------  SOLUTION
      VSOL (NDLN, IN) = VPRD(1,IN)-DT*EXP(X+Y)/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBE_IXY_T(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT, UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------

      IDZ = NDLN

      U     = -1.0D0
      V     = -1.0D0
      H0    = 3.0D0
      FN    = 0.025D0
      DO IN=1,NNT
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         X          = VCORG(1, IN)
         Y          = VCORG(2, IN)
         VPRN(3,IN) = H0 + EXP(X+Y)*SQRT(2.0D0)/2.0D0 + VDLG(IDZ,IN)
         VPRN(4,IN) = FN

C-------  SOLUTION
      VSOL (NDLN, IN) = VPRD(1,IN)+DT*EXP(X+Y)/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QB1B_X(NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VPRN,
     &                NPRD, VPRD,
     &                NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------
      H0    = 0.10D0
      FN    = 0.025D0
      DO IN=1,NNT
         X          = (VCORG(1, IN) - VCORG(1, 1))
         U          = X
         V          = SQRT(1.0D0 - X*X)
         VPRN(1,IN) = U*H0
         VPRN(2,IN) = V*H0
         VPRN(3,IN) = H0 + VPRD(1,IN)
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)!-DT/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE QBH(NDIM, NNX, NNY, NNT, VCORG,
     &               NNEL, NELT, KNG,
     &               NPRN, VPRN,
     &               NPRD, VPRD,
     &               NDLN, VSOL, VDLG, DT,UN_LMDA)

      INCLUDE 'gentest.fi'

      DIMENSION VCORG(NDIM,1)
      DIMENSION KNG  (NNEL,1)
      DIMENSION VPRN (NPRN,1)
      DIMENSION VPRD (NPRD,1)
      DIMENSION VSOL (NDLN,1)
      DIMENSION VDLG (NDLN,1)
C-----------------------------------------------------------------------------
      FN        = 0.025D0
      FK         = 0.410D0
      THREE_TWO = 3.0D0 / 2.0D0
      DO IN=1,NNT
         Y          = MIN(9.99999990D-01,VCORG(2, IN) - VCORG(2, 1))
         U          = 1.0D0 - Y
         V          = SQRT(1.0D0 - U*U)
         H          = FK*FK/2.0D0*V/U
         VPRN(1,IN) = U
         VPRN(2,IN) = V
         VPRN(3,IN) = H
         VPRN(4,IN) = FN

C-------  SOLUTION
         VSOL (NDLN, IN) = VPRD(1,IN)!-DT/UN_LMDA
         DO ID = 1,NDLN-1
            VSOL (ID, IN) = VDLG(ID,IN)
         ENDDO
      ENDDO

      RETURN
      END
            