C=============================================================================
C
C=============================================================================
      SUBROUTINE GENERR (SUBCMD,
     &                   NDIM, NNX, NNY, NNT, VCORG,
     &                   NNEL, NELT, KNG,
     &                   NPRN, VHSS,
     &                   NDLN, VDLG)

      INCLUDE 'gentest.fi'

      CHARACTER*(*) SUBCMD
      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, NNT)
      INTEGER KNG  (NNEL, NELT)
      REAL*8  VHSS (NPRN, NNT)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IDEB, IFIN
      CHARACTER*256 TYP_CONC
      CHARACTER*256 PARAM_CMD

      INTEGER INSTR, LENSTR
C-----------------------------------------------------------------------------

      IDEB = INSTR(SUBCMD, '(')
      IF (IDEB .NE. -1) THEN
         IFIN = INSTR(SUBCMD, ')')
         TYP_CONC  = SUBCMD(1:IDEB-1)
         PARAM_CMD = SUBCMD(IDEB+1:IFIN-1)
      ELSE
         TYP_CONC  = SUBCMD
         PARAM_CMD = ' '
      ENDIF

      IF (TYP_CONC .EQ. 'C') THEN
         CALL H_C    (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'X') THEN
         CALL H_X    (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'Y') THEN
         CALL H_Y    (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'XY') THEN
         CALL H_XY   (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'X2') THEN
         CALL H_X2   (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'Y2') THEN
         CALL H_Y2   (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'X2Y2') THEN
         CALL H_X2Y2 (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSEIF (TYP_CONC .EQ. 'EXY') THEN
         CALL H_EXY  (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)
      ELSE
         WRITE(*,*) 'OPTION INCONNUE:TYPECONC=', TYP_CONC
         STOP
      ENDIF

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_C (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         VDLG(1, IN) = 1.0D0
         VHSS(1, IN) = 0.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_X (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         VDLG(1, IN) = VCORG(1, IN)
         VHSS(1, IN) = 0.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_Y (NDIM, NNX, NNY, NNT, VCORG,
     &                NNEL, NELT, KNG,
     &                NPRN, VHSS,
     &                NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1,NNT
         VDLG(1, IN) = VCORG(2, IN) !C=y
         VHSS(1, IN) = 0.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_XY (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VHSS,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG(1, IN) = X+Y
         VHSS(1, IN) = 0.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_X2 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VHSS,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         X = VCORG(1, IN)
         VDLG(1, IN) = X*X
         VHSS(1, IN) = 2.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 0.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_Y2 (NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VHSS,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         Y = VCORG(2, IN)
         VDLG(1, IN) = Y*Y
         VHSS(1, IN) = 0.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 2.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_X2Y2(NDIM, NNX, NNY, NNT, VCORG,
     &                  NNEL, NELT, KNG,
     &                  NPRN, VHSS,
     &                  NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         VDLG(1, IN) = X*X + Y*Y
         VHSS(1, IN) = 2.0D0
         VHSS(2, IN) = 0.0D0
         VHSS(3, IN) = 2.0D0
      ENDDO

      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE H_EXY(NDIM, NNX, NNY, NNT, VCORG,
     &                 NNEL, NELT, KNG,
     &                 NPRN, VHSS,
     &                 NDLN, VDLG)

      INCLUDE 'gentest.fi'

      INTEGER NDIM, NNX, NNY, NNT
      INTEGER NNEL, NELT, NPRN, NDLN
      REAL*8  VCORG(NDIM, 1)
      INTEGER KNG  (NNEL, 1)
      REAL*8  VHSS (NPRN, 1)
      REAL*8  VDLG (NDLN, NNT)

      INTEGER IN
C-----------------------------------------------------------------------------

      DO IN=1, NNT
         X = VCORG(1, IN)
         Y = VCORG(2, IN)
         E = EXP(-X-Y)
         VDLG(1, IN) = E
         VHSS(1, IN) = E
         VHSS(2, IN) = E
         VHSS(3, IN) = E
      ENDDO

      RETURN
      END
