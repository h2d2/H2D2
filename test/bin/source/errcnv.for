C=============================================================================
C
C=============================================================================
      PROGRAM ERRCNV

      IMPLICIT NONE

      INTEGER      LISFICHIERS
      CHARACTER*256 LIGNE
      CHARACTER*24 NOMREP
      CHARACTER*24 NOMCOR
      CHARACTER*24 NOMINI
      CHARACTER*24 NOMFIN
      CHARACTER*24 TOKEN
      REAL*8 ERRL2(99)
      REAL*8 ERRMX(99)
      REAL*8 DELTA(99)

      REAL*8  DELTATMP, ERRL2TMP, ERRMXTMP
      REAL*8  TGTMIN, TGTMAX, TGTEL2, TGTTOL
      REAL*8  DERR, DDEL, PENTE
      REAL*8  VAL
      INTEGER IERR, ISTAT
      INTEGER I, J, INDEQ, IFIC
      INTEGER LNOMREP
      INTEGER NFIC
      LOGICAL LRESGLOB, ESTOK

      INTEGER LENSTR
      INTEGER SYSTEM
C-----------------------------------------------------------------------------

      ISTAT = SYSTEM('dir /ad /b *.* > dir_lst.tmp')                         ! Langage WIN
C      ISTAT = SYSTEM('find . -maxdepth 1 -type d -print > dir_lst.tmp')      ! Langage UNIX
      IF (ISTAT .eq. -1) GOTO 9900

      OPEN(UNIT=10,
     &     FILE='dir_lst.tmp',
     &     ACCESS= 'SEQUENTIAL',
     &     FORM= 'FORMATTED',
     &     STATUS='OLD',
     &     ERR=9901)

C ---  LIS LES DONNEES
      NFIC = 0
100   CONTINUE
         READ(10, '(A)', ERR=9902, END=110) NOMREP
         LNOMREP= LENSTR(NOMREP)
         IF (LNOMREP .LE. 0) GOTO 110
         NOMCOR = NOMREP(1:LNOMREP) // '/' // 'test.cor'
         NOMINI = NOMREP(1:LNOMREP) // '/' // 'test.ini'
         NOMFIN = NOMREP(1:LNOMREP) // '/' // 'test.fin'
         IERR = LISFICHIERS(NOMCOR(1:LENSTR(NOMCOR)),
     &                      NOMINI(1:LENSTR(NOMINI)),
     &                      NOMFIN(1:LENSTR(NOMFIN)),
     &                      DELTATMP,
     &                      ERRL2TMP,
     &                      ERRMXTMP)
         IF (IERR .EQ. 0) THEN
            NFIC = NFIC + 1
            DELTA(NFIC) = DELTATMP
            ERRL2(NFIC) = ERRL2TMP
            ERRMX(NFIC) = ERRMXTMP
         ENDIF
      GOTO 100
110   CONTINUE
      CLOSE(10)

C ---  TRIE LES DONNEES
      DO I=1,(NFIC-1)
         DO J=(I+1),NFIC
            IF (DELTA(J) .GT. DELTA(I)) THEN
               DELTATMP = DELTA(I)
               DELTA(I) = DELTA(J)
               DELTA(J) = DELTATMP
               ERRL2TMP = ERRL2(I)
               ERRL2(I) = ERRL2(J)
               ERRL2(J) = ERRL2TMP
               ERRMXTMP = ERRMX(I)
               ERRMX(I) = ERRMX(J)
               ERRMX(J) = ERRMXTMP
            ENDIF
         ENDDO
      ENDDO

C ---  LIS LES DONNEES CIBLE
      LRESGLOB = .FALSE.
      TGTEL2 = -1.0D0
      TGTTOL = -1.0D0
      TGTMAX = -1.0D0
      TGTMIN = -1.0D0
      OPEN(UNIT=10, FILE='test.tgt', STATUS='OLD', ERR=9901)
200   CONTINUE
         READ(10, '(A)', ERR=9902, END=210) LIGNE
         INDEQ = INDEX(LIGNE, '=')
         IF (INDEQ .LE. 1) GOTO 200
         TOKEN = LIGNE(1:INDEQ-1)
         CALL TRIMSTR(TOKEN)
         CALL UCASESTR(TOKEN)
         READ(LIGNE(INDEQ+1:LENSTR(LIGNE)), *) VAL
         IF     (TOKEN(1:LENSTR(TOKEN)) .EQ. 'ERRL2') THEN
            TGTEL2 = VAL
C         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'ERRMAX') THEN
C            TGTEMX = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'PENTE') THEN
C           PASS
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'TOL') THEN
            TGTTOL = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'MAX') THEN
            TGTMAX = VAL
         ELSEIF (TOKEN(1:LENSTR(TOKEN)) .EQ. 'MIN') THEN
            TGTMIN = VAL
         ENDIF
      GOTO 200
210   CONTINUE
      CLOSE(10)
      IF (TGTEL2 .LT. 0.0D0 .AND. TGTTOL .LT. 0.0D0 .AND.
     &    TGTMAX .LT. 0.0D0 .AND. TGTMIN .LT. 0.0D0) GOTO 9900

C ---  ANALYSE
      OPEN(UNIT=10, FILE='test.err', STATUS='UNKNOWN')
      IF (NFIC .LT. 1) THEN
         WRITE(10,'(A)') 'pas de fichiers'
      ELSEIF (NFIC .LT. 2) THEN
         WRITE(10,'(A,3(1PE25.17E3))') 'del, errl2, errMax =',
     &                DELTA(NFIC), ERRL2(NFIC), ERRMX(NFIC)
         IF (TGTTOL .GE. 0.0D0) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target tol=', TGTEL2, TGTTOL
            IF (ABS(ERRL2(NFIC)-TGTEL2) .LE. TGTTOL) LRESGLOB = .TRUE.
         ELSEIF (TGTMAX .GE. 0.0D0) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target max=', TGTMAX
            IF (ERRL2(NFIC) .LE. TGTMAX) LRESGLOB = .TRUE.
         ELSEIF (TGTMIN .GE. 0.0D0) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target min=', TGTMIN
            IF (ERRL2(NFIC) .GE. TGTMIN) LRESGLOB = .TRUE.
         ENDIF
      ELSE
         DO IFIC=1,NFIC
            WRITE(10,'(A,3(1PE25.17E3))') 'del, errl2, errMax =',
     &                   DELTA(IFIC), ERRL2(IFIC), ERRMX(IFIC)
         ENDDO
         IF (ERRL2(NFIC) .LT. 1.0D-15 .OR.
     &       ERRL2(NFIC) .LT. 1.0D-15) THEN
            PENTE = 1.0D123
         ELSE
            DERR  = LOG(ERRL2(NFIC))-LOG(ERRL2(NFIC-1))
            DDEL  = LOG(DELTA(NFIC))-LOG(DELTA(NFIC-1))
            PENTE = DERR/DDEL
         ENDIF
         WRITE(10,'(A,3(1PE25.17E3))') 'pente=', PENTE

         LRESGLOB = .TRUE.
         IF ((TGTTOL .GE. 0.0D0) .AND. (TGTEL2 .GE. 0.0D0)) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target errl2=', TGTEL2,TGTTOL
            ESTOK = .FALSE.
            IF ((ABS(ERRL2(NFIC)-TGTEL2) .LE. TGTTOL)) ESTOK = .TRUE.
            LRESGLOB = LRESGLOB .AND. ESTOK
         ENDIF
         IF (TGTMAX .GE. 0.0D0) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target pente max=', TGTMAX
            ESTOK = .FALSE.
            IF (PENTE .LE. TGTMAX) ESTOK = .TRUE.
            LRESGLOB = LRESGLOB .AND. ESTOK
         ENDIF
         IF (TGTMIN .GE. 0.0D0) THEN
            WRITE(10,'(A,3(1PE25.17E3))') 'target pente min=', TGTMIN
            ESTOK = .FALSE.
            IF (PENTE .GE. TGTMIN) ESTOK = .TRUE.
            LRESGLOB = LRESGLOB .AND. ESTOK
         ENDIF
      ENDIF


C---     Résultat global
9000  CONTINUE
      IF (LRESGLOB) THEN
         WRITE(10,'(A)') 'test=ok'
      ELSE
         WRITE(10,'(A)') 'test=erreur'
      ENDIF
      CLOSE(10)

      GOTO 9999
C-----------------------------------------------------------------------------
9900  WRITE(6,*) 'ERREUR A LA LECTURE DU REPERTOIRE'
      GOTO 9999
9901  WRITE(6,*) 'ERREUR A L''OUVERTURE DU FICHIER'
      GOTO 9999
9902  WRITE(6,*) 'ERREUR A LA LECTURE DU FICHIER'
      GOTO 9999

9999  CONTINUE
C      ISTAT = SYSTEM('del /f /q dir_lst.tmp')
      ISTAT = SYSTEM('rm -f dir_lst.tmp')

      END

C=============================================================================
C
C=============================================================================
      FUNCTION LISFICHIERS(NOMCOR, NOMINI, NOMFIN, DEL, ERRL2, ERRMAX)

      IMPLICIT NONE

      INTEGER LISFICHIERS
      CHARACTER*(*) NOMCOR
      CHARACTER*(*) NOMINI
      CHARACTER*(*) NOMFIN

      REAL*8 C_EX(10)
      REAL*8 C_EF(10)
      REAL*8 ERRMAX, SUM, DEL, ERRL2
      REAL*8 X1, Y1, X2, Y2
      INTEGER IERR
      INTEGER IN, ID
      INTEGER NNT, NDLN, NDIM
C-----------------------------------------------------------------------------

      IERR = 1
      ERRMAX = 0.0D0
      SUM  = 0.0D0

      OPEN(UNIT=20, FILE=NOMINI, STATUS='OLD', ERR=999)
      OPEN(UNIT=21, FILE=NOMFIN, STATUS='OLD', ERR=999)
      OPEN(UNIT=22, FILE=NOMCOR, STATUS='OLD', ERR=999)

      READ (20,*) NNT, NDLN
      READ (21,*) NNT, NDLN
      DO IN=1, NNT
         READ(20,*) (C_EX(ID),ID=1,NDLN)
         READ(21,*) (C_EF(ID),ID=1,NDLN)
         DO ID=1,NDLN
            DEL = ABS(C_EX(ID)-C_EF(ID))
            ERRMAX = MAX(ERRMAX, DEL)
            SUM = SUM + DEL*DEL
         ENDDO
      ENDDO
      SUM = SUM / DBLE(NDLN)

      READ (22,*) NNT, NDIM
      READ (22,*) X1,Y1
      READ (22,*) X2,Y2

      DEL = Y2-Y1
      ERRL2 = SQRT(SUM) / DBLE(NNT)
      IERR = 0

999   CONTINUE
      CLOSE(20)
      CLOSE(21)
      CLOSE(22)

      LISFICHIERS = IERR
      RETURN
      END

C=============================================================================
C
C=============================================================================
      SUBROUTINE TRIMSTR(STR)

      IMPLICIT NONE

      CHARACTER*(*) STR

      INTEGER ILLIN
      INTEGER I

      INTEGER LENSTR
C-----------------------------------------------------------------------------

C------- LENGTH
      ILLIN = LENSTR(STR)

C-------  REMOVE LEADING BLANKS
      I = 0
100   CONTINUE
         I = I + 1
      IF ((STR(I:I) .EQ. ' ') .AND. (I .LT. ILLIN)) GOTO 100
      STR = STR(I:ILLIN)

      RETURN
      END

C=============================================================================
C
C=============================================================================
      FUNCTION LENSTR(STR)

      IMPLICIT NONE

      CHARACTER*(*) STR

      INTEGER I
      INTEGER LENSTR
C-----------------------------------------------------------------------------

      DO I=LEN(STR),1,-1
         IF (STR(I:I) .NE. ' ') GOTO 100
      ENDDO
100   LENSTR = I

      RETURN
      END

C=============================================================================
C     ALL UPPERCASE EXCEPT BETWEEN '
C=============================================================================
      SUBROUTINE UCASESTR(STR)
      CHARACTER*(*) STR

      INTEGER I
      LOGICAL ITRUP

      INTEGER LENSTR
C-----------------------------------------------------------------------

      ITRUP = .TRUE.
      DO I=1,LENSTR(STR)
         IF (STR(I:I) .EQ. '''') ITRUP = .NOT. ITRUP
         IF ((STR(I:I) .GE. 'a') .AND. (STR(I:I) .LE. 'z')) THEN
            IF (ITRUP) STR(I:I) = CHAR(ICHAR(STR(I:I))-32)
         ENDIF
      ENDDO

      RETURN
      END

