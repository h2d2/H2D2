#=====================================================================
# $Header$
# $Date$
# $Author$
# $Locker$
#=====================================================================

#=====================================================================
# Description:
#    Fichier include de Makefile pour faire les tests
#
#=====================================================================

# --- Variable pour appel de test_part_num.py
TEST_PART_NUM = python $(BT_GENTEST_BINDIR)/test_part_num.py
# --- Variable pour appeler le générateur de fichiers test
ifdef PROG_GENTEST
BT_GENTEST = $(BT_GENTEST_BINDIR)/$(PROG_GENTEST)
endif
ifndef BT_GENTEST
BT_GENTEST = $(BT_GENTEST_BINDIR)/$(PROG_GENTEST)
endif
# --- Variable pour appeler l'analyseur de résultats
ifdef PROG_ERRCNV
BT_ERRCNV = $(BT_GENTEST_BINDIR)/$(PROG_ERRCNV)
endif
ifndef BT_ERRCNV
BT_ERRCNV = $(BT_GENTEST_BINDIR)/$(PROG_ERRCNV)
endif

# ---  Canonise la variable des cas tests
CAS_TEST:=$(subst ;, ,$(CAS_TEST))
CAS_TEST:=$(subst X,x,$(CAS_TEST))

# ---  Transforme les path windows en path unix
ifdef CYGWIN
    @echo BT_GENTEST:=$(shell cygpath -u $(BT_GENTEST))
    @echo BT_TARGET_DIR:=$(shell cygpath -u $(BT_TARGET_DIR))
    @echo BT_TARGET_EXE:=$(shell cygpath -u $(BT_TARGET_EXE))
endif

# ---  Suffixes connus
.SUFFIXES: 
.SUFFIXES: .exe .bat .err .fin .out .inp .ini .tst .tpn .mkdir .rmdir

# ---  Règles implicites
%.mkdir:
	@if test ! -d $(*F); then mkdir -p $(*F);	fi

%.rmdir:
	@if ((test -d $(*F)) && (test $(*F) != 'CVS')); then \
		rm -f -r $(*F); \
	fi

%.tst: ../%.tst
	@echo \'test\' > $@
	@echo $(NELX) >> $@
	@echo $(NELY) >> $@
	@cat  $<      >> $@

%.ini: %.tst
	@echo rule .ini:.tst
	@pwd
	@echo $(BT_GENTEST) < $<
	@$(BT_GENTEST) < $<

%.inp: ../%.inp
	@echo rule .inp.inp
	@pwd
	@echo cp $< $@
	@cp $< $@

%.tpn: ../%.tpn
	@cp $< $@

%.out: %.inp
	@echo rule .out:.inp
	@pwd
	@echo $(BT_TARGET_DIR)/$(BT_TARGET_EXE) -fi=$(<F) -fo=$(@F)
ifdef NB_PROC	# on démarre h2d2 avec plusieurs process
	-@mpiexec -n $(NB_PROC) $(BT_TARGET_DIR)/$(BT_TARGET_EXE) -fi=$(<F) -fo=$(@F)
else		# on démarre h2d2 normalement
#	-@$(BT_TARGET_DIR)/$(BT_TARGET_EXE) -fi=$(<F) -fo=$(@F)
	-@env cmd /c "$(BT_TARGET_DIR)/$(BT_TARGET_EXE) -fi=$(<F) -fo=$(@F) & exit"
endif

%.fin: %.out
	@echo rule .fin:.out
	@echo > NUL

%.err:
	@echo rule .err:.fin
	@pwd
	$(CMD_ERR)

%.exe:
	@echo rule .exe

%.bat:
	@echo rule .bat

# ---  Règles explicites
all:	$(foreach cas,$(CAS_TEST),$(cas).mkdir) $(CAS_TEST) test_err

clean:	$(foreach cas,$(wildcard *),$(cas).rmdir)
	@rm -r -f test.err
	@rm -r -f *.tmp

.PHONY: $(CAS_TEST)

$(CAS_TEST):
ifeq ($(strip $(CAS_TEST)), .)	# cas ou on n'a pas à générer les fichiers de test
	-@$(BT_TARGET_EXE) -fi=test.inp -fo=test.out
DEP_ERR =
CMD_ERR =
else
	@cd $@; \
	$(MAKE) $(if $(MAKEFLAGS),-$(MAKEFLAGS)) -f ../makefile.mak NELX=$(word 1, $(subst x, ,$@)) NELY=$(word 2, $(subst x, ,$@)) test_fin;
	@echo --------  Rule $@ done
endif

ifeq ($(TYPE_TEST), part_num)	# vérifie si on teste le partitionnement ou la numérotation, sinon, on prend le TEST_FIN = test.fin
TEST_FIN = test.err
DEP_ERR = test.out test.tpn
CMD_ERR = @$(TEST_PART_NUM) test $(PART_NUM) $(NB_DOM)
else
TEST_FIN = test.fin
TEST_ERR = test.err
DEP_ERR ?= $(foreach cas,$(CAS_TEST),$(cas)/test.fin) test.tgt
CMD_ERR ?= @$(BT_ERRCNV)
endif

test.ini: $(BT_GENTEST)
test.out: test.ini test.inp $(BT_TARGET_EXE)
test_fin: $(TEST_FIN)
test.err: $(DEP_ERR)
test_err: $(TEST_ERR)
