C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes : Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D$SMP$ASMFE
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$SMP$ASMFEV(VCORG,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VDIMP,
     &                           VDSLC,
     &                           VDSLR,
     &                           IEV,
     &                           VFE)

      INCLUDE 'lmcmmn.fc'
      REAL*8  VCORG(JJ$NDIM, JJ$NNT)
      INTEGER KNGV (JJ$NCELV,JJ$NELV)
      INTEGER KNGS (JJ$NCELS,JJ$NELS)
      REAL*8  VDJV (JJ$NDJV, JJ$NELV)
      REAL*8  VDJS (JJ$NDJS, JJ$NELS)
      REAL*8  VPRGL(JJ$NPRGL)
      REAL*8  VPRNO(JJ$NPRNO,JJ$NNT)
      REAL*8  VPREV(JJ$NPREV,JJ$NELV)
      REAL*8  VPRES(JJ$NPREV,JJ$NELV)
      REAL*8  VDIMP(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLC(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLR(JJ$NDLN, JJ$NNT)
      INTEGER IEV
      REAL*8  VFE  (JJ$NDLN, JJ$NNT)
C-----------------------------------------------------------------------

      RETURN
      END

