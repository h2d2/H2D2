C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Sousroutines:
C
C Description: ÉQUATION : ÉQUATION DE CONVECTION-DIFFUSION
C                         EULERIENNE 2-D. FORMULATION NON-CONSERVATIVE
C                         POUR  (C).
C              ÉLÉMENT  : T3 - LINÉAIRE
C              MÉTHODE DE GALERKIN STANDARD
C
C Notes : Fichier contenant les subroutines de base pour le calcul de
C         transport-diffusion de concentration avec cinétiques comprises.
C************************************************************************

C************************************************************************
C Sommaire:  CD2D$SMP$ASMFE
C
C Description:
C     ASSEMBLAGE DU VECTEUR {VFG} DÙ AUX TERMES CONSTANTS
C         SOLLICITATIONS SUR LE CONTOUR CONSTANTES (CAUCHY)
C
C Entrée:
C
C Sortie: VFG
C
C Notes:
C************************************************************************
      SUBROUTINE CD2D$SMP$ASMFEC(VCORG,
     &                           KNGV,
     &                           KNGS,
     &                           VDJV,
     &                           VDJS,
     &                           VPRGL,
     &                           VPRNO,
     &                           VPREV,
     &                           VPRES,
     &                           VDIMP,
     &                           VDSLC,
     &                           VDSLR,
     &                           IEC,
     &                           VFE)

      INCLUDE 'lmcmmn.fc'
      REAL*8  VCORG(JJ$NDIM, JJ$NNT)
      INTEGER KNGV (JJ$NCELV,JJ$NELV)
      INTEGER KNGS (JJ$NCELS,JJ$NELS)
      REAL*8  VDJV (JJ$NDJV, JJ$NELV)
      REAL*8  VDJS (JJ$NDJS, JJ$NELS)
      REAL*8  VPRGL(JJ$NPRGL)
      REAL*8  VPRNO(JJ$NPRNO,JJ$NNT)
      REAL*8  VPREV(JJ$NPREV,JJ$NELV)
      REAL*8  VPRES(JJ$NPREV,JJ$NELV)
      REAL*8  VDIMP(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLC(JJ$NDLN, JJ$NNT)
      REAL*8  VDSLR(JJ$NDLN, JJ$NNT)
      INTEGER IEC
      REAL*8  VFE  (JJ$NDLN, JJ$NNT)
C-----------------------------------------------------------------------

      NP1 = KNGS(1,IEC)
      NP2 = KNGS(2,IEC)

C---       CONSTANTE DÉPENDANT DU VOLUME
      DETJ = VDJS(3,IEC)*UN_2
      REP  = UN_6*DETJ

C---       FLUX NORMAL (V*H)
      QN1 = VDJS(4,IEC)*VPRNO(3,NP1)
      QN2 = VDJS(5,IEC)*VPRNO(3,NP2)
      SQN = QN1 + QN2

C---       MATRICE DE RIGIDITE ELEMENTAIRE DE CAUCHY-ENTRANT
      VKE11 = SQN + QN1+QN1
      VKE21 = SQN
      VKE12 = SQN
      VKE22 = SQN + QN2+QN2

C---       BOUCLE D'ASSEMBLAGE SUR LES DDL
      DO ID=1,JJ$NDLN
         C1 = VDIMP(ID,NP1)
         C2 = VDIMP(ID,NP2)
         VFE(ID,NP1) = (VKE11*C1 + VKE12*C2)*REP
         VFE(ID,NP2) = (VKE21*C1 + VKE22*C2)*REP
      ENDDO

      RETURN
      END

