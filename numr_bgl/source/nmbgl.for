C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Groupe:  Numerotation
C Objet:   algo de renumérotation BGL
C Type:    Concret
C
C Sousroutines:
C
C Note:
C     Structure de la table de redistribution locale: KDISTR(NPROC+2, NNL)
C        NNO_PR1 NNO_PR2 ...  NNO_PRn NNO_Glb IPROC   ! .LE. 0 si absent
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_000()
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_000

      IMPLICIT NONE

      INCLUDE 'nmbgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(NM_BGL_NOBJMAX,
     &                   NM_BGL_HBASE,
     &                   'Renumerotation BGL')

      NM_BGL_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_999()
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_999

      IMPLICIT NONE

      INCLUDE 'nmbgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER  IERR
      EXTERNAL NM_BGL_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(NM_BGL_NOBJMAX,
     &                   NM_BGL_HBASE,
     &                   NM_BGL_DTR)

      NM_BGL_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_BGL_CTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_CTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmbgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   NM_BGL_NOBJMAX,
     &                   NM_BGL_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(NM_BGL_HVALIDE(HOBJ))
         IOB = HOBJ - NM_BGL_HBASE

         NM_BGL_HPRNT(IOB) = 0
         NM_BGL_HFPRT(IOB) = 0
         NM_BGL_HFRNM(IOB) = 0
      ENDIF

      NM_BGL_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_BGL_DTR(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_DTR

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmbgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = NM_BGL_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   NM_BGL_NOBJMAX,
     &                   NM_BGL_HBASE)

      NM_BGL_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_BGL_INI(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_INI

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_bgl.fi'
      INCLUDE 'c_scotch.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFPRT, HFRNM

      EXTERNAL C_PSCTCH_PART
      EXTERNAL C_BGL_RENUM
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = NM_BGL_RST(HOBJ)

C---     CONSTRUIS LES FONCTIONS
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFPRT)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFPRT, C_PSCTCH_PART)
      IF (ERR_GOOD()) IERR = SO_FUNC_CTR   (HFRNM)
      IF (ERR_GOOD()) IERR = SO_FUNC_INIFNC(HFRNM, C_BGL_RENUM)

C---     CONSTRUIS LE PARENT
      IF (ERR_GOOD()) IERR = NM_NBSE_CTR   (HPRNT)
      IF (ERR_GOOD()) IERR = NM_NBSE_INI   (HPRNT, HFPRT, HFRNM)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - NM_BGL_HBASE
         NM_BGL_HPRNT(IOB) = HPRNT
         NM_BGL_HFPRT(IOB) = HFPRT
         NM_BGL_HFRNM(IOB) = HFRNM
      ENDIF

      NM_BGL_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION NM_BGL_RST(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_RST

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'sofunc.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT, HFPRT, HFRNM
C------------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_BGL_HBASE
      HPRNT = NM_BGL_HPRNT(IOB)
      HFPRT = NM_BGL_HFPRT(IOB)
      HFRNM = NM_BGL_HFRNM(IOB)

C---     DETRUIS LES ATTRIBUTS
      IF (SO_FUNC_HVALIDE(HFRNM)) IERR = SO_FUNC_DTR(HFRNM)
      IF (SO_FUNC_HVALIDE(HFPRT)) IERR = SO_FUNC_DTR(HFPRT)
      IF (NM_NBSE_HVALIDE(HPRNT)) IERR = NM_NBSE_DTR(HPRNT)

C---     RESET
      NM_BGL_HPRNT(IOB) = 0
      NM_BGL_HFPRT(IOB) = 0
      NM_BGL_HFRNM(IOB) = 0

      NM_BGL_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction NM_BGL_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_REQHBASE()
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_REQHBASE

      IMPLICIT NONE

      INCLUDE 'nmbgl.fi'
      INCLUDE 'nmbgl.fc'
C------------------------------------------------------------------------

      NM_BGL_REQHBASE = NM_BGL_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction NM_BGL_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_HVALIDE(HOBJ)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_HVALIDE

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'nmbgl.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'nmbgl.fc'
C------------------------------------------------------------------------

      NM_BGL_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  NM_BGL_NOBJMAX,
     &                                  NM_BGL_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Partitionne un maillage.
C
C Description:
C     La fonction NM_BGL_PART partitionne le maillage donné par HELEM en NPROC
C     sous-domaines. Si NPROC .LE. 0, alors on partitionne pour le nombre
C     de process dans le groupe MPI (cluster).
C     <p>
C     La partition n'est pas utilisable immédiatement car il n'y a pas
C     redistribution des données entre les process. La redistribution a lieu au
C     chargement des données. La partition doit donc être sauvée puis chargée.
C     Le maillage devra également être relu.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur le maillage à partitionner
C     NPART       Nombre de sous-domaines demandés
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_PART (HOBJ, HELEM, NPART)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_PART

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM
      INTEGER NPART

      INCLUDE 'c_bgl.fi'
      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR, IRET
      INTEGER IOB
      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_BGL_HBASE
      HPRNT = NM_BGL_HPRNT(IOB)

C---     APPEL LE PARENT
      IERR  = NM_NBSE_PART(HPRNT, HELEM, NPART)

C---     VERIFIE L'ERREUR ET ASSIGNE LE BON MESSAGE
      IF (ERR_BAD()) THEN
         IRET = C_BGL_ASGMSG(ERR_BUF)
         CALL ERR_AJT(ERR_BUF)
      ENDIF

      NM_BGL_PART = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Renumérote un maillage.
C
C Description:
C     La fonction NM_BGL_RENUM renumérote le maillage HELEM afin d'en
C     réduire la largeur de bande.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     HELEM       Handle sur le maillage à partitionner
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_RENUM (HOBJ, HELEM)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_RENUM

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HELEM

      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_BGL_HBASE
      HPRNT = NM_BGL_HPRNT(IOB)

C---     APPEL LE PARENT
      IERR  = NM_NBSE_RENUM(HPRNT, HELEM)

      NM_BGL_RENUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Construis une renumérotation.
C
C Description:
C     La fonction NM_BGL_GENNUM retourne un renumérotation globale
C     nouvellement créée à partir des données de l'objet. Il est de
C     la responsabilité de l'utilisateur de disposer de HNUM.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C     HNUM        Handle sur la renumérotation
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_GENNUM (HOBJ, HNUM)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_GENNUM

      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER HNUM

      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_BGL_HBASE
      HPRNT = NM_BGL_HPRNT(IOB)

C---     APPEL LE PARENT
      IERR  = NM_NBSE_GENNUM(HPRNT, HNUM)

      NM_BGL_GENNUM = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Ecris la table de redistribution.
C
C Description:
C     La fonction NM_BGL_SAUVE écris la table de redistribution dans le
C     fichier NOMFIC.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C     NOMFIC      Nom du fichier
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION NM_BGL_SAUVE (HOBJ, NOMFIC)
CDEC$ATTRIBUTES DLLEXPORT :: NM_BGL_SAUVE

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) NOMFIC

      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'nmnbse.fi'
      INCLUDE 'nmbgl.fc'

      INTEGER IERR
      INTEGER IOB
      INTEGER HPRNT
C-----------------------------------------------------------------------
D     CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
C-----------------------------------------------------------------------

      IERR = ERR_OK

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - NM_BGL_HBASE
      HPRNT = NM_BGL_HPRNT(IOB)

C---     APPEL LE PARENT
      IERR  = NM_NBSE_SAUVE(HPRNT, NOMFIC)

      NM_BGL_SAUVE = ERR_TYP()
      RETURN
      END
