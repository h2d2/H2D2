C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_BGL_XEQCTR construit un objet.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_BGL_XEQCTR(IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_NM_BGL_XEQCTR

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'nmbgl_ic.fi'
      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_NM_BGL_AID()
            GOTO 9999
         ENDIF
      ENDIF

C-------  EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_BGL'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     CONSTRUIS, INITIALISE ET CHARGE L'OBJET
      HOBJ = 0
      IF (ERR_GOOD()) IERR = NM_BGL_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = NM_BGL_INI(HOBJ)

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
         WRITE(IPRM, '(2A,I12)') 'H', ',', HOBJ
      ENDIF

C<comment>
C  The constructor <b>bgl</b> constructs an object and returns a handle on this object.
C</comment>

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NM_BGL_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_NM_BGL_XEQCTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_NM_BGL_XEQMTH(HOBJ, IMTH, IPRM)
CDEC$ATTRIBUTES DLLEXPORT :: IC_NM_BGL_XEQMTH

      IMPLICIT NONE

      INTEGER HOBJ
      CHARACTER*(*) IMTH
      CHARACTER*(*) IPRM

      INCLUDE 'nmbgl_ic.fi'
      INCLUDE 'nmbgl.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HELEM, HNUM
      INTEGER NPROC
      CHARACTER*(256) NOMFIC
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     METHODES
C     <comment>The method <b>part</b> partitions the finite-element mesh in
C     submeshes, suitable for distributed computing.</comment>
      IF (IMTH .EQ. 'part') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))

C        <comment>Handle on the grid</comment>
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IERR = SP_STRN_TKI(IPRM, ',', 1, HELEM)
         IF (IERR .NE. 0) GOTO 9901
C        <comment>Number of domain (default = number of process)</comment>
         IERR = SP_STRN_TKI(IPRM, ',', 2, NPROC)
         IF (IERR .NE. 0) NPROC = -1
         IERR = NM_BGL_PART(HOBJ, HELEM, NPROC)

C     <comment>The method <b>renum</b> renumbers, taking into account the data in parentheses
C     (h_ele, h_numc, for example).</comment>
      ELSEIF (IMTH .EQ. 'renum') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))

C        <comment>Handle on the grid</comment>
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IERR = SP_STRN_TKI(IPRM, ',', 1, HELEM)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NM_BGL_RENUM(HOBJ, HELEM)

C     <comment>The method <b>gen_num</b> generates the renumbering.</comment>
      ELSEIF (IMTH .EQ. 'gen_num') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))

         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         HNUM = 0
         IERR = NM_BGL_GENNUM(HOBJ, HNUM)
C        <comment>Handle on new renumbering.</comment>
         IF (ERR_GOOD())  WRITE(IPRM, '(2A,I12)') 'H', ',', HNUM

C     <comment>The method <b>save</b> saves the renumbering to the specified file.</comment>
      ELSEIF (IMTH .EQ. 'save') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))

C        <comment>File name</comment>
         IF (SP_STRN_LEN(IPRM) .LE. 0) GOTO 9900
         IERR = SP_STRN_TKS(IPRM, ',', 1, NOMFIC)
         IF (IERR .NE. 0) GOTO 9901
         IERR = NM_BGL_SAUVE(HOBJ, NOMFIC)

C     <comment>The method <b>del</b> deletes the object. The handle shall not be used anymore to reference the object.</comment>
      ELSEIF (IMTH .EQ. 'del') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         IERR = NM_BGL_DTR(HOBJ)

C     <comment>The method <b>print</b> prints information about the object.</comment>
      ELSEIF (IMTH .EQ. 'print') THEN
D        CALL ERR_PRE(NM_BGL_HVALIDE(HOBJ))
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
C         IERR = NM_BGL_PRN(HOBJ)
         CALL LOG_ECRIS('<!-- Test NM_BGL_PRN(HOBJ) -->')

C     <comment>The method <b>help</b> displays the help content for the class.</comment>
      ELSEIF (IMTH .EQ. 'help') THEN
         IF (SP_STRN_LEN(IPRM) .GT. 0) GOTO 9901
         CALL IC_NM_BGL_AID()

      ELSE
         GOTO 9903
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_ATTENDUS'
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9901  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES',': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988
9903  WRITE(ERR_BUF, '(3A)') 'ERR_METHODE_INVALIDE', ': ',
     &                       IMTH(1:SP_STRN_LEN(IMTH))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_NM_BGL_AID()

9999  CONTINUE
      IC_NM_BGL_XEQMTH = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_BGL_REQCLS()
CDEC$ATTRIBUTES DLLEXPORT :: IC_NM_BGL_REQCLS

      IMPLICIT NONE

      INCLUDE 'nmbgl_ic.fi'
C-------------------------------------------------------------------------

C   <comment>
C   The class <b>bgl</b> represents the PT-Scotch algorithm to partition
C   and the BGL Sloan algorithm to renumber a finite-element grid.
C   </comment>
      IC_NM_BGL_REQCLS = 'bgl'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_NM_BGL_REQHDL()
CDEC$ATTRIBUTES DLLEXPORT :: IC_NM_BGL_REQHDL

      IMPLICIT NONE

      INCLUDE 'nmbgl_ic.fi'
      INCLUDE 'nmbgl.fi'
C-------------------------------------------------------------------------

      IC_NM_BGL_REQHDL = NM_BGL_REQHBASE()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     La fonction IC_NM_BGL_AID écris dans le log l'aide relative
C     à la commande.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_NM_BGL_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('nmbgl_ic.hlp')

      RETURN
      END

