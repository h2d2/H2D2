//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_slum
// Entry point: extern "C" void fake_dll_slvr_slum()
//
// This file is generated automatically, any change will be lost.
// Generated 2018-08-12 15:30:16.958000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_SLUM
F_PROT(IC_SLUM_CMD, ic_slum_cmd);
F_PROT(IC_SLUM_REQCMD, ic_slum_reqcmd);
 
// ---  class MR_SLUM
F_PROT(MR_SLUM_000, mr_slum_000);
F_PROT(MR_SLUM_999, mr_slum_999);
F_PROT(MR_SLUM_CTR, mr_slum_ctr);
F_PROT(MR_SLUM_DTR, mr_slum_dtr);
F_PROT(MR_SLUM_INI, mr_slum_ini);
F_PROT(MR_SLUM_RST, mr_slum_rst);
F_PROT(MR_SLUM_REQHBASE, mr_slum_reqhbase);
F_PROT(MR_SLUM_HVALIDE, mr_slum_hvalide);
F_PROT(MR_SLUM_ESTDIRECTE, mr_slum_estdirecte);
F_PROT(MR_SLUM_ASMMTX, mr_slum_asmmtx);
F_PROT(MR_SLUM_FCTMTX, mr_slum_fctmtx);
F_PROT(MR_SLUM_RESMTX, mr_slum_resmtx);
F_PROT(MR_SLUM_XEQ, mr_slum_xeq);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_slum()
{
   static char libname[] = "slvr_slum";
 
   // ---  class IC_SLUM
   F_RGST(IC_SLUM_CMD, ic_slum_cmd, libname);
   F_RGST(IC_SLUM_REQCMD, ic_slum_reqcmd, libname);
 
   // ---  class MR_SLUM
   F_RGST(MR_SLUM_000, mr_slum_000, libname);
   F_RGST(MR_SLUM_999, mr_slum_999, libname);
   F_RGST(MR_SLUM_CTR, mr_slum_ctr, libname);
   F_RGST(MR_SLUM_DTR, mr_slum_dtr, libname);
   F_RGST(MR_SLUM_INI, mr_slum_ini, libname);
   F_RGST(MR_SLUM_RST, mr_slum_rst, libname);
   F_RGST(MR_SLUM_REQHBASE, mr_slum_reqhbase, libname);
   F_RGST(MR_SLUM_HVALIDE, mr_slum_hvalide, libname);
   F_RGST(MR_SLUM_ESTDIRECTE, mr_slum_estdirecte, libname);
   F_RGST(MR_SLUM_ASMMTX, mr_slum_asmmtx, libname);
   F_RGST(MR_SLUM_FCTMTX, mr_slum_fctmtx, libname);
   F_RGST(MR_SLUM_RESMTX, mr_slum_resmtx, libname);
   F_RGST(MR_SLUM_XEQ, mr_slum_xeq, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
