C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION IC_SLUM_CMD(IPRM)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SLUM_CMD
CDEC$ ENDIF

      IMPLICIT NONE

      CHARACTER*(*) IPRM

      INCLUDE 'icslum.fi'
      INCLUDE 'mrslum.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
      INCLUDE 'spstrn.fi'

      INTEGER IERR
      INTEGER HOBJ
      INTEGER HRES
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     TRAITEMENT SPÉCIAL POUR AFFICHER L'AIDE
      IF (SP_STRN_LEN(IPRM) .GT. 0) THEN
         IF (IPRM(1:SP_STRN_LEN(IPRM)) .EQ. 'help') THEN
            CALL IC_SLUM_AID()
            GOTO 9999
         ENDIF
      ENDIF

C---     EN-TETE DE COMMANDE
      LOG_BUF = ' '
      CALL LOG_ECRIS(LOG_BUF)
      WRITE (LOG_BUF,'(A)') 'MSG_CMD_SUPERLU_MT'
      CALL LOG_ECRIS(LOG_BUF)
      CALL LOG_INCIND()

C---     CONTROLE LES PARAM
      IF (SP_STRN_LEN(IPRM) .NE. 0) GOTO 9900

C---     CONSTRUIS ET INITIALISE L'OBJET CONCRET
      HRES = 0
      IF (ERR_GOOD()) IERR = MR_SLUM_CTR(HRES)
      IF (ERR_GOOD()) IERR = MR_SLUM_INI(HRES)

C---     Construis et initialise le parent-proxy
      HOBJ = 0
      IF (ERR_GOOD()) IERR = MR_PRXY_CTR(HOBJ)
      IF (ERR_GOOD()) IERR = MR_PRXY_INI(HOBJ, HRES)

C---      IMPRESSION DES PARAMETRES DU BLOC
      IF (ERR_GOOD()) THEN
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_PRECOND#<25>#= ',
C     &                                 NPREC
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_REDEMARAGES#<25>#= ',
C     &                                 NRDEM
C         CALL LOG_ECRIS(LOG_BUF)
C         WRITE (LOG_BUF,'(15X,A,I12)') 'MSG_NBR_ITERATIONS#<25>#= ',
C     &                                 NITER
C         CALL LOG_ECRIS(LOG_BUF)
      ENDIF

C---     RETOURNE LA HANDLE
      IF (ERR_GOOD()) THEN
C        <comment>Return value: Handle on the matrix solver object</comment>
         WRITE(IPRM, '(2A,I12)') 'H', ',', HRES
      ENDIF

      GOTO 9999
C------------------------------------------------------------------------
9900  WRITE(ERR_BUF, '(3A)') 'ERR_PARAMETRES_INVALIDES', ': ',
     &                       IPRM(1:SP_STRN_LEN(IPRM))
      CALL ERR_ASG(ERR_ERR, ERR_BUF)
      GOTO 9988

9988  CONTINUE
      CALL IC_SLUM_AID()

9999  CONTINUE
      CALL LOG_DECIND()
      IC_SLUM_CMD = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION IC_SLUM_REQCMD()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: IC_SLUM_REQCMD
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'icslum.fi'
C-------------------------------------------------------------------------

C<comment>
C  The command <b>superlu_mt</b> constructs a matrix solver, based on the
C  SuperLU algorithm (external package). It is a general purpose library for
C  the direct solution of large, sparse, non-symmetric systems of linear
C  equations on high performance machines. <b>_sp</b> stands for single process
C  but will benefit from parallelized linear algebra kernels (BLAS).
C</comment>

      IC_SLUM_REQCMD = 'superlu_mt'

      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      SUBROUTINE IC_SLUM_AID()

      IMPLICIT NONE

      INCLUDE 'log.fi'

      INTEGER IERR
C-------------------------------------------------------------------------

      IERR = LOG_ECRISFIC('icslum.hlp')

      RETURN
      END

