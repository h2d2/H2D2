C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LMGO_L3L_ASMESCL
C
C Description:
C     La fonction LMGO_L3L_ASMESCL assemble les éléments de surface
C     des conditions limites. Pour un élément L3L, il suffit que les
C     deux noeuds sommets d'un élément se trouvent dans la liste des
C     noeuds de CL pour que l'élément soit reconnu.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LMGO_L3L_ASMESCL(VCORG,
     &                          KNGS,
     &                          KCLLIM,
     &                          KCLNOD,
     &                          KCLELE,
     &                          VCLDST)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: LMGO_L3L_ASMESCL
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'eacmmn.fc'
      INCLUDE 'egcmmn.fc'

      REAL*8  VCORG (EG_CMMN_NDIM, EG_CMMN_NNL)
      INTEGER KNGS  (EG_CMMN_NCELS,EG_CMMN_NELS)
      INTEGER KCLLIM( 7, EG_CMMN_NCLLIM)
      INTEGER KCLNOD(    EG_CMMN_NCLNOD)
      INTEGER KCLELE(    EG_CMMN_NCLELE)
      REAL*8  VCLDST(    EG_CMMN_NCLNOD)

      INCLUDE 'lmgo_l3l.fi'
      INCLUDE 'err.fi'
      INCLUDE 'log.fi'
C-----------------------------------------------------------------------

      CALL LOG_TODO('LMGO_L3L_ASMESCL jamais teste, SUREMENT FAUX!!!')
      CALL ERR_ASR(.FALSE.)

      LMGO_L3L_ASMESCL = ERR_TYP()
      RETURN
      END
