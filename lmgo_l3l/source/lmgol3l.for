C************************************************************************
C --- Copyright (c) INRS 2014-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C
C Interface:
C   H2D2 Module: LM
C      H2D2 Class: LM_GOL3L
C         FTN (Sub)Module: LM_GOL3L_M
C            Public:
C            Private:
C               SUBROUTINE LM_GOL3L_CTR
C            
C            FTN Type: LM_GOL3L_T
C   Public:
C     INTEGER LM_GOL3L_DTR
C   Private:
C     INTEGER LM_GOL3L_INIPRMS
C
C************************************************************************

      MODULE LM_GOL3L_M

      USE LM_GEOM_M, ONLY : LM_GEOM_T
      USE LM_GDTA_M, ONLY : LM_GDTA_T
      IMPLICIT NONE

      PUBLIC

      !========================================================================
      ! ---   Classe
      !========================================================================
      TYPE, EXTENDS(LM_GEOM_T) :: LM_GOL3L_T
!        pass
      CONTAINS
         ! ---  Méthodes virtuelles
         PROCEDURE, PUBLIC :: DTR      => LM_GOL3L_DTR
         PROCEDURE, PUBLIC :: ASMESCL  => LM_GOL3L_ASMESCL
         !!!PROCEDURE, PUBLIC :: ASMPEAU  => LM_GOL3L_ASMPEAU
         PROCEDURE, PUBLIC :: CLCERR   => LM_GOL3L_CLCERR
         PROCEDURE, PUBLIC :: CLCJELS  => LM_GOL3L_CLCJELS
         PROCEDURE, PUBLIC :: CLCJELV  => LM_GOL3L_CLCJELV
         PROCEDURE, PUBLIC :: CLCSPLT  => LM_GOL3L_CLCSPLT
         PROCEDURE, PUBLIC :: INTRP    => LM_GOL3L_INTRP
         PROCEDURE, PUBLIC :: LCLELV   => LM_GOL3L_LCLELV

         ! ---  Méthodes privées
         PROCEDURE, PRIVATE :: INIPRMS => LM_GOL3L_INIPRMS
         PROCEDURE, PRIVATE :: DUMMY   => LM_GOL3L_DUMMY
      END TYPE LM_GOL3L_T

      !========================================================================
      ! ---  Constructor - Destructor
      !========================================================================
      PUBLIC :: LM_GOL3L_CTR
      PUBLIC :: DEL
      INTERFACE DEL
         PROCEDURE :: LM_GOL3L_DTR
      END INTERFACE DEL

      !========================================================================
      ! ---  Sub-module
      !========================================================================
      INTERFACE
!!!         MODULE INTEGER FUNCTION LM_GOL3L_ASMPEAU(SELF)
!!!            IMPORT :: LM_GOL3L_T
!!!            CLASS(LM_GOL3L_T), INTENT(IN) :: SELF
!!!         END FUNCTION LM_GOL3L_ASMPEAU

         MODULE INTEGER FUNCTION LM_GOL3L_CLCJELV(SELF)
            CLASS(LM_GOL3L_T), INTENT(INOUT), TARGET :: SELF
         END FUNCTION LM_GOL3L_CLCJELV

      END INTERFACE


      CONTAINS

C************************************************************************
C Sommaire:
C
C Description: Constructeur de la classe
C
C Entrée:
C     HKID     Handle sur l'héritier qui a alloué la structure
C
C Sortie:
C     HOBJ     Handle sur l'objet créé
C
C Notes:
C     l'élément est concret
C************************************************************************
      FUNCTION LM_GOL3L_CTR() RESULT(SELF)

      USE LM_GEOM_M, ONLY: LM_GEOM_CTR

      INCLUDE 'err.fi'

      INTEGER IERR, IRET
      CLASS(LM_GEOM_T), POINTER :: OPRNT
      TYPE (LM_GOL3L_T), POINTER :: SELF
C------------------------------------------------------------------------

      IERR = ERR_OK

C---     Alloue la structure
      SELF => NULL()
         ALLOCATE (SELF, STAT=IRET)
         IF (IRET .NE. 0) GOTO 9900

C---     Construis le parent
      IF (ERR_GOOD()) OPRNT => LM_GEOM_CTR(SELF)

C---     Initialise les dimensions
      IF (ERR_GOOD()) IERR = SELF%INIPRMS()

      GOTO 9999
C------------------------------------------------------------------------
9900  CALL ERR_ASG(ERR_ERR, 'ERR_ALLOCATION_MEMOIRE')
      GOTO 9999

9999  CONTINUE
      RETURN
      END FUNCTION LM_GOL3L_CTR

C************************************************************************
C Sommaire:
C
C Description:
C     Le destructeur LM_GOL3L_DTR est la partie virtuelle du processus
C     de destruction. Il est appelé par le parent sur appel de DEL.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOL3L_DTR(SELF)

      USE LM_GEOM_M, ONLY: LM_GEOM_DTR

      CLASS(LM_GOL3L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'err.fi'

      INTEGER IERR
      CLASS(LM_GOL3L_T), POINTER :: SELF_P
C------------------------------------------------------------------------

C---     Détruis le parent
      IERR = LM_GEOM_DTR(SELF)

C---     Désalloue la structure
      IF (ERR_GOOD()) THEN
         SELF_P => SELF
         DEALLOCATE(SELF_P)
         SELF_P => NULL()
      ENDIF

      LM_GOL3L_DTR = ERR_TYP()
      RETURN
      END FUNCTION LM_GOL3L_DTR

C************************************************************************
C Sommaire:
C
C Description:
C     La méthode LM_GOL3L_INIPRMS initialise les paramètres de
C     dimension de l'objet. Ils sont vus comme des attributs statiques.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOL3L_INIPRMS(SELF)

      CLASS(LM_GOL3L_T), INTENT(INOUT), TARGET :: SELF

      INCLUDE 'egtpgeo.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
C-----------------------------------------------------------------------

C---     Récupère les données
      GDTA => SELF%GDTA

C---     Initialise les paramètres invariant
      GDTA%NDIM = 1

      GDTA%ITPEV = EG_TPGEO_L3L
      GDTA%NNELV = 3
      GDTA%NCELV = 3        ! NNELV
      GDTA%NDJV  = 2        !

      GDTA%ITPES = EG_TPGEO_P1
      GDTA%NNELS = 1
      GDTA%NCELS = 1 + 1    ! NNELS + 1
      GDTA%NDJS  = 1        ! 1

      GDTA%ITPEZ = EG_TPGEO_L2
      GDTA%NNELZ = 2
      GDTA%NCELZ = 2        ! NNELZ
      GDTA%NDJZ  = 0        ! NDIM*2 + 1
      GDTA%NEV2EZ= 2

      LM_GOL3L_INIPRMS = ERR_TYP()
      RETURN
      END FUNCTION LM_GOL3L_INIPRMS

C************************************************************************
C Sommaire:  Fonction virtuelles non implantées
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      INTEGER FUNCTION LM_GOL3L_DUMMY(SELF)

      CLASS(LM_GOL3L_T), INTENT(IN) :: SELF

      INCLUDE 'err.fi'
C-----------------------------------------------------------------------

      CALL ERR_ASG(ERR_FTL, 'Function shall never be called')

      LM_GOL3L_DUMMY = ERR_TYP()
      RETURN
      END FUNCTION LM_GOL3L_DUMMY

      INTEGER FUNCTION LM_GOL3L_ASMESCL(SELF)
         CLASS(LM_GOL3L_T), INTENT(INOUT), TARGET :: SELF
         LM_GOL3L_ASMESCL = SELF%DUMMY()
      END FUNCTION LM_GOL3L_ASMESCL

      INTEGER FUNCTION LM_GOL3L_CLCERR(SELF, VFRM, VHESS, VHNRM, ITPCLC)
         CLASS(LM_GOL3L_T), INTENT(IN), TARGET :: SELF
         REAL*8,  INTENT(IN)  :: VFRM (:)
         REAL*8,  INTENT(OUT) :: VHESS(:,:)
         REAL*8,  INTENT(OUT) :: VHNRM(:)
         INTEGER, INTENT(IN)  :: ITPCLC
         LM_GOL3L_CLCERR = SELF%DUMMY()
      END FUNCTION LM_GOL3L_CLCERR

      INTEGER FUNCTION LM_GOL3L_CLCJELS(SELF)
         CLASS(LM_GOL3L_T), INTENT(INOUT), TARGET :: SELF
         LM_GOL3L_CLCJELS = SELF%DUMMY()
      END FUNCTION LM_GOL3L_CLCJELS

      INTEGER FUNCTION LM_GOL3L_CLCSPLT(SELF, KNGZ)
         CLASS(LM_GOL3L_T), INTENT(IN), TARGET :: SELF
         INTEGER, INTENT(OUT) :: KNGZ(:, :)
         LM_GOL3L_CLCSPLT = SELF%DUMMY()
      END FUNCTION LM_GOL3L_CLCSPLT

      INTEGER FUNCTION LM_GOL3L_INTRP(SELF,
     &                           KELE, VCORE,
     &                           INDXS, VSRC,
     &                           INDXD, VDST)
         CLASS(LM_GOL3L_T), INTENT(IN), TARGET :: SELF
         INTEGER, INTENT(IN)  :: KELE (:)
         REAL*8,  INTENT(IN)  :: VCORE(:, :)
         INTEGER, INTENT(IN)  :: INDXS
         REAL*8,  INTENT(IN)  :: VSRC (:, :)
         INTEGER, INTENT(IN)  :: INDXD
         REAL*8,  INTENT(OUT) :: VDST (:, :)
         LM_GOL3L_INTRP = SELF%DUMMY()
      END FUNCTION LM_GOL3L_INTRP

      INTEGER FUNCTION LM_GOL3L_LCLELV(SELF, TOL, VCORP, KELEP, VCORE)
         CLASS(LM_GOL3L_T), INTENT(IN), TARGET :: SELF
         REAL*8,  INTENT(IN)  :: TOL
         REAL*8,  INTENT(IN)  :: VCORP(:, :)
         INTEGER, INTENT(OUT) :: KELEP(:)
         REAL*8,  INTENT(OUT) :: VCORE(:, :)
         LM_GOL3L_LCLELV = SELF%DUMMY()
      END FUNCTION LM_GOL3L_LCLELV

      END MODULE LM_GOL3L_M

