C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:  LM_GOL3L_ASMPEAU
C
C Description:
C     La fonction LM_GOL3L_ASMPEAU assemble la peau pour des
C     éléments de volume de type L3L.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION LM_GOL3L_ASMPEAU(HOBJ,
     &                          KPNT,
     &                          NLIEN,
     &                          KLIEN,
     &                          INEXT)

      USE LM_GOL3L_M
      IMPLICIT NONE

      INTEGER HOBJ
      INTEGER KPNT (*)
      INTEGER NLIEN
      INTEGER KLIEN(4,*)
      INTEGER INEXT

      INCLUDE 'lmgol3l.fi'
      INCLUDE 'splstc.fi'
      INCLUDE 'err.fi'

      TYPE (LM_GDTA_T), POINTER :: GDTA
      INTEGER, DIMENSION(:,:), POINTER :: KNGV
      INTEGER NNL, NCELV, NELLV

      INTEGER IE
      INTEGER IERR
      INTEGER ICLEF
      INTEGER INFO(3)
      LOGICAL NVLNK
C----------------------------------------------------------------------
D     CALL ERR_PRE(LM_GOL3L_HVALIDE(HOBJ))
C----------------------------------------------------------------------

      CALL LOG_TODO('LM_GOL3L_ASMPEAU   jamais teste, SUREMENT FAUX!!!')

C---     Récupère les attributs
      GDTA => LM_GOL3L_REQGDTA(HOBJ)
D     CALL ERR_PRE(GDTA%NNL   .GE. 1)
D     CALL ERR_PRE(GDTA%NELLV .GE. 1)
D     CALL ERR_PRE(GDTA%NCELV .EQ. 3)
      NNL   = GDTA%NNL
      NELLV = GDTA%NNELV
      KNGV  => GDTA%KNGV

C---     Boucle sur les éléments
      NVLNK = .FALSE.
      INFO(2) = 0
      DO IE=1,NELLV

C---        NOEUD 1
         INFO(1) = KNGV(1, IE)
         INFO(3) = IE
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

C---        NOEUD 2
         INFO(1) = KNGV(3, IE)
         INFO(3) = IE
         IERR = SP_LSTC_AJTCHN (NNL, KPNT, 3, NLIEN, KLIEN, INEXT,
     &                          ICLEF, INFO, 2, NVLNK)
         IF (ERR_BAD()) GOTO 9999

      ENDDO

9999  CONTINUE
      LM_GOL3L_ASMPEAU = ERR_TYP()
      RETURN
      END

