//************************************************************************
// H2D2 - External declaration of public symbols
// Module: tool_gbp
// Entry point: extern "C" void fake_dll_tool_gbp()
//
// This file is generated automatically, any change will be lost.
// Generated 2017-11-09 13:59:09.943000
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class TL_GBP
F_PROT(TL_GBP_000, tl_gbp_000);
F_PROT(TL_GBP_999, tl_gbp_999);
F_PROT(TL_GBP_CTR, tl_gbp_ctr);
F_PROT(TL_GBP_DTR, tl_gbp_dtr);
F_PROT(TL_GBP_INI, tl_gbp_ini);
F_PROT(TL_GBP_RST, tl_gbp_rst);
F_PROT(TL_GBP_REQHBASE, tl_gbp_reqhbase);
F_PROT(TL_GBP_HVALIDE, tl_gbp_hvalide);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_tool_gbp()
{
   static char libname[] = "tool_gbp";
 
   // ---  class TL_GBP
   F_RGST(TL_GBP_000, tl_gbp_000, libname);
   F_RGST(TL_GBP_999, tl_gbp_999, libname);
   F_RGST(TL_GBP_CTR, tl_gbp_ctr, libname);
   F_RGST(TL_GBP_DTR, tl_gbp_dtr, libname);
   F_RGST(TL_GBP_INI, tl_gbp_ini, libname);
   F_RGST(TL_GBP_RST, tl_gbp_rst, libname);
   F_RGST(TL_GBP_REQHBASE, tl_gbp_reqhbase, libname);
   F_RGST(TL_GBP_HVALIDE, tl_gbp_hvalide, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
