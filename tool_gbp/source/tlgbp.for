C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      BLOCK DATA TL_GBP_DATA

      IMPLICIT NONE

      INCLUDE 'tlgbp.fc'
C------------------------------------------------------------------------

      DATA TL_GBP_HOBJ /0/
      DATA TL_GBP_NCNT /0/

      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TL_GBP_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tlgbp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'

      INTEGER IERR
      INTEGER HOBJ
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(TL_GBP_NOBJMAX,
     &                   TL_GBP_HBASE,
     &                   'Google Breakpad')

      IERR = TL_GBP_CTR(HOBJ)

      TL_GBP_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C     Il est possible qu'une erreur survienne avant le CTR soit appelé.
C************************************************************************
      FUNCTION TL_GBP_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tlgbp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'

      INTEGER  IERR
      INTEGER  HOBJ
      EXTERNAL TL_GBP_DTR
C------------------------------------------------------------------------

      HOBJ = TL_GBP_HOBJ
      IF (TL_GBP_HVALIDE(HOBJ)) IERR = TL_GBP_DTR(HOBJ)

      IERR = OB_OBJC_999(TL_GBP_NOBJMAX,
     &                   TL_GBP_HBASE,
     &                   TL_GBP_DTR)

      TL_GBP_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TL_GBP_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tlgbp.fi'
      INCLUDE 'c_gbp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'

      INTEGER IERR, IRET
C------------------------------------------------------------------------

      HOBJ = TL_GBP_HOBJ
      IF (HOBJ .EQ. 0) THEN
D        CALL ERR_ASR(TL_GBP_HOBJ .EQ. 0)
         IERR = OB_OBJC_CTR(HOBJ,
     &                      TL_GBP_NOBJMAX,
     &                      TL_GBP_HBASE)
         TL_GBP_HOBJ = HOBJ
      ENDIF

      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(TL_GBP_HVALIDE(HOBJ))
         IF (TL_GBP_NCNT .EQ. 0) IRET = C_GBP_INIT()
         TL_GBP_NCNT = TL_GBP_NCNT + 1
      ENDIF

      TL_GBP_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TL_GBP_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tlgbp.fi'
      INCLUDE 'c_gbp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'

      INTEGER IERR, IREt
C-----------------------------------------------------------------------
D     CALL ERR_PRE(TL_GBP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      TL_GBP_NCNT = TL_GBP_NCNT - 1
      IF (TL_GBP_NCNT .EQ. 0) THEN
         IRET = C_GBP_RELEASE()
D        CALL ERR_ASR(HOBJ .EQ. TL_GBP_HOBJ)
         IERR = TL_GBP_RST(HOBJ)
         IERR = OB_OBJC_DTR(HOBJ,
     &                      TL_GBP_NOBJMAX,
     &                      TL_GBP_HBASE)
      ENDIF

      TL_GBP_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TL_GBP_INI(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tlgbp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(TL_GBP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      TL_GBP_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION TL_GBP_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tlgbp.fi'
      INCLUDE 'err.fi'
      INCLUDE 'tlgbp.fc'
C------------------------------------------------------------------------
D     CALL ERR_PRE(TL_GBP_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      TL_GBP_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction TL_GBP_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TL_GBP_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'tlgbp.fi'
      INCLUDE 'tlgbp.fc'
C------------------------------------------------------------------------

      TL_GBP_REQHBASE = TL_GBP_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction TL_GBP_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION TL_GBP_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: TL_GBP_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'tlgbp.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'tlgbp.fc'
C------------------------------------------------------------------------

      TL_GBP_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  TL_GBP_NOBJMAX,
     &                                  TL_GBP_HBASE)
      RETURN
      END


