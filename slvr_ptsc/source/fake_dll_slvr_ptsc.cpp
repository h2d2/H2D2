//************************************************************************
// H2D2 - External declaration of public symbols
// Module: slvr_ptsc
// Entry point: extern "C" void fake_dll_slvr_ptsc()
//
// This file is generated automatically, any change will be lost.
// Generated 2019-01-30 08:47:12.030979
//************************************************************************
 
#include "cconfig.h"
 
 
#ifdef FAKE_DLL
 
 
#undef STRINGIF2
#undef STRINGIFY
#undef F_SMBL
#undef F_NAME
#undef F_PROT
#undef F_RGST
#undef M_SMBL
#undef M_NAME
#undef M_PROT
#undef M_RGST
 
#define STRINGIF2(f) # f
#define STRINGIFY(f) STRINGIF2( f )
 
#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)
#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )
#define F_PROT(F, f) void F_SMBL(F, f)()
#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)
 
#ifdef F2C_CONF_DECOR_MDL
#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)
#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )
#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()
#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)
#else
#  define M_PROT(M, m, F, f)
#  define M_RGST(M, m, F, f, l)
#endif
 
#ifdef __cplusplus
extern "C"
{
#endif
 
 
// ---  class IC_MR_PTSC
F_PROT(IC_MR_PTSC_XEQCTR, ic_mr_ptsc_xeqctr);
F_PROT(IC_MR_PTSC_XEQMTH, ic_mr_ptsc_xeqmth);
F_PROT(IC_MR_PTSC_REQCLS, ic_mr_ptsc_reqcls);
F_PROT(IC_MR_PTSC_REQHDL, ic_mr_ptsc_reqhdl);
 
// ---  class MR_PTSC
F_PROT(MR_PTSC_000, mr_ptsc_000);
F_PROT(MR_PTSC_999, mr_ptsc_999);
F_PROT(MR_PTSC_CTR, mr_ptsc_ctr);
F_PROT(MR_PTSC_DTR, mr_ptsc_dtr);
F_PROT(MR_PTSC_INI, mr_ptsc_ini);
F_PROT(MR_PTSC_RST, mr_ptsc_rst);
F_PROT(MR_PTSC_REQHBASE, mr_ptsc_reqhbase);
F_PROT(MR_PTSC_HVALIDE, mr_ptsc_hvalide);
F_PROT(MR_PTSC_ESTDIRECTE, mr_ptsc_estdirecte);
F_PROT(MR_PTSC_XEQ, mr_ptsc_xeq);
 
// ---  class MX_PTSC
F_PROT(MX_PTSC_000, mx_ptsc_000);
F_PROT(MX_PTSC_999, mx_ptsc_999);
F_PROT(MX_PTSC_CTR, mx_ptsc_ctr);
F_PROT(MX_PTSC_DTR, mx_ptsc_dtr);
F_PROT(MX_PTSC_INI, mx_ptsc_ini);
F_PROT(MX_PTSC_RST, mx_ptsc_rst);
F_PROT(MX_PTSC_HVALIDE, mx_ptsc_hvalide);
F_PROT(MX_PTSC_ASMKE, mx_ptsc_asmke);
F_PROT(MX_PTSC_DIMMAT, mx_ptsc_dimmat);
F_PROT(MX_PTSC_MULVAL, mx_ptsc_mulval);
F_PROT(MX_PTSC_MULVEC, mx_ptsc_mulvec);
F_PROT(MX_PTSC_REQNEQL, mx_ptsc_reqneql);
F_PROT(MX_PTSC_REQLCOLG, mx_ptsc_reqlcolg);
F_PROT(MX_PTSC_REQLLING, mx_ptsc_reqlling);
F_PROT(MX_PTSC_REQXMAT, mx_ptsc_reqxmat);
 
void fake_dll_lib_reg(void (*)(), const char*, const char*);
 
void fake_dll_slvr_ptsc()
{
   static char libname[] = "slvr_ptsc";
 
   // ---  class IC_MR_PTSC
   F_RGST(IC_MR_PTSC_XEQCTR, ic_mr_ptsc_xeqctr, libname);
   F_RGST(IC_MR_PTSC_XEQMTH, ic_mr_ptsc_xeqmth, libname);
   F_RGST(IC_MR_PTSC_REQCLS, ic_mr_ptsc_reqcls, libname);
   F_RGST(IC_MR_PTSC_REQHDL, ic_mr_ptsc_reqhdl, libname);
 
   // ---  class MR_PTSC
   F_RGST(MR_PTSC_000, mr_ptsc_000, libname);
   F_RGST(MR_PTSC_999, mr_ptsc_999, libname);
   F_RGST(MR_PTSC_CTR, mr_ptsc_ctr, libname);
   F_RGST(MR_PTSC_DTR, mr_ptsc_dtr, libname);
   F_RGST(MR_PTSC_INI, mr_ptsc_ini, libname);
   F_RGST(MR_PTSC_RST, mr_ptsc_rst, libname);
   F_RGST(MR_PTSC_REQHBASE, mr_ptsc_reqhbase, libname);
   F_RGST(MR_PTSC_HVALIDE, mr_ptsc_hvalide, libname);
   F_RGST(MR_PTSC_ESTDIRECTE, mr_ptsc_estdirecte, libname);
   F_RGST(MR_PTSC_XEQ, mr_ptsc_xeq, libname);
 
   // ---  class MX_PTSC
   F_RGST(MX_PTSC_000, mx_ptsc_000, libname);
   F_RGST(MX_PTSC_999, mx_ptsc_999, libname);
   F_RGST(MX_PTSC_CTR, mx_ptsc_ctr, libname);
   F_RGST(MX_PTSC_DTR, mx_ptsc_dtr, libname);
   F_RGST(MX_PTSC_INI, mx_ptsc_ini, libname);
   F_RGST(MX_PTSC_RST, mx_ptsc_rst, libname);
   F_RGST(MX_PTSC_HVALIDE, mx_ptsc_hvalide, libname);
   F_RGST(MX_PTSC_ASMKE, mx_ptsc_asmke, libname);
   F_RGST(MX_PTSC_DIMMAT, mx_ptsc_dimmat, libname);
   F_RGST(MX_PTSC_MULVAL, mx_ptsc_mulval, libname);
   F_RGST(MX_PTSC_MULVEC, mx_ptsc_mulvec, libname);
   F_RGST(MX_PTSC_REQNEQL, mx_ptsc_reqneql, libname);
   F_RGST(MX_PTSC_REQLCOLG, mx_ptsc_reqlcolg, libname);
   F_RGST(MX_PTSC_REQLLING, mx_ptsc_reqlling, libname);
   F_RGST(MX_PTSC_REQXMAT, mx_ptsc_reqxmat, libname);
}
 
#ifdef __cplusplus
}
#endif
 
#endif    // FAKE_DLL
 
