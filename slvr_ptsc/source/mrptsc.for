C************************************************************************
C --- Copyright (c) INRS 2003-2017
C --- Institut National de la Recherche Scientifique (INRS)
C ---
C --- Distributed under the GNU Lesser General Public License, Version 3.0.
C --- See accompanying file LICENSE.txt.
C************************************************************************

C************************************************************************
C Fichier: $Id$
C Sousroutines:
C************************************************************************

C************************************************************************
C Sommaire: Initialise l'objet
C
C Description:
C     La fonction <code>MR_PTSC_000(...)</code> initialise les tables
C     internes de l'objet. Elle doit être appelée avant toute utilisation
C     des fonctionnalités des objets.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_000()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_000
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER IERR
      INTEGER I
C------------------------------------------------------------------------

      IERR = OB_OBJC_000(MR_PTSC_NOBJMAX,
     &                   MR_PTSC_HBASE,
     &                   'Solver PetSC')

      DO I=1, MR_PTSC_NOBJMAX
         MR_PTSC_HMTX(I) = 0
      ENDDO

      MR_PTSC_000 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_999()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_999
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER  IERR
      EXTERNAL MR_PTSC_DTR
C------------------------------------------------------------------------

      IERR = OB_OBJC_999(MR_PTSC_NOBJMAX,
     &                   MR_PTSC_HBASE,
     &                   MR_PTSC_DTR)

      MR_PTSC_999 = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Constructeur de l'objet
C     Recherche la prochaine case de libre
C     Retourne l'indice de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PTSC_CTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_CTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER IERR
      INTEGER IOB
C------------------------------------------------------------------------

      IERR = OB_OBJC_CTR(HOBJ,
     &                   MR_PTSC_NOBJMAX,
     &                   MR_PTSC_HBASE)
      IF (ERR_GOOD()) THEN
D        CALL ERR_ASR(MR_PTSC_HVALIDE(HOBJ))
         IOB = HOBJ - MR_PTSC_HBASE
      ENDIF

      MR_PTSC_CTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Destructeur de l'objet
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PTSC_DTR(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_DTR
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER  IERR
C-----------------------------------------------------------------------
D     CALL ERR_PRE(MR_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

      IERR = MR_PTSC_RST(HOBJ)
      IERR = OB_OBJC_DTR(HOBJ,
     &                   MR_PTSC_NOBJMAX,
     &                   MR_PTSC_HBASE)

      MR_PTSC_DTR = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C     Initialise et dimensionne
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PTSC_INI(HOBJ, FCFG)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_INI
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER       HOBJ
      CHARACTER*(*) FCFG

      INCLUDE 'mrptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'mxptsc.fi'
      INCLUDE 'spstrn.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX

      INTEGER F_PTSC_ERRMSG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PTSC_HVALIDE(HOBJ))
D     CALL ERR_PRE(SP_STRN_LEN(FCFG) .GT. 0)
C------------------------------------------------------------------------

C---     RESET LES DONNEES
      IERR = MR_PTSC_RST(HOBJ)

C---     INITIALISE PETSc
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_INITIALIZE(FCFG)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     INITIALISE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_PTSC_CTR(HMTX)
      IF (ERR_GOOD()) IERR = MX_PTSC_INI(HMTX)

C---     ASSIGNE LES ATTRIBUTS
      IF (ERR_GOOD()) THEN
         IOB = HOBJ - MR_PTSC_HBASE
         MR_PTSC_HMTX(IOB) = HMTX
      ENDIF

      MR_PTSC_INI = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C
C************************************************************************
      FUNCTION MR_PTSC_RST(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_RST
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'mxptsc.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER IOB
      INTEGER IERR
      INTEGER HMTX

      INTEGER F_PTSC_ERRMSG
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB  = HOBJ - MR_PTSC_HBASE
      HMTX = MR_PTSC_HMTX(IOB)

C---     DETRUIS LA MATRICE
      IF (MX_PTSC_HVALIDE(HMTX)) IERR = MX_PTSC_DTR(HMTX)

C---     RESET PETSc
      IERR = C_PTSC_FINALIZE()
      IERR = F_PTSC_ERRMSG(IERR)

C---     RESET LES ATTRIBUTS
      MR_PTSC_HMTX(IOB) = 0

      MR_PTSC_RST = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire: Retourne HBASE.
C
C Description:
C     La fonction MR_PTSC_REQHBASE retourne l'ID unique qui identifie
C     la classe.
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_REQHBASE()
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_REQHBASE
CDEC$ ENDIF

      IMPLICIT NONE

      INCLUDE 'mrptsc.fi'
      INCLUDE 'mrptsc.fc'
C------------------------------------------------------------------------

      MR_PTSC_REQHBASE = MR_PTSC_HBASE
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si l'objet est valide
C
C Description:
C     La fonction MR_PTSC_HVALIDE permet de valider un objet. Elle
C     retourne .TRUE. si le handle qui lui est passé est valide.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_HVALIDE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_HVALIDE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrptsc.fi'
      INCLUDE 'obobjc.fi'
      INCLUDE 'mrptsc.fc'
C------------------------------------------------------------------------

      MR_PTSC_HVALIDE = OB_OBJC_HVALIDE(HOBJ,
     &                                  MR_PTSC_NOBJMAX,
     &                                  MR_PTSC_HBASE)
      RETURN
      END

C************************************************************************
C Sommaire: Retourne .TRUE. si la méthode de résolution est directe
C
C Description:
C     La fonction MR_PTSC_ESTDIRECTE retourne .TRUE. si la méthode de
C     résolution est directe.
C
C Entrée:
C     HOBJ        Handle sur l'objet
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_ESTDIRECTE(HOBJ)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_ESTDIRECTE
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER HOBJ

      INCLUDE 'mrptsc.fi'
C------------------------------------------------------------------------

      MR_PTSC_ESTDIRECTE = .FALSE.
      RETURN
      END

C************************************************************************
C Sommaire: Résous le système matriciel.
C
C Description:
C     La fonction MR_PTSC_XEQ résous complètement le système matriciel.
C     La matrice est dimensionnée et assemblée, le second membre est
C     assemblé puis le système résolu.
C
C Entrée:
C     HOBJ     Handle sur l'objet courant
C     HSIM     Handle sur la simulation
C     HALG     Handle sur l'algorithme, paramètre des fonctions F_xx
C     F_K      Fonction à appeler pour assembler la matrice
C     F_KU     Fonction à appeler pour assembler le produit K.U
C     F_F      Fonction à appeler pour assembler le second membre
C
C Sortie:
C     VSOL     Solution du système matriciel
C
C Notes:
C************************************************************************
      FUNCTION MR_PTSC_XEQ(HOBJ,
     &                     HSIM,
     &                     HALG,
     &                     F_K,
     &                     F_KU,
     &                     F_F,
     &                     VSOL)
CDEC$ IF DEFINED(MODE_DYNAMIC)
CDEC$    ATTRIBUTES DLLEXPORT :: MR_PTSC_XEQ
CDEC$ ENDIF

      IMPLICIT NONE

      INTEGER  HOBJ
      INTEGER  HSIM
      INTEGER  HALG
      INTEGER  F_K
      INTEGER  F_KU
      INTEGER  F_F
      REAL*8   VSOL(*)
      EXTERNAL F_K
      EXTERNAL F_KU
      EXTERNAL F_F

      INCLUDE 'mrptsc.fi'
      INCLUDE 'err.fi'
      INCLUDE 'c_petsc.fi'
      INCLUDE 'mrreso.fi'
      INCLUDE 'mxptsc.fi'
      INCLUDE 'soallc.fi'
      INCLUDE 'mrptsc.fc'

      INTEGER*8 XMAT, XSPT, XRHS, XINI
      INTEGER IERR
      INTEGER IOB
      INTEGER HMTX
      INTEGER LDLG
      INTEGER LLING
      INTEGER NEQL
      REAL*8  VDUM

      INTEGER F_PTSC_ERRMSG
      EXTERNAL MX_PTSC_ASMKE
C------------------------------------------------------------------------
D     CALL ERR_PRE(MR_PTSC_HVALIDE(HOBJ))
C------------------------------------------------------------------------

C---     RECUPERE LES ATTRIBUTS
      IOB = HOBJ - MR_PTSC_HBASE
      HMTX  = MR_PTSC_HMTX(IOB)
D     CALL ERR_ASR(MX_PTSC_HVALIDE(HMTX))

C---     DIMENSIONNE LA MATRICE
      IF (ERR_GOOD()) IERR = MX_PTSC_DIMMAT(HMTX, HSIM)
      IF (ERR_GOOD()) XMAT = MX_PTSC_REQXMAT(HMTX)

C---     ASSEMBLE LA MATRICE
      IF (ERR_GOOD()) IERR = F_K(HALG, HMTX, MX_PTSC_ASMKE)

C---     LANCE L'ASSEMBLAGE GLOBAL DE LA MATRICE
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_MAT_ASMBEGIN(XMAT)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     ASSEMBLE LE MEMBRE DE DROITE
      IF (ERR_GOOD()) IERR = F_F(HALG, VSOL, VDUM, .FALSE.)

C---     TERMINE L'ASSEMBLAGE GLOBAL DE LA MATRICE
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_MAT_ASMEND(XMAT)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     RECUPERE LES VARIABLES DE LA MATRICE
      IF (ERR_GOOD()) THEN
         NEQL  = MX_PTSC_REQNEQL (HMTX)
         LLING = MX_PTSC_REQLLING(HMTX)
      ENDIF

C---     CREE LA SOLUTION INITIALE
      XINI = 0
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_VEC_CREATE(XINI,
     &                            NEQL,
     &                            KA(SO_ALLC_REQKIND(KA,LLING)))
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     CREE LE VECTEUR RHS
      XRHS = 0
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_VEC_CREATE(XRHS,
     &                            NEQL,
     &                            KA(SO_ALLC_REQKIND(KA,LLING)))
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     CHARGE LE VECTEUR RHS
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_VEC_SETVAL(XRHS,
     &                            NEQL,
     &                            KA(SO_ALLC_REQKIND(KA,LLING)),
     &                            VSOL)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     CREE LE SOLVEUR PETSc
      XSPT = 0
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_SLES_CREATE(XSPT)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     RESOUS
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_SLES_SOLVE(XSPT, XMAT, XRHS, XINI)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     RECUPERE LE RESULTAT
      IF (ERR_GOOD()) THEN
         IERR = C_PTSC_VEC_GETVAL(XRHS,
     &                            NEQL,
     &                            KA(SO_ALLC_REQKIND(KA,LLING)),
     &                            VSOL)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     DETRUIS LE SOLVEUR PETSc
      IF (XSPT .NE. 0) THEN
         IERR = C_PTSC_SLES_DESTROY(XSPT)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     DETRUIS LE VECTEUR RHS
      IF (XRHS .NE. 0) THEN
         IERR = C_PTSC_VEC_DESTROY(XRHS)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

C---     DETRUIS LE VECTEUR SOLUTION INITIALE
      IF (XINI .NE. 0) THEN
         IERR = C_PTSC_VEC_DESTROY(XINI)
         IERR = F_PTSC_ERRMSG(IERR)
      ENDIF

      MR_PTSC_XEQ = ERR_TYP()
      RETURN
      END

C************************************************************************
C Sommaire:
C
C Description:
C
C Entrée:
C
C Sortie:
C
C Notes:
C************************************************************************
      FUNCTION F_PTSC_ERRMSG(IERR)

      IMPLICIT NONE

      INTEGER F_PTSC_ERRMSG
      INTEGER IERR

      INCLUDE 'err.fi'
      INCLUDE 'c_petsc.fi'

      INTEGER IDUM
C------------------------------------------------------------------------
C------------------------------------------------------------------------

      IF (IERR .NE. 0) THEN
         CALL ERR_ASG(ERR_ERR, 'ERR_PETSC_RUN_TIME_ERROR')
         IDUM = C_PTSC_ERRMSG(IERR, ERR_BUF)
         IF (IDUM .EQ. 0) CALL ERR_AJT(ERR_BUF)
      ENDIF

      F_PTSC_ERRMSG = ERR_TYP()
      RETURN
      END
